.class public Lcom/marco/popUp/PopUp;
.super Ljava/lang/Object;
.source "PopUp.java"


# static fields
.field private static bottom:Landroid/widget/TextView; = null

.field private static change40:Ljava/lang/String; = "<b>Changelog 4.0</b>\n- built in XMLs (/GCam/Configs7/Minilux): G-Type, Juan All, Juan SRZ, Pretty, Sharp & Natural, X-Type\n- added XML, AWB, Astro buttons on ViewFinder\n- added XML menu, Standard configurations, ViewFinder buttons and Special settings to the settings\n- description of the xmls, hide/show them\n- Standard configurations: quick switch between xmls for up to 4 specific scenarios\n- ViewFinder buttons: customize and hide/show the buttons\n- Special settings: additional stuff that got added\n- removed hdr off\n- many settings are made persistent (unchangeable with xmls)\n- added a pop-up on first start with quick info / first steps\n"

.field private static change401:Ljava/lang/String; = "<b>Hotfix 4.0.1</b>\n- updated juan all XML\n- fixed button positions + doubled awb button when background off\n- made gestures, faces, social persistent\n- made grid persistent + added it to special settings\n- added Luxportrait and Quick Shot! XMLs\n"

.field private static change402:Ljava/lang/String; = "<b>Hotfix 4.0.2</b>\n- fixed no xmls issue\n- added 12mp mode\n- added more options to hdr+ frames\n- added hdr+ extra XML\n"

.field private static change41:Ljava/lang/String; = "<b>Changelog 4.1</b>\n- rebased on Urnyx05 v1.7, thanks to him\n- added user xml section and moved existing xmls to advanced xmls (does not mean they are better, these are just from MJL team members)\n- many new xmls\n- new standard configurations for: sport, nature and zoom\n- new iconset for standard configurations (you can choose between new and old in the settings)\n- added a bar for switching standard configuration directly when clicking xml button (can be turned on/off), the bar contains only standard configurations with a set xml\n- added base model selection (front/back), exposure compensation, exposure correction (time) separate for hdr+e / nightsight / astro, jpg compression rate\n- added Pixel 4 awb\n- added shortcuts to nightsight and portrait\n- added post processing from Miniflex, be aware: it takes few seconds to apply to the picture\n- added post processing options: vignette, brightness, sepia and saturation\n- updated post processing examples\n- removed vibration when camera mode changes\n"

.field private static change42:Ljava/lang/String; = "<b>Changelog 4.2</b>\n- rebased on Urnyx05 v2.0, thanks to him\n- removed 12/16mp mode (too many issues), now included xmls are all 12mp only\n- added option to set a limit to file size (e.g. < 10MB)\n- added advanced Tone Curve to post processing\n- and some new and updated XMLs\n"

.field private static change43:Ljava/lang/String; = "<b>Changelog 4.3</b>\n- rebased on Urnyx05 v2.1, thanks to him\n- fixed front camera video (thx Urnyx)\n- fixed tap to focus issue (thx Metzger100)\n- added XML update/delete/upload functionality from Miniqueta\n- added XML popup for new XMLs and download progressbar from Miniqueta\n- added default mode + selfie mode \n- added reset setting\n"

.field private static guide1:Ljava/lang/String; = "<b>1 XML loading area</b>\nDouble tap inside one of the marked areas to open the XML pop up.\nHere you can open a drop down menu and select an XML matching the scenario you want to take a picture of.\n"

.field private static guide2a:Ljava/lang/String; = "<b>2a XML button</b>\nWhen this button is empty, you have no XML loaded, follow <b>1</b> to change this.\nIf an XML is loaded, the button will show the last character (or for better recognition an emoji) of the currently loaded XML.\nBy long pressing this button, a description of the XML opens up with more info about the XML and for which scenario(s) it is made for.\nInside the setting: \"Standard configurations\" you can set XMLs for specific scenarios. This allows you to click the XML button once to jump between scenarios. In addition the button icon changes to the scenario icon (instead of the last character/emoji) when chosen XML is loaded.\n"

.field private static guide2b:Ljava/lang/String; = "<b>2b XML bar</b>\nThis bar appears after clicking <b>2a</b>.\nAll standard configurations where you set a XML for will be shown here.\nClick on an icon to activate the set XML for the chosen scenario.\nThe bar will disappear afterwards.\n"

.field private static guide3:Ljava/lang/String; = "<b>3 AWB button</b>\nAuto White Balance. It tries to shift the colors in the image to create a \"real\" white (different from what your eyes see).\nA rule of thumb for this: off in daylight, on in low or artificial light.\n"

.field private static guide4:Ljava/lang/String; = "<b>4 Astro button</b>\nThis button is only visible in NightSight mode and turns the AstroPhotography feature on or off.\nWhen activated, you can take pictures of the stars, it captures more light and the capture time is increased.\nUse it only when the phone is on a tripod.\n"

.field private static image:Landroid/widget/ImageView; = null

.field private static sc:Landroid/widget/ScrollView; = null

.field private static welcome:Ljava/lang/String; = "<h2>Welcome to MJL v4.3 Minilux</h2><font color=\"red\"><b>To show this message again, long press the AWB button.</b>\nPlease take your time and read this pop up message thoroughly.</font>\n\nMJL has built in XMLs, adds a lot of quality of life improvements and customizability.\nSince version 4.1 it has some unique built in post processing options which give you the best quality out of every GCam out there.\nHere are some general tips, guides and first steps:\n\n<b>Settings</b>\nMost settings which do not influence image quality are fixed/persisten and <b>can not</b> be changed by XMLs.\n<b>You</b> have to go into the settings (long press camera switch button or pull down from top) and customize them to your own preference (e.g. Camera Sound, Location, ViewFinder buttons...)\n\n<b>XMLs</b>\nXMLs are saved configuration files for different scenarios (so it is quick and easy to switch between them and you don\'t have to adjust every setting manually).\nThey are saved in a separate folder so they don\'t get mixed up with xml files for different GCams. Use the provided XMLs only with this mod.\n\n<b>Interface</b>"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/widget/ImageView;
    .locals 1

    .line 28
    sget-object v0, Lcom/marco/popUp/PopUp;->image:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 28
    invoke-static {p0}, Lcom/marco/popUp/PopUp;->chooseText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200()Landroid/widget/TextView;
    .locals 1

    .line 28
    sget-object v0, Lcom/marco/popUp/PopUp;->bottom:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300()Landroid/widget/ScrollView;
    .locals 1

    .line 28
    sget-object v0, Lcom/marco/popUp/PopUp;->sc:Landroid/widget/ScrollView;

    return-object v0
.end method

.method public static askingXML()V
    .locals 4

    .line 42
    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget-object v1, Lcom/marco/FixMarco;->cameraActivity:Lcom/google/android/apps/camera/legacy/app/activity/main/CameraActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Update popup"

    .line 43
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 44
    new-instance v1, Landroid/widget/TextView;

    sget-object v2, Lcom/marco/FixMarco;->cameraActivity:Lcom/google/android/apps/camera/legacy/app/activity/main/CameraActivity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/16 v2, 0x32

    const/16 v3, 0x14

    .line 45
    invoke-virtual {v1, v2, v3, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    const/high16 v2, 0x41900000    # 18.0f

    .line 46
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 47
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const/4 v2, 0x0

    .line 48
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const-string v2, "If you want to get a popup when a new XML is online click yes, if you don\'t want to get a popup click no.\nThis can be changed later in Settings -> XML Menu."

    .line 51
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    new-instance v1, Lcom/marco/popUp/PopUp$1;

    invoke-direct {v1}, Lcom/marco/popUp/PopUp$1;-><init>()V

    const-string v2, "Yes"

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 69
    new-instance v1, Lcom/marco/popUp/PopUp$2;

    invoke-direct {v1}, Lcom/marco/popUp/PopUp$2;-><init>()V

    const-string v2, "No"

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 80
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private static chooseText(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 248
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_0

    :sswitch_0
    const-string v0, "4.1.1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 p0, 0x8

    goto/16 :goto_1

    :sswitch_1
    const-string v0, "4.0.2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x7

    goto :goto_1

    :sswitch_2
    const-string v0, "4.0.1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x6

    goto :goto_1

    :sswitch_3
    const-string v0, "4.3"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 p0, 0xa

    goto :goto_1

    :sswitch_4
    const-string v0, "4.2"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/16 p0, 0x9

    goto :goto_1

    :sswitch_5
    const-string v0, "4.0"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x5

    goto :goto_1

    :sswitch_6
    const-string v0, "2b"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x2

    goto :goto_1

    :sswitch_7
    const-string v0, "2a"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_1

    :sswitch_8
    const-string v0, "4"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x4

    goto :goto_1

    :sswitch_9
    const-string v0, "3"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x3

    goto :goto_1

    :sswitch_a
    const-string v0, "1"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    goto :goto_1

    :cond_0
    :goto_0
    const/4 p0, -0x1

    :goto_1
    packed-switch p0, :pswitch_data_0

    const-string p0, ""

    return-object p0

    .line 270
    :pswitch_0
    sget-object p0, Lcom/marco/popUp/PopUp;->change43:Ljava/lang/String;

    return-object p0

    .line 268
    :pswitch_1
    sget-object p0, Lcom/marco/popUp/PopUp;->change42:Ljava/lang/String;

    return-object p0

    .line 266
    :pswitch_2
    sget-object p0, Lcom/marco/popUp/PopUp;->change41:Ljava/lang/String;

    return-object p0

    .line 264
    :pswitch_3
    sget-object p0, Lcom/marco/popUp/PopUp;->change402:Ljava/lang/String;

    return-object p0

    .line 262
    :pswitch_4
    sget-object p0, Lcom/marco/popUp/PopUp;->change401:Ljava/lang/String;

    return-object p0

    .line 260
    :pswitch_5
    sget-object p0, Lcom/marco/popUp/PopUp;->change40:Ljava/lang/String;

    return-object p0

    .line 258
    :pswitch_6
    sget-object p0, Lcom/marco/popUp/PopUp;->guide4:Ljava/lang/String;

    return-object p0

    .line 256
    :pswitch_7
    sget-object p0, Lcom/marco/popUp/PopUp;->guide3:Ljava/lang/String;

    return-object p0

    .line 254
    :pswitch_8
    sget-object p0, Lcom/marco/popUp/PopUp;->guide2b:Ljava/lang/String;

    return-object p0

    .line 252
    :pswitch_9
    sget-object p0, Lcom/marco/popUp/PopUp;->guide2a:Ljava/lang/String;

    return-object p0

    .line 250
    :pswitch_a
    sget-object p0, Lcom/marco/popUp/PopUp;->guide1:Ljava/lang/String;

    return-object p0

    nop

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_a
        0x33 -> :sswitch_9
        0x34 -> :sswitch_8
        0x66f -> :sswitch_7
        0x670 -> :sswitch_6
        0xc8f6 -> :sswitch_5
        0xc8f8 -> :sswitch_4
        0xc8f9 -> :sswitch_3
        0x2f26939 -> :sswitch_2
        0x2f2693a -> :sswitch_1
        0x2f26cfa -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static firstStart()V
    .locals 2

    .line 35
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "oneplus6"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    invoke-static {}, Lcom/marco/popUp/PopUp;->warningPopup()V

    goto :goto_0

    .line 38
    :cond_0
    invoke-static {}, Lcom/marco/popUp/PopUp;->guidePopup()V

    :goto_0
    return-void
.end method

.method public static foundXML()V
    .locals 5

    .line 109
    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget-object v1, Lcom/marco/FixMarco;->cameraActivity:Lcom/google/android/apps/camera/legacy/app/activity/main/CameraActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "New XML available"

    .line 110
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 111
    new-instance v1, Landroid/widget/TextView;

    sget-object v2, Lcom/marco/FixMarco;->cameraActivity:Lcom/google/android/apps/camera/legacy/app/activity/main/CameraActivity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/16 v2, 0x32

    const/16 v3, 0x14

    .line 112
    invoke-virtual {v1, v2, v3, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    const/high16 v2, 0x41900000    # 18.0f

    .line 113
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 114
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const-string v2, "A change was detect. This could be either:\n- a new XML was added\n- an existing XML got updated or\n- an old XML got removed.\nBy canceling this popup will not show up again until next change is detected."

    .line 120
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    .line 121
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 122
    new-instance v1, Lcom/marco/popUp/PopUp$4;

    invoke-direct {v1}, Lcom/marco/popUp/PopUp$4;-><init>()V

    const-string v2, "Update"

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 128
    new-instance v1, Lcom/marco/popUp/PopUp$5;

    invoke-direct {v1}, Lcom/marco/popUp/PopUp$5;-><init>()V

    const-string v2, "Cancel"

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 134
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/marco/popUp/PopUp$6;

    invoke-direct {v2, v0}, Lcom/marco/popUp/PopUp$6;-><init>(Landroid/app/AlertDialog$Builder;)V

    const-wide/16 v3, 0x96

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private static guideButton(Ljava/lang/String;I)Landroid/widget/Button;
    .locals 3

    .line 277
    new-instance v0, Landroid/widget/Button;

    sget-object v1, Lcom/marco/FixMarco;->staticContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 278
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v2, 0x384

    div-int/2addr v2, p1

    const/4 p1, -0x2

    invoke-direct {v1, v2, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 279
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 280
    invoke-static {}, Lcom/marco/popUp/PopUp;->guideButtonOnClick()Landroid/view/View$OnClickListener;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method private static guideButtonOnClick()Landroid/view/View$OnClickListener;
    .locals 1

    .line 285
    new-instance v0, Lcom/marco/popUp/PopUp$9;

    invoke-direct {v0}, Lcom/marco/popUp/PopUp$9;-><init>()V

    return-object v0
.end method

.method public static guidePopup()V
    .locals 14

    .line 174
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 175
    new-instance v1, Landroid/widget/ScrollView;

    sget-object v2, Lcom/marco/FixMarco;->staticContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/marco/popUp/PopUp;->sc:Landroid/widget/ScrollView;

    .line 176
    new-instance v1, Landroid/widget/LinearLayout;

    sget-object v2, Lcom/marco/FixMarco;->staticContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 177
    new-instance v2, Landroid/widget/TextView;

    sget-object v3, Lcom/marco/FixMarco;->staticContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 178
    new-instance v3, Landroid/widget/ImageView;

    sget-object v4, Lcom/marco/FixMarco;->staticContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sput-object v3, Lcom/marco/popUp/PopUp;->image:Landroid/widget/ImageView;

    .line 179
    new-instance v3, Landroid/widget/TextView;

    sget-object v4, Lcom/marco/FixMarco;->staticContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    sput-object v3, Lcom/marco/popUp/PopUp;->bottom:Landroid/widget/TextView;

    .line 180
    new-instance v3, Landroid/widget/LinearLayout;

    sget-object v4, Lcom/marco/FixMarco;->staticContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 181
    new-instance v4, Landroid/widget/TextView;

    sget-object v5, Lcom/marco/FixMarco;->staticContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 182
    new-instance v5, Landroid/widget/LinearLayout;

    sget-object v6, Lcom/marco/FixMarco;->staticContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v6, 0x1

    .line 184
    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    const/16 v6, 0x11

    .line 185
    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 186
    sget-object v7, Lcom/marco/popUp/PopUp;->sc:Landroid/widget/ScrollView;

    invoke-virtual {v7, v0}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 187
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v7, 0x0

    .line 188
    invoke-virtual {v3, v7}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 189
    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 190
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 191
    invoke-virtual {v5, v7}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 192
    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 193
    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 194
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 195
    sget-object v6, Lcom/marco/popUp/PopUp;->bottom:Landroid/widget/TextView;

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 196
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v0, 0x14

    .line 197
    invoke-virtual {v2, v0, v7, v0, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 198
    sget-object v6, Lcom/marco/popUp/PopUp;->bottom:Landroid/widget/TextView;

    invoke-virtual {v6, v0, v7, v0, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 199
    invoke-virtual {v4, v0, v7, v0, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 200
    sget-object v6, Lcom/marco/popUp/PopUp;->image:Landroid/widget/ImageView;

    invoke-virtual {v6, v0, v7, v0, v7}, Landroid/widget/ImageView;->setPadding(IIII)V

    const/high16 v0, 0x41800000    # 16.0f

    .line 201
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 202
    sget-object v6, Lcom/marco/popUp/PopUp;->bottom:Landroid/widget/TextView;

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 203
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setTextSize(F)V

    const-string v0, "1"

    const-string v6, "2a"

    const-string v8, "2b"

    const-string v9, "3"

    const-string v10, "4"

    .line 205
    filled-new-array {v0, v6, v8, v9, v10}, [Ljava/lang/String;

    move-result-object v0

    move v6, v7

    :goto_0
    const/4 v8, 0x5

    if-ge v6, v8, :cond_0

    .line 206
    aget-object v9, v0, v6

    .line 207
    invoke-static {v9, v8}, Lcom/marco/popUp/PopUp;->guideButton(Ljava/lang/String;I)Landroid/widget/Button;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x6

    const-string v8, "4.0"

    const-string v9, "4.0.1"

    const-string v10, "4.0.2"

    const-string v11, "4.1.1"

    const-string v12, "4.2"

    const-string v13, "4.3"

    .line 208
    filled-new-array/range {v8 .. v13}, [Ljava/lang/String;

    move-result-object v6

    move v8, v7

    :goto_1
    if-ge v8, v0, :cond_1

    .line 209
    aget-object v9, v6, v8

    .line 210
    invoke-static {v9, v0}, Lcom/marco/popUp/PopUp;->guideButton(Ljava/lang/String;I)Landroid/widget/Button;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 212
    :cond_1
    sget-object v0, Lcom/marco/popUp/PopUp;->welcome:Ljava/lang/String;

    const-string v6, "\n"

    const-string v8, "<br>"

    invoke-virtual {v0, v6, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v7}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    sget-object v0, Lcom/marco/popUp/PopUp;->image:Landroid/widget/ImageView;

    const-string v9, "interface.jpg"

    invoke-static {v9}, Lcom/marco/fixes/Fixes;->bitmapFromAssets(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 216
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "<b>Click a button above to show the provided info for that number or to see the changelogs.</b>\n"

    .line 217
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    sget-object v9, Lcom/marco/popUp/PopUp;->bottom:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v6, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v7}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "<b>Changelogs</b>"

    .line 219
    invoke-virtual {v0, v6, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v7}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 222
    sget-object v0, Lcom/marco/popUp/PopUp;->image:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 223
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 224
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 225
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 226
    sget-object v0, Lcom/marco/popUp/PopUp;->bottom:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 227
    sget-object v0, Lcom/marco/popUp/PopUp;->sc:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 229
    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget-object v1, Lcom/marco/FixMarco;->cameraActivity:Lcom/google/android/apps/camera/legacy/app/activity/main/CameraActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 231
    sget-object v1, Lcom/marco/popUp/PopUp;->sc:Landroid/widget/ScrollView;

    .line 232
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 233
    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/marco/popUp/PopUp$8;

    invoke-direct {v2}, Lcom/marco/popUp/PopUp$8;-><init>()V

    const-string v3, "Okay"

    .line 234
    invoke-virtual {v1, v3, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 240
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 241
    sget v1, Lcom/marco/FixMarco;->screenx:I

    sget v2, Lcom/marco/FixMarco;->screeny:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 242
    sget v2, Lcom/marco/FixMarco;->screenx:I

    sget v3, Lcom/marco/FixMarco;->screeny:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-double v2, v2

    const-wide v4, 0x3fec28f5c28f5c29L    # 0.88

    mul-double/2addr v2, v4

    double-to-int v2, v2

    .line 243
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 244
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    return-void
.end method

.method public static updatefailed()V
    .locals 4

    .line 84
    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget-object v1, Lcom/marco/FixMarco;->cameraActivity:Lcom/google/android/apps/camera/legacy/app/activity/main/CameraActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Update Failed..."

    .line 85
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 86
    new-instance v1, Landroid/widget/TextView;

    sget-object v2, Lcom/marco/FixMarco;->cameraActivity:Lcom/google/android/apps/camera/legacy/app/activity/main/CameraActivity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/16 v2, 0x32

    const/16 v3, 0x14

    .line 87
    invoke-virtual {v1, v2, v3, v2, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    const/high16 v2, 0x41900000    # 18.0f

    .line 88
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 89
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const-string v2, "Try the following things:\n- restart phone\n- WiFi AND mobile data\n- firewall/adguard/vpn OFF\n- vpn to germany\nIf those steps don\'t help:\n- send a log to the group\nA log guide is in pinned message."

    .line 98
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    .line 99
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 100
    new-instance v1, Lcom/marco/popUp/PopUp$3;

    invoke-direct {v1}, Lcom/marco/popUp/PopUp$3;-><init>()V

    const-string v2, "Ok"

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 105
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private static warningPopup()V
    .locals 4

    .line 148
    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget-object v1, Lcom/marco/FixMarco;->cameraActivity:Lcom/google/android/apps/camera/legacy/app/activity/main/CameraActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Warning!"

    .line 149
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "This GCam is modded for <font color=\"red\"><b>OnePlus 6</b></font> and <font color=\"red\"><b>OnePlus 6t</b></font>.\n\nYou are using a different, <font color=\"red\"><b>not supported</b></font> phone.\n\nBe aware you <b>will</b> face errors and bugs which will not be fixed."

    const-string v2, "\n"

    const-string v3, "<br>"

    .line 150
    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 151
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "I don\'t care."

    const/4 v2, 0x0

    .line 152
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 153
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    const/4 v1, -0x2

    .line 155
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 156
    new-instance v2, Lcom/marco/popUp/PopUp$7;

    invoke-direct {v2, v0}, Lcom/marco/popUp/PopUp$7;-><init>(Landroid/app/AlertDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
