.class final Lcom/marco/popUp/PopUp$7;
.super Ljava/lang/Object;
.source "PopUp.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/marco/popUp/PopUp;->warningPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$alertDialog:Landroid/app/AlertDialog;


# direct methods
.method constructor <init>(Landroid/app/AlertDialog;)V
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/marco/popUp/PopUp$7;->val$alertDialog:Landroid/app/AlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 159
    iget-object p1, p0, Lcom/marco/popUp/PopUp$7;->val$alertDialog:Landroid/app/AlertDialog;

    const-string v0, "<font color=\"red\"><b>This message will stay 12 seconds more to give you time to read again and understand it.</b></font>\n\nThis GCam is modded for <font color=\"red\"><b>OnePlus 6</b></font> and <font color=\"red\"><b>OnePlus 6t</b></font>.\n\nYou are using a different, <font color=\"red\"><b>not supported</b></font> phone.\n\nBe aware you <b>will</b> face errors and bugs which will not be fixed.\n\n<font color=\"red\"><b>This message will stay 12 seconds more to give you time to read again and understand it.</b></font>"

    const-string v1, "\n"

    const-string v2, "<br>"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 160
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v0, Lcom/marco/popUp/PopUp$7$1;

    invoke-direct {v0, p0}, Lcom/marco/popUp/PopUp$7$1;-><init>(Lcom/marco/popUp/PopUp$7;)V

    const-wide/16 v1, 0x2ee0

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 167
    iget-object p1, p0, Lcom/marco/popUp/PopUp$7;->val$alertDialog:Landroid/app/AlertDialog;

    const/4 v0, -0x2

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object p1

    const-string v1, ""

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object p1, p0, Lcom/marco/popUp/PopUp$7;->val$alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
