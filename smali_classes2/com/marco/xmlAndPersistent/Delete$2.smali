.class Lcom/marco/xmlAndPersistent/Delete$2;
.super Ljava/lang/Object;
.source "Delete.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/marco/xmlAndPersistent/Delete;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/marco/xmlAndPersistent/Delete;


# direct methods
.method constructor <init>(Lcom/marco/xmlAndPersistent/Delete;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/marco/xmlAndPersistent/Delete$2;->this$0:Lcom/marco/xmlAndPersistent/Delete;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .line 63
    :try_start_0
    new-instance p1, Ljava/io/FileWriter;

    iget-object p2, p0, Lcom/marco/xmlAndPersistent/Delete$2;->this$0:Lcom/marco/xmlAndPersistent/Delete;

    invoke-static {p2}, Lcom/marco/xmlAndPersistent/Delete;->access$100(Lcom/marco/xmlAndPersistent/Delete;)Ljava/io/File;

    move-result-object p2

    const/4 v0, 0x1

    invoke-direct {p1, p2, v0}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 65
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    const/4 p1, 0x0

    :goto_0
    const/4 p2, 0x0

    move v0, p2

    .line 67
    :goto_1
    iget-object v1, p0, Lcom/marco/xmlAndPersistent/Delete$2;->this$0:Lcom/marco/xmlAndPersistent/Delete;

    invoke-static {v1}, Lcom/marco/xmlAndPersistent/Delete;->access$000(Lcom/marco/xmlAndPersistent/Delete;)[Z

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 68
    iget-object v1, p0, Lcom/marco/xmlAndPersistent/Delete$2;->this$0:Lcom/marco/xmlAndPersistent/Delete;

    invoke-static {v1}, Lcom/marco/xmlAndPersistent/Delete;->access$000(Lcom/marco/xmlAndPersistent/Delete;)[Z

    move-result-object v1

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_0

    .line 69
    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/marco/strings/MarcosStrings;->xmlPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/marco/xmlAndPersistent/Delete$2;->this$0:Lcom/marco/xmlAndPersistent/Delete;

    invoke-static {v4}, Lcom/marco/xmlAndPersistent/Delete;->access$200(Lcom/marco/xmlAndPersistent/Delete;)[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    if-eqz p1, :cond_0

    .line 72
    :try_start_1
    iget-object v1, p0, Lcom/marco/xmlAndPersistent/Delete$2;->this$0:Lcom/marco/xmlAndPersistent/Delete;

    invoke-static {v1}, Lcom/marco/xmlAndPersistent/Delete;->access$200(Lcom/marco/xmlAndPersistent/Delete;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-virtual {p1, v1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    const-string v1, "\n"

    .line 73
    invoke-virtual {p1, v1}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    .line 76
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    if-eqz p1, :cond_2

    .line 81
    :try_start_2
    invoke-virtual {p1}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_3

    :catch_2
    move-exception p1

    .line 83
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    .line 85
    :cond_2
    :goto_3
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 86
    :goto_4
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/Delete$2;->this$0:Lcom/marco/xmlAndPersistent/Delete;

    invoke-static {v0}, Lcom/marco/xmlAndPersistent/Delete;->access$000(Lcom/marco/xmlAndPersistent/Delete;)[Z

    move-result-object v0

    array-length v0, v0

    if-ge p2, v0, :cond_4

    .line 87
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/Delete$2;->this$0:Lcom/marco/xmlAndPersistent/Delete;

    invoke-static {v0}, Lcom/marco/xmlAndPersistent/Delete;->access$000(Lcom/marco/xmlAndPersistent/Delete;)[Z

    move-result-object v0

    aget-boolean v0, v0, p2

    if-nez v0, :cond_3

    .line 88
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/Delete$2;->this$0:Lcom/marco/xmlAndPersistent/Delete;

    invoke-static {v0}, Lcom/marco/xmlAndPersistent/Delete;->access$200(Lcom/marco/xmlAndPersistent/Delete;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p2

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 p2, p2, 0x1

    goto :goto_4

    .line 90
    :cond_4
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p2

    if-lez p2, :cond_5

    .line 91
    iget-object p2, p0, Lcom/marco/xmlAndPersistent/Delete$2;->this$0:Lcom/marco/xmlAndPersistent/Delete;

    invoke-static {p2, p1}, Lcom/marco/xmlAndPersistent/Delete;->access$300(Lcom/marco/xmlAndPersistent/Delete;Ljava/util/ArrayList;)V

    :cond_5
    return-void
.end method
