.class public Lcom/marco/xmlAndPersistent/AsyncFTPdownload;
.super Landroid/os/AsyncTask;
.source "AsyncFTPdownload.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private after:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private before:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private changed:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private changes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private del:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private dir:Ljava/io/File;

.field private ftp:Lorg/apache/commons/net/ftp/FTPClient;

.field private ip:Ljava/lang/String;

.field private password:Ljava/lang/String;

.field private portNumber:I

.field private previx:Ljava/lang/String;

.field private progressbar:Landroid/widget/ProgressBar;

.field private succsess:Z

.field private toBeRenamed:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private user:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 44
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/16 v0, 0x15

    .line 49
    iput v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->portNumber:I

    const-string v0, "user"

    .line 50
    iput-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->user:Ljava/lang/String;

    const-string v0, ""

    .line 51
    iput-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->password:Ljava/lang/String;

    const-string v0, "mini-"

    .line 54
    iput-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->previx:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/marco/xmlAndPersistent/AsyncFTPdownload;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->changesPopupWindow()V

    return-void
.end method

.method private changesPopupString()Ljava/lang/String;
    .locals 6

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {}, Lcom/marco/fixes/Fixes;->xmlFiles()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->after:Ljava/util/ArrayList;

    .line 146
    new-instance v1, Ljava/util/TreeSet;

    sget-object v2, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-direct {v1, v2}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 147
    iget-object v2, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->after:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 148
    iget-object v4, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->before:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 149
    invoke-virtual {v1, v3}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 150
    :cond_1
    invoke-virtual {v1}, Ljava/util/TreeSet;->size()I

    move-result v2

    const-string v3, "\n"

    if-lez v2, :cond_2

    const-string v2, "Added: \n"

    .line 151
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 152
    invoke-virtual {v1}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 153
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 156
    :cond_2
    new-instance v1, Ljava/util/TreeSet;

    sget-object v2, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-direct {v1, v2}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 157
    iget-object v2, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->changed:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 158
    iget-object v5, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->before:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 159
    invoke-virtual {v1, v4}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 160
    :cond_4
    invoke-virtual {v1}, Ljava/util/TreeSet;->size()I

    move-result v2

    if-lez v2, :cond_5

    const-string v2, "Updated: \n"

    .line 161
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    invoke-virtual {v1}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 163
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 166
    :cond_5
    new-instance v1, Ljava/util/TreeSet;

    sget-object v2, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-direct {v1, v2}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 167
    iget-object v2, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->before:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 168
    iget-object v5, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->after:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 169
    invoke-virtual {v1, v4}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 170
    :cond_7
    invoke-virtual {v1}, Ljava/util/TreeSet;->size()I

    move-result v2

    if-lez v2, :cond_8

    const-string v2, "Removed: \n"

    .line 171
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    invoke-virtual {v1}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 173
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 176
    :cond_8
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    if-ne v1, v2, :cond_9

    const-string v0, "Nothing changed."

    return-object v0

    .line 178
    :cond_9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private changesPopupWindow()V
    .locals 5

    .line 122
    sget-object v0, Lcom/marco/FixMarco;->staticSettingsContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/marco/FixMarco;->staticSettingsContext:Landroid/content/Context;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/marco/FixMarco;->cameraActivity:Lcom/google/android/apps/camera/legacy/app/activity/main/CameraActivity;

    .line 123
    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 124
    new-instance v2, Landroid/widget/ScrollView;

    invoke-direct {v2, v0}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    const-string v3, "Changes:"

    .line 125
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 126
    new-instance v3, Landroid/widget/TextView;

    invoke-direct {v3, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/16 v0, 0x32

    const/16 v4, 0x14

    .line 127
    invoke-virtual {v3, v0, v4, v0, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    const/high16 v0, 0x41900000    # 18.0f

    .line 128
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 129
    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 130
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 131
    invoke-direct {p0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->changesPopupString()Ljava/lang/String;

    move-result-object v0

    .line 132
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    .line 133
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 134
    new-instance v0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload$3;

    invoke-direct {v0, p0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload$3;-><init>(Lcom/marco/xmlAndPersistent/AsyncFTPdownload;)V

    const-string v2, "Close"

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 139
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private getIPv4()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Ljava/net/UnknownHostException;
        }
    .end annotation

    .line 367
    new-instance v0, Ljava/net/URL;

    sget-object v1, Lcom/marco/strings/MarcosStrings;->server:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/InetAddress;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v0

    if-nez v0, :cond_0

    .line 369
    sget-object v1, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    const-string v2, "Getting address failed."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v1, ""

    .line 370
    iput-object v1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ip:Ljava/lang/String;

    .line 371
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    .line 372
    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v3

    .line 373
    sget-object v4, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Check IP: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    invoke-static {v3}, Lcom/marco/fixes/Fixes;->validIP(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 375
    iput-object v3, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ip:Ljava/lang/String;

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 377
    :cond_2
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ip:Ljava/lang/String;

    invoke-static {v0}, Lcom/marco/fixes/Fixes;->validIP(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 378
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    const-string v1, "No IPv4 address found."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method

.method private redownloadmissing(Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const-string v0, "/"

    .line 324
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 325
    sget-object v1, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "minilist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v1, v3, :cond_0

    .line 328
    sget-object p1, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    const-string v0, "Download completed!"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    .line 331
    :cond_0
    sget-object v1, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Download incomplete! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    sub-int/2addr v4, v2

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, " XMLs missing!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    .line 335
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    move v3, v2

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v3, v4, :cond_1

    :goto_1
    if-ge v3, v4, :cond_1

    .line 338
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mini-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, ".xml"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    add-int/2addr v3, v2

    goto :goto_0

    .line 344
    :cond_2
    invoke-virtual {v1}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 347
    :try_start_0
    new-instance v3, Ljava/io/BufferedOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    new-instance v5, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->dir:Ljava/io/File;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    const-string v4, "-"

    .line 348
    invoke-virtual {v1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x4

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    .line 349
    iget-object v5, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/marco/strings/MarcosStrings;->gcamversion:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Lorg/apache/commons/net/ftp/FTPClient;->retrieveFile(Ljava/lang/String;Ljava/io/OutputStream;)Z

    move-result v5
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v6, "Downloaded: "

    if-eqz v5, :cond_3

    .line 350
    :try_start_1
    sget-object v3, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    iget-object v1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->toBeRenamed:Ljava/util/LinkedList;

    invoke-virtual {v1, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 352
    :cond_3
    iget-object v5, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/marco/strings/MarcosStrings;->gcamversion:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7, v3}, Lorg/apache/commons/net/ftp/FTPClient;->retrieveFile(Ljava/lang/String;Ljava/io/OutputStream;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 353
    sget-object v3, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    iget-object v1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->toBeRenamed:Ljava/util/LinkedList;

    invoke-virtual {v1, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 356
    :cond_4
    sget-object v3, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NOT downloaded: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->dir:Ljava/io/File;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v1

    .line 360
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    :cond_5
    return-void
.end method

.method private refactor()V
    .locals 7

    .line 469
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->toBeRenamed:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 470
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->toBeRenamed:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 471
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->dir:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->previx:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ".xml"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    invoke-static {v2}, Lcom/marco/fixes/Fixes;->getLastLineFromFile(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    .line 473
    iget-object v3, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->changes:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 474
    iget-object v3, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->changed:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 475
    :cond_0
    invoke-static {v2}, Lcom/marco/fixes/Fixes;->removeLastLineFromFile(Ljava/io/File;)V

    .line 476
    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->dir:Ljava/io/File;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 478
    invoke-static {v2, v3}, Lcom/marco/fixes/Fixes;->createFileCopy(Ljava/io/File;Ljava/io/File;)V

    .line 479
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private removeXMLs()V
    .locals 6

    .line 414
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/marco/strings/MarcosStrings;->xmlPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "/.dellist"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/marco/strings/MarcosStrings;->xmlPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "/del.txt"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    const-string v3, "Missing: "

    if-nez v2, :cond_0

    .line 417
    sget-object v2, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 419
    sget-object v2, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    :cond_1
    iget-object v2, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->previx:Ljava/lang/String;

    invoke-static {v2}, Lcom/marco/fixes/Fixes;->removeDownlodedXmls(Ljava/lang/String;)V

    .line 423
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->del:Ljava/util/ArrayList;

    .line 424
    invoke-direct {p0, v2, v0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->xmlsToDelete(Ljava/util/ArrayList;Ljava/io/File;)V

    .line 425
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->del:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->xmlsToDelete(Ljava/util/ArrayList;Ljava/io/File;)V

    .line 426
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 428
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->del:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_2

    return-void

    .line 431
    :cond_2
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->del:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 432
    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/marco/strings/MarcosStrings;->xmlPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 434
    sget-object v1, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Deleting: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    return-void
.end method

.method private retry()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, " : "

    const-string v1, "Files in: "

    const-string v2, "Try with again witz WiFi or Mobile Data!"

    const-string v3, "Something (e.g. a firewall) is blocking your traffic!"

    const-string v4, "Zero files! Maybe firewall"

    .line 385
    iget-object v5, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v5}, Lorg/apache/commons/net/ftp/FTPClient;->logout()Z

    .line 386
    iget-object v5, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v5}, Lorg/apache/commons/net/ftp/FTPClient;->disconnect()V

    .line 389
    :try_start_0
    iget-object v5, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    const-string v6, "UTF-8"

    invoke-virtual {v5, v6}, Lorg/apache/commons/net/ftp/FTPClient;->setControlEncoding(Ljava/lang/String;)V

    .line 390
    iget-object v5, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lorg/apache/commons/net/ftp/FTPClient;->setAutodetectUTF8(Z)V

    .line 391
    iget-object v5, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v5}, Lorg/apache/commons/net/ftp/FTPClient;->enterLocalPassiveMode()V

    .line 392
    sget-object v5, Lcom/marco/strings/MarcosStrings;->serverv6:Ljava/lang/String;

    iput-object v5, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ip:Ljava/lang/String;

    .line 393
    iget-object v6, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    iget v7, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->portNumber:I

    invoke-virtual {v6, v5, v7}, Lorg/apache/commons/net/ftp/FTPClient;->connect(Ljava/lang/String;I)V

    .line 394
    iget-object v5, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    iget-object v6, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->user:Ljava/lang/String;

    iget-object v7, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->password:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Lorg/apache/commons/net/ftp/FTPClient;->login(Ljava/lang/String;Ljava/lang/String;)Z

    .line 395
    iget-object v5, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Lorg/apache/commons/net/ftp/FTPClient;->setFileType(I)Z

    .line 396
    iget-object v5, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v5}, Lorg/apache/commons/net/ftp/FTPClient;->enterLocalPassiveMode()V

    .line 397
    sget-object v5, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    const-string v6, "IPv6 conntected."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    iget-object v5, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    sget-object v6, Lcom/marco/strings/MarcosStrings;->gcamversion:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lorg/apache/commons/net/ftp/FTPClient;->listFiles(Ljava/lang/String;)[Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v5

    array-length v5, v5

    if-lez v5, :cond_0

    .line 403
    sget-object v2, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_0

    :catchall_0
    move-exception v5

    goto :goto_2

    :catch_0
    move-exception v5

    .line 399
    :try_start_1
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401
    iget-object v5, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    sget-object v6, Lcom/marco/strings/MarcosStrings;->gcamversion:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lorg/apache/commons/net/ftp/FTPClient;->listFiles(Ljava/lang/String;)[Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v5

    array-length v5, v5

    if-lez v5, :cond_0

    .line 403
    sget-object v2, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/marco/strings/MarcosStrings;->gcamversion:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 405
    :cond_0
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    invoke-static {v3}, Lcom/marco/FixMarco;->toaster(Ljava/lang/String;)V

    .line 407
    invoke-static {v2}, Lcom/marco/FixMarco;->toaster(Ljava/lang/String;)V

    :goto_1
    return-void

    .line 401
    :goto_2
    iget-object v6, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    sget-object v7, Lcom/marco/strings/MarcosStrings;->gcamversion:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lorg/apache/commons/net/ftp/FTPClient;->listFiles(Ljava/lang/String;)[Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v6

    array-length v6, v6

    if-lez v6, :cond_1

    .line 403
    sget-object v2, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/marco/strings/MarcosStrings;->gcamversion:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 405
    :cond_1
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    invoke-static {v3}, Lcom/marco/FixMarco;->toaster(Ljava/lang/String;)V

    .line 407
    invoke-static {v2}, Lcom/marco/FixMarco;->toaster(Ljava/lang/String;)V

    .line 409
    :goto_3
    throw v5
.end method

.method private xmlsToDelete(Ljava/util/ArrayList;Ljava/io/File;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    .line 439
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 444
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, p2}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 445
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 446
    :cond_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 448
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 450
    :goto_1
    sget-object v1, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Removes from file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    sget-object v1, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "File has lines: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    .line 454
    :try_start_1
    new-instance v1, Ljava/util/Scanner;

    invoke-direct {v1, p2}, Ljava/util/Scanner;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    goto :goto_2

    :catch_1
    move-exception p2

    .line 456
    invoke-virtual {p2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    :goto_2
    if-nez v0, :cond_2

    return-void

    .line 460
    :cond_2
    :goto_3
    invoke-virtual {v0}, Ljava/util/Scanner;->hasNextLine()Z

    move-result p2

    if-eqz p2, :cond_3

    .line 461
    invoke-virtual {v0}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object p2

    .line 462
    sget-object v1, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "To remove: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 465
    :cond_3
    invoke-virtual {v0}, Ljava/util/Scanner;->close()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 22

    move-object/from16 v1, p0

    const-string v2, "del.txt"

    const-string v3, " port: "

    const-string v4, "Disconnected"

    const-string v5, "Logout"

    const-string v6, "Logout failed"

    const/4 v7, 0x0

    .line 221
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 183
    sput-boolean v7, Lcom/marco/FixMarco;->xmlDownloadFinished:Z

    .line 184
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    const-string v9, "Download starts here"

    invoke-static {v0, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->changes:Ljava/util/ArrayList;

    .line 186
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->changed:Ljava/util/ArrayList;

    .line 187
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {}, Lcom/marco/fixes/Fixes;->xmlFiles()[Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    invoke-direct {v0, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->before:Ljava/util/ArrayList;

    .line 188
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->after:Ljava/util/ArrayList;

    const/4 v9, 0x1

    .line 190
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    new-array v0, v9, [Ljava/lang/Integer;

    aput-object v10, v0, v7

    invoke-virtual {v1, v0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->publishProgress([Ljava/lang/Object;)V

    .line 191
    iput-boolean v7, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->succsess:Z

    .line 192
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->toBeRenamed:Ljava/util/LinkedList;

    .line 193
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 194
    new-instance v11, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/marco/strings/MarcosStrings;->xmlPath:Ljava/lang/String;

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v11, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->dir:Ljava/io/File;

    .line 195
    new-instance v0, Lorg/apache/commons/net/ftp/FTPClient;

    invoke-direct {v0}, Lorg/apache/commons/net/ftp/FTPClient;-><init>()V

    iput-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    .line 196
    sget-object v0, Lcom/marco/strings/MarcosStrings;->serverv6:Ljava/lang/String;

    iput-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ip:Ljava/lang/String;

    .line 198
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    const-string v11, "UTF-8"

    invoke-virtual {v0, v11}, Lorg/apache/commons/net/ftp/FTPClient;->setControlEncoding(Ljava/lang/String;)V

    .line 199
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v0, v9}, Lorg/apache/commons/net/ftp/FTPClient;->setAutodetectUTF8(Z)V

    .line 200
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPClient;->enterLocalPassiveMode()V

    const/4 v11, 0x2

    :try_start_0
    new-array v0, v9, [Ljava/lang/Integer;

    .line 203
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v0, v7

    invoke-virtual {v1, v0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->publishProgress([Ljava/lang/Object;)V

    .line 204
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Try connecting to ip: "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v14, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ip:Ljava/lang/String;

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v0, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    iget-object v12, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ip:Ljava/lang/String;

    iget v14, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->portNumber:I

    invoke-virtual {v0, v12, v14}, Lorg/apache/commons/net/ftp/FTPClient;->connect(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 207
    sget-object v12, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Failed connection to ip: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v11, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ip:Ljava/lang/String;

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v11, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->portNumber:I

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v12, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 210
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->getIPv4()V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    .line 212
    :goto_0
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :goto_1
    :try_start_2
    new-array v0, v9, [Ljava/lang/Integer;

    const/4 v11, 0x3

    .line 216
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v0, v7

    invoke-virtual {v1, v0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->publishProgress([Ljava/lang/Object;)V

    .line 217
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    iget-object v11, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ip:Ljava/lang/String;

    iget v12, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->portNumber:I

    invoke-virtual {v0, v11, v12}, Lorg/apache/commons/net/ftp/FTPClient;->connect(Ljava/lang/String;I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8

    :goto_2
    new-array v0, v9, [Ljava/lang/Integer;

    const/4 v11, 0x4

    .line 225
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v0, v7

    invoke-virtual {v1, v0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->publishProgress([Ljava/lang/Object;)V

    .line 226
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Connected to ip: "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v14, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ip:Ljava/lang/String;

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->portNumber:I

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    :try_start_3
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    iget-object v3, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->user:Ljava/lang/String;

    iget-object v12, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->password:Ljava/lang/String;

    invoke-virtual {v0, v3, v12}, Lorg/apache/commons/net/ftp/FTPClient;->login(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-string v3, " pass: "

    if-eqz v0, :cond_d

    .line 229
    :try_start_4
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Login name: "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v14, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->user:Ljava/lang/String;

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->password:Ljava/lang/String;

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lorg/apache/commons/net/ftp/FTPClient;->setFileType(I)Z

    .line 235
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPClient;->enterLocalPassiveMode()V

    .line 237
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    sget-object v3, Lcom/marco/strings/MarcosStrings;->gcamversion:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lorg/apache/commons/net/ftp/FTPClient;->listFiles(Ljava/lang/String;)[Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v0

    .line 238
    array-length v3, v0

    if-lez v3, :cond_0

    .line 239
    sget-object v3, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Files in: "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v14, Lcom/marco/strings/MarcosStrings;->gcamversion:Ljava/lang/String;

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, " : "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v14, v0

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v3, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "Downloading, this could take some seconds!"

    .line 240
    invoke-static {v3}, Lcom/marco/FixMarco;->toaster(Ljava/lang/String;)V

    goto :goto_3

    .line 242
    :cond_0
    iget-object v3, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ip:Ljava/lang/String;

    invoke-static {v3}, Lcom/marco/fixes/Fixes;->validIP(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 243
    sget-object v3, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    const-string v12, "Retry with ipv6"

    invoke-static {v3, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    invoke-direct/range {p0 .. p0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->retry()V

    goto :goto_3

    .line 246
    :cond_1
    sget-object v3, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    const-string v12, "Nothing to download, ipv4 and ipv6 failed."

    invoke-static {v3, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :goto_3
    array-length v3, v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v12, v7

    :goto_4
    const-string v14, ".xml"

    if-ge v12, v3, :cond_3

    :try_start_5
    aget-object v15, v0, v12

    .line 251
    invoke-virtual {v15}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-virtual {v15}, Lorg/apache/commons/net/ftp/FTPFile;->getTimestamp()Ljava/util/Calendar;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v16

    sget-wide v18, Lcom/marco/FixMarco;->lastLocalModified:J

    cmp-long v11, v16, v18

    if-lez v11, :cond_2

    .line 252
    iget-object v11, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->changes:Ljava/util/ArrayList;

    invoke-virtual {v15}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v12, v12, 0x1

    const/4 v11, 0x4

    goto :goto_4

    .line 253
    :cond_3
    iget-object v3, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-static {v3, v9, v0}, Lcom/marco/fixes/Fixes;->lastFTPmod(Lorg/apache/commons/net/ftp/FTPClient;Z[Lorg/apache/commons/net/ftp/FTPFile;)Z

    new-array v0, v9, [Ljava/lang/Integer;

    const/4 v3, 0x5

    .line 256
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v7

    invoke-virtual {v1, v0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->publishProgress([Ljava/lang/Object;)V

    .line 257
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    sget-object v3, Lcom/marco/strings/MarcosStrings;->gcamversion:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lorg/apache/commons/net/ftp/FTPClient;->listFiles(Ljava/lang/String;)[Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v0

    array-length v0, v0

    .line 258
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 259
    iget-object v11, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    sget-object v12, Lcom/marco/strings/MarcosStrings;->gcamversion:Ljava/lang/String;

    invoke-virtual {v11, v12}, Lorg/apache/commons/net/ftp/FTPClient;->listFiles(Ljava/lang/String;)[Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v11

    array-length v12, v11

    move v15, v7

    :goto_5
    if-ge v15, v12, :cond_a

    aget-object v16, v11, v15

    .line 260
    invoke-virtual/range {v16 .. v16}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-virtual/range {v16 .. v16}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v7

    iget-object v9, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->previx:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 261
    new-instance v7, Ljava/io/BufferedOutputStream;

    new-instance v9, Ljava/io/FileOutputStream;

    move-object/from16 v19, v11

    new-instance v11, Ljava/io/File;

    move/from16 v20, v12

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v21, v14

    iget-object v14, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->dir:Ljava/io/File;

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {v16 .. v16}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v11, v12, v14}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v7, v9}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 262
    iget-object v9, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/marco/strings/MarcosStrings;->gcamversion:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {v16 .. v16}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11, v7}, Lorg/apache/commons/net/ftp/FTPClient;->retrieveFile(Ljava/lang/String;Ljava/io/OutputStream;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 263
    invoke-virtual/range {v16 .. v16}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v9

    .line 264
    sget-object v11, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Downloaded: "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    iget-object v11, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->previx:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    const-string v12, "."

    invoke-virtual {v9, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v12

    invoke-virtual {v9, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 267
    :cond_4
    sget-object v9, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Download "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {v16 .. v16}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, " failed"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :goto_6
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    .line 269
    iget-object v7, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->toBeRenamed:Ljava/util/LinkedList;

    invoke-virtual/range {v16 .. v16}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v16 .. v16}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "-"

    invoke-virtual {v11, v12}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v11

    const/4 v12, 0x1

    add-int/2addr v11, v12

    invoke-virtual/range {v16 .. v16}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    const/4 v14, 0x4

    sub-int/2addr v12, v14

    invoke-virtual {v9, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 271
    iget-boolean v7, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->succsess:Z

    if-nez v7, :cond_5

    .line 272
    iget-object v7, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->toBeRenamed:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    if-lez v7, :cond_5

    const/4 v7, 0x1

    .line 273
    iput-boolean v7, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->succsess:Z

    .line 274
    :cond_5
    iget-object v7, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->toBeRenamed:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    int-to-float v7, v7

    int-to-float v9, v0

    div-float/2addr v7, v9

    const/high16 v9, 0x3e800000    # 0.25f

    cmpg-float v9, v7, v9

    if-gez v9, :cond_6

    const/4 v9, 0x1

    new-array v7, v9, [Ljava/lang/Integer;

    const/4 v9, 0x6

    .line 277
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/4 v11, 0x0

    aput-object v9, v7, v11

    invoke-virtual {v1, v7}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->publishProgress([Ljava/lang/Object;)V

    goto :goto_7

    :cond_6
    const/high16 v9, 0x3f000000    # 0.5f

    cmpg-float v9, v7, v9

    if-gez v9, :cond_7

    const/4 v9, 0x1

    new-array v7, v9, [Ljava/lang/Integer;

    const/4 v9, 0x7

    .line 280
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/4 v11, 0x0

    aput-object v9, v7, v11

    invoke-virtual {v1, v7}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->publishProgress([Ljava/lang/Object;)V

    goto :goto_7

    :cond_7
    const/high16 v9, 0x3f400000    # 0.75f

    cmpg-float v7, v7, v9

    if-gez v7, :cond_8

    const/4 v7, 0x1

    new-array v9, v7, [Ljava/lang/Integer;

    const/16 v7, 0x8

    .line 283
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v11, 0x0

    aput-object v7, v9, v11

    invoke-virtual {v1, v9}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->publishProgress([Ljava/lang/Object;)V

    goto :goto_7

    :cond_8
    const/4 v7, 0x1

    new-array v9, v7, [Ljava/lang/Integer;

    const/16 v7, 0x9

    .line 286
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const/4 v11, 0x0

    aput-object v7, v9, v11

    invoke-virtual {v1, v9}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->publishProgress([Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_7

    :cond_9
    move-object/from16 v19, v11

    move/from16 v20, v12

    move-object/from16 v21, v14

    const/4 v14, 0x4

    :goto_7
    add-int/lit8 v15, v15, 0x1

    move-object/from16 v11, v19

    move/from16 v12, v20

    move-object/from16 v14, v21

    const/4 v7, 0x0

    const/4 v9, 0x1

    goto/16 :goto_5

    .line 289
    :cond_a
    :try_start_6
    invoke-direct {v1, v3}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->redownloadmissing(Ljava/util/ArrayList;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_8

    :catch_3
    move-exception v0

    move-object v3, v0

    .line 291
    :try_start_7
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 293
    :goto_8
    new-instance v0, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    new-instance v7, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->dir:Ljava/io/File;

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v9, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 294
    iget-object v3, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/marco/strings/MarcosStrings;->gcamversion:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2, v0}, Lorg/apache/commons/net/ftp/FTPClient;->retrieveFile(Ljava/lang/String;Ljava/io/OutputStream;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 295
    sget-object v2, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    const-string v3, "Downloaded: del.txt"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 297
    :cond_b
    sget-object v2, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    const-string v3, "Download del.txt failed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    :goto_9
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    const/4 v2, 0x1

    new-array v0, v2, [Ljava/lang/Integer;

    const/16 v2, 0xa

    .line 300
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v0, v3

    invoke-virtual {v1, v0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->publishProgress([Ljava/lang/Object;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 304
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    if-eqz v0, :cond_11

    .line 306
    :try_start_8
    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPClient;->logout()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 307
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 309
    :cond_c
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    :goto_a
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPClient;->disconnect()V

    .line 311
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto/16 :goto_e

    .line 231
    :cond_d
    :try_start_9
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Login failed: name: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->user:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->password:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 304
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    if-eqz v0, :cond_f

    .line 306
    :try_start_a
    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPClient;->logout()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 307
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 309
    :cond_e
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    :goto_b
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPClient;->disconnect()V

    .line 311
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4

    goto :goto_c

    :catch_4
    move-exception v0

    .line 313
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :cond_f
    :goto_c
    return-object v8

    :catchall_0
    move-exception v0

    move-object v2, v0

    goto :goto_f

    :catch_5
    move-exception v0

    .line 302
    :try_start_b
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 304
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    if-eqz v0, :cond_11

    .line 306
    :try_start_c
    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPClient;->logout()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 307
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 309
    :cond_10
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    :goto_d
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPClient;->disconnect()V

    .line 311
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_6

    goto :goto_e

    :catch_6
    move-exception v0

    .line 313
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 317
    :cond_11
    :goto_e
    iget-boolean v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->succsess:Z

    if-eqz v0, :cond_12

    return-object v10

    :cond_12
    return-object v8

    .line 304
    :goto_f
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    if-eqz v0, :cond_14

    .line 306
    :try_start_d
    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPClient;->logout()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 307
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_10

    .line 309
    :cond_13
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    invoke-static {v0, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    :goto_10
    iget-object v0, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPClient;->disconnect()V

    .line 311
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_7

    goto :goto_11

    :catch_7
    move-exception v0

    .line 313
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 316
    :cond_14
    :goto_11
    throw v2

    :catch_8
    move-exception v0

    .line 219
    sget-object v2, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->ip:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->portNumber:I

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    return-object v8
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 7

    .line 78
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    const/4 p1, 0x1

    new-array v0, p1, [Ljava/lang/Integer;

    const/16 v1, 0xb

    .line 80
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->publishProgress([Ljava/lang/Object;)V

    .line 81
    invoke-direct {p0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->refactor()V

    new-array v0, p1, [Ljava/lang/Integer;

    const/16 v1, 0xc

    .line 83
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->publishProgress([Ljava/lang/Object;)V

    .line 84
    invoke-direct {p0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->removeXMLs()V

    .line 86
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload$1;

    invoke-direct {v1, p0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload$1;-><init>(Lcom/marco/xmlAndPersistent/AsyncFTPdownload;)V

    const-wide/16 v3, 0x64

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 93
    sget-wide v0, Lcom/marco/FixMarco;->lastOnlineModified:J

    sget-wide v5, Lcom/marco/FixMarco;->lastLocalModified:J

    cmp-long v0, v0, v5

    if-lez v0, :cond_0

    .line 94
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/marco/strings/MarcosStrings;->xmlPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "/.lastmod"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 97
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 99
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/Throwable;

    invoke-direct {v6}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v6}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v6

    aget-object v2, v6, v2

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v2, v6}, Lcom/marco/fixes/Fixes;->printLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 102
    :goto_0
    sget-wide v1, Lcom/marco/FixMarco;->lastOnlineModified:J

    sput-wide v1, Lcom/marco/FixMarco;->lastLocalModified:J

    .line 103
    sget-wide v1, Lcom/marco/FixMarco;->lastOnlineModified:J

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/marco/fixes/Fixes;->writeLineToFile(Ljava/io/File;Ljava/lang/String;)V

    .line 104
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "writing "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-wide v5, Lcom/marco/FixMarco;->lastOnlineModified:J

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :cond_0
    iget-boolean v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->succsess:Z

    if-eqz v0, :cond_1

    const-string v0, "XMLs updated!"

    .line 107
    invoke-static {v0}, Lcom/marco/FixMarco;->toaster(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const-string v0, "Update failed, send a log!"

    .line 109
    invoke-static {v0}, Lcom/marco/FixMarco;->toaster(Ljava/lang/String;)V

    .line 110
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/marco/xmlAndPersistent/AsyncFTPdownload$2;

    invoke-direct {v1, p0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload$2;-><init>(Lcom/marco/xmlAndPersistent/AsyncFTPdownload;)V

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 117
    :goto_1
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    const-string v1, "Download ends here"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    sput-boolean p1, Lcom/marco/FixMarco;->xmlDownloadFinished:Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 44
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 2

    .line 69
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 70
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->progressbar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 71
    aget-object p1, p1, v1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 44
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method

.method public setProgress(Landroid/widget/ProgressBar;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->progressbar:Landroid/widget/ProgressBar;

    return-void
.end method
