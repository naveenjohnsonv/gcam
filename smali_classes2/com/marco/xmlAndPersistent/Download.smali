.class public Lcom/marco/xmlAndPersistent/Download;
.super Ljava/lang/Object;
.source "Download.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# static fields
.field private static counter:I


# instance fields
.field private context:Landroid/content/Context;

.field private final f15560a:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/marco/xmlAndPersistent/Download;->f15560a:Landroid/app/Activity;

    return-void
.end method

.method static synthetic access$000(Lcom/marco/xmlAndPersistent/Download;)Landroid/content/Context;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/marco/xmlAndPersistent/Download;->context:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$108()I
    .locals 2

    .line 17
    sget v0, Lcom/marco/xmlAndPersistent/Download;->counter:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/marco/xmlAndPersistent/Download;->counter:I

    return v0
.end method

.method public static progress(Landroid/content/Context;)V
    .locals 5

    .line 58
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Update progress"

    .line 59
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 60
    new-instance v1, Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    const v3, 0x1010078

    invoke-direct {v1, p0, v2, v3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 p0, 0xc

    .line 61
    invoke-virtual {v1, p0}, Landroid/widget/ProgressBar;->setMax(I)V

    const/4 v2, 0x0

    .line 62
    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    const/16 v3, 0x32

    const/16 v4, 0x14

    .line 63
    invoke-virtual {v1, v3, v4, v3, v4}, Landroid/widget/ProgressBar;->setPadding(IIII)V

    .line 64
    invoke-virtual {v1, p0}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 65
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 66
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 67
    new-instance p0, Lcom/marco/xmlAndPersistent/Download$3;

    invoke-direct {p0}, Lcom/marco/xmlAndPersistent/Download$3;-><init>()V

    const-string v3, "Close"

    invoke-virtual {v0, v3, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 73
    new-instance p0, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;

    invoke-direct {p0}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;-><init>()V

    .line 74
    invoke-virtual {p0, v1}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->setProgress(Landroid/widget/ProgressBar;)V

    new-array v1, v2, [Ljava/lang/Void;

    .line 75
    invoke-virtual {p0, v1}, Lcom/marco/xmlAndPersistent/AsyncFTPdownload;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 76
    new-instance p0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {p0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 79
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 81
    sput v2, Lcom/marco/xmlAndPersistent/Download;->counter:I

    .line 82
    new-instance v1, Lcom/marco/xmlAndPersistent/Download$4;

    invoke-direct {v1, p0, v0}, Lcom/marco/xmlAndPersistent/Download$4;-><init>(Landroid/os/Handler;Landroid/app/AlertDialog;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {p0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .line 28
    iget-object p1, p0, Lcom/marco/xmlAndPersistent/Download;->f15560a:Landroid/app/Activity;

    iput-object p1, p0, Lcom/marco/xmlAndPersistent/Download;->context:Landroid/content/Context;

    .line 29
    sput-object p1, Lcom/marco/FixMarco;->staticSettingsContext:Landroid/content/Context;

    .line 30
    new-instance p1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/marco/xmlAndPersistent/Download;->context:Landroid/content/Context;

    invoke-direct {p1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v0, "Update XMLs"

    .line 31
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 32
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/marco/xmlAndPersistent/Download;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x32

    const/16 v2, 0x14

    .line 33
    invoke-virtual {v0, v1, v2, v1, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    const/high16 v1, 0x41900000    # 18.0f

    .line 34
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 35
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const-string v1, "This will update all XMLs: \n- existing XMLs will be overwritten with latest version.\n- new XMLs will be added.\nThis action causes a little data volume\nIf this is not working for you, maybe you got a firewall blocking it."

    .line 41
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    new-instance v0, Lcom/marco/xmlAndPersistent/Download$1;

    invoke-direct {v0, p0}, Lcom/marco/xmlAndPersistent/Download$1;-><init>(Lcom/marco/xmlAndPersistent/Download;)V

    const-string v1, "Update"

    invoke-virtual {p1, v1, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 48
    new-instance v0, Lcom/marco/xmlAndPersistent/Download$2;

    invoke-direct {v0, p0}, Lcom/marco/xmlAndPersistent/Download$2;-><init>(Lcom/marco/xmlAndPersistent/Download;)V

    const-string v1, "Cancel"

    invoke-virtual {p1, v1, v0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 53
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    const/4 p1, 0x1

    return p1
.end method
