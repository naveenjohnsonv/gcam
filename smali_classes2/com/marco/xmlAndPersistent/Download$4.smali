.class final Lcom/marco/xmlAndPersistent/Download$4;
.super Ljava/lang/Object;
.source "Download.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/marco/xmlAndPersistent/Download;->progress(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$ad:Landroid/app/AlertDialog;

.field final synthetic val$handler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/os/Handler;Landroid/app/AlertDialog;)V
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/marco/xmlAndPersistent/Download$4;->val$handler:Landroid/os/Handler;

    iput-object p2, p0, Lcom/marco/xmlAndPersistent/Download$4;->val$ad:Landroid/app/AlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 85
    invoke-static {}, Lcom/marco/xmlAndPersistent/Download;->access$108()I

    move-result v0

    const/16 v1, 0x1f4

    if-le v0, v1, :cond_0

    return-void

    .line 87
    :cond_0
    sget-boolean v0, Lcom/marco/FixMarco;->xmlDownloadFinished:Z

    if-nez v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/Download$4;->val$handler:Landroid/os/Handler;

    const-wide/16 v1, 0x64

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/Download$4;->val$ad:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    const/4 v0, 0x0

    .line 91
    sput-boolean v0, Lcom/marco/FixMarco;->xmlDownloadFinished:Z

    :goto_0
    return-void
.end method
