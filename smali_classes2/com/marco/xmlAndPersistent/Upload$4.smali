.class Lcom/marco/xmlAndPersistent/Upload$4;
.super Ljava/lang/Object;
.source "Upload.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/marco/xmlAndPersistent/Upload;->chooseXml()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/marco/xmlAndPersistent/Upload;


# direct methods
.method constructor <init>(Lcom/marco/xmlAndPersistent/Upload;)V
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/marco/xmlAndPersistent/Upload$4;->this$0:Lcom/marco/xmlAndPersistent/Upload;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .line 89
    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    sput-object p1, Lcom/marco/xmlAndPersistent/Upload;->xmllist:Ljava/util/LinkedList;

    const/4 p1, 0x0

    move p2, p1

    .line 90
    :goto_0
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/Upload$4;->this$0:Lcom/marco/xmlAndPersistent/Upload;

    invoke-static {v0}, Lcom/marco/xmlAndPersistent/Upload;->access$100(Lcom/marco/xmlAndPersistent/Upload;)[Z

    move-result-object v0

    array-length v0, v0

    if-ge p2, v0, :cond_1

    .line 91
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/Upload$4;->this$0:Lcom/marco/xmlAndPersistent/Upload;

    invoke-static {v0}, Lcom/marco/xmlAndPersistent/Upload;->access$100(Lcom/marco/xmlAndPersistent/Upload;)[Z

    move-result-object v0

    aget-boolean v0, v0, p2

    if-eqz v0, :cond_0

    .line 92
    sget-object v0, Lcom/marco/xmlAndPersistent/Upload;->xmllist:Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/marco/xmlAndPersistent/Upload$4;->this$0:Lcom/marco/xmlAndPersistent/Upload;

    invoke-static {v1}, Lcom/marco/xmlAndPersistent/Upload;->access$200(Lcom/marco/xmlAndPersistent/Upload;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, p2

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 93
    :cond_1
    new-instance p2, Lcom/marco/xmlAndPersistent/AsyncFTPupload;

    invoke-direct {p2}, Lcom/marco/xmlAndPersistent/AsyncFTPupload;-><init>()V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, p1

    invoke-virtual {p2, v0}, Lcom/marco/xmlAndPersistent/AsyncFTPupload;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
