.class public Lcom/marco/xmlAndPersistent/Upload;
.super Ljava/lang/Object;
.source "Upload.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# static fields
.field public static password:Ljava/lang/String;

.field public static xmllist:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private context:Landroid/content/Context;

.field private final f15560a:Landroid/app/Activity;

.field private values:[Z

.field private xmls:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/marco/xmlAndPersistent/Upload;->f15560a:Landroid/app/Activity;

    .line 33
    invoke-static {}, Lcom/marco/fixes/Fixes;->xmlFiles()[Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/marco/xmlAndPersistent/Upload;->xmls:[Ljava/lang/String;

    .line 34
    array-length p1, p1

    new-array p1, p1, [Z

    iput-object p1, p0, Lcom/marco/xmlAndPersistent/Upload;->values:[Z

    return-void
.end method

.method static synthetic access$000(Lcom/marco/xmlAndPersistent/Upload;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/marco/xmlAndPersistent/Upload;->chooseXml()V

    return-void
.end method

.method static synthetic access$100(Lcom/marco/xmlAndPersistent/Upload;)[Z
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/marco/xmlAndPersistent/Upload;->values:[Z

    return-object p0
.end method

.method static synthetic access$200(Lcom/marco/xmlAndPersistent/Upload;)[Ljava/lang/String;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/marco/xmlAndPersistent/Upload;->xmls:[Ljava/lang/String;

    return-object p0
.end method

.method private chooseXml()V
    .locals 4

    .line 76
    invoke-static {}, Lcom/marco/fixes/Fixes;->xmlFiles()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/marco/xmlAndPersistent/Upload;->xmls:[Ljava/lang/String;

    .line 77
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/marco/xmlAndPersistent/Upload;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 78
    iget-object v1, p0, Lcom/marco/xmlAndPersistent/Upload;->xmls:[Ljava/lang/String;

    iget-object v2, p0, Lcom/marco/xmlAndPersistent/Upload;->values:[Z

    new-instance v3, Lcom/marco/xmlAndPersistent/Upload$3;

    invoke-direct {v3, p0}, Lcom/marco/xmlAndPersistent/Upload$3;-><init>(Lcom/marco/xmlAndPersistent/Upload;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x0

    .line 84
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const-string v1, "Check XMLs to upload!"

    .line 85
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 86
    new-instance v1, Lcom/marco/xmlAndPersistent/Upload$4;

    invoke-direct {v1, p0}, Lcom/marco/xmlAndPersistent/Upload$4;-><init>(Lcom/marco/xmlAndPersistent/Upload;)V

    const-string v2, "Upload"

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 96
    new-instance v1, Lcom/marco/xmlAndPersistent/Upload$5;

    invoke-direct {v1, p0}, Lcom/marco/xmlAndPersistent/Upload$5;-><init>(Lcom/marco/xmlAndPersistent/Upload;)V

    const-string v2, "Cancel"

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 101
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6

    .line 38
    iget-object p1, p0, Lcom/marco/xmlAndPersistent/Upload;->f15560a:Landroid/app/Activity;

    iput-object p1, p0, Lcom/marco/xmlAndPersistent/Upload;->context:Landroid/content/Context;

    .line 39
    invoke-static {}, Lcom/marco/fixes/Fixes;->xmlFiles()[Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/marco/xmlAndPersistent/Upload;->xmls:[Ljava/lang/String;

    .line 40
    new-instance p1, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/marco/xmlAndPersistent/Upload;->context:Landroid/content/Context;

    invoke-direct {p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    .line 41
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 42
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 43
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/marco/xmlAndPersistent/Upload;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x32

    const/16 v2, 0x14

    .line 44
    invoke-virtual {v0, v1, v2, v1, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 45
    new-instance v3, Landroid/widget/EditText;

    iget-object v4, p0, Lcom/marco/xmlAndPersistent/Upload;->context:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    const/high16 v4, 0x41900000    # 18.0f

    .line 46
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextSize(F)V

    const-string v5, "Enter your password to upload XMLs.\nThis protection prevents spam and flooding my server."

    .line 47
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v5, "Enter password here!"

    .line 48
    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 49
    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setTextSize(F)V

    .line 50
    invoke-virtual {v3, v1, v2, v1, v2}, Landroid/widget/EditText;->setPadding(IIII)V

    .line 51
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 52
    invoke-virtual {p1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 53
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/marco/xmlAndPersistent/Upload;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Upload XMLs!"

    .line 54
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 55
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 56
    new-instance p1, Lcom/marco/xmlAndPersistent/Upload$1;

    invoke-direct {p1, p0, v3}, Lcom/marco/xmlAndPersistent/Upload$1;-><init>(Lcom/marco/xmlAndPersistent/Upload;Landroid/widget/EditText;)V

    const-string v1, "OK"

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 66
    new-instance p1, Lcom/marco/xmlAndPersistent/Upload$2;

    invoke-direct {p1, p0}, Lcom/marco/xmlAndPersistent/Upload$2;-><init>(Lcom/marco/xmlAndPersistent/Upload;)V

    const-string v1, "Cancel"

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 71
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    const/4 p1, 0x0

    return p1
.end method
