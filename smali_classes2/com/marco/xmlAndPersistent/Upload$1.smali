.class Lcom/marco/xmlAndPersistent/Upload$1;
.super Ljava/lang/Object;
.source "Upload.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/marco/xmlAndPersistent/Upload;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/marco/xmlAndPersistent/Upload;

.field final synthetic val$edit:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/marco/xmlAndPersistent/Upload;Landroid/widget/EditText;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/marco/xmlAndPersistent/Upload$1;->this$0:Lcom/marco/xmlAndPersistent/Upload;

    iput-object p2, p0, Lcom/marco/xmlAndPersistent/Upload$1;->val$edit:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 59
    iget-object p1, p0, Lcom/marco/xmlAndPersistent/Upload$1;->val$edit:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    sput-object p1, Lcom/marco/xmlAndPersistent/Upload;->password:Ljava/lang/String;

    .line 60
    sget-object p1, Lcom/marco/xmlAndPersistent/Upload;->password:Ljava/lang/String;

    invoke-static {p1}, Lcom/marco/fixes/Fixes;->crypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    sget-object p2, Lcom/marco/strings/MarcosStrings;->crypt:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 61
    iget-object p1, p0, Lcom/marco/xmlAndPersistent/Upload$1;->this$0:Lcom/marco/xmlAndPersistent/Upload;

    invoke-static {p1}, Lcom/marco/xmlAndPersistent/Upload;->access$000(Lcom/marco/xmlAndPersistent/Upload;)V

    goto :goto_0

    :cond_0
    const-string p1, "Wrong password!"

    .line 63
    invoke-static {p1}, Lcom/marco/FixMarco;->toaster(Ljava/lang/String;)V

    :goto_0
    return-void
.end method
