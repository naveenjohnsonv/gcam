.class public Lcom/marco/xmlAndPersistent/AsyncFTPcheck;
.super Landroid/os/AsyncTask;
.source "AsyncFTPcheck.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private dir:Ljava/io/File;

.field private ftp:Lorg/apache/commons/net/ftp/FTPClient;

.field private ip:Ljava/lang/String;

.field private password:Ljava/lang/String;

.field private portNumber:I

.field private success:Z

.field private user:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 20
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/16 v0, 0x15

    .line 23
    iput v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->portNumber:I

    const-string v0, "user"

    .line 24
    iput-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->user:Ljava/lang/String;

    const-string v0, ""

    .line 25
    iput-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->password:Ljava/lang/String;

    return-void
.end method

.method private getIPv4()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Ljava/net/UnknownHostException;
        }
    .end annotation

    .line 93
    new-instance v0, Ljava/net/URL;

    sget-object v1, Lcom/marco/strings/MarcosStrings;->server:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/InetAddress;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v0

    if-nez v0, :cond_0

    .line 95
    sget-object v1, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    const-string v2, "Getting address failed."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v1, ""

    .line 96
    iput-object v1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ip:Ljava/lang/String;

    .line 97
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    .line 98
    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v3

    .line 99
    sget-object v4, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Check IP: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-static {v3}, Lcom/marco/fixes/Fixes;->validIP(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 101
    iput-object v3, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ip:Ljava/lang/String;

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 103
    :cond_2
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ip:Ljava/lang/String;

    invoke-static {v0}, Lcom/marco/fixes/Fixes;->validIP(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 104
    sget-object v0, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    const-string v1, "No IPv4 address found."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 4

    const/4 p1, 0x0

    .line 62
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 39
    iput-boolean p1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->success:Z

    .line 40
    sget-object v1, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    const-string v2, "Check starts here"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    .line 42
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/marco/strings/MarcosStrings;->xmlPath:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->dir:Ljava/io/File;

    .line 43
    new-instance v1, Lorg/apache/commons/net/ftp/FTPClient;

    invoke-direct {v1}, Lorg/apache/commons/net/ftp/FTPClient;-><init>()V

    iput-object v1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    .line 44
    sget-object v1, Lcom/marco/strings/MarcosStrings;->serverv6:Ljava/lang/String;

    iput-object v1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ip:Ljava/lang/String;

    .line 45
    iget-object v1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Lorg/apache/commons/net/ftp/FTPClient;->setControlEncoding(Ljava/lang/String;)V

    .line 46
    iget-object v1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/commons/net/ftp/FTPClient;->setAutodetectUTF8(Z)V

    .line 47
    iget-object v1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v1}, Lorg/apache/commons/net/ftp/FTPClient;->enterLocalPassiveMode()V

    .line 48
    sget-object v1, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    const-string v2, "Connecting"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    :try_start_0
    iget-object v1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    iget-object v2, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ip:Ljava/lang/String;

    iget v3, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->portNumber:I

    invoke-virtual {v1, v2, v3}, Lorg/apache/commons/net/ftp/FTPClient;->connect(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    .line 52
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 54
    :try_start_1
    invoke-direct {p0}, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->getIPv4()V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    .line 56
    :goto_0
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 59
    :goto_1
    :try_start_2
    iget-object v1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    iget-object v2, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ip:Ljava/lang/String;

    iget v3, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->portNumber:I

    invoke-virtual {v1, v2, v3}, Lorg/apache/commons/net/ftp/FTPClient;->connect(Ljava/lang/String;I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7

    .line 65
    :goto_2
    sget-object v1, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    const-string v2, "Logging in"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    :try_start_3
    iget-object v1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    iget-object v2, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->user:Ljava/lang/String;

    iget-object v3, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->password:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/apache/commons/net/ftp/FTPClient;->login(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v1, :cond_1

    .line 77
    iget-object p1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    if-eqz p1, :cond_0

    .line 79
    :try_start_4
    invoke-virtual {p1}, Lorg/apache/commons/net/ftp/FTPClient;->logout()Z

    .line 80
    iget-object p1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {p1}, Lorg/apache/commons/net/ftp/FTPClient;->disconnect()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_3

    :catch_3
    move-exception p1

    .line 82
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    :cond_0
    :goto_3
    return-object v0

    .line 69
    :cond_1
    :try_start_5
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPClient;->setFileType(I)Z

    .line 70
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPClient;->enterLocalPassiveMode()V

    .line 72
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    iget-object v1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    sget-object v2, Lcom/marco/strings/MarcosStrings;->gcamversion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/apache/commons/net/ftp/FTPClient;->listFiles(Ljava/lang/String;)[Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/marco/fixes/Fixes;->lastFTPmod(Lorg/apache/commons/net/ftp/FTPClient;Z[Lorg/apache/commons/net/ftp/FTPFile;)Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 77
    iget-object p1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    if-eqz p1, :cond_2

    .line 79
    :try_start_6
    invoke-virtual {p1}, Lorg/apache/commons/net/ftp/FTPClient;->logout()Z

    .line 80
    iget-object p1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {p1}, Lorg/apache/commons/net/ftp/FTPClient;->disconnect()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_4

    :catchall_0
    move-exception p1

    goto :goto_5

    :catch_4
    move-exception p1

    .line 75
    :try_start_7
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 77
    iget-object p1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    if-eqz p1, :cond_2

    .line 79
    :try_start_8
    invoke-virtual {p1}, Lorg/apache/commons/net/ftp/FTPClient;->logout()Z

    .line 80
    iget-object p1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {p1}, Lorg/apache/commons/net/ftp/FTPClient;->disconnect()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_4

    :catch_5
    move-exception p1

    .line 82
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    .line 86
    :cond_2
    :goto_4
    iget-boolean p1, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->success:Z

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 77
    :goto_5
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    if-eqz v0, :cond_3

    .line 79
    :try_start_9
    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPClient;->logout()Z

    .line 80
    iget-object v0, p0, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->ftp:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPClient;->disconnect()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_6

    :catch_6
    move-exception v0

    .line 82
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 85
    :cond_3
    :goto_6
    throw p1

    :catch_7
    move-exception p1

    .line 61
    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 1

    .line 32
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 33
    sget-object p1, Lcom/marco/strings/MarcosStrings;->TAG:Ljava/lang/String;

    const-string v0, "Check ends here"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/marco/xmlAndPersistent/AsyncFTPcheck;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
