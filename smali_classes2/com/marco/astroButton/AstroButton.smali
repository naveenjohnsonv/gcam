.class public Lcom/marco/astroButton/AstroButton;
.super Ljava/lang/Object;
.source "AstroButton.java"


# instance fields
.field private applicationContext:Landroid/content/Context;

.field public astro:Landroid/widget/TextView;

.field public mOrientationEventListener:Landroid/view/OrientationEventListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "astro"

    .line 28
    invoke-static {v0}, Lcom/marco/fixes/Fixes;->removeButton(Ljava/lang/String;)V

    .line 29
    invoke-static {}, Lcom/marco/fixes/Fixes;->inButtonHideModes()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 31
    :cond_0
    iput-object p1, p0, Lcom/marco/astroButton/AstroButton;->applicationContext:Landroid/content/Context;

    .line 32
    invoke-direct {p0}, Lcom/marco/astroButton/AstroButton;->astroIndicator()V

    return-void
.end method

.method static synthetic access$000(Lcom/marco/astroButton/AstroButton;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/marco/astroButton/AstroButton;->updateAstroIndicator()V

    return-void
.end method

.method private astroIndicator()V
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/marco/astroButton/AstroButton;->applicationContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    .line 38
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0x64

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 39
    iget-object v1, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 40
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    const/high16 v1, 0x41500000    # 13.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 41
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 42
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    const-string v1, "astro"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/marco/fixes/Fixes;->buttonOrientation(Landroid/widget/TextView;)V

    .line 44
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 45
    sget-object v0, Lcom/marco/FixMarco;->parentView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 46
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 48
    :cond_0
    invoke-static {}, Lcom/marco/fixes/Fixes;->isAutoRotaion()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    new-instance v0, Lcom/marco/astroButton/AstroButton$1;

    iget-object v1, p0, Lcom/marco/astroButton/AstroButton;->applicationContext:Landroid/content/Context;

    const/4 v2, 0x3

    invoke-direct {v0, p0, v1, v2}, Lcom/marco/astroButton/AstroButton$1;-><init>(Lcom/marco/astroButton/AstroButton;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/marco/astroButton/AstroButton;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    .line 56
    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->canDetectOrientation()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/marco/astroButton/AstroButton;->astroOnClick()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    invoke-direct {p0}, Lcom/marco/astroButton/AstroButton;->updateAstroIndicator()V

    return-void
.end method

.method private astroOnClick()Landroid/view/View$OnClickListener;
    .locals 1

    .line 64
    new-instance v0, Lcom/marco/astroButton/AstroButton$2;

    invoke-direct {v0, p0}, Lcom/marco/astroButton/AstroButton$2;-><init>(Lcom/marco/astroButton/AstroButton;)V

    return-object v0
.end method

.method private updateAstroIndicator()V
    .locals 6

    const-string v0, "pref_category_ASTRO_switch"

    .line 79
    invoke-static {v0}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_7

    sget v0, Lcom/marco/FixMarco;->mode:I

    const/16 v2, 0xc

    if-eq v0, v2, :cond_0

    goto/16 :goto_4

    :cond_0
    const-string v0, "pref_astrophoto_key"

    .line 85
    invoke-static {v0}, Lcom/FixBSG;->MenuValueString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "on"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    const-string v3, "pref_category_ASTRO_switch_background"

    if-eqz v0, :cond_2

    .line 86
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    invoke-static {v3}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v2, :cond_1

    const-string v4, "vf-icons/button_awb_on.png"

    goto :goto_0

    :cond_1
    const-string v4, "vf-icons/button_awb_on_empty.png"

    .line 87
    :goto_0
    invoke-static {v4}, Lcom/marco/fixes/Fixes;->drawableFromAssets(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 86
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 88
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    const-string v4, "Astro\nOn"

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    sput-boolean v2, Lcom/marco/FixMarco;->astroIsOn:Z

    goto :goto_2

    .line 92
    :cond_2
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    invoke-static {v3}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v2, :cond_3

    const-string v4, "vf-icons/button_awb_off.png"

    goto :goto_1

    :cond_3
    const-string v4, "vf-icons/button_awb_off_empty.png"

    .line 93
    :goto_1
    invoke-static {v4}, Lcom/marco/fixes/Fixes;->drawableFromAssets(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 92
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 94
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    const-string v4, "Astro\nOff"

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    .line 95
    sput-boolean v0, Lcom/marco/FixMarco;->astroIsOn:Z

    :goto_2
    const-string v0, "pref_category_ASTRO_switch_background_color"

    .line 97
    invoke-static {v0}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_5

    .line 98
    iget-object v4, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    invoke-static {v3}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v5

    if-ne v5, v2, :cond_4

    const-string v2, "vf-icons/button_empty.png"

    goto :goto_3

    :cond_4
    const-string v2, "vf-icons/button_empty_empty.png"

    .line 99
    :goto_3
    invoke-static {v2}, Lcom/marco/fixes/Fixes;->drawableFromAssets(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 98
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 100
    :cond_5
    invoke-static {v3}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_6

    invoke-static {v0}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    .line 101
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_6
    return-void

    .line 80
    :cond_7
    :goto_4
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 82
    iget-object v0, p0, Lcom/marco/astroButton/AstroButton;->astro:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
