.class public Lcom/marco/postProcessing/Reset;
.super Ljava/lang/Object;
.source "Reset.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private context:Landroid/content/Context;

.field private final f15560a:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/marco/postProcessing/Reset;->f15560a:Landroid/app/Activity;

    return-void
.end method

.method static synthetic access$000(Lcom/marco/postProcessing/Reset;)Landroid/content/Context;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/marco/postProcessing/Reset;->context:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 3

    .line 35
    iget-object p1, p0, Lcom/marco/postProcessing/Reset;->f15560a:Landroid/app/Activity;

    iput-object p1, p0, Lcom/marco/postProcessing/Reset;->context:Landroid/content/Context;

    .line 36
    new-instance p1, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/marco/postProcessing/Reset;->context:Landroid/content/Context;

    invoke-direct {p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    .line 37
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 38
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 39
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/marco/postProcessing/Reset;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const-string v2, "Are you sure to reset everything?\nYou will also lose every not included xml!"

    .line 41
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 45
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/marco/postProcessing/Reset;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "Warning!"

    .line 46
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 47
    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 48
    new-instance p1, Lcom/marco/postProcessing/Reset$1;

    invoke-direct {p1, p0}, Lcom/marco/postProcessing/Reset$1;-><init>(Lcom/marco/postProcessing/Reset;)V

    const-string v2, "Yes"

    invoke-virtual {v1, v2, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const-string p1, "Cancel"

    const/4 v2, 0x0

    .line 65
    invoke-virtual {v1, p1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 66
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    return v0
.end method
