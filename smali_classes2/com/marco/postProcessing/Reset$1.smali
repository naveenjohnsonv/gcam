.class Lcom/marco/postProcessing/Reset$1;
.super Ljava/lang/Object;
.source "Reset.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/marco/postProcessing/Reset;->onPreferenceClick(Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/marco/postProcessing/Reset;


# direct methods
.method constructor <init>(Lcom/marco/postProcessing/Reset;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/marco/postProcessing/Reset$1;->this$0:Lcom/marco/postProcessing/Reset;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 52
    new-instance p1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object p2

    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object p2

    sget-object v0, Lcom/marco/strings/MarcosStrings;->xmlPath:Ljava/lang/String;

    invoke-direct {p1, p2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    invoke-static {p1}, Lcom/marco/fixes/Fixes;->deleteDirectory(Ljava/io/File;)Z

    .line 54
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 55
    invoke-static {}, Lcom/marco/fixes/Fixes;->getShpXML()Ljava/io/File;

    move-result-object p1

    .line 56
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 58
    :try_start_0
    iget-object p1, p0, Lcom/marco/postProcessing/Reset$1;->this$0:Lcom/marco/postProcessing/Reset;

    invoke-static {p1}, Lcom/marco/postProcessing/Reset;->access$000(Lcom/marco/postProcessing/Reset;)Landroid/content/Context;

    move-result-object p1

    const-string p2, "activity"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/ActivityManager;

    .line 59
    invoke-virtual {p1}, Landroid/app/ActivityManager;->clearApplicationUserData()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    invoke-static {}, Lcom/marco/fixes/Fixes;->restart()V

    return-void

    :catchall_0
    move-exception p1

    invoke-static {}, Lcom/marco/fixes/Fixes;->restart()V

    .line 62
    throw p1
.end method
