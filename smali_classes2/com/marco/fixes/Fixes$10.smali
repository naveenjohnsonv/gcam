.class final Lcom/marco/fixes/Fixes$10;
.super Ljava/lang/Object;
.source "Fixes.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/marco/fixes/Fixes;->lastFTPmod(Lorg/apache/commons/net/ftp/FTPClient;Z[Lorg/apache/commons/net/ftp/FTPFile;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lorg/apache/commons/net/ftp/FTPFile;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 1274
    check-cast p1, Lorg/apache/commons/net/ftp/FTPFile;

    check-cast p2, Lorg/apache/commons/net/ftp/FTPFile;

    invoke-virtual {p0, p1, p2}, Lcom/marco/fixes/Fixes$10;->compare(Lorg/apache/commons/net/ftp/FTPFile;Lorg/apache/commons/net/ftp/FTPFile;)I

    move-result p1

    return p1
.end method

.method public compare(Lorg/apache/commons/net/ftp/FTPFile;Lorg/apache/commons/net/ftp/FTPFile;)I
    .locals 2

    .line 1276
    invoke-virtual {p1}, Lorg/apache/commons/net/ftp/FTPFile;->getTimestamp()Ljava/util/Calendar;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p2}, Lorg/apache/commons/net/ftp/FTPFile;->getTimestamp()Ljava/util/Calendar;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide p1

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Long;->compare(JJ)I

    move-result p1

    return p1
.end method
