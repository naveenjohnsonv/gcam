.class public Lcom/marco/xmlButton/XmlButton;
.super Ljava/lang/Object;
.source "XmlButton.java"


# instance fields
.field private applicationContext:Landroid/content/Context;

.field public ll:Landroid/widget/LinearLayout;

.field public mOrientationEventListener:Landroid/view/OrientationEventListener;

.field public xml:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "xml"

    .line 40
    invoke-static {v0}, Lcom/marco/fixes/Fixes;->removeButton(Ljava/lang/String;)V

    .line 41
    invoke-static {}, Lcom/marco/fixes/Fixes;->inButtonHideModes()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "xmlbar"

    invoke-static {v0}, Lcom/marco/fixes/Fixes;->getButton(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 43
    :cond_0
    iput-object p1, p0, Lcom/marco/xmlButton/XmlButton;->applicationContext:Landroid/content/Context;

    .line 44
    invoke-direct {p0}, Lcom/marco/xmlButton/XmlButton;->xmlIndicator()V

    :cond_1
    :goto_0
    return-void
.end method

.method static synthetic access$000(Lcom/marco/xmlButton/XmlButton;)Landroid/content/Context;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/marco/xmlButton/XmlButton;->applicationContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$100(Lcom/marco/xmlButton/XmlButton;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/marco/xmlButton/XmlButton;->xmlBar()V

    return-void
.end method

.method static synthetic access$200(Lcom/marco/xmlButton/XmlButton;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/marco/xmlButton/XmlButton;->oldSwitch()V

    return-void
.end method

.method private adjustedXML(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 77
    invoke-static {p1}, Lcom/FixBSG;->MenuValueString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private barImageClick(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .line 175
    new-instance v0, Lcom/marco/xmlButton/XmlButton$3;

    invoke-direct {v0, p0, p1}, Lcom/marco/xmlButton/XmlButton$3;-><init>(Lcom/marco/xmlButton/XmlButton;Ljava/lang/String;)V

    return-object v0
.end method

.method private imageViewBar(Ljava/lang/String;)Landroid/widget/ImageView;
    .locals 5

    const-string v0, "pref_"

    const-string v1, ""

    .line 159
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "xml_key"

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 160
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v2, 0x76

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 161
    new-instance v2, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/marco/xmlButton/XmlButton;->applicationContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const-string v3, "close"

    .line 162
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 163
    invoke-static {p1}, Lcom/FixBSG;->MenuValueString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "NONE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    return-object v2

    :cond_0
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 165
    invoke-virtual {v2, v3, v4, v3, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 166
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const-string v1, "pref_category_barcolor"

    .line 167
    invoke-static {v1}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "bar-icons/icon"

    goto :goto_0

    :cond_1
    const-string v1, "bar-colored-icons/icon"

    .line 168
    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".png"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/marco/fixes/Fixes;->drawableFromAssets(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 169
    invoke-direct {p0, p1}, Lcom/marco/xmlButton/XmlButton;->barImageClick(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object p1

    invoke-virtual {v2, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    return-object v2
.end method

.method private oldSwitch()V
    .locals 8

    .line 235
    invoke-direct {p0}, Lcom/marco/xmlButton/XmlButton;->updateXmlIndicator()V

    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/marco/fixes/Fixes;->getLastXML()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".xml"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 237
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 238
    sget-object v2, Lcom/marco/strings/MarcosStrings;->menustrings:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 239
    invoke-static {v3}, Lcom/FixBSG;->MenuValueString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    :goto_1
    const-string v2, "NONE"

    .line 240
    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    .line 243
    :cond_1
    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    .line 244
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->applicationContext:Landroid/content/Context;

    const-string v1, "All standard configurations are set to NONE.\nSo this has no effect!"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    .line 247
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v7, Lcom/marco/strings/MarcosStrings;->xmlPath:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 249
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 250
    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-ne v4, v3, :cond_3

    .line 251
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->applicationContext:Landroid/content/Context;

    const-string v1, "You only have ONE standard configuration set.\nSo changing has no effect!"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    .line 254
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v0, v3

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v2

    rem-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 258
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 261
    :goto_2
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 267
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_5

    .line 268
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->applicationContext:Landroid/content/Context;

    const-string v1, "One or more chosen standard configs are not activated.\nGo to XML Menu and activate them!"

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    .line 271
    :cond_5
    invoke-static {}, Lcom/marco/fixes/Fixes;->getShpXML()Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 272
    invoke-static {}, Lcom/marco/fixes/Fixes;->getShpXML()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/marco/fixes/Fixes;->copyFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :cond_6
    invoke-static {}, Lcom/marco/fixes/Fixes;->restart()V

    return-void
.end method

.method private setIcon(Z)Ljava/lang/String;
    .locals 13

    .line 318
    invoke-static {}, Lcom/marco/fixes/Fixes;->getLastXML()Ljava/lang/String;

    move-result-object v0

    const-string v1, "pref_category_barcolor"

    .line 319
    invoke-static {v1}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "vf-icons/"

    goto :goto_0

    :cond_0
    const-string v1, "vf-colored-icons/"

    :goto_0
    const-string v2, "pref_dayxml_key"

    .line 320
    invoke-direct {p0, v2}, Lcom/marco/xmlButton/XmlButton;->adjustedXML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "pref_nightxml_key"

    .line 321
    invoke-direct {p0, v3}, Lcom/marco/xmlButton/XmlButton;->adjustedXML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "pref_peoplexml_key"

    .line 322
    invoke-direct {p0, v4}, Lcom/marco/xmlButton/XmlButton;->adjustedXML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "pref_foodxml_key"

    .line 323
    invoke-direct {p0, v5}, Lcom/marco/xmlButton/XmlButton;->adjustedXML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "pref_sportxml_key"

    .line 324
    invoke-direct {p0, v6}, Lcom/marco/xmlButton/XmlButton;->adjustedXML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "pref_naturexml_key"

    .line 325
    invoke-direct {p0, v7}, Lcom/marco/xmlButton/XmlButton;->adjustedXML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "pref_zoomxml_key"

    .line 326
    invoke-direct {p0, v8}, Lcom/marco/xmlButton/XmlButton;->adjustedXML(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, ""

    .line 328
    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    invoke-virtual {v5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_16

    :cond_1
    const-string v10, "pref_xmlswitch_key"

    invoke-static {v10}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_16

    .line 329
    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_2

    if-eqz p1, :cond_3

    :cond_2
    if-eqz p1, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/marco/strings/MarcosStrings;->xmlPath:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, "/.day"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v2, v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 330
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "iconday.png"

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 331
    :cond_4
    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    if-eqz p1, :cond_6

    :cond_5
    if-eqz p1, :cond_7

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/marco/strings/MarcosStrings;->xmlPath:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "/.night"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v2, v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 332
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "iconnight.png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 333
    :cond_7
    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    if-eqz p1, :cond_9

    :cond_8
    if-eqz p1, :cond_a

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/marco/strings/MarcosStrings;->xmlPath:Ljava/lang/String;

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "/.people"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 334
    :cond_9
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "iconpeople.png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 335
    :cond_a
    invoke-virtual {v5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    if-eqz p1, :cond_c

    :cond_b
    if-eqz p1, :cond_d

    invoke-virtual {v5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_d

    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/marco/strings/MarcosStrings;->xmlPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "/.food"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 336
    :cond_c
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "iconfood.png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 337
    :cond_d
    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_e

    if-eqz p1, :cond_f

    :cond_e
    if-eqz p1, :cond_10

    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_10

    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/marco/strings/MarcosStrings;->xmlPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "/.sport"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 338
    :cond_f
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "iconsport.png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 339
    :cond_10
    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_11

    if-eqz p1, :cond_12

    :cond_11
    if-eqz p1, :cond_13

    invoke-virtual {v7, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_13

    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/marco/strings/MarcosStrings;->xmlPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "/.nature"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 340
    :cond_12
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "iconnature.png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v9, v2

    .line 341
    :cond_13
    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_14

    if-eqz p1, :cond_15

    :cond_14
    if-eqz p1, :cond_16

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_16

    new-instance p1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/marco/strings/MarcosStrings;->xmlPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "/.zoom"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result p1

    if-eqz p1, :cond_16

    .line 342
    :cond_15
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "iconzoom.png"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    :cond_16
    return-object v9
.end method

.method private updateXmlIndicator()V
    .locals 9

    .line 279
    invoke-static {}, Lcom/marco/fixes/Fixes;->getLastXML()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Quick"

    .line 280
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    .line 281
    sput-boolean v2, Lcom/marco/FixMarco;->fastAndQuick:Z

    :cond_0
    const-string v1, "pref_category_XML_switch"

    .line 283
    invoke-static {v1}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v1

    const-string v3, ""

    const/4 v4, 0x0

    if-eqz v1, :cond_a

    invoke-static {}, Lcom/marco/fixes/Fixes;->inButtonHideModes()Z

    move-result v1

    if-eqz v1, :cond_1

    goto/16 :goto_4

    .line 290
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v5, 0x0

    const/4 v6, 0x2

    if-le v1, v2, :cond_2

    .line 291
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, v6

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 292
    invoke-virtual {v1, v5}, Ljava/lang/String;->codePointAt(I)I

    move-result v7

    const/16 v8, 0x2bc

    if-ge v7, v8, :cond_3

    .line 293
    invoke-virtual {v1, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 295
    :cond_3
    :goto_0
    iget-object v7, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 296
    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    const-string v7, "pref_category_XML_switch_background"

    invoke-static {v7}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v8

    if-ne v8, v2, :cond_4

    const-string v8, "vf-icons/button_empty.png"

    goto :goto_1

    :cond_4
    const-string v8, "vf-icons/button_empty_empty.png"

    .line 297
    :goto_1
    invoke-static {v8}, Lcom/marco/fixes/Fixes;->drawableFromAssets(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 296
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 298
    invoke-static {v7}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "pref_category_XML_switch_background_color"

    invoke-static {v1}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    .line 299
    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 301
    :cond_5
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v6, :cond_6

    return-void

    :cond_6
    const-string v0, "pref_category_bar"

    .line 303
    invoke-static {v0}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v2, :cond_7

    goto :goto_2

    :cond_7
    move v2, v5

    :goto_2
    invoke-direct {p0, v2}, Lcom/marco/xmlButton/XmlButton;->setIcon(Z)Ljava/lang/String;

    move-result-object v0

    .line 304
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_8

    .line 305
    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/marco/fixes/Fixes;->drawableFromAssets(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    .line 308
    :cond_8
    invoke-direct {p0, v5}, Lcom/marco/xmlButton/XmlButton;->setIcon(Z)Ljava/lang/String;

    move-result-object v0

    .line 309
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_9

    .line 310
    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311
    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/marco/fixes/Fixes;->drawableFromAssets(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_9
    :goto_3
    return-void

    .line 284
    :cond_a
    :goto_4
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 285
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 286
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 287
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    return-void
.end method

.method private xmlBar()V
    .locals 4

    .line 122
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->applicationContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/marco/xmlButton/XmlButton;->ll:Landroid/widget/LinearLayout;

    .line 123
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 124
    sget-object v1, Lcom/marco/strings/MarcosStrings;->menustrings:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "xml"

    .line 125
    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 126
    invoke-static {v2}, Lcom/FixBSG;->MenuValueString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 127
    iget-object v3, p0, Lcom/marco/xmlButton/XmlButton;->ll:Landroid/widget/LinearLayout;

    invoke-direct {p0, v2}, Lcom/marco/xmlButton/XmlButton;->imageViewBar(Ljava/lang/String;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 130
    :cond_1
    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->ll:Landroid/widget/LinearLayout;

    const-string v2, "close"

    invoke-direct {p0, v2}, Lcom/marco/xmlButton/XmlButton;->imageViewBar(Ljava/lang/String;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 134
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-nez v1, :cond_2

    const-string v0, "No Standard Configuration selected!"

    .line 135
    invoke-static {v0}, Lcom/marco/FixMarco;->toaster(Ljava/lang/String;)V

    return-void

    .line 139
    :cond_2
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 140
    iget-object v2, p0, Lcom/marco/xmlButton/XmlButton;->ll:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 141
    iget-object v2, p0, Lcom/marco/xmlButton/XmlButton;->ll:Landroid/widget/LinearLayout;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 142
    iget-object v2, p0, Lcom/marco/xmlButton/XmlButton;->ll:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 143
    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->ll:Landroid/widget/LinearLayout;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "xmlbar"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    .line 144
    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->ll:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getX()F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setX(F)V

    .line 145
    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->ll:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getY()F

    move-result v2

    const/high16 v3, 0x41200000    # 10.0f

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setY(F)V

    .line 146
    sget v1, Lcom/marco/FixMarco;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 147
    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->ll:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getX()F

    move-result v2

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x78

    int-to-float v0, v0

    sub-float/2addr v2, v0

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setX(F)V

    .line 148
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->ll:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getY()F

    move-result v1

    const/high16 v2, 0x41a00000    # 20.0f

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setY(F)V

    .line 150
    :cond_3
    sget v0, Lcom/marco/FixMarco;->orientation:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 151
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->ll:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getX()F

    move-result v1

    add-float/2addr v1, v3

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setX(F)V

    .line 152
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->ll:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getY()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setY(F)V

    .line 154
    :cond_4
    sget-object v0, Lcom/marco/FixMarco;->parentView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 155
    sget-object v0, Lcom/marco/FixMarco;->parentView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method private xmlIndicator()V
    .locals 3

    .line 48
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->applicationContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    .line 50
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0x64

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 51
    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 52
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    const/high16 v1, 0x41c80000    # 25.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 53
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 54
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    const-string v1, "xml"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 55
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/marco/fixes/Fixes;->buttonOrientation(Landroid/widget/TextView;)V

    .line 56
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 57
    sget-object v0, Lcom/marco/FixMarco;->parentView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 59
    :cond_0
    invoke-static {}, Lcom/marco/fixes/Fixes;->isAutoRotaion()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    new-instance v0, Lcom/marco/xmlButton/XmlButton$1;

    iget-object v1, p0, Lcom/marco/xmlButton/XmlButton;->applicationContext:Landroid/content/Context;

    const/4 v2, 0x3

    invoke-direct {v0, p0, v1, v2}, Lcom/marco/xmlButton/XmlButton$1;-><init>(Lcom/marco/xmlButton/XmlButton;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/marco/xmlButton/XmlButton;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    .line 66
    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->canDetectOrientation()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/marco/xmlButton/XmlButton;->xmlOnClick()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLongClickable(Z)V

    .line 72
    iget-object v0, p0, Lcom/marco/xmlButton/XmlButton;->xml:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/marco/xmlButton/XmlButton;->xmlOnLongClick()Landroid/view/View$OnLongClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 73
    invoke-direct {p0}, Lcom/marco/xmlButton/XmlButton;->updateXmlIndicator()V

    return-void
.end method

.method private xmlOnClick()Landroid/view/View$OnClickListener;
    .locals 1

    .line 216
    new-instance v0, Lcom/marco/xmlButton/XmlButton$4;

    invoke-direct {v0, p0}, Lcom/marco/xmlButton/XmlButton$4;-><init>(Lcom/marco/xmlButton/XmlButton;)V

    return-object v0
.end method

.method private xmlOnLongClick()Landroid/view/View$OnLongClickListener;
    .locals 1

    .line 96
    new-instance v0, Lcom/marco/xmlButton/XmlButton$2;

    invoke-direct {v0, p0}, Lcom/marco/xmlButton/XmlButton$2;-><init>(Lcom/marco/xmlButton/XmlButton;)V

    return-object v0
.end method
