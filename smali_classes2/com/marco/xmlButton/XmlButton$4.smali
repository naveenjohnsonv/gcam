.class Lcom/marco/xmlButton/XmlButton$4;
.super Ljava/lang/Object;
.source "XmlButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/marco/xmlButton/XmlButton;->xmlOnClick()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/marco/xmlButton/XmlButton;


# direct methods
.method constructor <init>(Lcom/marco/xmlButton/XmlButton;)V
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/marco/xmlButton/XmlButton$4;->this$0:Lcom/marco/xmlButton/XmlButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    const-string p1, "pref_xmlswitch_key"

    .line 219
    invoke-static {p1}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "pref_category_XML_switch"

    invoke-static {p1}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 221
    :cond_0
    sget p1, Lcom/FixBSG;->sHdr_process:I

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    .line 222
    iget-object p1, p0, Lcom/marco/xmlButton/XmlButton$4;->this$0:Lcom/marco/xmlButton/XmlButton;

    invoke-static {p1}, Lcom/marco/xmlButton/XmlButton;->access$000(Lcom/marco/xmlButton/XmlButton;)Landroid/content/Context;

    move-result-object p1

    const-string v1, "HDR+ processing, try again after it has finished."

    invoke-static {p1, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    :cond_1
    const-string p1, "pref_category_bar"

    .line 226
    invoke-static {p1}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result p1

    if-ne p1, v0, :cond_2

    .line 227
    iget-object p1, p0, Lcom/marco/xmlButton/XmlButton$4;->this$0:Lcom/marco/xmlButton/XmlButton;

    invoke-static {p1}, Lcom/marco/xmlButton/XmlButton;->access$100(Lcom/marco/xmlButton/XmlButton;)V

    goto :goto_0

    .line 229
    :cond_2
    iget-object p1, p0, Lcom/marco/xmlButton/XmlButton$4;->this$0:Lcom/marco/xmlButton/XmlButton;

    invoke-static {p1}, Lcom/marco/xmlButton/XmlButton;->access$200(Lcom/marco/xmlButton/XmlButton;)V

    :cond_3
    :goto_0
    return-void
.end method
