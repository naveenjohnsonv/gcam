.class public Lcom/marco/awbButton/AwbButton;
.super Ljava/lang/Object;
.source "AwbButton.java"


# instance fields
.field private applicationContext:Landroid/content/Context;

.field public awb:Landroid/widget/TextView;

.field public mOrientationEventListener:Landroid/view/OrientationEventListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "awb"

    .line 33
    invoke-static {v0}, Lcom/marco/fixes/Fixes;->removeButton(Ljava/lang/String;)V

    .line 34
    invoke-static {}, Lcom/marco/fixes/Fixes;->inButtonHideModes()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 36
    :cond_0
    iput-object p1, p0, Lcom/marco/awbButton/AwbButton;->applicationContext:Landroid/content/Context;

    .line 37
    invoke-direct {p0}, Lcom/marco/awbButton/AwbButton;->awbIndicator()V

    return-void
.end method

.method static synthetic access$000(Lcom/marco/awbButton/AwbButton;)Landroid/view/View$OnClickListener;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/marco/awbButton/AwbButton;->awbOnClick()Landroid/view/View$OnClickListener;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/marco/awbButton/AwbButton;)Landroid/content/Context;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/marco/awbButton/AwbButton;->applicationContext:Landroid/content/Context;

    return-object p0
.end method

.method private awbIndicator()V
    .locals 4

    .line 42
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/marco/awbButton/AwbButton;->applicationContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    .line 44
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0x64

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 45
    iget-object v1, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 46
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    const/high16 v1, 0x41500000    # 13.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 47
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 48
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    const-string v1, "awb"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 50
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/marco/fixes/Fixes;->buttonOrientation(Landroid/widget/TextView;)V

    .line 51
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 52
    sget-object v0, Lcom/marco/FixMarco;->parentView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 54
    :cond_0
    invoke-static {}, Lcom/marco/fixes/Fixes;->isAutoRotaion()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    new-instance v0, Lcom/marco/awbButton/AwbButton$1;

    iget-object v1, p0, Lcom/marco/awbButton/AwbButton;->applicationContext:Landroid/content/Context;

    const/4 v2, 0x3

    invoke-direct {v0, p0, v1, v2}, Lcom/marco/awbButton/AwbButton$1;-><init>(Lcom/marco/awbButton/AwbButton;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/marco/awbButton/AwbButton;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    .line 62
    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->canDetectOrientation()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLongClickable(Z)V

    .line 67
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/marco/awbButton/AwbButton;->awbOnLongClick()Landroid/view/View$OnLongClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 68
    invoke-direct {p0}, Lcom/marco/awbButton/AwbButton;->updateAwbIndicator()V

    .line 69
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/marco/awbButton/AwbButton$2;

    invoke-direct {v1, p0}, Lcom/marco/awbButton/AwbButton$2;-><init>(Lcom/marco/awbButton/AwbButton;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private awbOnClick()Landroid/view/View$OnClickListener;
    .locals 1

    .line 79
    new-instance v0, Lcom/marco/awbButton/AwbButton$3;

    invoke-direct {v0, p0}, Lcom/marco/awbButton/AwbButton$3;-><init>(Lcom/marco/awbButton/AwbButton;)V

    return-object v0
.end method

.method private awbOnLongClick()Landroid/view/View$OnLongClickListener;
    .locals 1

    .line 113
    new-instance v0, Lcom/marco/awbButton/AwbButton$4;

    invoke-direct {v0, p0}, Lcom/marco/awbButton/AwbButton$4;-><init>(Lcom/marco/awbButton/AwbButton;)V

    return-object v0
.end method

.method private updateAwbIndicator()V
    .locals 6

    const-string v0, "pref_category_AWB_switch"

    .line 124
    invoke-static {v0}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v0

    const-string v1, ""

    const/4 v2, 0x0

    if-eqz v0, :cond_e

    invoke-static {}, Lcom/marco/fixes/Fixes;->inButtonHideModes()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_7

    :cond_0
    const-string v0, "pref_ehn_awb_key"

    .line 130
    invoke-static {v0}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v0

    const-string v3, "pref_googleawb_key"

    .line 131
    invoke-static {v3}, Lcom/FixBSG;->MenuValueString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "on"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 132
    sput-boolean v3, Lcom/marco/FixMarco;->awbIsOn:Z

    const-string v4, "pref_category_AWB_switch_background"

    const/4 v5, 0x1

    if-nez v3, :cond_2

    .line 136
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    invoke-static {v4}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v5, :cond_1

    const-string v1, "vf-icons/button_awb_off.png"

    goto :goto_0

    :cond_1
    const-string v1, "vf-icons/button_awb_off_empty.png"

    .line 137
    :goto_0
    invoke-static {v1}, Lcom/marco/fixes/Fixes;->drawableFromAssets(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 136
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const-string v1, "AWB\nOff"

    goto :goto_5

    :cond_2
    if-ne v0, v5, :cond_4

    .line 140
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    invoke-static {v4}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v5, :cond_3

    const-string v1, "vf-icons/button_awb_p3.png"

    goto :goto_1

    :cond_3
    const-string v1, "vf-icons/button_awb_p3_empty.png"

    .line 141
    :goto_1
    invoke-static {v1}, Lcom/marco/fixes/Fixes;->drawableFromAssets(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 140
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const-string v1, "AWB\nP3"

    goto :goto_5

    :cond_4
    const/4 v3, 0x4

    if-ne v0, v3, :cond_6

    .line 144
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    invoke-static {v4}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v5, :cond_5

    const-string v1, "vf-icons/button_awb_p4.png"

    goto :goto_2

    :cond_5
    const-string v1, "vf-icons/button_awb_p4_empty.png"

    .line 145
    :goto_2
    invoke-static {v1}, Lcom/marco/fixes/Fixes;->drawableFromAssets(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 144
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const-string v1, "AWB\nP4"

    goto :goto_5

    :cond_6
    const/4 v3, 0x2

    if-ne v0, v3, :cond_8

    .line 148
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    invoke-static {v4}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v5, :cond_7

    const-string v1, "vf-icons/button_awb_on.png"

    goto :goto_3

    :cond_7
    const-string v1, "vf-icons/button_awb_on_empty.png"

    .line 149
    :goto_3
    invoke-static {v1}, Lcom/marco/fixes/Fixes;->drawableFromAssets(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 148
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const-string v1, "AWB\nOn"

    goto :goto_5

    :cond_8
    const/4 v3, 0x3

    if-ne v0, v3, :cond_a

    .line 152
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    invoke-static {v4}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v5, :cond_9

    const-string v1, "vf-icons/button_awb_imx.png"

    goto :goto_4

    :cond_9
    const-string v1, "vf-icons/button_awb_imx_empty.png"

    .line 153
    :goto_4
    invoke-static {v1}, Lcom/marco/fixes/Fixes;->drawableFromAssets(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 152
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const-string v1, "AWB\nIMX"

    .line 155
    :cond_a
    :goto_5
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, "pref_category_AWB_switch_background_color"

    .line 156
    invoke-static {v0}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_c

    .line 157
    iget-object v1, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    invoke-static {v4}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v3

    if-ne v3, v5, :cond_b

    const-string v3, "vf-icons/button_empty.png"

    goto :goto_6

    :cond_b
    const-string v3, "vf-icons/button_empty_empty.png"

    .line 158
    :goto_6
    invoke-static {v3}, Lcom/marco/fixes/Fixes;->drawableFromAssets(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 157
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 159
    :cond_c
    invoke-static {v4}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_d

    invoke-static {v0}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_d

    .line 160
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_d
    return-void

    .line 125
    :cond_e
    :goto_7
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 127
    iget-object v0, p0, Lcom/marco/awbButton/AwbButton;->awb:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
