.class Lcom/marco/awbButton/AwbButton$3;
.super Ljava/lang/Object;
.source "AwbButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/marco/awbButton/AwbButton;->awbOnClick()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/marco/awbButton/AwbButton;


# direct methods
.method constructor <init>(Lcom/marco/awbButton/AwbButton;)V
    .locals 0

    .line 79
    iput-object p1, p0, Lcom/marco/awbButton/AwbButton$3;->this$0:Lcom/marco/awbButton/AwbButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .line 82
    sget p1, Lcom/FixBSG;->sHdr_process:I

    if-eqz p1, :cond_0

    .line 83
    iget-object p1, p0, Lcom/marco/awbButton/AwbButton$3;->this$0:Lcom/marco/awbButton/AwbButton;

    invoke-static {p1}, Lcom/marco/awbButton/AwbButton;->access$100(Lcom/marco/awbButton/AwbButton;)Landroid/content/Context;

    move-result-object p1

    const-string v0, "HDR+ processing, try again after it has finished."

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    const-string p1, "pref_ehn_awb_key"

    .line 87
    invoke-static {p1}, Lcom/FixBSG;->MenuValue(Ljava/lang/String;)I

    move-result v0

    const-string v1, "pref_googleawb_key"

    .line 88
    invoke-static {v1}, Lcom/FixBSG;->MenuValueString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "on"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v4, 0x2

    if-nez v2, :cond_1

    .line 91
    invoke-static {v1, v3}, Lcom/FixBSG;->setMenuValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/FixBSG;->setMenuValue(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const-string v2, "off"

    if-ne v0, v4, :cond_4

    .line 95
    invoke-static {}, Lcom/marco/fixes/Fixes;->awbXmlMode()I

    move-result v0

    if-eq v0, v4, :cond_3

    invoke-static {}, Lcom/marco/fixes/Fixes;->awbXmlMode()I

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    .line 98
    :cond_2
    invoke-static {}, Lcom/marco/fixes/Fixes;->awbXmlMode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/FixBSG;->setMenuValue(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 96
    :cond_3
    :goto_0
    invoke-static {v1, v2}, Lcom/FixBSG;->setMenuValue(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 100
    :cond_4
    invoke-static {v1, v2}, Lcom/FixBSG;->setMenuValue(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const/4 p1, 0x0

    .line 102
    :goto_2
    sget-object v0, Lcom/marco/FixMarco;->parentView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge p1, v0, :cond_6

    .line 103
    sget-object v0, Lcom/marco/FixMarco;->parentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 104
    sget-object v0, Lcom/marco/FixMarco;->parentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "awb"

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 105
    sget-object v0, Lcom/marco/FixMarco;->parentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    :cond_5
    add-int/lit8 p1, p1, 0x1

    goto :goto_2

    .line 107
    :cond_6
    invoke-static {}, Lcom/google/android/apps/camera/legacy/app/activity/main/CameraActivity;->reinit()V

    return-void
.end method
