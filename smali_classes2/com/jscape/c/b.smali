.class public Lcom/jscape/c/b;
.super Ljava/lang/Object;


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/Hashtable;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x19

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x23

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "eC\u0001\u0008Q\u0019gjE\u0002\u0000`\u0016qUI\u001eEK\u0003bD@\tX\u0018iH\u0008E^\u0002nDI\u001eE_\u0011#G^\u000b\u0010]\u0012mR_B"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x32

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/c/b;->b:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/4 v13, 0x5

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v14, 0x3

    if-eq v12, v14, :cond_4

    const/4 v14, 0x4

    if-eq v12, v14, :cond_3

    if-eq v12, v13, :cond_2

    const/16 v13, 0x20

    goto :goto_2

    :cond_2
    const/16 v13, 0x54

    goto :goto_2

    :cond_3
    const/16 v13, 0x13

    goto :goto_2

    :cond_4
    const/16 v13, 0x46

    goto :goto_2

    :cond_5
    const/16 v13, 0x4f

    goto :goto_2

    :cond_6
    const/16 v13, 0xf

    :cond_7
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>([Ljava/lang/String;)V
    .locals 4

    invoke-static {}, Lcom/jscape/c/a;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    array-length v1, p1

    rem-int/lit8 v1, v1, 0x2

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    sget-object v3, Lcom/jscape/c/b;->b:[Ljava/lang/String;

    aget-object v2, v3, v2

    invoke-static {v1, v2}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    iput-object v1, p0, Lcom/jscape/c/b;->a:Ljava/util/Hashtable;

    invoke-direct {p0, p1}, Lcom/jscape/c/b;->a([Ljava/lang/String;)V

    if-nez v0, :cond_2

    const/4 p1, 0x4

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_2
    return-void
.end method

.method private a([Ljava/lang/String;)V
    .locals 5

    invoke-static {}, Lcom/jscape/c/a;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/jscape/c/b;->a:Ljava/util/Hashtable;

    aget-object v3, p1, v1

    add-int/lit8 v4, v1, 0x1

    aget-object v4, p1, v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x2

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a()Ljava/util/Hashtable;
    .locals 1

    iget-object v0, p0, Lcom/jscape/c/b;->a:Ljava/util/Hashtable;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/c/b;->b:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/c/b;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
