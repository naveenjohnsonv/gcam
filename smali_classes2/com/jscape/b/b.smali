.class public Lcom/jscape/b/b;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String; = "\n"

.field private static final c:Ljava/lang/String; = "\r"

.field private static final f:[Ljava/lang/String;


# instance fields
.field private d:Ljava/lang/String;

.field private e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/jscape/b/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x4

    const-string v5, "x!\u001ce\u000czu\u001d|b\u0014d80\u0003f, \u0010<\u001dpR\u000fo 0\u0003a1\u001bb>4\u0003tr\u0014d$\u0010\u001fv~\u0004h82L2"

    const/16 v6, 0x32

    move v8, v3

    const/4 v7, -0x1

    const/4 v9, 0x0

    :goto_0
    const/16 v10, 0x38

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    const/4 v15, 0x0

    :goto_2
    const/4 v2, 0x2

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_1

    add-int/lit8 v12, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v12

    goto :goto_0

    :cond_0
    const-string v5, "D@\u0002D@"

    move v6, v0

    move v8, v2

    move v9, v12

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v2, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v2

    :goto_3
    const/16 v10, 0x27

    add-int/2addr v7, v11

    add-int v2, v7, v8

    invoke-virtual {v5, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/b/b;->f:[Ljava/lang/String;

    aget-object v0, v1, v3

    sput-object v0, Lcom/jscape/b/b;->a:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v4, v15, 0x7

    if-eqz v4, :cond_9

    if-eq v4, v11, :cond_8

    if-eq v4, v2, :cond_7

    const/4 v2, 0x3

    if-eq v4, v2, :cond_6

    if-eq v4, v3, :cond_5

    if-eq v4, v0, :cond_4

    const/16 v2, 0x39

    goto :goto_4

    :cond_4
    const/16 v2, 0x58

    goto :goto_4

    :cond_5
    const/16 v2, 0x29

    goto :goto_4

    :cond_6
    const/16 v2, 0x2d

    goto :goto_4

    :cond_7
    const/16 v2, 0x49

    goto :goto_4

    :cond_8
    const/16 v2, 0x6d

    goto :goto_4

    :cond_9
    const/16 v2, 0x6e

    :goto_4
    xor-int/2addr v2, v10

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v12, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/jscape/b/b;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/b/b;->d:Ljava/lang/String;

    new-instance p1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object p1, p0, Lcom/jscape/b/b;->e:Ljava/util/Set;

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/b/b;->d:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcom/jscape/b/c;)V
    .locals 1

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jscape/b/b;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/b/b;->f:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    const-string v1, "\n"

    invoke-virtual {p0, p1, v0, p1, v1}, Lcom/jscape/b/b;->a(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/io/File;Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/b/b;->f:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    const-string v1, "\n"

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/jscape/b/b;->a(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/b/l;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    :try_start_0
    invoke-virtual {p1, p3}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/jscape/b/b;->f:[Ljava/lang/String;

    aget-object v1, v4, v1

    invoke-static {v3, v1}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    goto :goto_1

    :cond_0
    move-object v2, p3

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/b/b;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_1
    move-object v2, p1

    :goto_0
    move-object v8, v2

    move v2, v1

    move-object v1, v8

    :goto_1
    const/4 v3, 0x0

    :try_start_1
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v6, Ljava/io/OutputStreamWriter;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-direct {v6, v5, v7}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v3, v6}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    new-instance v6, Lcom/jscape/util/LineReader;

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object p2

    invoke-direct {v6, v4, p2}, Lcom/jscape/util/LineReader;-><init>(Ljava/io/InputStream;[C)V

    :cond_2
    invoke-virtual {v6}, Lcom/jscape/util/LineReader;->readLine()Ljava/lang/String;

    move-result-object p2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz p2, :cond_5

    :try_start_4
    invoke-virtual {v3, p2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-nez v0, :cond_4

    :try_start_5
    invoke-virtual {v6}, Lcom/jscape/util/LineReader;->hasDelimiter()Z

    move-result p2
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-nez v0, :cond_3

    if-eqz p2, :cond_2

    :try_start_6
    invoke-virtual {v3, p4}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    move v2, p2

    goto :goto_4

    :cond_4
    :goto_2
    if-eqz v0, :cond_2

    const/4 p2, 0x4

    new-array p2, p2, [I

    invoke-static {p2}, Lcom/jscape/util/aq;->b([I)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3

    :catch_1
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/b/b;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catch_2
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/b/b;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_5
    :goto_3
    :try_start_9
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->flush()V

    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V

    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-nez v0, :cond_7

    :goto_4
    if-eqz v2, :cond_6

    :try_start_a
    new-instance p2, Lcom/jscape/b/e;

    invoke-direct {p2}, Lcom/jscape/b/e;-><init>()V

    invoke-virtual {p2, v1, p3}, Lcom/jscape/b/e;->c(Ljava/io/File;Ljava/io/File;)V

    :cond_6
    invoke-virtual {p0, p1, v1}, Lcom/jscape/b/b;->g(Ljava/io/File;Ljava/io/File;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    invoke-static {v4}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    invoke-static {v5}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    :cond_7
    return-void

    :catch_3
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/b/b;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_5

    :catchall_1
    move-exception p1

    move-object v5, v3

    :goto_5
    move-object v3, v4

    goto :goto_6

    :catchall_2
    move-exception p1

    move-object v5, v3

    :goto_6
    invoke-static {v3}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    invoke-static {v5}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    throw p1
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/b/b;->d:Ljava/lang/String;

    return-void
.end method

.method public b(Lcom/jscape/b/c;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/b/b;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public b(Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/b/b;->f:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    const-string v1, "\r"

    invoke-virtual {p0, p1, v0, p1, v1}, Lcom/jscape/b/b;->a(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public b(Ljava/io/File;Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/b/b;->f:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    const-string v1, "\r"

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/jscape/b/b;->a(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public c(Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/b/b;->f:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    const-string v1, "\n"

    invoke-virtual {p0, p1, v1, p1, v0}, Lcom/jscape/b/b;->a(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public c(Ljava/io/File;Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/b/b;->f:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    const-string v1, "\n"

    invoke-virtual {p0, p1, v1, p2, v0}, Lcom/jscape/b/b;->a(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public d(Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "\n"

    const-string v1, "\r"

    invoke-virtual {p0, p1, v0, p1, v1}, Lcom/jscape/b/b;->a(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public d(Ljava/io/File;Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "\n"

    const-string v1, "\r"

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/jscape/b/b;->a(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public e(Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/b/b;->f:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    const-string v1, "\r"

    invoke-virtual {p0, p1, v1, p1, v0}, Lcom/jscape/b/b;->a(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public e(Ljava/io/File;Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/b/b;->f:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    const-string v1, "\r"

    invoke-virtual {p0, p1, v1, p2, v0}, Lcom/jscape/b/b;->a(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public f(Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "\r"

    const-string v1, "\n"

    invoke-virtual {p0, p1, v0, p1, v1}, Lcom/jscape/b/b;->a(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public f(Ljava/io/File;Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "\r"

    const-string v1, "\n"

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/jscape/b/b;->a(Ljava/io/File;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method protected g(Ljava/io/File;Ljava/io/File;)V
    .locals 2

    new-instance v0, Lcom/jscape/b/i;

    invoke-direct {v0, p0, p1, p2}, Lcom/jscape/b/i;-><init>(Ljava/lang/Object;Ljava/io/File;Ljava/io/File;)V

    invoke-static {}, Lcom/jscape/b/l;->c()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/jscape/b/b;->e:Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/b/c;

    invoke-interface {v1, v0}, Lcom/jscape/b/c;->a(Lcom/jscape/b/i;)V

    if-eqz p1, :cond_0

    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/b/b;->f:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/b/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/b/b;->e:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
