.class public Lcom/jscape/b/a;
.super Ljava/lang/Object;


# instance fields
.field private a:Z

.field private b:Lcom/jscape/b/g;

.field private c:[Ljava/io/File;

.field private d:I


# direct methods
.method public constructor <init>([Ljava/io/File;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/b/a;->a:Z

    iput-object p1, p0, Lcom/jscape/b/a;->c:[Ljava/io/File;

    iput p2, p0, Lcom/jscape/b/a;->d:I

    return-void
.end method

.method static a(Lcom/jscape/b/a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/b/a;->a:Z

    return p1
.end method

.method static a(Lcom/jscape/b/a;)[Ljava/io/File;
    .locals 0

    iget-object p0, p0, Lcom/jscape/b/a;->c:[Ljava/io/File;

    return-object p0
.end method

.method static b(Lcom/jscape/b/a;)I
    .locals 0

    iget p0, p0, Lcom/jscape/b/a;->d:I

    return p0
.end method


# virtual methods
.method public a()V
    .locals 1

    invoke-static {}, Lcom/jscape/b/l;->c()Ljava/lang/String;

    move-result-object v0

    monitor-enter p0

    if-nez v0, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/b/a;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Lcom/jscape/b/g;

    invoke-direct {v0, p0}, Lcom/jscape/b/g;-><init>(Lcom/jscape/b/a;)V

    iput-object v0, p0, Lcom/jscape/b/a;->b:Lcom/jscape/b/g;

    invoke-virtual {v0}, Lcom/jscape/b/g;->start()V

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/b/a;->a:Z

    return-void
.end method
