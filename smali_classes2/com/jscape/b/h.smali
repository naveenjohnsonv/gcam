.class Lcom/jscape/b/h;
.super Ljava/lang/Thread;


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field final a:Lcom/jscape/b/d;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/16 v2, 0xd

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x5f

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "N\u0000z~9hKjHyt>d\u001c,I>[;m@-\u0000~nrc@h\u0006?p=eLk\u0001zys \u0004\u001b,I>[;m@-\u0006p=>nKj\rm=7yL~\u001cl<s "

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x46

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/b/h;->b:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    if-eqz v13, :cond_7

    if-eq v13, v8, :cond_6

    const/4 v14, 0x2

    if-eq v13, v14, :cond_5

    if-eq v13, v0, :cond_4

    const/4 v14, 0x4

    if-eq v13, v14, :cond_3

    const/4 v14, 0x5

    if-eq v13, v14, :cond_2

    const/16 v13, 0x7a

    goto :goto_2

    :cond_2
    const/16 v13, 0x5e

    goto :goto_2

    :cond_3
    move v13, v2

    goto :goto_2

    :cond_4
    const/16 v13, 0x42

    goto :goto_2

    :cond_5
    const/16 v13, 0x40

    goto :goto_2

    :cond_6
    const/16 v13, 0x37

    goto :goto_2

    :cond_7
    const/16 v13, 0x52

    :goto_2
    xor-int/2addr v13, v7

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method constructor <init>(Lcom/jscape/b/d;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/b/h;->a:Lcom/jscape/b/d;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/InterruptedException;)Ljava/lang/InterruptedException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public run()V
    .locals 10

    iget-object v0, p0, Lcom/jscape/b/h;->a:Lcom/jscape/b/d;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/jscape/b/d;->a(Lcom/jscape/b/d;Z)Z

    invoke-static {}, Lcom/jscape/b/l;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/jscape/b/h;->a:Lcom/jscape/b/d;

    invoke-static {v2}, Lcom/jscape/b/d;->a(Lcom/jscape/b/d;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    :goto_0
    :try_start_0
    iget-object v4, p0, Lcom/jscape/b/h;->a:Lcom/jscape/b/d;

    invoke-static {v4}, Lcom/jscape/b/d;->b(Lcom/jscape/b/d;)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_3

    if-nez v4, :cond_0

    return-void

    :cond_0
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v5, Lcom/jscape/b/h;->b:[Ljava/lang/String;

    const/4 v6, 0x0

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_1
    iget-object v4, p0, Lcom/jscape/b/h;->a:Lcom/jscape/b/d;

    invoke-static {v4}, Lcom/jscape/b/d;->a(Lcom/jscape/b/d;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v0, :cond_5

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/jscape/b/h;->a:Lcom/jscape/b/d;

    invoke-static {v4}, Lcom/jscape/b/d;->a(Lcom/jscape/b/d;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v4
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    if-nez v0, :cond_1

    cmp-long v7, v4, v2

    if-eqz v7, :cond_2

    :cond_1
    :try_start_2
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v3, Lcom/jscape/b/h;->b:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    move-wide v2, v4

    :cond_2
    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    :try_start_3
    iget-object v4, p0, Lcom/jscape/b/h;->a:Lcom/jscape/b/d;

    invoke-static {v4}, Lcom/jscape/b/d;->c(Lcom/jscape/b/d;)I

    move-result v4

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_0
    move-exception v2

    move-wide v8, v4

    move-object v4, v2

    move-wide v2, v8

    goto :goto_3

    :cond_4
    :goto_1
    :try_start_4
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    sget-object v5, Lcom/jscape/b/h;->b:[Ljava/lang/String;

    const/4 v7, 0x2

    aget-object v5, v5, v7

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/jscape/b/h;->a:Lcom/jscape/b/d;

    invoke-static {v4, v6}, Lcom/jscape/b/d;->a(Lcom/jscape/b/d;Z)Z
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_1
    move-exception v4

    :try_start_5
    invoke-static {v4}, Lcom/jscape/b/h;->a(Ljava/lang/InterruptedException;)Ljava/lang/InterruptedException;

    move-result-object v4

    throw v4
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_2

    :cond_5
    :goto_2
    return-void

    :catch_2
    move-exception v4

    :goto_3
    invoke-virtual {v4}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/b/h;->a(Ljava/lang/InterruptedException;)Ljava/lang/InterruptedException;

    move-result-object v0

    throw v0
.end method
