.class Lcom/jscape/filetransfer/AftpFileTransfer$SslHandshakeEventForwarder;
.super Ljava/lang/Object;

# interfaces
.implements Ljavax/net/ssl/HandshakeCompletedListener;


# instance fields
.field final a:Lcom/jscape/filetransfer/AftpFileTransfer;


# direct methods
.method private constructor <init>(Lcom/jscape/filetransfer/AftpFileTransfer;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer$SslHandshakeEventForwarder;->a:Lcom/jscape/filetransfer/AftpFileTransfer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(Lcom/jscape/filetransfer/AftpFileTransfer;Lcom/jscape/filetransfer/AftpFileTransfer$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer$SslHandshakeEventForwarder;-><init>(Lcom/jscape/filetransfer/AftpFileTransfer;)V

    return-void
.end method


# virtual methods
.method public handshakeCompleted(Ljavax/net/ssl/HandshakeCompletedEvent;)V
    .locals 3

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$SslHandshakeEventForwarder;->a:Lcom/jscape/filetransfer/AftpFileTransfer;

    new-instance v1, Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransfer$SslHandshakeEventForwarder;->a:Lcom/jscape/filetransfer/AftpFileTransfer;

    invoke-direct {v1, v2, p1}, Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljavax/net/ssl/HandshakeCompletedEvent;)V

    invoke-virtual {v0, v1}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method
