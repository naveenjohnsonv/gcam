.class public Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ftp/FtpErrorListener;


# instance fields
.field private final a:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lcom/jscape/inet/ftp/FtpDownloadEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lcom/jscape/inet/ftp/FtpUploadEvent;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lcom/jscape/filetransfer/FtpTransfer;


# direct methods
.method protected constructor <init>(Lcom/jscape/filetransfer/FtpTransfer;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->a:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->b:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method


# virtual methods
.method public changeDir(Lcom/jscape/inet/ftp/FtpChangeDirEvent;)V
    .locals 3

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    new-instance v1, Lcom/jscape/filetransfer/FileTransferChangeDirEvent;

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpChangeDirEvent;->getDirectory()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/jscape/filetransfer/FileTransferChangeDirEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jscape/filetransfer/FtpTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method public commandSent(Lcom/jscape/inet/ftp/FtpCommandEvent;)V
    .locals 3

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    new-instance v1, Lcom/jscape/filetransfer/FileTransferCommandEvent;

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpCommandEvent;->getCommand()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/jscape/filetransfer/FileTransferCommandEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jscape/filetransfer/FtpTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method public connected(Lcom/jscape/inet/ftp/FtpConnectedEvent;)V
    .locals 3

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    new-instance v1, Lcom/jscape/filetransfer/FileTransferConnectedEvent;

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpConnectedEvent;->getHostname()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/jscape/filetransfer/FileTransferConnectedEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jscape/filetransfer/FtpTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method public connectionLost(Lcom/jscape/inet/ftp/FtpConnectionLostEvent;)V
    .locals 0

    return-void
.end method

.method public createDir(Lcom/jscape/inet/ftp/FtpCreateDirEvent;)V
    .locals 4

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    new-instance v1, Lcom/jscape/filetransfer/FileTransferCreateDirEvent;

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpCreateDirEvent;->getDirectory()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpCreateDirEvent;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, v3, p1}, Lcom/jscape/filetransfer/FileTransferCreateDirEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jscape/filetransfer/FtpTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method public deleteDir(Lcom/jscape/inet/ftp/FtpDeleteDirEvent;)V
    .locals 4

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    new-instance v1, Lcom/jscape/filetransfer/FileTransferDeleteDirEvent;

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpDeleteDirEvent;->getDirectory()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpDeleteDirEvent;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, v3, p1}, Lcom/jscape/filetransfer/FileTransferDeleteDirEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jscape/filetransfer/FtpTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method public deleteFile(Lcom/jscape/inet/ftp/FtpDeleteFileEvent;)V
    .locals 4

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    new-instance v1, Lcom/jscape/filetransfer/FileTransferDeleteFileEvent;

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpDeleteFileEvent;->getFile()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpDeleteFileEvent;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, v3, p1}, Lcom/jscape/filetransfer/FileTransferDeleteFileEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jscape/filetransfer/FtpTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method public disconnected(Lcom/jscape/inet/ftp/FtpDisconnectedEvent;)V
    .locals 3

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    new-instance v1, Lcom/jscape/filetransfer/FileTransferDisconnectedEvent;

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpDisconnectedEvent;->getHostname()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/jscape/filetransfer/FileTransferDisconnectedEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jscape/filetransfer/FtpTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method public download(Lcom/jscape/inet/ftp/FtpDownloadEvent;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public error(Lcom/jscape/inet/ftp/FtpErrorEvent;)V
    .locals 12

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    new-instance v7, Lcom/jscape/filetransfer/FileTransferErrorEvent;

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpErrorEvent;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpErrorEvent;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpErrorEvent;->getLocalPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpErrorEvent;->getMode()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long p1, v8, v10

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    move v6, p1

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/jscape/filetransfer/FileTransferErrorEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v7}, Lcom/jscape/filetransfer/FtpTransfer;->raiseErrorEvent(Lcom/jscape/filetransfer/FileTransferErrorEvent;)V

    return-void
.end method

.method public listing(Lcom/jscape/inet/ftp/FtpListingEvent;)V
    .locals 0

    return-void
.end method

.method public progress(Lcom/jscape/inet/ftp/FtpProgressEvent;)V
    .locals 14

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    new-instance v13, Lcom/jscape/filetransfer/FileTransferProgressEvent;

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpProgressEvent;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpProgressEvent;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpProgressEvent;->getLocalFilePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpProgressEvent;->getMode()I

    move-result v6

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpProgressEvent;->getBytes()J

    move-result-wide v7

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpProgressEvent;->getReadBytes()J

    move-result-wide v9

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpProgressEvent;->getTotalBytes()J

    move-result-wide v11

    move-object v1, v13

    invoke-direct/range {v1 .. v12}, Lcom/jscape/filetransfer/FileTransferProgressEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJ)V

    invoke-virtual {v0, v13}, Lcom/jscape/filetransfer/FtpTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method public raiseDownloadEvent()V
    .locals 13

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/ftp/FtpDownloadEvent;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    new-instance v12, Lcom/jscape/filetransfer/FileTransferDownloadEvent;

    iget-object v3, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpDownloadEvent;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpDownloadEvent;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpDownloadEvent;->getLocalPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpDownloadEvent;->getSize()J

    move-result-wide v7

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpDownloadEvent;->getTime()J

    move-result-wide v9

    const/4 v11, 0x0

    move-object v2, v12

    invoke-direct/range {v2 .. v11}, Lcom/jscape/filetransfer/FileTransferDownloadEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZ)V

    invoke-virtual {v1, v12}, Lcom/jscape/filetransfer/FtpTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    :cond_0
    return-void
.end method

.method public raiseUploadEvent()V
    .locals 12

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->b:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/ftp/FtpUploadEvent;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    new-instance v11, Lcom/jscape/filetransfer/FileTransferUploadEvent;

    iget-object v3, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpUploadEvent;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpUploadEvent;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpUploadEvent;->getLocalPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpUploadEvent;->getSize()J

    move-result-wide v7

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpUploadEvent;->getTime()J

    move-result-wide v9

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/jscape/filetransfer/FileTransferUploadEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    invoke-virtual {v1, v11}, Lcom/jscape/filetransfer/FtpTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    :cond_0
    return-void
.end method

.method public renameFile(Lcom/jscape/inet/ftp/FtpRenameFileEvent;)V
    .locals 5

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    new-instance v1, Lcom/jscape/filetransfer/FileTransferRenameFileEvent;

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpRenameFileEvent;->getOldName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpRenameFileEvent;->getNewName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpRenameFileEvent;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, v3, v4, p1}, Lcom/jscape/filetransfer/FileTransferRenameFileEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jscape/filetransfer/FtpTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method public responseReceived(Lcom/jscape/inet/ftp/FtpResponseEvent;)V
    .locals 3

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    new-instance v1, Lcom/jscape/filetransfer/FileTransferResponseEvent;

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->c:Lcom/jscape/filetransfer/FtpTransfer;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpResponseEvent;->getResponse()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/jscape/filetransfer/FileTransferResponseEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jscape/filetransfer/FtpTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method public upload(Lcom/jscape/inet/ftp/FtpUploadEvent;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-void
.end method
