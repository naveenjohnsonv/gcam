.class Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;
.super Ljava/io/OutputStream;


# instance fields
.field private final a:Ljava/io/OutputStream;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:J

.field private final f:J

.field private final g:J

.field private h:J

.field private i:J

.field final j:Lcom/jscape/filetransfer/AftpFileTransfer;


# direct methods
.method private constructor <init>(Lcom/jscape/filetransfer/AftpFileTransfer;Ljava/io/OutputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJ)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->j:Lcom/jscape/filetransfer/AftpFileTransfer;

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    iput-object p2, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->a:Ljava/io/OutputStream;

    iput-object p3, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->d:Ljava/lang/String;

    iput-wide p6, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->e:J

    iput-wide p8, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->f:J

    iput-wide p10, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->g:J

    return-void
.end method

.method constructor <init>(Lcom/jscape/filetransfer/AftpFileTransfer;Ljava/io/OutputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLcom/jscape/filetransfer/AftpFileTransfer$1;)V
    .locals 0

    invoke-direct/range {p0 .. p11}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;-><init>(Lcom/jscape/filetransfer/AftpFileTransfer;Ljava/io/OutputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJ)V

    return-void
.end method

.method private a()V
    .locals 1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->c()V

    :cond_1
    return-void
.end method

.method private b()Z
    .locals 7

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->h:J

    iget-wide v3, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->i:J

    sub-long v3, v1, v3

    iget-wide v5, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->g:J

    cmp-long v3, v3, v5

    if-eqz v0, :cond_2

    if-gtz v3, :cond_1

    iget-wide v3, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->f:J

    cmp-long v3, v1, v3

    if-eqz v0, :cond_2

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v3, 0x1

    :cond_2
    move v0, v3

    :goto_1
    return v0
.end method

.method private c()V
    .locals 14

    iget-wide v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->h:J

    iput-wide v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->i:J

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->j:Lcom/jscape/filetransfer/AftpFileTransfer;

    new-instance v13, Lcom/jscape/filetransfer/FileTransferProgressEvent;

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->j:Lcom/jscape/filetransfer/AftpFileTransfer;

    iget-object v3, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->c:Ljava/lang/String;

    iget-object v5, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->d:Ljava/lang/String;

    iget-wide v6, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->e:J

    iget-wide v8, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->h:J

    add-long v7, v6, v8

    iget-wide v11, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->f:J

    const/4 v6, 0x1

    const-wide/16 v9, 0x0

    move-object v1, v13

    invoke-direct/range {v1 .. v12}, Lcom/jscape/filetransfer/FileTransferProgressEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJ)V

    invoke-virtual {v0, v13}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

.method public write(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    iget-wide v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->h:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->h:J

    invoke-direct {p0}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->a()V

    return-void
.end method

.method public write([BII)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    iget-wide p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->h:J

    int-to-long v0, p3

    add-long/2addr p1, v0

    iput-wide p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->h:J

    invoke-direct {p0}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;->a()V

    return-void
.end method
