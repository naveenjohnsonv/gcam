.class public Lcom/jscape/filetransfer/SftpFileTransferParameters;
.super Lcom/jscape/filetransfer/FileTransferParameters;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field protected clientKeyFile:Ljava/io/File;

.field protected clientKeyFilePassword:Ljava/lang/String;

.field protected downloadBlockSize:Lcom/jscape/util/ae;

.field protected timeZone:Ljava/util/TimeZone;

.field protected transferMode:Lcom/jscape/filetransfer/TransferMode;

.field protected uploadBlockSize:Lcom/jscape/util/ae;

.field protected wireEncoding:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "\u001c\u001a\u0004\u001fD\u0014sTx\u001d\u0000K\u0010AY@\u0014R\u0019\u001c\u001a\u0012\u0003A\u001e|Dq\u0014\u0016n\u0012~Uj\u0010\u001c[\u000c}B^LH\u0014\u001c\u001a\u0015\u0000_\u0015~_[\u0015-D\u0014q[i\u0018\u0015MF+c\\\u0005\u001fn\u0012~Un\u0003\u000eF\u0008tUH!\u000eZ\u001a\u007fUN\u0014\u001d[[iSV\u0018\nF\u000fYUC7\u0006D\u001e/\u0017\u000f\u001c\u001a\u0005\u001dI\u0015aV_\u0003\"G\u001fw\r"

    const/16 v4, 0x7d

    const/16 v5, 0x12

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x72

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x1c

    const/16 v3, 0x10

    const-string v5, "\u0004\u0002\u001e\u001eB\u0006OFA\u0006\u0013Y\rm\u0015\u0005\u000b\u0004\u0002\u001d\u001e]\u0006PGL\u000cJ"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x6a

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    const/4 v1, 0x3

    if-eqz v15, :cond_8

    if-eq v15, v9, :cond_7

    const/4 v2, 0x2

    if-eq v15, v2, :cond_9

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x60

    goto :goto_4

    :cond_4
    const/16 v1, 0x9

    goto :goto_4

    :cond_5
    const/16 v1, 0x5a

    goto :goto_4

    :cond_6
    const/16 v1, 0x1d

    goto :goto_4

    :cond_7
    const/16 v1, 0x48

    goto :goto_4

    :cond_8
    const/16 v1, 0x42

    :cond_9
    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Lcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/io/PrintStream;Ljava/io/File;Ljava/io/File;Ljava/lang/String;Lcom/jscape/filetransfer/TransferMode;Lcom/jscape/util/ae;Lcom/jscape/util/ae;Ljava/lang/String;Ljava/util/TimeZone;)V
    .locals 13

    move-object v12, p0

    sget-object v1, Lcom/jscape/filetransfer/Protocol;->SFTP:Lcom/jscape/filetransfer/Protocol;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/jscape/filetransfer/FileTransferParameters;-><init>(Lcom/jscape/filetransfer/Protocol;Ljava/lang/String;Ljava/lang/Integer;Lcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/io/PrintStream;Ljava/io/File;)V

    move-object/from16 v0, p11

    iput-object v0, v12, Lcom/jscape/filetransfer/SftpFileTransferParameters;->clientKeyFile:Ljava/io/File;

    move-object/from16 v0, p12

    iput-object v0, v12, Lcom/jscape/filetransfer/SftpFileTransferParameters;->clientKeyFilePassword:Ljava/lang/String;

    move-object/from16 v0, p13

    iput-object v0, v12, Lcom/jscape/filetransfer/SftpFileTransferParameters;->transferMode:Lcom/jscape/filetransfer/TransferMode;

    move-object/from16 v0, p14

    iput-object v0, v12, Lcom/jscape/filetransfer/SftpFileTransferParameters;->uploadBlockSize:Lcom/jscape/util/ae;

    move-object/from16 v0, p15

    iput-object v0, v12, Lcom/jscape/filetransfer/SftpFileTransferParameters;->downloadBlockSize:Lcom/jscape/util/ae;

    move-object/from16 v0, p16

    iput-object v0, v12, Lcom/jscape/filetransfer/SftpFileTransferParameters;->wireEncoding:Ljava/lang/String;

    move-object/from16 v0, p17

    iput-object v0, v12, Lcom/jscape/filetransfer/SftpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    return-void
.end method

.method private static b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public applySpecificTo(Lcom/jscape/filetransfer/SftpFileTransfer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->transferMode:Lcom/jscape/filetransfer/TransferMode;
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->transferMode:Lcom/jscape/filetransfer/TransferMode;

    invoke-virtual {v1, p1}, Lcom/jscape/filetransfer/TransferMode;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->uploadBlockSize:Lcom/jscape/util/ae;
    :try_end_2
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_2 .. :try_end_2} :catch_7

    if-eqz v0, :cond_2

    if-eqz v1, :cond_1

    :try_start_3
    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->uploadBlockSize:Lcom/jscape/util/ae;

    invoke-virtual {v1}, Lcom/jscape/util/ae;->a()J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {p1, v1}, Lcom/jscape/filetransfer/SftpFileTransfer;->setBlockTransferSize(I)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_3
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_3 .. :try_end_3} :catch_8

    :cond_1
    if-eqz v0, :cond_3

    :try_start_4
    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->downloadBlockSize:Lcom/jscape/util/ae;
    :try_end_4
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    :try_start_5
    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->downloadBlockSize:Lcom/jscape/util/ae;

    invoke-virtual {v1}, Lcom/jscape/util/ae;->a()J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {p1, v1}, Lcom/jscape/filetransfer/SftpFileTransfer;->setBlockTransferSize(I)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_5
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_2

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    :try_start_6
    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->wireEncoding:Ljava/lang/String;
    :try_end_6
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_6 .. :try_end_6} :catch_4

    if-eqz v0, :cond_4

    :try_start_7
    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->wireEncoding:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->setWireEncoding(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;

    goto :goto_3

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_7 .. :try_end_7} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_4
    :goto_3
    :try_start_8
    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {p1, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->setTimeZone(Ljava/util/TimeZone;)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_8
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_8 .. :try_end_8} :catch_6

    :cond_5
    return-void

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :catch_7
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_9 .. :try_end_9} :catch_8

    :catch_8
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public bridge synthetic applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransferParameters;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/SftpFileTransferParameters;

    move-result-object p1

    return-object p1
.end method

.method public applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/SftpFileTransferParameters;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-super {p0, p1}, Lcom/jscape/filetransfer/FileTransferParameters;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    instance-of v0, p1, Lcom/jscape/filetransfer/SftpFileTransfer;
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    check-cast p1, Lcom/jscape/filetransfer/SftpFileTransfer;

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransferParameters;->applySpecificTo(Lcom/jscape/filetransfer/SftpFileTransfer;)V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    return-object p0
.end method

.method public getClientKeyFile()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->clientKeyFile:Ljava/io/File;

    return-object v0
.end method

.method public getClientKeyFilePassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->clientKeyFilePassword:Ljava/lang/String;

    return-object v0
.end method

.method public getDownloadBlockSize()Lcom/jscape/util/ae;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->downloadBlockSize:Lcom/jscape/util/ae;

    return-object v0
.end method

.method public getTimeZone()Ljava/util/TimeZone;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    return-object v0
.end method

.method public getTransferMode()Lcom/jscape/filetransfer/TransferMode;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->transferMode:Lcom/jscape/filetransfer/TransferMode;

    return-object v0
.end method

.method public getUploadBlockSize()Lcom/jscape/util/ae;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->uploadBlockSize:Lcom/jscape/util/ae;

    return-object v0
.end method

.method public getWireEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->wireEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public setClientKeyFile(Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->clientKeyFile:Ljava/io/File;

    return-void
.end method

.method public setClientKeyFilePassword(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->clientKeyFilePassword:Ljava/lang/String;

    return-void
.end method

.method public setDownloadBlockSize(Lcom/jscape/util/ae;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->downloadBlockSize:Lcom/jscape/util/ae;

    return-void
.end method

.method public setTimeZone(Ljava/util/TimeZone;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    return-void
.end method

.method public setTransferMode(Lcom/jscape/filetransfer/TransferMode;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->transferMode:Lcom/jscape/filetransfer/TransferMode;

    return-void
.end method

.method public setUploadBlockSize(Lcom/jscape/util/ae;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->uploadBlockSize:Lcom/jscape/util/ae;

    return-void
.end method

.method public setWireEncoding(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->wireEncoding:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/SftpFileTransferParameters;->a:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->clientKeyFile:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->clientKeyFilePassword:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->transferMode:Lcom/jscape/filetransfer/TransferMode;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->uploadBlockSize:Lcom/jscape/util/ae;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->downloadBlockSize:Lcom/jscape/util/ae;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->wireEncoding:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
