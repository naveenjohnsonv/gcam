.class public Lcom/jscape/filetransfer/FileTransferProgressEvent;
.super Lcom/jscape/filetransfer/FileTransferEvent;


# static fields
.field public static final DOWNLOAD:I = 0x1

.field public static final UPLOAD:I

.field private static final i:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:J

.field private final g:J

.field private final h:J


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x7

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "\u0010\u0010k/K\u0007U]]h{\u0000\u0011\u0010\u0010a)D\u0003WzYa#w\u0003OT\r*\u0010\u0010\u0010l$T\rWIDh\u0016F\u0016S\u0001\u0017\u000c\u0010\u0010\u007f#F\u0006yEDh5\u001a!zYa#s\u0010ZRCk#U2ISW\u007f#T\u0011~JUc2\u0007\u0019YEDh5\u001a"

    const/16 v5, 0x5e

    const/16 v6, 0xc

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x36

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x15

    const-string v4, "mm\u001dT>z{\rmm\u0004T.~*\u00034\u0004^)\""

    move v6, v0

    move v8, v11

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x4b

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/filetransfer/FileTransferProgressEvent;->i:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v0, v14, 0x7

    if-eqz v0, :cond_9

    if-eq v0, v10, :cond_8

    const/4 v2, 0x2

    if-eq v0, v2, :cond_7

    const/4 v2, 0x3

    if-eq v0, v2, :cond_6

    const/4 v2, 0x4

    if-eq v0, v2, :cond_5

    const/4 v2, 0x5

    if-eq v0, v2, :cond_4

    const/16 v0, 0xd

    goto :goto_4

    :cond_4
    const/16 v0, 0x54

    goto :goto_4

    :cond_5
    const/16 v0, 0x11

    goto :goto_4

    :cond_6
    const/16 v0, 0x70

    goto :goto_4

    :cond_7
    const/16 v0, 0x3b

    goto :goto_4

    :cond_8
    const/4 v0, 0x6

    goto :goto_4

    :cond_9
    const/16 v0, 0xa

    :goto_4
    xor-int/2addr v0, v9

    xor-int/2addr v0, v15

    int-to-char v0, v0

    aput-char v0, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v0, 0x7

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;IJJ)V
    .locals 12

    const-string v4, ""

    const-wide/16 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move/from16 v5, p4

    move-wide/from16 v6, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v0 .. v11}, Lcom/jscape/filetransfer/FileTransferProgressEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJ)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;IJJJ)V
    .locals 12

    const-string v4, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move/from16 v5, p4

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move-wide/from16 v10, p9

    invoke-direct/range {v0 .. v11}, Lcom/jscape/filetransfer/FileTransferProgressEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJ)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJ)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/FileTransferEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    iput-object p2, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->d:Ljava/lang/String;

    iput p5, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->e:I

    iput-wide p6, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->f:J

    iput-wide p8, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->h:J

    iput-wide p10, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->g:J

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/filetransfer/FileTransferListener;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/jscape/filetransfer/FileTransferListener;->progress(Lcom/jscape/filetransfer/FileTransferProgressEvent;)V

    return-void
.end method

.method public getAbsolutePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getBytes()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->f:J

    return-wide v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalFilePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getMode()I
    .locals 1

    iget v0, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->e:I

    return v0
.end method

.method public getReadBytes()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->h:J

    return-wide v0
.end method

.method public getTotalBytes()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->g:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/FileTransferProgressEvent;->i:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->g:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->h:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/filetransfer/FileTransferProgressEvent;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
