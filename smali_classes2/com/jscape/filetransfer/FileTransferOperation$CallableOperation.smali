.class public Lcom/jscape/filetransfer/FileTransferOperation$CallableOperation;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/jscape/filetransfer/FileTransferOperation$CallableOperation;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final operation:Lcom/jscape/filetransfer/FileTransferOperation;

.field public final transfer:Lcom/jscape/filetransfer/FileTransfer;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xb

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x1

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    const-string v7, "\u001cS\'e4Y\tV\u0016!*\u001ds\u0012?{4U\u0016U<#r\'V\u000eY\u001c=7.X\nU\u00012c<X\u0014\r"

    invoke-virtual {v7, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v8, v4

    move v9, v2

    :goto_1
    if-gt v8, v9, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x29

    if-ge v3, v4, :cond_0

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v13, v4

    move v4, v3

    move v3, v13

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/filetransfer/FileTransferOperation$CallableOperation;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v10, v4, v9

    rem-int/lit8 v11, v9, 0x7

    if-eqz v11, :cond_7

    if-eq v11, v6, :cond_6

    if-eq v11, v0, :cond_5

    const/4 v12, 0x3

    if-eq v11, v12, :cond_4

    const/4 v12, 0x4

    if-eq v11, v12, :cond_3

    const/4 v12, 0x5

    if-eq v11, v12, :cond_2

    const/16 v11, 0x78

    goto :goto_2

    :cond_2
    const/16 v11, 0x35

    goto :goto_2

    :cond_3
    const/16 v11, 0x57

    goto :goto_2

    :cond_4
    const/16 v11, 0x15

    goto :goto_2

    :cond_5
    const/16 v11, 0x51

    goto :goto_2

    :cond_6
    const/16 v11, 0x71

    goto :goto_2

    :cond_7
    const/16 v11, 0x32

    :goto_2
    xor-int/2addr v11, v0

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v4, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransferOperation;Lcom/jscape/filetransfer/FileTransfer;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferOperation$CallableOperation;->operation:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/filetransfer/FileTransferOperation$CallableOperation;->transfer:Lcom/jscape/filetransfer/FileTransfer;

    return-void
.end method


# virtual methods
.method public call()Lcom/jscape/filetransfer/FileTransferOperation$CallableOperation;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferOperation$CallableOperation;->operation:Lcom/jscape/filetransfer/FileTransferOperation;

    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferOperation$CallableOperation;->transfer:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0, v1}, Lcom/jscape/filetransfer/FileTransferOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    return-object p0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/filetransfer/FileTransferOperation$CallableOperation;->call()Lcom/jscape/filetransfer/FileTransferOperation$CallableOperation;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/FileTransferOperation$CallableOperation;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferOperation$CallableOperation;->operation:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferOperation$CallableOperation;->transfer:Lcom/jscape/filetransfer/FileTransfer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
