.class public Lcom/jscape/filetransfer/FileTransferRemoteSort$FileSizeComparator;
.super Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    check-cast p1, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    check-cast p2, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    new-instance v1, Ljava/lang/Long;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilesize()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    new-instance p1, Ljava/lang/Long;

    invoke-virtual {p2}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilesize()J

    move-result-wide v2

    invoke-direct {p1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    if-eqz v0, :cond_0

    move-object p2, p1

    goto :goto_0

    :cond_0
    move-object p2, v1

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object v1, p2

    :goto_1
    invoke-virtual {v1, p1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result p1

    iget-boolean p2, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort$FileSizeComparator;->isAscendent:Z

    if-eqz v0, :cond_3

    if-nez p2, :cond_2

    neg-int p1, p1

    :cond_2
    move p2, p1

    :cond_3
    return p2
.end method
