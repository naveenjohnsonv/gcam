.class public Lcom/jscape/filetransfer/FileTransferRemoteSort;
.super Ljava/lang/Object;


# instance fields
.field private ascendent:Z

.field private comparator:Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;

.field private data:Ljava/util/Enumeration;


# direct methods
.method public constructor <init>(Ljava/util/Enumeration;Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort;->ascendent:Z

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort;->data:Ljava/util/Enumeration;

    iput-object p2, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort;->comparator:Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;

    return-void
.end method

.method private asArrayList(Ljava/util/Enumeration;)Ljava/util/List;
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-nez v0, :cond_0

    :cond_1
    return-object v1
.end method

.method private getDataByType(Ljava/util/List;ZZ)Ljava/util/List;
    .locals 4

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    invoke-virtual {v2}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->isDirectory()Z

    move-result v3

    if-eqz v0, :cond_3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_1

    if-eqz p2, :cond_2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-nez v0, :cond_a

    goto :goto_0

    :cond_1
    move v3, p2

    goto :goto_1

    :cond_2
    :goto_0
    invoke-virtual {v2}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->isLink()Z

    move-result v3

    :cond_3
    :goto_1
    if-eqz v0, :cond_6

    if-eqz v3, :cond_5

    if-eqz v0, :cond_4

    if-eqz p3, :cond_5

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-nez v0, :cond_a

    goto :goto_2

    :cond_4
    move v3, p3

    goto :goto_3

    :cond_5
    :goto_2
    invoke-virtual {v2}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->isDirectory()Z

    move-result v3

    :cond_6
    :goto_3
    if-eqz v0, :cond_7

    if-nez v3, :cond_a

    invoke-virtual {v2}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->isLink()Z

    move-result v3

    :cond_7
    if-eqz v0, :cond_8

    if-nez v3, :cond_a

    move v3, p2

    :cond_8
    if-eqz v0, :cond_9

    if-nez v3, :cond_a

    move v3, p3

    :cond_9
    if-eqz v0, :cond_a

    if-nez v3, :cond_a

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    if-nez v0, :cond_0

    :cond_b
    return-object v1
.end method


# virtual methods
.method public getComparator()Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort;->comparator:Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;

    return-object v0
.end method

.method public getData()Ljava/util/Enumeration;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort;->data:Ljava/util/Enumeration;

    return-object v0
.end method

.method public isAscendent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort;->ascendent:Z

    return v0
.end method

.method public setAscendent(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort;->ascendent:Z

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort;->comparator:Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;

    invoke-virtual {v0, p1}, Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;->setAscendent(Z)V

    return-void
.end method

.method public setComparator(Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort;->comparator:Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;

    return-void
.end method

.method public setData(Ljava/util/Enumeration;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort;->data:Ljava/util/Enumeration;

    return-void
.end method

.method public sort()Ljava/util/Enumeration;
    .locals 6

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort;->data:Ljava/util/Enumeration;

    invoke-direct {p0, v0}, Lcom/jscape/filetransfer/FileTransferRemoteSort;->asArrayList(Ljava/util/Enumeration;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/jscape/filetransfer/FileTransferRemoteSort;->getDataByType(Ljava/util/List;ZZ)Ljava/util/List;

    move-result-object v3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v0, v2, v1}, Lcom/jscape/filetransfer/FileTransferRemoteSort;->getDataByType(Ljava/util/List;ZZ)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v2, v2}, Lcom/jscape/filetransfer/FileTransferRemoteSort;->getDataByType(Ljava/util/List;ZZ)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort;->comparator:Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;

    invoke-static {v3, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort;->comparator:Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort;->comparator:Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-boolean v5, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort;->ascendent:Z

    if-eqz v4, :cond_1

    if-eqz v5, :cond_0

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    if-nez v4, :cond_1

    :cond_0
    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    invoke-static {v2}, Ljava/util/Collections;->enumeration(Ljava/util/Collection;)Ljava/util/Enumeration;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort;->data:Ljava/util/Enumeration;

    return-object v0
.end method
