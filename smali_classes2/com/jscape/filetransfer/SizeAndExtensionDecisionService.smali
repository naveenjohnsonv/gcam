.class public Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/filetransfer/CompressionDecisionService;


# static fields
.field public static final DEFAULT_COMPRESSION_EXCLUDED_EXTENSIONS:[Ljava/lang/String;

.field public static final DEFAULT_COMPRESSION_MIN_FILE_SIZE:Ljava/lang/Long;

.field private static final c:[Ljava/lang/String;


# instance fields
.field private final a:J

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 23

    const/16 v0, 0x4e

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "}:J\\6P~5\u007fKa-Hn?iFK;O6\u0003\u007fvU\u0005\u007fsAB9\u0004\u007fi\u0018^\u0004\u007foGE\u0004\u007foL\u0014\u0004\u007f{]G\u0004\u007fyIW\u0004\u007fkKE\u0004\u007fvU\\\u0004\u007f~HG\u0008\u007fjNV!Uf6\u0004\u007fyNF\u0004\u007f`@K\u0004\u007f{]N\u0004\u007frDM\u0004\u007f`FT\u0003\u007f~K\u0005\u007fjNUc\u0004\u007fiKE\u0004\u007fvGE\u0003\u007fhD\u0004\u007f~NV\u0003\u007foL\u0004\u007fb_\u0017\u0004\u007fmFI\u0005\u007fiFP-\u0004\u007fy_P\u0004\u007fvUK\u0004\u007fbNV\u0003\u007fxG\u0004\u007foL\u0016\u0004\u007f~BC\u0004\u007fsLA\u0004\u007f{LA\u0002\u007fp\u0004\u007fjNO\u0002\u007f`\u0004\u007fo]\u0016\u0005\u007f`FT-\u0004\u007fjFI\u0005\u007fvUI4\u0003\u007fhU\u001c\u0015\u007fIE P\u007f\u0002\u007f]R<_nqaBM;zb=\u007f|M/Y6\u0004\u007fi^\\\u0005\u007fnM^g\u0004\u007fjFP\u0004\u007fhNV\u0003\u007f`U\u0004\u007f{C^\u0004\u007foLJ\u0004\u007fqHF\u0005\u007fjNUm\u0004\u007fnC^\u0004\u007foLE\u0004\u007foJ\u0016\u0003\u007f-U\u0004\u007f{IE\u0003\u007frN\u0004\u007fnH^\u0006\u007fiIE\'W\u0004\u007fiJJ\u0002\u007f\\\u0004\u007fjJE\u0018\u0013{K\u00048Ueq|FH0\u001cx8`J\u0004#]g$\u007f\u0001\u0002\u007f@\u0004\u007f}LE\u0004\u007fiFP\u0004\u007f{_O\u0004\u007fxU\u0016\u0004\u007fvUL\u0003\u007fbU\u0003\u007fxN\u0004\u007fiJE\u0004\u007fcU\u0015\u0005\u007fjNUb"

    const/16 v5, 0x1b3

    move v8, v3

    const/4 v6, -0x1

    const/16 v7, 0x15

    :goto_0
    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v7

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/16 v12, 0x4b

    move v14, v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v15, v10

    move v1, v3

    :goto_2
    const/16 v17, 0x40

    const/16 v18, 0x1e

    const/16 v19, 0x1a

    const/4 v2, 0x2

    const/4 v11, 0x4

    if-gt v15, v1, :cond_3

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    const/16 v10, 0x8

    if-eqz v13, :cond_1

    add-int/lit8 v2, v8, 0x1

    aput-object v1, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v2

    goto :goto_0

    :cond_0
    const-string v4, "ucCV\u0003uw_"

    move v8, v2

    move v5, v10

    move v7, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v13, v8, 0x1

    aput-object v1, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v1

    move v8, v13

    :goto_3
    add-int/2addr v6, v9

    add-int v1, v6, v7

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v13, v3

    const/16 v14, 0x41

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->c:[Ljava/lang/String;

    const-wide/32 v0, 0xa00000

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->DEFAULT_COMPRESSION_MIN_FILE_SIZE:Ljava/lang/Long;

    new-array v0, v12, [Ljava/lang/String;

    sget-object v1, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->c:[Ljava/lang/String;

    const/16 v4, 0x45

    aget-object v5, v1, v4

    aput-object v5, v0, v3

    const/16 v3, 0x3e

    aget-object v5, v1, v3

    aput-object v5, v0, v9

    const/16 v5, 0x4d

    aget-object v5, v1, v5

    aput-object v5, v0, v2

    aget-object v5, v1, v9

    const/4 v6, 0x3

    aput-object v5, v0, v6

    const/16 v5, 0x29

    aget-object v6, v1, v5

    aput-object v6, v0, v11

    const/16 v6, 0x1c

    aget-object v7, v1, v6

    const/4 v8, 0x5

    aput-object v7, v0, v8

    const/16 v7, 0x2a

    aget-object v8, v1, v7

    const/4 v9, 0x6

    aput-object v8, v0, v9

    const/16 v8, 0x3c

    aget-object v13, v1, v8

    const/4 v14, 0x7

    aput-object v13, v0, v14

    const/16 v13, 0x47

    aget-object v13, v1, v13

    aput-object v13, v0, v10

    const/16 v13, 0x9

    const/16 v15, 0x25

    aget-object v15, v1, v15

    aput-object v15, v0, v13

    const/16 v13, 0xa

    const/16 v15, 0x41

    aget-object v16, v1, v15

    aput-object v16, v0, v13

    const/16 v13, 0xb

    aget-object v2, v1, v2

    aput-object v2, v0, v13

    const/16 v2, 0xc

    const/16 v13, 0x38

    aget-object v13, v1, v13

    aput-object v13, v0, v2

    const/16 v2, 0xd

    const/4 v13, 0x3

    aget-object v13, v1, v13

    aput-object v13, v0, v2

    const/16 v2, 0xe

    const/16 v13, 0x22

    aget-object v13, v1, v13

    aput-object v13, v0, v2

    const/16 v2, 0xf

    const/16 v13, 0x39

    aget-object v13, v1, v13

    aput-object v13, v0, v2

    const/16 v2, 0x10

    const/16 v13, 0x31

    aget-object v13, v1, v13

    aput-object v13, v0, v2

    const/16 v2, 0x11

    const/16 v13, 0x44

    aget-object v13, v1, v13

    aput-object v13, v0, v2

    const/16 v2, 0x12

    aget-object v9, v1, v9

    aput-object v9, v0, v2

    const/16 v2, 0x13

    const/16 v9, 0xe

    aget-object v9, v1, v9

    aput-object v9, v0, v2

    const/16 v2, 0x14

    const/16 v9, 0x48

    aget-object v9, v1, v9

    aput-object v9, v0, v2

    aget-object v2, v1, v18

    const/16 v9, 0x15

    aput-object v2, v0, v9

    const/16 v2, 0x16

    const/16 v9, 0xc

    aget-object v9, v1, v9

    aput-object v9, v0, v2

    const/16 v2, 0x17

    aget-object v9, v1, v14

    aput-object v9, v0, v2

    const/16 v2, 0x18

    const/16 v9, 0x1b

    aget-object v9, v1, v9

    aput-object v9, v0, v2

    const/16 v2, 0x19

    const/16 v9, 0x16

    aget-object v9, v1, v9

    aput-object v9, v0, v2

    const/16 v2, 0x11

    aget-object v2, v1, v2

    aput-object v2, v0, v19

    const/16 v2, 0x1b

    const/16 v9, 0xa

    aget-object v9, v1, v9

    aput-object v9, v0, v2

    const/16 v2, 0x20

    aget-object v2, v1, v2

    aput-object v2, v0, v6

    const/16 v2, 0x1d

    const/16 v6, 0x42

    aget-object v6, v1, v6

    aput-object v6, v0, v2

    const/16 v2, 0x3a

    aget-object v2, v1, v2

    aput-object v2, v0, v18

    const/16 v2, 0x1f

    const/16 v6, 0xf

    aget-object v6, v1, v6

    aput-object v6, v0, v2

    const/16 v2, 0x20

    const/16 v6, 0x21

    aget-object v6, v1, v6

    aput-object v6, v0, v2

    const/16 v2, 0x21

    const/16 v6, 0x23

    aget-object v6, v1, v6

    aput-object v6, v0, v2

    const/16 v2, 0x22

    const/16 v6, 0x33

    aget-object v6, v1, v6

    aput-object v6, v0, v2

    const/16 v2, 0x23

    const/16 v6, 0x46

    aget-object v6, v1, v6

    aput-object v6, v0, v2

    const/16 v2, 0x24

    const/16 v6, 0x14

    aget-object v6, v1, v6

    aput-object v6, v0, v2

    const/16 v2, 0x25

    const/16 v6, 0x9

    aget-object v6, v1, v6

    aput-object v6, v0, v2

    const/16 v2, 0x26

    const/16 v6, 0x24

    aget-object v6, v1, v6

    aput-object v6, v0, v2

    const/16 v2, 0x27

    const/16 v6, 0xb

    aget-object v6, v1, v6

    aput-object v6, v0, v2

    const/16 v2, 0x28

    const/16 v6, 0x12

    aget-object v6, v1, v6

    aput-object v6, v0, v2

    aget-object v2, v1, v12

    aput-object v2, v0, v5

    const/16 v2, 0x34

    aget-object v2, v1, v2

    aput-object v2, v0, v7

    const/16 v2, 0x2b

    const/16 v5, 0x3f

    aget-object v5, v1, v5

    aput-object v5, v0, v2

    const/16 v2, 0x2c

    const/16 v5, 0x28

    aget-object v5, v1, v5

    aput-object v5, v0, v2

    const/16 v2, 0x2d

    const/16 v5, 0x2e

    aget-object v5, v1, v5

    aput-object v5, v0, v2

    const/16 v2, 0x2e

    aget-object v5, v1, v10

    aput-object v5, v0, v2

    const/16 v2, 0x2f

    const/16 v5, 0x2f

    aget-object v5, v1, v5

    aput-object v5, v0, v2

    const/16 v2, 0x30

    const/16 v20, 0x15

    aget-object v5, v1, v20

    aput-object v5, v0, v2

    const/16 v2, 0x31

    const/16 v5, 0x13

    aget-object v5, v1, v5

    aput-object v5, v0, v2

    const/16 v2, 0x32

    const/16 v5, 0x49

    aget-object v5, v1, v5

    aput-object v5, v0, v2

    const/16 v2, 0x33

    const/16 v5, 0x3d

    aget-object v5, v1, v5

    aput-object v5, v0, v2

    const/16 v2, 0x34

    const/16 v5, 0x4c

    aget-object v5, v1, v5

    aput-object v5, v0, v2

    const/16 v2, 0x35

    const/16 v5, 0x43

    aget-object v5, v1, v5

    aput-object v5, v0, v2

    const/16 v2, 0x36

    aget-object v5, v1, v19

    aput-object v5, v0, v2

    const/16 v2, 0x37

    const/16 v5, 0x2c

    aget-object v5, v1, v5

    aput-object v5, v0, v2

    const/16 v2, 0x38

    const/16 v5, 0x3b

    aget-object v5, v1, v5

    aput-object v5, v0, v2

    const/16 v2, 0x39

    const/16 v5, 0x2d

    aget-object v5, v1, v5

    aput-object v5, v0, v2

    const/16 v2, 0x3a

    const/16 v5, 0x35

    aget-object v5, v1, v5

    aput-object v5, v0, v2

    const/16 v2, 0x3b

    const/16 v5, 0x17

    aget-object v5, v1, v5

    aput-object v5, v0, v2

    const/4 v2, 0x5

    aget-object v2, v1, v2

    aput-object v2, v0, v8

    const/16 v2, 0x3d

    const/16 v5, 0x1f

    aget-object v5, v1, v5

    aput-object v5, v0, v2

    const/16 v2, 0x32

    aget-object v2, v1, v2

    aput-object v2, v0, v3

    const/16 v2, 0x3f

    const/16 v3, 0x26

    aget-object v3, v1, v3

    aput-object v3, v0, v2

    const/16 v2, 0x37

    aget-object v2, v1, v2

    aput-object v2, v0, v17

    const/16 v2, 0x36

    aget-object v2, v1, v2

    const/16 v21, 0x41

    aput-object v2, v0, v21

    const/16 v2, 0x42

    aget-object v3, v1, v11

    aput-object v3, v0, v2

    const/16 v2, 0x43

    const/16 v3, 0x19

    aget-object v3, v1, v3

    aput-object v3, v0, v2

    const/16 v2, 0x44

    const/16 v3, 0x1d

    aget-object v3, v1, v3

    aput-object v3, v0, v2

    const/16 v2, 0x18

    aget-object v2, v1, v2

    aput-object v2, v0, v4

    const/16 v2, 0x46

    const/16 v3, 0x4a

    aget-object v3, v1, v3

    aput-object v3, v0, v2

    const/16 v2, 0x47

    const/16 v3, 0x10

    aget-object v3, v1, v3

    aput-object v3, v0, v2

    const/16 v2, 0x48

    const/16 v3, 0x27

    aget-object v3, v1, v3

    aput-object v3, v0, v2

    const/16 v2, 0x49

    const/16 v3, 0xd

    aget-object v3, v1, v3

    aput-object v3, v0, v2

    const/16 v2, 0x4a

    const/16 v3, 0x30

    aget-object v1, v1, v3

    aput-object v1, v0, v2

    sput-object v0, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->DEFAULT_COMPRESSION_EXCLUDED_EXTENSIONS:[Ljava/lang/String;

    return-void

    :cond_3
    const/16 v20, 0x15

    const/16 v21, 0x41

    aget-char v22, v10, v1

    rem-int/lit8 v3, v1, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v9, :cond_8

    if-eq v3, v2, :cond_7

    const/4 v2, 0x3

    if-eq v3, v2, :cond_6

    if-eq v3, v11, :cond_5

    const/4 v2, 0x5

    if-eq v3, v2, :cond_4

    goto :goto_4

    :cond_4
    const/16 v17, 0x77

    goto :goto_4

    :cond_5
    move/from16 v17, v18

    goto :goto_4

    :cond_6
    const/16 v17, 0x6f

    goto :goto_4

    :cond_7
    const/16 v17, 0x64

    goto :goto_4

    :cond_8
    const/16 v17, 0x51

    goto :goto_4

    :cond_9
    move/from16 v17, v19

    :goto_4
    xor-int v2, v14, v17

    xor-int v2, v22, v2

    int-to-char v2, v2

    aput-char v2, v10, v1

    add-int/lit8 v1, v1, 0x1

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method public constructor <init>(JLjava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-eqz v0, :cond_1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    sget-object v1, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->c:[Ljava/lang/String;

    const/16 v2, 0x40

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    iput-wide p1, p0, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->a:J

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    iput-object p1, p0, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->b:Ljava/util/List;

    invoke-direct {p0, p3}, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->a(Ljava/util/List;)V

    return-void
.end method

.method public varargs constructor <init>(Ljava/lang/Long;[Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, v0, v1, p1}, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;-><init>(JLjava/util/List;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->b:Ljava/util/List;

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private a(J)Z
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->a:J

    cmp-long p1, p1, v1

    if-eqz v0, :cond_1

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    return p1
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v0, :cond_4

    if-eqz v0, :cond_2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    if-nez v0, :cond_0

    goto :goto_1

    :cond_2
    :goto_0
    return v2

    :cond_3
    :goto_1
    const/4 v2, 0x1

    :cond_4
    return v2
.end method

.method public static defaultService()Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;
    .locals 3

    new-instance v0, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;

    sget-object v1, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->DEFAULT_COMPRESSION_MIN_FILE_SIZE:Ljava/lang/Long;

    sget-object v2, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->DEFAULT_COMPRESSION_EXCLUDED_EXTENSIONS:[Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;-><init>(Ljava/lang/Long;[Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public compressionRequiredFor(Ljava/lang/String;J)Z
    .locals 1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, p3}, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->a(J)Z

    move-result p2

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->a(Ljava/lang/String;)Z

    move-result p2

    :cond_0
    if-eqz v0, :cond_2

    if-eqz p2, :cond_1

    const/4 p2, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move p1, p2

    :goto_1
    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->c:[Ljava/lang/String;

    const/16 v2, 0x2b

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
