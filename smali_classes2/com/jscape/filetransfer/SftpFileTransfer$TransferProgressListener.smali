.class Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/sftp/FileService$TransferListener;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:J

.field private final f:J

.field final g:Lcom/jscape/filetransfer/SftpFileTransfer;


# direct methods
.method private constructor <init>(Lcom/jscape/filetransfer/SftpFileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJ)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;->g:Lcom/jscape/filetransfer/SftpFileTransfer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;->c:Ljava/lang/String;

    iput p5, p0, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;->d:I

    iput-wide p6, p0, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;->e:J

    iput-wide p8, p0, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;->f:J

    return-void
.end method

.method constructor <init>(Lcom/jscape/filetransfer/SftpFileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLcom/jscape/filetransfer/SftpFileTransfer$1;)V
    .locals 0

    invoke-direct/range {p0 .. p9}, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;-><init>(Lcom/jscape/filetransfer/SftpFileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJ)V

    return-void
.end method


# virtual methods
.method public onTransferProgress(J)V
    .locals 15

    move-object v0, p0

    iget-object v1, v0, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;->g:Lcom/jscape/filetransfer/SftpFileTransfer;

    new-instance v14, Lcom/jscape/filetransfer/FileTransferProgressEvent;

    iget-object v3, v0, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;->g:Lcom/jscape/filetransfer/SftpFileTransfer;

    iget-object v4, v0, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;->a:Ljava/lang/String;

    iget-object v5, v0, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;->b:Ljava/lang/String;

    iget-object v6, v0, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;->c:Ljava/lang/String;

    iget v7, v0, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;->d:I

    iget-wide v8, v0, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;->e:J

    add-long v8, v8, p1

    iget-wide v12, v0, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;->f:J

    const-wide/16 v10, 0x0

    move-object v2, v14

    invoke-direct/range {v2 .. v13}, Lcom/jscape/filetransfer/FileTransferProgressEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJ)V

    invoke-virtual {v1, v14}, Lcom/jscape/filetransfer/SftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method
