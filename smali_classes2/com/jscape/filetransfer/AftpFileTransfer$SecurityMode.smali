.class public abstract enum Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final $VALUES:[Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

.field public static final enum AUTHENTICATION_PROTECTED:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

.field public static final enum FULL_TIME_PROTECTED:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

.field public static final enum NO_PROTECTION:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xd

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x1

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    const-string v7, "cO&mIIShC-tTH\u0013kU5qDRN`E&mIIShC-x_\u0018lU-u^HSdC8iRIIrP+rOCDyE="

    invoke-virtual {v7, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v8, v4

    move v9, v2

    :goto_1
    const/4 v10, 0x2

    if-gt v8, v9, :cond_1

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v8, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x3a

    if-ge v3, v4, :cond_0

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v8

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    new-instance v3, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode$1;

    aget-object v4, v1, v2

    invoke-direct {v3, v4, v2}, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode$1;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->NO_PROTECTION:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    new-instance v3, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode$2;

    aget-object v4, v1, v6

    invoke-direct {v3, v4, v6}, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode$2;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->FULL_TIME_PROTECTED:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    new-instance v3, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode$3;

    aget-object v1, v1, v10

    invoke-direct {v3, v1, v10}, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode$3;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->AUTHENTICATION_PROTECTED:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    new-array v0, v0, [Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    sget-object v1, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->NO_PROTECTION:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->FULL_TIME_PROTECTED:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    aput-object v1, v0, v6

    aput-object v3, v0, v10

    sput-object v0, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->$VALUES:[Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    return-void

    :cond_1
    aget-char v11, v4, v9

    rem-int/lit8 v12, v9, 0x7

    const/16 v13, 0x7b

    if-eqz v12, :cond_6

    if-eq v12, v6, :cond_5

    if-eq v12, v10, :cond_7

    if-eq v12, v0, :cond_4

    const/4 v10, 0x4

    if-eq v12, v10, :cond_3

    const/4 v10, 0x5

    if-eq v12, v10, :cond_2

    const/16 v10, 0x7c

    goto :goto_2

    :cond_2
    const/16 v10, 0x7d

    goto :goto_2

    :cond_3
    const/16 v10, 0x60

    goto :goto_2

    :cond_4
    const/16 v10, 0x46

    goto :goto_2

    :cond_5
    move v10, v13

    goto :goto_2

    :cond_6
    const/16 v10, 0x56

    :cond_7
    :goto_2
    xor-int/2addr v10, v13

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v4, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILcom/jscape/filetransfer/AftpFileTransfer$1;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static modeFor(Ljava/lang/String;)Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;
    .locals 0

    :try_start_0
    invoke-static {p0}, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->valueOf(Ljava/lang/String;)Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    sget-object p0, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->NO_PROTECTION:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;
    .locals 1

    const-class v0, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    return-object p0
.end method

.method public static values()[Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;
    .locals 1

    sget-object v0, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->$VALUES:[Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    invoke-virtual {v0}, [Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    return-object v0
.end method


# virtual methods
.method public abstract apply(Lcom/jscape/inet/a/k;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method protected login(Lcom/jscape/inet/a/k;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {p4}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    :try_start_1
    instance-of v1, p1, Lcom/jscape/inet/a/p;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    :cond_0
    if-eqz v1, :cond_1

    :try_start_2
    move-object v1, p1

    check-cast v1, Lcom/jscape/inet/a/p;

    invoke-virtual {v1, p4}, Lcom/jscape/inet/a/p;->e(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    if-nez v0, :cond_2

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-interface {p1, p2, p3}, Lcom/jscape/inet/a/k;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-void

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method
