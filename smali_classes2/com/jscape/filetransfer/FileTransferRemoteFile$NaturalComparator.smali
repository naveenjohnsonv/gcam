.class public Lcom/jscape/filetransfer/FileTransferRemoteFile$NaturalComparator;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/jscape/filetransfer/FileTransferRemoteFile;Lcom/jscape/filetransfer/FileTransferRemoteFile;)I
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->a(Lcom/jscape/filetransfer/FileTransferRemoteFile;)Z

    move-result v1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    invoke-static {p2}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->a(Lcom/jscape/filetransfer/FileTransferRemoteFile;)Z

    move-result v1

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    invoke-static {p2}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->a(Lcom/jscape/filetransfer/FileTransferRemoteFile;)Z

    move-result v1

    :cond_1
    if-eqz v0, :cond_3

    if-eqz v1, :cond_2

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->a(Lcom/jscape/filetransfer/FileTransferRemoteFile;)Z

    move-result v1

    if-eqz v0, :cond_3

    if-nez v1, :cond_2

    const/4 p1, 0x1

    return p1

    :cond_2
    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->b(Lcom/jscape/filetransfer/FileTransferRemoteFile;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p2}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->b(Lcom/jscape/filetransfer/FileTransferRemoteFile;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    :cond_3
    return v1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    check-cast p2, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/filetransfer/FileTransferRemoteFile$NaturalComparator;->compare(Lcom/jscape/filetransfer/FileTransferRemoteFile;Lcom/jscape/filetransfer/FileTransferRemoteFile;)I

    move-result p1

    return p1
.end method
