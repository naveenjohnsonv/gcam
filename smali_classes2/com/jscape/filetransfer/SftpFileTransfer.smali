.class public Lcom/jscape/filetransfer/SftpFileTransfer;
.super Lcom/jscape/filetransfer/AbstractFileTransfer;


# static fields
.field private static final a:I = 0x16

.field private static final b:Lcom/jscape/util/Time;

.field private static final c:I = 0x7fee

.field private static final d:I = 0x8

.field private static final e:Ljava/lang/String; = "."

.field private static final f:Ljava/lang/String; = "/"

.field private static final g:Lcom/jscape/util/Time;

.field private static final h:Lcom/jscape/inet/ssh/util/KeyPairAssembler;

.field private static final i:I = 0x3

.field private static final j:I = 0x3

.field private static final k:[B

.field private static final z:[Ljava/lang/String;


# instance fields
.field private l:Ljava/lang/Integer;

.field private m:Ljava/lang/Boolean;

.field private n:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

.field private o:Ljava/security/KeyPair;

.field private p:Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

.field private q:Lcom/jscape/inet/sftp/SftpConfiguration;

.field private r:I

.field private s:Z

.field private t:I

.field private u:Ljava/lang/String;

.field private v:Lcom/jscape/inet/sftp/FileService;

.field private volatile w:Lcom/jscape/filetransfer/UploadDirectoryOperation;

.field private volatile x:Lcom/jscape/filetransfer/DownloadDirectoryOperation;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "\u0006\u00183kg\u0013X!\u0002%z7\u0015R!\u0005)qyM\'\u0000\u0006%}~\u0005^6\u0012`nv\u0017_sQem0C^ V.qcCVs\u0012)lr\u0000C<\u000490\u0019\u0011\u0017$>g\nG6\u001a)prC@:\u0018$q`CD:\u000c%0\u0014\u0006\u00183kg\u0013X!\u0002%z7\u0015R!\u0005)qyM$}\\\u001bnG>l27\u001dEd0j\u0008\u0005\u0013CL\u0014`\u000e-/QJ8E\u0001+\u001bzS>\u0019y.\u0015\u001f,{7\u0013R!\u001b)md\nX=\u0005`wy\u0005X!\u001b!j~\u000cYs\u001f3>y\u000cCs\u00176\u007f~\u000fV1\u001a%0\u000e?\u001f.{9\u0010R#\u00172\u007fc\u000cE\u0002^|\r\u00000\u0014N77E2\u00183xr\u0011\u0017\u0006\u00183kg\u0013X!\u0002%z7\u0008R*V&qe\u000eV\'X\u0003>\u0012u\u0002}X"

    const/16 v4, 0xfe

    const/16 v5, 0x14

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x71

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x5

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v13, v10

    move v14, v2

    :goto_2
    const/4 v15, 0x2

    if-gt v13, v14, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v12, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x3e

    const/16 v3, 0x19

    const-string v5, "ecPJ\u0013~3Bn]\u0004\u000674NlP\u0005\u001470NxQD$\t(o\u001a3J\u0018FCi1\u0010D\u001e|qg78`\u0014zY[%>L1u_o\u000e\'Jm\r"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v17, v5

    move v5, v3

    move-object/from16 v3, v17

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    add-int/2addr v6, v9

    add-int v8, v6, v5

    invoke-virtual {v3, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v12, v2

    move v8, v11

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/filetransfer/SftpFileTransfer;->z:[Ljava/lang/String;

    const-wide/16 v0, 0x3c

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->seconds(J)Lcom/jscape/util/Time;

    move-result-object v0

    sput-object v0, Lcom/jscape/filetransfer/SftpFileTransfer;->b:Lcom/jscape/util/Time;

    const-wide/16 v0, 0xa

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->seconds(J)Lcom/jscape/util/Time;

    move-result-object v0

    sput-object v0, Lcom/jscape/filetransfer/SftpFileTransfer;->g:Lcom/jscape/util/Time;

    new-instance v0, Lcom/jscape/inet/ssh/util/KeyPairAssembler;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;-><init>()V

    sput-object v0, Lcom/jscape/filetransfer/SftpFileTransfer;->h:Lcom/jscape/inet/ssh/util/KeyPairAssembler;

    new-array v0, v15, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jscape/filetransfer/SftpFileTransfer;->k:[B

    return-void

    :cond_3
    aget-char v16, v10, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v9, :cond_8

    if-eq v1, v15, :cond_7

    const/4 v15, 0x3

    if-eq v1, v15, :cond_6

    const/4 v15, 0x4

    if-eq v1, v15, :cond_5

    if-eq v1, v11, :cond_4

    const/16 v1, 0x46

    goto :goto_4

    :cond_4
    const/16 v1, 0x12

    goto :goto_4

    :cond_5
    const/16 v1, 0x66

    goto :goto_4

    :cond_6
    const/16 v1, 0x6f

    goto :goto_4

    :cond_7
    const/16 v1, 0x31

    goto :goto_4

    :cond_8
    const/4 v1, 0x7

    goto :goto_4

    :cond_9
    const/16 v1, 0x22

    :goto_4
    xor-int/2addr v1, v8

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v10, v14

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2

    :array_0
    .array-data 1
        0xdt
        0xat
    .end array-data
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/util/SshParameters;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/SftpConfiguration;->defaultConfiguration()Lcom/jscape/inet/sftp/SftpConfiguration;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;-><init>(Lcom/jscape/inet/ssh/util/SshParameters;Lcom/jscape/inet/sftp/SftpConfiguration;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/util/SshParameters;Lcom/jscape/inet/sftp/SftpConfiguration;)V
    .locals 1

    sget-object v0, Ljava/util/logging/Level;->OFF:Ljava/util/logging/Level;

    invoke-static {v0}, Lcom/jscape/util/j/b;->a(Ljava/util/logging/Level;)Lcom/jscape/util/j/b;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;-><init>(Lcom/jscape/inet/ssh/util/SshParameters;Lcom/jscape/inet/sftp/SftpConfiguration;Ljava/util/logging/Logger;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/util/SshParameters;Lcom/jscape/inet/sftp/SftpConfiguration;Ljava/util/logging/Logger;)V
    .locals 28

    move-object/from16 v0, p0

    move-object/from16 v16, p2

    move-object/from16 v26, p3

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyPort()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyUsername()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyPassword()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/ssh/util/SshParameters;->getHostname()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/ssh/util/SshParameters;->getPort()I

    move-result v7

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/ssh/util/SshParameters;->getConnectionTimeout()J

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/ssh/util/SshParameters;->getReadingTimeout()J

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/jscape/util/Time;->millis(J)Lcom/jscape/util/Time;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/ssh/util/SshParameters;->getSocketTrafficClass()Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/ssh/util/SshParameters;->getTcpNoDelay()Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/ssh/util/SshParameters;->getHostKeyVerifier()Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/ssh/util/SshParameters;->getUsername()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/ssh/util/SshParameters;->getPassword()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/ssh/util/SshParameters;->getKeyPair()Ljava/security/KeyPair;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/ssh/util/SshParameters;->getClientAuthentication()Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    move-result-object v15

    sget-object v24, Lcom/jscape/filetransfer/SftpFileTransfer;->DEFAULT_LOCAL_DIRECTORY:Ljava/io/File;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v27

    const/16 v17, 0x3

    const/16 v18, 0x7fee

    const/16 v19, 0x0

    const/16 v20, 0x8

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const-string v25, "."

    invoke-direct/range {v0 .. v27}, Lcom/jscape/filetransfer/SftpFileTransfer;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/jscape/util/Time;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;Lcom/jscape/inet/sftp/SftpConfiguration;IIZIZZZLjava/io/File;Ljava/lang/String;Ljava/util/logging/Logger;Ljava/util/Set;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 20

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v15, p3

    move-object/from16 v16, p4

    new-instance v13, Lcom/jscape/inet/ssh/util/SshParameters;

    move-object v0, v13

    sget-object v1, Lcom/jscape/filetransfer/SftpFileTransfer;->b:Lcom/jscape/util/Time;

    invoke-virtual {v1}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v8

    sget-object v1, Lcom/jscape/filetransfer/SftpFileTransfer;->b:Lcom/jscape/util/Time;

    invoke-virtual {v1}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v10

    sget-object v14, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;->NULL:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v12, 0x0

    const/16 v17, 0x0

    move-object/from16 v19, v13

    move-object/from16 v13, v17

    const/16 v18, 0x0

    invoke-direct/range {v0 .. v18}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLjava/lang/Integer;Ljava/lang/Boolean;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/jscape/filetransfer/SftpFileTransfer;-><init>(Lcom/jscape/inet/ssh/util/SshParameters;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/jscape/inet/sftp/SftpConfiguration;)V
    .locals 20

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v15, p3

    move-object/from16 v16, p4

    new-instance v13, Lcom/jscape/inet/ssh/util/SshParameters;

    move-object v0, v13

    sget-object v1, Lcom/jscape/filetransfer/SftpFileTransfer;->b:Lcom/jscape/util/Time;

    invoke-virtual {v1}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v8

    sget-object v1, Lcom/jscape/filetransfer/SftpFileTransfer;->b:Lcom/jscape/util/Time;

    invoke-virtual {v1}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v10

    sget-object v14, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;->NULL:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v12, 0x0

    const/16 v17, 0x0

    move-object/from16 v19, v13

    move-object/from16 v13, v17

    const/16 v18, 0x0

    invoke-direct/range {v0 .. v18}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLjava/lang/Integer;Ljava/lang/Boolean;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, v19

    invoke-direct {v0, v2, v1}, Lcom/jscape/filetransfer/SftpFileTransfer;-><init>(Lcom/jscape/inet/ssh/util/SshParameters;Lcom/jscape/inet/sftp/SftpConfiguration;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/jscape/util/Time;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;Lcom/jscape/inet/sftp/SftpConfiguration;IIZIZZZLjava/io/File;Ljava/lang/String;Ljava/util/logging/Logger;Ljava/util/Set;)V
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Lcom/jscape/util/Time;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            "Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/security/KeyPair;",
            "Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;",
            "Lcom/jscape/inet/sftp/SftpConfiguration;",
            "IIZIZZZ",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            "Ljava/util/logging/Logger;",
            "Ljava/util/Set<",
            "Lcom/jscape/filetransfer/FileTransferListener;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v15, p0

    move/from16 v0, p17

    move/from16 v14, p20

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v11, p12

    move-object/from16 v12, p13

    move/from16 v13, p18

    move/from16 v17, p21

    move/from16 v18, p22

    move/from16 v19, p23

    move-object/from16 v21, p24

    move-object/from16 v22, p26

    move-object/from16 v23, p27

    invoke-virtual/range {p8 .. p8}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v9

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v20

    const/16 v16, 0x0

    move/from16 v14, v16

    const/16 v16, 0x2

    move/from16 v15, v16

    const/16 v16, 0x0

    invoke-direct/range {v1 .. v23}, Lcom/jscape/filetransfer/AbstractFileTransfer;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;IZILjava/lang/String;ZZZLjava/util/TimeZone;Ljava/io/File;Ljava/util/logging/Logger;Ljava/util/Set;)V

    move-object/from16 v2, p9

    iput-object v2, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->l:Ljava/lang/Integer;

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v3, p10

    iput-object v3, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->m:Ljava/lang/Boolean;

    invoke-static/range {p11 .. p11}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object/from16 v3, p11

    iput-object v3, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->n:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    move-object/from16 v3, p14

    iput-object v3, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->o:Ljava/security/KeyPair;

    move-object/from16 v3, p15

    iput-object v3, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->p:Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    :try_start_0
    invoke-static/range {p16 .. p16}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object/from16 v3, p16

    iput-object v3, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->q:Lcom/jscape/inet/sftp/SftpConfiguration;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v3, 0x3

    if-eqz v2, :cond_2

    if-eq v0, v3, :cond_1

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    move v2, v0

    :goto_1
    sget-object v4, Lcom/jscape/filetransfer/SftpFileTransfer;->z:[Ljava/lang/String;

    aget-object v3, v4, v3

    invoke-static {v2, v3}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    iput v0, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->r:I

    move/from16 v0, p19

    iput-boolean v0, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->s:Z

    move/from16 v0, p20

    int-to-long v2, v0

    const-wide/16 v5, 0x0

    const/4 v7, 0x2

    aget-object v4, v4, v7

    invoke-static {v2, v3, v5, v6, v4}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput v0, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->t:I

    invoke-static/range {p25 .. p25}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object/from16 v0, p25

    iput-object v0, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 20

    move-object/from16 v6, p1

    move-object/from16 v15, p2

    move-object/from16 v16, p3

    new-instance v13, Lcom/jscape/inet/ssh/util/SshParameters;

    move-object v0, v13

    sget-object v1, Lcom/jscape/filetransfer/SftpFileTransfer;->b:Lcom/jscape/util/Time;

    invoke-virtual {v1}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v8

    sget-object v1, Lcom/jscape/filetransfer/SftpFileTransfer;->b:Lcom/jscape/util/Time;

    invoke-virtual {v1}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v10

    sget-object v14, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;->NULL:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x16

    const/4 v12, 0x0

    const/16 v17, 0x0

    move-object/from16 v19, v13

    move-object/from16 v13, v17

    const/16 v18, 0x0

    invoke-direct/range {v0 .. v18}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLjava/lang/Integer;Ljava/lang/Boolean;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/jscape/filetransfer/SftpFileTransfer;-><init>(Lcom/jscape/inet/ssh/util/SshParameters;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 21

    move-object/from16 v6, p1

    move-object/from16 v15, p2

    move-object/from16 v16, p3

    move-object/from16 v17, p4

    new-instance v13, Lcom/jscape/inet/ssh/util/SshParameters;

    move-object v0, v13

    sget-object v1, Lcom/jscape/filetransfer/SftpFileTransfer;->b:Lcom/jscape/util/Time;

    invoke-virtual {v1}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v8

    sget-object v1, Lcom/jscape/filetransfer/SftpFileTransfer;->b:Lcom/jscape/util/Time;

    invoke-virtual {v1}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v10

    sget-object v14, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;->NULL:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0x16

    const/4 v12, 0x0

    const/16 v18, 0x0

    move-object/from16 v20, v13

    move-object/from16 v13, v18

    const-string v18, ""

    const/16 v19, 0x0

    invoke-direct/range {v0 .. v19}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLjava/lang/Integer;Ljava/lang/Boolean;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/jscape/filetransfer/SftpFileTransfer;-><init>(Lcom/jscape/inet/ssh/util/SshParameters;)V

    return-void
.end method

.method private a(Lcom/jscape/inet/sftp/FileService$ServerException;)Lcom/jscape/filetransfer/FileTransferException;
    .locals 2

    const/4 v0, 0x3

    :try_start_0
    iget v1, p1, Lcom/jscape/inet/sftp/FileService$ServerException;->code:I

    if-ne v0, v1, :cond_0

    new-instance p1, Lcom/jscape/filetransfer/FileTransferDeniedException;

    invoke-direct {p1}, Lcom/jscape/filetransfer/FileTransferDeniedException;-><init>()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_0
    new-instance v0, Lcom/jscape/filetransfer/FileTransferException;

    invoke-direct {v0, p1}, Lcom/jscape/filetransfer/FileTransferException;-><init>(Ljava/lang/Throwable;)V

    return-object v0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    instance-of v1, p1, Lcom/jscape/inet/sftp/FileService$ServerException;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_5

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    check-cast p1, Lcom/jscape/inet/sftp/FileService$ServerException;

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Lcom/jscape/inet/sftp/FileService$ServerException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_6

    return-object p1

    :cond_0
    :try_start_2
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v0, :cond_3

    instance-of v1, v1, Lcom/jscape/inet/sftp/FileService$ServerException;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    :try_start_3
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/sftp/FileService$ServerException;

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Lcom/jscape/inet/sftp/FileService$ServerException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    return-object p1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    :cond_3
    if-eqz v0, :cond_4

    if-eqz v1, :cond_5

    :try_start_4
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    if-eqz v0, :cond_4

    :try_start_5
    instance-of v0, v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$InvalidCredentialsException;
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_3

    if-eqz v0, :cond_5

    :try_start_6
    new-instance p1, Lcom/jscape/filetransfer/FileTransferAuthenticationException;

    invoke-direct {p1}, Lcom/jscape/filetransfer/FileTransferAuthenticationException;-><init>()V
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_4

    return-object p1

    :catch_2
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_3

    :catch_3
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_4
    move-object p1, v1

    :cond_5
    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    return-object p1

    :catch_5
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_6

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private a(Lcom/jscape/inet/sftp/PathInfo;)Lcom/jscape/filetransfer/FileTransferRemoteFile;
    .locals 11

    new-instance v10, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    iget-object v1, p1, Lcom/jscape/inet/sftp/PathInfo;->name:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/jscape/inet/sftp/PathInfo;->directory()Z

    move-result v3

    invoke-virtual {p1}, Lcom/jscape/inet/sftp/PathInfo;->link()Z

    move-result v4

    iget-wide v5, p1, Lcom/jscape/inet/sftp/PathInfo;->size:J

    new-instance v7, Ljava/util/Date;

    iget-wide v8, p1, Lcom/jscape/inet/sftp/PathInfo;->modificationTime:J

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p1}, Lcom/jscape/inet/sftp/PathInfo;->readable()Z

    move-result v8

    invoke-virtual {p1}, Lcom/jscape/inet/sftp/PathInfo;->writable()Z

    move-result v9

    const-string v2, ""

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/jscape/filetransfer/FileTransferRemoteFile;-><init>(Ljava/lang/String;Ljava/lang/String;ZZJLjava/util/Date;ZZ)V

    return-object v10
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;
    .locals 12

    new-instance v11, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;

    const/4 v5, 0x1

    const/4 v10, 0x0

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    invoke-direct/range {v0 .. v10}, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;-><init>(Lcom/jscape/filetransfer/SftpFileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLcom/jscape/filetransfer/SftpFileTransfer$1;)V

    return-object v11
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-static {p1, p2}, Lcom/jscape/inet/sftp/PathTools;->resolve(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 11

    move-object v9, p0

    new-instance v10, Lcom/jscape/filetransfer/FileTransferDownloadEvent;

    iget-object v3, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-wide v5, p3

    move-wide/from16 v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/jscape/filetransfer/FileTransferDownloadEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    invoke-virtual {p0, v10}, Lcom/jscape/filetransfer/SftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;
    .locals 12

    new-instance v11, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;

    const/4 v5, 0x0

    const/4 v10, 0x0

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    invoke-direct/range {v0 .. v10}, Lcom/jscape/filetransfer/SftpFileTransfer$TransferProgressListener;-><init>(Lcom/jscape/filetransfer/SftpFileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLcom/jscape/filetransfer/SftpFileTransfer$1;)V

    return-object v11
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    new-instance v6, Lcom/jscape/filetransfer/FileTransferErrorEvent;

    iget-object v3, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    const/4 v5, 0x1

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/jscape/filetransfer/FileTransferErrorEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p0, v6}, Lcom/jscape/filetransfer/SftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 11

    move-object v9, p0

    new-instance v10, Lcom/jscape/filetransfer/FileTransferUploadEvent;

    iget-object v3, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p2

    move-object v4, p1

    move-wide v5, p3

    move-wide/from16 v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/jscape/filetransfer/FileTransferUploadEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    invoke-virtual {p0, v10}, Lcom/jscape/filetransfer/SftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    new-instance v6, Lcom/jscape/filetransfer/FileTransferErrorEvent;

    iget-object v3, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, v6

    move-object v1, p0

    move-object v2, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/jscape/filetransfer/FileTransferErrorEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p0, v6}, Lcom/jscape/filetransfer/SftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method


# virtual methods
.method public abortDownloadThread(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->x:Lcom/jscape/filetransfer/DownloadDirectoryOperation;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v1, p1}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->cancel(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public abortDownloadThreads()V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->x:Lcom/jscape/filetransfer/DownloadDirectoryOperation;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->cancel()V

    :cond_1
    return-void
.end method

.method public abortUploadThread(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->w:Lcom/jscape/filetransfer/UploadDirectoryOperation;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->cancel(Ljava/io/File;)V

    :cond_1
    return-void
.end method

.method public abortUploadThreads()V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->w:Lcom/jscape/filetransfer/UploadDirectoryOperation;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->cancel()V

    :cond_1
    return-void
.end method

.method protected authentication()Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;
    .locals 9

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->p:Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->p:Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :try_start_2
    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->password:Ljava/lang/String;

    invoke-static {v1}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v1
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_6

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v0, :cond_2

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    :try_start_3
    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->o:Ljava/security/KeyPair;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_8

    if-eqz v1, :cond_1

    :try_start_4
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    iget-object v5, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->username:Ljava/lang/String;

    iget-object v6, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->o:Ljava/security/KeyPair;

    invoke-static {v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->defaultAuthentication(Ljava/lang/String;Ljava/security/KeyPair;)Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;

    move-result-object v5

    aput-object v5, v1, v4

    iget-object v5, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->username:Ljava/lang/String;

    iget-object v6, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->password:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->defaultAuthentication(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;

    move-result-object v5

    aput-object v5, v1, v3

    iget-object v5, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->username:Ljava/lang/String;

    new-array v3, v3, [Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;

    sget-object v7, Lcom/jscape/filetransfer/SftpFileTransfer;->z:[Ljava/lang/String;

    const/16 v8, 0xd

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->password:Ljava/lang/String;

    invoke-direct {v6, v7, v8}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v3, v4

    invoke-static {v5, v3}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;->defaultAuthentication(Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;)Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;-><init>([Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_9

    return-object v0

    :cond_1
    if-eqz v0, :cond_3

    :try_start_5
    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->password:Ljava/lang/String;

    invoke-static {v1}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v1
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_2
    :goto_0
    if-eqz v1, :cond_3

    :try_start_6
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;

    new-array v1, v2, [Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    iget-object v2, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->username:Ljava/lang/String;

    iget-object v5, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->password:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->defaultAuthentication(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->username:Ljava/lang/String;

    new-array v5, v3, [Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;

    sget-object v7, Lcom/jscape/filetransfer/SftpFileTransfer;->z:[Ljava/lang/String;

    const/4 v8, 0x4

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->password:Ljava/lang/String;

    invoke-direct {v6, v7, v8}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v5, v4

    invoke-static {v2, v5}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;->defaultAuthentication(Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;)Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;-><init>([Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_3

    return-object v0

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_3
    if-eqz v0, :cond_4

    :try_start_7
    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->o:Ljava/security/KeyPair;
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_4

    if-eqz v0, :cond_4

    :try_start_8
    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->username:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->o:Ljava/security/KeyPair;

    invoke-static {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->defaultAuthentication(Ljava/lang/String;Ljava/security/KeyPair;)Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;

    move-result-object v0

    return-object v0

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_5

    :catch_5
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_4
    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->username:Ljava/lang/String;

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/authentication/NoneClientAuthentication;->defaultAuthentication(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/authentication/NoneClientAuthentication;

    move-result-object v0

    return-object v0

    :catch_6
    move-exception v0

    :try_start_9
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_7

    :catch_7
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_8

    :catch_8
    move-exception v0

    :try_start_b
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_9

    :catch_9
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public bridge synthetic connect()Lcom/jscape/filetransfer/FileTransfer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/filetransfer/SftpFileTransfer;->connect()Lcom/jscape/filetransfer/SftpFileTransfer;

    move-result-object v0

    return-object v0
.end method

.method public connect()Lcom/jscape/filetransfer/SftpFileTransfer;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/filetransfer/SftpFileTransfer;->initService()V

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/jscape/inet/sftp/FileService;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/jscape/inet/sftp/FileService;->infoAbout(Ljava/lang/String;Z)Lcom/jscape/inet/sftp/PathInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/jscape/inet/sftp/PathInfo;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    sget-object v0, Lcom/jscape/filetransfer/SftpFileTransfer;->z:[Ljava/lang/String;

    const/16 v1, 0x8

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/jscape/util/ak;->a(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->executorService:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/jscape/filetransfer/FileTransferConnectedEvent;

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->hostname:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/jscape/filetransfer/FileTransferConnectedEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/jscape/filetransfer/SftpFileTransfer;->disposeService()V

    invoke-virtual {p0}, Lcom/jscape/filetransfer/SftpFileTransfer;->disposeExecutorService()V

    invoke-direct {p0, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
.end method

.method public copy()Lcom/jscape/filetransfer/FileTransfer;
    .locals 31

    move-object/from16 v0, p0

    new-instance v29, Lcom/jscape/filetransfer/SftpFileTransfer;

    move-object/from16 v1, v29

    iget-object v2, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->proxyType:Ljava/lang/String;

    iget-object v3, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->proxyHost:Ljava/lang/String;

    iget v4, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->proxyPort:I

    iget-object v5, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->proxyUsername:Ljava/lang/String;

    iget-object v6, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->proxyPassword:Ljava/lang/String;

    iget-object v7, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->hostname:Ljava/lang/String;

    iget v8, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->port:I

    iget-wide v9, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->timeout:J

    invoke-static {v9, v10}, Lcom/jscape/util/Time;->millis(J)Lcom/jscape/util/Time;

    move-result-object v9

    iget-object v10, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->l:Ljava/lang/Integer;

    iget-object v11, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->m:Ljava/lang/Boolean;

    iget-object v12, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->n:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    iget-object v13, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->username:Ljava/lang/String;

    iget-object v14, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->password:Ljava/lang/String;

    iget-object v15, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->o:Ljava/security/KeyPair;

    move-object/from16 v30, v1

    iget-object v1, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->p:Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->q:Lcom/jscape/inet/sftp/SftpConfiguration;

    move-object/from16 v17, v1

    iget v1, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->r:I

    move/from16 v18, v1

    iget v1, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->transferBufferSizeBytes:I

    move/from16 v19, v1

    iget-boolean v1, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->s:Z

    move/from16 v20, v1

    iget v1, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->t:I

    move/from16 v21, v1

    iget-boolean v1, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->uploadedFileTimestampPreservingRequired:Z

    move/from16 v22, v1

    iget-boolean v1, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->downloadedFileTimestampPreservingRequired:Z

    move/from16 v23, v1

    iget-boolean v1, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->checksumVerificationRequired:Z

    move/from16 v24, v1

    iget-object v1, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->currentLocalDirectory:Ljava/nio/file/Path;

    invoke-interface {v1}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object v25

    iget-object v1, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    move-object/from16 v26, v1

    iget-object v1, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->logger:Ljava/util/logging/Logger;

    move-object/from16 v27, v1

    iget-object v1, v0, Lcom/jscape/filetransfer/SftpFileTransfer;->listeners:Ljava/util/Set;

    move-object/from16 v28, v1

    move-object/from16 v1, v30

    invoke-direct/range {v1 .. v28}, Lcom/jscape/filetransfer/SftpFileTransfer;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/jscape/util/Time;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;Lcom/jscape/inet/sftp/SftpConfiguration;IIZIZZZLjava/io/File;Ljava/lang/String;Ljava/util/logging/Logger;Ljava/util/Set;)V

    return-object v29
.end method

.method public declared-synchronized createSymbolicLink(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p2}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v0, p1, p2}, Lcom/jscape/inet/sftp/FileService;->createSymbolicLink(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    throw p1
.end method

.method public deleteDir(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v1, v0}, Lcom/jscape/inet/sftp/FileService;->removeDirectory(Ljava/lang/String;)V

    new-instance v0, Lcom/jscape/filetransfer/FileTransferDeleteDirEvent;

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/filetransfer/FileTransferDeleteDirEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public deleteDir(Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz p2, :cond_0

    :try_start_0
    new-instance p2, Lcom/jscape/filetransfer/RecursiveDeleteOperation;

    invoke-direct {p2, p1}, Lcom/jscape/filetransfer/RecursiveDeleteOperation;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p0}, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/RecursiveDeleteOperation;
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->deleteDir(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v0, p1}, Lcom/jscape/inet/sftp/FileService;->removeFile(Ljava/lang/String;)V

    new-instance v0, Lcom/jscape/filetransfer/FileTransferDeleteFileEvent;

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/filetransfer/FileTransferDeleteFileEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public disconnect()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/jscape/filetransfer/SftpFileTransfer;->disposeService()V

    invoke-virtual {p0}, Lcom/jscape/filetransfer/SftpFileTransfer;->disposeExecutorService()V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    new-instance v0, Lcom/jscape/filetransfer/FileTransferDisconnectedEvent;

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->hostname:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/jscape/filetransfer/FileTransferDisconnectedEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method protected disposeService()V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    invoke-interface {v1}, Lcom/jscape/inet/sftp/FileService;->j()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    :cond_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {v9, v10}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iget-object v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    const/4 v13, 0x1

    invoke-interface {v2, v15, v13}, Lcom/jscape/inet/sftp/FileService;->sizeOf(Ljava/lang/String;Z)J

    move-result-wide v7

    iget-object v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v2, v15, v13}, Lcom/jscape/inet/sftp/FileService;->modificationTimeOf(Ljava/lang/String;Z)J

    move-result-wide v11

    invoke-virtual/range {p0 .. p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->resolveLocalPathFor(Ljava/lang/String;)Ljava/io/File;

    move-result-object v14

    new-instance v5, Lcom/jscape/util/h/p;

    const-wide/16 v2, 0x0

    invoke-static {v14, v2, v3, v13}, Lcom/jscape/util/h/r;->a(Ljava/io/File;JZ)Lcom/jscape/util/h/r;

    move-result-object v2

    invoke-direct {v5, v2}, Lcom/jscape/util/h/p;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    iget-object v3, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const-wide/16 v16, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v19, v5

    move-wide/from16 v5, v16

    :try_start_2
    invoke-direct/range {v1 .. v8}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;

    move-result-object v8

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v20
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    :try_start_3
    iget-boolean v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->s:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v2, :cond_3

    :try_start_4
    iget-object v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    const-wide/16 v3, 0x0

    iget v5, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->transferMode:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_1

    if-ne v5, v13, :cond_0

    move v5, v13

    goto :goto_0

    :cond_0
    move/from16 v16, v1

    goto :goto_1

    :cond_1
    :goto_0
    move/from16 v16, v5

    :goto_1
    :try_start_5
    iget-object v5, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->executorService:Ljava/util/concurrent/ExecutorService;

    move-wide v6, v11

    move-object v11, v2

    move-object v12, v15

    move v2, v13

    move-object/from16 v22, v14

    move-wide v13, v3

    move-object v3, v15

    move-object/from16 v15, v19

    move-object/from16 v17, v8

    move-object/from16 v18, v5

    invoke-interface/range {v11 .. v18}, Lcom/jscape/inet/sftp/FileService;->readFile(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    move-wide v0, v6

    goto :goto_4

    :catch_0
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_1
    move-exception v0

    move-object v1, v0

    :try_start_7
    invoke-static {v1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catch_3
    move-exception v0

    :try_start_9
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :cond_3
    move-wide v6, v11

    move v2, v13

    move-object/from16 v22, v14

    move-object v3, v15

    :goto_2
    :try_start_a
    iget-object v4, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    const-wide/16 v11, 0x0

    iget v13, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->transferMode:I
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    if-eqz v0, :cond_5

    if-ne v13, v2, :cond_4

    move v13, v2

    goto :goto_3

    :cond_4
    move v13, v1

    :cond_5
    :goto_3
    move-object v2, v4

    move-wide v4, v11

    move-wide v0, v6

    move-object/from16 v6, v19

    move v7, v13

    :try_start_b
    invoke-interface/range {v2 .. v8}, Lcom/jscape/inet/sftp/FileService;->readFile(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/sftp/FileService$TransferListener;)V

    :goto_4
    move-object/from16 v8, v22

    invoke-virtual {v9, v8, v10}, Lcom/jscape/filetransfer/SftpFileTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v9, v8, v0, v1}, Lcom/jscape/filetransfer/SftpFileTransfer;->safeSetModificationTime(Ljava/io/File;J)V

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v19 .. v19}, Lcom/jscape/util/h/p;->a()J

    move-result-wide v4

    invoke-static/range {v20 .. v21}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v7}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    invoke-static/range {v19 .. v19}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    return-object v8

    :catch_4
    move-exception v0

    :try_start_c
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :catch_5
    move-exception v0

    :try_start_d
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_5

    :catch_6
    move-exception v0

    goto :goto_6

    :catchall_1
    move-exception v0

    move-object/from16 v19, v5

    :goto_5
    move-object/from16 v1, v19

    goto :goto_8

    :catch_7
    move-exception v0

    move-object/from16 v19, v5

    :goto_6
    move-object/from16 v2, p1

    move-object/from16 v1, v19

    goto :goto_7

    :catchall_2
    move-exception v0

    goto :goto_8

    :catch_8
    move-exception v0

    move-object/from16 v2, p1

    :goto_7
    :try_start_e
    invoke-direct {v9, v10, v2}, Lcom/jscape/filetransfer/SftpFileTransfer;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    :goto_8
    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    throw v0
.end method

.method public downloadDir(Ljava/lang/String;IZI)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :try_start_0
    new-instance p1, Lcom/jscape/filetransfer/DownloadDirectoryOperation;

    sget-object v1, Lcom/jscape/filetransfer/FileTransferOperation;->NULL:Lcom/jscape/filetransfer/FileTransferOperation;

    const/4 v0, 0x1

    add-int/2addr p2, v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    sget-object v4, Lcom/jscape/filetransfer/SftpFileTransfer;->g:Lcom/jscape/util/Time;

    const-string v5, "/"

    if-lez p4, :cond_0

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    move-object v6, p2

    move-object v0, p1

    invoke-direct/range {v0 .. v6}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;-><init>(Lcom/jscape/filetransfer/FileTransferOperation;Ljava/lang/String;ILcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/Integer;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->x:Lcom/jscape/filetransfer/DownloadDirectoryOperation;

    iget-boolean p1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->checksumVerificationRequired:Z

    invoke-virtual {p0, p3}, Lcom/jscape/filetransfer/SftpFileTransfer;->setChecksumVerificationRequired(Z)Lcom/jscape/filetransfer/FileTransfer;

    iget-object p2, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->x:Lcom/jscape/filetransfer/DownloadDirectoryOperation;

    invoke-virtual {p2, p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->setChecksumVerificationRequired(Z)Lcom/jscape/filetransfer/FileTransfer;

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public exists(Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v0, p1}, Lcom/jscape/inet/sftp/FileService;->exists(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getClientAuthentication()Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->p:Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    return-object v0
.end method

.method public getConfiguration()Lcom/jscape/inet/sftp/SftpConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->q:Lcom/jscape/inet/sftp/SftpConfiguration;

    return-object v0
.end method

.method public getDir()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    return-object v0
.end method

.method public getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Enumeration<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->getDirListing(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    return-object p1
.end method

.method public getDirListing(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Enumeration<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v1, p1}, Lcom/jscape/inet/sftp/FileService;->listDirectory(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/sftp/PathInfo$NaturalComparator;

    invoke-direct {v1}, Lcom/jscape/inet/sftp/PathInfo$NaturalComparator;-><init>()V

    invoke-static {p1, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/sftp/PathInfo;

    iget-object v3, v2, Lcom/jscape/inet/sftp/PathInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, v2}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Lcom/jscape/inet/sftp/PathInfo;)Lcom/jscape/filetransfer/FileTransferRemoteFile;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_1
    if-nez v0, :cond_0

    :cond_2
    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public declared-synchronized getFileAccessTimestamp(Ljava/lang/String;)Ljava/util/Date;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/jscape/inet/sftp/FileService;->accessTimeOf(Ljava/lang/String;Z)J

    move-result-wide v0

    new-instance p1, Ljava/util/Date;

    invoke-direct {p1, v0, v1}, Ljava/util/Date;-><init>(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getFileCreationTimestamp(Ljava/lang/String;)Ljava/util/Date;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/jscape/inet/sftp/FileService;->createTimeOf(Ljava/lang/String;Z)J

    move-result-wide v0

    new-instance p1, Ljava/util/Date;

    invoke-direct {p1, v0, v1}, Ljava/util/Date;-><init>(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    throw p1
.end method

.method public getFilePermissions(Ljava/lang/String;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2}, Lcom/jscape/inet/sftp/FileService;->permissionsOf(Ljava/lang/String;Z)Lcom/jscape/util/e/PosixFilePermissions;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance p1, Lcom/jscape/filetransfer/FileTransferException;

    sget-object v0, Lcom/jscape/filetransfer/SftpFileTransfer;->z:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Lcom/jscape/filetransfer/FileTransferException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-static {p1}, Lcom/jscape/util/e/a;->a(Lcom/jscape/util/e/PosixFilePermissions;)I

    move-result p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    return p1

    :catch_1
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    const/4 v2, 0x1

    invoke-interface {v1, p1, v2}, Lcom/jscape/inet/sftp/FileService;->modificationTimeOf(Ljava/lang/String;Z)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getFilesize(Ljava/lang/String;)J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/jscape/inet/sftp/FileService;->sizeOf(Ljava/lang/String;Z)J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getHostKeyVerifier()Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->n:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    return-object v0
.end method

.method public getImplementation()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    return-object v0
.end method

.method public getKeyPair()Ljava/security/KeyPair;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->o:Ljava/security/KeyPair;

    return-object v0
.end method

.method public getPipelineWindowSize()I
    .locals 1

    iget v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->t:I

    return v0
.end method

.method public getSocketTrafficClass()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->l:Ljava/lang/Integer;

    return-object v0
.end method

.method public getSymbolicLinkTargetPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/jscape/inet/sftp/FileService;->infoAbout(Ljava/lang/String;Z)Lcom/jscape/inet/sftp/PathInfo;

    move-result-object p1

    iget-object p1, p1, Lcom/jscape/inet/sftp/PathInfo;->name:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getTcpNoDelay()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->m:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getType(Ljava/lang/String;)Lcom/jscape/util/e/UnixFileType;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/jscape/inet/sftp/FileService;->typeOf(Ljava/lang/String;Z)Lcom/jscape/util/e/UnixFileType;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getVersion()I
    .locals 1

    iget v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->r:I

    return v0
.end method

.method protected initService()V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v1, p0

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/filetransfer/SftpFileTransfer;->authentication()Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/filetransfer/SftpFileTransfer;->rawConnector()Lcom/jscape/util/k/a/v;

    move-result-object v3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v18, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;

    iget-object v2, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->q:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-object v2, v2, Lcom/jscape/inet/sftp/SftpConfiguration;->sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

    iget-object v4, v2, Lcom/jscape/inet/ssh/SshConfiguration;->identificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    iget-object v2, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->q:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-object v2, v2, Lcom/jscape/inet/sftp/SftpConfiguration;->sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

    iget-object v5, v2, Lcom/jscape/inet/ssh/SshConfiguration;->keyExchangeFactory:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;

    iget-object v2, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->q:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-object v2, v2, Lcom/jscape/inet/sftp/SftpConfiguration;->sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

    iget-object v6, v2, Lcom/jscape/inet/ssh/SshConfiguration;->encryptionFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;

    iget-object v2, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->q:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-object v2, v2, Lcom/jscape/inet/sftp/SftpConfiguration;->sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

    iget-object v7, v2, Lcom/jscape/inet/ssh/SshConfiguration;->macFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;

    iget-object v2, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->q:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-object v2, v2, Lcom/jscape/inet/sftp/SftpConfiguration;->sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

    iget-object v8, v2, Lcom/jscape/inet/ssh/SshConfiguration;->compressionFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;

    iget-object v9, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->n:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    iget-object v2, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->q:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-object v2, v2, Lcom/jscape/inet/sftp/SftpConfiguration;->sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

    iget v11, v2, Lcom/jscape/inet/ssh/SshConfiguration;->initialWindowSize:I

    iget-object v2, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->q:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-object v2, v2, Lcom/jscape/inet/sftp/SftpConfiguration;->sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

    iget v12, v2, Lcom/jscape/inet/ssh/SshConfiguration;->maxPacketSize:I

    iget-object v13, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->logger:Ljava/util/logging/Logger;

    move-object/from16 v2, v18

    invoke-direct/range {v2 .. v13}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;-><init>(Lcom/jscape/util/k/a/v;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;IILjava/util/logging/Logger;)V

    if-eqz v0, :cond_0

    :try_start_0
    iget v2, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->r:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    sget-object v2, Lcom/jscape/filetransfer/SftpFileTransfer;->z:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v3, v2, v3

    const/4 v4, 0x7

    aget-object v2, v2, v4

    invoke-static {v3, v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v15

    :try_start_1
    new-instance v2, Lcom/jscape/inet/sftp/SftpFileService4;

    iget v13, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->transferBufferSizeBytes:I

    iget-object v3, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->q:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-boolean v14, v3, Lcom/jscape/inet/sftp/SftpConfiguration;->subsystemResponseRequired:Z

    iget v3, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->t:I

    iget-object v4, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->logger:Ljava/util/logging/Logger;

    move-object v11, v2

    move-object/from16 v12, v18

    move/from16 v16, v3

    move-object/from16 v17, v4

    invoke-direct/range {v11 .. v17}, Lcom/jscape/inet/sftp/SftpFileService4;-><init>(Lcom/jscape/util/k/a/v;IZ[BILjava/util/logging/Logger;)V

    iput-object v2, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_1
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    new-instance v0, Lcom/jscape/inet/sftp/SftpFileService3;

    iget v6, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->transferBufferSizeBytes:I

    iget-object v2, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->q:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-boolean v7, v2, Lcom/jscape/inet/sftp/SftpConfiguration;->subsystemResponseRequired:Z

    iget v8, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->t:I

    iget-object v9, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->logger:Ljava/util/logging/Logger;

    move-object v4, v0

    move-object/from16 v5, v18

    invoke-direct/range {v4 .. v9}, Lcom/jscape/inet/sftp/SftpFileService3;-><init>(Lcom/jscape/util/k/a/v;IZILjava/util/logging/Logger;)V

    iput-object v0, v1, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    :cond_1
    return-void
.end method

.method public interrupt()V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-super {p0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->interrupt()V

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    invoke-interface {v1}, Lcom/jscape/inet/sftp/FileService;->cancelTransferOperation()V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public isConnected()Z
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_0

    if-eqz v1, :cond_2

    :try_start_1
    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    :try_start_2
    invoke-interface {v1}, Lcom/jscape/inet/sftp/FileService;->h()Z

    move-result v1
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_2

    :try_start_3
    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v1}, Lcom/jscape/inet/sftp/FileService;->available()Z

    move-result v1

    :cond_1
    if-eqz v0, :cond_3

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    move v0, v1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public isDirectory(Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->getType(Ljava/lang/String;)Lcom/jscape/util/e/UnixFileType;

    move-result-object p1

    :try_start_0
    sget-object v0, Lcom/jscape/util/e/UnixFileType;->DIRECTORY:Lcom/jscape/util/e/UnixFileType;
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public isPipelinedTransferEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->s:Z

    return v0
.end method

.method public makeDirRecursive(Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    const-string v1, ""

    array-length v2, p1

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v2, :cond_3

    aget-object v4, p1, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_3

    if-eqz v0, :cond_2

    :try_start_1
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v5, :cond_1

    :try_start_2
    invoke-direct {p0, v1, v4}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v0, :cond_2

    :try_start_3
    invoke-virtual {p0, v1}, Lcom/jscape/filetransfer/SftpFileTransfer;->exists(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->ownerDirectoryPermissions()Lcom/jscape/util/e/PosixFilePermissions;

    move-result-object v5

    invoke-interface {v4, v1, v5}, Lcom/jscape/inet/sftp/FileService;->createDirectory(Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;)V

    new-instance v4, Lcom/jscape/filetransfer/FileTransferCreateDirEvent;

    iget-object v5, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    invoke-direct {v4, p0, v1, v5}, Lcom/jscape/filetransfer/FileTransferCreateDirEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/jscape/filetransfer/SftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :cond_2
    :goto_1
    if-nez v0, :cond_0

    :cond_3
    return-void

    :catch_2
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method protected rawConnector()Lcom/jscape/util/k/a/v;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jscape/util/k/a/v<",
            "Lcom/jscape/util/k/a/C;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/k/g;

    new-instance v1, Lcom/jscape/util/k/TransportAddress;

    iget-object v2, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->hostname:Ljava/lang/String;

    iget v3, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->port:I

    invoke-direct {v1, v2, v3}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/lang/String;I)V

    iget-wide v2, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->timeout:J

    invoke-direct {v0, v1, v2, v3}, Lcom/jscape/util/k/g;-><init>(Lcom/jscape/util/k/TransportAddress;J)V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v11, Lcom/jscape/util/k/b/j;

    iget-object v6, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->l:Ljava/lang/Integer;

    iget-object v7, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->m:Ljava/lang/Boolean;

    iget-wide v2, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->timeout:J

    invoke-static {v2, v3}, Lcom/jscape/util/Time;->millis(J)Lcom/jscape/util/Time;

    move-result-object v10

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/jscape/util/k/b/j;-><init>(ZLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/jscape/util/Time;Lcom/jscape/util/Time;)V

    :try_start_0
    iget-object v2, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->proxyType:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_1

    :try_start_1
    iget-object v2, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->proxyHost:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    if-eqz v2, :cond_1

    new-instance v1, Lcom/jscape/inet/c/f;

    iget-object v2, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->proxyHost:Ljava/lang/String;

    iget v3, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->proxyPort:I

    iget-object v4, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->proxyUsername:Ljava/lang/String;

    iget-object v5, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->proxyPassword:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/jscape/inet/c/f;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->proxyType:Ljava/lang/String;

    invoke-static {v2}, Lcom/jscape/inet/c/d;->a(Ljava/lang/String;)Lcom/jscape/inet/c/e;

    move-result-object v2

    new-instance v3, Lcom/jscape/inet/c/g;

    invoke-direct {v3, v1, v2, v0, v11}, Lcom/jscape/inet/c/g;-><init>(Lcom/jscape/inet/c/f;Lcom/jscape/inet/c/e;Lcom/jscape/util/k/g;Lcom/jscape/util/k/b/j;)V

    return-object v3

    :cond_1
    new-instance v1, Lcom/jscape/util/k/b/i;

    invoke-direct {v1, v0, v11}, Lcom/jscape/util/k/b/i;-><init>(Lcom/jscape/util/k/g;Lcom/jscape/util/k/b/j;)V

    return-object v1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public renameFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v2, v0, v1}, Lcom/jscape/inet/sftp/FileService;->rename(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/jscape/filetransfer/FileTransferRenameFileEvent;

    iget-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/jscape/filetransfer/FileTransferRenameFileEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public resumeDownload(Ljava/io/OutputStream;Ljava/lang/String;J)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-direct {v9, v10}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/jscape/filetransfer/SftpFileTransfer;->getFilesize(Ljava/lang/String;)J

    move-result-wide v1

    sub-long v7, v1, p3

    new-instance v12, Lcom/jscape/util/h/p;

    move-object/from16 v1, p1

    invoke-direct {v12, v1}, Lcom/jscape/util/h/p;-><init>(Ljava/io/OutputStream;)V

    iget-object v3, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    const-string v4, ""

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-wide/from16 v5, p3

    invoke-direct/range {v1 .. v8}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;

    move-result-object v13

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v14
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6

    const/16 v16, 0x0

    const/4 v8, 0x1

    if-eqz v0, :cond_2

    :try_start_1
    iget-boolean v1, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->s:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v1, :cond_2

    :try_start_2
    iget-object v1, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    iget v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->transferMode:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v0, :cond_1

    if-ne v2, v8, :cond_0

    move v2, v8

    goto :goto_0

    :cond_0
    move/from16 v6, v16

    goto :goto_1

    :cond_1
    :goto_0
    move v6, v2

    :goto_1
    :try_start_3
    iget-object v7, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->executorService:Ljava/util/concurrent/ExecutorService;

    move-object v2, v11

    move-wide/from16 v3, p3

    move-object v5, v12

    move-object/from16 v17, v7

    move-object v7, v13

    move v10, v8

    move-object/from16 v8, v17

    invoke-interface/range {v1 .. v8}, Lcom/jscape/inet/sftp/FileService;->readFile(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    if-nez v0, :cond_5

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6

    :catch_1
    move-exception v0

    move-object v1, v0

    :try_start_5
    invoke-static {v1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    :catch_3
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    :cond_2
    move v10, v8

    :goto_2
    :try_start_8
    iget-object v1, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    iget v8, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->transferMode:I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    if-eqz v0, :cond_4

    if-ne v8, v10, :cond_3

    move v8, v10

    goto :goto_3

    :cond_3
    move/from16 v6, v16

    goto :goto_4

    :cond_4
    :goto_3
    move v6, v8

    :goto_4
    move-object v2, v11

    move-wide/from16 v3, p3

    move-object v5, v12

    move-object v7, v13

    :try_start_9
    invoke-interface/range {v1 .. v7}, Lcom/jscape/inet/sftp/FileService;->readFile(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/sftp/FileService$TransferListener;)V

    :cond_5
    const-string v3, ""

    invoke-virtual {v12}, Lcom/jscape/util/h/p;->a()J

    move-result-wide v4

    invoke-static {v14, v15}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v7}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6

    return-void

    :catch_4
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    :catch_5
    move-exception v0

    :try_start_b
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6

    :catch_6
    move-exception v0

    const-string v1, ""

    move-object/from16 v2, p2

    invoke-direct {v9, v2, v1}, Lcom/jscape/filetransfer/SftpFileTransfer;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
.end method

.method public resumeDownload(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {v9, v10}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    iget-object v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    const/4 v13, 0x1

    invoke-interface {v2, v15, v13}, Lcom/jscape/inet/sftp/FileService;->sizeOf(Ljava/lang/String;Z)J

    move-result-wide v7

    iget-object v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v2, v15, v13}, Lcom/jscape/inet/sftp/FileService;->modificationTimeOf(Ljava/lang/String;Z)J

    move-result-wide v11

    invoke-virtual/range {p0 .. p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->resolveLocalPathFor(Ljava/lang/String;)Ljava/io/File;

    move-result-object v14
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    new-instance v5, Lcom/jscape/util/h/p;

    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v14}, Ljava/io/File;->length()J

    move-result-wide v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    goto :goto_0

    :cond_0
    const-wide/16 v2, 0x0

    :goto_0
    :try_start_2
    invoke-static {v14, v2, v3, v13}, Lcom/jscape/util/h/r;->a(Ljava/io/File;JZ)Lcom/jscape/util/h/r;

    move-result-object v2

    invoke-direct {v5, v2}, Lcom/jscape/util/h/p;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_9
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    iget-object v3, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v19, v5

    move-wide/from16 v5, p3

    :try_start_4
    invoke-direct/range {v1 .. v8}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;

    move-result-object v8

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v20
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    :try_start_5
    iget-boolean v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->s:Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v2, :cond_4

    :try_start_6
    iget-object v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    iget v3, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->transferMode:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v0, :cond_2

    if-ne v3, v13, :cond_1

    move v3, v13

    goto :goto_1

    :cond_1
    move/from16 v16, v1

    goto :goto_2

    :cond_2
    :goto_1
    move/from16 v16, v3

    :goto_2
    :try_start_7
    iget-object v3, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->executorService:Ljava/util/concurrent/ExecutorService;

    move-wide v6, v11

    move-object v11, v2

    move-object v12, v15

    move v2, v13

    move-object v4, v14

    move-wide/from16 v13, p3

    move-object v5, v15

    move-object/from16 v15, v19

    move-object/from16 v17, v8

    move-object/from16 v18, v3

    invoke-interface/range {v11 .. v18}, Lcom/jscape/inet/sftp/FileService;->readFile(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V

    if-nez v0, :cond_3

    new-array v3, v2, [I

    invoke-static {v3}, Lcom/jscape/util/aq;->b([I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    :cond_3
    move-object v0, v4

    move-wide v11, v6

    goto :goto_5

    :catch_0
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catch_1
    move-exception v0

    move-object v1, v0

    :try_start_9
    invoke-static {v1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_2
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_3
    move-exception v0

    :try_start_b
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_4
    move-wide v6, v11

    move v2, v13

    move-object v4, v14

    move-object v5, v15

    :goto_3
    :try_start_c
    iget-object v3, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    iget v13, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->transferMode:I
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    if-eqz v0, :cond_6

    if-ne v13, v2, :cond_5

    move v13, v2

    goto :goto_4

    :cond_5
    move v13, v1

    :cond_6
    :goto_4
    move-object v2, v3

    move-object v3, v5

    move-object v0, v4

    move-wide/from16 v4, p3

    move-wide v11, v6

    move-object/from16 v6, v19

    move v7, v13

    :try_start_d
    invoke-interface/range {v2 .. v8}, Lcom/jscape/inet/sftp/FileService;->readFile(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/sftp/FileService$TransferListener;)V

    :goto_5
    invoke-virtual {v9, v0, v10}, Lcom/jscape/filetransfer/SftpFileTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v9, v0, v11, v12}, Lcom/jscape/filetransfer/SftpFileTransfer;->safeSetModificationTime(Ljava/io/File;J)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v19 .. v19}, Lcom/jscape/util/h/p;->a()J

    move-result-wide v4

    invoke-static/range {v20 .. v21}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v7}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    invoke-static/range {v19 .. v19}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    return-void

    :catch_4
    move-exception v0

    :try_start_e
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_5
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :catch_5
    move-exception v0

    :try_start_f
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_6
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_6

    :catch_6
    move-exception v0

    goto :goto_7

    :catchall_1
    move-exception v0

    move-object/from16 v19, v5

    :goto_6
    move-object/from16 v1, v19

    goto :goto_9

    :catch_7
    move-exception v0

    move-object/from16 v19, v5

    :goto_7
    move-object/from16 v2, p1

    move-object/from16 v1, v19

    goto :goto_8

    :catch_8
    move-exception v0

    :try_start_10
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_9
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    :catchall_2
    move-exception v0

    goto :goto_9

    :catch_9
    move-exception v0

    move-object/from16 v2, p1

    :goto_8
    :try_start_11
    invoke-direct {v9, v10, v2}, Lcom/jscape/filetransfer/SftpFileTransfer;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    :goto_9
    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    throw v0
.end method

.method public resumeUpload(Ljava/io/File;Ljava/lang/String;J)V
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    move-wide/from16 v11, p3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {v9, v10}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v2

    sub-long v7, v2, v11

    new-instance v14, Lcom/jscape/util/h/f;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-object/from16 v15, p1

    :try_start_1
    invoke-static {v15, v11, v12}, Lcom/jscape/util/h/i;->a(Ljava/io/File;J)Lcom/jscape/util/h/i;

    move-result-object v2

    invoke-direct {v14, v2}, Lcom/jscape/util/h/f;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v1, p0

    move-wide/from16 v5, p3

    invoke-direct/range {v1 .. v8}, Lcom/jscape/filetransfer/SftpFileTransfer;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;

    move-result-object v16

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v17
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/16 v19, 0x0

    const/4 v8, 0x1

    if-eqz v0, :cond_2

    :try_start_3
    iget-boolean v1, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->s:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v1, :cond_2

    :try_start_4
    iget-object v1, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    iget v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->transferMode:I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_1

    if-ne v2, v8, :cond_0

    move v2, v8

    goto :goto_0

    :cond_0
    move/from16 v6, v19

    goto :goto_1

    :cond_1
    :goto_0
    move v6, v2

    :goto_1
    :try_start_5
    iget-object v7, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->executorService:Ljava/util/concurrent/ExecutorService;

    move-object v2, v14

    move-object v3, v13

    move-wide/from16 v4, p3

    move-object/from16 v20, v7

    move-object/from16 v7, v16

    move v11, v8

    move-object/from16 v8, v20

    invoke-interface/range {v1 .. v8}, Lcom/jscape/inet/sftp/FileService;->writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-nez v0, :cond_5

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_1
    move-exception v0

    move-object v1, v0

    :try_start_7
    invoke-static {v1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catch_3
    move-exception v0

    :try_start_9
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :cond_2
    move v11, v8

    :goto_2
    :try_start_a
    iget-object v1, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    iget v8, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->transferMode:I
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    if-eqz v0, :cond_4

    if-ne v8, v11, :cond_3

    move v8, v11

    goto :goto_3

    :cond_3
    move/from16 v6, v19

    goto :goto_4

    :cond_4
    :goto_3
    move v6, v8

    :goto_4
    move-object v2, v14

    move-object v3, v13

    move-wide/from16 v4, p3

    move-object/from16 v7, v16

    :try_start_b
    invoke-interface/range {v1 .. v7}, Lcom/jscape/inet/sftp/FileService;->writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;)V

    :cond_5
    invoke-virtual/range {p0 .. p2}, Lcom/jscape/filetransfer/SftpFileTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p2}, Lcom/jscape/filetransfer/SftpFileTransfer;->safeSetModificationTime(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14}, Lcom/jscape/util/h/f;->a()J

    move-result-wide v4

    invoke-static/range {v17 .. v18}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/jscape/filetransfer/SftpFileTransfer;->b(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    invoke-static {v14}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    return-void

    :catch_4
    move-exception v0

    :try_start_c
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :catch_5
    move-exception v0

    :try_start_d
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catchall_0
    move-exception v0

    move-object v1, v14

    goto :goto_6

    :catch_6
    move-exception v0

    move-object v1, v14

    goto :goto_5

    :catch_7
    move-exception v0

    goto :goto_5

    :catchall_1
    move-exception v0

    goto :goto_6

    :catch_8
    move-exception v0

    move-object/from16 v15, p1

    :goto_5
    :try_start_e
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2, v10}, Lcom/jscape/filetransfer/SftpFileTransfer;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    :goto_6
    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    throw v0
.end method

.method public resumeUpload(Ljava/io/InputStream;JLjava/lang/String;J)V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p4

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-direct {v9, v10}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Lcom/jscape/util/h/f;

    move-object/from16 v13, p1

    invoke-direct {v12, v13}, Lcom/jscape/util/h/f;-><init>(Ljava/io/InputStream;)V

    iget-object v3, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    const-string v4, ""

    move-object/from16 v1, p0

    move-object/from16 v2, p4

    move-wide/from16 v5, p5

    move-wide/from16 v7, p2

    invoke-direct/range {v1 .. v8}, Lcom/jscape/filetransfer/SftpFileTransfer;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;

    move-result-object v14

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v15
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6

    const/16 v17, 0x0

    const/4 v8, 0x1

    if-eqz v0, :cond_2

    :try_start_1
    iget-boolean v1, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->s:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v1, :cond_2

    :try_start_2
    iget-object v1, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    iget v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->transferMode:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v0, :cond_1

    if-ne v2, v8, :cond_0

    move v2, v8

    goto :goto_0

    :cond_0
    move/from16 v6, v17

    goto :goto_1

    :cond_1
    :goto_0
    move v6, v2

    :goto_1
    :try_start_3
    iget-object v7, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->executorService:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v2, p1

    move-object v3, v11

    move-wide/from16 v4, p5

    move-object/from16 v18, v7

    move-object v7, v14

    move v13, v8

    move-object/from16 v8, v18

    invoke-interface/range {v1 .. v8}, Lcom/jscape/inet/sftp/FileService;->writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    if-nez v0, :cond_5

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6

    :catch_1
    move-exception v0

    move-object v1, v0

    :try_start_5
    invoke-static {v1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    :catch_3
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    :cond_2
    move v13, v8

    :goto_2
    :try_start_8
    iget-object v1, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    iget v8, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->transferMode:I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    if-eqz v0, :cond_4

    if-ne v8, v13, :cond_3

    move v8, v13

    goto :goto_3

    :cond_3
    move/from16 v6, v17

    goto :goto_4

    :cond_4
    :goto_3
    move v6, v8

    :goto_4
    move-object/from16 v2, p1

    move-object v3, v11

    move-wide/from16 v4, p5

    move-object v7, v14

    :try_start_9
    invoke-interface/range {v1 .. v7}, Lcom/jscape/inet/sftp/FileService;->writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;)V

    :cond_5
    const-string v2, ""

    invoke-virtual {v12}, Lcom/jscape/util/h/f;->a()J

    move-result-wide v4

    invoke-static/range {v15 .. v16}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v3, p4

    invoke-direct/range {v1 .. v7}, Lcom/jscape/filetransfer/SftpFileTransfer;->b(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6

    return-void

    :catch_4
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    :catch_5
    move-exception v0

    :try_start_b
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6

    :catch_6
    move-exception v0

    const-string v1, ""

    invoke-direct {v9, v1, v10}, Lcom/jscape/filetransfer/SftpFileTransfer;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
.end method

.method public sameChecksum(Ljava/io/File;Ljava/lang/String;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/jscape/a/h;->d()Lcom/jscape/a/h;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/jscape/a/e;->a(Ljava/io/File;Lcom/jscape/a/d;)[B

    move-result-object p1

    invoke-direct {p0, p2}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p2}, Lcom/jscape/filetransfer/SftpFileTransfer;->getFilesize(Ljava/lang/String;)J

    move-result-wide v4

    const/4 v6, 0x0

    const/4 p2, 0x1

    new-array v7, p2, [Ljava/lang/String;

    const/4 p2, 0x0

    sget-object v8, Lcom/jscape/filetransfer/SftpFileTransfer;->z:[Ljava/lang/String;

    const/16 v9, 0xa

    aget-object v8, v8, v9

    aput-object v8, v7, p2

    invoke-interface/range {v0 .. v7}, Lcom/jscape/inet/sftp/FileService;->hashOf(Ljava/lang/String;JJI[Ljava/lang/String;)Lcom/jscape/inet/sftp/HashInfo;

    move-result-object p2

    iget-object p2, p2, Lcom/jscape/inet/sftp/HashInfo;->hash:[B

    invoke-static {p1, p2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public setClientAuthentication(Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)Lcom/jscape/filetransfer/SftpFileTransfer;
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->p:Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    return-object p0
.end method

.method public setConfiguration(Lcom/jscape/inet/sftp/SftpConfiguration;)Lcom/jscape/filetransfer/SftpFileTransfer;
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->q:Lcom/jscape/inet/sftp/SftpConfiguration;

    return-object p0
.end method

.method public setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    const/4 v3, 0x1

    invoke-interface {v2, v1, v3}, Lcom/jscape/inet/sftp/FileService;->typeOf(Ljava/lang/String;Z)Lcom/jscape/util/e/UnixFileType;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    :try_start_1
    sget-object v0, Lcom/jscape/util/e/UnixFileType;->DIRECTORY:Lcom/jscape/util/e/UnixFileType;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-ne v2, v0, :cond_0

    :try_start_2
    iput-object v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    new-instance p1, Lcom/jscape/filetransfer/FileTransferChangeDirEvent;

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    invoke-direct {p1, p0, v0}, Lcom/jscape/filetransfer/FileTransferChangeDirEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :cond_0
    :try_start_3
    new-instance v0, Ljava/lang/Exception;

    sget-object v1, Lcom/jscape/filetransfer/SftpFileTransfer;->z:[Ljava/lang/String;

    aget-object v1, v1, v3

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :cond_1
    :goto_0
    return-object p0

    :catch_1
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public setDirUp()Lcom/jscape/filetransfer/FileTransfer;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/filetransfer/SftpFileTransfer;->z:[Ljava/lang/String;

    const/16 v1, 0xb

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;

    return-object p0
.end method

.method public setFileAccessTimestamp(Ljava/lang/String;Ljava/util/Date;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-interface {v0, p1, v1, v2}, Lcom/jscape/inet/sftp/FileService;->accessTime(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public setFileCreationTimestamp(Ljava/lang/String;Ljava/util/Date;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-interface {v0, p1, v1, v2}, Lcom/jscape/inet/sftp/FileService;->createTime(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public setFilePermissions(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p2}, Lcom/jscape/util/e/a;->a(I)Lcom/jscape/util/e/PosixFilePermissions;

    move-result-object p2

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v0, p1, p2}, Lcom/jscape/inet/sftp/FileService;->permissions(Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-interface {v0, p1, v1, v2}, Lcom/jscape/inet/sftp/FileService;->modificationTime(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public setHostKeyVerifier(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;)Lcom/jscape/filetransfer/SftpFileTransfer;
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->n:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    return-object p0
.end method

.method public setKeyPair(Ljava/io/File;Ljava/lang/String;)Lcom/jscape/filetransfer/SftpFileTransfer;
    .locals 2

    :try_start_0
    sget-object v0, Lcom/jscape/filetransfer/SftpFileTransfer;->h:Lcom/jscape/inet/ssh/util/KeyPairAssembler;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->restoreKeyPair(Ljava/io/File;Ljava/lang/String;)Ljava/security/KeyPair;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->o:Ljava/security/KeyPair;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/jscape/filetransfer/SftpFileTransfer;->z:[Ljava/lang/String;

    const/16 v1, 0x9

    aget-object v0, v0, v1

    invoke-direct {p2, v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public setKeyPair(Ljava/security/KeyPair;)Lcom/jscape/filetransfer/SftpFileTransfer;
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->o:Ljava/security/KeyPair;

    return-object p0
.end method

.method public setPipelineWindowSize(I)Lcom/jscape/filetransfer/SftpFileTransfer;
    .locals 5

    iget v0, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->t:I

    int-to-long v0, v0

    sget-object v2, Lcom/jscape/filetransfer/SftpFileTransfer;->z:[Ljava/lang/String;

    const/16 v3, 0xc

    aget-object v2, v2, v3

    const-wide/16 v3, 0x0

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->t:I

    return-object p0
.end method

.method public setPipelinedTransferEnabled(Z)Lcom/jscape/filetransfer/SftpFileTransfer;
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->s:Z

    return-object p0
.end method

.method public setSocketTrafficClass(Ljava/lang/Integer;)Lcom/jscape/filetransfer/SftpFileTransfer;
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->l:Ljava/lang/Integer;

    return-object p0
.end method

.method public setTcpNoDelay(Ljava/lang/Boolean;)Lcom/jscape/filetransfer/SftpFileTransfer;
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->m:Ljava/lang/Boolean;

    return-object p0
.end method

.method public setVersion(I)Lcom/jscape/filetransfer/SftpFileTransfer;
    .locals 4

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->r:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    const/4 v3, 0x3

    if-eq v1, v3, :cond_1

    :try_start_1
    iget v1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->r:I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    if-ne v1, v0, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    :goto_1
    sget-object v0, Lcom/jscape/filetransfer/SftpFileTransfer;->z:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-static {v1, v0}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    iput p1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->r:I

    return-object p0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public upload(Ljava/io/File;Ljava/lang/String;Z)V
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {v9, v10}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_16
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    const-wide/16 v2, 0x0

    const/4 v11, 0x3

    if-eqz v0, :cond_0

    if-eqz p3, :cond_3

    :try_start_1
    iget v4, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->r:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v2, v0

    :try_start_2
    invoke-static {v2}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_16
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    :cond_0
    move/from16 v4, p3

    :goto_0
    if-eqz v0, :cond_1

    if-ne v4, v11, :cond_3

    if-eqz v0, :cond_2

    :try_start_3
    invoke-virtual {v9, v10}, Lcom/jscape/filetransfer/SftpFileTransfer;->exists(Ljava/lang/String;)Z

    move-result v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v2, v0

    :try_start_4
    invoke-static {v2}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_1
    if-eqz v4, :cond_3

    :cond_2
    invoke-virtual {v9, v10}, Lcom/jscape/filetransfer/SftpFileTransfer;->getFilesize(Ljava/lang/String;)J

    move-result-wide v4

    move-wide/from16 v19, v4

    goto :goto_2

    :cond_3
    move-wide/from16 v19, v2

    :goto_2
    new-instance v15, Lcom/jscape/util/h/f;

    move-object/from16 v13, p1

    invoke-static {v13, v2, v3}, Lcom/jscape/util/h/i;->a(Ljava/io/File;J)Lcom/jscape/util/h/i;

    move-result-object v2

    invoke-direct {v15, v2}, Lcom/jscape/util/h/f;-><init>(Ljava/io/InputStream;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_16
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :try_start_5
    iget-object v3, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->u:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v8}, Lcom/jscape/filetransfer/SftpFileTransfer;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;

    move-result-object v8

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v21
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_15
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    const/4 v1, 0x0

    const/4 v12, 0x1

    if-eqz v0, :cond_c

    if-eqz p3, :cond_a

    :try_start_6
    iget v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->r:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v0, :cond_d

    if-le v2, v11, :cond_a

    if-eqz v0, :cond_6

    :try_start_7
    iget-boolean v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->s:Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v2, :cond_6

    :try_start_8
    iget-object v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    iget v3, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->transferMode:I
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_a
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v0, :cond_5

    if-ne v3, v12, :cond_4

    move v3, v12

    goto :goto_3

    :cond_4
    move v5, v1

    goto :goto_4

    :cond_5
    :goto_3
    move v5, v3

    :goto_4
    :try_start_9
    iget-object v7, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->executorService:Ljava/util/concurrent/ExecutorService;

    move-object v3, v15

    move-object v4, v14

    move-object v6, v8

    invoke-interface/range {v2 .. v7}, Lcom/jscape/inet/sftp/FileService;->appendFile(Ljava/io/InputStream;Ljava/lang/String;ZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-nez v0, :cond_9

    goto :goto_5

    :catch_2
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_d
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_6
    :goto_5
    :try_start_b
    iget-object v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    iget v3, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->transferMode:I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    if-eqz v0, :cond_8

    if-ne v3, v12, :cond_7

    move v3, v12

    goto :goto_6

    :cond_7
    move v3, v1

    :cond_8
    :goto_6
    :try_start_c
    invoke-interface {v2, v15, v14, v3, v8}, Lcom/jscape/inet/sftp/FileService;->appendFile(Ljava/io/InputStream;Ljava/lang/String;ZLcom/jscape/inet/sftp/FileService$TransferListener;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    if-nez v0, :cond_9

    goto :goto_7

    :cond_9
    move-object/from16 v23, v15

    goto/16 :goto_f

    :catch_3
    move-exception v0

    move-object v1, v0

    :try_start_d
    invoke-static {v1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_c
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catch_4
    move-exception v0

    :try_start_e
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_5
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :catch_5
    move-exception v0

    :try_start_f
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_d
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :catchall_0
    move-exception v0

    move-object v1, v15

    goto/16 :goto_13

    :catch_6
    move-exception v0

    move-object v1, v0

    :try_start_10
    invoke-static {v1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_7
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :catch_7
    move-exception v0

    :try_start_11
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_8
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :catch_8
    move-exception v0

    :try_start_12
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_9
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :catch_9
    move-exception v0

    :try_start_13
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_a
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :catch_a
    move-exception v0

    :try_start_14
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_b
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    :catch_b
    move-exception v0

    :try_start_15
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_d
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    :cond_a
    :goto_7
    if-eqz v0, :cond_b

    :try_start_16
    iget-boolean v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->s:Z
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_c
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    goto :goto_8

    :catch_c
    move-exception v0

    :try_start_17
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_d
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    :catch_d
    move-exception v0

    move-object v1, v15

    goto/16 :goto_12

    :cond_b
    move v3, v12

    move-object v4, v14

    move-object/from16 v23, v15

    goto :goto_c

    :cond_c
    move/from16 v2, p3

    :cond_d
    :goto_8
    if-eqz v2, :cond_b

    :try_start_18
    iget-object v11, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    iget v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->transferMode:I
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_10
    .catchall {:try_start_18 .. :try_end_18} :catchall_2

    if-eqz v0, :cond_f

    if-ne v2, v12, :cond_e

    move v2, v12

    goto :goto_9

    :cond_e
    move/from16 v16, v1

    goto :goto_a

    :cond_f
    :goto_9
    move/from16 v16, v2

    :goto_a
    :try_start_19
    iget-object v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->executorService:Ljava/util/concurrent/ExecutorService;
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_f
    .catchall {:try_start_19 .. :try_end_19} :catchall_2

    move v3, v12

    move-object v12, v15

    move-object v13, v14

    move-object v4, v14

    move-object/from16 v23, v15

    move-wide/from16 v14, v19

    move-object/from16 v17, v8

    move-object/from16 v18, v2

    :try_start_1a
    invoke-interface/range {v11 .. v18}, Lcom/jscape/inet/sftp/FileService;->writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_e
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1

    if-nez v0, :cond_12

    goto :goto_c

    :catch_e
    move-exception v0

    goto :goto_b

    :catch_f
    move-exception v0

    move-object/from16 v23, v15

    :goto_b
    :try_start_1b
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_12
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    :catch_10
    move-exception v0

    move-object/from16 v23, v15

    :try_start_1c
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_11
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    :catch_11
    move-exception v0

    :try_start_1d
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_12
    .catchall {:try_start_1d .. :try_end_1d} :catchall_1

    :catch_12
    move-exception v0

    goto :goto_11

    :goto_c
    :try_start_1e
    iget-object v2, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->v:Lcom/jscape/inet/sftp/FileService;

    iget v12, v9, Lcom/jscape/filetransfer/SftpFileTransfer;->transferMode:I
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_13
    .catchall {:try_start_1e .. :try_end_1e} :catchall_1

    if-eqz v0, :cond_11

    if-ne v12, v3, :cond_10

    move v12, v3

    goto :goto_d

    :cond_10
    move v7, v1

    goto :goto_e

    :cond_11
    :goto_d
    move v7, v12

    :goto_e
    move-object/from16 v3, v23

    move-wide/from16 v5, v19

    :try_start_1f
    invoke-interface/range {v2 .. v8}, Lcom/jscape/inet/sftp/FileService;->writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;)V

    :cond_12
    :goto_f
    invoke-virtual/range {p0 .. p2}, Lcom/jscape/filetransfer/SftpFileTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p2}, Lcom/jscape/filetransfer/SftpFileTransfer;->safeSetModificationTime(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {v23 .. v23}, Lcom/jscape/util/h/f;->a()J

    move-result-wide v4

    invoke-static/range {v21 .. v22}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/jscape/filetransfer/SftpFileTransfer;->b(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_12
    .catchall {:try_start_1f .. :try_end_1f} :catchall_1

    invoke-static/range {v23 .. v23}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    return-void

    :catchall_1
    move-exception v0

    goto :goto_10

    :catch_13
    move-exception v0

    :try_start_20
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_14
    .catchall {:try_start_20 .. :try_end_20} :catchall_1

    :catch_14
    move-exception v0

    :try_start_21
    invoke-static {v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_12
    .catchall {:try_start_21 .. :try_end_21} :catchall_1

    :catchall_2
    move-exception v0

    move-object/from16 v23, v15

    :goto_10
    move-object/from16 v1, v23

    goto :goto_13

    :catch_15
    move-exception v0

    move-object/from16 v23, v15

    :goto_11
    move-object/from16 v1, v23

    goto :goto_12

    :catchall_3
    move-exception v0

    goto :goto_13

    :catch_16
    move-exception v0

    :goto_12
    :try_start_22
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2, v10}, Lcom/jscape/filetransfer/SftpFileTransfer;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v0}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_3

    :goto_13
    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    throw v0
.end method

.method public uploadDir(Ljava/io/File;IZLjava/lang/String;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    new-instance v7, Lcom/jscape/filetransfer/UploadDirectoryOperation;

    sget-object v1, Lcom/jscape/filetransfer/FileTransferOperation;->NULL:Lcom/jscape/filetransfer/FileTransferOperation;

    const/4 v0, 0x1

    add-int/2addr p2, v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    sget-object v5, Lcom/jscape/filetransfer/SftpFileTransfer;->g:Lcom/jscape/util/Time;

    if-lez p5, :cond_0

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    move-object v6, p2

    move-object v0, v7

    move-object v2, p1

    move-object v3, p4

    invoke-direct/range {v0 .. v6}, Lcom/jscape/filetransfer/UploadDirectoryOperation;-><init>(Lcom/jscape/filetransfer/FileTransferOperation;Ljava/io/File;Ljava/lang/String;ILcom/jscape/util/Time;Ljava/lang/Integer;)V

    iput-object v7, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->w:Lcom/jscape/filetransfer/UploadDirectoryOperation;

    iget-boolean p1, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->checksumVerificationRequired:Z

    invoke-virtual {p0, p3}, Lcom/jscape/filetransfer/SftpFileTransfer;->setChecksumVerificationRequired(Z)Lcom/jscape/filetransfer/FileTransfer;

    iget-object p2, p0, Lcom/jscape/filetransfer/SftpFileTransfer;->w:Lcom/jscape/filetransfer/UploadDirectoryOperation;

    invoke-virtual {p2, p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/UploadDirectoryOperation;

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->setChecksumVerificationRequired(Z)Lcom/jscape/filetransfer/FileTransfer;

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public uploadUnique(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;

    invoke-direct {v0, p2}, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FindUniqueFilenameOperation;

    move-result-object p2

    invoke-virtual {p2}, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;->filename()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v0

    int-to-long v2, v0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/jscape/filetransfer/SftpFileTransfer;->upload(Ljava/io/InputStream;JLjava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p2

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/SftpFileTransfer;->a(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method
