.class public Lcom/jscape/filetransfer/AftpFileTransferParameters;
.super Lcom/jscape/filetransfer/FileTransferParameters;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field protected clientKeyFile:Ljava/io/File;

.field protected clientKeyFilePassword:Ljava/lang/String;

.field protected compressionExcludedFileExtensions:[Ljava/lang/String;

.field protected compressionMinFileSize:Ljava/lang/Long;

.field protected downloadRate:Lcom/jscape/util/A;

.field protected securityMode:Lcom/jscape/filetransfer/AftpSecurityMode;

.field protected timeZone:Ljava/util/TimeZone;

.field protected uploadRate:Lcom/jscape/util/A;

.field protected useCompression:Ljava/lang/Boolean;

.field protected useCongestionControl:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "[\u0010\u0000\u0002\u0010M$\u0003{\u0006\u0017?A&\u0012`\u0002\u001d\n_%\u0005T^I\r[\u0010\u0016\u001e\u0015G+\u0013b\u0002\u001a\u001c\u0015\u0017[\u0010\u0016\u001d\u001ck%\u0019W\u0006\u001d\rA%\u0019s\u000c\u0000\rZ%\u001b\r\u0019[\u0010\u0000\u0001\u0014X8\u0012C\u0010\u0007\u0016F\u0007\u001e^%\u0007\u0015M\u0019\u001eJ\u0006S)6V\u0017\u001e?A&\u0012d\u0011\u000f\u0017[,\u0012B3\u000f\u000bI\'\u0012D\u0006\u001c\n\u00081\u0004U\u0000\u001b\u000bA>\u000e}\u000c\n\u001c\u0015\u000b[\u0010\u0017\u0007\u0014M\u0010\u0018^\u0006S$[\u0010\u0000\u0001\u0014X8\u0012C\u0010\u0007\u0016F\u000f\u000fS\u000f\u001b\u001dM.1Y\u000f\u000b<P>\u0012^\u0010\u0007\u0016F9J\u0011[\u0010\u0016\u001d\u001ck%\u001a@\u0011\u000b\n[#\u0018^^"

    const/16 v4, 0xc6

    const/16 v5, 0x19

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x12

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x20

    const/16 v3, 0x10

    const-string v5, "-fvtf;Ru\rpaI7Pd{\u000f-fqwx0Pn\'qJn*Y<"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x64

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x58

    goto :goto_4

    :cond_4
    const/16 v1, 0x3a

    goto :goto_4

    :cond_5
    const/16 v1, 0x6b

    goto :goto_4

    :cond_6
    const/16 v1, 0x7c

    goto :goto_4

    :cond_7
    const/16 v1, 0x71

    goto :goto_4

    :cond_8
    const/16 v1, 0x22

    goto :goto_4

    :cond_9
    const/16 v1, 0x65

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Lcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/io/PrintStream;Ljava/io/File;Lcom/jscape/filetransfer/AftpSecurityMode;Ljava/io/File;Ljava/lang/String;Lcom/jscape/util/A;Lcom/jscape/util/A;Ljava/lang/Boolean;Ljava/lang/Boolean;[Ljava/lang/String;Ljava/lang/Long;Ljava/util/TimeZone;)V
    .locals 13

    move-object v12, p0

    sget-object v1, Lcom/jscape/filetransfer/Protocol;->AFTP:Lcom/jscape/filetransfer/Protocol;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/jscape/filetransfer/FileTransferParameters;-><init>(Lcom/jscape/filetransfer/Protocol;Ljava/lang/String;Ljava/lang/Integer;Lcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/io/PrintStream;Ljava/io/File;)V

    move-object/from16 v0, p11

    iput-object v0, v12, Lcom/jscape/filetransfer/AftpFileTransferParameters;->securityMode:Lcom/jscape/filetransfer/AftpSecurityMode;

    move-object/from16 v0, p12

    iput-object v0, v12, Lcom/jscape/filetransfer/AftpFileTransferParameters;->clientKeyFile:Ljava/io/File;

    move-object/from16 v0, p13

    iput-object v0, v12, Lcom/jscape/filetransfer/AftpFileTransferParameters;->clientKeyFilePassword:Ljava/lang/String;

    move-object/from16 v0, p14

    iput-object v0, v12, Lcom/jscape/filetransfer/AftpFileTransferParameters;->uploadRate:Lcom/jscape/util/A;

    move-object/from16 v0, p15

    iput-object v0, v12, Lcom/jscape/filetransfer/AftpFileTransferParameters;->downloadRate:Lcom/jscape/util/A;

    move-object/from16 v0, p16

    iput-object v0, v12, Lcom/jscape/filetransfer/AftpFileTransferParameters;->useCongestionControl:Ljava/lang/Boolean;

    move-object/from16 v0, p17

    iput-object v0, v12, Lcom/jscape/filetransfer/AftpFileTransferParameters;->useCompression:Ljava/lang/Boolean;

    move-object/from16 v0, p18

    iput-object v0, v12, Lcom/jscape/filetransfer/AftpFileTransferParameters;->compressionExcludedFileExtensions:[Ljava/lang/String;

    move-object/from16 v0, p19

    iput-object v0, v12, Lcom/jscape/filetransfer/AftpFileTransferParameters;->compressionMinFileSize:Ljava/lang/Long;

    move-object/from16 v0, p20

    iput-object v0, v12, Lcom/jscape/filetransfer/AftpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public applyClientKeyFile(Lcom/jscape/filetransfer/AftpFileTransfer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->clientKeyFile:Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->clientKeyFile:Ljava/io/File;

    :cond_1
    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->clientKeyFilePassword:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/jscape/util/i/d;->a(Ljava/io/File;Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->clientKeyFilePassword:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/jscape/util/i/d;->a(Ljava/security/KeyStore;Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->setSslKeyStore(Ljava/security/KeyStore;)Lcom/jscape/filetransfer/AftpFileTransfer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public applySpecificTo(Lcom/jscape/filetransfer/AftpFileTransfer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->securityMode:Lcom/jscape/filetransfer/AftpSecurityMode;
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->securityMode:Lcom/jscape/filetransfer/AftpSecurityMode;

    invoke-virtual {v1, p1}, Lcom/jscape/filetransfer/AftpSecurityMode;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->applyClientKeyFile(Lcom/jscape/filetransfer/AftpFileTransfer;)V

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    :try_start_3
    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->uploadRate:Lcom/jscape/util/A;
    :try_end_3
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_3 .. :try_end_3} :catch_d

    if-eqz v0, :cond_3

    if-eqz v1, :cond_2

    :try_start_4
    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->uploadRate:Lcom/jscape/util/A;

    invoke-virtual {v1}, Lcom/jscape/util/A;->a()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/jscape/filetransfer/AftpFileTransfer;->setUploadRate(J)Lcom/jscape/filetransfer/AftpFileTransfer;
    :try_end_4
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_4 .. :try_end_4} :catch_e

    :cond_2
    if-eqz v0, :cond_4

    :try_start_5
    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->downloadRate:Lcom/jscape/util/A;
    :try_end_5
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    if-eqz v1, :cond_4

    :try_start_6
    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->downloadRate:Lcom/jscape/util/A;

    invoke-virtual {v1}, Lcom/jscape/util/A;->a()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/jscape/filetransfer/AftpFileTransfer;->setDownloadRate(J)Lcom/jscape/filetransfer/AftpFileTransfer;
    :try_end_6
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_2

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_4
    :goto_2
    :try_start_7
    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->useCongestionControl:Ljava/lang/Boolean;
    :try_end_7
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_7 .. :try_end_7} :catch_b

    if-eqz v0, :cond_6

    if-eqz v1, :cond_5

    :try_start_8
    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->useCongestionControl:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/jscape/filetransfer/AftpFileTransfer;->setUseCongestionControl(Z)Lcom/jscape/filetransfer/AftpFileTransfer;
    :try_end_8
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_8 .. :try_end_8} :catch_c

    :cond_5
    if-eqz v0, :cond_a

    :try_start_9
    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->useCongestionControl:Ljava/lang/Boolean;
    :try_end_9
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_9 .. :try_end_9} :catch_4

    goto :goto_3

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_6
    :goto_3
    if-eqz v1, :cond_a

    :try_start_a
    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->useCongestionControl:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1
    :try_end_a
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_a .. :try_end_a} :catch_6

    if-nez v1, :cond_7

    :try_start_b
    invoke-virtual {p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->suppressCompressionDecision()V
    :try_end_b
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_b .. :try_end_b} :catch_7

    if-nez v0, :cond_a

    :cond_7
    :try_start_c
    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->compressionMinFileSize:Ljava/lang/Long;

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->compressionExcludedFileExtensions:[Ljava/lang/String;
    :try_end_c
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_c .. :try_end_c} :catch_5

    if-eqz v0, :cond_9

    if-eqz v2, :cond_8

    :try_start_d
    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->compressionExcludedFileExtensions:[Ljava/lang/String;
    :try_end_d
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_d .. :try_end_d} :catch_9

    goto :goto_4

    :cond_8
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_5

    :cond_9
    :goto_4
    move-object v0, v2

    :goto_5
    invoke-virtual {p1, v1, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->setCompressionDecision(Ljava/lang/Long;[Ljava/lang/String;)Lcom/jscape/filetransfer/AftpFileTransfer;

    goto :goto_7

    :catch_5
    move-exception p1

    goto :goto_6

    :catch_6
    move-exception p1

    :try_start_e
    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_e
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_e .. :try_end_e} :catch_7

    :catch_7
    move-exception p1

    :try_start_f
    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_f
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_f .. :try_end_f} :catch_5

    :goto_6
    :try_start_10
    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_10
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_10 .. :try_end_10} :catch_8

    :catch_8
    move-exception p1

    :try_start_11
    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_11
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_11 .. :try_end_11} :catch_9

    :catch_9
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_a
    :goto_7
    :try_start_12
    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {p1, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->setTimeZone(Ljava/util/TimeZone;)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_12
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_12 .. :try_end_12} :catch_a

    :cond_b
    return-void

    :catch_a
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :catch_b
    move-exception p1

    :try_start_13
    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_13
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_13 .. :try_end_13} :catch_c

    :catch_c
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :catch_d
    move-exception p1

    :try_start_14
    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_14
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_14 .. :try_end_14} :catch_e

    :catch_e
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-super {p0, p1}, Lcom/jscape/filetransfer/FileTransferParameters;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    instance-of v0, p1, Lcom/jscape/filetransfer/AftpFileTransfer;
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    check-cast p1, Lcom/jscape/filetransfer/AftpFileTransfer;

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->applySpecificTo(Lcom/jscape/filetransfer/AftpFileTransfer;)V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    return-object p0
.end method

.method public getClientKeyFile()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->clientKeyFile:Ljava/io/File;

    return-object v0
.end method

.method public getClientKeyFilePassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->clientKeyFilePassword:Ljava/lang/String;

    return-object v0
.end method

.method public getCompressionExcludedFileExtensions()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->compressionExcludedFileExtensions:[Ljava/lang/String;

    return-object v0
.end method

.method public getCompressionMinFileSize()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->compressionMinFileSize:Ljava/lang/Long;

    return-object v0
.end method

.method public getDownloadRate()Lcom/jscape/util/A;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->downloadRate:Lcom/jscape/util/A;

    return-object v0
.end method

.method public getSecurityMode()Lcom/jscape/filetransfer/AftpSecurityMode;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->securityMode:Lcom/jscape/filetransfer/AftpSecurityMode;

    return-object v0
.end method

.method public getTimeZone()Ljava/util/TimeZone;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    return-object v0
.end method

.method public getUploadRate()Lcom/jscape/util/A;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->uploadRate:Lcom/jscape/util/A;

    return-object v0
.end method

.method public getUseCompression()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->useCompression:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getUseCongestionControl()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->useCongestionControl:Ljava/lang/Boolean;

    return-object v0
.end method

.method public setClientKeyFile(Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->clientKeyFile:Ljava/io/File;

    return-void
.end method

.method public setClientKeyFilePassword(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->clientKeyFilePassword:Ljava/lang/String;

    return-void
.end method

.method public setCompressionExcludedFileExtensions([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->compressionExcludedFileExtensions:[Ljava/lang/String;

    return-void
.end method

.method public setCompressionMinFileSize(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->compressionMinFileSize:Ljava/lang/Long;

    return-void
.end method

.method public setDownloadRate(Lcom/jscape/util/A;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->downloadRate:Lcom/jscape/util/A;

    return-void
.end method

.method public setSecurityMode(Lcom/jscape/filetransfer/AftpSecurityMode;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->securityMode:Lcom/jscape/filetransfer/AftpSecurityMode;

    return-void
.end method

.method public setTimeZone(Ljava/util/TimeZone;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    return-void
.end method

.method public setUploadRate(Lcom/jscape/util/A;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->uploadRate:Lcom/jscape/util/A;

    return-void
.end method

.method public setUseCompression(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->useCompression:Ljava/lang/Boolean;

    return-void
.end method

.method public setUseCongestionControl(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->useCongestionControl:Ljava/lang/Boolean;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/AftpFileTransferParameters;->a:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->securityMode:Lcom/jscape/filetransfer/AftpSecurityMode;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->clientKeyFile:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->clientKeyFilePassword:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->uploadRate:Lcom/jscape/util/A;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->downloadRate:Lcom/jscape/util/A;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->useCongestionControl:Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->useCompression:Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->compressionExcludedFileExtensions:[Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->compressionMinFileSize:Ljava/lang/Long;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
