.class public Lcom/jscape/filetransfer/FindUniqueFilenameOperation;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransferOperation;


# static fields
.field private static final a:I = 0x64

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x4

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x4c

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "\u000e}`\u0006\u001en|7\u001a^JQB`!\u001cB\r\u0017^`,\u0004Y\u000f\u0017Mg)\u0010B\u000bZN "

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x23

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;->d:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    if-eqz v13, :cond_7

    if-eq v13, v8, :cond_6

    if-eq v13, v0, :cond_5

    const/4 v14, 0x3

    if-eq v13, v14, :cond_4

    if-eq v13, v2, :cond_3

    const/4 v14, 0x5

    if-eq v13, v14, :cond_2

    const/16 v13, 0x7b

    goto :goto_2

    :cond_2
    const/16 v13, 0x26

    goto :goto_2

    :cond_3
    const/16 v13, 0x60

    goto :goto_2

    :cond_4
    const/16 v13, 0x39

    goto :goto_2

    :cond_5
    const/16 v13, 0x9

    goto :goto_2

    :cond_6
    const/16 v13, 0x42

    goto :goto_2

    :cond_7
    const/16 v13, 0x67

    :goto_2
    xor-int/2addr v13, v7

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;->b:Ljava/lang/String;

    return-void
.end method

.method private static a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/a/d;)Ljava/lang/String;
    .locals 2

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;->a([BLcom/jscape/a/d;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a([BLcom/jscape/a/d;)Ljava/lang/String;
    .locals 0

    invoke-interface {p2}, Lcom/jscape/a/d;->b()Lcom/jscape/a/d;

    invoke-interface {p2, p1}, Lcom/jscape/a/d;->b([B)[B

    move-result-object p1

    const-string p2, ""

    invoke-static {p1, p2}, Lcom/jscape/util/W;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Lcom/jscape/filetransfer/FileTransfer;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/filetransfer/FileTransfer;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-interface {p1}, Lcom/jscape/filetransfer/FileTransfer;->getNameListing()Ljava/util/Enumeration;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public bridge synthetic applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FindUniqueFilenameOperation;

    move-result-object p1

    return-object p1
.end method

.method public applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FindUniqueFilenameOperation;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;->c:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;->a(Lcom/jscape/filetransfer/FileTransfer;)Ljava/util/List;

    move-result-object p1

    invoke-static {}, Lcom/jscape/a/h;->d()Lcom/jscape/a/h;

    move-result-object v0

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move-object v3, p0

    move v4, v2

    :cond_0
    invoke-direct {v3, v0}, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;->a(Lcom/jscape/a/d;)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;->d:[Ljava/lang/String;

    aget-object v6, v6, v2

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, v3, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;->b:Ljava/lang/String;

    aput-object v8, v7, v2

    const/4 v8, 0x1

    aput-object v5, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    const/16 v7, 0x64

    if-eqz v6, :cond_1

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    if-eqz v1, :cond_3

    if-eq v4, v7, :cond_2

    iput-object v5, v3, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;->c:Ljava/lang/String;

    return-object v3

    :cond_2
    :try_start_0
    new-instance p1, Lcom/jscape/filetransfer/FileTransferException;

    sget-object v0, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;->d:[Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-direct {p1, v0}, Lcom/jscape/filetransfer/FileTransferException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    if-lt v4, v7, :cond_0

    goto :goto_0
.end method

.method public filename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FindUniqueFilenameOperation;->c:Ljava/lang/String;

    return-object v0
.end method
