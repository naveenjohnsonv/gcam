.class public Lcom/jscape/filetransfer/FileTransferPipedInputStream;
.super Ljava/io/PipedInputStream;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/jscape/filetransfer/FileTransfer;

.field private final b:Ljava/lang/String;

.field private final c:J

.field private final d:Ljava/io/PipedOutputStream;

.field private volatile e:Ljava/lang/Exception;


# direct methods
.method private constructor <init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;JLjava/io/PipedOutputStream;)V
    .locals 0

    invoke-direct {p0}, Ljava/io/PipedInputStream;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->a:Lcom/jscape/filetransfer/FileTransfer;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->b:Ljava/lang/String;

    iput-wide p3, p0, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->c:J

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->d:Ljava/io/PipedOutputStream;

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->e:Ljava/lang/Exception;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->e:Ljava/lang/Exception;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method public static streamFor(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;J)Lcom/jscape/filetransfer/FileTransferPipedInputStream;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v6, Ljava/io/PipedOutputStream;

    invoke-direct {v6}, Ljava/io/PipedOutputStream;-><init>()V

    new-instance v7, Lcom/jscape/filetransfer/FileTransferPipedInputStream;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move-object v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/jscape/filetransfer/FileTransferPipedInputStream;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;JLjava/io/PipedOutputStream;)V

    invoke-virtual {v6, v7}, Ljava/io/PipedOutputStream;->connect(Ljava/io/PipedInputStream;)V

    return-object v7
.end method


# virtual methods
.method public declared-synchronized read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->a()V

    invoke-super {p0}, Ljava/io/PipedInputStream;->read()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized read([BII)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->a()V

    invoke-super {p0, p1, p2, p3}, Ljava/io/PipedInputStream;->read([BII)I

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public run()V
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->a:Lcom/jscape/filetransfer/FileTransfer;

    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->d:Ljava/io/PipedOutputStream;

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->b:Ljava/lang/String;

    iget-wide v3, p0, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->c:J

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/jscape/filetransfer/FileTransfer;->resumeDownload(Ljava/io/OutputStream;Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_1
    iput-object v0, p0, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->e:Ljava/lang/Exception;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->d:Ljava/io/PipedOutputStream;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    return-void

    :goto_1
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->d:Ljava/io/PipedOutputStream;

    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    throw v0
.end method
