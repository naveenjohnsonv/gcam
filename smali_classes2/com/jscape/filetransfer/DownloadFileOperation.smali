.class public Lcom/jscape/filetransfer/DownloadFileOperation;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransferOperation;


# static fields
.field private static final q:[Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/filetransfer/FileTransferOperation;

.field private final b:Lcom/jscape/filetransfer/RemoteDirectory;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/io/File;

.field private final e:Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;

.field private final f:I

.field private final g:Lcom/jscape/util/Time;

.field private final h:Z

.field private final i:Lcom/jscape/filetransfer/DownloadFileOperation$Listener;

.field private j:Lcom/jscape/filetransfer/FileTransfer;

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:I

.field private n:Z

.field private o:Ljava/lang/Exception;

.field private volatile p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "#@pitSOi\u0005vHaO]{\u0005cb(\u0012#@v~xRHj$mip^H`\u0012}&\u000c#@htv\\PI\th~(\u000b#@eoaXQ\u007f\u0014w&\u0008#@aigRN2\u0010#@eoaXQ\u007f\u0014T~gTSk]\n#@wnv^Y|\u00139\u000e#@izm|H{\u0005ikaN\u0001\u0013#@`~yXHj2avzIYI\th~(\u000c#@gz{^Yc\u000ca\u007f(\u0017M\u0001`;x\\D/\u0001popPL{\u0013$mtQIjN"

    const/16 v4, 0xac

    const/16 v5, 0x13

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x63

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x3b

    const/16 v3, 0xe

    const-string v5, "w\u0014\"*,\u0006\u001c>r9#$TO,\u001f[\'!-\u0006\t?r9#$&\u0018>F1;(\u0006\u0006{O  2\u001d+4Z>*\"\u001d\'+Q\".5\u0000\u00075\t"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x37

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/filetransfer/DownloadFileOperation;->q:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    const/4 v1, 0x3

    if-eqz v15, :cond_8

    if-eq v15, v9, :cond_9

    const/4 v2, 0x2

    if-eq v15, v2, :cond_7

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x5f

    goto :goto_4

    :cond_4
    const/16 v1, 0x5e

    goto :goto_4

    :cond_5
    const/16 v1, 0x76

    goto :goto_4

    :cond_6
    const/16 v1, 0x78

    goto :goto_4

    :cond_7
    const/16 v1, 0x67

    goto :goto_4

    :cond_8
    const/16 v1, 0x6c

    :cond_9
    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransferOperation;Lcom/jscape/filetransfer/RemoteDirectory;Ljava/lang/String;Ljava/io/File;Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;ILcom/jscape/util/Time;ZLcom/jscape/filetransfer/DownloadFileOperation$Listener;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->b:Lcom/jscape/filetransfer/RemoteDirectory;

    if-eqz v0, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    if-eqz v0, :cond_2

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p3

    :cond_2
    :goto_0
    iput-object p3, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->c:Ljava/lang/String;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->d:Ljava/io/File;

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->e:Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;

    int-to-long p1, p6

    const-wide/16 p3, 0x0

    sget-object p5, Lcom/jscape/filetransfer/DownloadFileOperation;->q:[Ljava/lang/String;

    const/16 v0, 0xa

    aget-object p5, p5, v0

    invoke-static {p1, p2, p3, p4, p5}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p6, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->f:I

    invoke-static {p7}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p7, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->g:Lcom/jscape/util/Time;

    iput-boolean p8, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->h:Z

    invoke-static {p9}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p9, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->i:Lcom/jscape/filetransfer/DownloadFileOperation$Listener;

    return-void
.end method

.method static a(Lcom/jscape/filetransfer/DownloadFileOperation;)Ljava/io/File;
    .locals 0

    iget-object p0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->d:Ljava/io/File;

    return-object p0
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->o:Ljava/lang/Exception;

    iget p1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->m:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->m:I

    return-void
.end method

.method private a()Z
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->n:Z

    if-eqz v0, :cond_0

    if-nez v1, :cond_2

    iget v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->m:I

    :cond_0
    if-eqz v0, :cond_1

    iget v2, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->f:I

    if-ge v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->p:Z

    :cond_1
    if-eqz v0, :cond_3

    if-nez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method private static b(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method static b(Lcom/jscape/filetransfer/DownloadFileOperation;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->c:Ljava/lang/String;

    return-object p0
.end method

.method private b()V
    .locals 1

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadFileOperation;->e()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadFileOperation;->f()V

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadFileOperation;->g()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadFileOperation;->h()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadFileOperation;->i()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadFileOperation;->j()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadFileOperation;->k()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/jscape/filetransfer/DownloadFileOperation;->a(Ljava/lang/Exception;)V

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadFileOperation;->m()V

    :goto_0
    return-void
.end method

.method static c(Lcom/jscape/filetransfer/DownloadFileOperation;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iget-object p0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->j:Lcom/jscape/filetransfer/FileTransfer;

    return-object p0
.end method

.method private c()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/TransferOperationCancelledException;
        }
    .end annotation

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->p:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/filetransfer/TransferOperationCancelledException;

    invoke-direct {v0}, Lcom/jscape/filetransfer/TransferOperationCancelledException;-><init>()V

    throw v0
    :try_end_0
    .catch Lcom/jscape/filetransfer/TransferOperationCancelledException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method static d(Lcom/jscape/filetransfer/DownloadFileOperation;)I
    .locals 0

    iget p0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->m:I

    return p0
.end method

.method private d()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->o:Ljava/lang/Exception;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->o:Ljava/lang/Exception;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    throw v1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private e()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->n:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->o:Ljava/lang/Exception;

    return-void
.end method

.method private f()V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->m:I

    if-lez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->g:Lcom/jscape/util/Time;

    invoke-virtual {v0}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/jscape/util/D;->f(J)V

    :cond_1
    return-void
.end method

.method private g()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->j:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->isConnected()Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->k:Z

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->j:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->connect()Lcom/jscape/filetransfer/FileTransfer;

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->j:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v1, v2}, Lcom/jscape/filetransfer/FileTransferOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->j:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->getDir()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->l:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->b:Lcom/jscape/filetransfer/RemoteDirectory;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->j:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v1, v2}, Lcom/jscape/filetransfer/RemoteDirectory;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->d:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    :try_start_3
    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->j:Lcom/jscape/filetransfer/FileTransfer;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    if-eqz v0, :cond_3

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    new-instance v1, Ljava/io/File;

    const-string v0, "."

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :cond_3
    :goto_1
    invoke-interface {v2, v1}, Lcom/jscape/filetransfer/FileTransfer;->setLocalDir(Ljava/io/File;)Lcom/jscape/filetransfer/FileTransfer;

    return-void

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private h()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->e:Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;

    invoke-virtual {v0, p0}, Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;->apply(Lcom/jscape/filetransfer/DownloadFileOperation;)V

    return-void
.end method

.method private i()V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->h:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->j:Lcom/jscape/filetransfer/FileTransfer;

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/jscape/filetransfer/FileTransfer;->deleteFile(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method

.method private j()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->j:Lcom/jscape/filetransfer/FileTransfer;

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->l:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/jscape/filetransfer/FileTransfer;->setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private k()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->n:Z

    return-void
.end method

.method private l()V
    .locals 1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->k:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadFileOperation;->m()V

    :cond_1
    return-void
.end method

.method private m()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->j:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method


# virtual methods
.method public applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/DownloadFileOperation;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iput-object p1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->j:Lcom/jscape/filetransfer/FileTransfer;

    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadFileOperation;->a()Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_1

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadFileOperation;->b()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_2

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadFileOperation;->c()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadFileOperation;->d()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadFileOperation;->l()V

    iget-object p1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->i:Lcom/jscape/filetransfer/DownloadFileOperation$Listener;

    invoke-interface {p1, p0}, Lcom/jscape/filetransfer/DownloadFileOperation$Listener;->onOperationCompleted(Lcom/jscape/filetransfer/DownloadFileOperation;)V

    return-object p0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_1
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadFileOperation;->l()V

    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->i:Lcom/jscape/filetransfer/DownloadFileOperation$Listener;

    invoke-interface {v0, p0}, Lcom/jscape/filetransfer/DownloadFileOperation$Listener;->onOperationCompleted(Lcom/jscape/filetransfer/DownloadFileOperation;)V

    throw p1
.end method

.method public bridge synthetic applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/DownloadFileOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/DownloadFileOperation;

    move-result-object p1

    return-object p1
.end method

.method public cancel()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->p:Z

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->j:Lcom/jscape/filetransfer/FileTransfer;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->interrupt()V

    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/DownloadFileOperation;->q:[Ljava/lang/String;

    const/16 v2, 0xc

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->b:Lcom/jscape/filetransfer/RemoteDirectory;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0xb

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->d:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->e:Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->f:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->g:Lcom/jscape/util/Time;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->h:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->m:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->n:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->o:Ljava/lang/Exception;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/filetransfer/DownloadFileOperation;->p:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
