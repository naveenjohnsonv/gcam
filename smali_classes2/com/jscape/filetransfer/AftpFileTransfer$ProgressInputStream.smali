.class Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;
.super Ljava/io/InputStream;


# instance fields
.field private final a:Ljava/io/InputStream;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:J

.field private final f:J

.field private final g:J

.field private h:J

.field private i:J

.field final j:Lcom/jscape/filetransfer/AftpFileTransfer;


# direct methods
.method private constructor <init>(Lcom/jscape/filetransfer/AftpFileTransfer;Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJ)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->j:Lcom/jscape/filetransfer/AftpFileTransfer;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    iput-object p2, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->a:Ljava/io/InputStream;

    iput-object p3, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->d:Ljava/lang/String;

    iput-wide p6, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->e:J

    iput-wide p8, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->f:J

    iput-wide p10, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->g:J

    return-void
.end method

.method constructor <init>(Lcom/jscape/filetransfer/AftpFileTransfer;Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLcom/jscape/filetransfer/AftpFileTransfer$1;)V
    .locals 0

    invoke-direct/range {p0 .. p11}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;-><init>(Lcom/jscape/filetransfer/AftpFileTransfer;Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJ)V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->c()V

    :cond_1
    return-void
.end method

.method private b()Z
    .locals 5

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->h:J

    iget-wide v3, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->i:J

    sub-long/2addr v1, v3

    iget-wide v3, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->g:J

    cmp-long v1, v1, v3

    if-eqz v0, :cond_1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method private c()V
    .locals 14

    iget-wide v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->h:J

    iput-wide v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->i:J

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->j:Lcom/jscape/filetransfer/AftpFileTransfer;

    new-instance v13, Lcom/jscape/filetransfer/FileTransferProgressEvent;

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->j:Lcom/jscape/filetransfer/AftpFileTransfer;

    iget-object v3, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->c:Ljava/lang/String;

    iget-object v5, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->d:Ljava/lang/String;

    iget-wide v6, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->e:J

    iget-wide v8, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->h:J

    add-long v7, v6, v8

    iget-wide v11, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->f:J

    const/4 v6, 0x0

    const-wide/16 v9, 0x0

    move-object v1, v13

    invoke-direct/range {v1 .. v12}, Lcom/jscape/filetransfer/FileTransferProgressEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJ)V

    invoke-virtual {v0, v13}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method


# virtual methods
.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    return v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void
.end method

.method public mark(I)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->a:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->mark(I)V

    return-void
.end method

.method public markSupported()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    return v0
.end method

.method public read()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->a:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    :try_start_0
    iget-wide v2, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->h:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->h:J

    invoke-direct {p0}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    return v1
.end method

.method public read([BII)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->a:Ljava/io/InputStream;

    invoke-virtual {v1, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result p1

    if-eqz v0, :cond_0

    const/4 p2, -0x1

    if-eq p1, p2, :cond_0

    :try_start_0
    iget-wide p2, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->h:J

    int-to-long v0, p1

    add-long/2addr p2, v0

    iput-wide p2, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->h:J

    invoke-direct {p0}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    return p1
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    return-void
.end method

.method public skip(J)J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->a:Ljava/io/InputStream;

    invoke-virtual {v1, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide p1

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-wide v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->h:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->h:J

    invoke-direct {p0}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    return-wide p1
.end method
