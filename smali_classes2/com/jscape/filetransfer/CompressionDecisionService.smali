.class public interface abstract Lcom/jscape/filetransfer/CompressionDecisionService;
.super Ljava/lang/Object;


# static fields
.field public static final ALWAYS:Lcom/jscape/filetransfer/CompressionDecisionService;

.field public static final NEVER:Lcom/jscape/filetransfer/CompressionDecisionService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jscape/filetransfer/CompressionDecisionService$1;

    invoke-direct {v0}, Lcom/jscape/filetransfer/CompressionDecisionService$1;-><init>()V

    sput-object v0, Lcom/jscape/filetransfer/CompressionDecisionService;->NEVER:Lcom/jscape/filetransfer/CompressionDecisionService;

    new-instance v0, Lcom/jscape/filetransfer/CompressionDecisionService$2;

    invoke-direct {v0}, Lcom/jscape/filetransfer/CompressionDecisionService$2;-><init>()V

    sput-object v0, Lcom/jscape/filetransfer/CompressionDecisionService;->ALWAYS:Lcom/jscape/filetransfer/CompressionDecisionService;

    return-void
.end method


# virtual methods
.method public abstract compressionRequiredFor(Ljava/lang/String;J)Z
.end method
