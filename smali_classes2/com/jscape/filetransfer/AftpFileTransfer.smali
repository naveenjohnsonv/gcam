.class public Lcom/jscape/filetransfer/AftpFileTransfer;
.super Lcom/jscape/filetransfer/AbstractFileTransfer;


# static fields
.field private static final ATTEMPT_PERIOD:Lcom/jscape/util/Time;

.field private static final DEFAULT_BIT_RATE:Lcom/jscape/util/A;

.field private static final DEFAULT_COMPRESSION_DECISION_SERVICE:Lcom/jscape/filetransfer/CompressionDecisionService;

.field private static final DEFAULT_DATA_PROTECTION_ALGORITHM:Ljava/lang/String;

.field private static final DEFAULT_DATA_PROTECTION_KEY_LENGTH:I = 0x10

.field public static final DEFAULT_PORT:I = 0xbb8

.field private static final DEFAULT_REMOTE_DIRECTORY:Ljava/lang/String; = "/"

.field private static final DEFAULT_TIMEOUT:Lcom/jscape/util/Time;

.field private static final DEFAULT_USE_CONGESTION_CONTROL:Z = true

.field private static final FILE_SEPARATOR:Ljava/lang/String; = "/"

.field private static final LEADING_CURRENT_DIRECTORY:Ljava/lang/String;

.field private static final PROGRESS_THRESHOLD:I = 0x19000

.field private static final ROOT_PATH:Ljava/lang/String; = "/"

.field private static final a:[Ljava/lang/String;


# instance fields
.field private authToken:Ljava/lang/String;

.field private compressionDecisionService:Lcom/jscape/filetransfer/CompressionDecisionService;

.field private currentRemoteDirectory:Ljava/lang/String;

.field private dataProtectionAlgorithm:Ljava/lang/String;

.field private dataProtectionKeyLength:I

.field private volatile downloadDirectoryOperation:Lcom/jscape/filetransfer/DownloadDirectoryOperation;

.field private downloadRate:Lcom/jscape/util/A;

.field private securityMode:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

.field private service:Lcom/jscape/inet/a/k;

.field private sslKeyStore:Ljava/security/KeyStore;

.field private volatile uploadDirectoryOperation:Lcom/jscape/filetransfer/UploadDirectoryOperation;

.field private uploadRate:Lcom/jscape/util/A;

.field private useCongestionControl:Z


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x2

    const/4 v3, 0x0

    const-string v4, "E+\'8t\u007f\u0010b@\u0007\u000e`:\u0003jR\u0006K#?\u0000,\u0006\u0007\u0018$t\u001c\u007f\u0006\u000fK`s\u0001nE\u001a\u0004vc]\u0005Nw5Vx\u0002D+\u0003*AI\r*BN#+r\u001c\nji\u0015nT"

    const/16 v5, 0x45

    move v7, v2

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x3d

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/4 v5, 0x6

    const-string v4, "$J\u0003K ("

    move v7, v2

    move v8, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x5c

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/filetransfer/AftpFileTransfer;->a:[Ljava/lang/String;

    aget-object v1, v0, v3

    sput-object v1, Lcom/jscape/filetransfer/AftpFileTransfer;->LEADING_CURRENT_DIRECTORY:Ljava/lang/String;

    const/4 v1, 0x7

    aget-object v0, v0, v1

    sput-object v0, Lcom/jscape/filetransfer/AftpFileTransfer;->DEFAULT_DATA_PROTECTION_ALGORITHM:Ljava/lang/String;

    const-wide/16 v0, 0x3c

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->seconds(J)Lcom/jscape/util/Time;

    move-result-object v0

    sput-object v0, Lcom/jscape/filetransfer/AftpFileTransfer;->DEFAULT_TIMEOUT:Lcom/jscape/util/Time;

    const-wide/32 v0, 0xafc8

    invoke-static {v0, v1}, Lcom/jscape/util/A;->b(J)Lcom/jscape/util/A;

    move-result-object v0

    sput-object v0, Lcom/jscape/filetransfer/AftpFileTransfer;->DEFAULT_BIT_RATE:Lcom/jscape/util/A;

    sget-object v0, Lcom/jscape/filetransfer/CompressionDecisionService;->NEVER:Lcom/jscape/filetransfer/CompressionDecisionService;

    sput-object v0, Lcom/jscape/filetransfer/AftpFileTransfer;->DEFAULT_COMPRESSION_DECISION_SERVICE:Lcom/jscape/filetransfer/CompressionDecisionService;

    const-wide/16 v0, 0xa

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->seconds(J)Lcom/jscape/util/Time;

    move-result-object v0

    sput-object v0, Lcom/jscape/filetransfer/AftpFileTransfer;->ATTEMPT_PERIOD:Lcom/jscape/util/Time;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v10, :cond_8

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v1, 0x53

    goto :goto_4

    :cond_4
    const/16 v1, 0x1b

    goto :goto_4

    :cond_5
    const/16 v1, 0x36

    goto :goto_4

    :cond_6
    const/16 v1, 0x4e

    goto :goto_4

    :cond_7
    const/16 v1, 0x27

    goto :goto_4

    :cond_8
    const/16 v1, 0x39

    goto :goto_4

    :cond_9
    const/16 v1, 0x56

    :goto_4
    xor-int/2addr v1, v9

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v2, 0x2

    goto/16 :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;ILcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;Ljava/security/KeyStore;Ljava/lang/String;ILcom/jscape/util/A;Lcom/jscape/util/A;ZLcom/jscape/filetransfer/CompressionDecisionService;ZZZLjava/io/File;Ljava/lang/String;Ljava/util/logging/Logger;Ljava/util/Set;)V
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/jscape/util/Time;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;",
            "Ljava/security/KeyStore;",
            "Ljava/lang/String;",
            "I",
            "Lcom/jscape/util/A;",
            "Lcom/jscape/util/A;",
            "Z",
            "Lcom/jscape/filetransfer/CompressionDecisionService;",
            "ZZZ",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            "Ljava/util/logging/Logger;",
            "Ljava/util/Set<",
            "Lcom/jscape/filetransfer/FileTransferListener;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v15, p0

    move-object/from16 v0, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move/from16 v16, p15

    move/from16 v17, p16

    move/from16 v18, p17

    move-object/from16 v20, p18

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    invoke-virtual/range {p3 .. p3}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v8

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v19

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x2

    const/16 v23, 0x0

    move-object/from16 v15, v23

    invoke-direct/range {v0 .. v22}, Lcom/jscape/filetransfer/AbstractFileTransfer;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;IZILjava/lang/String;ZZZLjava/util/TimeZone;Ljava/io/File;Ljava/util/logging/Logger;Ljava/util/Set;)V

    move-object/from16 v1, p6

    iput-object v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->authToken:Ljava/lang/String;

    invoke-static/range {p7 .. p7}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object/from16 v1, p7

    iput-object v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->securityMode:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    move-object/from16 v1, p8

    iput-object v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->sslKeyStore:Ljava/security/KeyStore;

    move-object/from16 v1, p9

    iput-object v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->dataProtectionAlgorithm:Ljava/lang/String;

    move/from16 v1, p10

    iput v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->dataProtectionKeyLength:I

    invoke-static/range {p11 .. p11}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object/from16 v1, p11

    iput-object v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->downloadRate:Lcom/jscape/util/A;

    invoke-static/range {p12 .. p12}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object/from16 v1, p12

    iput-object v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->uploadRate:Lcom/jscape/util/A;

    move/from16 v1, p13

    iput-boolean v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->useCongestionControl:Z

    invoke-static/range {p14 .. p14}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object/from16 v1, p14

    iput-object v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->compressionDecisionService:Lcom/jscape/filetransfer/CompressionDecisionService;

    invoke-static/range {p19 .. p19}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    const-string v1, "/"

    move-object/from16 v2, p19

    invoke-direct {v0, v1, v2}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 7

    sget-object v0, Ljava/util/logging/Level;->OFF:Ljava/util/logging/Level;

    invoke-static {v0}, Lcom/jscape/util/j/b;->a(Ljava/util/logging/Level;)Lcom/jscape/util/j/b;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/jscape/filetransfer/AftpFileTransfer;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/logging/Logger;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/logging/Logger;)V
    .locals 22

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v20, p5

    sget-object v3, Lcom/jscape/filetransfer/AftpFileTransfer;->DEFAULT_TIMEOUT:Lcom/jscape/util/Time;

    sget-object v7, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->NO_PROTECTION:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    sget-object v6, Lcom/jscape/filetransfer/AftpFileTransfer;->a:[Ljava/lang/String;

    const/4 v8, 0x4

    aget-object v9, v6, v8

    sget-object v12, Lcom/jscape/filetransfer/AftpFileTransfer;->DEFAULT_BIT_RATE:Lcom/jscape/util/A;

    move-object v11, v12

    sget-object v14, Lcom/jscape/filetransfer/AftpFileTransfer;->DEFAULT_COMPRESSION_DECISION_SERVICE:Lcom/jscape/filetransfer/CompressionDecisionService;

    sget-object v18, Lcom/jscape/filetransfer/AftpFileTransfer;->DEFAULT_LOCAL_DIRECTORY:Ljava/io/File;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v21

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x10

    const/4 v13, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const-string v19, "/"

    invoke-direct/range {v0 .. v21}, Lcom/jscape/filetransfer/AftpFileTransfer;-><init>(Ljava/lang/String;ILcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;Ljava/security/KeyStore;Ljava/lang/String;ILcom/jscape/util/A;Lcom/jscape/util/A;ZLcom/jscape/filetransfer/CompressionDecisionService;ZZZLjava/io/File;Ljava/lang/String;Ljava/util/logging/Logger;Ljava/util/Set;)V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private downloadListener(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZ)Lcom/jscape/inet/a/l;
    .locals 12

    if-eqz p8, :cond_0

    sget-object v0, Lcom/jscape/inet/a/l;->a:Lcom/jscape/inet/a/l;

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;

    const/4 v6, 0x1

    const/4 v11, 0x0

    move-object v1, v0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-wide/from16 v7, p4

    move-wide/from16 v9, p6

    invoke-direct/range {v1 .. v11}, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;-><init>(Lcom/jscape/filetransfer/AftpFileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLcom/jscape/filetransfer/AftpFileTransfer$1;)V

    :goto_0
    return-object v0
.end method

.method private fileFor(Lcom/jscape/inet/a/a/b/ac;)Lcom/jscape/filetransfer/FileTransferRemoteFile;
    .locals 11

    new-instance v10, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    iget-object v1, p1, Lcom/jscape/inet/a/a/b/ac;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/jscape/inet/a/a/b/ac;->a()Z

    move-result v3

    iget-wide v5, p1, Lcom/jscape/inet/a/a/b/ac;->e:J

    new-instance v7, Ljava/util/Date;

    iget-wide v8, p1, Lcom/jscape/inet/a/a/b/ac;->f:J

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    iget-boolean v8, p1, Lcom/jscape/inet/a/a/b/ac;->c:Z

    iget-boolean v9, p1, Lcom/jscape/inet/a/a/b/ac;->d:Z

    const-string v2, ""

    const/4 v4, 0x0

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/jscape/filetransfer/FileTransferRemoteFile;-><init>(Ljava/lang/String;Ljava/lang/String;ZZJLjava/util/Date;ZZ)V

    return-object v10
.end method

.method private initService()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->initService4()V

    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->startService()V
    :try_end_0
    .catch Lcom/jscape/inet/a/b; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->initService3()V

    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->startService()V
    :try_end_1
    .catch Lcom/jscape/inet/a/b; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->initService2()V

    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->startService()V

    :goto_0
    return-void
.end method

.method private raiseDownloadErrorEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    new-instance v6, Lcom/jscape/filetransfer/FileTransferErrorEvent;

    iget-object v3, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    const/4 v5, 0x1

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/jscape/filetransfer/FileTransferErrorEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p0, v6}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method private raiseDownloadEvent(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 12

    move-object v10, p0

    new-instance v11, Lcom/jscape/filetransfer/FileTransferDownloadEvent;

    iget-object v3, v10, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    iget-boolean v9, v10, Lcom/jscape/filetransfer/AftpFileTransfer;->interrupted:Z

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-wide v5, p3

    move-wide/from16 v7, p5

    invoke-direct/range {v0 .. v9}, Lcom/jscape/filetransfer/FileTransferDownloadEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZ)V

    invoke-virtual {p0, v11}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method private raiseUploadErrorEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    new-instance v6, Lcom/jscape/filetransfer/FileTransferErrorEvent;

    iget-object v3, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, v6

    move-object v1, p0

    move-object v2, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/jscape/filetransfer/FileTransferErrorEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p0, v6}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method private raiseUploadEvent(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 11

    move-object v9, p0

    new-instance v10, Lcom/jscape/filetransfer/FileTransferUploadEvent;

    iget-object v3, v9, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p2

    move-object v4, p1

    move-wide v5, p3

    move-wide/from16 v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/jscape/filetransfer/FileTransferUploadEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    invoke-virtual {p0, v10}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method private resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private resolveRemotePathFor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/jscape/filetransfer/AftpFileTransfer;->stripLeadingCurrentDirectory(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/jscape/filetransfer/AftpFileTransfer;->stripTrailingSeparator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string v1, "/"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v0, :cond_2

    if-eqz v2, :cond_0

    return-object p2

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0

    :cond_1
    move-object p2, p1

    move-object p1, v1

    goto :goto_1

    :cond_2
    :goto_0
    if-eqz v2, :cond_3

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    sget-object v0, Lcom/jscape/filetransfer/AftpFileTransfer;->a:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v3, v0, v2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v2, v4

    const/4 p1, 0x1

    aput-object p2, v2, p1

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x3

    aget-object p2, v0, p2

    :goto_1
    invoke-virtual {p1, p2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private safeFileSizeOf(Ljava/lang/String;)J
    .locals 2

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->getFilesize(Ljava/lang/String;)J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    :catch_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method private stripLeadingCurrentDirectory(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/filetransfer/AftpFileTransfer;->a:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    if-eqz v0, :cond_0

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    aget-object v2, v1, v0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method private stripTrailingSeparator(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    if-le v1, v2, :cond_1

    if-eqz v0, :cond_1

    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    :cond_0
    if-eqz v1, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method private uploadListener(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZ)Lcom/jscape/inet/a/l;
    .locals 12

    if-eqz p8, :cond_0

    sget-object v0, Lcom/jscape/inet/a/l;->a:Lcom/jscape/inet/a/l;

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;

    const/4 v6, 0x0

    const/4 v11, 0x0

    move-object v1, v0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-wide/from16 v7, p4

    move-wide/from16 v9, p6

    invoke-direct/range {v1 .. v11}, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;-><init>(Lcom/jscape/filetransfer/AftpFileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLcom/jscape/filetransfer/AftpFileTransfer$1;)V

    :goto_0
    return-object v0
.end method

.method private wrap(Lcom/jscape/inet/a/d;)Lcom/jscape/filetransfer/FileTransferException;
    .locals 2

    sget-object v0, Lcom/jscape/filetransfer/AftpFileTransfer$1;->a:[I

    iget-object v1, p1, Lcom/jscape/inet/a/d;->c:Lcom/jscape/inet/a/a/b/ErrorCode;

    invoke-virtual {v1}, Lcom/jscape/inet/a/a/b/ErrorCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/jscape/filetransfer/FileTransferException;

    invoke-direct {v0, p1}, Lcom/jscape/filetransfer/FileTransferException;-><init>(Ljava/lang/Throwable;)V

    return-object v0

    :cond_0
    new-instance p1, Lcom/jscape/filetransfer/FileTransferDeniedException;

    invoke-direct {p1}, Lcom/jscape/filetransfer/FileTransferDeniedException;-><init>()V

    return-object p1

    :cond_1
    new-instance p1, Lcom/jscape/filetransfer/FileTransferAuthenticationException;

    invoke-direct {p1}, Lcom/jscape/filetransfer/FileTransferAuthenticationException;-><init>()V

    return-object p1
.end method

.method private wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    instance-of v1, p1, Lcom/jscape/inet/a/d;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    :goto_0
    check-cast p1, Lcom/jscape/inet/a/d;

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Lcom/jscape/inet/a/d;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v0, :cond_3

    instance-of v1, v1, Lcom/jscape/inet/a/d;

    :cond_1
    if-eqz v1, :cond_2

    :goto_1
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    :cond_3
    if-eqz v0, :cond_4

    if-eqz v1, :cond_5

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v0, :cond_4

    instance-of v0, v1, Lcom/jscape/inet/a/d;

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    goto :goto_1

    :cond_4
    move-object p1, v1

    :cond_5
    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public abortDownloadThread(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->downloadDirectoryOperation:Lcom/jscape/filetransfer/DownloadDirectoryOperation;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v1, p1}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->cancel(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public abortDownloadThreads()V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->downloadDirectoryOperation:Lcom/jscape/filetransfer/DownloadDirectoryOperation;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->cancel()V

    :cond_1
    return-void
.end method

.method public abortUploadThread(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->uploadDirectoryOperation:Lcom/jscape/filetransfer/UploadDirectoryOperation;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->cancel(Ljava/io/File;)V

    :cond_1
    return-void
.end method

.method public abortUploadThreads()V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->uploadDirectoryOperation:Lcom/jscape/filetransfer/UploadDirectoryOperation;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->cancel()V

    :cond_1
    return-void
.end method

.method public connect()Lcom/jscape/filetransfer/AftpFileTransfer;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->initService()V

    sget-object v0, Lcom/jscape/filetransfer/AftpFileTransfer;->a:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/jscape/util/ak;->a(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->executorService:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/jscape/filetransfer/FileTransferConnectedEvent;

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->hostname:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/jscape/filetransfer/FileTransferConnectedEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->disposeService()V

    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->disposeExecutorService()V

    invoke-direct {p0, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
.end method

.method public bridge synthetic connect()Lcom/jscape/filetransfer/FileTransfer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->connect()Lcom/jscape/filetransfer/AftpFileTransfer;

    move-result-object v0

    return-object v0
.end method

.method protected connectionProtector()Lcom/jscape/util/k/a/s;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->securityMode:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    sget-object v1, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->NO_PROTECTION:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/jscape/util/k/b/m;

    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->sslContext()Ljavax/net/ssl/SSLContext;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/jscape/util/k/b/a;->a:Lcom/jscape/util/k/b/a;

    const/4 v7, 0x1

    const/4 v8, 0x0

    new-instance v9, Lcom/jscape/filetransfer/AftpFileTransfer$SslHandshakeEventForwarder;

    const/4 v1, 0x0

    invoke-direct {v9, p0, v1}, Lcom/jscape/filetransfer/AftpFileTransfer$SslHandshakeEventForwarder;-><init>(Lcom/jscape/filetransfer/AftpFileTransfer;Lcom/jscape/filetransfer/AftpFileTransfer$1;)V

    move-object v2, v0

    invoke-direct/range {v2 .. v9}, Lcom/jscape/util/k/b/m;-><init>(Ljavax/net/ssl/SSLSocketFactory;[Ljava/lang/String;[Ljava/lang/String;Lcom/jscape/util/k/b/a;ZZLjavax/net/ssl/HandshakeCompletedListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/jscape/util/k/a/s;->a:Lcom/jscape/util/k/a/s;

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public copy()Lcom/jscape/filetransfer/FileTransfer;
    .locals 25

    move-object/from16 v0, p0

    new-instance v23, Lcom/jscape/filetransfer/AftpFileTransfer;

    move-object/from16 v1, v23

    iget-object v2, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->hostname:Ljava/lang/String;

    iget v3, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->port:I

    iget-wide v4, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->timeout:J

    invoke-static {v4, v5}, Lcom/jscape/util/Time;->millis(J)Lcom/jscape/util/Time;

    move-result-object v4

    iget-object v5, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->username:Ljava/lang/String;

    iget-object v6, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->password:Ljava/lang/String;

    iget-object v7, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->authToken:Ljava/lang/String;

    iget-object v8, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->securityMode:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    iget-object v9, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->sslKeyStore:Ljava/security/KeyStore;

    iget-object v10, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->dataProtectionAlgorithm:Ljava/lang/String;

    iget v11, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->dataProtectionKeyLength:I

    iget-object v12, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->downloadRate:Lcom/jscape/util/A;

    iget-object v13, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->uploadRate:Lcom/jscape/util/A;

    iget-boolean v14, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->useCongestionControl:Z

    iget-object v15, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->compressionDecisionService:Lcom/jscape/filetransfer/CompressionDecisionService;

    move-object/from16 v24, v1

    iget-boolean v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->downloadedFileTimestampPreservingRequired:Z

    move/from16 v16, v1

    iget-boolean v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->uploadedFileTimestampPreservingRequired:Z

    move/from16 v17, v1

    iget-boolean v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->checksumVerificationRequired:Z

    move/from16 v18, v1

    iget-object v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentLocalDirectory:Ljava/nio/file/Path;

    invoke-interface {v1}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object v19

    iget-object v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    move-object/from16 v20, v1

    iget-object v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->logger:Ljava/util/logging/Logger;

    move-object/from16 v21, v1

    iget-object v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer;->listeners:Ljava/util/Set;

    move-object/from16 v22, v1

    move-object/from16 v1, v24

    invoke-direct/range {v1 .. v22}, Lcom/jscape/filetransfer/AftpFileTransfer;-><init>(Ljava/lang/String;ILcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;Ljava/security/KeyStore;Ljava/lang/String;ILcom/jscape/util/A;Lcom/jscape/util/A;ZLcom/jscape/filetransfer/CompressionDecisionService;ZZZLjava/io/File;Ljava/lang/String;Ljava/util/logging/Logger;Ljava/util/Set;)V

    return-object v23
.end method

.method public deleteDir(Ljava/lang/String;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    invoke-interface {v1, v0, p2}, Lcom/jscape/inet/a/k;->a(Ljava/lang/String;Z)V

    new-instance p2, Lcom/jscape/filetransfer/FileTransferDeleteDirEvent;

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-direct {p2, p0, p1, v0}, Lcom/jscape/filetransfer/FileTransferDeleteDirEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/jscape/inet/a/k;->a(Ljava/lang/String;Z)V

    new-instance v0, Lcom/jscape/filetransfer/FileTransferDeleteFileEvent;

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/filetransfer/FileTransferDeleteFileEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public disconnect()V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->disposeService()V

    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->disposeExecutorService()V

    :cond_1
    new-instance v0, Lcom/jscape/filetransfer/FileTransferDisconnectedEvent;

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->hostname:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/jscape/filetransfer/FileTransferDisconnectedEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method

.method protected disposeService()V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-interface {v1}, Lcom/jscape/inet/a/k;->j()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    :cond_1
    return-void
.end method

.method public download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 27
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    move-object/from16 v14, p0

    move-object/from16 v15, p2

    const/16 v16, 0x0

    :try_start_0
    invoke-direct {v14, v15}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    invoke-interface {v1, v0}, Lcom/jscape/inet/a/k;->a(Ljava/lang/String;)Lcom/jscape/inet/a/a/b/ac;

    move-result-object v13

    iget-wide v11, v13, Lcom/jscape/inet/a/a/b/ac;->e:J

    invoke-virtual/range {p0 .. p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveLocalPathFor(Ljava/lang/String;)Ljava/io/File;

    move-result-object v9

    iget-object v1, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->compressionDecisionService:Lcom/jscape/filetransfer/CompressionDecisionService;

    iget-object v2, v13, Lcom/jscape/inet/a/a/b/ac;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v11, v12}, Lcom/jscape/filetransfer/CompressionDecisionService;->compressionRequiredFor(Ljava/lang/String;J)Z

    move-result v17
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    if-eqz v17, :cond_0

    :try_start_1
    new-instance v10, Lcom/jscape/util/h/p;

    new-instance v7, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;

    invoke-static {v9, v2, v3, v1}, Lcom/jscape/util/h/r;->a(Ljava/io/File;JZ)Lcom/jscape/util/h/r;

    move-result-object v3

    iget-object v5, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    const-wide/16 v18, 0x0

    const-wide/32 v20, 0x19000

    const/16 v22, 0x0

    move-object v1, v7

    move-object/from16 v2, p0

    move-object/from16 v4, p2

    move-object/from16 v23, v7

    move-wide/from16 v7, v18

    move-object/from16 v24, v9

    move-object/from16 v25, v10

    move-wide v9, v11

    move-wide/from16 v18, v11

    move-wide/from16 v11, v20

    move-object/from16 v26, v13

    move-object/from16 v13, v22

    invoke-direct/range {v1 .. v13}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;-><init>(Lcom/jscape/filetransfer/AftpFileTransfer;Ljava/io/OutputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLcom/jscape/filetransfer/AftpFileTransfer$1;)V

    move-object/from16 v2, v23

    move-object/from16 v1, v25

    invoke-direct {v1, v2}, Lcom/jscape/util/h/p;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object/from16 v16, v1

    move-object/from16 v10, v24

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    move-object/from16 v24, v9

    move-wide/from16 v18, v11

    move-object/from16 v26, v13

    new-instance v4, Lcom/jscape/util/h/p;

    move-object/from16 v10, v24

    invoke-static {v10, v2, v3, v1}, Lcom/jscape/util/h/r;->a(Ljava/io/File;JZ)Lcom/jscape/util/h/r;

    move-result-object v1

    invoke-direct {v4, v1}, Lcom/jscape/util/h/p;-><init>(Ljava/io/OutputStream;)V

    move-object/from16 v16, v4

    :goto_0
    iget-object v3, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-wide/from16 v7, v18

    move/from16 v9, v17

    invoke-direct/range {v1 .. v9}, Lcom/jscape/filetransfer/AftpFileTransfer;->downloadListener(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZ)Lcom/jscape/inet/a/l;

    move-result-object v7

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v8

    iget-object v1, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    const-wide/16 v3, 0x0

    move-object v2, v0

    move-object/from16 v5, v16

    move/from16 v6, v17

    invoke-interface/range {v1 .. v7}, Lcom/jscape/inet/a/k;->a(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/a/l;)V

    invoke-virtual {v14, v10, v15}, Lcom/jscape/filetransfer/AftpFileTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v26

    iget-wide v0, v0, Lcom/jscape/inet/a/a/b/ac;->f:J

    invoke-virtual {v14, v10, v0, v1}, Lcom/jscape/filetransfer/AftpFileTransfer;->safeSetModificationTime(Ljava/io/File;J)V

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Lcom/jscape/util/h/p;->a()J

    move-result-wide v4

    invoke-static {v8, v9}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v7}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseDownloadEvent(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-static/range {v16 .. v16}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    return-object v10

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    move-object/from16 v1, p1

    :try_start_3
    invoke-direct {v14, v15, v1}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseDownloadErrorEvent(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v14, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_1
    invoke-static/range {v16 .. v16}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    throw v0
.end method

.method public downloadDir(Ljava/lang/String;IZI)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :try_start_0
    new-instance p1, Lcom/jscape/filetransfer/DownloadDirectoryOperation;

    sget-object v1, Lcom/jscape/filetransfer/FileTransferOperation;->NULL:Lcom/jscape/filetransfer/FileTransferOperation;

    const/4 v0, 0x1

    add-int/2addr p2, v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    sget-object v4, Lcom/jscape/filetransfer/AftpFileTransfer;->ATTEMPT_PERIOD:Lcom/jscape/util/Time;

    const-string v5, "/"

    if-lez p4, :cond_0

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    move-object v6, p2

    move-object v0, p1

    invoke-direct/range {v0 .. v6}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;-><init>(Lcom/jscape/filetransfer/FileTransferOperation;Ljava/lang/String;ILcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/Integer;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->downloadDirectoryOperation:Lcom/jscape/filetransfer/DownloadDirectoryOperation;

    iget-boolean p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->checksumVerificationRequired:Z

    invoke-virtual {p0, p3}, Lcom/jscape/filetransfer/AftpFileTransfer;->setChecksumVerificationRequired(Z)Lcom/jscape/filetransfer/FileTransfer;

    iget-object p2, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->downloadDirectoryOperation:Lcom/jscape/filetransfer/DownloadDirectoryOperation;

    invoke-virtual {p2, p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->setChecksumVerificationRequired(Z)Lcom/jscape/filetransfer/FileTransfer;

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public exists(Ljava/lang/String;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    invoke-interface {v0, p1}, Lcom/jscape/inet/a/k;->a(Ljava/lang/String;)Lcom/jscape/inet/a/a/b/ac;
    :try_end_0
    .catch Lcom/jscape/inet/a/d; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    :try_start_1
    iget-object v0, p1, Lcom/jscape/inet/a/d;->c:Lcom/jscape/inet/a/a/b/ErrorCode;

    sget-object v1, Lcom/jscape/inet/a/a/b/ErrorCode;->INVALID_PATH:Lcom/jscape/inet/a/a/b/ErrorCode;
    :try_end_1
    .catch Lcom/jscape/inet/a/d; {:try_start_1 .. :try_end_1} :catch_2

    if-ne v0, v1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Lcom/jscape/inet/a/d;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public getAuthToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->authToken:Ljava/lang/String;

    return-object v0
.end method

.method public getDataProtectionAlgorithm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->dataProtectionAlgorithm:Ljava/lang/String;

    return-object v0
.end method

.method public getDataProtectionKeyLength()I
    .locals 1

    iget v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->dataProtectionKeyLength:I

    return v0
.end method

.method public getDir()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    return-object v0
.end method

.method public getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Enumeration<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->getDirListing(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    return-object p1
.end method

.method public getDirListing(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Enumeration<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    invoke-interface {v1, p1}, Lcom/jscape/inet/a/k;->d(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/a/a/b/ad;

    invoke-direct {v1}, Lcom/jscape/inet/a/a/b/ad;-><init>()V

    invoke-interface {p1, v1}, Ljava/util/List;->sort(Ljava/util/Comparator;)V

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/a/a/b/ac;

    iget-object v3, v2, Lcom/jscape/inet/a/a/b/ac;->a:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, v2}, Lcom/jscape/filetransfer/AftpFileTransfer;->fileFor(Lcom/jscape/inet/a/a/b/ac;)Lcom/jscape/filetransfer/FileTransferRemoteFile;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_1
    if-nez v0, :cond_0

    :cond_2
    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getDownloadRate()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->downloadRate:Lcom/jscape/util/A;

    invoke-virtual {v0}, Lcom/jscape/util/A;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    invoke-interface {v0, p1}, Lcom/jscape/inet/a/k;->a(Ljava/lang/String;)Lcom/jscape/inet/a/a/b/ac;

    move-result-object p1

    new-instance v0, Ljava/util/Date;

    iget-wide v1, p1, Lcom/jscape/inet/a/a/b/ac;->f:J

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getFilesize(Ljava/lang/String;)J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    invoke-interface {v0, p1}, Lcom/jscape/inet/a/k;->a(Ljava/lang/String;)Lcom/jscape/inet/a/a/b/ac;

    move-result-object p1

    iget-wide v0, p1, Lcom/jscape/inet/a/a/b/ac;->e:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getImplementation()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    return-object v0
.end method

.method public getSecurityMode()Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->securityMode:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    return-object v0
.end method

.method public getSslKeyStore()Ljava/security/KeyStore;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->sslKeyStore:Ljava/security/KeyStore;

    return-object v0
.end method

.method public getUploadRate()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->uploadRate:Lcom/jscape/util/A;

    invoke-virtual {v0}, Lcom/jscape/util/A;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method protected initService2()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->rawConnector()Lcom/jscape/util/k/b/i;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->connectionProtector()Lcom/jscape/util/k/a/s;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->dataProtectionAlgorithm:Ljava/lang/String;

    iget v3, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->dataProtectionKeyLength:I

    iget-object v4, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->downloadRate:Lcom/jscape/util/A;

    iget-object v5, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->uploadRate:Lcom/jscape/util/A;

    iget-boolean v6, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->useCongestionControl:Z

    iget-object v7, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->logger:Ljava/util/logging/Logger;

    invoke-static/range {v0 .. v7}, Lcom/jscape/inet/a/n;->a(Lcom/jscape/util/k/a/v;Lcom/jscape/util/k/a/s;Ljava/lang/String;ILcom/jscape/util/A;Lcom/jscape/util/A;ZLjava/util/logging/Logger;)Lcom/jscape/inet/a/n;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    return-void
.end method

.method protected initService3()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->rawConnector()Lcom/jscape/util/k/b/i;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->connectionProtector()Lcom/jscape/util/k/a/s;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->dataProtectionAlgorithm:Ljava/lang/String;

    iget v3, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->dataProtectionKeyLength:I

    iget-object v4, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->downloadRate:Lcom/jscape/util/A;

    iget-object v5, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->uploadRate:Lcom/jscape/util/A;

    iget-boolean v6, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->useCongestionControl:Z

    iget-object v7, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->logger:Ljava/util/logging/Logger;

    invoke-static/range {v0 .. v7}, Lcom/jscape/inet/a/o;->a(Lcom/jscape/util/k/a/v;Lcom/jscape/util/k/a/s;Ljava/lang/String;ILcom/jscape/util/A;Lcom/jscape/util/A;ZLjava/util/logging/Logger;)Lcom/jscape/inet/a/o;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    return-void
.end method

.method protected initService4()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->rawConnector()Lcom/jscape/util/k/b/i;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/filetransfer/AftpFileTransfer;->connectionProtector()Lcom/jscape/util/k/a/s;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->dataProtectionAlgorithm:Ljava/lang/String;

    iget v3, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->dataProtectionKeyLength:I

    iget-object v4, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->downloadRate:Lcom/jscape/util/A;

    iget-object v5, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->uploadRate:Lcom/jscape/util/A;

    iget-boolean v6, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->useCongestionControl:Z

    iget-object v7, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->logger:Ljava/util/logging/Logger;

    invoke-static/range {v0 .. v7}, Lcom/jscape/inet/a/p;->a(Lcom/jscape/util/k/a/v;Lcom/jscape/util/k/a/s;Ljava/lang/String;ILcom/jscape/util/A;Lcom/jscape/util/A;ZLjava/util/logging/Logger;)Lcom/jscape/inet/a/p;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    return-void
.end method

.method public interrupt()V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->interrupt()V

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-interface {v1}, Lcom/jscape/inet/a/k;->f()V

    :cond_1
    return-void
.end method

.method public isConnected()Z
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_2

    :cond_0
    invoke-interface {v1}, Lcom/jscape/inet/a/k;->h()Z

    move-result v1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    invoke-interface {v1}, Lcom/jscape/inet/a/k;->a()Z

    move-result v1

    :cond_1
    if-eqz v0, :cond_3

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public isDirectory(Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    invoke-interface {v0, p1}, Lcom/jscape/inet/a/k;->a(Ljava/lang/String;)Lcom/jscape/inet/a/a/b/ac;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/a/a/b/ac;->a()Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public isUseCongestionControl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->useCongestionControl:Z

    return v0
.end method

.method public makeDirRecursive(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    invoke-interface {v1, v0}, Lcom/jscape/inet/a/k;->c(Ljava/lang/String;)V

    new-instance v0, Lcom/jscape/filetransfer/FileTransferCreateDirEvent;

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/filetransfer/FileTransferCreateDirEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method protected rawConnector()Lcom/jscape/util/k/b/i;
    .locals 4

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->hostname:Ljava/lang/String;

    iget v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->port:I

    iget-wide v2, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->timeout:J

    invoke-static {v0, v1, v2, v3}, Lcom/jscape/util/k/b/i;->a(Ljava/lang/String;IJ)Lcom/jscape/util/k/b/i;

    move-result-object v0

    return-object v0
.end method

.method public renameFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    invoke-interface {v2, v0, v1}, Lcom/jscape/inet/a/k;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/jscape/filetransfer/FileTransferRenameFileEvent;

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/jscape/filetransfer/FileTransferRenameFileEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public resumeDownload(Ljava/io/OutputStream;Ljava/lang/String;J)V
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    move-object/from16 v14, p0

    move-object/from16 v15, p2

    :try_start_0
    invoke-direct {v14, v15}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    invoke-interface {v1, v0}, Lcom/jscape/inet/a/k;->a(Ljava/lang/String;)Lcom/jscape/inet/a/a/b/ac;

    move-result-object v1

    iget-wide v2, v1, Lcom/jscape/inet/a/a/b/ac;->e:J

    sub-long v11, v2, p3

    iget-object v2, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->compressionDecisionService:Lcom/jscape/filetransfer/CompressionDecisionService;

    iget-object v1, v1, Lcom/jscape/inet/a/a/b/ac;->a:Ljava/lang/String;

    invoke-interface {v2, v1, v11, v12}, Lcom/jscape/filetransfer/CompressionDecisionService;->compressionRequiredFor(Ljava/lang/String;J)Z

    move-result v16
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v16, :cond_0

    :try_start_1
    new-instance v13, Lcom/jscape/util/h/p;

    new-instance v9, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;

    iget-object v5, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    const-string v6, ""

    const-wide/32 v17, 0x19000

    const/16 v19, 0x0

    move-object v1, v9

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-wide/from16 v7, p3

    move-object/from16 v20, v9

    move-wide v9, v11

    move-wide/from16 v21, v11

    move-wide/from16 v11, v17

    move-object v15, v13

    move-object/from16 v13, v19

    invoke-direct/range {v1 .. v13}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;-><init>(Lcom/jscape/filetransfer/AftpFileTransfer;Ljava/io/OutputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLcom/jscape/filetransfer/AftpFileTransfer$1;)V

    move-object/from16 v1, v20

    invoke-direct {v15, v1}, Lcom/jscape/util/h/p;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object v13, v15

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    move-wide/from16 v21, v11

    new-instance v13, Lcom/jscape/util/h/p;

    move-object/from16 v1, p1

    invoke-direct {v13, v1}, Lcom/jscape/util/h/p;-><init>(Ljava/io/OutputStream;)V

    :goto_0
    iget-object v3, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    const-string v4, ""

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-wide/from16 v5, p3

    move-wide/from16 v7, v21

    move/from16 v9, v16

    invoke-direct/range {v1 .. v9}, Lcom/jscape/filetransfer/AftpFileTransfer;->downloadListener(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZ)Lcom/jscape/inet/a/l;

    move-result-object v7

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v8

    iget-object v1, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    move-object v2, v0

    move-wide/from16 v3, p3

    move-object v5, v13

    move/from16 v6, v16

    invoke-interface/range {v1 .. v7}, Lcom/jscape/inet/a/k;->a(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/a/l;)V

    const-string v3, ""

    invoke-virtual {v13}, Lcom/jscape/util/h/p;->a()J

    move-result-wide v4

    invoke-static {v8, v9}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v7}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseDownloadEvent(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    return-void

    :catch_1
    move-exception v0

    const-string v1, ""

    move-object/from16 v2, p2

    invoke-direct {v14, v2, v1}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseDownloadErrorEvent(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v14, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
.end method

.method public resumeDownload(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 27
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    move-object/from16 v14, p0

    move-object/from16 v15, p2

    move-wide/from16 v11, p3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    const/16 v16, 0x0

    :try_start_0
    invoke-direct {v14, v15}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    invoke-interface {v1, v0}, Lcom/jscape/inet/a/k;->a(Ljava/lang/String;)Lcom/jscape/inet/a/a/b/ac;

    move-result-object v13

    iget-wide v1, v13, Lcom/jscape/inet/a/a/b/ac;->e:J

    sub-long v9, v1, v11

    invoke-virtual/range {p0 .. p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveLocalPathFor(Ljava/lang/String;)Ljava/io/File;

    move-result-object v7

    iget-object v1, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->compressionDecisionService:Lcom/jscape/filetransfer/CompressionDecisionService;

    iget-object v2, v13, Lcom/jscape/inet/a/a/b/ac;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v9, v10}, Lcom/jscape/filetransfer/CompressionDecisionService;->compressionRequiredFor(Ljava/lang/String;J)Z

    move-result v17
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    if-eqz v17, :cond_0

    :try_start_1
    new-instance v8, Lcom/jscape/util/h/p;

    new-instance v6, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;

    invoke-static {v7, v11, v12, v1}, Lcom/jscape/util/h/r;->a(Ljava/io/File;JZ)Lcom/jscape/util/h/r;

    move-result-object v3

    iget-object v5, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v18

    const-wide/32 v19, 0x19000

    const/16 v21, 0x0

    move-object v1, v6

    move-object/from16 v2, p0

    move-object/from16 v4, p2

    move-object/from16 v22, v6

    move-object/from16 v6, v18

    move-object/from16 v18, v7

    move-object/from16 v23, v8

    move-wide/from16 v7, p3

    move-wide/from16 v24, v9

    move-wide/from16 v11, v19

    move-object/from16 v26, v13

    move-object/from16 v13, v21

    invoke-direct/range {v1 .. v13}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressOutputStream;-><init>(Lcom/jscape/filetransfer/AftpFileTransfer;Ljava/io/OutputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLcom/jscape/filetransfer/AftpFileTransfer$1;)V

    move-object/from16 v2, v22

    move-object/from16 v1, v23

    invoke-direct {v1, v2}, Lcom/jscape/util/h/p;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object/from16 v16, v1

    move-object/from16 v10, v18

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    move-object/from16 v18, v7

    move-wide/from16 v24, v9

    move-object/from16 v26, v13

    :try_start_3
    new-instance v2, Lcom/jscape/util/h/p;

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v3
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :cond_1
    const-wide/16 v3, 0x0

    :goto_0
    move-object/from16 v10, v18

    :try_start_4
    invoke-static {v10, v3, v4, v1}, Lcom/jscape/util/h/r;->a(Ljava/io/File;JZ)Lcom/jscape/util/h/r;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/jscape/util/h/p;-><init>(Ljava/io/OutputStream;)V

    move-object/from16 v16, v2

    :goto_1
    iget-object v3, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-wide/from16 v5, p3

    move-wide/from16 v7, v24

    move/from16 v9, v17

    invoke-direct/range {v1 .. v9}, Lcom/jscape/filetransfer/AftpFileTransfer;->downloadListener(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZ)Lcom/jscape/inet/a/l;

    move-result-object v7

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v8

    iget-object v1, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    move-object v2, v0

    move-wide/from16 v3, p3

    move-object/from16 v5, v16

    move/from16 v6, v17

    invoke-interface/range {v1 .. v7}, Lcom/jscape/inet/a/k;->a(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/a/l;)V

    invoke-virtual {v14, v10, v15}, Lcom/jscape/filetransfer/AftpFileTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v26

    iget-wide v0, v0, Lcom/jscape/inet/a/a/b/ac;->f:J

    invoke-virtual {v14, v10, v0, v1}, Lcom/jscape/filetransfer/AftpFileTransfer;->safeSetModificationTime(Ljava/io/File;J)V

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Lcom/jscape/util/h/p;->a()J

    move-result-wide v4

    invoke-static {v8, v9}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v7}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseDownloadEvent(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-static/range {v16 .. v16}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    :try_start_5
    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "IB4M0b"

    invoke-static {v0}, Lcom/jscape/filetransfer/FileTransferEvent;->b(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :cond_2
    return-void

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    move-object/from16 v1, p1

    :try_start_7
    invoke-direct {v14, v15, v1}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseDownloadErrorEvent(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v14, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :goto_2
    invoke-static/range {v16 .. v16}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    throw v0
.end method

.method public resumeUpload(Ljava/io/File;Ljava/lang/String;J)V
    .locals 26
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v13, p2

    move-wide/from16 v11, p3

    const/16 v16, 0x0

    :try_start_0
    invoke-direct {v14, v13}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v1

    sub-long v9, v1, v11

    iget-object v1, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->compressionDecisionService:Lcom/jscape/filetransfer/CompressionDecisionService;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v9, v10}, Lcom/jscape/filetransfer/CompressionDecisionService;->compressionRequiredFor(Ljava/lang/String;J)Z

    move-result v17
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v17, :cond_0

    :try_start_1
    new-instance v7, Lcom/jscape/util/h/f;

    new-instance v8, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;

    invoke-static {v15, v11, v12}, Lcom/jscape/util/h/i;->a(Ljava/io/File;J)Lcom/jscape/util/h/i;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-wide/32 v18, 0x19000

    const/16 v20, 0x0

    move-object v1, v8

    move-object/from16 v2, p0

    move-object/from16 v21, v7

    move-object/from16 v22, v8

    move-wide/from16 v7, p3

    move-wide/from16 v23, v9

    move-wide v14, v11

    move-wide/from16 v11, v18

    move-object v14, v13

    move-object/from16 v13, v20

    :try_start_2
    invoke-direct/range {v1 .. v13}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;-><init>(Lcom/jscape/filetransfer/AftpFileTransfer;Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLcom/jscape/filetransfer/AftpFileTransfer$1;)V

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct {v1, v2}, Lcom/jscape/util/h/f;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object/from16 v10, p1

    move-wide/from16 v11, p3

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v14, v13

    :goto_0
    :try_start_3
    invoke-static {v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    move-wide/from16 v23, v9

    move-object v14, v13

    new-instance v1, Lcom/jscape/util/h/f;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object/from16 v10, p1

    move-wide/from16 v11, p3

    :try_start_4
    invoke-static {v10, v11, v12}, Lcom/jscape/util/h/i;->a(Ljava/io/File;J)Lcom/jscape/util/h/i;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/jscape/util/h/f;-><init>(Ljava/io/InputStream;)V

    :goto_1
    move-object/from16 v16, v1

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object/from16 v13, p0

    :try_start_5
    iget-object v3, v13, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v1, p0

    move-wide/from16 v5, p3

    move-wide/from16 v7, v23

    move/from16 v9, v17

    invoke-direct/range {v1 .. v9}, Lcom/jscape/filetransfer/AftpFileTransfer;->uploadListener(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZ)Lcom/jscape/inet/a/l;

    move-result-object v7

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v8

    iget-object v1, v13, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    move-object/from16 v2, v16

    move-object v3, v0

    move-wide/from16 v4, p3

    move/from16 v6, v17

    invoke-interface/range {v1 .. v7}, Lcom/jscape/inet/a/k;->a(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/a/l;)V

    invoke-virtual/range {p0 .. p2}, Lcom/jscape/filetransfer/AftpFileTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p2}, Lcom/jscape/filetransfer/AftpFileTransfer;->safeSetModificationTime(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {v16 .. v16}, Lcom/jscape/util/h/f;->a()J

    move-result-wide v4

    invoke-static {v8, v9}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseUploadEvent(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    invoke-static/range {v16 .. v16}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    return-void

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    move-object/from16 v13, p0

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object/from16 v13, p0

    goto :goto_3

    :catch_4
    move-exception v0

    move-object/from16 v13, p0

    move-object/from16 v10, p1

    goto :goto_2

    :catchall_1
    move-exception v0

    move-object v13, v14

    goto :goto_3

    :catch_5
    move-exception v0

    move-object v10, v15

    move-object/from16 v25, v14

    move-object v14, v13

    move-object/from16 v13, v25

    :goto_2
    :try_start_6
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v13, v1, v14}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseUploadErrorEvent(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v13, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :catchall_2
    move-exception v0

    :goto_3
    invoke-static/range {v16 .. v16}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    throw v0
.end method

.method public resumeUpload(Ljava/io/InputStream;JLjava/lang/String;J)V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    move-object v10, p0

    move-object/from16 v11, p4

    :try_start_0
    invoke-direct {p0, v11}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v12, Lcom/jscape/util/h/f;

    move-object v13, p1

    invoke-direct {v12, p1}, Lcom/jscape/util/h/f;-><init>(Ljava/io/InputStream;)V

    iget-object v3, v10, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    const-string v4, ""

    const/4 v9, 0x0

    move-object v1, p0

    move-object/from16 v2, p4

    move-wide/from16 v5, p5

    move-wide/from16 v7, p2

    invoke-direct/range {v1 .. v9}, Lcom/jscape/filetransfer/AftpFileTransfer;->uploadListener(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZ)Lcom/jscape/inet/a/l;

    move-result-object v7

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v8

    iget-object v1, v10, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    const/4 v6, 0x0

    move-object v2, p1

    move-object v3, v0

    move-wide/from16 v4, p5

    invoke-interface/range {v1 .. v7}, Lcom/jscape/inet/a/k;->a(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/a/l;)V

    const-string v2, ""

    invoke-virtual {v12}, Lcom/jscape/util/h/f;->a()J

    move-result-wide v4

    invoke-static {v8, v9}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object v1, p0

    move-object/from16 v3, p4

    invoke-direct/range {v1 .. v7}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseUploadEvent(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    const-string v1, ""

    invoke-direct {p0, v1, v11}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseUploadErrorEvent(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
.end method

.method public sameChecksum(Ljava/io/File;Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/jscape/a/h;->d()Lcom/jscape/a/h;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/jscape/a/e;->a(Ljava/io/File;Lcom/jscape/a/d;)[B

    move-result-object p1

    invoke-direct {p0, p2}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    invoke-interface {v0, p2}, Lcom/jscape/inet/a/k;->b(Ljava/lang/String;)[B

    move-result-object p2

    invoke-static {p1, p2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public setAuthToken(Ljava/lang/String;)Lcom/jscape/filetransfer/AftpFileTransfer;
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->authToken:Ljava/lang/String;

    return-object p0
.end method

.method public varargs setCompressionDecision(Ljava/lang/Long;[Ljava/lang/String;)Lcom/jscape/filetransfer/AftpFileTransfer;
    .locals 1

    new-instance v0, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;

    invoke-direct {v0, p1, p2}, Lcom/jscape/filetransfer/SizeAndExtensionDecisionService;-><init>(Ljava/lang/Long;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->compressionDecisionService:Lcom/jscape/filetransfer/CompressionDecisionService;

    return-object p0
.end method

.method public setDataProtectionAlgorithm(Ljava/lang/String;)Lcom/jscape/filetransfer/AftpFileTransfer;
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->dataProtectionAlgorithm:Ljava/lang/String;

    return-object p0
.end method

.method public setDataProtectionKeyLength(I)Lcom/jscape/filetransfer/AftpFileTransfer;
    .locals 0

    iput p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->dataProtectionKeyLength:I

    return-object p0
.end method

.method public setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    invoke-interface {v2, v1}, Lcom/jscape/inet/a/k;->a(Ljava/lang/String;)Lcom/jscape/inet/a/a/b/ac;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {v2}, Lcom/jscape/inet/a/a/b/ac;->a()Z

    move-result v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v0, :cond_0

    :try_start_2
    iput-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    new-instance p1, Lcom/jscape/filetransfer/FileTransferChangeDirEvent;

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-direct {p1, p0, v0}, Lcom/jscape/filetransfer/FileTransferChangeDirEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :cond_0
    :try_start_3
    new-instance v0, Ljava/lang/Exception;

    sget-object v1, Lcom/jscape/filetransfer/AftpFileTransfer;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :cond_1
    :goto_0
    return-object p0

    :catch_1
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public setDirUp()Lcom/jscape/filetransfer/FileTransfer;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->stripTrailingSeparator(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    iput-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    new-instance v0, Lcom/jscape/filetransfer/FileTransferChangeDirEvent;

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/jscape/filetransfer/FileTransferChangeDirEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-object p0
.end method

.method public setDownloadRate(J)Lcom/jscape/filetransfer/AftpFileTransfer;
    .locals 0

    invoke-static {p1, p2}, Lcom/jscape/util/A;->a(J)Lcom/jscape/util/A;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->downloadRate:Lcom/jscape/util/A;

    return-object p0
.end method

.method public setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-interface {v0, p1, v1, v2}, Lcom/jscape/inet/a/k;->a(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public setSecurityMode(Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;)Lcom/jscape/filetransfer/AftpFileTransfer;
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->securityMode:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    return-object p0
.end method

.method public setSslKeyStore(Ljava/security/KeyStore;)Lcom/jscape/filetransfer/AftpFileTransfer;
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->sslKeyStore:Ljava/security/KeyStore;

    return-object p0
.end method

.method public setUploadRate(J)Lcom/jscape/filetransfer/AftpFileTransfer;
    .locals 0

    invoke-static {p1, p2}, Lcom/jscape/util/A;->a(J)Lcom/jscape/util/A;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->uploadRate:Lcom/jscape/util/A;

    return-object p0
.end method

.method public setUseCongestionControl(Z)Lcom/jscape/filetransfer/AftpFileTransfer;
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->useCongestionControl:Z

    return-object p0
.end method

.method protected sslContext()Ljavax/net/ssl/SSLContext;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->sslKeyStore:Ljava/security/KeyStore;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->sslKeyStore:Ljava/security/KeyStore;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/jscape/util/k/c/SslContextFactory;->defaultContext()Ljavax/net/ssl/SSLContext;

    move-result-object v0

    goto :goto_1

    :cond_1
    :goto_0
    const-string v0, ""

    invoke-static {v1, v0}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor(Ljava/security/KeyStore;Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method protected startService()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/jscape/inet/a/k;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->securityMode:Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    iget-object v2, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->username:Ljava/lang/String;

    iget-object v3, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->password:Ljava/lang/String;

    iget-object v4, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->authToken:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/jscape/filetransfer/AftpFileTransfer$SecurityMode;->apply(Lcom/jscape/inet/a/k;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public suppressCompressionDecision()V
    .locals 1

    sget-object v0, Lcom/jscape/filetransfer/CompressionDecisionService;->NEVER:Lcom/jscape/filetransfer/CompressionDecisionService;

    iput-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->compressionDecisionService:Lcom/jscape/filetransfer/CompressionDecisionService;

    return-void
.end method

.method public upload(Ljava/io/File;Ljava/lang/String;Z)V
    .locals 27
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v13, p2

    const/16 v16, 0x0

    :try_start_0
    invoke-direct {v14, v13}, Lcom/jscape/filetransfer/AftpFileTransfer;->resolveRemotePathFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v1, 0x0

    if-eqz p3, :cond_0

    :try_start_1
    invoke-direct {v14, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->safeFileSizeOf(Ljava/lang/String;)J

    move-result-wide v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-wide/from16 v17, v3

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_2
    invoke-static {v1}, Lcom/jscape/filetransfer/AftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    move-wide/from16 v17, v1

    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v11

    iget-object v3, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->compressionDecisionService:Lcom/jscape/filetransfer/CompressionDecisionService;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v11, v12}, Lcom/jscape/filetransfer/CompressionDecisionService;->compressionRequiredFor(Ljava/lang/String;J)Z

    move-result v19
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v19, :cond_1

    :try_start_3
    new-instance v9, Lcom/jscape/util/h/f;

    new-instance v10, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;

    invoke-static {v15, v1, v2}, Lcom/jscape/util/h/i;->a(Ljava/io/File;J)Lcom/jscape/util/h/i;

    move-result-object v3

    iget-object v5, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    const-wide/32 v20, 0x19000

    const/16 v22, 0x0

    move-object v1, v10

    move-object/from16 v2, p0

    move-object/from16 v4, p2

    move-wide/from16 v7, v17

    move-object/from16 v23, v9

    move-object/from16 v24, v10

    move-wide v9, v11

    move-wide/from16 v25, v11

    move-wide/from16 v11, v20

    move-object/from16 v13, v22

    invoke-direct/range {v1 .. v13}, Lcom/jscape/filetransfer/AftpFileTransfer$ProgressInputStream;-><init>(Lcom/jscape/filetransfer/AftpFileTransfer;Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLcom/jscape/filetransfer/AftpFileTransfer$1;)V

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-direct {v1, v2}, Lcom/jscape/util/h/f;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object/from16 v16, v1

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    move-wide/from16 v25, v11

    new-instance v3, Lcom/jscape/util/h/f;

    invoke-static {v15, v1, v2}, Lcom/jscape/util/h/i;->a(Ljava/io/File;J)Lcom/jscape/util/h/i;

    move-result-object v1

    invoke-direct {v3, v1}, Lcom/jscape/util/h/f;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v16, v3

    :goto_1
    iget-object v3, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-wide/from16 v5, v17

    move-wide/from16 v7, v25

    move/from16 v9, v19

    invoke-direct/range {v1 .. v9}, Lcom/jscape/filetransfer/AftpFileTransfer;->uploadListener(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZ)Lcom/jscape/inet/a/l;

    move-result-object v7

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v8

    iget-object v1, v14, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    move-object/from16 v2, v16

    move-object v3, v0

    move-wide/from16 v4, v17

    move/from16 v6, v19

    invoke-interface/range {v1 .. v7}, Lcom/jscape/inet/a/k;->a(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/a/l;)V

    invoke-virtual/range {p0 .. p2}, Lcom/jscape/filetransfer/AftpFileTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p2}, Lcom/jscape/filetransfer/AftpFileTransfer;->safeSetModificationTime(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {v16 .. v16}, Lcom/jscape/util/h/f;->a()J

    move-result-wide v4

    invoke-static {v8, v9}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseUploadEvent(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-static/range {v16 .. v16}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    :try_start_5
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v2, p2

    invoke-direct {v14, v1, v2}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseUploadErrorEvent(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v14, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :goto_2
    invoke-static/range {v16 .. v16}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    throw v0
.end method

.method public uploadDir(Ljava/io/File;IZLjava/lang/String;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    new-instance v7, Lcom/jscape/filetransfer/UploadDirectoryOperation;

    sget-object v1, Lcom/jscape/filetransfer/FileTransferOperation;->NULL:Lcom/jscape/filetransfer/FileTransferOperation;

    const/4 v0, 0x1

    add-int/2addr p2, v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    sget-object v5, Lcom/jscape/filetransfer/AftpFileTransfer;->ATTEMPT_PERIOD:Lcom/jscape/util/Time;

    if-lez p5, :cond_0

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    move-object v6, p2

    move-object v0, v7

    move-object v2, p1

    move-object v3, p4

    invoke-direct/range {v0 .. v6}, Lcom/jscape/filetransfer/UploadDirectoryOperation;-><init>(Lcom/jscape/filetransfer/FileTransferOperation;Ljava/io/File;Ljava/lang/String;ILcom/jscape/util/Time;Ljava/lang/Integer;)V

    iput-object v7, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->uploadDirectoryOperation:Lcom/jscape/filetransfer/UploadDirectoryOperation;

    iget-boolean p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->checksumVerificationRequired:Z

    invoke-virtual {p0, p3}, Lcom/jscape/filetransfer/AftpFileTransfer;->setChecksumVerificationRequired(Z)Lcom/jscape/filetransfer/FileTransfer;

    iget-object p2, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->uploadDirectoryOperation:Lcom/jscape/filetransfer/UploadDirectoryOperation;

    invoke-virtual {p2, p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/UploadDirectoryOperation;

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->setChecksumVerificationRequired(Z)Lcom/jscape/filetransfer/FileTransfer;

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public uploadUnique(Ljava/io/File;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/jscape/inet/a/k;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/jscape/filetransfer/AftpFileTransfer;->upload(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public uploadUnique(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->service:Lcom/jscape/inet/a/k;

    iget-object v1, p0, Lcom/jscape/filetransfer/AftpFileTransfer;->currentRemoteDirectory:Ljava/lang/String;

    invoke-interface {v0, v1, p2}, Lcom/jscape/inet/a/k;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v0

    int-to-long v4, v0

    const/4 v7, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-virtual/range {v2 .. v7}, Lcom/jscape/filetransfer/AftpFileTransfer;->upload(Ljava/io/InputStream;JLjava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p2

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/AftpFileTransfer;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method
