.class final enum Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy$3;
.super Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;-><init>(Ljava/lang/String;ILcom/jscape/filetransfer/DownloadFileOperation$1;)V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/jscape/filetransfer/DownloadFileOperation;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadFileOperation;->d(Lcom/jscape/filetransfer/DownloadFileOperation;)I

    move-result v1

    if-lez v1, :cond_0

    sget-object v1, Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy$3;->ALWAYS_RESUME:Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;

    invoke-virtual {v1, p1}, Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;->apply(Lcom/jscape/filetransfer/DownloadFileOperation;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    :cond_0
    :try_start_1
    sget-object v0, Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy$3;->FROM_SCRATCH:Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;

    invoke-virtual {v0, p1}, Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;->apply(Lcom/jscape/filetransfer/DownloadFileOperation;)V

    :cond_1
    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy$3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy$3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method
