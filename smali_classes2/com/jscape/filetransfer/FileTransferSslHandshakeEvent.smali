.class public Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;
.super Lcom/jscape/filetransfer/FileTransferEvent;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private final a:Ljavax/net/ssl/HandshakeCompletedEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "\u00054lf\r~T-.ff+_F/\u0015am=\u007f]\"6eF/i[7}{a8\u007fP\u0006+em-1"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;->c:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v5, 0x20

    if-eqz v4, :cond_6

    const/4 v6, 0x1

    if-eq v4, v6, :cond_5

    const/4 v6, 0x2

    if-eq v4, v6, :cond_4

    const/4 v6, 0x3

    if-eq v4, v6, :cond_3

    const/4 v6, 0x4

    if-eq v4, v6, :cond_2

    const/4 v6, 0x5

    if-eq v4, v6, :cond_1

    const/16 v4, 0x15

    goto :goto_1

    :cond_1
    const/16 v4, 0x2c

    goto :goto_1

    :cond_2
    const/16 v4, 0x79

    goto :goto_1

    :cond_3
    const/16 v4, 0x23

    goto :goto_1

    :cond_4
    move v4, v5

    goto :goto_1

    :cond_5
    const/16 v4, 0x7d

    goto :goto_1

    :cond_6
    const/16 v4, 0x63

    :goto_1
    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransfer;Ljavax/net/ssl/HandshakeCompletedEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/FileTransferEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;->a:Ljavax/net/ssl/HandshakeCompletedEvent;

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/filetransfer/FileTransferListener;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/jscape/filetransfer/FileTransferListener;->sslHandshakeCompleted(Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;)V

    return-void
.end method

.method public getCipherSuite()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;->a:Ljavax/net/ssl/HandshakeCompletedEvent;

    invoke-virtual {v0}, Ljavax/net/ssl/HandshakeCompletedEvent;->getCipherSuite()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocalCertificates()[Ljava/security/cert/Certificate;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;->a:Ljavax/net/ssl/HandshakeCompletedEvent;

    invoke-virtual {v0}, Ljavax/net/ssl/HandshakeCompletedEvent;->getLocalCertificates()[Ljava/security/cert/Certificate;

    move-result-object v0

    return-object v0
.end method

.method public getLocalPrincipal()Ljava/security/Principal;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;->a:Ljavax/net/ssl/HandshakeCompletedEvent;

    invoke-virtual {v0}, Ljavax/net/ssl/HandshakeCompletedEvent;->getLocalPrincipal()Ljava/security/Principal;

    move-result-object v0

    return-object v0
.end method

.method public getPeerCertificateChain()[Ljavax/security/cert/X509Certificate;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;->a:Ljavax/net/ssl/HandshakeCompletedEvent;

    invoke-virtual {v0}, Ljavax/net/ssl/HandshakeCompletedEvent;->getPeerCertificateChain()[Ljavax/security/cert/X509Certificate;

    move-result-object v0
    :try_end_0
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPeerCertificates()[Ljava/security/cert/Certificate;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;->a:Ljavax/net/ssl/HandshakeCompletedEvent;

    invoke-virtual {v0}, Ljavax/net/ssl/HandshakeCompletedEvent;->getPeerCertificates()[Ljava/security/cert/Certificate;

    move-result-object v0
    :try_end_0
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPeerPrincipal()Ljava/security/Principal;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;->a:Ljavax/net/ssl/HandshakeCompletedEvent;

    invoke-virtual {v0}, Ljavax/net/ssl/HandshakeCompletedEvent;->getPeerPrincipal()Ljava/security/Principal;

    move-result-object v0
    :try_end_0
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSession()Ljavax/net/ssl/SSLSession;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;->a:Ljavax/net/ssl/HandshakeCompletedEvent;

    invoke-virtual {v0}, Ljavax/net/ssl/HandshakeCompletedEvent;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;->a:Ljavax/net/ssl/HandshakeCompletedEvent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
