.class public Lcom/jscape/filetransfer/FileTransferRemoteSort$FileNameComparator;
.super Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    check-cast p1, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    check-cast p2, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilename()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilename()Ljava/lang/String;

    move-result-object p2

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    move-object v1, p2

    goto :goto_0

    :cond_0
    move-object v1, p1

    :goto_0
    if-eqz v0, :cond_2

    if-nez v1, :cond_3

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    move-object p1, v1

    :cond_3
    invoke-virtual {p1, p2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result p1

    :goto_1
    iget-boolean p2, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort$FileNameComparator;->isAscendent:Z

    if-eqz v0, :cond_5

    if-nez p2, :cond_4

    neg-int p1, p1

    :cond_4
    move p2, p1

    :cond_5
    return p2
.end method
