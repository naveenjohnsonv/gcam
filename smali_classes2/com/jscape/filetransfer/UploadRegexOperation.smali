.class public Lcom/jscape/filetransfer/UploadRegexOperation;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransferOperation;
.implements Lcom/jscape/filetransfer/UploadFileOperation$Listener;


# static fields
.field private static final r:[Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/filetransfer/FileTransferOperation;

.field private final b:Ljava/io/File;

.field private final c:Lcom/jscape/util/m/f;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Z

.field private final g:I

.field private final h:Lcom/jscape/util/Time;

.field private final i:I

.field private final j:Z

.field private k:Lcom/jscape/filetransfer/FileTransfer;

.field private l:Z

.field private m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/io/File;",
            "Lcom/jscape/filetransfer/UploadFileOperation;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/concurrent/ExecutorService;

.field private o:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Lcom/jscape/filetransfer/FileTransfer;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/concurrent/CountDownLatch;

.field private volatile q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x10

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "rhr\u001aFO+7$c:SK\u00080;o\u0010E\u0002J\u0010rhg\u000b_Z\u0000.<V\u001aYV\u0002:u\u000crhe\u001eE\\\u00082$c\u001b\u0016*\u0010\'&\u0019BS\u0008-hk\u001e_\\\u0005;,&\rNX\u00182)t_NG\u001d,-u\u000cBP\u0003~.i\nE[C\u001brhb\u001aGZ\u0019;\u0004i\u001cJS+7$c\u000cyZ\u001c+!t\u001aO\u0002\u0013rht\u001aFP\u0019;\u000co\rN\\\u00191:\u007fB\u000c+\u000b8j\u0010J[?;/c\u0007dO\u0008,)r\u0016DQM%8i\u000c_|\u00020&c\u001c_p\u001d;:g\u000bBP\u0003c\u0017\u001c)b_F^\u0015~)r\u000bNR\u001d*;&\tJS\u0018;f\u0017\u001c)b__W\u001f;)b_HP\u00180<&\tJS\u0018;f\u001a\u001b:t\u0010Y\u001f\u0002<<g\u0016EV\u00039h`\u0016GZM2!u\u000b\u0005\u0015rh`\u001eBS\"0\rk\u000f_F+7$c,NKP&\r8c\u001cBY\u0004;,&\u000fJK\u00050)k\u001a\u000bV\u001e~&i\u000b\u000b^M:!t\u001aHK\u0002,1(\u000erhk\u001eS~\u0019*-k\u000f_LP\u000erhr\u0017YZ\u000c:\u000bi\nEKP"

    const/16 v5, 0x160

    const/16 v6, 0x15

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x16

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x1c

    const/16 v4, 0xa

    const-string v6, "\u0015\u000f\u000cy8;b\\]\\\u0011\u0015\u000f\rw/9f}F\u0013}/,eKV\\"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v16, v6

    move v6, v4

    move-object/from16 v4, v16

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x71

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/filetransfer/UploadRegexOperation;->r:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v0, v14, 0x7

    if-eqz v0, :cond_9

    if-eq v0, v10, :cond_8

    const/4 v2, 0x2

    if-eq v0, v2, :cond_7

    const/4 v2, 0x3

    if-eq v0, v2, :cond_6

    const/4 v2, 0x4

    if-eq v0, v2, :cond_5

    const/4 v2, 0x5

    if-eq v0, v2, :cond_4

    const/16 v0, 0x7b

    goto :goto_4

    :cond_4
    const/16 v0, 0x29

    goto :goto_4

    :cond_5
    const/16 v0, 0x3d

    goto :goto_4

    :cond_6
    const/16 v0, 0x69

    goto :goto_4

    :cond_7
    const/16 v0, 0x10

    goto :goto_4

    :cond_8
    const/16 v0, 0x5e

    goto :goto_4

    :cond_9
    const/16 v0, 0x48

    :goto_4
    xor-int/2addr v0, v9

    xor-int/2addr v0, v15

    int-to-char v0, v0

    aput-char v0, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/16 v0, 0x10

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransferOperation;Ljava/io/File;Lcom/jscape/util/m/f;Ljava/lang/String;Ljava/lang/String;ZILcom/jscape/util/Time;Ljava/lang/Integer;Z)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z

    move-result p1

    sget-object v1, Lcom/jscape/filetransfer/UploadRegexOperation;->r:[Ljava/lang/String;

    const/16 v2, 0xb

    aget-object v2, v1, v2

    invoke-static {p1, v2}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    iput-object p2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->b:Ljava/io/File;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->c:Lcom/jscape/util/m/f;

    iput-object p4, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->e:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->f:Z

    const/4 p1, 0x0

    if-eqz v0, :cond_2

    int-to-long p2, p7

    const-wide/16 p4, 0x0

    const/4 p6, 0x7

    aget-object p6, v1, p6

    invoke-static {p2, p3, p4, p5, p6}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p7, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->g:I

    invoke-static {p8}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p8, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->h:Lcom/jscape/util/Time;

    if-eqz p9, :cond_1

    invoke-virtual {p9}, Ljava/lang/Integer;->intValue()I

    move-result p7

    if-eqz v0, :cond_2

    if-ltz p7, :cond_0

    goto :goto_0

    :cond_0
    move p7, p1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p7, 0x1

    :cond_2
    :goto_1
    sget-object p2, Lcom/jscape/filetransfer/UploadRegexOperation;->r:[Ljava/lang/String;

    const/16 p3, 0x8

    aget-object p2, p2, p3

    invoke-static {p7, p2}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    if-eqz v0, :cond_3

    if-eqz p9, :cond_4

    :cond_3
    invoke-virtual {p9}, Ljava/lang/Integer;->intValue()I

    move-result p1

    :cond_4
    iput p1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->i:I

    iput-boolean p10, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->j:Z

    return-void
.end method

.method private a(Ljava/io/File;)Lcom/jscape/filetransfer/UploadFileOperation;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/filetransfer/UploadFileOperation;

    return-object p1
.end method

.method private a(Ljava/io/File;Lcom/jscape/filetransfer/RemoteDirectory;)Lcom/jscape/filetransfer/UploadFileOperation;
    .locals 11

    new-instance v10, Lcom/jscape/filetransfer/UploadFileOperation;

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    iget-object v4, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->e:Ljava/lang/String;

    sget-object v5, Lcom/jscape/filetransfer/UploadFileOperation$TransferStrategy;->RESUME_AFTER_FIRST_ATTEMPT:Lcom/jscape/filetransfer/UploadFileOperation$TransferStrategy;

    iget v6, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->g:I

    iget-object v7, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->h:Lcom/jscape/util/Time;

    iget-boolean v8, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->j:Z

    move-object v0, v10

    move-object v2, p1

    move-object v3, p2

    move-object v9, p0

    invoke-direct/range {v0 .. v9}, Lcom/jscape/filetransfer/UploadFileOperation;-><init>(Lcom/jscape/filetransfer/FileTransferOperation;Ljava/io/File;Lcom/jscape/filetransfer/RemoteDirectory;Ljava/lang/String;Lcom/jscape/filetransfer/UploadFileOperation$TransferStrategy;ILcom/jscape/util/Time;ZLcom/jscape/filetransfer/UploadFileOperation$Listener;)V

    return-object v10
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    if-eqz v0, :cond_0

    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->isConnected()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->l:Z

    iget-object v0, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->connect()Lcom/jscape/filetransfer/FileTransfer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0, v1}, Lcom/jscape/filetransfer/FileTransferOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    :cond_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private a(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {v0, p1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->o:Ljava/util/concurrent/BlockingQueue;

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->o:Ljava/util/concurrent/BlockingQueue;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v1, v2}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    :cond_0
    if-ge v1, p1, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v2}, Lcom/jscape/filetransfer/FileTransfer;->copy()Lcom/jscape/filetransfer/FileTransfer;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-direct {p0, v2}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Lcom/jscape/filetransfer/FileTransfer;)V

    iget-object v3, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->o:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v3, v2}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_1

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadRegexOperation;->f()V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method private a(Lcom/jscape/filetransfer/FileTransfer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-interface {p1}, Lcom/jscape/filetransfer/FileTransfer;->connect()Lcom/jscape/filetransfer/FileTransfer;

    iget-object v0, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-interface {v0, p1}, Lcom/jscape/filetransfer/FileTransferOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    return-void
.end method

.method private a(Lcom/jscape/filetransfer/UploadFileOperation;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-virtual {p1, v0}, Lcom/jscape/filetransfer/UploadFileOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/UploadFileOperation;
    :try_end_0
    .catch Lcom/jscape/filetransfer/TransferOperationCancelledException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private b()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->m:Ljava/util/Map;

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadRegexOperation;->k()Lcom/jscape/filetransfer/RemoteDirectory;

    move-result-object v1

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadRegexOperation;->l()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x0

    :cond_0
    if-ge v4, v3, :cond_1

    aget-object v5, v2, v4

    invoke-direct {p0, v5, v1}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/io/File;Lcom/jscape/filetransfer/RemoteDirectory;)Lcom/jscape/filetransfer/UploadFileOperation;

    move-result-object v6

    iget-object v7, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->m:Ljava/util/Map;

    invoke-interface {v7, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private c()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->p:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method private d()Z
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->i:I

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    if-le v1, v2, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method private e()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget v0, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->i:I

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->n:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v0}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(I)V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/FileTransferOperation;

    iget-object v3, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->n:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;

    iget-object v5, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->o:Ljava/util/concurrent/BlockingQueue;

    invoke-direct {v4, v2, v5}, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;-><init>(Lcom/jscape/filetransfer/FileTransferOperation;Ljava/util/concurrent/BlockingQueue;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private f()V
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->o:Ljava/util/concurrent/BlockingQueue;

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    move-object v2, p0

    goto :goto_1

    :cond_1
    move-object v2, p0

    :goto_0
    check-cast v1, Lcom/jscape/filetransfer/FileTransfer;

    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->close()V

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    :goto_1
    iget-object v1, v2, Lcom/jscape/filetransfer/UploadRegexOperation;->o:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :cond_3
    :goto_2
    return-void
.end method

.method private g()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->p:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadRegexOperation;->f()V

    throw v0

    :catch_0
    :goto_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadRegexOperation;->f()V

    return-void
.end method

.method private h()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/TransferOperationCancelledException;
        }
    .end annotation

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->q:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/filetransfer/TransferOperationCancelledException;

    invoke-direct {v0}, Lcom/jscape/filetransfer/TransferOperationCancelledException;-><init>()V

    throw v0
    :try_end_0
    .catch Lcom/jscape/filetransfer/TransferOperationCancelledException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private i()V
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->n:Ljava/util/concurrent/ExecutorService;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    iput-object v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->n:Ljava/util/concurrent/ExecutorService;

    :cond_0
    iput-object v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->p:Ljava/util/concurrent/CountDownLatch;

    :cond_1
    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->l:Z

    if-eqz v0, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadRegexOperation;->j()V

    :cond_3
    return-void
.end method

.method private j()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private k()Lcom/jscape/filetransfer/RemoteDirectory;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v2}, Lcom/jscape/filetransfer/FileTransfer;->getDir()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :try_start_0
    iget-object v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->d:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v1, :cond_0

    if-eqz v2, :cond_1

    :try_start_1
    iget-object v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->d:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    if-eqz v1, :cond_1

    if-nez v2, :cond_1

    :try_start_3
    iget-object v1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :cond_1
    new-instance v1, Lcom/jscape/filetransfer/RemoteDirectory$PathElementsDirectory;

    invoke-direct {v1, v0}, Lcom/jscape/filetransfer/RemoteDirectory$PathElementsDirectory;-><init>(Ljava/util/List;)V

    return-object v1

    :catch_0
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private l()[Ljava/io/File;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->b:Ljava/io/File;

    new-instance v2, Lcom/jscape/util/m/c;

    iget-object v3, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->c:Lcom/jscape/util/m/f;

    invoke-direct {v2, v3}, Lcom/jscape/util/m/c;-><init>(Lcom/jscape/util/m/f;)V

    invoke-virtual {v1, v2}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_3

    :try_start_0
    iget-boolean v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->f:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    :try_start_1
    array-length v2, v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    if-eqz v2, :cond_2

    :cond_1
    return-object v1

    :cond_2
    :try_start_2
    new-instance v0, Ljava/lang/Exception;

    sget-object v1, Lcom/jscape/filetransfer/UploadRegexOperation;->r:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_3
    :try_start_5
    new-instance v0, Ljava/lang/Exception;

    sget-object v1, Lcom/jscape/filetransfer/UploadRegexOperation;->r:[Ljava/lang/String;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private m()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/UploadFileOperation;

    invoke-direct {p0, v2}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Lcom/jscape/filetransfer/UploadFileOperation;)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method


# virtual methods
.method public applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iput-object p1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->q:Z

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadRegexOperation;->a()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadRegexOperation;->b()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadRegexOperation;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadRegexOperation;->d()Z

    move-result p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p1, :cond_0

    :try_start_2
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadRegexOperation;->e()V

    if-nez v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadRegexOperation;->m()V

    :cond_1
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadRegexOperation;->g()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadRegexOperation;->h()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadRegexOperation;->i()V

    return-object p0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_1
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadRegexOperation;->i()V

    throw p1
.end method

.method public cancel()V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->q:Z

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/UploadFileOperation;

    invoke-virtual {v2}, Lcom/jscape/filetransfer/UploadFileOperation;->cancel()V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public cancel(Ljava/io/File;)V
    .locals 1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/UploadRegexOperation;->a(Ljava/io/File;)Lcom/jscape/filetransfer/UploadFileOperation;

    move-result-object p1

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/jscape/filetransfer/UploadFileOperation;->cancel()V

    :cond_1
    return-void
.end method

.method public fileCount()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public onOperationCompleted(Lcom/jscape/filetransfer/UploadFileOperation;)V
    .locals 0

    iget-object p1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->p:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/UploadRegexOperation;->r:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0xf

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->b:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0xe

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->c:Lcom/jscape/util/m/f;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v2, 0xa

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->f:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v2, 0xc

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->h:Lcom/jscape/util/Time;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0xd

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->i:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->j:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/filetransfer/UploadRegexOperation;->q:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
