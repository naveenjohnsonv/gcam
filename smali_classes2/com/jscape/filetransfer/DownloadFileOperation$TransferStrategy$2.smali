.class final enum Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy$2;
.super Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;-><init>(Ljava/lang/String;ILcom/jscape/filetransfer/DownloadFileOperation$1;)V

    return-void
.end method


# virtual methods
.method public apply(Lcom/jscape/filetransfer/DownloadFileOperation;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadFileOperation;->a(Lcom/jscape/filetransfer/DownloadFileOperation;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadFileOperation;->c(Lcom/jscape/filetransfer/DownloadFileOperation;)Lcom/jscape/filetransfer/FileTransfer;

    move-result-object v2

    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadFileOperation;->b(Lcom/jscape/filetransfer/DownloadFileOperation;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0, v1}, Lcom/jscape/filetransfer/FileTransfer;->resumeDownload(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    sget-object v0, Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy$2;->FROM_SCRATCH:Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;

    invoke-virtual {v0, p1}, Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;->apply(Lcom/jscape/filetransfer/DownloadFileOperation;)V

    :goto_0
    return-void
.end method
