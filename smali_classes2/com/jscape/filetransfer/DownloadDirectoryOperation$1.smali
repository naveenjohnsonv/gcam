.class Lcom/jscape/filetransfer/DownloadDirectoryOperation$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lcom/jscape/filetransfer/DownloadDirectoryOperation;


# direct methods
.method constructor <init>(Lcom/jscape/filetransfer/DownloadDirectoryOperation;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation$1;->a:Lcom/jscape/filetransfer/DownloadDirectoryOperation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation$1;->a:Lcom/jscape/filetransfer/DownloadDirectoryOperation;

    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Lcom/jscape/filetransfer/DownloadDirectoryOperation;)V

    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation$1;->a:Lcom/jscape/filetransfer/DownloadDirectoryOperation;

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation$1;->a:Lcom/jscape/filetransfer/DownloadDirectoryOperation;

    invoke-static {v1}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->b(Lcom/jscape/filetransfer/DownloadDirectoryOperation;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation$1;->a:Lcom/jscape/filetransfer/DownloadDirectoryOperation;

    invoke-static {v2}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->c(Lcom/jscape/filetransfer/DownloadDirectoryOperation;)Lcom/jscape/filetransfer/FileTransfer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/jscape/filetransfer/DirectoryService;->asTree(Ljava/lang/String;Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/util/b/s;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Lcom/jscape/filetransfer/DownloadDirectoryOperation;Lcom/jscape/util/b/s;)Lcom/jscape/util/b/s;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    return-object v0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation$1;->a:Lcom/jscape/filetransfer/DownloadDirectoryOperation;

    invoke-static {v1}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->d(Lcom/jscape/filetransfer/DownloadDirectoryOperation;)V

    throw v0
.end method
