.class public abstract enum Lcom/jscape/filetransfer/TransferMode;
.super Ljava/lang/Enum;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransferOperation;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/filetransfer/TransferMode;",
        ">;",
        "Lcom/jscape/filetransfer/FileTransferOperation;"
    }
.end annotation


# static fields
.field public static final enum ASCII:Lcom/jscape/filetransfer/TransferMode;

.field public static final enum AUTO:Lcom/jscape/filetransfer/TransferMode;

.field public static final enum BINARY:Lcom/jscape/filetransfer/TransferMode;

.field private static final a:[Lcom/jscape/filetransfer/TransferMode;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x6

    new-array v1, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0[1\u0002\u0004\u0010{\u0011\"\u0005\u0010}\u0006$=\u00050]&\u0004\u001d"

    const/16 v6, 0x15

    move v9, v4

    const/4 v7, -0x1

    const/4 v8, 0x4

    :goto_0
    const/16 v10, 0x33

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    move v15, v4

    :goto_2
    const/4 v0, 0x5

    const/4 v2, 0x3

    const/4 v3, 0x2

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_1

    add-int/lit8 v0, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v0

    const/4 v0, 0x6

    goto :goto_0

    :cond_0
    const/16 v6, 0xd

    const-string v5, "\ry\u001528t\u0006-Y5\u0012\u0018T"

    move v9, v0

    const/4 v7, -0x1

    const/4 v8, 0x6

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v8, v0

    move v9, v12

    :goto_3
    const/16 v10, 0x2d

    add-int/2addr v7, v11

    add-int v0, v7, v8

    invoke-virtual {v5, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v13, v4

    const/4 v0, 0x6

    goto :goto_1

    :cond_2
    new-instance v5, Lcom/jscape/filetransfer/TransferMode$1;

    aget-object v6, v1, v2

    aget-object v7, v1, v3

    invoke-direct {v5, v6, v4, v7}, Lcom/jscape/filetransfer/TransferMode$1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/jscape/filetransfer/TransferMode;->ASCII:Lcom/jscape/filetransfer/TransferMode;

    new-instance v5, Lcom/jscape/filetransfer/TransferMode$2;

    aget-object v0, v1, v0

    const/4 v6, 0x4

    aget-object v6, v1, v6

    invoke-direct {v5, v0, v11, v6}, Lcom/jscape/filetransfer/TransferMode$2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/jscape/filetransfer/TransferMode;->BINARY:Lcom/jscape/filetransfer/TransferMode;

    new-instance v0, Lcom/jscape/filetransfer/TransferMode$3;

    aget-object v5, v1, v4

    aget-object v1, v1, v11

    invoke-direct {v0, v5, v3, v1}, Lcom/jscape/filetransfer/TransferMode$3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/filetransfer/TransferMode;->AUTO:Lcom/jscape/filetransfer/TransferMode;

    new-array v1, v2, [Lcom/jscape/filetransfer/TransferMode;

    sget-object v2, Lcom/jscape/filetransfer/TransferMode;->ASCII:Lcom/jscape/filetransfer/TransferMode;

    aput-object v2, v1, v4

    sget-object v2, Lcom/jscape/filetransfer/TransferMode;->BINARY:Lcom/jscape/filetransfer/TransferMode;

    aput-object v2, v1, v11

    aput-object v0, v1, v3

    sput-object v1, Lcom/jscape/filetransfer/TransferMode;->a:[Lcom/jscape/filetransfer/TransferMode;

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v4, v15, 0x7

    if-eqz v4, :cond_9

    if-eq v4, v11, :cond_8

    if-eq v4, v3, :cond_7

    if-eq v4, v2, :cond_6

    const/4 v2, 0x4

    if-eq v4, v2, :cond_5

    if-eq v4, v0, :cond_4

    const/16 v0, 0x1d

    goto :goto_4

    :cond_4
    const/16 v0, 0x20

    goto :goto_4

    :cond_5
    const/16 v0, 0x67

    goto :goto_4

    :cond_6
    const/4 v2, 0x4

    const/16 v0, 0x7e

    goto :goto_4

    :cond_7
    const/4 v2, 0x4

    const/16 v0, 0x56

    goto :goto_4

    :cond_8
    const/4 v2, 0x4

    const/16 v0, 0x3d

    goto :goto_4

    :cond_9
    const/4 v2, 0x4

    const/16 v0, 0x42

    :goto_4
    xor-int/2addr v0, v10

    xor-int v0, v16, v0

    int-to-char v0, v0

    aput-char v0, v12, v15

    add-int/lit8 v15, v15, 0x1

    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/jscape/filetransfer/TransferMode;->name:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/jscape/filetransfer/TransferMode$1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/filetransfer/TransferMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static modeFor(Ljava/lang/String;)Lcom/jscape/filetransfer/TransferMode;
    .locals 6

    invoke-static {}, Lcom/jscape/filetransfer/TransferMode;->values()[Lcom/jscape/filetransfer/TransferMode;

    move-result-object v0

    array-length v1, v0

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v1, :cond_3

    aget-object v4, v0, v3

    if-eqz v2, :cond_2

    if-eqz v2, :cond_4

    iget-object v5, v4, Lcom/jscape/filetransfer/TransferMode;->name:Ljava/lang/String;

    invoke-virtual {v5, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    :cond_2
    if-nez v2, :cond_0

    :cond_3
    sget-object v4, Lcom/jscape/filetransfer/TransferMode;->BINARY:Lcom/jscape/filetransfer/TransferMode;

    :cond_4
    return-object v4
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/filetransfer/TransferMode;
    .locals 1

    const-class v0, Lcom/jscape/filetransfer/TransferMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/filetransfer/TransferMode;

    return-object p0
.end method

.method public static values()[Lcom/jscape/filetransfer/TransferMode;
    .locals 1

    sget-object v0, Lcom/jscape/filetransfer/TransferMode;->a:[Lcom/jscape/filetransfer/TransferMode;

    invoke-virtual {v0}, [Lcom/jscape/filetransfer/TransferMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/filetransfer/TransferMode;

    return-object v0
.end method


# virtual methods
.method public abstract applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method
