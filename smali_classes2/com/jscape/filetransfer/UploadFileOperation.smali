.class public Lcom/jscape/filetransfer/UploadFileOperation;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransferOperation;


# static fields
.field private static final r:[Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/filetransfer/FileTransferOperation;

.field private final b:Ljava/io/File;

.field private final c:Lcom/jscape/filetransfer/RemoteDirectory;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/jscape/filetransfer/UploadFileOperation$TransferStrategy;

.field private final f:Z

.field private final g:I

.field private final h:Lcom/jscape/util/Time;

.field private final i:Z

.field private final j:Lcom/jscape/filetransfer/UploadFileOperation$Listener;

.field private k:Lcom/jscape/filetransfer/FileTransfer;

.field private l:Z

.field private m:Ljava/lang/String;

.field private n:I

.field private o:Z

.field private p:Ljava/lang/Exception;

.field private volatile q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "xT{<8nw12`50<$\u001bxT{<;`n1&l4:uf\u0012\u001de<\u0007dr!\u001d{<1<\u0017\u0016\u0015my8`{t\u0015}-0ls \u0007)/4mv1Z\u000cxTz,6bf1\u0010l=h\u000bxTh-!dn$\u0000zd\u0008xTl+\'nqi\u000cxTe66`o\u0012\u001de<h\u0010xTh-!dn$\u0000Y<\'hl0I\u0013xT}+4op2\u0011{\n!sb \u0011n h\u001axTm<9dw18f:4mE=\u0018l\u000b0pv=\u0006l=h*\u0001\u0004e64eE=\u0018l\u0016%dq5\u0000`6;!x$\u001bz-\u0016nm:\u0011j-\u001aqf&\u0015}0:o>\u0012xT{<8nw10`+0bw;\u0006pd!\u0007\u0004l:<gj1\u0010))4uk:\u0015d<uhpt\u001af-u`#2\u001de<{"

    const/16 v4, 0x111

    const/16 v5, 0xe

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x62

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x1b

    const/16 v3, 0xc

    const-string v5, "(\u0004:hk26hH<m8\u000e(\u00044h}\u0010\'pA4yq\"n"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x32

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/filetransfer/UploadFileOperation;->r:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x61

    goto :goto_4

    :cond_4
    const/16 v1, 0x63

    goto :goto_4

    :cond_5
    const/16 v1, 0x37

    goto :goto_4

    :cond_6
    const/16 v1, 0x3b

    goto :goto_4

    :cond_7
    const/16 v1, 0x6b

    goto :goto_4

    :cond_8
    const/16 v1, 0x16

    goto :goto_4

    :cond_9
    const/16 v1, 0x36

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransferOperation;Ljava/io/File;Lcom/jscape/filetransfer/RemoteDirectory;Ljava/lang/String;Lcom/jscape/filetransfer/UploadFileOperation$TransferStrategy;ILcom/jscape/util/Time;ZLcom/jscape/filetransfer/UploadFileOperation$Listener;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iput-object p1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/filetransfer/UploadFileOperation;->c:Lcom/jscape/filetransfer/RemoteDirectory;

    invoke-virtual {p2}, Ljava/io/File;->isFile()Z

    move-result p1

    sget-object p3, Lcom/jscape/filetransfer/UploadFileOperation;->r:[Ljava/lang/String;

    const/16 v1, 0xc

    aget-object p3, p3, v1

    invoke-static {p1, p3}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    iput-object p2, p0, Lcom/jscape/filetransfer/UploadFileOperation;->b:Ljava/io/File;

    invoke-static {p4}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result p1

    const-string p3, "."

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    if-eqz v0, :cond_3

    invoke-virtual {p4, p3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    goto :goto_0

    :cond_0
    const-string p4, ""

    goto :goto_1

    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    goto :goto_1

    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    :cond_3
    :goto_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->d:Ljava/lang/String;

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/filetransfer/UploadFileOperation;->e:Lcom/jscape/filetransfer/UploadFileOperation$TransferStrategy;

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result p1

    if-eqz v0, :cond_5

    if-lez p1, :cond_4

    const/4 p1, 0x1

    goto :goto_2

    :cond_4
    const/4 p1, 0x0

    :cond_5
    :goto_2
    iput-boolean p1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->f:Z

    int-to-long p1, p6

    const-wide/16 p3, 0x0

    sget-object p5, Lcom/jscape/filetransfer/UploadFileOperation;->r:[Ljava/lang/String;

    const/4 v0, 0x2

    aget-object p5, p5, v0

    invoke-static {p1, p2, p3, p4, p5}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p6, p0, Lcom/jscape/filetransfer/UploadFileOperation;->g:I

    invoke-static {p7}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p7, p0, Lcom/jscape/filetransfer/UploadFileOperation;->h:Lcom/jscape/util/Time;

    iput-boolean p8, p0, Lcom/jscape/filetransfer/UploadFileOperation;->i:Z

    invoke-static {p9}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p9, p0, Lcom/jscape/filetransfer/UploadFileOperation;->j:Lcom/jscape/filetransfer/UploadFileOperation$Listener;

    return-void
.end method

.method static a(Lcom/jscape/filetransfer/UploadFileOperation;)Ljava/io/File;
    .locals 0

    iget-object p0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->b:Ljava/io/File;

    return-object p0
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->p:Ljava/lang/Exception;

    iget p1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->n:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->n:I

    return-void
.end method

.method private a()Z
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->o:Z

    if-eqz v0, :cond_0

    if-nez v1, :cond_2

    iget v1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->n:I

    :cond_0
    if-eqz v0, :cond_1

    iget v2, p0, Lcom/jscape/filetransfer/UploadFileOperation;->g:I

    if-ge v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->q:Z

    :cond_1
    if-eqz v0, :cond_3

    if-nez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method private static b(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method static b(Lcom/jscape/filetransfer/UploadFileOperation;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->d:Ljava/lang/String;

    return-object p0
.end method

.method private b()V
    .locals 1

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadFileOperation;->e()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadFileOperation;->f()V

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadFileOperation;->g()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadFileOperation;->h()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadFileOperation;->i()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadFileOperation;->j()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadFileOperation;->k()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadFileOperation;->l()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/jscape/filetransfer/UploadFileOperation;->a(Ljava/lang/Exception;)V

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadFileOperation;->n()V

    :goto_0
    return-void
.end method

.method static c(Lcom/jscape/filetransfer/UploadFileOperation;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iget-object p0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    return-object p0
.end method

.method private c()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/TransferOperationCancelledException;
        }
    .end annotation

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->q:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/filetransfer/TransferOperationCancelledException;

    invoke-direct {v0}, Lcom/jscape/filetransfer/TransferOperationCancelledException;-><init>()V

    throw v0
    :try_end_0
    .catch Lcom/jscape/filetransfer/TransferOperationCancelledException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method static d(Lcom/jscape/filetransfer/UploadFileOperation;)I
    .locals 0

    iget p0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->n:I

    return p0
.end method

.method private d()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->p:Ljava/lang/Exception;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->p:Ljava/lang/Exception;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    throw v1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/filetransfer/UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private e()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->o:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->p:Ljava/lang/Exception;

    return-void
.end method

.method private f()V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->n:I

    if-lez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->h:Lcom/jscape/util/Time;

    invoke-virtual {v0}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/jscape/util/D;->f(J)V

    :cond_1
    return-void
.end method

.method private g()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->isConnected()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->l:Z

    iget-object v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->connect()Lcom/jscape/filetransfer/FileTransfer;

    iget-object v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0, v1}, Lcom/jscape/filetransfer/FileTransferOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->getDir()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->m:Ljava/lang/String;

    iget-object v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->c:Lcom/jscape/filetransfer/RemoteDirectory;

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0, v1}, Lcom/jscape/filetransfer/RemoteDirectory;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/filetransfer/UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    return-void
.end method

.method private h()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->e:Lcom/jscape/filetransfer/UploadFileOperation$TransferStrategy;

    invoke-virtual {v0, p0}, Lcom/jscape/filetransfer/UploadFileOperation$TransferStrategy;->apply(Lcom/jscape/filetransfer/UploadFileOperation;)V

    return-void
.end method

.method private i()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->f:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/filetransfer/UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadFileOperation;->b:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/jscape/filetransfer/FileTransfer;->renameFile(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private j()V
    .locals 1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->i:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object v0

    invoke-static {v0}, Ljava/nio/file/Files;->delete(Ljava/nio/file/Path;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method

.method private k()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/jscape/filetransfer/FileTransfer;->setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private l()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->o:Z

    return-void
.end method

.method private m()V
    .locals 1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->l:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadFileOperation;->n()V

    :cond_1
    return-void
.end method

.method private n()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method


# virtual methods
.method public bridge synthetic applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/UploadFileOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/UploadFileOperation;

    move-result-object p1

    return-object p1
.end method

.method public applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/UploadFileOperation;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iput-object p1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadFileOperation;->a()Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_1

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadFileOperation;->b()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_2

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/filetransfer/UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadFileOperation;->c()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadFileOperation;->d()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadFileOperation;->m()V

    iget-object p1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->j:Lcom/jscape/filetransfer/UploadFileOperation$Listener;

    invoke-interface {p1, p0}, Lcom/jscape/filetransfer/UploadFileOperation$Listener;->onOperationCompleted(Lcom/jscape/filetransfer/UploadFileOperation;)V

    return-object p0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_1
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadFileOperation;->m()V

    iget-object v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->j:Lcom/jscape/filetransfer/UploadFileOperation$Listener;

    invoke-interface {v0, p0}, Lcom/jscape/filetransfer/UploadFileOperation$Listener;->onOperationCompleted(Lcom/jscape/filetransfer/UploadFileOperation;)V

    throw p1
.end method

.method public cancel()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/filetransfer/UploadFileOperation;->q:Z

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->interrupt()V

    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/UploadFileOperation;->r:[Ljava/lang/String;

    const/16 v2, 0xa

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadFileOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadFileOperation;->b:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0xb

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadFileOperation;->c:Lcom/jscape/filetransfer/RemoteDirectory;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadFileOperation;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadFileOperation;->e:Lcom/jscape/filetransfer/UploadFileOperation$TransferStrategy;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/filetransfer/UploadFileOperation;->f:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v2, 0xe

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/filetransfer/UploadFileOperation;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadFileOperation;->h:Lcom/jscape/util/Time;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/filetransfer/UploadFileOperation;->i:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/filetransfer/UploadFileOperation;->n:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/filetransfer/UploadFileOperation;->o:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadFileOperation;->p:Ljava/lang/Exception;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/filetransfer/UploadFileOperation;->q:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
