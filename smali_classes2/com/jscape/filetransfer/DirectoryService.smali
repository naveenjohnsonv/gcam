.class public Lcom/jscape/filetransfer/DirectoryService;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "\u0017\u0018tvcK\u000b!\u000c1ekY\n*\t|p*D\u0011d\u0006~a*LB \u0001cpiY\r6\u0011?"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/filetransfer/DirectoryService;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x7d

    goto :goto_1

    :cond_1
    const/16 v4, 0x32

    goto :goto_1

    :cond_2
    const/16 v4, 0x15

    goto :goto_1

    :cond_3
    const/16 v4, 0xa

    goto :goto_1

    :cond_4
    const/16 v4, 0xe

    goto :goto_1

    :cond_5
    const/16 v4, 0x77

    goto :goto_1

    :cond_6
    const/16 v4, 0x5b

    :goto_1
    const/16 v5, 0x1f

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/filetransfer/FileTransfer;",
            ")",
            "Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {p0}, Lcom/jscape/filetransfer/FileTransfer;->getDirListing()Ljava/util/Enumeration;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    :try_start_0
    invoke-virtual {v3}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->isDirectory()Z

    move-result v4
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v1, :cond_2

    if-eqz v4, :cond_1

    :try_start_1
    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_2

    if-nez v1, :cond_2

    :cond_1
    :try_start_2
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    goto :goto_1

    :cond_2
    :goto_0
    if-nez v1, :cond_0

    goto :goto_2

    :catch_1
    move-exception p0

    :try_start_3
    invoke-static {p0}, Lcom/jscape/filetransfer/DirectoryService;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p0

    throw p0
    :try_end_3
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p0

    :try_start_4
    invoke-static {p0}, Lcom/jscape/filetransfer/DirectoryService;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p0

    throw p0
    :try_end_4
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_4 .. :try_end_4} :catch_0

    :goto_1
    invoke-static {p0}, Lcom/jscape/filetransfer/DirectoryService;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p0

    throw p0

    :cond_3
    :goto_2
    new-instance p0, Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v2, v1}, Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;-><init>(Ljava/util/Collection;Ljava/util/Collection;Lcom/jscape/filetransfer/DirectoryService$1;)V

    return-object p0
.end method

.method private static a(Ljava/io/File;)Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p0

    array-length v3, p0

    const/4 v4, 0x0

    :cond_0
    if-ge v4, v3, :cond_4

    aget-object v5, p0, v4

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v1, :cond_2

    if-eqz v6, :cond_1

    invoke-interface {v0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    if-nez v1, :cond_3

    :cond_1
    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v6

    :cond_2
    if-eqz v1, :cond_3

    if-eqz v6, :cond_3

    invoke-interface {v2, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v4, v4, 0x1

    if-nez v1, :cond_0

    :cond_4
    new-instance p0, Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v2, v1}, Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;-><init>(Ljava/util/Collection;Ljava/util/Collection;Lcom/jscape/filetransfer/DirectoryService$1;)V

    return-object p0
.end method

.method private static a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;
    .locals 0

    return-object p0
.end method

.method private static a(Ljava/util/Collection;Lcom/jscape/util/b/s;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/io/File;",
            ">;",
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/filetransfer/DirectoryService$Directory<",
            "Ljava/io/File;",
            ">;>;)V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-static {v1}, Lcom/jscape/filetransfer/DirectoryService;->a(Ljava/io/File;)Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;

    move-result-object v2

    new-instance v3, Lcom/jscape/filetransfer/DirectoryService$Directory;

    iget-object v4, v2, Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;->files:Ljava/util/Collection;

    invoke-direct {v3, v1, v4}, Lcom/jscape/filetransfer/DirectoryService$Directory;-><init>(Ljava/lang/Object;Ljava/util/Collection;)V

    invoke-virtual {p1, v3}, Lcom/jscape/util/b/s;->d(Ljava/lang/Object;)Lcom/jscape/util/b/s;

    move-result-object v1

    iget-object v2, v2, Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;->directories:Ljava/util/Collection;

    invoke-static {v2, v1}, Lcom/jscape/filetransfer/DirectoryService;->a(Ljava/util/Collection;Lcom/jscape/util/b/s;)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private static a(Ljava/util/Collection;Lcom/jscape/util/b/s;Lcom/jscape/filetransfer/FileTransfer;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;",
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/filetransfer/DirectoryService$Directory<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;>;",
            "Lcom/jscape/filetransfer/FileTransfer;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    invoke-virtual {v1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Lcom/jscape/filetransfer/FileTransfer;->setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;

    invoke-static {p2}, Lcom/jscape/filetransfer/DirectoryService;->a(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;

    move-result-object v2

    new-instance v3, Lcom/jscape/filetransfer/DirectoryService$Directory;

    iget-object v4, v2, Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;->files:Ljava/util/Collection;

    invoke-direct {v3, v1, v4}, Lcom/jscape/filetransfer/DirectoryService$Directory;-><init>(Ljava/lang/Object;Ljava/util/Collection;)V

    invoke-virtual {p1, v3}, Lcom/jscape/util/b/s;->d(Ljava/lang/Object;)Lcom/jscape/util/b/s;

    move-result-object v1

    iget-object v2, v2, Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;->directories:Ljava/util/Collection;

    invoke-static {v2, v1, p2}, Lcom/jscape/filetransfer/DirectoryService;->a(Ljava/util/Collection;Lcom/jscape/util/b/s;Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-interface {p2}, Lcom/jscape/filetransfer/FileTransfer;->setDirUp()Lcom/jscape/filetransfer/FileTransfer;

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public static asTree(Ljava/io/File;)Lcom/jscape/util/b/s;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/filetransfer/DirectoryService$Directory<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    sget-object v1, Lcom/jscape/filetransfer/DirectoryService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    invoke-static {p0}, Lcom/jscape/filetransfer/DirectoryService;->a(Ljava/io/File;)Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;

    move-result-object v0

    new-instance v1, Lcom/jscape/filetransfer/DirectoryService$Directory;

    iget-object v2, v0, Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;->files:Ljava/util/Collection;

    invoke-direct {v1, p0, v2}, Lcom/jscape/filetransfer/DirectoryService$Directory;-><init>(Ljava/lang/Object;Ljava/util/Collection;)V

    invoke-static {v1}, Lcom/jscape/util/b/s;->a(Ljava/lang/Object;)Lcom/jscape/util/b/s;

    move-result-object p0

    iget-object v0, v0, Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;->directories:Ljava/util/Collection;

    invoke-static {v0, p0}, Lcom/jscape/filetransfer/DirectoryService;->a(Ljava/util/Collection;Lcom/jscape/util/b/s;)V

    return-object p0
.end method

.method public static asTree(Ljava/lang/String;Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/util/b/s;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/jscape/filetransfer/FileTransfer;",
            ")",
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/filetransfer/DirectoryService$Directory<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-interface {p1}, Lcom/jscape/filetransfer/FileTransfer;->getDir()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, p0}, Lcom/jscape/filetransfer/FileTransfer;->setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;

    invoke-static {p1}, Lcom/jscape/filetransfer/DirectoryService;->a(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;

    move-result-object v1

    new-instance v12, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9}, Ljava/util/Date;-><init>()V

    const-string v4, ""

    const/4 v5, 0x1

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x1

    move-object v2, v12

    move-object v3, p0

    invoke-direct/range {v2 .. v11}, Lcom/jscape/filetransfer/FileTransferRemoteFile;-><init>(Ljava/lang/String;Ljava/lang/String;ZZJLjava/util/Date;ZZ)V

    new-instance p0, Lcom/jscape/filetransfer/DirectoryService$Directory;

    iget-object v2, v1, Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;->files:Ljava/util/Collection;

    invoke-direct {p0, v12, v2}, Lcom/jscape/filetransfer/DirectoryService$Directory;-><init>(Ljava/lang/Object;Ljava/util/Collection;)V

    invoke-static {p0}, Lcom/jscape/util/b/s;->a(Ljava/lang/Object;)Lcom/jscape/util/b/s;

    move-result-object p0

    iget-object v1, v1, Lcom/jscape/filetransfer/DirectoryService$DirectoryEntries;->directories:Ljava/util/Collection;

    invoke-static {v1, p0, p1}, Lcom/jscape/filetransfer/DirectoryService;->a(Ljava/util/Collection;Lcom/jscape/util/b/s;Lcom/jscape/filetransfer/FileTransfer;)V

    invoke-interface {p1, v0}, Lcom/jscape/filetransfer/FileTransfer;->setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;

    return-object p0
.end method
