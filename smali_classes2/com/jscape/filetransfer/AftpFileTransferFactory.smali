.class public Lcom/jscape/filetransfer/AftpFileTransferFactory;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransferFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public transferFor(Lcom/jscape/filetransfer/FileTransferParameters;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferFactory$OperationException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/filetransfer/AftpFileTransfer;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->getHostname()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->getPort()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->getPort()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    const/16 v2, 0xbb8

    :goto_0
    :try_start_1
    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->getUsername()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->getPassword()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/jscape/filetransfer/AftpFileTransfer;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/jscape/filetransfer/FileTransferParameters;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    return-object v0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AftpFileTransferFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    new-instance v0, Lcom/jscape/filetransfer/FileTransferFactory$OperationException;

    invoke-direct {v0, p1}, Lcom/jscape/filetransfer/FileTransferFactory$OperationException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method
