.class public Lcom/jscape/filetransfer/FtpsImplicitTransfer;
.super Lcom/jscape/filetransfer/FtpsTransfer;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, ""

    invoke-direct {p0, v0, v0, v0}, Lcom/jscape/filetransfer/FtpsImplicitTransfer;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            "Ljava/util/Set<",
            "Lcom/jscape/filetransfer/FileTransferListener;",
            ">;)V"
        }
    .end annotation

    invoke-direct/range {p0 .. p6}, Lcom/jscape/filetransfer/FtpsTransfer;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/util/Set;)V

    iget-object p1, p0, Lcom/jscape/filetransfer/FtpsImplicitTransfer;->ftps:Lcom/jscape/inet/ftps/Ftps;

    sget-object p2, Lcom/jscape/inet/ftps/Ftps;->IMPLICIT_SSL:Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ftps/Ftps;->setConnectionType(Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x3de

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/jscape/filetransfer/FtpsImplicitTransfer;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    new-instance v5, Ljava/io/File;

    const-string v0, "."

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move v2, p4

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/jscape/filetransfer/FtpsImplicitTransfer;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/util/Set;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 7

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v6

    const/16 v2, 0x3de

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/jscape/filetransfer/FtpsImplicitTransfer;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/util/Set;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result p4

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/jscape/filetransfer/FtpsImplicitTransfer;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public copy()Lcom/jscape/filetransfer/FileTransfer;
    .locals 8

    new-instance v7, Lcom/jscape/filetransfer/FtpsImplicitTransfer;

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpsImplicitTransfer;->ftps:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->getHostname()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpsImplicitTransfer;->ftps:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->getPort()I

    move-result v2

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpsImplicitTransfer;->ftps:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->getUsername()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpsImplicitTransfer;->ftps:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->getPassword()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpsImplicitTransfer;->ftps:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v5

    iget-object v6, p0, Lcom/jscape/filetransfer/FtpsImplicitTransfer;->listeners:Ljava/util/Set;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/jscape/filetransfer/FtpsImplicitTransfer;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/util/Set;)V

    invoke-virtual {p0}, Lcom/jscape/filetransfer/FtpsImplicitTransfer;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/jscape/filetransfer/FtpsImplicitTransfer;->setDebugStream(Ljava/io/PrintStream;)Lcom/jscape/filetransfer/FileTransfer;

    invoke-virtual {p0}, Lcom/jscape/filetransfer/FtpsImplicitTransfer;->getDebug()Z

    move-result v0

    invoke-virtual {v7, v0}, Lcom/jscape/filetransfer/FtpsImplicitTransfer;->setDebug(Z)Lcom/jscape/filetransfer/FileTransfer;

    return-object v7
.end method
