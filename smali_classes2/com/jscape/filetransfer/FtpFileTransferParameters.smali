.class public Lcom/jscape/filetransfer/FtpFileTransferParameters;
.super Lcom/jscape/filetransfer/FileTransferParameters;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field protected natAddress:Ljava/lang/String;

.field protected timeZone:Ljava/util/TimeZone;

.field protected transferBlockSize:Lcom/jscape/util/ae;

.field protected transferMode:Lcom/jscape/filetransfer/TransferMode;

.field protected useCompression:Ljava/lang/Boolean;

.field protected useEprtCommand:Ljava/lang/Boolean;

.field protected useEpsvCommand:Ljava/lang/Boolean;

.field protected useIP6AutoDetection:Ljava/lang/Boolean;

.field protected usePassiveMode:Ljava/lang/Boolean;

.field protected wireEncoding:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "!1K7\'\u001d$~g}+/55cu\u0003\u0014!1J6#6\'ktL\u0006.77fBW>\'e\u0010!1I-0=\u0011crQ +6306\u0011!1K7\'\u001b;`aL!1+=b\u007f\u0003\u0011!1K7\'\u001d$\u007fe}+/55cu\u0003\u000f!1J6#6\'ktL\t-<10*KeN\u0002+41Yc_*1>1\u007fA_6#51ytL7b#!~tn%1+={ts+&=i\u0016!1K7\'\u0011\u0004;PK0-\u001c1yt]0+7:0"

    const/16 v4, 0xad

    const/16 v5, 0x11

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x73

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x1a

    const/16 v3, 0xe

    const-string v5, "2\"C6%\n#zpH$\"v`\u000b2\"Y><.\u001dqlHj"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x60

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x27

    goto :goto_4

    :cond_4
    const/16 v1, 0x2b

    goto :goto_4

    :cond_5
    const/16 v1, 0x31

    goto :goto_4

    :cond_6
    const/16 v1, 0x37

    goto :goto_4

    :cond_7
    const/16 v1, 0x4d

    goto :goto_4

    :cond_8
    const/16 v1, 0x62

    goto :goto_4

    :cond_9
    const/16 v1, 0x7e

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Lcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/io/PrintStream;Ljava/io/File;Ljava/lang/Boolean;Lcom/jscape/filetransfer/TransferMode;Lcom/jscape/util/ae;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/TimeZone;)V
    .locals 13

    move-object v12, p0

    sget-object v1, Lcom/jscape/filetransfer/Protocol;->FTP:Lcom/jscape/filetransfer/Protocol;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/jscape/filetransfer/FileTransferParameters;-><init>(Lcom/jscape/filetransfer/Protocol;Ljava/lang/String;Ljava/lang/Integer;Lcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/io/PrintStream;Ljava/io/File;)V

    move-object/from16 v0, p11

    iput-object v0, v12, Lcom/jscape/filetransfer/FtpFileTransferParameters;->usePassiveMode:Ljava/lang/Boolean;

    move-object/from16 v0, p12

    iput-object v0, v12, Lcom/jscape/filetransfer/FtpFileTransferParameters;->transferMode:Lcom/jscape/filetransfer/TransferMode;

    move-object/from16 v0, p13

    iput-object v0, v12, Lcom/jscape/filetransfer/FtpFileTransferParameters;->transferBlockSize:Lcom/jscape/util/ae;

    move-object/from16 v0, p14

    iput-object v0, v12, Lcom/jscape/filetransfer/FtpFileTransferParameters;->wireEncoding:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, v12, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useCompression:Ljava/lang/Boolean;

    move-object/from16 v0, p16

    iput-object v0, v12, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useEprtCommand:Ljava/lang/Boolean;

    move-object/from16 v0, p17

    iput-object v0, v12, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useEpsvCommand:Ljava/lang/Boolean;

    move-object/from16 v0, p18

    iput-object v0, v12, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useIP6AutoDetection:Ljava/lang/Boolean;

    move-object/from16 v0, p19

    iput-object v0, v12, Lcom/jscape/filetransfer/FtpFileTransferParameters;->natAddress:Ljava/lang/String;

    move-object/from16 v0, p20

    iput-object v0, v12, Lcom/jscape/filetransfer/FtpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    return-void
.end method

.method private static b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public applySpecificTo(Lcom/jscape/filetransfer/FtpTransfer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->usePassiveMode:Ljava/lang/Boolean;
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->usePassiveMode:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/jscape/filetransfer/FtpTransfer;->setPassive(Z)Lcom/jscape/filetransfer/FileTransfer;

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->transferMode:Lcom/jscape/filetransfer/TransferMode;
    :try_end_2
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v1, :cond_1

    :try_start_3
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->transferMode:Lcom/jscape/filetransfer/TransferMode;

    invoke-virtual {v1, p1}, Lcom/jscape/filetransfer/TransferMode;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    :try_start_4
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->transferBlockSize:Lcom/jscape/util/ae;
    :try_end_4
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_4 .. :try_end_4} :catch_4

    if-eqz v1, :cond_2

    :try_start_5
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->transferBlockSize:Lcom/jscape/util/ae;

    invoke-virtual {v1}, Lcom/jscape/util/ae;->a()J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {p1, v1}, Lcom/jscape/filetransfer/FtpTransfer;->setBlockTransferSize(I)Lcom/jscape/filetransfer/FileTransfer;

    goto :goto_2

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_2
    :goto_2
    if-eqz v0, :cond_3

    :try_start_6
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->wireEncoding:Ljava/lang/String;
    :try_end_6
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_6 .. :try_end_6} :catch_6

    if-eqz v1, :cond_3

    :try_start_7
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->wireEncoding:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/jscape/filetransfer/FtpTransfer;->setWireEncoding(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;

    goto :goto_3

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_3
    :goto_3
    :try_start_8
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useCompression:Ljava/lang/Boolean;
    :try_end_8
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_8 .. :try_end_8} :catch_f

    if-eqz v0, :cond_5

    if-eqz v1, :cond_4

    :try_start_9
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useCompression:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/jscape/filetransfer/FtpTransfer;->setCompression(Z)Lcom/jscape/filetransfer/FtpTransfer;
    :try_end_9
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_9 .. :try_end_9} :catch_10

    :cond_4
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useEprtCommand:Ljava/lang/Boolean;

    :cond_5
    if-eqz v0, :cond_7

    if-eqz v1, :cond_6

    :try_start_a
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useEprtCommand:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/jscape/filetransfer/FtpTransfer;->setUseEPRT(Z)Lcom/jscape/filetransfer/FtpTransfer;
    :try_end_a
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_a .. :try_end_a} :catch_8

    goto :goto_4

    :catch_8
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_6
    :goto_4
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useEpsvCommand:Ljava/lang/Boolean;

    :cond_7
    if-eqz v0, :cond_9

    if-eqz v1, :cond_8

    :try_start_b
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useEpsvCommand:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/jscape/filetransfer/FtpTransfer;->setUseEPSV(Z)Lcom/jscape/filetransfer/FtpTransfer;
    :try_end_b
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_b .. :try_end_b} :catch_9

    goto :goto_5

    :catch_9
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_8
    :goto_5
    if-eqz v0, :cond_a

    :try_start_c
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useIP6AutoDetection:Ljava/lang/Boolean;
    :try_end_c
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_c .. :try_end_c} :catch_a

    goto :goto_6

    :catch_a
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_9
    :goto_6
    if-eqz v1, :cond_a

    :try_start_d
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useIP6AutoDetection:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/jscape/filetransfer/FtpTransfer;->setAutoDetectIpv6(Z)Lcom/jscape/filetransfer/FtpTransfer;
    :try_end_d
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_d .. :try_end_d} :catch_b

    goto :goto_7

    :catch_b
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_a
    :goto_7
    if-eqz v0, :cond_b

    :try_start_e
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->natAddress:Ljava/lang/String;
    :try_end_e
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_e .. :try_end_e} :catch_c

    if-eqz v0, :cond_b

    :try_start_f
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->natAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/jscape/filetransfer/FtpTransfer;->setNATAddress(Ljava/lang/String;)Lcom/jscape/filetransfer/FtpTransfer;

    goto :goto_8

    :catch_c
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_f
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_f .. :try_end_f} :catch_d

    :catch_d
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_b
    :goto_8
    :try_start_10
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {p1, v0}, Lcom/jscape/filetransfer/FtpTransfer;->setTimeZone(Ljava/util/TimeZone;)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_10
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_10 .. :try_end_10} :catch_e

    :cond_c
    return-void

    :catch_e
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :catch_f
    move-exception p1

    :try_start_11
    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_11
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_11 .. :try_end_11} :catch_10

    :catch_10
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public bridge synthetic applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FtpFileTransferParameters;

    move-result-object p1

    return-object p1
.end method

.method public applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FtpFileTransferParameters;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-super {p0, p1}, Lcom/jscape/filetransfer/FileTransferParameters;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    instance-of v0, p1, Lcom/jscape/filetransfer/FtpTransfer;
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    check-cast p1, Lcom/jscape/filetransfer/FtpTransfer;

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->applySpecificTo(Lcom/jscape/filetransfer/FtpTransfer;)V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpFileTransferParameters;->b(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    return-object p0
.end method

.method public getNatAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->natAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeZone()Ljava/util/TimeZone;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    return-object v0
.end method

.method public getTransferBlockSize()Lcom/jscape/util/ae;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->transferBlockSize:Lcom/jscape/util/ae;

    return-object v0
.end method

.method public getTransferMode()Lcom/jscape/filetransfer/TransferMode;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->transferMode:Lcom/jscape/filetransfer/TransferMode;

    return-object v0
.end method

.method public getUseCompression()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useCompression:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getUseEprtCommand()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useEprtCommand:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getUseEpsvCommand()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useEpsvCommand:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getUseIP6AutoDetection()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useIP6AutoDetection:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getUsePassiveMode()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->usePassiveMode:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getWireEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->wireEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public setNatAddress(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->natAddress:Ljava/lang/String;

    return-void
.end method

.method public setTimeZone(Ljava/util/TimeZone;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    return-void
.end method

.method public setTransferBlockSize(Lcom/jscape/util/ae;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->transferBlockSize:Lcom/jscape/util/ae;

    return-void
.end method

.method public setTransferMode(Lcom/jscape/filetransfer/TransferMode;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->transferMode:Lcom/jscape/filetransfer/TransferMode;

    return-void
.end method

.method public setUseCompression(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useCompression:Ljava/lang/Boolean;

    return-void
.end method

.method public setUseEprtCommand(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useEprtCommand:Ljava/lang/Boolean;

    return-void
.end method

.method public setUseEpsvCommand(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useEpsvCommand:Ljava/lang/Boolean;

    return-void
.end method

.method public setUseIP6AutoDetection(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useIP6AutoDetection:Ljava/lang/Boolean;

    return-void
.end method

.method public setUsePassiveMode(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->usePassiveMode:Ljava/lang/Boolean;

    return-void
.end method

.method public setWireEncoding(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->wireEncoding:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/FtpFileTransferParameters;->a:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->usePassiveMode:Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->transferMode:Lcom/jscape/filetransfer/TransferMode;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->transferBlockSize:Lcom/jscape/util/ae;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->wireEncoding:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x3

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useCompression:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useEprtCommand:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useEpsvCommand:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x7

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->useIP6AutoDetection:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v3, 0x8

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->natAddress:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/filetransfer/FtpFileTransferParameters;->timeZone:Ljava/util/TimeZone;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
