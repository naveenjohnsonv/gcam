.class public Lcom/jscape/filetransfer/DownloadDirectoryOperation;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransferOperation;
.implements Lcom/jscape/filetransfer/DownloadFileOperation$Listener;


# static fields
.field private static final o:[Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/filetransfer/FileTransferOperation;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Lcom/jscape/util/Time;

.field private final e:Ljava/lang/String;

.field private final f:I

.field private g:Lcom/jscape/filetransfer/FileTransfer;

.field private h:Z

.field private i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/jscape/filetransfer/DownloadFileOperation;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/concurrent/ExecutorService;

.field private k:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Lcom/jscape/filetransfer/FileTransfer;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/jscape/util/b/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/filetransfer/DirectoryService$Directory<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Ljava/util/concurrent/CountDownLatch;

.field private volatile n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "\u0003}V<>5\u001da}Fh69\u00155o\u0012j28\u0010$2\u0010m<Sh\'1\u00081hby!=\n%!\u000em<Ft!1\u0004%_]i= X\u0013m<@y>;\u0011$X[n67\u0011.nK!t\u0016m<@y>;\u0011$X[n67\u0011.nKH!1\u0000|\u000em<_}+\u0015\u00115y_l\'\'X\u0011m<Tu?16$lSn2 \n3!\u0015\u000cm<Q}=7\u0000-pWxn"

    const/16 v5, 0x90

    move v8, v3

    const/4 v6, -0x1

    const/16 v7, 0x17

    :goto_0
    const/16 v9, 0x3b

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x49

    const-string v4, "Y\'\u000cf}fM~\'\u000cfjaJu2H0hbJ~h1_)\u001f(ea^\u007f\u0002\u00014lmKt4\u0011\tykMz2\u0001)g.Dk)\u001b2JaQu#\u000b2F~Zi\'\u001c/f`\u0002"

    move v8, v11

    const/4 v6, -0x1

    const/16 v7, 0x17

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x61

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->o:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v1, v14, 0x7

    const/16 v16, 0x27

    if-eqz v1, :cond_7

    if-eq v1, v10, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_6

    const/4 v2, 0x3

    if-eq v1, v2, :cond_8

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v16, 0x5e

    goto :goto_4

    :cond_4
    const/16 v16, 0x6f

    goto :goto_4

    :cond_5
    const/16 v16, 0x68

    goto :goto_4

    :cond_6
    const/16 v16, 0x9

    goto :goto_4

    :cond_7
    const/16 v16, 0x7a

    :cond_8
    :goto_4
    xor-int v1, v9, v16

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransferOperation;Ljava/lang/String;ILcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->b:Ljava/lang/String;

    const/4 p2, 0x0

    if-eqz p1, :cond_2

    int-to-long v0, p3

    const-wide/16 v2, 0x0

    sget-object v4, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->o:[Ljava/lang/String;

    aget-object v4, v4, p2

    invoke-static {v0, v1, v2, v3, v4}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p3, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->c:I

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->d:Lcom/jscape/util/Time;

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->e:Ljava/lang/String;

    if-eqz p6, :cond_1

    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result p3

    if-eqz p1, :cond_2

    if-ltz p3, :cond_0

    goto :goto_0

    :cond_0
    move p3, p2

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p3, 0x1

    :cond_2
    :goto_1
    sget-object p4, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->o:[Ljava/lang/String;

    const/16 p5, 0x8

    aget-object p4, p4, p5

    invoke-static {p3, p4}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    if-eqz p1, :cond_3

    if-eqz p6, :cond_4

    :cond_3
    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result p2

    :cond_4
    iput p2, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->f:I

    return-void
.end method

.method private a(Lcom/jscape/filetransfer/RemoteDirectory;Ljava/io/File;)Lcom/jscape/filetransfer/DownloadFileOperation;
    .locals 11

    new-instance v10, Lcom/jscape/filetransfer/DownloadFileOperation;

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    sget-object v5, Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;->RESUME_AFTER_FIRST_ATTEMPT:Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;

    iget v6, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->c:I

    iget-object v7, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->d:Lcom/jscape/util/Time;

    const/4 v3, 0x0

    const/4 v8, 0x0

    move-object v0, v10

    move-object v2, p1

    move-object v4, p2

    move-object v9, p0

    invoke-direct/range {v0 .. v9}, Lcom/jscape/filetransfer/DownloadFileOperation;-><init>(Lcom/jscape/filetransfer/FileTransferOperation;Lcom/jscape/filetransfer/RemoteDirectory;Ljava/lang/String;Ljava/io/File;Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;ILcom/jscape/util/Time;ZLcom/jscape/filetransfer/DownloadFileOperation$Listener;)V

    return-object v10
.end method

.method static a(Lcom/jscape/filetransfer/DownloadDirectoryOperation;Lcom/jscape/util/b/s;)Lcom/jscape/util/b/s;
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->l:Lcom/jscape/util/b/s;

    return-object p1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :cond_0
    const/16 v0, 0x2f

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    :cond_1
    if-ltz v1, :cond_2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    :cond_2
    return-object p1
.end method

.method private a(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v1, :cond_1

    if-eqz v1, :cond_3

    iget-object v3, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    if-nez v1, :cond_0

    :cond_2
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_3
    return-object v2
.end method

.method private a()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->isConnected()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->h:Z

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-direct {p0, v0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Lcom/jscape/filetransfer/FileTransfer;)V

    :cond_1
    return-void
.end method

.method private a(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {v0, p1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->k:Ljava/util/concurrent/BlockingQueue;

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->k:Ljava/util/concurrent/BlockingQueue;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v1, v2}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    :cond_0
    if-ge v1, p1, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v2}, Lcom/jscape/filetransfer/FileTransfer;->copy()Lcom/jscape/filetransfer/FileTransfer;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-direct {p0, v2}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Lcom/jscape/filetransfer/FileTransfer;)V

    iget-object v3, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->k:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v3, v2}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_1

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->i()V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method static a(Lcom/jscape/filetransfer/DownloadDirectoryOperation;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a()V

    return-void
.end method

.method private a(Lcom/jscape/filetransfer/FileTransfer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-interface {p1}, Lcom/jscape/filetransfer/FileTransfer;->connect()Lcom/jscape/filetransfer/FileTransfer;

    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-interface {v0, p1}, Lcom/jscape/filetransfer/FileTransferOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    return-void
.end method

.method private a(Lcom/jscape/filetransfer/FileTransferOperation;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {p1, v0}, Lcom/jscape/filetransfer/FileTransferOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    :try_end_0
    .catch Lcom/jscape/filetransfer/TransferOperationCancelledException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private a(Ljava/io/File;Lcom/jscape/util/b/s;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/filetransfer/DirectoryService$Directory<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p2}, Lcom/jscape/util/b/s;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/filetransfer/DirectoryService$Directory;

    iget-object v0, v0, Lcom/jscape/filetransfer/DirectoryService$Directory;->path:Ljava/lang/Object;

    check-cast v0, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    invoke-virtual {v0}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilename()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/jscape/util/Q;->b(Ljava/io/File;)Ljava/io/File;

    invoke-virtual {p2}, Lcom/jscape/util/b/s;->c()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/jscape/util/b/s;

    invoke-direct {p0, v2, p2}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Ljava/io/File;Lcom/jscape/util/b/s;)V

    if-nez v1, :cond_0

    :cond_1
    return-void
.end method

.method private a(Ljava/util/List;Lcom/jscape/util/b/s;Ljava/io/File;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/filetransfer/DirectoryService$Directory<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;>;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    invoke-virtual {p2}, Lcom/jscape/util/b/s;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/filetransfer/DirectoryService$Directory;

    iget-object v0, v0, Lcom/jscape/filetransfer/DirectoryService$Directory;->path:Ljava/lang/Object;

    check-cast v0, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    invoke-virtual {v0}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilename()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Ljava/io/File;

    invoke-direct {p0, v0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/jscape/util/b/s;->a()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/jscape/filetransfer/DirectoryService$Directory;

    iget-object p3, p3, Lcom/jscape/filetransfer/DirectoryService$Directory;->files:Ljava/util/Collection;

    invoke-direct {p0, p1, p3, v2}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Ljava/util/List;Ljava/util/Collection;Ljava/io/File;)V

    invoke-virtual {p2}, Lcom/jscape/util/b/s;->c()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/jscape/util/b/s;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0, p1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v0, p3, v2}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Ljava/util/List;Lcom/jscape/util/b/s;Ljava/io/File;)V

    if-nez v1, :cond_0

    :cond_1
    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/Collection;Ljava/io/File;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    new-instance v0, Lcom/jscape/filetransfer/RemoteDirectory$PathElementsDirectory;

    invoke-direct {v0, p1}, Lcom/jscape/filetransfer/RemoteDirectory$PathElementsDirectory;-><init>(Ljava/util/List;)V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    invoke-virtual {v2}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilename()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p3, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v0, v3}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Lcom/jscape/filetransfer/RemoteDirectory;Ljava/io/File;)Lcom/jscape/filetransfer/DownloadFileOperation;

    move-result-object v3

    iget-object v4, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->i:Ljava/util/Map;

    invoke-direct {p0, p1, v2}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v1, :cond_0

    :cond_1
    return-void
.end method

.method private b(Ljava/lang/String;)Lcom/jscape/filetransfer/DownloadFileOperation;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/filetransfer/DownloadFileOperation;

    return-object p1
.end method

.method static b(Lcom/jscape/filetransfer/DownloadDirectoryOperation;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->b:Ljava/lang/String;

    return-object p0
.end method

.method private b()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/filetransfer/DownloadDirectoryOperation$1;

    invoke-direct {v0, p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation$1;-><init>(Lcom/jscape/filetransfer/DownloadDirectoryOperation;)V

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->d:Lcom/jscape/util/Time;

    invoke-virtual {v1}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v1

    iget v3, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->c:I

    invoke-static {v0, v1, v2, v3}, Lcom/jscape/util/x;->a(Ljava/util/concurrent/Callable;JI)Ljava/lang/Object;

    return-void
.end method

.method static c(Lcom/jscape/filetransfer/DownloadDirectoryOperation;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iget-object p0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    return-object p0
.end method

.method private c()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->getLocalDir()Ljava/io/File;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->l:Lcom/jscape/util/b/s;

    invoke-direct {p0, v0, v1}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Ljava/io/File;Lcom/jscape/util/b/s;)V

    return-void
.end method

.method private d()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->i:Ljava/util/Map;

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->e()Ljava/util/LinkedList;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->l:Lcom/jscape/util/b/s;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v2}, Lcom/jscape/filetransfer/FileTransfer;->getLocalDir()Ljava/io/File;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Ljava/util/List;Lcom/jscape/util/b/s;Ljava/io/File;)V

    return-void
.end method

.method static d(Lcom/jscape/filetransfer/DownloadDirectoryOperation;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->m()V

    return-void
.end method

.method private e()Ljava/util/LinkedList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->getDir()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private f()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->m:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method private g()Z
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->f:I

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    if-le v1, v2, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method private h()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->f:I

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    iput-object v2, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->j:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(I)V

    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/FileTransferOperation;

    iget-object v3, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;

    iget-object v5, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->k:Ljava/util/concurrent/BlockingQueue;

    invoke-direct {v4, v2, v5}, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;-><init>(Lcom/jscape/filetransfer/FileTransferOperation;Ljava/util/concurrent/BlockingQueue;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    if-nez v1, :cond_0

    :cond_1
    return-void
.end method

.method private i()V
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->k:Ljava/util/concurrent/BlockingQueue;

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    move-object v2, p0

    goto :goto_1

    :cond_1
    move-object v2, p0

    :goto_0
    check-cast v1, Lcom/jscape/filetransfer/FileTransfer;

    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->close()V

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    :goto_1
    iget-object v1, v2, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->k:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :cond_3
    :goto_2
    return-void
.end method

.method private j()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->m:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->i()V

    throw v0

    :catch_0
    :goto_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->i()V

    return-void
.end method

.method private k()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/TransferOperationCancelledException;
        }
    .end annotation

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->n:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/filetransfer/TransferOperationCancelledException;

    invoke-direct {v0}, Lcom/jscape/filetransfer/TransferOperationCancelledException;-><init>()V

    throw v0
    :try_end_0
    .catch Lcom/jscape/filetransfer/TransferOperationCancelledException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private l()V
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->j:Ljava/util/concurrent/ExecutorService;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    iput-object v2, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->j:Ljava/util/concurrent/ExecutorService;

    :cond_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->i()V

    iput-object v2, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->l:Lcom/jscape/util/b/s;

    iput-object v2, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->m:Ljava/util/concurrent/CountDownLatch;

    :cond_1
    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->h:Z

    if-eqz v0, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->m()V

    :cond_3
    return-void
.end method

.method private m()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private n()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-direct {p0, v2}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Lcom/jscape/filetransfer/FileTransferOperation;)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method


# virtual methods
.method public applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iput-object p1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->n:Z

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->b()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->c()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->d()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->f()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->g()Z

    move-result p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p1, :cond_0

    :try_start_2
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->h()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :try_start_3
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->n()V

    :cond_1
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->j()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->k()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->l()V

    return-object p0

    :catch_0
    move-exception p1

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_2
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_3
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_0
    :try_start_7
    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :goto_1
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->l()V

    throw p1
.end method

.method public cancel()V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->n:Z

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/DownloadFileOperation;

    invoke-virtual {v2}, Lcom/jscape/filetransfer/DownloadFileOperation;->cancel()V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public cancel(Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->b(Ljava/lang/String;)Lcom/jscape/filetransfer/DownloadFileOperation;

    move-result-object p1

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/jscape/filetransfer/DownloadFileOperation;->cancel()V

    :cond_1
    return-void
.end method

.method public fileCount()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public onOperationCompleted(Lcom/jscape/filetransfer/DownloadFileOperation;)V
    .locals 0

    iget-object p1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->m:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->o:[Ljava/lang/String;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->c:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->d:Lcom/jscape/util/Time;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x6

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->f:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->l:Lcom/jscape/util/b/s;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/filetransfer/DownloadDirectoryOperation;->n:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
