.class public Lcom/jscape/filetransfer/FileTransferRemoteFile;
.super Ljava/lang/Object;


# static fields
.field public static final DIRECTORY_FILTER:Lcom/jscape/filetransfer/FileTransferRemoteFile$Filter;

.field public static final FILE_FILTER:Lcom/jscape/filetransfer/FileTransferRemoteFile$Filter;

.field private static final j:[Ljava/lang/String;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Z

.field private e:J

.field private f:Ljava/util/Date;

.field private g:Z

.field private h:Z

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v4, "En\u0000,~!4\u0008:\u0003x\u000cEn\u0002,`!\u0013\u001d!\u0014</\u000bEn\u00117{0\u0011\u000b\"\u0003x\"/\'\n F6\u0011\u0007=\u0000 `\u0016\u0015\u0004!\u0012 T-\u001c\u000cn\u001d#{(\u0015\u0007/\u000b /c\nEn\u001f s6#\u000c:[\u0007En\n,|/M\u000bEn\u0000,~!\u0003\u00004\u0003x"

    const/16 v5, 0x66

    const/4 v6, -0x1

    const/16 v7, 0xb

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x40

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x1a

    const-string v4, "^u\u000f;h;\n\u00109\u0018c\u000e^u\u00117g4?\u0013\'\u001a;}bL"

    move v8, v11

    const/4 v6, -0x1

    const/16 v7, 0xb

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x5b

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->j:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/filetransfer/FileTransferRemoteFile$1;

    invoke-direct {v0}, Lcom/jscape/filetransfer/FileTransferRemoteFile$1;-><init>()V

    sput-object v0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->FILE_FILTER:Lcom/jscape/filetransfer/FileTransferRemoteFile$Filter;

    new-instance v0, Lcom/jscape/filetransfer/FileTransferRemoteFile$2;

    invoke-direct {v0}, Lcom/jscape/filetransfer/FileTransferRemoteFile$2;-><init>()V

    sput-object v0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->DIRECTORY_FILTER:Lcom/jscape/filetransfer/FileTransferRemoteFile$Filter;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v1, v14, 0x7

    const/4 v2, 0x5

    const/4 v3, 0x4

    if-eqz v1, :cond_8

    if-eq v1, v10, :cond_7

    const/4 v10, 0x2

    if-eq v1, v10, :cond_6

    const/4 v10, 0x3

    if-eq v1, v10, :cond_9

    if-eq v1, v3, :cond_5

    if-eq v1, v2, :cond_4

    const/16 v2, 0x30

    goto :goto_4

    :cond_4
    move v2, v3

    goto :goto_4

    :cond_5
    const/16 v2, 0x52

    goto :goto_4

    :cond_6
    const/16 v2, 0x26

    goto :goto_4

    :cond_7
    const/16 v2, 0xe

    goto :goto_4

    :cond_8
    const/16 v2, 0x29

    :cond_9
    :goto_4
    xor-int v1, v9, v2

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v10, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 11

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, Lcom/jscape/filetransfer/FileTransferRemoteFile;-><init>(Ljava/lang/String;Ljava/lang/String;ZZJLjava/util/Date;ZZZ)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZJLjava/util/Date;ZZ)V
    .locals 11

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-wide/from16 v5, p5

    move-object/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/jscape/filetransfer/FileTransferRemoteFile;-><init>(Ljava/lang/String;Ljava/lang/String;ZZJLjava/util/Date;ZZZ)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZJLjava/util/Date;ZZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->b:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->c:Z

    iput-boolean p4, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->d:Z

    iput-wide p5, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->e:J

    iput-object p7, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->f:Ljava/util/Date;

    iput-boolean p8, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->g:Z

    iput-boolean p9, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->h:Z

    iput-boolean p10, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->i:Z

    return-void
.end method

.method static a(Lcom/jscape/filetransfer/FileTransferRemoteFile;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->c:Z

    return p0
.end method

.method static b(Lcom/jscape/filetransfer/FileTransferRemoteFile;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->a:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public getFileDate()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->f:Ljava/util/Date;

    return-object v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getFilesize()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->e:J

    return-wide v0
.end method

.method public getIsYearSet()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->i:Z

    return v0
.end method

.method public getLinkTarget()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->b:Ljava/lang/String;

    return-object v0
.end method

.method public isDirectory()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->c:Z

    return v0
.end method

.method public isHidden()Z
    .locals 2

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->a:Ljava/lang/String;

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isLink()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->d:Z

    return v0
.end method

.method public isReadable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->g:Z

    return v0
.end method

.method public isWritable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->h:Z

    return v0
.end method

.method public setDirectory(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->c:Z

    return-void
.end method

.method public setFileDate(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->f:Ljava/util/Date;

    return-void
.end method

.method public setFilename(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->a:Ljava/lang/String;

    return-void
.end method

.method public setFilesize(J)V
    .locals 0

    iput-wide p1, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->e:J

    return-void
.end method

.method public setIsYearSet(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->i:Z

    return-void
.end method

.method public setLink(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->d:Z

    return-void
.end method

.method public setLinkTarget(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->b:Ljava/lang/String;

    return-void
.end method

.method public setReadable(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->g:Z

    return-void
.end method

.method public setWritable(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->h:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/FileTransferRemoteFile;->j:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v3, 0x8

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->c:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->d:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->f:Ljava/util/Date;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->g:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->h:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/filetransfer/FileTransferRemoteFile;->i:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
