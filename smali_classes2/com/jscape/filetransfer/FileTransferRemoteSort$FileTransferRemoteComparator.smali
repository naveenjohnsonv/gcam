.class public abstract Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field protected isAscendent:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;->isAscendent:Z

    return-void
.end method


# virtual methods
.method public abstract compare(Ljava/lang/Object;Ljava/lang/Object;)I
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    if-ne p0, p1, :cond_0

    return v1

    :cond_0
    move-object v2, p1

    goto :goto_0

    :cond_1
    move-object v2, p0

    :goto_0
    const/4 v3, 0x0

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v2, v4, :cond_3

    goto :goto_2

    :cond_2
    move-object p1, v2

    :cond_3
    check-cast p1, Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;

    iget-boolean v2, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;->isAscendent:Z

    if-eqz v0, :cond_4

    iget-boolean p1, p1, Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;->isAscendent:Z

    if-ne v2, p1, :cond_5

    goto :goto_1

    :cond_4
    move v1, v2

    :goto_1
    move v3, v1

    :cond_5
    :goto_2
    return v3
.end method

.method public getAscendent()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;->isAscendent:Z

    return v0
.end method

.method public hashCode()I
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;->isAscendent:Z

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public setAscendent(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/FileTransferRemoteSort$FileTransferRemoteComparator;->isAscendent:Z

    return-void
.end method
