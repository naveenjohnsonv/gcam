.class public Lcom/jscape/filetransfer/UploadDirectoryOperation;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransferOperation;
.implements Lcom/jscape/filetransfer/UploadFileOperation$Listener;


# static fields
.field private static final o:[Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/filetransfer/FileTransferOperation;

.field private final b:Ljava/io/File;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Lcom/jscape/util/Time;

.field private final f:I

.field private g:Lcom/jscape/filetransfer/FileTransfer;

.field private h:Z

.field private i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/io/File;",
            "Lcom/jscape/filetransfer/UploadFileOperation;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/concurrent/ExecutorService;

.field private k:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Lcom/jscape/filetransfer/FileTransfer;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lcom/jscape/util/b/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/filetransfer/DirectoryService$Directory<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Ljava/util/concurrent/CountDownLatch;

.field private volatile n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "e\u0016oI\u0002|\u0018\r_qC\u0002i\u001b;O>/\u001cFoI\u0000y0 DfE\u0015r\u00060ysC\u0013|\u0000 Ym\u0006\u001am\u001b:B@I\u000fs\u0011*BLV\u0004o\u0015=_lH\\\u0015e\u0016oI\u0002|\u0018\r_qC\u0002i\u001b;OWT\u0004xI\u000ce\u0016`G\u000f~\u0011%ZfB\\\u0017\u000bWg\u0006\u0015u\u0006,Wg\u0006\u0002r\u0001\'B#P\u0000q\u0001,\u0018\u0010e\u0016bR\u0015x\u00199BSC\u0013t\u001b-\u000b\u0017\u000bWg\u0006\u000c|\u000ciWwR\u0004p\u0004=E#P\u0000q\u0001,\u0018&\u001aFfE\u0008{\u001d,R#V\u0000i\u001c\'WnCAt\u0007iXlRA|T-_qC\u0002i\u001b;O-\u0015e\u0016wC\u000cm2 Zfc\u0019i\u0011\'EjI\u000f S"

    const/16 v4, 0xe2

    const/16 v5, 0x11

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x4d

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x1d

    const/16 v3, 0xe

    const-string v5, "q\u0002cZ\u0007l\u00019axG\u001b}]\u000eq\u0002zS\rH\u0014)GzB\u0001z]"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x59

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->o:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    const/4 v1, 0x4

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v2, 0x2

    if-eq v15, v2, :cond_7

    const/4 v2, 0x3

    if-eq v15, v2, :cond_6

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x39

    goto :goto_4

    :cond_4
    const/16 v1, 0x50

    goto :goto_4

    :cond_5
    const/16 v1, 0x2c

    goto :goto_4

    :cond_6
    const/16 v1, 0x6b

    goto :goto_4

    :cond_7
    const/16 v1, 0x4e

    goto :goto_4

    :cond_8
    const/16 v1, 0x7b

    :cond_9
    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransferOperation;Ljava/io/File;Ljava/lang/String;ILcom/jscape/util/Time;Ljava/lang/Integer;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z

    move-result v0

    sget-object v1, Lcom/jscape/filetransfer/UploadDirectoryOperation;->o:[Ljava/lang/String;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-static {v0, v2}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    iput-object p2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->b:Ljava/io/File;

    iput-object p3, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->c:Ljava/lang/String;

    const/4 p2, 0x0

    if-eqz p1, :cond_2

    int-to-long v2, p4

    const-wide/16 v4, 0x0

    const/4 p3, 0x6

    aget-object p3, v1, p3

    invoke-static {v2, v3, v4, v5, p3}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p4, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->d:I

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->e:Lcom/jscape/util/Time;

    if-eqz p6, :cond_1

    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result p4

    if-eqz p1, :cond_2

    if-ltz p4, :cond_0

    goto :goto_0

    :cond_0
    move p4, p2

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p4, 0x1

    :cond_2
    :goto_1
    sget-object p3, Lcom/jscape/filetransfer/UploadDirectoryOperation;->o:[Ljava/lang/String;

    const/4 p5, 0x4

    aget-object p3, p3, p5

    invoke-static {p4, p3}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    if-eqz p1, :cond_3

    if-eqz p6, :cond_4

    :cond_3
    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result p2

    :cond_4
    iput p2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->f:I

    return-void
.end method

.method private a(Ljava/io/File;)Lcom/jscape/filetransfer/UploadFileOperation;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/filetransfer/UploadFileOperation;

    return-object p1
.end method

.method private a(Ljava/io/File;Lcom/jscape/filetransfer/RemoteDirectory;)Lcom/jscape/filetransfer/UploadFileOperation;
    .locals 11

    new-instance v10, Lcom/jscape/filetransfer/UploadFileOperation;

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    iget-object v4, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->c:Ljava/lang/String;

    sget-object v5, Lcom/jscape/filetransfer/UploadFileOperation$TransferStrategy;->RESUME_AFTER_FIRST_ATTEMPT:Lcom/jscape/filetransfer/UploadFileOperation$TransferStrategy;

    iget v6, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->d:I

    iget-object v7, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->e:Lcom/jscape/util/Time;

    const/4 v8, 0x0

    move-object v0, v10

    move-object v2, p1

    move-object v3, p2

    move-object v9, p0

    invoke-direct/range {v0 .. v9}, Lcom/jscape/filetransfer/UploadFileOperation;-><init>(Lcom/jscape/filetransfer/FileTransferOperation;Ljava/io/File;Lcom/jscape/filetransfer/RemoteDirectory;Ljava/lang/String;Lcom/jscape/filetransfer/UploadFileOperation$TransferStrategy;ILcom/jscape/util/Time;ZLcom/jscape/filetransfer/UploadFileOperation$Listener;)V

    return-object v10
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->b:Ljava/io/File;

    invoke-static {v0}, Lcom/jscape/filetransfer/DirectoryService;->asTree(Ljava/io/File;)Lcom/jscape/util/b/s;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->l:Lcom/jscape/util/b/s;

    return-void
.end method

.method private a(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {v0, p1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->k:Ljava/util/concurrent/BlockingQueue;

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->k:Ljava/util/concurrent/BlockingQueue;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v1, v2}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    :cond_0
    if-ge v1, p1, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v2}, Lcom/jscape/filetransfer/FileTransfer;->copy()Lcom/jscape/filetransfer/FileTransfer;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-direct {p0, v2}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Lcom/jscape/filetransfer/FileTransfer;)V

    iget-object v3, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->k:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v3, v2}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_1

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->h()V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method private a(Lcom/jscape/filetransfer/FileTransfer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-interface {p1}, Lcom/jscape/filetransfer/FileTransfer;->connect()Lcom/jscape/filetransfer/FileTransfer;

    iget-object v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-interface {v0, p1}, Lcom/jscape/filetransfer/FileTransferOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    return-void
.end method

.method private a(Lcom/jscape/filetransfer/FileTransferOperation;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {p1, v0}, Lcom/jscape/filetransfer/FileTransferOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    :try_end_0
    .catch Lcom/jscape/filetransfer/TransferOperationCancelledException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method static a(Lcom/jscape/filetransfer/UploadDirectoryOperation;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->c()V

    return-void
.end method

.method static a(Lcom/jscape/filetransfer/UploadDirectoryOperation;Lcom/jscape/util/b/s;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Lcom/jscape/util/b/s;)V

    return-void
.end method

.method private a(Lcom/jscape/util/b/s;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/filetransfer/DirectoryService$Directory<",
            "Ljava/io/File;",
            ">;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/jscape/util/b/s;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/filetransfer/DirectoryService$Directory;

    iget-object v1, v1, Lcom/jscape/filetransfer/DirectoryService$Directory;->path:Ljava/lang/Object;

    check-cast v1, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    iget-object v2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    if-eqz v0, :cond_1

    invoke-interface {v2, v1}, Lcom/jscape/filetransfer/FileTransfer;->exists(Ljava/lang/String;)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v2, :cond_0

    :try_start_1
    iget-object v2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v2, v1}, Lcom/jscape/filetransfer/FileTransfer;->makeDir(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    iget-object v2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    :cond_1
    invoke-interface {v2, v1}, Lcom/jscape/filetransfer/FileTransfer;->setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;

    invoke-virtual {p1}, Lcom/jscape/util/b/s;->c()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/util/b/s;

    :try_start_2
    invoke-direct {p0, v1}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Lcom/jscape/util/b/s;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    if-eqz v0, :cond_4

    if-nez v0, :cond_2

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {p1}, Lcom/jscape/filetransfer/FileTransfer;->setDirUp()Lcom/jscape/filetransfer/FileTransfer;

    :cond_4
    return-void

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private a(Lcom/jscape/util/b/s;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/filetransfer/DirectoryService$Directory<",
            "Ljava/io/File;",
            ">;>;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/jscape/util/b/s;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/filetransfer/DirectoryService$Directory;

    iget-object v0, v0, Lcom/jscape/filetransfer/DirectoryService$Directory;->path:Ljava/lang/Object;

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/jscape/util/b/s;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/filetransfer/DirectoryService$Directory;

    iget-object v1, v1, Lcom/jscape/filetransfer/DirectoryService$Directory;->files:Ljava/util/Collection;

    new-instance v2, Lcom/jscape/filetransfer/RemoteDirectory$PathElementsDirectory;

    invoke-direct {v2, p2}, Lcom/jscape/filetransfer/RemoteDirectory$PathElementsDirectory;-><init>(Ljava/util/List;)V

    invoke-direct {p0, v1, v2}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Ljava/util/Collection;Lcom/jscape/filetransfer/RemoteDirectory;)V

    invoke-virtual {p1}, Lcom/jscape/util/b/s;->c()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/util/b/s;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2, p2}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v1, v2}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Lcom/jscape/util/b/s;Ljava/util/List;)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private a(Ljava/util/Collection;Lcom/jscape/filetransfer/RemoteDirectory;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/io/File;",
            ">;",
            "Lcom/jscape/filetransfer/RemoteDirectory;",
            ")V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-direct {p0, v1, p2}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Ljava/io/File;Lcom/jscape/filetransfer/RemoteDirectory;)Lcom/jscape/filetransfer/UploadFileOperation;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->i:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method static b(Lcom/jscape/filetransfer/UploadDirectoryOperation;)Lcom/jscape/util/b/s;
    .locals 0

    iget-object p0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->l:Lcom/jscape/util/b/s;

    return-object p0
.end method

.method private b()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/filetransfer/UploadDirectoryOperation$1;

    invoke-direct {v0, p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation$1;-><init>(Lcom/jscape/filetransfer/UploadDirectoryOperation;)V

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->e:Lcom/jscape/util/Time;

    invoke-virtual {v1}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v1

    iget v3, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->d:I

    invoke-static {v0, v1, v2, v3}, Lcom/jscape/util/x;->a(Ljava/util/concurrent/Callable;JI)Ljava/lang/Object;

    return-void
.end method

.method private c()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    if-eqz v0, :cond_0

    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->isConnected()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->h:Z

    iget-object v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->connect()Lcom/jscape/filetransfer/FileTransfer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0, v1}, Lcom/jscape/filetransfer/FileTransferOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    :cond_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method static c(Lcom/jscape/filetransfer/UploadDirectoryOperation;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->l()V

    return-void
.end method

.method private d()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->i:Ljava/util/Map;

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->m()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->l:Lcom/jscape/util/b/s;

    invoke-direct {p0, v1, v0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Lcom/jscape/util/b/s;Ljava/util/List;)V

    return-void
.end method

.method private e()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->m:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method private f()Z
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->f:I

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    if-le v1, v2, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method private g()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->f:I

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->j:Ljava/util/concurrent/ExecutorService;

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(I)V

    iget-object v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/FileTransferOperation;

    iget-object v3, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;

    iget-object v5, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->k:Ljava/util/concurrent/BlockingQueue;

    invoke-direct {v4, v2, v5}, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;-><init>(Lcom/jscape/filetransfer/FileTransferOperation;Ljava/util/concurrent/BlockingQueue;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    if-nez v1, :cond_0

    :cond_1
    return-void
.end method

.method private h()V
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->k:Ljava/util/concurrent/BlockingQueue;

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    move-object v2, p0

    goto :goto_1

    :cond_1
    move-object v2, p0

    :goto_0
    check-cast v1, Lcom/jscape/filetransfer/FileTransfer;

    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->close()V

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    :goto_1
    iget-object v1, v2, Lcom/jscape/filetransfer/UploadDirectoryOperation;->k:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :cond_3
    :goto_2
    return-void
.end method

.method private i()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->m:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->h()V

    throw v0

    :catch_0
    :goto_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->h()V

    return-void
.end method

.method private j()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/TransferOperationCancelledException;
        }
    .end annotation

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->n:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/filetransfer/TransferOperationCancelledException;

    invoke-direct {v0}, Lcom/jscape/filetransfer/TransferOperationCancelledException;-><init>()V

    throw v0
    :try_end_0
    .catch Lcom/jscape/filetransfer/TransferOperationCancelledException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private k()V
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->j:Ljava/util/concurrent/ExecutorService;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    iput-object v2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->j:Ljava/util/concurrent/ExecutorService;

    :cond_0
    iput-object v2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->l:Lcom/jscape/util/b/s;

    iput-object v2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->m:Ljava/util/concurrent/CountDownLatch;

    :cond_1
    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->h:Z

    if-eqz v0, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->l()V

    :cond_3
    return-void
.end method

.method private l()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private m()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->getDir()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private n()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-direct {p0, v2}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Lcom/jscape/filetransfer/FileTransferOperation;)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method


# virtual methods
.method public bridge synthetic applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/UploadDirectoryOperation;

    move-result-object p1

    return-object p1
.end method

.method public applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/UploadDirectoryOperation;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iput-object p1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->g:Lcom/jscape/filetransfer/FileTransfer;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->n:Z

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->b()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->d()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->e()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->f()Z

    move-result p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p1, :cond_0

    :try_start_2
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->g()V

    if-nez v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->n()V

    :cond_1
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->i()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->j()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->k()V

    return-object p0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_1
    invoke-direct {p0}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->k()V

    throw p1
.end method

.method public cancel()V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->n:Z

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/UploadFileOperation;

    invoke-virtual {v2}, Lcom/jscape/filetransfer/UploadFileOperation;->cancel()V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public cancel(Ljava/io/File;)V
    .locals 1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a(Ljava/io/File;)Lcom/jscape/filetransfer/UploadFileOperation;

    move-result-object p1

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/jscape/filetransfer/UploadFileOperation;->cancel()V

    :cond_1
    return-void
.end method

.method public fileCount()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public onOperationCompleted(Lcom/jscape/filetransfer/UploadFileOperation;)V
    .locals 0

    iget-object p1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->m:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/UploadDirectoryOperation;->o:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->b:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v2, 0xa

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->e:Lcom/jscape/util/Time;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->f:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->l:Lcom/jscape/util/b/s;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/filetransfer/UploadDirectoryOperation;->n:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
