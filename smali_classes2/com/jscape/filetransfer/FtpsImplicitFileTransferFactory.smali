.class public Lcom/jscape/filetransfer/FtpsImplicitFileTransferFactory;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransferFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public transferFor(Lcom/jscape/filetransfer/FileTransferParameters;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferFactory$OperationException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/filetransfer/FtpsImplicitTransfer;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->getHostname()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->getUsername()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->getPassword()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/jscape/filetransfer/FtpsImplicitTransfer;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/jscape/filetransfer/FileTransferParameters;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    new-instance v0, Lcom/jscape/filetransfer/FileTransferFactory$OperationException;

    invoke-direct {v0, p1}, Lcom/jscape/filetransfer/FileTransferFactory$OperationException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method
