.class public abstract enum Lcom/jscape/filetransfer/AftpSecurityMode;
.super Ljava/lang/Enum;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransferOperation;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/filetransfer/AftpSecurityMode;",
        ">;",
        "Lcom/jscape/filetransfer/FileTransferOperation;"
    }
.end annotation


# static fields
.field public static final enum CREDENTIALS_AND_DATA:Lcom/jscape/filetransfer/AftpSecurityMode;

.field public static final enum CREDENTIALS_ONLY:Lcom/jscape/filetransfer/AftpSecurityMode;

.field public static final enum NONE:Lcom/jscape/filetransfer/AftpSecurityMode;

.field private static final a:[Lcom/jscape/filetransfer/AftpSecurityMode;

.field private static final b:[Ljava/lang/String;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x7

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "S}nzJ\u001c*Yngm\u000f\u001d0\\v\u0010s]NZj<\nyNGMp=\u0010|V\u0012eaxk_\u00021B{nz\u000f\u001f1Tj1>\u0012S}nzJ\u001c*Yngm\u000fT~Tn\u007f\u007f\u0004~@E["

    const/16 v5, 0x4c

    const/16 v6, 0x10

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x75

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x5

    const/4 v0, 0x4

    const/4 v2, 0x2

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v2, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v2

    const/4 v0, 0x7

    goto :goto_0

    :cond_0
    const/16 v5, 0x19

    const-string v4, "?\u0001\u0004\u001a\u0014\u0012</;\u000b]k\u0018/&,\u0011Rq\u00151.>\u001aR"

    move v6, v0

    move v8, v2

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v6, v0

    move v8, v11

    :goto_3
    const/16 v9, 0x14

    add-int/2addr v7, v10

    add-int v0, v7, v6

    invoke-virtual {v4, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    const/4 v0, 0x7

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/filetransfer/AftpSecurityMode;->b:[Ljava/lang/String;

    new-instance v1, Lcom/jscape/filetransfer/AftpSecurityMode$1;

    sget-object v4, Lcom/jscape/filetransfer/AftpSecurityMode;->b:[Ljava/lang/String;

    aget-object v0, v4, v0

    aget-object v5, v4, v15

    invoke-direct {v1, v0, v3, v5}, Lcom/jscape/filetransfer/AftpSecurityMode$1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/jscape/filetransfer/AftpSecurityMode;->NONE:Lcom/jscape/filetransfer/AftpSecurityMode;

    new-instance v0, Lcom/jscape/filetransfer/AftpSecurityMode$2;

    const/4 v1, 0x6

    aget-object v1, v4, v1

    const/4 v5, 0x3

    aget-object v6, v4, v5

    invoke-direct {v0, v1, v10, v6}, Lcom/jscape/filetransfer/AftpSecurityMode$2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/filetransfer/AftpSecurityMode;->CREDENTIALS_AND_DATA:Lcom/jscape/filetransfer/AftpSecurityMode;

    new-instance v0, Lcom/jscape/filetransfer/AftpSecurityMode$3;

    aget-object v1, v4, v10

    aget-object v4, v4, v3

    invoke-direct {v0, v1, v2, v4}, Lcom/jscape/filetransfer/AftpSecurityMode$3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/filetransfer/AftpSecurityMode;->CREDENTIALS_ONLY:Lcom/jscape/filetransfer/AftpSecurityMode;

    new-array v1, v5, [Lcom/jscape/filetransfer/AftpSecurityMode;

    sget-object v4, Lcom/jscape/filetransfer/AftpSecurityMode;->NONE:Lcom/jscape/filetransfer/AftpSecurityMode;

    aput-object v4, v1, v3

    sget-object v3, Lcom/jscape/filetransfer/AftpSecurityMode;->CREDENTIALS_AND_DATA:Lcom/jscape/filetransfer/AftpSecurityMode;

    aput-object v3, v1, v10

    aput-object v0, v1, v2

    sput-object v1, Lcom/jscape/filetransfer/AftpSecurityMode;->a:[Lcom/jscape/filetransfer/AftpSecurityMode;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v3, v14, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v10, :cond_8

    if-eq v3, v2, :cond_7

    const/4 v2, 0x3

    if-eq v3, v2, :cond_6

    if-eq v3, v0, :cond_5

    if-eq v3, v15, :cond_4

    const/16 v0, 0x2b

    goto :goto_4

    :cond_4
    const/4 v0, 0x7

    goto :goto_4

    :cond_5
    const/16 v0, 0x5a

    goto :goto_4

    :cond_6
    const/16 v0, 0x6b

    goto :goto_4

    :cond_7
    const/16 v0, 0x7e

    goto :goto_4

    :cond_8
    const/16 v0, 0x7a

    goto :goto_4

    :cond_9
    const/16 v0, 0x45

    :goto_4
    xor-int/2addr v0, v9

    xor-int v0, v16, v0

    int-to-char v0, v0

    aput-char v0, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v0, 0x7

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/jscape/filetransfer/AftpSecurityMode;->name:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/jscape/filetransfer/AftpSecurityMode$1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/filetransfer/AftpSecurityMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method

.method public static modeFor(Ljava/lang/String;)Lcom/jscape/filetransfer/AftpSecurityMode;
    .locals 6

    invoke-static {}, Lcom/jscape/filetransfer/AftpSecurityMode;->values()[Lcom/jscape/filetransfer/AftpSecurityMode;

    move-result-object v0

    array-length v1, v0

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v5, v4, Lcom/jscape/filetransfer/AftpSecurityMode;->name:Ljava/lang/String;

    invoke-virtual {v5, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v5, :cond_0

    return-object v4

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/filetransfer/AftpSecurityMode;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/filetransfer/AftpSecurityMode;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_1
    if-eqz v2, :cond_2

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/filetransfer/AftpSecurityMode;->b:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/filetransfer/AftpSecurityMode;
    .locals 1

    const-class v0, Lcom/jscape/filetransfer/AftpSecurityMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/filetransfer/AftpSecurityMode;

    return-object p0
.end method

.method public static values()[Lcom/jscape/filetransfer/AftpSecurityMode;
    .locals 1

    sget-object v0, Lcom/jscape/filetransfer/AftpSecurityMode;->a:[Lcom/jscape/filetransfer/AftpSecurityMode;

    invoke-virtual {v0}, [Lcom/jscape/filetransfer/AftpSecurityMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/filetransfer/AftpSecurityMode;

    return-object v0
.end method


# virtual methods
.method public abstract applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method
