.class public abstract Lcom/jscape/filetransfer/FileTransferEvent;
.super Ljava/util/EventObject;


# static fields
.field private static b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "L6Sqec"

    invoke-static {v0}, Lcom/jscape/filetransfer/FileTransferEvent;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected constructor <init>(Lcom/jscape/filetransfer/FileTransfer;)V
    .locals 0

    invoke-direct {p0, p1}, Ljava/util/EventObject;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/filetransfer/FileTransferEvent;->b:Ljava/lang/String;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/filetransfer/FileTransferEvent;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public abstract accept(Lcom/jscape/filetransfer/FileTransferListener;)V
.end method
