.class public Lcom/jscape/filetransfer/FileTransferParameters;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransferOperation;


# static fields
.field private static final o:[Ljava/lang/String;


# instance fields
.field protected connectionTimeout:Lcom/jscape/util/Time;

.field protected debugStream:Ljava/io/PrintStream;

.field protected hostname:Ljava/lang/String;

.field protected localDirectory:Ljava/io/File;

.field protected password:Ljava/lang/String;

.field protected port:Ljava/lang/Integer;

.field protected preserveFileTimestampOnDownload:Ljava/lang/Boolean;

.field protected preserveFileTimestampOnUpload:Ljava/lang/Boolean;

.field protected protocol:Lcom/jscape/filetransfer/Protocol;

.field protected useDebugMode:Ljava/lang/Boolean;

.field protected username:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Lgk\u0005-\u0002G33}\u0005.\u001a\u001d\u000cLgg\u000f<\u0003N\u0001*j]h\u000cLg\u007f\u0001<\u0004W\u000f5k]h\u0007Lg\u007f\u000f=\u0003\u001d\u000fLgz\u0013*3E\u00022h- \u0013E]\u0014Lgl\u000f!\u0019E\u00033f\u000f!#I\r\"`\u0015;J Lg\u007f\u0012*\u0004E\u00121j&&\u001bE4.b\u0005<\u0003A\r7@\u000e\u001a\u0007L\u000f&k]!&.c\u0005\u001b\u0005A\u000e4i\u0005=\'A\u0012&b\u0005;\u0012R\u0013gt\u0010=\u0018T\u000f$`\u000cr"

    const/16 v5, 0x98

    move v8, v3

    const/4 v6, -0x1

    const/16 v7, 0xe

    :goto_0
    const/16 v9, 0x41

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x2f

    const/16 v4, 0x22

    const-string v6, "P{c\u000e6\u0018Y\u000e-v::\u0007Y(2~\u0019 \u001f]\u0011+\\\u0012\u0017\u0004K\u00127|\u001d7V\u000cP{f\u000f6\u0019R\u001d6vAt"

    move v7, v4

    move-object v4, v6

    move v8, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x5d

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/filetransfer/FileTransferParameters;->o:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v1, v14, 0x7

    const/16 v16, 0x21

    if-eqz v1, :cond_8

    if-eq v1, v10, :cond_7

    const/4 v2, 0x2

    if-eq v1, v2, :cond_6

    const/4 v2, 0x3

    if-eq v1, v2, :cond_8

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v16, 0x61

    goto :goto_4

    :cond_4
    const/16 v16, 0x36

    goto :goto_4

    :cond_5
    const/16 v16, 0xe

    goto :goto_4

    :cond_6
    const/16 v16, 0x4e

    goto :goto_4

    :cond_7
    const/16 v16, 0x6

    :cond_8
    :goto_4
    xor-int v1, v9, v16

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/Protocol;Ljava/lang/String;Ljava/lang/Integer;Lcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/io/PrintStream;Ljava/io/File;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->protocol:Lcom/jscape/filetransfer/Protocol;

    iput-object p2, p0, Lcom/jscape/filetransfer/FileTransferParameters;->hostname:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/filetransfer/FileTransferParameters;->port:Ljava/lang/Integer;

    iput-object p4, p0, Lcom/jscape/filetransfer/FileTransferParameters;->connectionTimeout:Lcom/jscape/util/Time;

    iput-object p5, p0, Lcom/jscape/filetransfer/FileTransferParameters;->username:Ljava/lang/String;

    iput-object p6, p0, Lcom/jscape/filetransfer/FileTransferParameters;->password:Ljava/lang/String;

    iput-object p7, p0, Lcom/jscape/filetransfer/FileTransferParameters;->preserveFileTimestampOnUpload:Ljava/lang/Boolean;

    iput-object p8, p0, Lcom/jscape/filetransfer/FileTransferParameters;->preserveFileTimestampOnDownload:Ljava/lang/Boolean;

    iput-object p9, p0, Lcom/jscape/filetransfer/FileTransferParameters;->useDebugMode:Ljava/lang/Boolean;

    iput-object p10, p0, Lcom/jscape/filetransfer/FileTransferParameters;->debugStream:Ljava/io/PrintStream;

    iput-object p11, p0, Lcom/jscape/filetransfer/FileTransferParameters;->localDirectory:Ljava/io/File;

    return-void
.end method

.method private static a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->hostname:Ljava/lang/String;
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->hostname:Ljava/lang/String;

    invoke-interface {p1, v1}, Lcom/jscape/filetransfer/FileTransfer;->setHostname(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->port:Ljava/lang/Integer;
    :try_end_2
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v1, :cond_1

    :try_start_3
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->port:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {p1, v1}, Lcom/jscape/filetransfer/FileTransfer;->setPort(I)Lcom/jscape/filetransfer/FileTransfer;

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    :try_start_4
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->connectionTimeout:Lcom/jscape/util/Time;
    :try_end_4
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_4 .. :try_end_4} :catch_4

    if-eqz v1, :cond_2

    :try_start_5
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->connectionTimeout:Lcom/jscape/util/Time;

    invoke-virtual {v1}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v1

    invoke-interface {p1, v1, v2}, Lcom/jscape/filetransfer/FileTransfer;->setTimeout(J)Lcom/jscape/filetransfer/FileTransfer;

    goto :goto_2

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_2
    :goto_2
    :try_start_6
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->username:Ljava/lang/String;
    :try_end_6
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_6 .. :try_end_6} :catch_11

    if-eqz v0, :cond_4

    if-eqz v1, :cond_3

    :try_start_7
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->username:Ljava/lang/String;

    invoke-interface {p1, v1}, Lcom/jscape/filetransfer/FileTransfer;->setUsername(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_7
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_7 .. :try_end_7} :catch_12

    :cond_3
    if-eqz v0, :cond_5

    :try_start_8
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->password:Ljava/lang/String;
    :try_end_8
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_3

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_4
    :goto_3
    if-eqz v1, :cond_5

    :try_start_9
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->password:Ljava/lang/String;

    invoke-interface {p1, v1}, Lcom/jscape/filetransfer/FileTransfer;->setPassword(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_9
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_9 .. :try_end_9} :catch_7

    goto :goto_4

    :catch_7
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_5
    :goto_4
    :try_start_a
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->preserveFileTimestampOnUpload:Ljava/lang/Boolean;
    :try_end_a
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_a .. :try_end_a} :catch_f

    if-eqz v0, :cond_7

    if-eqz v1, :cond_6

    :try_start_b
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->preserveFileTimestampOnUpload:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {p1, v1}, Lcom/jscape/filetransfer/FileTransfer;->setPreserveFileUploadTimestamp(Z)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_b
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_b .. :try_end_b} :catch_10

    :cond_6
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->preserveFileTimestampOnDownload:Ljava/lang/Boolean;

    :cond_7
    if-eqz v0, :cond_9

    if-eqz v1, :cond_8

    :try_start_c
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->preserveFileTimestampOnDownload:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {p1, v1}, Lcom/jscape/filetransfer/FileTransfer;->setPreserveFileDownloadTimestamp(Z)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_c
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_c .. :try_end_c} :catch_8

    goto :goto_5

    :catch_8
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_8
    :goto_5
    if-eqz v0, :cond_a

    :try_start_d
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->useDebugMode:Ljava/lang/Boolean;
    :try_end_d
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_d .. :try_end_d} :catch_9

    goto :goto_6

    :catch_9
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_9
    :goto_6
    if-eqz v1, :cond_a

    :try_start_e
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->useDebugMode:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {p1, v1}, Lcom/jscape/filetransfer/FileTransfer;->setDebug(Z)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_e
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_e .. :try_end_e} :catch_a

    goto :goto_7

    :catch_a
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_a
    :goto_7
    if-eqz v0, :cond_b

    :try_start_f
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->debugStream:Ljava/io/PrintStream;
    :try_end_f
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_f .. :try_end_f} :catch_b

    if-eqz v1, :cond_b

    :try_start_10
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->debugStream:Ljava/io/PrintStream;

    invoke-interface {p1, v1}, Lcom/jscape/filetransfer/FileTransfer;->setDebugStream(Ljava/io/PrintStream;)Lcom/jscape/filetransfer/FileTransfer;

    goto :goto_8

    :catch_b
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_10
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_10 .. :try_end_10} :catch_c

    :catch_c
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_b
    :goto_8
    if-eqz v0, :cond_c

    :try_start_11
    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferParameters;->localDirectory:Ljava/io/File;
    :try_end_11
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_11 .. :try_end_11} :catch_d

    if-eqz v0, :cond_c

    :try_start_12
    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferParameters;->localDirectory:Ljava/io/File;

    invoke-interface {p1, v0}, Lcom/jscape/filetransfer/FileTransfer;->setLocalDir(Ljava/io/File;)Lcom/jscape/filetransfer/FileTransfer;

    goto :goto_9

    :catch_d
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_12
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_12 .. :try_end_12} :catch_e

    :catch_e
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_c
    :goto_9
    return-object p0

    :catch_f
    move-exception p1

    :try_start_13
    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_13
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_13 .. :try_end_13} :catch_10

    :catch_10
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :catch_11
    move-exception p1

    :try_start_14
    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_14
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_14 .. :try_end_14} :catch_12

    :catch_12
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getConnectionTimeout()Lcom/jscape/util/Time;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferParameters;->connectionTimeout:Lcom/jscape/util/Time;

    return-object v0
.end method

.method public getDebugStream()Ljava/io/PrintStream;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferParameters;->debugStream:Ljava/io/PrintStream;

    return-object v0
.end method

.method public getHostname()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferParameters;->hostname:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalDirectory()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferParameters;->localDirectory:Ljava/io/File;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferParameters;->password:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferParameters;->port:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPreserveFileTimestampOnDownload()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferParameters;->preserveFileTimestampOnDownload:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getPreserveFileTimestampOnUpload()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferParameters;->preserveFileTimestampOnUpload:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getProtocol()Lcom/jscape/filetransfer/Protocol;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferParameters;->protocol:Lcom/jscape/filetransfer/Protocol;

    return-object v0
.end method

.method public getUseDebugMode()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferParameters;->useDebugMode:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferParameters;->username:Ljava/lang/String;

    return-object v0
.end method

.method public setConnectionTimeout(Lcom/jscape/util/Time;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->connectionTimeout:Lcom/jscape/util/Time;

    return-void
.end method

.method public setDebugStream(Ljava/io/PrintStream;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->debugStream:Ljava/io/PrintStream;

    return-void
.end method

.method public setHostname(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->hostname:Ljava/lang/String;

    return-void
.end method

.method public setLocalDirectory(Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->localDirectory:Ljava/io/File;

    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->password:Ljava/lang/String;

    return-void
.end method

.method public setPort(Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->port:Ljava/lang/Integer;

    return-void
.end method

.method public setPreserveFileTimestampOnDownload(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->preserveFileTimestampOnDownload:Ljava/lang/Boolean;

    return-void
.end method

.method public setPreserveFileTimestampOnUpload(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->preserveFileTimestampOnUpload:Ljava/lang/Boolean;

    return-void
.end method

.method public setProtocol(Lcom/jscape/filetransfer/Protocol;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->protocol:Lcom/jscape/filetransfer/Protocol;

    return-void
.end method

.method public setUseDebugMode(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->useDebugMode:Ljava/lang/Boolean;

    return-void
.end method

.method public setUsername(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->username:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/FileTransferParameters;->o:[Ljava/lang/String;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferParameters;->protocol:Lcom/jscape/filetransfer/Protocol;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferParameters;->hostname:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x3

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/FileTransferParameters;->port:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/FileTransferParameters;->connectionTimeout:Lcom/jscape/util/Time;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v3, 0x9

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/FileTransferParameters;->username:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/FileTransferParameters;->password:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferParameters;->preserveFileTimestampOnUpload:Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferParameters;->preserveFileTimestampOnDownload:Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferParameters;->useDebugMode:Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferParameters;->debugStream:Ljava/io/PrintStream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
