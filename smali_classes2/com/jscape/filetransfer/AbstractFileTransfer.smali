.class public abstract Lcom/jscape/filetransfer/AbstractFileTransfer;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransfer;


# static fields
.field protected static final DEFAULT_LOCAL_DIRECTORY:Ljava/io/File;

.field protected static final FILE_LISTING_TEMPLATE:Ljava/lang/String;

.field private static final y:[Ljava/lang/String;


# instance fields
.field protected checksumVerificationRequired:Z

.field protected connected:Z

.field protected currentLocalDirectory:Ljava/nio/file/Path;

.field protected downloadedFileTimestampPreservingRequired:Z

.field protected executorService:Ljava/util/concurrent/ExecutorService;

.field protected hostname:Ljava/lang/String;

.field protected volatile interrupted:Z

.field protected final listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/jscape/filetransfer/FileTransferListener;",
            ">;"
        }
    .end annotation
.end field

.field protected final logger:Ljava/util/logging/Logger;

.field protected passiveTransferModeRequired:Z

.field protected password:Ljava/lang/String;

.field protected port:I

.field protected proxyHost:Ljava/lang/String;

.field protected proxyPassword:Ljava/lang/String;

.field protected proxyPort:I

.field protected proxyType:Ljava/lang/String;

.field protected proxyUsername:Ljava/lang/String;

.field protected timeZone:Ljava/util/TimeZone;

.field protected timeout:J

.field protected transferBufferSizeBytes:I

.field protected transferMode:I

.field protected uploadedFileTimestampPreservingRequired:Z

.field protected username:Ljava/lang/String;

.field protected wireEncoding:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "g\\\u0002E\t\u001e1\u000e^\u001bG\u0004\u001buJ[\u0006A\u0006\u0003:\\KZ\u0002\u0000\u0019\u0002\u0000\u0019\u008a\u0012ID\u0008\u0006\u001f:GQ\u0011\u0008UT\u0011g`\u0008\u0015F1\u001cbw\t\u001aE\u000cdS\u0012\u000f\u0016I\u0014=A[\u0017AIGv\\W\u0015@\u0004\u00159KNE\u0007\u000b\u0018;\u0003@\u0011E\u0001\u00167BW\t\u0004\u001eDyMZ\u001bM\u0006\u0012y\u001e\u0011\u0003V\u000c\u00034L^\u0011XTT;A\\YS\u0017\u001e!OP\u0018A\u0018W.\u001aOT_P[1OF\u0011\u0008(:xJVY]\u001c\u000e,\u000ez<\u001e\u0008\u001ao]A\t)o\u0010aB\u0011V\u0004\u0003<A\\TA\u0017\u0005:\\\u001c\u0018g\\\u0002E\t\u001e1\u000e^\u001bG\u0004\u001buJ[\u0006A\u0006\u0003:\\KZ"

    const/16 v4, 0xd3

    const/16 v5, 0x18

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x5f

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0xcb

    const/16 v3, 0x40

    const-string v5, "dW\u0014MI\u00181G]\u0013[\u001c\u0016yOW\u000bE\u0008\u000f:J\u001e\u001eG\u001b[5M]\u0019DI\u001d0N[X\u000fL\u0008~\u0002_\u0016LI\t<OQ\u000cMI\u001d0N[X\u000fL\u0008~\u000c\u008a\u001eEH\u0004\n\u00136K]\u001d\u0004YX\u001dkl\u0004\u0019J=\u0010n{\u0005\u0016I\u0000h_\u001e\u0003\u001aE\u00181MW\u001bMEKzP[\u0019L\u0008\u00195GBI\u000b\u0007\u00147\u000fL\u001dI\r\u001a;N[\u0005\u0008\u0012HuAV\u0017A\n\u001eu\u0012\u001d\u000fZ\u0000\u000f8@R\u001dTXX7MPU_\u001b\u0012-C\\\u0014M\u0014[\"\u0016CXS\\W=CJ\u001d\u0004$6tFZUQ\u0010\u0002 \u0002v0\u0012\u0004\u0016cQM\u0005%c"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x53

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->y:[Ljava/lang/String;

    const/4 v1, 0x7

    aget-object v0, v0, v1

    sput-object v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->FILE_LISTING_TEMPLATE:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    const-string v1, "."

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->DEFAULT_LOCAL_DIRECTORY:Ljava/io/File;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0xa

    goto :goto_4

    :cond_4
    const/16 v1, 0x28

    goto :goto_4

    :cond_5
    const/16 v1, 0x3a

    goto :goto_4

    :cond_6
    const/16 v1, 0x7b

    goto :goto_4

    :cond_7
    const/16 v1, 0x2b

    goto :goto_4

    :cond_8
    const/16 v1, 0x6d

    goto :goto_4

    :cond_9
    const/16 v1, 0x71

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;Ljava/lang/String;IZILjava/lang/String;ZZZLjava/util/TimeZone;Ljava/io/File;Ljava/util/logging/Logger;Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IJ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IZI",
            "Ljava/lang/String;",
            "ZZZ",
            "Ljava/util/TimeZone;",
            "Ljava/io/File;",
            "Ljava/util/logging/Logger;",
            "Ljava/util/Set<",
            "Lcom/jscape/filetransfer/FileTransferListener;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    iput-object v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->proxyType:Ljava/lang/String;

    move-object v1, p2

    iput-object v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->proxyHost:Ljava/lang/String;

    move v1, p3

    iput v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->proxyPort:I

    move-object v1, p4

    iput-object v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->proxyUsername:Ljava/lang/String;

    move-object v1, p5

    iput-object v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->proxyPassword:Ljava/lang/String;

    move-object v1, p6

    iput-object v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->hostname:Ljava/lang/String;

    move v1, p7

    iput v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->port:I

    move-wide v1, p8

    iput-wide v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->timeout:J

    move-object v1, p10

    iput-object v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->username:Ljava/lang/String;

    move-object v1, p11

    iput-object v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->password:Ljava/lang/String;

    move/from16 v1, p12

    iput v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->transferBufferSizeBytes:I

    move/from16 v1, p13

    iput-boolean v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->passiveTransferModeRequired:Z

    move/from16 v1, p14

    iput v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->transferMode:I

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->wireEncoding:Ljava/lang/String;

    move/from16 v1, p16

    iput-boolean v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->uploadedFileTimestampPreservingRequired:Z

    move/from16 v1, p17

    iput-boolean v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->downloadedFileTimestampPreservingRequired:Z

    move/from16 v1, p18

    iput-boolean v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->checksumVerificationRequired:Z

    invoke-static/range {p19 .. p19}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object/from16 v1, p19

    iput-object v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->timeZone:Ljava/util/TimeZone;

    invoke-virtual/range {p20 .. p20}, Ljava/io/File;->isDirectory()Z

    move-result v1

    sget-object v2, Lcom/jscape/filetransfer/AbstractFileTransfer;->y:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-static {v1, v2}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    invoke-virtual/range {p20 .. p20}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object v1

    invoke-interface {v1}, Ljava/nio/file/Path;->normalize()Ljava/nio/file/Path;

    move-result-object v1

    invoke-interface {v1}, Ljava/nio/file/Path;->toAbsolutePath()Ljava/nio/file/Path;

    move-result-object v1

    iput-object v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->currentLocalDirectory:Ljava/nio/file/Path;

    invoke-static/range {p21 .. p21}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object/from16 v1, p21

    iput-object v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->logger:Ljava/util/logging/Logger;

    invoke-static/range {p22 .. p22}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    new-instance v1, Ljava/util/concurrent/CopyOnWriteArraySet;

    move-object/from16 v2, p22

    invoke-direct {v1, v2}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>(Ljava/util/Collection;)V

    iput-object v1, v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->listeners:Ljava/util/Set;

    return-void
.end method

.method private static b(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public addFileTransferListener(Lcom/jscape/filetransfer/FileTransferListener;)V
    .locals 1

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public clearProxySettings()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->proxyType:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->proxyHost:Ljava/lang/String;

    const/4 v1, 0x0

    iput v1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->proxyPort:I

    iput-object v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->proxyUsername:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->proxyPassword:Ljava/lang/String;

    return-void
.end method

.method public close()V
    .locals 0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public deleteDir(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->deleteDir(Ljava/lang/String;Z)V

    return-void
.end method

.method protected disposeExecutorService()V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->executorService:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->executorService:Ljava/util/concurrent/ExecutorService;

    :cond_1
    return-void
.end method

.method public download(Ljava/lang/String;)Ljava/io/File;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    return-object p1
.end method

.method public download(Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->resumeDownload(Ljava/io/OutputStream;Ljava/lang/String;J)V

    return-void
.end method

.method public downloadDir(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->downloadDir(Ljava/lang/String;IZ)V

    return-void
.end method

.method public downloadDir(Ljava/lang/String;IZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->downloadDir(Ljava/lang/String;IZI)V

    return-void
.end method

.method protected formatRemoteFile(Lcom/jscape/filetransfer/FileTransferRemoteFile;)Ljava/lang/String;
    .locals 8

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/filetransfer/AbstractFileTransfer;->y:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->isDirectory()Z

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v0, :cond_1

    if-eqz v4, :cond_0

    move v4, v6

    goto :goto_0

    :cond_0
    move v4, v5

    :cond_1
    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilename()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x2

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->isReadable()Z

    move-result v7

    if-eqz v0, :cond_3

    if-eqz v7, :cond_2

    move v7, v6

    goto :goto_1

    :cond_2
    move v7, v5

    :cond_3
    :goto_1
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v4

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->isWritable()Z

    move-result v4

    if-eqz v0, :cond_4

    if-eqz v4, :cond_5

    move v5, v6

    goto :goto_2

    :cond_4
    move v5, v4

    :cond_5
    :goto_2
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v2

    const/4 v0, 0x4

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilesize()J

    move-result-wide v4

    long-to-double v4, v4

    invoke-static {v4, v5}, Lcom/jscape/util/C;->a(D)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v0

    const/4 v0, 0x5

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFileDate()Ljava/util/Date;

    move-result-object p1

    aput-object p1, v3, v0

    invoke-static {v1, v3}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getBlockTransferSize()I
    .locals 1

    iget v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->transferBufferSizeBytes:I

    return v0
.end method

.method public getDebug()Z
    .locals 2

    iget-object v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->logger:Ljava/util/logging/Logger;

    invoke-virtual {v0}, Ljava/util/logging/Logger;->getLevel()Ljava/util/logging/Level;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getDebugStream()Ljava/io/PrintStream;
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->logger:Ljava/util/logging/Logger;

    if-eqz v0, :cond_1

    instance-of v0, v1, Lcom/jscape/util/j/b;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    check-cast v1, Lcom/jscape/util/j/b;

    invoke-virtual {v1}, Lcom/jscape/util/j/b;->a()Ljava/io/OutputStream;

    move-result-object v0

    check-cast v0, Ljava/io/PrintStream;

    :goto_1
    return-object v0
.end method

.method public getDirListing()Ljava/util/Enumeration;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->y:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getDirListingAsString()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->y:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->getDirListingAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDirListingAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    invoke-virtual {p0, v2}, Lcom/jscape/filetransfer/AbstractFileTransfer;->formatRemoteFile(Lcom/jscape/filetransfer/FileTransferRemoteFile;)Ljava/lang/String;

    move-result-object v2

    :try_start_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getHostname()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->hostname:Ljava/lang/String;

    return-object v0
.end method

.method public getInputStream(Ljava/lang/String;J)Ljava/io/InputStream;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    invoke-static {p0, p1, p2, p3}, Lcom/jscape/filetransfer/FileTransferPipedInputStream;->streamFor(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;J)Lcom/jscape/filetransfer/FileTransferPipedInputStream;

    move-result-object p1

    iget-object p2, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->executorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p2, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getLocalDir()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->currentLocalDirectory:Ljava/nio/file/Path;

    invoke-interface {v0}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public getLocalDirListing()Ljava/util/Enumeration;
    .locals 2

    sget-object v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->y:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->getLocalDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getLocalDirListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 3

    invoke-virtual {p0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->getLocalDir()Ljava/io/File;

    move-result-object v0

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/jscape/filetransfer/AbstractFileTransfer$1;

    invoke-direct {v2, p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer$1;-><init>(Lcom/jscape/filetransfer/AbstractFileTransfer;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object p1

    if-eqz v1, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    goto :goto_1

    :cond_1
    :goto_0
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->enumeration(Ljava/util/Collection;)Ljava/util/Enumeration;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public getMode()I
    .locals 1

    iget v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->transferMode:I

    return v0
.end method

.method public getNameListing()Ljava/util/Enumeration;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->y:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->getNameListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getNameListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {v2}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    return-object p1
.end method

.method public getOutputStream(Ljava/lang/String;J)Ljava/io/OutputStream;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    :try_start_0
    invoke-static/range {v0 .. v5}, Lcom/jscape/filetransfer/FileTransferPipedOutputStream;->streamFor(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;JJ)Lcom/jscape/filetransfer/FileTransferPipedOutputStream;

    move-result-object p1

    iget-object p2, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->executorService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p2, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getPassive()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->passiveTransferModeRequired:Z

    return v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->password:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    iget v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->port:I

    return v0
.end method

.method public getRecursiveDirectoryFileCount(Ljava/lang/String;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    :try_start_0
    invoke-virtual {v2}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->isDirectory()Z

    move-result v2
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_3

    if-nez v2, :cond_1

    add-int/lit8 v1, v1, 0x1

    :cond_1
    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    move v2, v1

    :cond_3
    return v2
.end method

.method public getRecursiveDirectorySize(Ljava/lang/String;)J
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x0

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    invoke-virtual {v3}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilesize()J

    move-result-wide v3

    add-long/2addr v1, v3

    if-eqz v0, :cond_1

    if-nez v0, :cond_0

    :cond_1
    return-wide v1
.end method

.method public getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    new-instance v0, Ljava/util/Vector;

    new-instance v1, Lcom/jscape/filetransfer/RecursiveFileListOperation;

    invoke-direct {v1, p1}, Lcom/jscape/filetransfer/RecursiveFileListOperation;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lcom/jscape/filetransfer/RecursiveFileListOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/RecursiveFileListOperation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/filetransfer/RecursiveFileListOperation;->files()Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/Vector;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getTimeout()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->timeout:J

    return-wide v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->username:Ljava/lang/String;

    return-object v0
.end method

.method public getWireEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->wireEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public interrupt()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->interrupted:Z

    return-void
.end method

.method public interrupted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->interrupted:Z

    return v0
.end method

.method public isChecksumVerificationRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->checksumVerificationRequired:Z

    return v0
.end method

.method protected logError(Ljava/lang/Exception;)V
    .locals 4

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/filetransfer/AbstractFileTransfer;->y:[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method public makeDir(Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->makeDirRecursive(Ljava/lang/String;)V

    return-void
.end method

.method public makeLocalDir(Ljava/lang/String;)Ljava/io/File;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->resolveLocalPathFor(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/Q;->b(Ljava/io/File;)Ljava/io/File;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public mdelete(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {v1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->isDirectory()Z

    move-result v2
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_2

    :try_start_1
    invoke-virtual {v1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilename()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/jscape/filetransfer/AbstractFileTransfer;->deleteDir(Ljava/lang/String;Z)V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    if-nez v0, :cond_3

    :cond_2
    :try_start_2
    invoke-virtual {v1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilename()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->deleteFile(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    if-nez v0, :cond_0

    :cond_4
    return-void
.end method

.method public mdownload(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz v0, :cond_1

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->interrupted:Z
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_3

    :try_start_1
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    move-object v1, p0

    :goto_0
    check-cast v1, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    :try_start_2
    invoke-virtual {v1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilename()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->download(Ljava/lang/String;)Ljava/io/File;
    :try_end_2
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    if-nez v0, :cond_0

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    return-void
.end method

.method public mdownload(Ljava/util/Enumeration;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_1

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->interrupted:Z
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_2

    :try_start_1
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    move-object v1, p0

    :goto_0
    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->download(Ljava/lang/String;)Ljava/io/File;

    if-nez v0, :cond_0

    :cond_2
    return-void
.end method

.method public mupload(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->getLocalDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz v0, :cond_1

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->interrupted:Z
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_3

    :try_start_1
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    move-object v1, p0

    :goto_0
    check-cast v1, Ljava/io/File;

    :try_start_2
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0, v1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->upload(Ljava/io/File;)V
    :try_end_2
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    if-nez v0, :cond_0

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    return-void
.end method

.method public mupload(Ljava/util/Enumeration;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_1

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->interrupted:Z
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_2

    :try_start_1
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    move-object v1, p0

    :goto_0
    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->upload(Ljava/lang/String;)V

    if-nez v0, :cond_0

    :cond_2
    return-void
.end method

.method protected raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->listeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/FileTransferListener;

    invoke-virtual {p1, v2}, Lcom/jscape/filetransfer/FileTransferEvent;->accept(Lcom/jscape/filetransfer/FileTransferListener;)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public removeFileTransferListener(Lcom/jscape/filetransfer/FileTransferListener;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->interrupted:Z

    return-void
.end method

.method protected resolveLocalPathFor(Ljava/lang/String;)Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->currentLocalDirectory:Ljava/nio/file/Path;

    invoke-interface {v0, p1}, Ljava/nio/file/Path;->resolve(Ljava/lang/String;)Ljava/nio/file/Path;

    move-result-object p1

    invoke-interface {p1}, Ljava/nio/file/Path;->normalize()Ljava/nio/file/Path;

    move-result-object p1

    invoke-interface {p1}, Ljava/nio/file/Path;->toAbsolutePath()Ljava/nio/file/Path;

    move-result-object p1

    invoke-interface {p1}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object p1

    return-object p1
.end method

.method public resumeDownload(Ljava/lang/String;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1, p1, p2, p3}, Lcom/jscape/filetransfer/AbstractFileTransfer;->resumeDownload(Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method public resumeUpload(Ljava/io/File;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/jscape/filetransfer/AbstractFileTransfer;->resumeUpload(Ljava/io/File;Ljava/lang/String;J)V

    return-void
.end method

.method public resumeUpload(Ljava/lang/String;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->resolveLocalPathFor(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/filetransfer/AbstractFileTransfer;->resumeUpload(Ljava/io/File;J)V

    return-void
.end method

.method protected safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->checksumVerificationRequired:Z
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p0, p1, p2}, Lcom/jscape/filetransfer/AbstractFileTransfer;->sameChecksum(Ljava/io/File;Ljava/lang/String;)Z

    move-result v1
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    goto :goto_1

    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    goto :goto_2

    :cond_2
    :try_start_2
    new-instance v0, Ljava/lang/Exception;

    sget-object v1, Lcom/jscape/filetransfer/AbstractFileTransfer;->y:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    aput-object p2, v2, p1

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_3 .. :try_end_3} :catch_0

    :goto_1
    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->logError(Ljava/lang/Exception;)V

    :goto_2
    return-void

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method protected safeSetModificationTime(Ljava/io/File;J)V
    .locals 1

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->downloadedFileTimestampPreservingRequired:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p1

    invoke-static {p2, p3}, Ljava/nio/file/attribute/FileTime;->fromMillis(J)Ljava/nio/file/attribute/FileTime;

    move-result-object p2

    invoke-static {p1, p2}, Ljava/nio/file/Files;->setLastModifiedTime(Ljava/nio/file/Path;Ljava/nio/file/attribute/FileTime;)Ljava/nio/file/Path;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->logError(Ljava/lang/Exception;)V

    :goto_0
    return-void

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method protected safeSetModificationTime(Ljava/io/File;Ljava/lang/String;)V
    .locals 3

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->uploadedFileTimestampPreservingRequired:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p0, p2, v0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->logError(Ljava/lang/Exception;)V

    :goto_0
    return-void

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method protected safeSetModificationTime(Ljava/lang/String;Ljava/io/File;)V
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->downloadedFileTimestampPreservingRequired:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p2}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p2

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/nio/file/attribute/FileTime;->fromMillis(J)Ljava/nio/file/attribute/FileTime;

    move-result-object p1

    invoke-static {p2, p1}, Ljava/nio/file/Files;->setLastModifiedTime(Ljava/nio/file/Path;Ljava/nio/file/attribute/FileTime;)Ljava/nio/file/Path;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception p1

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->logError(Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method

.method protected safeSizeOf(Ljava/lang/String;)J
    .locals 2

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->getFilesize(Ljava/lang/String;)J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    :catch_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public setAscii()Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    const/4 v0, 0x1

    iput v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->transferMode:I

    return-object p0
.end method

.method public setAuto(Z)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    :cond_1
    :goto_0
    iput p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->transferMode:I

    return-object p0
.end method

.method public setBinary()Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->transferMode:I

    return-object p0
.end method

.method public setBlockTransferSize(I)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iput p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->transferBufferSizeBytes:I

    return-object p0
.end method

.method public setChecksumVerificationRequired(Z)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->checksumVerificationRequired:Z

    return-object p0
.end method

.method public setDebug(Z)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->logger:Ljava/util/logging/Logger;

    if-eqz p1, :cond_0

    sget-object p1, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    goto :goto_0

    :cond_0
    sget-object p1, Ljava/util/logging/Level;->OFF:Ljava/util/logging/Level;

    :goto_0
    invoke-virtual {v0, p1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    return-object p0
.end method

.method public setDebugStream(Ljava/io/PrintStream;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 2

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->logger:Ljava/util/logging/Logger;

    instance-of v1, v0, Lcom/jscape/util/j/b;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/jscape/util/j/b;

    invoke-virtual {v0, p1}, Lcom/jscape/util/j/b;->a(Ljava/io/OutputStream;)V

    :cond_0
    return-object p0
.end method

.method public setFileModificationTime(Ljava/lang/String;Ljava/util/Date;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/jscape/filetransfer/AbstractFileTransfer;->setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)Lcom/jscape/filetransfer/FileTransfer;

    return-object p0
.end method

.method public setHostname(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->hostname:Ljava/lang/String;

    return-object p0
.end method

.method public setLocalDir(Ljava/io/File;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 3

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    sget-object v1, Lcom/jscape/filetransfer/AbstractFileTransfer;->y:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p1

    invoke-interface {p1}, Ljava/nio/file/Path;->normalize()Ljava/nio/file/Path;

    move-result-object p1

    invoke-interface {p1}, Ljava/nio/file/Path;->toAbsolutePath()Ljava/nio/file/Path;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->currentLocalDirectory:Ljava/nio/file/Path;

    return-object p0
.end method

.method public setPassive(Z)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->passiveTransferModeRequired:Z

    return-object p0
.end method

.method public setPassword(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->password:Ljava/lang/String;

    return-object p0
.end method

.method public setPort(I)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iput p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->port:I

    return-object p0
.end method

.method public setPreserveFileDownloadTimestamp(Z)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->downloadedFileTimestampPreservingRequired:Z

    return-object p0
.end method

.method public setPreserveFileUploadTimestamp(Z)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->uploadedFileTimestampPreservingRequired:Z

    return-object p0
.end method

.method public setProxyAuthentication(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->proxyUsername:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->proxyPassword:Ljava/lang/String;

    return-object p0
.end method

.method public setProxyHost(Ljava/lang/String;I)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->proxyHost:Ljava/lang/String;

    iput p2, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->proxyPort:I

    return-object p0
.end method

.method public setProxyType(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->proxyType:Ljava/lang/String;

    return-object p0
.end method

.method public setTimeZone(Ljava/util/TimeZone;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->timeZone:Ljava/util/TimeZone;

    return-object p0
.end method

.method public setTimeout(J)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iput-wide p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->timeout:J

    return-object p0
.end method

.method public setUsername(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->username:Ljava/lang/String;

    return-object p0
.end method

.method public setWireEncoding(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AbstractFileTransfer;->wireEncoding:Ljava/lang/String;

    return-object p0
.end method

.method public upload(Ljava/io/File;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->upload(Ljava/io/File;Z)V

    return-void
.end method

.method public upload(Ljava/io/File;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->upload(Ljava/io/File;Ljava/lang/String;Z)V

    return-void
.end method

.method public upload(Ljava/io/File;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/jscape/filetransfer/AbstractFileTransfer;->upload(Ljava/io/File;Ljava/lang/String;Z)V

    return-void
.end method

.method public upload(Ljava/io/InputStream;JLjava/lang/String;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    if-eqz p5, :cond_0

    :try_start_0
    invoke-virtual {p0, p4}, Lcom/jscape/filetransfer/AbstractFileTransfer;->safeSizeOf(Ljava/lang/String;)J

    move-result-wide v0
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    move-wide v7, v0

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-virtual/range {v2 .. v8}, Lcom/jscape/filetransfer/AbstractFileTransfer;->resumeUpload(Ljava/io/InputStream;JLjava/lang/String;J)V

    return-void
.end method

.method public upload(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->upload(Ljava/lang/String;Z)V

    return-void
.end method

.method public upload(Ljava/lang/String;Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->upload(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/jscape/filetransfer/AbstractFileTransfer;->renameFile(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public upload(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->upload(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public upload(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->resolveLocalPathFor(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/filetransfer/AbstractFileTransfer;->upload(Ljava/io/File;Ljava/lang/String;Z)V

    return-void
.end method

.method public upload(Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->resolveLocalPathFor(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/jscape/filetransfer/AbstractFileTransfer;->upload(Ljava/io/File;Ljava/lang/String;Z)V

    return-void
.end method

.method public upload([BLjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->upload([BLjava/lang/String;Z)V

    return-void
.end method

.method public upload([BLjava/lang/String;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length p1, p1

    int-to-long v2, p1

    move-object v0, p0

    move-object v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/jscape/filetransfer/AbstractFileTransfer;->upload(Ljava/io/InputStream;JLjava/lang/String;Z)V

    return-void
.end method

.method public uploadDir(Ljava/io/File;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lcom/jscape/filetransfer/AbstractFileTransfer;->uploadDir(Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public uploadDir(Ljava/io/File;IZLjava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/jscape/filetransfer/AbstractFileTransfer;->uploadDir(Ljava/io/File;IZLjava/lang/String;I)V

    return-void
.end method

.method public uploadDir(Ljava/io/File;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0, p2}, Lcom/jscape/filetransfer/AbstractFileTransfer;->uploadDir(Ljava/io/File;IZLjava/lang/String;)V

    return-void
.end method

.method public uploadUnique(Ljava/io/File;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/nio/file/OpenOption;

    invoke-static {v1, v2}, Ljava/nio/file/Files;->newInputStream(Ljava/nio/file/Path;[Ljava/nio/file/OpenOption;)Ljava/io/InputStream;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v1, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->uploadUnique(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v1, :cond_0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :cond_0
    :goto_0
    return-object p1

    :catchall_1
    move-exception p1

    :try_start_4
    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_2
    move-exception v2

    if-eqz v1, :cond_1

    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v3

    :try_start_6
    invoke-virtual {p1, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    if-nez v0, :cond_1

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    goto :goto_1

    :catchall_4
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    throw v2
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public uploadUnique(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->resolveLocalPathFor(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/AbstractFileTransfer;->uploadUnique(Ljava/io/File;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
