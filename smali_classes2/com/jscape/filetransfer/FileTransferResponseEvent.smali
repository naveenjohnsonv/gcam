.class public Lcom/jscape/filetransfer/FileTransferResponseEvent;
.super Lcom/jscape/filetransfer/FileTransferEvent;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "~]\u0017C%kgVG\u001dC\u0003KcKD\u0014H\u0002|CNQ\u0015RQbt]G\u000bI\u001fjc\u0005\u0013"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/filetransfer/FileTransferResponseEvent;->c:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x38

    goto :goto_1

    :cond_1
    const/16 v4, 0x27

    goto :goto_1

    :cond_2
    const/16 v4, 0x4f

    goto :goto_1

    :cond_3
    const/16 v4, 0x18

    goto :goto_1

    :cond_4
    const/16 v4, 0x45

    goto :goto_1

    :cond_5
    const/16 v4, 0xa

    goto :goto_1

    :cond_6
    const/4 v4, 0x6

    :goto_1
    const/16 v5, 0x3e

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/FileTransferEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    iput-object p2, p0, Lcom/jscape/filetransfer/FileTransferResponseEvent;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/filetransfer/FileTransferListener;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/jscape/filetransfer/FileTransferListener;->responseReceived(Lcom/jscape/filetransfer/FileTransferResponseEvent;)V

    return-void
.end method

.method public getResponse()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferResponseEvent;->a:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/FileTransferResponseEvent;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferResponseEvent;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
