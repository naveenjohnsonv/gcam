.class Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/a/l;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:J

.field private final g:J

.field final h:Lcom/jscape/filetransfer/AftpFileTransfer;


# direct methods
.method private constructor <init>(Lcom/jscape/filetransfer/AftpFileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJ)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;->h:Lcom/jscape/filetransfer/AftpFileTransfer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;->d:Ljava/lang/String;

    iput p5, p0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;->e:I

    iput-wide p6, p0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;->f:J

    iput-wide p8, p0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;->g:J

    return-void
.end method

.method constructor <init>(Lcom/jscape/filetransfer/AftpFileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLcom/jscape/filetransfer/AftpFileTransfer$1;)V
    .locals 0

    invoke-direct/range {p0 .. p9}, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;-><init>(Lcom/jscape/filetransfer/AftpFileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJ)V

    return-void
.end method


# virtual methods
.method public onTransferProgress(J)V
    .locals 15

    move-object v0, p0

    iget-object v1, v0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;->h:Lcom/jscape/filetransfer/AftpFileTransfer;

    new-instance v14, Lcom/jscape/filetransfer/FileTransferProgressEvent;

    iget-object v3, v0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;->h:Lcom/jscape/filetransfer/AftpFileTransfer;

    iget-object v4, v0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;->b:Ljava/lang/String;

    iget-object v5, v0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;->c:Ljava/lang/String;

    iget-object v6, v0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;->d:Ljava/lang/String;

    iget v7, v0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;->e:I

    iget-wide v8, v0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;->f:J

    add-long v8, v8, p1

    iget-wide v12, v0, Lcom/jscape/filetransfer/AftpFileTransfer$TransferProgressListener;->g:J

    const-wide/16 v10, 0x0

    move-object v2, v14

    invoke-direct/range {v2 .. v13}, Lcom/jscape/filetransfer/FileTransferProgressEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJ)V

    invoke-virtual {v1, v14}, Lcom/jscape/filetransfer/AftpFileTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method
