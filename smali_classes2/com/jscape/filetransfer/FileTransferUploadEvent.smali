.class public Lcom/jscape/filetransfer/FileTransferUploadEvent;
.super Lcom/jscape/filetransfer/FileTransferEvent;


# static fields
.field private static final h:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:J

.field private final f:J

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x7

    const-string v4, "cB@v08\n\rcBUk)/^-\u0017@z.`#\t\u000bXz\t/V!\u0011Rz/\u0008G#\rU{\u0018+R!\u0016\u0014d;4[*\u000cUr8`\u0010\u0008cBD~)5\nh"

    const/16 v5, 0x42

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x4b

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v1

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v1

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v5, 0x15

    const/16 v1, 0xd

    const-string v4, "Srh@\u000e\u000ck/3pGPJ\u0007SrwF\u0017\u0008:"

    move v7, v10

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v1

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v10

    :goto_3
    const/16 v8, 0x7b

    add-int/2addr v6, v9

    add-int v10, v6, v1

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->h:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    const/16 v16, 0x16

    const/4 v2, 0x4

    if-eqz v15, :cond_7

    if-eq v15, v9, :cond_6

    const/4 v3, 0x2

    if-eq v15, v3, :cond_5

    const/4 v3, 0x3

    if-eq v15, v3, :cond_4

    if-eq v15, v2, :cond_8

    const/4 v2, 0x5

    if-eq v15, v2, :cond_8

    const/16 v16, 0x7c

    goto :goto_4

    :cond_4
    const/16 v16, 0x54

    goto :goto_4

    :cond_5
    const/16 v16, 0x7f

    goto :goto_4

    :cond_6
    const/16 v16, 0x29

    goto :goto_4

    :cond_7
    move/from16 v16, v2

    :cond_8
    :goto_4
    xor-int v2, v8, v16

    xor-int/2addr v2, v14

    int-to-char v2, v2

    aput-char v2, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 10

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-wide v5, p5

    move-wide/from16 v7, p7

    invoke-direct/range {v0 .. v9}, Lcom/jscape/filetransfer/FileTransferUploadEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/util/Map;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/filetransfer/FileTransfer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJ",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/FileTransferEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    iput-object p2, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->d:Ljava/lang/String;

    iput-wide p5, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->e:J

    iput-wide p7, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->f:J

    iput-object p9, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->g:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/filetransfer/FileTransferListener;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/jscape/filetransfer/FileTransferListener;->upload(Lcom/jscape/filetransfer/FileTransferUploadEvent;)V

    return-void
.end method

.method public attributeFor(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public attributeFor(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/FileTransferUploadEvent;->attributeFor(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    move-object p2, p1

    :cond_1
    return-object p2
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->e:J

    return-wide v0
.end method

.method public getTime()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->f:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/FileTransferUploadEvent;->h:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x3

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferUploadEvent;->g:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
