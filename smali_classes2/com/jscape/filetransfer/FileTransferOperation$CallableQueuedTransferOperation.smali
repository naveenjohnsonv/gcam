.class public Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final operation:Lcom/jscape/filetransfer/FileTransferOperation;

.field public final transferQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Lcom/jscape/filetransfer/FileTransfer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x10

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x61

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\u000bU AJ:\u0004A\u0010&b^1\u0002BH\u0019w\u001a;_d$\u0012U\u0014 ZD:W\\\u001a$VY5\u0003N\u001a:\u000e"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x2a

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/16 v13, 0x35

    if-eqz v12, :cond_5

    if-eq v12, v7, :cond_4

    if-eq v12, v0, :cond_6

    const/4 v14, 0x3

    if-eq v12, v14, :cond_3

    const/4 v14, 0x4

    if-eq v12, v14, :cond_2

    const/4 v14, 0x5

    if-eq v12, v14, :cond_6

    const/16 v13, 0x16

    goto :goto_2

    :cond_2
    const/16 v13, 0x4a

    goto :goto_2

    :cond_3
    const/16 v13, 0x52

    goto :goto_2

    :cond_4
    const/16 v13, 0x14

    goto :goto_2

    :cond_5
    const/16 v13, 0x46

    :cond_6
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransferOperation;Ljava/util/concurrent/BlockingQueue;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/filetransfer/FileTransferOperation;",
            "Ljava/util/concurrent/BlockingQueue<",
            "Lcom/jscape/filetransfer/FileTransfer;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;->operation:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;->transferQueue:Ljava/util/concurrent/BlockingQueue;

    return-void
.end method


# virtual methods
.method public call()Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;->transferQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/filetransfer/FileTransfer;

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;->operation:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-interface {v1, v0}, Lcom/jscape/filetransfer/FileTransferOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;->transferQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1, v0}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    return-object p0

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;->transferQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v2, v0}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V

    throw v1
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;->call()Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;->operation:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;->transferQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
