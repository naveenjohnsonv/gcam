.class public Lcom/jscape/filetransfer/FtpsTransfer$SslHandshakeEventConverter;
.super Ljava/lang/Object;

# interfaces
.implements Ljavax/net/ssl/HandshakeCompletedListener;


# instance fields
.field final a:Lcom/jscape/filetransfer/FtpsTransfer;


# direct methods
.method protected constructor <init>(Lcom/jscape/filetransfer/FtpsTransfer;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/filetransfer/FtpsTransfer$SslHandshakeEventConverter;->a:Lcom/jscape/filetransfer/FtpsTransfer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handshakeCompleted(Ljavax/net/ssl/HandshakeCompletedEvent;)V
    .locals 3

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpsTransfer$SslHandshakeEventConverter;->a:Lcom/jscape/filetransfer/FtpsTransfer;

    new-instance v1, Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpsTransfer$SslHandshakeEventConverter;->a:Lcom/jscape/filetransfer/FtpsTransfer;

    invoke-direct {v1, v2, p1}, Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljavax/net/ssl/HandshakeCompletedEvent;)V

    invoke-virtual {v0, v1}, Lcom/jscape/filetransfer/FtpsTransfer;->raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V

    return-void
.end method
