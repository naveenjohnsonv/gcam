.class public interface abstract Lcom/jscape/filetransfer/FileTransfer;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/AutoCloseable;


# static fields
.field public static final ASCII:I = 0x1

.field public static final AUTO:I = 0x0

.field public static final BINARY:I = 0x2

.field public static final CURRENT_DIRECTORY:Ljava/lang/String; = "."

.field public static final PARENT_DIRECTORY:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "G\t"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/filetransfer/FileTransfer;->PARENT_DIRECTORY:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v5, 0x4b

    if-eqz v4, :cond_4

    const/4 v6, 0x1

    if-eq v4, v6, :cond_3

    const/4 v6, 0x2

    if-eq v4, v6, :cond_5

    const/4 v6, 0x3

    if-eq v4, v6, :cond_5

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v5, 0x7a

    goto :goto_1

    :cond_1
    const/16 v5, 0x27

    goto :goto_1

    :cond_2
    const/16 v5, 0x1a

    goto :goto_1

    :cond_3
    const/16 v5, 0x3e

    goto :goto_1

    :cond_4
    const/16 v5, 0x70

    :cond_5
    :goto_1
    const/16 v4, 0x19

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public abstract abortDownloadThread(Ljava/lang/String;)V
.end method

.method public abstract abortDownloadThreads()V
.end method

.method public abstract abortUploadThread(Ljava/lang/String;)V
.end method

.method public abstract abortUploadThreads()V
.end method

.method public abstract addFileTransferListener(Lcom/jscape/filetransfer/FileTransferListener;)V
.end method

.method public abstract clearProxySettings()V
.end method

.method public abstract close()V
.end method

.method public abstract connect()Lcom/jscape/filetransfer/FileTransfer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract copy()Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract deleteDir(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract deleteDir(Ljava/lang/String;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract deleteFile(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract disconnect()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract download(Ljava/lang/String;)Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract download(Ljava/io/OutputStream;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract downloadDir(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract downloadDir(Ljava/lang/String;IZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract downloadDir(Ljava/lang/String;IZI)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract exists(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract getBlockTransferSize()I
.end method

.method public abstract getDebug()Z
.end method

.method public abstract getDebugStream()Ljava/io/PrintStream;
.end method

.method public abstract getDir()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract getDirListing()Ljava/util/Enumeration;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Enumeration<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract getDirListingAsString()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract getDirListingAsString(Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract getFilesize(Ljava/lang/String;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract getHostname()Ljava/lang/String;
.end method

.method public abstract getImplementation()Ljava/lang/Object;
.end method

.method public abstract getInputStream(Ljava/lang/String;J)Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract getLocalDir()Ljava/io/File;
.end method

.method public abstract getLocalDirListing()Ljava/util/Enumeration;
.end method

.method public abstract getMode()I
.end method

.method public abstract getNameListing()Ljava/util/Enumeration;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract getNameListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract getOutputStream(Ljava/lang/String;J)Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract getPassive()Z
.end method

.method public abstract getPassword()Ljava/lang/String;
.end method

.method public abstract getPort()I
.end method

.method public abstract getRecursiveDirectoryFileCount(Ljava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract getRecursiveDirectorySize(Ljava/lang/String;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract getTimeout()J
.end method

.method public abstract getUsername()Ljava/lang/String;
.end method

.method public abstract getWireEncoding()Ljava/lang/String;
.end method

.method public abstract interrupt()V
.end method

.method public abstract interrupted()Z
.end method

.method public abstract isChecksumVerificationRequired()Z
.end method

.method public abstract isConnected()Z
.end method

.method public abstract isDirectory(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract makeDir(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract makeDirRecursive(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract makeLocalDir(Ljava/lang/String;)Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract mdelete(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract mdownload(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract mdownload(Ljava/util/Enumeration;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract mupload(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract mupload(Ljava/util/Enumeration;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract removeFileTransferListener(Lcom/jscape/filetransfer/FileTransferListener;)V
.end method

.method public abstract renameFile(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract reset()V
.end method

.method public abstract resumeDownload(Ljava/io/OutputStream;Ljava/lang/String;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract resumeDownload(Ljava/lang/String;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract resumeDownload(Ljava/lang/String;Ljava/lang/String;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract resumeUpload(Ljava/io/File;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract resumeUpload(Ljava/io/File;Ljava/lang/String;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract resumeUpload(Ljava/io/InputStream;JLjava/lang/String;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract resumeUpload(Ljava/lang/String;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract sameChecksum(Ljava/io/File;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract setAscii()Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setAuto(Z)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setBinary()Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setBlockTransferSize(I)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setChecksumVerificationRequired(Z)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setDebug(Z)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setDebugStream(Ljava/io/PrintStream;)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract setDirUp()Lcom/jscape/filetransfer/FileTransfer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract setFileModificationTime(Ljava/lang/String;Ljava/util/Date;)Lcom/jscape/filetransfer/FileTransfer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)Lcom/jscape/filetransfer/FileTransfer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract setHostname(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setLocalDir(Ljava/io/File;)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setPassive(Z)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setPassword(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setPort(I)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setPreserveFileDownloadTimestamp(Z)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setPreserveFileUploadTimestamp(Z)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setProxyAuthentication(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setProxyHost(Ljava/lang/String;I)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setProxyType(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setTimeZone(Ljava/util/TimeZone;)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setTimeout(J)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setUsername(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract setWireEncoding(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
.end method

.method public abstract upload(Ljava/io/File;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract upload(Ljava/io/File;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract upload(Ljava/io/File;Ljava/lang/String;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract upload(Ljava/io/File;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract upload(Ljava/io/InputStream;JLjava/lang/String;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract upload(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract upload(Ljava/lang/String;Ljava/io/File;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract upload(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract upload(Ljava/lang/String;Ljava/lang/String;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract upload(Ljava/lang/String;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract upload([BLjava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract upload([BLjava/lang/String;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract uploadDir(Ljava/io/File;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract uploadDir(Ljava/io/File;IZLjava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract uploadDir(Ljava/io/File;IZLjava/lang/String;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract uploadDir(Ljava/io/File;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract uploadUnique(Ljava/io/File;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract uploadUnique(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method

.method public abstract uploadUnique(Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation
.end method
