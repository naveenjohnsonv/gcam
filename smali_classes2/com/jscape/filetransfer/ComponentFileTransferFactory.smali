.class public Lcom/jscape/filetransfer/ComponentFileTransferFactory;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransferFactory;


# static fields
.field private static final a:[Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;

.field private static final c:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/jscape/filetransfer/Protocol;",
            "Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const-string v0, "@\u0013f \rI\u001dg\tp1]I\u0000z\tz6\u0012UH5"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    const/4 v4, 0x5

    const/4 v5, 0x3

    const/4 v6, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x4

    if-gt v1, v3, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/filetransfer/ComponentFileTransferFactory;->c:Ljava/lang/String;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;

    new-instance v1, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;

    sget-object v3, Lcom/jscape/filetransfer/Protocol;->FTP:Lcom/jscape/filetransfer/Protocol;

    new-instance v9, Lcom/jscape/filetransfer/FtpFileTransferFactory;

    invoke-direct {v9}, Lcom/jscape/filetransfer/FtpFileTransferFactory;-><init>()V

    invoke-direct {v1, v3, v9}, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;-><init>(Lcom/jscape/filetransfer/Protocol;Lcom/jscape/filetransfer/FileTransferFactory;)V

    aput-object v1, v0, v2

    new-instance v1, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;

    sget-object v2, Lcom/jscape/filetransfer/Protocol;->FTPS:Lcom/jscape/filetransfer/Protocol;

    new-instance v3, Lcom/jscape/filetransfer/FtpsFileTransferFactory;

    invoke-direct {v3}, Lcom/jscape/filetransfer/FtpsFileTransferFactory;-><init>()V

    invoke-direct {v1, v2, v3}, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;-><init>(Lcom/jscape/filetransfer/Protocol;Lcom/jscape/filetransfer/FileTransferFactory;)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;

    sget-object v2, Lcom/jscape/filetransfer/Protocol;->FTPS_AUTH_TLS:Lcom/jscape/filetransfer/Protocol;

    new-instance v3, Lcom/jscape/filetransfer/FtpsFileTransferFactory;

    invoke-direct {v3}, Lcom/jscape/filetransfer/FtpsFileTransferFactory;-><init>()V

    invoke-direct {v1, v2, v3}, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;-><init>(Lcom/jscape/filetransfer/Protocol;Lcom/jscape/filetransfer/FileTransferFactory;)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;

    sget-object v2, Lcom/jscape/filetransfer/Protocol;->FTPS_AUTH_SSL:Lcom/jscape/filetransfer/Protocol;

    new-instance v3, Lcom/jscape/filetransfer/FtpsFileTransferFactory;

    invoke-direct {v3}, Lcom/jscape/filetransfer/FtpsFileTransferFactory;-><init>()V

    invoke-direct {v1, v2, v3}, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;-><init>(Lcom/jscape/filetransfer/Protocol;Lcom/jscape/filetransfer/FileTransferFactory;)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;

    sget-object v2, Lcom/jscape/filetransfer/Protocol;->FTPS_IMPLICIT:Lcom/jscape/filetransfer/Protocol;

    new-instance v3, Lcom/jscape/filetransfer/FtpsImplicitFileTransferFactory;

    invoke-direct {v3}, Lcom/jscape/filetransfer/FtpsImplicitFileTransferFactory;-><init>()V

    invoke-direct {v1, v2, v3}, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;-><init>(Lcom/jscape/filetransfer/Protocol;Lcom/jscape/filetransfer/FileTransferFactory;)V

    aput-object v1, v0, v8

    new-instance v1, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;

    sget-object v2, Lcom/jscape/filetransfer/Protocol;->SFTP:Lcom/jscape/filetransfer/Protocol;

    new-instance v3, Lcom/jscape/filetransfer/SftpFileTransferFactory;

    invoke-direct {v3}, Lcom/jscape/filetransfer/SftpFileTransferFactory;-><init>()V

    invoke-direct {v1, v2, v3}, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;-><init>(Lcom/jscape/filetransfer/Protocol;Lcom/jscape/filetransfer/FileTransferFactory;)V

    aput-object v1, v0, v4

    const/4 v1, 0x6

    new-instance v2, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;

    sget-object v3, Lcom/jscape/filetransfer/Protocol;->AFTP:Lcom/jscape/filetransfer/Protocol;

    new-instance v4, Lcom/jscape/filetransfer/AftpFileTransferFactory;

    invoke-direct {v4}, Lcom/jscape/filetransfer/AftpFileTransferFactory;-><init>()V

    invoke-direct {v2, v3, v4}, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;-><init>(Lcom/jscape/filetransfer/Protocol;Lcom/jscape/filetransfer/FileTransferFactory;)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/jscape/filetransfer/ComponentFileTransferFactory;->a:[Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;

    return-void

    :cond_0
    aget-char v9, v0, v3

    rem-int/lit8 v10, v3, 0x7

    const/16 v11, 0x6c

    if-eqz v10, :cond_3

    if-eq v10, v7, :cond_4

    if-eq v10, v6, :cond_3

    if-eq v10, v5, :cond_2

    if-eq v10, v8, :cond_4

    if-eq v10, v4, :cond_1

    const/16 v8, 0xb

    goto :goto_1

    :cond_1
    const/16 v8, 0x40

    goto :goto_1

    :cond_2
    const/16 v8, 0x2c

    goto :goto_1

    :cond_3
    move v8, v11

    :cond_4
    :goto_1
    const/16 v4, 0x79

    xor-int/2addr v4, v8

    xor-int/2addr v4, v9

    int-to-char v4, v4

    aput-char v4, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0
.end method

.method public varargs constructor <init>([Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/filetransfer/ComponentFileTransferFactory;->b:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/ComponentFileTransferFactory;->a([Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;)V

    return-void
.end method

.method private a(Lcom/jscape/filetransfer/Protocol;)Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/ComponentFileTransferFactory;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/filetransfer/ComponentFileTransferFactory;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/ComponentFileTransferFactory;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method

.method private a([Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;)V
    .locals 6

    array-length v0, p1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v0, :cond_1

    aget-object v3, p1, v2

    iget-object v4, p0, Lcom/jscape/filetransfer/ComponentFileTransferFactory;->b:Ljava/util/Map;

    iget-object v5, v3, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;->protocol:Lcom/jscape/filetransfer/Protocol;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    if-nez v1, :cond_0

    :cond_1
    return-void
.end method

.method public static defaultFactory()Lcom/jscape/filetransfer/ComponentFileTransferFactory;
    .locals 2

    new-instance v0, Lcom/jscape/filetransfer/ComponentFileTransferFactory;

    sget-object v1, Lcom/jscape/filetransfer/ComponentFileTransferFactory;->a:[Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;

    invoke-direct {v0, v1}, Lcom/jscape/filetransfer/ComponentFileTransferFactory;-><init>([Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;)V

    return-object v0
.end method


# virtual methods
.method public supports(Lcom/jscape/filetransfer/Protocol;)Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/ComponentFileTransferFactory;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public transferFor(Lcom/jscape/filetransfer/FileTransferParameters;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferFactory$OperationException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FileTransferParameters;->getProtocol()Lcom/jscape/filetransfer/Protocol;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/filetransfer/ComponentFileTransferFactory;->a(Lcom/jscape/filetransfer/Protocol;)Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;

    move-result-object v0

    iget-object v0, v0, Lcom/jscape/filetransfer/ComponentFileTransferFactory$Entry;->factory:Lcom/jscape/filetransfer/FileTransferFactory;

    invoke-interface {v0, p1}, Lcom/jscape/filetransfer/FileTransferFactory;->transferFor(Lcom/jscape/filetransfer/FileTransferParameters;)Lcom/jscape/filetransfer/FileTransfer;

    move-result-object p1

    return-object p1
.end method
