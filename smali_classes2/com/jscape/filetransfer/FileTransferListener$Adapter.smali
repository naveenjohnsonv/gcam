.class public Lcom/jscape/filetransfer/FileTransferListener$Adapter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransferListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public changeDir(Lcom/jscape/filetransfer/FileTransferChangeDirEvent;)V
    .locals 0

    return-void
.end method

.method public commandSent(Lcom/jscape/filetransfer/FileTransferCommandEvent;)V
    .locals 0

    return-void
.end method

.method public connected(Lcom/jscape/filetransfer/FileTransferConnectedEvent;)V
    .locals 0

    return-void
.end method

.method public createDir(Lcom/jscape/filetransfer/FileTransferCreateDirEvent;)V
    .locals 0

    return-void
.end method

.method public deleteDir(Lcom/jscape/filetransfer/FileTransferDeleteDirEvent;)V
    .locals 0

    return-void
.end method

.method public deleteFile(Lcom/jscape/filetransfer/FileTransferDeleteFileEvent;)V
    .locals 0

    return-void
.end method

.method public disconnected(Lcom/jscape/filetransfer/FileTransferDisconnectedEvent;)V
    .locals 0

    return-void
.end method

.method public download(Lcom/jscape/filetransfer/FileTransferDownloadEvent;)V
    .locals 0

    return-void
.end method

.method public progress(Lcom/jscape/filetransfer/FileTransferProgressEvent;)V
    .locals 0

    return-void
.end method

.method public renameFile(Lcom/jscape/filetransfer/FileTransferRenameFileEvent;)V
    .locals 0

    return-void
.end method

.method public responseReceived(Lcom/jscape/filetransfer/FileTransferResponseEvent;)V
    .locals 0

    return-void
.end method

.method public sslHandshakeCompleted(Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;)V
    .locals 0

    return-void
.end method

.method public upload(Lcom/jscape/filetransfer/FileTransferUploadEvent;)V
    .locals 0

    return-void
.end method
