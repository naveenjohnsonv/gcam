.class public Lcom/jscape/filetransfer/FtpTransfer;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransfer;


# static fields
.field private static final h:[Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/inet/ftp/Ftp;

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/jscape/filetransfer/FileTransferListener;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private final g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x4

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x40

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "Z\u0000B\u0001@Q%} 2Dxr/z6gJ0z%b(sSs\u007flw*`\u0007|x/p)2Ay{)1b7T77-\u007f!2Uuz#e 2Ay{)1b7T79\u0002:-"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x48

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move/from16 v16, v5

    move v5, v4

    move/from16 v4, v16

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/filetransfer/FtpTransfer;->h:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    const/4 v14, 0x5

    if-eqz v13, :cond_6

    if-eq v13, v8, :cond_5

    const/4 v15, 0x2

    if-eq v13, v15, :cond_4

    if-eq v13, v0, :cond_7

    if-eq v13, v2, :cond_3

    if-eq v13, v14, :cond_2

    const/16 v14, 0x50

    goto :goto_2

    :cond_2
    const/16 v14, 0x67

    goto :goto_2

    :cond_3
    const/16 v14, 0x52

    goto :goto_2

    :cond_4
    const/16 v14, 0x51

    goto :goto_2

    :cond_5
    const/16 v14, 0xc

    goto :goto_2

    :cond_6
    const/16 v14, 0x57

    :cond_7
    :goto_2
    xor-int v13, v7, v14

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, ""

    invoke-direct {p0, v0, v0, v0}, Lcom/jscape/filetransfer/FtpTransfer;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            "Ljava/util/Set<",
            "Lcom/jscape/filetransfer/FileTransferListener;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jscape/inet/ftp/Ftp;

    invoke-direct {v0, p1, p3, p4, p2}, Lcom/jscape/inet/ftp/Ftp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    new-instance p1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {p1, p6}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->b:Ljava/util/Set;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->d:Z

    new-instance p1, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    invoke-direct {p1, p0}, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;-><init>(Lcom/jscape/filetransfer/FtpTransfer;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    iget-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p1, p5}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V

    iget-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    iget-object p2, p0, Lcom/jscape/filetransfer/FtpTransfer;->g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ftp/Ftp;->addFtpListener(Lcom/jscape/inet/ftp/FtpListener;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/io/File;

    const-string v1, "."

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/jscape/filetransfer/FtpTransfer;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    new-instance v5, Ljava/io/File;

    const-string v0, "."

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move v2, p4

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/jscape/filetransfer/FtpTransfer;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/util/Set;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 7

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v6

    const/16 v2, 0x15

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/jscape/filetransfer/FtpTransfer;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/util/Set;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result p4

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/jscape/filetransfer/FtpTransfer;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/util/Enumeration;)Ljava/util/Vector;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    :cond_0
    :try_start_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ftp/FtpFile;

    new-instance v3, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    invoke-direct {v3}, Lcom/jscape/filetransfer/FileTransferRemoteFile;-><init>()V

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->setFilename(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->setDirectory(Z)V

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->isLink()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->setLink(Z)V

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->getFilesize()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->setFilesize(J)V

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->getLinkTarget()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->setLinkTarget(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/Ftp;->getFtpFileParser()Lcom/jscape/inet/ftp/FtpFileParser;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/jscape/inet/ftp/FtpFileParser;->getDateTime(Lcom/jscape/inet/ftp/FtpFile;)Ljava/util/Date;

    move-result-object v2
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {v3, v2}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->setFileDate(Ljava/util/Date;)V

    if-eqz v0, :cond_1

    invoke-virtual {v1, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/filetransfer/FtpTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_0
    return-object v1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method


# virtual methods
.method public abortDownloadThread(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->abortDownloadThread(Ljava/lang/String;)V

    return-void
.end method

.method public abortDownloadThreads()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->abortDownloadThreads()V

    return-void
.end method

.method public abortUploadThread(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->abortUploadThread(Ljava/lang/String;)V

    return-void
.end method

.method public abortUploadThreads()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->abortUploadThreads()V

    return-void
.end method

.method public addFileTransferListener(Lcom/jscape/filetransfer/FileTransferListener;)V
    .locals 1

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public changePassword(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->changePassword(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public checksum(Ljava/io/File;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->checksum(Ljava/io/File;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public clearProxySettings()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->clearProxySettings()V

    return-void
.end method

.method public close()V
    .locals 0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/filetransfer/FtpTransfer;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public bridge synthetic connect()Lcom/jscape/filetransfer/FileTransfer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/filetransfer/FtpTransfer;->connect()Lcom/jscape/filetransfer/FtpTransfer;

    move-result-object v0

    return-object v0
.end method

.method public connect()Lcom/jscape/filetransfer/FtpTransfer;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->connect()Lcom/jscape/inet/ftp/Ftp;
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v0, 0x0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    sget-object v2, Lcom/jscape/filetransfer/FtpTransfer;->h:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/jscape/inet/ftp/Ftp;->isFeatureSupported(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/jscape/filetransfer/FtpTransfer;->c:Z
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    :try_start_2
    iput-boolean v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->c:Z
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_0
    return-object p0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
.end method

.method public connect(Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->connect(Z)Lcom/jscape/inet/ftp/Ftp;
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public copy()Lcom/jscape/filetransfer/FileTransfer;
    .locals 8

    new-instance v7, Lcom/jscape/filetransfer/FtpTransfer;

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getHostname()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getPort()I

    move-result v2

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getUsername()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getPassword()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v5

    iget-object v6, p0, Lcom/jscape/filetransfer/FtpTransfer;->b:Ljava/util/Set;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/jscape/filetransfer/FtpTransfer;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/util/Set;)V

    invoke-virtual {p0}, Lcom/jscape/filetransfer/FtpTransfer;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/jscape/filetransfer/FtpTransfer;->setDebugStream(Ljava/io/PrintStream;)Lcom/jscape/filetransfer/FileTransfer;

    invoke-virtual {p0}, Lcom/jscape/filetransfer/FtpTransfer;->getDebug()Z

    move-result v0

    invoke-virtual {v7, v0}, Lcom/jscape/filetransfer/FtpTransfer;->setDebug(Z)Lcom/jscape/filetransfer/FileTransfer;

    return-object v7
.end method

.method public deleteDir(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->deleteDir(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public deleteDir(Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->deleteDir(Ljava/lang/String;Z)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->deleteFile(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public disconnect()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
.end method

.method public download(Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->download(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/jscape/filetransfer/FtpTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->raiseDownloadEvent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/jscape/filetransfer/FtpTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/jscape/filetransfer/FtpTransfer;->g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    invoke-virtual {p2}, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->raiseDownloadEvent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public download(Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->download(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public downloadDir(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->downloadDir(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public downloadDir(Ljava/lang/String;IZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/inet/ftp/Ftp;->downloadDir(Ljava/lang/String;IZ)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public downloadDir(Ljava/lang/String;IZI)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/jscape/inet/ftp/Ftp;->downloadDir(Ljava/lang/String;IZI)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public exists(Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/FtpTransfer;->getFilesize(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/FtpTransfer;->getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return v0

    :catch_1
    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/FtpTransfer;->isDirectory(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public getBlockTransferSize()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getBlockTransferSize()I

    move-result v0

    return v0
.end method

.method public getDebug()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getDebug()Z

    move-result v0

    return v0
.end method

.method public getDebugStream()Ljava/io/PrintStream;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v0

    return-object v0
.end method

.method public getDir()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
.end method

.method public getDirListing()Ljava/util/Enumeration;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/filetransfer/FtpTransfer;->c:Z
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_3

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/filetransfer/FtpTransfer;->d:Z
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_5

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_2
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ftp/Ftp;->getMachineDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/Ftp;->getFtpFileParser()Lcom/jscape/inet/ftp/FtpFileParser;

    move-result-object v2

    iget-object v1, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    new-instance v3, Lcom/jscape/inet/ftp/MLSDParser;

    invoke-direct {v3}, Lcom/jscape/inet/ftp/MLSDParser;-><init>()V

    invoke-virtual {v1, v3}, Lcom/jscape/inet/ftp/Ftp;->setFtpFileParser(Lcom/jscape/inet/ftp/FtpFileParser;)V
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :cond_0
    if-eqz v0, :cond_2

    :try_start_3
    iget-boolean v1, p0, Lcom/jscape/filetransfer/FtpTransfer;->e:Z
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/filetransfer/FtpTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    sget-object v1, Lcom/jscape/filetransfer/FtpTransfer;->h:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ftp/Ftp;->getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_3

    :cond_2
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getDirListing()Ljava/util/Enumeration;

    move-result-object v0

    :goto_1
    invoke-direct {p0, v0}, Lcom/jscape/filetransfer/FtpTransfer;->a(Ljava/util/Enumeration;)Ljava/util/Vector;

    move-result-object v0
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_1

    if-eqz v2, :cond_3

    :try_start_5
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v1, v2}, Lcom/jscape/inet/ftp/Ftp;->setFtpFileParser(Lcom/jscape/inet/ftp/FtpFileParser;)V
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/filetransfer/FtpTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_3
    :goto_2
    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_1

    return-object v0

    :catch_3
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/filetransfer/FtpTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_4

    :catch_4
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/filetransfer/FtpTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_5

    :catch_5
    move-exception v0

    :try_start_9
    invoke-static {v0}, Lcom/jscape/filetransfer/FtpTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_1

    :goto_3
    invoke-static {v0}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
.end method

.method public getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Enumeration<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->c:Z
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    const-string v1, ""

    invoke-virtual {v0, v1, p1}, Lcom/jscape/inet/ftp/Ftp;->getMachineDirListing(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getFtpFileParser()Lcom/jscape/inet/ftp/FtpFileParser;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    new-instance v2, Lcom/jscape/inet/ftp/MLSDParser;

    invoke-direct {v2}, Lcom/jscape/inet/ftp/MLSDParser;-><init>()V

    invoke-virtual {v1, v2}, Lcom/jscape/inet/ftp/Ftp;->setFtpFileParser(Lcom/jscape/inet/ftp/FtpFileParser;)V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->getDirListingRegex(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/FtpTransfer;->a(Ljava/util/Enumeration;)Ljava/util/Vector;

    move-result-object p1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_2

    if-eqz v0, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v1, v0}, Lcom/jscape/inet/ftp/Ftp;->setFtpFileParser(Lcom/jscape/inet/ftp/FtpFileParser;)V
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/filetransfer/FtpTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_2

    return-object p1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getDirListingAsString()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getDirListingAsString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
.end method

.method public getDirListingAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->getDirListingRegexAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getErrorOnSizeCommand()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getErrorOnSizeCommand()Z

    move-result v0

    return v0
.end method

.method public getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getFilesize(Ljava/lang/String;)J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->getFilesize(Ljava/lang/String;)J

    move-result-wide v0
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getHostname()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getHostname()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getImplementation()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    return-object v0
.end method

.method public getInputStream(Ljava/lang/String;J)Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/inet/ftp/Ftp;->getInputStream(Ljava/lang/String;J)Ljava/io/InputStream;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getLocalChecksum(Ljava/io/File;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->getLocalChecksum(Ljava/io/File;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getLocalDir()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public getLocalDirListing()Ljava/util/Enumeration;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDirListing()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public getMode()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getMode()I

    move-result v0

    return v0
.end method

.method public getNameListing()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getNameListing()Ljava/util/Enumeration;

    move-result-object v0
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
.end method

.method public getNameListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->getNameListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getOutputStream(Ljava/lang/String;J)Ljava/io/OutputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/jscape/inet/ftp/Ftp;->getOutputStream(Ljava/lang/String;JZ)Ljava/io/OutputStream;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public getPassive()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getPassive()Z

    move-result v0

    return v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getPassword()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPerformMLSDListing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->d:Z

    return v0
.end method

.method public getPort()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getPort()I

    move-result v0

    return v0
.end method

.method public getRecursiveDirectoryFileCount(Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->getRecursiveDirectoryFileCount(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getRecursiveDirectorySize(Ljava/lang/String;)J
    .locals 2

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->getRecursiveDirectorySize(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getRemoteFileChecksum(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->getRemoteFileChecksum(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    new-instance v0, Ljava/util/Vector;

    new-instance v1, Lcom/jscape/filetransfer/RecursiveFileListOperation;

    invoke-direct {v1, p1}, Lcom/jscape/filetransfer/RecursiveFileListOperation;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lcom/jscape/filetransfer/RecursiveFileListOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/RecursiveFileListOperation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/filetransfer/RecursiveFileListOperation;->files()Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/Vector;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getTcpNoDelay()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getTcpNoDelay()Z

    move-result v0

    return v0
.end method

.method public getTimeout()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getTimeout()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getUseEPSV()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getUseEPSV()Z

    move-result v0

    return v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getUsername()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWireEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getWireEncoding()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public interrupt()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->interrupt()V

    return-void
.end method

.method public interrupted()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->interrupted()Z

    move-result v0

    return v0
.end method

.method public isChecksumVerificationRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->f:Z

    return v0
.end method

.method public isConnected()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->isConnected()Z

    move-result v0

    return v0
.end method

.method public isDirectory(Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/filetransfer/FtpTransfer;->getDir()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/FtpTransfer;->setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p1, 0x1

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/FtpTransfer;->setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    return p1

    :catchall_0
    move-exception p1

    :try_start_2
    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/FtpTransfer;->setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    throw p1

    :catch_2
    const/4 p1, 0x0

    :try_start_3
    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/FtpTransfer;->setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    return p1
.end method

.method public isFeatureSupported(Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->isFeatureSupported(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public issueCommand(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->issueCommand(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public login()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->login()V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
.end method

.method public makeDir(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->makeDir(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public makeDirRecursive(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->makeDirRecursive(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public makeLocalDir(Ljava/lang/String;)Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->makeLocalDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    return-object p1
.end method

.method public mdelete(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->mdelete(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public mdownload(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->mdownload(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public mdownload(Ljava/util/Enumeration;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->mdownload(Ljava/util/Enumeration;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public mupload(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->mupload(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public mupload(Ljava/util/Enumeration;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->mupload(Ljava/util/Enumeration;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method protected raiseErrorEvent(Lcom/jscape/filetransfer/FileTransferErrorEvent;)V
    .locals 4

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/FtpTransfer;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/FileTransferListener;

    if-eqz v0, :cond_1

    instance-of v3, v2, Lcom/jscape/filetransfer/FileTransferErrorListener;

    if-eqz v3, :cond_2

    :cond_1
    check-cast v2, Lcom/jscape/filetransfer/FileTransferErrorListener;

    invoke-interface {v2, p1}, Lcom/jscape/filetransfer/FileTransferErrorListener;->error(Lcom/jscape/filetransfer/FileTransferErrorEvent;)V

    :cond_2
    if-nez v0, :cond_0

    :cond_3
    return-void
.end method

.method protected raiseEvent(Lcom/jscape/filetransfer/FileTransferEvent;)V
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/FtpTransfer;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/FileTransferListener;

    invoke-virtual {p1, v2}, Lcom/jscape/filetransfer/FileTransferEvent;->accept(Lcom/jscape/filetransfer/FileTransferListener;)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public removeFileTransferListener(Lcom/jscape/filetransfer/FileTransferListener;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public renameFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->renameFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public reset()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->reset()V

    return-void
.end method

.method public resumeDownload(Ljava/io/OutputStream;Ljava/lang/String;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    return-void
.end method

.method public resumeDownload(Ljava/lang/String;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/inet/ftp/Ftp;->resumeDownload(Ljava/lang/String;J)Ljava/io/File;

    move-result-object p2

    invoke-virtual {p0, p2, p1}, Lcom/jscape/filetransfer/FtpTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->raiseDownloadEvent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public resumeDownload(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/jscape/inet/ftp/Ftp;->resumeDownload(Ljava/lang/String;Ljava/lang/String;J)Ljava/io/File;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/jscape/filetransfer/FtpTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->raiseDownloadEvent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public resumeUpload(Ljava/io/File;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/inet/ftp/Ftp;->resumeUpload(Ljava/io/File;J)V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/jscape/filetransfer/FtpTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->raiseUploadEvent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public resumeUpload(Ljava/io/File;Ljava/lang/String;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/jscape/inet/ftp/Ftp;->resumeUpload(Ljava/io/File;Ljava/lang/String;J)V

    invoke-virtual {p0, p1, p2}, Lcom/jscape/filetransfer/FtpTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->raiseUploadEvent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public resumeUpload(Ljava/io/InputStream;JLjava/lang/String;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    return-void
.end method

.method public resumeUpload(Ljava/lang/String;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/inet/ftp/Ftp;->resumeUpload(Ljava/lang/String;J)V

    new-instance p2, Ljava/io/File;

    invoke-virtual {p0}, Lcom/jscape/filetransfer/FtpTransfer;->getLocalDir()Ljava/io/File;

    move-result-object p3

    invoke-direct {p2, p3, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, p2, p1}, Lcom/jscape/filetransfer/FtpTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->raiseUploadEvent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method protected safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/filetransfer/FtpTransfer;->f:Z
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p0, p1, p2}, Lcom/jscape/filetransfer/FtpTransfer;->sameChecksum(Ljava/io/File;Ljava/lang/String;)Z

    move-result v1
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    :try_start_2
    new-instance v0, Ljava/lang/Exception;

    sget-object v1, Lcom/jscape/filetransfer/FtpTransfer;->h:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    aput-object p2, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/filetransfer/FtpTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    :goto_0
    return-void

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public sameChecksum(Ljava/io/File;Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->checksum(Ljava/io/File;Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public setAscii()Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->setAscii()V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-object p0
.end method

.method public setAuto(Z)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setAuto(Z)V

    return-object p0
.end method

.method public setAutoDetectIpv6(Z)Lcom/jscape/filetransfer/FtpTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setAutoDetectIpv6(Z)V

    return-object p0
.end method

.method public setBinary()Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->setBinary()V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-object p0
.end method

.method public setBlockTransferSize(I)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setBlockTransferSize(I)V

    return-object p0
.end method

.method public setChecksumVerificationRequired(Z)Lcom/jscape/filetransfer/FileTransfer;
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->f:Z

    return-object p0
.end method

.method public setCompression(Z)Lcom/jscape/filetransfer/FtpTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setCompression(Z)V

    return-object p0
.end method

.method public setConnectBeforeCommand(Z)Lcom/jscape/filetransfer/FtpTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setConnectBeforeCommand(Z)V

    return-object p0
.end method

.method public setDebug(Z)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setDebug(Z)V

    return-object p0
.end method

.method public setDebugStream(Ljava/io/PrintStream;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setDebugStream(Ljava/io/PrintStream;)V

    return-object p0
.end method

.method public setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public setDirUp()Lcom/jscape/filetransfer/FileTransfer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->setDirUp()V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
.end method

.method public setErrorOnSizeCommand(Z)Lcom/jscape/filetransfer/FtpTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setErrorOnSizeCommand(Z)V

    return-object p0
.end method

.method public setFileModificationTime(Ljava/lang/String;Ljava/util/Date;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->setFileModificationTime(Ljava/lang/String;Ljava/util/Date;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public setHostname(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setHostname(Ljava/lang/String;)V

    return-object p0
.end method

.method public setLocalDir(Ljava/io/File;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V

    return-object p0
.end method

.method public setNATAddress(Ljava/lang/String;)Lcom/jscape/filetransfer/FtpTransfer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setNATAddress(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public setPassive(Z)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setPassive(Z)V

    return-object p0
.end method

.method public setPassword(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setPassword(Ljava/lang/String;)V

    return-object p0
.end method

.method public setPerformMLSDListing(Z)Lcom/jscape/filetransfer/FtpTransfer;
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->d:Z

    return-object p0
.end method

.method public setPort(I)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setPort(I)V

    return-object p0
.end method

.method public setPreserveFileDownloadTimestamp(Z)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setPreserveDownloadTimestamp(Z)V

    return-object p0
.end method

.method public setPreserveFileUploadTimestamp(Z)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setPreserveUploadTimestamp(Z)V

    return-object p0
.end method

.method public setProxyAuthentication(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->setProxyAuthentication(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setProxyHost(Ljava/lang/String;I)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->setProxyHost(Ljava/lang/String;I)V

    return-object p0
.end method

.method public setProxyType(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setProxyType(Ljava/lang/String;)V

    return-object p0
.end method

.method public setTcpNoDelay(Z)Lcom/jscape/filetransfer/FtpTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setTcpNoDelay(Z)V

    return-object p0
.end method

.method public setTimeZone(Ljava/util/TimeZone;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setTimezone(Ljava/util/TimeZone;)V

    return-object p0
.end method

.method public setTimeout(J)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    long-to-int p1, p1

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setTimeout(I)V

    return-object p0
.end method

.method public setUseEPRT(Z)Lcom/jscape/filetransfer/FtpTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setUseEPRT(Z)V

    return-object p0
.end method

.method public setUseEPSV(Z)Lcom/jscape/filetransfer/FtpTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setUseEPSV(Z)V

    return-object p0
.end method

.method public setUseExtendedDirectoryListing(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->e:Z

    return-void
.end method

.method public setUsername(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setUsername(Ljava/lang/String;)V

    return-object p0
.end method

.method public setWireEncoding(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->setWireEncoding(Ljava/lang/String;)V

    return-object p0
.end method

.method public upload(Ljava/io/File;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v2, p1}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/io/File;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v2

    move-object v6, v1

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->interrupted()Z

    move-result v0
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_1

    goto :goto_0

    :catch_1
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/filetransfer/FtpTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    new-instance v0, Lcom/jscape/filetransfer/FileTransferErrorEvent;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    move-object v3, v0

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/jscape/filetransfer/FileTransferErrorEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/FtpTransfer;->raiseErrorEvent(Lcom/jscape/filetransfer/FileTransferErrorEvent;)V

    :cond_1
    invoke-static {v2}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public upload(Ljava/io/File;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2}, Lcom/jscape/filetransfer/FtpTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->raiseUploadEvent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public upload(Ljava/io/File;Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/io/File;Ljava/lang/String;Z)V

    invoke-virtual {p0, p1, p2}, Lcom/jscape/filetransfer/FtpTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->raiseUploadEvent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public upload(Ljava/io/File;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/io/File;Z)V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/jscape/filetransfer/FtpTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->raiseUploadEvent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public upload(Ljava/io/InputStream;JLjava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    return-void
.end method

.method public upload(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/jscape/filetransfer/FtpTransfer;->getLocalDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p1}, Lcom/jscape/filetransfer/FtpTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->raiseUploadEvent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public upload(Ljava/lang/String;Ljava/io/File;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v2, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/lang/String;Ljava/io/File;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    move-object v4, v1

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->interrupted()Z

    move-result v0
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_1

    goto :goto_0

    :catch_1
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/filetransfer/FtpTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FtpTransfer;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    new-instance v0, Lcom/jscape/filetransfer/FileTransferErrorEvent;

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/jscape/filetransfer/FileTransferErrorEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcom/jscape/filetransfer/FtpTransfer;->raiseErrorEvent(Lcom/jscape/filetransfer/FileTransferErrorEvent;)V

    :cond_1
    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public upload(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/jscape/filetransfer/FtpTransfer;->getLocalDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/jscape/filetransfer/FtpTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->raiseUploadEvent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public upload(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/lang/String;Ljava/lang/String;Z)V

    new-instance p3, Ljava/io/File;

    invoke-virtual {p0}, Lcom/jscape/filetransfer/FtpTransfer;->getLocalDir()Ljava/io/File;

    move-result-object v0

    invoke-direct {p3, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, p3, p2}, Lcom/jscape/filetransfer/FtpTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->raiseUploadEvent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public upload(Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/lang/String;Z)V

    new-instance p2, Ljava/io/File;

    invoke-virtual {p0}, Lcom/jscape/filetransfer/FtpTransfer;->getLocalDir()Ljava/io/File;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, p2, p1}, Lcom/jscape/filetransfer/FtpTransfer;->safeCheckChecksum(Ljava/io/File;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/filetransfer/FtpTransfer;->g:Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;

    invoke-virtual {p1}, Lcom/jscape/filetransfer/FtpTransfer$FtpEventConverter;->raiseUploadEvent()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public upload([BLjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->upload([BLjava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public upload([BLjava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/inet/ftp/Ftp;->upload([BLjava/lang/String;Z)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public uploadDir(Ljava/io/File;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->uploadDir(Ljava/io/File;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public uploadDir(Ljava/io/File;IIZLjava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/jscape/inet/ftp/Ftp;->uploadDir(Ljava/io/File;IIZLjava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public uploadDir(Ljava/io/File;IZLjava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/jscape/filetransfer/FtpTransfer;->uploadDir(Ljava/io/File;IIZLjava/lang/String;)V

    return-void
.end method

.method public uploadDir(Ljava/io/File;IZLjava/lang/String;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/jscape/inet/ftp/Ftp;->uploadDir(Ljava/io/File;IZLjava/lang/String;I)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public uploadDir(Ljava/io/File;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->uploadDir(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public uploadUnique(Ljava/io/File;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->uploadUnique(Ljava/io/File;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public uploadUnique(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->uploadUnique(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method

.method public uploadUnique(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/FtpTransfer;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/Ftp;->uploadUnique(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
.end method
