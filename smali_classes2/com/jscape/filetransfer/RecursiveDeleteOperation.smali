.class public Lcom/jscape/filetransfer/RecursiveDeleteOperation;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransferOperation;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lcom/jscape/filetransfer/FileTransfer;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const-string v0, "``"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->c:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v5, 0x31

    const/4 v6, 0x4

    if-eqz v4, :cond_5

    const/4 v7, 0x1

    if-eq v4, v7, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    if-eq v4, v6, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v5, 0x3a

    goto :goto_1

    :cond_1
    move v5, v6

    goto :goto_1

    :cond_2
    const/16 v5, 0x3f

    goto :goto_1

    :cond_3
    const/16 v5, 0x79

    goto :goto_1

    :cond_4
    const/16 v5, 0xf

    :cond_5
    :goto_1
    const/16 v4, 0x7f

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->a:Ljava/lang/String;

    return-void
.end method

.method private static a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->b:Lcom/jscape/filetransfer/FileTransfer;

    if-eqz v0, :cond_0

    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->isConnected()Z

    move-result v0
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->b:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->connect()Lcom/jscape/filetransfer/FileTransfer;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object v0

    throw v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->b:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->getDir()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->b:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v2, p1}, Lcom/jscape/filetransfer/FileTransfer;->setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;

    iget-object v2, p0, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->b:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v2}, Lcom/jscape/filetransfer/FileTransfer;->getDirListing()Ljava/util/Enumeration;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    invoke-virtual {v3}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilename()Ljava/lang/String;

    move-result-object v4

    if-eqz v1, :cond_7

    :try_start_0
    invoke-virtual {v3}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->isDirectory()Z

    move-result v5
    :try_end_0
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v1, :cond_3

    if-eqz v5, :cond_2

    :try_start_1
    sget-object v5, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->c:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5
    :try_end_1
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_1 .. :try_end_1} :catch_4

    if-eqz v1, :cond_1

    if-nez v5, :cond_0

    const-string v5, "."

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    :cond_1
    if-eqz v1, :cond_3

    if-eqz v5, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {v3}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->isDirectory()Z

    move-result v5

    :cond_3
    if-eqz v5, :cond_4

    :try_start_2
    invoke-direct {p0, v4}, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_2 .. :try_end_2} :catch_0

    if-nez v1, :cond_5

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_4
    :goto_1
    iget-object v3, p0, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->b:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v3, v4}, Lcom/jscape/filetransfer/FileTransfer;->deleteFile(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_3 .. :try_end_3} :catch_1

    :cond_5
    if-nez v1, :cond_0

    goto :goto_2

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Lcom/jscape/filetransfer/FileTransferException; {:try_start_6 .. :try_end_6} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->a(Lcom/jscape/filetransfer/FileTransferException;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1

    :cond_6
    :goto_2
    iget-object v1, p0, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->b:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v1, v0}, Lcom/jscape/filetransfer/FileTransfer;->setDir(Ljava/lang/String;)Lcom/jscape/filetransfer/FileTransfer;

    iget-object v0, p0, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->b:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0, p1}, Lcom/jscape/filetransfer/FileTransfer;->deleteDir(Ljava/lang/String;)V

    :cond_7
    return-void
.end method


# virtual methods
.method public bridge synthetic applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/RecursiveDeleteOperation;

    move-result-object p1

    return-object p1
.end method

.method public applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/RecursiveDeleteOperation;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->b:Lcom/jscape/filetransfer/FileTransfer;

    invoke-direct {p0}, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->a()V

    iget-object p1, p0, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->a:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/RecursiveDeleteOperation;->a(Ljava/lang/String;)V

    return-object p0
.end method
