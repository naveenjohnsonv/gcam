.class public final enum Lcom/jscape/filetransfer/Protocol;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/filetransfer/Protocol;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AFTP:Lcom/jscape/filetransfer/Protocol;

.field public static final enum AMAZON_S3:Lcom/jscape/filetransfer/Protocol;

.field public static final enum AS2:Lcom/jscape/filetransfer/Protocol;

.field public static final enum FTP:Lcom/jscape/filetransfer/Protocol;

.field public static final enum FTPS:Lcom/jscape/filetransfer/Protocol;

.field public static final enum FTPS_AUTH_SSL:Lcom/jscape/filetransfer/Protocol;

.field public static final enum FTPS_AUTH_TLS:Lcom/jscape/filetransfer/Protocol;

.field public static final enum FTPS_IMPLICIT:Lcom/jscape/filetransfer/Protocol;

.field public static final enum REST:Lcom/jscape/filetransfer/Protocol;

.field public static final enum SFTP:Lcom/jscape/filetransfer/Protocol;

.field public static final enum WEBDAV:Lcom/jscape/filetransfer/Protocol;

.field private static final synthetic a:[Lcom/jscape/filetransfer/Protocol;

.field private static final b:[Ljava/lang/String;


# instance fields
.field public final defaultPort:I

.field public final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0x17

    new-array v0, v0, [Ljava/lang/String;

    const/16 v1, 0xd

    const/4 v3, 0x0

    const-string v4, "\u001dc\u000b\u0003L6\u0019\u000f\u007f\u0004\u0004_$\t\u001az\u001a\n\\9a\u0008\u0004\r\u001dc\u000b\u0003>>\u0001\u000b{\u0012\u0013Z#\u0003\u001adi\u0004\tr\u0008\u0004\u0004\u0008q\u000f\u0000\u0004\u0008q\u000f\u0000\u0004\u001dc\u000b\u0003\u0003\u001adi\t\u001az\u001a\n\\9\u0013\u0008\u0004\r\u001dc\u000b\u0003L>\u0001\u000b{\u0012\u0013Z#\u0006\u000cr\u0019\u0014R!\u0006\u000cr\u0019\u0014R!\r\u001dc\u000b\u0003>6\u0019\u000f\u007fv\u0004_$\u0004\u001aq\u000f\u0000\u0004\u001aq\u000f\u0000\u0004\tr\u0008\u0004\u0004\u001dc\u000b\u0003\u0003\u001dc\u000b\u001b\u000eY(%c\u0007#)C>43\u0007>4C43|\u001bl5V65)W\r\u001dc\u000b\u0003L6\u0019\u000f\u007f\u0004\u0003@;"

    const/16 v5, 0xb7

    move v7, v1

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x66

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/16 v15, 0x11

    const/4 v2, 0x3

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const-string v4, "\u001e`\u0008\r\u001e`\u0008\u0000=5\u001a\u000c|u\u0000C8"

    move v7, v2

    move v8, v11

    move v5, v15

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v7, v2

    move v8, v11

    :goto_3
    const/16 v9, 0x65

    add-int/2addr v6, v10

    add-int v2, v6, v7

    invoke-virtual {v4, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/filetransfer/Protocol;->b:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/filetransfer/Protocol;

    sget-object v4, Lcom/jscape/filetransfer/Protocol;->b:[Ljava/lang/String;

    const/16 v5, 0x12

    aget-object v5, v4, v5

    const/16 v6, 0x15

    aget-object v7, v4, v6

    invoke-direct {v0, v5, v3, v7, v6}, Lcom/jscape/filetransfer/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/jscape/filetransfer/Protocol;->FTP:Lcom/jscape/filetransfer/Protocol;

    new-instance v0, Lcom/jscape/filetransfer/Protocol;

    aget-object v5, v4, v15

    const/4 v7, 0x7

    aget-object v8, v4, v7

    invoke-direct {v0, v5, v10, v8, v6}, Lcom/jscape/filetransfer/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/jscape/filetransfer/Protocol;->FTPS:Lcom/jscape/filetransfer/Protocol;

    new-instance v0, Lcom/jscape/filetransfer/Protocol;

    aget-object v5, v4, v3

    aget-object v1, v4, v1

    const/4 v8, 0x2

    invoke-direct {v0, v5, v8, v1, v6}, Lcom/jscape/filetransfer/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/jscape/filetransfer/Protocol;->FTPS_AUTH_TLS:Lcom/jscape/filetransfer/Protocol;

    new-instance v0, Lcom/jscape/filetransfer/Protocol;

    const/16 v1, 0x14

    aget-object v1, v4, v1

    const/16 v5, 0x16

    aget-object v8, v4, v5

    invoke-direct {v0, v1, v2, v8, v6}, Lcom/jscape/filetransfer/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/jscape/filetransfer/Protocol;->FTPS_AUTH_SSL:Lcom/jscape/filetransfer/Protocol;

    new-instance v0, Lcom/jscape/filetransfer/Protocol;

    const/16 v1, 0xa

    aget-object v6, v4, v1

    const/4 v8, 0x2

    aget-object v9, v4, v8

    const/16 v8, 0x3de

    const/4 v11, 0x4

    invoke-direct {v0, v6, v11, v9, v8}, Lcom/jscape/filetransfer/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/jscape/filetransfer/Protocol;->FTPS_IMPLICIT:Lcom/jscape/filetransfer/Protocol;

    new-instance v0, Lcom/jscape/filetransfer/Protocol;

    const/4 v6, 0x6

    aget-object v8, v4, v6

    const/4 v9, 0x5

    aget-object v11, v4, v9

    invoke-direct {v0, v8, v9, v11, v5}, Lcom/jscape/filetransfer/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/jscape/filetransfer/Protocol;->SFTP:Lcom/jscape/filetransfer/Protocol;

    new-instance v0, Lcom/jscape/filetransfer/Protocol;

    const/16 v5, 0xf

    aget-object v5, v4, v5

    const/16 v8, 0xe

    aget-object v8, v4, v8

    const/16 v9, 0xbb8

    invoke-direct {v0, v5, v6, v8, v9}, Lcom/jscape/filetransfer/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/jscape/filetransfer/Protocol;->AFTP:Lcom/jscape/filetransfer/Protocol;

    new-instance v0, Lcom/jscape/filetransfer/Protocol;

    const/16 v5, 0x9

    aget-object v8, v4, v5

    aget-object v9, v4, v10

    const/16 v11, 0x50

    invoke-direct {v0, v8, v7, v9, v11}, Lcom/jscape/filetransfer/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/jscape/filetransfer/Protocol;->AMAZON_S3:Lcom/jscape/filetransfer/Protocol;

    new-instance v0, Lcom/jscape/filetransfer/Protocol;

    const/16 v8, 0x8

    aget-object v9, v4, v8

    aget-object v12, v4, v2

    invoke-direct {v0, v9, v8, v12, v11}, Lcom/jscape/filetransfer/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/jscape/filetransfer/Protocol;->AS2:Lcom/jscape/filetransfer/Protocol;

    new-instance v0, Lcom/jscape/filetransfer/Protocol;

    const/16 v9, 0x10

    aget-object v9, v4, v9

    const/4 v12, 0x4

    aget-object v13, v4, v12

    invoke-direct {v0, v9, v5, v13, v11}, Lcom/jscape/filetransfer/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/jscape/filetransfer/Protocol;->REST:Lcom/jscape/filetransfer/Protocol;

    new-instance v0, Lcom/jscape/filetransfer/Protocol;

    const/16 v9, 0xb

    aget-object v12, v4, v9

    const/16 v13, 0xc

    aget-object v4, v4, v13

    invoke-direct {v0, v12, v1, v4, v11}, Lcom/jscape/filetransfer/Protocol;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/jscape/filetransfer/Protocol;->WEBDAV:Lcom/jscape/filetransfer/Protocol;

    new-array v4, v9, [Lcom/jscape/filetransfer/Protocol;

    sget-object v9, Lcom/jscape/filetransfer/Protocol;->FTP:Lcom/jscape/filetransfer/Protocol;

    aput-object v9, v4, v3

    sget-object v3, Lcom/jscape/filetransfer/Protocol;->FTPS:Lcom/jscape/filetransfer/Protocol;

    aput-object v3, v4, v10

    sget-object v3, Lcom/jscape/filetransfer/Protocol;->FTPS_AUTH_TLS:Lcom/jscape/filetransfer/Protocol;

    const/4 v9, 0x2

    aput-object v3, v4, v9

    sget-object v3, Lcom/jscape/filetransfer/Protocol;->FTPS_AUTH_SSL:Lcom/jscape/filetransfer/Protocol;

    aput-object v3, v4, v2

    sget-object v2, Lcom/jscape/filetransfer/Protocol;->FTPS_IMPLICIT:Lcom/jscape/filetransfer/Protocol;

    const/4 v3, 0x4

    aput-object v2, v4, v3

    sget-object v2, Lcom/jscape/filetransfer/Protocol;->SFTP:Lcom/jscape/filetransfer/Protocol;

    const/4 v3, 0x5

    aput-object v2, v4, v3

    sget-object v2, Lcom/jscape/filetransfer/Protocol;->AFTP:Lcom/jscape/filetransfer/Protocol;

    aput-object v2, v4, v6

    sget-object v2, Lcom/jscape/filetransfer/Protocol;->AMAZON_S3:Lcom/jscape/filetransfer/Protocol;

    aput-object v2, v4, v7

    sget-object v2, Lcom/jscape/filetransfer/Protocol;->AS2:Lcom/jscape/filetransfer/Protocol;

    aput-object v2, v4, v8

    sget-object v2, Lcom/jscape/filetransfer/Protocol;->REST:Lcom/jscape/filetransfer/Protocol;

    aput-object v2, v4, v5

    aput-object v0, v4, v1

    sput-object v4, Lcom/jscape/filetransfer/Protocol;->a:[Lcom/jscape/filetransfer/Protocol;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v1, v14, 0x7

    const/16 v17, 0x3d

    if-eqz v1, :cond_7

    if-eq v1, v10, :cond_6

    const/4 v3, 0x2

    if-eq v1, v3, :cond_7

    if-eq v1, v2, :cond_5

    const/4 v2, 0x4

    if-eq v1, v2, :cond_4

    const/4 v2, 0x5

    if-eq v1, v2, :cond_8

    const/16 v15, 0x2a

    goto :goto_4

    :cond_4
    const/16 v15, 0x75

    goto :goto_4

    :cond_5
    const/16 v15, 0x36

    goto :goto_4

    :cond_6
    const/16 v15, 0x51

    goto :goto_4

    :cond_7
    move/from16 v15, v17

    :cond_8
    :goto_4
    xor-int v1, v9, v15

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/16 v1, 0xd

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/jscape/filetransfer/Protocol;->name:Ljava/lang/String;

    iput p4, p0, Lcom/jscape/filetransfer/Protocol;->defaultPort:I

    return-void
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method

.method public static protocolFor(Ljava/lang/String;)Lcom/jscape/filetransfer/Protocol;
    .locals 6

    invoke-static {}, Lcom/jscape/filetransfer/Protocol;->values()[Lcom/jscape/filetransfer/Protocol;

    move-result-object v0

    array-length v1, v0

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v5, v4, Lcom/jscape/filetransfer/Protocol;->name:Ljava/lang/String;

    invoke-virtual {v5, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v5, :cond_0

    return-object v4

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/filetransfer/Protocol;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/filetransfer/Protocol;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_1
    if-eqz v2, :cond_2

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/filetransfer/Protocol;->b:[Ljava/lang/String;

    const/16 v3, 0x13

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/filetransfer/Protocol;
    .locals 1

    const-class v0, Lcom/jscape/filetransfer/Protocol;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/filetransfer/Protocol;

    return-object p0
.end method

.method public static values()[Lcom/jscape/filetransfer/Protocol;
    .locals 1

    sget-object v0, Lcom/jscape/filetransfer/Protocol;->a:[Lcom/jscape/filetransfer/Protocol;

    invoke-virtual {v0}, [Lcom/jscape/filetransfer/Protocol;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/filetransfer/Protocol;

    return-object v0
.end method
