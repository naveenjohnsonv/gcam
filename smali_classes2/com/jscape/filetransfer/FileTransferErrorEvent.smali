.class public Lcom/jscape/filetransfer/FileTransferErrorEvent;
.super Lcom/jscape/filetransfer/FileTransferEvent;


# static fields
.field private static final f:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "\u0018hd=:\u000b\u00190rn=\u001c<\n,nz\u001d\u0018\u001c\u0016*!s>\u0007\u0015\u001d0`e=S^\u0008r!x9\u001a\u0011Ey"

    const/16 v5, 0x2b

    const/16 v6, 0x22

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x6f

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x15

    const/4 v4, 0x7

    const-string v6, "=n*xES\n\r=n+xBW[A/3\u007f\u001c\u0011"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v16, v6

    move v6, v4

    move-object/from16 v4, v16

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x20

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/filetransfer/FileTransferErrorEvent;->f:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v2, 0x17

    goto :goto_4

    :cond_4
    const/16 v2, 0x16

    goto :goto_4

    :cond_5
    move v2, v10

    goto :goto_4

    :cond_6
    const/16 v2, 0x37

    goto :goto_4

    :cond_7
    const/16 v2, 0x67

    goto :goto_4

    :cond_8
    const/16 v2, 0x6e

    goto :goto_4

    :cond_9
    const/16 v2, 0x31

    :goto_4
    xor-int/2addr v2, v9

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransfer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/FileTransferEvent;-><init>(Lcom/jscape/filetransfer/FileTransfer;)V

    iput-object p2, p0, Lcom/jscape/filetransfer/FileTransferErrorEvent;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/filetransfer/FileTransferErrorEvent;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/filetransfer/FileTransferErrorEvent;->d:Ljava/lang/String;

    iput p5, p0, Lcom/jscape/filetransfer/FileTransferErrorEvent;->e:I

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/filetransfer/FileTransferListener;)V
    .locals 1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/jscape/filetransfer/FileTransferErrorListener;

    if-eqz v0, :cond_1

    :cond_0
    check-cast p1, Lcom/jscape/filetransfer/FileTransferErrorListener;

    invoke-interface {p1, p0}, Lcom/jscape/filetransfer/FileTransferErrorListener;->error(Lcom/jscape/filetransfer/FileTransferErrorEvent;)V

    :cond_1
    return-void
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferErrorEvent;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferErrorEvent;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getMode()J
    .locals 2

    iget v0, p0, Lcom/jscape/filetransfer/FileTransferErrorEvent;->e:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/FileTransferErrorEvent;->c:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/FileTransferErrorEvent;->f:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/FileTransferErrorEvent;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/FileTransferErrorEvent;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x3

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/FileTransferErrorEvent;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/filetransfer/FileTransferErrorEvent;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
