.class public Lcom/jscape/filetransfer/DownloadRegexOperation;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/filetransfer/FileTransferOperation;
.implements Lcom/jscape/filetransfer/DownloadFileOperation$Listener;


# static fields
.field private static final r:[Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/filetransfer/FileTransferOperation;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/jscape/util/m/f;

.field private final d:J

.field private final e:Z

.field private final f:I

.field private final g:Lcom/jscape/util/Time;

.field private final h:Ljava/lang/String;

.field private final i:I

.field private final j:Z

.field private k:Lcom/jscape/filetransfer/FileTransfer;

.field private l:Z

.field private m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/jscape/filetransfer/DownloadFileOperation;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/concurrent/ExecutorService;

.field private o:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Lcom/jscape/filetransfer/FileTransfer;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/concurrent/CountDownLatch;

.field private volatile q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "\u00143\u000cOU3*Lv\u000c^Y\u0001c\u000e\u00143\u0015F_\u0017?\\P\u000e[C\u0006c-||\u0016@A\u001d?\\A\u0004IH\n\u0011Hv\u0013OY\u001b1V3\u001a^B\u0001*{|\u000f@H\u0011*wc\u0004\\L\u00067W}\\\u000c\u00143\u0002OC\u0011;T\u007f\u0004J\u0010\u0011\u00143\u0007GA\u0017\r]c\u0000\\L\u00061J.F\u0017zr\u0005\u000e@\u0013&\u0018r\u0015ZH\u001f.L`AXL\u001e+]=\u0013\u00143\u0013K@\u001d*]W\u0008\\H\u0011*Wa\u0018\u0013\n\u0017zr\u0005\u000eY\u001a,]r\u0005\u000eN\u001d+VgAXL\u001e+]=*v|AHD\u001e;K3\u000cOY\u00116]wA\\H\u0015+Tr\u0013\u000eH\n.Jv\u0012]D\u001d0\u0018u\u000e[C\u0016p\u0010\u00143\u0000ZY\u00173Hg1K_\u001b1\\.\n\u00143\u000cOY\u00116]a\\"

    const/16 v4, 0xf5

    const/16 v5, 0xe

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x54

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x2a

    const/16 v3, 0x14

    const-string v5, "\"\u00053}w!\u001ckw2ut0\rHL;}hy\u0015\"\u00051yr(!hk8^r(\r}c8mu U"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x62

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/filetransfer/DownloadRegexOperation;->r:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0xa

    goto :goto_4

    :cond_4
    const/16 v1, 0x26

    goto :goto_4

    :cond_5
    const/16 v1, 0x79

    goto :goto_4

    :cond_6
    const/16 v1, 0x7a

    goto :goto_4

    :cond_7
    const/16 v1, 0x35

    goto :goto_4

    :cond_8
    const/16 v1, 0x47

    goto :goto_4

    :cond_9
    const/16 v1, 0x6c

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/filetransfer/FileTransferOperation;Ljava/lang/String;Lcom/jscape/util/m/f;JZILcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/Integer;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->b:Ljava/lang/String;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->c:Lcom/jscape/util/m/f;

    iput-wide p4, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->d:J

    iput-boolean p6, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->e:Z

    const/4 p1, 0x0

    if-eqz v0, :cond_2

    int-to-long p2, p7

    const-wide/16 p4, 0x0

    sget-object p6, Lcom/jscape/filetransfer/DownloadRegexOperation;->r:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object p6, p6, v1

    invoke-static {p2, p3, p4, p5, p6}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p7, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->f:I

    invoke-static {p8}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p8, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->g:Lcom/jscape/util/Time;

    invoke-static {p9}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p9, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->h:Ljava/lang/String;

    if-eqz p10, :cond_1

    invoke-virtual {p10}, Ljava/lang/Integer;->intValue()I

    move-result p7

    if-eqz v0, :cond_2

    if-ltz p7, :cond_0

    goto :goto_0

    :cond_0
    move p7, p1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p7, 0x1

    :cond_2
    :goto_1
    sget-object p2, Lcom/jscape/filetransfer/DownloadRegexOperation;->r:[Ljava/lang/String;

    const/4 p3, 0x7

    aget-object p2, p2, p3

    invoke-static {p7, p2}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    if-eqz v0, :cond_3

    if-eqz p10, :cond_4

    :cond_3
    invoke-virtual {p10}, Ljava/lang/Integer;->intValue()I

    move-result p1

    :cond_4
    iput p1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->i:I

    iput-boolean p11, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->j:Z

    return-void
.end method

.method private a(Lcom/jscape/filetransfer/RemoteDirectory;Ljava/io/File;)Lcom/jscape/filetransfer/DownloadFileOperation;
    .locals 11

    new-instance v10, Lcom/jscape/filetransfer/DownloadFileOperation;

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    sget-object v5, Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;->RESUME_AFTER_FIRST_ATTEMPT:Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;

    iget v6, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->f:I

    iget-object v7, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->g:Lcom/jscape/util/Time;

    iget-boolean v8, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->j:Z

    const/4 v3, 0x0

    move-object v0, v10

    move-object v2, p1

    move-object v4, p2

    move-object v9, p0

    invoke-direct/range {v0 .. v9}, Lcom/jscape/filetransfer/DownloadFileOperation;-><init>(Lcom/jscape/filetransfer/FileTransferOperation;Lcom/jscape/filetransfer/RemoteDirectory;Ljava/lang/String;Ljava/io/File;Lcom/jscape/filetransfer/DownloadFileOperation$TransferStrategy;ILcom/jscape/util/Time;ZLcom/jscape/filetransfer/DownloadFileOperation$Listener;)V

    return-object v10
.end method

.method private a(Ljava/lang/String;)Lcom/jscape/filetransfer/DownloadFileOperation;
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/filetransfer/DownloadFileOperation;

    return-object p1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v1, :cond_1

    if-eqz v1, :cond_3

    iget-object v3, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->h:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    if-nez v1, :cond_0

    :cond_2
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_3
    return-object v2
.end method

.method private a(Lcom/jscape/filetransfer/RemoteDirectory;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/filetransfer/RemoteDirectory;",
            ")",
            "Ljava/util/List<",
            "Lcom/jscape/filetransfer/FileTransferRemoteFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {p1, v0}, Lcom/jscape/filetransfer/RemoteDirectory;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->getDirListing()Ljava/util/Enumeration;

    move-result-object v1

    invoke-static {v1}, Lcom/jscape/util/b/h;->b(Ljava/util/Enumeration;)Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    :try_start_0
    invoke-virtual {v2}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->isDirectory()Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_5

    if-eqz v0, :cond_1

    if-nez v3, :cond_3

    :try_start_1
    iget-object v3, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->c:Lcom/jscape/util/m/f;

    invoke-virtual {v2}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/jscape/util/m/f;->a(Ljava/lang/String;)Z

    move-result v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    :cond_1
    if-eqz v0, :cond_2

    if-eqz v3, :cond_3

    :try_start_2
    invoke-virtual {v2}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFileDate()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-direct {p0, v3, v4}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(J)Z

    move-result v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    :try_start_3
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    if-nez v0, :cond_0

    goto :goto_2

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_4
    :goto_2
    iget-boolean v3, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->e:Z

    :cond_5
    if-eqz v0, :cond_6

    if-eqz v3, :cond_7

    if-eqz v0, :cond_7

    :try_start_6
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_3

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_6
    :goto_3
    if-nez v3, :cond_8

    :cond_7
    return-object p1

    :cond_8
    :try_start_7
    new-instance p1, Ljava/lang/Exception;

    sget-object v0, Lcom/jscape/filetransfer/DownloadRegexOperation;->r:[Ljava/lang/String;

    const/16 v1, 0x8

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private a()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    nop

    move-wide v0, v0

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->g:Lcom/jscape/util/Time;

    invoke-virtual {v1}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v1

    iget v3, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->f:I

    invoke-static {v0, v1, v2, v3}, Lcom/jscape/util/x;->a(Ljava/util/concurrent/Callable;JI)Ljava/lang/Object;

    return-void
.end method

.method private a(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {v0, p1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->o:Ljava/util/concurrent/BlockingQueue;

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->o:Ljava/util/concurrent/BlockingQueue;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v1, v2}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    :cond_0
    if-ge v1, p1, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v2}, Lcom/jscape/filetransfer/FileTransfer;->copy()Lcom/jscape/filetransfer/FileTransfer;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-direct {p0, v2}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Lcom/jscape/filetransfer/FileTransfer;)V

    iget-object v3, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->o:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v3, v2}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_1

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->h()V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method private a(Lcom/jscape/filetransfer/FileTransfer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-interface {p1}, Lcom/jscape/filetransfer/FileTransfer;->connect()Lcom/jscape/filetransfer/FileTransfer;

    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-interface {v0, p1}, Lcom/jscape/filetransfer/FileTransferOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    return-void
.end method

.method private a(Lcom/jscape/filetransfer/FileTransferOperation;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {p1, v0}, Lcom/jscape/filetransfer/FileTransferOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    :try_end_0
    .catch Lcom/jscape/filetransfer/TransferOperationCancelledException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private a(J)Z
    .locals 5

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->d:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v0, :cond_2

    if-eqz v1, :cond_1

    invoke-static {p1, p2}, Lcom/jscape/util/D;->e(J)J

    move-result-wide p1

    iget-wide v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->d:J

    cmp-long v1, p1, v1

    if-eqz v0, :cond_2

    if-lez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move p1, v1

    :goto_1
    return p1
.end method

.method private b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    if-eqz v0, :cond_0

    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->isConnected()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->l:Z

    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->connect()Lcom/jscape/filetransfer/FileTransfer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0, v1}, Lcom/jscape/filetransfer/FileTransferOperation;->applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;

    :cond_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private c()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->m:Ljava/util/Map;

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->d()Ljava/util/LinkedList;

    move-result-object v1

    new-instance v2, Lcom/jscape/filetransfer/RemoteDirectory$PathElementsDirectory;

    invoke-direct {v2, v1}, Lcom/jscape/filetransfer/RemoteDirectory$PathElementsDirectory;-><init>(Ljava/util/List;)V

    invoke-direct {p0, v2}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Lcom/jscape/filetransfer/RemoteDirectory;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jscape/filetransfer/FileTransferRemoteFile;

    invoke-virtual {v4}, Lcom/jscape/filetransfer/FileTransferRemoteFile;->getFilename()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v6}, Lcom/jscape/filetransfer/FileTransfer;->getLocalDir()Ljava/io/File;

    move-result-object v6

    invoke-direct {v5, v6, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v2, v5}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Lcom/jscape/filetransfer/RemoteDirectory;Ljava/io/File;)Lcom/jscape/filetransfer/DownloadFileOperation;

    move-result-object v5

    iget-object v6, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->m:Ljava/util/Map;

    invoke-direct {p0, v1, v4}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private d()Ljava/util/LinkedList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v2}, Lcom/jscape/filetransfer/FileTransfer;->getDir()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->b:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_0

    :try_start_1
    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v0, :cond_0

    if-nez v2, :cond_0

    :try_start_2
    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    return-object v1
.end method

.method private e()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->p:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method private f()Z
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->i:I

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    if-le v1, v2, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method private g()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->i:I

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->n:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(I)V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/FileTransferOperation;

    iget-object v3, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->n:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;

    iget-object v5, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->o:Ljava/util/concurrent/BlockingQueue;

    invoke-direct {v4, v2, v5}, Lcom/jscape/filetransfer/FileTransferOperation$CallableQueuedTransferOperation;-><init>(Lcom/jscape/filetransfer/FileTransferOperation;Ljava/util/concurrent/BlockingQueue;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private h()V
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->o:Ljava/util/concurrent/BlockingQueue;

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    move-object v2, p0

    goto :goto_1

    :cond_1
    move-object v2, p0

    :goto_0
    check-cast v1, Lcom/jscape/filetransfer/FileTransfer;

    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcom/jscape/filetransfer/FileTransfer;->close()V

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    :goto_1
    iget-object v1, v2, Lcom/jscape/filetransfer/DownloadRegexOperation;->o:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :cond_3
    :goto_2
    return-void
.end method

.method private i()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->p:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->h()V

    throw v0

    :catch_0
    :goto_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->h()V

    return-void
.end method

.method private j()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/TransferOperationCancelledException;
        }
    .end annotation

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->q:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/filetransfer/TransferOperationCancelledException;

    invoke-direct {v0}, Lcom/jscape/filetransfer/TransferOperationCancelledException;-><init>()V

    throw v0
    :try_end_0
    .catch Lcom/jscape/filetransfer/TransferOperationCancelledException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private k()V
    .locals 3

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->n:Ljava/util/concurrent/ExecutorService;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    iput-object v2, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->n:Ljava/util/concurrent/ExecutorService;

    :cond_0
    iput-object v2, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->p:Ljava/util/concurrent/CountDownLatch;

    :cond_1
    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->l:Z

    if-eqz v0, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->l()V

    :cond_3
    return-void
.end method

.method private l()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    invoke-interface {v0}, Lcom/jscape/filetransfer/FileTransfer;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private m()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-direct {p0, v2}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Lcom/jscape/filetransfer/FileTransferOperation;)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private n()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->b()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    return-object v0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->l()V

    throw v0
.end method


# virtual methods
.method public applyTo(Lcom/jscape/filetransfer/FileTransfer;)Lcom/jscape/filetransfer/FileTransferOperation;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/filetransfer/FileTransferException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iput-object p1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->k:Lcom/jscape/filetransfer/FileTransfer;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->q:Z

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->e()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->f()Z

    move-result p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p1, :cond_0

    :try_start_2
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->g()V

    if-nez v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->m()V

    :cond_1
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->i()V

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->j()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->k()V

    return-object p0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/filetransfer/FileTransferException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/filetransfer/FileTransferException;

    move-result-object p1

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_1
    invoke-direct {p0}, Lcom/jscape/filetransfer/DownloadRegexOperation;->k()V

    throw p1
.end method

.method public cancel()V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->q:Z

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->m:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/filetransfer/DownloadFileOperation;

    invoke-virtual {v2}, Lcom/jscape/filetransfer/DownloadFileOperation;->cancel()V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public cancel(Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/jscape/filetransfer/FileTransferEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/jscape/filetransfer/DownloadRegexOperation;->a(Ljava/lang/String;)Lcom/jscape/filetransfer/DownloadFileOperation;

    move-result-object p1

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/jscape/filetransfer/DownloadFileOperation;->cancel()V

    :cond_1
    return-void
.end method

.method public fileCount()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public onOperationCompleted(Lcom/jscape/filetransfer/DownloadFileOperation;)V
    .locals 0

    iget-object p1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->p:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/filetransfer/DownloadRegexOperation;->r:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->a:Lcom/jscape/filetransfer/FileTransferOperation;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v3, 0xa

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->c:Lcom/jscape/util/m/f;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v3, 0xc

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->e:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->f:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v3, 0x9

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->g:Lcom/jscape/util/Time;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->i:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v2, 0xb

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->j:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/filetransfer/DownloadRegexOperation;->q:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
