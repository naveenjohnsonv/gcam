.class public interface abstract Lcom/jscape/filetransfer/FileTransferListener;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/EventListener;


# virtual methods
.method public abstract changeDir(Lcom/jscape/filetransfer/FileTransferChangeDirEvent;)V
.end method

.method public abstract commandSent(Lcom/jscape/filetransfer/FileTransferCommandEvent;)V
.end method

.method public abstract connected(Lcom/jscape/filetransfer/FileTransferConnectedEvent;)V
.end method

.method public abstract createDir(Lcom/jscape/filetransfer/FileTransferCreateDirEvent;)V
.end method

.method public abstract deleteDir(Lcom/jscape/filetransfer/FileTransferDeleteDirEvent;)V
.end method

.method public abstract deleteFile(Lcom/jscape/filetransfer/FileTransferDeleteFileEvent;)V
.end method

.method public abstract disconnected(Lcom/jscape/filetransfer/FileTransferDisconnectedEvent;)V
.end method

.method public abstract download(Lcom/jscape/filetransfer/FileTransferDownloadEvent;)V
.end method

.method public abstract progress(Lcom/jscape/filetransfer/FileTransferProgressEvent;)V
.end method

.method public abstract renameFile(Lcom/jscape/filetransfer/FileTransferRenameFileEvent;)V
.end method

.method public abstract responseReceived(Lcom/jscape/filetransfer/FileTransferResponseEvent;)V
.end method

.method public abstract sslHandshakeCompleted(Lcom/jscape/filetransfer/FileTransferSslHandshakeEvent;)V
.end method

.method public abstract upload(Lcom/jscape/filetransfer/FileTransferUploadEvent;)V
.end method
