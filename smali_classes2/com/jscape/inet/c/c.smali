.class public Lcom/jscape/inet/c/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/c/e;


# static fields
.field private static final b:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/b/a/b/i;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final c:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x5

    const-string v4, "n->u\u0003\u0005n->u\u0003\u0008\u0003\nP\u0000_H\u0003z.\u00020g?\u001d\u0010C,~L\u0004$Y]91|)P\u0014H8-e7\u0015YL?~_u\u0003Y\u0011f`$u\u0003$\u0017k{w~/\u0004+p7\u001f\u0010C,~L\u0004$)\r;,k(\tY@.-w1\u0017\u001c\r**$\u000bU\n\rws:pU\npq~!#^"

    const/16 v5, 0x73

    move v7, v1

    const/4 v6, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x16

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x1c

    const/16 v4, 0x13

    const-string v6, "W`\'dE\u0018 rf sN\\\u001bff!sR\u0008Es;u_\u0015Dt"

    move v7, v4

    move-object v4, v6

    move v8, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x5a

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/c/c;->d:[Ljava/lang/String;

    invoke-static {}, Lcom/jscape/inet/b/a/a/m;->a()Lcom/jscape/inet/b/a/a/m;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/c/c;->b:Lcom/jscape/util/h/I;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    const/4 v3, 0x4

    if-eq v2, v3, :cond_5

    if-eq v2, v1, :cond_4

    const/16 v2, 0x3b

    goto :goto_4

    :cond_4
    const/16 v2, 0x6f

    goto :goto_4

    :cond_5
    const/16 v2, 0x66

    goto :goto_4

    :cond_6
    const/16 v2, 0x46

    goto :goto_4

    :cond_7
    const/16 v2, 0x12

    goto :goto_4

    :cond_8
    const/16 v2, 0x48

    goto :goto_4

    :cond_9
    const/16 v2, 0x5d

    :goto_4
    xor-int/2addr v2, v9

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/util/logging/Logger;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/c/c;->c:Ljava/util/logging/Logger;

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/util/k/a/r;Lcom/jscape/inet/b/a/b/i;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/c/f;->e()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/c/c;->c:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/c/c;->c:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/c/c;->d:[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v0, v0, v3

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object p1

    aput-object p1, v3, v4

    const/4 p1, 0x2

    aput-object p2, v3, p1

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private b(Lcom/jscape/util/k/a/r;Lcom/jscape/inet/b/a/b/i;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/c/f;->e()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/c/c;->c:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/c/c;->c:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/c/c;->d:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v0, v0, v3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object p1

    aput-object p1, v3, v4

    const/4 p1, 0x2

    aput-object p2, v3, p1

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/b/a/b/g;
    .locals 5

    invoke-static {}, Lcom/jscape/inet/c/f;->e()[Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/c/c;->d:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const-string p2, ""

    :cond_1
    :goto_0
    aput-object p2, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-instance p2, Lcom/jscape/inet/b/a/b/g;

    sget-object v0, Lcom/jscape/inet/c/c;->d:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v1, v0, v1

    const/4 v3, 0x6

    aget-object v0, v0, v3

    new-array v2, v2, [Ljava/lang/Object;

    sget-object v3, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-static {p1}, Lcom/jscape/util/Base64Format;->formatData([B)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v1, p1}, Lcom/jscape/inet/b/a/b/g;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p2
.end method

.method protected a(Lcom/jscape/inet/b/a/b/j;Lcom/jscape/util/k/a/A;)Lcom/jscape/inet/b/a/i;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/b/a/b/j;",
            "Lcom/jscape/util/k/a/A<",
            "Lcom/jscape/inet/b/a/b/i;",
            ">;)",
            "Lcom/jscape/inet/b/a/i;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/c/c;->b(Lcom/jscape/inet/b/a/b/j;Lcom/jscape/util/k/a/A;)V

    invoke-virtual {p0, p2}, Lcom/jscape/inet/c/c;->a(Lcom/jscape/util/k/a/A;)Lcom/jscape/inet/b/a/i;

    move-result-object p1

    return-object p1
.end method

.method protected a(Lcom/jscape/inet/b/a/b/u;)Lcom/jscape/inet/b/a/i;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v7, Lcom/jscape/inet/b/a/i;

    iget-object v1, p1, Lcom/jscape/inet/b/a/b/u;->a:Ljava/lang/String;

    iget v2, p1, Lcom/jscape/inet/b/a/b/u;->c:I

    iget-object v3, p1, Lcom/jscape/inet/b/a/b/u;->d:Ljava/lang/String;

    new-instance v4, Lcom/jscape/inet/b/a/b/h;

    iget-object p1, p1, Lcom/jscape/inet/b/a/b/u;->e:[Lcom/jscape/inet/b/a/b/g;

    invoke-direct {v4, p1}, Lcom/jscape/inet/b/a/b/h;-><init>([Lcom/jscape/inet/b/a/b/g;)V

    new-instance v5, Lcom/jscape/util/h/k;

    invoke-direct {v5}, Lcom/jscape/util/h/k;-><init>()V

    new-instance v6, Lcom/jscape/inet/b/a/b/h;

    const/4 p1, 0x0

    new-array p1, p1, [Lcom/jscape/inet/b/a/b/g;

    invoke-direct {v6, p1}, Lcom/jscape/inet/b/a/b/h;-><init>([Lcom/jscape/inet/b/a/b/g;)V

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/b/a/i;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/jscape/inet/b/a/b/h;Ljava/io/InputStream;Lcom/jscape/inet/b/a/b/h;)V

    return-object v7
.end method

.method protected a(Lcom/jscape/util/k/a/A;)Lcom/jscape/inet/b/a/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/A<",
            "Lcom/jscape/inet/b/a/b/i;",
            ">;)",
            "Lcom/jscape/inet/b/a/i;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p1}, Lcom/jscape/util/k/a/A;->read()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/b/a/b/u;

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/c/c;->b(Lcom/jscape/util/k/a/r;Lcom/jscape/inet/b/a/b/i;)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/c/c;->a(Lcom/jscape/inet/b/a/b/u;)Lcom/jscape/inet/b/a/i;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/c/c;->a(Lcom/jscape/inet/b/a/i;)V

    return-object p1
.end method

.method public a(Lcom/jscape/util/k/a/C;Lcom/jscape/util/k/TransportAddress;Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/util/k/a/C;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/k/a/B;

    sget-object v1, Lcom/jscape/inet/c/c;->b:Lcom/jscape/util/h/I;

    invoke-direct {v0, p1, v1}, Lcom/jscape/util/k/a/B;-><init>(Lcom/jscape/util/k/a/C;Lcom/jscape/util/h/I;)V

    invoke-static {}, Lcom/jscape/inet/c/f;->e()[Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/jscape/inet/c/c;->d:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v4, 0x2

    new-array v5, v4, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/jscape/util/k/TransportAddress;->getHost()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-virtual {p2}, Lcom/jscape/util/k/TransportAddress;->getPort()I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    const/4 v6, 0x1

    aput-object p2, v5, v6

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    :try_start_0
    invoke-static {p3}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v1, :cond_0

    if-eqz v2, :cond_1

    :try_start_1
    new-array v2, v6, [Lcom/jscape/inet/b/a/b/g;

    invoke-virtual {p0, p3, p4}, Lcom/jscape/inet/c/c;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/b/a/b/g;

    move-result-object p3

    aput-object p3, v2, v3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :cond_0
    move v3, v2

    :cond_1
    new-array v2, v3, [Lcom/jscape/inet/b/a/b/g;

    :goto_0
    new-instance p3, Lcom/jscape/inet/b/a/b/k;

    sget-object p4, Lcom/jscape/inet/c/c;->d:[Ljava/lang/String;

    aget-object p4, p4, v4

    new-instance v3, Lcom/jscape/util/h/k;

    invoke-direct {v3}, Lcom/jscape/util/h/k;-><init>()V

    invoke-direct {p3, p2, p4, v2, v3}, Lcom/jscape/inet/b/a/b/k;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/jscape/inet/b/a/b/g;Ljava/io/InputStream;)V

    :try_start_2
    invoke-virtual {p0, p3, v0}, Lcom/jscape/inet/c/c;->a(Lcom/jscape/inet/b/a/b/j;Lcom/jscape/util/k/a/A;)Lcom/jscape/inet/b/a/i;

    if-nez v1, :cond_2

    new-array p2, v6, [I

    invoke-static {p2}, Lcom/jscape/util/aq;->b([I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/c/c;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/c/c;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/c/c;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method protected a(Lcom/jscape/inet/b/a/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/inet/b/a/i;->c()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/inet/b/a/d;

    invoke-direct {v0, p1}, Lcom/jscape/inet/b/a/d;-><init>(Lcom/jscape/inet/b/a/i;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/c/c;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method protected b(Lcom/jscape/inet/b/a/b/j;Lcom/jscape/util/k/a/A;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/b/a/b/j;",
            "Lcom/jscape/util/k/a/A<",
            "Lcom/jscape/inet/b/a/b/i;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p2, p1}, Lcom/jscape/inet/c/c;->a(Lcom/jscape/util/k/a/r;Lcom/jscape/inet/b/a/b/i;)V

    invoke-interface {p2, p1}, Lcom/jscape/util/k/a/A;->write(Ljava/lang/Object;)V

    return-void
.end method
