.class public Lcom/jscape/inet/c/f;
.super Ljava/lang/Object;


# static fields
.field private static e:[Ljava/lang/String;

.field private static final f:[Ljava/lang/String;


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/String;

    invoke-static {v3}, Lcom/jscape/inet/c/f;->b([Ljava/lang/String;)V

    const/4 v3, 0x7

    const-string v6, "2Z<ZBJv\u0017N\u0008#MIz.m\u0019>\\@J$lZ7]_M?#]"

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x47

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v3

    invoke-virtual {v6, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    const/16 v13, 0x1f

    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v14, v11

    const/4 v15, 0x0

    :goto_2
    const/16 v16, 0xc

    if-gt v14, v15, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v3

    if-ge v7, v13, :cond_0

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v3

    move v8, v11

    goto :goto_0

    :cond_0
    const-string v6, "\u001ar\u0011n}d\rW?\u0001 ?\u0012\u001ar\u0014|ke\u0014Y \u0000 ?<I\u001cxN:"

    move v8, v11

    move/from16 v3, v16

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v3

    if-ge v7, v13, :cond_2

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v3

    move v8, v11

    :goto_3
    const/16 v9, 0x6f

    add-int/2addr v7, v10

    add-int v11, v7, v3

    invoke-virtual {v6, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/c/f;->f:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v17, v11, v15

    rem-int/lit8 v4, v15, 0x7

    if-eqz v4, :cond_9

    if-eq v4, v10, :cond_8

    const/4 v5, 0x2

    if-eq v4, v5, :cond_7

    if-eq v4, v2, :cond_6

    if-eq v4, v0, :cond_5

    const/4 v5, 0x5

    if-eq v4, v5, :cond_4

    goto :goto_4

    :cond_4
    const/16 v16, 0x79

    goto :goto_4

    :cond_5
    const/16 v16, 0x77

    goto :goto_4

    :cond_6
    const/16 v16, 0x72

    goto :goto_4

    :cond_7
    const/16 v16, 0xb

    goto :goto_4

    :cond_8
    const/16 v16, 0x3d

    goto :goto_4

    :cond_9
    const/16 v16, 0x59

    :goto_4
    xor-int v4, v9, v16

    xor-int v4, v17, v4

    int-to-char v4, v4

    aput-char v4, v11, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 2

    const-string v0, ""

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v0, v0}, Lcom/jscape/inet/c/f;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/inet/c/f;)V
    .locals 3

    iget-object v0, p1, Lcom/jscape/inet/c/f;->a:Ljava/lang/String;

    iget v1, p1, Lcom/jscape/inet/c/f;->b:I

    iget-object v2, p1, Lcom/jscape/inet/c/f;->c:Ljava/lang/String;

    iget-object p1, p1, Lcom/jscape/inet/c/f;->d:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/jscape/inet/c/f;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/jscape/inet/c/f;->e()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/c/f;->a:Ljava/lang/String;

    iput p2, p0, Lcom/jscape/inet/c/f;->b:I

    const-string p1, ""

    if-eqz v0, :cond_1

    if-eqz p3, :cond_0

    goto :goto_0

    :cond_0
    move-object p3, p1

    :cond_1
    :goto_0
    iput-object p3, p0, Lcom/jscape/inet/c/f;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    if-eqz p4, :cond_2

    goto :goto_1

    :cond_2
    move-object p4, p1

    :cond_3
    :goto_1
    iput-object p4, p0, Lcom/jscape/inet/c/f;->d:Ljava/lang/String;

    return-void
.end method

.method public static b([Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/c/f;->e:[Ljava/lang/String;

    return-void
.end method

.method public static e()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/inet/c/f;->e:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/c/f;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/c/f;->b:I

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/c/f;->a:Ljava/lang/String;

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/c/f;->b:I

    return v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/c/f;->e()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/jscape/inet/c/f;->c:Ljava/lang/String;

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/c/f;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/c/f;->e()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/jscape/inet/c/f;->d:Ljava/lang/String;

    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/c/f;->d:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/c/f;->f:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/c/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/jscape/inet/c/f;->b:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/c/f;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
