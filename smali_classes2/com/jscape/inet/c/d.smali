.class public Lcom/jscape/inet/c/d;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/jscape/inet/c/e;

.field public static final b:Lcom/jscape/inet/c/e;

.field public static final c:Lcom/jscape/inet/c/e;

.field private static final d:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "{-U\u0000G\'\u0011K\r{e~`9I\u0012se}}?\\Lu\u0006{-U\u0000G&"

    const/16 v5, 0x1f

    const/4 v6, 0x6

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x43

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    const/4 v15, 0x4

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x16

    const-string v4, "\u0016@4m\u0011={\r\u0013\u0008\u0016O?d\u0005\u0013\u000b\u000bI*:\u0003"

    move v8, v11

    move v6, v15

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x35

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/c/d;->d:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/c/c;

    sget-object v1, Lcom/jscape/inet/c/d;->d:[Ljava/lang/String;

    aget-object v2, v1, v10

    invoke-static {v2}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/jscape/inet/c/c;-><init>(Ljava/util/logging/Logger;)V

    sput-object v0, Lcom/jscape/inet/c/d;->a:Lcom/jscape/inet/c/e;

    new-instance v0, Lcom/jscape/inet/c/h;

    aget-object v2, v1, v15

    invoke-static {v2}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/jscape/inet/c/h;-><init>(Ljava/util/logging/Logger;)V

    sput-object v0, Lcom/jscape/inet/c/d;->b:Lcom/jscape/inet/c/e;

    new-instance v0, Lcom/jscape/inet/c/i;

    aget-object v1, v1, v15

    invoke-static {v1}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/inet/c/i;-><init>(Ljava/util/logging/Logger;)V

    sput-object v0, Lcom/jscape/inet/c/d;->c:Lcom/jscape/inet/c/e;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    if-eq v2, v15, :cond_5

    if-eq v2, v0, :cond_4

    const/16 v2, 0x19

    goto :goto_4

    :cond_4
    const/16 v2, 0x50

    goto :goto_4

    :cond_5
    const/16 v2, 0x57

    goto :goto_4

    :cond_6
    const/16 v2, 0x8

    goto :goto_4

    :cond_7
    const/16 v2, 0x55

    goto :goto_4

    :cond_8
    const/16 v2, 0x21

    goto :goto_4

    :cond_9
    const/16 v2, 0x6b

    :goto_4
    xor-int/2addr v2, v9

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/inet/c/e;
    .locals 3

    invoke-static {}, Lcom/jscape/inet/c/f;->e()[Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/c/d;->d:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    sget-object p0, Lcom/jscape/inet/c/d;->a:Lcom/jscape/inet/c/e;

    return-object p0

    :cond_0
    sget-object v1, Lcom/jscape/inet/c/d;->d:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    :cond_1
    if-eqz v0, :cond_3

    if-eqz v1, :cond_2

    sget-object p0, Lcom/jscape/inet/c/d;->b:Lcom/jscape/inet/c/e;

    return-object p0

    :cond_2
    sget-object v0, Lcom/jscape/inet/c/d;->d:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    :cond_3
    if-eqz v1, :cond_4

    sget-object p0, Lcom/jscape/inet/c/d;->c:Lcom/jscape/inet/c/e;

    return-object p0

    :cond_4
    sget-object p0, Lcom/jscape/inet/c/e;->a:Lcom/jscape/inet/c/e;

    return-object p0
.end method
