.class public Lcom/jscape/inet/c/h;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/c/e;


# static fields
.field private static final b:Lcom/jscape/inet/c/a/a/a/a/g;

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final c:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "0=\u0017\'\u001f7g\u001es\'\u00071\u0015ZMs\u0004:\u001d&pY>\u0011;\u0001?n\u001cs\u0015<R\u0005,\nsHeL~,\n\u000eNhW-\'16&\u0000/\u001d7g\u001es\'\u00071\u0015ZMs\u0004:\u001d&pY>\u0011;\u0001?n\u001cs\u0015<R\u0005,\nsHeL~,\n\u000eNhW-\'"

    const/16 v5, 0x63

    const/16 v6, 0x31

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x71

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x32

    const/16 v4, 0x1d

    const-string v6, "#2\u001e?\r4e\u00029\u001fg\u0014?e\u00191\u0012)\u001aqk\u0013/\u0008&\u001a4<V\u0014%\u00138\u000c.e&\u0006.\u0014?\u0004qc\u0004.\u00145Gq"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v16, v6

    move v6, v4

    move-object/from16 v4, v16

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x7e

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/c/h;->d:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/c/a/a/a/a/g;

    new-array v1, v3, [Lcom/jscape/inet/c/a/a/a/a/h;

    invoke-direct {v0, v1}, Lcom/jscape/inet/c/a/a/a/a/g;-><init>([Lcom/jscape/inet/c/a/a/a/a/h;)V

    invoke-static {v0}, Lcom/jscape/inet/c/a/a/a/a/i;->a(Lcom/jscape/inet/c/a/a/a/a/g;)Lcom/jscape/inet/c/a/a/a/a/g;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/c/h;->b:Lcom/jscape/inet/c/a/a/a/a/g;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    const/4 v3, 0x3

    if-eqz v2, :cond_8

    if-eq v2, v10, :cond_7

    const/4 v10, 0x2

    if-eq v2, v10, :cond_6

    if-eq v2, v3, :cond_5

    if-eq v2, v0, :cond_9

    const/4 v10, 0x5

    if-eq v2, v10, :cond_4

    const/16 v3, 0x78

    goto :goto_4

    :cond_4
    const/16 v3, 0x2f

    goto :goto_4

    :cond_5
    const/16 v3, 0x39

    goto :goto_4

    :cond_6
    const/4 v10, 0x5

    move v3, v10

    goto :goto_4

    :cond_7
    const/16 v3, 0x22

    goto :goto_4

    :cond_8
    const/16 v3, 0x8

    :cond_9
    :goto_4
    xor-int v2, v9, v3

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v3, 0x0

    const/4 v10, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/util/logging/Logger;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/c/h;->c:Ljava/util/logging/Logger;

    return-void
.end method

.method private a(Ljava/lang/Class;Ljava/io/InputStream;Lcom/jscape/util/k/a/r;)Lcom/jscape/inet/c/a/a/a/b/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/jscape/inet/c/a/a/a/b/b;",
            ">(",
            "Ljava/lang/Class<",
            "TM;>;",
            "Ljava/io/InputStream;",
            "Lcom/jscape/util/k/a/r;",
            ")TM;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/c/h;->b:Lcom/jscape/inet/c/a/a/a/a/g;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/c/a/a/a/a/g;->a(Ljava/lang/Class;Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/a/b/b;

    move-result-object p1

    invoke-direct {p0, p3, p1}, Lcom/jscape/inet/c/h;->b(Lcom/jscape/util/k/a/r;Lcom/jscape/inet/c/a/a/a/b/b;)V

    return-object p1
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/inet/c/a/a/a/b/b;Ljava/io/OutputStream;Lcom/jscape/util/k/a/r;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p3, p1}, Lcom/jscape/inet/c/h;->a(Lcom/jscape/util/k/a/r;Lcom/jscape/inet/c/a/a/a/b/b;)V

    sget-object p3, Lcom/jscape/inet/c/h;->b:Lcom/jscape/inet/c/a/a/a/a/g;

    invoke-virtual {p3, p1, p2}, Lcom/jscape/inet/c/a/a/a/a/g;->a(Lcom/jscape/inet/c/a/a/a/b/b;Ljava/io/OutputStream;)V

    return-void
.end method

.method private a(Lcom/jscape/util/k/a/r;Lcom/jscape/inet/c/a/a/a/b/b;)V
    .locals 7

    invoke-static {}, Lcom/jscape/inet/c/f;->e()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/c/h;->c:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/c/h;->c:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/c/h;->d:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v0, v0, v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object p1

    aput-object p1, v4, v3

    const/4 p1, 0x2

    aput-object p2, v4, p1

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private b(Lcom/jscape/util/k/a/r;Lcom/jscape/inet/c/a/a/a/b/b;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/c/f;->e()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/c/h;->c:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/c/h;->c:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/c/h;->d:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v3, 0x1

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object p1

    aput-object p1, v4, v3

    const/4 p1, 0x2

    aput-object p2, v4, p1

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/util/k/a/C;Lcom/jscape/util/k/TransportAddress;Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/util/k/a/C;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance p4, Lcom/jscape/util/k/a/a;

    invoke-direct {p4, p1}, Lcom/jscape/util/k/a/a;-><init>(Lcom/jscape/util/k/a/C;)V

    new-instance v0, Lcom/jscape/util/k/a/e;

    invoke-direct {v0, p1}, Lcom/jscape/util/k/a/e;-><init>(Lcom/jscape/util/k/a/C;)V

    new-instance v1, Lcom/jscape/inet/c/a/a/a/b/e;

    invoke-virtual {p2}, Lcom/jscape/util/k/TransportAddress;->getPort()I

    move-result v2

    invoke-virtual {p2}, Lcom/jscape/util/k/TransportAddress;->inetAddress()Ljava/net/InetAddress;

    move-result-object p2

    check-cast p2, Ljava/net/Inet4Address;

    invoke-direct {v1, v2, p2, p3}, Lcom/jscape/inet/c/a/a/a/b/e;-><init>(ILjava/net/Inet4Address;Ljava/lang/String;)V

    invoke-direct {p0, v1, v0, p1}, Lcom/jscape/inet/c/h;->a(Lcom/jscape/inet/c/a/a/a/b/b;Ljava/io/OutputStream;Lcom/jscape/util/k/a/r;)V

    const-class p2, Lcom/jscape/inet/c/a/a/a/b/f;

    invoke-direct {p0, p2, p4, p1}, Lcom/jscape/inet/c/h;->a(Ljava/lang/Class;Ljava/io/InputStream;Lcom/jscape/util/k/a/r;)Lcom/jscape/inet/c/a/a/a/b/b;

    move-result-object p2

    check-cast p2, Lcom/jscape/inet/c/a/a/a/b/f;

    :try_start_0
    iget p3, p2, Lcom/jscape/inet/c/a/a/a/b/f;->a:I

    sget-object p4, Lcom/jscape/inet/c/a/a/a/b/a;->a:Lcom/jscape/inet/c/a/a/a/b/a;

    iget p4, p4, Lcom/jscape/inet/c/a/a/a/b/a;->e:I

    if-ne p3, p4, :cond_0

    return-object p1

    :cond_0
    new-instance p1, Lcom/jscape/inet/c/b;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p4, Lcom/jscape/inet/c/h;->d:[Ljava/lang/String;

    const/4 v0, 0x3

    aget-object p4, p4, v0

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p2, Lcom/jscape/inet/c/a/a/a/b/f;->a:I

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/jscape/inet/c/b;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/c/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method
