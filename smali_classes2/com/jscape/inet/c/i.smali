.class public Lcom/jscape/inet/c/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/c/e;


# static fields
.field private static final b:Lcom/jscape/inet/c/a/a/b/a/n;

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final c:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, ":\u0010d\u0015r+\u0003\u0019-H&X>F\u001b-H,\u001b>B\u001c+O;OjJ\n>S7Np\u0003\u000f>N2Dz\r1 1D1LwM\u000e\u007ft\u0011bUp\\\u007fW,NfZI2B-R\u007fD\u000c\u007fF*\u0001E\u0006\u001a\u007f\u001bs\u001f>\u0006\u001a\u0002\u001d~\u0004m\r1&*S9NwM\u000e\u007ft\u0011bUp\\\u007fW,NfZI2B-R\u007fD\u000c\u007fF*\u0001E\u0006\u001a\u007f\u001bs\u001f>\u0006\u001a\u0002\u001d~\u0004m\r\u001d<1B&Q{@\u001d:C~Hp@\u00062N0F>N\u000c,T?F{\u0019I"

    const/16 v4, 0xac

    const/16 v5, 0x2a

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0xc

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x50

    const/16 v3, 0x14

    const-string v5, "t^*[<eMWc\u0006h\u0016p\u0008Uc\u0006bUp;t^*[<eMWc\u0006h\u0016p\u0008Uc\u0006bUp\u0018Ib\u001c`\u001f?\u001fSt\r0\u000e%\u0019Ot\u0007d\u00063\u000cSx\u0006~O=\u0008Sy\u0006tOxHT8G"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x42

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/c/i;->d:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/c/a/a/b/a/n;

    new-array v1, v2, [Lcom/jscape/inet/c/a/a/b/a/o;

    invoke-direct {v0, v1}, Lcom/jscape/inet/c/a/a/b/a/n;-><init>([Lcom/jscape/inet/c/a/a/b/a/o;)V

    invoke-static {v0}, Lcom/jscape/inet/c/a/a/b/a/p;->a(Lcom/jscape/inet/c/a/a/b/a/n;)Lcom/jscape/inet/c/a/a/b/a/n;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/c/i;->b:Lcom/jscape/inet/c/a/a/b/a/n;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x2f

    goto :goto_4

    :cond_4
    const/16 v1, 0x12

    goto :goto_4

    :cond_5
    const/16 v1, 0x2d

    goto :goto_4

    :cond_6
    const/16 v1, 0x52

    goto :goto_4

    :cond_7
    const/16 v1, 0x2b

    goto :goto_4

    :cond_8
    const/16 v1, 0x53

    goto :goto_4

    :cond_9
    const/16 v1, 0x65

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/util/logging/Logger;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/c/i;->c:Ljava/util/logging/Logger;

    return-void
.end method

.method private a(Lcom/jscape/util/k/a/C;Ljava/io/InputStream;Ljava/io/OutputStream;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/c/a/a/b/b/f;

    sget-object v1, Lcom/jscape/inet/c/a/a/b/b/a;->a:Lcom/jscape/inet/c/a/a/b/b/a;

    sget-object v2, Lcom/jscape/inet/c/a/a/b/b/a;->c:Lcom/jscape/inet/c/a/a/b/b/a;

    invoke-direct {v0, v1, v2}, Lcom/jscape/inet/c/a/a/b/b/f;-><init>(Lcom/jscape/inet/c/a/a/b/b/a;Lcom/jscape/inet/c/a/a/b/b/a;)V

    invoke-direct {p0, v0, p3, p1}, Lcom/jscape/inet/c/i;->a(Lcom/jscape/inet/c/a/a/b/b/e;Ljava/io/OutputStream;Lcom/jscape/util/k/a/r;)V

    invoke-static {}, Lcom/jscape/inet/c/f;->e()[Ljava/lang/String;

    move-result-object p3

    const-class v0, Lcom/jscape/inet/c/a/a/b/b/g;

    invoke-direct {p0, v0, p2, p1}, Lcom/jscape/inet/c/i;->a(Ljava/lang/Class;Ljava/io/InputStream;Lcom/jscape/util/k/a/r;)Lcom/jscape/inet/c/a/a/b/b/e;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/c/a/a/b/b/g;

    :try_start_0
    iget p2, p1, Lcom/jscape/inet/c/a/a/b/b/g;->a:I

    if-eqz p3, :cond_2

    sget-object v0, Lcom/jscape/inet/c/a/a/b/b/a;->a:Lcom/jscape/inet/c/a/a/b/b/a;

    iget v0, v0, Lcom/jscape/inet/c/a/a/b/b/a;->e:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eq p2, v0, :cond_1

    :try_start_1
    iget p2, p1, Lcom/jscape/inet/c/a/a/b/b/g;->a:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz p3, :cond_2

    :try_start_2
    sget-object p3, Lcom/jscape/inet/c/a/a/b/b/a;->c:Lcom/jscape/inet/c/a/a/b/b/a;

    iget p3, p3, Lcom/jscape/inet/c/a/a/b/b/a;->e:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    if-ne p2, p3, :cond_0

    goto :goto_0

    :cond_0
    :try_start_3
    new-instance p2, Lcom/jscape/inet/c/b;

    sget-object p3, Lcom/jscape/inet/c/i;->d:[Ljava/lang/String;

    const/4 v0, 0x5

    aget-object p3, p3, v0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget p1, p1, Lcom/jscape/inet/c/a/a/b/b/g;->a:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v1

    invoke-static {p3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/c/b;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_1
    :goto_0
    iget p2, p1, Lcom/jscape/inet/c/a/a/b/b/g;->a:I

    :cond_2
    return p2

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/c/i;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/c/i;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/c/i;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/c/i;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method private a(Ljava/lang/Class;Ljava/io/InputStream;Lcom/jscape/util/k/a/r;)Lcom/jscape/inet/c/a/a/b/b/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/jscape/inet/c/a/a/b/b/e;",
            ">(",
            "Ljava/lang/Class<",
            "TM;>;",
            "Ljava/io/InputStream;",
            "Lcom/jscape/util/k/a/r;",
            ")TM;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/c/i;->b:Lcom/jscape/inet/c/a/a/b/a/n;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/c/a/a/b/a/n;->a(Ljava/lang/Class;Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/b/b/e;

    move-result-object p1

    invoke-direct {p0, p3, p1}, Lcom/jscape/inet/c/i;->b(Lcom/jscape/util/k/a/r;Lcom/jscape/inet/c/a/a/b/b/e;)V

    return-object p1
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/inet/c/a/a/b/b/e;Ljava/io/OutputStream;Lcom/jscape/util/k/a/r;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p3, p1}, Lcom/jscape/inet/c/i;->a(Lcom/jscape/util/k/a/r;Lcom/jscape/inet/c/a/a/b/b/e;)V

    sget-object p3, Lcom/jscape/inet/c/i;->b:Lcom/jscape/inet/c/a/a/b/a/n;

    invoke-virtual {p3, p1, p2}, Lcom/jscape/inet/c/a/a/b/a/n;->a(Lcom/jscape/inet/c/a/a/b/b/e;Ljava/io/OutputStream;)V

    return-void
.end method

.method private a(Lcom/jscape/util/k/a/r;Lcom/jscape/inet/c/a/a/b/b/e;)V
    .locals 7

    invoke-static {}, Lcom/jscape/inet/c/f;->e()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/c/i;->c:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/c/i;->c:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/c/i;->d:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v0, v0, v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object p1

    aput-object p1, v4, v5

    aput-object p2, v4, v3

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/jscape/util/k/a/C;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/c/a/a/b/b/m;

    invoke-direct {v0, p1, p2}, Lcom/jscape/inet/c/a/a/b/b/m;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0, p4, p5}, Lcom/jscape/inet/c/i;->a(Lcom/jscape/inet/c/a/a/b/b/e;Ljava/io/OutputStream;Lcom/jscape/util/k/a/r;)V

    const-class p1, Lcom/jscape/inet/c/a/a/b/b/n;

    invoke-direct {p0, p1, p3, p5}, Lcom/jscape/inet/c/i;->a(Ljava/lang/Class;Ljava/io/InputStream;Lcom/jscape/util/k/a/r;)Lcom/jscape/inet/c/a/a/b/b/e;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/c/a/a/b/b/n;

    :try_start_0
    iget p1, p1, Lcom/jscape/inet/c/a/a/b/b/n;->a:I

    sget-object p2, Lcom/jscape/inet/c/a/a/b/b/d;->a:Lcom/jscape/inet/c/a/a/b/b/d;

    iget p2, p2, Lcom/jscape/inet/c/a/a/b/b/d;->b:I

    if-ne p1, p2, :cond_0

    return-void

    :cond_0
    new-instance p1, Lcom/jscape/inet/c/b;

    sget-object p2, Lcom/jscape/inet/c/i;->d:[Ljava/lang/String;

    const/4 p3, 0x0

    aget-object p2, p2, p3

    invoke-direct {p1, p2}, Lcom/jscape/inet/c/b;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/c/i;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method private b(Lcom/jscape/util/k/a/r;Lcom/jscape/inet/c/a/a/b/b/e;)V
    .locals 7

    invoke-static {}, Lcom/jscape/inet/c/f;->e()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/c/i;->c:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/c/i;->c:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/c/i;->d:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v0, v0, v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object p1

    aput-object p1, v4, v3

    const/4 p1, 0x2

    aput-object p2, v4, p1

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/util/k/a/C;Lcom/jscape/util/k/TransportAddress;Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/util/k/a/C;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v6, Lcom/jscape/util/k/a/a;

    invoke-direct {v6, p1}, Lcom/jscape/util/k/a/a;-><init>(Lcom/jscape/util/k/a/C;)V

    invoke-static {}, Lcom/jscape/inet/c/f;->e()[Ljava/lang/String;

    move-result-object v0

    new-instance v7, Lcom/jscape/util/k/a/e;

    invoke-direct {v7, p1}, Lcom/jscape/util/k/a/e;-><init>(Lcom/jscape/util/k/a/C;)V

    invoke-direct {p0, p1, v6, v7}, Lcom/jscape/inet/c/i;->a(Lcom/jscape/util/k/a/C;Ljava/io/InputStream;Ljava/io/OutputStream;)I

    move-result v1

    if-eqz v0, :cond_1

    :try_start_0
    sget-object v0, Lcom/jscape/inet/c/a/a/b/b/a;->c:Lcom/jscape/inet/c/a/a/b/b/a;

    iget v0, v0, Lcom/jscape/inet/c/a/a/b/b/a;->e:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v1, v0, :cond_0

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    move-object v3, v6

    move-object v4, v7

    move-object v5, p1

    :try_start_1
    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/c/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/jscape/util/k/a/C;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    new-instance p3, Lcom/jscape/inet/c/a/a/b/b/k;

    invoke-virtual {p2}, Lcom/jscape/util/k/TransportAddress;->inetAddress()Ljava/net/InetAddress;

    move-result-object p4

    invoke-virtual {p2}, Lcom/jscape/util/k/TransportAddress;->getPort()I

    move-result p2

    invoke-direct {p3, p4, p2}, Lcom/jscape/inet/c/a/a/b/b/k;-><init>(Ljava/net/InetAddress;I)V

    invoke-direct {p0, p3, v7, p1}, Lcom/jscape/inet/c/i;->a(Lcom/jscape/inet/c/a/a/b/b/e;Ljava/io/OutputStream;Lcom/jscape/util/k/a/r;)V

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/c/i;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/c/i;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    const-class p2, Lcom/jscape/inet/c/a/a/b/b/l;

    invoke-direct {p0, p2, v6, p1}, Lcom/jscape/inet/c/i;->a(Ljava/lang/Class;Ljava/io/InputStream;Lcom/jscape/util/k/a/r;)Lcom/jscape/inet/c/a/a/b/b/e;

    move-result-object p2

    check-cast p2, Lcom/jscape/inet/c/a/a/b/b/l;

    :try_start_3
    iget p3, p2, Lcom/jscape/inet/c/a/a/b/b/l;->a:I

    sget-object p4, Lcom/jscape/inet/c/a/a/b/b/b;->a:Lcom/jscape/inet/c/a/a/b/b/b;

    iget p4, p4, Lcom/jscape/inet/c/a/a/b/b/b;->j:I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    if-ne p3, p4, :cond_3

    :try_start_4
    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p2

    if-nez p2, :cond_2

    const/4 p2, 0x3

    new-array p2, p2, [Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/inet/c/f;->b([Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :cond_2
    return-object p1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/c/i;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_3
    :try_start_5
    new-instance p1, Lcom/jscape/inet/c/b;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p4, Lcom/jscape/inet/c/i;->d:[Ljava/lang/String;

    const/4 v0, 0x4

    aget-object p4, p4, v0

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p2, Lcom/jscape/inet/c/a/a/b/b/l;->a:I

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/jscape/inet/c/b;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/c/i;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method
