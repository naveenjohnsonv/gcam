.class public Lcom/jscape/inet/c/g;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/k/a/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/k/a/v<",
        "Lcom/jscape/util/k/a/C;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/inet/c/f;

.field private final b:Lcom/jscape/inet/c/e;

.field private final c:Lcom/jscape/util/k/b/i;

.field private final d:Lcom/jscape/util/k/g;


# direct methods
.method public constructor <init>(Lcom/jscape/inet/c/f;Lcom/jscape/inet/c/e;Lcom/jscape/util/k/g;Lcom/jscape/util/k/b/j;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/c/g;->a:Lcom/jscape/inet/c/f;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/c/g;->b:Lcom/jscape/inet/c/e;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/c/g;->d:Lcom/jscape/util/k/g;

    new-instance p1, Lcom/jscape/util/k/b/i;

    new-instance p2, Lcom/jscape/util/k/g;

    invoke-virtual {p3}, Lcom/jscape/util/k/g;->a()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    new-instance v1, Lcom/jscape/util/k/TransportAddress;

    iget-object v2, p0, Lcom/jscape/inet/c/g;->a:Lcom/jscape/inet/c/f;

    invoke-virtual {v2}, Lcom/jscape/inet/c/f;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/inet/c/g;->a:Lcom/jscape/inet/c/f;

    invoke-virtual {v3}, Lcom/jscape/inet/c/f;->b()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p3}, Lcom/jscape/util/k/g;->c()J

    move-result-wide v2

    invoke-direct {p2, v0, v1, v2, v3}, Lcom/jscape/util/k/g;-><init>(Lcom/jscape/util/k/TransportAddress;Lcom/jscape/util/k/TransportAddress;J)V

    invoke-direct {p1, p2, p4}, Lcom/jscape/util/k/b/i;-><init>(Lcom/jscape/util/k/g;Lcom/jscape/util/k/b/j;)V

    iput-object p1, p0, Lcom/jscape/inet/c/g;->c:Lcom/jscape/util/k/b/i;

    return-void
.end method


# virtual methods
.method public a()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/c/g;->d:Lcom/jscape/util/k/g;

    invoke-virtual {v0}, Lcom/jscape/util/k/g;->b()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/jscape/util/k/a/C;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/c;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/c/g;->c:Lcom/jscape/util/k/b/i;

    invoke-virtual {v0}, Lcom/jscape/util/k/b/i;->b()Lcom/jscape/util/k/a/C;

    move-result-object v0

    check-cast v0, Lcom/jscape/util/k/b/l;

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/c/g;->b:Lcom/jscape/inet/c/e;

    iget-object v2, p0, Lcom/jscape/inet/c/g;->d:Lcom/jscape/util/k/g;

    invoke-virtual {v2}, Lcom/jscape/util/k/g;->b()Lcom/jscape/util/k/TransportAddress;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/inet/c/g;->a:Lcom/jscape/inet/c/f;

    invoke-virtual {v3}, Lcom/jscape/inet/c/f;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/jscape/inet/c/g;->a:Lcom/jscape/inet/c/f;

    invoke-virtual {v4}, Lcom/jscape/inet/c/f;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v0, v2, v3, v4}, Lcom/jscape/inet/c/e;->a(Lcom/jscape/util/k/a/C;Lcom/jscape/util/k/TransportAddress;Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/util/k/a/C;

    iget-object v1, p0, Lcom/jscape/inet/c/g;->d:Lcom/jscape/util/k/g;

    invoke-virtual {v1}, Lcom/jscape/util/k/g;->b()Lcom/jscape/util/k/TransportAddress;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jscape/util/k/b/l;->b(Lcom/jscape/util/k/TransportAddress;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v1

    invoke-virtual {v0}, Lcom/jscape/util/k/b/l;->close()V

    new-instance v0, Lcom/jscape/util/k/a/c;

    invoke-direct {v0, v1}, Lcom/jscape/util/k/a/c;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public bridge synthetic connect()Lcom/jscape/util/k/a/r;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/c;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/inet/c/g;->b()Lcom/jscape/util/k/a/C;

    move-result-object v0

    return-object v0
.end method
