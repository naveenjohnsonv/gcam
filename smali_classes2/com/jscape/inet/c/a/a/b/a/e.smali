.class public abstract Lcom/jscape/inet/c/a/a/b/a/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/c/a/a/b/b/e;",
        ">;"
    }
.end annotation


# static fields
.field protected static final a:I = 0x5

.field private static b:[Lcom/jscape/util/aq;

.field private static final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/jscape/inet/c/a/a/b/a/e;->b([Lcom/jscape/util/aq;)V

    const-string v0, "t{MA\u001a\u0014\u0000Sa[PJ\t\nRf_S\u000fD\u0019DgM]\u0005\nU\u0001"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/c/a/a/b/a/e;->f:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x73

    goto :goto_1

    :cond_1
    const/16 v4, 0x78

    goto :goto_1

    :cond_2
    const/16 v4, 0x76

    goto :goto_1

    :cond_3
    const/16 v4, 0x28

    goto :goto_1

    :cond_4
    const/16 v4, 0x22

    goto :goto_1

    :cond_5
    const/16 v4, 0x9

    goto :goto_1

    :cond_6
    const/16 v4, 0x3d

    :goto_1
    const/16 v5, 0x1c

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static b(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method public static b([Lcom/jscape/util/aq;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/c/a/a/b/a/e;->b:[Lcom/jscape/util/aq;

    return-void
.end method

.method public static b()[Lcom/jscape/util/aq;
    .locals 1

    sget-object v0, Lcom/jscape/inet/c/a/a/b/a/e;->b:[Lcom/jscape/util/aq;

    return-object v0
.end method


# virtual methods
.method protected a(I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/c/a/a/b/a/e;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/c/a/a/b/a/e;->b(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method
