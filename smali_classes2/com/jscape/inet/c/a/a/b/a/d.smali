.class public Lcom/jscape/inet/c/a/a/b/a/d;
.super Lcom/jscape/inet/c/a/a/b/a/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/c/a/a/b/a/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/b/b/e;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lcom/jscape/inet/c/a/a/b/a/d;->a(I)V

    invoke-static {p1}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result p1

    and-int/lit16 p1, p1, 0xff

    new-instance v0, Lcom/jscape/inet/c/a/a/b/b/n;

    invoke-direct {v0, p1}, Lcom/jscape/inet/c/a/a/b/b/n;-><init>(I)V

    return-object v0
.end method

.method public a(Lcom/jscape/inet/c/a/a/b/b/e;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/c/a/a/b/b/n;

    const/4 v0, 0x1

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    iget p1, p1, Lcom/jscape/inet/c/a/a/b/b/n;->a:I

    int-to-byte p1, p1

    invoke-static {p1, p2}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/c/a/a/b/a/d;->a(Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/b/b/e;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/c/a/a/b/b/e;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/c/a/a/b/a/d;->a(Lcom/jscape/inet/c/a/a/b/b/e;Ljava/io/OutputStream;)V

    return-void
.end method
