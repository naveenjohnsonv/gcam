.class public Lcom/jscape/inet/c/a/a/b/a/n;
.super Ljava/lang/Object;


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class;",
            "Lcom/jscape/inet/c/a/a/b/a/o;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class;",
            "Lcom/jscape/inet/c/a/a/b/a/o;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/16 v2, 0x1e

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x8

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "K|e\nLX=lfs\u001b\u001cE7maw\u0018Y\u00081rse\u000c\u0006\u0008wm<\u0018K|e\nLX=lfs\u001b\u001cE7maw\u0018Y\u0012r;a8"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x37

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/c/a/a/b/a/n;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    if-eqz v13, :cond_7

    if-eq v13, v8, :cond_6

    if-eq v13, v0, :cond_5

    const/4 v14, 0x3

    if-eq v13, v14, :cond_4

    const/4 v14, 0x4

    if-eq v13, v14, :cond_3

    const/4 v14, 0x5

    if-eq v13, v14, :cond_2

    const/16 v13, 0x5a

    goto :goto_2

    :cond_2
    const/16 v13, 0x20

    goto :goto_2

    :cond_3
    const/16 v13, 0x34

    goto :goto_2

    :cond_4
    const/16 v13, 0x77

    goto :goto_2

    :cond_5
    move v13, v2

    goto :goto_2

    :cond_6
    const/16 v13, 0x1a

    goto :goto_2

    :cond_7
    const/16 v13, 0x16

    :goto_2
    xor-int/2addr v13, v7

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method public varargs constructor <init>([Lcom/jscape/inet/c/a/a/b/a/o;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/c/a/a/b/a/n;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/c/a/a/b/a/n;->b:Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/c/a/a/b/a/n;->a([Lcom/jscape/inet/c/a/a/b/a/o;)Lcom/jscape/inet/c/a/a/b/a/n;

    return-void
.end method

.method private a(Lcom/jscape/inet/c/a/a/b/b/e;)Lcom/jscape/inet/c/a/a/b/a/o;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/c/a/a/b/a/e;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/c/a/a/b/a/n;->b:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/c/a/a/b/a/o;

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lcom/jscape/inet/c/a/a/b/a/n;->c:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/c/a/a/b/a/n;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method

.method private a(Ljava/lang/Class;)Lcom/jscape/inet/c/a/a/b/a/o;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/c/a/a/b/a/e;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/c/a/a/b/a/n;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/c/a/a/b/a/o;

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lcom/jscape/inet/c/a/a/b/a/n;->c:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/c/a/a/b/a/n;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public varargs a([Lcom/jscape/inet/c/a/a/b/a/o;)Lcom/jscape/inet/c/a/a/b/a/n;
    .locals 9

    array-length v0, p1

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p1, v2

    iget-object v4, p0, Lcom/jscape/inet/c/a/a/b/a/n;->a:Ljava/util/Map;

    iget-object v5, v3, Lcom/jscape/inet/c/a/a/b/a/o;->a:Ljava/lang/Class;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v3, Lcom/jscape/inet/c/a/a/b/a/o;->c:[Ljava/lang/Class;

    array-length v5, v4

    move v6, v1

    :goto_1
    if-ge v6, v5, :cond_0

    aget-object v7, v4, v6

    iget-object v8, p0, Lcom/jscape/inet/c/a/a/b/a/n;->b:Ljava/util/Map;

    invoke-interface {v8, v7, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object p0
.end method

.method public a(Ljava/lang/Class;Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/b/b/e;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/c/a/a/b/a/n;->a(Ljava/lang/Class;)Lcom/jscape/inet/c/a/a/b/a/o;

    move-result-object p1

    iget-object p1, p1, Lcom/jscape/inet/c/a/a/b/a/o;->b:Lcom/jscape/util/h/I;

    invoke-interface {p1, p2}, Lcom/jscape/util/h/I;->read(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/c/a/a/b/b/e;

    return-object p1
.end method

.method public a(Lcom/jscape/inet/c/a/a/b/b/e;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/c/a/a/b/a/n;->a(Lcom/jscape/inet/c/a/a/b/b/e;)Lcom/jscape/inet/c/a/a/b/a/o;

    move-result-object v0

    new-instance v1, Lcom/jscape/util/h/o;

    invoke-direct {v1}, Lcom/jscape/util/h/o;-><init>()V

    iget-object v0, v0, Lcom/jscape/inet/c/a/a/b/a/o;->b:Lcom/jscape/util/h/I;

    invoke-interface {v0, p1, v1}, Lcom/jscape/util/h/I;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->a()[B

    move-result-object p1

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->b()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p2, p1, v1, v0}, Ljava/io/OutputStream;->write([BII)V

    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    return-void
.end method
