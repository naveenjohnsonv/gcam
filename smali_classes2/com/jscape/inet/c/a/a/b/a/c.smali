.class public Lcom/jscape/inet/c/a/a/b/a/c;
.super Lcom/jscape/inet/c/a/a/b/a/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/c/a/a/b/a/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/b/b/e;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lcom/jscape/inet/c/a/a/b/a/c;->a(I)V

    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, p1}, Lcom/jscape/util/h/a/d;->a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v1, p1}, Lcom/jscape/util/h/a/d;->a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/c/a/a/b/b/m;

    invoke-direct {v1, v0, p1}, Lcom/jscape/inet/c/a/a/b/b/m;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public a(Lcom/jscape/inet/c/a/a/b/b/e;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/c/a/a/b/b/m;

    const/4 v0, 0x1

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    iget-object v0, p1, Lcom/jscape/inet/c/a/a/b/b/m;->a:Ljava/lang/String;

    sget-object v1, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, v1, p2}, Lcom/jscape/util/h/a/d;->a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/c/a/a/b/b/m;->c:Ljava/lang/String;

    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {p1, v0, p2}, Lcom/jscape/util/h/a/d;->a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/c/a/a/b/a/c;->a(Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/b/b/e;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/c/a/a/b/b/e;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/c/a/a/b/a/c;->a(Lcom/jscape/inet/c/a/a/b/b/e;Ljava/io/OutputStream;)V

    return-void
.end method
