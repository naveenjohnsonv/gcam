.class public Lcom/jscape/inet/c/a/a/b/a/f;
.super Lcom/jscape/inet/c/a/a/b/a/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/c/a/a/b/a/e;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/b/b/e;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lcom/jscape/inet/c/a/a/b/a/f;->a(I)V

    invoke-static {p1}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    new-array v1, v0, [I

    invoke-static {}, Lcom/jscape/inet/c/a/a/b/a/e;->b()[Lcom/jscape/util/aq;

    move-result-object v2

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v0, :cond_1

    invoke-static {p1}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    aput v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    if-eqz v2, :cond_0

    :cond_1
    :try_start_0
    new-instance p1, Lcom/jscape/inet/c/a/a/b/b/f;

    invoke-direct {p1, v1}, Lcom/jscape/inet/c/a/a/b/b/f;-><init>([I)V

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/util/aq;

    invoke-static {v0}, Lcom/jscape/inet/c/a/a/b/a/e;->b([Lcom/jscape/util/aq;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/c/a/a/b/a/f;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method public a(Lcom/jscape/inet/c/a/a/b/b/e;Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/c/a/a/b/b/f;

    invoke-static {}, Lcom/jscape/inet/c/a/a/b/a/e;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    const/4 v1, 0x5

    invoke-static {v1, p2}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    iget-object v1, p1, Lcom/jscape/inet/c/a/a/b/b/f;->a:[I

    array-length v1, v1

    int-to-byte v1, v1

    invoke-static {v1, p2}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    const/4 v1, 0x0

    :cond_0
    iget-object v2, p1, Lcom/jscape/inet/c/a/a/b/b/f;->a:[I

    array-length v2, v2

    if-ge v1, v2, :cond_1

    iget-object v2, p1, Lcom/jscape/inet/c/a/a/b/b/f;->a:[I

    aget v2, v2, v1

    int-to-byte v2, v2

    invoke-static {v2, p2}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    add-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/c/a/a/b/a/f;->a(Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/b/b/e;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/c/a/a/b/b/e;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/c/a/a/b/a/f;->a(Lcom/jscape/inet/c/a/a/b/b/e;Ljava/io/OutputStream;)V

    return-void
.end method
