.class public Lcom/jscape/inet/c/a/a/b/b/f;
.super Lcom/jscape/inet/c/a/a/b/b/e;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public final a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "\u0000#hF#(J(5}Z/)P\u000c3hF)\"l$\'iK52\u001e:;yZ.)Z2k"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/c/a/a/b/b/f;->c:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v5, 0x35

    if-eqz v4, :cond_4

    const/4 v6, 0x1

    if-eq v4, v6, :cond_3

    const/4 v6, 0x2

    if-eq v4, v6, :cond_2

    const/4 v6, 0x3

    if-eq v4, v6, :cond_1

    const/4 v6, 0x4

    if-eq v4, v6, :cond_5

    const/4 v6, 0x5

    if-eq v4, v6, :cond_5

    const/16 v5, 0x4d

    goto :goto_1

    :cond_1
    const/16 v5, 0x5d

    goto :goto_1

    :cond_2
    const/16 v5, 0x6f

    goto :goto_1

    :cond_3
    const/16 v5, 0x25

    goto :goto_1

    :cond_4
    const/16 v5, 0x32

    :cond_5
    :goto_1
    const/16 v4, 0x73

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/jscape/inet/c/a/a/b/b/a;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [I

    iget p1, p1, Lcom/jscape/inet/c/a/a/b/b/a;->e:I

    const/4 v1, 0x0

    aput p1, v0, v1

    invoke-direct {p0, v0}, Lcom/jscape/inet/c/a/a/b/b/f;-><init>([I)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/inet/c/a/a/b/b/a;Lcom/jscape/inet/c/a/a/b/b/a;)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [I

    iget p1, p1, Lcom/jscape/inet/c/a/a/b/b/a;->e:I

    const/4 v1, 0x0

    aput p1, v0, v1

    iget p1, p2, Lcom/jscape/inet/c/a/a/b/b/a;->e:I

    const/4 p2, 0x1

    aput p1, v0, p2

    invoke-direct {p0, v0}, Lcom/jscape/inet/c/a/a/b/b/f;-><init>([I)V

    return-void
.end method

.method public varargs constructor <init>([I)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/c/a/a/b/b/e;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/c/a/a/b/b/f;->a:[I

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/c/a/a/b/b/f;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/c/a/a/b/b/f;->a:[I

    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
