.class public Lcom/jscape/inet/c/a/a/b/a/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/c/a/a/b/b/e;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/b/b/e;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/c/a/a/b/a/m;->a(Ljava/io/InputStream;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/util/h/a/q;->a(Ljava/io/InputStream;)S

    move-result p1

    const v1, 0xffff

    and-int/2addr p1, v1

    new-instance v1, Lcom/jscape/inet/c/a/a/b/b/i;

    invoke-direct {v1, v0, p1}, Lcom/jscape/inet/c/a/a/b/b/i;-><init>(Ljava/net/InetAddress;I)V

    return-object v1
.end method

.method public a(Lcom/jscape/inet/c/a/a/b/b/e;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/c/a/a/b/b/i;

    iget-object v0, p1, Lcom/jscape/inet/c/a/a/b/b/i;->a:Ljava/net/InetAddress;

    invoke-static {v0, p2}, Lcom/jscape/inet/c/a/a/b/a/m;->a(Ljava/net/InetAddress;Ljava/io/OutputStream;)V

    iget p1, p1, Lcom/jscape/inet/c/a/a/b/b/i;->c:I

    int-to-short p1, p1

    invoke-static {p1, p2}, Lcom/jscape/util/h/a/q;->a(SLjava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/c/a/a/b/a/a;->a(Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/b/b/e;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/c/a/a/b/b/e;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/c/a/a/b/a/a;->a(Lcom/jscape/inet/c/a/a/b/b/e;Ljava/io/OutputStream;)V

    return-void
.end method
