.class public Lcom/jscape/inet/c/a/a/b/b/l;
.super Lcom/jscape/inet/c/a/a/b/b/e;


# static fields
.field private static final e:[Ljava/lang/String;


# instance fields
.field public final a:I

.field public final c:Ljava/net/InetAddress;

.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x18

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x3a

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "ppf\u0013#\u0011mazx\u000e-\u0011zV?p\r6\u001e}Fl6\u000c\u001f?i\u00117\u0011mcpy\n\u007f\u000f\u001f?i\u00117\u0011mr{o\u000c\'\u000cz\u000e"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x35

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/c/a/a/b/b/l;->e:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v13, 0x2

    if-eq v12, v13, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x33

    goto :goto_2

    :cond_2
    const/16 v12, 0x45

    goto :goto_2

    :cond_3
    const/16 v12, 0x78

    goto :goto_2

    :cond_4
    const/16 v12, 0x44

    goto :goto_2

    :cond_5
    const/16 v12, 0x31

    goto :goto_2

    :cond_6
    const/16 v12, 0x25

    goto :goto_2

    :cond_7
    const/16 v12, 0x9

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(ILjava/net/InetAddress;I)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/c/a/a/b/b/e;-><init>()V

    iput p1, p0, Lcom/jscape/inet/c/a/a/b/b/l;->a:I

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/c/a/a/b/b/l;->c:Ljava/net/InetAddress;

    iput p3, p0, Lcom/jscape/inet/c/a/a/b/b/l;->d:I

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/jscape/inet/c/a/a/b/b/e;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/c/a/a/b/b/l;->e:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/jscape/inet/c/a/a/b/b/l;->a:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    aget-object v4, v2, v3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/c/a/a/b/b/l;->c:Ljava/net/InetAddress;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v4, 0x1

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/c/a/a/b/b/l;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_0

    new-array v0, v3, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object v1
.end method
