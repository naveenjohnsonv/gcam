.class public Lcom/jscape/inet/c/a/a/b/a/p;
.super Ljava/lang/Object;


# static fields
.field public static final a:[Lcom/jscape/inet/c/a/a/b/a/k;

.field public static final b:[Lcom/jscape/inet/c/a/a/b/a/o;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v0, 0x3

    new-array v1, v0, [Lcom/jscape/inet/c/a/a/b/a/k;

    new-instance v2, Lcom/jscape/inet/c/a/a/b/a/k;

    const-class v3, Lcom/jscape/inet/c/a/a/b/b/k;

    new-instance v4, Lcom/jscape/inet/c/a/a/b/a/l;

    invoke-direct {v4}, Lcom/jscape/inet/c/a/a/b/a/l;-><init>()V

    const/4 v5, 0x1

    invoke-direct {v2, v5, v3, v4}, Lcom/jscape/inet/c/a/a/b/a/k;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lcom/jscape/inet/c/a/a/b/a/k;

    const-class v4, Lcom/jscape/inet/c/a/a/b/b/j;

    new-instance v6, Lcom/jscape/inet/c/a/a/b/a/j;

    invoke-direct {v6}, Lcom/jscape/inet/c/a/a/b/a/j;-><init>()V

    const/4 v7, 0x2

    invoke-direct {v2, v7, v4, v6}, Lcom/jscape/inet/c/a/a/b/a/k;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    aput-object v2, v1, v5

    new-instance v2, Lcom/jscape/inet/c/a/a/b/a/k;

    const-class v4, Lcom/jscape/inet/c/a/a/b/b/i;

    new-instance v6, Lcom/jscape/inet/c/a/a/b/a/a;

    invoke-direct {v6}, Lcom/jscape/inet/c/a/a/b/a/a;-><init>()V

    invoke-direct {v2, v0, v4, v6}, Lcom/jscape/inet/c/a/a/b/a/k;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    aput-object v2, v1, v7

    sput-object v1, Lcom/jscape/inet/c/a/a/b/a/p;->a:[Lcom/jscape/inet/c/a/a/b/a/k;

    const/4 v1, 0x6

    new-array v1, v1, [Lcom/jscape/inet/c/a/a/b/a/o;

    new-instance v2, Lcom/jscape/inet/c/a/a/b/a/o;

    const-class v4, Lcom/jscape/inet/c/a/a/b/b/f;

    new-instance v6, Lcom/jscape/inet/c/a/a/b/a/f;

    invoke-direct {v6}, Lcom/jscape/inet/c/a/a/b/a/f;-><init>()V

    new-array v8, v5, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/c/a/a/b/b/f;

    aput-object v9, v8, v3

    invoke-direct {v2, v4, v6, v8}, Lcom/jscape/inet/c/a/a/b/a/o;-><init>(Ljava/lang/Class;Lcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v3

    new-instance v2, Lcom/jscape/inet/c/a/a/b/a/o;

    const-class v4, Lcom/jscape/inet/c/a/a/b/b/g;

    new-instance v6, Lcom/jscape/inet/c/a/a/b/a/g;

    invoke-direct {v6}, Lcom/jscape/inet/c/a/a/b/a/g;-><init>()V

    new-array v8, v5, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/c/a/a/b/b/g;

    aput-object v9, v8, v3

    invoke-direct {v2, v4, v6, v8}, Lcom/jscape/inet/c/a/a/b/a/o;-><init>(Ljava/lang/Class;Lcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v5

    new-instance v2, Lcom/jscape/inet/c/a/a/b/a/o;

    const-class v4, Lcom/jscape/inet/c/a/a/b/b/m;

    new-instance v6, Lcom/jscape/inet/c/a/a/b/a/c;

    invoke-direct {v6}, Lcom/jscape/inet/c/a/a/b/a/c;-><init>()V

    new-array v8, v5, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/c/a/a/b/b/m;

    aput-object v9, v8, v3

    invoke-direct {v2, v4, v6, v8}, Lcom/jscape/inet/c/a/a/b/a/o;-><init>(Ljava/lang/Class;Lcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/jscape/inet/c/a/a/b/a/o;

    const-class v4, Lcom/jscape/inet/c/a/a/b/b/n;

    new-instance v6, Lcom/jscape/inet/c/a/a/b/a/d;

    invoke-direct {v6}, Lcom/jscape/inet/c/a/a/b/a/d;-><init>()V

    new-array v8, v5, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/c/a/a/b/b/n;

    aput-object v9, v8, v3

    invoke-direct {v2, v4, v6, v8}, Lcom/jscape/inet/c/a/a/b/a/o;-><init>(Ljava/lang/Class;Lcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v0

    new-instance v2, Lcom/jscape/inet/c/a/a/b/a/o;

    const-class v4, Lcom/jscape/inet/c/a/a/b/b/h;

    new-instance v6, Lcom/jscape/inet/c/a/a/b/a/h;

    sget-object v8, Lcom/jscape/inet/c/a/a/b/a/p;->a:[Lcom/jscape/inet/c/a/a/b/a/k;

    invoke-direct {v6, v8}, Lcom/jscape/inet/c/a/a/b/a/h;-><init>([Lcom/jscape/inet/c/a/a/b/a/k;)V

    new-array v0, v0, [Ljava/lang/Class;

    const-class v8, Lcom/jscape/inet/c/a/a/b/b/k;

    aput-object v8, v0, v3

    const-class v8, Lcom/jscape/inet/c/a/a/b/b/j;

    aput-object v8, v0, v5

    const-class v8, Lcom/jscape/inet/c/a/a/b/b/i;

    aput-object v8, v0, v7

    invoke-direct {v2, v4, v6, v0}, Lcom/jscape/inet/c/a/a/b/a/o;-><init>(Ljava/lang/Class;Lcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/4 v0, 0x4

    aput-object v2, v1, v0

    new-instance v0, Lcom/jscape/inet/c/a/a/b/a/o;

    const-class v2, Lcom/jscape/inet/c/a/a/b/b/l;

    new-instance v4, Lcom/jscape/inet/c/a/a/b/a/i;

    invoke-direct {v4}, Lcom/jscape/inet/c/a/a/b/a/i;-><init>()V

    new-array v5, v5, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/c/a/a/b/b/l;

    aput-object v6, v5, v3

    invoke-direct {v0, v2, v4, v5}, Lcom/jscape/inet/c/a/a/b/a/o;-><init>(Ljava/lang/Class;Lcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/4 v2, 0x5

    aput-object v0, v1, v2

    sput-object v1, Lcom/jscape/inet/c/a/a/b/a/p;->b:[Lcom/jscape/inet/c/a/a/b/a/o;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/jscape/inet/c/a/a/b/a/n;)Lcom/jscape/inet/c/a/a/b/a/n;
    .locals 1

    sget-object v0, Lcom/jscape/inet/c/a/a/b/a/p;->b:[Lcom/jscape/inet/c/a/a/b/a/o;

    invoke-virtual {p0, v0}, Lcom/jscape/inet/c/a/a/b/a/n;->a([Lcom/jscape/inet/c/a/a/b/a/o;)Lcom/jscape/inet/c/a/a/b/a/n;

    move-result-object p0

    return-object p0
.end method
