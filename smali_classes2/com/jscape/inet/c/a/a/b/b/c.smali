.class public final enum Lcom/jscape/inet/c/a/a/b/b/c;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/c/a/a/b/b/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/jscape/inet/c/a/a/b/b/c;

.field public static final enum b:Lcom/jscape/inet/c/a/a/b/b/c;

.field public static final enum c:Lcom/jscape/inet/c/a/a/b/b/c;

.field private static final synthetic e:[Lcom/jscape/inet/c/a/a/b/b/c;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x4

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x5b

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "65QR\r!8OIq\u0018r;?VWd\u000e\u000773QXu\u0008u"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    const/4 v12, 0x2

    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x1a

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/jscape/inet/c/a/a/b/b/c;

    aget-object v4, v1, v12

    invoke-direct {v2, v4, v3, v8}, Lcom/jscape/inet/c/a/a/b/b/c;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/c/a/a/b/b/c;->a:Lcom/jscape/inet/c/a/a/b/b/c;

    new-instance v2, Lcom/jscape/inet/c/a/a/b/b/c;

    aget-object v4, v1, v3

    invoke-direct {v2, v4, v8, v12}, Lcom/jscape/inet/c/a/a/b/b/c;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/c/a/a/b/b/c;->b:Lcom/jscape/inet/c/a/a/b/b/c;

    new-instance v2, Lcom/jscape/inet/c/a/a/b/b/c;

    aget-object v1, v1, v8

    invoke-direct {v2, v1, v12, v0}, Lcom/jscape/inet/c/a/a/b/b/c;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/c/a/a/b/b/c;->c:Lcom/jscape/inet/c/a/a/b/b/c;

    new-array v0, v0, [Lcom/jscape/inet/c/a/a/b/b/c;

    sget-object v1, Lcom/jscape/inet/c/a/a/b/b/c;->a:Lcom/jscape/inet/c/a/a/b/b/c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/jscape/inet/c/a/a/b/b/c;->b:Lcom/jscape/inet/c/a/a/b/b/c;

    aput-object v1, v0, v8

    aput-object v2, v0, v12

    sput-object v0, Lcom/jscape/inet/c/a/a/b/b/c;->e:[Lcom/jscape/inet/c/a/a/b/b/c;

    return-void

    :cond_1
    aget-char v13, v4, v11

    rem-int/lit8 v14, v11, 0x7

    if-eqz v14, :cond_7

    if-eq v14, v8, :cond_6

    if-eq v14, v12, :cond_5

    if-eq v14, v0, :cond_4

    if-eq v14, v2, :cond_3

    const/4 v12, 0x5

    if-eq v14, v12, :cond_2

    const/16 v12, 0x7a

    goto :goto_2

    :cond_2
    const/16 v12, 0x10

    goto :goto_2

    :cond_3
    const/16 v12, 0x6b

    goto :goto_2

    :cond_4
    const/16 v12, 0x4d

    goto :goto_2

    :cond_5
    const/16 v12, 0x44

    goto :goto_2

    :cond_6
    const/16 v12, 0x27

    goto :goto_2

    :cond_7
    const/16 v12, 0x2f

    :goto_2
    xor-int/2addr v12, v7

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/c/a/a/b/b/c;->d:I

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/inet/c/a/a/b/b/c;
    .locals 1

    const-class v0, Lcom/jscape/inet/c/a/a/b/b/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/c/a/a/b/b/c;

    return-object p0
.end method

.method public static a()[Lcom/jscape/inet/c/a/a/b/b/c;
    .locals 1

    sget-object v0, Lcom/jscape/inet/c/a/a/b/b/c;->e:[Lcom/jscape/inet/c/a/a/b/b/c;

    invoke-virtual {v0}, [Lcom/jscape/inet/c/a/a/b/b/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/c/a/a/b/b/c;

    return-object v0
.end method
