.class public Lcom/jscape/inet/c/a/a/b/a/m;
.super Ljava/lang/Object;


# static fields
.field private static final a:I = 0x1

.field private static final b:I = 0x3

.field private static final c:I = 0x4

.field private static final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "|ht\u000bIn9[rb\u001a\u0019\u007f2Mtb\rJ>5FbbD\u0019"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/c/a/a/b/a/m;->d:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x78

    goto :goto_1

    :cond_1
    const/16 v4, 0x30

    goto :goto_1

    :cond_2
    const/16 v4, 0x17

    goto :goto_1

    :cond_3
    const/16 v4, 0x50

    goto :goto_1

    :cond_4
    const/16 v4, 0x29

    goto :goto_1

    :cond_5
    const/16 v4, 0x28

    goto :goto_1

    :cond_6
    const/4 v4, 0x7

    :goto_1
    const/16 v5, 0x2e

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method public static a(Ljava/io/InputStream;)Ljava/net/InetAddress;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/c/a/a/b/a/e;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    invoke-static {p0}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    const/4 v2, 0x4

    if-nez v0, :cond_3

    const/4 v0, 0x1

    if-eq v1, v0, :cond_2

    const/4 v0, 0x3

    if-eq v1, v0, :cond_1

    if-ne v1, v2, :cond_0

    const/16 v0, 0x10

    new-array v0, v0, [B

    invoke-static {v0, p0}, Lcom/jscape/util/X;->a([BLjava/io/InputStream;)[B

    move-result-object p0

    invoke-static {p0}, Ljava/net/Inet6Address;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object p0

    return-object p0

    :cond_0
    new-instance p0, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/c/a/a/b/a/m;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, p0}, Lcom/jscape/util/h/a/d;->a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object p0

    return-object p0

    :cond_2
    move v1, v2

    :cond_3
    new-array v0, v1, [B

    invoke-static {v0, p0}, Lcom/jscape/util/X;->a([BLjava/io/InputStream;)[B

    move-result-object p0

    invoke-static {p0}, Ljava/net/Inet4Address;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/net/InetAddress;Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/c/a/a/b/a/e;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    :try_start_0
    instance-of v1, p0, Ljava/net/Inet4Address;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    move-object v1, p0

    check-cast v1, Ljava/net/Inet4Address;

    const/4 v2, 0x1

    :try_start_1
    invoke-static {v2, p1}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    invoke-virtual {v1}, Ljava/net/Inet4Address;->getAddress()[B

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v0, :cond_3

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_2
    invoke-static {p0}, Lcom/jscape/inet/c/a/a/b/a/m;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_0
    :goto_0
    if-nez v0, :cond_2

    instance-of v1, p0, Ljava/net/Inet6Address;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/c/a/a/b/a/m;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_1
    if-eqz v1, :cond_3

    :cond_2
    check-cast p0, Ljava/net/Inet6Address;

    const/4 v0, 0x4

    invoke-static {v0, p1}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    invoke-virtual {p0}, Ljava/net/Inet6Address;->getAddress()[B

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/io/OutputStream;->write([B)V

    :cond_3
    return-void

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/c/a/a/b/a/m;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method
