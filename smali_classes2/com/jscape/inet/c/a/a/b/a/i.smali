.class public Lcom/jscape/inet/c/a/a/b/a/i;
.super Lcom/jscape/inet/c/a/a/b/a/e;


# static fields
.field private static final c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/c/a/a/b/a/e;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/b/b/e;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v0}, Lcom/jscape/inet/c/a/a/b/a/i;->a(I)V

    invoke-static {p1}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-static {p1}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    invoke-static {}, Lcom/jscape/inet/c/a/a/b/a/e;->b()[Lcom/jscape/util/aq;

    move-result-object v1

    invoke-static {p1}, Lcom/jscape/inet/c/a/a/b/a/m;->a(Ljava/io/InputStream;)Ljava/net/InetAddress;

    move-result-object v2

    invoke-static {p1}, Lcom/jscape/util/h/a/q;->a(Ljava/io/InputStream;)S

    move-result p1

    const v3, 0xffff

    and-int/2addr p1, v3

    new-instance v3, Lcom/jscape/inet/c/a/a/b/b/l;

    invoke-direct {v3, v0, v2, p1}, Lcom/jscape/inet/c/a/a/b/b/l;-><init>(ILjava/net/InetAddress;I)V

    if-eqz v1, :cond_0

    const/4 p1, 0x5

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object v3
.end method

.method public a(Lcom/jscape/inet/c/a/a/b/b/e;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/c/a/a/b/b/l;

    const/4 v0, 0x5

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    iget v0, p1, Lcom/jscape/inet/c/a/a/b/b/l;->a:I

    int-to-byte v0, v0

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    const/4 v0, 0x0

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    iget-object v0, p1, Lcom/jscape/inet/c/a/a/b/b/l;->c:Ljava/net/InetAddress;

    invoke-static {v0, p2}, Lcom/jscape/inet/c/a/a/b/a/m;->a(Ljava/net/InetAddress;Ljava/io/OutputStream;)V

    iget p1, p1, Lcom/jscape/inet/c/a/a/b/b/l;->d:I

    int-to-short p1, p1

    invoke-static {p1, p2}, Lcom/jscape/util/h/a/q;->a(SLjava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/c/a/a/b/a/i;->a(Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/b/b/e;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/c/a/a/b/b/e;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/c/a/a/b/a/i;->a(Lcom/jscape/inet/c/a/a/b/b/e;Ljava/io/OutputStream;)V

    return-void
.end method
