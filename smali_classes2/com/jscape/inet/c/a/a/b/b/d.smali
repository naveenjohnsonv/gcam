.class public final enum Lcom/jscape/inet/c/a/a/b/b/d;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/c/a/a/b/b/d;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/jscape/inet/c/a/a/b/b/d;

.field private static final synthetic c:[Lcom/jscape/inet/c/a/a/b/b/d;


# instance fields
.field public final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const-string v0, "\r\u0011lbAH\n"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    const/4 v4, 0x1

    if-gt v1, v3, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/c/a/a/b/b/d;

    invoke-direct {v1, v0, v2, v2}, Lcom/jscape/inet/c/a/a/b/b/d;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/jscape/inet/c/a/a/b/b/d;->a:Lcom/jscape/inet/c/a/a/b/b/d;

    new-array v0, v4, [Lcom/jscape/inet/c/a/a/b/b/d;

    aput-object v1, v0, v2

    sput-object v0, Lcom/jscape/inet/c/a/a/b/b/d;->c:[Lcom/jscape/inet/c/a/a/b/b/d;

    return-void

    :cond_0
    aget-char v5, v0, v3

    rem-int/lit8 v6, v3, 0x7

    const/4 v7, 0x2

    if-eqz v6, :cond_6

    if-eq v6, v4, :cond_5

    if-eq v6, v7, :cond_4

    const/4 v4, 0x3

    if-eq v6, v4, :cond_3

    const/4 v4, 0x4

    if-eq v6, v4, :cond_2

    const/4 v4, 0x5

    if-eq v6, v4, :cond_1

    const/16 v4, 0x5b

    goto :goto_1

    :cond_1
    const/16 v4, 0x19

    goto :goto_1

    :cond_2
    const/4 v4, 0x6

    goto :goto_1

    :cond_3
    const/16 v4, 0x23

    goto :goto_1

    :cond_4
    const/16 v4, 0x2d

    goto :goto_1

    :cond_5
    const/16 v4, 0x46

    goto :goto_1

    :cond_6
    const/16 v4, 0x5c

    :goto_1
    xor-int/2addr v4, v7

    xor-int/2addr v4, v5

    int-to-char v4, v4

    aput-char v4, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/c/a/a/b/b/d;->b:I

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/inet/c/a/a/b/b/d;
    .locals 1

    const-class v0, Lcom/jscape/inet/c/a/a/b/b/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/c/a/a/b/b/d;

    return-object p0
.end method

.method public static a()[Lcom/jscape/inet/c/a/a/b/b/d;
    .locals 1

    sget-object v0, Lcom/jscape/inet/c/a/a/b/b/d;->c:[Lcom/jscape/inet/c/a/a/b/b/d;

    invoke-virtual {v0}, [Lcom/jscape/inet/c/a/a/b/b/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/c/a/a/b/b/d;

    return-object v0
.end method
