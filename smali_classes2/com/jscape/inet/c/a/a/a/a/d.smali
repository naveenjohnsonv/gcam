.class public Lcom/jscape/inet/c/a/a/a/a/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/c/a/a/a/b/b;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:I

.field private static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "O<D@\u0004)\th&RQT4\u0003i!VR\u0011y\u0010\u007f D\\\u001b7\\:"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/c/a/a/a/a/d;->b:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x44

    goto :goto_1

    :cond_1
    const/16 v4, 0x7b

    goto :goto_1

    :cond_2
    const/16 v4, 0x56

    goto :goto_1

    :cond_3
    const/16 v4, 0x17

    goto :goto_1

    :cond_4
    const/16 v4, 0x15

    goto :goto_1

    :cond_5
    const/16 v4, 0x70

    goto :goto_1

    :cond_6
    const/16 v4, 0x38

    :goto_1
    const/16 v5, 0x22

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a(I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/c/a/a/a/a/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/c/a/a/a/a/d;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/a/b/b;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-static {}, Lcom/jscape/inet/c/a/a/a/a/h;->c()Z

    move-result v1

    invoke-direct {p0, v0}, Lcom/jscape/inet/c/a/a/a/a/d;->a(I)V

    invoke-static {p1}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-static {p1}, Lcom/jscape/util/h/a/q;->a(Ljava/io/InputStream;)S

    move-result v2

    const v3, 0xffff

    and-int/2addr v2, v3

    invoke-static {p1}, Lcom/jscape/inet/c/a/a/a/a/f;->a(Ljava/io/InputStream;)Ljava/net/Inet4Address;

    move-result-object p1

    new-instance v3, Lcom/jscape/inet/c/a/a/a/b/f;

    invoke-direct {v3, v0, v2, p1}, Lcom/jscape/inet/c/a/a/a/b/f;-><init>(IILjava/net/Inet4Address;)V

    if-eqz v1, :cond_0

    const/4 p1, 0x4

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object v3
.end method

.method public a(Lcom/jscape/inet/c/a/a/a/b/b;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/c/a/a/a/b/f;

    const/4 v0, 0x0

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    iget v0, p1, Lcom/jscape/inet/c/a/a/a/b/f;->a:I

    int-to-byte v0, v0

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    iget v0, p1, Lcom/jscape/inet/c/a/a/a/b/f;->c:I

    int-to-short v0, v0

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/q;->a(SLjava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/c/a/a/a/b/f;->d:Ljava/net/Inet4Address;

    invoke-static {p1, p2}, Lcom/jscape/inet/c/a/a/a/a/f;->a(Ljava/net/Inet4Address;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/c/a/a/a/a/d;->a(Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/a/b/b;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/c/a/a/a/b/b;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/c/a/a/a/a/d;->a(Lcom/jscape/inet/c/a/a/a/b/b;Ljava/io/OutputStream;)V

    return-void
.end method
