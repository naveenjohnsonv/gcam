.class public Lcom/jscape/inet/c/a/a/a/b/f;
.super Lcom/jscape/inet/c/a/a/a/b/b;


# static fields
.field private static final e:[Ljava/lang/String;


# instance fields
.field public final a:I

.field public final c:I

.field public final d:Ljava/net/Inet4Address;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x7

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v3

    :goto_0
    const/16 v6, 0x24

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v2, v4

    const-string v8, "\'m}\u0011Lp9\u0018H\"`\u0013_j`Y(~\u000eQjwnmv\u000c[wqg90\u0005\'md\u000e\u0003"

    invoke-virtual {v8, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v3

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x26

    if-ge v2, v4, :cond_0

    invoke-virtual {v8, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v2

    move v2, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/c/a/a/a/b/f;->e:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/16 v13, 0x20

    if-eqz v12, :cond_6

    if-eq v12, v7, :cond_5

    const/4 v14, 0x2

    if-eq v12, v14, :cond_4

    if-eq v12, v0, :cond_3

    const/4 v14, 0x4

    if-eq v12, v14, :cond_2

    const/4 v14, 0x5

    goto :goto_2

    :cond_2
    const/16 v13, 0x1a

    goto :goto_2

    :cond_3
    const/16 v13, 0x5a

    goto :goto_2

    :cond_4
    const/16 v13, 0x29

    goto :goto_2

    :cond_5
    const/16 v13, 0x69

    goto :goto_2

    :cond_6
    const/16 v13, 0x2f

    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(IILjava/net/Inet4Address;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/c/a/a/a/b/b;-><init>()V

    iput p1, p0, Lcom/jscape/inet/c/a/a/a/b/f;->a:I

    iput p2, p0, Lcom/jscape/inet/c/a/a/a/b/f;->c:I

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/c/a/a/a/b/f;->d:Ljava/net/Inet4Address;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/c/a/a/a/b/f;->e:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/c/a/a/a/b/f;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/c/a/a/a/b/f;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/c/a/a/a/b/f;->d:Ljava/net/Inet4Address;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
