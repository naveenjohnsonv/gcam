.class public Lcom/jscape/inet/c/a/a/a/b/d;
.super Lcom/jscape/inet/c/a/a/a/b/c;


# static fields
.field private static final e:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x5

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x72

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "\u001dea^\u0011\n\u001de}]I\u0016\u0002Ux/\u0012s,fJ~\u0001:D {Z\u000c\u001f;^7|\u0013"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x23

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/c/a/a/a/b/d;->e:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    if-eqz v13, :cond_7

    if-eq v13, v8, :cond_6

    const/4 v14, 0x2

    if-eq v13, v14, :cond_5

    if-eq v13, v0, :cond_4

    const/4 v14, 0x4

    if-eq v13, v14, :cond_3

    if-eq v13, v2, :cond_2

    const/16 v13, 0x39

    goto :goto_2

    :cond_2
    const/16 v13, 0x16

    goto :goto_2

    :cond_3
    const/16 v13, 0x5e

    goto :goto_2

    :cond_4
    const/16 v13, 0x5c

    goto :goto_2

    :cond_5
    const/16 v13, 0x7a

    goto :goto_2

    :cond_6
    const/16 v13, 0x37

    goto :goto_2

    :cond_7
    const/16 v13, 0x43

    :goto_2
    xor-int/2addr v13, v7

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method public constructor <init>(ILjava/net/Inet4Address;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/inet/c/a/a/a/b/c;-><init>(ILjava/net/Inet4Address;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/jscape/inet/c/a/a/a/b/b;->b()[Lcom/jscape/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/c/a/a/a/b/d;->e:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/c/a/a/a/b/d;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/c/a/a/a/b/d;->c:Ljava/net/Inet4Address;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/c/a/a/a/b/d;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/jscape/util/aq;

    invoke-static {v1}, Lcom/jscape/inet/c/a/a/a/b/b;->b([Lcom/jscape/util/aq;)V

    :cond_0
    return-object v0
.end method
