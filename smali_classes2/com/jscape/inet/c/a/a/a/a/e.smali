.class public Lcom/jscape/inet/c/a/a/a/a/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/c/a/a/a/b/b;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/a/b/b;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/q;->a(Ljava/io/InputStream;)S

    move-result v0

    const v1, 0xffff

    and-int/2addr v0, v1

    invoke-static {p1}, Lcom/jscape/inet/c/a/a/a/a/f;->a(Ljava/io/InputStream;)Ljava/net/Inet4Address;

    move-result-object v1

    sget-object v2, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-static {v2, p1}, Lcom/jscape/inet/c/a/a/a/a/j;->a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    new-instance v2, Lcom/jscape/inet/c/a/a/a/b/e;

    invoke-direct {v2, v0, v1, p1}, Lcom/jscape/inet/c/a/a/a/b/e;-><init>(ILjava/net/Inet4Address;Ljava/lang/String;)V

    return-object v2
.end method

.method public a(Lcom/jscape/inet/c/a/a/a/b/b;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/c/a/a/a/b/e;

    iget v0, p1, Lcom/jscape/inet/c/a/a/a/b/e;->a:I

    int-to-short v0, v0

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/q;->a(SLjava/io/OutputStream;)V

    iget-object v0, p1, Lcom/jscape/inet/c/a/a/a/b/e;->c:Ljava/net/Inet4Address;

    invoke-static {v0, p2}, Lcom/jscape/inet/c/a/a/a/a/f;->a(Ljava/net/Inet4Address;Ljava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/c/a/a/a/b/e;->d:Ljava/lang/String;

    sget-object v0, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-static {p1, v0, p2}, Lcom/jscape/inet/c/a/a/a/a/j;->a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/c/a/a/a/a/e;->a(Ljava/io/InputStream;)Lcom/jscape/inet/c/a/a/a/b/b;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/c/a/a/a/b/b;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/c/a/a/a/a/e;->a(Lcom/jscape/inet/c/a/a/a/b/b;Ljava/io/OutputStream;)V

    return-void
.end method
