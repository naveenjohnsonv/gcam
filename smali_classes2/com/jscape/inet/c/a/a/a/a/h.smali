.class public Lcom/jscape/inet/c/a/a/a/a/h;
.super Ljava/lang/Object;


# static fields
.field private static d:Z

.field private static final e:[Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/Class;

.field public final b:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/c/a/a/a/b/b;",
            ">;"
        }
    .end annotation
.end field

.field public final c:[Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v2}, Lcom/jscape/inet/c/a/a/a/a/h;->b(Z)V

    const/4 v3, 0x0

    const/16 v4, 0x18

    const/4 v5, -0x1

    move v6, v3

    :goto_0
    add-int/2addr v5, v2

    add-int/2addr v4, v5

    const-string v7, "\u0011\u0014jF:\u001deSSPQ \u001amZQ^X2\u001a\u007fXG \u0008\u0011\u0014~[7\u000co\u0000\u001bxZiF*IwOQ|P:\u0007kpQnG2\u000ei~X|G T"

    invoke-virtual {v7, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    array-length v8, v5

    move v9, v3

    :goto_1
    if-gt v8, v9, :cond_1

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v5}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v8, v6, 0x1

    aput-object v5, v1, v6

    const/16 v5, 0x3d

    if-ge v4, v5, :cond_0

    invoke-virtual {v7, v4}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v6, v8

    move v14, v5

    move v5, v4

    move v4, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/c/a/a/a/a/h;->e:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v10, v5, v9

    rem-int/lit8 v11, v9, 0x7

    const/16 v12, 0x33

    if-eqz v11, :cond_5

    if-eq v11, v2, :cond_6

    const/4 v13, 0x2

    if-eq v11, v13, :cond_4

    if-eq v11, v0, :cond_6

    const/4 v12, 0x4

    if-eq v11, v12, :cond_3

    const/4 v12, 0x5

    if-eq v11, v12, :cond_2

    const/16 v12, 0xb

    goto :goto_2

    :cond_2
    const/16 v12, 0x6e

    goto :goto_2

    :cond_3
    const/16 v12, 0x54

    goto :goto_2

    :cond_4
    const/16 v12, 0x1a

    goto :goto_2

    :cond_5
    const/16 v12, 0x3a

    :cond_6
    :goto_2
    const/4 v11, 0x7

    xor-int/2addr v11, v12

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v5, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public varargs constructor <init>(Ljava/lang/Class;Lcom/jscape/util/h/I;[Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/c/a/a/a/b/b;",
            ">;[",
            "Ljava/lang/Class;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/c/a/a/a/a/h;->a:Ljava/lang/Class;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/c/a/a/a/a/h;->b:Lcom/jscape/util/h/I;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/c/a/a/a/a/h;->c:[Ljava/lang/Class;

    return-void
.end method

.method public static b(Z)V
    .locals 0

    sput-boolean p0, Lcom/jscape/inet/c/a/a/a/a/h;->d:Z

    return-void
.end method

.method public static b()Z
    .locals 1

    sget-boolean v0, Lcom/jscape/inet/c/a/a/a/a/h;->d:Z

    return v0
.end method

.method public static c()Z
    .locals 1

    invoke-static {}, Lcom/jscape/inet/c/a/a/a/a/h;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/jscape/inet/c/a/a/a/a/h;->b()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/c/a/a/a/a/h;->e:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/c/a/a/a/a/h;->a:Ljava/lang/Class;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v4, v2, v3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/c/a/a/a/a/h;->b:Lcom/jscape/util/h/I;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/c/a/a/a/a/h;->c:[Ljava/lang/Class;

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v2

    if-nez v2, :cond_0

    xor-int/2addr v0, v3

    invoke-static {v0}, Lcom/jscape/inet/c/a/a/a/a/h;->b(Z)V

    :cond_0
    return-object v1
.end method
