.class public Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;
.super Lcom/jscape/inet/ssh/protocol/messages/Message;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/ssh/protocol/messages/Message<",
        "Lcom/jscape/inet/ssh/protocol/messages/IdentificationString$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final banner:Ljava/lang/String;

.field public final comments:Ljava/lang/String;

.field public final protocolVersion:Ljava/lang/String;

.field public rawData:[B

.field public final softwareVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "\u001c5sYyviu+wWapn:7%Npm~<4k\u0016\u0003gu5\u0013y{uJzkb64inpm~<4k\u00052\u000f\u001c5sYyviu9dV{z\u007f{\u001e\u001c?`Vavk<8dL|pc\u0006/wQ{x-.9dV{z\u007fh|\u0003gu5\u0005X\u0008Vp8\u0019\u001c5sYyviu(j^ahl\'>%Npm~<4k\u0016\u0005_\u0008Vp8\u0013y{vWskz4)`npm~<4k\u00052"

    const/16 v4, 0x9e

    const/16 v5, 0x19

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x5f

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    const/4 v14, 0x4

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x11

    const-string v3, "-#f>\u000cRPM|SYC\u0010\u0004].\u0019"

    move v7, v10

    move v5, v14

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x74

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v10, v13

    rem-int/lit8 v1, v13, 0x7

    if-eqz v1, :cond_8

    if-eq v1, v9, :cond_9

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    if-eq v1, v14, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v14, 0x52

    goto :goto_4

    :cond_4
    const/16 v14, 0x40

    goto :goto_4

    :cond_5
    const/16 v14, 0x4a

    goto :goto_4

    :cond_6
    const/16 v14, 0x67

    goto :goto_4

    :cond_7
    const/16 v14, 0x5a

    goto :goto_4

    :cond_8
    const/16 v14, 0xa

    :cond_9
    :goto_4
    xor-int v1, v8, v14

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/messages/Message;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/messages/Message;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    if-eqz p1, :cond_3

    :cond_0
    sget-object v3, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->a:[Ljava/lang/String;

    const/16 v4, 0xa

    aget-object v4, v3, v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v0, :cond_1

    if-nez v4, :cond_4

    const/4 v4, 0x6

    aget-object v3, v3, v4

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    :cond_1
    if-eqz v0, :cond_2

    if-nez v4, :cond_4

    sget-object v3, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->a:[Ljava/lang/String;

    const/16 v4, 0x8

    aget-object v3, v3, v4

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    :cond_2
    if-eqz v0, :cond_5

    if-nez v4, :cond_4

    :cond_3
    move v4, v2

    goto :goto_0

    :cond_4
    move v4, v1

    :cond_5
    :goto_0
    sget-object v3, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->a:[Ljava/lang/String;

    const/4 v5, 0x3

    aget-object v3, v3, v5

    invoke-static {v4, v3}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->banner:Ljava/lang/String;

    const-string p1, " "

    invoke-virtual {p2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v0, :cond_8

    if-nez v3, :cond_7

    const-string v3, "-"

    invoke-virtual {p2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v0, :cond_8

    if-eqz v3, :cond_6

    goto :goto_1

    :cond_6
    move v3, v1

    goto :goto_2

    :cond_7
    :goto_1
    move v3, v2

    :cond_8
    :goto_2
    sget-object v4, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->a:[Ljava/lang/String;

    aget-object v1, v4, v1

    invoke-static {v3, v1}, Lcom/jscape/util/aq;->b(ZLjava/lang/String;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->protocolVersion:Ljava/lang/String;

    invoke-virtual {p3, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    const/4 p2, 0x7

    aget-object p2, v4, p2

    invoke-static {p1, p2}, Lcom/jscape/util/aq;->b(ZLjava/lang/String;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->softwareVersion:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->comments:Ljava/lang/String;

    if-nez v0, :cond_9

    new-array p1, v2, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_9
    return-void
.end method

.method public static clientVersion20(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;
    .locals 3

    new-instance v0, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1, p0, p1}, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static serverVersion20(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;
    .locals 3

    new-instance v0, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->a:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public accept(Lcom/jscape/inet/ssh/protocol/messages/IdentificationString$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/messages/IdentificationString$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString$Handler;->handle(Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public bridge synthetic accept(Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->accept(Lcom/jscape/inet/ssh/protocol/messages/IdentificationString$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public handlerClass()Ljava/lang/Class;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString$Handler;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/messages/Message;->b()[Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->a:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->banner:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->protocolVersion:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v3, 0x9

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->softwareVersion:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v3, 0xb

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->comments:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/messages/Message;->b([Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method
