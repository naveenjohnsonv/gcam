.class public abstract Lcom/jscape/inet/ssh/protocol/messages/Message;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<H::",
        "Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;


# instance fields
.field public number:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/messages/Message;->b()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/messages/Message;->b([Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b([Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/ssh/protocol/messages/Message;->b:[Ljava/lang/String;

    return-void
.end method

.method public static b()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/messages/Message;->b:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public abstract accept(Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TH;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract handlerClass()Ljava/lang/Class;
.end method
