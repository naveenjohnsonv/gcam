.class public Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;
.super Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;


# static fields
.field private static final a:Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;

.field private static final f:[Ljava/lang/String;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Ljava/security/KeyPair;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "E\u0016 7\"y|\u001bz#8+bx\u000eS\u00167+*>\u0007E\u0016)35d$"

    const/16 v5, 0x1d

    const/16 v6, 0x15

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0xf

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x34

    const/16 v4, 0xa

    const-string v6, "]\u000e8/:ad\u0003\u0013})![8\"=lJ\u0014W\u0019\"=jo\u0005o/:<jo\u0005G9/ fn\u001f\u000e!;\'js\u001fO7+i("

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v16, v6

    move v6, v4

    move-object/from16 v4, v16

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x17

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->f:[Ljava/lang/String;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;->defaultService()Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->a:Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v2, 0x16

    goto :goto_4

    :cond_4
    const/16 v2, 0x18

    goto :goto_4

    :cond_5
    const/16 v2, 0x43

    goto :goto_4

    :cond_6
    const/16 v2, 0x59

    goto :goto_4

    :cond_7
    const/16 v2, 0x4d

    goto :goto_4

    :cond_8
    const/16 v2, 0x39

    goto :goto_4

    :cond_9
    const/16 v2, 0x66

    :goto_4
    xor-int/2addr v2, v9

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Ljava/security/KeyPair;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;)V

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->c:Ljava/lang/String;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->d:Ljava/security/KeyPair;

    return-void
.end method

.method private static a(Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$InvalidCredentialsException;)Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$InvalidCredentialsException;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$InvalidCredentialsException;
        }
    .end annotation

    :try_start_0
    instance-of p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthPkOk;

    if-eqz p1, :cond_0

    return-void

    :cond_0
    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$InvalidCredentialsException;

    invoke-direct {p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$InvalidCredentialsException;-><init>()V

    throw p1
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$InvalidCredentialsException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->a(Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$InvalidCredentialsException;)Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$InvalidCredentialsException;

    move-result-object p1

    throw p1
.end method

.method private a(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKey;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->d:Ljava/security/KeyPair;

    invoke-virtual {v2}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v2

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifierFor(Ljava/security/Key;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->d:Ljava/security/KeyPair;

    invoke-virtual {v3}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v3

    invoke-direct {v0, v1, p2, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKey;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/PublicKey;)V

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->receive(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->a(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    return-void
.end method

.method private b(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;Ljava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->a:Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->session()Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    move-result-object v1

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->sessionId:[B

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->d:Ljava/security/KeyPair;

    invoke-virtual {v0, v1, v2, p2, v3}, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;->signatureFor([BLjava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;)Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    move-result-object v9

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;

    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->d:Ljava/security/KeyPair;

    invoke-virtual {v1}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v1

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifierFor(Ljava/security/Key;)Ljava/lang/String;

    move-result-object v7

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->d:Ljava/security/KeyPair;

    invoke-virtual {v1}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v8

    move-object v4, v0

    move-object v6, p2

    invoke-direct/range {v4 .. v9}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->receive(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->assertSuccess(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    return-void
.end method

.method public static defaultAuthentication(Ljava/lang/String;Ljava/security/KeyPair;)Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;
    .locals 2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyAuthenticationMessages;->defaultMessages()Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyAuthenticationMessages;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Ljava/security/KeyPair;)V

    return-object v0
.end method


# virtual methods
.method public applyTo(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)V

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->a(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->b(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;Ljava/lang/String;)V

    return-void
.end method

.method protected init(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)V

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->session()Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    move-result-object p1

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->messageCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyAuthenticationMessages;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->b()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->f:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->c:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x27

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v4, 0x1

    aget-object v5, v2, v4

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->d:Ljava/security/KeyPair;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v5, 0x2

    aget-object v5, v2, v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->banner:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v5, 0x0

    aget-object v2, v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->bannerLanguageTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v2

    if-nez v2, :cond_0

    add-int/2addr v0, v4

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->b(I)V

    :cond_0
    return-object v1
.end method
