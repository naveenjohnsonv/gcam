.class public Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;
.super Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "`Z#<A\u0005qG@5-\u0011\u0000mPFp _\u0013q\u0015D\"&\\\u0005j\u000f\u0014"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;->d:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0xa

    goto :goto_1

    :cond_1
    const/16 v4, 0x61

    goto :goto_1

    :cond_2
    const/16 v4, 0x25

    goto :goto_1

    :cond_3
    const/16 v4, 0x5d

    goto :goto_1

    :cond_4
    const/16 v4, 0x44

    goto :goto_1

    :cond_5
    const/16 v4, 0x20

    goto :goto_1

    :cond_6
    const/16 v4, 0x21

    :goto_1
    const/16 v5, 0x14

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;)V

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;->a:Ljava/util/List;

    return-void
.end method

.method public varargs constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;)V
    .locals 0

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p3

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->c()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;

    if-nez v0, :cond_1

    :try_start_0
    iget-object v3, v2, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;->regex:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_0

    :try_start_1
    iget-object p1, v2, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;->response:Ljava/lang/String;

    goto :goto_1

    :cond_0
    if-nez v0, :cond_2

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    return-object p1

    :cond_2
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static b(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static defaultAuthentication(Ljava/lang/String;Ljava/util/List;)Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;",
            ">;)",
            "Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;->defaultMessages()Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public static varargs defaultAuthentication(Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;)Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;
    .locals 2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;->defaultMessages()Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;)V

    return-object v0
.end method


# virtual methods
.method protected responsesFor(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/UserAuthInfoRequestPrompt;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance p1, Ljava/util/ArrayList;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p2

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->c()I

    move-result p2

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/ssh/protocol/v2/messages/UserAuthInfoRequestPrompt;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/messages/UserAuthInfoRequestPrompt;->prompt:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez p2, :cond_1

    :try_start_0
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p2, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object p1
.end method
