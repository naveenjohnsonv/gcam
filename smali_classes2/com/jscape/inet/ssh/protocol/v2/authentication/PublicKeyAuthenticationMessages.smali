.class public Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyAuthenticationMessages;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;


# static fields
.field public static final REQUEST_TYPES:[Ljava/lang/Class;

.field private static final b:[Ljava/lang/String;


# instance fields
.field private final a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/16 v2, 0x9

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x8

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "Nq2m;FP[}\u0004Pk>d"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    const/4 v12, 0x3

    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0xe

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyAuthenticationMessages;->b:[Ljava/lang/String;

    new-array v1, v12, [Ljava/lang/Class;

    const-class v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNone;

    aput-object v2, v1, v3

    const-class v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKey;

    aput-object v2, v1, v8

    const-class v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;

    aput-object v2, v1, v0

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyAuthenticationMessages;->REQUEST_TYPES:[Ljava/lang/Class;

    return-void

    :cond_1
    aget-char v13, v4, v11

    rem-int/lit8 v14, v11, 0x7

    if-eqz v14, :cond_7

    if-eq v14, v8, :cond_6

    if-eq v14, v0, :cond_5

    if-eq v14, v12, :cond_4

    const/4 v12, 0x4

    if-eq v14, v12, :cond_3

    const/4 v12, 0x5

    if-eq v14, v12, :cond_2

    const/16 v12, 0x33

    goto :goto_2

    :cond_2
    const/16 v12, 0x2d

    goto :goto_2

    :cond_3
    const/16 v12, 0x5a

    goto :goto_2

    :cond_4
    move v12, v2

    goto :goto_2

    :cond_5
    const/16 v12, 0x58

    goto :goto_2

    :cond_6
    const/16 v12, 0xc

    goto :goto_2

    :cond_7
    const/16 v12, 0x36

    :goto_2
    xor-int/2addr v12, v7

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method public varargs constructor <init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyAuthenticationMessages;->a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    return-void
.end method

.method private static a()[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;
    .locals 7

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    const/4 v1, 0x0

    new-array v2, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)V

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyAuthenticationMessages;->b()[Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;)V

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyAuthenticationMessages;->REQUEST_TYPES:[Ljava/lang/Class;

    const/16 v6, 0x32

    invoke-direct {v3, v6, v4, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v3, v2, v1

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthPkOkCodec;

    invoke-direct {v4, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthPkOkCodec;-><init>(Lcom/jscape/util/h/I;)V

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthPkOk;

    aput-object v6, v5, v1

    const/16 v1, 0x3c

    invoke-direct {v3, v1, v4, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v3, v2, v0

    return-object v2
.end method

.method private static b()[Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;
    .locals 12

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    const/4 v1, 0x0

    new-array v2, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)V

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object v0

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;

    new-array v3, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;

    invoke-direct {v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;)V

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signatures;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;

    move-result-object v2

    const/4 v3, 0x2

    new-array v4, v3, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;

    sget-object v6, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyAuthenticationMessages;->b:[Ljava/lang/String;

    const/4 v7, 0x1

    aget-object v8, v6, v7

    new-instance v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestNoneCodec;

    invoke-direct {v9}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestNoneCodec;-><init>()V

    new-array v10, v7, [Ljava/lang/Class;

    const-class v11, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNone;

    aput-object v11, v10, v1

    invoke-direct {v5, v8, v9, v10}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$MethodCodec;[Ljava/lang/Class;)V

    aput-object v5, v4, v1

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;

    aget-object v6, v6, v1

    new-instance v8, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPublicKeyCodec;

    invoke-direct {v8, v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPublicKeyCodec;-><init>(Lcom/jscape/util/h/I;Lcom/jscape/util/h/I;)V

    new-array v0, v3, [Ljava/lang/Class;

    const-class v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKey;

    aput-object v2, v0, v1

    const-class v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;

    aput-object v1, v0, v7

    invoke-direct {v5, v6, v8, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$MethodCodec;[Ljava/lang/Class;)V

    aput-object v5, v4, v7

    return-object v4
.end method

.method public static defaultMessages()Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyAuthenticationMessages;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/AuthenticationMessages;->ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyAuthenticationMessages;->a()[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/jscape/util/G;->a([[Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyAuthenticationMessages;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    new-array v2, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    invoke-direct {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyAuthenticationMessages;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)V

    return-object v1
.end method

.method public static init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyAuthenticationMessages;->a()[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AuthenticationMessages;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public update(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyAuthenticationMessages;->a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p1

    return-object p1
.end method
