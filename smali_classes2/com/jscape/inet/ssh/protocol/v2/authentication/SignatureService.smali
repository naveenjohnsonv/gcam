.class public Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;
.super Ljava/lang/Object;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "Ljava/security/PublicKey;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "yS\u0007k`\\\u0019l_"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;->c:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v5, 0x4a

    if-eqz v4, :cond_5

    const/4 v6, 0x1

    if-eq v4, v6, :cond_4

    const/4 v6, 0x2

    if-eq v4, v6, :cond_3

    const/4 v6, 0x3

    if-eq v4, v6, :cond_2

    const/4 v6, 0x4

    if-eq v4, v6, :cond_5

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v5, 0x31

    goto :goto_1

    :cond_1
    const/16 v5, 0x7c

    goto :goto_1

    :cond_2
    const/16 v5, 0x44

    goto :goto_1

    :cond_3
    const/16 v5, 0x26

    goto :goto_1

    :cond_4
    const/16 v5, 0x65

    :cond_5
    :goto_1
    const/16 v4, 0x43

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/jscape/util/h/I;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/h/I<",
            "Ljava/security/PublicKey;",
            ">;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;->a:Lcom/jscape/util/h/I;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;

    return-void
.end method

.method private a([BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/PublicKey;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/o;

    invoke-direct {v0}, Lcom/jscape/util/h/o;-><init>()V

    invoke-static {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    const/16 p1, 0x32

    invoke-virtual {v0, p1}, Lcom/jscape/util/h/o;->write(I)V

    invoke-static {p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V

    invoke-static {p3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUsAsciiValue(Ljava/lang/String;Ljava/io/OutputStream;)V

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;->c:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUsAsciiValue(Ljava/lang/String;Ljava/io/OutputStream;)V

    const/4 p1, 0x1

    invoke-static {p1, v0}, Lcom/jscape/util/h/a/a;->a(ZLjava/io/OutputStream;)V

    invoke-static {p4, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUsAsciiValue(Ljava/lang/String;Ljava/io/OutputStream;)V

    invoke-virtual {p0, p5}, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;->publicKeyBlob(Ljava/security/PublicKey;)[B

    move-result-object p1

    invoke-static {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->d()[B

    move-result-object p1

    return-object p1
.end method

.method public static defaultService()Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;->defaultService(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;

    move-result-object v0

    return-object v0
.end method

.method public static defaultService(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;
    .locals 4

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    const/4 v2, 0x0

    new-array v3, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    invoke-direct {v1, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)V

    invoke-static {v1, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object v1

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v2, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    invoke-direct {v3, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)V

    invoke-static {v3, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;->initSha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;-><init>(Lcom/jscape/util/h/I;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    return-object v0
.end method

.method public static defaultService(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;
    .locals 4

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    const/4 v2, 0x0

    new-array v3, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    invoke-direct {v1, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)V

    invoke-static {v1, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object v1

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v2, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    invoke-direct {v3, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)V

    invoke-static {v3, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;->initSha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;-><init>(Lcom/jscape/util/h/I;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    return-object v0
.end method


# virtual methods
.method public assertSignatureValid(Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/PublicKey;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;->a([BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/PublicKey;)[B

    move-result-object p2

    iget-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;

    invoke-interface {p3, p2, p6, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;->assertSignatureValid([BLjava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V

    return-void
.end method

.method protected publicKeyBlob(Ljava/security/PublicKey;)[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/o;

    invoke-direct {v0}, Lcom/jscape/util/h/o;-><init>()V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;->a:Lcom/jscape/util/h/I;

    invoke-interface {v1, p1, v0}, Lcom/jscape/util/h/I;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->d()[B

    move-result-object p1

    return-object p1
.end method

.method public signatureFor([BLjava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;)Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p4}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifierFor(Ljava/security/Key;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p4}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v1 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;->a([BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/PublicKey;)[B

    move-result-object p1

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/SignatureService;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;

    invoke-virtual {p4}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object p3

    invoke-interface {p2, p1, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;->sign([BLjava/security/PrivateKey;)Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    move-result-object p1

    return-object p1
.end method
