.class public Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "/\u000fs1Pz@\u0002\u0014]-VqK\u0018!k5WqK\u0018\t} K}J\u0002@e J`M\t\u000ej(\\uQ\u0005\u000fp2\u0002"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;->b:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x16

    goto :goto_1

    :cond_1
    const/16 v4, 0x27

    goto :goto_1

    :cond_2
    const/16 v4, 0xc

    goto :goto_1

    :cond_3
    const/16 v4, 0x72

    goto :goto_1

    :cond_4
    const/16 v4, 0x2d

    goto :goto_1

    :cond_5
    const/16 v4, 0x53

    goto :goto_1

    :cond_6
    const/16 v4, 0x5f

    :goto_1
    const/16 v5, 0x33

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;->a:Ljava/util/List;

    return-void
.end method

.method public varargs constructor <init>([Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V
    .locals 0

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;-><init>(Ljava/util/List;)V

    return-void
.end method

.method private static a(Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException;)Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public applyTo(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->c()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    if-nez v0, :cond_0

    :try_start_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;->a(Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException;)Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException;

    move-result-object p1

    throw p1

    :cond_0
    move-object v3, v1

    :goto_1
    check-cast v3, Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    :try_start_1
    invoke-interface {v3, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;->applyTo(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-void

    :catch_1
    move-exception v2

    :catch_2
    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    throw v2
.end method

.method public authentications()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;->a:Ljava/util/List;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
