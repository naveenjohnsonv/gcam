.class public Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractivePredefinedClientAuthentication;
.super Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;)V

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractivePredefinedClientAuthentication;->a:Ljava/util/List;

    return-void
.end method

.method public varargs constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p3

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractivePredefinedClientAuthentication;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public static defaultAuthentication(Ljava/lang/String;Ljava/util/List;)Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractivePredefinedClientAuthentication;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractivePredefinedClientAuthentication;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractivePredefinedClientAuthentication;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;->defaultMessages()Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractivePredefinedClientAuthentication;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method protected responsesFor(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/UserAuthInfoRequestPrompt;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractivePredefinedClientAuthentication;->a:Ljava/util/List;

    return-object p1
.end method
