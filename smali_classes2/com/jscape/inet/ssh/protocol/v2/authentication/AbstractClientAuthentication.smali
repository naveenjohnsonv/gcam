.class public abstract Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthBanner$Handler;


# static fields
.field private static b:I

.field private static final e:[Ljava/lang/String;


# instance fields
.field protected banner:Ljava/lang/String;

.field protected bannerLanguageTag:Ljava/lang/String;

.field protected final codecFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/16 v2, 0x5b

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->b(I)V

    const/4 v2, 0x0

    const/16 v3, 0x15

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x4e

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\u001b.a0r\u0016QEBb?{\rUPkW0{E\u0013&vlp%n\u0019WCMo8y\u0016@v{w9y\u0016@^mb%u\u0017Z\u0017ua0r\u0016QE3$"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x3c

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->e:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x7a

    goto :goto_2

    :cond_2
    const/16 v12, 0x36

    goto :goto_2

    :cond_3
    const/16 v12, 0x52

    goto :goto_2

    :cond_4
    const/16 v12, 0x1f

    goto :goto_2

    :cond_5
    const/16 v12, 0x4d

    goto :goto_2

    :cond_6
    const/16 v12, 0x40

    goto :goto_2

    :cond_7
    const/16 v12, 0x79

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->codecFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;

    return-void
.end method

.method private static a(Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException;)Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException;
    .locals 0

    return-object p0
.end method

.method public static b()I
    .locals 1

    sget v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->b:I

    return v0
.end method

.method public static b(I)V
    .locals 0

    sput p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->b:I

    return-void
.end method

.method public static c()I
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->b()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0xf

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method protected assertSuccess(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException;,
            Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$InvalidCredentialsException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->c()I

    move-result v0

    :try_start_0
    instance-of v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthSuccess;
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    if-nez v0, :cond_2

    :try_start_1
    instance-of v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthFailure;
    :try_end_1
    .catch Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->a(Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException;)Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    if-eqz v1, :cond_3

    :cond_2
    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthFailure;

    :try_start_2
    iget-boolean p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthFailure;->partialSuccess:Z

    if-eqz p1, :cond_3

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException;

    invoke-direct {p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException;-><init>()V

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->a(Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException;)Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException;

    move-result-object p1

    throw p1

    :cond_3
    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$InvalidCredentialsException;

    invoke-direct {p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$InvalidCredentialsException;-><init>()V

    throw p1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->a(Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException;)Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$PartialSuccessException;

    move-result-object p1

    throw p1
.end method

.method public banner()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->banner:Ljava/lang/String;

    return-object v0
.end method

.method public bannerLanguageTag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->bannerLanguageTag:Ljava/lang/String;

    return-object v0
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthBanner;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthBanner;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    iget-object p2, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthBanner;->message:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->banner:Ljava/lang/String;

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthBanner;->languageTag:Ljava/lang/String;

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->bannerLanguageTag:Ljava/lang/String;

    return-void
.end method

.method protected init(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)V
    .locals 0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->banner:Ljava/lang/String;

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->bannerLanguageTag:Ljava/lang/String;

    return-void
.end method

.method protected intermediateMessage(Lcom/jscape/inet/ssh/protocol/messages/Message;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/messages/Message;->handlerClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result p1

    return p1
.end method

.method protected receive(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->c()I

    move-result v0

    move-object v1, p0

    :cond_0
    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->read()Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->intermediateMessage(Lcom/jscape/inet/ssh/protocol/messages/Message;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    :goto_0
    if-eqz v2, :cond_0

    if-nez v0, :cond_2

    return-object v2

    :cond_2
    :goto_1
    invoke-virtual {v2, v1, p1}, Lcom/jscape/inet/ssh/protocol/messages/Message;->accept(Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->e:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->banner:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->bannerLanguageTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
