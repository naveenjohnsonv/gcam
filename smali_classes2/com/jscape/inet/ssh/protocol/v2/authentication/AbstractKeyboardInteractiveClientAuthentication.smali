.class public abstract Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;
.super Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;


# static fields
.field protected static final LANGUAGE_TAG:Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# instance fields
.field protected final username:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, -0x1

    move v4, v0

    move v5, v2

    :goto_0
    const/16 v6, 0x26

    const/4 v7, 0x1

    add-int/2addr v3, v7

    add-int/2addr v4, v3

    const-string v8, "-&\u0002-&"

    invoke-virtual {v8, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    array-length v9, v3

    move v10, v2

    :goto_1
    const/4 v11, 0x5

    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v3}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v6, v5, 0x1

    aput-object v3, v1, v5

    if-ge v4, v11, :cond_0

    invoke-virtual {v8, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;->c:[Ljava/lang/String;

    aget-object v0, v1, v7

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;->LANGUAGE_TAG:Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v3, v10

    rem-int/lit8 v13, v10, 0x7

    const/16 v14, 0x6e

    if-eqz v13, :cond_6

    if-eq v13, v7, :cond_6

    if-eq v13, v0, :cond_5

    const/4 v14, 0x3

    if-eq v13, v14, :cond_4

    const/4 v14, 0x4

    if-eq v13, v14, :cond_3

    if-eq v13, v11, :cond_2

    const/16 v14, 0x54

    goto :goto_2

    :cond_2
    const/16 v14, 0x7f

    goto :goto_2

    :cond_3
    const/16 v14, 0x53

    goto :goto_2

    :cond_4
    const/16 v14, 0x77

    goto :goto_2

    :cond_5
    const/16 v14, 0x2d

    :cond_6
    :goto_2
    xor-int v11, v6, v14

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v3, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method protected constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;)V

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;->username:Ljava/lang/String;

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public applyTo(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)V

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->b()I

    move-result v0

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;->sendRequest(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;->receive(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object p2

    instance-of v1, p2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthInfoRequest;

    if-eqz v1, :cond_1

    :try_start_0
    move-object v1, p2

    check-cast v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthInfoRequest;

    invoke-virtual {p0, v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;->handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthInfoRequest;Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_2

    if-nez v0, :cond_3

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;->assertSuccess(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    if-nez v0, :cond_4

    :cond_3
    if-nez v0, :cond_0

    :cond_4
    return-void

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method protected handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthInfoRequest;Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthInfoRequest;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthInfoRequest;->instruction:Ljava/lang/String;

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthInfoRequest;->prompts:Ljava/util/List;

    invoke-virtual {p0, v0, v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;->responsesFor(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthInfoResponse;

    invoke-direct {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthInfoResponse;-><init>(Ljava/util/List;)V

    invoke-virtual {p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    return-void
.end method

.method protected init(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;->codecFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->session()Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    move-result-object p1

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->messageCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    invoke-interface {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;->update(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    return-void
.end method

.method protected abstract responsesFor(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/UserAuthInfoRequestPrompt;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method protected sendRequest(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestKeyboardInteractive;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;->username:Ljava/lang/String;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;->c:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v1, p2, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestKeyboardInteractive;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    return-void
.end method
