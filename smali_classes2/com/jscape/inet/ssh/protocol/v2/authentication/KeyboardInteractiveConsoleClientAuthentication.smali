.class public Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveConsoleClientAuthentication;
.super Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;


# instance fields
.field private final a:Lcom/jscape/util/a/j;


# direct methods
.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Lcom/jscape/util/a/j;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractKeyboardInteractiveClientAuthentication;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;)V

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveConsoleClientAuthentication;->a:Lcom/jscape/util/a/j;

    return-void
.end method

.method private static b(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static systemConsoleAuthentication(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveConsoleClientAuthentication;
    .locals 2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveConsoleClientAuthentication;

    invoke-static {}, Lcom/jscape/util/a/j;->a()Lcom/jscape/util/a/j;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveConsoleClientAuthentication;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Lcom/jscape/util/a/j;)V

    return-object v0
.end method

.method public static systemConsoleAuthentication(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveConsoleClientAuthentication;
    .locals 3

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveConsoleClientAuthentication;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;->defaultMessages()Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;

    move-result-object v1

    invoke-static {}, Lcom/jscape/util/a/j;->a()Lcom/jscape/util/a/j;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveConsoleClientAuthentication;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Lcom/jscape/util/a/j;)V

    return-object v0
.end method


# virtual methods
.method protected responsesFor(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/UserAuthInfoRequestPrompt;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveConsoleClientAuthentication;->a:Lcom/jscape/util/a/j;

    invoke-virtual {v0, p1}, Lcom/jscape/util/a/j;->b(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveConsoleClientAuthentication;->a:Lcom/jscape/util/a/j;

    invoke-virtual {p1, p2}, Lcom/jscape/util/a/j;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->b()I

    move-result p1

    new-instance p2, Ljava/util/ArrayList;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :cond_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/ssh/protocol/v2/messages/UserAuthInfoRequestPrompt;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveConsoleClientAuthentication;->a:Lcom/jscape/util/a/j;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/messages/UserAuthInfoRequestPrompt;->prompt:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/jscape/util/a/j;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_1

    :try_start_0
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveConsoleClientAuthentication;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object p2
.end method
