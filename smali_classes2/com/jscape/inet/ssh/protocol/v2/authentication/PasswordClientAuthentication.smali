.class public Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;
.super Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field private a:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x15

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x6

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\u0008Is/uU\u0014V%p |N\u0010C\u000cE/|\u0006V\u001c\u0008Ia/hH\u0006K\u001bus<\u0011[\u000eC;i7\u001b\u0013E\u0007\u007f+i\u0006V+t\u0008b=lT\u0003@(d:s^\u001fP\u0000r/oR\u001eJ&a+iZ\u0005M\u0006\u007fn`N\u0002A\u001b\u007f/v^L\u0003"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x5e

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->d:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v13, 0x2

    if-eq v12, v13, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x77

    goto :goto_2

    :cond_2
    const/16 v12, 0x3d

    goto :goto_2

    :cond_3
    const/16 v12, 0x1d

    goto :goto_2

    :cond_4
    const/16 v12, 0x48

    goto :goto_2

    :cond_5
    const/16 v12, 0x17

    goto :goto_2

    :cond_6
    const/16 v12, 0x6f

    goto :goto_2

    :cond_7
    const/16 v12, 0x22

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;)V

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->a:Ljava/lang/String;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->c:Ljava/lang/String;

    return-void
.end method

.method private a(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPassword;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->c:Ljava/lang/String;

    invoke-direct {v0, v1, p2, v2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPassword;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->receive(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->assertSuccess(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    return-void
.end method

.method public static defaultAuthentication(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;
    .locals 2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordAuthenticationMessages;->defaultMessages()Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordAuthenticationMessages;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public applyTo(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)V

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->a(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;Ljava/lang/String;)V

    return-void
.end method

.method protected init(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->codecFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->session()Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    move-result-object p1

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->messageCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    invoke-interface {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;->update(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->d:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->banner:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->bannerLanguageTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updatePassword(Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->c:Ljava/lang/String;

    return-void
.end method

.method public updateUsername(Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->a:Ljava/lang/String;

    return-void
.end method
