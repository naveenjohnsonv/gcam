.class public Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordPublicKeyAuthenticationMessages;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;


# static fields
.field public static final REQUEST_TYPES:[Ljava/lang/Class;

.field private static final b:[Ljava/lang/String;


# instance fields
.field private final a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "l\u00053U&%GcM#Y=!Gf\u0003>^?!\tw\u0015([ \'^b\u0019"

    const/16 v5, 0x1e

    const/16 v6, 0x14

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x4e

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v2, 0x3

    const/4 v15, 0x2

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v2, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v2

    goto :goto_0

    :cond_0
    const/16 v5, 0xd

    const-string v4, "P6\u001dk\u0008N8\u0000}\u0007\u0012~Z"

    move v6, v0

    move v8, v2

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v6, v2

    move v8, v11

    :goto_3
    const/16 v9, 0x77

    add-int/2addr v7, v10

    add-int v2, v7, v6

    invoke-virtual {v4, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordPublicKeyAuthenticationMessages;->b:[Ljava/lang/String;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Class;

    const-class v4, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNone;

    aput-object v4, v1, v3

    const-class v3, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPassword;

    aput-object v3, v1, v10

    const-class v3, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword;

    aput-object v3, v1, v15

    const-class v3, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKey;

    aput-object v3, v1, v2

    const-class v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;

    aput-object v2, v1, v0

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestKeyboardInteractive;

    const/4 v2, 0x5

    aput-object v0, v1, v2

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordPublicKeyAuthenticationMessages;->REQUEST_TYPES:[Ljava/lang/Class;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v3, v14, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v10, :cond_8

    if-eq v3, v15, :cond_7

    if-eq v3, v2, :cond_6

    if-eq v3, v0, :cond_5

    const/4 v2, 0x5

    if-eq v3, v2, :cond_4

    const/16 v2, 0x7b

    goto :goto_4

    :cond_4
    const/16 v2, 0xa

    goto :goto_4

    :cond_5
    const/4 v2, 0x7

    goto :goto_4

    :cond_6
    const/16 v2, 0x79

    goto :goto_4

    :cond_7
    move v2, v0

    goto :goto_4

    :cond_8
    const/16 v2, 0x2e

    goto :goto_4

    :cond_9
    const/16 v2, 0x49

    :goto_4
    xor-int/2addr v2, v9

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method public varargs constructor <init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordPublicKeyAuthenticationMessages;->a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    return-void
.end method

.method private static a()[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordPublicKeyAuthenticationMessages;->b()[Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;)V

    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordPublicKeyAuthenticationMessages;->REQUEST_TYPES:[Ljava/lang/Class;

    const/16 v4, 0x32

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method

.method private static b()[Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;
    .locals 13

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    const/4 v1, 0x0

    new-array v2, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)V

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object v0

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;

    new-array v3, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;

    invoke-direct {v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;)V

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signatures;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;

    move-result-object v2

    const/4 v3, 0x4

    new-array v3, v3, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordPublicKeyAuthenticationMessages;->b:[Ljava/lang/String;

    const/4 v6, 0x2

    aget-object v7, v5, v6

    new-instance v8, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestNoneCodec;

    invoke-direct {v8}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestNoneCodec;-><init>()V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/Class;

    const-class v11, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNone;

    aput-object v11, v10, v1

    invoke-direct {v4, v7, v8, v10}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$MethodCodec;[Ljava/lang/Class;)V

    aput-object v4, v3, v1

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;

    const/4 v7, 0x3

    aget-object v8, v5, v7

    new-instance v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPasswordCodec;

    invoke-direct {v10}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPasswordCodec;-><init>()V

    new-array v11, v6, [Ljava/lang/Class;

    const-class v12, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPassword;

    aput-object v12, v11, v1

    const-class v12, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword;

    aput-object v12, v11, v9

    invoke-direct {v4, v8, v10, v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$MethodCodec;[Ljava/lang/Class;)V

    aput-object v4, v3, v9

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;

    aget-object v8, v5, v9

    new-instance v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPublicKeyCodec;

    invoke-direct {v10, v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPublicKeyCodec;-><init>(Lcom/jscape/util/h/I;Lcom/jscape/util/h/I;)V

    new-array v0, v6, [Ljava/lang/Class;

    const-class v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKey;

    aput-object v2, v0, v1

    const-class v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;

    aput-object v2, v0, v9

    invoke-direct {v4, v8, v10, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$MethodCodec;[Ljava/lang/Class;)V

    aput-object v4, v3, v6

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;

    aget-object v2, v5, v1

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestKeyboardInteractiveCodec;

    invoke-direct {v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestKeyboardInteractiveCodec;-><init>()V

    new-array v5, v9, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestKeyboardInteractive;

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v4, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$MethodCodec;[Ljava/lang/Class;)V

    aput-object v0, v3, v7

    return-object v3
.end method

.method public static defaultMessages()Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordPublicKeyAuthenticationMessages;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/AuthenticationMessages;->ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordPublicKeyAuthenticationMessages;->a()[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/jscape/util/G;->a([[Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordPublicKeyAuthenticationMessages;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    new-array v2, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    invoke-direct {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordPublicKeyAuthenticationMessages;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)V

    return-object v1
.end method

.method public static init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordPublicKeyAuthenticationMessages;->a()[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AuthenticationMessages;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p0

    return-object p0
.end method

.method public static initKeyboardInteractiveEntry(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;
    .locals 8

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/authentication/AbstractClientAuthentication;->b()I

    move-result v0

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthInfoRequestCodec;

    invoke-direct {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthInfoRequestCodec;-><init>()V

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthInfoRequest;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    const/16 v6, 0x3c

    invoke-direct {v2, v6, v3, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthInfoResponseCodec;

    invoke-direct {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthInfoResponseCodec;-><init>()V

    new-array v5, v4, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthInfoResponse;

    aput-object v6, v5, v7

    const/16 v6, 0x3d

    invoke-direct {v2, v6, v3, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p0

    if-nez v0, :cond_0

    const/4 v0, 0x3

    new-array v0, v0, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object p0
.end method

.method public static initPasswordChangeRequestEntry(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthPasswdChangeReqCodec;

    invoke-direct {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthPasswdChangeReqCodec;-><init>()V

    new-array v0, v0, [Ljava/lang/Class;

    const-class v4, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthPasswdChangeReq;

    const/4 v5, 0x0

    aput-object v4, v0, v5

    const/16 v4, 0x3c

    invoke-direct {v2, v4, v3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v5

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p0

    return-object p0
.end method

.method public static initPublicKeyOkEntry(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;
    .locals 6

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    const/4 v1, 0x0

    new-array v2, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)V

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object v0

    const/4 v2, 0x1

    new-array v3, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthPkOkCodec;

    invoke-direct {v5, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthPkOkCodec;-><init>(Lcom/jscape/util/h/I;)V

    new-array v0, v2, [Ljava/lang/Class;

    const-class v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthPkOk;

    aput-object v2, v0, v1

    const/16 v2, 0x3c

    invoke-direct {v4, v2, v5, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v4, v3, v1

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public update(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordPublicKeyAuthenticationMessages;->a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p1

    return-object p1
.end method
