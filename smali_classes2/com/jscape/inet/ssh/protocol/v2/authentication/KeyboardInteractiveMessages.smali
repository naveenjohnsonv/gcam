.class public Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;


# static fields
.field public static final ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

.field public static final METHOD_ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;

.field public static final REQUEST_TYPES:[Ljava/lang/Class;


# instance fields
.field private final a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x4

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x52

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "Nz\u001f\u0006\u0014Kp\u0008\u0001d,kD8\u0018\r\u007f(kAv\u0005\n}("

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    const/4 v12, 0x3

    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x19

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    new-array v2, v0, [Ljava/lang/Class;

    const-class v4, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNone;

    aput-object v4, v2, v3

    const-class v4, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestKeyboardInteractive;

    aput-object v4, v2, v8

    sput-object v2, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;->REQUEST_TYPES:[Ljava/lang/Class;

    new-array v2, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;

    aget-object v5, v1, v3

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestNoneCodec;

    invoke-direct {v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestNoneCodec;-><init>()V

    new-array v7, v8, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNone;

    aput-object v9, v7, v3

    invoke-direct {v4, v5, v6, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$MethodCodec;[Ljava/lang/Class;)V

    aput-object v4, v2, v3

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;

    aget-object v1, v1, v8

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestKeyboardInteractiveCodec;

    invoke-direct {v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestKeyboardInteractiveCodec;-><init>()V

    new-array v6, v8, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestKeyboardInteractive;

    aput-object v7, v6, v3

    invoke-direct {v4, v1, v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$MethodCodec;[Ljava/lang/Class;)V

    aput-object v4, v2, v8

    sput-object v2, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;->METHOD_ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;

    new-array v1, v12, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    const/16 v4, 0x32

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec;

    sget-object v6, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;->METHOD_ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;

    invoke-direct {v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$Entry;)V

    sget-object v6, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;->REQUEST_TYPES:[Ljava/lang/Class;

    invoke-direct {v2, v4, v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v3

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    const/16 v4, 0x3c

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthInfoRequestCodec;

    invoke-direct {v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthInfoRequestCodec;-><init>()V

    new-array v6, v8, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthInfoRequest;

    aput-object v7, v6, v3

    invoke-direct {v2, v4, v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v8

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    const/16 v4, 0x3d

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthInfoResponseCodec;

    invoke-direct {v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthInfoResponseCodec;-><init>()V

    new-array v6, v8, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthInfoResponse;

    aput-object v7, v6, v3

    invoke-direct {v2, v4, v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v0

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;->ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    return-void

    :cond_1
    aget-char v13, v4, v11

    rem-int/lit8 v14, v11, 0x7

    if-eqz v14, :cond_7

    if-eq v14, v8, :cond_6

    if-eq v14, v0, :cond_5

    if-eq v14, v12, :cond_4

    if-eq v14, v2, :cond_3

    const/4 v12, 0x5

    if-eq v14, v12, :cond_2

    const/16 v12, 0x4b

    goto :goto_2

    :cond_2
    const/16 v12, 0x1f

    goto :goto_2

    :cond_3
    const/16 v12, 0x59

    goto :goto_2

    :cond_4
    const/16 v12, 0x31

    goto :goto_2

    :cond_5
    const/16 v12, 0x23

    goto :goto_2

    :cond_6
    const/16 v12, 0x47

    goto :goto_2

    :cond_7
    const/16 v12, 0x72

    :goto_2
    xor-int/2addr v12, v7

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1
.end method

.method public varargs constructor <init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;->a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    return-void
.end method

.method public static defaultMessages()Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/AuthenticationMessages;->ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;->ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/jscape/util/G;->a([[Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    new-array v2, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    invoke-direct {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)V

    return-object v1
.end method


# virtual methods
.method public update(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveMessages;->a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p1

    return-object p1
.end method
