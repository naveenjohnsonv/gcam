.class public Lcom/jscape/inet/ssh/protocol/v2/authentication/AuthenticationMessages;
.super Ljava/lang/Object;


# static fields
.field public static final ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthFailureCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthFailureCodec;-><init>()V

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthFailure;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    const/16 v5, 0x33

    invoke-direct {v1, v5, v2, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthSuccessCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthSuccessCodec;-><init>()V

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthSuccess;

    aput-object v5, v4, v6

    const/16 v5, 0x34

    invoke-direct {v1, v5, v2, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v3

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthBannerCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthBannerCodec;-><init>()V

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthBanner;

    aput-object v4, v3, v6

    const/16 v4, 0x35

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AuthenticationMessages;->ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/AuthenticationMessages;->ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p0

    return-object p0
.end method
