.class final Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$2;
.super Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    return-void
.end method


# virtual methods
.method public update(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 4

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$2;->name:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v3, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group1Sha1(ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p2

    invoke-direct {v1, v2, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    aput-object v1, v0, v3

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p1

    return-object p1
.end method

.method public update(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 4

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$2;->name:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v3, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group1Sha1(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p2

    invoke-direct {v1, v2, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    aput-object v1, v0, v3

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p1

    return-object p1
.end method
