.class public Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$OperationException;
.super Ljava/lang/Exception;


# static fields
.field private static final serialVersionUID:J = -0x71bea3bb19f5d627L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$OperationException;
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v0

    if-nez v0, :cond_1

    instance-of v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$OperationException;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$OperationException;

    invoke-direct {v0, p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$OperationException;-><init>(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    :goto_0
    move-object v0, p0

    check-cast v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$OperationException;

    :goto_1
    return-object v0
.end method
