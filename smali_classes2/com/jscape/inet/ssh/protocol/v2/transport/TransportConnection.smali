.class public Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/k/a/A;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDisconnect$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgIgnore$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDebug$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit$Handler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/k/a/A<",
        "Lcom/jscape/inet/ssh/protocol/messages/Message;",
        ">;",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDisconnect$Handler;",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgIgnore$Handler;",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDebug$Handler;",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit$Handler;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static s:[Lcom/jscape/util/aq;

.field private static final t:[Ljava/lang/String;


# instance fields
.field private final d:Lcom/jscape/util/k/a/A;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/k/a/A<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

.field private final f:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;

.field private final g:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;

.field private final h:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;

.field private final i:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;

.field private final j:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

.field private final k:Ljava/util/concurrent/locks/Lock;

.field private final l:Ljava/util/concurrent/locks/Lock;

.field private final m:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final n:Ljava/util/logging/Logger;

.field private o:Z

.field private p:Z

.field private q:Ljava/lang/Long;

.field private r:Ljava/lang/Exception;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->b([Lcom/jscape/util/aq;)V

    const/4 v2, 0x0

    const-string v3, "3G\u0005uw\u000f|\u0016\t\u0002uw\u0017x\u0008\t\u0014bw\u000eoT\u001d/G\u0002eu\u0011r\u0008]\u0014t%\u0008s\u0019F\u001cyk\u0006=\u0017L\u0002cd\u0006xT$/G\u0014hu\u0004~\u000eL\u00150V2U%d\"WZ*X\"v8^L5=\u0017L\u0002cd\u0006xT*/G\u0002eu\u0011r\u0008]\u0014t%2N2\t\u001cuv\u0012|\u001dLQqqAF_ZQ,(_=_Z,*%Dn\u0002\u001fG\u00105Y\u0014bd\u0015t\u0015GQuw\u0013r\u0008\u0007\u001b4FQsj\u000cp\u0015GQqi\u0006r\u0008@\u0005xh\u0012=\u001cF\u0004~aO\u0016?[\u0003\u007fwAn\u001fG\u0015yk\u0006=\u0017L\u0002cd\u0006xT\'3G\u0012\u007fh\u0008s\u001d\t\"CMAp\u001fZ\u0002qb\u0004=\u001b]QK \u0012=F\u0004O0 \u0012@@\tTc\'5\\\u0005wj\u0008s\u001d\t\"CMAp\u001fZ\u0002qb\u0004=\u001b]QK \u0012=F\u0004O0 \u0012@@\tTc"

    const/16 v4, 0x11b

    const/16 v5, 0x16

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x35

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    const/4 v14, 0x4

    const/4 v15, 0x2

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x2c

    const-string v3, "O\u0017)y*i`&T>Y\u0010N.uP!M\u0016S)!Y YY@4ujhYY\u001dmk\u0011hY$\u001b`pB"

    move v7, v10

    move v5, v15

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x65

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->t:[Ljava/lang/String;

    aget-object v0, v0, v14

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->b:Ljava/util/List;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifiers()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->c:Ljava/util/List;

    return-void

    :cond_3
    aget-char v16, v10, v13

    rem-int/lit8 v1, v13, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v9, :cond_8

    if-eq v1, v15, :cond_7

    const/4 v15, 0x3

    if-eq v1, v15, :cond_6

    if-eq v1, v14, :cond_5

    const/4 v14, 0x5

    if-eq v1, v14, :cond_4

    const/16 v1, 0x28

    goto :goto_4

    :cond_4
    const/16 v1, 0x54

    goto :goto_4

    :cond_5
    const/16 v1, 0x30

    goto :goto_4

    :cond_6
    const/16 v1, 0x25

    goto :goto_4

    :cond_7
    const/16 v1, 0x44

    goto :goto_4

    :cond_8
    const/16 v1, 0x1c

    goto :goto_4

    :cond_9
    const/16 v1, 0x4f

    :goto_4
    xor-int/2addr v1, v8

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/util/k/a/A;Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Ljava/util/logging/Logger;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/A<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;",
            "Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;",
            "Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;",
            "Ljava/util/logging/Logger;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e()[Lcom/jscape/util/aq;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->d:Lcom/jscape/util/k/a/A;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->f:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->g:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->h:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;

    invoke-static {p6}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p6, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->i:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;

    invoke-static {p7}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p7, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->j:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    new-instance p1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->k:Ljava/util/concurrent/locks/Lock;

    new-instance p1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->l:Ljava/util/concurrent/locks/Lock;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-static {p8}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p8, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->n:Ljava/util/logging/Logger;

    if-eqz v0, :cond_0

    const/4 p1, 0x5

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-void
.end method

.method private a()Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->d:Lcom/jscape/util/k/a/A;

    invoke-interface {v0}, Lcom/jscape/util/k/a/A;->read()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/ssh/protocol/messages/Message;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e()[Lcom/jscape/util/aq;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->d(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    :try_start_0
    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->b(Lcom/jscape/inet/ssh/protocol/messages/Message;)Z

    move-result v2
    :try_end_0
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v1, :cond_1

    if-eqz v2, :cond_0

    :try_start_1
    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->c(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    :try_end_1
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_1 .. :try_end_1} :catch_3

    if-nez v1, :cond_2

    :try_start_2
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->closed()Z

    move-result v2
    :try_end_2
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_0

    :cond_0
    return-object v0

    :cond_1
    :goto_0
    if-eqz v2, :cond_5

    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->r:Ljava/lang/Exception;
    :try_end_3
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_3 .. :try_end_3} :catch_0

    if-nez v1, :cond_4

    if-nez v0, :cond_3

    goto :goto_1

    :cond_3
    :try_start_4
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->r:Ljava/lang/Exception;
    :try_end_4
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_4
    invoke-static {v0}, Lcom/jscape/util/k/a/Connection$ConnectionException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/util/k/a/Connection$ConnectionException;

    move-result-object v0

    throw v0

    :catch_0
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_5
    :goto_1
    const/4 v0, 0x0

    return-object v0

    :catch_2
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_6 .. :try_end_6} :catch_3

    :catch_3
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_7 .. :try_end_7} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->f(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->d:Lcom/jscape/util/k/a/A;

    invoke-interface {v0, p1}, Lcom/jscape/util/k/a/A;->write(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
.end method

.method private a(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->sessionId:[B

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->asKeyFactory([B)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->g:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->algorithms:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->encryptionServerToClient:Ljava/lang/String;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$Mode;->DECRYPTION:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$Mode;

    invoke-interface {v0, v1, p1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;->serverToClientEncryptionFor(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$Mode;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->h:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->algorithms:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->macServerToClient:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;->serverToClientMacFor(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    move-result-object p1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->i:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->algorithms:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->compressionServerToClient:Ljava/lang/String;

    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$Mode;->DECOMPRESSION:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$Mode;

    invoke-interface {v1, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;->serverToClientCompressionFor(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$Mode;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    invoke-virtual {v2, v0, p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->readingAlgorithms(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;)V

    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->n:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    if-nez v0, :cond_0

    if-eqz v1, :cond_2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->d:Lcom/jscape/util/k/a/A;

    invoke-interface {v0}, Lcom/jscape/util/k/a/A;->closed()Z

    move-result v1

    :cond_0
    if-nez v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->n:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->t:[Ljava/lang/String;

    const/4 v3, 0x7

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    return-void
.end method

.method private b()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->f:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;

    invoke-interface {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;->keyExchanges()Ljava/util/List;

    move-result-object v1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->g:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;

    invoke-interface {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;->encryptions()Ljava/util/List;

    move-result-object v4

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->h:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;

    invoke-interface {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;->macs()Ljava/util/List;

    move-result-object v6

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->i:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;

    invoke-interface {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;->compressions()Ljava/util/List;

    move-result-object v8

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->c:Ljava/util/List;

    sget-object v10, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->b:Ljava/util/List;

    move-object v3, v4

    move-object v5, v6

    move-object v7, v8

    move-object v9, v10

    invoke-static/range {v1 .. v10}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->messageFor(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    move-result-object v1

    iput-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->clientKexInitMessage:Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->clientKexInitMessage:Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    return-void
.end method

.method private b(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->sessionId:[B

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->asKeyFactory([B)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->g:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->algorithms:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->encryptionClientToServer:Ljava/lang/String;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$Mode;->ENCRYPTION:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$Mode;

    invoke-interface {v0, v1, p1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;->clientToServerEncryptionFor(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$Mode;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->h:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->algorithms:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->macClientToServer:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;->clientToServerMacFor(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    move-result-object p1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->i:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->algorithms:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->compressionClientToServer:Ljava/lang/String;

    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$Mode;->COMPRESSION:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$Mode;

    invoke-interface {v1, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;->clientToServerCompressionFor(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$Mode;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    invoke-virtual {v2, v0, p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->writingAlgorithms(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;)V

    return-void
.end method

.method private b(Ljava/lang/Throwable;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->n:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->n:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->t:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method public static b([Lcom/jscape/util/aq;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->s:[Lcom/jscape/util/aq;

    return-void
.end method

.method private b(Lcom/jscape/inet/ssh/protocol/messages/Message;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/messages/Message;->handlerClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result p1

    return p1
.end method

.method private c()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection$ProtocolException;
        }
    .end annotation

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->p:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection$ProtocolException;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection$ProtocolException;-><init>(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection$1;)V

    throw v0
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection$ProtocolException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private c(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->d:Lcom/jscape/util/k/a/A;

    invoke-virtual {p1, p0, v0}, Lcom/jscape/inet/ssh/protocol/messages/Message;->accept(Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    new-instance p1, Lcom/jscape/util/k/a/Connection$ConnectionException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->t:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-direct {p1, v1, v0}, Lcom/jscape/util/k/a/Connection$ConnectionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
.end method

.method private d()V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->n:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->n:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->t:[Ljava/lang/String;

    const/16 v3, 0xb

    aget-object v0, v0, v3

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->d:Lcom/jscape/util/k/a/A;

    invoke-interface {v5}, Lcom/jscape/util/k/a/A;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->d:Lcom/jscape/util/k/a/A;

    invoke-interface {v5}, Lcom/jscape/util/k/a/A;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->algorithms:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private d(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->n:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->n:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->t:[Ljava/lang/String;

    const/16 v3, 0x8

    aget-object v0, v0, v3

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private e(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->n:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->n:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->t:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v0, v0, v3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public static e()[Lcom/jscape/util/aq;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->s:[Lcom/jscape/util/aq;

    return-object v0
.end method

.method private f(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->n:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->n:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->t:[Ljava/lang/String;

    const/16 v3, 0x9

    aget-object v0, v0, v3

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public attributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->d:Lcom/jscape/util/k/a/A;

    invoke-interface {v0}, Lcom/jscape/util/k/a/A;->attributes()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public base()Lcom/jscape/util/k/a/A;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jscape/util/k/a/A<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->d:Lcom/jscape/util/k/a/A;

    return-object v0
.end method

.method public close()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e()[Lcom/jscape/util/aq;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->d:Lcom/jscape/util/k/a/A;

    goto :goto_0

    :cond_1
    move-object v0, p0

    :goto_0
    invoke-interface {v0}, Lcom/jscape/util/k/a/A;->close()V

    return-void
.end method

.method public closed()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public creationTime()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->d:Lcom/jscape/util/k/a/A;

    invoke-interface {v0}, Lcom/jscape/util/k/a/A;->creationTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public debug(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDebug;

    invoke-direct {v0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDebug;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    return-void
.end method

.method public disconnect(Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;Ljava/lang/String;)V
    .locals 3

    :try_start_0
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDisconnect;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->t:[Ljava/lang/String;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-direct {v0, p1, p2, v1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDisconnect;-><init>(Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    :try_end_0
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->close()V

    return-void

    :goto_1
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->close()V

    throw p1
.end method

.method public exchangeKeys()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->l:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->b()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->o:Z

    :cond_0
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->o:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    if-nez v0, :cond_2

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->p:Z
    :try_end_1
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_2

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a()Lcom/jscape/inet/ssh/protocol/messages/Message;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->l:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :try_start_3
    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/jscape/util/aq;

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->b([Lcom/jscape/util/aq;)V
    :try_end_3
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_3 .. :try_end_3} :catch_1

    :cond_3
    return-void

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->l:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDebug;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDebug;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDisconnect;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDisconnect;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->close()V

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgIgnore;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgIgnore;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;Lcom/jscape/util/k/a/x;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e()[Lcom/jscape/util/aq;

    move-result-object p2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->l:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->c()V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iput-object p1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->serverKexInitMessage:Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection$ProtocolException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p2, :cond_1

    :try_start_1
    iget-boolean p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->o:Z

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->b()V
    :try_end_1
    .catch Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection$ProtocolException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->o:Z

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->p:Z

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->initAlgorithms()V

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->d()V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->f:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object p2, p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->algorithms:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;

    iget-object p2, p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->keyExchange:Ljava/lang/String;

    invoke-interface {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;->keyExchangeFor(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;

    move-result-object p1

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->j:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    invoke-interface {p1, p0, p2, v1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;->exchangeKeys(Lcom/jscape/util/k/a/A;Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;

    move-result-object p1

    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgNewKeys;

    invoke-direct {p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgNewKeys;-><init>()V

    invoke-direct {p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->hostKey:Ljava/security/PublicKey;

    iput-object v1, p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->hostKey:Ljava/security/PublicKey;

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->exchangeHash:[B

    invoke-virtual {p2, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->initSessionId([B)V

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->b(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->read()Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object p2

    check-cast p2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgNewKeys;

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;)V

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->p:Z

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->q:Ljava/lang/Long;
    :try_end_2
    .catch Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection$ProtocolException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception p1

    goto :goto_3

    :catch_1
    move-exception p1

    :try_start_3
    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->b(Ljava/lang/Throwable;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->r:Ljava/lang/Exception;

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->BY_APPLICATION:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    sget-object p2, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->t:[Ljava/lang/String;

    aget-object p2, p2, v0

    :goto_1
    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->disconnect(Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;Ljava/lang/String;)V

    goto :goto_2

    :catch_2
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->b(Ljava/lang/Throwable;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->r:Ljava/lang/Exception;

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->KEY_EXCHANGE_FAILED:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    sget-object p2, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->t:[Ljava/lang/String;

    const/4 v0, 0x6

    aget-object p2, p2, v0

    goto :goto_1

    :catch_3
    move-exception p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->r:Ljava/lang/Exception;

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->PROTOCOL_ERROR:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    sget-object p2, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->t:[Ljava/lang/String;

    const/4 v0, 0x2

    aget-object p2, p2, v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :goto_2
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->l:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :goto_3
    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->l:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
.end method

.method public ignore([B)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgIgnore;

    invoke-direct {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgIgnore;-><init>([B)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    return-void
.end method

.method public lastKeyExchangeDate()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->q:Ljava/lang/Long;

    return-object v0
.end method

.method public localAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->d:Lcom/jscape/util/k/a/A;

    invoke-interface {v0}, Lcom/jscape/util/k/a/A;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method public read()Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e()[Lcom/jscape/util/aq;

    move-result-object v0

    :cond_0
    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a()Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object v1

    :cond_1
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    return-object v1
.end method

.method public bridge synthetic read()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->read()Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object v0

    return-object v0
.end method

.method public readBytes()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->packetCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->readBytes()J

    move-result-wide v0

    return-wide v0
.end method

.method public readMessages()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->packetCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->readMessages()J

    move-result-wide v0

    return-wide v0
.end method

.method public remoteAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->d:Lcom/jscape/util/k/a/A;

    invoke-interface {v0}, Lcom/jscape/util/k/a/A;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method public session()Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->d:Lcom/jscape/util/k/a/A;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->l:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->a(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->l:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->l:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/messages/Message;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    return-void
.end method

.method public writtenBytes()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->packetCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->writtenBytes()J

    move-result-wide v0

    return-wide v0
.end method

.method public writtenMessages()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->e:Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->packetCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->writtenMessages()J

    move-result-wide v0

    return-wide v0
.end method
