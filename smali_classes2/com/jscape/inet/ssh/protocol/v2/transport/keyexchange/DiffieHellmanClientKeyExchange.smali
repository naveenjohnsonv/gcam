.class public Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
.super Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field protected final hostKeySignatureVerificationRequired:Z


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x6

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "j\u001dpdTk\u0006j\u001dpdTk\u0004j\u001dp`\u0006j\u001dpcPo"

    const/16 v5, 0x19

    move v7, v0

    const/4 v6, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x54

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0xb

    const-string v4, "\u001dj\u0007\u0014\'\u0018\u0004\u001dj\u0007\u0017"

    move v7, v0

    move v8, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x23

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->b:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v0, v14, 0x7

    const/4 v2, 0x5

    if-eqz v0, :cond_8

    if-eq v0, v10, :cond_7

    const/4 v3, 0x2

    if-eq v0, v3, :cond_6

    const/4 v3, 0x3

    if-eq v0, v3, :cond_9

    const/4 v3, 0x4

    if-eq v0, v3, :cond_5

    if-eq v0, v2, :cond_4

    const/16 v2, 0x4e

    goto :goto_4

    :cond_4
    const/16 v2, 0xd

    goto :goto_4

    :cond_5
    const/16 v2, 0x31

    goto :goto_4

    :cond_6
    const/16 v2, 0x65

    goto :goto_4

    :cond_7
    move v2, v10

    goto :goto_4

    :cond_8
    const/16 v2, 0x6d

    :cond_9
    :goto_4
    xor-int v0, v9, v2

    xor-int/2addr v0, v15

    int-to-char v0, v0

    aput-char v0, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v0, 0x6

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;ZLjava/lang/Object;)V
    .locals 6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;Ljava/lang/Object;)V

    iput-boolean p5, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->hostKeySignatureVerificationRequired:Z

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static group14Sha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 8

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_14:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_14:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->g:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->b:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v4, v0, v1

    move-object v0, v7

    move-object v1, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;ZLjava/lang/Object;)V

    return-object v7
.end method

.method public static group14Sha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 8

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_14:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_14:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->g:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->b:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v4, v0, v1

    move-object v0, v7

    move-object v1, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;ZLjava/lang/Object;)V

    return-object v7
.end method

.method public static group14Sha1(Z)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group14Sha1(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group14Sha1(ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group14Sha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group14Sha1(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group14Sha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group14Sha256(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 8

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_14:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_14:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->g:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->b:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v4, v0, v1

    move-object v0, v7

    move-object v1, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;ZLjava/lang/Object;)V

    return-object v7
.end method

.method public static group14Sha256(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 8

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_14:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_14:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->g:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->b:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object v4, v0, v1

    move-object v0, v7

    move-object v1, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;ZLjava/lang/Object;)V

    return-object v7
.end method

.method public static group14Sha256(Z)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group14Sha256(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group14Sha256(ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group14Sha256(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group14Sha256(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group14Sha256(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group15Sha512(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 8

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_15:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_15:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->g:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->b:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v4, v0, v1

    move-object v0, v7

    move-object v1, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;ZLjava/lang/Object;)V

    return-object v7
.end method

.method public static group15Sha512(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 8

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_15:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_15:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->g:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->b:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v4, v0, v1

    move-object v0, v7

    move-object v1, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;ZLjava/lang/Object;)V

    return-object v7
.end method

.method public static group15Sha512(Z)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group15Sha512(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group15Sha512(ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group15Sha512(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group15Sha512(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group15Sha512(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group16Sha512(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 8

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_16:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_16:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->g:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->b:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v4, v0, v1

    move-object v0, v7

    move-object v1, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;ZLjava/lang/Object;)V

    return-object v7
.end method

.method public static group16Sha512(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 8

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_16:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_16:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->g:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->b:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v4, v0, v1

    move-object v0, v7

    move-object v1, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;ZLjava/lang/Object;)V

    return-object v7
.end method

.method public static group16Sha512(Z)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group16Sha512(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group16Sha512(ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group16Sha512(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group16Sha512(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group16Sha512(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group17Sha512(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 8

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_17:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_17:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->g:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->b:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v4, v0, v1

    move-object v0, v7

    move-object v1, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;ZLjava/lang/Object;)V

    return-object v7
.end method

.method public static group17Sha512(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 8

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_17:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_17:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->g:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->b:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v4, v0, v1

    move-object v0, v7

    move-object v1, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;ZLjava/lang/Object;)V

    return-object v7
.end method

.method public static group17Sha512(Z)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group17Sha512(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group17Sha512(ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group17Sha512(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group17Sha512(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group17Sha512(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group18Sha512(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 8

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_18:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_18:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->g:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->b:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v4, v0, v1

    move-object v0, v7

    move-object v1, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;ZLjava/lang/Object;)V

    return-object v7
.end method

.method public static group18Sha512(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 8

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_18:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_18:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->g:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->b:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v4, v0, v1

    move-object v0, v7

    move-object v1, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;ZLjava/lang/Object;)V

    return-object v7
.end method

.method public static group18Sha512(Z)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group18Sha512(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group18Sha512(ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group18Sha512(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group18Sha512(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group18Sha512(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group1Sha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 8

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->g:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->b:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v4, v0, v1

    move-object v0, v7

    move-object v1, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;ZLjava/lang/Object;)V

    return-object v7
.end method

.method public static group1Sha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 8

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->g:Ljava/math/BigInteger;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->b:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v4, v0, v1

    move-object v0, v7

    move-object v1, p0

    move v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;ZLjava/lang/Object;)V

    return-object v7
.end method

.method public static group1Sha1(Z)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group1Sha1(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group1Sha1(ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group1Sha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static group1Sha1(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;
    .locals 1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group1Sha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public exchangeKeys(Lcom/jscape/util/k/a/A;Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/A<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;",
            "Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;",
            ")",
            "Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v11, p0

    move-object/from16 v0, p2

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->codecFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->messageCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    invoke-interface {v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;->update(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->generateKeyPair()Ljava/security/KeyPair;

    move-result-object v1

    invoke-virtual {v1}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v2

    check-cast v2, Ljavax/crypto/interfaces/DHPrivateKey;

    invoke-interface {v2}, Ljavax/crypto/interfaces/DHPrivateKey;->getX()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v1}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v1

    check-cast v1, Ljavax/crypto/interfaces/DHPublicKey;

    invoke-interface {v1}, Ljavax/crypto/interfaces/DHPublicKey;->getY()Ljava/math/BigInteger;

    move-result-object v8

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhInit;

    invoke-direct {v1, v8}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhInit;-><init>(Ljava/math/BigInteger;)V

    move-object/from16 v3, p1

    invoke-interface {v3, v1}, Lcom/jscape/util/k/a/A;->write(Ljava/lang/Object;)V

    invoke-interface/range {p1 .. p1}, Lcom/jscape/util/k/a/A;->read()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;

    invoke-interface/range {p1 .. p1}, Lcom/jscape/util/k/a/A;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jscape/util/k/TransportAddress;->inetAddress()Ljava/net/InetAddress;

    move-result-object v1

    iget-object v3, v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;->hostKey:Ljava/security/PublicKey;

    move-object/from16 v4, p3

    invoke-interface {v4, v1, v3}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;->assertKeyValid(Ljava/net/InetAddress;Ljava/security/PublicKey;)V

    iget-object v1, v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;->f:Ljava/math/BigInteger;

    invoke-virtual {v11, v2, v1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->computeSharedSecret(Ljava/math/BigInteger;Ljava/math/BigInteger;)[B

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->hash()Lcom/jscape/a/f;

    move-result-object v15

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->clientIdentificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    iget-object v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->serverIdentificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->clientKexInitMessage:Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    iget-object v6, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->serverKexInitMessage:Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    iget-object v7, v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;->hostKeyBlob:[B

    iget-object v9, v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;->f:Ljava/math/BigInteger;

    move-object/from16 v1, p0

    move-object v2, v15

    move-object v10, v14

    invoke-virtual/range {v1 .. v10}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->exchangeHashFor(Lcom/jscape/a/d;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;[BLjava/math/BigInteger;Ljava/math/BigInteger;[B)[B

    move-result-object v0

    if-nez v12, :cond_0

    :try_start_0
    iget-boolean v1, v11, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->hostKeySignatureVerificationRequired:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_1

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_1
    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    iget-object v1, v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;->hostKey:Ljava/security/PublicKey;

    iget-object v2, v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;->signature:Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    invoke-virtual {v11, v0, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->verifySignature([BLjava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V

    :cond_1
    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;

    iget-object v2, v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;->hostKey:Ljava/security/PublicKey;

    invoke-direct {v1, v15, v14, v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;-><init>(Lcom/jscape/a/d;[B[BLjava/security/PublicKey;)V

    return-object v1
.end method

.method protected verifySignature([BLjava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->provider:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, 0x0

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->provider:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;->initSha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->provider:Ljava/lang/Object;

    instance-of v1, v0, Ljava/security/Provider;

    :cond_1
    if-eqz v1, :cond_2

    :try_start_2
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->provider:Ljava/lang/Object;

    check-cast v1, Ljava/security/Provider;

    invoke-static {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;->initSha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)V

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;->initSha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    move-result-object v0

    :goto_0
    invoke-interface {v0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;->assertSignatureValid([BLjava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V

    return-void

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method
