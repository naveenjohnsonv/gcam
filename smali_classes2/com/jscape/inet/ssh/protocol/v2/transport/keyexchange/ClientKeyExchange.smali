.class public interface abstract Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;
.super Ljava/lang/Object;


# virtual methods
.method public abstract exchangeKeys(Lcom/jscape/util/k/a/A;Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/A<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;",
            "Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;",
            ")",
            "Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method
