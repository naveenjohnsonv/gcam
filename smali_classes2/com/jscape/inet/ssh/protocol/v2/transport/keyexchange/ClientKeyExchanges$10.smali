.class final Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$10;
.super Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;


# static fields
.field private static final c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x4

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x6a

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "R\u0018VQ\u0004R\u0018VQ"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x9

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$10;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    if-eqz v13, :cond_7

    if-eq v13, v8, :cond_6

    if-eq v13, v0, :cond_5

    const/4 v14, 0x3

    if-eq v13, v14, :cond_4

    if-eq v13, v2, :cond_3

    const/4 v14, 0x5

    if-eq v13, v14, :cond_2

    const/16 v13, 0x41

    goto :goto_2

    :cond_2
    const/16 v13, 0x29

    goto :goto_2

    :cond_3
    const/16 v13, 0x72

    goto :goto_2

    :cond_4
    const/16 v13, 0xa

    goto :goto_2

    :cond_5
    const/16 v13, 0x7d

    goto :goto_2

    :cond_6
    const/16 v13, 0x3a

    goto :goto_2

    :cond_7
    const/16 v13, 0x6b

    :goto_2
    xor-int/2addr v13, v7

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method constructor <init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    return-void
.end method


# virtual methods
.method public update(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 5

    const/4 v0, 0x1

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$10;->name:Ljava/lang/String;

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$10;->c:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-static {v4, v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->defaultExchange(Ljava/lang/String;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;

    move-result-object p2

    invoke-direct {v2, v3, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    const/4 p2, 0x0

    aput-object v2, v1, p2

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p1

    return-object p1
.end method

.method public update(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$10;->name:Ljava/lang/String;

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$10;->c:[Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-static {v4, v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->defaultExchange(Ljava/lang/String;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;

    move-result-object p2

    invoke-direct {v2, v3, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    aput-object v2, v1, v5

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p1

    return-object p1
.end method
