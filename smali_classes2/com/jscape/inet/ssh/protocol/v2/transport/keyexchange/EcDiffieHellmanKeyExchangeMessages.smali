.class public Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;


# instance fields
.field private final a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;


# direct methods
.method public varargs constructor <init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;->a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    return-void
.end method

.method public static defaultMessages(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/lang/String;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;

    move-result-object p0

    return-object p0
.end method

.method public static defaultMessages(Ljava/lang/String;Ljava/lang/Object;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;
    .locals 1

    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;

    move-result-object p0

    goto :goto_0

    :cond_0
    instance-of v0, p1, Ljava/security/Provider;

    if-eqz v0, :cond_1

    check-cast p1, Ljava/security/Provider;

    invoke-static {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/lang/String;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;

    move-result-object p0

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static defaultMessages(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;
    .locals 10

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    const/4 v1, 0x0

    new-array v2, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)V

    invoke-static {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object v0

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;

    new-array v3, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;

    invoke-direct {v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;)V

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signatures;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;

    move-result-object v2

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexEcdhInitCodec;

    invoke-direct {v6, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexEcdhInitCodec;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v7, 0x1

    new-array v8, v7, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhInit;

    aput-object v9, v8, v1

    const/16 v9, 0x1e

    invoke-direct {v5, v9, v6, v8}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v4, v1

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexEcdhReplyCodec;

    invoke-direct {v6, v0, v2, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexEcdhReplyCodec;-><init>(Lcom/jscape/util/h/I;Lcom/jscape/util/h/I;Ljava/lang/String;Ljava/lang/Object;)V

    new-array p0, v7, [Ljava/lang/Class;

    const-class p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhReply;

    aput-object p1, p0, v1

    const/16 p1, 0x1f

    invoke-direct {v5, p1, v6, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v4, v7

    invoke-direct {v3, v4}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)V

    return-object v3
.end method

.method public static defaultMessages(Ljava/lang/String;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;
    .locals 10

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    const/4 v1, 0x0

    new-array v2, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)V

    invoke-static {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object v0

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;

    new-array v3, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;

    invoke-direct {v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;)V

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signatures;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;

    move-result-object v2

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexEcdhInitCodec;

    invoke-direct {v6, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexEcdhInitCodec;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v7, 0x1

    new-array v8, v7, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhInit;

    aput-object v9, v8, v1

    const/16 v9, 0x1e

    invoke-direct {v5, v9, v6, v8}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v4, v1

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexEcdhReplyCodec;

    invoke-direct {v6, v0, v2, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexEcdhReplyCodec;-><init>(Lcom/jscape/util/h/I;Lcom/jscape/util/h/I;Ljava/lang/String;Ljava/lang/Object;)V

    new-array p0, v7, [Ljava/lang/Class;

    const-class p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhReply;

    aput-object p1, p0, v1

    const/16 p1, 0x1f

    invoke-direct {v5, p1, v6, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v4, v7

    invoke-direct {v3, v4}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)V

    return-object v3
.end method


# virtual methods
.method public update(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;->a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p1

    return-object p1
.end method
