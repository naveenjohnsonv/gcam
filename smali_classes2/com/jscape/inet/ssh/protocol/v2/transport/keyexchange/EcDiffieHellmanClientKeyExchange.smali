.class public Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;
.super Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;


# instance fields
.field protected final hostKeySignatureVerificationRequired:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1, p3}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    iput-boolean p2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->hostKeySignatureVerificationRequired:Z

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public exchangeKeys(Lcom/jscape/util/k/a/A;Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/A<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;",
            "Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;",
            ")",
            "Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v11, p0

    move-object/from16 v0, p2

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->codecFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->messageCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    invoke-interface {v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;->update(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->generateKeyPair()Ljava/security/KeyPair;

    move-result-object v1

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhInit;

    invoke-virtual {v1}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhInit;-><init>(Ljava/security/PublicKey;)V

    move-object/from16 v3, p1

    invoke-interface {v3, v2}, Lcom/jscape/util/k/a/A;->write(Ljava/lang/Object;)V

    invoke-interface/range {p1 .. p1}, Lcom/jscape/util/k/a/A;->read()Ljava/lang/Object;

    move-result-object v2

    move-object v13, v2

    check-cast v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhReply;

    invoke-interface/range {p1 .. p1}, Lcom/jscape/util/k/a/A;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jscape/util/k/TransportAddress;->inetAddress()Ljava/net/InetAddress;

    move-result-object v2

    iget-object v3, v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhReply;->hostKey:Ljava/security/PublicKey;

    move-object/from16 v4, p3

    invoke-interface {v4, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;->assertKeyValid(Ljava/net/InetAddress;Ljava/security/PublicKey;)V

    iget-object v2, v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhReply;->qs:Ljava/security/PublicKey;

    invoke-virtual {v11, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->computeSharedSecret(Ljava/security/KeyPair;Ljava/security/PublicKey;)[B

    move-result-object v14

    invoke-virtual {v1}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v2

    invoke-virtual {v11, v2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->hashFor(Ljava/security/PublicKey;)Lcom/jscape/a/f;

    move-result-object v15

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->clientIdentificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    iget-object v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->serverIdentificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->clientKexInitMessage:Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    iget-object v6, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->serverKexInitMessage:Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    iget-object v7, v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhReply;->hostKeyBlob:[B

    invoke-virtual {v1}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v8

    iget-object v9, v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhReply;->qs:Ljava/security/PublicKey;

    move-object/from16 v1, p0

    move-object v2, v15

    move-object v10, v14

    invoke-virtual/range {v1 .. v10}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->exchangeHashFor(Lcom/jscape/a/d;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;[BLjava/security/PublicKey;Ljava/security/PublicKey;[B)[B

    move-result-object v0

    if-nez v12, :cond_0

    :try_start_0
    iget-boolean v1, v11, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->hostKeySignatureVerificationRequired:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_1

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_1
    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    iget-object v1, v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhReply;->hostKey:Ljava/security/PublicKey;

    iget-object v2, v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhReply;->signature:Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    invoke-virtual {v11, v0, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->verifySignature([BLjava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V

    :cond_1
    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;

    iget-object v2, v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhReply;->hostKey:Ljava/security/PublicKey;

    invoke-direct {v1, v15, v14, v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;-><init>(Lcom/jscape/a/d;[B[BLjava/security/PublicKey;)V

    return-object v1
.end method

.method protected verifySignature([BLjava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->provider:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, 0x0

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->provider:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;->initSha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->provider:Ljava/lang/Object;

    instance-of v1, v0, Ljava/security/Provider;

    :cond_1
    if-eqz v1, :cond_2

    :try_start_2
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->provider:Ljava/lang/Object;

    check-cast v1, Ljava/security/Provider;

    invoke-static {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;->initSha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)V

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;->initSha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    move-result-object v0

    :goto_0
    invoke-interface {v0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;->assertSignatureValid([BLjava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V

    return-void

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method
