.class public Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final exchangeHash:[B

.field public final hash:Lcom/jscape/a/d;

.field public final hostKey:Ljava/security/PublicKey;

.field public final sharedSecret:[B


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "Gu5Y\u000fFp\u000525i\r]yV\u000fGu#I\r\\t\u000f\u00065B\u001eKeV"

    const/16 v5, 0x1f

    const/16 v6, 0xf

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x68

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x23

    const/16 v4, 0xa

    const-string v6, "\u0014&k\u001dL\t\t]\u007f>\u0018scz7G\u001e*Yhd\u0017m\u00181MjwRD\u0015#Kn>"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v16, v6

    move v6, v4

    move-object/from16 v4, v16

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x3b

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    const/4 v3, 0x3

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v10, 0x2

    if-eq v2, v10, :cond_7

    if-eq v2, v3, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v3, 0x79

    goto :goto_4

    :cond_4
    const/16 v3, 0x46

    goto :goto_4

    :cond_5
    move v3, v0

    goto :goto_4

    :cond_6
    const/16 v3, 0x49

    goto :goto_4

    :cond_7
    const/16 v3, 0x38

    goto :goto_4

    :cond_8
    const/16 v3, 0x3d

    :cond_9
    :goto_4
    xor-int v2, v9, v3

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v10, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/a/d;[B[BLjava/security/PublicKey;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->hash:Lcom/jscape/a/d;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->sharedSecret:[B

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->exchangeHash:[B

    iput-object p4, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->hostKey:Ljava/security/PublicKey;

    return-void
.end method


# virtual methods
.method public asKeyFactory([B)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;
    .locals 4

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->hash:Lcom/jscape/a/d;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->sharedSecret:[B

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->exchangeHash:[B

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;-><init>(Lcom/jscape/a/d;[B[B[B)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->a:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->hash:Lcom/jscape/a/d;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->sharedSecret:[B

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->exchangeHash:[B

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;->hostKey:Ljava/security/PublicKey;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
