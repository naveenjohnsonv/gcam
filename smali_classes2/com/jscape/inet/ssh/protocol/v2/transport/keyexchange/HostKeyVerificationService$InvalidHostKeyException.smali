.class public Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$InvalidHostKeyException;
.super Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$OperationException;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final serialVersionUID:J = 0x12e62337970d8a1fL


# instance fields
.field public final host:Ljava/net/InetAddress;

.field public final key:Ljava/security/PublicKey;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "\u0017)jua\u007f\u000f~/sgy6\u0000;>2"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$InvalidHostKeyException;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x32

    goto :goto_1

    :cond_1
    const/16 v4, 0x4f

    goto :goto_1

    :cond_2
    const/16 v4, 0x54

    goto :goto_1

    :cond_3
    const/16 v4, 0x4d

    goto :goto_1

    :cond_4
    const/16 v4, 0x45

    goto :goto_1

    :cond_5
    const/16 v4, 0x1e

    goto :goto_1

    :cond_6
    const/4 v4, 0x7

    :goto_1
    const/16 v5, 0x59

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/net/InetAddress;Ljava/security/PublicKey;)V
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$InvalidHostKeyException;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$OperationException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$InvalidHostKeyException;->host:Ljava/net/InetAddress;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$InvalidHostKeyException;->key:Ljava/security/PublicKey;

    return-void
.end method
