.class public interface abstract Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;
.super Ljava/lang/Object;


# virtual methods
.method public abstract keyExchangeFor(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;
.end method

.method public abstract keyExchanges()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method
