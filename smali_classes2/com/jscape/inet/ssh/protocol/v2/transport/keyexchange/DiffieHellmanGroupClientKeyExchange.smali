.class public Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;
.super Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupKeyExchange;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;


# static fields
.field public static final DEFAULT_MAX:I

.field public static final DEFAULT_MIN:I

.field public static final DEFAULT_N:I

.field private static final a:I = 0x400

.field private static final b:I = 0x2000

.field private static final h:[Ljava/lang/String;


# instance fields
.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x6

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "#wy$G>\u0006#wy,Q>\u000c#wd7PuFk2fx\u0018\u0004#wzx"

    const/16 v5, 0x1f

    move v7, v0

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x29

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/16 v15, 0x16

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x4b

    const-string v4, "&]\u001b\u000e3\u001a+\u0011L_]=\u0012!DJ\u001eB!\r7J4 U\u0019H=\r\u000c\u0001P\u0013C5\u0006\u0003\u0016S\n^\u0017\u0004-\u0001R\u000be1\u0011\u0001\u001c_\u0017O:\u000f!DG\u0017O\'\u0000\u0005\u0008[\u0010\\=\u001c,\t\u0001X"

    move v8, v11

    move v7, v15

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x42

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->h:[Ljava/lang/String;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->bitLength()I

    move-result v0

    sput v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->DEFAULT_MIN:I

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_14:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->bitLength()I

    move-result v0

    sput v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->DEFAULT_N:I

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->GROUP_18:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroup;->p:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->bitLength()I

    move-result v0

    sput v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->DEFAULT_MAX:I

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v0, v14, 0x7

    if-eqz v0, :cond_8

    if-eq v0, v10, :cond_7

    const/4 v2, 0x2

    if-eq v0, v2, :cond_6

    const/4 v2, 0x3

    if-eq v0, v2, :cond_5

    const/4 v2, 0x4

    if-eq v0, v2, :cond_9

    const/4 v2, 0x5

    if-eq v0, v2, :cond_4

    const/4 v15, 0x6

    goto :goto_4

    :cond_4
    const/16 v15, 0x2a

    goto :goto_4

    :cond_5
    const/16 v15, 0x6c

    goto :goto_4

    :cond_6
    const/16 v15, 0x3d

    goto :goto_4

    :cond_7
    const/16 v15, 0x7e

    goto :goto_4

    :cond_8
    const/16 v15, 0x26

    :cond_9
    :goto_4
    xor-int v0, v9, v15

    xor-int v0, v16, v0

    int-to-char v0, v0

    aput-char v0, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v0, 0x6

    goto/16 :goto_2
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Ljava/lang/Object;IIIZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Ljava/lang/Object;)V

    const/16 p1, 0x400

    if-gt p1, p4, :cond_0

    if-gt p4, p5, :cond_0

    if-gt p5, p6, :cond_0

    const/16 p1, 0x2000

    if-gt p6, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    sget-object p2, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->h:[Ljava/lang/String;

    const/4 p3, 0x4

    aget-object p2, p2, p3

    invoke-static {p1, p2}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    iput p4, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->c:I

    iput p5, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->d:I

    iput p6, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->e:I

    iput-boolean p7, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->f:Z

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static defaultExchange(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;
    .locals 9

    new-instance v8, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;

    sget v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->DEFAULT_MIN:I

    sget v5, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->DEFAULT_N:I

    sget v6, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->DEFAULT_MAX:I

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Ljava/lang/Object;IIIZ)V

    return-object v8
.end method

.method public static defaultExchange(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;
    .locals 9

    new-instance v8, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;

    sget v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->DEFAULT_MIN:I

    sget v5, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->DEFAULT_N:I

    sget v6, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->DEFAULT_MAX:I

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;Ljava/lang/Object;IIIZ)V

    return-object v8
.end method

.method public static defaultExchange(Ljava/lang/String;Z)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->defaultExchange(Ljava/lang/String;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static defaultExchange(Ljava/lang/String;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;
    .locals 1

    invoke-static {p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyGroupExchangeMessages;->defaultMessages(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyGroupExchangeMessages;

    move-result-object v0

    invoke-static {v0, p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->defaultExchange(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;

    move-result-object p0

    return-object p0
.end method

.method public static defaultExchange(Ljava/lang/String;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;
    .locals 1

    invoke-static {p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyGroupExchangeMessages;->defaultMessages(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyGroupExchangeMessages;

    move-result-object v0

    invoke-static {v0, p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->defaultExchange(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/lang/String;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public exchangeKeys(Lcom/jscape/util/k/a/A;Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/A<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;",
            "Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;",
            ")",
            "Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v15, p0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    iget-object v2, v15, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->codecFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;

    iget-object v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->messageCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    invoke-interface {v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;->update(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v16

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexRequest;

    iget v3, v15, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->c:I

    iget v4, v15, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->d:I

    iget v5, v15, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->e:I

    invoke-direct {v2, v3, v4, v5}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexRequest;-><init>(III)V

    invoke-interface {v0, v2}, Lcom/jscape/util/k/a/A;->write(Ljava/lang/Object;)V

    invoke-interface/range {p1 .. p1}, Lcom/jscape/util/k/a/A;->read()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexGroup;

    iget-object v3, v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexGroup;->p:Ljava/math/BigInteger;

    iget-object v4, v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexGroup;->g:Ljava/math/BigInteger;

    invoke-virtual {v15, v3, v4}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->generateKeyPair(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/security/KeyPair;

    move-result-object v3

    invoke-virtual {v3}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v4

    check-cast v4, Ljavax/crypto/interfaces/DHPrivateKey;

    invoke-interface {v4}, Ljavax/crypto/interfaces/DHPrivateKey;->getX()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v3}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v3

    check-cast v3, Ljavax/crypto/interfaces/DHPublicKey;

    invoke-interface {v3}, Ljavax/crypto/interfaces/DHPublicKey;->getY()Ljava/math/BigInteger;

    move-result-object v13

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexInit;

    invoke-direct {v3, v13}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexInit;-><init>(Ljava/math/BigInteger;)V

    invoke-interface {v0, v3}, Lcom/jscape/util/k/a/A;->write(Ljava/lang/Object;)V

    invoke-interface/range {p1 .. p1}, Lcom/jscape/util/k/a/A;->read()Ljava/lang/Object;

    move-result-object v3

    move-object v14, v3

    check-cast v14, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;

    invoke-interface/range {p1 .. p1}, Lcom/jscape/util/k/a/A;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/util/k/TransportAddress;->inetAddress()Ljava/net/InetAddress;

    move-result-object v0

    iget-object v3, v14, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->hostKey:Ljava/security/PublicKey;

    move-object/from16 v5, p3

    invoke-interface {v5, v0, v3}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;->assertKeyValid(Ljava/net/InetAddress;Ljava/security/PublicKey;)V

    iget-object v0, v14, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->f:Ljava/math/BigInteger;

    iget-object v3, v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexGroup;->p:Ljava/math/BigInteger;

    invoke-virtual {v15, v4, v0, v3}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->computeSharedSecret(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)[B

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->hash()Lcom/jscape/a/f;

    move-result-object v12

    iget-object v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->clientIdentificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->serverIdentificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    iget-object v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->clientKexInitMessage:Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->serverKexInitMessage:Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    iget-object v7, v14, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->hostKeyBlob:[B

    iget v1, v15, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iget v1, v15, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iget v1, v15, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget-object v11, v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexGroup;->p:Ljava/math/BigInteger;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexGroup;->g:Ljava/math/BigInteger;

    iget-object v1, v14, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->f:Ljava/math/BigInteger;

    move-object/from16 v17, v1

    move-object/from16 v1, p0

    move-object/from16 v18, v2

    move-object v2, v12

    move-object/from16 v19, v12

    move-object/from16 v12, v18

    move-object/from16 v20, v14

    move-object/from16 v14, v17

    move-object v15, v0

    invoke-virtual/range {v1 .. v15}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->exchangeHashFor(Lcom/jscape/a/d;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;[BLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;[B)[B

    move-result-object v1

    if-nez v16, :cond_1

    move-object/from16 v2, p0

    :try_start_0
    iget-boolean v3, v2, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->f:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    move-object/from16 v3, v20

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_1
    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    move-object/from16 v2, p0

    :goto_0
    move-object/from16 v3, v20

    iget-object v4, v3, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->hostKey:Ljava/security/PublicKey;

    iget-object v5, v3, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->signature:Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    invoke-virtual {v2, v1, v4, v5}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->verifySignature([BLjava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V

    :goto_1
    :try_start_2
    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;

    iget-object v3, v3, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->hostKey:Ljava/security/PublicKey;

    move-object/from16 v5, v19

    invoke-direct {v4, v5, v0, v1, v3}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/KeyExchangeResult;-><init>(Lcom/jscape/a/d;[B[BLjava/security/PublicKey;)V

    if-eqz v16, :cond_2

    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    return-object v4

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->h:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->hashAlgorithm:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->provider:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected verifySignature([BLjava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->provider:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, 0x0

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->provider:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;->initSha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->provider:Ljava/lang/Object;

    instance-of v1, v0, Ljava/security/Provider;

    :cond_1
    if-eqz v1, :cond_2

    :try_start_2
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->provider:Ljava/lang/Object;

    check-cast v1, Ljava/security/Provider;

    invoke-static {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;->initSha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v2, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)V

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;->initSha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    move-result-object v0

    :goto_0
    invoke-interface {v0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;->assertSignatureValid([BLjava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V

    return-void

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method
