.class public Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static b:[I


# instance fields
.field public final clientKeyExchange:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;

.field public final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b([I)V

    const/16 v2, 0x14

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x5d

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "e\u007f \t\u001e|v=\u0014&\u001c2a{!>-\u0002\u0012$\r\u000c17\u0017\u000e9c\'>.\u0000J>"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x22

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    if-eqz v13, :cond_7

    if-eq v13, v8, :cond_6

    if-eq v13, v0, :cond_5

    const/4 v14, 0x3

    if-eq v13, v14, :cond_4

    const/4 v14, 0x4

    if-eq v13, v14, :cond_3

    const/4 v14, 0x5

    if-eq v13, v14, :cond_2

    const/16 v13, 0x45

    goto :goto_2

    :cond_2
    const/16 v13, 0x44

    goto :goto_2

    :cond_3
    const/16 v13, 0x2a

    goto :goto_2

    :cond_4
    const/16 v13, 0x38

    goto :goto_2

    :cond_5
    const/16 v13, 0x1e

    goto :goto_2

    :cond_6
    move v13, v0

    goto :goto_2

    :cond_7
    move v13, v2

    :goto_2
    xor-int/2addr v13, v7

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->name:Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->clientKeyExchange:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;

    return-void
.end method

.method public static b([I)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b:[I

    return-void
.end method

.method public static b()[I
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b:[I

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->clientKeyExchange:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
