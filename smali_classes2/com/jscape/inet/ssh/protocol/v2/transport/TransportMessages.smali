.class public Lcom/jscape/inet/ssh/protocol/v2/transport/TransportMessages;
.super Ljava/lang/Object;


# static fields
.field public static final ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgDisconnectCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgDisconnectCodec;-><init>()V

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDisconnect;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-direct {v1, v3, v2, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgIgnoreCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgIgnoreCodec;-><init>()V

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgIgnore;

    aput-object v5, v4, v6

    const/4 v5, 0x2

    invoke-direct {v1, v5, v2, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v3

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUnimplementedCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUnimplementedCodec;-><init>()V

    new-array v4, v3, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUnimplemented;

    aput-object v7, v4, v6

    const/4 v7, 0x3

    invoke-direct {v1, v7, v2, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgDebugCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgDebugCodec;-><init>()V

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDebug;

    aput-object v5, v4, v6

    const/4 v5, 0x4

    invoke-direct {v1, v5, v2, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgServiceRequestCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgServiceRequestCodec;-><init>()V

    new-array v4, v3, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceRequest;

    aput-object v7, v4, v6

    const/4 v7, 0x5

    invoke-direct {v1, v7, v2, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgServiceAcceptCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgServiceAcceptCodec;-><init>()V

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceAccept;

    aput-object v5, v4, v6

    const/4 v5, 0x6

    invoke-direct {v1, v5, v2, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexInitCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexInitCodec;-><init>()V

    new-array v4, v3, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    aput-object v7, v4, v6

    const/16 v7, 0x14

    invoke-direct {v1, v7, v2, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgNewKeysCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgNewKeysCodec;-><init>()V

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgNewKeys;

    aput-object v4, v3, v6

    const/16 v4, 0x15

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportMessages;->ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportMessages;->ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p0

    return-object p0
.end method
