.class public interface abstract Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;
.super Ljava/lang/Object;


# static fields
.field public static final NULL:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$1;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$1;-><init>()V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;->NULL:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    return-void
.end method


# virtual methods
.method public abstract assertKeyValid(Ljava/net/InetAddress;Ljava/security/PublicKey;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$OperationException;
        }
    .end annotation
.end method
