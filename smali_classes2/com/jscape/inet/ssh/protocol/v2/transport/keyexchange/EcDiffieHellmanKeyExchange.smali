.class public Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;
.super Ljava/lang/Object;


# static fields
.field protected static final ACCEPTABLE_COORDINATE_SIZE:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected static final KEY_ALGORITHM:Ljava/lang/String;

.field protected static final MAX_KEY_GENERATION_ATTEMPTS:I = 0x3e8

.field private static final a:[Ljava/lang/String;


# instance fields
.field protected final codecFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;

.field protected final curveIdentifier:Ljava/lang/String;

.field protected final curveOid:Ljava/lang/String;

.field protected final provider:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "V EsU.FI?GmY/XT FsZ#\"|\u00052\u001f:\u0011\u0002`\u0012/\u000cn\u001f\u0008`W+\u000cv\u001f\u0003.3\u0015Mq\u0013\u001e.\u0007<\u0004hX\u0004\"M3\u0015\u000cV Ds\\)DI>YnY"

    const/16 v4, 0x49

    const/16 v5, 0x13

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x6c

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x4

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v13, v10

    move v14, v2

    :goto_2
    const/4 v15, 0x5

    const/4 v1, 0x3

    if-gt v13, v14, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    if-eqz v12, :cond_1

    add-int/lit8 v1, v7, 0x1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v1

    goto :goto_0

    :cond_0
    const/16 v4, 0x11

    const/16 v3, 0xc

    const-string v5, ">H,\u001b4A,!V1\u00060\u0004J%[}"

    move v7, v1

    const/4 v6, -0x1

    move-object/from16 v17, v5

    move v5, v3

    move-object/from16 v3, v17

    goto :goto_3

    :cond_1
    add-int/lit8 v10, v7, 0x1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v5, v1

    move v7, v10

    :goto_3
    add-int/2addr v6, v9

    add-int v1, v6, v5

    invoke-virtual {v3, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v12, v2

    move v8, v11

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->a:[Ljava/lang/String;

    aget-object v3, v0, v15

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->KEY_ALGORITHM:Ljava/lang/String;

    new-array v3, v1, [Lcom/jscape/util/ap;

    aget-object v4, v0, v2

    const/16 v5, 0x20

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/jscape/util/ap;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;

    move-result-object v4

    aput-object v4, v3, v2

    aget-object v1, v0, v1

    const/16 v2, 0x30

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/jscape/util/ap;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;

    move-result-object v1

    aput-object v1, v3, v9

    aget-object v0, v0, v11

    const/16 v1, 0x42

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jscape/util/ap;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, v3, v1

    invoke-static {v3}, Lcom/jscape/util/G;->a([Lcom/jscape/util/ap;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->ACCEPTABLE_COORDINATE_SIZE:Ljava/util/Map;

    return-void

    :cond_3
    const/4 v2, 0x2

    aget-char v16, v10, v14

    rem-int/lit8 v15, v14, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    if-eq v15, v2, :cond_7

    if-eq v15, v1, :cond_6

    if-eq v15, v11, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x1a

    goto :goto_4

    :cond_4
    const/16 v1, 0x76

    goto :goto_4

    :cond_5
    move v1, v9

    goto :goto_4

    :cond_6
    const/16 v1, 0x31

    goto :goto_4

    :cond_7
    const/16 v1, 0x1b

    goto :goto_4

    :cond_8
    const/16 v1, 0x62

    goto :goto_4

    :cond_9
    const/16 v1, 0xb

    :goto_4
    xor-int/2addr v1, v8

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v10, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v2, 0x0

    goto/16 :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->curveIdentifier:Ljava/lang/String;

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->oidFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->curveOid:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->curveIdentifier:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/lang/String;Ljava/lang/Object;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchangeMessages;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->codecFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;

    return-void
.end method

.method private a(Ljava/security/KeyPair;)Z
    .locals 10

    invoke-virtual {p1}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object p1

    check-cast p1, Ljava/security/interfaces/ECPublicKey;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v0

    invoke-interface {p1}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object v1

    sget-object v2, Ljava/security/spec/ECPoint;->POINT_INFINITY:Ljava/security/spec/ECPoint;

    invoke-virtual {v1, v2}, Ljava/security/spec/ECPoint;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x0

    if-nez v0, :cond_9

    if-eqz v2, :cond_0

    move v2, v3

    goto/16 :goto_2

    :cond_0
    invoke-interface {p1}, Ljava/security/interfaces/ECPublicKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object p1

    invoke-virtual {p1}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object p1

    invoke-virtual {p1}, Ljava/security/spec/EllipticCurve;->getField()Ljava/security/spec/ECField;

    move-result-object v2

    check-cast v2, Ljava/security/spec/ECFieldFp;

    invoke-virtual {v2}, Ljava/security/spec/ECFieldFp;->getP()Ljava/math/BigInteger;

    move-result-object v2

    sget-object v4, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v2, v4}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v1}, Ljava/security/spec/ECPoint;->getAffineX()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v5}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->removeLeadingZeroes([B)[B

    move-result-object v5

    new-instance v6, Ljava/math/BigInteger;

    const/4 v7, 0x1

    invoke-direct {v6, v7, v5}, Ljava/math/BigInteger;-><init>(I[B)V

    invoke-virtual {v1}, Ljava/security/spec/ECPoint;->getAffineY()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->removeLeadingZeroes([B)[B

    move-result-object v1

    new-instance v8, Ljava/math/BigInteger;

    invoke-direct {v8, v7, v1}, Ljava/math/BigInteger;-><init>(I[B)V

    array-length v7, v5

    if-nez v0, :cond_7

    array-length v1, v1

    if-ne v7, v1, :cond_8

    array-length v1, v5

    if-nez v0, :cond_2

    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->ACCEPTABLE_COORDINATE_SIZE:Ljava/util/Map;

    iget-object v9, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->curveOid:Ljava/lang/String;

    array-length v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v7, v9, v5}, Ljava/util/Map;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-eq v1, v5, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v6, v4}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v1

    :cond_2
    if-nez v0, :cond_5

    if-gtz v1, :cond_6

    invoke-virtual {v8, v4}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v1

    if-nez v0, :cond_4

    if-lez v1, :cond_3

    goto :goto_0

    :cond_3
    sget-object v0, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    sget-object v1, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v8, v0, v2}, Ljava/math/BigInteger;->modPow(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p1}, Ljava/security/spec/EllipticCurve;->getA()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {p1}, Ljava/security/spec/EllipticCurve;->getB()Ljava/math/BigInteger;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p1

    sget-object v1, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    sget-object v3, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v1, v3}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    sget-object v3, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v1, v3}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v6, v1, v2}, Ljava/math/BigInteger;->modPow(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p1

    invoke-virtual {p1, v2}, Ljava/math/BigInteger;->mod(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v1

    :cond_4
    return v1

    :cond_5
    move v3, v1

    :cond_6
    :goto_0
    return v3

    :cond_7
    move v3, v7

    :cond_8
    :goto_1
    return v3

    :cond_9
    :goto_2
    return v2
.end method

.method private static b(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method protected computeSharedSecret(Ljava/security/KeyPair;Ljava/security/PublicKey;)[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, 0x5

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->a:[Ljava/lang/String;

    aget-object v0, v0, v2

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Ljavax/crypto/KeyAgreement;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/KeyAgreement;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    instance-of v1, v0, Ljava/security/Provider;

    :cond_1
    if-eqz v1, :cond_2

    :try_start_2
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->a:[Ljava/lang/String;

    aget-object v0, v0, v2

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    check-cast v1, Ljava/security/Provider;

    invoke-static {v0, v1}, Ljavax/crypto/KeyAgreement;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/KeyAgreement;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->a:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-static {v0}, Ljavax/crypto/KeyAgreement;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyAgreement;

    move-result-object v0

    :goto_0
    invoke-virtual {p1}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljavax/crypto/KeyAgreement;->init(Ljava/security/Key;)V

    const/4 p1, 0x1

    invoke-virtual {v0, p2, p1}, Ljavax/crypto/KeyAgreement;->doPhase(Ljava/security/Key;Z)Ljava/security/Key;

    invoke-virtual {v0}, Ljavax/crypto/KeyAgreement;->generateSecret()[B

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->removeLeadingZeroes([B)[B

    move-result-object p2

    new-instance v0, Ljava/math/BigInteger;

    invoke-direct {v0, p1, p2}, Ljava/math/BigInteger;-><init>(I[B)V

    new-instance p1, Lcom/jscape/util/h/o;

    invoke-direct {p1}, Lcom/jscape/util/h/o;-><init>()V

    invoke-static {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->writeValue(Ljava/math/BigInteger;Ljava/io/OutputStream;)V

    invoke-virtual {p1}, Lcom/jscape/util/h/o;->d()[B

    move-result-object p1

    return-object p1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method protected exchangeHashFor(Lcom/jscape/a/d;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;[BLjava/security/PublicKey;Ljava/security/PublicKey;[B)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/o;

    invoke-direct {v0}, Lcom/jscape/util/h/o;-><init>()V

    iget-object p2, p2, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->rawData:[B

    invoke-static {p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    iget-object p2, p3, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->rawData:[B

    invoke-static {p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    iget-object p2, p4, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->rawData:[B

    invoke-static {p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    iget-object p2, p5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->rawData:[B

    invoke-static {p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    invoke-static {p6, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    invoke-static {p7, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->writeValue(Ljava/security/PublicKey;Ljava/io/OutputStream;)V

    invoke-static {p8, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->writeValue(Ljava/security/PublicKey;Ljava/io/OutputStream;)V

    invoke-virtual {v0, p9}, Lcom/jscape/util/h/o;->write([B)V

    invoke-interface {p1}, Lcom/jscape/a/d;->b()Lcom/jscape/a/d;

    move-result-object p1

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object p2

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result p3

    const/4 p4, 0x0

    invoke-interface {p1, p2, p4, p3}, Lcom/jscape/a/d;->a([BII)Lcom/jscape/a/d;

    move-result-object p1

    invoke-interface {p1}, Lcom/jscape/a/d;->c()[B

    move-result-object p1

    return-object p1
.end method

.method protected generateKeyPair()Ljava/security/KeyPair;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->a:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    instance-of v1, v1, Ljava/security/Provider;

    :cond_1
    const/4 v2, 0x5

    if-eqz v1, :cond_2

    :try_start_2
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->a:[Ljava/lang/String;

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    check-cast v2, Ljava/security/Provider;

    invoke-static {v1, v2}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyPairGenerator;

    move-result-object v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_2
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->a:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    move-result-object v1

    :goto_0
    new-instance v2, Ljava/security/spec/ECGenParameterSpec;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->curveOid:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/security/spec/ECGenParameterSpec;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/security/KeyPairGenerator;->initialize(Ljava/security/spec/AlgorithmParameterSpec;)V

    const/4 v2, 0x0

    :goto_1
    const/16 v3, 0x3e8

    if-ge v2, v3, :cond_5

    invoke-virtual {v1}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;

    move-result-object v3

    if-nez v0, :cond_4

    :try_start_3
    invoke-direct {p0, v3}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->a(Ljava/security/KeyPair;)Z

    move-result v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    if-eqz v4, :cond_3

    return-object v3

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :catch_1
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_4
    :goto_2
    if-nez v0, :cond_5

    goto :goto_1

    :cond_5
    new-instance v0, Ljava/lang/Exception;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_3
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method protected hashFor(Ljava/security/PublicKey;)Lcom/jscape/a/f;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->hashAlgorithmFor(Ljava/security/PublicKey;)Ljava/lang/String;

    move-result-object p1

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/jscape/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/a/f;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    instance-of v1, v0, Ljava/security/Provider;

    :cond_1
    if-eqz v1, :cond_2

    :try_start_2
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    check-cast v0, Ljava/security/Provider;

    invoke-static {p1, v0}, Lcom/jscape/a/f;->a(Ljava/lang/String;Ljava/security/Provider;)Lcom/jscape/a/f;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    const/4 v0, 0x0

    check-cast v0, Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/jscape/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/a/f;

    move-result-object p1

    :goto_0
    return-object p1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method protected hostKeyBlobFor(Ljava/security/PublicKey;)[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v0

    new-instance v1, Lcom/jscape/util/h/o;

    invoke-direct {v1}, Lcom/jscape/util/h/o;-><init>()V

    :try_start_0
    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    instance-of v2, v2, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v3, 0x0

    if-nez v0, :cond_1

    if-eqz v2, :cond_0

    :try_start_1
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    new-array v2, v3, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)V

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    instance-of v2, v0, Ljava/security/Provider;

    :cond_1
    if-eqz v2, :cond_2

    :try_start_2
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    new-array v2, v3, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)V

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    check-cast v2, Ljava/security/Provider;

    invoke-static {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    new-array v2, v3, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)V

    const/4 v2, 0x0

    check-cast v2, Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object v0

    :goto_0
    invoke-interface {v0, p1, v1}, Lcom/jscape/util/h/I;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->d()[B

    move-result-object p1

    return-object p1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method protected removeLeadingZeroes([B)[B
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    aget-byte v2, p1, v1

    if-nez v2, :cond_1

    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_2

    if-eqz v0, :cond_0

    :cond_1
    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    array-length v0, p1

    sub-int/2addr v0, v1

    new-array v0, v0, [B

    invoke-static {p1, v1, v0}, Lcom/jscape/util/v;->a([BI[B)[B

    move-result-object p1

    :goto_0
    return-object p1
.end method
