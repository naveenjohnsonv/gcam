.class final Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$6;
.super Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    return-void
.end method


# virtual methods
.method public update(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$6;->name:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group15Sha512(ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p2

    invoke-direct {v2, v3, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    const/4 p2, 0x0

    aput-object v2, v1, p2

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p1

    return-object p1
.end method

.method public update(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$6;->name:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group15Sha512(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object p2

    invoke-direct {v2, v3, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    const/4 p2, 0x0

    aput-object v2, v1, p2

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p1

    return-object p1
.end method
