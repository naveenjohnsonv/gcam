.class public abstract Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;
.super Ljava/lang/Object;


# static fields
.field protected static final KEY_ALGORITHM:Ljava/lang/String;

.field protected static final MAX_KEY_GENERATION_ATTEMPTS:I = 0x3e8

.field private static final a:[Ljava/lang/String;


# instance fields
.field protected final codecFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;

.field protected final g:Ljava/math/BigInteger;

.field protected final hashAlgorithm:Ljava/lang/String;

.field protected final p:Ljava/math/BigInteger;

.field protected final provider:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x4

    const/4 v3, 0x0

    const-string v4, "A#2\u000c\u0004A#%\u000c\u0011A#=P\u0008\u0019f\u0001d:C\u0012\u0005O\u0000>r\u0002)K#(q\'^\tQ@\u0008m0C\u001a\u0005N\u0002muG\u001a\u001dN\t#\u0011y[\u001aB\u0014#%P\u0012\u0003\t\')j3W\u0012\u0014o\u0008o9\\\u001a\u001fl\u0008z\u0010I\u0018\u0019F\u0003d0\u0011\u0000\u0012H\tf6w\u001a\u0012S\u0002q,\u000c"

    const/16 v5, 0x6a

    move v7, v1

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x6f

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x2

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0xe

    const-string v4, "e\u0007\u000b\roi\u000fXK\u0002E*k@"

    move v8, v11

    move v7, v15

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x23

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->a:[Ljava/lang/String;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->KEY_ALGORITHM:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    if-eq v2, v15, :cond_7

    const/4 v15, 0x3

    if-eq v2, v15, :cond_6

    if-eq v2, v1, :cond_5

    const/4 v15, 0x5

    if-eq v2, v15, :cond_4

    const/16 v15, 0x48

    goto :goto_4

    :cond_4
    const/16 v15, 0x1e

    goto :goto_4

    :cond_5
    const/16 v15, 0x14

    goto :goto_4

    :cond_6
    const/16 v15, 0x5e

    goto :goto_4

    :cond_7
    const/16 v15, 0x3a

    goto :goto_4

    :cond_8
    const/16 v15, 0x6c

    :cond_9
    :goto_4
    xor-int v2, v9, v15

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->codecFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->p:Ljava/math/BigInteger;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->g:Ljava/math/BigInteger;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->hashAlgorithm:Ljava/lang/String;

    iput-object p5, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    return-void
.end method

.method private static b(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method protected computeSharedSecret(Ljava/math/BigInteger;Ljava/math/BigInteger;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->p:Ljava/math/BigInteger;

    invoke-virtual {p2, p1, v0}, Ljava/math/BigInteger;->modPow(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p1

    new-instance p2, Lcom/jscape/util/h/o;

    invoke-direct {p2}, Lcom/jscape/util/h/o;-><init>()V

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->writeValue(Ljava/math/BigInteger;Ljava/io/OutputStream;)V

    invoke-virtual {p2}, Lcom/jscape/util/h/o;->d()[B

    move-result-object p1

    return-object p1
.end method

.method protected exchangeHashFor(Lcom/jscape/a/d;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;[BLjava/math/BigInteger;Ljava/math/BigInteger;[B)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/o;

    invoke-direct {v0}, Lcom/jscape/util/h/o;-><init>()V

    iget-object p2, p2, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->rawData:[B

    invoke-static {p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    iget-object p2, p3, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->rawData:[B

    invoke-static {p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    iget-object p2, p4, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->rawData:[B

    invoke-static {p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    iget-object p2, p5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->rawData:[B

    invoke-static {p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    invoke-static {p6, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    invoke-static {p7, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->writeValue(Ljava/math/BigInteger;Ljava/io/OutputStream;)V

    invoke-static {p8, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->writeValue(Ljava/math/BigInteger;Ljava/io/OutputStream;)V

    invoke-virtual {v0, p9}, Lcom/jscape/util/h/o;->write([B)V

    invoke-interface {p1}, Lcom/jscape/a/d;->b()Lcom/jscape/a/d;

    move-result-object p1

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object p2

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result p3

    const/4 p4, 0x0

    invoke-interface {p1, p2, p4, p3}, Lcom/jscape/a/d;->a([BII)Lcom/jscape/a/d;

    move-result-object p1

    invoke-interface {p1}, Lcom/jscape/a/d;->c()[B

    move-result-object p1

    return-object p1
.end method

.method protected generateKeyPair()Ljava/security/KeyPair;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->a:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    instance-of v1, v1, Ljava/security/Provider;

    :cond_1
    const/4 v2, 0x6

    if-eqz v1, :cond_2

    :try_start_2
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->a:[Ljava/lang/String;

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    check-cast v2, Ljava/security/Provider;

    invoke-static {v1, v2}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyPairGenerator;

    move-result-object v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_2
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->a:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    move-result-object v1

    :goto_0
    new-instance v2, Ljavax/crypto/spec/DHParameterSpec;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->p:Ljava/math/BigInteger;

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->g:Ljava/math/BigInteger;

    invoke-direct {v2, v3, v4}, Ljavax/crypto/spec/DHParameterSpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    invoke-virtual {v1, v2}, Ljava/security/KeyPairGenerator;->initialize(Ljava/security/spec/AlgorithmParameterSpec;)V

    const/4 v2, 0x0

    :goto_1
    const/16 v3, 0x3e8

    if-ge v2, v3, :cond_5

    invoke-virtual {v1}, Ljava/security/KeyPairGenerator;->genKeyPair()Ljava/security/KeyPair;

    move-result-object v3

    if-nez v0, :cond_4

    :try_start_3
    invoke-virtual {p0, v3}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->keyPairValid(Ljava/security/KeyPair;)Z

    move-result v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    if-eqz v4, :cond_3

    return-object v3

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :catch_1
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_4
    :goto_2
    if-nez v0, :cond_5

    goto :goto_1

    :cond_5
    new-instance v0, Ljava/lang/Exception;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->a:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_3
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method protected hash()Lcom/jscape/a/f;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->hashAlgorithm:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/jscape/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/a/f;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_1

    :cond_0
    if-nez v0, :cond_2

    :try_start_2
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    instance-of v1, v0, Ljava/security/Provider;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    :try_start_3
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->hashAlgorithm:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    check-cast v1, Ljava/security/Provider;

    invoke-static {v0, v1}, Lcom/jscape/a/f;->a(Ljava/lang/String;Ljava/security/Provider;)Lcom/jscape/a/f;

    move-result-object v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->hashAlgorithm:Ljava/lang/String;

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/jscape/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/a/f;

    move-result-object v0

    :goto_1
    return-object v0

    :catch_2
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method protected hostKeyBlobFor(Ljava/security/PublicKey;)[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v0

    new-instance v1, Lcom/jscape/util/h/o;

    invoke-direct {v1}, Lcom/jscape/util/h/o;-><init>()V

    :try_start_0
    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    instance-of v2, v2, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v3, 0x0

    if-nez v0, :cond_1

    if-eqz v2, :cond_0

    :try_start_1
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    new-array v2, v3, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)V

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    instance-of v2, v0, Ljava/security/Provider;

    :cond_1
    if-eqz v2, :cond_2

    :try_start_2
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    new-array v2, v3, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)V

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    check-cast v2, Ljava/security/Provider;

    invoke-static {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    new-array v2, v3, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)V

    const/4 v2, 0x0

    check-cast v2, Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object v0

    :goto_0
    invoke-interface {v0, p1, v1}, Lcom/jscape/util/h/I;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->d()[B

    move-result-object p1

    return-object p1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method protected keyPairValid(Ljava/security/KeyPair;)Z
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v0

    invoke-virtual {p1}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object p1

    check-cast p1, Ljavax/crypto/interfaces/DHPublicKey;

    invoke-interface {p1}, Ljavax/crypto/interfaces/DHPublicKey;->getY()Ljava/math/BigInteger;

    move-result-object p1

    sget-object v1, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {p1, v1}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v1

    if-nez v0, :cond_0

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->p:Ljava/math/BigInteger;

    sget-object v2, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v1, v2}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v1

    :cond_0
    if-nez v0, :cond_2

    if-gez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move p1, v1

    :goto_1
    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->a:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->codecFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->p:Ljava/math/BigInteger;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->g:Ljava/math/BigInteger;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->hashAlgorithm:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchange;->provider:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
