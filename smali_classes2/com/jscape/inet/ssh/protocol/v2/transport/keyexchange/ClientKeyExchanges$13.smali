.class final Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$13;
.super Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;


# static fields
.field private static final c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x6

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x1

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    const-string v7, "\u00106z\u0008h`\u0006\u00106z\u0008h`"

    invoke-virtual {v7, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v8, v4

    move v9, v2

    :goto_1
    const/16 v10, 0xd

    if-gt v8, v9, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    if-ge v3, v10, :cond_0

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$13;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v9

    rem-int/lit8 v12, v9, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v6, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x3a

    goto :goto_2

    :cond_2
    const/16 v12, 0x5b

    goto :goto_2

    :cond_3
    const/16 v12, 0x50

    goto :goto_2

    :cond_4
    const/16 v12, 0x37

    goto :goto_2

    :cond_5
    const/16 v12, 0x36

    goto :goto_2

    :cond_6
    const/16 v12, 0x73

    goto :goto_2

    :cond_7
    const/16 v12, 0x4e

    :goto_2
    xor-int/2addr v10, v12

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v4, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method constructor <init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    return-void
.end method


# virtual methods
.method public update(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 5

    const/4 v0, 0x1

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$13;->name:Ljava/lang/String;

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$13;->c:[Ljava/lang/String;

    aget-object v0, v4, v0

    const/4 v4, 0x0

    invoke-static {v0, v4, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->defaultExchange(Ljava/lang/String;ZLjava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;

    move-result-object p2

    invoke-direct {v2, v3, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    aput-object v2, v1, v4

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p1

    return-object p1
.end method

.method public update(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$13;->name:Ljava/lang/String;

    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$13;->c:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-static {v3, v4, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->defaultExchange(Ljava/lang/String;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;

    move-result-object p2

    invoke-direct {v1, v2, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    aput-object v1, v0, v4

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p1

    return-object p1
.end method
