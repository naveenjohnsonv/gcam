.class public Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;
.super Ljava/lang/Object;


# static fields
.field public static final ALL:[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final ALL_DISABLED_SIGNATURE_VERIFICATION:[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final DH_GROUP14_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final DH_GROUP14_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final DH_GROUP14_SHA256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final DH_GROUP15_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final DH_GROUP16_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final DH_GROUP17_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final DH_GROUP18_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final DH_GROUP1_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final DH_GROUP1_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final DH_GROUP_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final DH_GROUP_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final DH_GROUP_SHA256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final DH_GROUP_SHA256_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final ECDH_SHA2_NIST_B_233:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final ECDH_SHA2_NIST_B_409:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final ECDH_SHA2_NIST_K_163:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final ECDH_SHA2_NIST_K_233:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final ECDH_SHA2_NIST_K_283:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final ECDH_SHA2_NIST_K_409:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final ECDH_SHA2_NIST_P_192:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final ECDH_SHA2_NIST_P_224:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final ECDH_SHA2_NIST_P_256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final ECDH_SHA2_NIST_P_384:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final ECDH_SHA2_NIST_P_521:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

.field public static final ECDH_SHA2_NIST_T_571:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x29

    new-array v0, v0, [Ljava/lang/String;

    const/16 v1, 0x24

    const/4 v3, 0x0

    const-string v4, "Or\\Nh \rC~VDl$N\u0006|HGt5\rNcY@`+GN6I@`w\u0015\u001d\u0012Nx^@,6HJ)\u0017Fh6T[.\u0008\u0019\u001aOr\\Nh \rC~VDl$N\u0006|HGt5\u0011\u0006hRI0\u000b\u001a5\t\u00060v\u0012\u0005+\u0014\u0019\u0016Nx^@,6HJ)\u0017\u0019/v\u000e\u001a(\u0008\u00061k\u0013\u001d\u0004xS{\u0019\u0008ErI\\qv\u0018\u001f\u0008ErI\\qw\u0015\u001d\u000c\u001a5\t\u00060v\u0012\u0005+\u0014\u001b7\u001aOr\\Nh \rC~VDl$N\u0006|HGt5\u0011\u0006hRI0\u001dOr\\Nh \rC~VDl$N\u0006|HGt5\u0011\u00136I@`p\u0011\u0019\u0016Nx^@,6HJ)\u0017\u0019/v\u000e\u001a(\u0008\u00061k\u0012\u001d\"Or\\Nh \rC~VDl$N\u0006|HGt5\rNcY@`+GN6I@`t\u0012Nx^@,6HJ)\u0017Fh6T[)\u000f\u001e\"Or\\Nh \rC~VDl$N\u0006|HGt5\rNcY@`+GN6I@`t\u0016Nx^@,6HJ)\u0017\u0019/v\u000e\u001a(\u0008\u00061k\u0013\u001c\u000c\u001a5\t\u00060v\u0012\u0005+\u0014\u00197\u001dOr\\Nh \rC~VDl$N\u0006|HGt5\u0011\u001c6I@`p\u0011\u0019\u0016Nx^@,6HJ)\u0017\u0019/v\u000e\u001a(\u0008\u00061k\u0013\u0013\u0015Nx^@,6HJ)\u0017\u0019/v\u000e\u001a(\u0008\u00061k\u0011\u0004xS{\u0019\u001bOr\\Nh \rC~VDl$N\u0006|HGt5\u0011\u001f6I@`t$Or\\Nh \rC~VDl$N\u0006|HGt5\rNcY@`+GN6I@`w\u0015\u001d\u001bOr\\Nh \rC~VDl$N\u0006|HGt5\u0011\u001f6I@`t\u000c\u001a5\t\u00060v\u0012\u0005+\u0014\u001a6\u0008ErI\\qp\u0012\u001a\u000c\u001a5\t\u00060v\u0012\u0005+\u0014\u001b2\u000c\u001a5\t\u00060v\u0012\u0005+\u0014\u001a7\u001dOr\\Nh \rC~VDl$N\u0006|HGt5\u0011\u001f6I@`w\u0015\u001d\u0016Nx^@,6HJ)\u0017\u0019/v\u000e\u001a(\u0008\u00061k\u0011\u001d\u000c\u001a5\t\u00060v\u0012\u0005+\u0014\u001b9\u001dOr\\Nh \rC~VDl$N\u0006|HGt5\u0011\u001e6I@`p\u0011\u0019\u000c\u001a5\t\u00060v\u0012\u0005+\u0014\u001b6\u001dNx^@,6HJ)\u0017\u0019/w\u000e\u0013/\n\u00060u\u0010\u001f.\u0014\u001b/t\u000e\u001a\u0006xS{\u001a4s\u0013\u001a5\u0008\u00069q\u0010\u0005*\n\u00185p\u000e\u00185\u000b\u00060\u0006xS{\u001a4s\u0012Nx^@,6HJ)\u0017Fh6T[(\u0002\u001c\u0016Nx^@,6HJ)\u0017\u0019/v\u000e\u001a(\u0008\u00061k\u0012\u001c"

    const/16 v5, 0x31a

    move v7, v1

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x70

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x5

    const/4 v2, 0x2

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    const/16 v11, 0x1d

    if-eqz v12, :cond_1

    add-int/lit8 v2, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v2

    goto :goto_0

    :cond_0
    const/16 v5, 0x34

    const-string v4, "\\aO]{3\u001ePmEW\u007f7]\u0015o[Tg&\u0002\u000e%ZSsc\u0002\n\u0016]kMS?%[Y:\u0004\n<e\u001d\t;\u001b\u0015\"x\u0000\u000b"

    move v8, v2

    move v7, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v7, v2

    move v8, v12

    :goto_3
    const/16 v9, 0x63

    add-int/2addr v6, v10

    add-int v2, v6, v7

    invoke-virtual {v4, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$1;

    const/16 v5, 0x9

    aget-object v6, v0, v5

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v7

    invoke-static {v10, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group1Sha1(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$1;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP1_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$2;

    aget-object v6, v0, v2

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group1Sha1(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$2;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP1_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$3;

    const/16 v6, 0x17

    aget-object v6, v0, v6

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v7

    invoke-static {v10, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group14Sha1(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$3;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP14_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$4;

    const/16 v6, 0x15

    aget-object v7, v0, v6

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v8

    invoke-static {v3, v8}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group14Sha1(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object v8

    invoke-direct {v4, v7, v8}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$4;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP14_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$5;

    const/16 v7, 0x1c

    aget-object v7, v0, v7

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v8

    invoke-static {v10, v8}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group14Sha256(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object v8

    invoke-direct {v4, v7, v8}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$5;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP14_SHA256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$6;

    const/16 v7, 0x1f

    aget-object v7, v0, v7

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v8

    invoke-static {v10, v8}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group15Sha512(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object v8

    invoke-direct {v4, v7, v8}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$6;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP15_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$7;

    const/16 v7, 0x27

    aget-object v7, v0, v7

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v8

    invoke-static {v10, v8}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group16Sha512(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object v8

    invoke-direct {v4, v7, v8}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$7;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP16_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$8;

    const/16 v7, 0x11

    aget-object v8, v0, v7

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v9

    invoke-static {v10, v9}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group17Sha512(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object v9

    invoke-direct {v4, v8, v9}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$8;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP17_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$9;

    const/16 v8, 0xa

    aget-object v9, v0, v8

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v12

    invoke-static {v10, v12}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;->group18Sha512(ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanClientKeyExchange;

    move-result-object v12

    invoke-direct {v4, v9, v12}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$9;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP18_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$10;

    const/16 v9, 0xc

    aget-object v12, v0, v9

    const/16 v13, 0x14

    aget-object v14, v0, v13

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v13

    invoke-static {v14, v10, v13}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->defaultExchange(Ljava/lang/String;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;

    move-result-object v13

    invoke-direct {v4, v12, v13}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$10;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$11;

    const/16 v12, 0xe

    aget-object v13, v0, v12

    aget-object v14, v0, v15

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v7

    invoke-static {v14, v3, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->defaultExchange(Ljava/lang/String;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;

    move-result-object v7

    invoke-direct {v4, v13, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$11;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$12;

    aget-object v7, v0, v3

    aget-object v1, v0, v1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v13

    invoke-static {v1, v10, v13}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->defaultExchange(Ljava/lang/String;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;

    move-result-object v1

    invoke-direct {v4, v7, v1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$12;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$13;

    const/16 v4, 0x16

    aget-object v4, v0, v4

    const/16 v7, 0x22

    aget-object v7, v0, v7

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v13

    invoke-static {v7, v3, v13}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;->defaultExchange(Ljava/lang/String;ZLjava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanGroupClientKeyExchange;

    move-result-object v7

    invoke-direct {v1, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$13;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA256_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$14;

    const/16 v4, 0xd

    aget-object v7, v0, v4

    new-instance v13, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;

    const/4 v14, 0x7

    aget-object v12, v0, v14

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v4

    invoke-direct {v13, v12, v10, v4}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;-><init>(Ljava/lang/String;ZLjava/lang/Object;)V

    invoke-direct {v1, v7, v13}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$14;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$15;

    const/16 v4, 0x25

    aget-object v4, v0, v4

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;

    const/4 v12, 0x6

    aget-object v13, v0, v12

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v9

    invoke-direct {v7, v13, v10, v9}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;-><init>(Ljava/lang/String;ZLjava/lang/Object;)V

    invoke-direct {v1, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$15;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_384:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$16;

    aget-object v4, v0, v10

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;

    const/16 v9, 0x19

    aget-object v9, v0, v9

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v13

    invoke-direct {v7, v9, v10, v13}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;-><init>(Ljava/lang/String;ZLjava/lang/Object;)V

    invoke-direct {v1, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$16;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_521:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$17;

    const/16 v4, 0x13

    aget-object v7, v0, v4

    new-instance v9, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;

    const/4 v13, 0x3

    aget-object v4, v0, v13

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v13

    invoke-direct {v9, v4, v10, v13}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;-><init>(Ljava/lang/String;ZLjava/lang/Object;)V

    invoke-direct {v1, v7, v9}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$17;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_163:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$18;

    const/16 v4, 0x21

    aget-object v4, v0, v4

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;

    const/16 v9, 0x23

    aget-object v9, v0, v9

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v13

    invoke-direct {v7, v9, v10, v13}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;-><init>(Ljava/lang/String;ZLjava/lang/Object;)V

    invoke-direct {v1, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$18;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_192:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$19;

    const/16 v4, 0x28

    aget-object v4, v0, v4

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;

    const/16 v9, 0x1a

    aget-object v9, v0, v9

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v13

    invoke-direct {v7, v9, v10, v13}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;-><init>(Ljava/lang/String;ZLjava/lang/Object;)V

    invoke-direct {v1, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$19;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_224:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$20;

    const/16 v4, 0xb

    aget-object v7, v0, v4

    new-instance v9, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;

    const/16 v13, 0x1b

    aget-object v13, v0, v13

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v4

    invoke-direct {v9, v13, v10, v4}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;-><init>(Ljava/lang/String;ZLjava/lang/Object;)V

    invoke-direct {v1, v7, v9}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$20;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_233:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$21;

    const/16 v4, 0x26

    aget-object v4, v0, v4

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;

    const/16 v9, 0x18

    aget-object v9, v0, v9

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v13

    invoke-direct {v7, v9, v10, v13}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;-><init>(Ljava/lang/String;ZLjava/lang/Object;)V

    invoke-direct {v1, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$21;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_B_233:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$22;

    aget-object v4, v0, v11

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;

    const/16 v9, 0x10

    aget-object v11, v0, v9

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v13

    invoke-direct {v7, v11, v10, v13}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;-><init>(Ljava/lang/String;ZLjava/lang/Object;)V

    invoke-direct {v1, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$22;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_283:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$23;

    const/4 v4, 0x4

    aget-object v7, v0, v4

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;

    const/16 v11, 0x8

    aget-object v11, v0, v11

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v13

    invoke-direct {v4, v11, v10, v13}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;-><init>(Ljava/lang/String;ZLjava/lang/Object;)V

    invoke-direct {v1, v7, v4}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$23;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_409:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$24;

    const/16 v4, 0xf

    aget-object v4, v0, v4

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;

    const/16 v11, 0x20

    aget-object v11, v0, v11

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v13

    invoke-direct {v7, v11, v10, v13}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;-><init>(Ljava/lang/String;ZLjava/lang/Object;)V

    invoke-direct {v1, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$24;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_B_409:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$25;

    const/16 v4, 0x12

    aget-object v4, v0, v4

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;

    const/16 v11, 0x1e

    aget-object v0, v0, v11

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v11

    invoke-direct {v7, v0, v10, v11}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/EcDiffieHellmanClientKeyExchange;-><init>(Ljava/lang/String;ZLjava/lang/Object;)V

    invoke-direct {v1, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$25;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchange;)V

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_T_571:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    new-array v0, v6, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP1_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    aput-object v4, v0, v3

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP14_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    aput-object v4, v0, v10

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP14_SHA256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    aput-object v4, v0, v2

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP15_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v6, 0x3

    aput-object v4, v0, v6

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP16_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v6, 0x4

    aput-object v4, v0, v6

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP17_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    aput-object v4, v0, v15

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP18_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    aput-object v4, v0, v12

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    aput-object v4, v0, v14

    const/16 v4, 0x8

    sget-object v6, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    aput-object v6, v0, v4

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    aput-object v4, v0, v5

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_384:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    aput-object v4, v0, v8

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_521:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v5, 0xb

    aput-object v4, v0, v5

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_163:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v5, 0xc

    aput-object v4, v0, v5

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_192:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v5, 0xd

    aput-object v4, v0, v5

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_224:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v5, 0xe

    aput-object v4, v0, v5

    const/16 v4, 0xf

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_233:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    aput-object v5, v0, v4

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_B_233:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    aput-object v4, v0, v9

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_283:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v5, 0x11

    aput-object v4, v0, v5

    const/16 v4, 0x12

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_409:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    aput-object v5, v0, v4

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_B_409:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v5, 0x13

    aput-object v4, v0, v5

    const/16 v4, 0x14

    aput-object v1, v0, v4

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ALL:[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP1_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    aput-object v1, v0, v3

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP14_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    aput-object v1, v0, v10

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA256_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ALL_DISABLED_SIGNATURE_VERIFICATION:[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    return-void

    :cond_3
    const/4 v1, 0x3

    aget-char v16, v11, v14

    rem-int/lit8 v3, v14, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v10, :cond_8

    if-eq v3, v2, :cond_7

    if-eq v3, v1, :cond_6

    const/4 v1, 0x4

    if-eq v3, v1, :cond_5

    if-eq v3, v15, :cond_4

    const/16 v1, 0x50

    goto :goto_4

    :cond_4
    const/16 v1, 0x35

    goto :goto_4

    :cond_5
    const/16 v1, 0x71

    goto :goto_4

    :cond_6
    const/16 v1, 0x58

    goto :goto_4

    :cond_7
    const/16 v1, 0x4a

    goto :goto_4

    :cond_8
    const/16 v1, 0x6b

    goto :goto_4

    :cond_9
    const/16 v1, 0x5b

    :goto_4
    xor-int/2addr v1, v9

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/16 v1, 0x24

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs init(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v0

    array-length v1, p2

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v1, :cond_2

    aget-object v3, p2, v2

    invoke-virtual {v3, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;->update(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object v3

    if-nez v0, :cond_1

    add-int/lit8 v2, v2, 0x1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_1
    move-object p0, v3

    :cond_2
    :goto_0
    return-object p0
.end method

.method public static varargs init(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 4

    array-length v0, p2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;->b()[I

    move-result-object v1

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v0, :cond_2

    aget-object v3, p2, v2

    invoke-virtual {v3, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;->update(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object v3

    if-nez v1, :cond_1

    add-int/lit8 v2, v2, 0x1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    move-object p0, v3

    :cond_2
    :goto_0
    return-object p0
.end method

.method public static varargs init(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAll(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initAll(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAll(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 0

    invoke-static {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initRequired(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initOptional(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAll(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 0

    invoke-static {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initRequired(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initOptional(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAllClient(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initAllClient(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAllClient(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 3

    const/16 v0, 0x15

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP14_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP14_SHA256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP15_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP16_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP17_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP18_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP1_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_384:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_521:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_163:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_192:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_224:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_233:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_B_233:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_283:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_409:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_B_409:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_T_571:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAllClient(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 3

    const/16 v0, 0x15

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP14_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP14_SHA256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP15_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP16_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP17_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP18_SHA512:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP1_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_384:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_521:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_163:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_192:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_224:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_233:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_B_233:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_283:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_409:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_B_409:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_T_571:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAllClientDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initAllClientDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAllClientDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA256_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP14_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP1_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAllClientDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA256_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP14_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP1_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAllDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initAllDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAllDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 0

    invoke-static {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initRequiredDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initOptionalDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAllDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 0

    invoke-static {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initRequiredDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initOptionalDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initOptional(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initOptional(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initOptional(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 3

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_163:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_192:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_224:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_233:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_B_233:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_283:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_409:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_B_409:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_T_571:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initOptional(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 3

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_163:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_192:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_224:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_233:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_B_233:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_283:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_K_409:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_B_409:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_T_571:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initOptionalDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initOptionalDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initOptionalDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA256_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initOptionalDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP_SHA256_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRequired(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initRequired(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRequired(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP1_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP14_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_384:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_521:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRequired(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP1_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP14_SHA1:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_256:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_384:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->ECDH_SHA2_NIST_P_521:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRequiredDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initRequiredDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRequiredDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP1_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP14_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRequiredDisabledSignatureVerification(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP1_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->DH_GROUP14_SHA1_DISABLED_SIGNATURE_VERIFICATION:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->init(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges$Entry;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object p0

    return-object p0
.end method
