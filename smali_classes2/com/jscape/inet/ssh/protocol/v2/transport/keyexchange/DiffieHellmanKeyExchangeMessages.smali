.class public Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Factory;


# instance fields
.field private final a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;


# direct methods
.method public varargs constructor <init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    return-void
.end method

.method public static defaultMessages()Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->defaultMessages(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    move-result-object v0

    return-object v0
.end method

.method public static defaultMessages(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;
    .locals 9

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    const/4 v1, 0x0

    new-array v2, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)V

    invoke-static {v0, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object p0

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;

    new-array v2, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;)V

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signatures;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;

    move-result-object v0

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhInitCodec;

    invoke-direct {v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhInitCodec;-><init>()V

    const/4 v6, 0x1

    new-array v7, v6, [Ljava/lang/Class;

    const-class v8, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhInit;

    aput-object v8, v7, v1

    const/16 v8, 0x1e

    invoke-direct {v4, v8, v5, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v4, v3, v1

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhReplyCodec;

    invoke-direct {v5, p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhReplyCodec;-><init>(Lcom/jscape/util/h/I;Lcom/jscape/util/h/I;)V

    new-array p0, v6, [Ljava/lang/Class;

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;

    aput-object v0, p0, v1

    const/16 v0, 0x1f

    invoke-direct {v4, v0, v5, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v4, v3, v6

    invoke-direct {v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)V

    return-object v2
.end method

.method public static defaultMessages(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;
    .locals 9

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    const/4 v1, 0x0

    new-array v2, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)V

    invoke-static {v0, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object p0

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;

    new-array v2, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;)V

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signatures;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;

    move-result-object v0

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhInitCodec;

    invoke-direct {v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhInitCodec;-><init>()V

    const/4 v6, 0x1

    new-array v7, v6, [Ljava/lang/Class;

    const-class v8, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhInit;

    aput-object v8, v7, v1

    const/16 v8, 0x1e

    invoke-direct {v4, v8, v5, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v4, v3, v1

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhReplyCodec;

    invoke-direct {v5, p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhReplyCodec;-><init>(Lcom/jscape/util/h/I;Lcom/jscape/util/h/I;)V

    new-array p0, v6, [Ljava/lang/Class;

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;

    aput-object v0, p0, v1

    const/16 v0, 0x1f

    invoke-direct {v4, v0, v5, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v4, v3, v6

    invoke-direct {v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)V

    return-object v2
.end method


# virtual methods
.method public update(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/DiffieHellmanKeyExchangeMessages;->a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p1

    return-object p1
.end method
