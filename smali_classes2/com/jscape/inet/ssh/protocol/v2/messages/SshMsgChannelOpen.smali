.class public abstract Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpen;
.super Lcom/jscape/inet/ssh/protocol/messages/Message;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<H::",
        "Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;",
        ">",
        "Lcom/jscape/inet/ssh/protocol/messages/Message<",
        "TH;>;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final initialWindowSize:J

.field public final maxPacketSize:I

.field public final senderChannel:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x18

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x1

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    const-string v7, "Cav\u0016K?\u001fuisZ\u0002&\u001fod}A\u0002\"\u001f{e<\u0014Cav\u0016O0\u000e!psUI4\u0002!s{LG\u007f"

    invoke-virtual {v7, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v8, v4

    move v9, v2

    :goto_1
    if-gt v8, v9, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x2d

    if-ge v3, v4, :cond_0

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpen;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v10, v4, v9

    rem-int/lit8 v11, v9, 0x7

    const/16 v12, 0x53

    if-eqz v11, :cond_7

    if-eq v11, v6, :cond_6

    if-eq v11, v0, :cond_5

    const/4 v13, 0x3

    if-eq v11, v13, :cond_4

    const/4 v13, 0x4

    if-eq v11, v13, :cond_3

    const/4 v13, 0x5

    if-eq v11, v13, :cond_2

    const/16 v11, 0x25

    goto :goto_2

    :cond_2
    move v11, v0

    goto :goto_2

    :cond_3
    const/16 v11, 0x71

    goto :goto_2

    :cond_4
    const/16 v11, 0x65

    goto :goto_2

    :cond_5
    const/16 v11, 0x41

    goto :goto_2

    :cond_6
    move v11, v12

    goto :goto_2

    :cond_7
    const/16 v11, 0x52

    :goto_2
    xor-int/2addr v11, v12

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v4, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method protected constructor <init>(IJI)V
    .locals 3

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/messages/Message;-><init>()V

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpen;->senderChannel:I

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpen;->a:[Ljava/lang/String;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    const-wide/16 v1, 0x0

    invoke-static {p2, p3, v1, v2, v0}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    iput-wide p2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpen;->initialWindowSize:J

    int-to-long p2, p4

    const/4 v0, 0x1

    aget-object p1, p1, v0

    invoke-static {p2, p3, v1, v2, p1}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p4, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpen;->maxPacketSize:I

    return-void
.end method
