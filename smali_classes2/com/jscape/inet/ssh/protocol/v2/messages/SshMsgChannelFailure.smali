.class public Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure;
.super Lcom/jscape/inet/ssh/protocol/messages/Message;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/ssh/protocol/messages/Message<",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final recipientChannel:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "J]]\u001f,a)qO[<:j,xGY\'-cJb\\P16v\u0003|@A\u00117g\u0004wKYo"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/4 v5, 0x2

    if-eqz v4, :cond_5

    const/4 v6, 0x1

    if-eq v4, v6, :cond_4

    if-eq v4, v5, :cond_6

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v5, 0x5d

    goto :goto_1

    :cond_1
    const/16 v5, 0x31

    goto :goto_1

    :cond_2
    const/16 v5, 0x68

    goto :goto_1

    :cond_3
    const/16 v5, 0x65

    goto :goto_1

    :cond_4
    const/16 v5, 0x19

    goto :goto_1

    :cond_5
    const/16 v5, 0x2e

    :cond_6
    :goto_1
    const/16 v4, 0x37

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/messages/Message;-><init>()V

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure;->recipientChannel:I

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure;->accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure$Handler;->handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public handlerClass()Ljava/lang/Class;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure$Handler;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure;->recipientChannel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
