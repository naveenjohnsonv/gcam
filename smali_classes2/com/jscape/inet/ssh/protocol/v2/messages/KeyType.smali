.class public final enum Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DSA:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

.field public static final enum NIST_B_233:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

.field public static final enum NIST_B_409:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

.field public static final enum NIST_K_163:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

.field public static final enum NIST_K_233:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

.field public static final enum NIST_K_283:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

.field public static final enum NIST_K_409:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

.field public static final enum NIST_P_192:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

.field public static final enum NIST_P_224:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

.field public static final enum NIST_P_256:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

.field public static final enum NIST_P_384:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

.field public static final enum NIST_P_521:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

.field public static final enum NIST_T_571:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

.field public static final enum RSA:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

.field private static final synthetic a:[Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

.field private static final b:[Ljava/lang/String;


# instance fields
.field public final algorithm:Ljava/lang/String;

.field public final identifier:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0x23

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v4, "\u000e[%)9u,u%G\u0003\u0004A7\n\u000e[%)9j,q$E\u00073a\u001eP\u0014R\u0012\n\u000e[%)9q,u G\n\u000e[%)9c,r!E\u0017%q\u0012\u000e\u0007\u000c\u0000(sDPW\u000f@n#EOH\u0011]s!\u0017%q\u0012\u000e\u0007\u000c\u0000(sDPW\u000f@n#EOH\u0011]s$\n\u000e[%)9j,r*E\u0013%q\u0012\u000e\u0007\u000c\u0000(sDP\u0008H\u00004bEER\u0002\u0005Q\n\u000e[%)9j,t\"O\n\u000e[%)9q,s*B\u0013%q\u0012\u000e\u0007\u000c\u0000(sDP\u0008H\u00004bCOW\u0017%q\u0012\u000e\u0007\u000c\u0000(sDPW\u000f@n#EOH\u0011]r$\u0019\u0015|\u0005\u0008\u0016Q\u001c2f\u0013\u0019FJ\u001692\u0002\u0004\u0016DI`7\u0005S\n\u000e[%)9j,r!E\n\u000e[%)9c,t\"O\u0017%q\u0012\u000e\u0007\u000c\u0000(sDPW\u000f@n#EOH\u0011]r%\u0005\u0005Q2.\'\u0017%q\u0012\u000e\u0007\u000c\u0000(sDPW\u000f@n#EOH\u0011]s*\n\u000e[%)9q,q+D\u0013%q\u0012\u000e\u0007\u000c\u0000(sDP\u0008H\u00004bDHP\n\u000e[%)9q,r B\n\u000e[%)9q,r\'@\r%q\u0012\u000e\u0007\u000c\u0000(sDPCR\u0003\u0012A7\u0016%q\u0012\u000e\u0007\u000c\u0000(sDPW\u000f@n#EOH\u0011]q\u0002\u0005Q\u0017%q\u0012\u000e\u0007\u000c\u0000(sDPW\u000f@n#EOH\u0011]s%\u001e%q\u0012\u000e\u0007\u000c\u0000(sDPW\u000fAn*BMH\u0010Cp&CSU\u000fBn#\u0003\u0012A7\u00073a\u001eP\u0002R\u0000"

    const/16 v5, 0x1d5

    move v8, v2

    const/4 v6, -0x1

    const/16 v7, 0xa

    :goto_0
    const/16 v9, 0x28

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v2

    :goto_2
    const/16 v15, 0x9

    const/4 v1, 0x2

    const/4 v3, 0x3

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    const/16 v11, 0x1b

    if-eqz v12, :cond_1

    add-int/lit8 v1, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v1

    goto :goto_0

    :cond_0
    const-string v4, "\u0013V \u00172f\u0005\u0019\u0010\u001b\u0017?dSG@\u0018Wy4RX_\u0006Jf3"

    move v8, v1

    move v7, v3

    move v5, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v1

    move v8, v12

    :goto_3
    const/16 v9, 0x3f

    add-int/2addr v6, v10

    add-int v1, v6, v7

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->b:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->b:[Ljava/lang/String;

    const/16 v5, 0x1a

    aget-object v5, v4, v5

    aget-object v6, v4, v3

    const/16 v7, 0x1f

    aget-object v7, v4, v7

    invoke-direct {v0, v5, v2, v6, v7}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->RSA:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    aget-object v5, v4, v10

    const/16 v6, 0x20

    aget-object v6, v4, v6

    const/16 v7, 0x21

    aget-object v7, v4, v7

    invoke-direct {v0, v5, v10, v6, v7}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->DSA:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    const/16 v5, 0x18

    aget-object v5, v4, v5

    const/16 v6, 0x16

    aget-object v6, v4, v6

    const/16 v7, 0x1c

    aget-object v7, v4, v7

    invoke-direct {v0, v5, v1, v6, v7}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_P_256:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    const/16 v5, 0xc

    aget-object v6, v4, v5

    aget-object v7, v4, v15

    const/16 v8, 0xa

    aget-object v9, v4, v8

    invoke-direct {v0, v6, v3, v7, v9}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_P_384:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    const/4 v6, 0x4

    aget-object v7, v4, v6

    const/16 v9, 0xd

    aget-object v12, v4, v9

    aget-object v13, v4, v8

    invoke-direct {v0, v7, v6, v12, v13}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_P_521:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    aget-object v6, v4, v1

    aget-object v7, v4, v11

    aget-object v11, v4, v8

    const/4 v12, 0x5

    invoke-direct {v0, v6, v12, v7, v11}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_K_163:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    const/16 v6, 0x15

    aget-object v6, v4, v6

    const/16 v7, 0x1e

    aget-object v7, v4, v7

    aget-object v11, v4, v8

    const/4 v12, 0x6

    invoke-direct {v0, v6, v12, v7, v11}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_P_192:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    const/16 v6, 0x17

    aget-object v6, v4, v6

    aget-object v7, v4, v12

    aget-object v11, v4, v8

    const/4 v13, 0x7

    invoke-direct {v0, v6, v13, v7, v11}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_P_224:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    const/16 v6, 0x10

    aget-object v6, v4, v6

    const/16 v7, 0xe

    aget-object v11, v4, v7

    aget-object v14, v4, v8

    const/16 v12, 0x8

    invoke-direct {v0, v6, v12, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_K_233:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    const/4 v6, 0x5

    aget-object v11, v4, v6

    const/16 v6, 0x12

    aget-object v6, v4, v6

    aget-object v14, v4, v8

    invoke-direct {v0, v11, v15, v6, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_B_233:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    aget-object v6, v4, v12

    const/16 v11, 0x22

    aget-object v11, v4, v11

    aget-object v14, v4, v8

    invoke-direct {v0, v6, v8, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_K_283:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    const/16 v6, 0xb

    aget-object v11, v4, v6

    aget-object v14, v4, v13

    aget-object v15, v4, v8

    invoke-direct {v0, v11, v6, v14, v15}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_K_409:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    const/16 v11, 0x11

    aget-object v11, v4, v11

    const/16 v14, 0x1d

    aget-object v14, v4, v14

    aget-object v15, v4, v8

    invoke-direct {v0, v11, v5, v14, v15}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_B_409:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    aget-object v11, v4, v2

    const/16 v14, 0x14

    aget-object v14, v4, v14

    aget-object v4, v4, v8

    invoke-direct {v0, v11, v9, v14, v4}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_T_571:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    new-array v4, v7, [Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->RSA:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    aput-object v7, v4, v2

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->DSA:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    aput-object v2, v4, v10

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_P_256:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    aput-object v2, v4, v1

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_P_384:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    aput-object v1, v4, v3

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_P_521:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    const/4 v2, 0x4

    aput-object v1, v4, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_K_163:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    const/4 v2, 0x5

    aput-object v1, v4, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_P_192:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    const/4 v2, 0x6

    aput-object v1, v4, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_P_224:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    aput-object v1, v4, v13

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_K_233:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    aput-object v1, v4, v12

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_B_233:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    const/16 v15, 0x9

    aput-object v1, v4, v15

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_K_283:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    const/16 v16, 0xa

    aput-object v1, v4, v16

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_K_409:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    aput-object v1, v4, v6

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->NIST_B_409:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    aput-object v1, v4, v5

    aput-object v0, v4, v9

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->a:[Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    return-void

    :cond_3
    const/16 v16, 0xa

    aget-char v17, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_8

    if-eq v2, v10, :cond_7

    if-eq v2, v1, :cond_6

    if-eq v2, v3, :cond_5

    const/4 v1, 0x4

    if-eq v2, v1, :cond_4

    const/4 v1, 0x5

    if-eq v2, v1, :cond_9

    const/16 v15, 0x5b

    goto :goto_4

    :cond_4
    const/16 v15, 0x4e

    goto :goto_4

    :cond_5
    const/16 v15, 0x55

    goto :goto_4

    :cond_6
    const/16 v15, 0x5e

    goto :goto_4

    :cond_7
    const/16 v15, 0x3a

    goto :goto_4

    :cond_8
    const/16 v15, 0x68

    :cond_9
    :goto_4
    xor-int v1, v9, v15

    xor-int v1, v17, v1

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v2, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifier:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->algorithm:Ljava/lang/String;

    return-void
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method

.method public static identifierFor(Ljava/security/Key;)Ljava/lang/String;
    .locals 9

    invoke-interface {p0}, Ljava/security/Key;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest;->a()[I

    move-result-object v1

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_7

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-nez v1, :cond_8

    const/16 v7, 0x89e

    const/4 v8, -0x1

    if-eq v2, v7, :cond_5

    const v7, 0x10992

    if-eq v2, v7, :cond_3

    const v7, 0x13e20

    if-eq v2, v7, :cond_0

    const v7, 0x3ebd434

    if-eq v2, v7, :cond_6

    goto :goto_0

    :cond_0
    :try_start_1
    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->b:[Ljava/lang/String;

    const/16 v7, 0x1f

    aget-object v2, v2, v7

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_8

    if-nez v1, :cond_8

    if-eqz v2, :cond_2

    if-eqz v1, :cond_1

    move v8, v5

    goto :goto_1

    :cond_1
    move v2, v5

    goto :goto_3

    :cond_2
    :goto_0
    move v2, v8

    goto :goto_3

    :cond_3
    :goto_1
    :try_start_2
    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->b:[Ljava/lang/String;

    const/16 v7, 0x21

    aget-object v2, v2, v7

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    if-nez v1, :cond_8

    if-eqz v2, :cond_2

    if-eqz v1, :cond_4

    move v8, v6

    goto :goto_2

    :cond_4
    move v2, v6

    goto :goto_3

    :catch_0
    move-exception p0

    :try_start_3
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :cond_5
    :goto_2
    :try_start_4
    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->b:[Ljava/lang/String;

    const/16 v7, 0xa

    aget-object v2, v2, v7

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_4

    if-nez v1, :cond_8

    if-eqz v2, :cond_2

    if-eqz v1, :cond_7

    move v8, v4

    :cond_6
    :try_start_5
    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->b:[Ljava/lang/String;

    const/16 v7, 0x13

    aget-object v2, v2, v7

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_2

    if-nez v1, :cond_8

    if-eqz v2, :cond_2

    move v2, v3

    goto :goto_3

    :catch_2
    move-exception p0

    :try_start_6
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_3

    :catch_3
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :cond_7
    move v2, v4

    goto :goto_3

    :catch_4
    move-exception p0

    :try_start_7
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_5

    :catch_5
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :cond_8
    :goto_3
    if-eqz v2, :cond_c

    if-eq v2, v6, :cond_b

    if-eq v2, v4, :cond_a

    if-ne v2, v3, :cond_9

    goto :goto_4

    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->b:[Ljava/lang/String;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p0, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    :goto_4
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->identifierFor(Ljava/security/Key;)Ljava/lang/String;

    move-result-object p0

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->b:[Ljava/lang/String;

    const/16 v1, 0x19

    aget-object v0, v0, v1

    new-array v1, v6, [Ljava/lang/Object;

    aput-object p0, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_b
    sget-object p0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->DSA:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    iget-object p0, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifier:Ljava/lang/String;

    return-object p0

    :cond_c
    :try_start_8
    sget-object p0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->RSA:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    iget-object p0, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifier:Ljava/lang/String;
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_6

    return-object p0

    :catch_6
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :catch_7
    move-exception p0

    :try_start_9
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_8

    :catch_8
    move-exception p0

    :try_start_a
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
    :try_end_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_9

    :catch_9
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
.end method

.method public static identifiers()[Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->values()[Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    move-result-object v0

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->values()[Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    move-result-object v3

    aget-object v3, v3, v2

    iget-object v3, v3, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifier:Ljava/lang/String;

    aput-object v3, v1, v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->a:[Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    invoke-virtual {v0}, [Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    return-object v0
.end method
