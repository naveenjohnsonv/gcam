.class public final enum Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CONNECTION:Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;

.field public static final enum USERAUTH:Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;

.field private static final synthetic a:[Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "/l?e-\u0003\\2\u000c\tL\u0012\u001a\u0019%m\u0008^\u000fC\u0004"

    const/16 v5, 0x15

    const/16 v6, 0x8

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x7b

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x3

    const/4 v2, 0x2

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v2, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v2

    goto :goto_0

    :cond_0
    const/16 v5, 0x19

    const/16 v4, 0xa

    const-string v6, "/f\"o?\u0003J%f\"\u000e\u001fZ\u0004\u000c\u0019/p\u0002L\u000fU\u0013/p"

    move v8, v2

    const/4 v7, -0x1

    move-object/from16 v17, v6

    move v6, v4

    move-object/from16 v4, v17

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v6, v2

    move v8, v11

    :goto_3
    const/16 v9, 0x6d

    add-int/2addr v7, v10

    add-int v2, v7, v6

    invoke-virtual {v4, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;

    aget-object v4, v1, v3

    aget-object v5, v1, v10

    invoke-direct {v0, v4, v3, v5}, Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;->USERAUTH:Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;

    aget-object v4, v1, v2

    aget-object v1, v1, v15

    invoke-direct {v0, v4, v10, v1}, Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;->CONNECTION:Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;

    new-array v1, v2, [Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;->USERAUTH:Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;

    aput-object v2, v1, v3

    aput-object v0, v1, v10

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;->a:[Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v3, v14, 0x7

    if-eqz v3, :cond_8

    if-eq v3, v10, :cond_7

    if-eq v3, v2, :cond_8

    if-eq v3, v15, :cond_6

    if-eq v3, v0, :cond_5

    const/4 v2, 0x5

    if-eq v3, v2, :cond_4

    const/16 v2, 0x73

    goto :goto_4

    :cond_4
    const/16 v2, 0x2d

    goto :goto_4

    :cond_5
    const/16 v2, 0x17

    goto :goto_4

    :cond_6
    const/16 v2, 0x4c

    goto :goto_4

    :cond_7
    const/16 v2, 0x44

    goto :goto_4

    :cond_8
    move v2, v10

    :goto_4
    xor-int/2addr v2, v9

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;->name:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;->a:[Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;

    invoke-virtual {v0}, [Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;

    return-object v0
.end method
