.class public Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;
.super Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequest;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequest<",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final publicKey:Ljava/security/PublicKey;

.field public final publicKeyAlgorithm:Ljava/lang/String;

.field public final signature:Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "NBx\u0003\u0006\u0012Q\u0001)m\u000fY\u000cNB{\u001f\u0003\u0010Y\u0016\u0017z\u0013Y31\u0011`;\u0017\u0019m\u0011\u0007z7\u0011\nP0\u0007y\u0003\u0001\rL2\u0017j\u001a\r\u001ds\u0007\u001b[\u001f\u0003\u0010Y\u0016\u0017z\u0013D\u0005M\u0011\u0007z\u0018\u0005\u0013]_E"

    const/16 v5, 0x4d

    const/16 v6, 0xc

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x65

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x26

    const/16 v4, 0x16

    const-string v6, "MA{\u0000\u0005\u0011R\u0002*n\u000c&\u0011\\\u000e\u0013b\u0001\u000f\u0010\u0006F\u000fMAx\u0010\u0015\u000bR\u0002\u0004E\u0014\n\u0018\u0006F"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v17, v6

    move v6, v4

    move-object/from16 v4, v17

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x66

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    const/16 v16, 0x7

    if-eqz v2, :cond_8

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    const/4 v3, 0x4

    if-eq v2, v3, :cond_5

    if-eq v2, v0, :cond_4

    const/16 v16, 0x5d

    goto :goto_4

    :cond_4
    const/16 v16, 0x1b

    goto :goto_4

    :cond_5
    move/from16 v16, v10

    goto :goto_4

    :cond_6
    const/16 v16, 0x13

    goto :goto_4

    :cond_7
    const/16 v16, 0x6d

    :cond_8
    :goto_4
    xor-int v2, v9, v16

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;->publicKeyAlgorithm:Ljava/lang/String;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;->publicKey:Ljava/security/PublicKey;

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;->signature:Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;->accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature$Handler;->handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public handlerClass()Ljava/lang/Class;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature$Handler;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;->a:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;->username:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;->serviceName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x3

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;->publicKeyAlgorithm:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;->publicKey:Ljava/security/PublicKey;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;->signature:Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
