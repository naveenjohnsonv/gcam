.class public Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;
.super Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest<",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field public final terminalCharactersWidth:I

.field public final terminalPixelsHeight:I

.field public final terminalPixelsWidth:I

.field public final terminalRowsHeight:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "yq0\u0019v\u000590!+\u0001%\u0016yq3\u001dj\u001c\u0002;0+(q\t\u000e9\"\u0010\u0011|\u0005\u0003h3\u0006\"/5k\u0016(=0)\u0016}\u001d90 2\u001dk\u0005<<?#\u0017o2\u00034? \u001d8\n\u001902.\u0008q\u0014\u0005!\u0012/\u0019v\u001f\u000e9l\u0015yq3\u001dj\u001c\u0002;0+*w\u0006\u0018\u001d4.\u001fp\u0005V"

    const/16 v4, 0x6d

    const/16 v5, 0xc

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x5c

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x32

    const/16 v3, 0x1a

    const-string v5, "NF\u0004*]+5\u000c\u0007\u001c\u000cG\'.\u0003\u0005\u0004*]5\u000b\u000b\u0002\u0004\'\u0012\u0017NF\u0004*]+5\u000c\u0007\u001c\u001fF>9\u000e\u00158*F!4\u0016["

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x6b

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->d:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x37

    goto :goto_4

    :cond_4
    const/16 v1, 0x2d

    goto :goto_4

    :cond_5
    const/16 v1, 0x44

    goto :goto_4

    :cond_6
    const/16 v1, 0x24

    goto :goto_4

    :cond_7
    const/16 v1, 0x1b

    goto :goto_4

    :cond_8
    const/16 v1, 0xd

    goto :goto_4

    :cond_9
    const/16 v1, 0x9

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(IIIII)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest;-><init>(IZ)V

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->terminalCharactersWidth:I

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->terminalRowsHeight:I

    iput p4, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->terminalPixelsWidth:I

    iput p5, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->terminalPixelsHeight:I

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange$Handler;->handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public handlerClass()Ljava/lang/Class;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange$Handler;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->d:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->recipientChannel:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->wantReply:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->terminalCharactersWidth:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->terminalRowsHeight:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->terminalPixelsWidth:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->terminalPixelsHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
