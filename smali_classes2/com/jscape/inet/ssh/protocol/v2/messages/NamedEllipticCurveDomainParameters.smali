.class public Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "\u001f\t,5q[F\u0000\u0016.+}ZX\u001d\t/5~\u0008@Nmo9ZD\u001f\u0008@Nmo9\\N\u001a\u0019{Imn9\u001f\u0019\\S{\u007fi\u0004\u0013W\u0007jb9\nL\u000e\u0002m5\u001b{Imn9\u001f\u0019\\S{\u007fi\u000c\u0003\\Q{;=\u0016\u0006K\u001d>>:A\u0008@Nmo9]C\u0018"

    const/16 v4, 0x64

    const/16 v5, 0x13

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x68

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    const/4 v14, 0x5

    const/4 v15, 0x3

    const/4 v1, 0x2

    const/16 v16, 0x7

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    if-eqz v11, :cond_1

    add-int/lit8 v1, v7, 0x1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v1

    goto :goto_0

    :cond_0
    const/16 v4, 0x19

    const/16 v3, 0xc

    const-string v5, ")?\u001b\u0003Njr6!\u0006\u001eJ\u000c)?\u001b\u0003Njr6!\u0006\u001eK"

    move v7, v1

    const/4 v6, -0x1

    move-object/from16 v18, v5

    move v5, v3

    move-object/from16 v3, v18

    goto :goto_3

    :cond_1
    add-int/lit8 v10, v7, 0x1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v5, v1

    move v7, v10

    :goto_3
    const/16 v8, 0x5e

    add-int/2addr v6, v9

    add-int v1, v6, v5

    invoke-virtual {v3, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->b:[Ljava/lang/String;

    new-array v3, v15, [Lcom/jscape/util/ap;

    aget-object v4, v0, v2

    aget-object v5, v0, v14

    invoke-static {v4, v5}, Lcom/jscape/util/ap;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;

    move-result-object v4

    aput-object v4, v3, v2

    aget-object v2, v0, v16

    aget-object v4, v0, v1

    invoke-static {v2, v4}, Lcom/jscape/util/ap;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;

    move-result-object v2

    aput-object v2, v3, v9

    const/4 v2, 0x6

    aget-object v2, v0, v2

    aget-object v0, v0, v9

    invoke-static {v2, v0}, Lcom/jscape/util/ap;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;

    move-result-object v0

    aput-object v0, v3, v1

    invoke-static {v3}, Lcom/jscape/util/G;->a([Lcom/jscape/util/ap;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->a:Ljava/util/Map;

    return-void

    :cond_3
    aget-char v17, v10, v13

    rem-int/lit8 v2, v13, 0x7

    if-eqz v2, :cond_8

    if-eq v2, v9, :cond_7

    if-eq v2, v1, :cond_6

    if-eq v2, v15, :cond_5

    const/4 v1, 0x4

    if-eq v2, v1, :cond_4

    if-eq v2, v14, :cond_9

    const/16 v16, 0x1e

    goto :goto_4

    :cond_4
    const/16 v16, 0x21

    goto :goto_4

    :cond_5
    const/16 v16, 0x73

    goto :goto_4

    :cond_6
    const/16 v16, 0x76

    goto :goto_4

    :cond_7
    const/16 v16, 0x4f

    goto :goto_4

    :cond_8
    const/16 v16, 0x46

    :cond_9
    :goto_4
    xor-int v1, v8, v16

    xor-int v1, v17, v1

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    const/4 v2, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method

.method private static a(Lorg/bouncycastle/jce/spec/ECParameterSpec;)Ljava/lang/String;
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest;->a()[I

    move-result-object v0

    invoke-static {}, Lorg/bouncycastle/asn1/x9/ECNamedCurveTable;->getNames()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lorg/bouncycastle/asn1/x9/ECNamedCurveTable;->getByName(Ljava/lang/String;)Lorg/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object v3

    :try_start_0
    invoke-virtual {v3}, Lorg/bouncycastle/asn1/x9/X9ECParameters;->getN()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {p0}, Lorg/bouncycastle/jce/spec/ECParameterSpec;->getN()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3

    if-nez v0, :cond_0

    if-eqz v4, :cond_3

    :try_start_1
    invoke-virtual {v3}, Lorg/bouncycastle/asn1/x9/X9ECParameters;->getH()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {p0}, Lorg/bouncycastle/jce/spec/ECParameterSpec;->getH()Ljava/math/BigInteger;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4

    :cond_0
    if-nez v0, :cond_1

    if-eqz v4, :cond_3

    :try_start_2
    invoke-virtual {v3}, Lorg/bouncycastle/asn1/x9/X9ECParameters;->getCurve()Lorg/bouncycastle/math/ec/ECCurve;

    move-result-object v4

    invoke-virtual {p0}, Lorg/bouncycastle/jce/spec/ECParameterSpec;->getCurve()Lorg/bouncycastle/math/ec/ECCurve;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/bouncycastle/math/ec/ECCurve;->equals(Lorg/bouncycastle/math/ec/ECCurve;)Z

    move-result v4
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_1
    if-nez v0, :cond_2

    if-eqz v4, :cond_3

    :try_start_3
    invoke-virtual {v3}, Lorg/bouncycastle/asn1/x9/X9ECParameters;->getG()Lorg/bouncycastle/math/ec/ECPoint;

    move-result-object v3

    invoke-virtual {p0}, Lorg/bouncycastle/jce/spec/ECParameterSpec;->getG()Lorg/bouncycastle/math/ec/ECPoint;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/bouncycastle/math/ec/ECPoint;->equals(Lorg/bouncycastle/math/ec/ECPoint;)Z

    move-result v4
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :cond_2
    :goto_2
    if-eqz v4, :cond_3

    :try_start_4
    invoke-static {v2}, Lorg/bouncycastle/asn1/x9/ECNamedCurveTable;->getOID(Ljava/lang/String;)Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object p0

    invoke-virtual {p0}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    return-object p0

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :cond_3
    if-nez v0, :cond_4

    goto :goto_0

    :catch_3
    move-exception p0

    :try_start_5
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->b:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static identifierFor(Ljava/security/Key;)Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest;->a()[I

    move-result-object v0

    :try_start_0
    instance-of v1, p0, Ljava/security/interfaces/ECPublicKey;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    check-cast p0, Ljava/security/interfaces/ECPublicKey;

    invoke-interface {p0}, Ljava/security/interfaces/ECPublicKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object p0

    :goto_0
    invoke-static {p0}, Lorg/bouncycastle/jcajce/provider/asymmetric/util/EC5Util;->convertSpec(Ljava/security/spec/ECParameterSpec;)Lorg/bouncycastle/jce/spec/ECParameterSpec;

    move-result-object p0

    :goto_1
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->a(Lorg/bouncycastle/jce/spec/ECParameterSpec;)Ljava/lang/String;

    move-result-object p0

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->a:Ljava/util/Map;

    invoke-interface {v0, p0, p0}, Ljava/util/Map;->getOrDefault(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0

    :cond_0
    instance-of v1, p0, Lorg/bouncycastle/jce/interfaces/ECPublicKey;

    :cond_1
    if-nez v0, :cond_3

    if-eqz v1, :cond_2

    check-cast p0, Lorg/bouncycastle/jce/interfaces/ECPublicKey;

    invoke-interface {p0}, Lorg/bouncycastle/jce/interfaces/ECPublicKey;->getParameters()Lorg/bouncycastle/jce/spec/ECParameterSpec;

    move-result-object p0

    goto :goto_1

    :cond_2
    instance-of v1, p0, Ljava/security/interfaces/ECPrivateKey;

    :cond_3
    if-nez v0, :cond_5

    if-eqz v1, :cond_4

    check-cast p0, Ljava/security/interfaces/ECPrivateKey;

    invoke-interface {p0}, Ljava/security/interfaces/ECPrivateKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object p0

    goto :goto_0

    :cond_4
    if-nez v0, :cond_6

    :try_start_1
    instance-of v1, p0, Lorg/bouncycastle/jce/interfaces/ECPrivateKey;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :cond_5
    :goto_2
    if-eqz v1, :cond_7

    :cond_6
    check-cast p0, Lorg/bouncycastle/jce/interfaces/ECPrivateKey;

    invoke-interface {p0}, Lorg/bouncycastle/jce/interfaces/ECPrivateKey;->getParameters()Lorg/bouncycastle/jce/spec/ECParameterSpec;

    move-result-object p0

    goto :goto_1

    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->b:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
.end method

.method public static oidFor(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    :try_start_0
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :cond_1
    return-object p0
.end method

.method public static parametersFor(Ljava/lang/String;)Lorg/bouncycastle/asn1/x9/X9ECParameters;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest;->a()[I

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    :try_start_0
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance p0, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lorg/bouncycastle/asn1/x9/ECNamedCurveTable;->getByOID(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;)Lorg/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :cond_1
    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :cond_2
    :goto_0
    new-instance v1, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-direct {v1, p0}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lorg/bouncycastle/asn1/x9/ECNamedCurveTable;->getByOID(Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;)Lorg/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object v1

    if-nez v0, :cond_3

    if-nez v1, :cond_3

    invoke-static {p0}, Lorg/bouncycastle/asn1/x9/ECNamedCurveTable;->getByName(Ljava/lang/String;)Lorg/bouncycastle/asn1/x9/X9ECParameters;

    move-result-object p0

    move-object v1, p0

    :cond_3
    return-object v1
.end method
