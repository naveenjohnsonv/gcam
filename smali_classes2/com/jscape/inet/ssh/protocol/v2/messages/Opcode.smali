.class public final enum Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CS7:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum CS8:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum ECHO:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum ECHOCTL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum ECHOE:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum ECHOK:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum ECHOKE:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum ECHONL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum ICANON:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum ICRNL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum IEXTEN:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum IGNCR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum IGNPAR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum IMAXBEL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum INLCR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum INPCK:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum ISIG:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum ISTRIP:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum IUCLC:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum IXANY:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum IXOFF:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum IXON:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum NOFLSH:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum OCRNL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum OLCUC:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum ONLCR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum ONLRET:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum ONOCR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum OPOST:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum PARENB:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum PARMRK:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum PARODD:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum PENDIN:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum TOSTOP:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum TTY_OP_ISPEED:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum TTY_OP_OSPEED:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VDISCARD:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VDSUSP:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VEOF:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VEOL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VEOL2:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VERASE:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VFLUSH:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VINTR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VKILL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VLNEXT:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VQUIT:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VREPRINT:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VSTART:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VSTATUS:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VSTOP:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VSUSP:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VSWTCH:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum VWERASE:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field public static final enum XCASE:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

.field private static final synthetic a:[Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;


# instance fields
.field public final code:I


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0x37

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x4

    const/4 v2, 0x0

    const-string v3, "\u0013C\u0006w\u0005\u0019C\u001cv\u0002\u0006\u0006A\u001cw\n~\u0008\u0000R\u000bh\u001cs[\u0002\u0006\u0000D\u001dm\u001dj\u0008\u0000D\u0007k\r{G\u0012\u0006\u001fE\u0016l\u000bt\u0004\u001fX\u0001v\u0006\u001fC\u000fv\u0001t\u0007\u001fM\u000f`\u000c\u007fY\r\u0002T\u0017g\u0001jJ\u0019S\u001e}\u000b~\u0006\u0006E\u0000|\u0007t\u0004\u0000E\u0001~\u0003\u0015Sy\u0005\u0019P\u0001k\u001a\u0005\u001fN\u0002{\u001c\u0005\u0019N\u0001{\u001c\u0005\u0000Q\u001bq\u001a\u0006\u0019N\u0002j\u000bn\u0005\u0000E\u0001t|\u0006\u0000S\u001ay\u001cn\u0005\u0000S\u001aw\u001e\u0007\u0013C\u0006w\rnY\u0007\u0000S\u001ay\u001aoF\u0005\u0013C\u0006w\u000b\u0005\u0000K\u0007t\u0002\u0004\u0000E\u0001t\u0007\u0000W\u000bj\u000fiP\u0006\u0018O\u0008t\u001dr\u0006\u001fG\u0000h\u000fh\r\u0002T\u0017g\u0001jJ\u001fS\u001e}\u000b~\u0005\u0013C\u0006w\u0005\u0005\u001fC\u001cv\u0002\u0005\u001fG\u0000{\u001c\u0005\u001fU\rt\r\u0006\u0002O\u001dl\u0001j\u0006\u0013C\u0006w\u0000v\u0006\u001fS\u001aj\u0007j\u0006\u0013C\u0006w\u0005\u007f\u0005\u0019N\u0002{\u001c\u0004\u001fS\u0007\u007f\u0006\u0006A\u001c}\u0000x\u0005\u001fX\u0001~\u0008\u0005\u001fX\u000fv\u0017\u0005\u0000S\u001bk\u001e\u0005\u0019L\rm\r\u0005\u000eC\u000fk\u000b\u0005\u001fN\u001e{\u0005\u0006\u0000S\u0019l\rr\u0005\u0000I\u0000l\u001c\u0003\u0015Sv\u0006\u0006A\u001cu\u001cq\u0006\u0000L\u0000}\u0016n"

    const/16 v4, 0x163

    const/4 v5, -0x1

    move v6, v1

    move v7, v2

    :goto_0
    const/4 v8, 0x1

    add-int/2addr v5, v8

    add-int v9, v5, v6

    invoke-virtual {v3, v5, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const/4 v10, -0x1

    const/16 v13, 0x8

    :goto_1
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    array-length v14, v9

    move v15, v2

    :goto_2
    const/4 v12, 0x3

    const/4 v11, 0x2

    if-gt v14, v15, :cond_3

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v9}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v13}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    const/4 v13, 0x6

    const/16 v14, 0xd

    if-eqz v10, :cond_1

    add-int/lit8 v10, v7, 0x1

    aput-object v9, v0, v7

    add-int/2addr v5, v6

    if-ge v5, v4, :cond_0

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v7, v10

    goto :goto_0

    :cond_0
    const/4 v3, -0x1

    const-string v4, ":\u007f&C\'E\u0006:|8W\'H"

    move v7, v10

    move v6, v13

    goto :goto_3

    :cond_1
    add-int/lit8 v10, v7, 0x1

    aput-object v9, v0, v7

    add-int/2addr v5, v6

    if-ge v5, v4, :cond_2

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v14, v4

    move v7, v10

    move-object v4, v3

    move v3, v5

    :goto_3
    add-int/lit8 v5, v3, 0x1

    add-int v3, v5, v6

    invoke-virtual {v4, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    move v10, v2

    move-object v3, v4

    move v4, v14

    const/16 v13, 0x32

    goto :goto_1

    :cond_2
    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v4, 0x31

    aget-object v4, v0, v4

    invoke-direct {v3, v4, v2, v8}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VINTR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v4, 0x11

    aget-object v5, v0, v4

    invoke-direct {v3, v5, v8, v11}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VQUIT:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v5, 0x35

    aget-object v6, v0, v5

    invoke-direct {v3, v6, v11, v12}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VERASE:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v6, 0x19

    aget-object v6, v0, v6

    invoke-direct {v3, v6, v12, v1}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VKILL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v6, 0xc

    aget-object v7, v0, v6

    const/4 v9, 0x5

    invoke-direct {v3, v7, v1, v9}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VEOF:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v7, 0x1a

    aget-object v7, v0, v7

    invoke-direct {v3, v7, v9, v13}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VEOL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v7, 0x13

    aget-object v7, v0, v7

    const/4 v9, 0x7

    invoke-direct {v3, v7, v13, v9}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VEOL2:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v7, 0x14

    aget-object v7, v0, v7

    const/16 v10, 0x8

    invoke-direct {v3, v7, v9, v10}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VSTART:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v7, 0x15

    aget-object v7, v0, v7

    const/16 v15, 0x9

    invoke-direct {v3, v7, v10, v15}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VSTOP:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v7, 0x2c

    aget-object v7, v0, v7

    const/16 v10, 0xa

    invoke-direct {v3, v7, v15, v10}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VSUSP:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aget-object v7, v0, v1

    const/16 v1, 0xb

    invoke-direct {v3, v7, v10, v1}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VDSUSP:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aget-object v7, v0, v12

    invoke-direct {v3, v7, v1, v6}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VREPRINT:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v7, 0x1b

    aget-object v7, v0, v7

    invoke-direct {v3, v7, v6, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VWERASE:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v7, 0x34

    aget-object v6, v0, v7

    const/16 v12, 0xe

    invoke-direct {v3, v6, v14, v12}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VLNEXT:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v6, 0x36

    aget-object v10, v0, v6

    const/16 v11, 0xf

    invoke-direct {v3, v10, v12, v11}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VFLUSH:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x30

    aget-object v11, v0, v10

    const/16 v10, 0xf

    const/16 v14, 0x10

    invoke-direct {v3, v11, v10, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VSWTCH:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x17

    aget-object v10, v0, v10

    const/16 v11, 0x10

    invoke-direct {v3, v10, v11, v4}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VSTATUS:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/4 v10, 0x5

    aget-object v11, v0, v10

    const/16 v10, 0x12

    invoke-direct {v3, v11, v4, v10}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VDISCARD:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x1d

    aget-object v11, v0, v10

    const/16 v10, 0x12

    const/16 v14, 0x1e

    invoke-direct {v3, v11, v10, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->IGNPAR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x33

    aget-object v10, v0, v10

    const/16 v11, 0x13

    const/16 v14, 0x1f

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->PARMRK:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x2f

    aget-object v10, v0, v10

    const/16 v11, 0x14

    const/16 v14, 0x20

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->INPCK:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x25

    aget-object v10, v0, v10

    const/16 v11, 0x15

    const/16 v14, 0x21

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ISTRIP:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0xf

    aget-object v10, v0, v10

    const/16 v11, 0x16

    const/16 v14, 0x22

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->INLCR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x21

    aget-object v10, v0, v10

    const/16 v11, 0x17

    const/16 v14, 0x23

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->IGNCR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x20

    aget-object v10, v0, v10

    const/16 v11, 0x18

    const/16 v14, 0x24

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ICRNL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x22

    aget-object v10, v0, v10

    const/16 v11, 0x19

    const/16 v14, 0x25

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->IUCLC:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aget-object v10, v0, v9

    const/16 v11, 0x1a

    const/16 v14, 0x26

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->IXON:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x2b

    aget-object v10, v0, v10

    const/16 v11, 0x1b

    const/16 v14, 0x27

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->IXANY:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x2a

    aget-object v10, v0, v10

    const/16 v11, 0x1c

    const/16 v14, 0x28

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->IXOFF:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aget-object v10, v0, v15

    const/16 v11, 0x29

    const/16 v14, 0x1d

    invoke-direct {v3, v10, v14, v11}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->IMAXBEL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x28

    aget-object v10, v0, v10

    const/16 v11, 0x1e

    const/16 v14, 0x32

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ISIG:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x8

    aget-object v11, v0, v10

    const/16 v10, 0x1f

    const/16 v14, 0x33

    invoke-direct {v3, v11, v10, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ICANON:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x2e

    aget-object v10, v0, v10

    const/16 v11, 0x20

    invoke-direct {v3, v10, v11, v7}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->XCASE:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aget-object v10, v0, v2

    const/16 v11, 0x21

    invoke-direct {v3, v10, v11, v5}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ECHO:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x18

    aget-object v10, v0, v10

    const/16 v11, 0x22

    invoke-direct {v3, v10, v11, v6}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ECHOE:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x1f

    aget-object v10, v0, v10

    const/16 v11, 0x23

    const/16 v14, 0x37

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ECHOK:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x24

    aget-object v10, v0, v10

    const/16 v11, 0x24

    const/16 v14, 0x38

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ECHONL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x1c

    aget-object v10, v0, v10

    const/16 v11, 0x25

    const/16 v14, 0x39

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->NOFLSH:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x23

    aget-object v10, v0, v10

    const/16 v11, 0x26

    const/16 v14, 0x3a

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->TOSTOP:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aget-object v10, v0, v13

    const/16 v11, 0x27

    const/16 v14, 0x3b

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->IEXTEN:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x16

    aget-object v10, v0, v10

    const/16 v11, 0x28

    const/16 v14, 0x3c

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ECHOCTL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x26

    aget-object v10, v0, v10

    const/16 v11, 0x29

    const/16 v14, 0x3d

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ECHOKE:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aget-object v10, v0, v1

    const/16 v11, 0x2a

    const/16 v14, 0x3e

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->PENDIN:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aget-object v10, v0, v12

    const/16 v11, 0x2b

    const/16 v14, 0x46

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->OPOST:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x2d

    aget-object v10, v0, v10

    const/16 v11, 0x2c

    const/16 v14, 0x47

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->OLCUC:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x27

    aget-object v10, v0, v10

    const/16 v11, 0x2d

    const/16 v14, 0x48

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ONLCR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aget-object v10, v0, v8

    const/16 v11, 0x2e

    const/16 v14, 0x49

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->OCRNL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x10

    aget-object v10, v0, v10

    const/16 v11, 0x2f

    const/16 v14, 0x4a

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ONOCR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x12

    aget-object v10, v0, v10

    const/16 v11, 0x4b

    const/16 v14, 0x30

    invoke-direct {v3, v10, v14, v11}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ONLRET:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0xd

    aget-object v11, v0, v10

    const/16 v10, 0x31

    const/16 v14, 0x5a

    invoke-direct {v3, v11, v10, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->CS7:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x32

    aget-object v11, v0, v10

    const/16 v14, 0x5b

    invoke-direct {v3, v11, v10, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->CS8:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x29

    aget-object v10, v0, v10

    const/16 v11, 0x33

    const/16 v14, 0x5c

    invoke-direct {v3, v10, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->PARENB:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/4 v10, 0x2

    aget-object v11, v0, v10

    const/16 v10, 0x5d

    invoke-direct {v3, v11, v7, v10}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->PARODD:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0x1e

    aget-object v10, v0, v10

    const/16 v11, 0x80

    invoke-direct {v3, v10, v5, v11}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->TTY_OP_ISPEED:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v10, 0xa

    aget-object v0, v0, v10

    const/16 v10, 0x81

    invoke-direct {v3, v0, v6, v10}, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->TTY_OP_OSPEED:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v0, 0x37

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    sget-object v10, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VINTR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v10, v0, v2

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VQUIT:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v8

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VERASE:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/4 v8, 0x2

    aput-object v2, v0, v8

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VKILL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/4 v8, 0x3

    aput-object v2, v0, v8

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VEOF:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/4 v8, 0x4

    aput-object v2, v0, v8

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VEOL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/4 v8, 0x5

    aput-object v2, v0, v8

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VEOL2:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v13

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VSTART:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v9

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VSTOP:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v11, 0x8

    aput-object v2, v0, v11

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VSUSP:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v15

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VDSUSP:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v8, 0xa

    aput-object v2, v0, v8

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VREPRINT:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VWERASE:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VLNEXT:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VFLUSH:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v1, v0, v12

    const/16 v1, 0xf

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VSWTCH:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VSTATUS:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->VDISCARD:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v1, v0, v4

    const/16 v1, 0x12

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->IGNPAR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->PARMRK:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->INPCK:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ISTRIP:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->INLCR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->IGNCR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ICRNL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->IUCLC:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->IXON:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->IXANY:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->IXOFF:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->IMAXBEL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v12, 0x1d

    aput-object v1, v0, v12

    const/16 v1, 0x1e

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ISIG:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ICANON:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->XCASE:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ECHO:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ECHOE:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ECHOK:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ECHONL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->NOFLSH:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->TOSTOP:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->IEXTEN:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ECHOCTL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ECHOKE:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->PENDIN:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->OPOST:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->OLCUC:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ONLCR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->OCRNL:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ONOCR:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->ONLRET:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v17, 0x30

    aput-object v1, v0, v17

    const/16 v1, 0x31

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->CS7:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->CS8:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    const/16 v16, 0x32

    aput-object v1, v0, v16

    const/16 v1, 0x33

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->PARENB:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v2, v0, v1

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->PARODD:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->TTY_OP_ISPEED:Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    aput-object v1, v0, v5

    aput-object v3, v0, v6

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->a:[Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    return-void

    :cond_3
    const/16 v11, 0x8

    const/16 v12, 0x1d

    const/16 v16, 0x32

    const/16 v17, 0x30

    aget-char v1, v9, v15

    rem-int/lit8 v2, v15, 0x7

    if-eqz v2, :cond_8

    if-eq v2, v8, :cond_7

    const/4 v8, 0x2

    if-eq v2, v8, :cond_6

    const/4 v8, 0x3

    if-eq v2, v8, :cond_5

    const/4 v8, 0x4

    if-eq v2, v8, :cond_6

    const/4 v8, 0x5

    if-eq v2, v8, :cond_4

    move v2, v12

    goto :goto_4

    :cond_4
    move/from16 v2, v16

    goto :goto_4

    :cond_5
    move/from16 v2, v17

    goto :goto_4

    :cond_6
    const/16 v2, 0x46

    goto :goto_4

    :cond_7
    move v2, v11

    goto :goto_4

    :cond_8
    const/16 v2, 0x5e

    :goto_4
    xor-int/2addr v2, v13

    xor-int/2addr v1, v2

    int-to-char v1, v1

    aput-char v1, v9, v15

    add-int/lit8 v15, v15, 0x1

    const/4 v1, 0x4

    const/4 v2, 0x0

    const/4 v8, 0x1

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->code:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->a:[Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    invoke-virtual {v0}, [Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/ssh/protocol/v2/messages/Opcode;

    return-object v0
.end method
