.class public final enum Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum SSH_EXTENDED_DATA_STDERR:Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;

.field private static final synthetic a:[Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;


# instance fields
.field public final code:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const-string v0, "\roJT\u00118^\u001brFN\u0010?N\u001fhCT\u00074N\u001bnP"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    const/4 v4, 0x1

    if-gt v1, v3, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;

    invoke-direct {v1, v0, v2, v4}, Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;->SSH_EXTENDED_DATA_STDERR:Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;

    new-array v0, v4, [Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;->a:[Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;

    return-void

    :cond_0
    aget-char v5, v0, v3

    rem-int/lit8 v6, v3, 0x7

    const/4 v7, 0x3

    if-eqz v6, :cond_5

    if-eq v6, v4, :cond_4

    const/4 v8, 0x2

    if-eq v6, v8, :cond_6

    if-eq v6, v7, :cond_3

    const/4 v4, 0x4

    if-eq v6, v4, :cond_2

    const/4 v4, 0x5

    if-eq v6, v4, :cond_1

    const/16 v4, 0x9

    goto :goto_1

    :cond_1
    const/16 v4, 0x63

    goto :goto_1

    :cond_2
    const/16 v4, 0x57

    goto :goto_1

    :cond_3
    const/16 v4, 0x8

    goto :goto_1

    :cond_4
    const/16 v4, 0x3f

    goto :goto_1

    :cond_5
    const/16 v4, 0x5d

    :cond_6
    :goto_1
    xor-int/2addr v4, v7

    xor-int/2addr v4, v5

    int-to-char v4, v4

    aput-char v4, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;->code:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;->a:[Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;

    invoke-virtual {v0}, [Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;

    return-object v0
.end method
