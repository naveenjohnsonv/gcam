.class public Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;
.super Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest<",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field public final terminalCharactersWidth:I

.field public final terminalEnvironmentVariable:Ljava/lang/String;

.field public final terminalModes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/TerminalMode;",
            ">;"
        }
    .end annotation
.end field

.field public final terminalPixelsHeight:I

.field public final terminalPixelsWidth:I

.field public final terminalRowsHeight:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "bJ;GS\u0016\u0001 \u000b#rH\u0003\r\"\u0019\u0007GH\u001c\u0000:W\u0015bJ;GS\u0016\u0001 \u000b#pN\u000c\u001b\u0006\u000f&EI\u000fU\u001abJ;GS\u0016\u0001 \u000b#aI\u001a\u001a/\t;GS\u0008?\'\u000e;J\u001c\u001fbJ;GS\u0016\u0001 \u000b#gO\r\u0001<\u0005!OD\u0015\u001c\u0018\u000b=K@\u0019\u0004+Wh\u0010bJ;GS\u0016\u0001 \u000b#oN\u001f\r=W-\u001d\u0019\'oR\u001c+&\u000b!LD\u0017:+\u001b:GR\u000f8:\u0013\u001dGP[\u0013<\u000f,KQ\u0012\r \u001e\u000cJ@\u0015\u0006+\u0006r"

    const/16 v4, 0xa7

    const/16 v5, 0x17

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/4 v8, 0x1

    add-int/2addr v6, v8

    add-int v9, v6, v5

    invoke-virtual {v3, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x7

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    array-length v12, v9

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v9}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x23

    const/16 v3, 0x16

    const-string v5, "Og\u0016j~;,\r&\u000e_e. \u000f45fh\"-^\u000cOg\u0015nb\"\u0017\u00067\u000ev1"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v10, 0x2a

    add-int/2addr v6, v8

    add-int v9, v6, v5

    invoke-virtual {v3, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->d:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v9, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v8, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x6f

    goto :goto_4

    :cond_4
    const/16 v1, 0x7c

    goto :goto_4

    :cond_5
    const/16 v1, 0x26

    goto :goto_4

    :cond_6
    const/16 v1, 0x25

    goto :goto_4

    :cond_7
    const/16 v1, 0x48

    goto :goto_4

    :cond_8
    const/16 v1, 0x6d

    goto :goto_4

    :cond_9
    const/16 v1, 0x49

    :goto_4
    xor-int/2addr v1, v10

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v9, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(IZLjava/lang/String;IIIILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            "IIII",
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/TerminalMode;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest;-><init>(IZ)V

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalEnvironmentVariable:Ljava/lang/String;

    iput p4, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalCharactersWidth:I

    iput p5, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalRowsHeight:I

    iput p6, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalPixelsWidth:I

    iput p7, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalPixelsHeight:I

    invoke-static {p8}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p8, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalModes:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq$Handler;->handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public handlerClass()Ljava/lang/Class;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq$Handler;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->d:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->recipientChannel:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->wantReply:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalEnvironmentVariable:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalCharactersWidth:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalRowsHeight:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalPixelsWidth:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalPixelsHeight:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalModes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
