.class public Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;
.super Lcom/jscape/inet/ssh/protocol/messages/Message;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/ssh/protocol/messages/Message<",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final description:Ljava/lang/String;

.field public final languageTag:Ljava/lang/String;

.field public final reasonCode:I

.field public final recipientChannel:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "rL~9\u0000\u00152?\u000bw\u000c\u000f\u0015zy+\r\u001fz\u0015\u001d\u0015\u00046\r|6\u000b\u001e\u0008.\t|\u001e\u000f\u001b++\u001ewx\u0015\u0000\"=\u0005b1\u000b\u001c3\u001d\u0004s6\u0000\u0017+c"

    const/16 v6, 0x3b

    move v9, v4

    const/4 v7, -0x1

    const/16 v8, 0xf

    :goto_0
    const/16 v10, 0x74

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    move v15, v4

    :goto_2
    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    add-int/lit8 v12, v9, 0x1

    if-eqz v13, :cond_1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v12

    goto :goto_0

    :cond_0
    const/16 v6, 0x1d

    const-string v5, "RlV\u001d=1\u0015\u0017<F\u0011!<ZY\rRl@\u001d/!\u0008\u0010\u000f]\u001c+o"

    move v9, v12

    const/4 v7, -0x1

    const/16 v8, 0xf

    goto :goto_3

    :cond_1
    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v12

    :goto_3
    const/16 v10, 0x54

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v13, v4

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v2, v15, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v11, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v2, 0x33

    goto :goto_4

    :cond_4
    const/4 v2, 0x6

    goto :goto_4

    :cond_5
    const/16 v2, 0x1a

    goto :goto_4

    :cond_6
    const/16 v2, 0x2c

    goto :goto_4

    :cond_7
    const/16 v2, 0x66

    goto :goto_4

    :cond_8
    const/16 v2, 0x18

    goto :goto_4

    :cond_9
    const/16 v2, 0x2a

    :goto_4
    xor-int/2addr v2, v10

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v12, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/messages/Message;-><init>()V

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;->recipientChannel:I

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;->reasonCode:I

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;->description:Ljava/lang/String;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;->languageTag:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iget p2, p2, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;->code:I

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;->accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure$Handler;->handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public handlerClass()Ljava/lang/Class;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure$Handler;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;->recipientChannel:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;->reasonCode:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;->description:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;->languageTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
