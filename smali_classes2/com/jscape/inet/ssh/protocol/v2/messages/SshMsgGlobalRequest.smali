.class public abstract Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequest;
.super Lcom/jscape/inet/ssh/protocol/messages/Message;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<H::",
        "Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;",
        ">",
        "Lcom/jscape/inet/ssh/protocol/messages/Message<",
        "TH;>;"
    }
.end annotation


# instance fields
.field public final wantReply:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/messages/Message;-><init>()V

    iput-boolean p1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequest;->wantReply:Z

    return-void
.end method
