.class public abstract Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequest;
.super Lcom/jscape/inet/ssh/protocol/messages/Message;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<H::",
        "Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;",
        ">",
        "Lcom/jscape/inet/ssh/protocol/messages/Message<",
        "TH;>;"
    }
.end annotation


# instance fields
.field public final serviceName:Ljava/lang/String;

.field public final username:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/messages/Message;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequest;->username:Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequest;->serviceName:Ljava/lang/String;

    return-void
.end method
