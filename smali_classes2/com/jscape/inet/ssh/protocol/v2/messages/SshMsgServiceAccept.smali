.class public Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceAccept;
.super Lcom/jscape/inet/ssh/protocol/messages/Message;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/ssh/protocol/messages/Message<",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceAccept$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final serviceName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "\u0003\n7\u001cL\u0003{5\u000b)8\\\u0001i3\u001a:!KDS#\u001c-\'V\u0007M\u001e\u001824\u0002C"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceAccept;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x71

    goto :goto_1

    :cond_1
    const/16 v4, 0x3d

    goto :goto_1

    :cond_2
    const/16 v4, 0x66

    goto :goto_1

    :cond_3
    const/16 v4, 0x8

    goto :goto_1

    :cond_4
    const/4 v4, 0x6

    goto :goto_1

    :cond_5
    const/16 v4, 0x20

    goto :goto_1

    :cond_6
    const/16 v4, 0x9

    :goto_1
    const/16 v5, 0x59

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;)V
    .locals 0

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;->name:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceAccept;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/messages/Message;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceAccept;->serviceName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceAccept$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceAccept;->accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceAccept$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceAccept$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceAccept$Handler;->handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceAccept;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public handlerClass()Ljava/lang/Class;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceAccept$Handler;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceAccept;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceAccept;->serviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
