.class public Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;
.super Lcom/jscape/inet/ssh/protocol/messages/Message;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/ssh/protocol/messages/Message<",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final initialWindowSize:J

.field public final maxPacketSize:I

.field public final recipientChannel:I

.field public final senderChannel:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "d\\)b\u0014/6:?2f\u0014%6$A\u0014d\\3i\u0013?:)\u0010\rn\u0014/<?/3}\u001fv\u0010d\\7f\u0002\u001b2+\u0017?s)\")-A\u0018\n\u001d>\'\u0013%:<\u0015;kZ<:&\u00185pZ8:2\u0019t"

    const/16 v4, 0x4f

    const/16 v5, 0x10

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x2a

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x45

    const/16 v3, 0x14

    const-string v5, "+<\u001f\u00066\u000b\nI-\u001aE0\u000f\u0006I.\u0012\\>D0:.\u0013k(\r1\u0001<\u0015H>\u0006=\u00198\u0015e4\u0004\u0014\u0000/\u0016G/\u0003\u001d\u0007}\u0000T>\t\u001b\u00194\u001eH/)\u001a\u00083\u0015C7W"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0xb

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x79

    goto :goto_4

    :cond_4
    const/16 v1, 0x61

    goto :goto_4

    :cond_5
    const/16 v1, 0x50

    goto :goto_4

    :cond_6
    const/16 v1, 0x2d

    goto :goto_4

    :cond_7
    const/16 v1, 0x70

    goto :goto_4

    :cond_8
    const/16 v1, 0x56

    goto :goto_4

    :cond_9
    const/16 v1, 0x62

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(IIJI)V
    .locals 2

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/messages/Message;-><init>()V

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->recipientChannel:I

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->senderChannel:I

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->a:[Ljava/lang/String;

    const/4 p2, 0x3

    aget-object p2, p1, p2

    const-wide/16 v0, 0x0

    invoke-static {p3, p4, v0, v1, p2}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    iput-wide p3, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->initialWindowSize:J

    int-to-long p2, p5

    const/4 p4, 0x4

    aget-object p1, p1, p4

    invoke-static {p2, p3, v0, v1, p1}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p5, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->maxPacketSize:I

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation$Handler;->handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public handlerClass()Ljava/lang/Class;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation$Handler;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->a:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->recipientChannel:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->senderChannel:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->initialWindowSize:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->maxPacketSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
