.class public Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;
.super Lcom/jscape/inet/ssh/protocol/messages/Message;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/ssh/protocol/messages/Message<",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final f:Ljava/math/BigInteger;

.field public final hostKey:Ljava/security/PublicKey;

.field public hostKeyBlob:[B

.field public final signature:Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "lW\u000eUs\u0007QZ\\\"pG\u0005bmA\u0016ty@aWK\u0015lK\u0005c\u0002\u000c\u0013\u0004\u0015qg\u000e{KQ\u0014}="

    const/16 v5, 0x2a

    const/16 v6, 0x1d

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/4 v9, 0x1

    add-int/2addr v7, v9

    add-int v10, v7, v6

    invoke-virtual {v4, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0x13

    const/16 v12, 0x47

    move v14, v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v15, v10

    const/4 v2, 0x0

    :goto_2
    if-gt v15, v2, :cond_3

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v10, v8, 0x1

    if-eqz v13, :cond_1

    aput-object v2, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v10

    goto :goto_0

    :cond_0
    const-string v4, "GPTq\u000eGPZ#\'@\u0005\u000e\tp ;Vs"

    move v6, v0

    move v8, v10

    move v5, v11

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v2, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v6, v2

    move v8, v10

    :goto_3
    add-int/2addr v7, v9

    add-int v2, v7, v6

    invoke-virtual {v4, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v14, v11

    const/4 v13, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v10, v2

    rem-int/lit8 v3, v2, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v9, :cond_8

    const/4 v9, 0x2

    if-eq v3, v9, :cond_7

    const/4 v9, 0x3

    if-eq v3, v9, :cond_6

    if-eq v3, v0, :cond_5

    const/4 v9, 0x5

    if-eq v3, v9, :cond_4

    const/16 v3, 0x5d

    goto :goto_4

    :cond_4
    const/16 v3, 0x27

    goto :goto_4

    :cond_5
    move v3, v12

    goto :goto_4

    :cond_6
    const/16 v3, 0x5f

    goto :goto_4

    :cond_7
    const/16 v3, 0x21

    goto :goto_4

    :cond_8
    const/16 v3, 0x63

    goto :goto_4

    :cond_9
    const/16 v3, 0x78

    :goto_4
    xor-int/2addr v3, v14

    xor-int v3, v16, v3

    int-to-char v3, v3

    aput-char v3, v10, v2

    add-int/lit8 v2, v2, 0x1

    const/4 v9, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/security/PublicKey;Ljava/math/BigInteger;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/messages/Message;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->hostKey:Ljava/security/PublicKey;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->f:Ljava/math/BigInteger;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->signature:Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply$Handler;->handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public handlerClass()Ljava/lang/Class;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply$Handler;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->a:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->hostKey:Ljava/security/PublicKey;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->f:Ljava/math/BigInteger;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->signature:Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhGexReply;->hostKeyBlob:[B

    invoke-static {v1}, Lcom/jscape/util/W;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
