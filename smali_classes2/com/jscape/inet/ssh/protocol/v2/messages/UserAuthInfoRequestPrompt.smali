.class public Lcom/jscape/inet/ssh/protocol/v2/messages/UserAuthInfoRequestPrompt;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final echo:Z

.field public final prompt:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x7

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v3

    :goto_0
    const/16 v6, 0x1f

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v2, v4

    const-string v8, "@h\u0019 \u000cxn#9;\u00191%b\'\u0004\u0001\u0012%\u000bE6\u001d=\u00190\u0010G!\u0003%\u000c7Dl#\u001e\'\u00113\u0010*t"

    invoke-virtual {v8, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v3

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x2b

    if-ge v2, v4, :cond_0

    invoke-virtual {v8, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v2

    move v2, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/UserAuthInfoRequestPrompt;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x4c

    goto :goto_2

    :cond_2
    const/16 v12, 0x8

    goto :goto_2

    :cond_3
    const/16 v12, 0x7b

    goto :goto_2

    :cond_4
    const/16 v12, 0x5c

    goto :goto_2

    :cond_5
    const/16 v12, 0x63

    goto :goto_2

    :cond_6
    const/16 v12, 0x57

    goto :goto_2

    :cond_7
    const/16 v12, 0x73

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/UserAuthInfoRequestPrompt;->prompt:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/UserAuthInfoRequestPrompt;->echo:Z

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/UserAuthInfoRequestPrompt;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/UserAuthInfoRequestPrompt;->prompt:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/UserAuthInfoRequestPrompt;->echo:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
