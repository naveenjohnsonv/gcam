.class public Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword;
.super Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequest;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequest<",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final newPassword:Ljava/lang/String;

.field public final oldPassword:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const-string v0, "Yy8fI?%yo\"jO,\u0018Xo!^_+\u0004Do\'{[+\u0003}e\"O\u001a#\u001ffn\u0000JI+\u0007ex4\u0016\u001drZ  z\u000c\u0016x\u001eo}\u0000JI+\u0007ex4\u0016\u001drZ  z\u000cG"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v5, 0x5f

    const/4 v6, 0x5

    if-eqz v4, :cond_5

    const/4 v7, 0x1

    if-eq v4, v7, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    if-eq v4, v6, :cond_1

    const/16 v5, 0x25

    goto :goto_1

    :cond_1
    const/16 v5, 0xd

    goto :goto_1

    :cond_2
    const/16 v5, 0x6f

    goto :goto_1

    :cond_3
    const/16 v5, 0x7e

    goto :goto_1

    :cond_4
    move v5, v6

    :cond_5
    :goto_1
    const/16 v4, 0x55

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword;->oldPassword:Ljava/lang/String;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword;->newPassword:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword;->accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword$Handler;->handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public handlerClass()Ljava/lang/Class;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword$Handler;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword;->a:Ljava/lang/String;

    return-object v0
.end method
