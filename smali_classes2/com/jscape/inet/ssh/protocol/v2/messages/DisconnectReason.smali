.class public final enum Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AUTH_CANCELLED_BY_USER:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

.field public static final enum BY_APPLICATION:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

.field public static final enum COMPRESSION_ERROR:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

.field public static final enum CONNECTION_LOST:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

.field public static final enum HOST_KEY_NOT_VERIFIABLE:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

.field public static final enum ILLEGAL_USER_NAME:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

.field public static final enum KEY_EXCHANGE_FAILED:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

.field public static final enum MAC_ERROR:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

.field public static final enum NOT_ALLOWED_TO_CONNECT:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

.field public static final enum NO_MORE_AUTH_METHODS_AVAILABLE:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

.field public static final enum PROTOCOL_ERROR:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

.field public static final enum PROTOCOL_VERSION_NOT_SUPPORTED:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

.field public static final enum RESERVED:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

.field public static final enum SERVICE_NOT_AVAILABLE:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

.field public static final enum TOO_MANY_CONNECTIONS:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

.field private static final synthetic a:[Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;


# instance fields
.field public final code:I


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0xf

    new-array v1, v0, [Ljava/lang/String;

    const/16 v3, 0xe

    const/4 v4, 0x0

    const-string v5, "\u001d\u0002}`ZvZ\u0016\u0018cuCiX\u0015\u000c\u001epwCeS\u0000\u0015muUg@\u001e\u0012n`HjS\t\u0012\u001aa~OtD\u0010\t\u0013\u0014\u001e{~O~U\u0017\u001alfOyP\u001e\u0012ndN\u0016\u001e\u000eviUeW\u0011\u0018gmFcR\u0000\u0019{~_uS\r\u000f\u001c\u0014loOeB\u0016\u0014l~FiE\u000b\u0014\u000b\u0014m~GgX\u0006\u0004anDhS\u001c\u000fknDu\u0016\u0011\u0014v~KjZ\u0010\u000cgeUrY\u0000\u0018moDcU\u000b\u001e\u0011\u0014}lEtS\u0000\u001awuBy[\u001a\u000fjnNuI\u001e\rchFgT\u0013\u001e\u001e\u000f\tmuEeY\u0013\u0004tdXu_\u0010\u0015}oErI\u000c\u000erqEtB\u001a\u001f\u0017\u0017\u0014quUmS\u0006\u0004ln^y@\u001a\tkgCgT\u0013\u001e\u0011\u001c\u0014oqXcE\u000c\u0012moUcD\r\u0014p\u000e\u000f\tmuEeY\u0013\u0004gsXiD"

    const/16 v6, 0x10c

    move v8, v3

    move v9, v4

    const/4 v7, -0x1

    :goto_0
    const/16 v10, 0x17

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    move v15, v4

    :goto_2
    const/4 v2, 0x3

    const/4 v0, 0x2

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_1

    add-int/lit8 v0, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v0

    const/16 v0, 0xf

    goto :goto_0

    :cond_0
    const/16 v6, 0x1a

    const/16 v2, 0x11

    const-string v5, "hi\u0010\u001a3\u0019$~p\u000f\u001a&\u0007&`h\u0019\u0008s`\u000f\u001a&\u000e-e"

    move v9, v0

    move v8, v2

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v8, v0

    move v9, v12

    :goto_3
    const/16 v10, 0x69

    add-int/2addr v7, v11

    add-int v0, v7, v8

    invoke-virtual {v5, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v13, v4

    const/16 v0, 0xf

    goto :goto_1

    :cond_2
    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    const/4 v6, 0x7

    aget-object v7, v1, v6

    invoke-direct {v5, v7, v4, v11}, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->NOT_ALLOWED_TO_CONNECT:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    const/16 v7, 0xc

    aget-object v8, v1, v7

    invoke-direct {v5, v8, v11, v0}, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->PROTOCOL_ERROR:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aget-object v8, v1, v2

    invoke-direct {v5, v8, v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->KEY_EXCHANGE_FAILED:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aget-object v8, v1, v3

    const/4 v9, 0x4

    invoke-direct {v5, v8, v2, v9}, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->RESERVED:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aget-object v8, v1, v0

    const/4 v10, 0x5

    invoke-direct {v5, v8, v9, v10}, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->MAC_ERROR:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    const/16 v8, 0xb

    aget-object v9, v1, v8

    const/4 v12, 0x6

    invoke-direct {v5, v9, v10, v12}, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->COMPRESSION_ERROR:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aget-object v9, v1, v11

    invoke-direct {v5, v9, v12, v6}, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->SERVICE_NOT_AVAILABLE:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    const/16 v9, 0x9

    aget-object v10, v1, v9

    const/16 v13, 0x8

    invoke-direct {v5, v10, v6, v13}, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->PROTOCOL_VERSION_NOT_SUPPORTED:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    const/16 v10, 0xa

    aget-object v14, v1, v10

    invoke-direct {v5, v14, v13, v9}, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->HOST_KEY_NOT_VERIFIABLE:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    const/4 v14, 0x5

    aget-object v15, v1, v14

    invoke-direct {v5, v15, v9, v10}, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->CONNECTION_LOST:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aget-object v14, v1, v4

    invoke-direct {v5, v14, v10, v8}, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->BY_APPLICATION:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aget-object v14, v1, v12

    invoke-direct {v5, v14, v8, v7}, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->TOO_MANY_CONNECTIONS:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    const/4 v14, 0x4

    aget-object v15, v1, v14

    const/16 v14, 0xd

    invoke-direct {v5, v15, v7, v14}, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->AUTH_CANCELLED_BY_USER:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aget-object v15, v1, v13

    invoke-direct {v5, v15, v14, v3}, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->NO_MORE_AUTH_METHODS_AVAILABLE:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aget-object v1, v1, v14

    const/16 v15, 0xf

    invoke-direct {v5, v1, v3, v15}, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->ILLEGAL_USER_NAME:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    new-array v1, v15, [Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    sget-object v15, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->NOT_ALLOWED_TO_CONNECT:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aput-object v15, v1, v4

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->PROTOCOL_ERROR:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aput-object v4, v1, v11

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->KEY_EXCHANGE_FAILED:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aput-object v4, v1, v0

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->RESERVED:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aput-object v0, v1, v2

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->MAC_ERROR:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    const/4 v2, 0x4

    aput-object v0, v1, v2

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->COMPRESSION_ERROR:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    const/4 v2, 0x5

    aput-object v0, v1, v2

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->SERVICE_NOT_AVAILABLE:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aput-object v0, v1, v12

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->PROTOCOL_VERSION_NOT_SUPPORTED:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aput-object v0, v1, v6

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->HOST_KEY_NOT_VERIFIABLE:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aput-object v0, v1, v13

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->CONNECTION_LOST:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aput-object v0, v1, v9

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->BY_APPLICATION:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aput-object v0, v1, v10

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->TOO_MANY_CONNECTIONS:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aput-object v0, v1, v8

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->AUTH_CANCELLED_BY_USER:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aput-object v0, v1, v7

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->NO_MORE_AUTH_METHODS_AVAILABLE:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    aput-object v0, v1, v14

    aput-object v5, v1, v3

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->a:[Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    return-void

    :cond_3
    const/16 v16, 0xf

    aget-char v17, v12, v15

    rem-int/lit8 v3, v15, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v11, :cond_8

    if-eq v3, v0, :cond_7

    if-eq v3, v2, :cond_6

    const/4 v0, 0x4

    if-eq v3, v0, :cond_5

    const/4 v0, 0x5

    if-eq v3, v0, :cond_4

    move v0, v11

    goto :goto_4

    :cond_4
    const/16 v0, 0x31

    goto :goto_4

    :cond_5
    const/16 v0, 0x1d

    goto :goto_4

    :cond_6
    const/16 v0, 0x36

    goto :goto_4

    :cond_7
    const/16 v0, 0x35

    goto :goto_4

    :cond_8
    const/16 v0, 0x4c

    goto :goto_4

    :cond_9
    const/16 v0, 0x48

    :goto_4
    xor-int/2addr v0, v10

    xor-int v0, v17, v0

    int-to-char v0, v0

    aput-char v0, v12, v15

    add-int/lit8 v15, v15, 0x1

    move/from16 v0, v16

    const/16 v3, 0xe

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->code:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->a:[Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    invoke-virtual {v0}, [Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    return-object v0
.end method
