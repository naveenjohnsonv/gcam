.class public final enum Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum SSH_OPEN_ADMINISTRATIVELY_PROHIBITED:Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

.field public static final enum SSH_OPEN_CONNECT_FAILED:Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

.field public static final enum SSH_OPEN_RESOURCE_SHORTAGE:Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

.field public static final enum SSH_OPEN_UNKNOWN_CHANNEL_TYPE:Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

.field private static final synthetic a:[Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;


# instance fields
.field public final code:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "\u001fi1\u0004\u000f;,\u0002e,\u0015\u000b%&\u001bt&\u0018\u0008*\'\u0002\u007f5\u0004\u001429\t\u001a\u001fi1\u0004\u000f;,\u0002e+\u001e\u0013$<\u001ey<\u0004\u0013#&\u001en8\u001c\u0005"

    const/16 v6, 0x38

    move v9, v4

    const/4 v7, -0x1

    const/16 v8, 0x1d

    :goto_0
    const/16 v10, 0x76

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    move v15, v4

    :goto_2
    const/4 v2, 0x3

    const/4 v3, 0x2

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_1

    add-int/lit8 v2, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v2

    goto :goto_0

    :cond_0
    const/16 v6, 0x3c

    const/16 v3, 0x24

    const-string v5, "C5mXSgp^9dCQ~{Y5qU]c|F#i^Cgg_.lEUcpT\u0017C5mXSgp^9fHRypS2zA]~yU\""

    move v9, v2

    move v8, v3

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v2

    move v9, v12

    :goto_3
    const/16 v10, 0x2a

    add-int/2addr v7, v11

    add-int v2, v7, v8

    invoke-virtual {v5, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v13, v4

    goto :goto_1

    :cond_2
    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    aget-object v6, v1, v3

    invoke-direct {v5, v6, v4, v11}, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;->SSH_OPEN_ADMINISTRATIVELY_PROHIBITED:Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    aget-object v6, v1, v2

    invoke-direct {v5, v6, v11, v3}, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;->SSH_OPEN_CONNECT_FAILED:Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    aget-object v6, v1, v4

    invoke-direct {v5, v6, v3, v2}, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;->SSH_OPEN_UNKNOWN_CHANNEL_TYPE:Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    aget-object v1, v1, v11

    invoke-direct {v5, v1, v2, v0}, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;->SSH_OPEN_RESOURCE_SHORTAGE:Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;->SSH_OPEN_ADMINISTRATIVELY_PROHIBITED:Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;->SSH_OPEN_CONNECT_FAILED:Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    aput-object v1, v0, v11

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;->SSH_OPEN_UNKNOWN_CHANNEL_TYPE:Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    aput-object v1, v0, v3

    aput-object v5, v0, v2

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;->a:[Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v4, v15, 0x7

    if-eqz v4, :cond_9

    if-eq v4, v11, :cond_8

    if-eq v4, v3, :cond_7

    if-eq v4, v2, :cond_6

    if-eq v4, v0, :cond_5

    const/4 v2, 0x5

    if-eq v4, v2, :cond_4

    const/16 v2, 0x1f

    goto :goto_4

    :cond_4
    const/16 v2, 0x1d

    goto :goto_4

    :cond_5
    const/16 v2, 0x36

    goto :goto_4

    :cond_6
    const/16 v2, 0x2d

    goto :goto_4

    :cond_7
    const/16 v2, 0xf

    goto :goto_4

    :cond_8
    const/16 v2, 0x4c

    goto :goto_4

    :cond_9
    const/16 v2, 0x3a

    :goto_4
    xor-int/2addr v2, v10

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v12, v15

    add-int/lit8 v15, v15, 0x1

    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;->code:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;->a:[Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    invoke-virtual {v0}, [Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    return-object v0
.end method
