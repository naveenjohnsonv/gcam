.class public Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelWindowAdjust$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelExtendedData$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelEof$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelClose$Handler;


# static fields
.field protected static final WRITE_ALLOWED_TIMEOUT:Lcom/jscape/util/Time;

.field private static final i:[Ljava/lang/String;


# instance fields
.field protected volatile closed:Z

.field protected final connection:Lcom/jscape/util/k/a/x;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;"
        }
    .end annotation
.end field

.field protected final listeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Listener;",
            ">;"
        }
    .end annotation
.end field

.field protected final localId:I

.field protected final localMaxPacketSize:I

.field protected final localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

.field protected final localWindowMinSize:J

.field protected final logger:Ljava/util/logging/Logger;

.field protected final remoteId:I

.field protected final remoteMaxPacketSize:I

.field protected final remoteWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

.field protected writer:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Writer;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "\u000b?\u0003nJEKpv\u0001eFSjNq<hSA\u001a\u000f\u000b?\u001ddDKSBH\u0006oMKP\u001a\u0015\u000b?\u0003nJEKj~\u0017QHGLBk<hSA\u001a\u001abm\u001dn[\u0004ttWOrLJCNq\u0008!DATT~\u0008d\u0007 e~\u000b!EKDFsOlH\\\u0007W~\u000cjLP\u0007Tv\u0015d\tRFKj\n/\t\u000b?\u0018s@PBU\"\t\u000b?\u000cmFWBC\"\u0013Dp\u0002/CWDFo\n/@JBS1\u001crA\u0016\u000b?\u001ddDKSBR\u000eyyEDLz\u001bR@^B\u001a\u0011dw\u000eoGAK\u0007d\u0003nJEKn{R\u000b\u000b?\u001ddDKSBV\u000b<"

    const/16 v4, 0xd4

    const/16 v5, 0x15

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x19

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    const/16 v14, 0x30

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v3, 0xe

    const-string v4, "+\u001f#NjekPV!Efs:!E^+\u0001{ajhK*\u0001de\u007f\'O.Bbas\'L&[l$qfS:D\'"

    move v5, v3

    move-object v3, v4

    move v7, v10

    move v4, v14

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x39

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->i:[Ljava/lang/String;

    const-wide/16 v0, 0x1

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->millis(J)Lcom/jscape/util/Time;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->WRITE_ALLOWED_TIMEOUT:Lcom/jscape/util/Time;

    return-void

    :cond_3
    aget-char v15, v10, v13

    rem-int/lit8 v1, v13, 0x7

    const/16 v16, 0x3e

    if-eqz v1, :cond_8

    if-eq v1, v9, :cond_7

    const/4 v2, 0x2

    if-eq v1, v2, :cond_6

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    const/4 v2, 0x4

    if-eq v1, v2, :cond_9

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    goto :goto_4

    :cond_4
    const/16 v14, 0x3d

    goto :goto_5

    :cond_5
    const/16 v14, 0x18

    goto :goto_5

    :cond_6
    const/16 v14, 0x76

    goto :goto_5

    :cond_7
    const/4 v14, 0x6

    goto :goto_5

    :cond_8
    :goto_4
    move/from16 v14, v16

    :cond_9
    :goto_5
    xor-int v1, v8, v14

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/util/k/a/x;IIJIJI)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;IIJIJI)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->connection:Lcom/jscape/util/k/a/x;

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->localId:I

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteId:I

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-direct {p1, p4, p5}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;-><init>(J)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    const-wide/16 p1, 0x3

    div-long/2addr p4, p1

    iput-wide p4, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->localWindowMinSize:J

    int-to-long p1, p6

    sget-object p3, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->i:[Ljava/lang/String;

    const/4 p4, 0x4

    aget-object p4, p3, p4

    const-wide/16 v0, 0x0

    invoke-static {p1, p2, v0, v1, p4}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p6, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->localMaxPacketSize:I

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-direct {p1, p7, p8}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;-><init>(J)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    int-to-long p1, p9

    const/16 p4, 0xc

    aget-object p4, p3, p4

    invoke-static {p1, p2, v0, v1, p4}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p9, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteMaxPacketSize:I

    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->listeners:Ljava/util/Set;

    const/4 p1, 0x7

    aget-object p1, p3, p1

    invoke-static {p1}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->logger:Ljava/util/logging/Logger;

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;

    invoke-direct {p1, p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;-><init>(Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->writer:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Writer;

    return-void
.end method

.method private static a(Lcom/jscape/util/k/a/Connection$ConnectionException;)Lcom/jscape/util/k/a/Connection$ConnectionException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method protected adjustLocalWindow()V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->size()J

    move-result-wide v1

    if-nez v0, :cond_0

    iget-wide v3, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->localWindowMinSize:J

    cmp-long v0, v1, v3

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->space()J

    move-result-wide v1

    :cond_0
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelWindowAdjust;

    iget v3, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteId:I

    invoke-direct {v0, v3, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelWindowAdjust;-><init>(IJ)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v0, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->increase(J)V

    :cond_1
    return-void
.end method

.method public attributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->connection:Lcom/jscape/util/k/a/x;

    invoke-interface {v0}, Lcom/jscape/util/k/a/x;->attributes()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public bind(Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Listener;)V
    .locals 1

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public blockingWrite()V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->writer:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Writer;

    instance-of v0, v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;

    invoke-direct {v0, p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;-><init>(Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;)V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->writer:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Writer;

    :cond_1
    return-void
.end method

.method public close()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->closed:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->closed:Z

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelClose;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteId:I

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelClose;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->raiseClosedEvent()V

    :cond_1
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->writer:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Writer;

    invoke-interface {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Writer;->close()V

    return-void
.end method

.method public closed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->closed:Z

    return v0
.end method

.method public connection()Lcom/jscape/util/k/a/x;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->connection:Lcom/jscape/util/k/a/x;

    return-object v0
.end method

.method public creationTime()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->connection:Lcom/jscape/util/k/a/x;

    invoke-interface {v0}, Lcom/jscape/util/k/a/x;->creationTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public eof()V
    .locals 2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelEof;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteId:I

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelEof;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelClose;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelClose;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->close()V

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->raiseDataEvent(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;)V

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;->length:I

    invoke-virtual {p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->decrease(I)V

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->adjustLocalWindow()V

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelEof;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelEof;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->raiseEofEvent()V

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelExtendedData;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelExtendedData;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelWindowAdjust;Lcom/jscape/util/k/a/x;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelWindowAdjust;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->writer:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Writer;

    iget-wide v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelWindowAdjust;->bytesToAdd:J

    invoke-interface {p2, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Writer;->adjustWindow(J)V

    return-void
.end method

.method public localAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->connection:Lcom/jscape/util/k/a/x;

    invoke-interface {v0}, Lcom/jscape/util/k/a/x;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method public localId()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->localId:I

    return v0
.end method

.method protected logErrorSendingMessage(Ljava/lang/Throwable;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->i:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method protected nextDataPacketSize(I)I
    .locals 5

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteMaxPacketSize:I

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->size()J

    move-result-wide v1

    int-to-long v3, p1

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    long-to-int p1, v1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    return p1
.end method

.method protected raiseClosedEvent()V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->listeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Listener;

    invoke-interface {v2, p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Listener;->onClosed(Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;)V

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method protected raiseDataEvent(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->listeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Listener;

    iget-object v3, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;->data:[B

    invoke-interface {v2, p0, v3}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Listener;->onData(Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;[B)V

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method protected raiseEofEvent()V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->listeners:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Listener;

    invoke-interface {v2, p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Listener;->onEof(Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;)V

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method public remoteAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->connection:Lcom/jscape/util/k/a/x;

    invoke-interface {v0}, Lcom/jscape/util/k/a/x;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method public remoteId()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteId:I

    return v0
.end method

.method protected send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->connection:Lcom/jscape/util/k/a/x;

    invoke-interface {v0, p1}, Lcom/jscape/util/k/a/x;->write(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->logErrorSendingMessage(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method protected sendData([BIIZ)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz p4, :cond_0

    :try_start_0
    iget-object p4, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->connection:Lcom/jscape/util/k/a/x;

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelExtendedData;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteId:I

    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;->SSH_EXTENDED_DATA_STDERR:Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;

    move-object v1, v7

    move-object v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelExtendedData;-><init>(ILcom/jscape/inet/ssh/protocol/v2/messages/DataType;[BII)V

    invoke-interface {p4, v7}, Lcom/jscape/util/k/a/x;->write(Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->a(Lcom/jscape/util/k/a/Connection$ConnectionException;)Lcom/jscape/util/k/a/Connection$ConnectionException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    iget-object p4, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->connection:Lcom/jscape/util/k/a/x;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteId:I

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;-><init>(I[BII)V

    invoke-interface {p4, v0}, Lcom/jscape/util/k/a/x;->write(Ljava/lang/Object;)V
    :try_end_1
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->decrease(I)V

    return-void

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->a(Lcom/jscape/util/k/a/Connection$ConnectionException;)Lcom/jscape/util/k/a/Connection$ConnectionException;

    move-result-object p1

    throw p1
.end method

.method public sendRequestFailure(Z)V
    .locals 1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure;

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteId:I

    invoke-direct {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure;-><init>(I)V

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    :cond_0
    return-void
.end method

.method public sendRequestSuccess(Z)V
    .locals 1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelSuccess;

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteId:I

    invoke-direct {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelSuccess;-><init>(I)V

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->i:[Ljava/lang/String;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->localId:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v2, 0xa

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteId:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v2, 0xb

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->localWindowMinSize:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->localMaxPacketSize:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteMaxPacketSize:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->writer:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Writer;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->closed:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unbind(Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Listener;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->listeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public write([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->write([BIIZ)V

    return-void
.end method

.method protected write([BIIZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-lez p3, :cond_1

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->closed:Z
    :try_end_0
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_2

    if-nez v0, :cond_2

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->writer:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Writer;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Writer;->write([BIIZ)I

    move-result v1

    add-int/2addr p2, v1

    sub-int/2addr p3, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->a(Lcom/jscape/util/k/a/Connection$ConnectionException;)Lcom/jscape/util/k/a/Connection$ConnectionException;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->a(Lcom/jscape/util/k/a/Connection$ConnectionException;)Lcom/jscape/util/k/a/Connection$ConnectionException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->closed:Z

    :cond_2
    if-nez v1, :cond_3

    return-void

    :cond_3
    :try_start_2
    new-instance p1, Lcom/jscape/util/k/a/Connection$ConnectionClosedException;

    invoke-direct {p1}, Lcom/jscape/util/k/a/Connection$ConnectionClosedException;-><init>()V

    throw p1
    :try_end_2
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->a(Lcom/jscape/util/k/a/Connection$ConnectionException;)Lcom/jscape/util/k/a/Connection$ConnectionException;

    move-result-object p1

    throw p1
.end method

.method public writeStderr([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->write([BIIZ)V

    return-void
.end method
