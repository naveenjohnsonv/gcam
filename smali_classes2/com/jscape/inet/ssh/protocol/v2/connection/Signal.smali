.class public final enum Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum SIG_ABRT:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

.field public static final enum SIG_ALRM:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

.field public static final enum SIG_FPE:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

.field public static final enum SIG_HUP:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

.field public static final enum SIG_ILL:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

.field public static final enum SIG_INT:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

.field public static final enum SIG_KILL:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

.field public static final enum SIG_PIPE:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

.field public static final enum SIG_QUIT:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

.field public static final enum SIG_SEGV:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

.field public static final enum SIG_TERM:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

.field public static final enum SIG_USR1:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

.field public static final enum SIG_USR2:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

.field private static final synthetic a:[Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;


# instance fields
.field public final code:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0x1a

    new-array v1, v0, [Ljava/lang/String;

    const/16 v3, 0x8

    const/4 v4, 0x0

    const-string v5, "\u0014M^u]f7u\u0004\u0012WK\u001b\u0008\u0014M^uC|)\u000b\u0007\u0014M^uNe \u0004\u0017MIo\u0007\u0014M^uA{1\u0003\u0001T\\\u0008\u0014M^uIw7\u0013\u0004\u0006FK~\u0008\u0014M^u[p\"\u0011\u0004\u0012WK\u0018\u0003\u000eJM\u0004\u0006HKg\u0007\u0014M^u@`5\u0008\u0014M^uY`,\u0013\u0004\u0016QP~\u0004\u000cMUf\u0007\u0014M^uAy)\u0008\u0014M^uIy7\n\u0008\u0014M^uX|5\u0002\u0008\u0014M^u\\p7\n\u0003\u000fQI\u0008\u0014M^u]f7v\u0004\u0013AKg"

    const/16 v6, 0xa4

    move v8, v3

    move v9, v4

    const/4 v7, -0x1

    :goto_0
    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v8

    invoke-virtual {v5, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v13, v0

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v14, v11

    move v15, v4

    :goto_2
    const/16 v16, 0x12

    const/4 v0, 0x2

    const/4 v2, 0x3

    if-gt v14, v15, :cond_3

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v13}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v11

    if-eqz v12, :cond_1

    add-int/lit8 v0, v9, 0x1

    aput-object v11, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v0

    const/16 v0, 0x1a

    goto :goto_0

    :cond_0
    const-string v5, "3uh\u0004)|cA"

    move v9, v0

    move v8, v2

    move v6, v3

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v9, 0x1

    aput-object v11, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v8, v0

    move v9, v12

    :goto_3
    const/16 v13, 0x27

    add-int/2addr v7, v10

    add-int v0, v7, v8

    invoke-virtual {v5, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v4

    const/16 v0, 0x1a

    goto :goto_1

    :cond_2
    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    const/4 v6, 0x7

    aget-object v7, v1, v6

    aget-object v8, v1, v3

    invoke-direct {v5, v7, v4, v8}, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_ABRT:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    aget-object v7, v1, v16

    const/16 v8, 0xc

    aget-object v9, v1, v8

    invoke-direct {v5, v7, v10, v9}, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_ALRM:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    aget-object v7, v1, v2

    const/4 v9, 0x6

    aget-object v11, v1, v9

    invoke-direct {v5, v7, v0, v11}, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_FPE:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    const/16 v7, 0xd

    aget-object v11, v1, v7

    const/16 v12, 0x15

    aget-object v12, v1, v12

    invoke-direct {v5, v11, v2, v12}, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_HUP:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    const/16 v11, 0x11

    aget-object v11, v1, v11

    const/16 v12, 0x18

    aget-object v12, v1, v12

    const/4 v13, 0x4

    invoke-direct {v5, v11, v13, v12}, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_ILL:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    const/4 v11, 0x5

    aget-object v12, v1, v11

    const/16 v13, 0xb

    aget-object v14, v1, v13

    invoke-direct {v5, v12, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_INT:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    aget-object v11, v1, v0

    const/16 v12, 0x10

    aget-object v12, v1, v12

    invoke-direct {v5, v11, v9, v12}, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_KILL:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    const/16 v11, 0x13

    aget-object v11, v1, v11

    const/4 v12, 0x4

    aget-object v14, v1, v12

    invoke-direct {v5, v11, v6, v14}, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_PIPE:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    const/16 v11, 0xe

    aget-object v11, v1, v11

    const/16 v12, 0xf

    aget-object v12, v1, v12

    invoke-direct {v5, v11, v3, v12}, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_QUIT:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    const/16 v11, 0x9

    aget-object v12, v1, v11

    const/16 v14, 0x19

    aget-object v14, v1, v14

    invoke-direct {v5, v12, v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_SEGV:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    const/16 v12, 0x14

    aget-object v12, v1, v12

    const/16 v14, 0x17

    aget-object v14, v1, v14

    const/16 v15, 0xa

    invoke-direct {v5, v12, v15, v14}, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_TERM:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    const/16 v12, 0x16

    aget-object v12, v1, v12

    aget-object v14, v1, v10

    invoke-direct {v5, v12, v13, v14}, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_USR1:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    aget-object v12, v1, v4

    aget-object v1, v1, v15

    invoke-direct {v5, v12, v8, v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_USR2:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    new-array v1, v7, [Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_ABRT:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    aput-object v7, v1, v4

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_ALRM:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    aput-object v4, v1, v10

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_FPE:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    aput-object v4, v1, v0

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_HUP:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    aput-object v0, v1, v2

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_ILL:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    const/4 v2, 0x4

    aput-object v0, v1, v2

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_INT:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    const/4 v2, 0x5

    aput-object v0, v1, v2

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_KILL:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    aput-object v0, v1, v9

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_PIPE:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    aput-object v0, v1, v6

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_QUIT:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    aput-object v0, v1, v3

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_SEGV:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    aput-object v0, v1, v11

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_TERM:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    aput-object v0, v1, v15

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->SIG_USR1:Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    aput-object v0, v1, v13

    aput-object v5, v1, v8

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->a:[Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    return-void

    :cond_3
    aget-char v17, v11, v15

    rem-int/lit8 v3, v15, 0x7

    if-eqz v3, :cond_8

    if-eq v3, v10, :cond_7

    if-eq v3, v0, :cond_6

    if-eq v3, v2, :cond_5

    const/4 v0, 0x4

    if-eq v3, v0, :cond_9

    const/4 v0, 0x5

    if-eq v3, v0, :cond_4

    const/16 v16, 0x7f

    goto :goto_4

    :cond_4
    const/16 v16, 0x2f

    goto :goto_4

    :cond_5
    const/16 v16, 0x30

    goto :goto_4

    :cond_6
    move/from16 v16, v2

    goto :goto_4

    :cond_7
    const/16 v16, 0x1e

    goto :goto_4

    :cond_8
    const/16 v16, 0x5d

    :cond_9
    :goto_4
    xor-int v0, v13, v16

    xor-int v0, v17, v0

    int-to-char v0, v0

    aput-char v0, v11, v15

    add-int/lit8 v15, v15, 0x1

    const/16 v0, 0x1a

    const/16 v3, 0x8

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->code:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->a:[Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    invoke-virtual {v0}, [Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;

    return-object v0
.end method
