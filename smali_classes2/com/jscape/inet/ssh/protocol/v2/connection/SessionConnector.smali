.class public Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/k/a/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/k/a/v<",
        "Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;

.field private static final n:[Ljava/lang/String;


# instance fields
.field private final c:Lcom/jscape/util/k/a/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/k/a/v<",
            "Lcom/jscape/util/k/a/C;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

.field private final e:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;

.field private final f:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;

.field private final g:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;

.field private final h:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;

.field private final i:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

.field private final j:Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

.field private final k:I

.field private final l:I

.field private final m:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x4

    const-string v4, "\u001bOU(#c\u000f\u000f~S\'@MA\u0001tM=OM\u0004LpJnu\u000f\u0012L-\u0013p\u000e\u000f\u00121+\u001ek]#e\u0014\u0018vQ\'@MA\u0001tM=OM\u0004LpJnu\u000f\u0012L-\u0013p\u000e\u000f\u00121+\u001ek]$y2$1]!@D\u0004\u000feW!@\n\u0002\u001et_:KN[LJ\u001b=\u000e\u0016LR1\u001b=s\u0004\u0010e\u0011\tc_:GE\u000fLtL<AXO\u0003\u0018O\\!\u007f\u000f\u001fdN>AX\u0015\tu\u001e>\\E\u0015\u0003rQ\"\u000e\\\u0004\u001ebW!@\u0010AIb\u0010(\u007f\u000f\u001fdN>AX\u0015\tu\u001e=KX\u0017\tc\u001e>\\E\u0015\u0003rQ\"\u000e\\\u0004\u001ebW!@\u0010AIb\u0010\u001eh\u0000\u00081W G^\u0008\r}\u001e9GD\u0005\u0003f\u001e=GP\u0004Lg_\"[OO\u001ah\u0000\u00081S/V\n\u0011\rrU+Z\n\u0012\u0005k[nXK\r\u0019t\u0010"

    const/16 v5, 0x10b

    move v7, v1

    const/4 v6, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x75

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    const/4 v15, 0x5

    const/4 v2, 0x2

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v11, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const-string v4, "o/\u0002o/"

    move v7, v2

    move v8, v11

    move v5, v15

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v2, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v2

    :goto_3
    const/16 v9, 0x55

    add-int/2addr v6, v10

    add-int v2, v6, v7

    invoke-virtual {v4, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->n:[Ljava/lang/String;

    const/16 v1, 0xb

    aget-object v0, v0, v1

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->b:Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;-><init>()V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->a:Lcom/jscape/util/h/I;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v3, v14, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v10, :cond_8

    if-eq v3, v2, :cond_7

    const/4 v2, 0x3

    if-eq v3, v2, :cond_6

    if-eq v3, v1, :cond_5

    if-eq v3, v15, :cond_4

    const/16 v2, 0x5b

    goto :goto_4

    :cond_4
    const/16 v2, 0x3b

    goto :goto_4

    :cond_5
    const/16 v2, 0x4b

    goto :goto_4

    :cond_6
    const/16 v2, 0x64

    goto :goto_4

    :cond_7
    const/16 v2, 0x19

    goto :goto_4

    :cond_8
    const/16 v2, 0x14

    goto :goto_4

    :cond_9
    const/16 v2, 0x5f

    :goto_4
    xor-int/2addr v2, v9

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/util/k/a/v;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;IILjava/util/logging/Logger;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/v<",
            "Lcom/jscape/util/k/a/C;",
            ">;",
            "Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;",
            "Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;",
            "Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;",
            "Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;",
            "II",
            "Ljava/util/logging/Logger;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->c:Lcom/jscape/util/k/a/v;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->d:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->e:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->f:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->g:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;

    invoke-static {p6}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p6, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->h:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;

    invoke-static {p7}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p7, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->i:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    invoke-static {p8}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p8, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->j:Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    int-to-long p1, p9

    sget-object p3, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->n:[Ljava/lang/String;

    const/16 p4, 0x8

    aget-object p4, p3, p4

    const-wide/16 p5, 0x0

    invoke-static {p1, p2, p5, p6, p4}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p9, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->k:I

    int-to-long p1, p10

    const/16 p4, 0x9

    aget-object p3, p3, p4

    invoke-static {p1, p2, p5, p6, p3}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p10, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->l:I

    invoke-static {p11}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p11, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->m:Ljava/util/logging/Logger;

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_0

    const-string p1, "RGxB5"

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private static a(Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException;)Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/util/k/a/B;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/B<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->d:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    invoke-direct {p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->a(Lcom/jscape/inet/ssh/protocol/messages/Message;Lcom/jscape/util/k/a/x;)V

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->b(Lcom/jscape/util/k/a/B;)Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    invoke-direct {v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)V

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportMessages;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->d:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    invoke-direct {v1, v2, v3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;)V

    iget-object v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->packetCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;

    invoke-virtual {p1, v2}, Lcom/jscape/util/k/a/B;->a(Lcom/jscape/util/h/I;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->a(Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;)V

    return-object v1
.end method

.method private a(Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->n:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->protocolVersion:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    :try_start_1
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->n:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v1, v0, v1
    :try_end_1
    .catch Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->protocolVersion:Ljava/lang/String;

    goto :goto_1

    :cond_1
    :goto_0
    move-object v0, v1

    :goto_1
    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->d:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->protocolVersion:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-void

    :cond_2
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException;

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->protocolVersion:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$1;)V

    throw v0
    :try_end_2
    .catch Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->a(Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException;)Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->a(Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException;)Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->a(Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException;)Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException;

    move-result-object p1

    throw p1
.end method

.method private a(Lcom/jscape/inet/ssh/protocol/messages/Message;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0, p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->b(Lcom/jscape/util/k/a/r;Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    invoke-interface {p2, p1}, Lcom/jscape/util/k/a/x;->write(Ljava/lang/Object;)V

    return-void
.end method

.method private a(Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;Ljava/lang/String;Lcom/jscape/util/k/a/x;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;",
            "Ljava/lang/String;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDisconnect;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->n:[Ljava/lang/String;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-direct {v0, p1, p2, v1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDisconnect;-><init>(Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0, p3}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->a(Lcom/jscape/inet/ssh/protocol/messages/Message;Lcom/jscape/util/k/a/x;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-direct {p0, p3}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->a(Lcom/jscape/util/k/a/x;)V

    return-void

    :goto_1
    invoke-direct {p0, p3}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->a(Lcom/jscape/util/k/a/x;)V

    throw p1
.end method

.method private a(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceRequest;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;->USERAUTH:Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceRequest;-><init>(Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;)V

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->read()Lcom/jscape/inet/ssh/protocol/messages/Message;

    return-void
.end method

.method private a(Lcom/jscape/util/k/a/r;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->n:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v0, v0, v3

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object p1

    aput-object p1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private a(Lcom/jscape/util/k/a/r;Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 7

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->n:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v0, v0, v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object p1

    aput-object p1, v4, v3

    const/4 p1, 0x2

    aput-object p2, v4, p1

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private a(Lcom/jscape/util/k/a/x;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/jscape/util/k/a/x;->closed()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-interface {p1}, Lcom/jscape/util/k/a/x;->close()V

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->n:[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method private b(Lcom/jscape/util/k/a/B;)Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">(",
            "Lcom/jscape/util/k/a/B<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)TM;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/jscape/util/k/a/B;->read()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/ssh/protocol/messages/Message;

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->a(Lcom/jscape/util/k/a/r;Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    return-object v0
.end method

.method private b(Lcom/jscape/util/k/a/r;Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 7

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->n:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v0, v0, v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-interface {p1}, Lcom/jscape/util/k/a/r;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object p1

    aput-object p1, v4, v5

    aput-object p2, v4, v3

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public connect()Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/c;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->c:Lcom/jscape/util/k/a/v;

    invoke-interface {v0}, Lcom/jscape/util/k/a/v;->connect()Lcom/jscape/util/k/a/r;

    move-result-object v0

    check-cast v0, Lcom/jscape/util/k/a/C;

    new-instance v10, Lcom/jscape/util/k/a/B;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->a:Lcom/jscape/util/h/I;

    invoke-direct {v10, v0, v1}, Lcom/jscape/util/k/a/B;-><init>(Lcom/jscape/util/k/a/C;Lcom/jscape/util/h/I;)V

    invoke-direct {p0, v10}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->a(Lcom/jscape/util/k/a/r;)V

    :try_start_0
    invoke-direct {p0, v10}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->a(Lcom/jscape/util/k/a/B;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    move-result-object v11

    new-instance v12, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->e:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;

    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->f:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;

    iget-object v6, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->g:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;

    iget-object v7, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->h:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;

    iget-object v8, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->i:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    iget-object v9, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->m:Ljava/util/logging/Logger;

    move-object v1, v12

    move-object v2, v10

    move-object v3, v11

    invoke-direct/range {v1 .. v9}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;-><init>(Lcom/jscape/util/k/a/A;Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Ljava/util/logging/Logger;)V

    invoke-virtual {v12}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->exchangeKeys()V

    invoke-direct {p0, v12}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->a(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->j:Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;->CONNECTION:Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/messages/ServiceType;->name:Ljava/lang/String;

    invoke-interface {v1, v12, v2}, Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;->applyTo(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;Ljava/lang/String;)V

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->messageCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnectionMessages;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->k:I

    iget v3, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->l:I

    invoke-direct {v1, v12, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;-><init>(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;II)V
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v1

    invoke-interface {v0}, Lcom/jscape/util/k/a/C;->close()V

    new-instance v0, Lcom/jscape/util/k/a/c;

    invoke-direct {v0, v1}, Lcom/jscape/util/k/a/c;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;->PROTOCOL_VERSION_NOT_SUPPORTED:Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->n:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v3, v2, v3

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException;->getMessage()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    aput-object v6, v5, v7

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v3, v10}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->a(Lcom/jscape/inet/ssh/protocol/v2/messages/DisconnectReason;Ljava/lang/String;Lcom/jscape/util/k/a/x;)V

    new-instance v1, Lcom/jscape/util/k/a/c;

    const/4 v3, 0x7

    aget-object v2, v2, v3

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector$UnsupportedServerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/jscape/util/k/a/c;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public bridge synthetic connect()Lcom/jscape/util/k/a/r;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/c;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;->connect()Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    move-result-object v0

    return-object v0
.end method
