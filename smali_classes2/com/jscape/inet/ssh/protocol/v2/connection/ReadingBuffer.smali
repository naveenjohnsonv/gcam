.class public Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;
.super Ljava/lang/Object;


# static fields
.field private static final e:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "[B>;"
        }
    .end annotation
.end field

.field private b:I

.field private volatile c:I

.field private volatile d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xc

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x56

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "$c0$Arhi!=7\u001d\u0012$c2\'Riaf7\u0001=Srpa,?o\u0016Z&06IucJ674Ei$s!==Cpw5"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x36

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->e:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/4 v13, 0x4

    if-eqz v12, :cond_6

    if-eq v12, v7, :cond_5

    const/4 v14, 0x2

    if-eq v12, v14, :cond_4

    if-eq v12, v0, :cond_7

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v13, 0x52

    goto :goto_2

    :cond_2
    const/16 v13, 0x4d

    goto :goto_2

    :cond_3
    const/16 v13, 0x76

    goto :goto_2

    :cond_4
    const/4 v13, 0x7

    goto :goto_2

    :cond_5
    const/16 v13, 0x15

    goto :goto_2

    :cond_6
    const/16 v13, 0x5e

    :cond_7
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->a:Ljava/util/List;

    return-void
.end method

.method private a()[B
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->a:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    if-nez v0, :cond_1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->b:I

    array-length v3, v1

    if-ge v0, v3, :cond_0

    return-object v1

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iput v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->b:I

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, p0

    :goto_0
    check-cast v0, [B

    return-object v0
.end method


# virtual methods
.method public available()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->c:I

    return v0
.end method

.method public close()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->d:Z

    return-void
.end method

.method public closed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->d:Z

    return v0
.end method

.method public empty()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->c:I

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public put([B)V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    array-length v1, p1

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->c:I

    array-length p1, p1

    add-int/2addr v0, p1

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->c:I

    return-void
.end method

.method public read([BII)I
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->empty()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->d:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    return p1

    :cond_2
    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->a()[B

    move-result-object v0

    array-length v1, v0

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->b:I

    sub-int/2addr v1, v2

    invoke-static {v1, p3}, Ljava/lang/Math;->min(II)I

    move-result p3

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->b:I

    invoke-static {v0, v1, p1, p2, p3}, Lcom/jscape/util/v;->a([BI[BII)[B

    iget p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->b:I

    add-int/2addr p1, p3

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->b:I

    iget p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->c:I

    sub-int/2addr p1, p3

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->c:I

    return p3
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->e:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->a:Ljava/util/List;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
