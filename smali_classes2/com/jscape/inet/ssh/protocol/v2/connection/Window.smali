.class public Lcom/jscape/inet/ssh/protocol/v2/connection/Window;
.super Ljava/lang/Object;


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field private final a:J

.field private volatile b:J


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x7

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v3

    :goto_0
    const/16 v6, 0x26

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v2, v4

    const-string v8, "0)\u0000\u0004`\"\u001e\u000f^h\u0017Mi.Yy)\u0005\u000cv2F2\u0014K`\u001d\tu0\u0003g`\u001d\u0004n.BpZ\u001a\u0017\u007fz"

    invoke-virtual {v8, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v3

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x2c

    if-ge v2, v4, :cond_0

    invoke-virtual {v8, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v2

    move v2, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/4 v13, 0x5

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v14, 0x2

    if-eq v12, v14, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v14, 0x4

    if-eq v12, v14, :cond_3

    if-eq v12, v13, :cond_2

    goto :goto_2

    :cond_2
    const/16 v13, 0x61

    goto :goto_2

    :cond_3
    const/16 v13, 0x3c

    goto :goto_2

    :cond_4
    const/16 v13, 0x4b

    goto :goto_2

    :cond_5
    const/16 v13, 0x55

    goto :goto_2

    :cond_6
    const/16 v13, 0x2f

    goto :goto_2

    :cond_7
    const/16 v13, 0x3a

    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(J)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->c:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    const-wide/16 v1, 0x0

    invoke-static {p1, p2, v1, v2, v0}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    iput-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->a:J

    iput-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->b:J

    return-void
.end method


# virtual methods
.method public decrease(I)V
    .locals 6

    iget-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->b:J

    iget-wide v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->b:J

    int-to-long v4, p1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->b:J

    return-void
.end method

.method public increase(J)V
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->b:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->b:J

    return-void
.end method

.method public size()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->b:J

    return-wide v0
.end method

.method public space()J
    .locals 4

    iget-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->a:J

    iget-wide v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->b:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->c:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
