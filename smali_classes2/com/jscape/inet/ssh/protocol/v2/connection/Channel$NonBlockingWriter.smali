.class public Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Writer;


# instance fields
.field private final a:Lcom/jscape/util/h/o;

.field private final b:Lcom/jscape/util/h/o;

.field private final c:Ljava/util/concurrent/locks/Lock;

.field final d:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;


# direct methods
.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;)V
    .locals 2

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->d:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jscape/util/h/o;

    iget v1, p1, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteMaxPacketSize:I

    mul-int/lit8 v1, v1, 0x2

    invoke-direct {v0, v1}, Lcom/jscape/util/h/o;-><init>(I)V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->a:Lcom/jscape/util/h/o;

    new-instance v0, Lcom/jscape/util/h/o;

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteMaxPacketSize:I

    mul-int/lit8 p1, p1, 0x2

    invoke-direct {v0, p1}, Lcom/jscape/util/h/o;-><init>(I)V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->b:Lcom/jscape/util/h/o;

    new-instance p1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->c:Ljava/util/concurrent/locks/Lock;

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/util/h/o;)V
    .locals 8

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/util/h/o;->b()I

    move-result v1

    const/4 v2, 0x0

    move v3, v2

    :cond_0
    if-lez v1, :cond_2

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->d:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;

    invoke-virtual {v4, v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->nextDataPacketSize(I)I

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-lez v4, :cond_2

    :try_start_1
    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->d:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;

    invoke-virtual {p1}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v6

    iget-object v7, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->b:Lcom/jscape/util/h/o;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-ne p1, v7, :cond_1

    const/4 v7, 0x1

    goto :goto_0

    :cond_1
    move v7, v2

    :goto_0
    :try_start_2
    invoke-virtual {v5, v6, v3, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->sendData([BIIZ)V

    add-int/2addr v3, v4

    sub-int/2addr v1, v4

    if-eqz v0, :cond_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    invoke-virtual {p1, v3}, Lcom/jscape/util/h/o;->b(I)Lcom/jscape/util/h/o;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception p1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->d:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->logErrorSendingMessage(Ljava/lang/Throwable;)V

    :goto_2
    return-void
.end method


# virtual methods
.method public adjustWindow(J)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->d:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->increase(J)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->a:Lcom/jscape/util/h/o;

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->a(Lcom/jscape/util/h/o;)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->b:Lcom/jscape/util/h/o;

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->a(Lcom/jscape/util/h/o;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
.end method

.method public close()V
    .locals 0

    return-void
.end method

.method public write([BIIZ)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->d:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;

    invoke-virtual {v1, p3}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->nextDataPacketSize(I)I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p4, :cond_0

    :try_start_1
    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->b:Lcom/jscape/util/h/o;
    :try_end_1
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->a:Lcom/jscape/util/h/o;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    if-nez v0, :cond_3

    if-lez v1, :cond_2

    :try_start_3
    invoke-virtual {v2}, Lcom/jscape/util/h/o;->e()Z

    move-result v3
    :try_end_3
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v0, :cond_1

    if-eqz v3, :cond_2

    :try_start_4
    iget-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->d:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;

    invoke-virtual {p3, p1, p2, v1, p4}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->sendData([BIIZ)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v1

    :cond_1
    move p3, v3

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    invoke-virtual {v2, p1, p2, p3}, Lcom/jscape/util/h/o;->write([BII)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :cond_3
    move p3, v1

    :goto_1
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return p3

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$NonBlockingWriter;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
.end method
