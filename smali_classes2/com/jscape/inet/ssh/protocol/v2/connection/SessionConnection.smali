.class public Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/k/a/r;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequestUnknown$Handler;


# static fields
.field private static h:Ljava/lang/String;

.field private static final i:[Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

.field private final b:I

.field private final c:I

.field private final d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final e:Ljava/util/logging/Logger;

.field private f:I

.field private g:Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->b(Ljava/lang/String;)V

    const/4 v2, 0x0

    const-string v3, "MOt\u007fPWV{Gq3\u0019NVaJ\u007f(\u0019JVuK0)XUJj\u0000$B[|+PISj\u000es7XWQjBc\u007fXKZ/@\u007f+\u0019JJ\u007f^\u007f-M\\[!\u001eJ\\b0K\u0019P\u007fK~6W^\u001f|Kc,PVQ/Mx>WWZc\u0000&Z@c*IIP}Zu;\u0019TZ|]q8\\\u0019^{\u000eKzJ\u0019\u0003\"\u00100zJd\u0005/\u000bc\u0013lA}qSJ\\n^uqPWZ{\u0000c,Q"

    const/16 v4, 0x9d

    const/16 v5, 0x1e

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x54

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x31

    const/16 v3, 0x1a

    const-string v5, "PRibIEZ2Cl!OAV2@d8A\u0004Ts_x\'\n\u0016WA\u007f-V\u0004Qw]i+JC\u0002\u007fV~1ECG<"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v17, v5

    move v5, v3

    move-object/from16 v3, v17

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x49

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->i:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    const/16 v16, 0x6d

    if-eqz v15, :cond_7

    if-eq v15, v9, :cond_6

    const/4 v1, 0x2

    if-eq v15, v1, :cond_5

    const/4 v1, 0x3

    if-eq v15, v1, :cond_4

    const/4 v1, 0x4

    if-eq v15, v1, :cond_8

    const/4 v1, 0x5

    if-eq v15, v1, :cond_8

    const/16 v16, 0x6b

    goto :goto_4

    :cond_4
    const/16 v16, 0xb

    goto :goto_4

    :cond_5
    const/16 v16, 0x44

    goto :goto_4

    :cond_6
    const/16 v16, 0x7a

    goto :goto_4

    :cond_7
    const/16 v16, 0x5b

    :cond_8
    :goto_4
    xor-int v1, v8, v16

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;II)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    int-to-long v0, p2

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->i:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v3, p1, v2

    const-wide/16 v4, 0x0

    invoke-static {v0, v1, v4, v5, v3}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->b:I

    int-to-long v0, p3

    const/4 p2, 0x5

    aget-object p2, p1, p2

    invoke-static {v0, v1, v4, v5, p2}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->c:I

    new-instance p2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p2, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x4

    aget-object p1, p1, p2

    invoke-static {p1}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e:Ljava/util/logging/Logger;

    return-void
.end method

.method private a()Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->b()Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object v1

    :cond_1
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    return-object v1
.end method

.method private static a(Lcom/jscape/util/k/a/Connection$ConnectionException;)Lcom/jscape/util/k/a/Connection$ConnectionException;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->i:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method private b()Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->read()Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object v1

    if-nez v0, :cond_1

    :try_start_0
    invoke-direct {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->b(Lcom/jscape/inet/ssh/protocol/messages/Message;)Z

    move-result v0
    :try_end_0
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    return-object v1

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a(Lcom/jscape/util/k/a/Connection$ConnectionException;)Lcom/jscape/util/k/a/Connection$ConnectionException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a(Lcom/jscape/util/k/a/Connection$ConnectionException;)Lcom/jscape/util/k/a/Connection$ConnectionException;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    invoke-direct {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->c(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->h:Ljava/lang/String;

    return-void
.end method

.method private b(Lcom/jscape/inet/ssh/protocol/messages/Message;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/messages/Message;->handlerClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result p1

    return p1
.end method

.method private c()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->g:Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;
    :try_end_0
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->g:Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;
    :try_end_1
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    :try_start_2
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->closed()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    return-void

    :cond_2
    new-instance v0, Lcom/jscape/util/k/a/Connection$ConnectionException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->i:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/util/k/a/Connection$ConnectionException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a(Lcom/jscape/util/k/a/Connection$ConnectionException;)Lcom/jscape/util/k/a/Connection$ConnectionException;

    move-result-object v0

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a(Lcom/jscape/util/k/a/Connection$ConnectionException;)Lcom/jscape/util/k/a/Connection$ConnectionException;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a(Lcom/jscape/util/k/a/Connection$ConnectionException;)Lcom/jscape/util/k/a/Connection$ConnectionException;

    move-result-object v0

    throw v0
.end method

.method private c(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    invoke-virtual {p1, p0, v0}, Lcom/jscape/inet/ssh/protocol/messages/Message;->accept(Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    :goto_0
    return-void
.end method

.method private d()I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->f:I

    if-nez v0, :cond_1

    const v0, 0x7fffffff

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->f:I

    :cond_0
    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->f:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->f:I

    :cond_1
    return v1
.end method

.method private d(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    :try_start_0
    instance-of p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;

    if-eqz p1, :cond_0

    return-void

    :cond_0
    new-instance p1, Lcom/jscape/util/k/a/Connection$ConnectionException;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->i:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Lcom/jscape/util/k/a/Connection$ConnectionException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a(Lcom/jscape/util/k/a/Connection$ConnectionException;)Lcom/jscape/util/k/a/Connection$ConnectionException;

    move-result-object p1

    throw p1
.end method

.method public static e()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->h:Ljava/lang/String;

    return-object v0
.end method

.method private e(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->i:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v0, v0, v3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public attributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->attributes()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public base()Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    return-object v0
.end method

.method public close()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->close()V

    return-void
.end method

.method public closed()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public creationTime()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->creationTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequestUnknown;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequestUnknown;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    iget-boolean p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequestUnknown;->wantReply:Z

    if-eqz p1, :cond_0

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgRequestFailure;

    invoke-direct {p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgRequestFailure;-><init>()V

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    :cond_0
    return-void
.end method

.method public hostKey()Ljava/security/PublicKey;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->session()Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;

    move-result-object v0

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->hostKey:Ljava/security/PublicKey;

    return-object v0
.end method

.method public localAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method public openDirectTcpIpChannel(Ljava/lang/String;ILjava/lang/String;IZ)Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    move-object v0, p0

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->c()V

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->d()I

    move-result v10

    iget-object v11, v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    new-instance v12, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenDirectTcpIp;

    iget v1, v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->b:I

    int-to-long v3, v1

    iget v5, v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->c:I

    move-object v1, v12

    move v2, v10

    move-object v6, p1

    move v7, p2

    move-object/from16 v8, p3

    move/from16 v9, p4

    invoke-direct/range {v1 .. v9}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenDirectTcpIp;-><init>(IJILjava/lang/String;ILjava/lang/String;I)V

    invoke-virtual {v11, v12}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a()Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->d(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    check-cast v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;

    new-instance v12, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->senderChannel:I

    iget v3, v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->b:I

    int-to-long v5, v3

    iget v7, v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->c:I

    iget-wide v8, v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->initialWindowSize:J

    iget v11, v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->maxPacketSize:I

    move-object v1, v12

    move v3, v10

    move v10, v11

    move/from16 v11, p5

    invoke-direct/range {v1 .. v11}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;-><init>(Lcom/jscape/util/k/a/A;IIJIJIZ)V

    iput-object v12, v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->g:Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;

    return-object v12
.end method

.method public openSessionChannel(Z)Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->c()V

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->d()I

    move-result v2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenSession;

    iget v3, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->b:I

    int-to-long v3, v3

    iget v5, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->c:I

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenSession;-><init>(IJI)V

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a()Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->d(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    check-cast v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;

    new-instance v11, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    iget v3, v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->senderChannel:I

    iget v4, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->b:I

    int-to-long v4, v4

    iget v6, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->c:I

    iget-wide v7, v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->initialWindowSize:J

    iget v9, v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;->maxPacketSize:I

    move-object v0, v11

    move v10, p1

    invoke-direct/range {v0 .. v10}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;-><init>(Lcom/jscape/util/k/a/A;IIJIJIZ)V

    iput-object v11, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->g:Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;

    check-cast v11, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    return-object v11
.end method

.method public remoteAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->a:Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/TransportConnection;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
