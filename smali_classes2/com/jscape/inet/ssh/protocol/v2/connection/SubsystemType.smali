.class public final enum Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum SFTP:Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;

.field private static final synthetic a:[Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x4

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x5e

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "#\u001b]u\u0004\u0003;}U"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x9

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;

    aget-object v2, v1, v8

    aget-object v1, v1, v3

    invoke-direct {v0, v2, v3, v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;->SFTP:Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;

    new-array v1, v8, [Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;

    aput-object v0, v1, v3

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;->a:[Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    if-eqz v13, :cond_7

    if-eq v13, v8, :cond_6

    if-eq v13, v0, :cond_5

    const/4 v14, 0x3

    if-eq v13, v14, :cond_4

    if-eq v13, v2, :cond_3

    const/4 v14, 0x5

    if-eq v13, v14, :cond_2

    const/16 v13, 0x32

    goto :goto_2

    :cond_2
    const/16 v13, 0x25

    goto :goto_2

    :cond_3
    const/16 v13, 0x8

    goto :goto_2

    :cond_4
    const/16 v13, 0x5b

    goto :goto_2

    :cond_5
    const/16 v13, 0x77

    goto :goto_2

    :cond_6
    const/16 v13, 0x23

    goto :goto_2

    :cond_7
    const/16 v13, 0xe

    :goto_2
    xor-int/2addr v13, v7

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;->name:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;->a:[Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;

    invoke-virtual {v0}, [Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;

    return-object v0
.end method
