.class public Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;
.super Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestExitStatus$Handler;


# instance fields
.field private b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/jscape/util/k/a/A;IIJIJIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/A<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;IIJIJIZ)V"
        }
    .end annotation

    invoke-direct/range {p0 .. p10}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;-><init>(Lcom/jscape/util/k/a/A;IIJIJIZ)V

    return-void
.end method


# virtual methods
.method public breakSignal(IZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestBreak;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->remoteId:I

    invoke-direct {v0, v1, p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestBreak;-><init>(IZI)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    invoke-virtual {p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->processRequestResult(Z)V

    return-void
.end method

.method public environmentVariable(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestEnv;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->remoteId:I

    invoke-direct {v0, v1, p3, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestEnv;-><init>(IZLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    invoke-virtual {p0, p3}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->processRequestResult(Z)V

    return-void
.end method

.method public exec(Ljava/lang/String;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestExec;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->remoteId:I

    invoke-direct {v0, v1, p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestExec;-><init>(IZLjava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    invoke-virtual {p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->processRequestResult(Z)V

    return-void
.end method

.method public exitStatus()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestExitStatus;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestExitStatus;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestExitStatus;->exitStatus:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->b:Ljava/lang/Integer;

    return-void
.end method

.method public pseudoTerminal(Ljava/lang/String;IIIILjava/util/List;Z)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IIII",
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/TerminalMode;",
            ">;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    move-object v0, p0

    new-instance v10, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;

    iget v2, v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->remoteId:I

    move-object v1, v10

    move/from16 v3, p7

    move-object v4, p1

    move v5, p2

    move v6, p3

    move v7, p4

    move/from16 v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;-><init>(IZLjava/lang/String;IIIILjava/util/List;)V

    invoke-virtual {p0, v10}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    move/from16 v1, p7

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->processRequestResult(Z)V

    return-void
.end method

.method public shell(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestShell;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->remoteId:I

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestShell;-><init>(IZ)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->processRequestResult(Z)V

    return-void
.end method

.method public signal(Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/connection/Signal;->code:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->signal(Ljava/lang/String;)V

    return-void
.end method

.method public signal(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestSignal;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->remoteId:I

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestSignal;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    return-void
.end method

.method public subsystem(Ljava/lang/String;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestSubsystem;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->remoteId:I

    invoke-direct {v0, v1, p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestSubsystem;-><init>(IZLjava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    invoke-virtual {p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->processRequestResult(Z)V

    return-void
.end method

.method public windowChange(IIII)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->remoteId:I

    move-object v0, v6

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;-><init>(IIIII)V

    invoke-virtual {p0, v6}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    return-void
.end method
