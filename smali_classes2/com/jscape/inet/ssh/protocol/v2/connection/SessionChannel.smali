.class public Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;
.super Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestEnv$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange$Handler;


# static fields
.field private static final j:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/TerminalMode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "^\u0014z`i\r+\u001bQq>\u000f^\u0014gfg\u0017/\u0017c|mn\u0017,O\u0016^\u0014gfg\u0017/\u0017yt{Z\u00198\u0019QaPc\u0002>O\u0015^\u0014yli\u00197?UmSk\u001b0\u0017@Fjp\u001df\u000b^\u0014gfg\u0017/\u0017}q>\u0018!Qfpc\u001751\\tmd\u001d7ROyli\u00197;P(\u0017^\u0014pm|\u0011)\u001dZxfd\u000c\r\u0013F|bh\u0014>\u0001\t\t^\u0014voe\u000b>\u0016\t"

    const/16 v4, 0x8f

    const/16 v5, 0xb

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x3b

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x4

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v13, v10

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v12, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x24

    const/16 v3, 0xe

    const-string v5, "a+FSV&\u0008\u001abDXZ0Y\u0015a+FSV&\u0008\u001abDXZ0)$eyUO\"Y"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    add-int/2addr v6, v9

    add-int v8, v6, v5

    invoke-virtual {v3, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v8, v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->j:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v10, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v9, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    if-eq v1, v11, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v1, 0x60

    goto :goto_4

    :cond_4
    const/16 v1, 0x43

    goto :goto_4

    :cond_5
    const/16 v1, 0x31

    goto :goto_4

    :cond_6
    const/16 v1, 0x38

    goto :goto_4

    :cond_7
    const/16 v1, 0x2e

    goto :goto_4

    :cond_8
    const/16 v1, 0xf

    goto :goto_4

    :cond_9
    const/16 v1, 0x49

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v10, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/util/k/a/x;IIJIJI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;IIJIJI)V"
        }
    .end annotation

    invoke-direct/range {p0 .. p9}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;-><init>(Lcom/jscape/util/k/a/x;IIJIJI)V

    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->a:Ljava/util/Map;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->h:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public environmentVariables()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->a:Ljava/util/Map;

    return-object v0
.end method

.method public exitStatus(I)V
    .locals 2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestExitStatus;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->remoteId:I

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestExitStatus;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestEnv;Lcom/jscape/util/k/a/x;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestEnv;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->a:Ljava/util/Map;

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestEnv;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestEnv;->value:Ljava/lang/String;

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestEnv;->wantReply:Z

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->sendRequestSuccess(Z)V

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    iget-object p2, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalEnvironmentVariable:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->c:Ljava/lang/String;

    iget p2, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalCharactersWidth:I

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->d:I

    iget p2, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalRowsHeight:I

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->e:I

    iget p2, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalPixelsWidth:I

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->f:I

    iget p2, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalPixelsHeight:I

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->g:I

    iget-object p2, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalModes:Ljava/util/List;

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->h:Ljava/util/List;

    iget-boolean p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->wantReply:Z

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->sendRequestSuccess(Z)V

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    iget p2, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->terminalCharactersWidth:I

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->d:I

    iget p2, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->terminalRowsHeight:I

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->e:I

    iget p2, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->terminalPixelsWidth:I

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->f:I

    iget p2, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->terminalPixelsHeight:I

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->g:I

    iget-boolean p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->wantReply:Z

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->sendRequestSuccess(Z)V

    return-void
.end method

.method public occupied()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->b:Z

    return v0
.end method

.method public setOccupied()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->b:Z

    return-void
.end method

.method public terminalCharactersWidth()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->d:I

    return v0
.end method

.method public terminalEnvironmentVariable()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->c:Ljava/lang/String;

    return-object v0
.end method

.method public terminalModes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/TerminalMode;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->h:Ljava/util/List;

    return-object v0
.end method

.method public terminalPixelsHeight()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->g:I

    return v0
.end method

.method public terminalPixelsWidth()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->f:I

    return v0
.end method

.method public terminalRowsHeight()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->e:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->j:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->localId:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->remoteId:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->localWindowMinSize:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->localMaxPacketSize:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->remoteWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->remoteMaxPacketSize:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->a:Ljava/util/Map;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->b:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannel;->closed:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
