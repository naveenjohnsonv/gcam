.class public Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$Writer;


# instance fields
.field private final a:Ljava/util/concurrent/locks/Lock;

.field private final b:Ljava/util/concurrent/locks/Condition;

.field private volatile c:Z

.field final d:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;


# direct methods
.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->d:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->b:Ljava/util/concurrent/locks/Condition;

    return-void
.end method

.method private static a(Ljava/lang/InterruptedException;)Ljava/lang/InterruptedException;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->d:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->size()J

    move-result-wide v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->c:Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2

    if-nez v0, :cond_0

    if-nez v1, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->b:Ljava/util/concurrent/locks/Condition;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->WRITE_ALLOWED_TIMEOUT:Lcom/jscape/util/Time;

    iget-wide v2, v2, Lcom/jscape/util/Time;->value:J

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->WRITE_ALLOWED_TIMEOUT:Lcom/jscape/util/Time;

    iget-object v4, v4, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/locks/Condition;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    if-eqz v0, :cond_0

    :cond_1
    return-void

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->a(Ljava/lang/InterruptedException;)Ljava/lang/InterruptedException;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->a(Ljava/lang/InterruptedException;)Ljava/lang/InterruptedException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public adjustWindow(J)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->d:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->remoteWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->increase(J)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
.end method

.method public close()V
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->c:Z

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->b:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public write([BIIZ)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->a()V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->d:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;

    invoke-virtual {v0, p3}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->nextDataPacketSize(I)I

    move-result p3

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->d:Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel;->sendData([BIIZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return p3

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/Channel$BlockingWriter;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
.end method
