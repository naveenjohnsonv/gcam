.class public Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/k/a/C;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequestUnknown$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenUnknown$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelWindowAdjust$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelExtendedData$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelEof$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelClose$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestUnknown$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelSuccess$Handler;
.implements Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure$Handler;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# instance fields
.field protected final base:Lcom/jscape/util/k/a/A;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/k/a/A<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;"
        }
    .end annotation
.end field

.field protected final closeConfirmationRequired:Z

.field protected final closed:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected final localId:I

.field protected final localMaxPacketSize:I

.field protected final localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

.field protected final localWindowLock:Ljava/util/concurrent/locks/Lock;

.field protected final localWindowMinSize:J

.field protected final logger:Ljava/util/logging/Logger;

.field protected final oneByteReadingBuffer:[B

.field protected final oneByteWritingBuffer:[B

.field protected final readingBuffer:Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;

.field protected final receiveLock:Ljava/util/concurrent/locks/Lock;

.field protected final remoteId:I

.field protected final remoteMaxPacketSize:I

.field protected final remoteWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

.field protected requestSuccess:Ljava/lang/Boolean;

.field protected final stderrReadingBuffer:Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "/\u0001\u001e\u0005\u001e?.\u000f\u001d\u0008\u0003\u0002x}\u0007\u0016\u001f\u0019\rx8D\u0015)\u001c\u0002\u0004\t|)\u0003\u001c\u0002J\u0005l}\t\u001f\u0003\u0019\t{s\u0002\u000f\u001d\u0016/\u0001\u001e\u0005\u001e?>\u0006\u001c\u001f\u0003\u0002x}\t\u001b\r\u0004\u0002z1D\u0018)\u001b\r\u0004\u0002z1J\u0001\t\u001b\u0019z.\u001eS\n\u000b\u0005s(\u0018\u0016B!(\u0012\u0008J\u001ez0\u0005\u0007\tJ\u0001~%J\u0003\r\t\u0007z)J\u0000\u0005\u0010\t?+\u000b\u001f\u0019\u000fB\u0002\u000f\u001d\u0013\t\u001c\u0001D\u0006l>\u000b\u0003\tD\u0005q8\u001e]\u001f\u0019\u0004\u0019?\u001d\u001f\u001f\u001co2\u0018\u0007\t\u000eL|5\u000b\u001d\u0002\u000f\u0000?)\u0013\u0003\tD"

    const/16 v4, 0xb2

    const/16 v5, 0x16

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x68

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0x26

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v13, v10

    const/4 v14, 0x0

    :goto_2
    const/4 v15, 0x2

    if-gt v13, v14, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v12, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x47

    const-string v3, "qSQQR!|VIG@\u0002<vWNCCGqrP\u001dy\u0001Qq/\t\u0003\u0002\u0001Q\u000c)\u0004\u0018Q f\\F\u0004N>pEQ\u0002IC)3T\\AOG%3WTXA\u0002\'rHHG\n"

    move v7, v10

    move v5, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    add-int/2addr v6, v9

    add-int v8, v6, v5

    invoke-virtual {v3, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v8, v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->c:[Ljava/lang/String;

    aget-object v0, v0, v15

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v10, v14

    rem-int/lit8 v1, v14, 0x7

    const/4 v2, 0x4

    if-eqz v1, :cond_7

    if-eq v1, v9, :cond_6

    if-eq v1, v15, :cond_5

    const/4 v9, 0x3

    if-eq v1, v9, :cond_7

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v15, 0x35

    goto :goto_4

    :cond_4
    const/16 v15, 0x77

    goto :goto_4

    :cond_5
    move v15, v2

    goto :goto_4

    :cond_6
    const/16 v15, 0x1b

    :cond_7
    :goto_4
    xor-int v1, v8, v15

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v10, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v9, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/util/k/a/A;IIJIJIZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/A<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;IIJIJIZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->base:Lcom/jscape/util/k/a/A;

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localId:I

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->remoteId:I

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-direct {p1, p4, p5}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;-><init>(J)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    const-wide/16 p1, 0x3

    div-long/2addr p4, p1

    iput-wide p4, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindowMinSize:J

    int-to-long p1, p6

    sget-object p3, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->c:[Ljava/lang/String;

    const/16 p4, 0xa

    aget-object p4, p3, p4

    const-wide/16 v1, 0x0

    invoke-static {p1, p2, v1, v2, p4}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p6, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localMaxPacketSize:I

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-direct {p1, p7, p8}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;-><init>(J)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->remoteWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    int-to-long p1, p9

    const/4 p4, 0x5

    aget-object p4, p3, p4

    invoke-static {p1, p2, v1, v2, p4}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p9, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->remoteMaxPacketSize:I

    iput-boolean p10, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->closeConfirmationRequired:Z

    const/4 p1, 0x1

    new-array p2, p1, [B

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->oneByteReadingBuffer:[B

    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;

    invoke-direct {p2}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;-><init>()V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->readingBuffer:Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;

    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;

    invoke-direct {p2}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;-><init>()V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->stderrReadingBuffer:Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;

    new-array p2, p1, [B

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->oneByteWritingBuffer:[B

    new-instance p2, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receiveLock:Ljava/util/concurrent/locks/Lock;

    new-instance p2, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindowLock:Ljava/util/concurrent/locks/Lock;

    new-instance p2, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p4, 0x0

    invoke-direct {p2, p4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->closed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x7

    aget-object p2, p3, p2

    invoke-static {p2}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object p2

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->logger:Ljava/util/logging/Logger;

    if-eqz v0, :cond_0

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->c:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected adjustLocalWindow()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindowLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->size()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindowMinSize:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v1, v1, v3

    if-nez v0, :cond_1

    if-lez v1, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindowLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->readingBuffer:Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->available()I

    move-result v1

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->stderrReadingBuffer:Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->available()I

    move-result v2

    add-int/2addr v1, v2

    :cond_1
    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->space()J

    move-result-wide v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    int-to-long v4, v1

    sub-long/2addr v2, v4

    if-nez v0, :cond_3

    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_2

    :try_start_2
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelWindowAdjust;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->remoteId:I

    invoke-direct {v0, v1, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelWindowAdjust;-><init>(IJ)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v0, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->increase(J)V
    :try_end_2
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindowLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :cond_3
    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindowLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method protected assertNotClosed()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->closed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/util/k/a/Connection$ConnectionException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->c:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/util/k/a/Connection$ConnectionException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method protected assertRequestSuccess()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->requestSuccess:Ljava/lang/Boolean;
    :try_end_0
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->requestSuccess:Ljava/lang/Boolean;
    :try_end_1
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    return-void

    :cond_2
    new-instance v0, Lcom/jscape/util/k/a/Connection$ConnectionException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->c:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/util/k/a/Connection$ConnectionException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public attributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->base:Lcom/jscape/util/k/a/A;

    invoke-interface {v0}, Lcom/jscape/util/k/a/A;->attributes()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public availableBytes()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected awaitCloseConfirmation()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->closed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receiveLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receive()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receiveLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    if-nez v0, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receiveLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method protected awaitRequestResult()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->requestSuccess:Ljava/lang/Boolean;

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->requestSuccess:Ljava/lang/Boolean;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receiveLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receive()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receiveLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    if-eqz v0, :cond_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receiveLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_1
    :goto_0
    return-void
.end method

.method public close()V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->closed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelEof;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->remoteId:I

    invoke-direct {v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelEof;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->sendSafe(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelClose;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->remoteId:I

    invoke-direct {v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelClose;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->sendSafe(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    if-nez v0, :cond_2

    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->closeConfirmationRequired:Z

    :cond_1
    if-eqz v1, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->awaitCloseConfirmation()V

    :cond_3
    return-void
.end method

.method public closed()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->closed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public creationTime()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->base:Lcom/jscape/util/k/a/A;

    invoke-interface {v0}, Lcom/jscape/util/k/a/A;->creationTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public flush()V
    .locals 0

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelClose;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelClose;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->closed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->readingBuffer:Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->close()V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->stderrReadingBuffer:Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->close()V

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;Lcom/jscape/util/k/a/x;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindowLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    if-nez p2, :cond_0

    :try_start_0
    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {p2}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->size()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p2, v0, v2

    if-lez p2, :cond_0

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->readingBuffer:Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;->data:[B

    invoke-virtual {p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->put([B)V

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;->length:I

    invoke-virtual {p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->decrease(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindowLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1

    :cond_0
    :goto_0
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindowLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelEof;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelEof;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->readingBuffer:Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->close()V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->stderrReadingBuffer:Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->close()V

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelExtendedData;Lcom/jscape/util/k/a/x;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelExtendedData;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object p2

    iget v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelExtendedData;->dataTypeCode:I

    if-nez p2, :cond_1

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;->SSH_EXTENDED_DATA_STDERR:Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/messages/DataType;->code:I

    if-eq v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindowLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    if-nez p2, :cond_2

    :try_start_0
    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {p2}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->size()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    :cond_1
    if-lez v0, :cond_2

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->stderrReadingBuffer:Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelExtendedData;->data:[B

    invoke-virtual {p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->put([B)V

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelExtendedData;->data:[B

    array-length p1, p1

    invoke-virtual {p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->decrease(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindowLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1

    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localWindowLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->requestSuccess:Ljava/lang/Boolean;

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenUnknown;Lcom/jscape/util/k/a/x;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenUnknown;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenUnknown;->senderChannel:I

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;->SSH_OPEN_UNKNOWN_CHANNEL_TYPE:Lcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->c:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    const/4 v3, 0x6

    aget-object v1, v1, v3

    invoke-direct {p2, p1, v0, v2, v1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;-><init>(ILcom/jscape/inet/ssh/protocol/v2/messages/ChannelOpenFailureReason;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->sendSafe(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestUnknown;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestUnknown;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    iget-boolean p2, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestUnknown;->wantReply:Z

    if-eqz p2, :cond_0

    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure;

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestUnknown;->recipientChannel:I

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure;-><init>(I)V

    invoke-virtual {p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->sendSafe(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    :cond_0
    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelSuccess;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelSuccess;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->requestSuccess:Ljava/lang/Boolean;

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelWindowAdjust;Lcom/jscape/util/k/a/x;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelWindowAdjust;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->remoteWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    iget-wide v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelWindowAdjust;->bytesToAdd:J

    invoke-virtual {p2, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->increase(J)V

    return-void
.end method

.method public handle(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequestUnknown;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequestUnknown;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    iget-boolean p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequestUnknown;->wantReply:Z

    if-eqz p1, :cond_0

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgRequestFailure;

    invoke-direct {p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgRequestFailure;-><init>()V

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->sendSafe(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    :cond_0
    return-void
.end method

.method public localAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->base:Lcom/jscape/util/k/a/A;

    invoke-interface {v0}, Lcom/jscape/util/k/a/A;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method protected logErrorSendMessage(Ljava/lang/Throwable;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->c:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method protected logUnsupportedMessage(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->logger:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->c:[Ljava/lang/String;

    const/16 v3, 0x9

    aget-object v0, v0, v3

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected processReadingError(Ljava/lang/Exception;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->closed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1
    :try_end_0
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    const/4 v1, -0x1

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lcom/jscape/util/k/a/Connection$ConnectionException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/util/k/a/Connection$ConnectionException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return v1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method protected processRequestResult(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->awaitRequestResult()V

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->assertRequestSuccess()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->requestSuccess:Ljava/lang/Boolean;
    :try_end_0
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    return-void
.end method

.method public read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->oneByteReadingBuffer:[B

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->read([BII)I

    move-result v1

    const/4 v3, -0x1

    if-nez v0, :cond_0

    if-eq v1, v3, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->oneByteReadingBuffer:[B

    aget-byte v1, v0, v2
    :try_end_0
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v3, 0xff

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    and-int/2addr v1, v3

    :cond_1
    return v1
.end method

.method public read([BII)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->readingBuffer:Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->readBuffer(Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;[BII)I

    move-result p1

    return p1
.end method

.method protected readBuffer(Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;[BII)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->empty()Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    if-eqz v1, :cond_3

    :try_start_1
    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->closed()Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    if-nez v0, :cond_4

    if-nez v0, :cond_4

    if-nez v1, :cond_3

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receiveLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    if-nez v0, :cond_2

    :try_start_3
    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->empty()Z

    move-result v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->closed()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receive()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_1
    :try_start_5
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receiveLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_1
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :goto_0
    :try_start_8
    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receiveLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    goto :goto_2

    :catch_2
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    :catch_3
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_2
    invoke-virtual {p1, p2, p3, p4}, Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;->read([BII)I

    move-result v1

    :cond_4
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->adjustLocalWindow()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4

    return v1

    :catch_4
    move-exception p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->processReadingError(Ljava/lang/Exception;)I

    move-result p1

    return p1
.end method

.method public readStderr([BII)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->stderrReadingBuffer:Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->readBuffer(Lcom/jscape/inet/ssh/protocol/v2/connection/ReadingBuffer;[BII)I

    move-result p1

    return p1
.end method

.method protected receive()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->base:Lcom/jscape/util/k/a/A;

    invoke-interface {v0}, Lcom/jscape/util/k/a/A;->read()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/ssh/protocol/messages/Message;

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->base:Lcom/jscape/util/k/a/A;

    invoke-virtual {v0, p0, v1}, Lcom/jscape/inet/ssh/protocol/messages/Message;->accept(Lcom/jscape/inet/ssh/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->logUnsupportedMessage(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    :goto_0
    return-void
.end method

.method public remoteAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->base:Lcom/jscape/util/k/a/A;

    invoke-interface {v0}, Lcom/jscape/util/k/a/A;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method protected send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->base:Lcom/jscape/util/k/a/A;

    invoke-interface {v0, p1}, Lcom/jscape/util/k/a/A;->write(Ljava/lang/Object;)V

    return-void
.end method

.method protected sendData([BII)I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->remoteWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->size()J

    move-result-wide v1
    :try_end_0
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_0 .. :try_end_0} :catch_2

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_4

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->closed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1
    :try_end_1
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_1 .. :try_end_1} :catch_3

    if-nez v0, :cond_5

    if-nez v0, :cond_5

    if-nez v1, :cond_4

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receiveLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v1
    :try_end_2
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_2 .. :try_end_2} :catch_6

    if-nez v0, :cond_2

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    if-nez v0, :cond_3

    :try_start_3
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->remoteWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->size()J

    move-result-wide v1
    :try_end_3
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    cmp-long v1, v1, v3

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_2
    :goto_1
    if-nez v1, :cond_3

    if-nez v0, :cond_3

    :try_start_5
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->closed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receive()V
    :try_end_5
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_2
    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receiveLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1

    :cond_3
    :goto_3
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->receiveLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    if-eqz v0, :cond_0

    :cond_4
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->assertNotClosed()V

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->remoteMaxPacketSize:I

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->remoteWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->size()J

    move-result-wide v1

    int-to-long v3, p3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    long-to-int p3, v1

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v1

    :cond_5
    new-instance p3, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->remoteId:I

    invoke-direct {p3, v0, p1, p2, v1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;-><init>(I[BII)V

    invoke-virtual {p0, p3}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->remoteWindow:Lcom/jscape/inet/ssh/protocol/v2/connection/Window;

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/Window;->decrease(I)V

    return v1

    :catch_2
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_7 .. :try_end_7} :catch_3

    :catch_3
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_9 .. :try_end_9} :catch_5

    :catch_5
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Lcom/jscape/util/k/a/Connection$ConnectionException; {:try_start_a .. :try_end_a} :catch_6

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method protected sendSafe(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->send(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->logErrorSendMessage(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->base:Lcom/jscape/util/k/a/A;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(B)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->oneByteWritingBuffer:[B

    const/4 v1, 0x0

    aput-byte p1, v0, v1

    const/4 p1, 0x1

    invoke-virtual {p0, v0, v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->write([BII)V

    return-void
.end method

.method public write([BII)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->e()Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-lez p3, :cond_1

    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/connection/ChannelConnection;->sendData([BII)I

    move-result v1

    add-int/2addr p2, v1

    sub-int/2addr p3, v1

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method
