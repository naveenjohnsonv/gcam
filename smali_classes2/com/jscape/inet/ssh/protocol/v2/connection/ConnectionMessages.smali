.class public Lcom/jscape/inet/ssh/protocol/v2/connection/ConnectionMessages;
.super Ljava/lang/Object;


# static fields
.field public static final ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/16 v0, 0x15

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgDisconnectCodec;

    invoke-direct {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgDisconnectCodec;-><init>()V

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDisconnect;

    const/4 v7, 0x0

    aput-object v6, v5, v7

    invoke-direct {v2, v4, v3, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgIgnoreCodec;

    invoke-direct {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgIgnoreCodec;-><init>()V

    new-array v5, v4, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgIgnore;

    aput-object v6, v5, v7

    const/4 v6, 0x2

    invoke-direct {v2, v6, v3, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v4

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUnimplementedCodec;

    invoke-direct {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUnimplementedCodec;-><init>()V

    new-array v5, v4, [Ljava/lang/Class;

    const-class v8, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUnimplemented;

    aput-object v8, v5, v7

    const/4 v8, 0x3

    invoke-direct {v2, v8, v3, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v6

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgDebugCodec;

    invoke-direct {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgDebugCodec;-><init>()V

    new-array v5, v4, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgDebug;

    aput-object v6, v5, v7

    const/4 v6, 0x4

    invoke-direct {v2, v6, v3, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v8

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgServiceRequestCodec;

    invoke-direct {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgServiceRequestCodec;-><init>()V

    new-array v5, v4, [Ljava/lang/Class;

    const-class v8, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceRequest;

    aput-object v8, v5, v7

    const/4 v8, 0x5

    invoke-direct {v2, v8, v3, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v6

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgServiceAcceptCodec;

    invoke-direct {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgServiceAcceptCodec;-><init>()V

    new-array v5, v4, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgServiceAccept;

    aput-object v6, v5, v7

    const/4 v6, 0x6

    invoke-direct {v2, v6, v3, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v8

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexInitCodec;

    invoke-direct {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexInitCodec;-><init>()V

    new-array v5, v4, [Ljava/lang/Class;

    const-class v8, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    aput-object v8, v5, v7

    const/16 v8, 0x14

    invoke-direct {v2, v8, v3, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v6

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgNewKeysCodec;

    invoke-direct {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgNewKeysCodec;-><init>()V

    new-array v5, v4, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgNewKeys;

    aput-object v6, v5, v7

    invoke-direct {v2, v0, v3, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/4 v0, 0x7

    aput-object v2, v1, v0

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthFailureCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthFailureCodec;-><init>()V

    new-array v3, v4, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthFailure;

    aput-object v5, v3, v7

    const/16 v5, 0x33

    invoke-direct {v0, v5, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/16 v2, 0x8

    aput-object v0, v1, v2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthSuccessCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthSuccessCodec;-><init>()V

    new-array v3, v4, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthSuccess;

    aput-object v5, v3, v7

    const/16 v5, 0x34

    invoke-direct {v0, v5, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/16 v2, 0x9

    aput-object v0, v1, v2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthBannerCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthBannerCodec;-><init>()V

    new-array v3, v4, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthBanner;

    aput-object v5, v3, v7

    const/16 v5, 0x35

    invoke-direct {v0, v5, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/16 v2, 0xa

    aput-object v0, v1, v2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;

    new-array v3, v7, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;

    invoke-direct {v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;)V

    new-array v3, v7, [Ljava/lang/Class;

    const/16 v5, 0x50

    invoke-direct {v0, v5, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/16 v2, 0xb

    aput-object v0, v1, v2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgRequestFailureCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgRequestFailureCodec;-><init>()V

    new-array v3, v4, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgRequestFailure;

    aput-object v5, v3, v7

    const/16 v5, 0x52

    invoke-direct {v0, v5, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/16 v2, 0xc

    aput-object v0, v1, v2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenFailureCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenFailureCodec;-><init>()V

    new-array v3, v4, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenFailure;

    aput-object v5, v3, v7

    const/16 v5, 0x5c

    invoke-direct {v0, v5, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/16 v2, 0xd

    aput-object v0, v1, v2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelWindowAdjustCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelWindowAdjustCodec;-><init>()V

    new-array v3, v4, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelWindowAdjust;

    aput-object v5, v3, v7

    const/16 v5, 0x5d

    invoke-direct {v0, v5, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/16 v2, 0xe

    aput-object v0, v1, v2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelDataCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelDataCodec;-><init>()V

    new-array v3, v4, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;

    aput-object v5, v3, v7

    const/16 v5, 0x5e

    invoke-direct {v0, v5, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/16 v2, 0xf

    aput-object v0, v1, v2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelExtendedDataCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelExtendedDataCodec;-><init>()V

    new-array v3, v4, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelExtendedData;

    aput-object v5, v3, v7

    const/16 v5, 0x5f

    invoke-direct {v0, v5, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/16 v2, 0x10

    aput-object v0, v1, v2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelEofCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelEofCodec;-><init>()V

    new-array v3, v4, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelEof;

    aput-object v5, v3, v7

    const/16 v5, 0x60

    invoke-direct {v0, v5, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/16 v2, 0x11

    aput-object v0, v1, v2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelCloseCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelCloseCodec;-><init>()V

    new-array v3, v4, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelClose;

    aput-object v5, v3, v7

    const/16 v5, 0x61

    invoke-direct {v0, v5, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/16 v2, 0x12

    aput-object v0, v1, v2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelSuccessCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelSuccessCodec;-><init>()V

    new-array v3, v4, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelSuccess;

    aput-object v5, v3, v7

    const/16 v5, 0x63

    invoke-direct {v0, v5, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    const/16 v2, 0x13

    aput-object v0, v1, v2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelFailureCodec;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelFailureCodec;-><init>()V

    new-array v3, v4, [Ljava/lang/Class;

    const-class v4, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelFailure;

    aput-object v4, v3, v7

    const/16 v4, 0x64

    invoke-direct {v0, v4, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v0, v1, v8

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/ConnectionMessages;->ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/ConnectionMessages;->ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p0

    return-object p0
.end method
