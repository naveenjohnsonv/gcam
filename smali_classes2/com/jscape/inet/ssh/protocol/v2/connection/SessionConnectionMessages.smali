.class public Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnectionMessages;
.super Ljava/lang/Object;


# static fields
.field public static final CHANNEL_ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;

.field public static final CHANNEL_TYPES:[Ljava/lang/Class;

.field public static final ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

.field public static final REQUEST_ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;

.field public static final REQUEST_TYPES:[Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "LycSss?\\sa_`\u0006[yvXqk\r_y\u007fR\u007fp?KxpXwb\u0003M~g\t[esEitfM}\u0007Xdh\u001bbbc\u000bMhxB=tfIddE\u0005[xtZ|\u0004MhtU"

    const/16 v4, 0x4e

    const/16 v5, 0xc

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/4 v8, 0x1

    add-int/2addr v6, v8

    add-int v9, v6, v5

    invoke-virtual {v3, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const/4 v11, -0x1

    const/16 v12, 0xa

    :goto_1
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    array-length v13, v9

    move v14, v2

    :goto_2
    const/4 v15, 0x4

    const/4 v10, 0x3

    const/4 v1, 0x2

    if-gt v13, v14, :cond_3

    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v9}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v12}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    const/4 v12, 0x7

    if-eqz v11, :cond_1

    add-int/lit8 v1, v7, 0x1

    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v1

    goto :goto_0

    :cond_0
    const-string v3, "\u0003-:\u001d!0$\u0005\u0012:,\u000f#"

    move v7, v1

    move v5, v12

    const/16 v4, 0xd

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v7, 0x1

    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v5, v1

    move v7, v11

    :goto_3
    const/16 v12, 0x52

    add-int/2addr v6, v8

    add-int v1, v6, v5

    invoke-virtual {v3, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    move v11, v2

    goto :goto_1

    :cond_2
    new-array v3, v1, [Ljava/lang/Class;

    const-class v4, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenSession;

    aput-object v4, v3, v2

    const-class v4, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenDirectTcpIp;

    aput-object v4, v3, v8

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnectionMessages;->CHANNEL_TYPES:[Ljava/lang/Class;

    const/16 v3, 0x9

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;

    aput-object v5, v4, v2

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestEnv;

    aput-object v5, v4, v8

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestShell;

    aput-object v5, v4, v1

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestExec;

    aput-object v5, v4, v10

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestSubsystem;

    aput-object v5, v4, v15

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;

    const/4 v6, 0x5

    aput-object v5, v4, v6

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestSignal;

    const/4 v6, 0x6

    aput-object v5, v4, v6

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestBreak;

    aput-object v5, v4, v12

    const-class v5, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestExitStatus;

    const/16 v7, 0x8

    aput-object v5, v4, v7

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnectionMessages;->REQUEST_TYPES:[Ljava/lang/Class;

    new-array v4, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;

    aget-object v9, v0, v3

    new-instance v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenSessionCodec;

    invoke-direct {v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenSessionCodec;-><init>()V

    new-array v13, v8, [Ljava/lang/Class;

    const-class v14, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenSession;

    aput-object v14, v13, v2

    invoke-direct {v5, v9, v11, v13}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$TypeCodec;[Ljava/lang/Class;)V

    aput-object v5, v4, v2

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;

    aget-object v9, v0, v2

    new-instance v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenDirectTcpIpCodec;

    invoke-direct {v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenDirectTcpIpCodec;-><init>()V

    new-array v13, v8, [Ljava/lang/Class;

    const-class v14, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenDirectTcpIp;

    aput-object v14, v13, v2

    invoke-direct {v5, v9, v11, v13}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$TypeCodec;[Ljava/lang/Class;)V

    aput-object v5, v4, v8

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnectionMessages;->CHANNEL_ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;

    new-array v3, v3, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;

    const/4 v5, 0x5

    aget-object v9, v0, v5

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestPtyReqCodec;

    invoke-direct {v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestPtyReqCodec;-><init>()V

    new-array v11, v8, [Ljava/lang/Class;

    const-class v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;

    aput-object v13, v11, v2

    invoke-direct {v4, v9, v5, v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$RequestCodec;[Ljava/lang/Class;)V

    aput-object v4, v3, v2

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;

    aget-object v5, v0, v10

    new-instance v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestEnvCodec;

    invoke-direct {v9}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestEnvCodec;-><init>()V

    new-array v11, v8, [Ljava/lang/Class;

    const-class v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestEnv;

    aput-object v13, v11, v2

    invoke-direct {v4, v5, v9, v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$RequestCodec;[Ljava/lang/Class;)V

    aput-object v4, v3, v8

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;

    aget-object v5, v0, v12

    new-instance v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestShellCodec;

    invoke-direct {v9}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestShellCodec;-><init>()V

    new-array v11, v8, [Ljava/lang/Class;

    const-class v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestShell;

    aput-object v13, v11, v2

    invoke-direct {v4, v5, v9, v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$RequestCodec;[Ljava/lang/Class;)V

    aput-object v4, v3, v1

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;

    aget-object v5, v0, v7

    new-instance v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestExecCodec;

    invoke-direct {v9}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestExecCodec;-><init>()V

    new-array v11, v8, [Ljava/lang/Class;

    const-class v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestExec;

    aput-object v13, v11, v2

    invoke-direct {v4, v5, v9, v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$RequestCodec;[Ljava/lang/Class;)V

    aput-object v4, v3, v10

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;

    aget-object v5, v0, v15

    new-instance v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestSubsystemCodec;

    invoke-direct {v9}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestSubsystemCodec;-><init>()V

    new-array v11, v8, [Ljava/lang/Class;

    const-class v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestSubsystem;

    aput-object v13, v11, v2

    invoke-direct {v4, v5, v9, v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$RequestCodec;[Ljava/lang/Class;)V

    aput-object v4, v3, v15

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;

    aget-object v5, v0, v1

    new-instance v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestWindowChangeCodec;

    invoke-direct {v9}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestWindowChangeCodec;-><init>()V

    new-array v11, v8, [Ljava/lang/Class;

    const-class v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;

    aput-object v13, v11, v2

    invoke-direct {v4, v5, v9, v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$RequestCodec;[Ljava/lang/Class;)V

    const/4 v5, 0x5

    aput-object v4, v3, v5

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;

    aget-object v5, v0, v8

    new-instance v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestSignalCodec;

    invoke-direct {v9}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestSignalCodec;-><init>()V

    new-array v11, v8, [Ljava/lang/Class;

    const-class v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestSignal;

    aput-object v13, v11, v2

    invoke-direct {v4, v5, v9, v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$RequestCodec;[Ljava/lang/Class;)V

    aput-object v4, v3, v6

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;

    const/16 v16, 0xa

    aget-object v5, v0, v16

    new-instance v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestBreakCodec;

    invoke-direct {v9}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestBreakCodec;-><init>()V

    new-array v11, v8, [Ljava/lang/Class;

    const-class v13, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestBreak;

    aput-object v13, v11, v2

    invoke-direct {v4, v5, v9, v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$RequestCodec;[Ljava/lang/Class;)V

    aput-object v4, v3, v12

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;

    aget-object v0, v0, v6

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestExitStatusCodec;

    invoke-direct {v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestExitStatusCodec;-><init>()V

    new-array v6, v8, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestExitStatus;

    aput-object v9, v6, v2

    invoke-direct {v4, v0, v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$RequestCodec;[Ljava/lang/Class;)V

    aput-object v4, v3, v7

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnectionMessages;->REQUEST_ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;

    new-array v0, v10, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    const/16 v4, 0x5a

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;

    sget-object v6, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnectionMessages;->CHANNEL_ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;

    invoke-direct {v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;)V

    sget-object v6, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnectionMessages;->CHANNEL_TYPES:[Ljava/lang/Class;

    invoke-direct {v3, v4, v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v3, v0, v2

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    const/16 v4, 0x5b

    new-instance v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenConfirmationCodec;

    invoke-direct {v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenConfirmationCodec;-><init>()V

    new-array v6, v8, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenConfirmation;

    aput-object v7, v6, v2

    invoke-direct {v3, v4, v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v3, v0, v8

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    const/16 v3, 0x62

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec;

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnectionMessages;->REQUEST_ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;

    invoke-direct {v4, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$Entry;)V

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnectionMessages;->REQUEST_TYPES:[Ljava/lang/Class;

    invoke-direct {v2, v3, v4, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnectionMessages;->ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    return-void

    :cond_3
    const/16 v16, 0xa

    aget-char v17, v9, v14

    rem-int/lit8 v2, v14, 0x7

    const/16 v18, 0x1a

    if-eqz v2, :cond_8

    if-eq v2, v8, :cond_7

    if-eq v2, v1, :cond_6

    if-eq v2, v10, :cond_5

    if-eq v2, v15, :cond_7

    const/4 v1, 0x5

    if-eq v2, v1, :cond_4

    const/16 v15, 0x18

    goto :goto_4

    :cond_4
    const/16 v15, 0xd

    goto :goto_4

    :cond_5
    const/16 v15, 0x3c

    goto :goto_4

    :cond_6
    const/16 v15, 0x1b

    goto :goto_4

    :cond_7
    move/from16 v15, v18

    goto :goto_4

    :cond_8
    const/16 v15, 0x22

    :goto_4
    xor-int v1, v12, v15

    xor-int v1, v17, v1

    int-to-char v1, v1

    aput-char v1, v9, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v2, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnectionMessages;->ENTRIES:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/connection/ConnectionMessages;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    move-result-object p0

    return-object p0
.end method
