.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatureCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$EntryCodec;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "LP\u001a"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatureCodec;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x46

    goto :goto_1

    :cond_1
    const/16 v4, 0x1d

    goto :goto_1

    :cond_2
    const/16 v4, 0x4d

    goto :goto_1

    :cond_3
    const/16 v4, 0x7a

    goto :goto_1

    :cond_4
    const/16 v4, 0x4e

    goto :goto_1

    :cond_5
    const/16 v4, 0x13

    goto :goto_1

    :cond_6
    const/16 v4, 0xe

    :goto_1
    const/4 v5, 0x6

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readValue(Ljava/lang/String;Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readValue(Ljava/io/InputStream;)[B

    move-result-object p1

    new-instance v0, Lcom/jscape/util/h/e;

    invoke-direct {v0, p1}, Lcom/jscape/util/h/e;-><init>([B)V

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->readValue(Ljava/io/InputStream;)Ljava/math/BigInteger;

    move-result-object p1

    invoke-virtual {p1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object p1

    new-instance v1, Ljava/math/BigInteger;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, Ljava/math/BigInteger;-><init>(I[B)V

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->readValue(Ljava/io/InputStream;)Ljava/math/BigInteger;

    move-result-object p1

    invoke-virtual {p1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object p1

    new-instance v0, Ljava/math/BigInteger;

    invoke-direct {v0, v2, p1}, Ljava/math/BigInteger;-><init>(I[B)V

    new-instance p1, Lorg/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {p1}, Lorg/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    new-instance v2, Lorg/bouncycastle/asn1/ASN1Integer;

    invoke-direct {v2, v1}, Lorg/bouncycastle/asn1/ASN1Integer;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {p1, v2}, Lorg/bouncycastle/asn1/ASN1EncodableVector;->add(Lorg/bouncycastle/asn1/ASN1Encodable;)V

    new-instance v1, Lorg/bouncycastle/asn1/ASN1Integer;

    invoke-direct {v1, v0}, Lorg/bouncycastle/asn1/ASN1Integer;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {p1, v1}, Lorg/bouncycastle/asn1/ASN1EncodableVector;->add(Lorg/bouncycastle/asn1/ASN1Encodable;)V

    new-instance v0, Lorg/bouncycastle/asn1/DERSequence;

    invoke-direct {v0, p1}, Lorg/bouncycastle/asn1/DERSequence;-><init>(Lorg/bouncycastle/asn1/ASN1EncodableVector;)V

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatureCodec;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lorg/bouncycastle/asn1/DERSequence;->getEncoded(Ljava/lang/String;)[B

    move-result-object p1

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    invoke-direct {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;-><init>(Ljava/lang/String;[B)V

    return-object v0
.end method

.method public static writeValue(Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object p0, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;->blob:[B

    invoke-static {p0}, Lorg/bouncycastle/asn1/ASN1Primitive;->fromByteArray([B)Lorg/bouncycastle/asn1/ASN1Primitive;

    move-result-object p0

    check-cast p0, Lorg/bouncycastle/asn1/ASN1Sequence;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lorg/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    check-cast v0, Lorg/bouncycastle/asn1/ASN1Integer;

    invoke-virtual {v0}, Lorg/bouncycastle/asn1/ASN1Integer;->getPositiveValue()Ljava/math/BigInteger;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lorg/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lorg/bouncycastle/asn1/ASN1Encodable;

    move-result-object p0

    check-cast p0, Lorg/bouncycastle/asn1/ASN1Integer;

    invoke-virtual {p0}, Lorg/bouncycastle/asn1/ASN1Integer;->getPositiveValue()Ljava/math/BigInteger;

    move-result-object p0

    new-instance v1, Lcom/jscape/util/h/o;

    invoke-direct {v1}, Lcom/jscape/util/h/o;-><init>()V

    invoke-static {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->writeValue(Ljava/math/BigInteger;Ljava/io/OutputStream;)V

    invoke-static {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->writeValue(Ljava/math/BigInteger;Ljava/io/OutputStream;)V

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->d()[B

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    return-void
.end method


# virtual methods
.method public read(Ljava/lang/String;Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatureCodec;->readValue(Ljava/lang/String;Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatureCodec;->writeValue(Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;Ljava/io/OutputStream;)V

    return-void
.end method
