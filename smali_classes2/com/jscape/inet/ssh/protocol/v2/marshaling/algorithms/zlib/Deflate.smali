.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static final A:I = -0x2

.field private static final B:I = -0x3

.field private static final C:I = -0x4

.field private static final D:I = -0x5

.field private static final E:I = -0x6

.field private static final F:I = 0x2a

.field private static final G:I = 0x71

.field private static final H:I = 0x29a

.field private static final I:I = 0x8

.field private static final J:I = 0x0

.field private static final K:I = 0x1

.field private static final L:I = 0x2

.field private static final M:I = 0x0

.field private static final N:I = 0x1

.field private static final O:I = 0x2

.field private static final P:I = 0x10

.field private static final Q:I = 0x10

.field private static final R:I = 0x11

.field private static final S:I = 0x12

.field private static final T:I = 0x3

.field private static final U:I = 0x102

.field private static final V:I = 0x106

.field private static final W:I = 0xf

.field private static final X:I = 0x1e

.field private static final Y:I = 0x13

.field private static final Z:I = 0x1d

.field private static final a:I = 0x9

.field private static final aa:I = 0x100

.field private static final ab:I = 0x11e

.field private static final ac:I = 0x23d

.field private static final ad:I = 0x100

.field private static final b:I = -0x1

.field private static final c:I = 0xf

.field private static final d:I = 0x8

.field private static final e:I = 0x0

.field private static final f:I = 0x1

.field private static final g:I = 0x2

.field private static final h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

.field private static final i:[Ljava/lang/String;

.field private static final j:I = 0x0

.field private static final k:I = 0x1

.field private static final l:I = 0x2

.field private static final m:I = 0x3

.field private static final n:I = 0x20

.field private static final o:I = 0x1

.field private static final p:I = 0x2

.field private static final q:I = 0x0

.field private static final r:I = 0x0

.field private static final s:I = 0x1

.field private static final t:I = 0x2

.field private static final u:I = 0x3

.field private static final v:I = 0x4

.field private static final w:I = 0x0

.field private static final x:I = 0x1

.field private static final y:I = 0x2

.field private static final z:I = -0x1


# instance fields
.field a0:I

.field a1:I

.field a2:I

.field a3:I

.field a4:I

.field a5:I

.field a6:I

.field a7:I

.field a8:S

.field a9:I

.field aA:I

.field aB:I

.field aC:I

.field aD:I

.field aE:I

.field aF:I

.field aG:I

.field aH:I

.field aI:I

.field aJ:I

.field aK:I

.field aL:I

.field aM:I

.field aN:I

.field aO:[S

.field aP:[S

.field aQ:[S

.field aR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

.field aS:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

.field aT:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

.field aU:[S

.field aV:[S

.field aW:[I

.field aX:I

.field aY:I

.field aZ:[B

.field a_:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

.field ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

.field af:I

.field ag:[B

.field ah:I

.field ai:I

.field aj:I

.field ak:I

.field al:B

.field am:B

.field an:I

.field ao:I

.field ap:I

.field aq:I

.field ar:[B

.field as:I

.field at:[S

.field au:[S

.field av:I

.field aw:I

.field ax:I

.field ay:I

.field az:I


# direct methods
.method static constructor <clinit>()V
    .locals 23

    const/16 v0, 0x8

    new-array v1, v0, [Ljava/lang/String;

    const/16 v3, 0xa

    const/4 v4, 0x0

    const-string v5, "Rwuxt>nDmc\nGjkx56<Slu\u0014Hmdrx#/Ujeqps8Dqttz=\u000cRwuxt>nDqurg\nEbs|56<Slu\u000fOfby57\'Bwnr{2<X"

    const/16 v6, 0x52

    move v8, v3

    move v9, v4

    const/4 v7, -0x1

    :goto_0
    const/16 v10, 0x57

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    move v15, v4

    :goto_2
    const/4 v2, 0x2

    const/4 v0, 0x4

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_1

    add-int/lit8 v0, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v0

    const/16 v0, 0x8

    goto :goto_0

    :cond_0
    const/16 v6, 0x20

    const/16 v2, 0xc

    const-string v5, "\u00072%?4e*\u0000516#\u0013\u000c)0,7qc\u0006.&7%7g\u0000*,+("

    move v9, v0

    move v8, v2

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v8, v0

    move v9, v12

    :goto_3
    const/16 v10, 0x13

    add-int/2addr v7, v11

    add-int v0, v7, v8

    invoke-virtual {v5, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v13, v4

    const/16 v0, 0x8

    goto :goto_1

    :cond_2
    new-array v5, v3, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    sput-object v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v17, v6

    invoke-direct/range {v17 .. v22}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;-><init>(IIIII)V

    aput-object v6, v5, v4

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    const/16 v18, 0x4

    const/16 v19, 0x4

    const/16 v20, 0x8

    const/16 v21, 0x4

    const/16 v22, 0x1

    move-object/from16 v17, v6

    invoke-direct/range {v17 .. v22}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;-><init>(IIIII)V

    aput-object v6, v5, v11

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    const/16 v19, 0x5

    const/16 v20, 0x10

    const/16 v21, 0x8

    move-object/from16 v17, v6

    invoke-direct/range {v17 .. v22}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;-><init>(IIIII)V

    aput-object v6, v5, v2

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    const/16 v19, 0x6

    const/16 v20, 0x20

    const/16 v21, 0x20

    move-object/from16 v17, v6

    invoke-direct/range {v17 .. v22}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;-><init>(IIIII)V

    const/4 v7, 0x3

    aput-object v6, v5, v7

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    const/16 v19, 0x4

    const/16 v20, 0x10

    const/16 v21, 0x10

    const/16 v22, 0x2

    move-object/from16 v17, v6

    invoke-direct/range {v17 .. v22}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;-><init>(IIIII)V

    aput-object v6, v5, v0

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    const/16 v18, 0x8

    const/16 v19, 0x10

    const/16 v20, 0x20

    const/16 v21, 0x20

    move-object/from16 v17, v6

    invoke-direct/range {v17 .. v22}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;-><init>(IIIII)V

    const/4 v7, 0x5

    aput-object v6, v5, v7

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    const/16 v20, 0x80

    const/16 v21, 0x80

    move-object/from16 v17, v6

    invoke-direct/range {v17 .. v22}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;-><init>(IIIII)V

    const/4 v7, 0x6

    aput-object v6, v5, v7

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    const/16 v19, 0x20

    const/16 v21, 0x100

    move-object/from16 v17, v6

    invoke-direct/range {v17 .. v22}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;-><init>(IIIII)V

    const/4 v8, 0x7

    aput-object v6, v5, v8

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    const/16 v18, 0x20

    const/16 v19, 0x80

    const/16 v20, 0x102

    const/16 v21, 0x400

    move-object/from16 v17, v6

    invoke-direct/range {v17 .. v22}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;-><init>(IIIII)V

    const/16 v9, 0x8

    aput-object v6, v5, v9

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    const/16 v19, 0x102

    const/16 v21, 0x1000

    move-object/from16 v17, v6

    invoke-direct/range {v17 .. v22}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;-><init>(IIIII)V

    const/16 v9, 0x9

    aput-object v6, v5, v9

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x5

    aget-object v6, v1, v5

    aput-object v6, v3, v4

    aget-object v4, v1, v4

    aput-object v4, v3, v11

    const-string v4, ""

    aput-object v4, v3, v2

    aget-object v6, v1, v11

    const/4 v10, 0x3

    aput-object v6, v3, v10

    aget-object v6, v1, v10

    aput-object v6, v3, v0

    aget-object v0, v1, v0

    aput-object v0, v3, v5

    aget-object v0, v1, v8

    aput-object v0, v3, v7

    aget-object v0, v1, v7

    aput-object v0, v3, v8

    aget-object v0, v1, v2

    const/16 v16, 0x8

    aput-object v0, v3, v16

    aput-object v4, v3, v9

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->i:[Ljava/lang/String;

    return-void

    :cond_3
    const/16 v16, 0x8

    aget-char v17, v12, v15

    rem-int/lit8 v3, v15, 0x7

    if-eqz v3, :cond_8

    if-eq v3, v11, :cond_7

    if-eq v3, v2, :cond_6

    const/4 v2, 0x3

    if-eq v3, v2, :cond_5

    if-eq v3, v0, :cond_4

    const/4 v2, 0x5

    if-eq v3, v2, :cond_9

    const/16 v0, 0x19

    goto :goto_4

    :cond_4
    const/16 v0, 0x42

    goto :goto_4

    :cond_5
    const/16 v0, 0x4a

    goto :goto_4

    :cond_6
    const/16 v0, 0x50

    goto :goto_4

    :cond_7
    const/16 v0, 0x54

    goto :goto_4

    :cond_8
    const/16 v0, 0x76

    :cond_9
    :goto_4
    xor-int/2addr v0, v10

    xor-int v0, v17, v0

    int-to-char v0, v0

    aput-char v0, v12, v15

    add-int/lit8 v15, v15, 0x1

    move/from16 v0, v16

    const/16 v3, 0xa

    goto/16 :goto_2
.end method

.method constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ak:I

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aS:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aT:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    const/16 v0, 0x10

    new-array v1, v0, [S

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aU:[S

    new-array v0, v0, [S

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aV:[S

    const/16 v0, 0x23d

    new-array v1, v0, [I

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aZ:[B

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a_:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    const/16 p1, 0x47a

    new-array p1, p1, [S

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aO:[S

    const/16 p1, 0x7a

    new-array p1, p1, [S

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aP:[S

    const/16 p1, 0x4e

    new-array p1, p1, [S

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    return-void
.end method

.method private a(IIIII)I
    .locals 8

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    const/4 v1, 0x6

    if-nez v0, :cond_1

    const/4 v2, -0x1

    if-ne p1, v2, :cond_0

    move p1, v1

    :cond_0
    move v2, p1

    move p1, p3

    goto :goto_0

    :cond_1
    move v2, p1

    :goto_0
    const/16 v3, 0xf

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-nez v0, :cond_3

    if-gez p1, :cond_2

    const/4 p1, 0x0

    neg-int p3, p3

    if-eqz v0, :cond_5

    goto :goto_1

    :cond_2
    move p1, v5

    :goto_1
    move v6, p3

    move p3, p1

    move p1, v6

    goto :goto_2

    :cond_3
    move v6, p3

    move p3, v5

    :goto_2
    if-nez v0, :cond_6

    if-le p1, v3, :cond_4

    add-int/lit8 p3, v6, -0x10

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;

    invoke-direct {v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;-><init>()V

    iput-object v6, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    move p1, v4

    goto :goto_3

    :cond_4
    move p1, p3

    move p3, v6

    :cond_5
    :goto_3
    if-nez v0, :cond_c

    move v6, p3

    move v7, v5

    move p3, p1

    move p1, p4

    goto :goto_4

    :cond_6
    move v7, v3

    :goto_4
    if-lt p1, v7, :cond_d

    if-nez v0, :cond_c

    const/16 p1, 0x9

    if-gt p4, p1, :cond_d

    if-nez v0, :cond_e

    const/16 v7, 0x8

    if-ne p2, v7, :cond_d

    if-nez v0, :cond_b

    if-lt v6, p1, :cond_d

    if-nez v0, :cond_b

    if-gt v6, v3, :cond_d

    if-nez v0, :cond_a

    if-ltz v2, :cond_d

    if-nez v0, :cond_a

    if-gt v2, p1, :cond_d

    if-nez v0, :cond_9

    if-ltz p5, :cond_d

    if-nez v0, :cond_8

    if-le p5, v4, :cond_7

    goto :goto_5

    :cond_7
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput-object p0, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ak:I

    iput v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ap:I

    shl-int p1, v5, v6

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ao:I

    add-int/lit8 p3, p1, -0x1

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aq:I

    add-int/lit8 p3, p4, 0x7

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ax:I

    shl-int v0, v5, p3

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aw:I

    add-int/lit8 v3, v0, -0x1

    iput v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ay:I

    add-int/lit8 p3, p3, 0x3

    sub-int/2addr p3, v5

    div-int/lit8 p3, p3, 0x3

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->az:I

    mul-int/lit8 p3, p1, 0x2

    new-array p3, p3, [B

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    new-array p1, p1, [S

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->at:[S

    new-array p1, v0, [S

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->au:[S

    add-int/2addr p4, v1

    shl-int p1, v5, p4

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a1:I

    mul-int/lit8 p3, p1, 0x4

    new-array p3, p3, [B

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ag:[B

    mul-int/lit8 p3, p1, 0x4

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ah:I

    div-int/lit8 p3, p1, 0x2

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a3:I

    mul-int/lit8 p1, p1, 0x3

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a0:I

    iput v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I

    iput p5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aL:I

    int-to-byte p1, p2

    iput-byte p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->am:B

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->j()I

    move-result p5

    :cond_8
    return p5

    :cond_9
    move p2, p5

    goto :goto_6

    :cond_a
    move p2, v2

    goto :goto_6

    :cond_b
    move p2, v6

    goto :goto_6

    :cond_c
    move p2, p4

    goto :goto_6

    :cond_d
    :goto_5
    const/4 p2, -0x2

    :cond_e
    :goto_6
    return p2
.end method

.method static a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)I
    .locals 5

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    const/4 p0, -0x2

    return p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(Ljava/lang/CloneNotSupportedException;)Ljava/lang/CloneNotSupportedException;

    move-result-object p0

    throw p0

    :cond_0
    :try_start_1
    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_4

    const/4 v2, 0x0

    if-nez v0, :cond_2

    if-eqz v1, :cond_1

    :try_start_2
    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    array-length v1, v1

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v4, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    array-length v4, v4

    invoke-static {v1, v2, v3, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_2
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_2 .. :try_end_2} :catch_5

    :cond_1
    :try_start_3
    iget v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-wide v3, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iput-wide v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    if-nez v0, :cond_4

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B
    :try_end_3
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(Ljava/lang/CloneNotSupportedException;)Ljava/lang/CloneNotSupportedException;

    move-result-object p0

    throw p0

    :cond_2
    :goto_0
    if-eqz v1, :cond_3

    :try_start_4
    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B

    array-length v0, v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B

    iget-object v3, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B

    array-length v3, v3

    invoke-static {v0, v2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_4
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(Ljava/lang/CloneNotSupportedException;)Ljava/lang/CloneNotSupportedException;

    move-result-object p0

    throw p0

    :cond_3
    :goto_1
    iget v0, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOutIndex:I

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOutIndex:I

    iget v0, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    iget-wide v0, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalOut:J

    iput-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalOut:J

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    iget v0, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->t:I

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->t:I

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->copy()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    :try_start_5
    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    move-object p1, p0

    :cond_4
    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iput-object p0, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;
    :try_end_5
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    return v2

    :catch_4
    move-exception p0

    :try_start_6
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(Ljava/lang/CloneNotSupportedException;)Ljava/lang/CloneNotSupportedException;

    move-result-object p0

    throw p0
    :try_end_6
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_6 .. :try_end_6} :catch_5

    :catch_5
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(Ljava/lang/CloneNotSupportedException;)Ljava/lang/CloneNotSupportedException;

    move-result-object p0

    throw p0
.end method

.method private static a(Ljava/lang/CloneNotSupportedException;)Ljava/lang/CloneNotSupportedException;
    .locals 0

    return-object p0
.end method

.method static a([SII[B)Z
    .locals 3

    mul-int/lit8 v0, p1, 0x2

    aget-short v0, p0, v0

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v1

    mul-int/lit8 v2, p2, 0x2

    aget-short p0, p0, v2

    if-nez v1, :cond_3

    if-lt v0, p0, :cond_2

    if-nez v1, :cond_0

    if-ne v0, p0, :cond_1

    aget-byte v0, p3, p1

    if-nez v1, :cond_3

    aget-byte p0, p3, p2

    :cond_0
    if-gt v0, p0, :cond_1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :cond_3
    move p0, v0

    :goto_1
    return p0
.end method

.method private a([B)[B
    .locals 3

    array-length v0, p1

    new-array v1, v0, [B

    const/4 v2, 0x0

    invoke-static {p1, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v1
.end method

.method private a([I)[I
    .locals 3

    array-length v0, p1

    new-array v1, v0, [I

    const/4 v2, 0x0

    invoke-static {p1, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v1
.end method

.method private a([S)[S
    .locals 3

    array-length v0, p1

    new-array v1, v0, [S

    const/4 v2, 0x0

    invoke-static {p1, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v1
.end method


# virtual methods
.method a([BI)I
    .locals 8

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-eqz p1, :cond_8

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->af:I

    const/16 v2, 0x2a

    const/4 v3, 0x3

    const/4 v4, 0x0

    if-nez v0, :cond_1

    if-eq v1, v2, :cond_0

    goto/16 :goto_3

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v1, p1, v4, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->update([BII)V

    move v1, p2

    move v2, v3

    :cond_1
    if-nez v0, :cond_4

    if-ge v1, v2, :cond_2

    return v4

    :cond_2
    if-nez v0, :cond_3

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ao:I

    add-int/lit16 v2, v1, -0x106

    move v1, p2

    goto :goto_0

    :cond_3
    move v1, p2

    goto :goto_2

    :cond_4
    :goto_0
    if-le v1, v2, :cond_5

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ao:I

    add-int/lit16 v1, v1, -0x106

    sub-int/2addr p2, v1

    goto :goto_1

    :cond_5
    move v1, p2

    move p2, v4

    :goto_1
    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    invoke-static {p1, p2, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aA:I

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    aget-byte p2, p1, v4

    and-int/lit16 p2, p2, 0xff

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->az:I

    shl-int/2addr p2, v2

    const/4 v2, 0x1

    aget-byte p1, p1, v2

    and-int/lit16 p1, p1, 0xff

    xor-int/2addr p1, p2

    iget p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ay:I

    and-int/2addr p1, p2

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    move p2, v4

    :cond_6
    :goto_2
    add-int/lit8 p1, v1, -0x3

    if-gt p2, p1, :cond_7

    iget p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->az:I

    shl-int/2addr p1, v2

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    add-int/lit8 v5, p2, 0x2

    aget-byte v2, v2, v5

    and-int/lit16 v2, v2, 0xff

    xor-int/2addr p1, v2

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ay:I

    and-int/2addr p1, v2

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->at:[S

    iget v5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aq:I

    and-int/2addr v5, p2

    iget-object v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->au:[S

    aget-short v7, v6, p1

    aput-short v7, v2, v5

    int-to-short v2, p2

    aput-short v2, v6, p1

    add-int/lit8 p2, p2, 0x1

    if-eqz v0, :cond_6

    :cond_7
    return v4

    :cond_8
    :goto_3
    const/4 p1, -0x2

    return p1
.end method

.method a()V
    .locals 5

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ao:I

    const/4 v1, 0x2

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->as:I

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->au:[S

    iget v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aw:I

    add-int/lit8 v3, v3, -0x1

    const/4 v4, 0x0

    aput-short v4, v2, v3

    move v2, v4

    :cond_0
    iget v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aw:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_1

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->au:[S

    aput-short v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_2

    if-eqz v0, :cond_0

    :cond_1
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I

    aget-object v0, v0, v2

    iget v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;->b:I

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aJ:I

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I

    aget-object v0, v0, v2

    iget v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;->a:I

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aM:I

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I

    aget-object v0, v0, v2

    iget v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;->c:I

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aN:I

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I

    aget-object v0, v0, v2

    iget v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;->d:I

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aI:I

    iput v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iput v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aA:I

    iput v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aH:I

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    iput v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aD:I

    iput v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    :cond_2
    return-void
.end method

.method final a(B)V
    .locals 3

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ag:[B

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aj:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aj:I

    aput-byte p1, v0, v1

    return-void
.end method

.method final a(I)V
    .locals 1

    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    ushr-int/lit8 p1, p1, 0x8

    int-to-byte p1, p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    return-void
.end method

.method a(II)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_1

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a9:I

    rsub-int/lit8 v2, p2, 0x10

    const v3, 0xffff

    if-le v1, v2, :cond_0

    iget-short v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a8:S

    shl-int v1, p1, v1

    and-int/2addr v1, v3

    or-int/2addr v1, v2

    int-to-short v1, v1

    iput-short v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a8:S

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I)V

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a9:I

    rsub-int/lit8 v2, v1, 0x10

    ushr-int v2, p1, v2

    int-to-short v2, v2

    iput-short v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a8:S

    add-int/lit8 v2, p2, -0x10

    add-int/2addr v1, v2

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a9:I

    if-eqz v0, :cond_2

    :cond_0
    iget-short v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a8:S

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a9:I

    shl-int/2addr p1, v1

    and-int/2addr p1, v3

    or-int/2addr p1, v0

    int-to-short p1, p1

    iput-short p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a8:S

    :cond_1
    iget p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a9:I

    add-int/2addr p1, p2

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a9:I

    :cond_2
    return-void
.end method

.method a(III)V
    .locals 4

    add-int/lit16 v0, p1, -0x101

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(II)V

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    add-int/lit8 p2, p2, -0x1

    invoke-virtual {p0, p2, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(II)V

    add-int/lit8 v1, p3, -0x4

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(II)V

    const/4 v1, 0x0

    :cond_0
    if-ge v1, p3, :cond_1

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->p:[B

    aget-byte v3, v3, v1

    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x1

    aget-short v2, v2, v3

    const/4 v3, 0x3

    invoke-virtual {p0, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(II)V

    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_2

    if-eqz v0, :cond_0

    :cond_1
    iget-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aO:[S

    add-int/lit8 p1, p1, -0x1

    invoke-virtual {p0, p3, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->c([SI)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aP:[S

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->c([SI)V

    :cond_2
    return-void
.end method

.method a(IIZ)V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h()V

    const/16 v1, 0x8

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a7:I

    if-nez v0, :cond_1

    if-eqz p3, :cond_0

    int-to-short p3, p2

    invoke-virtual {p0, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I)V

    not-int p3, p2

    int-to-short p3, p3

    invoke-virtual {p0, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I)V

    :cond_0
    iget-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    invoke-virtual {p0, p3, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([BII)V

    :cond_1
    return-void
.end method

.method final a(I[S)V
    .locals 2

    mul-int/lit8 p1, p1, 0x2

    aget-short v0, p2, p1

    const v1, 0xffff

    and-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x1

    aget-short p1, p2, p1

    and-int/2addr p1, v1

    invoke-virtual {p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(II)V

    return-void
.end method

.method a(Z)V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aA:I

    if-nez v0, :cond_1

    if-ltz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, -0x1

    :cond_1
    :goto_0
    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aA:I

    sub-int/2addr v0, v2

    invoke-virtual {p0, v1, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->c(IIZ)V

    iget p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aA:I

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->a()V

    return-void
.end method

.method final a([BII)V
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ag:[B

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aj:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aj:I

    add-int/2addr p1, p3

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aj:I

    return-void
.end method

.method a([SI)V
    .locals 8

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    aget v0, v0, p2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v1

    shl-int/lit8 v2, p2, 0x1

    :cond_0
    iget v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aX:I

    if-gt v2, v3, :cond_6

    if-nez v1, :cond_3

    if-ge v2, v3, :cond_1

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    add-int/lit8 v4, v2, 0x1

    aget v5, v3, v4

    aget v3, v3, v2

    iget-object v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aZ:[B

    invoke-static {p1, v5, v3, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([SII[B)Z

    move-result v3

    if-nez v1, :cond_2

    if-eqz v3, :cond_1

    move v2, v4

    :cond_1
    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    aget v3, v3, v2

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aZ:[B

    invoke-static {p1, v0, v3, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([SII[B)Z

    move-result v3

    :cond_2
    move v7, v3

    move v3, v2

    move v2, v7

    goto :goto_0

    :cond_3
    move v3, v2

    :goto_0
    if-nez v1, :cond_5

    if-eqz v2, :cond_4

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    aget v4, v2, v3

    aput v4, v2, p2

    shl-int/lit8 p2, v3, 0x1

    move v2, p2

    move p2, v3

    :cond_5
    if-eqz v1, :cond_0

    :cond_6
    :goto_1
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    aput v0, p1, p2

    return-void
.end method

.method a([S[S)V
    .locals 7

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_7

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a2:I

    if-eqz v1, :cond_6

    const/4 v1, 0x0

    :cond_0
    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ag:[B

    iget v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a3:I

    mul-int/lit8 v4, v1, 0x2

    add-int v5, v3, v4

    aget-byte v5, v2, v5

    shl-int/lit8 v5, v5, 0x8

    const v6, 0xff00

    and-int/2addr v5, v6

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    aget-byte v3, v2, v3

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v3, v5

    iget v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a0:I

    add-int/2addr v4, v1

    aget-byte v2, v2, v4

    and-int/lit16 v2, v2, 0xff

    add-int/lit8 v1, v1, 0x1

    if-nez v3, :cond_1

    invoke-virtual {p0, v2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I[S)V

    if-eqz v0, :cond_4

    :cond_1
    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->t:[B

    aget-byte v4, v4, v2

    add-int/lit16 v5, v4, 0x100

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {p0, v5, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I[S)V

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->m:[I

    aget v5, v5, v4

    if-nez v0, :cond_3

    if-eqz v5, :cond_2

    sget-object v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->u:[I

    aget v4, v6, v4

    sub-int/2addr v2, v4

    invoke-virtual {p0, v2, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(II)V

    :cond_2
    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->a(I)I

    move-result v4

    invoke-virtual {p0, v4, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I[S)V

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->n:[I

    aget v5, v2, v4

    :cond_3
    if-nez v0, :cond_5

    if-eqz v5, :cond_4

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->v:[I

    aget v2, v2, v4

    sub-int/2addr v3, v2

    invoke-virtual {p0, v3, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(II)V

    :cond_4
    move v5, v1

    :cond_5
    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a2:I

    if-lt v5, v2, :cond_0

    :cond_6
    const/16 p2, 0x100

    invoke-virtual {p0, p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I[S)V

    :cond_7
    const/16 p2, 0x201

    aget-short p1, p1, p2

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a7:I

    return-void
.end method

.method b(III)I
    .locals 6

    const/16 v2, 0x8

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(IIIII)I

    move-result p1

    return p1
.end method

.method b()V
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aO:[S

    iput-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->w:[S

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;

    iput-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aS:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aP:[S

    iput-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->w:[S

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aS:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;->k:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;

    iput-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aT:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    iput-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->w:[S

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aT:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;->l:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;

    iput-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;

    const/4 v0, 0x0

    iput-short v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a8:S

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a9:I

    const/16 v0, 0x8

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a7:I

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->c()V

    return-void
.end method

.method final b(I)V
    .locals 1

    shr-int/lit8 v0, p1, 0x8

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    int-to-byte p1, p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    return-void
.end method

.method b(IIZ)V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_1

    if-eqz p3, :cond_0

    move p3, v1

    goto :goto_0

    :cond_0
    move p3, v2

    :cond_1
    :goto_0
    add-int/2addr v2, p3

    const/4 p3, 0x3

    invoke-virtual {p0, v2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(II)V

    invoke-virtual {p0, p1, p2, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(IIZ)V

    return-void
.end method

.method b([SI)V
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p2

    const/4 v2, 0x1

    aget-short v3, p1, v2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v4

    const/4 v7, 0x7

    const/4 v8, -0x1

    const/4 v9, 0x3

    const/4 v10, 0x0

    if-nez v4, :cond_1

    if-nez v3, :cond_0

    move v12, v9

    const/16 v11, 0x8a

    goto :goto_0

    :cond_0
    move v11, v7

    const/4 v12, 0x4

    :goto_0
    add-int/lit8 v13, v1, 0x1

    mul-int/lit8 v13, v13, 0x2

    add-int/2addr v13, v2

    aput-short v8, p1, v13

    move v13, v10

    goto :goto_1

    :cond_1
    move v13, v3

    move v11, v7

    const/4 v12, 0x4

    :goto_1
    move v14, v10

    :goto_2
    if-gt v13, v1, :cond_13

    add-int/lit8 v13, v13, 0x1

    mul-int/lit8 v15, v13, 0x2

    add-int/2addr v15, v2

    aget-short v15, p1, v15

    add-int/2addr v14, v2

    if-nez v4, :cond_4

    if-ge v14, v11, :cond_3

    if-nez v4, :cond_2

    if-ne v3, v15, :cond_3

    goto/16 :goto_9

    :cond_2
    move v5, v3

    move v12, v15

    goto :goto_3

    :cond_3
    move v5, v14

    if-nez v4, :cond_6

    goto :goto_3

    :cond_4
    move v12, v11

    move v5, v14

    :goto_3
    if-ge v5, v12, :cond_5

    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    mul-int/lit8 v12, v3, 0x2

    aget-short v16, v5, v12

    add-int v6, v16, v14

    int-to-short v6, v6

    aput-short v6, v5, v12

    if-eqz v4, :cond_b

    :cond_5
    move v5, v3

    :cond_6
    if-nez v4, :cond_8

    if-eqz v5, :cond_9

    if-eq v3, v8, :cond_7

    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    mul-int/lit8 v6, v3, 0x2

    aget-short v8, v5, v6

    add-int/2addr v8, v2

    int-to-short v8, v8

    aput-short v8, v5, v6

    :cond_7
    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    const/16 v6, 0x20

    aget-short v8, v5, v6

    add-int/2addr v8, v2

    int-to-short v8, v8

    aput-short v8, v5, v6

    if-eqz v4, :cond_b

    goto :goto_4

    :cond_8
    move v14, v5

    :cond_9
    :goto_4
    const/16 v5, 0xa

    if-gt v14, v5, :cond_a

    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    const/16 v6, 0x22

    aget-short v8, v5, v6

    add-int/2addr v8, v2

    int-to-short v8, v8

    aput-short v8, v5, v6

    if-eqz v4, :cond_b

    :cond_a
    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    const/16 v6, 0x24

    aget-short v8, v5, v6

    add-int/2addr v8, v2

    int-to-short v8, v8

    aput-short v8, v5, v6

    :cond_b
    if-nez v4, :cond_e

    if-nez v15, :cond_d

    if-eqz v4, :cond_c

    const/16 v11, 0x8a

    goto :goto_5

    :cond_c
    move v8, v3

    move v12, v9

    move v14, v10

    const/16 v11, 0x8a

    goto :goto_9

    :cond_d
    :goto_5
    move v5, v3

    goto :goto_6

    :cond_e
    move v5, v15

    :goto_6
    if-nez v4, :cond_11

    if-ne v5, v15, :cond_10

    const/4 v5, 0x6

    if-eqz v4, :cond_f

    goto :goto_7

    :cond_f
    move v8, v3

    move v11, v5

    move v12, v9

    goto :goto_8

    :cond_10
    :goto_7
    move v11, v7

    const/4 v5, 0x4

    :cond_11
    move v8, v3

    move v12, v5

    :goto_8
    move v14, v10

    :goto_9
    if-eqz v4, :cond_12

    goto :goto_a

    :cond_12
    move v3, v15

    goto/16 :goto_2

    :cond_13
    :goto_a
    return-void
.end method

.method b(II)Z
    .locals 15

    move-object v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ag:[B

    iget v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a3:I

    iget v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a2:I

    const/4 v6, 0x2

    mul-int/2addr v5, v6

    add-int/2addr v4, v5

    ushr-int/lit8 v5, v1, 0x8

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v3

    iget-object v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ag:[B

    iget v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a3:I

    iget v7, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a2:I

    mul-int/lit8 v8, v7, 0x2

    add-int/2addr v5, v8

    const/4 v8, 0x1

    add-int/2addr v5, v8

    int-to-byte v9, v1

    aput-byte v9, v4, v5

    iget v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a0:I

    add-int/2addr v5, v7

    int-to-byte v9, v2

    aput-byte v9, v4, v5

    add-int/2addr v7, v8

    iput v7, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a2:I

    if-nez v3, :cond_1

    if-nez v1, :cond_0

    iget-object v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aO:[S

    mul-int/lit8 v5, v2, 0x2

    aget-short v7, v4, v5

    add-int/2addr v7, v8

    int-to-short v7, v7

    aput-short v7, v4, v5

    if-eqz v3, :cond_2

    :cond_0
    iget v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a6:I

    add-int/2addr v4, v8

    iput v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a6:I

    add-int/lit8 v1, v1, -0x1

    iget-object v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aO:[S

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->t:[B

    aget-byte v2, v5, v2

    add-int/lit16 v2, v2, 0x100

    add-int/2addr v2, v8

    mul-int/2addr v2, v6

    aget-short v5, v4, v2

    add-int/2addr v5, v8

    int-to-short v5, v5

    aput-short v5, v4, v2

    :cond_1
    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aP:[S

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->a(I)I

    move-result v1

    mul-int/2addr v1, v6

    aget-short v4, v2, v1

    add-int/2addr v4, v8

    int-to-short v4, v4

    aput-short v4, v2, v1

    :cond_2
    iget v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a2:I

    and-int/lit16 v2, v1, 0x1fff

    const/4 v4, 0x0

    if-nez v3, :cond_a

    if-nez v2, :cond_9

    iget v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I

    if-nez v3, :cond_8

    if-le v2, v6, :cond_9

    mul-int/lit8 v1, v1, 0x8

    iget v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iget v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aA:I

    sub-int/2addr v2, v5

    move v5, v4

    :goto_0
    const/16 v7, 0x1e

    if-ge v5, v7, :cond_4

    int-to-long v9, v1

    iget-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aP:[S

    mul-int/lit8 v7, v5, 0x2

    aget-short v1, v1, v7

    int-to-long v11, v1

    const-wide/16 v13, 0x5

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->n:[I

    aget v1, v1, v5

    int-to-long v6, v1

    add-long/2addr v6, v13

    mul-long/2addr v11, v6

    add-long/2addr v9, v11

    long-to-int v1, v9

    add-int/lit8 v5, v5, 0x1

    if-nez v3, :cond_5

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_3
    const/4 v6, 0x2

    goto :goto_0

    :cond_4
    :goto_1
    ushr-int/lit8 v1, v1, 0x3

    :cond_5
    iget v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a6:I

    iget v6, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a2:I

    const/4 v7, 0x2

    div-int/2addr v6, v7

    if-nez v3, :cond_7

    if-ge v5, v6, :cond_9

    div-int/lit8 v6, v2, 0x2

    if-nez v3, :cond_6

    if-ge v1, v6, :cond_9

    return v8

    :cond_6
    move v2, v1

    goto :goto_2

    :cond_7
    move v2, v5

    goto :goto_2

    :cond_8
    move v7, v6

    goto :goto_2

    :cond_9
    iget v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a2:I

    :cond_a
    if-nez v3, :cond_b

    iget v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a1:I

    add-int/lit8 v6, v1, -0x1

    :goto_2
    if-ne v2, v6, :cond_c

    goto :goto_3

    :cond_b
    move v8, v2

    :goto_3
    move v4, v8

    :cond_c
    return v4
.end method

.method c(I)I
    .locals 7

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ah:I

    add-int/lit8 v2, v1, -0x5

    const/4 v3, 0x1

    const v4, 0xffff

    if-nez v0, :cond_1

    if-le v4, v2, :cond_0

    add-int/lit8 v1, v1, -0x5

    move-object v5, p0

    goto/16 :goto_4

    :cond_0
    move-object v5, p0

    move v1, v4

    goto/16 :goto_4

    :cond_1
    move-object v5, p0

    move v1, v4

    :goto_0
    const/4 v6, 0x0

    if-gt v4, v2, :cond_2

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->i()V

    iget v2, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    goto :goto_1

    :cond_2
    iget v2, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iget v4, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    add-int/2addr v2, v4

    iput v2, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iput v6, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    iget v2, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aA:I

    add-int/2addr v2, v1

    :cond_3
    iget v4, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    if-nez v0, :cond_5

    if-eqz v4, :cond_4

    if-nez v0, :cond_8

    if-lt v4, v2, :cond_6

    :cond_4
    iget v4, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    sub-int/2addr v4, v2

    iput v4, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    iput v2, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    invoke-virtual {v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(Z)V

    iget-object v2, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    :cond_5
    if-nez v0, :cond_7

    if-nez v4, :cond_6

    return v6

    :cond_6
    iget v2, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iget v4, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aA:I

    sub-int/2addr v2, v4

    move v4, v2

    :cond_7
    if-nez v0, :cond_9

    iget v2, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ao:I

    add-int/lit16 v2, v2, -0x106

    :cond_8
    if-lt v4, v2, :cond_16

    invoke-virtual {v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(Z)V

    iget-object v2, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    :cond_9
    if-nez v4, :cond_16

    if-nez v0, :cond_a

    return v6

    :cond_a
    move v2, v6

    :goto_1
    if-nez v0, :cond_d

    if-nez v2, :cond_c

    if-nez v0, :cond_b

    if-nez p1, :cond_c

    return v6

    :cond_b
    move v2, p1

    goto :goto_2

    :cond_c
    iget v2, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    :cond_d
    :goto_2
    if-nez v0, :cond_3

    if-nez v2, :cond_2

    const/4 v1, 0x4

    if-nez v0, :cond_f

    if-ne p1, v1, :cond_e

    move v2, v3

    goto :goto_3

    :cond_e
    move v2, v6

    goto :goto_3

    :cond_f
    move v2, p1

    :goto_3
    invoke-virtual {v5, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(Z)V

    iget-object v2, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    if-nez v0, :cond_12

    if-nez v2, :cond_13

    if-nez v0, :cond_10

    if-ne p1, v1, :cond_11

    const/4 p1, 0x2

    :cond_10
    move v6, p1

    :cond_11
    return v6

    :cond_12
    move p1, v2

    :cond_13
    if-nez v0, :cond_14

    if-ne p1, v1, :cond_15

    const/4 p1, 0x3

    :cond_14
    move v3, p1

    :cond_15
    return v3

    :cond_16
    :goto_4
    iget v4, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    move v2, v3

    goto/16 :goto_0
.end method

.method c(II)I
    .locals 6

    const/16 v2, 0x8

    const/16 v4, 0x8

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(IIIII)I

    move-result p1

    return p1
.end method

.method c()V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const/4 v1, 0x0

    move v2, v1

    :cond_0
    const/16 v3, 0x11e

    if-ge v2, v3, :cond_1

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aO:[S

    mul-int/lit8 v4, v2, 0x2

    aput-short v1, v3, v4

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_2

    if-eqz v0, :cond_0

    :cond_1
    move v2, v1

    :cond_2
    const/16 v3, 0x1e

    if-ge v2, v3, :cond_3

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aP:[S

    mul-int/lit8 v4, v2, 0x2

    aput-short v1, v3, v4

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_4

    if-eqz v0, :cond_2

    :cond_3
    move v2, v1

    :cond_4
    const/16 v3, 0x13

    if-ge v2, v3, :cond_5

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    mul-int/lit8 v4, v2, 0x2

    aput-short v1, v3, v4

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_6

    if-eqz v0, :cond_4

    :cond_5
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aO:[S

    const/16 v2, 0x200

    const/4 v3, 0x1

    aput-short v3, v0, v2

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a5:I

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a4:I

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a6:I

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a2:I

    :cond_6
    return-void
.end method

.method c(IIZ)V
    .locals 9

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x3

    if-nez v0, :cond_5

    if-lez v1, :cond_4

    iget-byte v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->al:B

    if-nez v0, :cond_1

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->f()V

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    invoke-virtual {v1, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->b(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aS:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    invoke-virtual {v1, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->b(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->d()I

    move-result v1

    iget v5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a4:I

    add-int/2addr v5, v4

    add-int/lit8 v5, v5, 0x7

    move v6, v4

    move v8, v5

    move v5, v1

    move v1, v8

    goto :goto_0

    :cond_1
    move v6, v2

    move v5, v3

    :goto_0
    ushr-int/2addr v1, v6

    iget v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a5:I

    add-int/2addr v6, v4

    add-int/lit8 v6, v6, 0x7

    ushr-int/2addr v6, v4

    if-nez v0, :cond_3

    if-gt v6, v1, :cond_6

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    move v1, v6

    goto :goto_3

    :cond_3
    move v7, v6

    goto :goto_4

    :cond_4
    move v5, v3

    :goto_1
    add-int/lit8 v1, p2, 0x5

    goto :goto_2

    :cond_5
    move v5, v3

    :goto_2
    move v6, v1

    :cond_6
    :goto_3
    add-int/lit8 v7, p2, 0x4

    move v8, v7

    move v7, v6

    move v6, v8

    :goto_4
    if-nez v0, :cond_9

    if-gt v6, v1, :cond_8

    const/4 v6, -0x1

    if-nez v0, :cond_7

    if-eq p1, v6, :cond_8

    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b(IIZ)V

    if-eqz v0, :cond_f

    goto :goto_5

    :cond_7
    move v1, v6

    goto :goto_6

    :cond_8
    :goto_5
    move p1, v7

    goto :goto_6

    :cond_9
    move p1, v6

    :goto_6
    const/4 p2, 0x1

    if-ne p1, v1, :cond_c

    if-nez v0, :cond_b

    if-eqz p3, :cond_a

    move p1, p2

    goto :goto_7

    :cond_a
    move p1, v3

    goto :goto_7

    :cond_b
    move p1, p3

    :goto_7
    add-int/2addr v2, p1

    invoke-virtual {p0, v2, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(II)V

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;->h:[S

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;->i:[S

    invoke-virtual {p0, p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([S[S)V

    if-eqz v0, :cond_f

    :cond_c
    if-nez v0, :cond_d

    if-eqz p3, :cond_e

    move v3, p2

    goto :goto_8

    :cond_d
    move v3, p3

    :cond_e
    :goto_8
    const/4 p1, 0x4

    add-int/2addr p1, v3

    invoke-virtual {p0, p1, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(II)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->x:I

    add-int/2addr p1, p2

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aS:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->x:I

    add-int/2addr v1, p2

    add-int/2addr v5, p2

    invoke-virtual {p0, p1, v1, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(III)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aO:[S

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aP:[S

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([S[S)V

    :cond_f
    if-nez v0, :cond_10

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->c()V

    if-eqz p3, :cond_11

    :cond_10
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h()V

    :cond_11
    return-void
.end method

.method c([SI)V
    .locals 17

    move-object/from16 v0, p0

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v1

    const/4 v2, 0x1

    aget-short v3, p1, v2

    const/4 v5, 0x4

    const/4 v6, 0x7

    const/4 v7, 0x0

    const/4 v8, 0x3

    if-nez v1, :cond_1

    if-nez v3, :cond_0

    move v10, v8

    const/16 v9, 0x8a

    goto :goto_0

    :cond_0
    move v10, v5

    move v9, v6

    :goto_0
    move v11, v7

    goto :goto_1

    :cond_1
    move v11, v3

    move v10, v5

    move v9, v6

    :goto_1
    const/4 v12, -0x1

    move/from16 v13, p2

    move v14, v7

    move v15, v12

    :goto_2
    if-gt v11, v13, :cond_14

    add-int/lit8 v11, v11, 0x1

    mul-int/lit8 v16, v11, 0x2

    add-int/lit8 v16, v16, 0x1

    aget-short v4, p1, v16

    add-int/2addr v14, v2

    if-nez v1, :cond_4

    if-ge v14, v9, :cond_3

    if-nez v1, :cond_2

    if-ne v3, v4, :cond_3

    goto/16 :goto_8

    :cond_2
    move v2, v3

    move v10, v4

    goto :goto_3

    :cond_3
    move v2, v14

    if-nez v1, :cond_7

    goto :goto_3

    :cond_4
    move v10, v9

    move v2, v14

    :goto_3
    if-ge v2, v10, :cond_6

    :cond_5
    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    invoke-virtual {v0, v3, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I[S)V

    add-int/2addr v14, v12

    if-nez v14, :cond_5

    goto :goto_4

    :cond_6
    move v2, v3

    :cond_7
    if-nez v1, :cond_a

    if-eqz v2, :cond_9

    if-eq v3, v15, :cond_8

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    invoke-virtual {v0, v3, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I[S)V

    add-int/lit8 v14, v14, -0x1

    :cond_8
    const/16 v2, 0x10

    iget-object v10, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    invoke-virtual {v0, v2, v10}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I[S)V

    add-int/lit8 v2, v14, -0x3

    const/4 v10, 0x2

    invoke-virtual {v0, v2, v10}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(II)V

    if-eqz v1, :cond_c

    :cond_9
    move v2, v14

    :cond_a
    const/16 v10, 0xa

    if-gt v2, v10, :cond_b

    const/16 v2, 0x11

    iget-object v10, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    invoke-virtual {v0, v2, v10}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I[S)V

    add-int/lit8 v2, v14, -0x3

    invoke-virtual {v0, v2, v8}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(II)V

    if-eqz v1, :cond_c

    :cond_b
    const/16 v2, 0x12

    iget-object v10, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    invoke-virtual {v0, v2, v10}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I[S)V

    add-int/lit8 v14, v14, -0xb

    invoke-virtual {v0, v14, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(II)V

    :cond_c
    :goto_4
    if-nez v1, :cond_f

    if-nez v4, :cond_e

    if-eqz v1, :cond_d

    const/16 v9, 0x8a

    goto :goto_5

    :cond_d
    move v15, v3

    move v14, v7

    move v10, v8

    const/16 v9, 0x8a

    goto :goto_8

    :cond_e
    :goto_5
    move v2, v3

    goto :goto_6

    :cond_f
    move v2, v4

    :goto_6
    if-nez v1, :cond_12

    if-ne v2, v4, :cond_11

    const/4 v2, 0x6

    if-eqz v1, :cond_10

    goto :goto_7

    :cond_10
    move v9, v2

    move v15, v3

    move v14, v7

    move v10, v8

    goto :goto_8

    :cond_11
    :goto_7
    move v2, v5

    move v9, v6

    :cond_12
    move v10, v2

    move v15, v3

    move v14, v7

    :goto_8
    if-eqz v1, :cond_13

    goto :goto_9

    :cond_13
    move v3, v4

    const/4 v2, 0x1

    goto/16 :goto_2

    :cond_14
    :goto_9
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iget-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ag:[B

    invoke-direct {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([B)[B

    move-result-object v1

    iput-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ag:[B

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v1

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    invoke-direct {p0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([B)[B

    move-result-object v2

    iput-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    :try_start_0
    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->at:[S

    invoke-direct {p0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([S)[S

    move-result-object v2

    iput-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->at:[S

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->au:[S

    invoke-direct {p0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([S)[S

    move-result-object v2

    iput-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->au:[S

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aO:[S

    invoke-direct {p0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([S)[S

    move-result-object v2

    iput-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aO:[S

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aP:[S

    invoke-direct {p0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([S)[S

    move-result-object v2

    iput-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aP:[S

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    invoke-direct {p0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([S)[S

    move-result-object v2

    iput-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aU:[S

    invoke-direct {p0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([S)[S

    move-result-object v2

    iput-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aU:[S

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aV:[S

    invoke-direct {p0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([S)[S

    move-result-object v2

    iput-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aV:[S

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    invoke-direct {p0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([I)[I

    move-result-object v2

    iput-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aZ:[B

    invoke-direct {p0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([B)[B

    move-result-object v2

    iput-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aZ:[B

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aO:[S

    iput-object v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->w:[S

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aS:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aP:[S

    iput-object v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->w:[S

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aT:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    iput-object v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->w:[S

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a_:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a_:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iput-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a_:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    :cond_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(Ljava/lang/CloneNotSupportedException;)Ljava/lang/CloneNotSupportedException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(Ljava/lang/CloneNotSupportedException;)Ljava/lang/CloneNotSupportedException;

    move-result-object v0

    throw v0
.end method

.method d()I
    .locals 5

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aO:[S

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->x:I

    invoke-virtual {p0, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b([SI)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aP:[S

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aS:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->x:I

    invoke-virtual {p0, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b([SI)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aT:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->b(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;)V

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const/16 v1, 0x12

    :cond_0
    const/4 v2, 0x3

    if-lt v1, v2, :cond_2

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aQ:[S

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->p:[B

    aget-byte v4, v4, v1

    mul-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-short v3, v3, v4

    if-nez v0, :cond_3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v1, -0x1

    if-eqz v0, :cond_0

    :cond_2
    :goto_0
    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a4:I

    add-int/lit8 v3, v1, 0x1

    mul-int/2addr v3, v2

    add-int/lit8 v3, v3, 0x5

    add-int/lit8 v3, v3, 0x5

    add-int/lit8 v3, v3, 0x4

    add-int/2addr v0, v3

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a4:I

    move v3, v1

    :cond_3
    return v3
.end method

.method d(I)I
    .locals 17

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const/4 v1, 0x0

    move-object/from16 v3, p0

    move-object v2, v0

    move v4, v1

    move/from16 v0, p1

    :cond_0
    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    const/16 v6, 0x106

    const/4 v7, 0x2

    const/4 v8, 0x3

    const/4 v9, 0x1

    if-ge v5, v6, :cond_1

    invoke-virtual {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->i()V

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    goto/16 :goto_2

    :cond_1
    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    :cond_2
    const v10, 0xffff

    if-nez v2, :cond_4

    if-lt v5, v8, :cond_3

    iget v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->az:I

    shl-int/2addr v4, v5

    iget-object v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    iget v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    add-int/lit8 v12, v11, 0x2

    aget-byte v5, v5, v12

    and-int/lit16 v5, v5, 0xff

    xor-int/2addr v4, v5

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ay:I

    and-int/2addr v4, v5

    iput v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    iget-object v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->au:[S

    aget-short v12, v5, v4

    and-int/2addr v12, v10

    iget-object v13, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->at:[S

    iget v14, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aq:I

    and-int/2addr v14, v11

    aget-short v15, v5, v4

    aput-short v15, v13, v14

    int-to-short v11, v11

    aput-short v11, v5, v4

    move v4, v12

    :cond_3
    int-to-long v11, v4

    const-wide/16 v13, 0x0

    cmp-long v5, v11, v13

    :cond_4
    if-nez v2, :cond_7

    if-eqz v5, :cond_6

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    sub-int/2addr v5, v4

    and-int/2addr v5, v10

    iget v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ao:I

    sub-int/2addr v11, v6

    if-nez v2, :cond_8

    if-gt v5, v11, :cond_6

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aL:I

    if-nez v2, :cond_5

    if-eq v5, v7, :cond_6

    invoke-virtual {v3, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->f(I)I

    move-result v5

    iput v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    goto :goto_0

    :cond_5
    move v11, v7

    goto :goto_1

    :cond_6
    :goto_0
    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    :cond_7
    if-nez v2, :cond_e

    move v11, v8

    :cond_8
    :goto_1
    if-lt v5, v11, :cond_d

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iget v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aF:I

    sub-int/2addr v5, v11

    iget v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    sub-int/2addr v11, v8

    invoke-virtual {v3, v5, v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b(II)Z

    move-result v5

    iget v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    iget v12, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    sub-int/2addr v11, v12

    iput v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    if-nez v2, :cond_c

    iget v13, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aJ:I

    if-gt v12, v13, :cond_b

    if-nez v2, :cond_c

    if-lt v11, v8, :cond_b

    add-int/lit8 v12, v12, -0x1

    iput v12, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    :cond_9
    iget v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    add-int/2addr v4, v9

    iput v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iget v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    iget v12, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->az:I

    shl-int/2addr v11, v12

    iget-object v12, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    add-int/lit8 v13, v4, 0x2

    aget-byte v12, v12, v13

    and-int/lit16 v12, v12, 0xff

    xor-int/2addr v11, v12

    iget v12, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ay:I

    and-int/2addr v11, v12

    iput v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    iget-object v12, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->au:[S

    aget-short v13, v12, v11

    and-int/2addr v13, v10

    iget-object v14, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->at:[S

    iget v15, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aq:I

    and-int/2addr v15, v4

    aget-short v16, v12, v11

    aput-short v16, v14, v15

    int-to-short v4, v4

    aput-short v4, v12, v11

    :cond_a
    iget v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    sub-int/2addr v4, v9

    iput v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    if-nez v4, :cond_9

    iget v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    add-int/2addr v4, v9

    iput v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    if-nez v2, :cond_a

    move v4, v13

    if-eqz v2, :cond_f

    :cond_b
    iget v10, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iget v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    add-int/2addr v10, v11

    iput v10, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iput v1, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    iget-object v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    aget-byte v10, v11, v10

    and-int/lit16 v10, v10, 0xff

    iput v10, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    :cond_c
    iget v10, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    iget v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->az:I

    shl-int/2addr v10, v11

    iget-object v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    iget v12, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    add-int/2addr v12, v9

    aget-byte v11, v11, v12

    and-int/lit16 v11, v11, 0xff

    xor-int/2addr v10, v11

    iget v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ay:I

    and-int/2addr v10, v11

    iput v10, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    if-eqz v2, :cond_f

    :cond_d
    iget-object v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    iget v10, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    aget-byte v5, v5, v10

    and-int/lit16 v5, v5, 0xff

    invoke-virtual {v3, v1, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b(II)Z

    move-result v5

    :cond_e
    iget v10, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    sub-int/2addr v10, v9

    iput v10, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    iget v10, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    add-int/2addr v10, v9

    iput v10, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    :cond_f
    if-nez v2, :cond_10

    if-eqz v5, :cond_0

    invoke-virtual {v3, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(Z)V

    iget-object v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    :cond_10
    if-nez v5, :cond_0

    if-nez v2, :cond_11

    return v1

    :cond_11
    move v5, v1

    :goto_2
    if-nez v2, :cond_14

    if-ge v5, v6, :cond_13

    if-nez v2, :cond_12

    if-nez v0, :cond_13

    return v1

    :cond_12
    move v5, v0

    goto :goto_3

    :cond_13
    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    :cond_14
    :goto_3
    if-nez v2, :cond_2

    if-nez v5, :cond_1

    const/4 v4, 0x4

    if-nez v2, :cond_16

    if-ne v0, v4, :cond_15

    move v5, v9

    goto :goto_4

    :cond_15
    move v5, v1

    goto :goto_4

    :cond_16
    move v5, v0

    :goto_4
    invoke-virtual {v3, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(Z)V

    iget-object v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    if-nez v2, :cond_19

    if-nez v3, :cond_1a

    if-nez v2, :cond_17

    if-ne v0, v4, :cond_18

    move v1, v7

    goto :goto_5

    :cond_17
    move v1, v0

    :cond_18
    :goto_5
    return v1

    :cond_19
    move v0, v3

    :cond_1a
    if-nez v2, :cond_1b

    if-ne v0, v4, :cond_1c

    goto :goto_6

    :cond_1b
    move v8, v0

    :goto_6
    move v9, v8

    :cond_1c
    return v9
.end method

.method d(II)I
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    const/4 p1, 0x6

    :cond_0
    if-nez v0, :cond_9

    if-ltz p1, :cond_8

    if-nez v0, :cond_9

    const/16 v1, 0x9

    if-gt p1, v1, :cond_8

    if-nez v0, :cond_a

    if-ltz p2, :cond_8

    const/4 v1, 0x2

    if-nez v0, :cond_2

    if-le p2, v1, :cond_1

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;->e:I

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    aget-object v2, v2, p1

    iget v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;->e:I

    goto :goto_0

    :cond_2
    move v2, v1

    move v1, p2

    :goto_0
    const/4 v3, 0x0

    if-nez v0, :cond_5

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    const-wide/16 v4, 0x0

    cmp-long v1, v1, v4

    if-nez v0, :cond_4

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->deflate(I)I

    move-result v1

    move v3, v1

    :cond_3
    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I

    :cond_4
    if-nez v0, :cond_7

    move v2, p1

    :cond_5
    if-eq v1, v2, :cond_6

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    aget-object p1, v0, p1

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;->b:I

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aJ:I

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I

    aget-object p1, p1, v0

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;->a:I

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aM:I

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I

    aget-object p1, p1, v0

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;->c:I

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aN:I

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I

    aget-object p1, p1, v0

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;->d:I

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aI:I

    :cond_6
    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aL:I

    move v1, v3

    :cond_7
    return v1

    :cond_8
    :goto_1
    const/4 p2, -0x2

    goto :goto_2

    :cond_9
    move p2, p1

    :cond_a
    :goto_2
    return p2
.end method

.method e(I)I
    .locals 17

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const/4 v1, 0x0

    move-object/from16 v3, p0

    move-object v2, v0

    move v4, v1

    move/from16 v0, p1

    :cond_0
    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    const v6, 0xffff

    const/16 v7, 0x106

    const/4 v8, 0x3

    const/4 v9, 0x2

    const/4 v10, 0x1

    if-ge v5, v7, :cond_5

    invoke-virtual {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->i()V

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    if-nez v2, :cond_4

    if-nez v2, :cond_3

    if-ge v5, v7, :cond_2

    if-nez v2, :cond_1

    if-nez v0, :cond_2

    return v1

    :cond_1
    move v5, v0

    goto :goto_0

    :cond_2
    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    goto :goto_0

    :cond_3
    move v11, v7

    goto :goto_1

    :cond_4
    :goto_0
    if-nez v2, :cond_6

    if-nez v5, :cond_5

    goto/16 :goto_8

    :cond_5
    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    :cond_6
    if-nez v2, :cond_8

    move v11, v8

    :goto_1
    if-lt v5, v11, :cond_7

    iget v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->az:I

    shl-int/2addr v4, v5

    iget-object v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    iget v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    add-int/lit8 v12, v11, 0x2

    aget-byte v5, v5, v12

    and-int/lit16 v5, v5, 0xff

    xor-int/2addr v4, v5

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ay:I

    and-int/2addr v4, v5

    iput v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    iget-object v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->au:[S

    aget-short v12, v5, v4

    and-int/2addr v12, v6

    iget-object v13, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->at:[S

    iget v14, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aq:I

    and-int/2addr v14, v11

    aget-short v15, v5, v4

    aput-short v15, v13, v14

    int-to-short v11, v11

    aput-short v11, v5, v4

    move v4, v12

    :cond_7
    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    iput v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aH:I

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aF:I

    iput v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aC:I

    iput v9, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    move v5, v4

    :cond_8
    if-nez v2, :cond_d

    if-eqz v5, :cond_c

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aH:I

    iget v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aJ:I

    if-nez v2, :cond_f

    if-ge v5, v11, :cond_c

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    sub-int/2addr v5, v4

    and-int/2addr v5, v6

    iget v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ao:I

    sub-int/2addr v11, v7

    if-nez v2, :cond_f

    if-gt v5, v11, :cond_c

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aL:I

    if-nez v2, :cond_a

    if-eq v5, v9, :cond_9

    invoke-virtual {v3, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->f(I)I

    move-result v5

    iput v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    :cond_9
    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    const/4 v7, 0x5

    move v11, v7

    goto :goto_2

    :cond_a
    move v11, v9

    :goto_2
    if-nez v2, :cond_f

    if-gt v5, v11, :cond_c

    if-nez v2, :cond_b

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aL:I

    if-eq v5, v10, :cond_b

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    if-nez v2, :cond_e

    if-ne v5, v8, :cond_c

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iget v7, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aF:I

    sub-int/2addr v5, v7

    const/16 v11, 0x1000

    if-nez v2, :cond_f

    if-le v5, v11, :cond_c

    :cond_b
    iput v9, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    :cond_c
    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aH:I

    :cond_d
    if-nez v2, :cond_17

    :cond_e
    move v11, v8

    :cond_f
    if-lt v5, v11, :cond_16

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    if-nez v2, :cond_17

    iget v7, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aH:I

    if-gt v5, v7, :cond_16

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iget v11, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    add-int/2addr v11, v5

    sub-int/2addr v11, v8

    sub-int/2addr v5, v10

    iget v12, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aC:I

    sub-int/2addr v5, v12

    add-int/lit8 v7, v7, -0x3

    invoke-virtual {v3, v5, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b(II)Z

    move-result v5

    iget v7, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    iget v12, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aH:I

    add-int/lit8 v13, v12, -0x1

    sub-int/2addr v7, v13

    iput v7, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    sub-int/2addr v12, v9

    iput v12, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aH:I

    :cond_10
    iget v7, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    add-int/2addr v7, v10

    iput v7, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    if-gt v7, v11, :cond_11

    iget v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    iget v12, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->az:I

    shl-int/2addr v4, v12

    iget-object v12, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    add-int/lit8 v7, v7, 0x2

    aget-byte v7, v12, v7

    and-int/lit16 v7, v7, 0xff

    xor-int/2addr v4, v7

    iget v7, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ay:I

    and-int/2addr v4, v7

    iput v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    iget-object v7, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->au:[S

    aget-short v4, v7, v4

    and-int/2addr v4, v6

    goto :goto_5

    :cond_11
    :goto_3
    iget v7, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aH:I

    sub-int/2addr v7, v10

    iput v7, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aH:I

    if-nez v7, :cond_10

    iput v1, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aD:I

    iput v9, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aB:I

    iget v7, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    add-int/2addr v7, v10

    iput v7, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    if-nez v2, :cond_15

    if-nez v2, :cond_12

    if-eqz v5, :cond_13

    invoke-virtual {v3, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(Z)V

    iget-object v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    :cond_12
    if-nez v2, :cond_14

    if-nez v5, :cond_13

    goto :goto_4

    :cond_13
    if-eqz v2, :cond_0

    goto :goto_6

    :cond_14
    move v1, v5

    :goto_4
    return v1

    :cond_15
    move v4, v5

    :goto_5
    iget-object v7, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->at:[S

    iget v12, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iget v13, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aq:I

    and-int/2addr v13, v12

    iget-object v14, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->au:[S

    iget v15, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    aget-short v16, v14, v15

    aput-short v16, v7, v13

    int-to-short v7, v12

    aput-short v7, v14, v15

    goto :goto_3

    :cond_16
    :goto_6
    if-nez v2, :cond_1c

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aD:I

    :cond_17
    if-eqz v5, :cond_1b

    iget-object v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    iget v6, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    sub-int/2addr v6, v10

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0xff

    invoke-virtual {v3, v1, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b(II)Z

    move-result v5

    if-nez v2, :cond_19

    if-eqz v5, :cond_18

    invoke-virtual {v3, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(Z)V

    :cond_18
    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    add-int/2addr v5, v10

    iput v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    sub-int/2addr v5, v10

    iput v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    iget-object v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    :cond_19
    if-nez v2, :cond_1a

    if-nez v5, :cond_0

    goto :goto_7

    :cond_1a
    move v1, v5

    :goto_7
    return v1

    :cond_1b
    iput v10, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aD:I

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    add-int/2addr v5, v10

    iput v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    :cond_1c
    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    sub-int/2addr v5, v10

    iput v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    if-eqz v2, :cond_0

    :goto_8
    if-nez v2, :cond_1d

    iget v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aD:I

    if-eqz v4, :cond_1d

    iget-object v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    sub-int/2addr v5, v10

    aget-byte v4, v4, v5

    and-int/lit16 v4, v4, 0xff

    invoke-virtual {v3, v1, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b(II)Z

    iput v1, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aD:I

    :cond_1d
    const/4 v4, 0x4

    if-nez v2, :cond_1f

    if-ne v0, v4, :cond_1e

    move v5, v10

    goto :goto_9

    :cond_1e
    move v5, v1

    goto :goto_9

    :cond_1f
    move v5, v0

    :goto_9
    invoke-virtual {v3, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(Z)V

    iget-object v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    if-nez v2, :cond_22

    if-nez v3, :cond_23

    if-nez v2, :cond_20

    if-ne v0, v4, :cond_21

    move v1, v9

    goto :goto_a

    :cond_20
    move v1, v0

    :cond_21
    :goto_a
    return v1

    :cond_22
    move v0, v3

    :cond_23
    if-nez v2, :cond_24

    if-ne v0, v4, :cond_25

    goto :goto_b

    :cond_24
    move v8, v0

    :goto_b
    move v10, v8

    :cond_25
    return v10
.end method

.method e()V
    .locals 5

    const/4 v0, 0x2

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(II)V

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v2

    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;->h:[S

    const/16 v4, 0x100

    invoke-virtual {p0, v4, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I[S)V

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->g()V

    if-nez v2, :cond_1

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a7:I

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v2, v2, 0xa

    iget v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a9:I

    sub-int/2addr v2, v3

    const/16 v3, 0x9

    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(II)V

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;->h:[S

    invoke-virtual {p0, v4, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I[S)V

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->g()V

    :cond_0
    const/4 v0, 0x7

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a7:I

    :cond_1
    return-void
.end method

.method f(I)I
    .locals 18

    move-object/from16 v0, p0

    iget v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aI:I

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v2

    iget v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iget v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aH:I

    iget v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ao:I

    add-int/lit16 v6, v5, -0x106

    if-nez v2, :cond_1

    if-le v3, v6, :cond_0

    add-int/lit16 v6, v5, -0x106

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    sub-int v5, v3, v6

    :goto_1
    iget v6, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aN:I

    iget v7, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aq:I

    iget v8, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    const/16 v9, 0x102

    add-int/2addr v8, v9

    iget-object v10, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    add-int v11, v3, v4

    add-int/lit8 v12, v11, -0x1

    aget-byte v12, v10, v12

    aget-byte v10, v10, v11

    iget v11, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aH:I

    iget v13, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aM:I

    if-nez v2, :cond_4

    if-lt v11, v13, :cond_2

    shr-int/lit8 v1, v1, 0x2

    :cond_2
    if-nez v2, :cond_3

    iget v13, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    move v11, v6

    goto :goto_2

    :cond_3
    move-object v14, v0

    move v11, v8

    move v13, v12

    move v8, v6

    move v12, v10

    move v10, v7

    move v7, v8

    move v6, v5

    move v5, v4

    move v4, v3

    move-object v3, v2

    move v2, v1

    move/from16 v1, p1

    goto :goto_4

    :cond_4
    :goto_2
    if-le v11, v13, :cond_5

    iget v6, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    :cond_5
    move-object v13, v0

    move v11, v10

    move v10, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v3

    move-object v3, v2

    move v2, v1

    move/from16 v1, p1

    :goto_3
    move-object v14, v13

    move v13, v12

    move v12, v11

    move v11, v10

    move v10, v8

    move v8, v7

    move v7, v1

    :goto_4
    iget-object v15, v14, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    add-int v16, v7, v5

    aget-byte v9, v15, v16

    if-ne v9, v12, :cond_6

    add-int/lit8 v16, v16, -0x1

    aget-byte v9, v15, v16

    move-object v15, v14

    move v14, v13

    move v13, v12

    move v12, v11

    move v11, v10

    move v10, v8

    move v8, v6

    move v6, v5

    move v5, v4

    move-object v4, v3

    move v3, v2

    move v2, v1

    move v1, v9

    move v9, v7

    move v7, v14

    goto/16 :goto_8

    :cond_6
    :goto_5
    iget-object v9, v14, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->at:[S

    and-int/2addr v1, v10

    aget-short v1, v9, v1

    const v9, 0xffff

    and-int/2addr v1, v9

    move v9, v7

    move-object v15, v14

    move v7, v6

    move v14, v13

    move v13, v12

    move v6, v5

    move v12, v11

    move v5, v4

    move v11, v10

    move-object v4, v3

    move v10, v8

    move v3, v2

    move v8, v7

    move v2, v1

    :goto_6
    if-nez v4, :cond_1a

    if-le v1, v7, :cond_8

    add-int/lit8 v1, v3, -0x1

    move-object v3, v4

    move v4, v5

    move v5, v6

    move v6, v8

    if-nez v1, :cond_7

    move v7, v9

    move v8, v10

    move v10, v11

    move v9, v12

    move v12, v13

    move v13, v14

    move-object v14, v15

    move/from16 v17, v2

    move v2, v1

    move/from16 v1, v17

    goto :goto_7

    :cond_7
    move v7, v10

    move v8, v11

    move v10, v12

    move v11, v13

    move v12, v14

    move-object v13, v15

    const/16 v9, 0x102

    move/from16 v17, v2

    move v2, v1

    move/from16 v1, v17

    goto :goto_3

    :cond_8
    move v1, v2

    move v2, v3

    move-object v3, v4

    move v4, v5

    move v5, v6

    move v6, v8

    move v7, v9

    move v8, v10

    move v10, v11

    move v9, v12

    move v12, v13

    move v13, v14

    move-object v14, v15

    :goto_7
    if-nez v3, :cond_1c

    iget v11, v14, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    if-nez v3, :cond_9

    move v1, v5

    move v6, v1

    move v7, v11

    move-object v15, v14

    goto/16 :goto_13

    :cond_9
    move-object v15, v14

    move v14, v13

    move v13, v12

    move v12, v9

    move v9, v7

    move v7, v11

    move v11, v10

    move v10, v8

    move v8, v6

    move v6, v5

    move v5, v4

    move-object v4, v3

    move v3, v2

    move v2, v1

    move v1, v6

    :goto_8
    if-nez v4, :cond_19

    if-ne v1, v7, :cond_18

    iget-object v1, v15, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    aget-byte v7, v1, v9

    aget-byte v0, v1, v5

    if-nez v4, :cond_17

    if-ne v7, v0, :cond_18

    add-int/lit8 v7, v9, 0x1

    aget-byte v0, v1, v7

    add-int/lit8 v9, v5, 0x1

    aget-byte v1, v1, v9

    if-eq v0, v1, :cond_a

    move-object/from16 v0, p0

    move v1, v2

    move v2, v3

    move-object v3, v4

    move v4, v5

    move v5, v6

    move v6, v8

    goto/16 :goto_11

    :cond_a
    add-int/lit8 v5, v5, 0x2

    add-int/lit8 v7, v7, 0x1

    :goto_9
    iget-object v0, v15, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    add-int/lit8 v5, v5, 0x1

    aget-byte v1, v0, v5

    add-int/lit8 v7, v7, 0x1

    aget-byte v9, v0, v7

    if-ne v1, v9, :cond_b

    add-int/lit8 v5, v5, 0x1

    aget-byte v1, v0, v5

    add-int/lit8 v7, v7, 0x1

    aget-byte v0, v0, v7

    move v9, v12

    move v12, v13

    move v13, v14

    move-object v14, v15

    move/from16 v17, v5

    move v5, v1

    move v1, v2

    move v2, v3

    move-object v3, v4

    move v4, v7

    move v7, v6

    move/from16 v6, v17

    goto/16 :goto_f

    :cond_b
    :goto_a
    sub-int v0, v12, v5

    move v1, v2

    move v2, v3

    move-object v3, v4

    move v4, v7

    move v9, v12

    move v12, v13

    move v13, v14

    move-object v14, v15

    const/16 v5, 0x102

    move v7, v6

    :cond_c
    :goto_b
    move v6, v8

    move v8, v10

    move v10, v11

    :goto_c
    sub-int/2addr v5, v0

    add-int/lit16 v0, v9, -0x102

    if-nez v3, :cond_11

    if-nez v3, :cond_10

    if-le v5, v7, :cond_f

    iput v1, v14, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aF:I

    if-nez v3, :cond_e

    if-lt v5, v8, :cond_d

    move v7, v4

    move v4, v0

    move-object/from16 v0, p0

    goto/16 :goto_7

    :cond_d
    iget-object v7, v14, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    add-int v11, v0, v5

    add-int/lit8 v12, v11, -0x1

    aget-byte v12, v7, v12

    aget-byte v7, v7, v11

    move v13, v12

    move v12, v7

    goto :goto_d

    :cond_e
    move v12, v5

    :goto_d
    move v7, v4

    move v11, v9

    goto :goto_e

    :cond_f
    move v5, v7

    move v11, v9

    move v7, v4

    :goto_e
    move v4, v0

    move-object/from16 v0, p0

    goto/16 :goto_5

    :cond_10
    move v11, v10

    move-object v15, v14

    move v10, v8

    move v14, v13

    move v8, v6

    move v6, v7

    move v13, v12

    move v12, v9

    move v9, v4

    move-object v4, v3

    move v3, v2

    move v2, v1

    move v1, v5

    move v5, v0

    goto/16 :goto_12

    :cond_11
    move v11, v10

    move v10, v8

    move v8, v6

    move v6, v0

    move v0, v7

    :goto_f
    if-nez v3, :cond_c

    if-ne v5, v0, :cond_15

    iget-object v0, v14, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    add-int/lit8 v5, v6, 0x1

    aget-byte v6, v0, v5

    add-int/lit8 v4, v4, 0x1

    aget-byte v15, v0, v4

    if-nez v3, :cond_14

    if-ne v6, v15, :cond_16

    add-int/lit8 v5, v5, 0x1

    aget-byte v6, v0, v5

    add-int/lit8 v4, v4, 0x1

    aget-byte v15, v0, v4

    if-nez v3, :cond_14

    if-ne v6, v15, :cond_16

    add-int/lit8 v5, v5, 0x1

    aget-byte v6, v0, v5

    add-int/lit8 v4, v4, 0x1

    aget-byte v15, v0, v4

    if-nez v3, :cond_14

    if-ne v6, v15, :cond_16

    add-int/lit8 v5, v5, 0x1

    aget-byte v6, v0, v5

    add-int/lit8 v4, v4, 0x1

    aget-byte v15, v0, v4

    if-nez v3, :cond_14

    if-ne v6, v15, :cond_16

    add-int/lit8 v5, v5, 0x1

    aget-byte v6, v0, v5

    add-int/lit8 v4, v4, 0x1

    aget-byte v15, v0, v4

    if-nez v3, :cond_14

    if-ne v6, v15, :cond_16

    add-int/lit8 v5, v5, 0x1

    aget-byte v6, v0, v5

    add-int/lit8 v4, v4, 0x1

    aget-byte v0, v0, v4

    if-nez v3, :cond_13

    if-ne v6, v0, :cond_16

    if-lt v5, v9, :cond_12

    goto :goto_10

    :cond_12
    move v6, v7

    move-object v15, v14

    move v7, v4

    move v14, v13

    move-object v4, v3

    move v13, v12

    move v3, v2

    move v12, v9

    move v2, v1

    goto/16 :goto_9

    :cond_13
    move v5, v6

    goto/16 :goto_b

    :cond_14
    move v5, v6

    move v6, v8

    move v8, v10

    move v10, v11

    move v0, v15

    goto/16 :goto_c

    :cond_15
    move v5, v6

    :cond_16
    :goto_10
    move v6, v7

    move-object v15, v14

    move v7, v4

    move v14, v13

    move-object v4, v3

    move v13, v12

    move v3, v2

    move v12, v9

    move v2, v1

    goto/16 :goto_a

    :cond_17
    move v1, v7

    move v7, v0

    goto :goto_12

    :cond_18
    move-object/from16 v0, p0

    move v1, v2

    move v2, v3

    move-object v3, v4

    move v4, v5

    move v5, v6

    move v6, v8

    move v7, v9

    :goto_11
    move v8, v10

    move v10, v11

    move v11, v12

    move v12, v13

    move v13, v14

    move-object v14, v15

    goto/16 :goto_5

    :cond_19
    :goto_12
    move-object/from16 v0, p0

    goto/16 :goto_6

    :cond_1a
    :goto_13
    if-gt v1, v7, :cond_1b

    return v6

    :cond_1b
    iget v5, v15, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    :cond_1c
    return v5
.end method

.method f()V
    .locals 7

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const/4 v1, 0x0

    move v2, v1

    move v3, v2

    :cond_0
    const/4 v4, 0x7

    if-ge v2, v4, :cond_2

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aO:[S

    mul-int/lit8 v5, v2, 0x2

    aget-short v4, v4, v5

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_1
    move v4, v1

    goto :goto_1

    :cond_2
    :goto_0
    move v4, v1

    :cond_3
    const/16 v5, 0x80

    if-ge v2, v5, :cond_4

    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aO:[S

    mul-int/lit8 v6, v2, 0x2

    aget-short v5, v5, v6

    add-int/2addr v4, v5

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_5

    if-eqz v0, :cond_3

    :cond_4
    :goto_1
    const/16 v5, 0x100

    if-ge v2, v5, :cond_5

    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aO:[S

    mul-int/lit8 v6, v2, 0x2

    aget-short v5, v5, v6

    add-int/2addr v3, v5

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_8

    if-eqz v0, :cond_4

    :cond_5
    if-nez v0, :cond_7

    ushr-int/lit8 v0, v4, 0x2

    if-le v3, v0, :cond_6

    goto :goto_2

    :cond_6
    const/4 v0, 0x1

    goto :goto_3

    :cond_7
    move v1, v3

    :goto_2
    move v0, v1

    :goto_3
    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->al:B

    :cond_8
    return-void
.end method

.method g(I)I
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->c(II)I

    move-result p1

    return p1
.end method

.method g()V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a9:I

    const/16 v2, 0x8

    const/16 v3, 0x10

    if-nez v0, :cond_1

    if-ne v1, v3, :cond_0

    iget-short v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a8:S

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I)V

    const/4 v1, 0x0

    iput-short v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a8:S

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a9:I

    if-eqz v0, :cond_3

    :cond_0
    if-nez v0, :cond_2

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a9:I

    move v3, v2

    :cond_1
    if-lt v1, v3, :cond_3

    iget-short v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a8:S

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    iget-short v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a8:S

    ushr-int/2addr v0, v2

    int-to-short v0, v0

    iput-short v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a8:S

    :cond_2
    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a9:I

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a9:I

    :cond_3
    return-void
.end method

.method h(I)I
    .locals 19

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v2

    const/4 v3, -0x2

    if-nez v2, :cond_41

    const/4 v4, 0x4

    if-gt v1, v4, :cond_40

    if-nez v2, :cond_41

    if-gez v1, :cond_0

    goto/16 :goto_13

    :cond_0
    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    if-nez v2, :cond_3f

    iget-object v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B

    if-eqz v5, :cond_3e

    if-nez v2, :cond_2

    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    if-nez v5, :cond_2

    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    if-nez v2, :cond_1

    if-nez v5, :cond_3e

    goto :goto_0

    :cond_1
    move v3, v5

    goto/16 :goto_12

    :cond_2
    :goto_0
    iget v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->af:I

    const/16 v6, 0x29a

    if-nez v2, :cond_5

    if-ne v5, v6, :cond_4

    if-nez v2, :cond_3

    if-eq v1, v4, :cond_4

    goto/16 :goto_11

    :cond_3
    move v5, v1

    goto :goto_1

    :cond_4
    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    :cond_5
    :goto_1
    const/4 v3, -0x5

    const/4 v7, 0x7

    if-nez v2, :cond_7

    if-nez v5, :cond_6

    iget-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->i:[Ljava/lang/String;

    aget-object v2, v2, v7

    :goto_2
    iput-object v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    return v3

    :cond_6
    iget v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->an:I

    :cond_7
    iput v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->an:I

    iget v8, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->af:I

    const-wide/32 v9, 0xffff

    const/16 v11, 0x10

    const/4 v12, 0x3

    const/16 v13, 0x8

    const/4 v14, 0x2

    const/4 v15, 0x1

    if-nez v2, :cond_10

    const/16 v6, 0x2a

    if-ne v8, v6, :cond_f

    iget v6, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ak:I

    const/16 v8, 0x71

    if-nez v2, :cond_9

    if-ne v6, v14, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->l()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;)V

    iput v8, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->af:I

    iget-object v6, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->reset()V

    if-eqz v2, :cond_f

    :cond_8
    iget v6, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ap:I

    sub-int/2addr v6, v13

    shl-int/2addr v6, v4

    add-int/2addr v6, v13

    move/from16 v16, v13

    goto :goto_3

    :cond_9
    move/from16 v16, v14

    :goto_3
    shl-int v6, v6, v16

    iget v13, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I

    sub-int/2addr v13, v15

    and-int/lit16 v13, v13, 0xff

    shr-int/2addr v13, v15

    if-nez v2, :cond_b

    if-le v13, v12, :cond_a

    move v13, v12

    :cond_a
    shl-int/lit8 v13, v13, 0x6

    goto :goto_4

    :cond_b
    move v6, v13

    move v13, v12

    :goto_4
    or-int/2addr v6, v13

    iget v13, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    if-nez v2, :cond_d

    if-eqz v13, :cond_c

    or-int/lit8 v6, v6, 0x20

    :cond_c
    rem-int/lit8 v13, v6, 0x1f

    rsub-int/lit8 v13, v13, 0x1f

    add-int/2addr v6, v13

    iput v8, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->af:I

    invoke-virtual {v0, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b(I)V

    if-nez v2, :cond_e

    iget v13, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    :cond_d
    if-eqz v13, :cond_e

    iget-object v6, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->getValue()J

    move-result-wide v17

    ushr-long v12, v17, v11

    long-to-int v8, v12

    invoke-virtual {v0, v8}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b(I)V

    and-long v12, v17, v9

    long-to-int v8, v12

    invoke-virtual {v0, v8}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b(I)V

    :cond_e
    iget-object v8, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v8, v8, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v8}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->reset()V

    :cond_f
    iget v8, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aj:I

    :cond_10
    const/4 v12, -0x1

    const/4 v13, 0x0

    if-nez v2, :cond_12

    if-eqz v8, :cond_11

    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->a()V

    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    if-nez v2, :cond_17

    if-nez v5, :cond_15

    iput v12, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->an:I

    return v13

    :cond_11
    iget-object v8, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v8, v8, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    :cond_12
    if-nez v2, :cond_16

    if-nez v8, :cond_15

    if-nez v2, :cond_14

    if-gt v1, v5, :cond_15

    if-nez v2, :cond_13

    if-eq v1, v4, :cond_15

    iget-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->i:[Ljava/lang/String;

    aget-object v2, v2, v7

    goto/16 :goto_2

    :cond_13
    move v5, v1

    move v8, v4

    goto :goto_6

    :cond_14
    move v8, v5

    move v5, v1

    goto :goto_6

    :cond_15
    iget v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->af:I

    goto :goto_5

    :cond_16
    move v5, v8

    :cond_17
    :goto_5
    if-nez v2, :cond_19

    const/16 v8, 0x29a

    :goto_6
    if-ne v5, v8, :cond_18

    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    if-nez v2, :cond_19

    if-eqz v5, :cond_18

    iget-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->i:[Ljava/lang/String;

    aget-object v2, v2, v7

    goto/16 :goto_2

    :cond_18
    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    :cond_19
    if-nez v2, :cond_1c

    if-nez v5, :cond_1b

    iget v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    if-nez v2, :cond_1c

    if-nez v5, :cond_1b

    if-nez v2, :cond_2e

    if-eqz v1, :cond_2e

    iget v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->af:I

    if-nez v2, :cond_1a

    const/16 v5, 0x29a

    if-eq v3, v5, :cond_2e

    goto :goto_7

    :cond_1a
    const/16 v4, 0x29a

    goto/16 :goto_d

    :cond_1b
    :goto_7
    move v5, v12

    :cond_1c
    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;

    iget v7, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I

    aget-object v3, v3, v7

    iget v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate$Config;->e:I

    if-nez v2, :cond_21

    if-eqz v3, :cond_1d

    if-eq v3, v15, :cond_1e

    if-eq v3, v14, :cond_1f

    goto :goto_8

    :cond_1d
    invoke-virtual/range {p0 .. p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->c(I)I

    move-result v5

    if-eqz v2, :cond_20

    :cond_1e
    invoke-virtual/range {p0 .. p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->d(I)I

    move-result v5

    if-eqz v2, :cond_20

    :cond_1f
    invoke-virtual/range {p0 .. p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->e(I)I

    move-result v5

    :cond_20
    :goto_8
    move v3, v5

    :cond_21
    if-nez v2, :cond_22

    if-eq v3, v14, :cond_23

    if-nez v2, :cond_24

    move v3, v5

    const/4 v7, 0x3

    goto :goto_9

    :cond_22
    move v7, v14

    :goto_9
    if-ne v3, v7, :cond_24

    :cond_23
    const/16 v3, 0x29a

    iput v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->af:I

    :cond_24
    if-nez v2, :cond_3b

    if-eqz v5, :cond_3a

    if-nez v2, :cond_26

    if-ne v5, v14, :cond_25

    goto/16 :goto_f

    :cond_25
    move v3, v15

    goto :goto_a

    :cond_26
    move v3, v14

    :goto_a
    if-nez v2, :cond_2f

    if-ne v5, v3, :cond_2e

    if-nez v2, :cond_29

    if-ne v1, v15, :cond_27

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->e()V

    if-eqz v2, :cond_2b

    :cond_27
    invoke-virtual {v0, v13, v13, v13}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b(IIZ)V

    if-nez v2, :cond_28

    const/4 v6, 0x3

    goto :goto_b

    :cond_28
    move v3, v1

    goto :goto_c

    :cond_29
    move v6, v15

    :goto_b
    if-ne v1, v6, :cond_2b

    move v3, v13

    :cond_2a
    iget v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aw:I

    if-ge v3, v5, :cond_2b

    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->au:[S

    aput-short v13, v5, v3

    add-int/lit8 v3, v3, 0x1

    if-nez v2, :cond_2c

    if-eqz v2, :cond_2a

    :cond_2b
    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    invoke-virtual {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->a()V

    :cond_2c
    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    :goto_c
    if-nez v2, :cond_2d

    if-nez v3, :cond_2e

    iput v12, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->an:I

    return v13

    :cond_2d
    move v1, v3

    :cond_2e
    if-nez v2, :cond_31

    move v3, v1

    goto :goto_d

    :cond_2f
    move v4, v3

    move v3, v5

    :goto_d
    if-eq v3, v4, :cond_30

    return v13

    :cond_30
    iget v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ak:I

    :cond_31
    if-nez v2, :cond_33

    if-gtz v1, :cond_32

    return v15

    :cond_32
    if-nez v2, :cond_34

    iget v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ak:I

    :cond_33
    if-ne v1, v14, :cond_34

    iget-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->getValue()J

    move-result-wide v3

    const-wide/16 v5, 0xff

    and-long v7, v3, v5

    long-to-int v1, v7

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    const/16 v1, 0x8

    shr-long v7, v3, v1

    and-long/2addr v7, v5

    long-to-int v1, v7

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    shr-long v7, v3, v11

    and-long/2addr v7, v5

    long-to-int v1, v7

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    const/16 v1, 0x18

    shr-long v7, v3, v1

    and-long/2addr v7, v5

    long-to-int v7, v7

    int-to-byte v7, v7

    invoke-virtual {v0, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    iget-object v7, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    and-long/2addr v7, v5

    long-to-int v7, v7

    int-to-byte v7, v7

    invoke-virtual {v0, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    iget-object v7, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    const/16 v12, 0x8

    shr-long/2addr v7, v12

    and-long/2addr v7, v5

    long-to-int v7, v7

    int-to-byte v7, v7

    invoke-virtual {v0, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    iget-object v7, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    shr-long/2addr v7, v11

    and-long/2addr v7, v5

    long-to-int v7, v7

    int-to-byte v7, v7

    invoke-virtual {v0, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    iget-object v7, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    shr-long/2addr v7, v1

    and-long/2addr v5, v7

    long-to-int v1, v5

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->l()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    move-result-object v1

    invoke-virtual {v1, v3, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->setCRC(J)V

    if-eqz v2, :cond_35

    :cond_34
    iget-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->getValue()J

    move-result-wide v3

    ushr-long v5, v3, v11

    long-to-int v1, v5

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b(I)V

    and-long/2addr v3, v9

    long-to-int v1, v3

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b(I)V

    :cond_35
    iget-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->a()V

    iget v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ak:I

    if-nez v2, :cond_37

    if-lez v1, :cond_36

    neg-int v1, v1

    iput v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ak:I

    :cond_36
    iget v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aj:I

    :cond_37
    if-nez v2, :cond_38

    if-eqz v1, :cond_39

    move v15, v13

    goto :goto_e

    :cond_38
    move v15, v1

    :cond_39
    :goto_e
    return v15

    :cond_3a
    :goto_f
    iget-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    :cond_3b
    if-nez v2, :cond_3c

    if-nez v5, :cond_3d

    iput v12, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->an:I

    goto :goto_10

    :cond_3c
    move v13, v5

    :cond_3d
    :goto_10
    return v13

    :cond_3e
    :goto_11
    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    :cond_3f
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->i:[Ljava/lang/String;

    aget-object v1, v1, v4

    iput-object v1, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    :goto_12
    return v3

    :cond_40
    :goto_13
    move v1, v3

    :cond_41
    return v1
.end method

.method h()V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a9:I

    const/4 v2, 0x0

    if-nez v0, :cond_1

    const/16 v3, 0x8

    if-le v1, v3, :cond_0

    iget-short v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a8:S

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I)V

    if-eqz v0, :cond_2

    :cond_0
    if-nez v0, :cond_3

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a9:I

    :cond_1
    if-lez v1, :cond_2

    iget-short v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a8:S

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    :cond_2
    iput-short v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a8:S

    :cond_3
    iput v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a9:I

    return-void
.end method

.method i()V
    .locals 12

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    move-object v1, p0

    :cond_0
    iget v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->as:I

    iget v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    sub-int/2addr v2, v3

    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    sub-int/2addr v2, v4

    const/16 v5, 0x106

    if-nez v2, :cond_2

    if-nez v0, :cond_1

    if-nez v4, :cond_2

    if-nez v0, :cond_3

    if-nez v3, :cond_2

    iget v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ao:I

    if-eqz v0, :cond_c

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1

    :cond_2
    :goto_0
    move v3, v2

    move v2, v3

    :cond_3
    :goto_1
    const/4 v4, -0x1

    if-nez v0, :cond_5

    if-ne v3, v4, :cond_4

    add-int/lit8 v2, v2, -0x1

    if-eqz v0, :cond_c

    :cond_4
    iget v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    if-nez v0, :cond_d

    iget v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ao:I

    add-int/2addr v6, v6

    sub-int/2addr v6, v5

    goto :goto_2

    :cond_5
    move v6, v4

    :goto_2
    if-lt v3, v6, :cond_c

    iget-object v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    iget v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ao:I

    const/4 v7, 0x0

    invoke-static {v3, v6, v3, v7, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aF:I

    iget v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ao:I

    sub-int/2addr v3, v6

    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aF:I

    iget v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    sub-int/2addr v3, v6

    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iget v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aA:I

    sub-int/2addr v3, v6

    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aA:I

    iget v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aw:I

    move v6, v3

    :cond_6
    iget-object v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->au:[S

    add-int/2addr v3, v4

    aget-short v9, v8, v3

    const v10, 0xffff

    and-int/2addr v9, v10

    iget v11, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ao:I

    if-lt v9, v11, :cond_7

    sub-int/2addr v9, v11

    int-to-short v9, v9

    goto :goto_3

    :cond_7
    move v9, v7

    :goto_3
    aput-short v9, v8, v3

    add-int/2addr v6, v4

    :cond_8
    if-nez v6, :cond_6

    iget v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ao:I

    if-nez v0, :cond_8

    move v3, v6

    :cond_9
    iget-object v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->at:[S

    add-int/2addr v6, v4

    aget-short v9, v8, v6

    and-int/2addr v9, v10

    iget v11, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ao:I

    if-lt v9, v11, :cond_a

    sub-int/2addr v9, v11

    int-to-short v9, v9

    goto :goto_4

    :cond_a
    move v9, v7

    :goto_4
    aput-short v9, v8, v6

    add-int/2addr v3, v4

    move v8, v3

    :cond_b
    if-nez v8, :cond_9

    iget v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ao:I

    add-int/2addr v8, v2

    if-nez v0, :cond_b

    move v2, v8

    :cond_c
    iget-object v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    :cond_d
    if-nez v0, :cond_f

    if-nez v3, :cond_e

    return-void

    :cond_e
    iget-object v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    iget v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    iget v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    add-int/2addr v6, v7

    invoke-virtual {v3, v4, v6, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->a([BII)I

    move-result v3

    :cond_f
    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    add-int/2addr v4, v3

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    const/4 v3, 0x3

    if-nez v0, :cond_11

    if-lt v4, v3, :cond_10

    iget-object v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aE:I

    aget-byte v6, v3, v4

    and-int/lit16 v6, v6, 0xff

    iput v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    iget v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->az:I

    shl-int/2addr v6, v7

    add-int/lit8 v4, v4, 0x1

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    xor-int/2addr v3, v6

    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ay:I

    and-int/2addr v3, v4

    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->av:I

    :cond_10
    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aG:I

    if-nez v0, :cond_12

    move v3, v5

    :cond_11
    if-ge v4, v3, :cond_13

    iget-object v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    :cond_12
    if-nez v4, :cond_0

    :cond_13
    if-nez v0, :cond_2

    return-void
.end method

.method j()I
    .locals 3

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalOut:J

    iput-wide v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    const/4 v2, 0x2

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->t:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aj:I

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ai:I

    if-nez v0, :cond_0

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ak:I

    if-gez v2, :cond_0

    neg-int v2, v2

    iput v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ak:I

    :cond_0
    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ak:I

    if-nez v0, :cond_2

    if-nez v2, :cond_1

    const/16 v2, 0x71

    goto :goto_0

    :cond_1
    const/16 v0, 0x2a

    goto :goto_1

    :cond_2
    :goto_0
    move v0, v2

    :goto_1
    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->af:I

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ae:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->reset()V

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->an:I

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b()V

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a()V

    return v1
.end method

.method k()I
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->af:I

    const/16 v2, 0x71

    const/16 v3, 0x2a

    if-nez v0, :cond_1

    if-eq v1, v3, :cond_0

    if-nez v0, :cond_2

    if-eq v1, v2, :cond_0

    const/16 v3, 0x29a

    if-nez v0, :cond_1

    if-eq v1, v3, :cond_0

    const/4 v0, -0x2

    return v0

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ag:[B

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->au:[S

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->at:[S

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ar:[B

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->af:I

    if-nez v0, :cond_4

    goto :goto_0

    :cond_1
    move v2, v3

    :cond_2
    :goto_0
    if-ne v1, v2, :cond_3

    const/4 v1, -0x3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :cond_4
    :goto_1
    return v1
.end method

.method declared-synchronized l()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a_:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a_:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a_:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
