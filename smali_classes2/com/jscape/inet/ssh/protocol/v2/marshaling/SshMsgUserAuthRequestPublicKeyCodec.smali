.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPublicKeyCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$MethodCodec;


# instance fields
.field private final a:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "Ljava/security/PublicKey;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/jscape/util/h/I;Lcom/jscape/util/h/I;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/h/I<",
            "Ljava/security/PublicKey;",
            ">;",
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPublicKeyCodec;->a:Lcom/jscape/util/h/I;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPublicKeyCodec;->b:Lcom/jscape/util/h/I;

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public read(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequest;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/a;->a(Ljava/io/InputStream;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readValue(Ljava/io/InputStream;)[B

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPublicKeyCodec;->a:Lcom/jscape/util/h/I;

    new-instance v2, Lcom/jscape/util/h/e;

    invoke-direct {v2, v0}, Lcom/jscape/util/h/e;-><init>([B)V

    invoke-interface {v1, v2}, Lcom/jscape/util/h/I;->read(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/security/PublicKey;

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPublicKeyCodec;->b:Lcom/jscape/util/h/I;

    invoke-interface {v0, p1}, Lcom/jscape/util/h/I;->read(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object p1

    move-object v6, p1

    check-cast v6, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v1 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V

    return-object p1

    :cond_0
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readValue(Ljava/io/InputStream;)[B

    move-result-object p1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPublicKeyCodec;->a:Lcom/jscape/util/h/I;

    new-instance v2, Lcom/jscape/util/h/e;

    invoke-direct {v2, p1}, Lcom/jscape/util/h/e;-><init>([B)V

    invoke-interface {v1, v2}, Lcom/jscape/util/h/I;->read(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/security/PublicKey;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKey;

    invoke-direct {v1, p2, p3, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKey;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/PublicKey;)V

    return-object v1
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequest;Ljava/io/OutputStream;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    :try_start_0
    instance-of v2, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;

    const/4 v3, 0x1

    invoke-static {v3, p2}, Lcom/jscape/util/h/a/a;->a(ZLjava/io/OutputStream;)V

    iget-object v3, v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;->publicKeyAlgorithm:Ljava/lang/String;

    invoke-static {v3, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUsAsciiValue(Ljava/lang/String;Ljava/io/OutputStream;)V

    new-instance v3, Lcom/jscape/util/h/o;

    invoke-direct {v3}, Lcom/jscape/util/h/o;-><init>()V

    :try_start_1
    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPublicKeyCodec;->a:Lcom/jscape/util/h/I;

    iget-object v5, v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;->publicKey:Ljava/security/PublicKey;

    invoke-interface {v4, v5, v3}, Lcom/jscape/util/h/I;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V

    invoke-virtual {v3}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v4

    invoke-virtual {v3}, Lcom/jscape/util/h/o;->b()I

    move-result v3

    invoke-static {v4, v1, v3, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BIILjava/io/OutputStream;)V

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPublicKeyCodec;->b:Lcom/jscape/util/h/I;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKeySignature;->signature:Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    invoke-interface {v3, v2, p2}, Lcom/jscape/util/h/I;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPublicKeyCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPublicKeyCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKey;

    invoke-static {v1, p2}, Lcom/jscape/util/h/a/a;->a(ZLjava/io/OutputStream;)V

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKey;->publicKeyAlgorithm:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUsAsciiValue(Ljava/lang/String;Ljava/io/OutputStream;)V

    new-instance v0, Lcom/jscape/util/h/o;

    invoke-direct {v0}, Lcom/jscape/util/h/o;-><init>()V

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPublicKeyCodec;->a:Lcom/jscape/util/h/I;

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPublicKey;->publicKey:Ljava/security/PublicKey;

    invoke-interface {v2, p1, v0}, Lcom/jscape/util/h/I;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object p1

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result v0

    invoke-static {p1, v1, v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BIILjava/io/OutputStream;)V

    :cond_1
    return-void
.end method
