.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;
.super Ljava/lang/Object;


# static fields
.field public static final NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

.field private static final a:[Ljava/lang/String;


# instance fields
.field public final algorithm:Ljava/lang/String;

.field public final digestLength:I

.field public final keyLength:I

.field public final name:Ljava/lang/String;

.field public final provider:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x4

    const/4 v3, 0x0

    const-string v4, "\u0016U\u001c\u0013\u0012:[\u0016V\u001e\u0011\u0010\u001dI\u0006V\u0016\u001d\u0019\u001fN\u001aX\r=T\u0006\u0004\u0003X\u000c\u0016[\u001f\u0013G_\u000f:[\u0016V\u0011\u001d\u000eXV\u0017\u0018\u001d\u000c\u001fV\u000cT\u001a\u0002\u0004\u0015\u000e\u001e\u001c_\u0000K]\u000fT\u001a\u0016\u001f\u001d\u001d\u0004\u000cv\u0017\u0018\u001d\u000c\u001fE"

    const/16 v5, 0x52

    move v7, v1

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x2f

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x1a

    const/16 v4, 0xc

    const-string v6, "C\r\u000e\u0004\u0014#\u0005\u0001J\u0011\tP\rC\r\u0004\r\n\u0000\u0012\u0006Y\r\u000cPH"

    move v7, v4

    move-object v4, v6

    move v8, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x38

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->a:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry$1;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->a:[Ljava/lang/String;

    aget-object v13, v1, v3

    const/4 v15, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x1

    const-string v14, ""

    move-object v12, v0

    invoke-direct/range {v12 .. v17}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry$1;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    const/16 v17, 0x57

    if-eqz v2, :cond_8

    if-eq v2, v10, :cond_7

    const/4 v3, 0x2

    if-eq v2, v3, :cond_6

    const/4 v3, 0x3

    if-eq v2, v3, :cond_5

    if-eq v2, v1, :cond_4

    const/4 v3, 0x5

    if-eq v2, v3, :cond_8

    const/16 v17, 0x58

    goto :goto_4

    :cond_4
    const/16 v17, 0x55

    goto :goto_4

    :cond_5
    const/16 v17, 0x59

    goto :goto_4

    :cond_6
    const/16 v17, 0x5d

    goto :goto_4

    :cond_7
    const/16 v17, 0x15

    :cond_8
    :goto_4
    xor-int v2, v9, v17

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v3, 0x0

    goto :goto_2
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->name:Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->algorithm:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->provider:Ljava/lang/Object;

    int-to-long p1, p4

    sget-object p3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->a:[Ljava/lang/String;

    const/4 v0, 0x1

    aget-object v0, p3, v0

    const-wide/16 v1, 0x0

    invoke-static {p1, p2, v1, v2, v0}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->digestLength:I

    int-to-long p1, p5

    const/4 p4, 0x3

    aget-object p3, p3, p4

    invoke-static {p1, p2, v1, v2, p3}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->keyLength:I

    return-void
.end method

.method private a([B)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->b()Z

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->provider:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->algorithm:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->provider:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->digestLength:I

    invoke-static {v0, v1, p1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->macFor(Ljava/lang/String;Ljava/lang/String;[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_1

    :cond_0
    if-nez v0, :cond_2

    :try_start_2
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->provider:Ljava/lang/Object;

    instance-of v1, v0, Ljava/security/Provider;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    :try_start_3
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->algorithm:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->provider:Ljava/lang/Object;

    check-cast v1, Ljava/security/Provider;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->digestLength:I

    invoke-static {v0, v1, p1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->macFor(Ljava/lang/String;Ljava/security/Provider;[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    move-result-object p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->algorithm:Ljava/lang/String;

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->digestLength:I

    invoke-static {v0, v1, p1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->macFor(Ljava/lang/String;Ljava/lang/String;[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    move-result-object p1

    :goto_1
    return-object p1

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static entryFor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;
    .locals 7

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    return-object v6
.end method

.method public static entryFor(Ljava/lang/String;Ljava/lang/String;Ljava/security/Provider;II)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;
    .locals 7

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    return-object v6
.end method


# virtual methods
.method public clientToServer(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory$OperationException;
        }
    .end annotation

    :try_start_0
    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->keyLength:I

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->integrityKeyClientToServer(I)[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->a([B)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory$OperationException;

    invoke-direct {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory$OperationException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public serverToClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory$OperationException;
        }
    .end annotation

    :try_start_0
    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->keyLength:I

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->integrityKeyServerToClient(I)[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->a([B)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory$OperationException;

    invoke-direct {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory$OperationException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->a:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x7

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->algorithm:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->provider:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->digestLength:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->keyLength:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
