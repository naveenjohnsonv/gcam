.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexEcdhInitCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/ssh/protocol/messages/Message;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexEcdhInitCodec;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexEcdhInitCodec;->b:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexEcdhInitCodec;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexEcdhInitCodec;->b:Ljava/lang/Object;

    invoke-static {v0, v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->readValue(Ljava/lang/String;Ljava/lang/Object;Ljava/io/InputStream;)Ljava/security/PublicKey;

    move-result-object p1

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhInit;

    invoke-direct {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhInit;-><init>(Ljava/security/PublicKey;)V

    return-object v0
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexEcdhInitCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhInit;

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexEcdhInit;->qc:Ljava/security/PublicKey;

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->writeValue(Ljava/security/PublicKey;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexEcdhInitCodec;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
