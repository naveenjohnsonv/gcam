.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;


# static fields
.field private static final a:I = 0xfff1

.field private static final b:I = 0x15b0


# instance fields
.field private c:J

.field private d:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    return-void
.end method

.method static a(JJJ)J
    .locals 16

    const-wide/32 v0, 0xfff1

    rem-long v2, p4, v0

    const-wide/32 v4, 0xffff

    and-long v6, p0, v4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v8

    mul-long v9, v2, v6

    rem-long/2addr v9, v0

    and-long v11, p2, v4

    add-long/2addr v11, v0

    const-wide/16 v13, 0x1

    sub-long/2addr v11, v13

    add-long/2addr v6, v11

    const/16 v11, 0x10

    shr-long v12, p0, v11

    and-long/2addr v12, v4

    shr-long v14, p2, v11

    and-long/2addr v4, v14

    add-long/2addr v12, v4

    add-long/2addr v12, v0

    sub-long/2addr v12, v2

    add-long/2addr v9, v12

    cmp-long v2, v6, v0

    if-nez v8, :cond_1

    if-ltz v2, :cond_0

    sub-long/2addr v6, v0

    :cond_0
    cmp-long v2, v6, v0

    :cond_1
    const-wide/32 v3, 0x1ffe2

    if-nez v8, :cond_3

    if-ltz v2, :cond_2

    sub-long/2addr v6, v0

    :cond_2
    cmp-long v2, v9, v3

    :cond_3
    if-nez v8, :cond_6

    if-ltz v2, :cond_4

    sub-long/2addr v9, v3

    :cond_4
    if-nez v8, :cond_5

    cmp-long v2, v9, v0

    goto :goto_0

    :cond_5
    move-wide v6, v9

    goto :goto_1

    :cond_6
    :goto_0
    if-ltz v2, :cond_7

    sub-long/2addr v9, v0

    :cond_7
    shl-long v0, v9, v11

    :goto_1
    or-long/2addr v0, v6

    return-wide v0
.end method


# virtual methods
.method public copy()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;
    .locals 3

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;-><init>()V

    iget-wide v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    iput-wide v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    iget-wide v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    iput-wide v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    return-object v0
.end method

.method public bridge synthetic copy()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;
    .locals 1

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->copy()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;

    move-result-object v0

    return-object v0
.end method

.method public getValue()J
    .locals 4

    iget-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    const/16 v2, 0x10

    shl-long/2addr v0, v2

    iget-wide v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public reset()V
    .locals 2

    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    return-void
.end method

.method public reset(J)V
    .locals 4

    const-wide/32 v0, 0xffff

    and-long v2, p1, v0

    iput-wide v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    const/16 v2, 0x10

    shr-long/2addr p1, v2

    and-long/2addr p1, v0

    iput-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    return-void
.end method

.method public update([BII)V
    .locals 11

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const/16 v1, 0x15b0

    const/4 v2, 0x1

    const-wide/32 v3, 0xfff1

    if-nez v0, :cond_1

    if-ne p3, v2, :cond_0

    iget-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    aget-byte p1, p1, p2

    and-int/lit16 p1, p1, 0xff

    int-to-long p1, p1

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    iget-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    add-long/2addr p1, v0

    iput-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    rem-long/2addr v0, v3

    iput-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    rem-long/2addr p1, v3

    iput-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    return-void

    :cond_0
    move v2, v1

    :cond_1
    div-int v2, p3, v2

    rem-int/2addr p3, v1

    :goto_0
    add-int/lit8 v5, v2, -0x1

    if-lez v2, :cond_6

    if-nez v0, :cond_7

    move v2, v1

    :goto_1
    add-int/lit8 v6, v2, -0x1

    if-lez v2, :cond_3

    iget-wide v7, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    add-int/lit8 v2, p2, 0x1

    aget-byte p2, p1, p2

    and-int/lit16 p2, p2, 0xff

    int-to-long v9, p2

    add-long/2addr v7, v9

    iput-wide v7, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    iget-wide v9, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    add-long/2addr v9, v7

    iput-wide v9, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    move p2, v2

    if-nez v0, :cond_4

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    move v2, v6

    goto :goto_1

    :cond_3
    :goto_2
    iget-wide v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    rem-long/2addr v6, v3

    iput-wide v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    iget-wide v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    rem-long/2addr v6, v3

    iput-wide v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    :cond_4
    if-eqz v0, :cond_5

    goto :goto_3

    :cond_5
    move v2, v5

    goto :goto_0

    :cond_6
    :goto_3
    move v1, p3

    :cond_7
    :goto_4
    add-int/lit8 p3, v1, -0x1

    if-lez v1, :cond_9

    iget-wide v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    add-int/lit8 v5, p2, 0x1

    aget-byte p2, p1, p2

    and-int/lit16 p2, p2, 0xff

    int-to-long v6, p2

    add-long/2addr v1, v6

    iput-wide v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    iget-wide v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    add-long/2addr v6, v1

    iput-wide v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    if-nez v0, :cond_a

    if-eqz v0, :cond_8

    goto :goto_5

    :cond_8
    move v1, p3

    move p2, v5

    goto :goto_4

    :cond_9
    :goto_5
    iget-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    rem-long/2addr p1, v3

    iput-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->c:J

    iget-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    rem-long/2addr p1, v3

    iput-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->d:J

    :cond_a
    return-void
.end method
