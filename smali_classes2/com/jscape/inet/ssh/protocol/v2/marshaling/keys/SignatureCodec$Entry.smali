.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final codec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$EntryCodec;

.field public final identifier:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x8

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x7b

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "tPXq \u000e\u0006e\u0013\u001d\u001eOl=K\u001e1\u0014^p0\u0002\u00031\u0015I#c"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x1c

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x1e

    goto :goto_2

    :cond_2
    const/16 v12, 0x10

    goto :goto_2

    :cond_3
    const/16 v12, 0x3f

    goto :goto_2

    :cond_4
    const/16 v12, 0x65

    goto :goto_2

    :cond_5
    const/16 v12, 0x40

    goto :goto_2

    :cond_6
    const/16 v12, 0xb

    goto :goto_2

    :cond_7
    const/16 v12, 0x23

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$EntryCodec;)V
    .locals 0

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifier:Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$EntryCodec;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$EntryCodec;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;->identifier:Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;->codec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$EntryCodec;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;->identifier:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;->codec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$EntryCodec;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
