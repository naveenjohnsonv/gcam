.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;


# static fields
.field private static final e:[Ljava/lang/String;


# instance fields
.field private final a:Ljavax/crypto/Mac;

.field private final b:I

.field private final c:[B

.field private final d:Lcom/jscape/util/h/o;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xd

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x1c

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\u0014~X\u0019&\u0006W%\u007f\\\'\"X\u000fr=Y= \u0000\u0004*QX: \u0011\u001fc\u0012\u001c|Yt#\u000c\u0010;nIt+\u0000\u00199iUz"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x30

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->e:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v13, 0x2

    if-eq v12, v13, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x6b

    goto :goto_2

    :cond_2
    const/16 v12, 0x79

    goto :goto_2

    :cond_3
    const/16 v12, 0x5b

    goto :goto_2

    :cond_4
    const/16 v12, 0x48

    goto :goto_2

    :cond_5
    const/16 v12, 0x21

    goto :goto_2

    :cond_6
    move v12, v7

    goto :goto_2

    :cond_7
    const/16 v12, 0x42

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljavax/crypto/Mac;I)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->a:Ljavax/crypto/Mac;

    invoke-virtual {p1}, Ljavax/crypto/Mac;->getMacLength()I

    move-result p1

    int-to-long v0, p1

    int-to-long v2, p2

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->e:[Ljava/lang/String;

    const/4 v4, 0x2

    aget-object p1, p1, v4

    invoke-static {v0, v1, v2, v3, p1}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->b:I

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->a:Ljavax/crypto/Mac;

    invoke-virtual {p1}, Ljavax/crypto/Mac;->getMacLength()I

    move-result p1

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->c:[B

    new-instance p1, Lcom/jscape/util/h/o;

    iget p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->b:I

    invoke-direct {p1, p2}, Lcom/jscape/util/h/o;-><init>(I)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->d:Lcom/jscape/util/h/o;

    return-void
.end method

.method private static a(Ljavax/crypto/Mac;[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {p0}, Ljavax/crypto/Mac;->getAlgorithm()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {p0, v0}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;

    invoke-direct {p1, p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;-><init>(Ljavax/crypto/Mac;I)V

    return-object p1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static macFor(Ljava/lang/String;Ljava/lang/String;[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->b()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    :try_start_0
    invoke-static {p0, p1}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_0
    move-object p0, p1

    :cond_1
    invoke-static {p0}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object p0

    :goto_0
    invoke-static {p0, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->a(Ljavax/crypto/Mac;[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    move-result-object p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-object p0

    :catch_1
    move-exception p0

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;

    invoke-direct {p1, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;-><init>(Ljava/lang/Throwable;)V

    throw p1
.end method

.method public static macFor(Ljava/lang/String;Ljava/security/Provider;[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;
        }
    .end annotation

    if-eqz p1, :cond_0

    :try_start_0
    invoke-static {p0, p1}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Mac;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_0
    invoke-static {p0}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object p0

    :goto_0
    invoke-static {p0, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->a(Ljavax/crypto/Mac;[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    move-result-object p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-object p0

    :catch_1
    move-exception p0

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;

    invoke-direct {p1, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;-><init>(Ljava/lang/Throwable;)V

    throw p1
.end method


# virtual methods
.method public apply([B[BIILjava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->a:Ljavax/crypto/Mac;

    invoke-virtual {v0, p1}, Ljavax/crypto/Mac;->update([B)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->a:Ljavax/crypto/Mac;

    invoke-virtual {p1, p2, p3, p4}, Ljavax/crypto/Mac;->update([BII)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->a:Ljavax/crypto/Mac;

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->c:[B

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Ljavax/crypto/Mac;->doFinal([BI)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->c:[B

    iget p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->b:I

    invoke-virtual {p5, p1, p3, p2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public assertMacValid([B[BII[B)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->d:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->c()Lcom/jscape/util/h/o;

    iget-object v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->d:Lcom/jscape/util/h/o;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v1 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->apply([B[BIILjava/io/OutputStream;)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->d:Lcom/jscape/util/h/o;

    invoke-virtual {p1}, Lcom/jscape/util/h/o;->a()[B

    move-result-object p1

    iget p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->b:I

    invoke-static {p1, p5, p2}, Lcom/jscape/util/v;->d([B[BI)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$InvalidMacException;

    invoke-direct {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$InvalidMacException;-><init>()V

    throw p1
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public digestLength()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->b:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->e:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->a:Ljavax/crypto/Mac;

    invoke-virtual {v2}, Ljavax/crypto/Mac;->getAlgorithm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMac;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
