.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final a:Ljavax/crypto/Cipher;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "(NRiVw \u001b]CEWzr\u0019N^\\Pq _"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;->b:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/4 v5, 0x2

    if-eqz v4, :cond_6

    const/4 v6, 0x1

    if-eq v4, v6, :cond_5

    if-eq v4, v5, :cond_4

    const/4 v6, 0x3

    if-eq v4, v6, :cond_3

    const/4 v6, 0x4

    if-eq v4, v6, :cond_2

    const/4 v6, 0x5

    if-eq v4, v6, :cond_1

    goto :goto_1

    :cond_1
    const/16 v5, 0x44

    goto :goto_1

    :cond_2
    const/16 v5, 0x68

    goto :goto_1

    :cond_3
    const/16 v5, 0x7c

    goto :goto_1

    :cond_4
    const/16 v5, 0x67

    goto :goto_1

    :cond_5
    const/16 v5, 0x7d

    goto :goto_1

    :cond_6
    const/16 v5, 0x32

    :goto_1
    const/16 v4, 0x50

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/crypto/Cipher;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;->a:Ljavax/crypto/Cipher;

    return-void
.end method

.method private static a(Ljavax/crypto/Cipher;[B[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->c()Z

    move-result v0

    invoke-virtual {p0}, Ljavax/crypto/Cipher;->getAlgorithm()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;->a(Ljava/lang/String;[B)Ljava/security/Key;

    move-result-object p1

    array-length v1, p2

    if-lez v1, :cond_0

    new-instance v1, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v1, p2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    :try_start_0
    invoke-virtual {p0, p3, p1, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0, p3, p1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;

    invoke-direct {p1, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;-><init>(Ljavax/crypto/Cipher;)V

    return-object p1

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private static a(Ljava/lang/String;[B)Ljava/security/Key;
    .locals 2

    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_0
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v0, p1, p0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v0
.end method

.method public static encryptionFor(Ljava/lang/String;Ljava/lang/String;[B[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption$OperationException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :try_start_0
    invoke-static {p0, p1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_0
    move-object p0, p1

    :cond_1
    invoke-static {p0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object p0

    :goto_0
    invoke-static {p0, p2, p3, p4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;->a(Ljavax/crypto/Cipher;[B[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    move-result-object p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-object p0

    :catch_1
    move-exception p0

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption$OperationException;

    invoke-direct {p1, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption$OperationException;-><init>(Ljava/lang/Throwable;)V

    throw p1
.end method

.method public static encryptionFor(Ljava/lang/String;Ljava/security/Provider;[B[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption$OperationException;
        }
    .end annotation

    if-eqz p1, :cond_0

    :try_start_0
    invoke-static {p0, p1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_0
    invoke-static {p0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object p0

    :goto_0
    invoke-static {p0, p2, p3, p4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;->a(Ljavax/crypto/Cipher;[B[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    move-result-object p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-object p0

    :catch_1
    move-exception p0

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption$OperationException;

    invoke-direct {p1, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption$OperationException;-><init>(Ljava/lang/Throwable;)V

    throw p1
.end method


# virtual methods
.method public apply([BII[BI)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption$OperationException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;->a:Ljavax/crypto/Cipher;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Ljavax/crypto/Cipher;->update([BII[BI)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption$OperationException;

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption$OperationException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public blockLength()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;->a:Ljavax/crypto/Cipher;

    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;->a:Ljavax/crypto/Cipher;

    invoke-virtual {v1}, Ljavax/crypto/Cipher;->getAlgorithm()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
