.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaSignatureCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$EntryCodec;


# static fields
.field private static final a:I = 0x14

.field private static final b:I = 0x14


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a([BII)[B
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;->b()[Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-lez p3, :cond_1

    aget-byte v1, p1, p2

    if-nez v0, :cond_2

    if-nez v0, :cond_2

    if-nez v1, :cond_1

    add-int/lit8 p2, p2, 0x1

    add-int/lit8 p3, p3, -0x1

    if-eqz v0, :cond_0

    :cond_1
    aget-byte v1, p1, p2

    :cond_2
    const/4 v2, 0x1

    if-nez v0, :cond_4

    if-lez v1, :cond_3

    move v1, p3

    goto :goto_0

    :cond_3
    move v1, p3

    :cond_4
    add-int/2addr v1, v2

    :goto_0
    aget-byte v3, p1, p2

    if-nez v0, :cond_5

    if-lez v3, :cond_6

    const/4 v0, 0x0

    move v2, v0

    goto :goto_1

    :cond_5
    move v2, v3

    :cond_6
    :goto_1
    new-array v0, v1, [B

    invoke-static {p1, p2, v0, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method private a([BIII)[B
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;->b()[Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-le p3, p4, :cond_1

    add-int/lit8 p2, p2, 0x1

    add-int/lit8 p3, p3, -0x1

    if-eqz v0, :cond_0

    :cond_1
    new-array v0, p4, [B

    sub-int/2addr p4, p3

    invoke-static {p1, p2, v0, p4, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method


# virtual methods
.method public read(Ljava/lang/String;Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readValue(Ljava/io/InputStream;)[B

    move-result-object p2

    const/4 v0, 0x0

    const/16 v1, 0x14

    invoke-direct {p0, p2, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaSignatureCodec;->a([BII)[B

    move-result-object v0

    invoke-direct {p0, p2, v1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaSignatureCodec;->a([BII)[B

    move-result-object p2

    new-instance v1, Lcom/jscape/util/h/o;

    invoke-direct {v1}, Lcom/jscape/util/h/o;-><init>()V

    const/16 v2, 0x30

    invoke-virtual {v1, v2}, Lcom/jscape/util/h/o;->write(I)V

    array-length v2, v0

    array-length v3, p2

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x4

    invoke-virtual {v1, v2}, Lcom/jscape/util/h/o;->write(I)V

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/jscape/util/h/o;->write(I)V

    array-length v3, v0

    invoke-virtual {v1, v3}, Lcom/jscape/util/h/o;->write(I)V

    invoke-virtual {v1, v0}, Lcom/jscape/util/h/o;->write([B)V

    invoke-virtual {v1, v2}, Lcom/jscape/util/h/o;->write(I)V

    array-length v0, p2

    invoke-virtual {v1, v0}, Lcom/jscape/util/h/o;->write(I)V

    invoke-virtual {v1, p2}, Lcom/jscape/util/h/o;->write([B)V

    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->d()[B

    move-result-object v0

    invoke-direct {p2, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;-><init>(Ljava/lang/String;[B)V

    return-object p2
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;Ljava/io/OutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;->blob:[B

    const/4 v1, 0x3

    aget-byte v0, v0, v1

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;->blob:[B

    const/4 v2, 0x4

    const/16 v3, 0x14

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaSignatureCodec;->a([BIII)[B

    move-result-object v1

    iget-object v2, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;->blob:[B

    add-int/lit8 v4, v0, 0x5

    aget-byte v2, v2, v4

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;->blob:[B

    add-int/lit8 v0, v0, 0x6

    invoke-direct {p0, p1, v0, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaSignatureCodec;->a([BIII)[B

    move-result-object p1

    array-length v0, v1

    array-length v2, p1

    add-int/2addr v0, v2

    new-array v0, v0, [B

    array-length v2, v1

    const/4 v3, 0x0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v1, v1

    array-length v2, p1

    invoke-static {p1, v3, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    return-void
.end method
