.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;
.super Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;


# static fields
.field private static final A:I = 0x2

.field private static final B:I = 0x3

.field private static final C:I = 0x4

.field private static final D:I = 0x9

.field private static final E:I = 0x0

.field private static final F:I = 0x1

.field private static final G:I = 0x2

.field private static final H:I = -0x1

.field private static final I:I = -0x2

.field private static final J:I = -0x3

.field private static final K:I = -0x4

.field private static final L:I = -0x5

.field private static final M:I = -0x6

.field private static final O:[Ljava/lang/String;

.field private static final w:I = 0xf

.field private static final x:I = 0xf

.field private static final y:I = 0x0

.field private static final z:I = 0x1


# instance fields
.field private N:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, -0x1

    move v4, v0

    move v5, v2

    :goto_0
    const/16 v6, 0x28

    const/4 v7, 0x1

    add-int/2addr v3, v7

    add-int/2addr v4, v3

    const-string v8, "i+\u0002i+"

    invoke-virtual {v8, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    array-length v9, v3

    move v10, v2

    :goto_1
    const/4 v11, 0x5

    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v3}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v6, v5, 0x1

    aput-object v3, v1, v5

    if-ge v4, v11, :cond_0

    invoke-virtual {v8, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    move v5, v6

    move/from16 v16, v4

    move v4, v3

    move/from16 v3, v16

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->O:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v3, v10

    rem-int/lit8 v13, v10, 0x7

    const/4 v14, 0x4

    if-eqz v13, :cond_6

    if-eq v13, v7, :cond_5

    if-eq v13, v0, :cond_4

    const/4 v15, 0x3

    if-eq v13, v15, :cond_7

    if-eq v13, v14, :cond_3

    if-eq v13, v11, :cond_2

    const/16 v14, 0x79

    goto :goto_2

    :cond_2
    const/16 v14, 0x55

    goto :goto_2

    :cond_3
    move v14, v7

    goto :goto_2

    :cond_4
    const/16 v14, 0x58

    goto :goto_2

    :cond_5
    const/16 v14, 0x23

    goto :goto_2

    :cond_6
    const/16 v14, 0x7b

    :cond_7
    :goto_2
    xor-int v11, v6, v14

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v3, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->N:Z

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;
        }
    .end annotation

    const/16 v0, 0xf

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;-><init>(II)V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;-><init>(IIZ)V

    return-void
.end method

.method public constructor <init>(III)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->N:Z

    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->init(III)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->O:[Ljava/lang/String;

    const/4 v0, 0x1

    aget-object p1, p1, v0

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->msg:Ljava/lang/String;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;

    move-result-object p1

    throw p1
.end method

.method public constructor <init>(IIILcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->N:Z

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->init(IIILcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->O:[Ljava/lang/String;

    const/4 p4, 0x1

    aget-object p1, p1, p4

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->msg:Ljava/lang/String;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;

    move-result-object p1

    throw p1
.end method

.method public constructor <init>(IIZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->N:Z

    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->init(IIZ)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->O:[Ljava/lang/String;

    aget-object p1, p1, v0

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->msg:Ljava/lang/String;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;

    move-result-object p1

    throw p1
.end method

.method public constructor <init>(IZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;
        }
    .end annotation

    const/16 v0, 0xf

    invoke-direct {p0, p1, v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;-><init>(IIZ)V

    return-void
.end method

.method private static a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public copy(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;)I
    .locals 1

    iget-boolean v0, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->N:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->N:Z

    invoke-static {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)I

    move-result p1

    return p1
.end method

.method public deflate(I)I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    const/4 p1, -0x2

    return p1

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    :cond_1
    invoke-virtual {v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h(I)I

    move-result p1

    if-nez v0, :cond_2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->N:Z

    :cond_2
    return p1
.end method

.method public end()I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->N:Z

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    const/4 v0, -0x2

    return v0

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    :cond_1
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->k()I

    move-result v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->free()V

    return v0
.end method

.method public finished()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->N:Z

    return v0
.end method

.method public init(I)I
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->init(II)I

    move-result p1

    return p1
.end method

.method public init(II)I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->init(IIZ)I

    move-result p1

    return p1
.end method

.method public init(III)I
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->N:Z

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    invoke-direct {v0, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b(III)I

    move-result p1

    return p1
.end method

.method public init(IIILcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;)I
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const/4 v1, -0x2

    if-nez v0, :cond_7

    const/16 v2, 0x9

    if-lt p2, v2, :cond_6

    if-nez v0, :cond_7

    const/16 v2, 0xf

    if-le p2, v2, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;->W_NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    if-nez v0, :cond_2

    if-ne p4, v2, :cond_1

    mul-int/lit8 p2, p2, -0x1

    if-eqz v0, :cond_5

    :cond_1
    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;->W_GZIP:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    :cond_2
    if-nez v0, :cond_4

    if-ne p4, v2, :cond_3

    add-int/lit8 p2, p2, 0x10

    if-eqz v0, :cond_5

    :cond_3
    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;->W_ANY:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    :cond_4
    if-ne p4, v2, :cond_5

    return v1

    :cond_5
    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->init(III)I

    move-result p1

    return p1

    :cond_6
    :goto_0
    move p2, v1

    :cond_7
    return p2
.end method

.method public init(IIZ)I
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->N:Z

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    invoke-direct {v1, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)V

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    if-nez v0, :cond_0

    if-eqz p3, :cond_1

    neg-int p2, p2

    goto :goto_0

    :cond_0
    move p2, p3

    :cond_1
    :goto_0
    invoke-virtual {v1, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->c(II)I

    move-result p1

    return p1
.end method

.method public init(IZ)I
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->init(IIZ)I

    move-result p1

    return p1
.end method

.method public params(II)I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    const/4 p1, -0x2

    return p1

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    :cond_1
    invoke-virtual {v1, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->d(II)I

    move-result p1

    return p1
.end method

.method public setDictionary([BI)I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    const/4 p1, -0x2

    return p1

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    :cond_1
    invoke-virtual {v1, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([BI)I

    move-result p1

    return p1
.end method
