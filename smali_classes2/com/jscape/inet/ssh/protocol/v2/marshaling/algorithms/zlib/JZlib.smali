.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;
.super Ljava/lang/Object;


# static fields
.field public static final DEF_WBITS:I = 0xf

.field public static final MAX_WBITS:I = 0xf

.field public static final W_ANY:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

.field public static final W_GZIP:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

.field public static final W_NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

.field public static final W_ZLIB:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

.field public static final Z_ASCII:B = 0x1t

.field public static final Z_BEST_COMPRESSION:I = 0x9

.field public static final Z_BEST_SPEED:I = 0x1

.field public static final Z_BINARY:B = 0x0t

.field public static final Z_BUF_ERROR:I = -0x5

.field public static final Z_DATA_ERROR:I = -0x3

.field public static final Z_DEFAULT_COMPRESSION:I = -0x1

.field public static final Z_DEFAULT_STRATEGY:I = 0x0

.field public static final Z_ERRNO:I = -0x1

.field public static final Z_FILTERED:I = 0x1

.field public static final Z_FINISH:I = 0x4

.field public static final Z_FULL_FLUSH:I = 0x3

.field public static final Z_HUFFMAN_ONLY:I = 0x2

.field public static final Z_MEM_ERROR:I = -0x4

.field public static final Z_NEED_DICT:I = 0x2

.field public static final Z_NO_COMPRESSION:I = 0x0

.field public static final Z_NO_FLUSH:I = 0x0

.field public static final Z_OK:I = 0x0

.field public static final Z_PARTIAL_FLUSH:I = 0x1

.field public static final Z_STREAM_END:I = 0x1

.field public static final Z_STREAM_ERROR:I = -0x2

.field public static final Z_SYNC_FLUSH:I = 0x2

.field public static final Z_UNKNOWN:B = 0x2t

.field public static final Z_VERSION_ERROR:I = -0x6

.field private static final a:Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x5

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v5, v4

    const-string v8, ")\u00191h\u0007\u0005)\u00191h\u0007"

    invoke-virtual {v8, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v3

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v9, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0xb

    if-ge v5, v4, :cond_0

    invoke-virtual {v8, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v9

    move/from16 v16, v5

    move v5, v4

    move/from16 v4, v16

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;->b:[Ljava/lang/String;

    aget-object v0, v1, v7

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;->a:Ljava/lang/String;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;->NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;->W_NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;->ZLIB:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;->W_ZLIB:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;->GZIP:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;->W_GZIP:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;->ANY:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;->W_ANY:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/16 v13, 0x67

    const/16 v14, 0x50

    if-eqz v12, :cond_5

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_4

    const/4 v15, 0x3

    if-eq v12, v15, :cond_3

    const/4 v15, 0x4

    if-eq v12, v15, :cond_6

    if-eq v12, v2, :cond_2

    const/16 v13, 0x4c

    goto :goto_2

    :cond_2
    const/16 v13, 0x5c

    goto :goto_2

    :cond_3
    const/16 v13, 0x16

    goto :goto_2

    :cond_4
    move v13, v14

    goto :goto_2

    :cond_5
    const/16 v13, 0x48

    :cond_6
    :goto_2
    xor-int v12, v14, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static adler32_combine(JJJ)J
    .locals 0

    invoke-static/range {p0 .. p5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;->a(JJJ)J

    move-result-wide p0

    return-wide p0
.end method

.method public static crc32_combine(JJJ)J
    .locals 0

    invoke-static/range {p0 .. p5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->a(JJJ)J

    move-result-wide p0

    return-wide p0
.end method

.method public static version()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;->b:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method
