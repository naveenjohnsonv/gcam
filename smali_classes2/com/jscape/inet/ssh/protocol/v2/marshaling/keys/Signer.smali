.class public interface abstract Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;
.super Ljava/lang/Object;


# virtual methods
.method public abstract assertSignatureValid([BLjava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract sign([BLjava/security/PrivateKey;)Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method
