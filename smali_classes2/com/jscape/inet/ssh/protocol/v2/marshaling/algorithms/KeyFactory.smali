.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;
.super Ljava/lang/Object;


# static fields
.field private static final a:B = 0x41t

.field private static final b:B = 0x42t

.field private static final c:B = 0x43t

.field private static final d:B = 0x44t

.field private static final e:B = 0x45t

.field private static final f:B = 0x46t

.field private static final k:[Ljava/lang/String;


# instance fields
.field private final g:Lcom/jscape/a/d;

.field private final h:[B

.field private final i:[B

.field private final j:[B


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "ZyM\u001a?\u0017\u001b{rLB\u0006=XjeZ\r=R\u000fg~D\u0007o\u0019\u001dv7M\u000c#\u0013\nh~F\u0005a\u0011DrQ$.\u0011\u000c`eQB4\u001a\u0019|\u007f\u0015\u000c#7[\u0007<\u0001\u0011`ya\u0006r"

    const/16 v5, 0x47

    const/16 v6, 0x28

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x52

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x1f

    const/16 v4, 0xf

    const-string v6, "XL6aWab\u001a\u000b6QUzkI\u000fXL qU{f\u0010?6zFlwI"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v16, v6

    move v6, v4

    move-object/from16 v4, v16

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x29

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->k:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    const/4 v3, 0x4

    if-eq v2, v3, :cond_5

    if-eq v2, v0, :cond_4

    const/16 v2, 0x2a

    goto :goto_4

    :cond_4
    const/16 v2, 0x20

    goto :goto_4

    :cond_5
    const/16 v2, 0x1d

    goto :goto_4

    :cond_6
    const/16 v2, 0x30

    goto :goto_4

    :cond_7
    const/16 v2, 0x7a

    goto :goto_4

    :cond_8
    const/16 v2, 0x45

    goto :goto_4

    :cond_9
    const/16 v2, 0x5d

    :goto_4
    xor-int/2addr v2, v9

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/a/d;[B[B[B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->g:Lcom/jscape/a/d;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->h:[B

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->i:[B

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->j:[B

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(BI)[B
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->c()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->g:Lcom/jscape/a/d;

    invoke-interface {v1}, Lcom/jscape/a/d;->b()Lcom/jscape/a/d;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->h:[B

    invoke-interface {v1, v2}, Lcom/jscape/a/d;->a([B)Lcom/jscape/a/d;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->i:[B

    invoke-interface {v1, v2}, Lcom/jscape/a/d;->a([B)Lcom/jscape/a/d;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/jscape/a/d;->a(B)Lcom/jscape/a/d;

    move-result-object p1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->j:[B

    invoke-interface {p1, v1}, Lcom/jscape/a/d;->a([B)Lcom/jscape/a/d;

    move-result-object p1

    invoke-interface {p1}, Lcom/jscape/a/d;->c()[B

    move-result-object p1

    :try_start_0
    array-length v1, p1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_1

    if-ne v1, p2, :cond_0

    goto :goto_1

    :cond_0
    if-eqz v0, :cond_3

    :try_start_1
    array-length v1, p1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    if-le v1, p2, :cond_2

    :try_start_2
    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->a([BI)[B

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->b([BI)[B

    move-result-object p1

    :cond_3
    :goto_1
    return-object p1

    :catch_2
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private a([BI)[B
    .locals 0

    new-array p2, p2, [B

    invoke-static {p1, p2}, Lcom/jscape/util/v;->a([B[B)[B

    move-result-object p1

    return-object p1
.end method

.method private b([BI)[B
    .locals 5

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->b()Z

    move-result v0

    new-instance v1, Lcom/jscape/util/h/o;

    invoke-direct {v1}, Lcom/jscape/util/h/o;-><init>()V

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, p1}, Lcom/jscape/util/h/o;->write([B)V

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/util/h/o;->b()I

    move-result p1

    if-ge p1, p2, :cond_1

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->g:Lcom/jscape/a/d;

    invoke-interface {p1}, Lcom/jscape/a/d;->b()Lcom/jscape/a/d;

    move-result-object p1

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->h:[B

    invoke-interface {p1, v3}, Lcom/jscape/a/d;->a([B)Lcom/jscape/a/d;

    move-result-object p1

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->i:[B

    invoke-interface {p1, v3}, Lcom/jscape/a/d;->a([B)Lcom/jscape/a/d;

    move-result-object p1

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v3

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->b()I

    move-result v4

    invoke-interface {p1, v3, v2, v4}, Lcom/jscape/a/d;->a([BII)Lcom/jscape/a/d;

    move-result-object p1

    invoke-interface {p1}, Lcom/jscape/a/d;->c()[B

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_1

    :try_start_1
    invoke-virtual {v1, p1}, Lcom/jscape/util/h/o;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Lcom/jscape/util/h/o;->a()[B

    move-result-object p1

    new-array p2, p2, [B

    invoke-static {p1, p2}, Lcom/jscape/util/v;->a([B[B)[B

    move-result-object p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    return-object p1

    :catch_1
    move-exception p1

    new-instance p2, Ljava/lang/RuntimeException;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->k:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-direct {p2, v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method


# virtual methods
.method public encryptionKeyClientToServer(I)[B
    .locals 1

    const/16 v0, 0x43

    invoke-direct {p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->a(BI)[B

    move-result-object p1

    return-object p1
.end method

.method public encryptionKeyServerToClient(I)[B
    .locals 1

    const/16 v0, 0x44

    invoke-direct {p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->a(BI)[B

    move-result-object p1

    return-object p1
.end method

.method public integrityKeyClientToServer(I)[B
    .locals 1

    const/16 v0, 0x45

    invoke-direct {p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->a(BI)[B

    move-result-object p1

    return-object p1
.end method

.method public integrityKeyServerToClient(I)[B
    .locals 1

    const/16 v0, 0x46

    invoke-direct {p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->a(BI)[B

    move-result-object p1

    return-object p1
.end method

.method public ivClientToServer(I)[B
    .locals 1

    const/16 v0, 0x41

    invoke-direct {p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->a(BI)[B

    move-result-object p1

    return-object p1
.end method

.method public ivServerToClient(I)[B
    .locals 1

    const/16 v0, 0x42

    invoke-direct {p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->a(BI)[B

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->k:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->g:Lcom/jscape/a/d;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->h:[B

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->i:[B

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->j:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
