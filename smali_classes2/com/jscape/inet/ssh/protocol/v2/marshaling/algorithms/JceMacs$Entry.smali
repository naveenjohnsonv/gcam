.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;
.super Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    return-void
.end method


# virtual methods
.method public update(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 4

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;->algorithm:Ljava/lang/String;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;->digestLength:I

    iget v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;->keyLength:I

    invoke-static {v0, v1, p2, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->entryFor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

    move-result-object p2

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p1

    return-object p1
.end method

.method public update(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 4

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;->algorithm:Ljava/lang/String;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;->digestLength:I

    iget v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;->keyLength:I

    invoke-static {v0, v1, p2, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->entryFor(Ljava/lang/String;Ljava/lang/String;Ljava/security/Provider;II)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

    move-result-object p2

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p1

    return-object p1
.end method
