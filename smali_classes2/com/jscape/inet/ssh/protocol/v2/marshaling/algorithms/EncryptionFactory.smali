.class public interface abstract Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;
.super Ljava/lang/Object;


# virtual methods
.method public abstract clientToServerEncryptionFor(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$Mode;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$OperationException;
        }
    .end annotation
.end method

.method public abstract encryptions()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract serverToClientEncryptionFor(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$Mode;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$OperationException;
        }
    .end annotation
.end method
