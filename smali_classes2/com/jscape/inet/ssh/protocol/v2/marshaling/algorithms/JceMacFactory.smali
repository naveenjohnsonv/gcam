.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x17

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x69

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "i[_\u000bT+$B[N)G1BX]T2G!\u0007P\u0005\u0017vVI3E8\rQL_\"\u0015)\u000eDWH/A \u000f\u0019\u0018"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    const/16 v11, 0x2f

    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    if-ge v3, v11, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->b:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v10

    rem-int/lit8 v13, v10, 0x7

    if-eqz v13, :cond_6

    if-eq v13, v7, :cond_5

    if-eq v13, v0, :cond_4

    const/4 v14, 0x3

    if-eq v13, v14, :cond_7

    const/4 v11, 0x4

    if-eq v13, v11, :cond_3

    const/4 v11, 0x5

    if-eq v13, v11, :cond_2

    const/16 v11, 0xb

    goto :goto_2

    :cond_2
    const/16 v11, 0x21

    goto :goto_2

    :cond_3
    const/16 v11, 0x5c

    goto :goto_2

    :cond_4
    const/16 v11, 0x53

    goto :goto_2

    :cond_5
    const/16 v11, 0x51

    goto :goto_2

    :cond_6
    const/16 v11, 0x4a

    :cond_7
    :goto_2
    xor-int/2addr v11, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public varargs constructor <init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->a:Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->c()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->b:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public clientToServerMacFor(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory$OperationException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->a(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->clientToServer(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    move-result-object p1

    return-object p1
.end method

.method public macs()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public serverToClientMacFor(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory$OperationException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->a(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->serverToClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    move-result-object p1

    return-object p1
.end method

.method public varargs set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->b()Z

    move-result v0

    array-length v1, p1

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v1, :cond_1

    aget-object v3, p1, v2

    if-nez v0, :cond_1

    :try_start_0
    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->a:Ljava/util/Map;

    iget-object v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->name:Ljava/lang/String;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->b:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->a:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
