.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$InvalidMacException;
.super Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "p\u0014\"\\E/J\u00197\u0015~\u0007"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$InvalidMacException;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x3b

    goto :goto_1

    :cond_1
    const/16 v4, 0x53

    goto :goto_1

    :cond_2
    const/16 v4, 0x3c

    goto :goto_1

    :cond_3
    const/16 v4, 0x28

    goto :goto_1

    :cond_4
    const/16 v4, 0x41

    goto :goto_1

    :cond_5
    const/16 v4, 0x6f

    goto :goto_1

    :cond_6
    const/16 v4, 0x2c

    :goto_1
    const/16 v5, 0x15

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$InvalidMacException;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;-><init>(Ljava/lang/String;)V

    return-void
.end method
