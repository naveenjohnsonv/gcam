.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readValue(Ljava/io/InputStream;)J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/h/a/i;->a(Ljava/io/InputStream;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static writeValue(JLjava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0, p1, p2}, Lcom/jscape/util/h/a/i;->a(JLjava/io/OutputStream;)V

    return-void
.end method
