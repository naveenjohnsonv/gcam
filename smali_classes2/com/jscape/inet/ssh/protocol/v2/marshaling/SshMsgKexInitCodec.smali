.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexInitCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/ssh/protocol/messages/Message;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:B = 0x14t


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a([B)[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/o;

    invoke-direct {v0}, Lcom/jscape/util/h/o;-><init>()V

    const/16 v1, 0x14

    invoke-static {v1, v0}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    invoke-virtual {v0, p1}, Lcom/jscape/util/h/o;->write([B)V

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->d()[B

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static/range {p1 .. p1}, Lcom/jscape/util/X;->c(Ljava/io/InputStream;)[B

    move-result-object v0

    new-instance v1, Lcom/jscape/util/h/e;

    invoke-direct {v1, v0}, Lcom/jscape/util/h/e;-><init>([B)V

    const/16 v2, 0x10

    invoke-static {v2, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/ByteArrayCodec;->readValue(ILjava/io/InputStream;)[B

    move-result-object v4

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v5

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v6

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v7

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v8

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v9

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v10

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v11

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v12

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v13

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v14

    invoke-static {v1}, Lcom/jscape/util/h/a/a;->a(Ljava/io/InputStream;)Z

    move-result v15

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    move-object v3, v1

    invoke-direct/range {v3 .. v15}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;-><init>([BLjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Z)V

    move-object/from16 v2, p0

    invoke-direct {v2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexInitCodec;->a([B)[B

    move-result-object v0

    iput-object v0, v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->rawData:[B

    return-object v1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexInitCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    new-instance v0, Lcom/jscape/util/h/o;

    invoke-direct {v0}, Lcom/jscape/util/h/o;-><init>()V

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->cookie:[B

    invoke-static {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/ByteArrayCodec;->writeValue([BLjava/io/OutputStream;)V

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->kexAlgorithms:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->writeUsAsciiValue(Ljava/util/List;Ljava/io/OutputStream;)V

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->serverHostKeyAlgorithms:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->writeUsAsciiValue(Ljava/util/List;Ljava/io/OutputStream;)V

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->encryptionAlgorithmsClientToServer:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->writeUsAsciiValue(Ljava/util/List;Ljava/io/OutputStream;)V

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->encryptionAlgorithmsServerToClient:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->writeUsAsciiValue(Ljava/util/List;Ljava/io/OutputStream;)V

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->macAlgorithmsClientToServer:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->writeUsAsciiValue(Ljava/util/List;Ljava/io/OutputStream;)V

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->macAlgorithmsServerToClient:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->writeUsAsciiValue(Ljava/util/List;Ljava/io/OutputStream;)V

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->compressionAlgorithmsClientToServer:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->writeUsAsciiValue(Ljava/util/List;Ljava/io/OutputStream;)V

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->compressionAlgorithmsServerToClient:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->writeUsAsciiValue(Ljava/util/List;Ljava/io/OutputStream;)V

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->languagesClientToServer:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->writeUsAsciiValue(Ljava/util/List;Ljava/io/OutputStream;)V

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->languagesServerToClient:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->writeUsAsciiValue(Ljava/util/List;Ljava/io/OutputStream;)V

    iget-boolean v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->firstKexPacketFollows:Z

    invoke-static {v1, v0}, Lcom/jscape/util/h/a/a;->a(ZLjava/io/OutputStream;)V

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->d()[B

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexInitCodec;->a([B)[B

    move-result-object v1

    iput-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->rawData:[B

    invoke-virtual {v0, p2}, Lcom/jscape/util/h/o;->a(Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexInitCodec;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
