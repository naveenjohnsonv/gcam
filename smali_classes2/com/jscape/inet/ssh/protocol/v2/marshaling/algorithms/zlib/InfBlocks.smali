.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;
.super Ljava/lang/Object;


# static fields
.field private static final V:[Ljava/lang/String;

.field private static final a:I = 0x5a0

.field private static final b:[I

.field static final c:[I

.field private static final d:I = 0x0

.field private static final e:I = 0x1

.field private static final f:I = 0x2

.field private static final g:I = -0x1

.field private static final h:I = -0x2

.field private static final i:I = -0x3

.field private static final j:I = -0x4

.field private static final k:I = -0x5

.field private static final l:I = -0x6

.field private static final m:I = 0x0

.field private static final n:I = 0x1

.field private static final o:I = 0x2

.field private static final p:I = 0x3

.field private static final q:I = 0x4

.field private static final r:I = 0x5

.field private static final s:I = 0x6

.field private static final t:I = 0x7

.field private static final u:I = 0x8

.field private static final v:I = 0x9


# instance fields
.field A:[I

.field B:[I

.field C:[I

.field D:[I

.field E:[I

.field F:[[I

.field G:[[I

.field H:[I

.field I:[I

.field private final J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;

.field K:I

.field L:I

.field M:I

.field N:[I

.field O:[B

.field P:I

.field Q:I

.field R:I

.field private S:Z

.field private final T:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;

.field private final U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

.field w:I

.field x:I

.field y:I

.field z:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "=ef>Eo+txd0[c+ti|0Jmo8n~8]n<\u0019=ef>Eo+tiy+\tj*:ld7\tt*$nq+"

    const/16 v5, 0x1c

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x7a

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    const/16 v12, 0x36

    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v13, v10

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v1, v7

    add-int/2addr v6, v5

    if-ge v6, v12, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x12

    const-string v5, "l47o\u0014>z%8-a\u001b<>q#1k#q5..\u00156p|z-k\u00160jmz.|X3wv. `\u001b2>v#,l\u0017;m"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v4

    move-object/from16 v4, v16

    goto :goto_3

    :cond_1
    aput-object v8, v1, v7

    add-int/2addr v6, v5

    if-ge v6, v12, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x2b

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->V:[Ljava/lang/String;

    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b:[I

    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->c:[I

    return-void

    :cond_3
    aget-char v15, v10, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v9, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v2, 0x35

    goto :goto_4

    :cond_4
    const/16 v2, 0x7c

    goto :goto_4

    :cond_5
    const/16 v2, 0x53

    goto :goto_4

    :cond_6
    const/16 v2, 0x25

    goto :goto_4

    :cond_7
    const/16 v2, 0x6a

    goto :goto_4

    :cond_8
    const/16 v2, 0x71

    goto :goto_4

    :cond_9
    const/16 v2, 0x2e

    :goto_4
    xor-int/2addr v2, v8

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v10, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x3
        0x7
        0xf
        0x1f
        0x3f
        0x7f
        0xff
        0x1ff
        0x3ff
        0x7ff
        0xfff
        0x1fff
        0x3fff
        0x7fff
        0xffff
    .end array-data

    :array_1
    .array-data 4
        0x10
        0x11
        0x12
        0x0
        0x8
        0x7
        0x9
        0x6
        0xa
        0x5
        0xb
        0x4
        0xc
        0x3
        0xd
        0x2
        0xe
        0x1
        0xf
    .end array-data
.end method

.method constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;I)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    new-array v1, v0, [I

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->B:[I

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v1

    new-array v2, v0, [I

    iput-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->C:[I

    new-array v2, v0, [I

    iput-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->D:[I

    new-array v2, v0, [I

    iput-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->E:[I

    new-array v2, v0, [[I

    iput-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->F:[[I

    new-array v2, v0, [[I

    iput-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->G:[[I

    new-array v2, v0, [I

    iput-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->H:[I

    new-array v2, v0, [I

    iput-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->I:[I

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;

    invoke-direct {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;-><init>()V

    iput-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->T:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    invoke-direct {v2, v3, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;)V

    iput-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;

    const/16 v2, 0x10e0

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->N:[I

    new-array v2, p2, [B

    iput-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I

    const/4 p2, 0x0

    if-nez v1, :cond_0

    if-nez p1, :cond_1

    move v0, p2

    goto :goto_0

    :cond_0
    move v0, p1

    :cond_1
    :goto_0
    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->S:Z

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->a()V

    return-void
.end method


# virtual methods
.method a(I)I
    .locals 30

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v3

    iget v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget v6, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget v7, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    const/4 v8, 0x1

    if-nez v3, :cond_1

    if-ge v6, v7, :cond_0

    sub-int/2addr v7, v6

    sub-int/2addr v7, v8

    goto :goto_1

    :cond_0
    iget v7, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    move v9, v6

    goto :goto_0

    :cond_1
    move v9, v7

    move v7, v6

    :goto_0
    sub-int/2addr v7, v9

    :goto_1
    move-object v10, v0

    move v9, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move-object v4, v3

    move v3, v2

    move v2, v1

    move/from16 v1, p1

    :goto_2
    iget v11, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    const/4 v13, 0x2

    const/4 v12, -0x3

    const/4 v15, 0x3

    const/4 v14, 0x0

    packed-switch v11, :pswitch_data_0

    const/4 v0, -0x2

    iput v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v6, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v5, v2, v5

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    invoke-virtual {v10, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v0

    return v0

    :pswitch_0
    iput v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v6, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v5, v2, v5

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    invoke-virtual {v10, v12}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v1

    return v1

    :pswitch_1
    move-object v11, v10

    move v10, v7

    const/4 v7, 0x5

    goto/16 :goto_2c

    :pswitch_2
    const/16 v8, 0xe

    const/4 v9, 0x4

    goto/16 :goto_24

    :goto_3
    :pswitch_3
    const/16 v8, 0xe

    goto/16 :goto_21

    :cond_2
    :pswitch_4
    if-ge v6, v15, :cond_7

    if-nez v4, :cond_6

    if-nez v4, :cond_5

    if-eqz v3, :cond_4

    if-eqz v4, :cond_3

    goto :goto_4

    :cond_3
    add-int/lit8 v3, v3, -0x1

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    add-int/lit8 v11, v2, 0x1

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/2addr v1, v6

    or-int/2addr v5, v1

    add-int/lit8 v6, v6, 0x8

    move v2, v11

    move v1, v14

    if-eqz v4, :cond_2

    goto :goto_5

    :cond_4
    move v14, v1

    :goto_4
    iput v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v6, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v5, v2, v5

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    invoke-virtual {v10, v14}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v3

    :cond_5
    return v3

    :cond_6
    move v11, v6

    move v6, v5

    move v5, v3

    goto :goto_6

    :cond_7
    :goto_5
    and-int/lit8 v11, v5, 0x7

    move/from16 v29, v5

    move v5, v3

    move v3, v11

    move v11, v6

    move/from16 v6, v29

    :goto_6
    and-int/lit8 v12, v3, 0x1

    iput v12, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->K:I

    ushr-int/2addr v3, v8

    if-nez v4, :cond_b

    if-eqz v3, :cond_a

    if-eq v3, v8, :cond_c

    if-eq v3, v13, :cond_9

    if-eq v3, v15, :cond_8

    goto/16 :goto_a

    :cond_8
    move v3, v15

    const/4 v8, -0x3

    goto :goto_9

    :cond_9
    move v3, v15

    const/4 v8, -0x3

    goto :goto_8

    :cond_a
    ushr-int/lit8 v3, v6, 0x3

    add-int/lit8 v11, v11, -0x3

    and-int/lit8 v6, v11, 0x7

    ushr-int/2addr v3, v6

    sub-int/2addr v11, v6

    move v6, v3

    goto :goto_7

    :cond_b
    move v11, v3

    :goto_7
    iput v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    if-eqz v4, :cond_d

    :cond_c
    iget-object v3, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->D:[I

    iget-object v12, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->E:[I

    iget-object v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->F:[[I

    iget-object v13, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->G:[[I

    iget-object v15, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    invoke-static {v3, v12, v8, v13, v15}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->a([I[I[[I[[ILcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)I

    iget-object v3, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;

    iget-object v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->D:[I

    aget v22, v8, v14

    iget-object v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->E:[I

    aget v23, v8, v14

    iget-object v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->F:[[I

    aget-object v24, v8, v14

    const/16 v25, 0x0

    iget-object v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->G:[[I

    aget-object v26, v8, v14

    const/16 v27, 0x0

    move-object/from16 v21, v3

    invoke-virtual/range {v21 .. v27}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->a(II[II[II)V

    const/4 v3, 0x3

    ushr-int/2addr v6, v3

    const/4 v8, -0x3

    add-int/2addr v11, v8

    const/4 v12, 0x6

    iput v12, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    if-eqz v4, :cond_d

    :goto_8
    ushr-int/2addr v6, v3

    add-int/2addr v11, v8

    iput v3, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    if-eqz v4, :cond_d

    :goto_9
    ushr-int/lit8 v1, v6, 0x3

    add-int/2addr v11, v8

    const/16 v3, 0x9

    iput v3, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    iget-object v3, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->V:[Ljava/lang/String;

    const/4 v6, 0x2

    aget-object v4, v4, v6

    iput-object v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    iput v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v11, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v5, v2, v5

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    const/4 v1, -0x3

    invoke-virtual {v10, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v1

    return v1

    :cond_d
    :goto_a
    if-eqz v4, :cond_7d

    move v3, v5

    move v5, v6

    move v6, v11

    :cond_e
    :pswitch_5
    const/16 v8, 0x20

    const v11, 0xffff

    if-ge v6, v8, :cond_13

    if-nez v4, :cond_12

    if-nez v4, :cond_11

    if-eqz v3, :cond_10

    if-eqz v4, :cond_f

    goto :goto_b

    :cond_f
    add-int/lit8 v3, v3, -0x1

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    add-int/lit8 v8, v2, 0x1

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/2addr v1, v6

    or-int/2addr v5, v1

    add-int/lit8 v6, v6, 0x8

    move v2, v8

    move v1, v14

    if-eqz v4, :cond_e

    goto :goto_c

    :cond_10
    move v14, v1

    :goto_b
    iput v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v6, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v5, v2, v5

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    invoke-virtual {v10, v14}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v3

    :cond_11
    return v3

    :cond_12
    move v8, v6

    move v6, v5

    move v5, v3

    goto :goto_d

    :cond_13
    :goto_c
    not-int v8, v5

    const/16 v12, 0x10

    ushr-int/2addr v8, v12

    and-int/2addr v8, v11

    move/from16 v29, v5

    move v5, v3

    move v3, v8

    move v8, v6

    move/from16 v6, v29

    :goto_d
    and-int/2addr v11, v6

    if-nez v4, :cond_15

    if-eq v3, v11, :cond_14

    const/16 v3, 0x9

    iput v3, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->V:[Ljava/lang/String;

    aget-object v3, v3, v14

    iput-object v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    iput v6, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v5, v2, v5

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    const/4 v1, -0x3

    invoke-virtual {v10, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v1

    return v1

    :cond_14
    iput v11, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->x:I

    move v3, v14

    move v6, v3

    goto :goto_e

    :cond_15
    move v6, v11

    :goto_e
    iget v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->x:I

    if-nez v4, :cond_17

    if-eqz v8, :cond_16

    const/4 v8, 0x2

    goto :goto_f

    :cond_16
    iget v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->K:I

    :cond_17
    if-nez v4, :cond_19

    if-eqz v8, :cond_18

    const/4 v8, 0x7

    goto :goto_f

    :cond_18
    move v8, v14

    :cond_19
    :goto_f
    iput v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    if-eqz v4, :cond_7c

    move/from16 v29, v5

    move v5, v3

    move/from16 v3, v29

    :pswitch_6
    if-nez v4, :cond_1b

    if-nez v3, :cond_1a

    iput v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v6, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v4, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v3, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v3, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v6, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v6, v2, v6

    int-to-long v8, v6

    add-long/2addr v4, v8

    iput-wide v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v3, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    invoke-virtual {v10, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v1

    return v1

    :cond_1a
    move v8, v9

    goto :goto_10

    :cond_1b
    move v8, v3

    :goto_10
    if-nez v4, :cond_2b

    if-nez v8, :cond_2a

    if-nez v4, :cond_20

    iget v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    if-ne v7, v8, :cond_1f

    iget v11, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    if-nez v4, :cond_1e

    if-eqz v11, :cond_1f

    if-nez v4, :cond_1d

    if-lez v11, :cond_1c

    add-int/lit8 v11, v11, 0x0

    const/4 v7, 0x1

    sub-int/2addr v11, v7

    goto :goto_12

    :cond_1c
    move v11, v14

    goto :goto_11

    :cond_1d
    move v8, v14

    :goto_11
    sub-int v11, v8, v11

    :goto_12
    move v7, v11

    move v8, v14

    goto :goto_13

    :cond_1e
    move v8, v7

    move v7, v11

    goto :goto_14

    :cond_1f
    move v8, v7

    move v7, v9

    :goto_13
    move v9, v7

    goto :goto_14

    :cond_20
    move v8, v7

    :goto_14
    if-nez v4, :cond_29

    if-nez v7, :cond_28

    iput v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    invoke-virtual {v10, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v1

    iget v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    if-nez v4, :cond_22

    if-ge v7, v8, :cond_21

    sub-int/2addr v8, v7

    const/4 v9, 0x1

    sub-int/2addr v8, v9

    goto :goto_16

    :cond_21
    iget v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    move v9, v7

    goto :goto_15

    :cond_22
    move v9, v8

    move v8, v7

    :goto_15
    sub-int/2addr v8, v9

    :goto_16
    if-nez v4, :cond_27

    iget v9, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    if-ne v7, v9, :cond_26

    iget v11, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    if-nez v4, :cond_25

    if-eqz v11, :cond_26

    if-nez v4, :cond_24

    if-lez v11, :cond_23

    add-int/lit8 v11, v11, 0x0

    const/4 v7, 0x1

    sub-int/2addr v11, v7

    move v8, v11

    goto :goto_18

    :cond_23
    move v11, v14

    goto :goto_17

    :cond_24
    move v9, v14

    :goto_17
    sub-int/2addr v9, v11

    move v8, v9

    :goto_18
    move v7, v14

    goto :goto_19

    :cond_25
    move v9, v8

    move v8, v11

    goto :goto_1a

    :cond_26
    :goto_19
    move v9, v8

    goto :goto_1a

    :cond_27
    move v9, v8

    move v8, v7

    :goto_1a
    if-nez v4, :cond_2b

    if-nez v8, :cond_2a

    iput v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v6, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v4, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v3, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v3, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v6, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v6, v2, v6

    int-to-long v8, v6

    add-long/2addr v4, v8

    iput-wide v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v3, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    invoke-virtual {v10, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v1

    return v1

    :cond_28
    move v7, v8

    goto :goto_1b

    :cond_29
    move/from16 v29, v8

    move v8, v7

    move/from16 v7, v29

    goto :goto_1c

    :cond_2a
    :goto_1b
    iget v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->x:I

    move v1, v14

    :cond_2b
    :goto_1c
    if-nez v4, :cond_2d

    if-le v8, v3, :cond_2c

    move v8, v3

    :cond_2c
    move v11, v9

    goto :goto_1d

    :cond_2d
    move v11, v3

    :goto_1d
    if-nez v4, :cond_2f

    if-le v8, v11, :cond_2e

    move v8, v9

    :cond_2e
    iget-object v11, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v11, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v12, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    invoke-static {v11, v2, v12, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v2, v8

    sub-int/2addr v3, v8

    add-int/2addr v7, v8

    move v11, v8

    goto :goto_1e

    :cond_2f
    move v9, v8

    :goto_1e
    sub-int/2addr v9, v11

    iget v11, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->x:I

    sub-int/2addr v11, v8

    if-nez v4, :cond_32

    iput v11, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->x:I

    if-eqz v11, :cond_31

    :cond_30
    const/16 v18, 0x1

    goto/16 :goto_3f

    :cond_31
    iget v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->K:I

    goto :goto_1f

    :cond_32
    move v8, v11

    :goto_1f
    if-nez v4, :cond_34

    if-eqz v8, :cond_33

    const/4 v8, 0x7

    goto :goto_20

    :cond_33
    move v8, v14

    :cond_34
    :goto_20
    iput v8, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    if-eqz v4, :cond_30

    goto/16 :goto_3

    :cond_35
    :goto_21
    if-ge v6, v8, :cond_3a

    if-nez v4, :cond_39

    if-nez v4, :cond_38

    if-eqz v3, :cond_37

    if-eqz v4, :cond_36

    goto :goto_22

    :cond_36
    add-int/lit8 v3, v3, -0x1

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    add-int/lit8 v9, v2, 0x1

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/2addr v1, v6

    or-int/2addr v5, v1

    add-int/lit8 v6, v6, 0x8

    move v2, v9

    move v1, v14

    if-eqz v4, :cond_35

    goto :goto_23

    :cond_37
    move v14, v1

    :goto_22
    iput v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v6, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v5, v2, v5

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    invoke-virtual {v10, v14}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v3

    :cond_38
    return v3

    :cond_39
    move-object v11, v10

    const/4 v9, 0x4

    move v10, v7

    move v7, v6

    move v6, v5

    move-object v5, v4

    move v4, v3

    goto :goto_25

    :cond_3a
    :goto_23
    and-int/lit16 v9, v5, 0x3fff

    iput v9, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->y:I

    and-int/lit8 v11, v9, 0x1f

    if-nez v4, :cond_7b

    const/16 v12, 0x1d

    if-gt v11, v12, :cond_7a

    shr-int/lit8 v9, v9, 0x5

    and-int/lit8 v9, v9, 0x1f

    if-nez v4, :cond_3c

    if-le v9, v12, :cond_3b

    goto/16 :goto_3e

    :cond_3b
    add-int/lit16 v11, v11, 0x102

    move v12, v9

    move v9, v11

    :cond_3c
    add-int/2addr v9, v12

    if-nez v4, :cond_3d

    iget-object v11, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->A:[I

    if-eqz v11, :cond_3d

    array-length v11, v11

    if-nez v4, :cond_3f

    if-ge v11, v9, :cond_3e

    :cond_3d
    new-array v11, v9, [I

    iput-object v11, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->A:[I

    if-eqz v4, :cond_40

    :cond_3e
    move v11, v14

    :cond_3f
    if-ge v11, v9, :cond_40

    iget-object v12, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->A:[I

    aput v14, v12, v11

    add-int/lit8 v11, v11, 0x1

    if-nez v4, :cond_41

    if-eqz v4, :cond_3f

    :cond_40
    ushr-int/lit8 v5, v5, 0xe

    add-int/lit8 v6, v6, -0xe

    iput v14, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->z:I

    :cond_41
    const/4 v9, 0x4

    iput v9, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    :goto_24
    iget v11, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->z:I

    move-object/from16 v29, v4

    move v4, v3

    move v3, v11

    move-object v11, v10

    move v10, v7

    move v7, v6

    move v6, v5

    move-object/from16 v5, v29

    :goto_25
    iget v12, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->y:I

    ushr-int/lit8 v12, v12, 0xa

    add-int/2addr v12, v9

    if-ge v3, v12, :cond_4a

    :cond_42
    if-nez v5, :cond_49

    const/4 v3, 0x3

    if-ge v7, v3, :cond_47

    if-nez v5, :cond_46

    if-nez v5, :cond_45

    if-eqz v4, :cond_44

    if-eqz v5, :cond_43

    goto :goto_26

    :cond_43
    add-int/lit8 v4, v4, -0x1

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    add-int/lit8 v3, v2, 0x1

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/2addr v1, v7

    or-int/2addr v6, v1

    add-int/lit8 v7, v7, 0x8

    move v2, v3

    move v1, v14

    if-eqz v5, :cond_42

    goto :goto_27

    :cond_44
    move v14, v1

    :goto_26
    iput v6, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v7, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v5, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v5, v2, v5

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v10, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    invoke-virtual {v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v4

    :cond_45
    return v4

    :cond_46
    move v3, v4

    goto :goto_28

    :cond_47
    :goto_27
    iget-object v3, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->A:[I

    sget-object v12, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->c:[I

    iget v13, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->z:I

    add-int/lit8 v15, v13, 0x1

    iput v15, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->z:I

    aget v12, v12, v13

    and-int/lit8 v13, v6, 0x7

    aput v13, v3, v12

    const/4 v3, 0x3

    ushr-int/2addr v6, v3

    move v3, v4

    move v4, v6

    :goto_28
    const/4 v6, -0x3

    add-int/2addr v7, v6

    if-eqz v5, :cond_48

    move v6, v4

    move v4, v3

    goto :goto_29

    :cond_48
    move v6, v7

    move v7, v10

    move-object v10, v11

    move-object/from16 v29, v5

    move v5, v4

    move-object/from16 v4, v29

    goto :goto_24

    :cond_49
    move v3, v7

    const/4 v9, 0x3

    goto :goto_2a

    :cond_4a
    :goto_29
    iget v3, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->z:I

    const/16 v9, 0x13

    move/from16 v29, v7

    move v7, v3

    move/from16 v3, v29

    :goto_2a
    if-ge v7, v9, :cond_4c

    iget-object v7, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->A:[I

    sget-object v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->c:[I

    iget v12, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->z:I

    add-int/lit8 v13, v12, 0x1

    iput v13, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->z:I

    aget v9, v9, v12

    aput v14, v7, v9

    if-nez v5, :cond_4d

    if-eqz v5, :cond_4b

    goto :goto_2b

    :cond_4b
    move v7, v3

    goto :goto_29

    :cond_4c
    :goto_2b
    iget-object v7, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->B:[I

    const/4 v9, 0x7

    aput v9, v7, v14

    :cond_4d
    iget-object v7, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->T:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;

    iget-object v9, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->A:[I

    iget-object v12, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->B:[I

    iget-object v13, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->C:[I

    iget-object v15, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->N:[I

    iget-object v8, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    move-object/from16 v21, v7

    move-object/from16 v22, v9

    move-object/from16 v23, v12

    move-object/from16 v24, v13

    move-object/from16 v25, v15

    move-object/from16 v26, v8

    invoke-virtual/range {v21 .. v26}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->a([I[I[I[ILcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)I

    move-result v7

    if-nez v5, :cond_51

    if-eqz v7, :cond_50

    if-nez v5, :cond_4f

    const/4 v1, -0x3

    if-ne v7, v1, :cond_4e

    const/4 v1, 0x0

    iput-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->A:[I

    const/16 v1, 0x9

    iput v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    :cond_4e
    iput v6, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v3, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v5, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v5, v2, v5

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v10, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    invoke-virtual {v11, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v7

    :cond_4f
    return v7

    :cond_50
    iput v14, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->z:I

    :cond_51
    const/4 v7, 0x5

    iput v7, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    move/from16 v29, v6

    move v6, v3

    move v3, v4

    move-object v4, v5

    move/from16 v5, v29

    :goto_2c
    iget v8, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->y:I

    iget v9, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->z:I

    and-int/lit8 v12, v8, 0x1f

    add-int/lit16 v12, v12, 0x102

    shr-int/2addr v8, v7

    and-int/lit8 v7, v8, 0x1f

    add-int/2addr v12, v7

    const/4 v7, -0x1

    if-lt v9, v12, :cond_52

    goto/16 :goto_39

    :cond_52
    iget-object v8, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->B:[I

    aget v8, v8, v14

    :cond_53
    if-ge v6, v8, :cond_57

    if-nez v4, :cond_58

    if-nez v4, :cond_56

    if-eqz v3, :cond_55

    if-eqz v4, :cond_54

    goto :goto_2d

    :cond_54
    add-int/lit8 v3, v3, -0x1

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    add-int/lit8 v9, v2, 0x1

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/2addr v1, v6

    or-int/2addr v5, v1

    add-int/lit8 v6, v6, 0x8

    move v2, v9

    move v1, v14

    if-eqz v4, :cond_53

    goto :goto_2e

    :cond_55
    move v14, v1

    :goto_2d
    iput v5, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v6, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v5, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v5, v2, v5

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v10, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    invoke-virtual {v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v3

    :cond_56
    return v3

    :cond_57
    :goto_2e
    iget-object v9, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->C:[I

    aget v9, v9, v14

    :cond_58
    iget-object v9, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->N:[I

    iget-object v12, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->C:[I

    aget v13, v12, v14

    sget-object v15, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b:[I

    aget v8, v15, v8

    and-int/2addr v8, v5

    add-int/2addr v13, v8

    const/4 v8, 0x3

    mul-int/2addr v13, v8

    const/16 v16, 0x1

    add-int/lit8 v13, v13, 0x1

    aget v13, v9, v13

    aget v12, v12, v14

    aget v15, v15, v13

    and-int/2addr v15, v5

    add-int/2addr v12, v15

    mul-int/2addr v12, v8

    const/4 v8, 0x2

    add-int/2addr v12, v8

    aget v9, v9, v12

    const/16 v12, 0x12

    if-nez v4, :cond_5b

    const/16 v15, 0x10

    if-ge v9, v15, :cond_5a

    ushr-int/2addr v5, v13

    sub-int/2addr v6, v13

    iget-object v15, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->A:[I

    iget v8, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->z:I

    add-int/lit8 v14, v8, 0x1

    iput v14, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->z:I

    aput v9, v15, v8

    if-eqz v4, :cond_59

    goto :goto_2f

    :cond_59
    move v8, v6

    move v6, v5

    move v5, v3

    const/16 v3, 0x10

    goto/16 :goto_38

    :cond_5a
    :goto_2f
    move v8, v12

    goto :goto_30

    :cond_5b
    const/16 v8, 0x10

    :goto_30
    if-nez v4, :cond_5d

    if-ne v9, v8, :cond_5c

    const/4 v8, 0x7

    goto :goto_31

    :cond_5c
    const/16 v8, 0xe

    :cond_5d
    sub-int v8, v9, v8

    :goto_31
    if-nez v4, :cond_5f

    if-ne v9, v12, :cond_5e

    const/16 v12, 0xb

    goto :goto_32

    :cond_5e
    const/4 v12, 0x3

    goto :goto_32

    :cond_5f
    move v12, v9

    :cond_60
    :goto_32
    add-int v14, v13, v8

    if-ge v6, v14, :cond_65

    if-nez v4, :cond_64

    if-nez v4, :cond_63

    if-eqz v3, :cond_62

    if-eqz v4, :cond_61

    const/4 v14, 0x0

    goto :goto_33

    :cond_61
    add-int/lit8 v3, v3, -0x1

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    add-int/lit8 v14, v2, 0x1

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/2addr v1, v6

    or-int/2addr v5, v1

    add-int/lit8 v6, v6, 0x8

    move v2, v14

    const/4 v1, 0x0

    if-eqz v4, :cond_60

    goto :goto_34

    :cond_62
    move v14, v1

    :goto_33
    iput v5, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v6, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v5, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v5, v2, v5

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v10, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    invoke-virtual {v11, v14}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v3

    :cond_63
    return v3

    :cond_64
    move v14, v13

    move v13, v12

    move v12, v8

    move v8, v6

    move v6, v5

    move v5, v3

    goto :goto_35

    :cond_65
    :goto_34
    ushr-int/2addr v5, v13

    sub-int/2addr v6, v13

    sget-object v13, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b:[I

    aget v13, v13, v8

    and-int/2addr v13, v5

    add-int/2addr v12, v13

    ushr-int/2addr v5, v8

    sub-int/2addr v6, v8

    iget v8, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->z:I

    iget v13, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->y:I

    add-int v14, v8, v12

    move/from16 v29, v5

    move v5, v3

    move v3, v14

    move v14, v13

    move v13, v12

    move v12, v8

    move v8, v6

    move/from16 v6, v29

    :goto_35
    if-nez v4, :cond_79

    and-int/lit8 v15, v14, 0x1f

    add-int/lit16 v15, v15, 0x102

    const/16 v18, 0x5

    shr-int/lit8 v14, v14, 0x5

    and-int/lit8 v14, v14, 0x1f

    add-int/2addr v15, v14

    if-gt v3, v15, :cond_78

    const/16 v3, 0x10

    if-nez v4, :cond_68

    if-ne v9, v3, :cond_67

    if-nez v4, :cond_66

    const/4 v14, 0x1

    if-ge v12, v14, :cond_67

    move/from16 v18, v14

    goto/16 :goto_3d

    :cond_66
    move v9, v12

    const/4 v14, 0x1

    goto :goto_36

    :cond_67
    if-nez v4, :cond_6a

    :cond_68
    move v14, v3

    :goto_36
    if-ne v9, v14, :cond_69

    iget-object v9, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->A:[I

    add-int/lit8 v14, v12, -0x1

    aget v9, v9, v14

    goto :goto_37

    :cond_69
    const/4 v9, 0x0

    :cond_6a
    :goto_37
    iget-object v14, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->A:[I

    add-int/lit8 v15, v12, 0x1

    aput v9, v14, v12

    add-int/2addr v13, v7

    if-nez v13, :cond_77

    if-nez v4, :cond_77

    iput v15, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->z:I

    :goto_38
    if-eqz v4, :cond_76

    move v3, v5

    move v5, v6

    move v6, v8

    :goto_39
    iget-object v8, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->C:[I

    const/4 v9, 0x0

    aput v7, v8, v9

    iget-object v7, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->D:[I

    const/16 v8, 0x9

    aput v8, v7, v9

    iget-object v8, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->E:[I

    const/4 v12, 0x6

    aput v12, v8, v9

    iget v9, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->y:I

    iget-object v12, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->T:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;

    and-int/lit8 v13, v9, 0x1f

    add-int/lit16 v13, v13, 0x101

    const/4 v14, 0x5

    shr-int/2addr v9, v14

    and-int/lit8 v9, v9, 0x1f

    const/4 v14, 0x1

    add-int/lit8 v21, v9, 0x1

    iget-object v9, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->A:[I

    iget-object v14, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->H:[I

    iget-object v15, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->I:[I

    iget-object v0, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->N:[I

    move/from16 v17, v1

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    move-object/from16 v19, v12

    move/from16 v20, v13

    move-object/from16 v22, v9

    move-object/from16 v23, v7

    move-object/from16 v24, v8

    move-object/from16 v25, v14

    move-object/from16 v26, v15

    move-object/from16 v27, v0

    move-object/from16 v28, v1

    invoke-virtual/range {v19 .. v28}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->a(II[I[I[I[I[I[ILcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)I

    move-result v0

    if-nez v4, :cond_6e

    if-eqz v0, :cond_6d

    if-nez v4, :cond_6c

    const/4 v1, -0x3

    if-ne v0, v1, :cond_6b

    const/4 v1, 0x0

    iput-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->A:[I

    const/16 v1, 0x9

    iput v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    :cond_6b
    iput v5, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v6, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v5, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v5, v2, v5

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v10, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    invoke-virtual {v11, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v0

    :cond_6c
    return v0

    :cond_6d
    iget-object v0, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->D:[I

    const/4 v7, 0x0

    aget v20, v1, v7

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->E:[I

    aget v21, v1, v7

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->N:[I

    iget-object v8, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->H:[I

    aget v23, v8, v7

    iget-object v8, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->I:[I

    aget v25, v8, v7

    move-object/from16 v19, v0

    move-object/from16 v22, v1

    move-object/from16 v24, v1

    invoke-virtual/range {v19 .. v25}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->a(II[II[II)V

    :cond_6e
    const/4 v0, 0x6

    iput v0, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    move v7, v10

    move-object v10, v11

    move/from16 v1, v17

    :pswitch_7
    iput v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v6, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v8, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v11, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v11, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v11, v2, v11

    int-to-long v11, v11

    add-long/2addr v8, v11

    iput-wide v8, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->a(I)I

    move-result v0

    if-nez v4, :cond_70

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6f

    invoke-virtual {v10, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v0

    return v0

    :cond_6f
    iget-object v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)V

    iget-object v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget v2, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget v3, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget v6, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    move v8, v5

    move v7, v6

    move v5, v2

    move v6, v3

    move v2, v0

    move v3, v1

    move v0, v8

    const/4 v1, 0x0

    goto :goto_3a

    :cond_70
    move v1, v0

    move v8, v7

    const/4 v7, 0x1

    :goto_3a
    if-nez v4, :cond_72

    if-ge v0, v7, :cond_71

    iget v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    sub-int/2addr v0, v8

    const/4 v7, 0x1

    sub-int/2addr v0, v7

    goto :goto_3b

    :cond_71
    iget v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    move v7, v8

    :cond_72
    sub-int/2addr v0, v7

    :goto_3b
    move v9, v0

    if-nez v4, :cond_74

    iget v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->K:I

    if-nez v0, :cond_74

    const/4 v12, 0x0

    iput v12, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    if-eqz v4, :cond_73

    goto :goto_3c

    :cond_73
    move-object/from16 v0, p0

    move v7, v8

    goto/16 :goto_40

    :cond_74
    :goto_3c
    const/4 v7, 0x7

    iput v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    move v7, v8

    :pswitch_8
    iput v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    invoke-virtual {v10, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v0

    iget v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    if-nez v4, :cond_75

    iget v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    iget v4, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    if-eq v1, v4, :cond_75

    iput v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v6, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v5, v2, v5

    int-to-long v5, v5

    add-long/2addr v3, v5

    iput-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    invoke-virtual {v10, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v0

    return v0

    :cond_75
    const/16 v0, 0x8

    iput v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    :pswitch_9
    iput v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v6, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v1, v2, v1

    int-to-long v5, v1

    add-long/2addr v3, v5

    iput-wide v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    const/4 v9, 0x1

    invoke-virtual {v10, v9}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v0

    return v0

    :cond_76
    move-object/from16 v0, p0

    move v3, v5

    move v5, v6

    move v6, v8

    const/4 v7, 0x5

    const/4 v14, 0x0

    goto/16 :goto_2c

    :cond_77
    move-object/from16 v0, p0

    move v12, v15

    goto/16 :goto_37

    :cond_78
    const/16 v18, 0x1

    :goto_3d
    const/4 v0, 0x0

    iput-object v0, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->A:[I

    const/16 v0, 0x9

    iput v0, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    iget-object v0, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->V:[Ljava/lang/String;

    aget-object v1, v1, v18

    iput-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    iput v6, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v8, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v0, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v0, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v1, v2, v1

    int-to-long v5, v1

    add-long/2addr v3, v5

    iput-wide v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v0, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v10, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    const/4 v0, -0x3

    invoke-virtual {v11, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v3

    :cond_79
    return v3

    :cond_7a
    :goto_3e
    const/16 v0, 0x9

    iput v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    iget-object v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->V:[Ljava/lang/String;

    const/4 v4, 0x3

    aget-object v1, v1, v4

    iput-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    iput v5, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v6, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v1, v2, v1

    int-to-long v5, v1

    add-long/2addr v3, v5

    iput-wide v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v0, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v7, v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    const/4 v0, -0x3

    invoke-virtual {v10, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v11

    :cond_7b
    return v11

    :goto_3f
    move-object/from16 v0, p0

    move/from16 v8, v18

    goto/16 :goto_2

    :cond_7c
    move-object/from16 v0, p0

    const/4 v8, 0x1

    move/from16 v29, v5

    move v5, v3

    move/from16 v3, v29

    goto/16 :goto_2

    :cond_7d
    move-object/from16 v0, p0

    move v3, v5

    move v5, v6

    move v6, v11

    :goto_40
    const/4 v8, 0x1

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
    .end packed-switch
.end method

.method a()V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    if-nez v0, :cond_1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    invoke-virtual {v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)V

    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    if-nez v0, :cond_2

    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->S:Z

    :cond_1
    if-eqz v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->reset()V

    :cond_3
    return-void
.end method

.method a([BII)V
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    return-void
.end method

.method b(I)I
    .locals 12

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOutIndex:I

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v1

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    if-nez v1, :cond_1

    iget v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    if-gt v2, v3, :cond_0

    goto :goto_0

    :cond_0
    iget v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    goto :goto_0

    :cond_1
    move v3, v2

    :goto_0
    sub-int/2addr v3, v2

    if-nez v1, :cond_2

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    if-le v3, v4, :cond_2

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    :cond_2
    const/4 v4, -0x5

    const/4 v5, 0x0

    if-nez v1, :cond_5

    if-eqz v3, :cond_4

    if-nez v1, :cond_3

    if-ne p1, v4, :cond_4

    move p1, v5

    goto :goto_1

    :cond_3
    move v6, p1

    goto :goto_2

    :cond_4
    :goto_1
    iget-object v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v7, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    sub-int/2addr v7, v3

    iput v7, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    iget-object v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v7, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalOut:J

    int-to-long v9, v3

    add-long/2addr v7, v9

    iput-wide v7, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalOut:J

    iget-boolean v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->S:Z

    move v11, v6

    move v6, p1

    move p1, v11

    goto :goto_2

    :cond_5
    move v6, p1

    move p1, v3

    :goto_2
    if-nez v1, :cond_8

    if-eqz p1, :cond_7

    if-nez v1, :cond_6

    if-lez v3, :cond_7

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    iget-object v7, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    invoke-interface {p1, v7, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->update([BII)V

    goto :goto_3

    :cond_6
    move p1, v3

    goto :goto_4

    :cond_7
    :goto_3
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    iget-object v7, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B

    invoke-static {p1, v2, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v0, v3

    add-int/2addr v2, v3

    move p1, v2

    :cond_8
    :goto_4
    if-nez v1, :cond_13

    iget v7, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    if-ne p1, v7, :cond_12

    iget p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    if-nez v1, :cond_a

    if-ne p1, v7, :cond_9

    iput v5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    :cond_9
    iget p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    add-int/lit8 v3, p1, 0x0

    if-nez v1, :cond_b

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v7, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    move p1, v3

    :cond_a
    if-le p1, v7, :cond_b

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    move v3, p1

    :cond_b
    if-nez v1, :cond_e

    if-eqz v3, :cond_d

    if-nez v1, :cond_c

    if-ne v6, v4, :cond_d

    move v6, v5

    goto :goto_5

    :cond_c
    move p1, v6

    goto :goto_6

    :cond_d
    :goto_5
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v2, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    sub-int/2addr v2, v3

    iput v2, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v7, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalOut:J

    int-to-long v9, v3

    add-long/2addr v7, v9

    iput-wide v7, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalOut:J

    iget-boolean p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->S:Z

    move v11, v6

    move v6, p1

    move p1, v11

    goto :goto_6

    :cond_e
    move p1, v6

    move v6, v3

    :goto_6
    if-nez v1, :cond_11

    if-eqz v6, :cond_10

    if-nez v1, :cond_f

    if-lez v3, :cond_10

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    invoke-interface {v1, v2, v5, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->update([BII)V

    goto :goto_7

    :cond_f
    move v2, v3

    goto :goto_8

    :cond_10
    :goto_7
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B

    invoke-static {v1, v5, v2, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v0, v3

    add-int/2addr v5, v3

    move v2, v5

    goto :goto_8

    :cond_11
    move v2, v6

    :goto_8
    move v6, p1

    :cond_12
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->U:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v0, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOutIndex:I

    iput v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    move p1, v6

    :cond_13
    return p1
.end method

.method b()V
    .locals 1

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->N:[I

    return-void
.end method

.method c()I
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->w:I

    const/4 v2, 0x1

    if-nez v0, :cond_1

    if-ne v1, v2, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method
