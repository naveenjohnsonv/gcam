.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;
.super Ljava/lang/Object;


# static fields
.field public static final ALL:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

.field public static final FIPS:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

.field public static final HMAC_MD5:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

.field public static final HMAC_MD5_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

.field public static final HMAC_SHA1:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

.field public static final HMAC_SHA1_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

.field public static final HMAC_SHA2_256:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

.field public static final HMAC_SHA2_256_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

.field public static final HMAC_SHA2_512:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

.field public static final HMAC_SHA2_512_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;


# direct methods
.method static constructor <clinit>()V
    .locals 30

    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "?N\u0011[<k66\u0012\u0008\u001fN\u0011[Bp?f\n\u001fn1{BP\u001fb\u0012B\u0010?N\u0011[<k66\u0011]\n$.sn\u0015\u000b?N\u0011[<u:b\u000eI\u000e\n\u001fn1{BP\u001fe\u0016F\u0008?N\u0011[<u:b\u0007\u001fN\u0011[\\\\k\r?N\u0011[<k66\u0011]\n$.\u0010?N\u0011[<k66\u0011]\r *sn\u0015\n\u001fn1{BP\u001fb\u0012B\u0008\u001fN\u0011[Bp?f\n\u001fn1{BP\u001fe\u0016F\u0007\u001fN\u0011[\\\\k"

    const/16 v5, 0x9c

    move v8, v3

    const/4 v6, -0x1

    const/16 v7, 0x9

    :goto_0
    const/16 v9, 0x12

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v14, v11

    move v15, v3

    :goto_2
    const/16 v16, 0xa

    const/4 v2, 0x5

    const/4 v12, 0x4

    const/4 v1, 0x3

    if-gt v14, v15, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    const/16 v11, 0xd

    if-eqz v13, :cond_1

    add-int/lit8 v1, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v1

    goto :goto_0

    :cond_0
    const-string v4, "7F\u0019S4c>>\u0019U\u0005(\"\u000c7F\u0019S4c>>\u001aU\t/"

    move v8, v1

    move v7, v11

    const/16 v5, 0x1a

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v13, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v1

    move v8, v13

    :goto_3
    add-int/2addr v6, v10

    add-int v1, v6, v7

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v13, v3

    const/16 v9, 0x1a

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    aget-object v19, v0, v3

    aget-object v20, v0, v10

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v21

    const/16 v22, 0x14

    const/16 v23, 0x14

    move-object/from16 v18, v4

    invoke-direct/range {v18 .. v23}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA1:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/16 v5, 0xf

    aget-object v25, v0, v5

    const/16 v5, 0xb

    aget-object v26, v0, v5

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v28, 0xc

    const/16 v29, 0x14

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA1_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v5, 0x6

    aget-object v19, v0, v5

    const/4 v6, 0x7

    aget-object v20, v0, v6

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v21

    const/16 v22, 0x10

    const/16 v23, 0x10

    move-object/from16 v18, v4

    invoke-direct/range {v18 .. v23}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_MD5:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    aget-object v25, v0, v12

    aget-object v26, v0, v11

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v29, 0x10

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_MD5_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/16 v7, 0x8

    aget-object v19, v0, v7

    const/16 v8, 0xc

    aget-object v20, v0, v8

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v21

    const/16 v22, 0x20

    const/16 v23, 0x20

    move-object/from16 v18, v4

    invoke-direct/range {v18 .. v23}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_256:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    aget-object v25, v0, v1

    aget-object v26, v0, v2

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v29, 0x20

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_256_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/16 v8, 0xe

    aget-object v19, v0, v8

    const/4 v8, 0x2

    aget-object v20, v0, v8

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v21

    const/16 v22, 0x40

    const/16 v23, 0x40

    move-object/from16 v18, v4

    invoke-direct/range {v18 .. v23}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_512:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/16 v17, 0x9

    aget-object v25, v0, v17

    aget-object v26, v0, v16

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v29, 0x40

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_512_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    new-array v0, v7, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA1:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    aput-object v7, v0, v3

    sget-object v8, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA1_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    aput-object v8, v0, v10

    sget-object v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_MD5:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v11, 0x2

    aput-object v9, v0, v11

    sget-object v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_MD5_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    aput-object v9, v0, v1

    sget-object v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_256:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    aput-object v9, v0, v12

    sget-object v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_256_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    aput-object v11, v0, v2

    sget-object v13, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_512:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    aput-object v13, v0, v5

    aput-object v4, v0, v6

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->ALL:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    new-array v0, v5, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    aput-object v7, v0, v3

    aput-object v8, v0, v10

    const/4 v3, 0x2

    aput-object v9, v0, v3

    aput-object v11, v0, v1

    aput-object v13, v0, v12

    aput-object v4, v0, v2

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->FIPS:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    return-void

    :cond_3
    const/16 v17, 0x9

    aget-char v18, v11, v15

    rem-int/lit8 v3, v15, 0x7

    if-eqz v3, :cond_8

    if-eq v3, v10, :cond_7

    const/4 v10, 0x2

    if-eq v3, v10, :cond_6

    if-eq v3, v1, :cond_5

    if-eq v3, v12, :cond_4

    if-eq v3, v2, :cond_9

    const/16 v16, 0x4c

    goto :goto_4

    :cond_4
    move/from16 v16, v1

    goto :goto_4

    :cond_5
    const/16 v16, 0x2a

    goto :goto_4

    :cond_6
    const/16 v16, 0x62

    goto :goto_4

    :cond_7
    const/16 v16, 0x31

    goto :goto_4

    :cond_8
    const/16 v16, 0x45

    :cond_9
    :goto_4
    xor-int v1, v9, v16

    xor-int v1, v18, v1

    int-to-char v1, v1

    aput-char v1, v11, v15

    add-int/lit8 v15, v15, 0x1

    const/4 v3, 0x0

    const/4 v10, 0x1

    goto/16 :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->b()Z

    move-result v0

    array-length v1, p2

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v1, :cond_2

    aget-object v3, p2, v2

    invoke-virtual {v3, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;->update(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object v3

    if-nez v0, :cond_1

    add-int/lit8 v2, v2, 0x1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_1
    move-object p0, v3

    :cond_2
    :goto_0
    return-object p0
.end method

.method public static varargs init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 4

    array-length v0, p2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->c()Z

    move-result v1

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v0, :cond_2

    aget-object v3, p2, v2

    invoke-virtual {v3, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;->update(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object v3

    if-eqz v1, :cond_1

    add-int/lit8 v2, v2, 0x1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_1
    move-object p0, v3

    :cond_2
    :goto_0
    return-object p0
.end method

.method public static varargs init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAll(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->initRequired(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->initRecommended(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->initOptional(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAllClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->initAllClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAllClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 5

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_MD5:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_256_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_256:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v4, 0x2

    aput-object v1, v0, v4

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_512_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v4, 0x3

    aput-object v1, v0, v4

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_512:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v4, 0x4

    aput-object v1, v0, v4

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA1_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v4, 0x5

    aput-object v1, v0, v4

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA1:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v4, 0x6

    aput-object v1, v0, v4

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_MD5_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v4, 0x7

    aput-object v1, v0, v4

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    new-array p1, v3, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

    aput-object v0, p1, v2

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAllClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 5

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_MD5:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_256_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_256:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v4, 0x2

    aput-object v1, v0, v4

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_512_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v4, 0x3

    aput-object v1, v0, v4

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_512:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v4, 0x4

    aput-object v1, v0, v4

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA1_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v4, 0x5

    aput-object v1, v0, v4

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA1:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v4, 0x6

    aput-object v1, v0, v4

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_MD5_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v4, 0x7

    aput-object v1, v0, v4

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    new-array p1, v3, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;->NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

    aput-object v0, p1, v2

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initFipsClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 4

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    const/4 v1, 0x6

    new-array v1, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_256:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_256_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_512:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_512_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA1:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA1_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    invoke-static {p0, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initOptional(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->initOptional(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initOptional(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_256_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_512:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_512_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_MD5:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_MD5_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initOptional(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_256_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_512:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_512_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_MD5:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_MD5_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRecommended(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->initRecommended(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRecommended(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_256:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA1_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRecommended(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA2_256:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA1_96:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRequired(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->initRequired(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRequired(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA1:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRequired(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->HMAC_SHA1:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object p0

    return-object p0
.end method
