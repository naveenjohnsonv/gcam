.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;
.super Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;


# static fields
.field private static final f:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x1a

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x52

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\'(q^nK#&>0[gKf.)i\u001a|Kj;9uT%\u001f,%uY`Jv\"myI+Wl;ms[gZv#,d_o\u0019z*9>"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x3a

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->f:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x51

    goto :goto_2

    :cond_2
    const/16 v12, 0x6b

    goto :goto_2

    :cond_3
    const/16 v12, 0x59

    goto :goto_2

    :cond_4
    const/16 v12, 0x68

    goto :goto_2

    :cond_5
    const/16 v12, 0x42

    goto :goto_2

    :cond_6
    const/16 v12, 0x1f

    goto :goto_2

    :cond_7
    const/16 v12, 0x1d

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x200

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;-><init>(Ljava/io/OutputStream;I)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;-><init>(Ljava/io/OutputStream;IZ)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;IZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    const/4 v1, -0x1

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;-><init>(II)V

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;-><init>(Ljava/io/OutputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;IZ)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->mydeflater:Z

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;IZ)V

    return-void
.end method

.method private static a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iget v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->af:I

    const/16 v1, 0x2a

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->f:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public getCRC()J
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    if-nez v0, :cond_1

    iget v0, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->af:I
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0x29a

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->f:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->l()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->getCRC()J

    move-result-wide v0

    return-wide v0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;

    move-result-object v0

    throw v0
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->a()V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->l()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->setComment(Ljava/lang/String;)V

    return-void
.end method

.method public setModifiedTime(J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->a()V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->l()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->setModifiedTime(J)V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->a()V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->l()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->setName(Ljava/lang/String;)V

    return-void
.end method

.method public setOS(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->a()V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->l()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->setOS(I)V

    return-void
.end method
