.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x17

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x1

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    const-string v7, "1<V\u0007j\n\u000f\u0016&@\u0016:\u001b\u000c\u0003=W\u001bn\u0012\r^r\u0019\'=H\u0002u\u0014\u0005\n&v\u001b}\u0014\u0005\u0016r^\u0017t\u000e\u0012\r7VO"

    invoke-virtual {v7, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v8, v4

    move v9, v2

    :goto_1
    if-gt v8, v9, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x31

    if-ge v3, v4, :cond_0

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->b:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v10, v4, v9

    rem-int/lit8 v11, v9, 0x7

    const/4 v12, 0x3

    if-eqz v11, :cond_7

    if-eq v11, v6, :cond_6

    if-eq v11, v0, :cond_5

    if-eq v11, v12, :cond_4

    const/4 v13, 0x4

    if-eq v11, v13, :cond_3

    const/4 v13, 0x5

    if-eq v11, v13, :cond_2

    const/16 v11, 0x63

    goto :goto_2

    :cond_2
    const/16 v11, 0x79

    goto :goto_2

    :cond_3
    const/16 v11, 0x19

    goto :goto_2

    :cond_4
    const/16 v11, 0x71

    goto :goto_2

    :cond_5
    const/16 v11, 0x26

    goto :goto_2

    :cond_6
    const/16 v11, 0x51

    goto :goto_2

    :cond_7
    const/16 v11, 0x67

    :goto_2
    xor-int/2addr v11, v12

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v4, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public varargs constructor <init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->a:Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;->b()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->a:Ljava/util/Map;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->b:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public assertSignatureValid([BLjava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-interface {p2}, Ljava/security/PublicKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->a(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    move-result-object v0

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;->signer:Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;

    invoke-interface {v0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;->assertSignatureValid([BLjava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V

    return-void
.end method

.method public varargs set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;
    .locals 6

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->a:Ljava/util/Map;

    iget-object v4, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;->algorithm:Ljava/lang/String;

    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public sign([BLjava/security/PrivateKey;)Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-interface {p2}, Ljava/security/PrivateKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->a(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    move-result-object v0

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;->signer:Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;

    invoke-interface {v0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;->sign([BLjava/security/PrivateKey;)Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->b:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->a:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
