.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;
.super Ljava/lang/Object;


# static fields
.field private static final a:I = 0xf

.field private static final b:I = 0x13

.field private static final c:I = 0x1e

.field private static final d:I = 0x100

.field private static final e:I = 0x1d

.field private static final f:I = 0x11e

.field private static final g:I = 0x23d

.field static final h:I = 0x7

.field static final i:I = 0x100

.field static final j:I = 0x10

.field static final k:I = 0x11

.field static final l:I = 0x12

.field static final m:[I

.field static final n:[I

.field static final o:[I

.field static final p:[B

.field static final q:I = 0x10

.field static final r:I = 0x200

.field static final s:[B

.field static final t:[B

.field static final u:[I

.field static final v:[I


# instance fields
.field w:[S

.field x:I

.field y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x1d

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->m:[I

    const/16 v1, 0x1e

    new-array v2, v1, [I

    fill-array-data v2, :array_1

    sput-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->n:[I

    const/16 v2, 0x13

    new-array v3, v2, [I

    fill-array-data v3, :array_2

    sput-object v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->o:[I

    new-array v2, v2, [B

    fill-array-data v2, :array_3

    sput-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->p:[B

    const/16 v2, 0x200

    new-array v2, v2, [B

    fill-array-data v2, :array_4

    sput-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->s:[B

    const/16 v2, 0x100

    new-array v2, v2, [B

    fill-array-data v2, :array_5

    sput-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->t:[B

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->u:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->v:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x1
        0x1
        0x1
        0x1
        0x2
        0x2
        0x2
        0x2
        0x3
        0x3
        0x3
        0x3
        0x4
        0x4
        0x4
        0x4
        0x5
        0x5
        0x5
        0x5
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x1
        0x1
        0x2
        0x2
        0x3
        0x3
        0x4
        0x4
        0x5
        0x5
        0x6
        0x6
        0x7
        0x7
        0x8
        0x8
        0x9
        0x9
        0xa
        0xa
        0xb
        0xb
        0xc
        0xc
        0xd
        0xd
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x2
        0x3
        0x7
    .end array-data

    :array_3
    .array-data 1
        0x10t
        0x11t
        0x12t
        0x0t
        0x8t
        0x7t
        0x9t
        0x6t
        0xat
        0x5t
        0xbt
        0x4t
        0xct
        0x3t
        0xdt
        0x2t
        0xet
        0x1t
        0xft
    .end array-data

    :array_4
    .array-data 1
        0x0t
        0x1t
        0x2t
        0x3t
        0x4t
        0x4t
        0x5t
        0x5t
        0x6t
        0x6t
        0x6t
        0x6t
        0x7t
        0x7t
        0x7t
        0x7t
        0x8t
        0x8t
        0x8t
        0x8t
        0x8t
        0x8t
        0x8t
        0x8t
        0x9t
        0x9t
        0x9t
        0x9t
        0x9t
        0x9t
        0x9t
        0x9t
        0xat
        0xat
        0xat
        0xat
        0xat
        0xat
        0xat
        0xat
        0xat
        0xat
        0xat
        0xat
        0xat
        0xat
        0xat
        0xat
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xbt
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xct
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xdt
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xet
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0xft
        0x0t
        0x0t
        0x10t
        0x11t
        0x12t
        0x12t
        0x13t
        0x13t
        0x14t
        0x14t
        0x14t
        0x14t
        0x15t
        0x15t
        0x15t
        0x15t
        0x16t
        0x16t
        0x16t
        0x16t
        0x16t
        0x16t
        0x16t
        0x16t
        0x17t
        0x17t
        0x17t
        0x17t
        0x17t
        0x17t
        0x17t
        0x17t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1ct
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
        0x1dt
    .end array-data

    :array_5
    .array-data 1
        0x0t
        0x1t
        0x2t
        0x3t
        0x4t
        0x5t
        0x6t
        0x7t
        0x8t
        0x8t
        0x9t
        0x9t
        0xat
        0xat
        0xbt
        0xbt
        0xct
        0xct
        0xct
        0xct
        0xdt
        0xdt
        0xdt
        0xdt
        0xet
        0xet
        0xet
        0xet
        0xft
        0xft
        0xft
        0xft
        0x10t
        0x10t
        0x10t
        0x10t
        0x10t
        0x10t
        0x10t
        0x10t
        0x11t
        0x11t
        0x11t
        0x11t
        0x11t
        0x11t
        0x11t
        0x11t
        0x12t
        0x12t
        0x12t
        0x12t
        0x12t
        0x12t
        0x12t
        0x12t
        0x13t
        0x13t
        0x13t
        0x13t
        0x13t
        0x13t
        0x13t
        0x13t
        0x14t
        0x14t
        0x14t
        0x14t
        0x14t
        0x14t
        0x14t
        0x14t
        0x14t
        0x14t
        0x14t
        0x14t
        0x14t
        0x14t
        0x14t
        0x14t
        0x15t
        0x15t
        0x15t
        0x15t
        0x15t
        0x15t
        0x15t
        0x15t
        0x15t
        0x15t
        0x15t
        0x15t
        0x15t
        0x15t
        0x15t
        0x15t
        0x16t
        0x16t
        0x16t
        0x16t
        0x16t
        0x16t
        0x16t
        0x16t
        0x16t
        0x16t
        0x16t
        0x16t
        0x16t
        0x16t
        0x16t
        0x16t
        0x17t
        0x17t
        0x17t
        0x17t
        0x17t
        0x17t
        0x17t
        0x17t
        0x17t
        0x17t
        0x17t
        0x17t
        0x17t
        0x17t
        0x17t
        0x17t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x18t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x19t
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1at
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1bt
        0x1ct
    .end array-data

    :array_6
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0xa
        0xc
        0xe
        0x10
        0x14
        0x18
        0x1c
        0x20
        0x28
        0x30
        0x38
        0x40
        0x50
        0x60
        0x70
        0x80
        0xa0
        0xc0
        0xe0
        0x0
    .end array-data

    :array_7
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
        0x6
        0x8
        0xc
        0x10
        0x18
        0x20
        0x30
        0x40
        0x60
        0x80
        0xc0
        0x100
        0x180
        0x200
        0x300
        0x400
        0x600
        0x800
        0xc00
        0x1000
        0x1800
        0x2000
        0x3000
        0x4000
        0x6000
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(I)I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_1

    const/16 v0, 0x100

    if-ge p0, v0, :cond_0

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->s:[B

    aget-byte p0, v0, p0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->s:[B

    ushr-int/lit8 p0, p0, 0x7

    add-int/2addr p0, v0

    aget-byte p0, v1, p0

    :cond_1
    :goto_0
    return p0
.end method

.method private static final a(II)I
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    and-int/lit8 v2, p0, 0x1

    or-int/2addr v1, v2

    ushr-int/lit8 p0, p0, 0x1

    shl-int/lit8 v1, v1, 0x1

    add-int/lit8 p1, p1, -0x1

    move v2, p1

    :cond_1
    if-gtz v2, :cond_0

    ushr-int/lit8 v2, v1, 0x1

    if-nez v0, :cond_1

    return v2
.end method

.method private static final a([SI[S[S)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const/4 v1, 0x0

    aput-short v1, p3, v1

    const/4 v2, 0x1

    move v4, v1

    move v3, v2

    :cond_0
    const/16 v5, 0xf

    if-gt v3, v5, :cond_1

    add-int/lit8 v5, v3, -0x1

    aget-short v5, p2, v5

    add-int/2addr v4, v5

    shl-int/2addr v4, v2

    int-to-short v4, v4

    aput-short v4, p3, v3

    add-int/lit8 v3, v3, 0x1

    if-eqz v0, :cond_0

    :cond_1
    if-gt v1, p1, :cond_3

    mul-int/lit8 p2, v1, 0x2

    add-int/lit8 v2, p2, 0x1

    aget-short v2, p0, v2

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    aget-short v3, p3, v2

    add-int/lit8 v4, v3, 0x1

    int-to-short v4, v4

    aput-short v4, p3, v2

    invoke-static {v3, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->a(II)I

    move-result v2

    int-to-short v2, v2

    aput-short v2, p0, p2

    :goto_0
    add-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_1

    :cond_3
    return-void
.end method


# virtual methods
.method a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;)V
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->w:[S

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v3

    iget-object v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;

    iget-object v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;->m:[S

    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;

    iget-object v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;->n:[I

    iget-object v6, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;

    iget v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;->o:I

    iget-object v7, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;

    iget v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;->q:I

    const/4 v8, 0x0

    move v9, v8

    :cond_0
    const/16 v10, 0xf

    const/4 v11, 0x1

    if-gt v9, v10, :cond_1

    iget-object v10, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aU:[S

    aput-short v8, v10, v9

    add-int/lit8 v9, v9, 0x1

    if-nez v3, :cond_2

    if-eqz v3, :cond_0

    :cond_1
    iget-object v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    iget v10, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aY:I

    aget v9, v9, v10

    mul-int/lit8 v9, v9, 0x2

    add-int/2addr v9, v11

    aput-short v8, v2, v9

    :cond_2
    iget v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aY:I

    add-int/2addr v9, v11

    move v10, v8

    :goto_0
    const/16 v12, 0x23d

    if-ge v9, v12, :cond_d

    iget-object v12, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    aget v12, v12, v9

    mul-int/lit8 v13, v12, 0x2

    add-int/lit8 v14, v13, 0x1

    aget-short v15, v2, v14

    mul-int/lit8 v15, v15, 0x2

    add-int/2addr v15, v11

    aget-short v15, v2, v15

    add-int/2addr v15, v11

    if-nez v3, :cond_c

    if-nez v3, :cond_5

    if-le v15, v7, :cond_3

    add-int/lit8 v10, v10, 0x1

    move v15, v7

    :cond_3
    int-to-short v8, v15

    aput-short v8, v2, v14

    if-nez v3, :cond_4

    iget v8, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->x:I

    move/from16 v16, v15

    move v15, v12

    goto :goto_1

    :cond_4
    move v8, v12

    goto :goto_2

    :cond_5
    move v8, v7

    move/from16 v16, v15

    :goto_1
    if-le v15, v8, :cond_6

    goto :goto_3

    :cond_6
    iget-object v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aU:[S

    aget-short v15, v8, v16

    add-int/2addr v15, v11

    int-to-short v15, v15

    aput-short v15, v8, v16

    move/from16 v15, v16

    const/4 v8, 0x0

    :goto_2
    if-nez v3, :cond_8

    if-lt v12, v6, :cond_7

    sub-int/2addr v12, v6

    aget v8, v5, v12

    :cond_7
    aget-short v12, v2, v13

    :cond_8
    iget v13, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a4:I

    add-int/2addr v15, v8

    mul-int/2addr v15, v12

    add-int/2addr v13, v15

    iput v13, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a4:I

    if-nez v3, :cond_a

    if-eqz v4, :cond_9

    iget v13, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a5:I

    aget-short v14, v4, v14

    add-int/2addr v14, v8

    mul-int/2addr v12, v14

    add-int/2addr v13, v12

    iput v13, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a5:I

    :cond_9
    :goto_3
    add-int/lit8 v9, v9, 0x1

    :cond_a
    if-eqz v3, :cond_b

    goto :goto_4

    :cond_b
    const/4 v8, 0x0

    goto :goto_0

    :cond_c
    move-object v5, v0

    move v6, v7

    move v4, v10

    goto/16 :goto_e

    :cond_d
    :goto_4
    if-nez v3, :cond_f

    if-nez v10, :cond_e

    return-void

    :cond_e
    move-object v5, v0

    goto/16 :goto_d

    :cond_f
    move-object v5, v0

    move v4, v10

    :cond_10
    :goto_5
    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aU:[S

    aget-short v6, v6, v10

    :goto_6
    if-nez v6, :cond_11

    add-int/lit8 v10, v10, -0x1

    if-nez v3, :cond_12

    if-eqz v3, :cond_10

    :cond_11
    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aU:[S

    aget-short v8, v6, v10

    sub-int/2addr v8, v11

    int-to-short v8, v8

    aput-short v8, v6, v10

    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aU:[S

    add-int/lit8 v8, v10, 0x1

    aget-short v12, v6, v8

    add-int/lit8 v12, v12, 0x2

    int-to-short v12, v12

    aput-short v12, v6, v8

    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aU:[S

    aget-short v8, v6, v7

    sub-int/2addr v8, v11

    int-to-short v8, v8

    aput-short v8, v6, v7

    add-int/lit8 v4, v4, -0x2

    :cond_12
    if-gtz v4, :cond_1d

    if-nez v3, :cond_1c

    :goto_7
    move v4, v7

    :goto_8
    if-eqz v4, :cond_1b

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aU:[S

    aget-short v4, v4, v7

    :goto_9
    if-eqz v4, :cond_19

    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    add-int/lit8 v9, v9, -0x1

    aget v6, v6, v9

    if-nez v3, :cond_18

    iget v8, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->x:I

    if-nez v3, :cond_14

    if-le v6, v8, :cond_13

    goto :goto_9

    :cond_13
    mul-int/lit8 v8, v6, 0x2

    add-int/2addr v8, v11

    if-nez v3, :cond_15

    aget-short v8, v2, v8

    move v10, v7

    goto :goto_a

    :cond_14
    move v10, v8

    move v8, v6

    :goto_a
    if-eq v8, v10, :cond_16

    iget v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a4:I

    int-to-long v12, v8

    int-to-long v14, v7

    mul-int/lit8 v6, v6, 0x2

    add-int/lit8 v8, v6, 0x1

    aget-short v10, v2, v8

    move-wide/from16 v17, v12

    int-to-long v11, v10

    sub-long/2addr v14, v11

    aget-short v6, v2, v6

    int-to-long v10, v6

    mul-long/2addr v14, v10

    add-long v12, v17, v14

    long-to-int v6, v12

    iput v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a4:I

    :cond_15
    int-to-short v6, v7

    aput-short v6, v2, v8

    :cond_16
    add-int/lit8 v4, v4, -0x1

    if-eqz v3, :cond_17

    goto :goto_b

    :cond_17
    const/4 v11, 0x1

    goto :goto_9

    :cond_18
    move v4, v6

    goto :goto_8

    :cond_19
    :goto_b
    add-int/lit8 v7, v7, -0x1

    if-eqz v3, :cond_1a

    goto :goto_c

    :cond_1a
    const/4 v11, 0x1

    goto :goto_7

    :cond_1b
    :goto_c
    return-void

    :cond_1c
    move v6, v7

    goto :goto_6

    :cond_1d
    move v10, v4

    :goto_d
    move v6, v7

    move v15, v6

    move v4, v10

    const/4 v7, 0x1

    :goto_e
    sub-int v10, v15, v7

    move v7, v6

    const/4 v11, 0x1

    goto/16 :goto_5
.end method

.method b(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;)V
    .locals 14

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->w:[S

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;->m:[S

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;

    iget v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/StaticTree;->p:I

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v3

    const/4 v4, 0x0

    iput v4, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aX:I

    const/16 v5, 0x23d

    iput v5, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aY:I

    const/4 v5, -0x1

    move v6, v4

    :cond_0
    const/4 v7, 0x1

    if-ge v6, v2, :cond_5

    mul-int/lit8 v8, v6, 0x2

    if-nez v3, :cond_4

    aget-short v9, v0, v8

    if-nez v3, :cond_3

    if-eqz v9, :cond_2

    iget-object v5, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    iget v9, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aX:I

    add-int/2addr v9, v7

    iput v9, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aX:I

    aput v6, v5, v9

    iget-object v5, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aZ:[B

    aput-byte v4, v5, v6

    if-eqz v3, :cond_1

    new-array v5, v7, [I

    invoke-static {v5}, Lcom/jscape/util/aq;->b([I)V

    move v5, v6

    goto :goto_0

    :cond_1
    move v5, v6

    goto :goto_2

    :cond_2
    :goto_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_3
    move-object v6, p0

    goto :goto_4

    :cond_4
    :goto_1
    aput-short v4, v0, v8

    :goto_2
    add-int/lit8 v6, v6, 0x1

    if-eqz v3, :cond_0

    :cond_5
    move-object v6, p0

    :goto_3
    iget v9, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aX:I

    :goto_4
    const/4 v8, 0x2

    if-ge v9, v8, :cond_a

    if-nez v3, :cond_b

    iget-object v9, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    iget v10, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aX:I

    add-int/2addr v10, v7

    iput v10, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aX:I

    if-nez v3, :cond_7

    if-ge v5, v8, :cond_6

    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_6
    move v11, v5

    move v5, v4

    goto :goto_6

    :cond_7
    :goto_5
    move v11, v5

    :goto_6
    aput v5, v9, v10

    mul-int/lit8 v9, v5, 0x2

    aput-short v7, v0, v9

    iget-object v10, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aZ:[B

    aput-byte v4, v10, v5

    iget v5, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a4:I

    sub-int/2addr v5, v7

    iput v5, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a4:I

    if-nez v3, :cond_8

    if-eqz v1, :cond_9

    iget v5, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a5:I

    add-int/lit8 v9, v9, 0x1

    aget-short v9, v1, v9

    sub-int/2addr v5, v9

    iput v5, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a5:I

    :cond_8
    if-eqz v3, :cond_9

    move v5, v11

    goto :goto_7

    :cond_9
    move v5, v11

    goto :goto_3

    :cond_a
    :goto_7
    iput v5, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->x:I

    :cond_b
    iget v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aX:I

    div-int/2addr v1, v8

    :cond_c
    if-lt v1, v7, :cond_d

    invoke-virtual {p1, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([SI)V

    add-int/lit8 v1, v1, -0x1

    if-eqz v3, :cond_c

    :cond_d
    :goto_8
    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    aget v1, v1, v7

    iget-object v4, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    iget-object v9, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    iget v10, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aX:I

    add-int/lit8 v11, v10, -0x1

    iput v11, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aX:I

    aget v9, v9, v10

    aput v9, v4, v7

    invoke-virtual {p1, v0, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([SI)V

    iget-object v4, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    aget v4, v4, v7

    iget-object v9, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    iget v10, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aY:I

    sub-int/2addr v10, v7

    iput v10, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aY:I

    aput v1, v9, v10

    iget-object v9, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    iget v10, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aY:I

    sub-int/2addr v10, v7

    iput v10, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aY:I

    aput v4, v9, v10

    mul-int/lit8 v9, v2, 0x2

    mul-int/lit8 v10, v1, 0x2

    aget-short v11, v0, v10

    mul-int/lit8 v12, v4, 0x2

    aget-short v13, v0, v12

    add-int/2addr v11, v13

    int-to-short v11, v11

    aput-short v11, v0, v9

    iget-object v9, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aZ:[B

    iget-object v11, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aZ:[B

    aget-byte v1, v11, v1

    iget-object v11, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aZ:[B

    aget-byte v4, v11, v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/2addr v1, v7

    int-to-byte v1, v1

    aput-byte v1, v9, v2

    add-int/2addr v10, v7

    add-int/2addr v12, v7

    int-to-short v1, v2

    aput-short v1, v0, v12

    aput-short v1, v0, v10

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    add-int/lit8 v4, v2, 0x1

    aput v2, v1, v7

    invoke-virtual {p1, v0, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([SI)V

    :cond_e
    iget v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aX:I

    if-ge v1, v8, :cond_f

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    iget v2, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aY:I

    sub-int/2addr v2, v7

    iput v2, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aY:I

    iget-object v9, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aW:[I

    aget v9, v9, v7

    aput v9, v1, v2

    invoke-virtual {v6, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;)V

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aU:[S

    iget-object v2, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aV:[S

    invoke-static {v0, v5, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Tree;->a([SI[S[S)V

    if-nez v3, :cond_e

    return-void

    :cond_f
    move v2, v4

    goto :goto_8
.end method
