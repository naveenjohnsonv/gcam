.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthFailureCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/ssh/protocol/messages/Message;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/util/h/a/a;->a(Ljava/io/InputStream;)Z

    move-result p1

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthFailure;

    invoke-direct {v1, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthFailure;-><init>(Ljava/util/List;Z)V

    return-object v1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthFailureCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthFailure;

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthFailure;->authenticationsThatCanContinue:Ljava/util/List;

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->writeUsAsciiValue(Ljava/util/List;Ljava/io/OutputStream;)V

    iget-boolean p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthFailure;->partialSuccess:Z

    invoke-static {p1, p2}, Lcom/jscape/util/h/a/a;->a(ZLjava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthFailureCodec;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
