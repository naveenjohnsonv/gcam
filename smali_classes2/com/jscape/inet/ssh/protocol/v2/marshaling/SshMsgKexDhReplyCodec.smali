.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhReplyCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/ssh/protocol/messages/Message;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "Ljava/security/PublicKey;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/jscape/util/h/I;Lcom/jscape/util/h/I;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/h/I<",
            "Ljava/security/PublicKey;",
            ">;",
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhReplyCodec;->a:Lcom/jscape/util/h/I;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhReplyCodec;->b:Lcom/jscape/util/h/I;

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readValue(Ljava/io/InputStream;)[B

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhReplyCodec;->a:Lcom/jscape/util/h/I;

    new-instance v2, Lcom/jscape/util/h/e;

    invoke-direct {v2, v0}, Lcom/jscape/util/h/e;-><init>([B)V

    invoke-interface {v1, v2}, Lcom/jscape/util/h/I;->read(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/security/PublicKey;

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->readValue(Ljava/io/InputStream;)Ljava/math/BigInteger;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhReplyCodec;->b:Lcom/jscape/util/h/I;

    invoke-interface {v3, p1}, Lcom/jscape/util/h/I;->read(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;

    invoke-direct {v3, v1, v2, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;-><init>(Ljava/security/PublicKey;Ljava/math/BigInteger;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V

    iput-object v0, v3, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;->hostKeyBlob:[B

    return-object v3
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhReplyCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;

    new-instance v0, Lcom/jscape/util/h/o;

    invoke-direct {v0}, Lcom/jscape/util/h/o;-><init>()V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhReplyCodec;->a:Lcom/jscape/util/h/I;

    iget-object v2, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;->hostKey:Ljava/security/PublicKey;

    invoke-interface {v1, v2, v0}, Lcom/jscape/util/h/I;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v1

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v1, v3, v2, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BIILjava/io/OutputStream;)V

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->d()[B

    move-result-object v0

    iput-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;->hostKeyBlob:[B

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;->f:Ljava/math/BigInteger;

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->writeValue(Ljava/math/BigInteger;Ljava/io/OutputStream;)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhReplyCodec;->b:Lcom/jscape/util/h/I;

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexDhReply;->signature:Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    invoke-interface {v0, p1, p2}, Lcom/jscape/util/h/I;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgKexDhReplyCodec;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
