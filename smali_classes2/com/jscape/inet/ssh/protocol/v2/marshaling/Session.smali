.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;
.super Ljava/lang/Object;


# instance fields
.field public algorithms:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;

.field public final clientIdentificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

.field public clientKexInitMessage:Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

.field public final messageCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

.field public final packetCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;

.field public final serverIdentificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

.field public serverKexInitMessage:Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

.field public sessionId:[B


# direct methods
.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;->messageCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;->messageCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    invoke-direct {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;-><init>(Lcom/jscape/util/h/I;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;->packetCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;->clientIdentificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;->serverIdentificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    return-void
.end method


# virtual methods
.method public initAlgorithms()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;->clientKexInitMessage:Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;->serverKexInitMessage:Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    invoke-static {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->algorithmsFor(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;->algorithms:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;

    return-void
.end method

.method public initSessionId([B)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;->sessionId:[B

    if-nez v0, :cond_1

    :cond_0
    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;->sessionId:[B

    :cond_1
    return-void
.end method

.method public readingAlgorithms(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;->packetCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->readingEncryption(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;->packetCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->readingMac(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;->packetCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->readingCompression(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;)V

    return-void
.end method

.method public writingAlgorithms(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;->packetCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->writingEncryption(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;->packetCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->writingMac(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;->packetCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->writingCompression(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;)V

    return-void
.end method
