.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final OS_AMIGA:B = 0x1t

.field public static final OS_ATARI:B = 0x5t

.field public static final OS_CPM:B = 0x9t

.field public static final OS_MACOS:B = 0x7t

.field public static final OS_MSDOS:B = 0x0t

.field public static final OS_OS2:B = 0x6t

.field public static final OS_QDOS:B = 0xct

.field public static final OS_RISCOS:B = 0xdt

.field public static final OS_TOPS20:B = 0xat

.field public static final OS_UNIX:B = 0x3t

.field public static final OS_UNKNOWN:B = -0x1t

.field public static final OS_VMCMS:B = 0x4t

.field public static final OS_VMS:B = 0x2t

.field public static final OS_WIN32:B = 0xbt

.field public static final OS_ZSYSTEM:B = 0x8t

.field private static final m:Ljava/lang/String;


# instance fields
.field a:Z

.field private b:Z

.field c:J

.field d:I

.field e:I

.field f:[B

.field g:[B

.field h:[B

.field i:I

.field j:J

.field k:Z

.field l:J


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const-string v0, "\'%I`"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->m:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/4 v5, 0x3

    const/4 v6, 0x2

    if-eqz v4, :cond_5

    const/4 v7, 0x1

    if-eq v4, v7, :cond_4

    if-eq v4, v6, :cond_3

    if-eq v4, v5, :cond_2

    const/4 v7, 0x4

    if-eq v4, v7, :cond_1

    const/4 v6, 0x5

    if-eq v4, v6, :cond_6

    const/16 v5, 0x19

    goto :goto_1

    :cond_1
    move v5, v6

    goto :goto_1

    :cond_2
    const/16 v5, 0x6b

    goto :goto_1

    :cond_3
    const/16 v5, 0x58

    goto :goto_1

    :cond_4
    const/16 v5, 0x7d

    goto :goto_1

    :cond_5
    const/16 v5, 0x63

    :cond_6
    :goto_1
    const/16 v4, 0x2b

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->b:Z

    const/16 v1, 0xff

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->e:I

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->k:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->l:J

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;)V
    .locals 8

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_8

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    if-nez v0, :cond_3

    :try_start_1
    iget-boolean v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->b:Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move v7, v4

    move v4, v1

    move v1, v7

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    move v4, v3

    :goto_1
    if-eqz v1, :cond_2

    or-int/lit8 v1, v4, 0x2

    goto :goto_2

    :cond_2
    move v1, v4

    :cond_3
    :goto_2
    :try_start_2
    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->f:[B
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_7

    if-nez v0, :cond_5

    if-eqz v4, :cond_4

    or-int/lit8 v1, v1, 0x4

    :cond_4
    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->g:[B

    :cond_5
    if-nez v0, :cond_7

    if-eqz v4, :cond_6

    or-int/lit8 v1, v1, 0x8

    :cond_6
    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->h:[B

    :cond_7
    if-eqz v4, :cond_8

    or-int/lit8 v1, v1, 0x10

    :cond_8
    :try_start_3
    iget v4, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_6

    const/4 v5, 0x4

    const/16 v6, 0x8

    if-nez v0, :cond_a

    if-ne v4, v2, :cond_9

    if-eqz v0, :cond_b

    goto :goto_3

    :cond_9
    move v5, v3

    :goto_3
    if-nez v0, :cond_c

    :try_start_4
    iget v4, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aK:I
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1

    const/16 v2, 0x9

    goto :goto_4

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_a
    move v5, v3

    :goto_4
    if-ne v4, v2, :cond_b

    or-int/lit8 v5, v5, 0x2

    :cond_b
    const/16 v2, -0x74e1

    invoke-virtual {p1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(I)V

    invoke-virtual {p1, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    iget-wide v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->l:J

    long-to-int v1, v1

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    iget-wide v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->l:J

    shr-long/2addr v1, v6

    long-to-int v1, v1

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    iget-wide v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->l:J

    const/16 v4, 0x10

    shr-long/2addr v1, v4

    long-to-int v1, v1

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    iget-wide v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->l:J

    const/16 v4, 0x18

    shr-long/2addr v1, v4

    long-to-int v1, v1

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    int-to-byte v1, v5

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    :cond_c
    :try_start_5
    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->e:I

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->f:[B
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_4

    if-nez v0, :cond_e

    if-eqz v1, :cond_d

    :try_start_6
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->f:[B

    array-length v1, v1

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->f:[B

    array-length v1, v1

    shr-int/2addr v1, v6

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->f:[B

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->f:[B

    array-length v2, v2

    invoke-virtual {p1, v1, v3, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([BII)V
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_5

    :cond_d
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->g:[B

    :cond_e
    if-nez v0, :cond_10

    if-eqz v1, :cond_f

    :try_start_7
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->g:[B

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->g:[B

    array-length v1, v1

    invoke-virtual {p1, v0, v3, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([BII)V

    invoke-virtual {p1, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_5

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_f
    :goto_5
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->h:[B

    :cond_10
    if-eqz v1, :cond_11

    :try_start_8
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->h:[B

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->h:[B

    array-length v1, v1

    invoke-virtual {p1, v0, v3, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([BII)V

    invoke-virtual {p1, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a(B)V
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_6

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_11
    :goto_6
    return-void

    :catch_4
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :catch_7
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :catch_8
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public clone()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    :try_start_0
    iget-object v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->f:[B
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v3, 0x0

    if-nez v0, :cond_1

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->f:[B

    array-length v4, v2

    new-array v5, v4, [B

    invoke-static {v2, v3, v5, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->f:[B

    :cond_0
    iget-object v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->g:[B

    :cond_1
    if-nez v0, :cond_3

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->g:[B

    array-length v4, v2

    new-array v5, v4, [B

    invoke-static {v2, v3, v5, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->g:[B

    :cond_2
    if-nez v0, :cond_4

    :try_start_1
    iget-object v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->h:[B
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_3
    :goto_0
    if-eqz v2, :cond_4

    iget-object v0, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->h:[B

    array-length v2, v0

    new-array v4, v2, [B

    invoke-static {v0, v3, v4, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->h:[B

    :cond_4
    return-object v1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public getCRC()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->j:J

    return-wide v0
.end method

.method public getComment()Ljava/lang/String;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->h:[B

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->h:[B

    sget-object v2, Ljava/nio/charset/StandardCharsets;->ISO_8859_1:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public getModifiedTime()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->l:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->g:[B

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->g:[B

    sget-object v2, Ljava/nio/charset/StandardCharsets;->ISO_8859_1:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public getOS()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->e:I

    return v0
.end method

.method public setCRC(J)V
    .locals 0

    iput-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->j:J

    return-void
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Ljava/nio/charset/StandardCharsets;->ISO_8859_1:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->h:[B

    return-void
.end method

.method public setModifiedTime(J)V
    .locals 0

    iput-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->l:J

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Ljava/nio/charset/StandardCharsets;->ISO_8859_1:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->g:[B

    return-void
.end method

.method public setOS(I)V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_2

    if-ltz p1, :cond_0

    const/16 v1, 0xd

    if-nez v0, :cond_1

    if-le p1, v1, :cond_3

    :cond_0
    const/16 v1, 0xff

    :cond_1
    move v2, v1

    move v1, p1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    move v2, p1

    :goto_0
    if-ne v1, v2, :cond_4

    :cond_3
    :try_start_0
    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->e:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_4

    return-void

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method
