.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x9

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x4a

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "/!#e^w@Y\u0013\u0019@1\u0011^Y~Ac5\u001aiX?ik>\u0013cXvfb?I+"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x23

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x58

    goto :goto_2

    :cond_2
    const/16 v12, 0x55

    goto :goto_2

    :cond_3
    const/16 v12, 0x60

    goto :goto_2

    :cond_4
    const/16 v12, 0x46

    goto :goto_2

    :cond_5
    const/16 v12, 0x3e

    goto :goto_2

    :cond_6
    const/16 v12, 0x18

    goto :goto_2

    :cond_7
    const/16 v12, 0x40

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->c:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->b:Ljava/lang/Object;

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a()Ljava/security/Signature;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;->b()[Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->b:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_1

    :cond_0
    if-nez v0, :cond_2

    :try_start_2
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/security/Provider;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    :try_start_3
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->b:Ljava/lang/Object;

    check-cast v1, Ljava/security/Provider;

    invoke-static {v0, v1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/Signature;

    move-result-object v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    :goto_1
    return-object v0

    :catch_2
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public static signerFor(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;
    .locals 1

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;

    invoke-direct {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method

.method public static signerFor(Ljava/lang/String;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;
    .locals 1

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;

    invoke-direct {v0, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public adjust([BLjava/security/PublicKey;)[B
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;->b()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    instance-of v1, p2, Ljava/security/interfaces/RSAKey;

    if-eqz v1, :cond_1

    :cond_0
    check-cast p2, Ljava/security/interfaces/RSAKey;

    invoke-interface {p2}, Ljava/security/interfaces/RSAKey;->getModulus()Ljava/math/BigInteger;

    move-result-object p2

    invoke-virtual {p2}, Ljava/math/BigInteger;->bitLength()I

    move-result p2

    if-nez v0, :cond_1

    array-length v0, p1

    mul-int/lit8 v0, v0, 0x8

    if-ge v0, p2, :cond_1

    sub-int/2addr p2, v0

    div-int/lit8 p2, p2, 0x8

    const/4 v0, 0x0

    array-length v1, p1

    add-int/2addr v1, p2

    new-array v1, v1, [B

    array-length v2, p1

    invoke-static {p1, v0, v1, p2, v2}, Lcom/jscape/util/v;->a([BI[BII)[B

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method public assertSignatureValid([BLjava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->a()Ljava/security/Signature;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    invoke-virtual {v0, p1}, Ljava/security/Signature;->update([B)V

    iget-object p1, p3, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;->blob:[B

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->adjust([BLjava/security/PublicKey;)[B

    move-result-object p1

    :try_start_0
    invoke-virtual {v0, p1}, Ljava/security/Signature;->verify([B)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer$InvalidSignatureException;

    invoke-direct {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer$InvalidSignatureException;-><init>()V

    throw p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public sign([BLjava/security/PrivateKey;)Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->a()Ljava/security/Signature;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;)V

    invoke-virtual {v0, p1}, Ljava/security/Signature;->update([B)V

    invoke-virtual {v0}, Ljava/security/Signature;->sign()[B

    move-result-object p1

    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->RSA:Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifier:Ljava/lang/String;

    invoke-direct {p2, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;-><init>(Ljava/lang/String;[B)V

    return-object p2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->c:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
