.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;
.super Ljava/lang/Object;


# static fields
.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->b()I

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3d

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->b(I)V

    :cond_0
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b()I
    .locals 1

    sget v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->b:I

    return v0
.end method

.method public static b(I)V
    .locals 0

    sput p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->b:I

    return-void
.end method

.method public static c()I
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->b()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x61

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public static readUsAsciiValue(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-static {v0, p0}, Lcom/jscape/util/h/a/g;->a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static readUtf8Value(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, p0}, Lcom/jscape/util/h/a/g;->a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static readValue(Ljava/io/InputStream;)[B
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/h/a/e;->a(Ljava/io/InputStream;)[B

    move-result-object p0

    return-object p0
.end method

.method public static writeUsAsciiValue(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-static {p0, v0, p1}, Lcom/jscape/util/h/a/g;->a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    return-void
.end method

.method public static writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {p0, v0, p1}, Lcom/jscape/util/h/a/g;->a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    return-void
.end method

.method public static writeValue([BIILjava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0, p1, p2, p3}, Lcom/jscape/util/h/a/e;->a([BIILjava/io/OutputStream;)V

    return-void
.end method

.method public static writeValue([BLjava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/jscape/util/h/a/e;->a([BLjava/io/OutputStream;)V

    return-void
.end method
