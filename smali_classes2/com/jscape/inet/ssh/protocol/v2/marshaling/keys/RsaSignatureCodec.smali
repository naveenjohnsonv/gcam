.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/RsaSignatureCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$EntryCodec;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/lang/String;Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readValue(Ljava/io/InputStream;)[B

    move-result-object p2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    invoke-direct {v0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;-><init>(Ljava/lang/String;[B)V

    return-object v0
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;->blob:[B

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    return-void
.end method
