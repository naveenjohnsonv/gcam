.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;
.super Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public hostKey:Ljava/security/PublicKey;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "@\u00019r5hm\u0005U0s!:\u001d@\u0001+{ qz\u001eh<{<sv\nH;\u007f&np\u0002r,l;ixQ\n@\u00010q!sT\tXe\u001c/M1{<sL\tR+w=i?\u0017L=m!fx\tb7z7d\"\u001d@\u0001;r;bq\u0018h<{<sv\nH;\u007f&np\u0002r,l;ixQ\u000c@\u0001+{!tv\u0003O\u0011zo\u0017@\u0001+{ qz\u001ej=f\u001biv\u0018l=m!fx\t\u001c"

    const/16 v4, 0x96

    const/16 v5, 0xd

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x2f

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    const/16 v14, 0xe

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x26

    const-string v3, "s2\u001bL\u0002_I+Q\u0004I\u0004W\u0011\u0017s2\u0008A\u0008QB+Y\u000eU(ZE+_\u000e^\u0012UK:/"

    move v7, v10

    move v5, v14

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x1c

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v10, v13

    rem-int/lit8 v1, v13, 0x7

    if-eqz v1, :cond_8

    if-eq v1, v9, :cond_9

    const/4 v14, 0x2

    if-eq v1, v14, :cond_7

    const/4 v14, 0x3

    if-eq v1, v14, :cond_6

    const/4 v14, 0x4

    if-eq v1, v14, :cond_5

    const/4 v14, 0x5

    if-eq v1, v14, :cond_4

    const/16 v14, 0x30

    goto :goto_4

    :cond_4
    const/16 v14, 0x28

    goto :goto_4

    :cond_5
    const/16 v14, 0x7d

    goto :goto_4

    :cond_6
    const/16 v14, 0x31

    goto :goto_4

    :cond_7
    const/16 v14, 0x77

    goto :goto_4

    :cond_8
    const/16 v14, 0x43

    :cond_9
    :goto_4
    xor-int v1, v8, v14

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/Session;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;)V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->a:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->messageCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->packetCodec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->clientIdentificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->serverIdentificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->clientKexInitMessage:Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->serverKexInitMessage:Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->algorithms:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->sessionId:[B

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/ClientSession;->hostKey:Ljava/security/PublicKey;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
