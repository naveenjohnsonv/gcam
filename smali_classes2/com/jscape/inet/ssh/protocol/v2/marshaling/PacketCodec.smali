.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/ssh/protocol/messages/Message;",
        ">;"
    }
.end annotation


# static fields
.field private static final A:[Ljava/lang/String;

.field private static final a:I = 0x5

.field private static final b:I = 0x4

.field private static final c:I = 0x8

.field private static final d:I = 0x4

.field private static final e:I = 0xff

.field private static final f:I = 0x88b8


# instance fields
.field private final g:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;

.field private final i:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;

.field private final j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

.field private final k:Lcom/jscape/util/h/o;

.field private final l:Lcom/jscape/util/h/o;

.field private final m:Ljava/util/Random;

.field private n:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

.field private o:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

.field private p:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

.field private q:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

.field private r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;

.field private s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;

.field private t:I

.field private u:I

.field private v:[B

.field private volatile w:J

.field private volatile x:J

.field private volatile y:J

.field private volatile z:J


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "jZ\u001e\u00066\"b(\u001d:\u0011.#n(\u0019\u000c:*;i#\u0008T\rjZ\u001e\u00066\"b(\u001d$\u0015<k\u001a\u0016\u001b\n\u001f:\"H)\u001e\u000c\u0017\u007f-f#\t\u001a\u001583H)\u001e\u000c\u0017b\u000fjZ\u001b\u0011>2F#\t\u001a\u001583x{\u0011jZ\u001e\u00066\"b(\u001d-\u001d)?x)\u0008T\u0014jZ\u001b\u0011>2b(\u001d,\u001a<$r6\u000e\u0000\u001b1k\u0018jZ\u001b\u0011>2b(\u001d:\u0011.#n(\u0019\u000c:*;i#\u0008T\u0011jZ\u001b\u0011>2b(\u001d-\u001d)?x)\u0008T\u000fjZ\u001e\u00066\"\u007f#\u0014+\r+3x{\u0012jZ\u001e\u00066\"\u007f#\u0014$\u0011,%j!\u001f\u001aI\u0015jZ\u001e\u00066\"b(\u001d*\u001b2&y#\t\u001a\u001d086\rjZ\u001b\u0011>2b(\u001d$\u0015<k\u0014jZ\u001e\u00066\"b(\u001d,\u001a<$r6\u000e\u0000\u001b1k\u0015jZ\u001b\u0011>2b(\u001d*\u001b2&y#\t\u001a\u001d086"

    const/16 v4, 0x115

    const/16 v5, 0x18

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x26

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x4a

    const/16 v3, 0xc

    const-string v5, "m]\u001c\u001695N8\t\u000b\u0000e=\u0014\u0013\u001d\u0006(!c3\t\u000b\u0017x\u0002_\t]\u001e\u0012;:i5]\u0002\u001666x)GNV+\u007f,\u0011\u0012\u001d\u000013`$]\n\u0016;#u1\t\u0007\u001c6qi3\u000f\u0001\u0001v"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x21

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->A:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x2d

    goto :goto_4

    :cond_4
    const/16 v1, 0x70

    goto :goto_4

    :cond_5
    const/16 v1, 0x79

    goto :goto_4

    :cond_6
    const/16 v1, 0x52

    goto :goto_4

    :cond_7
    const/16 v1, 0x4f

    goto :goto_4

    :cond_8
    const/16 v1, 0x5c

    goto :goto_4

    :cond_9
    const/16 v1, 0x60

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/util/h/I;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->g:Lcom/jscape/util/h/I;

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;

    invoke-direct {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->h:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;

    invoke-direct {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->i:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    const v0, 0x88b8

    invoke-direct {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;-><init>(I)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    new-instance p1, Lcom/jscape/util/h/o;

    invoke-direct {p1, v0}, Lcom/jscape/util/h/o;-><init>(I)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->k:Lcom/jscape/util/h/o;

    new-instance p1, Lcom/jscape/util/h/o;

    invoke-direct {p1, v0}, Lcom/jscape/util/h/o;-><init>(I)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->l:Lcom/jscape/util/h/o;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    new-instance p1, Ljava/util/Random;

    invoke-direct {p1}, Ljava/util/Random;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->m:Ljava/util/Random;

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;->NULL:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->n:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;->NULL:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->o:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;->NULL:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->p:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;->NULL:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->q:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;->NULL:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;->NULL:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;

    const/16 p1, 0x8

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->t:I

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->u:I

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->p:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    invoke-interface {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;->digestLength()I

    move-result p1

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->v:[B

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x4

    new-array p1, p1, [Ljava/lang/String;

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b([Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->h:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;->next()V

    return-void
.end method

.method private a(ILjava/io/InputStream;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->p:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    invoke-interface {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;->digestLength()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {v0, p2, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->fill(Ljava/io/InputStream;I)V

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->n:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    invoke-virtual {p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->buffer()[B

    move-result-object v3

    iget v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->t:I

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    invoke-virtual {p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->buffer()[B

    move-result-object v6

    iget v7, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->t:I

    move v5, p1

    invoke-interface/range {v2 .. v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;->apply([BII[BI)V

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    invoke-virtual {p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->buffer()[B

    move-result-object p2

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->t:I

    add-int/2addr v0, p1

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->v:[B

    invoke-static {p2, v0, p1}, Lcom/jscape/util/v;->a([BI[B)[B

    iget-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->w:J

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->position()I

    move-result v0

    int-to-long v0, v0

    add-long/2addr p1, v0

    iput-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->w:J

    iget-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->y:J

    const-wide/16 v0, 0x1

    add-long/2addr p1, v0

    iput-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->y:J

    return-void
.end method

.method private a(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->i:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;->value()I

    move-result v0

    iput v0, p1, Lcom/jscape/inet/ssh/protocol/messages/Message;->number:I

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->b(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->g()B

    move-result p1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->l:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x5

    add-int/lit8 v0, v0, 0x1

    sget-object v1, Lcom/jscape/util/y;->a:Lcom/jscape/util/K;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->l:Lcom/jscape/util/h/o;

    invoke-virtual {v2}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v0, v2, v3}, Lcom/jscape/util/K;->a(I[BI)[B

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->l:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v0

    const/4 v1, 0x4

    aput-byte p1, v0, v1

    return-void
.end method

.method private a(Ljava/io/InputStream;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->reset()V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->t:I

    invoke-virtual {v0, p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->fill(Ljava/io/InputStream;I)V

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->n:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->buffer()[B

    move-result-object v3

    iget v5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->t:I

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->buffer()[B

    move-result-object v6

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-interface/range {v2 .. v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;->apply([BII[BI)V

    return-void
.end method

.method private a(Ljava/io/OutputStream;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->o:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->l:Lcom/jscape/util/h/o;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->l:Lcom/jscape/util/h/o;

    invoke-virtual {v2}, Lcom/jscape/util/h/o;->b()I

    move-result v2

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->q:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    invoke-interface {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;->digestLength()I

    move-result v3

    sub-int v3, v2, v3

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->l:Lcom/jscape/util/h/o;

    invoke-virtual {v2}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v4

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;->apply([BII[BI)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->l:Lcom/jscape/util/h/o;

    invoke-virtual {v0, p1}, Lcom/jscape/util/h/o;->a(Ljava/io/OutputStream;)V

    iget-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->x:J

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->l:Lcom/jscape/util/h/o;

    invoke-virtual {p1}, Lcom/jscape/util/h/o;->b()I

    move-result p1

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->x:J

    iget-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->z:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->z:J

    return-void
.end method

.method private b()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/util/y;->a:Lcom/jscape/util/K;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->buffer()[B

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/jscape/util/K;->d([BI)I

    move-result v1

    if-nez v0, :cond_0

    if-ltz v1, :cond_1

    :cond_0
    const v2, 0xfffff

    if-nez v0, :cond_2

    if-gt v1, v2, :cond_1

    add-int/lit8 v1, v1, 0x4

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->t:I

    goto :goto_0

    :cond_1
    :try_start_0
    new-instance v0, Ljava/io/IOException;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->A:[Ljava/lang/String;

    const/16 v4, 0xf

    aget-object v2, v2, v4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v3

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_2
    :goto_0
    sub-int/2addr v1, v2

    return v1
.end method

.method private b(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->k:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->c()Lcom/jscape/util/h/o;

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->g:Lcom/jscape/util/h/I;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->k:Lcom/jscape/util/h/o;

    invoke-interface {v0, p1, v1}, Lcom/jscape/util/h/I;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->l:Lcom/jscape/util/h/o;

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/jscape/util/h/o;->a(I)Lcom/jscape/util/h/o;

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->k:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->k:Lcom/jscape/util/h/o;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->b()I

    move-result v1

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->l:Lcom/jscape/util/h/o;

    const/4 v3, 0x0

    invoke-interface {p1, v0, v3, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;->apply([BIILjava/io/OutputStream;)I

    return-void
.end method

.method private c()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->p:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->h:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;->valueBytes()[B

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->buffer()[B

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    invoke-virtual {v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->position()I

    move-result v4

    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->p:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    invoke-interface {v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;->digestLength()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->v:[B

    invoke-interface/range {v0 .. v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;->assertMacValid([B[BII[B)V
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$InvalidMacException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$InvalidMacException;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$InvalidMacException;-><init>()V

    throw v0
.end method

.method private d()Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->at(I)I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->position()I

    move-result v1

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->p:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    invoke-interface {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;->digestLength()I

    move-result v2

    sub-int/2addr v1, v2

    const/4 v2, 0x5

    sub-int/2addr v1, v2

    sub-int/2addr v1, v0

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    invoke-virtual {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->buffer()[B

    move-result-object v3

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->asOutputStream(I)Ljava/io/OutputStream;

    move-result-object v4

    invoke-interface {v0, v3, v2, v1, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;->apply([BIILjava/io/OutputStream;)I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->j:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;

    invoke-virtual {v1, v5, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->asInputStream(II)Ljava/io/InputStream;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->g:Lcom/jscape/util/h/I;

    invoke-interface {v1, v0}, Lcom/jscape/util/h/I;->read(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/ssh/protocol/messages/Message;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->h:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;->value()I

    move-result v1

    iput v1, v0, Lcom/jscape/inet/ssh/protocol/messages/Message;->number:I

    return-object v0
.end method

.method private e()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->i:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;->next()V

    return-void
.end method

.method private f()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->q:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->i:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;->valueBytes()[B

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->l:Lcom/jscape/util/h/o;

    invoke-virtual {v2}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->l:Lcom/jscape/util/h/o;

    invoke-virtual {v3}, Lcom/jscape/util/h/o;->b()I

    move-result v4

    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->l:Lcom/jscape/util/h/o;

    const/4 v3, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;->apply([B[BIILjava/io/OutputStream;)V

    return-void
.end method

.method private g()B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->l:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result v0

    const/4 v1, 0x4

    add-int/2addr v0, v1

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->u:I

    rem-int/2addr v0, v3

    if-nez v2, :cond_0

    if-lez v0, :cond_1

    add-int/2addr v3, v1

    sub-int/2addr v3, v0

    move v1, v3

    goto :goto_0

    :cond_0
    move v1, v0

    :cond_1
    :goto_0
    new-array v0, v1, [B

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->m:Ljava/util/Random;

    invoke-virtual {v2, v0}, Ljava/util/Random;->nextBytes([B)V

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->l:Lcom/jscape/util/h/o;

    invoke-virtual {v2, v0}, Lcom/jscape/util/h/o;->write([B)V

    int-to-byte v0, v1

    return v0
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->a()V

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->a(Ljava/io/InputStream;)V

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->b()I

    move-result v0

    invoke-direct {p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->a(ILjava/io/InputStream;)V

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->c()V

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->d()Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public readBytes()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->w:J

    return-wide v0
.end method

.method public readMessages()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->y:J

    return-wide v0
.end method

.method public readingCompression(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;

    return-void
.end method

.method public readingEncryption(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;)V
    .locals 1

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->n:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    invoke-interface {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;->blockLength()I

    move-result p1

    const/16 v0, 0x8

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->t:I

    return-void
.end method

.method public readingMac(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->p:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    invoke-interface {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;->digestLength()I

    move-result p1

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->v:[B

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->A:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->g:Lcom/jscape/util/h/I;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x6

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->h:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->i:Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$SequenceNumber;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    aget-object v4, v2, v3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->n:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v4, 0xc

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->o:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v4, 0xb

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->p:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v4, 0x1

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->q:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v4, 0xd

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v4, 0xa

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v4, 0x7

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->t:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v4, 0x4

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->u:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v4, 0xe

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->w:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v4, 0x8

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->x:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v4, 0x3

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->y:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v4, 0x9

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->z:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    new-array v0, v3, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object v1
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->e()V

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->a(Lcom/jscape/inet/ssh/protocol/messages/Message;)V

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->f()V

    invoke-direct {p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->a(Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method

.method public writingCompression(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;

    return-void
.end method

.method public writingEncryption(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;)V
    .locals 1

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->o:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    invoke-interface {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;->blockLength()I

    move-result p1

    const/16 v0, 0x8

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->u:I

    return-void
.end method

.method public writingMac(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->q:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    return-void
.end method

.method public writtenBytes()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->x:J

    return-wide v0
.end method

.method public writtenMessages()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec;->z:J

    return-wide v0
.end method
