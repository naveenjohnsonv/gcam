.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x2

    const/4 v3, 0x0

    const-string v4, "\u0011\u001c\u0005\u0011\u001c\r\u0000\r\u0003\u0010\u000c\u0008\u0006\u0007\u0017\u0008ayx\u0005\u0011\u001c\r\u0000\r\u0003\u0006\u000c\u0008\u0002\u0011\u001c\u0006\u0007\u0017\u0008ayx\u0004\u0007\u0017\u0008b\u0003\u0010\u000c\u0008"

    const/16 v5, 0x30

    move v7, v1

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x3e

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x3

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x8

    const-string v4, "pz~\u0004qa~\u0014"

    move v8, v11

    move v7, v15

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x48

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v2, v14, 0x7

    const/16 v17, 0x6a

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    if-eq v2, v1, :cond_7

    if-eq v2, v15, :cond_6

    const/4 v15, 0x4

    if-eq v2, v15, :cond_5

    const/4 v15, 0x5

    if-eq v2, v15, :cond_4

    goto :goto_4

    :cond_4
    const/16 v17, 0x70

    goto :goto_4

    :cond_5
    const/16 v17, 0x72

    goto :goto_4

    :cond_6
    const/16 v17, 0x6d

    goto :goto_4

    :cond_7
    const/16 v17, 0x77

    goto :goto_4

    :cond_8
    const/16 v17, 0x61

    :cond_9
    :goto_4
    xor-int v2, v9, v17

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static initSha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;
    .locals 7

    const/4 v0, 0x1

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;->a:[Ljava/lang/String;

    const/16 v4, 0xa

    aget-object v4, v3, v4

    const/16 v5, 0x8

    aget-object v5, v3, v5

    invoke-static {v5, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->signerFor(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    const/4 v4, 0x0

    aput-object v2, v1, v4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    const/16 v5, 0x9

    aget-object v5, v3, v5

    const/16 v6, 0xb

    aget-object v6, v3, v6

    invoke-static {v6, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceDsaSigner;->signerFor(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceDsaSigner;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    aput-object v2, v1, v4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    const/4 v5, 0x6

    aget-object v5, v3, v5

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;

    invoke-direct {v6, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;-><init>(Ljava/lang/Object;)V

    invoke-direct {v2, v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    aput-object v2, v1, v4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    aget-object v0, v3, v0

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;

    invoke-direct {v3, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;-><init>(Ljava/lang/Object;)V

    invoke-direct {v2, v0, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    aput-object v2, v1, v4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    return-object p0
.end method

.method public static initSha1(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;
    .locals 7

    const/4 v0, 0x1

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;->a:[Ljava/lang/String;

    const/16 v4, 0xa

    aget-object v4, v3, v4

    const/16 v5, 0xb

    aget-object v6, v3, v5

    invoke-static {v6, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->signerFor(Ljava/lang/String;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;

    move-result-object v6

    invoke-direct {v2, v4, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    const/4 v4, 0x0

    aput-object v2, v1, v4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    const/16 v6, 0x9

    aget-object v6, v3, v6

    aget-object v5, v3, v5

    invoke-static {v5, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceDsaSigner;->signerFor(Ljava/lang/String;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceDsaSigner;

    move-result-object v5

    invoke-direct {v2, v6, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    aput-object v2, v1, v4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    const/4 v5, 0x6

    aget-object v5, v3, v5

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;

    invoke-direct {v6, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;-><init>(Ljava/lang/Object;)V

    invoke-direct {v2, v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    aput-object v2, v1, v4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    aget-object v0, v3, v0

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;

    invoke-direct {v3, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;-><init>(Ljava/lang/Object;)V

    invoke-direct {v2, v0, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    aput-object v2, v1, v4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    return-object p0
.end method

.method public static initSha256(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;
    .locals 7

    const/4 v0, 0x1

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;->a:[Ljava/lang/String;

    const/16 v4, 0xa

    aget-object v4, v3, v4

    const/4 v5, 0x3

    aget-object v6, v3, v5

    invoke-static {v6, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->signerFor(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;

    move-result-object v6

    invoke-direct {v2, v4, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    const/4 v4, 0x0

    aput-object v2, v1, v4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    const/16 v6, 0x9

    aget-object v6, v3, v6

    aget-object v5, v3, v5

    invoke-static {v5, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceDsaSigner;->signerFor(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceDsaSigner;

    move-result-object v5

    invoke-direct {v2, v6, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    aput-object v2, v1, v4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    const/4 v5, 0x6

    aget-object v5, v3, v5

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;

    invoke-direct {v6, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;-><init>(Ljava/lang/Object;)V

    invoke-direct {v2, v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    aput-object v2, v1, v4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    aget-object v0, v3, v0

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;

    invoke-direct {v3, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;-><init>(Ljava/lang/Object;)V

    invoke-direct {v2, v0, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    aput-object v2, v1, v4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    return-object p0
.end method

.method public static initSha256(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;
    .locals 7

    const/4 v0, 0x1

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signers;->a:[Ljava/lang/String;

    const/4 v4, 0x5

    aget-object v4, v3, v4

    const/4 v5, 0x7

    aget-object v5, v3, v5

    invoke-static {v5, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;->signerFor(Ljava/lang/String;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceRsaSigner;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    const/4 v4, 0x0

    aput-object v2, v1, v4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    const/4 v5, 0x2

    aget-object v5, v3, v5

    const/4 v6, 0x3

    aget-object v6, v3, v6

    invoke-static {v6, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceDsaSigner;->signerFor(Ljava/lang/String;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceDsaSigner;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    aput-object v2, v1, v4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v1, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    aget-object v5, v3, v4

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;

    invoke-direct {v6, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;-><init>(Ljava/lang/Object;)V

    invoke-direct {v2, v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    aput-object v2, v1, v4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;

    const/4 v2, 0x4

    aget-object v2, v3, v2

    new-instance v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;

    invoke-direct {v3, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;-><init>(Ljava/lang/Object;)V

    invoke-direct {v1, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;)V

    aput-object v1, v0, v4

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/ComponentSigner;

    return-object p0
.end method
