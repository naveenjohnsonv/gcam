.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final compressionClientToServer:Ljava/lang/String;

.field public final compressionServerToClient:Ljava/lang/String;

.field public final encryptionClientToServer:Ljava/lang/String;

.field public final encryptionServerToClient:Ljava/lang/String;

.field public final keyExchange:Ljava/lang/String;

.field public final languageClientToServer:Ljava/lang/String;

.field public final languageServerToClient:Ljava/lang/String;

.field public final macClientToServer:Ljava/lang/String;

.field public final macServerToClient:Ljava/lang/String;

.field public final serverHostKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "E4[Ey94\u001b\\GS\u007f\u00044\u0010)\u000f\u0015E4EAh\u000c=\u0000qFT_ \u0002\u000cf^Eyrv\u0019(xOOy&%\u0001y[\u0000p$4\u0010QPCc.?\u000eq\u0015\u0007\u001cE4MNh=(\u0019`AOe\u001c4\u001bbMR_ \u0012\u0005}MN\u007frv\u001dE4KOf?#\u000cg[Id!\u0012\u0005}MN\u007f\u001b>:qZVn=lN\u001cE4MNh=(\u0019`AOe\u000c=\u0000qFT_ \u0002\u000cf^Eyrv\u0015E4EAh\u001c4\u001bbMR_ \u0012\u0005}MN\u007frv\u001aE4DAe($\u0008sMcg&4\u0007`|OX*#\u001fqZ\u001d,"

    const/16 v4, 0xca

    const/16 v5, 0x11

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x7d

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x38

    const/16 v3, 0x1d

    const-string v5, "\u001bj\u0015\u00118a}R9\u0005\u0017:\u007f\\R8\u0000\u001b\'E`t&\u001f\u001b;e2\u0010\u001a\u001bj\u001a\u001f;vzV-\u0013-0cyR8\"\u0011\u0016}fR$\u0002Cr"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x23

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x2c

    goto :goto_4

    :cond_4
    const/16 v1, 0x32

    goto :goto_4

    :cond_5
    const/16 v1, 0x76

    goto :goto_4

    :cond_6
    const/16 v1, 0x5d

    goto :goto_4

    :cond_7
    const/16 v1, 0x55

    goto :goto_4

    :cond_8
    const/16 v1, 0x69

    goto :goto_4

    :cond_9
    const/16 v1, 0x14

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->keyExchange:Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->serverHostKey:Ljava/lang/String;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->encryptionClientToServer:Ljava/lang/String;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->encryptionServerToClient:Ljava/lang/String;

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->macClientToServer:Ljava/lang/String;

    invoke-static {p6}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->macServerToClient:Ljava/lang/String;

    invoke-static {p7}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p7, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->compressionClientToServer:Ljava/lang/String;

    invoke-static {p8}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p8, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->compressionServerToClient:Ljava/lang/String;

    invoke-static {p9}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p9, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->languageClientToServer:Ljava/lang/String;

    invoke-static {p10}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p10, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->languageServerToClient:Ljava/lang/String;

    return-void
.end method

.method private static a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException;
    .locals 0

    return-object p0
.end method

.method private static a(Ljava/util/List;Ljava/util/List;Z)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->c()Z

    move-result v0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :try_start_0
    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_2

    if-eqz v2, :cond_1

    return-object v1

    :cond_1
    if-nez v0, :cond_0

    goto :goto_0

    :cond_2
    move p2, v2

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException;

    move-result-object p0

    throw p0

    :cond_3
    :goto_0
    if-nez p2, :cond_4

    const-string p0, ""

    return-object p0

    :cond_4
    :try_start_2
    new-instance p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException;

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException;-><init>()V

    throw p0
    :try_end_2
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException;

    move-result-object p0

    throw p0
.end method

.method public static algorithmsFor(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms$CommonAlgorithmsNotFoundException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->kexAlgorithms:Ljava/util/List;

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->kexAlgorithms:Ljava/util/List;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->a(Ljava/util/List;Ljava/util/List;Z)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->serverHostKeyAlgorithms:Ljava/util/List;

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->serverHostKeyAlgorithms:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->a(Ljava/util/List;Ljava/util/List;Z)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->encryptionAlgorithmsClientToServer:Ljava/util/List;

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->encryptionAlgorithmsClientToServer:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->a(Ljava/util/List;Ljava/util/List;Z)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->encryptionAlgorithmsServerToClient:Ljava/util/List;

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->encryptionAlgorithmsServerToClient:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->a(Ljava/util/List;Ljava/util/List;Z)Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->macAlgorithmsClientToServer:Ljava/util/List;

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->macAlgorithmsClientToServer:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->a(Ljava/util/List;Ljava/util/List;Z)Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->macAlgorithmsServerToClient:Ljava/util/List;

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->macAlgorithmsServerToClient:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->a(Ljava/util/List;Ljava/util/List;Z)Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->compressionAlgorithmsClientToServer:Ljava/util/List;

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->compressionAlgorithmsClientToServer:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->a(Ljava/util/List;Ljava/util/List;Z)Ljava/lang/String;

    move-result-object v10

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->compressionAlgorithmsServerToClient:Ljava/util/List;

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->compressionAlgorithmsServerToClient:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->a(Ljava/util/List;Ljava/util/List;Z)Ljava/lang/String;

    move-result-object v11

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->languagesClientToServer:Ljava/util/List;

    iget-object v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->languagesClientToServer:Ljava/util/List;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->a(Ljava/util/List;Ljava/util/List;Z)Ljava/lang/String;

    move-result-object v12

    iget-object p0, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->languagesServerToClient:Ljava/util/List;

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgKexInit;->languagesServerToClient:Ljava/util/List;

    invoke-static {p0, p1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->a(Ljava/util/List;Ljava/util/List;Z)Ljava/lang/String;

    move-result-object v13

    new-instance p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;

    move-object v3, p0

    invoke-direct/range {v3 .. v13}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->a:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->keyExchange:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->serverHostKey:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->encryptionClientToServer:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x3

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->encryptionServerToClient:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->macClientToServer:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x6

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->macServerToClient:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->compressionClientToServer:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v3, 0x8

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->compressionServerToClient:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x7

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->languageClientToServer:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v3, 0x9

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Algorithms;->languageServerToClient:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
