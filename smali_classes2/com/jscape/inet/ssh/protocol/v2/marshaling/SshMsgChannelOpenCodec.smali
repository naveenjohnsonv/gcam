.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/ssh/protocol/messages/Message;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v5, "b\u001c-=\u0012 !E\u0006;,B=+D\u0001?/\u0007jn\u001ab\u001c-=\u0012 !E\u0006;,B3&V\u001c0-\u000ep:N\u0002;rB"

    const/16 v6, 0x30

    const/4 v7, -0x1

    const/16 v8, 0x15

    const/4 v9, 0x0

    :goto_0
    const/16 v10, 0x5b

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    const/4 v15, 0x0

    :goto_2
    const/16 v16, 0x39

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    add-int/lit8 v12, v9, 0x1

    if-eqz v13, :cond_1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v12

    goto :goto_0

    :cond_0
    const/16 v5, 0x27

    const-string v6, "1TcPDbX\nFesRiT\u0012Be^Xa~\u0001\u0007poRd\u007f\u000bIlXYqi\u000bBx \u0011N\u0007|o^qr\u000c@NsCwr\u0007T6"

    move v8, v5

    move-object v5, v6

    move v9, v12

    move/from16 v6, v16

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v12

    :goto_3
    const/16 v10, 0xe

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->c:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v17, v12, v15

    rem-int/lit8 v2, v15, 0x7

    const/4 v3, 0x5

    if-eqz v2, :cond_8

    if-eq v2, v11, :cond_7

    const/4 v4, 0x2

    if-eq v2, v4, :cond_6

    const/4 v4, 0x3

    if-eq v2, v4, :cond_5

    if-eq v2, v0, :cond_9

    if-eq v2, v3, :cond_4

    const/16 v16, 0x15

    goto :goto_4

    :cond_4
    const/16 v16, 0xb

    goto :goto_4

    :cond_5
    const/16 v16, 0x13

    goto :goto_4

    :cond_6
    move/from16 v16, v3

    goto :goto_4

    :cond_7
    const/16 v16, 0x29

    goto :goto_4

    :cond_8
    const/16 v16, 0x6c

    :cond_9
    :goto_4
    xor-int v2, v10, v16

    xor-int v2, v17, v2

    int-to-char v2, v2

    aput-char v2, v12, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method public varargs constructor <init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->b:Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;

    return-void
.end method

.method private a(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpen;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->b:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->c:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method

.method private a(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->c:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v8

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readLongValue(Ljava/io/InputStream;)J

    move-result-wide v9

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v11

    :try_start_0
    invoke-direct {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->a(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;

    move-result-object v0

    iget-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;->codec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$TypeCodec;

    move-object v3, p1

    move v4, v8

    move-wide v5, v9

    move v7, v11

    invoke-interface/range {v2 .. v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$TypeCodec;->read(Ljava/io/InputStream;IJI)Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpen;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    invoke-static {p1}, Lcom/jscape/util/X;->c(Ljava/io/InputStream;)[B

    move-result-object v6

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenUnknown;

    move-object v0, p1

    move v2, v8

    move-wide v3, v9

    move v5, v11

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenUnknown;-><init>(Ljava/lang/String;IJI[B)V

    return-object p1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public varargs set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;
    .locals 9

    array-length v0, p1

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p1, v2

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->a:Ljava/util/Map;

    iget-object v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;->channelType:Ljava/lang/String;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;->messageClasses:[Ljava/lang/Class;

    array-length v5, v4

    move v6, v1

    :goto_1
    if-ge v6, v5, :cond_0

    aget-object v7, v4, v6

    iget-object v8, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->b:Ljava/util/Map;

    invoke-interface {v8, v7, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->c:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->a:Ljava/util/Map;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->b:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpen;

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->a(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpen;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;

    move-result-object v0

    iget-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;->channelType:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUsAsciiValue(Ljava/lang/String;Ljava/io/OutputStream;)V

    iget v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpen;->senderChannel:I

    invoke-static {v1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-wide v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpen;->initialWindowSize:J

    invoke-static {v1, v2, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(JLjava/io/OutputStream;)V

    iget v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpen;->maxPacketSize:I

    invoke-static {v1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$Entry;->codec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$TypeCodec;

    invoke-interface {v0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$TypeCodec;->write(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpen;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
