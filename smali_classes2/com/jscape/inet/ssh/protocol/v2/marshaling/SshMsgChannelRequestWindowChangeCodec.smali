.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestWindowChangeCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$RequestCodec;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;IZ)Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v2

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v3

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v4

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v5

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;

    move-object v0, p1

    move v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;-><init>(IIIII)V

    return-object p1
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;

    iget v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->terminalCharactersWidth:I

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->terminalRowsHeight:I

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->terminalPixelsWidth:I

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestWindowChange;->terminalPixelsHeight:I

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    return-void
.end method
