.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/ssh/protocol/messages/Message;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class;",
            "Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "Ue\u000fi}px\u0017\"=u`vx\u001c6E)*6\u0010VgcV\u0015*\u001azxVt\u00080\u001dh`G~\u001d \u001b;ovt\u0018!\u0011usA\u007f\r7\u0011~g9"

    const/16 v5, 0x3b

    const/16 v6, 0x11

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x34

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    const/16 v15, 0x30

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v4, 0x15

    const-string v5, "/(\u0008mgw}\u00082\u001e|7jw\t5\u001a\u007fr=2\u001a/(\u0008mgw}\u00082\u001e|7uw\u000b3\u001ekc\'|\u001b+\u001e\"7"

    move v6, v4

    move-object v4, v5

    move v8, v11

    move v5, v15

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x37

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->c:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_8

    if-eq v2, v10, :cond_7

    const/4 v3, 0x2

    if-eq v2, v3, :cond_6

    const/4 v3, 0x3

    if-eq v2, v3, :cond_5

    if-eq v2, v0, :cond_4

    const/4 v3, 0x5

    if-eq v2, v3, :cond_9

    const/16 v15, 0x25

    goto :goto_4

    :cond_4
    const/16 v15, 0x20

    goto :goto_4

    :cond_5
    const/16 v15, 0x2f

    goto :goto_4

    :cond_6
    const/16 v15, 0x4c

    goto :goto_4

    :cond_7
    const/16 v15, 0x71

    goto :goto_4

    :cond_8
    const/16 v15, 0x4d

    :cond_9
    :goto_4
    xor-int v2, v9, v15

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public varargs constructor <init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->b:Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;

    return-void
.end method

.method private a(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequest;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->b:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->c:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method

.method private a(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->c:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/util/h/a/a;->a(Ljava/io/InputStream;)Z

    move-result v1

    :try_start_0
    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->a(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;

    move-result-object v2

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;->codec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$RequestCodec;

    invoke-interface {v2, p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$RequestCodec;->read(Ljava/io/InputStream;Z)Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequest;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    invoke-static {p1}, Lcom/jscape/util/X;->c(Ljava/io/InputStream;)[B

    move-result-object p1

    new-instance v2, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequestUnknown;

    invoke-direct {v2, v0, v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequestUnknown;-><init>(Ljava/lang/String;Z[B)V

    return-object v2
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public varargs set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;
    .locals 9

    array-length v0, p1

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p1, v2

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->a:Ljava/util/Map;

    iget-object v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;->requestName:Ljava/lang/String;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;->messageClasses:[Ljava/lang/Class;

    array-length v5, v4

    move v6, v1

    :goto_1
    if-ge v6, v5, :cond_0

    aget-object v7, v4, v6

    iget-object v8, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->b:Ljava/util/Map;

    invoke-interface {v8, v7, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->c:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->a:Ljava/util/Map;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->b:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequest;

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->a(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequest;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;

    move-result-object v0

    iget-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;->requestName:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUsAsciiValue(Ljava/lang/String;Ljava/io/OutputStream;)V

    iget-boolean v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequest;->wantReply:Z

    invoke-static {v1, p2}, Lcom/jscape/util/h/a/a;->a(ZLjava/io/OutputStream;)V

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$Entry;->codec:Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$RequestCodec;

    invoke-interface {v0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec$RequestCodec;->write(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgGlobalRequest;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgGlobalRequestCodec;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
