.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/ssh/protocol/messages/Message;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:I = 0xff

.field private static final b:Ljava/lang/String;

.field private static final c:[B

.field private static final d:Ljava/lang/String;

.field private static final e:Ljava/lang/String;

.field private static final f:C = '-'

.field private static final g:C = ' '

.field private static final h:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x2

    const-string v5, "\u001c\u001c\u0010BE\n\u0000k\u001fU<msPk\u001dU\u001c\u001c\u0010BE\n\u0000k\u001fU<msPk\u001dU\u001c\u001c\u0002\u001c\u001c:Xx4L|FL1\u007f&H~[Aw\u007f!LdFG\u007f61YbFFv,b]b@\\~u-A0YMce+B~\u000fF~bbK\u007fZFu8"

    move v7, v3

    const/4 v6, -0x1

    const/4 v8, 0x0

    const/16 v9, 0x62

    :goto_0
    const/16 v10, 0x4a

    const/4 v11, 0x1

    add-int/2addr v6, v11

    add-int v12, v6, v7

    invoke-virtual {v5, v6, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    const/4 v15, 0x0

    :goto_2
    const/4 v1, 0x4

    const/4 v2, 0x3

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_1

    add-int/lit8 v1, v8, 0x1

    aput-object v10, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v9, :cond_0

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v1

    goto :goto_0

    :cond_0
    const/16 v2, 0x1d

    const/16 v5, 0xe

    const-string v6, "FA\u000e\u0004o\u001bQ8iwTo\u0019Q\u000eFA\u000e\u0004o\u001bQ8iwTo\u0019Q"

    move v8, v1

    move v9, v2

    move v7, v5

    move-object v5, v6

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v8, 0x1

    aput-object v10, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v9, :cond_2

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v1

    move v8, v12

    :goto_3
    const/16 v10, 0x4e

    add-int/2addr v6, v11

    add-int v1, v6, v7

    invoke-virtual {v5, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->h:[Ljava/lang/String;

    aget-object v2, v0, v2

    sput-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->b:Ljava/lang/String;

    aget-object v2, v0, v3

    sput-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->d:Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v0, v0, v2

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->e:Ljava/lang/String;

    new-array v0, v1, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->c:[B

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v4, v15, 0x7

    if-eqz v4, :cond_9

    if-eq v4, v11, :cond_8

    if-eq v4, v3, :cond_7

    if-eq v4, v2, :cond_6

    if-eq v4, v1, :cond_5

    const/4 v1, 0x5

    if-eq v4, v1, :cond_4

    const/16 v1, 0x62

    goto :goto_4

    :cond_4
    const/16 v1, 0x65

    goto :goto_4

    :cond_5
    const/16 v1, 0x5a

    goto :goto_4

    :cond_6
    const/16 v1, 0x67

    goto :goto_4

    :cond_7
    const/16 v1, 0x8

    goto :goto_4

    :cond_8
    const/16 v1, 0x5c

    goto :goto_4

    :cond_9
    const/16 v1, 0x5b

    :goto_4
    xor-int/2addr v1, v10

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v12, v15

    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_2

    :array_0
    .array-data 1
        0x53t
        0x53t
        0x48t
        0x2dt
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(CLjava/io/StringReader;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    :cond_0
    invoke-virtual {p2}, Ljava/io/StringReader;->read()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    if-nez v0, :cond_2

    if-nez v0, :cond_2

    if-eq v2, p1, :cond_1

    const v4, 0xffff

    and-int/2addr v4, v2

    int-to-char v4, v4

    :try_start_0
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    if-nez v0, :cond_3

    move p1, v3

    :cond_2
    if-ne v2, p1, :cond_4

    if-nez v0, :cond_4

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    if-lez v2, :cond_5

    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_5
    const/4 p1, 0x0

    :goto_2
    return-object p1
.end method

.method private a(Ljava/io/StringReader;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x2d

    invoke-direct {p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(CLjava/io/StringReader;)Ljava/lang/String;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(CLjava/io/StringReader;)Ljava/lang/String;

    move-result-object p1

    if-nez v1, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    if-nez v1, :cond_2

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    :try_start_1
    new-instance p1, Ljava/io/IOException;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->h:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    return-object p1
.end method

.method private a(Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;)[B

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->b(Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;)[B

    move-result-object p2

    iput-object p2, p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->rawData:[B

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_4

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x3

    if-nez v0, :cond_1

    if-nez v1, :cond_4

    if-nez v0, :cond_3

    :try_start_1
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->h:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_1
    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->h:[Ljava/lang/String;

    aget-object p1, p1, v2

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_3
    :goto_0
    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write([B)V

    :cond_4
    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private a(Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;)[B
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->h:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->protocolVersion:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->softwareVersion:Ljava/lang/String;

    aput-object v4, v3, v2

    iget-object v2, p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->comments:Ljava/lang/String;

    if-nez v0, :cond_0

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->comments:Ljava/lang/String;

    :cond_0
    if-nez v0, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->comments:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    const-string p1, ""

    goto :goto_1

    :cond_2
    :goto_0
    move-object p1, v2

    :goto_1
    const/4 v0, 0x2

    aput-object p1, v3, v0

    invoke-static {v1, v3}, Lcom/jscape/util/af;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    sget-object v0, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    return-object p1
.end method

.method private b(Ljava/io/StringReader;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x20

    invoke-direct {p0, v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(CLjava/io/StringReader;)Ljava/lang/String;

    move-result-object p1

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    :cond_1
    :goto_0
    return-object p1
.end method

.method private b(Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;)[B
    .locals 5

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->h:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->protocolVersion:Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->softwareVersion:Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    iget-object v3, p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->comments:Ljava/lang/String;

    if-nez v0, :cond_0

    if-eqz v3, :cond_1

    iget-object v3, p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->comments:Ljava/lang/String;

    :cond_0
    if-nez v0, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x20

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->comments:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    const-string p1, ""

    goto :goto_1

    :cond_2
    :goto_0
    move-object p1, v3

    :goto_1
    const/4 v0, 0x2

    aput-object p1, v2, v0

    invoke-static {v1, v2}, Lcom/jscape/util/af;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    sget-object v0, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    return-object p1
.end method

.method private c(Ljava/io/StringReader;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    :cond_0
    invoke-virtual {p1}, Ljava/io/StringReader;->read()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    const v3, 0xffff

    and-int/2addr v2, v3

    int-to-char v2, v2

    :try_start_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_2

    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    if-nez v0, :cond_3

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-lez p1, :cond_2

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    goto :goto_2

    :catch_1
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_2
    return-object p1
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/a/CommonLineReader;

    const/16 v1, 0xff

    invoke-direct {v0, p1, v1}, Lcom/jscape/util/h/a/CommonLineReader;-><init>(Ljava/io/InputStream;I)V

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    invoke-static {v0}, Lcom/jscape/util/h/a/CommonLineReader;->readLine(Lcom/jscape/util/h/a/CommonLineReader;)[B

    move-result-object v4

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->c:[B

    invoke-static {v4, v5}, Lcom/jscape/util/v;->b([B[B)Z

    move-result v5

    if-nez v5, :cond_2

    add-int/lit8 v5, v3, 0x1

    if-nez p1, :cond_3

    if-lez v3, :cond_0

    :try_start_0
    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->h:[Ljava/lang/String;

    aget-object v3, v3, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_1
    new-instance v3, Ljava/lang/String;

    sget-object v6, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v3, v4, v6}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_1

    goto :goto_2

    :cond_1
    move v3, v5

    goto :goto_0

    :cond_2
    :goto_2
    :try_start_1
    invoke-virtual {v0}, Lcom/jscape/util/h/a/CommonLineReader;->consumeLF()V

    if-nez p1, :cond_5

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :cond_3
    if-lez v3, :cond_4

    goto :goto_3

    :cond_4
    const/4 p1, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_4
    new-instance v0, Ljava/io/StringReader;

    new-instance v1, Ljava/lang/String;

    sget-object v2, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-direct {v1, v4, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-direct {v0, v1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(Ljava/io/StringReader;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->b(Ljava/io/StringReader;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->c(Ljava/io/StringReader;)Ljava/lang/String;

    move-result-object v0

    :try_start_2
    new-instance v3, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    invoke-direct {v3, p1, v1, v2, v0}, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v4, v3, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->rawData:[B
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    return-object v3

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;->banner:Ljava/lang/String;

    invoke-direct {p0, v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(Ljava/lang/String;Ljava/io/OutputStream;)V

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->a(Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Ljava/io/OutputStream;)V

    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/IdentificationStringCodec;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
