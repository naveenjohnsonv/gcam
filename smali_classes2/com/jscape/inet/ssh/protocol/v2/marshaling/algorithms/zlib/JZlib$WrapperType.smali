.class public final enum Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ANY:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

.field public static final enum GZIP:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

.field public static final enum NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

.field public static final enum ZLIB:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

.field private static final a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "U1y}\u0004\\$~h"

    const/16 v5, 0x9

    move v7, v0

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x1e

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v14, v11

    move v15, v3

    :goto_2
    const/4 v2, 0x2

    const/4 v12, 0x3

    if-gt v14, v15, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v13, :cond_1

    add-int/lit8 v2, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v2

    goto :goto_0

    :cond_0
    const/16 v5, 0x8

    const-string v4, "\u000ba?\u0004\u0010c/+"

    move v8, v2

    move v7, v12

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v7, v2

    move v8, v11

    :goto_3
    add-int/2addr v6, v10

    add-int v2, v6, v7

    invoke-virtual {v4, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v13, v3

    const/16 v9, 0x4f

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    aget-object v5, v1, v3

    invoke-direct {v4, v5, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;->NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    aget-object v5, v1, v12

    invoke-direct {v4, v5, v10}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;->ZLIB:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    aget-object v5, v1, v10

    invoke-direct {v4, v5, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;->GZIP:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    aget-object v1, v1, v2

    invoke-direct {v4, v1, v12}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;->ANY:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;->NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;->ZLIB:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;->GZIP:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    aput-object v1, v0, v2

    aput-object v4, v0, v12

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;->a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    return-void

    :cond_3
    aget-char v16, v11, v15

    rem-int/lit8 v3, v15, 0x7

    const/16 v17, 0x60

    const/4 v0, 0x5

    if-eqz v3, :cond_8

    if-eq v3, v10, :cond_7

    if-eq v3, v2, :cond_6

    const/4 v2, 0x4

    if-eq v3, v12, :cond_5

    if-eq v3, v2, :cond_9

    if-eq v3, v0, :cond_4

    const/16 v17, 0x1c

    goto :goto_4

    :cond_4
    const/16 v17, 0x4f

    goto :goto_4

    :cond_5
    const/16 v17, 0x26

    goto :goto_4

    :cond_6
    const/4 v2, 0x4

    const/16 v17, 0x29

    goto :goto_4

    :cond_7
    const/4 v2, 0x4

    goto :goto_4

    :cond_8
    const/4 v2, 0x4

    move/from16 v17, v0

    :cond_9
    :goto_4
    xor-int v0, v9, v17

    xor-int v0, v16, v0

    int-to-char v0, v0

    aput-char v0, v11, v15

    add-int/lit8 v15, v15, 0x1

    move v0, v2

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;
    .locals 1

    const-class v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;->a:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    invoke-virtual {v0}, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    return-object v0
.end method
