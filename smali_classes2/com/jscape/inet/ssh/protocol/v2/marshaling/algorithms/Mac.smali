.class public interface abstract Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;
.super Ljava/lang/Object;


# static fields
.field public static final NULL:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$1;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$1;-><init>()V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;->NULL:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;

    return-void
.end method


# virtual methods
.method public abstract apply([B[BIILjava/io/OutputStream;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;
        }
    .end annotation
.end method

.method public abstract assertMacValid([B[BII[B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;
        }
    .end annotation
.end method

.method public abstract digestLength()I
.end method
