.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;
.super Ljava/lang/Object;


# static fields
.field public static final NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;

.field private static final a:[Ljava/lang/String;


# instance fields
.field public final algorithm:Ljava/lang/String;

.field public final blockLength:I

.field public final keyLength:I

.field public final name:Ljava/lang/String;

.field public final provider:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "\"4?Dv;T@9>\nz*EN\u0011\"4?D\u007f2B\u0003>{\u0008x0J\u0014=u\r%;/\u0016d~V\u000e46\u0001 y\rLu:\u0008z1_\t!3\t y\u000cLu+\u0016r(D\u00040)Y:\n\u0001\'8\u0002r+_R`m\u000cLu0\u0001d\u0012H\u000e2/\u000c \n\u0001\'8\u0002r+_Qgc"

    const/16 v4, 0x6d

    const/16 v5, 0xf

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0xe

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    const/16 v14, 0x13

    const/4 v15, 0x4

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const-string v3, "VbmY\u000e\u0014-aP*e\u001ethm[1nH"

    move v7, v10

    move v4, v14

    move v5, v15

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x56

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry$1;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v12, v1, v2

    const/4 v14, 0x0

    const/16 v15, 0x8

    const/16 v16, 0x1

    const-string v13, ""

    move-object v11, v0

    invoke-direct/range {v11 .. v16}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry$1;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;

    return-void

    :cond_3
    aget-char v16, v10, v13

    rem-int/lit8 v1, v13, 0x7

    if-eqz v1, :cond_8

    if-eq v1, v9, :cond_7

    const/4 v2, 0x2

    if-eq v1, v2, :cond_6

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    if-eq v1, v15, :cond_9

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v14, 0x23

    goto :goto_4

    :cond_4
    const/16 v14, 0x50

    goto :goto_4

    :cond_5
    const/16 v14, 0x6a

    goto :goto_4

    :cond_6
    const/16 v14, 0x55

    goto :goto_4

    :cond_7
    const/16 v14, 0x5b

    goto :goto_4

    :cond_8
    const/16 v14, 0x6e

    :cond_9
    :goto_4
    xor-int v1, v8, v14

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->name:Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->algorithm:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->provider:Ljava/lang/Object;

    int-to-long p1, p4

    sget-object p3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a:[Ljava/lang/String;

    const/4 v0, 0x1

    aget-object v0, p3, v0

    const-wide/16 v1, 0x0

    invoke-static {p1, p2, v1, v2, v0}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    iput p4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->blockLength:I

    int-to-long p1, p5

    const/4 p4, 0x0

    aget-object p3, p3, p4

    invoke-static {p1, p2, v1, v2, p3}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->keyLength:I

    return-void
.end method

.method private a([B[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->c()Z

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->provider:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->algorithm:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->provider:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;->encryptionFor(Ljava/lang/String;Ljava/lang/String;[B[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_1

    :cond_0
    if-eqz v0, :cond_2

    :try_start_2
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->provider:Ljava/lang/Object;

    instance-of v1, v0, Ljava/security/Provider;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    :try_start_3
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->algorithm:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->provider:Ljava/lang/Object;

    check-cast v1, Ljava/security/Provider;

    invoke-static {v0, v1, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;->encryptionFor(Ljava/lang/String;Ljava/security/Provider;[B[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    move-result-object p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->algorithm:Ljava/lang/String;

    const/4 v1, 0x0

    check-cast v1, Ljava/lang/String;

    invoke-static {v0, v1, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryption;->encryptionFor(Ljava/lang/String;Ljava/lang/String;[B[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    move-result-object p1

    :goto_1
    return-object p1

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->c()Z

    move-result v0

    :try_start_0
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a:[Ljava/lang/String;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :try_start_1
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    if-eqz v1, :cond_2

    :cond_1
    :try_start_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->b(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    return-void

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private b(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/16 v0, 0x600

    new-array v5, v0, [B

    const/4 v3, 0x0

    const/16 v4, 0x600

    const/4 v6, 0x0

    move-object v1, p1

    move-object v2, v5

    invoke-interface/range {v1 .. v6}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;->apply([BII[BI)V

    return-void
.end method

.method public static entryFor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;
    .locals 7

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    return-object v6
.end method

.method public static entryFor(Ljava/lang/String;Ljava/lang/String;Ljava/security/Provider;II)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;
    .locals 7

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    return-object v6
.end method


# virtual methods
.method public clientToServer(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$Mode;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$OperationException;
        }
    .end annotation

    :try_start_0
    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->blockLength:I

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->ivClientToServer(I)[B

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->keyLength:I

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->encryptionKeyClientToServer(I)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$Mode;->ENCRYPTION:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$Mode;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-ne p2, v1, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x2

    :goto_0
    :try_start_2
    invoke-direct {p0, p1, v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a([B[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;)V

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$OperationException;

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$OperationException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public serverToClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$Mode;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$OperationException;
        }
    .end annotation

    :try_start_0
    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->blockLength:I

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->ivServerToClient(I)[B

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->keyLength:I

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;->encryptionKeyServerToClient(I)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$Mode;->ENCRYPTION:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$Mode;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-ne p2, v1, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x2

    :goto_0
    :try_start_2
    invoke-direct {p0, p1, v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a([B[BI)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Encryption;)V

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$OperationException;

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory$OperationException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->a:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x3

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->algorithm:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->provider:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->blockLength:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->keyLength:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
