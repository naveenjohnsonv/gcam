.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;
.super Ljava/lang/Object;


# instance fields
.field private a:[B

.field private b:I

.field private final c:Lcom/jscape/util/h/e;

.field private final d:Lcom/jscape/util/h/o;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->a:[B

    const/4 p1, 0x0

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->b:I

    new-instance p1, Lcom/jscape/util/h/e;

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->a:[B

    invoke-direct {p1, v0}, Lcom/jscape/util/h/e;-><init>([B)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->c:Lcom/jscape/util/h/e;

    new-instance p1, Lcom/jscape/util/h/o;

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->a:[B

    invoke-direct {p1, v0}, Lcom/jscape/util/h/o;-><init>([B)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->d:Lcom/jscape/util/h/o;

    return-void
.end method

.method private a()I
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->a:[B

    array-length v0, v0

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private a(I)V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->a()I

    move-result v1

    if-nez v0, :cond_0

    if-ge v1, p1, :cond_1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->a:[B

    array-length v0, v0

    mul-int/lit8 v1, v0, 0x2

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->b:I

    add-int/2addr p1, v0

    :cond_0
    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->a:[B

    new-array p1, p1, [B

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->b:I

    const/4 v2, 0x0

    invoke-static {v0, v2, p1, v2, v1}, Lcom/jscape/util/v;->a([BI[BII)[B

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->a:[B

    :cond_1
    return-void
.end method


# virtual methods
.method public asInputStream(II)Ljava/io/InputStream;
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->c:Lcom/jscape/util/h/e;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->a:[B

    invoke-virtual {v0, v1, p1, p2}, Lcom/jscape/util/h/e;->a([BII)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->c:Lcom/jscape/util/h/e;

    return-object p1
.end method

.method public asOutputStream(I)Ljava/io/OutputStream;
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->d:Lcom/jscape/util/h/o;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->a:[B

    invoke-virtual {v0, v1, p1}, Lcom/jscape/util/h/o;->a([BI)Lcom/jscape/util/h/o;

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->d:Lcom/jscape/util/h/o;

    return-object p1
.end method

.method public at(I)I
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->a:[B

    aget-byte p1, v0, p1

    and-int/lit16 p1, p1, 0xff

    return p1
.end method

.method public buffer()[B
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->a:[B

    return-object v0
.end method

.method public fill(Ljava/io/InputStream;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->a(I)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->a:[B

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->b:I

    invoke-static {v0, v1, p2, p1}, Lcom/jscape/util/X;->a([BIILjava/io/InputStream;)[B

    iget p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->b:I

    add-int/2addr p1, p2

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->b:I

    return-void
.end method

.method public position()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->b:I

    return v0
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/PacketCodec$ReadingBuffer;->b:I

    return-void
.end method
