.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "vtJ2g\u0008a[yJ\u0005$ u"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;->b:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x3f

    goto :goto_1

    :cond_1
    const/16 v4, 0x6c

    goto :goto_1

    :cond_2
    const/16 v4, 0x33

    goto :goto_1

    :cond_3
    const/16 v4, 0x40

    goto :goto_1

    :cond_4
    const/16 v4, 0x18

    goto :goto_1

    :cond_5
    const/16 v4, 0x20

    goto :goto_1

    :cond_6
    const/16 v4, 0xb

    :goto_1
    const/16 v5, 0x37

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;->a:Ljava/lang/Object;

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public assertSignatureValid([BLjava/security/PublicKey;Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;->a:Ljava/lang/Object;

    invoke-static {p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->signatureFor(Ljava/security/PublicKey;Ljava/lang/Object;)Ljava/security/Signature;

    move-result-object p2

    :try_start_0
    invoke-virtual {p2, p1}, Ljava/security/Signature;->update([B)V

    iget-object p1, p3, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;->blob:[B

    invoke-virtual {p2, p1}, Ljava/security/Signature;->verify([B)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer$InvalidSignatureException;

    invoke-direct {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signer$InvalidSignatureException;-><init>()V

    throw p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public sign([BLjava/security/PrivateKey;)Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {p2}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifierFor(Ljava/security/Key;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;->a:Ljava/lang/Object;

    invoke-static {p2, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->signatureFor(Ljava/security/PrivateKey;Ljava/lang/Object;)Ljava/security/Signature;

    move-result-object p2

    invoke-virtual {p2, p1}, Ljava/security/Signature;->update([B)V

    invoke-virtual {p2}, Ljava/security/Signature;->sign()[B

    move-result-object p1

    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;

    invoke-direct {p2, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/Signature;-><init>(Ljava/lang/String;[B)V

    return-object p2
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/JceEcSigner;->b:Ljava/lang/String;

    return-object v0
.end method
