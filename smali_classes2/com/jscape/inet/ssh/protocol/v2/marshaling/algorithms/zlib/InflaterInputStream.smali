.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;
.super Ljava/io/FilterInputStream;


# static fields
.field protected static final DEFAULT_BUFSIZE:I = 0x200

.field private static final f:[Ljava/lang/String;


# instance fields
.field private a:Z

.field private b:Z

.field protected buf:[B

.field private c:Z

.field private d:[B

.field private e:[B

.field protected final inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

.field protected myinflater:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "\u0015p\u0000[E\u0003h\u001alOAO\u0005h\u0015p\u001aAD\u0014\u001dz\u0008NT\u0018>\u0016?\u001cDI\u0001h\u001fz\u0001HT\u0019\u0018\u001e~\u001dD\u000f\u0003-\u0000z\u001b\u000fN\u001e<Sl\u001a_P\u001e:\u0007z\u000b\r k\u001dJA\u001ch\u0010s\u0000\\E\u0015\"\u0011j\tIE\u0003h\u0000v\u0015J\u0000\u001c=\u0000kOMEQ/\u0001z\u000e[E\u0003h\u0007w\u000eA\u0000A\u0008\u001dpOFN\u0001=\u0007"

    const/16 v4, 0x7b

    const/16 v5, 0x13

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x4d

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x31

    const/16 v3, 0xd

    const-string v5, "\u0018S%ry$P(K8d}-#\u001eI2oh,\u0013?B37}\'\u0014kH17B\u00059\t\u0007>yh<\u0004kT#e}(\u001d"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x75

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->f:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    const/4 v1, 0x5

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v2, 0x2

    if-eq v15, v2, :cond_7

    const/4 v2, 0x3

    if-eq v15, v2, :cond_6

    const/4 v2, 0x4

    if-eq v15, v2, :cond_5

    if-eq v15, v1, :cond_4

    goto :goto_4

    :cond_4
    const/16 v1, 0x3c

    goto :goto_4

    :cond_5
    const/16 v1, 0x6d

    goto :goto_4

    :cond_6
    const/16 v1, 0x62

    goto :goto_4

    :cond_7
    const/16 v1, 0x22

    goto :goto_4

    :cond_8
    const/16 v1, 0x52

    goto :goto_4

    :cond_9
    const/16 v1, 0x3e

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;-><init>(Ljava/io/InputStream;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x200

    invoke-direct {p0, p1, p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;-><init>(Ljava/io/InputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;I)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;-><init>(Ljava/io/InputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;IZ)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;IZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->b:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->c:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->myinflater:Z

    new-array v0, v1, [B

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->d:[B

    const/16 v0, 0x200

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->e:[B

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    if-lez p3, :cond_0

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    new-array p1, p3, [B

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->buf:[B

    iput-boolean p4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->c:Z

    return-void

    :cond_0
    :try_start_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    sget-object p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->f:[Ljava/lang/String;

    const/4 p3, 0x4

    aget-object p2, p2, p3

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :try_start_1
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    throw p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public constructor <init>(Ljava/io/InputStream;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-direct {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;-><init>(Z)V

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;-><init>(Ljava/io/InputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->myinflater:Z

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public available()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->b:Z

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->f:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    if-nez v0, :cond_3

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    return v0

    :cond_2
    const/4 v1, 0x1

    :cond_3
    return v1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    if-nez v0, :cond_0

    if-nez v1, :cond_4

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->myinflater:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    :cond_0
    if-nez v0, :cond_2

    if-eqz v1, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->end()I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    if-nez v0, :cond_3

    :try_start_3
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->c:Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    :try_start_4
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_3
    :goto_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a:Z

    :cond_4
    return-void

    :catch_3
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method protected fill()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6

    const/4 v2, 0x0

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->in:Ljava/io/InputStream;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->buf:[B

    array-length v4, v3

    invoke-virtual {v1, v3, v2, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->f:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7

    :cond_1
    :goto_0
    if-nez v0, :cond_7

    const/4 v3, -0x1

    const/4 v4, 0x1

    if-ne v1, v3, :cond_6

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    if-nez v0, :cond_4

    if-nez v1, :cond_3

    :try_start_3
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->finished()Z

    move-result v1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    if-nez v0, :cond_4

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->buf:[B

    aput-byte v2, v1, v2

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    move v1, v4

    goto :goto_3

    :cond_3
    :goto_1
    :try_start_4
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    iget-wide v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->S:J
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    const-wide/16 v3, -0x1

    cmp-long v1, v0, v3

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_4
    :goto_2
    if-eqz v1, :cond_5

    :try_start_5
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->f:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_5
    new-instance v0, Ljava/io/EOFException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->f:[Ljava/lang/String;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_2
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    :catch_3
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :catch_4
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    :catch_5
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_6
    :goto_3
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->buf:[B

    invoke-virtual {v0, v3, v2, v1, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->setInput([BIIZ)V

    :cond_7
    return-void

    :catch_6
    move-exception v0

    :try_start_9
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    :catch_7
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public getAvailIn()[B
    .locals 5

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->availIn:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    if-gtz v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->availIn:I

    :cond_1
    new-array v0, v1, [B

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->nextIn:[B

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->nextInIndex:I

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->availIn:I

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public getInflater()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    return-object v0
.end method

.method public getTotalIn()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->getTotalIn()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalOut()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->getTotalOut()J

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized mark(I)V
    .locals 0

    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public markSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x0

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->d:[B

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->read([BII)I

    move-result v1

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->f:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_0
    const/4 v3, -0x1

    if-nez v0, :cond_3

    if-ne v1, v3, :cond_2

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->d:[B

    aget-byte v1, v0, v2

    const/16 v3, 0xff

    :cond_3
    and-int/2addr v3, v1

    :goto_1
    return v3

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public read([BII)I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_9

    if-nez v1, :cond_13

    if-eqz p1, :cond_12

    if-nez v0, :cond_0

    if-ltz p2, :cond_2

    move v1, p3

    goto :goto_0

    :cond_0
    move v1, p2

    :goto_0
    if-nez v0, :cond_1

    if-ltz v1, :cond_2

    move v1, p3

    :cond_1
    if-nez v0, :cond_3

    :try_start_1
    array-length v2, p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    sub-int/2addr v2, p2

    if-gt v1, v2, :cond_2

    move v1, p3

    goto :goto_2

    :cond_2
    :try_start_2
    new-instance p1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {p1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw p1

    :catch_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_1
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_2
    const/4 v2, 0x0

    if-nez v0, :cond_5

    if-nez v1, :cond_4

    return v2

    :cond_4
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->b:Z

    :cond_5
    const/4 v3, -0x1

    if-nez v0, :cond_7

    if-eqz v1, :cond_6

    return v3

    :cond_6
    move v1, v2

    :cond_7
    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v4, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->setOutput([BII)V

    :cond_8
    iget-boolean p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->b:Z

    if-nez p1, :cond_10

    :try_start_3
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->availIn:I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    if-nez v0, :cond_11

    if-nez v0, :cond_a

    if-nez p1, :cond_9

    :try_start_4
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->fill()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7

    :cond_9
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {p1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->inflate(I)I

    move-result p1

    :cond_a
    iget-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget p3, p3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->nextOutIndex:I

    sub-int/2addr p3, p2

    add-int/2addr v1, p3

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget p2, p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->nextOutIndex:I

    if-nez v0, :cond_e

    const/4 p3, -0x3

    if-eq p1, p3, :cond_d

    const/4 p3, 0x2

    const/4 v4, 0x1

    if-eq p1, v4, :cond_b

    if-eq p1, p3, :cond_b

    goto :goto_3

    :cond_b
    :try_start_5
    iput-boolean v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->b:Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    if-nez v0, :cond_e

    if-ne p1, p3, :cond_c

    return v3

    :cond_c
    :goto_3
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->availOut:I

    goto :goto_4

    :catch_2
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_d
    :try_start_7
    new-instance p1, Ljava/io/IOException;

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object p2, p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->msg:Ljava/lang/String;

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_e
    :goto_4
    if-nez p1, :cond_f

    goto :goto_5

    :cond_f
    if-eqz v0, :cond_8

    goto :goto_5

    :catch_5
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    :catch_6
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    :catch_7
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_10
    :goto_5
    move p1, v1

    :cond_11
    return p1

    :cond_12
    :try_start_a
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    throw p1
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    :catch_8
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_13
    :try_start_b
    new-instance p1, Ljava/io/IOException;

    sget-object p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->f:[Ljava/lang/String;

    const/4 p3, 0x6

    aget-object p2, p2, p3

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_9

    :catch_9
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public readHeader()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [B

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v2, v1, v0, v0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->setInput([BIIZ)V

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v3, v1, v0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->setOutput([BII)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->inflate(I)I

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->e()Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v2, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x1

    :cond_1
    new-array v1, v1, [B

    :cond_2
    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v3, v1}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_5

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v3, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->setInput([B)V

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->inflate(I)I

    move-result v3

    if-nez v2, :cond_4

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    invoke-virtual {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->e()Z

    move-result v3

    goto :goto_0

    :cond_3
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->msg:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_4
    :goto_0
    if-nez v3, :cond_2

    return-void

    :cond_5
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->f:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public declared-synchronized reset()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->f:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public skip(J)J
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    const/4 v2, 0x1

    if-nez v0, :cond_1

    if-ltz v1, :cond_0

    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a:Z

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    sget-object p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->f:[Ljava/lang/String;

    aget-object p2, p2, v2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    if-nez v0, :cond_3

    if-nez v1, :cond_2

    const-wide/32 v3, 0x7fffffff

    invoke-static {p1, p2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    long-to-int v1, p1

    goto :goto_1

    :cond_2
    :try_start_1
    new-instance p1, Ljava/io/IOException;

    sget-object p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->f:[Ljava/lang/String;

    const/4 v0, 0x6

    aget-object p2, p2, v0

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    const/4 p1, 0x0

    move p2, p1

    :cond_4
    if-ge p2, v1, :cond_9

    sub-int v3, v1, p2

    if-nez v0, :cond_a

    :try_start_2
    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->e:[B

    array-length v4, v4
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    if-nez v0, :cond_6

    if-le v3, v4, :cond_5

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->e:[B

    array-length v3, v3

    :cond_5
    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->e:[B

    invoke-virtual {p0, v4, p1, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->read([BII)I

    move-result v3

    const/4 v4, -0x1

    :cond_6
    if-nez v0, :cond_7

    if-ne v3, v4, :cond_8

    :try_start_3
    iput-boolean v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->b:Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    if-eqz v0, :cond_9

    goto :goto_2

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_7
    move p2, v3

    move v3, v4

    :cond_8
    :goto_2
    add-int/2addr p2, v3

    if-eqz v0, :cond_4

    goto :goto_3

    :catch_4
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_9
    :goto_3
    move v3, p2

    :cond_a
    int-to-long p1, v3

    return-wide p1
.end method
