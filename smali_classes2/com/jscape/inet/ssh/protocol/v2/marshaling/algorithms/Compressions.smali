.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compressions;
.super Ljava/lang/Object;


# static fields
.field public static final ALL:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

.field public static final NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

.field public static final ZLIB:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const-string v0, "\u0017j\u001c2"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    const/4 v4, 0x2

    const/4 v5, 0x1

    if-gt v1, v3, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;->NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compressions;->NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compressions$1;

    invoke-direct {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compressions$1;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compressions;->ZLIB:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

    new-array v0, v4, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compressions;->NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

    aput-object v3, v0, v2

    aput-object v1, v0, v5

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compressions;->ALL:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

    return-void

    :cond_0
    aget-char v6, v0, v3

    rem-int/lit8 v7, v3, 0x7

    if-eqz v7, :cond_6

    if-eq v7, v5, :cond_5

    if-eq v7, v4, :cond_4

    const/4 v4, 0x3

    if-eq v7, v4, :cond_3

    const/4 v4, 0x4

    if-eq v7, v4, :cond_2

    const/4 v4, 0x5

    if-eq v7, v4, :cond_1

    const/16 v4, 0x77

    goto :goto_1

    :cond_1
    const/16 v4, 0x26

    goto :goto_1

    :cond_2
    const/16 v4, 0x4f

    goto :goto_1

    :cond_3
    const/16 v4, 0x5a

    goto :goto_1

    :cond_4
    const/16 v4, 0x7f

    goto :goto_1

    :cond_5
    const/16 v4, 0xc

    goto :goto_1

    :cond_6
    const/16 v4, 0x67

    :goto_1
    const/16 v5, 0xa

    xor-int/2addr v4, v5

    xor-int/2addr v4, v6

    int-to-char v4, v4

    aput-char v4, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static initAll(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;
    .locals 0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compressions;->initRequired(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compressions;->initOptional(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAllClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;->NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compressions;->ZLIB:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initOptional(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compressions;->ZLIB:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;

    return-object p0
.end method

.method public static initRequired(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;->NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;

    return-object p0
.end method
