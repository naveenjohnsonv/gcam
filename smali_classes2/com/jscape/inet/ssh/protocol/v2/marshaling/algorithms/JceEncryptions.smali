.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;
.super Ljava/lang/Object;


# static fields
.field public static final AES128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final AES128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final AES192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final AES192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final AES256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final AES256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final ALL:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final ARCFOUR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final ARCFOUR128:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final ARCFOUR256:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final BLOWFISH_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final BLOWFISH_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final CAST_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final CAST_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final FIPS:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final IDEA_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final IDEA_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final SERPENT128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final SERPENT128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final SERPENT192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final SERPENT192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final SERPENT256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final SERPENT256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final THREE_DES_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final THREE_DES_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final TWOFISH128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final TWOFISH128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final TWOFISH192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final TWOFISH192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final TWOFISH256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final TWOFISH256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

.field public static final TWOFISH_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;


# direct methods
.method static constructor <clinit>()V
    .locals 36

    const/16 v0, 0x35

    new-array v0, v0, [Ljava/lang/String;

    const-string v4, "Y\u001f(g,,\"%\u00198tF,\u0019Z;\u001eS\u0000\u000c\u0011\u0014N\u001f)R\r\u0007YI\u000e(\u0018\'\r&k>\u001e^\u0007\u0005\nk?\t\u0005\\T[i8\u0019\u000e~-\u0015Q\u0000\u0011\u001e;cH\u001a\n\u0016\u0004\u0016H6\u0015@\u000f\u000b\u0005bu9c;M8e\n\u001bS\r\u000b\u0018m\u000bi;\tCXPN\'9\u000eE\u0013I\u001b)c\\M5^\u0008Uy\u00062\u0017n>\u0013Y\u000e\u00089>\u001fDD\u0001\u0014i\u0004d5\u0014R\u0015^-\u0015Q\u0000\u0011\u001e%\u00198tF,\u0019Z;\u001eS\u0000\u000c\u0011\u0011K\u001f)\u0018* 5%\u0014\u0015g\u0008\u0006\u0012c4\u001d\u000e~-\u0015Q\u0000\u0011\u001e;cH\u001a\n\u0000\u0015\u000ey?\u0008G\u000c\u000c\u0002;hB\u001a\n\u0016\u0004\u0012C\u001e?vF!4Iu4X9\u0003\u0012n3\u0014P\u0015^-\u0015Q\u0000\u0011\u001e%\u00198tF,\u0019Z;\u001eS\u0000\u000c\u0011\u0011K\u001f)\u0018* 5%\u0014\u0015g\u0008\u0006\u0012c4\u001d\u000e~-\u0015Q\u0000\u0011\u001e8oL\u001a\n\u0016\u0004\nk?\t\u0005\\T[i.\u0008\u00089>\u001fDD\u0001\u0002x\nk?\t\u0006PP[i8\u0019\u0015Y\u001f(g,,\"%\u0019.eF,\u0019Z;\u001eS\u0000\u000c\u0011\nk(\u0019Q\u0006\u0017\u0004;hB\u000e~-\u0015Q\u0000\u0011\u001e;hB\u001a\n\u0000\u0015\u000b~-\u0015Q\u0000\u0011\u001e\'9\u0018T\u0015^-\u0015Q\u0000\u0011\u001e%\u0019.eF,\u0019Z;\u001eS\u0000\u000c\u0011\u0015Y\u001f(g,,\"%\u00198tF,\u0019Z;\u001eS\u0000\u000c\u0011\u0011K\u001f)\u0018*6$%\u0014\u0015g\u0008\u0006\u0012c4\u001d\u000ey?\u0008G\u000c\u000c\u0002;cH\u001a\n\u0000\u0015\u000ey?\u0008G\u000c\u000c\u00028oL\u001a\n\u0000\u0015\u0013I\u001b)c\\M5H\u0019Uy\u00062\u0017n>\u0013Y\u000e\u000bi;\tCXPN\'9\u0018T\u0008c>\u001fVD\u0001\u0014i\nk?\t\u0006[Z[i8\u0019\u000ch6\u0015@\u000f\u000b\u0005bw\u0019U\n\u0014N\u001f)R\r\u0007YI\u00189\u0018\'\r&k>\u001e^\u0007\u0005\u000e~-\u0015Q\u0000\u0011\u001e;hB\u001a\n\u0016\u0004\u0007k(\u0019Q\u0006\u0017\u0004\u000ey?\u0008G\u000c\u000c\u00028oL\u001a\n\u0016\u0004\nk?\t\u0006PP[i.\u0008\u000ey?\u0008G\u000c\u000c\u0002;hB\u001a\n\u0000\u0015\u000ch6\u0015@\u000f\u000b\u0005bw\u0019C\u001b\u000ey?\u0008G\u000c\u000c\u0002;cH\u001a\n\u0016\u0004\u0004K\u00089\u0003\u0008c>\u001fVD\u0001\u0002x\nk?\t\u0006[Z[i.\u0008\nk(\u0019Q\u0006\u0017\u00048oL\u000e~-\u0015Q\u0000\u0011\u001e8oL\u001a\n\u0000\u0015\u0016H6\u0015@\u000f\u000b\u0005bu9u*M8e\n\u001bS\r\u000b\u0018m\u0015Y\u001f(g,,\"%\u0019.eF,\u0019Z;\u001eS\u0000\u000c\u0011\u0011K\u001f)\u0018*6$%\u0014\u0015g\u0008\u0006\u0012c4\u001d\u0012C\u001e?vF!\"Xu4X9\u0003\u0012n3\u0014P"

    const/16 v5, 0x307

    const/4 v6, -0x1

    const/16 v7, 0x15

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x73

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x31

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v14, v11

    const/4 v15, 0x0

    :goto_2
    const/16 v16, 0x29

    const/16 v17, 0x11

    const/16 v18, 0x9

    const/16 v19, 0x1a

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x4

    if-gt v14, v15, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v13, :cond_1

    add-int/lit8 v1, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v1

    goto :goto_0

    :cond_0
    const-string v4, "\tJ{A\u0015\u001coW\u0013BS\\g[l\'\u0004n[\u0018y\\\u0011BNS"

    move v8, v1

    move v7, v3

    move/from16 v5, v19

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v1

    move v8, v11

    :goto_3
    add-int/2addr v6, v10

    add-int v1, v6, v7

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v9, v12

    const/4 v13, 0x0

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$1;

    const/16 v5, 0x8

    aget-object v25, v0, v5

    const/16 v27, 0x0

    const/16 v28, 0x8

    const/16 v29, 0x1

    const-string v26, ""

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$1;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v6, 0x7

    aget-object v31, v0, v6

    const/16 v7, 0x22

    aget-object v32, v0, v7

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v33

    const/16 v34, 0x8

    const/16 v35, 0x18

    move-object/from16 v30, v4

    invoke-direct/range {v30 .. v35}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->THREE_DES_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v7, 0x12

    aget-object v25, v0, v7

    aget-object v26, v0, v10

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v29, 0x18

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->THREE_DES_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v8, 0x21

    aget-object v31, v0, v8

    const/16 v8, 0x2f

    aget-object v32, v0, v8

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v33

    const/16 v35, 0x10

    move-object/from16 v30, v4

    invoke-direct/range {v30 .. v35}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->BLOWFISH_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v8, 0x28

    aget-object v25, v0, v8

    aget-object v26, v0, v3

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v29, 0x10

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->BLOWFISH_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v8, 0x16

    aget-object v31, v0, v8

    aget-object v32, v0, v18

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v33

    const/16 v34, 0x10

    move-object/from16 v30, v4

    invoke-direct/range {v30 .. v35}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v8, 0x23

    aget-object v25, v0, v8

    const/16 v8, 0x34

    aget-object v26, v0, v8

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v28, 0x10

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v8, 0xb

    aget-object v31, v0, v8

    const/16 v8, 0xe

    aget-object v32, v0, v8

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v33

    const/16 v35, 0x18

    move-object/from16 v30, v4

    invoke-direct/range {v30 .. v35}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aget-object v25, v0, v1

    const/16 v9, 0x18

    aget-object v26, v0, v9

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v29, 0x18

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v11, 0x17

    aget-object v31, v0, v11

    aget-object v32, v0, v8

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v33

    const/16 v35, 0x20

    move-object/from16 v30, v4

    invoke-direct/range {v30 .. v35}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v11, 0x2e

    aget-object v25, v0, v11

    aget-object v26, v0, v8

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v29, 0x20

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v11, 0x10

    aget-object v31, v0, v11

    aget-object v32, v0, v9

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v33

    move-object/from16 v30, v4

    invoke-direct/range {v30 .. v35}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v11, 0x20

    aget-object v25, v0, v11

    const/16 v11, 0xa

    aget-object v26, v0, v11

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v29, 0x10

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v11, 0x2c

    aget-object v31, v0, v11

    aget-object v32, v0, v19

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v33

    const/16 v35, 0x10

    move-object/from16 v30, v4

    invoke-direct/range {v30 .. v35}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v11, 0x13

    aget-object v25, v0, v11

    const/16 v11, 0xf

    aget-object v26, v0, v11

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v29, 0x18

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v13, 0x26

    aget-object v31, v0, v13

    aget-object v32, v0, v12

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v33

    const/16 v35, 0x18

    move-object/from16 v30, v4

    invoke-direct/range {v30 .. v35}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aget-object v25, v0, v2

    aget-object v26, v0, v11

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v29, 0x20

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aget-object v31, v0, v17

    aget-object v32, v0, v12

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v33

    const/16 v35, 0x20

    move-object/from16 v30, v4

    invoke-direct/range {v30 .. v35}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v12, 0x27

    aget-object v25, v0, v12

    const/4 v12, 0x0

    aget-object v26, v0, v12

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v29, 0x10

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v12, 0xc

    aget-object v31, v0, v12

    const/16 v12, 0x30

    aget-object v32, v0, v12

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v33

    const/16 v35, 0x10

    move-object/from16 v30, v4

    invoke-direct/range {v30 .. v35}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v12, 0x1b

    aget-object v25, v0, v12

    const/16 v12, 0x19

    aget-object v26, v0, v12

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v29, 0x18

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aget-object v31, v0, v16

    const/16 v13, 0x14

    aget-object v32, v0, v13

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v33

    const/16 v35, 0x18

    move-object/from16 v30, v4

    invoke-direct/range {v30 .. v35}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v14, 0x1c

    aget-object v25, v0, v14

    aget-object v26, v0, v12

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v29, 0x20

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v14, 0x25

    aget-object v31, v0, v14

    aget-object v32, v0, v13

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v33

    const/16 v35, 0x20

    move-object/from16 v30, v4

    invoke-direct/range {v30 .. v35}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v14, 0x1f

    aget-object v25, v0, v14

    const/16 v14, 0xd

    aget-object v26, v0, v14

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v28, 0x8

    const/16 v29, 0x10

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->IDEA_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v14, 0x2b

    aget-object v31, v0, v14

    const/16 v14, 0x32

    aget-object v32, v0, v14

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v33

    const/16 v34, 0x8

    const/16 v35, 0x10

    move-object/from16 v30, v4

    invoke-direct/range {v30 .. v35}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->IDEA_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v14, 0x1e

    aget-object v25, v0, v14

    const/16 v14, 0x1d

    aget-object v26, v0, v14

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->CAST_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v14, 0x5

    aget-object v31, v0, v14

    const/4 v14, 0x6

    aget-object v32, v0, v14

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v33

    move-object/from16 v30, v4

    invoke-direct/range {v30 .. v35}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->CAST_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v15, 0x24

    aget-object v25, v0, v15

    const/16 v15, 0x2a

    aget-object v26, v0, v15

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v28, 0x0

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v15, 0x15

    aget-object v31, v0, v15

    const/16 v15, 0x33

    aget-object v32, v0, v15

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v33

    const/16 v34, 0x0

    move-object/from16 v30, v4

    invoke-direct/range {v30 .. v35}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR128:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-instance v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v15, 0x2d

    aget-object v25, v0, v15

    const/16 v15, 0x33

    aget-object v26, v0, v15

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v27

    const/16 v29, 0x20

    move-object/from16 v24, v4

    invoke-direct/range {v24 .. v29}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;II)V

    sput-object v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR256:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v0, 0x1e

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    sget-object v15, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->THREE_DES_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v16, 0x0

    aput-object v15, v0, v16

    sget-object v16, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->THREE_DES_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v16, v0, v10

    sget-object v20, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->BLOWFISH_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v20, v0, v2

    sget-object v20, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->BLOWFISH_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v20, v0, v1

    sget-object v20, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v20, v0, v3

    sget-object v20, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v23, 0x5

    aput-object v20, v0, v23

    sget-object v20, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v20, v0, v14

    sget-object v20, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v20, v0, v6

    sget-object v20, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v20, v0, v5

    sget-object v20, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v20, v0, v18

    const/16 v18, 0xa

    sget-object v20, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v20, v0, v18

    const/16 v18, 0xb

    sget-object v20, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v20, v0, v18

    const/16 v18, 0xc

    sget-object v24, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v24, v0, v18

    const/16 v18, 0xd

    sget-object v25, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v25, v0, v18

    sget-object v18, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v18, v0, v8

    sget-object v8, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v8, v0, v11

    const/16 v11, 0x10

    sget-object v26, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v26, v0, v11

    sget-object v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v11, v0, v17

    sget-object v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v11, v0, v7

    const/16 v7, 0x13

    sget-object v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v11, v0, v7

    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v7, v0, v13

    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v21, 0x15

    aput-object v7, v0, v21

    const/16 v7, 0x16

    sget-object v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v11, v0, v7

    const/16 v7, 0x17

    sget-object v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->IDEA_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v11, v0, v7

    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->IDEA_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v7, v0, v9

    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->CAST_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v7, v0, v12

    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->CAST_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v7, v0, v19

    const/16 v7, 0x1b

    sget-object v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v9, v0, v7

    const/16 v7, 0x1c

    sget-object v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR128:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    aput-object v9, v0, v7

    const/16 v7, 0x1d

    aput-object v4, v0, v7

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ALL:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    new-array v0, v5, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v22, 0x0

    aput-object v15, v0, v22

    aput-object v16, v0, v10

    aput-object v20, v0, v2

    aput-object v25, v0, v1

    aput-object v8, v0, v3

    const/4 v1, 0x5

    aput-object v24, v0, v1

    aput-object v18, v0, v14

    aput-object v26, v0, v6

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->FIPS:[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    return-void

    :cond_3
    const/16 v21, 0x15

    const/16 v22, 0x0

    aget-char v24, v11, v15

    rem-int/lit8 v12, v15, 0x7

    if-eqz v12, :cond_8

    if-eq v12, v10, :cond_9

    if-eq v12, v2, :cond_7

    if-eq v12, v1, :cond_6

    if-eq v12, v3, :cond_5

    const/4 v1, 0x5

    if-eq v12, v1, :cond_4

    move/from16 v16, v1

    goto :goto_4

    :cond_4
    move/from16 v16, v17

    goto :goto_4

    :cond_5
    move/from16 v16, v19

    goto :goto_4

    :cond_6
    const/16 v16, 0x44

    goto :goto_4

    :cond_7
    move/from16 v16, v18

    goto :goto_4

    :cond_8
    const/16 v16, 0x79

    :cond_9
    :goto_4
    xor-int v1, v9, v16

    xor-int v1, v24, v1

    int-to-char v1, v1

    aput-char v1, v11, v15

    add-int/lit8 v15, v15, 0x1

    const/16 v12, 0x31

    goto/16 :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 4

    array-length v0, p2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->b()Z

    move-result v1

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v0, :cond_2

    aget-object v3, p2, v2

    invoke-virtual {v3, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;->update(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object v3

    if-nez v1, :cond_1

    add-int/lit8 v2, v2, 0x1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    move-object p0, v3

    :cond_2
    :goto_0
    return-object p0
.end method

.method public static varargs init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 4

    array-length v0, p2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->b()Z

    move-result v1

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v0, :cond_2

    aget-object v3, p2, v2

    invoke-virtual {v3, p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;->update(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object v3

    if-nez v1, :cond_1

    add-int/lit8 v2, v2, 0x1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    move-object p0, v3

    :cond_2
    :goto_0
    return-object p0
.end method

.method public static varargs init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAll(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->initRequired(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->initRecommended(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->initOptional(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAllClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->initAllClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initAllClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->c()Z

    move-result v0

    const/16 v1, 0x1c

    new-array v1, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->BLOWFISH_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v5, 0x3

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v5, 0x4

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v5, 0x5

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v5, 0x6

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v5, 0x7

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x8

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x9

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->CAST_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0xa

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->IDEA_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0xb

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->THREE_DES_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0xc

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0xd

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0xe

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0xf

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->BLOWFISH_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x10

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x11

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x12

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x13

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x14

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x15

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->IDEA_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x16

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->CAST_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x17

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->THREE_DES_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x18

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR128:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x19

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR256:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x1a

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x1b

    aput-object v2, v1, v5

    invoke-static {p0, p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    new-array p1, v4, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;

    aput-object v1, p1, v3

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_0

    xor-int/lit8 p1, v0, 0x1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->b(Z)V

    :cond_0
    return-object p0
.end method

.method public static initAllClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->b()Z

    move-result v0

    const/16 v1, 0x1c

    new-array v1, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->BLOWFISH_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v5, 0x3

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v5, 0x4

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v5, 0x5

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v5, 0x6

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v5, 0x7

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x8

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x9

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->CAST_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0xa

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->IDEA_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0xb

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->THREE_DES_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0xc

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0xd

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0xe

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0xf

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->BLOWFISH_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x10

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x11

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x12

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x13

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x14

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x15

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->IDEA_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x16

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->CAST_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x17

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->THREE_DES_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x18

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR128:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x19

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR256:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x1a

    aput-object v2, v1, v5

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v5, 0x1b

    aput-object v2, v1, v5

    invoke-static {p0, p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    new-array p1, v4, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;->NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;

    aput-object v1, p1, v3

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    if-eqz v0, :cond_0

    new-array p1, v4, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object p0
.end method

.method public static initFipsClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 4

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    const/16 v1, 0x8

    new-array v1, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->THREE_DES_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v3, 0x6

    aput-object v2, v1, v3

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->THREE_DES_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v3, 0x7

    aput-object v2, v1, v3

    invoke-static {p0, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initOptional(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->initOptional(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initOptional(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 3

    const/16 v0, 0x18

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->BLOWFISH_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->BLOWFISH_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->IDEA_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->IDEA_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->CAST_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->CAST_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR256:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR128:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initOptional(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 3

    const/16 v0, 0x18

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->BLOWFISH_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->BLOWFISH_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->TWOFISH128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT256_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT192_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->SERPENT128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->IDEA_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->IDEA_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->CAST_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->CAST_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR256:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR128:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->ARCFOUR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRecommended(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->initRecommended(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRecommended(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->THREE_DES_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRecommended(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES128_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES128_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES192_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->AES256_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->THREE_DES_CTR:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRequired(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->initRequired(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRequired(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->THREE_DES_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    return-object p0
.end method

.method public static initRequired(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->THREE_DES_CBC:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;Ljava/security/Provider;[Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object p0

    return-object p0
.end method
