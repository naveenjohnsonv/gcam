.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Ljava/security/PublicKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x3

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x71

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "\u0005\u0019\u0000\u0003\u0005\u0019\u0000"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/4 v4, 0x7

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;->b:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    const/16 v14, 0x30

    if-eqz v13, :cond_6

    if-eq v13, v8, :cond_5

    if-eq v13, v0, :cond_6

    if-eq v13, v2, :cond_4

    const/4 v14, 0x4

    if-eq v13, v14, :cond_3

    const/4 v14, 0x5

    if-eq v13, v14, :cond_2

    const/16 v14, 0x2c

    goto :goto_2

    :cond_2
    const/16 v14, 0x29

    goto :goto_2

    :cond_3
    const/16 v14, 0x4c

    goto :goto_2

    :cond_4
    const/16 v14, 0x7a

    goto :goto_2

    :cond_5
    const/16 v14, 0x3b

    :cond_6
    :goto_2
    xor-int v13, v7, v14

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;->a:Ljava/lang/Object;

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static codecFor(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;
    .locals 1

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;

    invoke-direct {v0, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static codecFor(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;
    .locals 1

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;

    invoke-direct {v0, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;->read(Ljava/io/InputStream;)Ljava/security/PublicKey;

    move-result-object p1

    return-object p1
.end method

.method public read(Ljava/io/InputStream;)Ljava/security/PublicKey;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->readValue(Ljava/io/InputStream;)Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->readValue(Ljava/io/InputStream;)Ljava/math/BigInteger;

    move-result-object v2

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->readValue(Ljava/io/InputStream;)Ljava/math/BigInteger;

    move-result-object v3

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->readValue(Ljava/io/InputStream;)Ljava/math/BigInteger;

    move-result-object p1

    :try_start_0
    new-instance v4, Ljava/security/spec/DSAPublicKeySpec;

    invoke-direct {v4, p1, v0, v2, v3}, Ljava/security/spec/DSAPublicKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    :try_start_1
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;->a:Ljava/lang/Object;

    instance-of p1, p1, Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v1, :cond_1

    if-eqz p1, :cond_0

    :try_start_2
    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;->b:[Ljava/lang/String;

    const/4 v0, 0x1

    aget-object p1, p1, v0

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {p1, v0}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :cond_0
    :try_start_3
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;->a:Ljava/lang/Object;

    instance-of p1, p1, Ljava/security/Provider;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :cond_1
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    :try_start_4
    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;->b:[Ljava/lang/String;

    aget-object p1, p1, v0

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;->a:Ljava/lang/Object;

    check-cast v0, Ljava/security/Provider;

    invoke-static {p1, v0}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyFactory;

    move-result-object p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    sget-object p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;->b:[Ljava/lang/String;

    aget-object p1, p1, v0

    invoke-static {p1}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object p1

    :goto_0
    invoke-virtual {p1, v4}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    return-object p1

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    :catch_2
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    :catch_3
    move-exception p1

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Ljava/security/PublicKey;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;->write(Ljava/security/PublicKey;Ljava/io/OutputStream;)V

    return-void
.end method

.method public write(Ljava/security/PublicKey;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Ljava/security/interfaces/DSAPublicKey;

    invoke-interface {p1}, Ljava/security/interfaces/DSAPublicKey;->getParams()Ljava/security/interfaces/DSAParams;

    move-result-object v0

    invoke-interface {v0}, Ljava/security/interfaces/DSAParams;->getP()Ljava/math/BigInteger;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->writeValue(Ljava/math/BigInteger;Ljava/io/OutputStream;)V

    invoke-interface {v0}, Ljava/security/interfaces/DSAParams;->getQ()Ljava/math/BigInteger;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->writeValue(Ljava/math/BigInteger;Ljava/io/OutputStream;)V

    invoke-interface {v0}, Ljava/security/interfaces/DSAParams;->getG()Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->writeValue(Ljava/math/BigInteger;Ljava/io/OutputStream;)V

    invoke-interface {p1}, Ljava/security/interfaces/DSAPublicKey;->getY()Ljava/math/BigInteger;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->writeValue(Ljava/math/BigInteger;Ljava/io/OutputStream;)V

    return-void
.end method
