.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;
.super Ljava/io/FilterInputStream;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private a:[B

.field private b:[B

.field protected compress:Z

.field protected deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

.field protected flush:I

.field protected iis:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;

.field protected in:Ljava/io/InputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "H\'N\u0018?R!B%\u0012T"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->c:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/4 v5, 0x5

    if-eqz v4, :cond_5

    const/4 v6, 0x1

    if-eq v4, v6, :cond_4

    const/4 v6, 0x2

    if-eq v4, v6, :cond_3

    const/4 v6, 0x3

    if-eq v4, v6, :cond_2

    const/4 v6, 0x4

    if-eq v4, v6, :cond_1

    if-eq v4, v5, :cond_6

    const/16 v5, 0x6b

    goto :goto_1

    :cond_1
    const/16 v5, 0x7d

    goto :goto_1

    :cond_2
    const/16 v5, 0x57

    goto :goto_1

    :cond_3
    const/16 v5, 0xb

    goto :goto_1

    :cond_4
    const/16 v5, 0x61

    goto :goto_1

    :cond_5
    const/16 v5, 0xf

    :cond_6
    :goto_1
    const/16 v4, 0x23

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;-><init>(Ljava/io/InputStream;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->flush:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->in:Ljava/io/InputStream;

    const/4 v0, 0x1

    new-array v1, v0, [B

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->a:[B

    const/16 v1, 0x200

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->b:[B

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->in:Ljava/io/InputStream;

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    invoke-direct {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->init(I)I

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->compress:Z

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->flush:I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->in:Ljava/io/InputStream;

    const/4 v1, 0x1

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->a:[B

    const/16 v1, 0x200

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->b:[B

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;

    invoke-direct {v1, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;-><init>(Ljava/io/InputStream;Z)V

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->iis:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->compress:Z

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->compress:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->end()I

    if-eqz v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->iis:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->close()V

    :cond_1
    return-void
.end method

.method public getFlushMode()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->flush:I

    return v0
.end method

.method public getTotalIn()J
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->compress:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget-wide v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->totalIn:J

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->iis:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->getTotalIn()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public getTotalOut()J
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->compress:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget-wide v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->totalOut:J

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->iis:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->getTotalOut()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->a:[B

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->read([BII)I

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, -0x1

    if-nez v0, :cond_1

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->a:[B

    aget-byte v1, v0, v3

    const/16 v2, 0xff

    :cond_1
    and-int/2addr v2, v1

    :goto_0
    return v2

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method public read([BII)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->compress:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    if-nez v0, :cond_a

    if-eqz v1, :cond_9

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    invoke-virtual {v1, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->setOutput([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->in:Ljava/io/InputStream;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->b:[B

    array-length v3, v2

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, -0x1

    if-ne v1, v3, :cond_1

    if-nez v0, :cond_2

    return v3

    :cond_1
    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->b:[B

    invoke-virtual {v3, v5, v4, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->setInput([BIIZ)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->flush:I

    invoke-virtual {v1, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->deflate(I)I

    move-result v3

    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->nextOutIndex:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    if-nez v0, :cond_4

    if-lez v1, :cond_3

    :try_start_3
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->nextOutIndex:I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    return p1

    :cond_3
    move v1, v3

    :cond_4
    if-nez v0, :cond_6

    if-ne v1, v2, :cond_5

    return v4

    :cond_5
    const/4 v2, -0x2

    move v1, v3

    :cond_6
    if-nez v0, :cond_7

    if-eq v1, v2, :cond_8

    const/4 v2, -0x3

    goto :goto_0

    :cond_7
    move v3, v1

    :goto_0
    if-eq v3, v2, :cond_8

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_8
    :try_start_4
    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStreamException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->c:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget-object p3, p3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->msg:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStreamException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_9
    :goto_1
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->iis:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;->read([BII)I

    move-result v1

    :cond_a
    return v1

    :catch_3
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method public setFlushMode(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->flush:I

    return-void
.end method

.method public skip(J)J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const/16 v1, 0x200

    int-to-long v2, v1

    cmp-long v2, p1, v2

    if-nez v0, :cond_1

    if-gez v2, :cond_0

    long-to-int v1, p1

    :cond_0
    move v2, v1

    :cond_1
    new-array p1, v2, [B

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZInputStream;->read([B)I

    move-result p1

    int-to-long p1, p1

    return-wide p1
.end method
