.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenDirectTcpIpCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelOpenCodec$TypeCodec;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;IJI)Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpen;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v6

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v7

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v8

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenDirectTcpIp;

    move-object v0, p1

    move v1, p2

    move-wide v2, p3

    move v4, p5

    invoke-direct/range {v0 .. v8}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenDirectTcpIp;-><init>(IJILjava/lang/String;ILjava/lang/String;I)V

    return-object p1
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpen;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenDirectTcpIp;

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenDirectTcpIp;->host:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUsAsciiValue(Ljava/lang/String;Ljava/io/OutputStream;)V

    iget v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenDirectTcpIp;->port:I

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenDirectTcpIp;->originatorAddress:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUsAsciiValue(Ljava/lang/String;Ljava/io/OutputStream;)V

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelOpenDirectTcpIp;->originatorPort:I

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    return-void
.end method
