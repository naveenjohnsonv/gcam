.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;
.super Ljava/lang/Object;


# static fields
.field private static final a:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->a:[B

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method public static readValue(Ljava/io/InputStream;)Ljava/math/BigInteger;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/h/a/e;->a(Ljava/io/InputStream;)[B

    move-result-object p0

    :try_start_0
    array-length v0, p0

    if-lez v0, :cond_0

    new-instance v0, Ljava/math/BigInteger;

    invoke-direct {v0, p0}, Ljava/math/BigInteger;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/math/BigInteger;

    const-string p0, "0"

    invoke-direct {v0, p0}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method

.method public static writeValue(Ljava/math/BigInteger;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->c()I

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    sget-object v0, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {p0, v0}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    :try_start_1
    sget-object p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->a:[B

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/MpintCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_0
    invoke-virtual {p0}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object p0

    :goto_0
    invoke-static {p0, p1}, Lcom/jscape/util/h/a/e;->a([BLjava/io/OutputStream;)V

    return-void
.end method
