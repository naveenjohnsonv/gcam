.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/TerminalModesCodec;
.super Ljava/lang/Object;


# static fields
.field private static final a:B


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/v2/messages/TerminalMode;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result p0

    goto :goto_0

    :cond_1
    move p0, v1

    :goto_0
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/TerminalMode;

    invoke-direct {v0, v1, p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/TerminalMode;-><init>(II)V

    return-object v0
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private static a(Lcom/jscape/inet/ssh/protocol/v2/messages/TerminalMode;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/TerminalMode;->opcode:I

    int-to-byte v0, v0

    invoke-static {v0, p1}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    iget p0, p0, Lcom/jscape/inet/ssh/protocol/v2/messages/TerminalMode;->value:I

    invoke-static {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    return-void
.end method

.method public static readValue(Ljava/io/InputStream;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/TerminalMode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readValue(Ljava/io/InputStream;)[B

    move-result-object p0

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/jscape/util/h/e;

    invoke-direct {v1, p0}, Lcom/jscape/util/h/e;-><init>([B)V

    new-instance p0, Ljava/util/LinkedList;

    invoke-direct {p0}, Ljava/util/LinkedList;-><init>()V

    :cond_0
    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/TerminalModesCodec;->a(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/v2/messages/TerminalMode;

    move-result-object v2

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    :try_start_0
    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/TerminalModesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    return-object p0
.end method

.method public static writeValue(Ljava/util/List;Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/protocol/v2/messages/TerminalMode;",
            ">;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/o;

    invoke-direct {v0}, Lcom/jscape/util/h/o;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ssh/protocol/v2/messages/TerminalMode;

    :try_start_0
    invoke-static {v2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/TerminalModesCodec;->a(Lcom/jscape/inet/ssh/protocol/v2/messages/TerminalMode;Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_2

    if-eqz v1, :cond_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/TerminalModesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    const/4 p0, 0x0

    invoke-static {p0, v0}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v1

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result v0

    invoke-static {v1, p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BIILjava/io/OutputStream;)V

    :cond_2
    return-void
.end method
