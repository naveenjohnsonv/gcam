.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestPtyReqCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$RequestCodec;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;IZ)Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v4

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v5

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v6

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v7

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/TerminalModesCodec;->readValue(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v8

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;

    move-object v0, p1

    move v1, p2

    move v2, p3

    invoke-direct/range {v0 .. v8}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;-><init>(IZLjava/lang/String;IIIILjava/util/List;)V

    return-object p1
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalEnvironmentVariable:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUsAsciiValue(Ljava/lang/String;Ljava/io/OutputStream;)V

    iget v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalCharactersWidth:I

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalRowsHeight:I

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalPixelsWidth:I

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalPixelsHeight:I

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestPtyReq;->terminalModes:Ljava/util/List;

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/TerminalModesCodec;->writeValue(Ljava/util/List;Ljava/io/OutputStream;)V

    return-void
.end method
