.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;
.super Ljava/lang/Object;


# static fields
.field private static final K:[Ljava/lang/String;

.field private static final a:[I

.field private static final b:I = 0x0

.field private static final c:I = 0x1

.field private static final d:I = 0x2

.field private static final e:I = -0x1

.field private static final f:I = -0x2

.field private static final g:I = -0x3

.field private static final h:I = -0x4

.field private static final i:I = -0x5

.field private static final j:I = -0x6

.field private static final k:I = 0x0

.field private static final l:I = 0x1

.field private static final m:I = 0x2

.field private static final n:I = 0x3

.field private static final o:I = 0x4

.field private static final p:I = 0x5

.field private static final q:I = 0x6

.field private static final r:I = 0x7

.field private static final s:I = 0x8

.field private static final t:I = 0x9


# instance fields
.field A:I

.field B:I

.field C:B

.field D:B

.field E:[I

.field F:I

.field G:[I

.field H:I

.field private final I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

.field private final J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

.field u:I

.field v:I

.field w:[I

.field x:I

.field y:I

.field z:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "m1\\U u\u001f$;CG8}\u0015g:\nW#x\u001e\u0015m1\\U u\u001f$;CG8}\u0015g:\nW#x\u001e"

    const/16 v5, 0x2b

    const/16 v6, 0x15

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x3d

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x37

    const/16 v4, 0x1b

    const-string v6, "b>SZ/z\u0010+<LO&a\u0015g\u007fI^-t\u0000cpFT\'v\u001bb>SZ/z\u0010+<LO&a\u0015g\u007fI^-t\u0000cpFT\'v"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v16, v6

    move v6, v4

    move-object/from16 v4, v16

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x32

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->K:[Ljava/lang/String;

    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->a:[I

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v2, 0x46

    goto :goto_4

    :cond_4
    const/16 v2, 0x21

    goto :goto_4

    :cond_5
    const/16 v2, 0x71

    goto :goto_4

    :cond_6
    const/16 v2, 0x9

    goto :goto_4

    :cond_7
    const/16 v2, 0x17

    goto :goto_4

    :cond_8
    const/16 v2, 0x62

    goto :goto_4

    :cond_9
    const/16 v2, 0x39

    :goto_4
    xor-int/2addr v2, v9

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    :array_0
    .array-data 4
        0x0
        0x1
        0x3
        0x7
        0xf
        0x1f
        0x3f
        0x7f
        0xff
        0x1ff
        0x3ff
        0x7ff
        0xfff
        0x1fff
        0x3fff
        0x7fff
        0xffff
    .end array-data
.end method

.method constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->x:I

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    return-void
.end method


# virtual methods
.method a(I)I
    .locals 19

    move-object/from16 v9, p0

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v10

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v1, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v2, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget-object v3, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v4, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    const/4 v11, 0x1

    if-nez v10, :cond_1

    if-ge v4, v5, :cond_0

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    sub-int/2addr v5, v4

    sub-int/2addr v5, v11

    goto :goto_1

    :cond_0
    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    move v6, v4

    goto :goto_0

    :cond_1
    move v6, v5

    move v5, v4

    :goto_0
    sub-int/2addr v5, v6

    :goto_1
    move v6, v5

    move v5, v4

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v0

    move/from16 v0, p1

    :cond_2
    :goto_2
    iget v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->u:I

    :goto_3
    const/16 v12, 0x9

    const/4 v13, -0x3

    const/4 v14, 0x7

    const/4 v15, 0x3

    const/4 v8, 0x0

    packed-switch v7, :pswitch_data_0

    const/4 v0, -0x2

    iget-object v6, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v3, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget-object v3, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v3, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v2, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v6, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v6, v1, v6

    int-to-long v6, v6

    add-long/2addr v3, v6

    iput-wide v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v2, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v1, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v1, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v1, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v0

    return v0

    :pswitch_0
    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v4, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v4, v1, v4

    int-to-long v6, v4

    add-long/2addr v2, v6

    iput-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v0, v13}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v0

    return v0

    :pswitch_1
    iget v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->A:I

    :cond_3
    if-ge v4, v7, :cond_8

    if-nez v10, :cond_7

    if-nez v10, :cond_6

    if-eqz v2, :cond_5

    if-eqz v10, :cond_4

    goto :goto_4

    :cond_4
    add-int/lit8 v2, v2, -0x1

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    add-int/lit8 v12, v1, 0x1

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/2addr v0, v4

    or-int/2addr v3, v0

    add-int/lit8 v4, v4, 0x8

    move v0, v8

    move v1, v12

    if-eqz v10, :cond_3

    goto :goto_5

    :cond_5
    move v8, v0

    :goto_4
    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v4, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v4, v1, v4

    int-to-long v6, v4

    add-long/2addr v2, v6

    iput-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v0, v8}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v2

    :cond_6
    return v2

    :cond_7
    move v4, v3

    move v3, v2

    goto :goto_6

    :cond_8
    :goto_5
    iget v12, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->B:I

    sget-object v13, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->a:[I

    aget v13, v13, v7

    and-int/2addr v13, v3

    add-int/2addr v12, v13

    iput v12, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->B:I

    shr-int/2addr v3, v7

    sub-int/2addr v4, v7

    move/from16 v18, v3

    move v3, v2

    move v2, v4

    move/from16 v4, v18

    :goto_6
    const/4 v7, 0x5

    iput v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->u:I

    move/from16 v18, v4

    move v4, v2

    move v2, v3

    move/from16 v3, v18

    :pswitch_2
    iget v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->B:I

    sub-int v7, v5, v7

    :cond_9
    if-gez v7, :cond_a

    iget-object v12, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v12, v12, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    add-int/2addr v7, v12

    if-nez v10, :cond_22

    if-eqz v10, :cond_9

    :cond_a
    move/from16 v18, v7

    move v7, v6

    move/from16 v6, v18

    :cond_b
    iget v12, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->v:I

    if-eqz v12, :cond_21

    if-nez v10, :cond_20

    if-nez v10, :cond_1d

    if-nez v7, :cond_1b

    if-nez v10, :cond_10

    iget-object v12, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v12, v12, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    if-ne v5, v12, :cond_f

    iget-object v12, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v12, v12, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    if-nez v10, :cond_e

    if-eqz v12, :cond_f

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    if-nez v10, :cond_d

    if-lez v5, :cond_c

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    sub-int/2addr v5, v8

    sub-int/2addr v5, v11

    goto :goto_8

    :cond_c
    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    move v7, v8

    goto :goto_7

    :cond_d
    move v7, v5

    move v5, v8

    :goto_7
    sub-int/2addr v5, v7

    :goto_8
    move v7, v8

    goto :goto_9

    :cond_e
    move/from16 v18, v7

    move v7, v5

    move v5, v12

    move/from16 v12, v18

    goto :goto_a

    :cond_f
    move/from16 v18, v7

    move v7, v5

    move/from16 v5, v18

    :goto_9
    move v12, v5

    goto :goto_a

    :cond_10
    move v12, v7

    move v7, v5

    :goto_a
    if-nez v10, :cond_1a

    if-nez v5, :cond_18

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v7, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v5, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v0

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    if-nez v10, :cond_12

    if-ge v5, v7, :cond_11

    iget-object v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    sub-int/2addr v7, v5

    sub-int/2addr v7, v11

    goto :goto_c

    :cond_11
    iget-object v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    move v12, v5

    goto :goto_b

    :cond_12
    move v12, v7

    move v7, v5

    :goto_b
    sub-int/2addr v7, v12

    :goto_c
    if-nez v10, :cond_17

    iget-object v12, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v12, v12, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    if-ne v5, v12, :cond_16

    iget-object v12, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v12, v12, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    if-nez v10, :cond_15

    if-eqz v12, :cond_16

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    if-nez v10, :cond_14

    if-lez v5, :cond_13

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    sub-int/2addr v5, v8

    sub-int/2addr v5, v11

    goto :goto_e

    :cond_13
    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    move v7, v8

    goto :goto_d

    :cond_14
    move v7, v5

    move v5, v8

    :goto_d
    sub-int/2addr v5, v7

    :goto_e
    move v7, v5

    move v5, v8

    goto :goto_f

    :cond_15
    move/from16 v18, v12

    move v12, v7

    move/from16 v7, v18

    goto :goto_10

    :cond_16
    :goto_f
    move v12, v7

    goto :goto_10

    :cond_17
    move v12, v7

    move v7, v5

    :goto_10
    if-nez v10, :cond_1e

    if-nez v7, :cond_19

    iget-object v6, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v3, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget-object v3, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v3, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v2, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v6, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v6, v1, v6

    int-to-long v6, v6

    add-long/2addr v3, v6

    iput-wide v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v2, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v1, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v1, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v1, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v0

    return v0

    :cond_18
    move v5, v7

    :cond_19
    move v7, v12

    goto :goto_11

    :cond_1a
    move/from16 v18, v7

    move v7, v5

    move/from16 v5, v18

    goto :goto_12

    :cond_1b
    :goto_11
    iget-object v12, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget-object v12, v12, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    add-int/lit8 v13, v5, 0x1

    iget-object v15, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget-object v15, v15, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    add-int/lit8 v16, v6, 0x1

    aget-byte v6, v15, v6

    aput-byte v6, v12, v5

    add-int/lit8 v7, v7, -0x1

    if-nez v10, :cond_1c

    move v12, v7

    move v5, v13

    move/from16 v6, v16

    move v7, v6

    goto :goto_12

    :cond_1c
    move v5, v13

    move/from16 v6, v16

    goto :goto_13

    :cond_1d
    move v12, v7

    :cond_1e
    :goto_12
    iget-object v13, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v13, v13, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    if-ne v7, v13, :cond_1f

    move v6, v8

    :cond_1f
    iget v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->v:I

    sub-int/2addr v7, v11

    iput v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->v:I

    move v7, v12

    :goto_13
    if-eqz v10, :cond_b

    goto :goto_14

    :cond_20
    move v6, v7

    goto/16 :goto_3

    :cond_21
    :goto_14
    iput v8, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->u:I

    move v6, v7

    :cond_22
    if-eqz v10, :cond_2

    :pswitch_3
    if-nez v10, :cond_32

    if-nez v6, :cond_31

    if-nez v10, :cond_27

    iget-object v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    if-ne v5, v7, :cond_26

    iget-object v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    if-nez v10, :cond_25

    if-eqz v7, :cond_26

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    if-nez v10, :cond_24

    if-lez v5, :cond_23

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    sub-int/2addr v5, v8

    sub-int/2addr v5, v11

    goto :goto_16

    :cond_23
    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    move v6, v8

    goto :goto_15

    :cond_24
    move v6, v5

    move v5, v8

    :goto_15
    sub-int/2addr v5, v6

    :goto_16
    move v6, v8

    goto :goto_17

    :cond_25
    move/from16 v18, v6

    move v6, v5

    move v5, v7

    move/from16 v7, v18

    goto :goto_18

    :cond_26
    move/from16 v18, v6

    move v6, v5

    move/from16 v5, v18

    :goto_17
    move v7, v5

    goto :goto_18

    :cond_27
    move v7, v6

    move v6, v5

    :goto_18
    if-nez v10, :cond_30

    if-nez v5, :cond_2f

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v6, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v5, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v0

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v6, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    if-nez v10, :cond_29

    if-ge v5, v6, :cond_28

    iget-object v6, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    sub-int/2addr v6, v5

    sub-int/2addr v6, v11

    goto :goto_1a

    :cond_28
    iget-object v6, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    move v7, v5

    goto :goto_19

    :cond_29
    move v7, v6

    move v6, v5

    :goto_19
    sub-int/2addr v6, v7

    :goto_1a
    if-nez v10, :cond_2e

    iget-object v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    if-ne v5, v7, :cond_2d

    iget-object v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    if-nez v10, :cond_2c

    if-eqz v7, :cond_2d

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    if-nez v10, :cond_2b

    if-lez v5, :cond_2a

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    sub-int/2addr v5, v8

    sub-int/2addr v5, v11

    goto :goto_1c

    :cond_2a
    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    move v6, v8

    goto :goto_1b

    :cond_2b
    move v6, v5

    move v5, v8

    :goto_1b
    sub-int/2addr v5, v6

    :goto_1c
    move v6, v8

    goto :goto_1d

    :cond_2c
    move/from16 v18, v6

    move v6, v5

    move v5, v7

    move/from16 v7, v18

    goto :goto_1e

    :cond_2d
    move/from16 v18, v6

    move v6, v5

    move/from16 v5, v18

    :goto_1d
    move v7, v5

    goto :goto_1e

    :cond_2e
    move v7, v6

    move v6, v5

    :goto_1e
    if-nez v10, :cond_30

    if-nez v5, :cond_2f

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v3, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget-object v3, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v3, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v2, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v5, v1, v5

    int-to-long v7, v5

    add-long/2addr v3, v7

    iput-wide v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v2, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v1, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v1, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v1, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v0

    return v0

    :cond_2f
    move v5, v6

    move v6, v7

    goto :goto_1f

    :cond_30
    move v0, v5

    move v5, v6

    move v6, v7

    goto :goto_20

    :cond_31
    :goto_1f
    move v0, v8

    goto :goto_20

    :cond_32
    move v0, v6

    :goto_20
    iget-object v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget-object v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    add-int/lit8 v12, v5, 0x1

    iget v13, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->z:I

    int-to-byte v13, v13

    aput-byte v13, v7, v5

    add-int/lit8 v6, v6, -0x1

    iput v8, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->u:I

    move v5, v12

    if-eqz v10, :cond_2

    :pswitch_4
    if-nez v10, :cond_34

    if-le v4, v14, :cond_33

    add-int/lit8 v4, v4, -0x8

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, -0x1

    :cond_33
    iget-object v6, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v5, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v5, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v0

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v6, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v14, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    move v6, v5

    move v5, v4

    move v4, v6

    goto :goto_21

    :cond_34
    move v6, v5

    move v5, v4

    :goto_21
    if-nez v10, :cond_36

    if-ge v4, v14, :cond_35

    iget-object v4, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    goto :goto_22

    :cond_35
    iget-object v4, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    :cond_36
    :goto_22
    if-nez v10, :cond_37

    iget-object v4, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    iget-object v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    if-eq v4, v7, :cond_37

    iget-object v4, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v3, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget-object v3, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v5, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v3, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v2, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v5, v1, v5

    int-to-long v7, v5

    add-long/2addr v3, v7

    iput-wide v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v2, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v1, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v1, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v1, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v0

    return v0

    :cond_37
    const/16 v0, 0x8

    iput v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->u:I

    move v4, v5

    move v5, v6

    :pswitch_5
    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v4, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v4, v1, v4

    int-to-long v6, v4

    add-long/2addr v2, v6

    iput-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v0, v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v0

    return v0

    :pswitch_6
    iget v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->A:I

    :cond_38
    if-ge v4, v7, :cond_3d

    if-nez v10, :cond_3c

    if-nez v10, :cond_3b

    if-eqz v2, :cond_3a

    if-eqz v10, :cond_39

    goto :goto_23

    :cond_39
    add-int/lit8 v2, v2, -0x1

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    add-int/lit8 v14, v1, 0x1

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/2addr v0, v4

    or-int/2addr v3, v0

    add-int/lit8 v4, v4, 0x8

    move v0, v8

    move v1, v14

    if-eqz v10, :cond_38

    goto :goto_24

    :cond_3a
    move v8, v0

    :goto_23
    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v4, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v4, v1, v4

    int-to-long v6, v4

    add-long/2addr v2, v6

    iput-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v0, v8}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v2

    :cond_3b
    return v2

    :cond_3c
    move v4, v2

    goto :goto_25

    :cond_3d
    :goto_24
    iget v14, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->v:I

    sget-object v16, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->a:[I

    aget v16, v16, v7

    and-int v16, v3, v16

    add-int v14, v14, v16

    iput v14, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->v:I

    shr-int/2addr v3, v7

    sub-int/2addr v4, v7

    :goto_25
    iget-byte v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->D:B

    iput v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->y:I

    iget-object v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->G:[I

    iput-object v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->w:[I

    iget v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->H:I

    iput v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->x:I

    iput v15, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->u:I

    :pswitch_7
    iget v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->y:I

    :goto_26
    if-ge v4, v7, :cond_43

    if-nez v10, :cond_42

    if-nez v10, :cond_41

    if-eqz v2, :cond_40

    if-eqz v10, :cond_3e

    goto :goto_27

    :cond_3e
    add-int/lit8 v2, v2, -0x1

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    add-int/lit8 v14, v1, 0x1

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/2addr v0, v4

    or-int/2addr v3, v0

    add-int/lit8 v4, v4, 0x8

    if-eqz v10, :cond_3f

    move v1, v14

    goto :goto_28

    :cond_3f
    move v0, v8

    move v1, v14

    goto :goto_26

    :cond_40
    move v8, v0

    :goto_27
    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v4, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v4, v1, v4

    int-to-long v6, v4

    add-long/2addr v2, v6

    iput-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v0, v8}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v2

    :cond_41
    return v2

    :cond_42
    move v7, v4

    move v4, v3

    move v3, v2

    goto :goto_29

    :cond_43
    move v8, v0

    :goto_28
    iget v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->x:I

    sget-object v14, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->a:[I

    aget v7, v14, v7

    and-int/2addr v7, v3

    add-int/2addr v0, v7

    mul-int/2addr v0, v15

    move v7, v4

    move v4, v3

    move v3, v2

    move v2, v0

    move v0, v8

    :goto_29
    iget-object v8, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->w:[I

    add-int/lit8 v14, v2, 0x1

    aget v15, v8, v14

    shr-int/2addr v4, v15

    aget v14, v8, v14

    sub-int/2addr v7, v14

    aget v14, v8, v2

    and-int/lit8 v15, v14, 0x10

    if-nez v10, :cond_45

    if-eqz v15, :cond_44

    and-int/lit8 v15, v14, 0xf

    iput v15, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->A:I

    add-int/lit8 v15, v2, 0x2

    aget v8, v8, v15

    iput v8, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->B:I

    const/4 v8, 0x4

    iput v8, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->u:I

    if-eqz v10, :cond_46

    :cond_44
    and-int/lit8 v15, v14, 0x40

    :cond_45
    if-nez v10, :cond_48

    if-nez v15, :cond_47

    iput v14, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->y:I

    div-int/lit8 v8, v2, 0x3

    iget-object v14, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->w:[I

    add-int/lit8 v2, v2, 0x2

    aget v2, v14, v2

    add-int/2addr v8, v2

    iput v8, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->x:I

    if-eqz v10, :cond_46

    goto :goto_2a

    :cond_46
    move v2, v3

    move v3, v4

    move v4, v7

    goto/16 :goto_2

    :cond_47
    :goto_2a
    iput v12, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->u:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->K:[Ljava/lang/String;

    aget-object v2, v2, v11

    iput-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v7, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v4, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v4, v1, v4

    int-to-long v6, v4

    add-long/2addr v2, v6

    iput-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v0, v13}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v15

    :cond_48
    return v15

    :pswitch_8
    move/from16 v16, v8

    goto/16 :goto_30

    :pswitch_9
    const/16 v7, 0x102

    if-nez v10, :cond_4a

    if-lt v6, v7, :cond_49

    const/16 v7, 0xa

    move v8, v2

    goto :goto_2b

    :cond_49
    move/from16 v16, v8

    goto/16 :goto_2f

    :cond_4a
    move v8, v6

    :goto_2b
    if-nez v10, :cond_4c

    if-lt v8, v7, :cond_4b

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v4, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v4, v1, v4

    int-to-long v6, v4

    add-long/2addr v2, v6

    iput-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-byte v1, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->C:B

    iget-byte v2, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->D:B

    iget-object v3, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->E:[I

    iget v4, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->F:I

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->G:[I

    iget v6, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->H:I

    iget-object v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget-object v8, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    move-object/from16 v0, p0

    const/16 v16, 0x0

    invoke-virtual/range {v0 .. v8}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->a(II[II[IILcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)I

    move-result v0

    iget-object v1, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v2, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v3, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget-object v4, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v8, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v5, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget v7, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    move v5, v8

    goto :goto_2c

    :cond_4b
    const/16 v16, 0x0

    goto :goto_2f

    :cond_4c
    const/16 v16, 0x0

    :goto_2c
    if-nez v10, :cond_4e

    iget-object v6, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    if-ge v8, v7, :cond_4d

    iget v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    sub-int/2addr v6, v5

    sub-int/2addr v6, v11

    goto :goto_2d

    :cond_4d
    iget v8, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    move v7, v5

    :cond_4e
    sub-int/2addr v8, v7

    move v6, v8

    :goto_2d
    if-nez v10, :cond_52

    if-eqz v0, :cond_51

    if-nez v10, :cond_50

    if-ne v0, v11, :cond_4f

    move v7, v14

    goto :goto_2e

    :cond_4f
    move v7, v12

    goto :goto_2e

    :cond_50
    move v7, v0

    :goto_2e
    iput v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->u:I

    if-eqz v10, :cond_2

    :cond_51
    :goto_2f
    iget-byte v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->C:B

    iput v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->y:I

    iget-object v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->E:[I

    iput-object v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->w:[I

    iget v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->F:I

    iput v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->x:I

    :cond_52
    iput v11, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->u:I

    :goto_30
    iget v7, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->y:I

    move v8, v0

    :cond_53
    if-ge v4, v7, :cond_58

    if-nez v10, :cond_57

    if-nez v10, :cond_56

    if-eqz v2, :cond_55

    if-eqz v10, :cond_54

    move/from16 v8, v16

    goto :goto_31

    :cond_54
    add-int/lit8 v2, v2, -0x1

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    add-int/lit8 v8, v1, 0x1

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/2addr v0, v4

    or-int/2addr v3, v0

    add-int/lit8 v4, v4, 0x8

    move v1, v8

    move/from16 v8, v16

    if-eqz v10, :cond_53

    goto :goto_32

    :cond_55
    :goto_31
    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v4, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v4, v1, v4

    int-to-long v6, v4

    add-long/2addr v2, v6

    iput-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v0, v8}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v2

    :cond_56
    return v2

    :cond_57
    move v7, v4

    move v0, v8

    move v4, v3

    move v3, v2

    goto :goto_33

    :cond_58
    :goto_32
    iget v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->x:I

    sget-object v16, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->a:[I

    aget v7, v16, v7

    and-int/2addr v7, v3

    add-int/2addr v0, v7

    mul-int/2addr v0, v15

    move v7, v4

    move v4, v3

    move v3, v2

    move v2, v0

    move v0, v8

    :goto_33
    iget-object v8, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->w:[I

    add-int/lit8 v15, v2, 0x1

    aget v16, v8, v15

    ushr-int v4, v4, v16

    aget v15, v8, v15

    sub-int/2addr v7, v15

    aget v15, v8, v2

    if-nez v10, :cond_5a

    if-nez v15, :cond_59

    add-int/lit8 v16, v2, 0x2

    aget v8, v8, v16

    iput v8, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->z:I

    const/4 v8, 0x6

    iput v8, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->u:I

    if-eqz v10, :cond_5f

    :cond_59
    and-int/lit8 v8, v15, 0x10

    goto :goto_34

    :cond_5a
    move v8, v15

    :goto_34
    const/4 v11, 0x2

    if-nez v10, :cond_5c

    if-eqz v8, :cond_5b

    and-int/lit8 v8, v15, 0xf

    iput v8, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->A:I

    iget-object v8, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->w:[I

    add-int/lit8 v17, v2, 0x2

    aget v8, v8, v17

    iput v8, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->v:I

    iput v11, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->u:I

    if-eqz v10, :cond_5f

    :cond_5b
    and-int/lit8 v8, v15, 0x40

    :cond_5c
    if-nez v10, :cond_5e

    if-nez v8, :cond_5d

    iput v15, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->y:I

    div-int/lit8 v8, v2, 0x3

    iget-object v13, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->w:[I

    add-int/lit8 v2, v2, 0x2

    aget v2, v13, v2

    add-int/2addr v8, v2

    iput v8, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->x:I

    if-eqz v10, :cond_5f

    :cond_5d
    and-int/lit8 v8, v15, 0x20

    :cond_5e
    if-nez v10, :cond_61

    if-eqz v8, :cond_60

    iput v14, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->u:I

    if-eqz v10, :cond_5f

    goto :goto_35

    :cond_5f
    move v2, v3

    move v3, v4

    move v4, v7

    const/4 v11, 0x1

    goto/16 :goto_2

    :cond_60
    :goto_35
    iput v12, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->u:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->K:[Ljava/lang/String;

    aget-object v2, v2, v11

    iput-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v7, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v4, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v4, v1, v4

    int-to-long v6, v4

    add-long/2addr v2, v6

    iput-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->I:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iput v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget-object v0, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->J:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    const/4 v1, -0x3

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b(I)I

    move-result v8

    :cond_61
    return v8

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_6
        :pswitch_7
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
    .end packed-switch
.end method

.method a(II[II[IILcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)I
    .locals 31

    move-object/from16 v0, p7

    move-object/from16 v1, p8

    iget v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v4

    iget v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iget v6, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iget v7, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    iget v8, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    const/4 v9, 0x1

    if-nez v4, :cond_1

    if-ge v7, v8, :cond_0

    iget v8, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->Q:I

    sub-int/2addr v8, v7

    sub-int/2addr v8, v9

    goto :goto_1

    :cond_0
    iget v8, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    move v10, v7

    goto :goto_0

    :cond_1
    move v10, v8

    move v8, v7

    :goto_0
    sub-int/2addr v8, v10

    :goto_1
    sget-object v10, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->a:[I

    aget v11, v10, p1

    aget v10, v10, p2

    move v12, v7

    move v13, v8

    move v14, v10

    move v15, v11

    move v7, v3

    move-object v8, v4

    move v10, v5

    move v11, v6

    move/from16 v3, p6

    move-object v4, v0

    move-object v5, v1

    move v6, v2

    move-object/from16 v0, p3

    move/from16 v1, p4

    move-object/from16 v2, p5

    :cond_2
    :goto_2
    const/16 v9, 0x14

    if-ge v11, v9, :cond_4

    add-int/lit8 v7, v7, -0x1

    iget-object v9, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    add-int/lit8 v16, v6, 0x1

    aget-byte v6, v9, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/2addr v6, v11

    or-int/2addr v10, v6

    add-int/lit8 v11, v11, 0x8

    if-nez v8, :cond_3

    if-eqz v8, :cond_3

    move/from16 v6, v16

    goto :goto_3

    :cond_3
    move/from16 v6, v16

    goto :goto_2

    :cond_4
    :goto_3
    and-int v9, v10, v15

    add-int v16, v1, v9

    const/16 v17, 0x3

    mul-int/lit8 v16, v16, 0x3

    aget v18, v0, v16

    const/16 v19, 0x0

    if-nez v8, :cond_7

    if-nez v18, :cond_6

    add-int/lit8 v20, v16, 0x1

    aget v21, v0, v20

    shr-int v10, v10, v21

    aget v20, v0, v20

    sub-int v11, v11, v20

    move/from16 p1, v1

    iget-object v1, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    add-int/lit8 v20, v12, 0x1

    add-int/lit8 v21, v16, 0x2

    move-object/from16 p2, v2

    aget v2, v0, v21

    int-to-byte v2, v2

    aput-byte v2, v1, v12

    add-int/lit8 v13, v13, -0x1

    if-eqz v8, :cond_5

    move/from16 v2, p1

    move-object v1, v0

    move v12, v9

    move/from16 v22, v18

    move v9, v7

    move/from16 v18, v15

    move-object v7, v5

    move v15, v13

    move v5, v3

    move v13, v11

    move v3, v2

    move-object v11, v8

    move v8, v6

    move-object v6, v4

    move-object/from16 v4, p2

    move/from16 v30, v16

    move/from16 v16, v14

    move/from16 v14, v20

    move/from16 v20, v30

    goto/16 :goto_18

    :cond_5
    move/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v12, v20

    goto/16 :goto_12

    :cond_6
    move/from16 p1, v1

    move-object/from16 p2, v2

    move/from16 v2, p1

    move-object v1, v0

    move/from16 v20, v16

    move/from16 v22, v18

    move/from16 v16, v14

    move/from16 v18, v15

    move v14, v12

    move v15, v13

    move v12, v9

    move v13, v11

    move v9, v7

    move-object v11, v8

    move-object v7, v5

    move v8, v6

    move v5, v3

    move-object v6, v4

    move v3, v2

    move-object/from16 v4, p2

    goto/16 :goto_18

    :cond_7
    move/from16 p1, v1

    move-object/from16 p2, v2

    move/from16 v2, p1

    move-object v1, v0

    move/from16 v20, v16

    move/from16 v21, v18

    move/from16 v22, v21

    move/from16 v16, v14

    move/from16 v18, v15

    move v14, v12

    move v15, v13

    move v12, v10

    move v13, v11

    move-object v10, v8

    move v11, v9

    move v8, v6

    move v9, v7

    move-object v6, v4

    move-object v7, v5

    move-object/from16 v4, p2

    move v5, v3

    move v3, v2

    :goto_4
    const/16 v23, -0x3

    if-eqz v21, :cond_8

    and-int/lit8 v21, v22, 0xf

    move-object/from16 p1, v1

    move/from16 p2, v2

    goto/16 :goto_5

    :cond_8
    and-int/lit8 v21, v22, 0x40

    if-nez v10, :cond_32

    if-nez v21, :cond_31

    add-int/lit8 v20, v20, 0x2

    aget v20, v0, v20

    add-int v11, v11, v20

    sget-object v20, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->a:[I

    aget v20, v20, v22

    and-int v20, v12, v20

    add-int v11, v11, v20

    add-int v20, v2, v11

    mul-int/lit8 v20, v20, 0x3

    aget v22, v0, v20

    if-nez v22, :cond_30

    add-int/lit8 v21, v20, 0x1

    aget v24, v0, v21

    shr-int v12, v12, v24

    aget v21, v0, v21

    sub-int v13, v13, v21

    move-object/from16 p1, v1

    iget-object v1, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    add-int/lit8 v21, v14, 0x1

    add-int/lit8 v24, v20, 0x2

    move/from16 p2, v2

    aget v2, v0, v24

    int-to-byte v2, v2

    aput-byte v2, v1, v14

    add-int/lit8 v15, v15, -0x1

    if-nez v10, :cond_a

    if-eqz v10, :cond_9

    move/from16 v14, v21

    goto/16 :goto_19

    :cond_9
    move-object/from16 v0, p1

    move v1, v3

    move-object v2, v4

    move v3, v5

    move-object v4, v6

    move-object v5, v7

    move v6, v8

    move v7, v9

    move-object v8, v10

    move v10, v12

    move v11, v13

    move v13, v15

    move/from16 v14, v16

    move/from16 v15, v18

    move/from16 v12, v21

    goto/16 :goto_12

    :cond_a
    move/from16 v14, v21

    move/from16 v21, v22

    :goto_5
    add-int/lit8 v1, v20, 0x2

    aget v1, v0, v1

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->a:[I

    aget v2, v2, v21

    and-int/2addr v2, v12

    add-int/2addr v1, v2

    shr-int v2, v12, v21

    sub-int v13, v13, v21

    :cond_b
    const/16 v12, 0xf

    if-ge v13, v12, :cond_d

    add-int/lit8 v9, v9, -0x1

    iget-object v12, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    add-int/lit8 v21, v8, 0x1

    aget-byte v8, v12, v8

    and-int/lit16 v8, v8, 0xff

    shl-int/2addr v8, v13

    or-int/2addr v2, v8

    add-int/lit8 v13, v13, 0x8

    if-nez v10, :cond_c

    move/from16 v8, v21

    if-eqz v10, :cond_b

    goto :goto_6

    :cond_c
    move v8, v2

    move/from16 v2, p2

    goto :goto_7

    :cond_d
    :goto_6
    and-int v11, v2, v16

    add-int v0, v5, v11

    mul-int/lit8 v20, v0, 0x3

    move-object v0, v4

    move/from16 v21, v8

    move v8, v2

    move v2, v5

    :goto_7
    aget v12, v0, v20

    :goto_8
    add-int/lit8 v22, v20, 0x1

    aget v24, v0, v22

    shr-int v8, v8, v24

    aget v22, v0, v22

    sub-int v13, v13, v22

    and-int/lit8 v22, v12, 0x10

    if-eqz v22, :cond_29

    move/from16 p3, v3

    and-int/lit8 v3, v12, 0xf

    if-nez v10, :cond_2a

    :goto_9
    if-ge v13, v3, :cond_10

    add-int/lit8 v9, v9, -0x1

    iget-object v12, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    add-int/lit8 v22, v21, 0x1

    aget-byte v12, v12, v21

    and-int/lit16 v12, v12, 0xff

    shl-int/2addr v12, v13

    or-int/2addr v8, v12

    add-int/lit8 v13, v13, 0x8

    if-nez v10, :cond_f

    if-eqz v10, :cond_e

    move/from16 v24, v22

    move/from16 v22, v9

    goto :goto_a

    :cond_e
    move/from16 v21, v22

    goto :goto_9

    :cond_f
    move-object/from16 v0, p1

    move/from16 v1, p3

    move-object v2, v4

    move v3, v5

    move-object v4, v6

    move-object v5, v7

    move v7, v9

    move v11, v13

    move v12, v14

    move v13, v15

    move/from16 v14, v16

    move/from16 v15, v18

    move/from16 v6, v22

    move-object/from16 v30, v10

    move v10, v8

    move-object/from16 v8, v30

    goto/16 :goto_12

    :cond_10
    move/from16 v22, v9

    move/from16 v24, v21

    :goto_a
    add-int/lit8 v9, v20, 0x2

    aget v9, v0, v9

    sget-object v12, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->a:[I

    aget v12, v12, v3

    and-int/2addr v12, v8

    add-int/2addr v9, v12

    shr-int v25, v8, v3

    sub-int v26, v13, v3

    sub-int v27, v15, v1

    if-nez v10, :cond_14

    if-lt v14, v9, :cond_14

    sub-int v8, v14, v9

    if-nez v10, :cond_12

    sub-int v12, v14, v8

    const/4 v13, 0x2

    if-lez v12, :cond_11

    if-le v13, v12, :cond_11

    iget-object v12, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    add-int/lit8 v15, v14, 0x1

    iget-object v13, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    add-int/lit8 v21, v8, 0x1

    aget-byte v8, v13, v8

    aput-byte v8, v12, v14

    iget-object v8, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    add-int/lit8 v14, v15, 0x1

    iget-object v12, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    add-int/lit8 v13, v21, 0x1

    aget-byte v12, v12, v21

    aput-byte v12, v8, v15

    add-int/lit8 v1, v1, -0x2

    if-eqz v10, :cond_1b

    move v8, v13

    :cond_11
    iget-object v12, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    iget-object v13, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    const/4 v15, 0x2

    invoke-static {v12, v8, v13, v14, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v14, v15

    add-int/2addr v8, v15

    add-int/lit8 v1, v1, -0x2

    :cond_12
    if-eqz v10, :cond_13

    goto :goto_b

    :cond_13
    move v13, v8

    goto :goto_f

    :cond_14
    :goto_b
    move v3, v14

    sub-int v8, v3, v9

    :cond_15
    iget v9, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    :goto_c
    add-int/2addr v8, v9

    if-ltz v8, :cond_15

    iget v9, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->P:I

    sub-int/2addr v9, v8

    if-nez v10, :cond_28

    if-nez v10, :cond_1c

    if-le v1, v9, :cond_1a

    sub-int/2addr v1, v9

    sub-int v12, v3, v8

    if-nez v10, :cond_19

    if-lez v12, :cond_17

    if-nez v10, :cond_18

    if-le v9, v12, :cond_17

    :goto_d
    iget-object v12, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    add-int/lit8 v13, v3, 0x1

    iget-object v14, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    add-int/lit8 v15, v8, 0x1

    aget-byte v8, v14, v8

    aput-byte v8, v12, v3

    add-int/lit8 v9, v9, -0x1

    if-nez v9, :cond_16

    move v3, v9

    move v14, v13

    goto :goto_e

    :cond_16
    move v3, v13

    move v8, v15

    goto :goto_d

    :cond_17
    iget-object v12, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    iget-object v13, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    invoke-static {v12, v8, v13, v3, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v3, v9

    :cond_18
    move/from16 v12, v19

    :cond_19
    move v14, v3

    move v3, v12

    :goto_e
    move/from16 v13, v19

    goto :goto_f

    :cond_1a
    move v14, v3

    move v13, v8

    move v3, v9

    :cond_1b
    :goto_f
    move v12, v3

    move v8, v13

    move v9, v8

    move v3, v1

    move v1, v14

    goto :goto_10

    :cond_1c
    move v14, v3

    move v12, v9

    move v3, v1

    :goto_10
    sub-int/2addr v1, v9

    if-nez v10, :cond_20

    if-lez v1, :cond_1e

    sub-int v1, v14, v8

    if-nez v10, :cond_1f

    if-le v3, v1, :cond_1e

    :goto_11
    iget-object v0, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    add-int/lit8 v1, v14, 0x1

    iget-object v2, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    add-int/lit8 v9, v8, 0x1

    aget-byte v2, v2, v8

    aput-byte v2, v0, v14

    add-int/lit8 v3, v3, -0x1

    if-nez v3, :cond_1d

    move-object/from16 v0, p1

    move v12, v1

    move-object v2, v4

    move v3, v5

    move-object v4, v6

    move-object v5, v7

    move-object v8, v10

    move/from16 v14, v16

    move/from16 v15, v18

    move/from16 v7, v22

    move/from16 v6, v24

    move/from16 v10, v25

    move/from16 v11, v26

    move/from16 v13, v27

    move/from16 v1, p3

    goto :goto_12

    :cond_1d
    move v14, v1

    move v8, v9

    goto :goto_11

    :cond_1e
    iget-object v1, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    iget-object v9, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->O:[B

    invoke-static {v1, v8, v9, v14, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v14, v3

    :cond_1f
    move/from16 v1, v19

    :cond_20
    if-eqz v10, :cond_21

    move/from16 v9, v22

    move/from16 v21, v24

    move/from16 v8, v25

    move/from16 v13, v26

    move/from16 v15, v27

    goto/16 :goto_15

    :cond_21
    move-object/from16 v0, p1

    move/from16 v1, p3

    move-object v2, v4

    move v3, v5

    move-object v4, v6

    move-object v5, v7

    move-object v8, v10

    move v12, v14

    move/from16 v14, v16

    move/from16 v15, v18

    move/from16 v7, v22

    move/from16 v6, v24

    move/from16 v10, v25

    move/from16 v11, v26

    move/from16 v13, v27

    :goto_12
    const/16 v9, 0x102

    if-nez v8, :cond_24

    if-lt v13, v9, :cond_23

    const/16 v9, 0xa

    if-nez v8, :cond_22

    if-ge v7, v9, :cond_2

    goto :goto_13

    :cond_22
    move v13, v7

    goto :goto_14

    :cond_23
    :goto_13
    iget v13, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    move v9, v7

    :cond_24
    :goto_14
    sub-int/2addr v13, v9

    shr-int/lit8 v0, v11, 0x3

    if-nez v8, :cond_25

    if-ge v0, v13, :cond_26

    move v0, v11

    move/from16 v13, v17

    :cond_25
    shr-int v13, v0, v13

    :cond_26
    add-int/2addr v7, v13

    sub-int/2addr v6, v13

    shl-int/lit8 v0, v13, 0x3

    sub-int/2addr v11, v0

    iput v10, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v11, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iput v7, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-wide v0, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget v2, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v2, v6, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iput v6, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v12, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v0

    if-nez v0, :cond_27

    const/4 v0, 0x1

    new-array v0, v0, [I

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b([I)V

    :cond_27
    return v19

    :cond_28
    move v8, v1

    goto/16 :goto_c

    :cond_29
    move/from16 p3, v3

    :goto_15
    and-int/lit8 v3, v12, 0x40

    :cond_2a
    if-nez v10, :cond_2d

    if-nez v3, :cond_2c

    add-int/lit8 v20, v20, 0x2

    aget v3, v0, v20

    add-int/2addr v11, v3

    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->a:[I

    aget v3, v3, v12

    and-int/2addr v3, v8

    add-int/2addr v11, v3

    add-int v3, v2, v11

    mul-int/lit8 v20, v3, 0x3

    aget v12, v0, v20

    if-eqz v10, :cond_2b

    goto :goto_16

    :cond_2b
    move/from16 v3, p3

    goto/16 :goto_8

    :cond_2c
    :goto_16
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->K:[Ljava/lang/String;

    aget-object v0, v0, v19

    iput-object v0, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    iget v0, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    sub-int v1, v0, v9

    shr-int/lit8 v3, v13, 0x3

    :cond_2d
    if-nez v10, :cond_2e

    if-ge v3, v1, :cond_2f

    move v3, v13

    move/from16 v1, v17

    :cond_2e
    shr-int v1, v3, v1

    :cond_2f
    add-int/2addr v9, v1

    sub-int v0, v21, v1

    shl-int/lit8 v1, v1, 0x3

    sub-int/2addr v13, v1

    iput v8, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v13, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iput v9, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-wide v1, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget v3, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v3, v0, v3

    int-to-long v3, v3

    add-long/2addr v1, v3

    iput-wide v1, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iput v0, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    :goto_17
    iput v14, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    return v23

    :cond_30
    move-object/from16 p1, v1

    move/from16 p2, v2

    move/from16 p3, v3

    move/from16 v30, v11

    move-object v11, v10

    move v10, v12

    move/from16 v12, v30

    :goto_18
    add-int/lit8 v21, v20, 0x1

    aget v23, v0, v21

    shr-int v10, v10, v23

    aget v21, v0, v21

    sub-int v13, v13, v21

    and-int/lit8 v21, v22, 0x10

    move/from16 v30, v12

    move v12, v10

    move-object v10, v11

    move/from16 v11, v30

    goto/16 :goto_4

    :cond_31
    :goto_19
    and-int/lit8 v21, v22, 0x20

    :cond_32
    if-nez v10, :cond_36

    if-eqz v21, :cond_35

    iget v0, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    sub-int/2addr v0, v9

    shr-int/lit8 v1, v13, 0x3

    if-nez v10, :cond_33

    if-ge v1, v0, :cond_34

    move v1, v13

    move/from16 v0, v17

    :cond_33
    shr-int v0, v1, v0

    :cond_34
    add-int/2addr v9, v0

    sub-int/2addr v8, v0

    shl-int/lit8 v0, v0, 0x3

    sub-int/2addr v13, v0

    iput v12, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v13, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iput v9, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-wide v0, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget v2, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v2, v8, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iput v8, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iput v14, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->R:I

    const/4 v0, 0x1

    return v0

    :cond_35
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->K:[Ljava/lang/String;

    aget-object v0, v0, v17

    iput-object v0, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    iget v0, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    sub-int v21, v0, v9

    :cond_36
    move/from16 v0, v21

    shr-int/lit8 v1, v13, 0x3

    if-nez v10, :cond_37

    if-ge v1, v0, :cond_38

    move v1, v13

    move/from16 v0, v17

    :cond_37
    shr-int v0, v1, v0

    :cond_38
    add-int/2addr v9, v0

    sub-int/2addr v8, v0

    shl-int/lit8 v0, v0, 0x3

    sub-int/2addr v13, v0

    iput v12, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->M:I

    iput v13, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->L:I

    iput v9, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-wide v0, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget v2, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v2, v8, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iput v8, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    goto/16 :goto_17
.end method

.method a(II[II[II)V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->u:I

    int-to-byte p1, p1

    iput-byte p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->C:B

    int-to-byte p1, p2

    iput-byte p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->D:B

    iput-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->E:[I

    iput p4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->F:I

    iput-object p5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->G:[I

    iput p6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->H:I

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfCodes;->w:[I

    return-void
.end method

.method a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)V
    .locals 0

    return-void
.end method
