.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x6

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x2

    const/4 v4, 0x0

    const-string v5, ".)\u0003/9j\u0003/9j\u000399j"

    const/16 v6, 0xe

    move v8, v3

    move v9, v4

    const/4 v7, -0x1

    :goto_0
    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v8

    invoke-virtual {v5, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x5

    move v14, v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v15, v11

    move v0, v4

    :goto_2
    if-gt v15, v0, :cond_3

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v11, v9, 0x1

    if-eqz v13, :cond_1

    aput-object v0, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v11

    const/4 v0, 0x6

    goto :goto_0

    :cond_0
    const-string v5, "\u0010\u0017\u0003\u0007\u0007T"

    move v8, v3

    move v9, v11

    const/4 v6, 0x6

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v0, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v8, v0

    move v9, v11

    :goto_3
    const/16 v14, 0x3b

    add-int/2addr v7, v10

    add-int v0, v7, v8

    invoke-virtual {v5, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v13, v4

    const/4 v0, 0x6

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v0

    rem-int/lit8 v2, v0, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    const/4 v3, 0x4

    if-eq v2, v3, :cond_5

    if-eq v2, v12, :cond_4

    const/16 v2, 0xd

    goto :goto_4

    :cond_4
    const/16 v2, 0x6d

    goto :goto_4

    :cond_5
    const/16 v2, 0x72

    goto :goto_4

    :cond_6
    const/4 v2, 0x7

    goto :goto_4

    :cond_7
    const/16 v2, 0x2e

    goto :goto_4

    :cond_8
    const/16 v2, 0x6f

    goto :goto_4

    :cond_9
    const/16 v2, 0x6e

    :goto_4
    xor-int/2addr v2, v14

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v0

    add-int/lit8 v0, v0, 0x1

    const/4 v3, 0x2

    goto :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    move-result-object p0

    return-object p0
.end method

.method public static init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;
    .locals 11

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->values()[Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_7

    aget-object v4, v0, v3

    iget-object v5, v4, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->algorithm:Ljava/lang/String;

    const/4 v6, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v7

    const/16 v8, 0x89e

    const/4 v9, 0x2

    const/4 v10, 0x1

    if-eq v7, v8, :cond_2

    const v8, 0x10992

    if-eq v7, v8, :cond_1

    const v8, 0x13e20

    if-eq v7, v8, :cond_0

    goto :goto_1

    :cond_0
    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->a:[Ljava/lang/String;

    const/4 v8, 0x5

    aget-object v7, v7, v8

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v6, v2

    goto :goto_1

    :cond_1
    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->a:[Ljava/lang/String;

    aget-object v7, v7, v9

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v6, v10

    goto :goto_1

    :cond_2
    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->a:[Ljava/lang/String;

    const/4 v8, 0x4

    aget-object v7, v7, v8

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v6, v9

    :cond_3
    :goto_1
    if-eqz v6, :cond_6

    if-eq v6, v10, :cond_5

    if-eq v6, v9, :cond_4

    goto :goto_2

    :cond_4
    new-array v5, v10, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    iget-object v4, v4, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifier:Ljava/lang/String;

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->codecFor(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;

    move-result-object v7

    invoke-direct {v6, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/util/h/I;)V

    aput-object v6, v5, v2

    invoke-virtual {p0, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    goto :goto_2

    :cond_5
    new-array v5, v10, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    iget-object v4, v4, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifier:Ljava/lang/String;

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;->codecFor(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;

    move-result-object v7

    invoke-direct {v6, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/util/h/I;)V

    aput-object v6, v5, v2

    invoke-virtual {p0, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    goto :goto_2

    :cond_6
    new-array v5, v10, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    iget-object v4, v4, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifier:Ljava/lang/String;

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/RsaPublicKeyCodec;->codecFor(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/RsaPublicKeyCodec;

    move-result-object v7

    invoke-direct {v6, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/util/h/I;)V

    aput-object v6, v5, v2

    invoke-virtual {p0, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_7
    return-object p0
.end method

.method public static init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;
    .locals 11

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->values()[Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_7

    aget-object v4, v0, v3

    iget-object v5, v4, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->algorithm:Ljava/lang/String;

    const/4 v6, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v7

    const/16 v8, 0x89e

    const/4 v9, 0x2

    const/4 v10, 0x1

    if-eq v7, v8, :cond_2

    const v8, 0x10992

    if-eq v7, v8, :cond_1

    const v8, 0x13e20

    if-eq v7, v8, :cond_0

    goto :goto_1

    :cond_0
    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->a:[Ljava/lang/String;

    const/4 v8, 0x3

    aget-object v7, v7, v8

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v6, v2

    goto :goto_1

    :cond_1
    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->a:[Ljava/lang/String;

    aget-object v7, v7, v10

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v6, v10

    goto :goto_1

    :cond_2
    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeys;->a:[Ljava/lang/String;

    aget-object v7, v7, v2

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v6, v9

    :cond_3
    :goto_1
    if-eqz v6, :cond_6

    if-eq v6, v10, :cond_5

    if-eq v6, v9, :cond_4

    goto :goto_2

    :cond_4
    new-array v5, v10, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    iget-object v4, v4, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifier:Ljava/lang/String;

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->codecFor(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;

    move-result-object v7

    invoke-direct {v6, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/util/h/I;)V

    aput-object v6, v5, v2

    invoke-virtual {p0, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    goto :goto_2

    :cond_5
    new-array v5, v10, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    iget-object v4, v4, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifier:Ljava/lang/String;

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;->codecFor(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaPublicKeyCodec;

    move-result-object v7

    invoke-direct {v6, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/util/h/I;)V

    aput-object v6, v5, v2

    invoke-virtual {p0, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    goto :goto_2

    :cond_6
    new-array v5, v10, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;

    iget-object v4, v4, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifier:Ljava/lang/String;

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/RsaPublicKeyCodec;->codecFor(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/RsaPublicKeyCodec;

    move-result-object v7

    invoke-direct {v6, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/util/h/I;)V

    aput-object v6, v5, v2

    invoke-virtual {p0, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec;

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_7
    return-object p0
.end method
