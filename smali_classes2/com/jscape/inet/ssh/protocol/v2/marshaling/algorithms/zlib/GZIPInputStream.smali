.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;
.super Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;


# static fields
.field private static final g:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x1f

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x7c

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "|!!\u000cqcAri-\u001c:~[ki\'\u000evsAs(0\n~0Mz=j\u0008q&d\u0006t`Ak\u0008q&d\u0006t`Ak"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x31

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->g:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v13, 0x2

    if-eq v12, v13, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x48

    goto :goto_2

    :cond_2
    const/16 v12, 0x6c

    goto :goto_2

    :cond_3
    const/16 v12, 0x66

    goto :goto_2

    :cond_4
    const/16 v12, 0x13

    goto :goto_2

    :cond_5
    const/16 v12, 0x38

    goto :goto_2

    :cond_6
    const/16 v12, 0x35

    goto :goto_2

    :cond_7
    const/16 v12, 0x63

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x200

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;-><init>(Ljava/io/InputStream;IZ)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;IZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    const/16 v1, 0x1f

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;-><init>(I)V

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;-><init>(Ljava/io/InputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;IZ)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->myinflater:Z

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InflaterInputStream;-><init>(Ljava/io/InputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;IZ)V

    return-void
.end method

.method private a([B)I
    .locals 7

    array-length v0, p1

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v1

    const/4 v2, 0x0

    :cond_0
    const/4 v3, -0x1

    :try_start_0
    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->in:Ljava/io/InputStream;

    array-length v5, p1

    sub-int/2addr v5, v2

    invoke-virtual {v4, p1, v2, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move v4, v3

    :goto_0
    move v5, v4

    :goto_1
    if-nez v1, :cond_2

    if-ne v5, v3, :cond_1

    goto :goto_3

    :cond_1
    add-int v5, v2, v4

    move v6, v0

    move v2, v5

    goto :goto_2

    :cond_2
    move v6, v3

    :goto_2
    if-lt v5, v6, :cond_0

    :goto_3
    move v5, v2

    if-nez v1, :cond_3

    return v5

    :cond_3
    move v2, v5

    goto :goto_1
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public getCRC()J
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    if-nez v0, :cond_1

    iget v0, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->g:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->getGZIPHeader()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->getCRC()J

    move-result-wide v0

    return-wide v0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->getGZIPHeader()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->getComment()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getModifiedtime()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->getGZIPHeader()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->getModifiedTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->getGZIPHeader()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOS()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->getGZIPHeader()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->getOS()I

    move-result v0

    return v0
.end method

.method public readHeader()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [B

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v3, v1, v0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->setOutput([BII)V

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v3, v1, v0, v0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->setInput([BIIZ)V

    const/16 v1, 0xa

    new-array v3, v1, [B

    invoke-direct {p0, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->a([B)I

    move-result v4

    const/4 v5, 0x1

    if-nez v2, :cond_2

    if-eq v4, v1, :cond_1

    if-lez v4, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v1, v3, v0, v4, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->setInput([BIIZ)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iput v0, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->nextInIndex:I

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iput v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->availIn:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->g:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v1, v3, v0, v4, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->setInput([BIIZ)V

    move v4, v5

    :cond_2
    new-array v1, v4, [B

    :cond_3
    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->availIn:I

    if-gtz v3, :cond_5

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v3, v1}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_4

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v3, v1, v0, v5, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->setInput([BIIZ)V

    goto :goto_1

    :cond_4
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->g:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_5
    :goto_1
    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->inflate(I)I

    move-result v3

    if-nez v2, :cond_9

    if-eqz v3, :cond_8

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->nextIn:[B

    array-length v1, v1

    rsub-int v1, v1, 0x800

    if-nez v2, :cond_7

    if-lez v1, :cond_6

    new-array v1, v1, [B

    invoke-direct {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->a([B)I

    move-result v3

    if-nez v2, :cond_7

    if-lez v3, :cond_6

    :try_start_2
    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget v4, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->availIn:I

    iget-object v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->nextInIndex:I

    add-int/2addr v4, v6

    iput v4, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->availIn:I

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iput v0, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->nextInIndex:I

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v2, v1, v0, v3, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->setInput([BIIZ)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_6
    :goto_2
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->availIn:I

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->nextInIndex:I

    add-int/2addr v2, v3

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->availIn:I

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iput v0, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->nextInIndex:I

    :cond_7
    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->msg:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPInputStream;->inflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    invoke-virtual {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->e()Z

    move-result v3

    :cond_9
    if-nez v3, :cond_3

    return-void
.end method
