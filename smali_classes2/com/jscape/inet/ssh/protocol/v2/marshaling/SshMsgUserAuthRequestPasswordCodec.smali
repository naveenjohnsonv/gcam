.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPasswordCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$MethodCodec;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public read(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/a;->a(Ljava/io/InputStream;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUtf8Value(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUtf8Value(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword;

    invoke-direct {v1, p2, p3, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_0
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUtf8Value(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPassword;

    invoke-direct {v0, p2, p3, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPassword;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequest;Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/MessageCodec;->b()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    instance-of v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v1, :cond_0

    move-object v1, p1

    check-cast v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword;

    const/4 v2, 0x1

    :try_start_1
    invoke-static {v2, p2}, Lcom/jscape/util/h/a/a;->a(ZLjava/io/OutputStream;)V

    iget-object v2, v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword;->oldPassword:Ljava/lang/String;

    invoke-static {v2, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestNewPassword;->newPassword:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPasswordCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestPasswordCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPassword;

    const/4 v0, 0x0

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/a;->a(ZLjava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestPassword;->password:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V

    :cond_1
    return-void
.end method
