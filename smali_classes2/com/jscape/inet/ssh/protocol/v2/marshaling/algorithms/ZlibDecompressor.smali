.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;


# static fields
.field private static final a:I = 0x9000

.field private static final f:Ljava/lang/String;


# instance fields
.field private final b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

.field private final c:[B

.field private d:I

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "F2jV\u0010,5\u007f1nDB-#o7lZ\u0010-\"n1q\u001a\u0010\u001b$}*vG\nh"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->f:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x24

    goto :goto_1

    :cond_1
    const/16 v4, 0x3c

    goto :goto_1

    :cond_2
    const/16 v4, 0x44

    goto :goto_1

    :cond_3
    const/16 v4, 0x40

    goto :goto_1

    :cond_4
    const/16 v4, 0x77

    goto :goto_1

    :cond_5
    const/16 v4, 0x2a

    goto :goto_1

    :cond_6
    const/16 v4, 0x68

    :goto_1
    const/16 v5, 0x74

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->inflateInit()I

    const v0, 0x9000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->c:[B

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->c:[B

    iput-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->b()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    const/4 v2, 0x0

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOutIndex:I

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->c:[B

    array-length v3, v3

    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->inflate(I)I

    move-result v1

    const/4 v4, -0x5

    if-nez v0, :cond_2

    if-eqz v1, :cond_1

    if-ne v1, v4, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/Exception;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->c:[B

    array-length v5, v5

    iget-object v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    sub-int/2addr v5, v6

    iput v5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->d:I

    :cond_2
    if-nez v0, :cond_3

    if-ne v1, v4, :cond_4

    move v2, v3

    goto :goto_1

    :cond_3
    move v2, v1

    :cond_4
    :goto_1
    iput-boolean v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->e:Z

    return-void
.end method

.method private a([BII)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput-object p1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput p2, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput p3, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->e:Z

    return-void
.end method


# virtual methods
.method public apply([BIILjava/io/OutputStream;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression$OperationException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac$OperationException;->c()Z

    move-result v0

    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->a([BII)V

    const/4 p1, 0x0

    move-object p2, p0

    move p3, p1

    :cond_0
    invoke-direct {p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->a()V

    iget-boolean v1, p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->e:Z

    if-nez v1, :cond_1

    iget-object v1, p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->c:[B

    iget v2, p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->d:I

    invoke-virtual {p4, v1, p1, v2}, Ljava/io/OutputStream;->write([BII)V

    iget v1, p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->d:I

    add-int/2addr p3, v1

    :cond_1
    iget-boolean v1, p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;->e:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    if-eqz v0, :cond_1

    return p3

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression$OperationException;

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression$OperationException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method
