.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;
.super Ljava/lang/Object;


# static fields
.field private static final A:I = 0x9

.field private static final B:I = 0xa

.field private static final C:I = 0xb

.field private static final D:I = 0xc

.field private static final E:I = 0xd

.field private static final F:I = 0xe

.field private static final G:I = 0xf

.field private static final H:I = 0x10

.field private static final I:I = 0x11

.field private static final J:I = 0x12

.field private static final K:I = 0x13

.field private static final L:I = 0x14

.field private static final M:I = 0x15

.field private static final N:I = 0x16

.field private static final O:I = 0x17

.field static final P:I = 0x40000000

.field private static final a:I = 0xf

.field private static ad:[B = null

.field private static final b:I = 0x20

.field private static final bb:[Ljava/lang/String;

.field static final c:I = 0x0

.field static final d:I = 0x1

.field static final e:I = 0x2

.field static final f:I = 0x3

.field static final g:I = 0x4

.field private static final h:I = 0x8

.field private static final i:I = 0x0

.field private static final j:I = 0x1

.field private static final k:I = 0x2

.field private static final l:I = -0x1

.field private static final m:I = -0x2

.field private static final n:I = -0x3

.field private static final o:I = -0x4

.field private static final p:I = -0x5

.field private static final q:I = -0x6

.field private static final r:I = 0x0

.field private static final s:I = 0x1

.field private static final t:I = 0x2

.field private static final u:I = 0x3

.field private static final v:I = 0x4

.field private static final w:I = 0x5

.field private static final x:I = 0x6

.field private static final y:I = 0x7

.field private static final z:I = 0x8


# instance fields
.field Q:I

.field R:I

.field S:J

.field T:J

.field U:I

.field V:I

.field W:I

.field X:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

.field private final Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

.field private Z:I

.field private aa:I

.field private ab:[B

.field ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

.field private ae:Ljava/io/ByteArrayOutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "YRXJ!\u0001!TCTAo\u0004:N\u001aBYV@n\u0012&\u0017TRCq\u0017-DDTAoE%RCUAe\u001aBYV@n\u0012&\u0017TRCq\u0017-DDTAoE%RCUAe\u0013^YKOm\u000c,\u0017@T@e\n?\u0017DTTd\u0016^Y^As\u0017-TC\u001dFd\u0004,RE\u001dMi\u0000+\\\u0016^Y^As\u0017-TC\u001dBd\u000b/C_\u001dMi\u0000+\\\u0014^Y^As\u0017-TC\u001dJ`\u0011)\u0017TUKb\u000e\u0016UVY\u000ed\u001d<EV\u001dHh\u0000$S\u0017QKo\u0002<_\u0018BYV@n\u0012&\u0017_XOe\u0000:\u0017QQOf\u0016hDRI\u0013_R\\Jd\u0017hTE^\u000el\u000c;ZVIMi"

    const/16 v4, 0xe0

    const/16 v5, 0xf

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x24

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    const/4 v14, 0x4

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x2b

    const/16 v3, 0x14

    const-string v5, "?8? \u0012vL5\"|+\u0001pHv54*\u0003o\u0016?8? \u0012vL5\"|\'\u0005eM3$|,\u0008aJ="

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v17, v5

    move v5, v3

    move-object/from16 v3, v17

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x45

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->bb:[Ljava/lang/String;

    new-array v0, v14, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ad:[B

    return-void

    :cond_3
    aget-char v15, v10, v13

    rem-int/lit8 v1, v13, 0x7

    const/16 v16, 0x13

    if-eqz v1, :cond_8

    if-eq v1, v9, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    if-eq v1, v14, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v16, 0x6c

    goto :goto_4

    :cond_4
    const/16 v16, 0x41

    goto :goto_4

    :cond_5
    const/16 v16, 0x25

    goto :goto_4

    :cond_6
    const/16 v16, 0xa

    goto :goto_4

    :cond_7
    const/16 v16, 0x19

    :cond_8
    :goto_4
    xor-int v1, v8, v16

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        -0x1t
        -0x1t
    .end array-data
.end method

.method constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->S:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->aa:I

    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ab:[B

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ae:Ljava/io/ByteArrayOutputStream;

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    return-void
.end method

.method private a(II)I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ae:Ljava/io/ByteArrayOutputStream;
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ae:Ljava/io/ByteArrayOutputStream;

    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    if-eqz v1, :cond_5

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    iput v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v3, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    aget-byte p1, p1, v1

    if-nez v0, :cond_3

    if-eqz p1, :cond_2

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ae:Ljava/io/ByteArrayOutputStream;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    invoke-virtual {v1, v3, v4, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_2
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1

    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    invoke-interface {v1, v3, v4, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->update([BII)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    add-int/2addr v3, v2

    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    :cond_3
    if-nez p1, :cond_4

    return p2

    :cond_4
    move p1, p2

    goto :goto_1

    :cond_5
    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;-><init>(I)V

    throw p2
.end method

.method private a(III)I
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    :try_start_0
    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->aa:I
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_0 .. :try_end_0} :catch_8

    const/4 v2, -0x1

    if-nez v0, :cond_1

    if-ne v1, v2, :cond_0

    :try_start_1
    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->aa:I

    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J
    :try_end_1
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_1 .. :try_end_1} :catch_9

    :cond_0
    move-object v3, p0

    goto :goto_2

    :cond_1
    move-object v3, p0

    :goto_0
    if-lez v1, :cond_5

    :try_start_2
    iget-object v1, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I
    :try_end_2
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_2 .. :try_end_2} :catch_0

    if-nez v0, :cond_6

    if-nez v0, :cond_3

    if-eqz v1, :cond_2

    move p2, p3

    goto :goto_1

    :cond_2
    :try_start_3
    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    invoke-direct {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;-><init>(I)V

    throw p1
    :try_end_3
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_3 .. :try_end_3} :catch_3

    :cond_3
    move p2, v1

    :goto_1
    iget-object v1, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v1, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-wide v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    iget-object v1, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v6, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v7, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    add-int/lit8 v8, v7, 0x1

    iput v8, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    aget-byte v1, v1, v7

    and-int/lit16 v1, v1, 0xff

    iget v6, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->aa:I

    sub-int v7, p1, v6

    mul-int/lit8 v7, v7, 0x8

    shl-int/2addr v1, v7

    int-to-long v7, v1

    or-long/2addr v4, v7

    iput-wide v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    add-int/lit8 v6, v6, -0x1

    iput v6, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->aa:I

    if-eqz v0, :cond_4

    goto :goto_3

    :cond_4
    :goto_2
    iget v1, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->aa:I

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_6 .. :try_end_6} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1

    :cond_5
    :goto_3
    move v1, p1

    :cond_6
    const/4 p3, 0x2

    if-nez v0, :cond_8

    if-ne v1, p3, :cond_7

    :try_start_7
    iget-wide v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    const-wide/32 v6, 0xffff

    and-long/2addr v4, v6

    iput-wide v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J
    :try_end_7
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_7 .. :try_end_7} :catch_4

    if-eqz v0, :cond_9

    goto :goto_4

    :catch_4
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_8 .. :try_end_8} :catch_5

    :catch_5
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_9 .. :try_end_9} :catch_6

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1

    :cond_7
    :goto_4
    if-nez v0, :cond_a

    const/4 p3, 0x4

    goto :goto_5

    :cond_8
    move p1, v1

    :goto_5
    if-ne p1, p3, :cond_9

    :try_start_a
    iget-wide v0, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    const-wide v4, 0xffffffffL

    and-long/2addr v0, v4

    iput-wide v0, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J
    :try_end_a
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_a .. :try_end_a} :catch_7

    goto :goto_6

    :catch_7
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1

    :cond_9
    :goto_6
    iput v2, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->aa:I

    move p1, p2

    :cond_a
    return p1

    :catch_8
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_b .. :try_end_b} :catch_9

    :catch_9
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1
.end method

.method private static a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;
    .locals 0

    return-object p0
.end method

.method private a(IJ)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const/4 v1, 0x0

    move v2, v1

    :cond_0
    if-ge v2, p1, :cond_1

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ab:[B

    const-wide/16 v4, 0xff

    and-long/2addr v4, p2

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v3, v2

    const/16 v3, 0x8

    shr-long/2addr p2, v3

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_2

    if-eqz v0, :cond_0

    :cond_1
    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object p2, p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    iget-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ab:[B

    invoke-interface {p2, p3, v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->update([BII)V

    :cond_2
    return-void
.end method

.method private b(II)I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ae:Ljava/io/ByteArrayOutputStream;
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ae:Ljava/io/ByteArrayOutputStream;

    :cond_1
    iget-wide v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_4

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I
    :try_end_2
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_2 .. :try_end_2} :catch_2

    if-nez v0, :cond_5

    const-wide/16 v2, 0x1

    const/4 v4, 0x1

    if-nez v0, :cond_3

    if-eqz v1, :cond_2

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    sub-int/2addr v1, v4

    iput v1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v5, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    add-long/2addr v5, v2

    iput-wide v5, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    aget-byte p1, p1, v1

    move p1, p2

    goto :goto_1

    :cond_2
    :try_start_3
    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;-><init>(I)V

    throw p2
    :try_end_3
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_3 .. :try_end_3} :catch_4

    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ae:Ljava/io/ByteArrayOutputStream;

    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    invoke-virtual {v1, v5, v6, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    iget-object v5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    invoke-interface {v1, v5, v6, v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->update([BII)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    add-int/2addr v5, v4

    iput v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-wide v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    sub-long/2addr v4, v2

    iput-wide v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    if-eqz v0, :cond_1

    goto :goto_2

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object p1

    throw p1

    :cond_4
    :goto_2
    move v1, p1

    :cond_5
    return v1
.end method


# virtual methods
.method a()I
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    const/4 v0, -0x2

    return v0

    :cond_0
    const-wide/16 v2, 0x0

    iput-wide v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalOut:J

    iput-wide v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    :cond_1
    const/4 v0, 0x0

    iput-object v0, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    const/16 v0, 0xe

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->aa:I

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->X:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->a()V

    const/4 v0, 0x0

    return v0
.end method

.method a(I)I
    .locals 8

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->X:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    const/4 v2, 0x0

    iput v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I

    const/4 v3, 0x1

    const/16 v4, 0x30

    if-nez v0, :cond_1

    if-gez p1, :cond_0

    neg-int p1, p1

    if-eqz v0, :cond_8

    :cond_0
    const/high16 v5, 0x40000000    # 2.0f

    and-int/2addr v5, p1

    move v7, v5

    move v5, p1

    move p1, v7

    goto :goto_0

    :cond_1
    move v5, p1

    :goto_0
    const/4 v6, 0x4

    if-nez v0, :cond_4

    if-eqz p1, :cond_3

    iput v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I

    const p1, -0x40000001    # -1.9999999f

    and-int/2addr p1, v5

    if-nez v0, :cond_2

    if-ge p1, v4, :cond_8

    and-int/lit8 p1, p1, 0xf

    if-eqz v0, :cond_8

    move v5, p1

    goto :goto_2

    :cond_2
    :goto_1
    move v5, p1

    goto :goto_4

    :cond_3
    :goto_2
    and-int/lit8 p1, v5, -0x20

    :cond_4
    if-nez v0, :cond_6

    if-eqz p1, :cond_5

    iput v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I

    and-int/lit8 p1, v5, 0xf

    if-eqz v0, :cond_8

    move v5, p1

    :cond_5
    shr-int/lit8 p1, v5, 0x4

    add-int/2addr p1, v3

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I

    move p1, v5

    :cond_6
    if-nez v0, :cond_9

    if-ge p1, v4, :cond_7

    and-int/lit8 p1, v5, 0xf

    goto :goto_3

    :cond_7
    move p1, v5

    :cond_8
    :goto_3
    if-nez v0, :cond_10

    const/16 v4, 0x8

    goto :goto_1

    :cond_9
    :goto_4
    if-lt p1, v4, :cond_f

    if-nez v0, :cond_e

    const/16 p1, 0xf

    if-le v5, p1, :cond_a

    goto :goto_7

    :cond_a
    if-nez v0, :cond_d

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->X:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    if-eqz p1, :cond_c

    iget v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->W:I

    if-nez v0, :cond_b

    if-eq v4, v5, :cond_c

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b()V

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->X:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    goto :goto_5

    :cond_b
    move v2, v4

    goto :goto_6

    :cond_c
    :goto_5
    iput v5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->W:I

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    shl-int v1, v3, v5

    invoke-direct {p1, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;I)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->X:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    :cond_d
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a()I

    :goto_6
    return v2

    :cond_e
    move p1, v5

    goto :goto_8

    :cond_f
    :goto_7
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->b()I

    const/4 p1, -0x2

    :cond_10
    :goto_8
    return p1
.end method

.method a([BI)I
    .locals 10

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    if-eqz v1, :cond_1

    :cond_0
    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    const/4 v2, 0x6

    const/4 v3, 0x0

    if-nez v0, :cond_3

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I

    if-nez v0, :cond_3

    if-eqz v1, :cond_2

    :cond_1
    const/4 p1, -0x2

    return p1

    :cond_2
    move v1, v3

    :cond_3
    iget v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    const/4 v5, 0x1

    if-nez v0, :cond_7

    if-ne v4, v2, :cond_5

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->getValue()J

    move-result-wide v6

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->reset()V

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v2, p1, v3, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->update([BII)V

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->getValue()J

    move-result-wide v8

    cmp-long v2, v8, v6

    if-nez v0, :cond_4

    if-eqz v2, :cond_5

    const/4 p1, -0x3

    return p1

    :cond_4
    move v4, v2

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->reset()V

    move v4, p2

    :goto_0
    if-nez v0, :cond_6

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->W:I

    shl-int v2, v5, v0

    goto :goto_1

    :cond_6
    move v3, v4

    goto :goto_2

    :cond_7
    :goto_1
    if-lt v4, v2, :cond_8

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->W:I

    shl-int v0, v5, v0

    sub-int/2addr v0, v5

    sub-int v1, p2, v0

    move p2, v0

    :cond_8
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->X:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v0, p1, v1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->a([BII)V

    const/4 p1, 0x7

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    :goto_2
    return v3
.end method

.method b()I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->X:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->b()V

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method b(I)I
    .locals 27

    move-object/from16 v1, p0

    move/from16 v0, p1

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v2

    :try_start_0
    iget-object v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;
    :try_end_0
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_0 .. :try_end_0} :catch_64

    const/4 v5, 0x0

    const/4 v6, 0x4

    if-nez v2, :cond_0

    if-eqz v3, :cond_1

    :try_start_1
    iget-object v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;
    :try_end_1
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_1 .. :try_end_1} :catch_65

    :cond_0
    :try_start_2
    iget-object v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B
    :try_end_2
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_2 .. :try_end_2} :catch_60

    if-nez v3, :cond_4

    :cond_1
    if-nez v2, :cond_3

    if-ne v0, v6, :cond_2

    :try_start_3
    iget v0, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I
    :try_end_3
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_3 .. :try_end_3} :catch_0

    if-nez v2, :cond_3

    const/16 v2, 0xe

    if-ne v0, v2, :cond_2

    return v5

    :catch_0
    move-exception v0

    goto/16 :goto_3c

    :cond_2
    const/4 v0, -0x2

    :cond_3
    return v0

    :cond_4
    const/4 v3, -0x5

    if-nez v2, :cond_6

    if-ne v0, v6, :cond_5

    move v0, v3

    goto :goto_0

    :cond_5
    move v0, v5

    :cond_6
    :goto_0
    iget v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    const/16 v4, 0x10

    const/16 v16, 0x18

    const/4 v13, 0x7

    const/4 v14, 0x0

    const-wide/16 v19, 0x1

    const/16 v15, 0x8

    const/16 v8, 0xd

    const/4 v11, 0x2

    const/4 v12, 0x1

    packed-switch v7, :pswitch_data_0

    :goto_1
    const/4 v0, -0x2

    goto/16 :goto_3b

    :pswitch_0
    :try_start_4
    invoke-direct {v1, v11, v3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(III)I

    move-result v3
    :try_end_4
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_4 .. :try_end_4} :catch_6

    :try_start_5
    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    long-to-int v7, v9

    const v9, 0xffff

    and-int/2addr v7, v9

    iput v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Z:I
    :try_end_5
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_5 .. :try_end_5} :catch_4

    and-int/lit16 v7, v7, 0xff

    if-nez v2, :cond_8

    if-eq v7, v15, :cond_7

    :try_start_6
    iget-object v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->bb:[Ljava/lang/String;

    aget-object v9, v9, v11

    iput-object v9, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    iput v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I
    :try_end_6
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_6 .. :try_end_6} :catch_5

    if-eqz v2, :cond_6

    :cond_7
    iget v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Z:I

    const v9, 0xe000

    goto :goto_2

    :cond_8
    move v9, v15

    :goto_2
    and-int/2addr v7, v9

    if-nez v2, :cond_a

    if-eqz v7, :cond_9

    :try_start_7
    iget-object v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->bb:[Ljava/lang/String;

    aget-object v9, v9, v15

    iput-object v9, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    iput v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I
    :try_end_7
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_7 .. :try_end_7} :catch_1

    if-eqz v2, :cond_6

    goto :goto_3

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_9
    :goto_3
    if-nez v2, :cond_b

    :try_start_8
    iget v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Z:I
    :try_end_8
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_8 .. :try_end_8} :catch_2

    and-int/lit16 v7, v7, 0x200

    goto :goto_4

    :catch_2
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_a
    :goto_4
    if-eqz v7, :cond_b

    :try_start_9
    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    invoke-direct {v1, v11, v9, v10}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(IJ)V
    :try_end_9
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_5

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_b
    :goto_5
    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    goto :goto_6

    :catch_4
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_a .. :try_end_a} :catch_5

    :catch_5
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :catch_6
    move-exception v0

    move-object v2, v0

    iget v0, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;->a:I

    return v0

    :goto_6
    :pswitch_1
    :try_start_b
    invoke-direct {v1, v6, v3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(III)I

    move-result v3
    :try_end_b
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_b .. :try_end_b} :catch_27

    if-nez v2, :cond_c

    :try_start_c
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    if-eqz v4, :cond_c

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    iput-wide v9, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->c:J
    :try_end_c
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_c .. :try_end_c} :catch_7

    goto :goto_7

    :catch_7
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_c
    :goto_7
    if-nez v2, :cond_d

    :try_start_d
    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Z:I

    and-int/lit16 v4, v4, 0x200

    if-eqz v4, :cond_d

    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    invoke-direct {v1, v6, v9, v10}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(IJ)V
    :try_end_d
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_d .. :try_end_d} :catch_8

    goto :goto_8

    :catch_8
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_d
    :goto_8
    const/16 v4, 0x11

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    :pswitch_2
    :try_start_e
    invoke-direct {v1, v11, v3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(III)I

    move-result v3
    :try_end_e
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_e .. :try_end_e} :catch_26

    if-nez v2, :cond_e

    :try_start_f
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    if-eqz v4, :cond_e

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    long-to-int v7, v9

    and-int/lit16 v7, v7, 0xff

    iput v7, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->d:I

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    long-to-int v7, v9

    shr-int/2addr v7, v15

    and-int/lit16 v7, v7, 0xff

    iput v7, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->e:I
    :try_end_f
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_f .. :try_end_f} :catch_9

    goto :goto_9

    :catch_9
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_e
    :goto_9
    if-nez v2, :cond_f

    :try_start_10
    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Z:I

    and-int/lit16 v4, v4, 0x200

    if-eqz v4, :cond_f

    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    invoke-direct {v1, v11, v9, v10}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(IJ)V
    :try_end_10
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_10 .. :try_end_10} :catch_a

    goto :goto_a

    :catch_a
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_f
    :goto_a
    const/16 v4, 0x12

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    :pswitch_3
    if-nez v2, :cond_11

    :try_start_11
    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Z:I
    :try_end_11
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_11 .. :try_end_11} :catch_f

    and-int/lit16 v4, v4, 0x400

    if-eqz v4, :cond_11

    :try_start_12
    invoke-direct {v1, v11, v3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(III)I

    move-result v3
    :try_end_12
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_12 .. :try_end_12} :catch_e

    if-nez v2, :cond_10

    :try_start_13
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    if-eqz v4, :cond_10

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    long-to-int v7, v9

    const v9, 0xffff

    and-int/2addr v7, v9

    new-array v7, v7, [B

    iput-object v7, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->f:[B
    :try_end_13
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_13 .. :try_end_13} :catch_b

    goto :goto_b

    :catch_b
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_10
    :goto_b
    if-nez v2, :cond_12

    :try_start_14
    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Z:I
    :try_end_14
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_14 .. :try_end_14} :catch_c

    and-int/lit16 v4, v4, 0x200

    if-eqz v4, :cond_12

    :try_start_15
    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    invoke-direct {v1, v11, v9, v10}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(IJ)V

    if-eqz v2, :cond_12

    goto :goto_c

    :catch_c
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_15
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_15 .. :try_end_15} :catch_d

    :catch_d
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :catch_e
    move-exception v0

    move-object v2, v0

    iget v0, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;->a:I

    return v0

    :catch_f
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_11
    :goto_c
    if-nez v2, :cond_12

    :try_start_16
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    if-eqz v4, :cond_12

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iput-object v14, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->f:[B
    :try_end_16
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_16 .. :try_end_16} :catch_10

    goto :goto_d

    :catch_10
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_12
    :goto_d
    const/16 v4, 0x13

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    :pswitch_4
    if-nez v2, :cond_16

    :try_start_17
    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Z:I
    :try_end_17
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_17 .. :try_end_17} :catch_16

    and-int/lit16 v4, v4, 0x400

    if-eqz v4, :cond_16

    :try_start_18
    invoke-direct {v1, v3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->b(II)I

    move-result v3
    :try_end_18
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_18 .. :try_end_18} :catch_15

    if-nez v2, :cond_13

    :try_start_19
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;
    :try_end_19
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_19 .. :try_end_19} :catch_11

    if-eqz v4, :cond_17

    goto :goto_e

    :catch_11
    move-exception v0

    move-object v2, v0

    :try_start_1a
    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_13
    :goto_e
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ae:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4
    :try_end_1a
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_1a .. :try_end_1a} :catch_15

    :try_start_1b
    iput-object v14, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ae:Ljava/io/ByteArrayOutputStream;
    :try_end_1b
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_1b .. :try_end_1b} :catch_12

    if-nez v2, :cond_15

    :try_start_1c
    array-length v7, v4

    iget-object v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iget-object v9, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->f:[B

    array-length v9, v9
    :try_end_1c
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_1c .. :try_end_1c} :catch_13

    if-ne v7, v9, :cond_14

    :try_start_1d
    iget-object v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iget-object v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->f:[B

    array-length v9, v4

    invoke-static {v4, v5, v7, v5, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    if-eqz v2, :cond_17

    :cond_14
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->bb:[Ljava/lang/String;

    aget-object v7, v7, v13

    iput-object v7, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    iput v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I
    :try_end_1d
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_1d .. :try_end_1d} :catch_14

    :cond_15
    if-eqz v2, :cond_6

    goto :goto_f

    :catch_12
    move-exception v0

    move-object v2, v0

    :try_start_1e
    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_1e
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_1e .. :try_end_1e} :catch_13

    :catch_13
    move-exception v0

    :try_start_1f
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_1f
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_1f .. :try_end_1f} :catch_14

    :catch_14
    move-exception v0

    :try_start_20
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_20
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_20 .. :try_end_20} :catch_15

    :catch_15
    move-exception v0

    iget v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;->a:I

    return v0

    :catch_16
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_16
    if-nez v2, :cond_17

    :try_start_21
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    if-eqz v4, :cond_17

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iput-object v14, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->f:[B
    :try_end_21
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_21 .. :try_end_21} :catch_17

    goto :goto_f

    :catch_17
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_17
    :goto_f
    const/16 v4, 0x14

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    :pswitch_5
    if-nez v2, :cond_19

    :try_start_22
    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Z:I
    :try_end_22
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_22 .. :try_end_22} :catch_1a

    and-int/lit16 v4, v4, 0x800

    if-eqz v4, :cond_19

    :try_start_23
    invoke-direct {v1, v3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(II)I

    move-result v3
    :try_end_23
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_23 .. :try_end_23} :catch_19

    if-nez v2, :cond_18

    :try_start_24
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    if-eqz v4, :cond_18

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iget-object v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ae:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    iput-object v7, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->g:[B
    :try_end_24
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_24 .. :try_end_24} :catch_18

    goto :goto_10

    :catch_18
    move-exception v0

    :try_start_25
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_18
    :goto_10
    iput-object v14, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ae:Ljava/io/ByteArrayOutputStream;
    :try_end_25
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_25 .. :try_end_25} :catch_19

    goto :goto_11

    :catch_19
    move-exception v0

    iget v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;->a:I

    return v0

    :catch_1a
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_19
    if-nez v2, :cond_1a

    :try_start_26
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    if-eqz v4, :cond_1a

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iput-object v14, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->g:[B
    :try_end_26
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_26 .. :try_end_26} :catch_1b

    goto :goto_11

    :catch_1b
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_1a
    :goto_11
    const/16 v4, 0x15

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    :pswitch_6
    if-nez v2, :cond_1c

    :try_start_27
    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Z:I
    :try_end_27
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_27 .. :try_end_27} :catch_1e

    and-int/lit16 v4, v4, 0x1000

    if-eqz v4, :cond_1c

    :try_start_28
    invoke-direct {v1, v3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(II)I

    move-result v3
    :try_end_28
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_28 .. :try_end_28} :catch_1d

    if-nez v2, :cond_1b

    :try_start_29
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    if-eqz v4, :cond_1b

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iget-object v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ae:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    iput-object v7, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->h:[B
    :try_end_29
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_29 .. :try_end_29} :catch_1c

    goto :goto_12

    :catch_1c
    move-exception v0

    :try_start_2a
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_1b
    :goto_12
    iput-object v14, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ae:Ljava/io/ByteArrayOutputStream;
    :try_end_2a
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_2a .. :try_end_2a} :catch_1d

    goto :goto_13

    :catch_1d
    move-exception v0

    iget v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;->a:I

    return v0

    :catch_1e
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_1c
    if-nez v2, :cond_1d

    :try_start_2b
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    if-eqz v4, :cond_1d

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iput-object v14, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->h:[B
    :try_end_2b
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_2b .. :try_end_2b} :catch_1f

    goto :goto_13

    :catch_1f
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_1d
    :goto_13
    const/16 v4, 0x16

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    :pswitch_7
    if-nez v2, :cond_20

    :try_start_2c
    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Z:I
    :try_end_2c
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_2c .. :try_end_2c} :catch_25

    and-int/lit16 v4, v4, 0x200

    if-eqz v4, :cond_1f

    :try_start_2d
    invoke-direct {v1, v11, v3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(III)I

    move-result v3
    :try_end_2d
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_2d .. :try_end_2d} :catch_24

    if-nez v2, :cond_1e

    :try_start_2e
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;
    :try_end_2e
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_2e .. :try_end_2e} :catch_20

    if-eqz v4, :cond_1e

    :try_start_2f
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    const-wide/32 v11, 0xffff

    and-long/2addr v9, v11

    long-to-int v7, v9

    iput v7, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->i:I

    goto :goto_14

    :catch_20
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_2f
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_2f .. :try_end_2f} :catch_21

    :catch_21
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_1e
    :goto_14
    if-nez v2, :cond_20

    :try_start_30
    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v4}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->getValue()J

    move-result-wide v11
    :try_end_30
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_30 .. :try_end_30} :catch_22

    const-wide/32 v14, 0xffff

    and-long/2addr v11, v14

    cmp-long v4, v9, v11

    if-eqz v4, :cond_1f

    :try_start_31
    iput v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->bb:[Ljava/lang/String;

    const/16 v8, 0x9

    aget-object v7, v7, v8

    iput-object v7, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    const/4 v4, 0x5

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->U:I

    if-eqz v2, :cond_6

    goto :goto_15

    :catch_22
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_31
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_31 .. :try_end_31} :catch_23

    :catch_23
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :catch_24
    move-exception v0

    move-object v2, v0

    iget v0, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;->a:I

    return v0

    :cond_1f
    :goto_15
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;

    invoke-direct {v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;-><init>()V

    iput-object v7, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    goto :goto_16

    :catch_25
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_20
    :goto_16
    iput v13, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    if-eqz v2, :cond_6

    goto/16 :goto_1

    :catch_26
    move-exception v0

    move-object v2, v0

    iget v0, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;->a:I

    return v0

    :catch_27
    move-exception v0

    move-object v2, v0

    iget v0, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;->a:I

    return v0

    :pswitch_8
    :try_start_32
    iget v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I
    :try_end_32
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_32 .. :try_end_32} :catch_3a

    if-nez v2, :cond_22

    if-nez v7, :cond_21

    :try_start_33
    iput v13, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I
    :try_end_33
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_33 .. :try_end_33} :catch_3b

    if-eqz v2, :cond_6

    :cond_21
    :try_start_34
    invoke-direct {v1, v11, v3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(III)I

    move-result v3
    :try_end_34
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_34 .. :try_end_34} :catch_28

    goto :goto_17

    :catch_28
    move-exception v0

    move-object v2, v0

    iget v0, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;->a:I

    return v0

    :cond_22
    move v3, v7

    :goto_17
    :try_start_35
    iget v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I
    :try_end_35
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_35 .. :try_end_35} :catch_37

    if-nez v2, :cond_24

    if-eq v7, v6, :cond_23

    :try_start_36
    iget v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I
    :try_end_36
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_36 .. :try_end_36} :catch_39

    and-int/2addr v7, v11

    if-nez v2, :cond_29

    if-eqz v7, :cond_28

    :cond_23
    :try_start_37
    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J
    :try_end_37
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_37 .. :try_end_37} :catch_29

    const-wide/32 v23, 0x8b1f

    cmp-long v7, v9, v23

    goto :goto_18

    :catch_29
    move-exception v0

    goto/16 :goto_28

    :cond_24
    :goto_18
    if-nez v2, :cond_29

    if-nez v7, :cond_28

    if-nez v2, :cond_26

    :try_start_38
    iget v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I

    if-ne v7, v6, :cond_25

    iput v11, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I
    :try_end_38
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_38 .. :try_end_38} :catch_2a

    :cond_25
    iget-object v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    new-instance v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;

    invoke-direct {v9}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;-><init>()V

    iput-object v9, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    invoke-direct {v1, v11, v9, v10}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(IJ)V

    goto :goto_19

    :catch_2a
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_26
    :goto_19
    if-nez v2, :cond_27

    :try_start_39
    iget-object v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    if-nez v7, :cond_27

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    invoke-direct {v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;-><init>()V

    iput-object v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;
    :try_end_39
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_39 .. :try_end_39} :catch_2b

    goto :goto_1a

    :catch_2b
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_27
    :goto_1a
    const/16 v7, 0x17

    iput v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    if-eqz v2, :cond_3f

    :cond_28
    iget v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I

    and-int/2addr v7, v11

    :cond_29
    if-nez v2, :cond_2b

    if-eqz v7, :cond_2a

    :try_start_3a
    iput v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    iget-object v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->bb:[Ljava/lang/String;

    aget-object v9, v9, v6

    iput-object v9, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;
    :try_end_3a
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_3a .. :try_end_3a} :catch_2c

    if-eqz v2, :cond_3f

    goto :goto_1b

    :catch_2c
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_2a
    :goto_1b
    iput v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Z:I

    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    long-to-int v7, v9

    and-int/lit16 v7, v7, 0xff

    iput v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->R:I

    shr-long/2addr v9, v15

    long-to-int v7, v9

    and-int/lit16 v7, v7, 0xff

    :cond_2b
    :try_start_3b
    iget v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I
    :try_end_3b
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_3b .. :try_end_3b} :catch_34

    and-int/2addr v9, v12

    if-nez v2, :cond_2d

    if-eqz v9, :cond_2c

    :try_start_3c
    iget v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->R:I

    shl-int/2addr v9, v15

    add-int/2addr v9, v7

    rem-int/lit8 v9, v9, 0x1f
    :try_end_3c
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_3c .. :try_end_3c} :catch_36

    if-nez v2, :cond_31

    if-eqz v9, :cond_30

    :cond_2c
    :try_start_3d
    iget v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->R:I
    :try_end_3d
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_3d .. :try_end_3d} :catch_2d

    const/16 v10, 0xf

    and-int/2addr v9, v10

    goto :goto_1c

    :catch_2d
    move-exception v0

    goto/16 :goto_27

    :cond_2d
    :goto_1c
    if-nez v2, :cond_31

    if-eq v9, v15, :cond_30

    if-nez v2, :cond_2f

    :try_start_3e
    iget v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I

    if-ne v9, v6, :cond_2e

    iget-object v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v10, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int/2addr v10, v11

    iput v10, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v10, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    add-int/2addr v10, v11

    iput v10, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    move v14, v7

    iget-wide v6, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    const-wide/16 v23, 0x2

    sub-long v6, v6, v23

    iput-wide v6, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iput v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I

    iput v13, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I
    :try_end_3e
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_3e .. :try_end_3e} :catch_2e

    if-eqz v2, :cond_3f

    goto :goto_1d

    :cond_2e
    move v14, v7

    :goto_1d
    iput v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    goto :goto_1e

    :catch_2e
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_2f
    move v14, v7

    :goto_1e
    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->bb:[Ljava/lang/String;

    const/16 v9, 0xb

    aget-object v7, v7, v9

    iput-object v7, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    if-eqz v2, :cond_3f

    goto :goto_1f

    :cond_30
    move v14, v7

    :goto_1f
    iget v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->R:I

    const/16 v7, 0xf

    and-int/lit8 v9, v6, 0xf

    goto :goto_20

    :cond_31
    move v14, v7

    :goto_20
    if-nez v2, :cond_33

    if-eq v9, v15, :cond_32

    :try_start_3f
    iput v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->bb:[Ljava/lang/String;

    aget-object v7, v7, v12

    iput-object v7, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;
    :try_end_3f
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_3f .. :try_end_3f} :catch_2f

    if-eqz v2, :cond_3f

    goto :goto_21

    :catch_2f
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_32
    :goto_21
    iget v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I

    const/4 v6, 0x4

    goto :goto_22

    :cond_33
    move v6, v15

    :goto_22
    if-nez v2, :cond_35

    if-ne v9, v6, :cond_34

    :try_start_40
    iput v12, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I
    :try_end_40
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_40 .. :try_end_40} :catch_30

    goto :goto_23

    :catch_30
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_34
    :goto_23
    iget v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->R:I

    const/4 v7, 0x4

    shr-int/2addr v6, v7

    add-int/lit8 v9, v6, 0x8

    iget v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->W:I

    :cond_35
    if-nez v2, :cond_37

    if-le v9, v6, :cond_36

    :try_start_41
    iput v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->bb:[Ljava/lang/String;

    const/4 v8, 0x3

    aget-object v7, v7, v8

    iput-object v7, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;
    :try_end_41
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_41 .. :try_end_41} :catch_31

    if-eqz v2, :cond_3f

    goto :goto_24

    :catch_31
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_36
    :goto_24
    if-nez v2, :cond_38

    :try_start_42
    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;

    invoke-direct {v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;-><init>()V

    iput-object v7, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;
    :try_end_42
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_42 .. :try_end_42} :catch_32

    const/16 v6, 0x20

    move v7, v14

    goto :goto_25

    :catch_32
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_37
    move v7, v9

    :goto_25
    and-int/2addr v6, v7

    if-nez v6, :cond_38

    :try_start_43
    iput v13, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I
    :try_end_43
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_43 .. :try_end_43} :catch_33

    if-eqz v2, :cond_3f

    goto :goto_26

    :catch_33
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_38
    :goto_26
    iput v11, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    goto/16 :goto_37

    :catch_34
    move-exception v0

    move-object v2, v0

    :try_start_44
    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_44
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_44 .. :try_end_44} :catch_35

    :catch_35
    move-exception v0

    :try_start_45
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_45
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_45 .. :try_end_45} :catch_36

    :catch_36
    move-exception v0

    :try_start_46
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_46
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_46 .. :try_end_46} :catch_2d

    :goto_27
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :catch_37
    move-exception v0

    move-object v2, v0

    :try_start_47
    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_47
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_47 .. :try_end_47} :catch_38

    :catch_38
    move-exception v0

    :try_start_48
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_48
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_48 .. :try_end_48} :catch_39

    :catch_39
    move-exception v0

    :try_start_49
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_49
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_49 .. :try_end_49} :catch_29

    :goto_28
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :catch_3a
    move-exception v0

    move-object v2, v0

    :try_start_4a
    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_4a
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_4a .. :try_end_4a} :catch_3b

    :catch_3b
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :pswitch_9
    const/4 v6, -0x3

    return v6

    :pswitch_a
    const/4 v6, -0x3

    iget-object v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->X:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v7, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->a(I)I

    move-result v3

    if-nez v2, :cond_39

    if-ne v3, v6, :cond_39

    :try_start_4b
    iput v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    iput v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->U:I
    :try_end_4b
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_4b .. :try_end_4b} :catch_3c

    if-eqz v2, :cond_3f

    goto :goto_29

    :catch_3c
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_39
    :goto_29
    if-nez v2, :cond_3a

    if-nez v3, :cond_3a

    move v3, v0

    :cond_3a
    if-nez v2, :cond_3d

    if-eq v3, v12, :cond_3b

    return v3

    :cond_3b
    :try_start_4c
    iget-object v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->getValue()J

    move-result-wide v6

    iput-wide v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->S:J

    iget-object v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->X:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    invoke-virtual {v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->a()V

    if-nez v2, :cond_3c

    iget v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I
    :try_end_4c
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_4c .. :try_end_4c} :catch_3d

    move v6, v0

    goto :goto_2a

    :cond_3c
    move v3, v0

    goto :goto_2c

    :catch_3d
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_3d
    move v6, v3

    :goto_2a
    if-nez v3, :cond_40

    const/16 v3, 0xc

    :try_start_4d
    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I
    :try_end_4d
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_4d .. :try_end_4d} :catch_3e

    if-eqz v2, :cond_3e

    goto :goto_2b

    :cond_3e
    move v3, v6

    :cond_3f
    const/4 v6, 0x4

    goto/16 :goto_0

    :catch_3e
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_40
    :goto_2b
    move v3, v6

    :goto_2c
    iput v15, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    :pswitch_b
    :try_start_4e
    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I
    :try_end_4e
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_4e .. :try_end_4e} :catch_56

    if-nez v2, :cond_42

    if-nez v6, :cond_41

    return v3

    :cond_41
    move v3, v0

    goto :goto_2d

    :cond_42
    move v3, v6

    :goto_2d
    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v7, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    sub-int/2addr v7, v12

    iput v7, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v10, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    add-long v10, v10, v19

    iput-wide v10, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v10, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    add-int/lit8 v11, v10, 0x1

    iput v11, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    aget-byte v6, v6, v10

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x18

    int-to-long v9, v6

    const-wide v25, 0xff000000L

    and-long v9, v9, v25

    iput-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    const/16 v6, 0x9

    iput v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    :pswitch_c
    :try_start_4f
    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I
    :try_end_4f
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_4f .. :try_end_4f} :catch_54

    if-nez v2, :cond_44

    if-nez v6, :cond_43

    return v3

    :cond_43
    move v3, v0

    goto :goto_2e

    :cond_44
    move v3, v6

    :goto_2e
    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v9, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    sub-int/2addr v9, v12

    iput v9, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v9, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    add-long v9, v9, v19

    iput-wide v9, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v11, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v13, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    add-int/lit8 v7, v13, 0x1

    iput v7, v11, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    aget-byte v6, v6, v13

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v4, v6, 0x10

    int-to-long v6, v4

    const-wide/32 v21, 0xff0000

    and-long v6, v6, v21

    add-long/2addr v9, v6

    iput-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    const/16 v4, 0xa

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    :pswitch_d
    :try_start_50
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I
    :try_end_50
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_50 .. :try_end_50} :catch_52

    if-nez v2, :cond_46

    if-nez v4, :cond_45

    return v3

    :cond_45
    move v3, v0

    goto :goto_2f

    :cond_46
    move v3, v4

    :goto_2f
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v6, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    sub-int/2addr v6, v12

    iput v6, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v6, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    add-long v6, v6, v19

    iput-wide v6, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-wide v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v10, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    add-int/lit8 v11, v10, 0x1

    iput v11, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    aget-byte v4, v4, v10

    and-int/lit16 v4, v4, 0xff

    shl-int/2addr v4, v15

    int-to-long v9, v4

    const-wide/32 v17, 0xff00

    and-long v9, v9, v17

    add-long/2addr v6, v9

    iput-wide v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    const/16 v4, 0xb

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    :pswitch_e
    :try_start_51
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I
    :try_end_51
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_51 .. :try_end_51} :catch_50

    if-nez v2, :cond_48

    if-nez v4, :cond_47

    return v3

    :cond_47
    iget-object v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    sub-int/2addr v4, v12

    iput v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v6, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    add-long v6, v6, v19

    iput-wide v6, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v9, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    add-int/lit8 v10, v9, 0x1

    iput v10, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    aget-byte v6, v6, v9

    int-to-long v6, v6

    const-wide/16 v9, 0xff

    and-long/2addr v6, v9

    add-long/2addr v3, v6

    iput-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Z:I

    move v3, v0

    :cond_48
    if-nez v2, :cond_4a

    if-eqz v4, :cond_49

    :try_start_52
    iget-wide v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    const-wide/32 v9, -0x1000000

    and-long/2addr v6, v9

    shr-long v6, v6, v16

    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    const-wide/32 v19, 0xff0000

    and-long v9, v9, v19

    shr-long/2addr v9, v15

    or-long/2addr v6, v9

    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    const-wide/32 v17, 0xff00

    and-long v9, v9, v17

    shl-long/2addr v9, v15

    or-long/2addr v6, v9

    iget-wide v9, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    const-wide/32 v17, 0xffff

    and-long v9, v9, v17

    shl-long v9, v9, v16

    or-long/2addr v6, v9

    const-wide v9, 0xffffffffL

    and-long/2addr v6, v9

    iput-wide v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J
    :try_end_52
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_52 .. :try_end_52} :catch_3f

    goto :goto_30

    :catch_3f
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_49
    :goto_30
    iget-wide v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->S:J

    long-to-int v4, v6

    :cond_4a
    if-nez v2, :cond_4c

    :try_start_53
    iget-wide v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J
    :try_end_53
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_53 .. :try_end_53} :catch_40

    long-to-int v6, v6

    if-eq v4, v6, :cond_4b

    :try_start_54
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->bb:[Ljava/lang/String;

    const/16 v7, 0xa

    aget-object v6, v6, v7

    iput-object v6, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;
    :try_end_54
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_54 .. :try_end_54} :catch_41

    if-eqz v2, :cond_4d

    :cond_4b
    if-nez v2, :cond_4d

    :try_start_55
    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Z:I
    :try_end_55
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_55 .. :try_end_55} :catch_42

    goto :goto_31

    :catch_40
    move-exception v0

    move-object v2, v0

    :try_start_56
    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_56
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_56 .. :try_end_56} :catch_41

    :catch_41
    move-exception v0

    :try_start_57
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_57
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_57 .. :try_end_57} :catch_42

    :catch_42
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_4c
    :goto_31
    if-eqz v4, :cond_4d

    if-nez v2, :cond_4d

    :try_start_58
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    if-eqz v4, :cond_4d

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    iget-wide v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    iput-wide v6, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;->j:J
    :try_end_58
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_58 .. :try_end_58} :catch_43

    goto :goto_32

    :catch_43
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_4d
    :goto_32
    const/16 v4, 0xf

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    :pswitch_f
    if-nez v2, :cond_51

    :try_start_59
    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->V:I
    :try_end_59
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_59 .. :try_end_59} :catch_4b

    if-eqz v4, :cond_51

    if-nez v2, :cond_51

    :try_start_5a
    iget v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Z:I
    :try_end_5a
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_5a .. :try_end_5a} :catch_4c

    if-eqz v4, :cond_51

    const/4 v4, 0x4

    :try_start_5b
    invoke-direct {v1, v4, v3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(III)I

    move-result v3
    :try_end_5b
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_5b .. :try_end_5b} :catch_4a

    if-nez v2, :cond_4e

    :try_start_5c
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;
    :try_end_5c
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_5c .. :try_end_5c} :catch_44

    if-eqz v4, :cond_4e

    :try_start_5d
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    sget-object v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->bb:[Ljava/lang/String;

    const/4 v7, 0x6

    aget-object v6, v6, v7

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_5d
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_5d .. :try_end_5d} :catch_45

    if-nez v2, :cond_4f

    if-eqz v4, :cond_4e

    :try_start_5e
    iput v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    const/4 v4, 0x5

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->U:I
    :try_end_5e
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_5e .. :try_end_5e} :catch_46

    if-eqz v2, :cond_3f

    goto :goto_33

    :catch_44
    move-exception v0

    :try_start_5f
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_5f
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_5f .. :try_end_5f} :catch_45

    :catch_45
    move-exception v0

    :try_start_60
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_60
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_60 .. :try_end_60} :catch_46

    :catch_46
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_4e
    :goto_33
    if-nez v2, :cond_50

    :try_start_61
    iget-wide v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v10, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalOut:J
    :try_end_61
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_61 .. :try_end_61} :catch_48

    const-wide v16, 0xffffffffL

    and-long v9, v10, v16

    cmp-long v4, v6, v9

    :cond_4f
    if-eqz v4, :cond_50

    :try_start_62
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->bb:[Ljava/lang/String;

    const/4 v7, 0x5

    aget-object v6, v6, v7

    iput-object v6, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    iput v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I
    :try_end_62
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_62 .. :try_end_62} :catch_47

    if-eqz v2, :cond_3f

    goto :goto_34

    :catch_47
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :catch_48
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_50
    :goto_34
    :try_start_63
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput-object v14, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;
    :try_end_63
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_63 .. :try_end_63} :catch_49

    if-eqz v2, :cond_52

    goto :goto_35

    :catch_49
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :catch_4a
    move-exception v0

    move-object v2, v0

    iget v0, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;->a:I

    return v0

    :catch_4b
    move-exception v0

    move-object v2, v0

    :try_start_64
    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_64
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_64 .. :try_end_64} :catch_4c

    :catch_4c
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_51
    :goto_35
    if-nez v2, :cond_52

    :try_start_65
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;
    :try_end_65
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_65 .. :try_end_65} :catch_4d

    if-eqz v4, :cond_52

    if-nez v2, :cond_52

    :try_start_66
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    sget-object v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->bb:[Ljava/lang/String;

    const/4 v7, 0x6

    aget-object v6, v6, v7

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_52

    iput v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    const/4 v4, 0x5

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->U:I
    :try_end_66
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_66 .. :try_end_66} :catch_4f

    if-eqz v2, :cond_3f

    goto :goto_36

    :catch_4d
    move-exception v0

    :try_start_67
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_67
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_67 .. :try_end_67} :catch_4e

    :catch_4e
    move-exception v0

    :try_start_68
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_68
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_68 .. :try_end_68} :catch_4f

    :catch_4f
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :cond_52
    :goto_36
    const/16 v0, 0xc

    iput v0, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    :pswitch_10
    return v12

    :catch_50
    move-exception v0

    :try_start_69
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_69
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_69 .. :try_end_69} :catch_51

    :catch_51
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :catch_52
    move-exception v0

    :try_start_6a
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_6a
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_6a .. :try_end_6a} :catch_53

    :catch_53
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :catch_54
    move-exception v0

    :try_start_6b
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_6b
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_6b .. :try_end_6b} :catch_55

    :catch_55
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :catch_56
    move-exception v0

    :try_start_6c
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_6c
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_6c .. :try_end_6c} :catch_57

    :catch_57
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :pswitch_11
    iput v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    iget-object v0, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->bb:[Ljava/lang/String;

    aget-object v2, v2, v5

    iput-object v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    iput v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->U:I

    const/4 v0, -0x2

    return v0

    :goto_37
    :pswitch_12
    :try_start_6d
    iget-object v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I
    :try_end_6d
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_6d .. :try_end_6d} :catch_5e

    if-nez v2, :cond_54

    if-nez v5, :cond_53

    return v3

    :cond_53
    move v3, v0

    goto :goto_38

    :cond_54
    move v3, v5

    :goto_38
    iget-object v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v6, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    sub-int/2addr v6, v12

    iput v6, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v6, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    add-long v6, v6, v19

    iput-wide v6, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v7, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    add-int/lit8 v8, v7, 0x1

    iput v8, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    aget-byte v5, v5, v7

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x18

    int-to-long v5, v5

    const-wide v7, 0xff000000L

    and-long/2addr v5, v7

    iput-wide v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    const/4 v5, 0x3

    iput v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    :pswitch_13
    :try_start_6e
    iget-object v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I
    :try_end_6e
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_6e .. :try_end_6e} :catch_5c

    if-nez v2, :cond_56

    if-nez v5, :cond_55

    return v3

    :cond_55
    move v3, v0

    goto :goto_39

    :cond_56
    move v3, v5

    :goto_39
    iget-object v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v6, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    sub-int/2addr v6, v12

    iput v6, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v6, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    add-long v6, v6, v19

    iput-wide v6, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-wide v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    iget-object v7, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v7, v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v8, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v9, v8, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    add-int/lit8 v10, v9, 0x1

    iput v10, v8, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    aget-byte v7, v7, v9

    and-int/lit16 v7, v7, 0xff

    shl-int/lit8 v4, v7, 0x10

    int-to-long v7, v4

    const-wide/32 v9, 0xff0000

    and-long/2addr v7, v9

    add-long/2addr v5, v7

    iput-wide v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    const/4 v4, 0x4

    iput v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    :pswitch_14
    :try_start_6f
    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I
    :try_end_6f
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_6f .. :try_end_6f} :catch_5a

    if-nez v2, :cond_57

    if-nez v4, :cond_58

    return v3

    :cond_57
    move v0, v4

    :cond_58
    iget-object v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    sub-int/2addr v4, v12

    iput v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    add-long v4, v4, v19

    iput-wide v4, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    iget-object v5, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v5, v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v6, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v7, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    add-int/lit8 v8, v7, 0x1

    iput v8, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    aget-byte v5, v5, v7

    and-int/lit16 v5, v5, 0xff

    shl-int/2addr v5, v15

    int-to-long v5, v5

    const-wide/32 v7, 0xff00

    and-long/2addr v5, v7

    add-long/2addr v3, v5

    iput-wide v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    const/4 v3, 0x5

    iput v3, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    move v3, v0

    :pswitch_15
    :try_start_70
    iget-object v0, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I
    :try_end_70
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_70 .. :try_end_70} :catch_58

    if-nez v2, :cond_5a

    if-nez v0, :cond_59

    return v3

    :cond_59
    iget-object v0, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    sub-int/2addr v2, v12

    iput v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object v0, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    add-long v2, v2, v19

    iput-wide v2, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-wide v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    iget-object v0, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget-object v4, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v5, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    add-int/lit8 v6, v5, 0x1

    iput v6, v4, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    aget-byte v0, v0, v5

    int-to-long v4, v0

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    add-long/2addr v2, v4

    iput-wide v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    iget-object v0, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    iget-wide v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->T:J

    invoke-interface {v0, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->reset(J)V

    const/4 v0, 0x6

    iput v0, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    goto :goto_3a

    :cond_5a
    move v11, v0

    :goto_3a
    return v11

    :catch_58
    move-exception v0

    :try_start_71
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_71
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_71 .. :try_end_71} :catch_59

    :catch_59
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :catch_5a
    move-exception v0

    :try_start_72
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_72
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_72 .. :try_end_72} :catch_5b

    :catch_5b
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :catch_5c
    move-exception v0

    :try_start_73
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_73
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_73 .. :try_end_73} :catch_5d

    :catch_5d
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :catch_5e
    move-exception v0

    :try_start_74
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_74
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_74 .. :try_end_74} :catch_5f

    :catch_5f
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :goto_3b
    return v0

    :catch_60
    move-exception v0

    move-object v2, v0

    :try_start_75
    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_75
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_75 .. :try_end_75} :catch_61

    :catch_61
    move-exception v0

    :try_start_76
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_76
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_76 .. :try_end_76} :catch_62

    :catch_62
    move-exception v0

    :try_start_77
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_77
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_77 .. :try_end_77} :catch_0

    :goto_3c
    :try_start_78
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_78
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_78 .. :try_end_78} :catch_63

    :catch_63
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :catch_64
    move-exception v0

    move-object v2, v0

    :try_start_79
    invoke-static {v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0
    :try_end_79
    .catch Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return; {:try_start_79 .. :try_end_79} :catch_65

    :catch_65
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate$Return;

    move-result-object v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_11
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_10
        :pswitch_9
        :pswitch_8
        :pswitch_f
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
    .end packed-switch
.end method

.method c()I
    .locals 11

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    if-nez v1, :cond_0

    const/4 v0, -0x2

    return v0

    :cond_0
    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    const/16 v2, 0xd

    const/4 v3, 0x0

    if-nez v0, :cond_2

    if-eq v1, v2, :cond_1

    iput v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    iput v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->U:I

    :cond_1
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    move v2, v1

    :cond_2
    if-nez v0, :cond_4

    if-nez v1, :cond_3

    const/4 v0, -0x5

    return v0

    :cond_3
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    :cond_4
    iget v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->U:I

    :cond_5
    const/4 v5, 0x4

    if-eqz v2, :cond_b

    if-nez v0, :cond_c

    if-nez v0, :cond_c

    if-ge v4, v5, :cond_b

    iget-object v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    aget-byte v6, v6, v1

    if-nez v0, :cond_7

    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ad:[B

    aget-byte v7, v7, v4

    if-ne v6, v7, :cond_6

    add-int/lit8 v4, v4, 0x1

    if-eqz v0, :cond_a

    :cond_6
    iget-object v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-object v6, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    aget-byte v6, v6, v1

    :cond_7
    if-nez v0, :cond_9

    if-eqz v6, :cond_8

    move v4, v3

    if-eqz v0, :cond_a

    :cond_8
    rsub-int/lit8 v6, v4, 0x4

    :cond_9
    move v4, v6

    :cond_a
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v2, v2, -0x1

    if-eqz v0, :cond_5

    :cond_b
    iget-object v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v7, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v9, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget v9, v9, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    sub-int v9, v1, v9

    int-to-long v9, v9

    add-long/2addr v7, v9

    iput-wide v7, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v1, v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    if-nez v0, :cond_d

    iput v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->U:I

    :cond_c
    if-eq v4, v5, :cond_d

    const/4 v0, -0x3

    return v0

    :cond_d
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iget-wide v4, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalOut:J

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a()I

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput-wide v0, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    iput-wide v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalOut:J

    const/4 v0, 0x7

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    return v3
.end method

.method d()I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Y:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->X:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;

    if-nez v0, :cond_2

    if-nez v1, :cond_2

    :cond_1
    const/4 v0, -0x2

    return v0

    :cond_2
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfBlocks;->c()I

    move-result v0

    return v0
.end method

.method e()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    if-nez v0, :cond_1

    const/4 v0, 0x2

    if-eq v1, v0, :cond_0

    const/4 v0, 0x3

    if-eq v1, v0, :cond_0

    const/4 v0, 0x4

    if-eq v1, v0, :cond_0

    const/4 v0, 0x5

    if-eq v1, v0, :cond_0

    const/16 v0, 0xe

    if-eq v1, v0, :cond_0

    packed-switch v1, :pswitch_data_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    :cond_1
    :goto_0
    return v1

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getGZIPHeader()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->ac:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/GZIPHeader;

    return-object v0
.end method
