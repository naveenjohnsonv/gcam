.class public interface abstract Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;
.super Ljava/lang/Object;


# virtual methods
.method public abstract clientToServerMacFor(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory$OperationException;
        }
    .end annotation
.end method

.method public abstract macs()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract serverToClientMacFor(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/KeyFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Mac;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory$OperationException;
        }
    .end annotation
.end method
