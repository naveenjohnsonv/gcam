.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;
.super Ljava/io/FilterOutputStream;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private a:Z

.field private b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;

.field protected buf:[B

.field protected bufsize:I

.field private c:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

.field protected compress:Z

.field private d:[B

.field protected flush:I

.field protected out:Ljava/io/OutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, ",]*,{L/+Tv`"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->e:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x23

    goto :goto_1

    :cond_1
    const/16 v4, 0x5d

    goto :goto_1

    :cond_2
    const/16 v4, 0x7f

    goto :goto_1

    :cond_3
    const/16 v4, 0x25

    goto :goto_1

    :cond_4
    const/16 v4, 0x29

    goto :goto_1

    :cond_5
    const/16 v4, 0x56

    goto :goto_1

    :cond_6
    const/16 v4, 0x20

    :goto_1
    const/16 v5, 0x65

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v0, 0x200

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->bufsize:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->flush:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->buf:[B

    iput-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->a:Z

    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->d:[B

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->out:Ljava/io/OutputStream;

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-direct {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->c:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->init()I

    iput-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->compress:Z

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;-><init>(Ljava/io/OutputStream;IZ)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;IZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v0, 0x200

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->bufsize:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->flush:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->buf:[B

    iput-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->a:Z

    const/4 v0, 0x1

    new-array v1, v0, [B

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->d:[B

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->out:Ljava/io/OutputStream;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    invoke-direct {v1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;-><init>(IZ)V

    new-instance p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;

    invoke-direct {p2, p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;)V

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->compress:Z

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->finish()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->end()V

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->out:Ljava/io/OutputStream;

    throw v1

    :catch_0
    :goto_0
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->end()V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->out:Ljava/io/OutputStream;

    return-void
.end method

.method public declared-synchronized end()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->a:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_2
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->compress:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    if-nez v0, :cond_3

    if-eqz v1, :cond_2

    :try_start_3
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->finish()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    if-eqz v0, :cond_3

    :cond_2
    :try_start_4
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->c:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->end()I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_3
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->a:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit p0

    return-void

    :catch_2
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public finish()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->compress:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :try_start_1
    new-array v2, v1, [B

    invoke-virtual {p0, v2, v1, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    goto :goto_0

    :catchall_0
    move-exception v0

    throw v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->finish()V

    :cond_1
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->flush()V

    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

.method public getFlushMode()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->flush:I

    return v0
.end method

.method public getTotalIn()J
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->compress:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->getTotalIn()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->c:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-wide v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->totalIn:J

    :goto_0
    return-wide v0
.end method

.method public getTotalOut()J
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->compress:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->getTotalOut()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->c:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-wide v0, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->totalOut:J

    :goto_0
    return-wide v0
.end method

.method public setFlushMode(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->flush:I

    return-void
.end method

.method public write(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->d:[B

    int-to-byte p1, p1

    const/4 v1, 0x0

    aput-byte p1, v0, v1

    const/4 p1, 0x1

    invoke-virtual {p0, v0, v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->write([BII)V

    return-void
.end method

.method public write([BII)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_1

    if-nez p3, :cond_0

    return-void

    :cond_0
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->compress:Z

    goto :goto_0

    :cond_1
    move v1, p3

    :goto_0
    const/4 v2, 0x0

    if-nez v0, :cond_4

    if-eqz v1, :cond_3

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->b:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;

    invoke-virtual {v1, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    return-void

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->c:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    const/4 v3, 0x1

    invoke-virtual {v1, p1, p2, p3, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->setInput([BIIZ)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move v1, v2

    goto :goto_2

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_4
    :goto_2
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->c:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->availIn:I

    if-lez p1, :cond_7

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->c:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->buf:[B

    array-length p3, p2

    invoke-virtual {p1, p2, v2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->setOutput([BII)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->c:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->flush:I

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->inflate(I)I

    move-result v1

    :try_start_2
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->c:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->nextOutIndex:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    if-nez v0, :cond_6

    if-lez p1, :cond_5

    :try_start_3
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->out:Ljava/io/OutputStream;

    iget-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->buf:[B

    iget-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->c:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget p3, p3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->nextOutIndex:I

    invoke-virtual {p1, p2, v2, p3}, Ljava/io/OutputStream;->write([BII)V

    :cond_5
    move p1, v1

    :cond_6
    if-eqz p1, :cond_4

    goto :goto_3

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_7
    :goto_3
    if-nez v1, :cond_8

    return-void

    :cond_8
    :try_start_4
    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStreamException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->e:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->c:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;

    iget-object p3, p3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflater;->msg:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStreamException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZOutputStream;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method
