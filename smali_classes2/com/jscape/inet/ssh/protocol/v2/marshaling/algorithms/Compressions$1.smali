.class final Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compressions$1;
.super Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public clientToServerCompressionFor(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$Mode;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$Mode;->COMPRESSION:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$Mode;

    if-ne p1, v0, :cond_0

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibCompressor;

    invoke-direct {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibCompressor;-><init>()V

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;

    invoke-direct {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;-><init>()V

    :goto_0
    return-object p1
.end method

.method public serverToClientCompressionFor(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$Mode;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$Mode;->COMPRESSION:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$Mode;

    if-ne p1, v0, :cond_0

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibCompressor;

    invoke-direct {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibCompressor;-><init>()V

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;

    invoke-direct {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ZlibDecompressor;-><init>()V

    :goto_0
    return-object p1
.end method
