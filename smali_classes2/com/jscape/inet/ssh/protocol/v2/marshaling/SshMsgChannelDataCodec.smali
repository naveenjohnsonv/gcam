.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelDataCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/ssh/protocol/messages/Message;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v0

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readValue(Ljava/io/InputStream;)[B

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;

    invoke-direct {v1, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;-><init>(I[B)V

    return-object v1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelDataCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;

    iget v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;->recipientChannel:I

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;->data:[B

    iget v1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;->offset:I

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelData;->length:I

    invoke-static {v0, v1, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BIILjava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelDataCodec;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
