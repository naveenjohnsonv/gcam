.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;
.super Ljava/io/FilterOutputStream;


# static fields
.field protected static final DEFAULT_BUFSIZE:I = 0x200

.field private static final e:[Ljava/lang/String;


# instance fields
.field private a:Z

.field private b:Z

.field protected buffer:[B

.field private final c:[B

.field private d:Z

.field protected final deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

.field protected mydeflater:Z


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x11

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x5e

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "=b6\u001aS\u0004</l\u007f\u0012S\u0006p:w:\u0008=j1\u001fE\u0008y?\"9v9\u0010S\u0012<(j%\u0013\u0016\ri(w\u007f\u0014S@{)f>\u0002S\u0012</k>\u0018\u0016P"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x3d

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->e:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/4 v13, 0x5

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v14, 0x2

    if-eq v12, v14, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v14, 0x4

    if-eq v12, v14, :cond_3

    if-eq v12, v13, :cond_2

    const/16 v13, 0x42

    goto :goto_2

    :cond_2
    const/16 v13, 0x3e

    goto :goto_2

    :cond_3
    const/16 v13, 0x68

    goto :goto_2

    :cond_4
    const/16 v13, 0x28

    goto :goto_2

    :cond_5
    move v13, v7

    goto :goto_2

    :cond_6
    const/16 v13, 0x5d

    :cond_7
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;-><init>(I)V

    const/16 v1, 0x200

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;IZ)V

    iput-boolean v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->mydeflater:Z

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x200

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;IZ)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;IZ)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;IZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->b:Z

    const/4 v1, 0x1

    new-array v2, v1, [B

    iput-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->c:[B

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->mydeflater:Z

    iput-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->d:Z

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    if-lez p3, :cond_0

    iput-object p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    new-array p1, p3, [B

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->buffer:[B

    iput-boolean p4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->d:Z

    return-void

    :cond_0
    :try_start_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    sget-object p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->e:[Ljava/lang/String;

    const/4 p3, 0x2

    aget-object p2, p2, p3

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_1
    :try_start_1
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    throw p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    if-nez v0, :cond_0

    if-nez v1, :cond_4

    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->finish()V

    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->mydeflater:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    :cond_0
    if-nez v0, :cond_2

    if-eqz v1, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->end()I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    if-nez v0, :cond_3

    :try_start_3
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->d:Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    :try_start_4
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_3
    :goto_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a:Z

    :cond_4
    return-void

    :catch_3
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method protected deflate(I)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->buffer:[B

    array-length v2, v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->setOutput([BII)V

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    invoke-virtual {v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->deflate(I)I

    move-result v1

    const/4 v2, -0x5

    if-eq v1, v2, :cond_1

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    :cond_0
    if-eqz v0, :cond_3

    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->availIn:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_2

    if-gtz v2, :cond_6

    goto :goto_0

    :cond_2
    move p1, v2

    :goto_0
    const/4 v2, 0x4

    if-eq p1, v2, :cond_6

    :cond_3
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->nextOutIndex:I

    if-nez v0, :cond_4

    if-lez p1, :cond_5

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->out:Ljava/io/OutputStream;

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->buffer:[B

    invoke-virtual {v0, v2, v3, p1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_4
    move v1, p1

    :cond_5
    :goto_1
    return v1

    :cond_6
    new-instance p1, Ljava/io/IOException;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->e:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :catch_1
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method public finish()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->finished()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflate(I)I

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method public flush()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_4

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->b:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_3

    if-nez v0, :cond_4

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->finished()Z

    move-result v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    if-nez v1, :cond_3

    :cond_0
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflate(I)I

    move-result v1

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->nextOutIndex:I

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->buffer:[B

    array-length v3, v3

    if-ge v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_0

    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->out:Ljava/io/OutputStream;

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_4
    move-object v0, p0

    :goto_1
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

.method public getDeflater()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    return-object v0
.end method

.method public getSyncFlush()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->b:Z

    return v0
.end method

.method public getTotalIn()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->getTotalIn()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalOut()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->getTotalOut()J

    move-result-wide v0

    return-wide v0
.end method

.method public setSyncFlush(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->b:Z

    return-void
.end method

.method public write(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->c:[B

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    const/4 v1, 0x0

    aput-byte p1, v0, v1

    const/4 p1, 0x1

    invoke-virtual {p0, v0, v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->write([BII)V

    return-void
.end method

.method public write([BII)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->finished()Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    const/4 v2, 0x1

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    move v1, p2

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance p1, Ljava/io/IOException;

    sget-object p2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->e:[Ljava/lang/String;

    aget-object p2, p2, v2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    :cond_1
    :goto_0
    const/4 v3, 0x0

    if-nez v0, :cond_3

    if-gez v1, :cond_2

    move v1, v2

    goto :goto_1

    :cond_2
    move v1, v3

    :cond_3
    :goto_1
    if-nez v0, :cond_5

    if-gez p3, :cond_4

    move v4, v2

    goto :goto_2

    :cond_4
    move v4, v3

    goto :goto_2

    :cond_5
    move v4, p3

    :goto_2
    or-int/2addr v1, v4

    add-int v4, p2, p3

    if-nez v0, :cond_7

    :try_start_2
    array-length v5, p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    if-le v4, v5, :cond_6

    move v4, v2

    goto :goto_3

    :cond_6
    move v4, v3

    goto :goto_3

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_7
    :goto_3
    or-int/2addr v1, v4

    if-nez v0, :cond_9

    if-nez v1, :cond_8

    move v1, p3

    goto :goto_4

    :cond_8
    :try_start_4
    new-instance p1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {p1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_9
    :goto_4
    if-nez v0, :cond_b

    if-nez v1, :cond_a

    return-void

    :cond_a
    iget-boolean v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->b:Z

    :cond_b
    if-nez v0, :cond_c

    if-eqz v1, :cond_d

    const/4 v1, 0x2

    :cond_c
    move v3, v1

    :cond_d
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    invoke-virtual {v1, p1, p2, p3, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->setInput([BIIZ)V

    :cond_e
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflater:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;

    iget p1, p1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflater;->availIn:I

    if-lez p1, :cond_10

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->deflate(I)I

    move-result p1

    if-ne p1, v2, :cond_f

    goto :goto_5

    :cond_f
    if-eqz v0, :cond_e

    :cond_10
    :goto_5
    return-void

    :catch_3
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/DeflaterOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method
