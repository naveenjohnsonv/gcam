.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthPkOkCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/ssh/protocol/messages/Message;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "Ljava/security/PublicKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/jscape/util/h/I;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/h/I<",
            "Ljava/security/PublicKey;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthPkOkCodec;->a:Lcom/jscape/util/h/I;

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readValue(Ljava/io/InputStream;)[B

    move-result-object p1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthPkOkCodec;->a:Lcom/jscape/util/h/I;

    new-instance v2, Lcom/jscape/util/h/e;

    invoke-direct {v2, p1}, Lcom/jscape/util/h/e;-><init>([B)V

    invoke-interface {v1, v2}, Lcom/jscape/util/h/I;->read(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/security/PublicKey;

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthPkOk;

    invoke-direct {v1, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthPkOk;-><init>(Ljava/lang/String;Ljava/security/PublicKey;)V

    return-object v1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthPkOkCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/ssh/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthPkOk;

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthPkOk;->publicKeyAlgorithm:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUsAsciiValue(Ljava/lang/String;Ljava/io/OutputStream;)V

    new-instance v0, Lcom/jscape/util/h/o;

    invoke-direct {v0}, Lcom/jscape/util/h/o;-><init>()V

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthPkOkCodec;->a:Lcom/jscape/util/h/I;

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthPkOk;->publicKey:Ljava/security/PublicKey;

    invoke-interface {v1, p1, v0}, Lcom/jscape/util/h/I;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object p1

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p1, v1, v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BIILjava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthPkOkCodec;->write(Lcom/jscape/inet/ssh/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
