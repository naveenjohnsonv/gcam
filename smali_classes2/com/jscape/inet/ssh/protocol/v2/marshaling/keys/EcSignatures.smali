.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "s}}\u000f\u0008ma\u0015JY\'\u0006\u0005FKSM7"

    const/16 v5, 0x12

    const/16 v6, 0xb

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x27

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0xd

    const/4 v4, 0x6

    const-string v6, "\u0019ZWHU/\u0006\u0019ZWIX-"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v16, v6

    move v6, v4

    move-object/from16 v4, v16

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x3b

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    const/4 v3, 0x3

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v10, 0x2

    if-eq v2, v10, :cond_7

    if-eq v2, v3, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v10, 0x5

    if-eq v2, v10, :cond_4

    goto :goto_4

    :cond_4
    const/16 v3, 0x22

    goto :goto_4

    :cond_5
    const/16 v3, 0x5b

    goto :goto_4

    :cond_6
    const/16 v3, 0x41

    goto :goto_4

    :cond_7
    const/16 v3, 0x2d

    goto :goto_4

    :cond_8
    const/16 v3, 0x29

    goto :goto_4

    :cond_9
    const/16 v3, 0x71

    :goto_4
    xor-int v2, v9, v3

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v10, 0x1

    goto :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private static a(I)Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->b(I)Ljava/lang/String;

    move-result-object p0

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->a:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Object;)Ljava/security/Signature;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;->b()[Ljava/lang/String;

    move-result-object v0

    :try_start_0
    instance-of v1, p1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    check-cast p1, Ljava/lang/String;

    invoke-static {p0, p1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Signature;

    move-result-object p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :cond_0
    instance-of v1, p1, Ljava/security/Provider;

    :cond_1
    if-eqz v1, :cond_2

    :try_start_2
    check-cast p1, Ljava/security/Provider;

    invoke-static {p0, p1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/Signature;

    move-result-object p0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_2
    invoke-static {p0}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object p0

    :goto_0
    return-object p0

    :catch_1
    move-exception p0

    :try_start_3
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
.end method

.method private static b(I)Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;->b()[Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x100

    if-nez v0, :cond_1

    if-gt p0, v1, :cond_0

    sget-object p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->a:[Ljava/lang/String;

    const/4 v0, 0x2

    aget-object p0, p0, v0

    return-object p0

    :cond_0
    const/16 v1, 0x180

    :cond_1
    if-gt p0, v1, :cond_2

    sget-object p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->a:[Ljava/lang/String;

    const/4 v0, 0x3

    aget-object p0, p0, v0

    return-object p0

    :cond_2
    sget-object p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->a:[Ljava/lang/String;

    const/4 v0, 0x1

    aget-object p0, p0, v0

    return-object p0
.end method

.method public static hashAlgorithmFor(Ljava/security/PublicKey;)Ljava/lang/String;
    .locals 0

    check-cast p0, Ljava/security/interfaces/ECPublicKey;

    invoke-interface {p0}, Ljava/security/interfaces/ECPublicKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object p0

    invoke-virtual {p0}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object p0

    invoke-virtual {p0}, Ljava/security/spec/EllipticCurve;->getField()Ljava/security/spec/ECField;

    move-result-object p0

    invoke-interface {p0}, Ljava/security/spec/ECField;->getFieldSize()I

    move-result p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->b(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static signatureAlgorithmFor(Ljava/security/PrivateKey;)Ljava/lang/String;
    .locals 0

    check-cast p0, Ljava/security/interfaces/ECPrivateKey;

    invoke-interface {p0}, Ljava/security/interfaces/ECPrivateKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object p0

    invoke-virtual {p0}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object p0

    invoke-virtual {p0}, Ljava/security/spec/EllipticCurve;->getField()Ljava/security/spec/ECField;

    move-result-object p0

    invoke-interface {p0}, Ljava/security/spec/ECField;->getFieldSize()I

    move-result p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->a(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static signatureAlgorithmFor(Ljava/security/PublicKey;)Ljava/lang/String;
    .locals 0

    check-cast p0, Ljava/security/interfaces/ECPublicKey;

    invoke-interface {p0}, Ljava/security/interfaces/ECPublicKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object p0

    invoke-virtual {p0}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object p0

    invoke-virtual {p0}, Ljava/security/spec/EllipticCurve;->getField()Ljava/security/spec/ECField;

    move-result-object p0

    invoke-interface {p0}, Ljava/security/spec/ECField;->getFieldSize()I

    move-result p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->a(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static signatureFor(Ljava/security/PrivateKey;Ljava/lang/Object;)Ljava/security/Signature;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->signatureAlgorithmFor(Ljava/security/PrivateKey;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/security/Signature;

    move-result-object p1

    invoke-virtual {p1, p0}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;)V

    return-object p1
.end method

.method public static signatureFor(Ljava/security/PublicKey;Ljava/lang/Object;)Ljava/security/Signature;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->signatureAlgorithmFor(Ljava/security/PublicKey;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatures;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/security/Signature;

    move-result-object p1

    invoke-virtual {p1, p0}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    return-object p1
.end method
