.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;


# static fields
.field private static b:[I = null

.field private static final c:I = 0x20


# instance fields
.field private a:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x100

    new-array v1, v0, [I

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->b:[I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    const/16 v2, 0x8

    move v3, v1

    :goto_1
    add-int/lit8 v2, v2, -0x1

    if-ltz v2, :cond_1

    and-int/lit8 v4, v3, 0x1

    if-eqz v4, :cond_0

    const v4, -0x12477ce0

    ushr-int/lit8 v3, v3, 0x1

    xor-int/2addr v3, v4

    goto :goto_1

    :cond_0
    ushr-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->b:[I

    aput v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->a:I

    return-void
.end method

.method static a(JJJ)J
    .locals 16

    const/16 v0, 0x20

    new-array v1, v0, [J

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v2

    new-array v3, v0, [J

    const-wide/16 v4, 0x1

    const-wide/16 v6, 0x0

    if-nez v2, :cond_1

    cmp-long v8, p4, v6

    if-gtz v8, :cond_0

    return-wide p0

    :cond_0
    const/4 v8, 0x0

    const-wide v9, 0xedb88320L

    aput-wide v9, v3, v8

    move-wide v8, v4

    goto :goto_0

    :cond_1
    move-wide/from16 v8, p4

    :goto_0
    const/4 v10, 0x1

    move v11, v10

    :cond_2
    if-ge v11, v0, :cond_3

    aput-wide v8, v3, v11

    shl-long/2addr v8, v10

    add-int/lit8 v11, v11, 0x1

    if-nez v2, :cond_4

    if-eqz v2, :cond_2

    :cond_3
    invoke-static {v1, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->a([J[J)V

    :cond_4
    invoke-static {v3, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->a([J[J)V

    move-wide/from16 v8, p4

    move-object v11, v1

    move-object v12, v2

    move-object v13, v3

    move-wide/from16 v0, p0

    move-wide/from16 v2, p2

    :cond_5
    invoke-static {v11, v13}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->a([J[J)V

    and-long v14, v8, v4

    cmp-long v14, v14, v6

    if-eqz v14, :cond_6

    invoke-static {v11, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->a([JJ)J

    move-result-wide v0

    :cond_6
    shr-long/2addr v8, v10

    cmp-long v14, v8, v6

    if-nez v12, :cond_8

    if-nez v14, :cond_7

    goto :goto_1

    :cond_7
    invoke-static {v13, v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->a([J[J)V

    and-long v14, v8, v4

    cmp-long v14, v14, v6

    :cond_8
    if-nez v12, :cond_a

    if-eqz v14, :cond_9

    invoke-static {v13, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->a([JJ)J

    move-result-wide v0

    :cond_9
    shr-long/2addr v8, v10

    cmp-long v14, v8, v6

    :cond_a
    if-nez v14, :cond_5

    :goto_1
    xor-long/2addr v0, v2

    if-nez v12, :cond_6

    return-wide v0
.end method

.method private static a([JJ)J
    .locals 8

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    move-wide v4, v1

    :cond_0
    cmp-long v6, p1, v1

    if-eqz v6, :cond_3

    const-wide/16 v6, 0x1

    and-long/2addr v6, p1

    if-nez v0, :cond_4

    if-nez v0, :cond_2

    cmp-long v6, v6, v1

    if-eqz v6, :cond_1

    aget-wide v6, p0, v3

    xor-long/2addr v4, v6

    :cond_1
    const/4 v6, 0x1

    shr-long/2addr p1, v6

    goto :goto_0

    :cond_2
    move-wide p1, v6

    :goto_0
    add-int/lit8 v3, v3, 0x1

    if-eqz v0, :cond_0

    :cond_3
    move-wide v6, v4

    :cond_4
    return-wide v6
.end method

.method static final a([J[J)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    const/16 v2, 0x20

    if-ge v1, v2, :cond_1

    aget-wide v2, p1, v1

    invoke-static {p1, v2, v3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->a([JJ)J

    move-result-wide v2

    aput-wide v2, p0, v1

    add-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method public static getCRC32Table()[I
    .locals 4

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->b:[I

    array-length v1, v0

    new-array v2, v1, [I

    const/4 v3, 0x0

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public copy()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;
    .locals 2

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;-><init>()V

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->a:I

    iput v1, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->a:I

    return-object v0
.end method

.method public bridge synthetic copy()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;
    .locals 1

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->copy()Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;

    move-result-object v0

    return-object v0
.end method

.method public getValue()J
    .locals 4

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->a:I

    int-to-long v0, v0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    return-wide v0
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->a:I

    return-void
.end method

.method public reset(J)V
    .locals 2

    const-wide v0, 0xffffffffL

    and-long/2addr p1, v0

    long-to-int p1, p1

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->a:I

    return-void
.end method

.method public update([BII)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->a:I

    not-int v1, v1

    :goto_0
    add-int/lit8 p3, p3, -0x1

    if-ltz p3, :cond_1

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->b:[I

    add-int/lit8 v3, p2, 0x1

    aget-byte p2, p1, p2

    xor-int/2addr p2, v1

    and-int/lit16 p2, p2, 0xff

    aget p2, v2, p2

    ushr-int/lit8 v1, v1, 0x8

    xor-int/2addr v1, p2

    if-nez v0, :cond_2

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    move p2, v3

    goto :goto_0

    :cond_1
    :goto_1
    not-int p1, v1

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/CRC32;->a:I

    :cond_2
    return-void
.end method
