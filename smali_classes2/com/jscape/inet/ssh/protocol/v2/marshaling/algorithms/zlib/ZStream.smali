.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;
.super Ljava/lang/Object;


# static fields
.field private static final a:I = 0xf

.field private static final b:I = 0xf

.field private static final c:I = 0x0

.field private static final d:I = 0x1

.field private static final e:I = 0x2

.field private static final f:I = 0x3

.field private static final g:I = 0x4

.field private static final h:I = 0x9

.field private static final i:I = 0x0

.field private static final j:I = 0x1

.field private static final k:I = 0x2

.field private static final l:I = -0x1

.field private static final m:I = -0x2

.field private static final n:I = -0x3

.field private static final o:I = -0x4

.field private static final p:I = -0x5

.field private static final q:I = -0x6

.field private static v:[I


# instance fields
.field public availIn:I

.field public availOut:I

.field public msg:Ljava/lang/String;

.field public nextIn:[B

.field public nextInIndex:I

.field public nextOut:[B

.field public nextOutIndex:I

.field r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

.field s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

.field t:I

.field public totalIn:J

.field public totalOut:J

.field u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    new-array v0, v0, [I

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b([I)V

    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Adler32;-><init>()V

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    return-void
.end method

.method public static b([I)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->v:[I

    return-void
.end method

.method public static b()[I
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->v:[I

    return-object v0
.end method


# virtual methods
.method a([BII)I
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    if-nez v0, :cond_1

    if-le v1, p3, :cond_0

    goto :goto_0

    :cond_0
    move p3, v1

    :goto_0
    move v1, p3

    :cond_1
    if-nez v0, :cond_3

    if-nez v1, :cond_2

    const/4 p1, 0x0

    return p1

    :cond_2
    iget p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    sub-int/2addr p3, v1

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    iget-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iget p3, p3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ak:I

    goto :goto_1

    :cond_3
    move p3, v1

    :goto_1
    if-nez v0, :cond_5

    if-eqz p3, :cond_4

    iget-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    invoke-interface {p3, v0, v2, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->update([BII)V

    :cond_4
    iget-object p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    invoke-static {p3, v0, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    add-int/2addr p1, v1

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    int-to-long v2, v1

    add-long/2addr p1, v2

    iput-wide p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    goto :goto_2

    :cond_5
    move v1, p3

    :goto_2
    return v1
.end method

.method a()V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aj:I

    if-nez v0, :cond_0

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    if-le v1, v2, :cond_0

    move v1, v2

    :cond_0
    if-nez v0, :cond_2

    if-nez v1, :cond_1

    return-void

    :cond_1
    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ag:[B

    array-length v2, v2

    goto :goto_0

    :cond_2
    move v2, v1

    :goto_0
    if-nez v0, :cond_4

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iget v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ai:I

    if-le v2, v3, :cond_3

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B

    array-length v2, v2

    if-nez v0, :cond_4

    iget v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOutIndex:I

    if-le v2, v3, :cond_3

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ag:[B

    array-length v2, v2

    if-nez v0, :cond_4

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iget v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ai:I

    add-int/2addr v3, v1

    if-lt v2, v3, :cond_3

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B

    array-length v2, v2

    :cond_3
    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iget-object v2, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ag:[B

    iget-object v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iget v3, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ai:I

    iget-object v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B

    iget v5, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOutIndex:I

    invoke-static {v2, v3, v4, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOutIndex:I

    add-int/2addr v2, v1

    iput v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOutIndex:I

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iget v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ai:I

    add-int/2addr v3, v1

    iput v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ai:I

    iget-wide v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalOut:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalOut:J

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    sub-int/2addr v2, v1

    iput v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    iget-object v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    iget v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aj:I

    sub-int/2addr v3, v1

    iput v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aj:I

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    if-nez v0, :cond_5

    iget v2, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->aj:I

    :cond_4
    if-nez v2, :cond_6

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    :cond_5
    const/4 v0, 0x0

    iput v0, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->ai:I

    :cond_6
    return-void
.end method

.method public deflate(I)I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    const/4 p1, -0x2

    return p1

    :cond_0
    invoke-virtual {v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->h(I)I

    move-result p1

    return p1
.end method

.method public deflateEnd()I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    const/4 v0, -0x2

    return v0

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->k()I

    move-result v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    return v0
.end method

.method public deflateInit(I)I
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->deflateInit(II)I

    move-result p1

    return p1
.end method

.method public deflateInit(II)I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->deflateInit(IIZ)I

    move-result p1

    return p1
.end method

.method public deflateInit(III)I
    .locals 1

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    invoke-direct {v0, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)V

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->b(III)I

    move-result p1

    return p1
.end method

.method public deflateInit(IIILcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;)I
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    const/4 v1, -0x2

    if-nez v0, :cond_7

    const/16 v2, 0x9

    if-lt p2, v2, :cond_6

    if-nez v0, :cond_7

    const/16 v2, 0xf

    if-le p2, v2, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;->W_NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    if-nez v0, :cond_2

    if-ne p4, v2, :cond_1

    mul-int/lit8 p2, p2, -0x1

    if-eqz v0, :cond_5

    :cond_1
    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;->W_GZIP:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    :cond_2
    if-nez v0, :cond_4

    if-ne p4, v2, :cond_3

    add-int/lit8 p2, p2, 0x10

    if-eqz v0, :cond_5

    :cond_3
    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;->W_ANY:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    :cond_4
    if-ne p4, v2, :cond_5

    return v1

    :cond_5
    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->deflateInit(III)I

    move-result p1

    return p1

    :cond_6
    :goto_0
    move p2, v1

    :cond_7
    return p2
.end method

.method public deflateInit(IIZ)I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    invoke-direct {v1, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)V

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    if-nez v0, :cond_0

    if-eqz p3, :cond_1

    neg-int p2, p2

    goto :goto_0

    :cond_0
    move p2, p3

    :cond_1
    :goto_0
    invoke-virtual {v1, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->c(II)I

    move-result p1

    return p1
.end method

.method public deflateInit(IZ)I
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->deflateInit(IIZ)I

    move-result p1

    return p1
.end method

.method public deflateParams(II)I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    const/4 p1, -0x2

    return p1

    :cond_0
    invoke-virtual {v1, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->d(II)I

    move-result p1

    return p1
.end method

.method public deflateSetDictionary([BI)I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->r:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    const/4 p1, -0x2

    return p1

    :cond_0
    invoke-virtual {v1, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Deflate;->a([BI)I

    move-result p1

    return p1
.end method

.method public end()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public finished()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public free()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    return-void
.end method

.method public getAdler()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->u:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;

    invoke-interface {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Checksum;->getValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAvailIn()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    return v0
.end method

.method public getAvailOut()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    return-object v0
.end method

.method public getNextIn()[B
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    return-object v0
.end method

.method public getNextInIndex()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    return v0
.end method

.method public getNextOut()[B
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B

    return-object v0
.end method

.method public getNextOutIndex()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOutIndex:I

    return v0
.end method

.method public getTotalIn()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalIn:J

    return-wide v0
.end method

.method public getTotalOut()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->totalOut:J

    return-wide v0
.end method

.method public inflate(I)I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    const/4 p1, -0x2

    goto :goto_0

    :cond_0
    invoke-virtual {v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->b(I)I

    move-result p1

    :goto_0
    return p1
.end method

.method public inflateEnd()I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    const/4 v0, -0x2

    return v0

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->b()I

    move-result v0

    return v0
.end method

.method public inflateFinished()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    iget v1, v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->Q:I

    if-nez v0, :cond_1

    const/16 v0, 0xc

    if-ne v1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public inflateInit()I
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->inflateInit(I)I

    move-result v0

    return v0
.end method

.method public inflateInit(I)I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->inflateInit(IZ)I

    move-result p1

    return p1
.end method

.method public inflateInit(ILcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;)I
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;->W_NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    if-ne p2, v1, :cond_0

    const/4 v2, 0x1

    if-eqz v0, :cond_4

    :cond_0
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;->W_GZIP:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    :cond_1
    if-nez v0, :cond_3

    if-ne p2, v1, :cond_2

    add-int/lit8 p1, p1, 0x10

    if-eqz v0, :cond_4

    :cond_2
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib;->W_ANY:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;

    :cond_3
    if-ne p2, v1, :cond_4

    const/high16 p2, 0x40000000    # 2.0f

    or-int/2addr p1, p2

    :cond_4
    invoke-virtual {p0, p1, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->inflateInit(IZ)I

    move-result p1

    return p1
.end method

.method public inflateInit(IZ)I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    invoke-direct {v1, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;-><init>(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)V

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    neg-int p1, p1

    goto :goto_0

    :cond_0
    move p1, p2

    :cond_1
    :goto_0
    invoke-virtual {v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a(I)I

    move-result p1

    return p1
.end method

.method public inflateInit(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;)I
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->inflateInit(ILcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/JZlib$WrapperType;)I

    move-result p1

    return p1
.end method

.method public inflateInit(Z)I
    .locals 1

    const/16 v0, 0xf

    invoke-virtual {p0, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->inflateInit(IZ)I

    move-result p1

    return p1
.end method

.method public inflateSetDictionary([BI)I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    const/4 p1, -0x2

    return p1

    :cond_0
    invoke-virtual {v1, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->a([BI)I

    move-result p1

    return p1
.end method

.method public inflateSync()I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    const/4 v0, -0x2

    return v0

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->c()I

    move-result v0

    return v0
.end method

.method public inflateSyncPoint()I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->s:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    const/4 v0, -0x2

    return v0

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/Inflate;->d()I

    move-result v0

    return v0
.end method

.method public setAvailIn(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    return-void
.end method

.method public setAvailOut(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    return-void
.end method

.method public setInput([B)V
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->setInput([BIIZ)V

    return-void
.end method

.method public setInput([BIIZ)V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    if-nez v0, :cond_2

    if-gtz p3, :cond_1

    if-nez v0, :cond_0

    if-eqz p4, :cond_1

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    if-eqz v1, :cond_1

    return-void

    :cond_0
    move v1, p4

    goto :goto_0

    :cond_1
    if-nez v0, :cond_5

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    goto :goto_0

    :cond_2
    move v1, p3

    :goto_0
    if-lez v1, :cond_4

    if-nez v0, :cond_3

    if-eqz p4, :cond_4

    iget p4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    add-int/2addr p4, p3

    :cond_3
    new-array p4, p4, [B

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iget v2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget v3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    const/4 v4, 0x0

    invoke-static {v1, v2, p4, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    invoke-static {p1, p2, p4, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object p4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iput v4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    iget p4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    add-int/2addr p4, p3

    iput p4, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    if-eqz v0, :cond_6

    :cond_4
    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    :cond_5
    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availIn:I

    :cond_6
    return-void
.end method

.method public setInput([BZ)V
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->setInput([BIIZ)V

    return-void
.end method

.method public setNextIn([B)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextIn:[B

    return-void
.end method

.method public setNextInIndex(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextInIndex:I

    return-void
.end method

.method public setNextOut([B)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B

    return-void
.end method

.method public setNextOutIndex(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOutIndex:I

    return-void
.end method

.method public setOutput([B)V
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->setOutput([BII)V

    return-void
.end method

.method public setOutput([BII)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOut:[B

    iput p2, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->nextOutIndex:I

    iput p3, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->availOut:I

    return-void
.end method
