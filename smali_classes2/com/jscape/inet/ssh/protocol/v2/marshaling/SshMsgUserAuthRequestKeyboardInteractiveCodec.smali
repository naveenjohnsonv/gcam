.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestKeyboardInteractiveCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgUserAuthRequestCodec$MethodCodec;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->readUtf8Value(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestKeyboardInteractive;

    invoke-direct {v1, p2, p3, v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestKeyboardInteractive;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-object v1
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequest;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestKeyboardInteractive;

    iget-object v0, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestKeyboardInteractive;->languageTag:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgUserAuthRequestKeyboardInteractive;->subMethods:Ljava/util/List;

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->writeUtf8Value(Ljava/util/List;Ljava/io/OutputStream;)V

    return-void
.end method
