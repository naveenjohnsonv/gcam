.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;
.super Ljava/lang/Object;


# static fields
.field private static final a:I = 0x5a0

.field private static final b:I = 0x0

.field private static final c:I = 0x1

.field private static final d:I = 0x2

.field private static final e:I = -0x1

.field private static final f:I = -0x2

.field private static final g:I = -0x3

.field private static final h:I = -0x4

.field private static final i:I = -0x5

.field private static final j:I = -0x6

.field static final k:I = 0x9

.field static final l:I = 0x5

.field static final m:[I

.field static final n:[I

.field static final o:[I

.field static final p:[I

.field static final q:[I

.field static final r:[I

.field static final s:I = 0xf

.field private static final z:[Ljava/lang/String;


# instance fields
.field t:[I

.field u:[I

.field v:[I

.field w:[I

.field x:[I

.field y:[I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x1e

    const-string v4, ",GqG]\u0012K ]w\u0008\\\u000bS [sD\u001f\u000eB+Nf@\u0010\u0016U L\u001c*_wZC\u0017E6J`AR\u0007CeM{[D\u0003I&L2\\B\u0007B\'*_wZC\u0017E6J`AR\u0007CeMkFQ\u000fN&\tpADBK Gu\\X\u0011\u00071[wM#,GqG]\u0012K ]w\u0008T\u001bI$D{K\u0010\u0000N1\t~M^\u0005S-Z2\\B\u0007B\u0018,GqG]\u0012K ]w\u0008T\u000bT1H|KUBS7Lw"

    const/16 v5, 0xa0

    move v8, v2

    move v7, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x9

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v2

    :goto_2
    const/16 v15, 0x20

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x43

    const-string v4, "7SuK^UT;Mq^I\u0016UrJwZBUG;Jm\u001fK\u0010^5JmL\"=H`MT\u0000R!]wVE\u0010TrRlKB\u0007Q>\u0011iZI\u0012D:\u001eqMB\u0010"

    move v8, v11

    move v7, v15

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    add-int/2addr v6, v10

    add-int v9, v6, v7

    invoke-virtual {v4, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v2

    move v9, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->z:[Ljava/lang/String;

    const/16 v0, 0x600

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->m:[I

    const/16 v0, 0x60

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->n:[I

    const/16 v0, 0x1f

    new-array v1, v0, [I

    fill-array-data v1, :array_2

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->o:[I

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->p:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->q:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->r:[I

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_8

    if-eq v1, v10, :cond_9

    const/4 v15, 0x2

    if-eq v1, v15, :cond_7

    const/4 v15, 0x3

    if-eq v1, v15, :cond_6

    const/4 v15, 0x4

    if-eq v1, v15, :cond_5

    const/4 v15, 0x5

    if-eq v1, v15, :cond_4

    const/16 v15, 0x2e

    goto :goto_4

    :cond_4
    const/16 v15, 0x6b

    goto :goto_4

    :cond_5
    const/16 v15, 0x39

    goto :goto_4

    :cond_6
    const/16 v15, 0x21

    goto :goto_4

    :cond_7
    const/16 v15, 0x1b

    goto :goto_4

    :cond_8
    const/16 v15, 0x4c

    :cond_9
    :goto_4
    xor-int v1, v9, v15

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2

    nop

    :array_0
    .array-data 4
        0x60
        0x7
        0x100
        0x0
        0x8
        0x50
        0x0
        0x8
        0x10
        0x54
        0x8
        0x73
        0x52
        0x7
        0x1f
        0x0
        0x8
        0x70
        0x0
        0x8
        0x30
        0x0
        0x9
        0xc0
        0x50
        0x7
        0xa
        0x0
        0x8
        0x60
        0x0
        0x8
        0x20
        0x0
        0x9
        0xa0
        0x0
        0x8
        0x0
        0x0
        0x8
        0x80
        0x0
        0x8
        0x40
        0x0
        0x9
        0xe0
        0x50
        0x7
        0x6
        0x0
        0x8
        0x58
        0x0
        0x8
        0x18
        0x0
        0x9
        0x90
        0x53
        0x7
        0x3b
        0x0
        0x8
        0x78
        0x0
        0x8
        0x38
        0x0
        0x9
        0xd0
        0x51
        0x7
        0x11
        0x0
        0x8
        0x68
        0x0
        0x8
        0x28
        0x0
        0x9
        0xb0
        0x0
        0x8
        0x8
        0x0
        0x8
        0x88
        0x0
        0x8
        0x48
        0x0
        0x9
        0xf0
        0x50
        0x7
        0x4
        0x0
        0x8
        0x54
        0x0
        0x8
        0x14
        0x55
        0x8
        0xe3
        0x53
        0x7
        0x2b
        0x0
        0x8
        0x74
        0x0
        0x8
        0x34
        0x0
        0x9
        0xc8
        0x51
        0x7
        0xd
        0x0
        0x8
        0x64
        0x0
        0x8
        0x24
        0x0
        0x9
        0xa8
        0x0
        0x8
        0x4
        0x0
        0x8
        0x84
        0x0
        0x8
        0x44
        0x0
        0x9
        0xe8
        0x50
        0x7
        0x8
        0x0
        0x8
        0x5c
        0x0
        0x8
        0x1c
        0x0
        0x9
        0x98
        0x54
        0x7
        0x53
        0x0
        0x8
        0x7c
        0x0
        0x8
        0x3c
        0x0
        0x9
        0xd8
        0x52
        0x7
        0x17
        0x0
        0x8
        0x6c
        0x0
        0x8
        0x2c
        0x0
        0x9
        0xb8
        0x0
        0x8
        0xc
        0x0
        0x8
        0x8c
        0x0
        0x8
        0x4c
        0x0
        0x9
        0xf8
        0x50
        0x7
        0x3
        0x0
        0x8
        0x52
        0x0
        0x8
        0x12
        0x55
        0x8
        0xa3
        0x53
        0x7
        0x23
        0x0
        0x8
        0x72
        0x0
        0x8
        0x32
        0x0
        0x9
        0xc4
        0x51
        0x7
        0xb
        0x0
        0x8
        0x62
        0x0
        0x8
        0x22
        0x0
        0x9
        0xa4
        0x0
        0x8
        0x2
        0x0
        0x8
        0x82
        0x0
        0x8
        0x42
        0x0
        0x9
        0xe4
        0x50
        0x7
        0x7
        0x0
        0x8
        0x5a
        0x0
        0x8
        0x1a
        0x0
        0x9
        0x94
        0x54
        0x7
        0x43
        0x0
        0x8
        0x7a
        0x0
        0x8
        0x3a
        0x0
        0x9
        0xd4
        0x52
        0x7
        0x13
        0x0
        0x8
        0x6a
        0x0
        0x8
        0x2a
        0x0
        0x9
        0xb4
        0x0
        0x8
        0xa
        0x0
        0x8
        0x8a
        0x0
        0x8
        0x4a
        0x0
        0x9
        0xf4
        0x50
        0x7
        0x5
        0x0
        0x8
        0x56
        0x0
        0x8
        0x16
        0xc0
        0x8
        0x0
        0x53
        0x7
        0x33
        0x0
        0x8
        0x76
        0x0
        0x8
        0x36
        0x0
        0x9
        0xcc
        0x51
        0x7
        0xf
        0x0
        0x8
        0x66
        0x0
        0x8
        0x26
        0x0
        0x9
        0xac
        0x0
        0x8
        0x6
        0x0
        0x8
        0x86
        0x0
        0x8
        0x46
        0x0
        0x9
        0xec
        0x50
        0x7
        0x9
        0x0
        0x8
        0x5e
        0x0
        0x8
        0x1e
        0x0
        0x9
        0x9c
        0x54
        0x7
        0x63
        0x0
        0x8
        0x7e
        0x0
        0x8
        0x3e
        0x0
        0x9
        0xdc
        0x52
        0x7
        0x1b
        0x0
        0x8
        0x6e
        0x0
        0x8
        0x2e
        0x0
        0x9
        0xbc
        0x0
        0x8
        0xe
        0x0
        0x8
        0x8e
        0x0
        0x8
        0x4e
        0x0
        0x9
        0xfc
        0x60
        0x7
        0x100
        0x0
        0x8
        0x51
        0x0
        0x8
        0x11
        0x55
        0x8
        0x83
        0x52
        0x7
        0x1f
        0x0
        0x8
        0x71
        0x0
        0x8
        0x31
        0x0
        0x9
        0xc2
        0x50
        0x7
        0xa
        0x0
        0x8
        0x61
        0x0
        0x8
        0x21
        0x0
        0x9
        0xa2
        0x0
        0x8
        0x1
        0x0
        0x8
        0x81
        0x0
        0x8
        0x41
        0x0
        0x9
        0xe2
        0x50
        0x7
        0x6
        0x0
        0x8
        0x59
        0x0
        0x8
        0x19
        0x0
        0x9
        0x92
        0x53
        0x7
        0x3b
        0x0
        0x8
        0x79
        0x0
        0x8
        0x39
        0x0
        0x9
        0xd2
        0x51
        0x7
        0x11
        0x0
        0x8
        0x69
        0x0
        0x8
        0x29
        0x0
        0x9
        0xb2
        0x0
        0x8
        0x9
        0x0
        0x8
        0x89
        0x0
        0x8
        0x49
        0x0
        0x9
        0xf2
        0x50
        0x7
        0x4
        0x0
        0x8
        0x55
        0x0
        0x8
        0x15
        0x50
        0x8
        0x102
        0x53
        0x7
        0x2b
        0x0
        0x8
        0x75
        0x0
        0x8
        0x35
        0x0
        0x9
        0xca
        0x51
        0x7
        0xd
        0x0
        0x8
        0x65
        0x0
        0x8
        0x25
        0x0
        0x9
        0xaa
        0x0
        0x8
        0x5
        0x0
        0x8
        0x85
        0x0
        0x8
        0x45
        0x0
        0x9
        0xea
        0x50
        0x7
        0x8
        0x0
        0x8
        0x5d
        0x0
        0x8
        0x1d
        0x0
        0x9
        0x9a
        0x54
        0x7
        0x53
        0x0
        0x8
        0x7d
        0x0
        0x8
        0x3d
        0x0
        0x9
        0xda
        0x52
        0x7
        0x17
        0x0
        0x8
        0x6d
        0x0
        0x8
        0x2d
        0x0
        0x9
        0xba
        0x0
        0x8
        0xd
        0x0
        0x8
        0x8d
        0x0
        0x8
        0x4d
        0x0
        0x9
        0xfa
        0x50
        0x7
        0x3
        0x0
        0x8
        0x53
        0x0
        0x8
        0x13
        0x55
        0x8
        0xc3
        0x53
        0x7
        0x23
        0x0
        0x8
        0x73
        0x0
        0x8
        0x33
        0x0
        0x9
        0xc6
        0x51
        0x7
        0xb
        0x0
        0x8
        0x63
        0x0
        0x8
        0x23
        0x0
        0x9
        0xa6
        0x0
        0x8
        0x3
        0x0
        0x8
        0x83
        0x0
        0x8
        0x43
        0x0
        0x9
        0xe6
        0x50
        0x7
        0x7
        0x0
        0x8
        0x5b
        0x0
        0x8
        0x1b
        0x0
        0x9
        0x96
        0x54
        0x7
        0x43
        0x0
        0x8
        0x7b
        0x0
        0x8
        0x3b
        0x0
        0x9
        0xd6
        0x52
        0x7
        0x13
        0x0
        0x8
        0x6b
        0x0
        0x8
        0x2b
        0x0
        0x9
        0xb6
        0x0
        0x8
        0xb
        0x0
        0x8
        0x8b
        0x0
        0x8
        0x4b
        0x0
        0x9
        0xf6
        0x50
        0x7
        0x5
        0x0
        0x8
        0x57
        0x0
        0x8
        0x17
        0xc0
        0x8
        0x0
        0x53
        0x7
        0x33
        0x0
        0x8
        0x77
        0x0
        0x8
        0x37
        0x0
        0x9
        0xce
        0x51
        0x7
        0xf
        0x0
        0x8
        0x67
        0x0
        0x8
        0x27
        0x0
        0x9
        0xae
        0x0
        0x8
        0x7
        0x0
        0x8
        0x87
        0x0
        0x8
        0x47
        0x0
        0x9
        0xee
        0x50
        0x7
        0x9
        0x0
        0x8
        0x5f
        0x0
        0x8
        0x1f
        0x0
        0x9
        0x9e
        0x54
        0x7
        0x63
        0x0
        0x8
        0x7f
        0x0
        0x8
        0x3f
        0x0
        0x9
        0xde
        0x52
        0x7
        0x1b
        0x0
        0x8
        0x6f
        0x0
        0x8
        0x2f
        0x0
        0x9
        0xbe
        0x0
        0x8
        0xf
        0x0
        0x8
        0x8f
        0x0
        0x8
        0x4f
        0x0
        0x9
        0xfe
        0x60
        0x7
        0x100
        0x0
        0x8
        0x50
        0x0
        0x8
        0x10
        0x54
        0x8
        0x73
        0x52
        0x7
        0x1f
        0x0
        0x8
        0x70
        0x0
        0x8
        0x30
        0x0
        0x9
        0xc1
        0x50
        0x7
        0xa
        0x0
        0x8
        0x60
        0x0
        0x8
        0x20
        0x0
        0x9
        0xa1
        0x0
        0x8
        0x0
        0x0
        0x8
        0x80
        0x0
        0x8
        0x40
        0x0
        0x9
        0xe1
        0x50
        0x7
        0x6
        0x0
        0x8
        0x58
        0x0
        0x8
        0x18
        0x0
        0x9
        0x91
        0x53
        0x7
        0x3b
        0x0
        0x8
        0x78
        0x0
        0x8
        0x38
        0x0
        0x9
        0xd1
        0x51
        0x7
        0x11
        0x0
        0x8
        0x68
        0x0
        0x8
        0x28
        0x0
        0x9
        0xb1
        0x0
        0x8
        0x8
        0x0
        0x8
        0x88
        0x0
        0x8
        0x48
        0x0
        0x9
        0xf1
        0x50
        0x7
        0x4
        0x0
        0x8
        0x54
        0x0
        0x8
        0x14
        0x55
        0x8
        0xe3
        0x53
        0x7
        0x2b
        0x0
        0x8
        0x74
        0x0
        0x8
        0x34
        0x0
        0x9
        0xc9
        0x51
        0x7
        0xd
        0x0
        0x8
        0x64
        0x0
        0x8
        0x24
        0x0
        0x9
        0xa9
        0x0
        0x8
        0x4
        0x0
        0x8
        0x84
        0x0
        0x8
        0x44
        0x0
        0x9
        0xe9
        0x50
        0x7
        0x8
        0x0
        0x8
        0x5c
        0x0
        0x8
        0x1c
        0x0
        0x9
        0x99
        0x54
        0x7
        0x53
        0x0
        0x8
        0x7c
        0x0
        0x8
        0x3c
        0x0
        0x9
        0xd9
        0x52
        0x7
        0x17
        0x0
        0x8
        0x6c
        0x0
        0x8
        0x2c
        0x0
        0x9
        0xb9
        0x0
        0x8
        0xc
        0x0
        0x8
        0x8c
        0x0
        0x8
        0x4c
        0x0
        0x9
        0xf9
        0x50
        0x7
        0x3
        0x0
        0x8
        0x52
        0x0
        0x8
        0x12
        0x55
        0x8
        0xa3
        0x53
        0x7
        0x23
        0x0
        0x8
        0x72
        0x0
        0x8
        0x32
        0x0
        0x9
        0xc5
        0x51
        0x7
        0xb
        0x0
        0x8
        0x62
        0x0
        0x8
        0x22
        0x0
        0x9
        0xa5
        0x0
        0x8
        0x2
        0x0
        0x8
        0x82
        0x0
        0x8
        0x42
        0x0
        0x9
        0xe5
        0x50
        0x7
        0x7
        0x0
        0x8
        0x5a
        0x0
        0x8
        0x1a
        0x0
        0x9
        0x95
        0x54
        0x7
        0x43
        0x0
        0x8
        0x7a
        0x0
        0x8
        0x3a
        0x0
        0x9
        0xd5
        0x52
        0x7
        0x13
        0x0
        0x8
        0x6a
        0x0
        0x8
        0x2a
        0x0
        0x9
        0xb5
        0x0
        0x8
        0xa
        0x0
        0x8
        0x8a
        0x0
        0x8
        0x4a
        0x0
        0x9
        0xf5
        0x50
        0x7
        0x5
        0x0
        0x8
        0x56
        0x0
        0x8
        0x16
        0xc0
        0x8
        0x0
        0x53
        0x7
        0x33
        0x0
        0x8
        0x76
        0x0
        0x8
        0x36
        0x0
        0x9
        0xcd
        0x51
        0x7
        0xf
        0x0
        0x8
        0x66
        0x0
        0x8
        0x26
        0x0
        0x9
        0xad
        0x0
        0x8
        0x6
        0x0
        0x8
        0x86
        0x0
        0x8
        0x46
        0x0
        0x9
        0xed
        0x50
        0x7
        0x9
        0x0
        0x8
        0x5e
        0x0
        0x8
        0x1e
        0x0
        0x9
        0x9d
        0x54
        0x7
        0x63
        0x0
        0x8
        0x7e
        0x0
        0x8
        0x3e
        0x0
        0x9
        0xdd
        0x52
        0x7
        0x1b
        0x0
        0x8
        0x6e
        0x0
        0x8
        0x2e
        0x0
        0x9
        0xbd
        0x0
        0x8
        0xe
        0x0
        0x8
        0x8e
        0x0
        0x8
        0x4e
        0x0
        0x9
        0xfd
        0x60
        0x7
        0x100
        0x0
        0x8
        0x51
        0x0
        0x8
        0x11
        0x55
        0x8
        0x83
        0x52
        0x7
        0x1f
        0x0
        0x8
        0x71
        0x0
        0x8
        0x31
        0x0
        0x9
        0xc3
        0x50
        0x7
        0xa
        0x0
        0x8
        0x61
        0x0
        0x8
        0x21
        0x0
        0x9
        0xa3
        0x0
        0x8
        0x1
        0x0
        0x8
        0x81
        0x0
        0x8
        0x41
        0x0
        0x9
        0xe3
        0x50
        0x7
        0x6
        0x0
        0x8
        0x59
        0x0
        0x8
        0x19
        0x0
        0x9
        0x93
        0x53
        0x7
        0x3b
        0x0
        0x8
        0x79
        0x0
        0x8
        0x39
        0x0
        0x9
        0xd3
        0x51
        0x7
        0x11
        0x0
        0x8
        0x69
        0x0
        0x8
        0x29
        0x0
        0x9
        0xb3
        0x0
        0x8
        0x9
        0x0
        0x8
        0x89
        0x0
        0x8
        0x49
        0x0
        0x9
        0xf3
        0x50
        0x7
        0x4
        0x0
        0x8
        0x55
        0x0
        0x8
        0x15
        0x50
        0x8
        0x102
        0x53
        0x7
        0x2b
        0x0
        0x8
        0x75
        0x0
        0x8
        0x35
        0x0
        0x9
        0xcb
        0x51
        0x7
        0xd
        0x0
        0x8
        0x65
        0x0
        0x8
        0x25
        0x0
        0x9
        0xab
        0x0
        0x8
        0x5
        0x0
        0x8
        0x85
        0x0
        0x8
        0x45
        0x0
        0x9
        0xeb
        0x50
        0x7
        0x8
        0x0
        0x8
        0x5d
        0x0
        0x8
        0x1d
        0x0
        0x9
        0x9b
        0x54
        0x7
        0x53
        0x0
        0x8
        0x7d
        0x0
        0x8
        0x3d
        0x0
        0x9
        0xdb
        0x52
        0x7
        0x17
        0x0
        0x8
        0x6d
        0x0
        0x8
        0x2d
        0x0
        0x9
        0xbb
        0x0
        0x8
        0xd
        0x0
        0x8
        0x8d
        0x0
        0x8
        0x4d
        0x0
        0x9
        0xfb
        0x50
        0x7
        0x3
        0x0
        0x8
        0x53
        0x0
        0x8
        0x13
        0x55
        0x8
        0xc3
        0x53
        0x7
        0x23
        0x0
        0x8
        0x73
        0x0
        0x8
        0x33
        0x0
        0x9
        0xc7
        0x51
        0x7
        0xb
        0x0
        0x8
        0x63
        0x0
        0x8
        0x23
        0x0
        0x9
        0xa7
        0x0
        0x8
        0x3
        0x0
        0x8
        0x83
        0x0
        0x8
        0x43
        0x0
        0x9
        0xe7
        0x50
        0x7
        0x7
        0x0
        0x8
        0x5b
        0x0
        0x8
        0x1b
        0x0
        0x9
        0x97
        0x54
        0x7
        0x43
        0x0
        0x8
        0x7b
        0x0
        0x8
        0x3b
        0x0
        0x9
        0xd7
        0x52
        0x7
        0x13
        0x0
        0x8
        0x6b
        0x0
        0x8
        0x2b
        0x0
        0x9
        0xb7
        0x0
        0x8
        0xb
        0x0
        0x8
        0x8b
        0x0
        0x8
        0x4b
        0x0
        0x9
        0xf7
        0x50
        0x7
        0x5
        0x0
        0x8
        0x57
        0x0
        0x8
        0x17
        0xc0
        0x8
        0x0
        0x53
        0x7
        0x33
        0x0
        0x8
        0x77
        0x0
        0x8
        0x37
        0x0
        0x9
        0xcf
        0x51
        0x7
        0xf
        0x0
        0x8
        0x67
        0x0
        0x8
        0x27
        0x0
        0x9
        0xaf
        0x0
        0x8
        0x7
        0x0
        0x8
        0x87
        0x0
        0x8
        0x47
        0x0
        0x9
        0xef
        0x50
        0x7
        0x9
        0x0
        0x8
        0x5f
        0x0
        0x8
        0x1f
        0x0
        0x9
        0x9f
        0x54
        0x7
        0x63
        0x0
        0x8
        0x7f
        0x0
        0x8
        0x3f
        0x0
        0x9
        0xdf
        0x52
        0x7
        0x1b
        0x0
        0x8
        0x6f
        0x0
        0x8
        0x2f
        0x0
        0x9
        0xbf
        0x0
        0x8
        0xf
        0x0
        0x8
        0x8f
        0x0
        0x8
        0x4f
        0x0
        0x9
        0xff
    .end array-data

    :array_1
    .array-data 4
        0x50
        0x5
        0x1
        0x57
        0x5
        0x101
        0x53
        0x5
        0x11
        0x5b
        0x5
        0x1001
        0x51
        0x5
        0x5
        0x59
        0x5
        0x401
        0x55
        0x5
        0x41
        0x5d
        0x5
        0x4001
        0x50
        0x5
        0x3
        0x58
        0x5
        0x201
        0x54
        0x5
        0x21
        0x5c
        0x5
        0x2001
        0x52
        0x5
        0x9
        0x5a
        0x5
        0x801
        0x56
        0x5
        0x81
        0xc0
        0x5
        0x6001
        0x50
        0x5
        0x2
        0x57
        0x5
        0x181
        0x53
        0x5
        0x19
        0x5b
        0x5
        0x1801
        0x51
        0x5
        0x7
        0x59
        0x5
        0x601
        0x55
        0x5
        0x61
        0x5d
        0x5
        0x6001
        0x50
        0x5
        0x4
        0x58
        0x5
        0x301
        0x54
        0x5
        0x31
        0x5c
        0x5
        0x3001
        0x52
        0x5
        0xd
        0x5a
        0x5
        0xc01
        0x56
        0x5
        0xc1
        0xc0
        0x5
        0x6001
    .end array-data

    :array_2
    .array-data 4
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        0xa
        0xb
        0xd
        0xf
        0x11
        0x13
        0x17
        0x1b
        0x1f
        0x23
        0x2b
        0x33
        0x3b
        0x43
        0x53
        0x63
        0x73
        0x83
        0xa3
        0xc3
        0xe3
        0x102
        0x0
        0x0
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x1
        0x1
        0x1
        0x1
        0x2
        0x2
        0x2
        0x2
        0x3
        0x3
        0x3
        0x3
        0x4
        0x4
        0x4
        0x4
        0x5
        0x5
        0x5
        0x5
        0x0
        0x70
        0x70
    .end array-data

    :array_4
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
        0x7
        0x9
        0xd
        0x11
        0x19
        0x21
        0x31
        0x41
        0x61
        0x81
        0xc1
        0x101
        0x181
        0x201
        0x301
        0x401
        0x601
        0x801
        0xc01
        0x1001
        0x1801
        0x2001
        0x3001
        0x4001
        0x6001
    .end array-data

    :array_5
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x1
        0x1
        0x2
        0x2
        0x3
        0x3
        0x4
        0x4
        0x5
        0x5
        0x6
        0x6
        0x7
        0x7
        0x8
        0x8
        0x9
        0x9
        0xa
        0xa
        0xb
        0xb
        0xc
        0xc
        0xd
        0xd
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->t:[I

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->u:[I

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->v:[I

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->w:[I

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->x:[I

    iput-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->y:[I

    return-void
.end method

.method private a([IIII[I[I[I[I[I[I[I)I
    .locals 28

    move-object/from16 v0, p0

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v1

    const/4 v2, 0x0

    move/from16 v4, p3

    move v3, v2

    :goto_0
    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->v:[I

    add-int v6, p2, v3

    aget v6, p1, v6

    aget v7, v5, v6

    const/4 v8, 0x1

    add-int/2addr v7, v8

    aput v7, v5, v6

    add-int/2addr v3, v8

    const/4 v5, -0x1

    add-int/2addr v4, v5

    move v6, v4

    :goto_1
    if-nez v6, :cond_44

    iget-object v6, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->v:[I

    aget v6, v6, v2

    move/from16 v7, p3

    if-nez v1, :cond_43

    if-nez v1, :cond_1

    if-ne v6, v7, :cond_0

    aput v5, p7, v2

    aput v2, p8, v2

    return v2

    :cond_0
    aget v6, p8, v2

    :cond_1
    move v4, v8

    :cond_2
    const/16 v9, 0xf

    if-gt v4, v9, :cond_4

    iget-object v10, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->v:[I

    aget v10, v10, v4

    if-nez v1, :cond_5

    if-eqz v10, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v4, v4, 0x1

    if-eqz v1, :cond_2

    :cond_4
    :goto_2
    move v10, v4

    :cond_5
    if-nez v1, :cond_7

    if-ge v6, v4, :cond_6

    move v6, v4

    :cond_6
    move/from16 v26, v9

    move v9, v6

    move/from16 v6, v26

    goto :goto_3

    :cond_7
    move v9, v6

    :cond_8
    :goto_3
    if-eqz v6, :cond_a

    iget-object v11, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->v:[I

    aget v11, v11, v6

    if-nez v1, :cond_b

    if-eqz v11, :cond_9

    goto :goto_4

    :cond_9
    add-int/lit8 v6, v6, -0x1

    if-eqz v1, :cond_8

    :cond_a
    :goto_4
    move v11, v6

    :cond_b
    if-nez v1, :cond_d

    if-le v9, v6, :cond_c

    move v9, v6

    :cond_c
    aput v9, p8, v2

    move v13, v4

    move v12, v9

    move v9, v8

    goto :goto_5

    :cond_d
    move v13, v6

    move v12, v9

    :goto_5
    shl-int/2addr v9, v13

    :cond_e
    const/4 v13, -0x3

    if-ge v4, v6, :cond_11

    iget-object v14, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->v:[I

    aget v14, v14, v4

    sub-int/2addr v9, v14

    if-nez v1, :cond_12

    if-nez v1, :cond_10

    if-gez v9, :cond_f

    return v13

    :cond_f
    add-int/lit8 v4, v4, 0x1

    shl-int/lit8 v9, v9, 0x1

    :cond_10
    if-eqz v1, :cond_e

    :cond_11
    iget-object v14, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->v:[I

    aget v14, v14, v6

    sub-int/2addr v9, v14

    :cond_12
    if-nez v1, :cond_14

    if-gez v9, :cond_13

    return v13

    :cond_13
    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->v:[I

    aget v4, v3, v6

    add-int/2addr v4, v9

    aput v4, v3, v6

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->y:[I

    aput v2, v3, v8

    move v4, v2

    move v3, v8

    const/4 v15, 0x2

    goto :goto_6

    :cond_14
    move v15, v9

    :goto_6
    add-int/2addr v6, v5

    if-eqz v6, :cond_16

    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->y:[I

    iget-object v14, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->v:[I

    aget v14, v14, v3

    add-int/2addr v4, v14

    aput v4, v5, v15

    add-int/2addr v15, v8

    add-int/2addr v3, v8

    if-nez v1, :cond_17

    if-eqz v1, :cond_15

    goto :goto_7

    :cond_15
    const/4 v5, -0x1

    goto :goto_6

    :cond_16
    :goto_7
    move v6, v2

    :cond_17
    move v3, v2

    :goto_8
    add-int v4, p2, v3

    aget v4, p1, v4

    if-eqz v4, :cond_18

    iget-object v5, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->y:[I

    aget v14, v5, v4

    add-int/lit8 v15, v14, 0x1

    aput v15, v5, v4

    aput v6, p11, v14

    :cond_18
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v6, v6, 0x1

    move v4, v6

    :goto_9
    if-lt v6, v7, :cond_42

    iget-object v3, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->y:[I

    aget v7, v3, v11

    aput v2, v3, v2

    neg-int v3, v12

    iget-object v4, v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->x:[I

    aput v2, v4, v2

    if-nez v1, :cond_41

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p9

    move/from16 v21, v2

    move/from16 v22, v21

    move/from16 v23, v22

    move/from16 v24, v23

    move v14, v9

    move v15, v10

    move v13, v11

    move/from16 v19, v12

    const/16 v16, -0x1

    move-object/from16 v9, p11

    move-object v2, v0

    move-object v10, v1

    move v11, v3

    move v12, v7

    move/from16 v1, p4

    move-object/from16 v3, p5

    move-object/from16 v7, p10

    :goto_a
    if-gt v15, v13, :cond_3d

    iget-object v8, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->v:[I

    aget v8, v8, v15

    if-nez v10, :cond_3c

    move/from16 v0, v19

    :goto_b
    add-int/lit8 v19, v8, -0x1

    if-eqz v8, :cond_3a

    :goto_c
    add-int v8, v11, v0

    move/from16 p1, v1

    if-le v15, v8, :cond_28

    add-int/lit8 v16, v16, 0x1

    sub-int v11, v13, v8

    if-nez v10, :cond_1a

    if-nez v10, :cond_19

    if-le v11, v0, :cond_1a

    move v11, v0

    goto :goto_d

    :cond_19
    move v1, v0

    move/from16 v24, v23

    const/16 v18, -0x3

    move/from16 v23, v22

    move/from16 v22, v21

    move/from16 v21, v19

    move/from16 v19, v16

    move/from16 v16, v15

    move v15, v14

    move v14, v13

    move v13, v12

    move v12, v11

    move-object v11, v10

    move-object v10, v9

    move v9, v8

    move-object v8, v7

    move-object v7, v6

    move v6, v12

    move-object/from16 v26, v2

    move/from16 v2, p1

    move-object/from16 p1, v5

    move-object v5, v4

    move-object v4, v3

    move-object/from16 v3, v26

    goto/16 :goto_18

    :cond_1a
    :goto_d
    sub-int v1, v15, v8

    move-object/from16 p3, v3

    const/16 v24, 0x1

    shl-int v3, v24, v1

    move-object/from16 p4, v4

    add-int/lit8 v4, v19, 0x1

    if-nez v10, :cond_23

    if-le v3, v4, :cond_21

    sub-int/2addr v3, v4

    if-nez v10, :cond_20

    if-ge v1, v11, :cond_21

    move v4, v15

    :goto_e
    add-int/lit8 v1, v1, 0x1

    if-ge v1, v11, :cond_1f

    shl-int/lit8 v3, v3, 0x1

    move/from16 p5, v1

    iget-object v1, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->v:[I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 p6, v9

    aget v9, v1, v4

    if-nez v10, :cond_1e

    if-nez v10, :cond_1c

    if-gt v3, v9, :cond_1b

    goto :goto_f

    :cond_1b
    aget v9, v1, v4

    :cond_1c
    sub-int/2addr v3, v9

    if-eqz v10, :cond_1d

    goto :goto_f

    :cond_1d
    move/from16 v1, p5

    move-object/from16 v9, p6

    const/16 v24, 0x1

    goto :goto_e

    :cond_1e
    move v1, v3

    move v4, v11

    move/from16 v3, p5

    move v11, v9

    goto :goto_10

    :cond_1f
    move/from16 p5, v1

    move-object/from16 p6, v9

    :goto_f
    move/from16 v1, p5

    goto :goto_11

    :cond_20
    move-object/from16 p6, v9

    move v3, v1

    move v4, v11

    :goto_10
    const/4 v9, 0x0

    goto :goto_13

    :cond_21
    move-object/from16 p6, v9

    :goto_11
    const/4 v3, 0x1

    shl-int v11, v3, v1

    const/4 v9, 0x0

    aget v3, v7, v9

    add-int/2addr v3, v11

    if-nez v10, :cond_22

    const/16 v4, 0x5a0

    goto :goto_12

    :cond_22
    move/from16 v24, v11

    const/16 v18, -0x3

    goto :goto_14

    :cond_23
    move-object/from16 p6, v9

    const/4 v9, 0x0

    :goto_12
    move/from16 v26, v3

    move v3, v1

    move/from16 v1, v26

    move/from16 v27, v11

    move v11, v4

    move/from16 v4, v27

    :goto_13
    const/16 v18, -0x3

    if-le v1, v11, :cond_24

    return v18

    :cond_24
    iget-object v1, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->x:[I

    aget v11, v7, v9

    aput v11, v1, v16

    aget v1, v7, v9

    add-int/2addr v1, v4

    aput v1, v7, v9

    if-nez v10, :cond_26

    move v1, v3

    move/from16 v24, v4

    move/from16 v23, v11

    move/from16 v3, v16

    :goto_14
    if-eqz v3, :cond_25

    iget-object v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->y:[I

    aput v21, v3, v16

    iget-object v3, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->w:[I

    int-to-byte v1, v1

    aput v1, v3, v9

    int-to-byte v1, v0

    const/4 v4, 0x1

    aput v1, v3, v4

    sub-int v1, v8, v0

    ushr-int v1, v21, v1

    iget-object v4, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->x:[I

    add-int/lit8 v9, v16, -0x1

    aget v11, v4, v9

    sub-int v11, v23, v11

    sub-int/2addr v11, v1

    const/16 v17, 0x2

    aput v11, v3, v17

    aget v4, v4, v9

    add-int/2addr v4, v1

    const/4 v1, 0x3

    mul-int/2addr v4, v1

    const/4 v9, 0x0

    invoke-static {v3, v9, v6, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    if-eqz v10, :cond_27

    :cond_25
    aput v23, v5, v9

    goto :goto_15

    :cond_26
    move/from16 v24, v4

    move/from16 v23, v11

    :goto_15
    if-eqz v10, :cond_27

    move v11, v8

    goto :goto_16

    :cond_27
    move/from16 v1, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v9, p6

    move v11, v8

    goto/16 :goto_c

    :cond_28
    move-object/from16 p3, v3

    move-object/from16 p4, v4

    move-object/from16 p6, v9

    const/16 v18, -0x3

    :goto_16
    iget-object v1, v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->w:[I

    sub-int v3, v15, v11

    int-to-byte v3, v3

    const/4 v4, 0x1

    aput v3, v1, v4

    move/from16 v1, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v9, p6

    move v8, v11

    :goto_17
    move-object/from16 p1, v5

    move-object v11, v10

    move-object v5, v4

    move-object v10, v9

    move-object v4, v3

    move v9, v8

    move-object v3, v2

    move-object v8, v7

    move v2, v1

    move-object v7, v6

    move/from16 v6, v24

    move v1, v0

    move v0, v12

    move/from16 v24, v23

    move/from16 v23, v22

    move/from16 v12, v23

    move/from16 v22, v21

    move/from16 v21, v19

    move/from16 v19, v16

    move/from16 v16, v15

    move v15, v14

    move v14, v13

    move v13, v0

    :goto_18
    if-nez v11, :cond_2c

    if-lt v12, v0, :cond_2a

    iget-object v0, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->w:[I

    const/16 v12, 0xc0

    const/16 v20, 0x0

    aput v12, v0, v20

    if-eqz v11, :cond_29

    goto :goto_19

    :cond_29
    move-object/from16 p3, v8

    const/16 v17, 0x2

    goto :goto_1f

    :cond_2a
    :goto_19
    if-nez v11, :cond_2b

    aget v12, v10, v23

    move v0, v2

    goto :goto_1a

    :cond_2b
    move-object/from16 p3, v8

    move-object v0, v10

    move/from16 v8, v23

    const/16 v17, 0x2

    goto :goto_1e

    :cond_2c
    :goto_1a
    if-ge v12, v0, :cond_2f

    iget-object v0, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->w:[I

    aget v12, v10, v23

    move-object/from16 p3, v8

    if-nez v11, :cond_2e

    const/16 v8, 0x100

    if-ge v12, v8, :cond_2d

    const/4 v12, 0x0

    goto :goto_1b

    :cond_2d
    const/16 v8, 0x60

    goto :goto_1c

    :cond_2e
    :goto_1b
    move v8, v12

    :goto_1c
    int-to-byte v8, v8

    const/4 v12, 0x0

    aput v8, v0, v12

    iget-object v0, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->w:[I

    add-int/lit8 v8, v23, 0x1

    aget v12, v10, v23

    const/16 v17, 0x2

    aput v12, v0, v17

    move/from16 v23, v8

    if-eqz v11, :cond_30

    goto :goto_1d

    :cond_2f
    move-object/from16 p3, v8

    const/16 v17, 0x2

    :goto_1d
    iget-object v0, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->w:[I

    aget v8, v10, v23

    sub-int/2addr v8, v2

    aget v8, v5, v8

    add-int/lit8 v8, v8, 0x10

    add-int/lit8 v8, v8, 0x40

    int-to-byte v8, v8

    const/4 v12, 0x0

    aput v8, v0, v12

    move/from16 v8, v23

    move/from16 v23, v17

    :goto_1e
    add-int/lit8 v12, v8, 0x1

    aget v8, v10, v8

    sub-int/2addr v8, v2

    aget v8, v4, v8

    aput v8, v0, v23

    move/from16 v23, v12

    :cond_30
    :goto_1f
    sub-int v0, v16, v9

    const/4 v8, 0x1

    shl-int v0, v8, v0

    ushr-int v8, v22, v9

    :goto_20
    if-ge v8, v6, :cond_32

    iget-object v12, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->w:[I

    add-int v25, v24, v8

    move/from16 p4, v2

    move-object/from16 p2, v4

    const/4 v2, 0x3

    mul-int/lit8 v4, v25, 0x3

    move-object/from16 p5, v5

    const/4 v5, 0x0

    invoke-static {v12, v5, v7, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v8, v0

    if-nez v11, :cond_33

    if-eqz v11, :cond_31

    goto :goto_21

    :cond_31
    move-object/from16 v4, p2

    move/from16 v2, p4

    move-object/from16 v5, p5

    goto :goto_20

    :cond_32
    move/from16 p4, v2

    move-object/from16 p2, v4

    move-object/from16 p5, v5

    const/4 v2, 0x3

    const/4 v5, 0x0

    :goto_21
    add-int/lit8 v0, v16, -0x1

    const/4 v4, 0x1

    shl-int v8, v4, v0

    :cond_33
    and-int v0, v22, v8

    if-eqz v0, :cond_34

    xor-int v22, v22, v8

    ushr-int/lit8 v8, v8, 0x1

    if-nez v11, :cond_35

    if-eqz v11, :cond_33

    :cond_34
    xor-int v0, v22, v8

    move/from16 v22, v0

    :cond_35
    const/4 v4, 0x1

    shl-int v0, v4, v9

    sub-int/2addr v0, v4

    :cond_36
    and-int v0, v22, v0

    iget-object v8, v3, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->y:[I

    aget v8, v8, v19

    if-eq v0, v8, :cond_39

    add-int/lit8 v19, v19, -0x1

    sub-int/2addr v9, v1

    shl-int v0, v4, v9

    sub-int/2addr v0, v4

    if-nez v11, :cond_38

    if-nez v11, :cond_37

    if-eqz v11, :cond_36

    move v0, v1

    move-object v2, v3

    move-object v8, v7

    move v12, v9

    move/from16 v21, v22

    move/from16 v22, v23

    move/from16 v23, v24

    move-object/from16 v7, p1

    move-object/from16 v3, p2

    move-object/from16 v9, p3

    move/from16 v1, p4

    move/from16 v24, v6

    move-object/from16 v6, p5

    goto/16 :goto_22

    :cond_37
    move-object/from16 v4, p5

    move v0, v1

    move-object v2, v3

    move v12, v9

    move/from16 v21, v22

    move/from16 v22, v23

    move/from16 v23, v24

    move-object/from16 v3, p2

    move-object/from16 v9, p3

    move/from16 v1, p4

    move/from16 v24, v6

    move-object v6, v7

    move-object/from16 v7, p1

    goto/16 :goto_23

    :cond_38
    move-object/from16 v5, p1

    move-object/from16 v4, p5

    move v0, v1

    move-object v2, v3

    move v8, v9

    move-object v9, v10

    move-object v10, v11

    move v12, v13

    move v13, v14

    move v14, v15

    move/from16 v15, v16

    move/from16 v16, v19

    move/from16 v19, v21

    move/from16 v21, v22

    move/from16 v22, v23

    move/from16 v23, v24

    move-object/from16 v3, p2

    move/from16 v1, p4

    move/from16 v24, v6

    move-object v6, v7

    move-object/from16 v7, p3

    goto/16 :goto_17

    :cond_39
    move-object/from16 v5, p1

    move-object/from16 v4, p5

    move v0, v1

    move-object v2, v3

    move v12, v13

    move v13, v14

    move v14, v15

    move/from16 v15, v16

    move/from16 v16, v19

    move/from16 v8, v21

    move/from16 v21, v22

    move/from16 v22, v23

    move/from16 v23, v24

    move-object/from16 v3, p2

    move/from16 v1, p4

    move/from16 v24, v6

    move-object v6, v7

    move-object/from16 v7, p3

    move-object/from16 v26, v11

    move v11, v9

    move-object v9, v10

    move-object/from16 v10, v26

    goto/16 :goto_b

    :cond_3a
    move/from16 p1, v1

    move-object/from16 p3, v3

    move-object/from16 p4, v4

    move-object v1, v5

    move-object/from16 p6, v9

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v17, 0x2

    const/16 v18, -0x3

    move-object v8, v6

    move-object v9, v7

    move/from16 v19, v16

    move-object/from16 v6, p4

    move-object v7, v1

    move/from16 v16, v15

    move/from16 v1, p1

    move v15, v14

    move v14, v13

    move v13, v12

    move v12, v11

    move-object v11, v10

    move-object/from16 v10, p6

    :goto_22
    add-int/lit8 v16, v16, 0x1

    move-object v4, v6

    move-object v6, v8

    :goto_23
    if-eqz v11, :cond_3b

    move-object v10, v11

    move v13, v14

    move v14, v15

    goto :goto_24

    :cond_3b
    move-object v5, v7

    move-object v7, v9

    move-object v9, v10

    move-object v10, v11

    move v11, v12

    move v12, v13

    move v13, v14

    move v14, v15

    move/from16 v15, v16

    move/from16 v16, v19

    const/4 v8, 0x1

    move/from16 v19, v0

    move-object/from16 v0, p0

    goto/16 :goto_a

    :cond_3c
    const/4 v5, 0x0

    goto :goto_25

    :cond_3d
    const/4 v5, 0x0

    :goto_24
    move v8, v14

    :goto_25
    if-nez v10, :cond_3e

    if-eqz v8, :cond_3f

    move v8, v13

    :cond_3e
    if-nez v10, :cond_40

    const/4 v0, 0x1

    if-eq v8, v0, :cond_3f

    const/4 v0, -0x5

    move v2, v0

    goto :goto_26

    :cond_3f
    move v2, v5

    goto :goto_26

    :cond_40
    move v2, v8

    :goto_26
    return v2

    :cond_41
    move v5, v2

    const/16 v17, 0x2

    move-object/from16 v0, p0

    move v3, v2

    move v4, v3

    move v6, v4

    goto/16 :goto_9

    :cond_42
    const/16 v17, 0x2

    move-object/from16 v0, p0

    move v6, v4

    goto/16 :goto_8

    :cond_43
    move-object/from16 v0, p0

    goto/16 :goto_1

    :cond_44
    move/from16 v7, p3

    move-object/from16 v0, p0

    goto/16 :goto_0
.end method

.method static a([I[I[[I[[ILcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)I
    .locals 1

    const/16 p4, 0x9

    const/4 v0, 0x0

    aput p4, p0, v0

    const/4 p0, 0x5

    aput p0, p1, v0

    sget-object p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->m:[I

    aput-object p0, p2, v0

    sget-object p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->n:[I

    aput-object p0, p3, v0

    return v0
.end method

.method private a(I)V
    .locals 7

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->t:[I

    const/16 v2, 0xf

    const/4 v3, 0x3

    const/16 v4, 0x10

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->t:[I

    new-array v1, p1, [I

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->u:[I

    new-array v1, v4, [I

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->v:[I

    new-array v1, v3, [I

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->w:[I

    new-array v1, v2, [I

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->x:[I

    new-array v1, v4, [I

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->y:[I

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->u:[I

    :cond_1
    array-length v1, v1

    const/4 v5, 0x0

    if-nez v0, :cond_3

    if-ge v1, p1, :cond_2

    new-array v1, p1, [I

    iput-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->u:[I

    :cond_2
    move v1, v5

    :cond_3
    if-ge v1, p1, :cond_4

    iget-object v6, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->u:[I

    aput v5, v6, v1

    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_5

    if-eqz v0, :cond_3

    :cond_4
    move v1, v5

    :cond_5
    if-ge v1, v4, :cond_6

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->v:[I

    aput v5, p1, v1

    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_7

    if-eqz v0, :cond_5

    :cond_6
    move v1, v5

    :cond_7
    if-ge v1, v3, :cond_8

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->w:[I

    aput v5, p1, v1

    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_9

    if-eqz v0, :cond_7

    :cond_8
    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->v:[I

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->x:[I

    invoke-static {p1, v5, v0, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->v:[I

    iget-object v0, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->y:[I

    invoke-static {p1, v5, v0, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_9
    return-void
.end method


# virtual methods
.method a(II[I[I[I[I[I[ILcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)I
    .locals 19

    move-object/from16 v12, p0

    move-object/from16 v13, p9

    const/16 v14, 0x120

    invoke-direct {v12, v14}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->a(I)V

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v15

    iget-object v10, v12, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->t:[I

    const/16 v16, 0x0

    aput v16, v10, v16

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->o:[I

    sget-object v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->p:[I

    iget-object v11, v12, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->u:[I

    const/4 v2, 0x0

    const/16 v4, 0x101

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v3, p1

    move-object/from16 v7, p6

    move-object/from16 v8, p4

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->a([IIII[I[I[I[I[I[I[I)I

    move-result v0

    const/16 v17, -0x4

    const/4 v11, -0x3

    if-nez v15, :cond_d

    if-nez v0, :cond_d

    aget v1, p4, v16

    if-nez v15, :cond_1

    if-nez v1, :cond_0

    goto/16 :goto_8

    :cond_0
    invoke-direct {v12, v14}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->a(I)V

    const/4 v4, 0x0

    sget-object v5, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->q:[I

    sget-object v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->r:[I

    iget-object v10, v12, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->t:[I

    iget-object v14, v12, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->u:[I

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move/from16 v2, p1

    move/from16 v3, p2

    move-object/from16 v7, p7

    move-object/from16 v8, p5

    move-object/from16 v9, p8

    move v12, v11

    move-object v11, v14

    invoke-direct/range {v0 .. v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->a([IIII[I[I[I[I[I[I[I)I

    move-result v0

    move v1, v0

    goto :goto_0

    :cond_1
    move v12, v11

    :goto_0
    if-nez v15, :cond_6

    if-nez v1, :cond_5

    aget v1, p5, v16

    if-nez v15, :cond_3

    if-nez v1, :cond_4

    if-nez v15, :cond_2

    const/16 v1, 0x101

    move/from16 v2, p1

    if-le v2, v1, :cond_4

    goto :goto_2

    :cond_2
    move/from16 v2, p1

    move/from16 v16, v2

    goto :goto_1

    :cond_3
    move/from16 v16, v1

    :cond_4
    :goto_1
    return v16

    :cond_5
    :goto_2
    move v1, v0

    :cond_6
    if-nez v15, :cond_9

    if-ne v1, v12, :cond_8

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->z:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iput-object v1, v13, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    if-eqz v15, :cond_7

    goto :goto_3

    :cond_7
    move v11, v0

    goto :goto_7

    :cond_8
    :goto_3
    const/4 v11, -0x5

    move v1, v0

    goto :goto_4

    :cond_9
    move v11, v12

    :goto_4
    if-nez v15, :cond_b

    if-ne v1, v11, :cond_a

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->z:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    iput-object v0, v13, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    move v11, v12

    if-eqz v15, :cond_c

    goto :goto_5

    :cond_a
    move v11, v0

    :goto_5
    if-nez v15, :cond_c

    move v1, v11

    move/from16 v0, v17

    goto :goto_6

    :cond_b
    move/from16 v18, v11

    move v11, v0

    move/from16 v0, v18

    :goto_6
    if-eq v1, v0, :cond_c

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->z:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    iput-object v0, v13, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    move v11, v12

    :cond_c
    :goto_7
    return v11

    :cond_d
    :goto_8
    move v12, v11

    if-nez v15, :cond_f

    if-ne v0, v12, :cond_e

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->z:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    iput-object v1, v13, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    if-eqz v15, :cond_10

    :cond_e
    if-nez v15, :cond_10

    move/from16 v1, v17

    goto :goto_9

    :cond_f
    move v1, v12

    :goto_9
    if-eq v0, v1, :cond_10

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->z:[Ljava/lang/String;

    aget-object v0, v0, v16

    iput-object v0, v13, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    move v0, v12

    :cond_10
    return v0
.end method

.method a([I[I[I[ILcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;)I
    .locals 16

    move-object/from16 v12, p0

    move-object/from16 v13, p5

    const/16 v0, 0x13

    invoke-direct {v12, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->a(I)V

    iget-object v0, v12, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->t:[I

    const/4 v14, 0x0

    aput v14, v0, v14

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->b()[I

    move-result-object v15

    iget-object v10, v12, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->t:[I

    iget-object v11, v12, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->u:[I

    const/4 v2, 0x0

    const/16 v3, 0x13

    const/16 v4, 0x13

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v7, p3

    move-object/from16 v8, p2

    move-object/from16 v9, p4

    invoke-direct/range {v0 .. v11}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->a([IIII[I[I[I[I[I[I[I)I

    move-result v0

    const/4 v1, -0x3

    if-nez v15, :cond_1

    if-ne v0, v1, :cond_0

    sget-object v2, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->z:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    iput-object v2, v13, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    if-eqz v15, :cond_3

    :cond_0
    if-nez v15, :cond_3

    const/4 v2, -0x5

    goto :goto_0

    :cond_1
    move v2, v1

    :goto_0
    if-eq v0, v2, :cond_2

    aget v2, p2, v14

    if-nez v15, :cond_4

    if-nez v2, :cond_3

    :cond_2
    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/InfTree;->z:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v0, v0, v2

    iput-object v0, v13, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/zlib/ZStream;->msg:Ljava/lang/String;

    move v0, v1

    :cond_3
    move v2, v0

    :cond_4
    return v2
.end method
