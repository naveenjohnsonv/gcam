.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signatures;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, -0x1

    move v4, v0

    move v5, v2

    :goto_0
    const/16 v6, 0x5b

    const/4 v7, 0x1

    add-int/2addr v3, v7

    add-int/2addr v4, v3

    const-string v8, "Ae)\u0003We)\u0002@u"

    invoke-virtual {v8, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    array-length v9, v3

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v3}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v6, v5, 0x1

    aput-object v3, v1, v5

    const/16 v3, 0xa

    if-ge v4, v3, :cond_0

    invoke-virtual {v8, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signatures;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v3, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v13, 0x2

    if-eq v12, v13, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x7f

    goto :goto_2

    :cond_2
    const/4 v12, 0x6

    goto :goto_2

    :cond_3
    const/16 v12, 0x13

    goto :goto_2

    :cond_4
    const/16 v12, 0x27

    goto :goto_2

    :cond_5
    const/16 v12, 0x33

    goto :goto_2

    :cond_6
    const/16 v12, 0x6d

    goto :goto_2

    :cond_7
    const/16 v12, 0x5e

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v3, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init(Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;
    .locals 11

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->values()[Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_7

    aget-object v4, v0, v3

    iget-object v5, v4, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->algorithm:Ljava/lang/String;

    const/4 v6, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v7

    const/16 v8, 0x89e

    const/4 v9, 0x2

    const/4 v10, 0x1

    if-eq v7, v8, :cond_2

    const v8, 0x10992

    if-eq v7, v8, :cond_1

    const v8, 0x13e20

    if-eq v7, v8, :cond_0

    goto :goto_1

    :cond_0
    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signatures;->a:[Ljava/lang/String;

    aget-object v7, v7, v10

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v6, v2

    goto :goto_1

    :cond_1
    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signatures;->a:[Ljava/lang/String;

    aget-object v7, v7, v2

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v6, v10

    goto :goto_1

    :cond_2
    sget-object v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/Signatures;->a:[Ljava/lang/String;

    aget-object v7, v7, v9

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v6, v9

    :cond_3
    :goto_1
    if-eqz v6, :cond_6

    if-eq v6, v10, :cond_5

    if-eq v6, v9, :cond_4

    goto :goto_2

    :cond_4
    new-array v5, v10, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;

    iget-object v4, v4, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifier:Ljava/lang/String;

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatureCodec;

    invoke-direct {v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcSignatureCodec;-><init>()V

    invoke-direct {v6, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$EntryCodec;)V

    aput-object v6, v5, v2

    invoke-virtual {p0, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;

    goto :goto_2

    :cond_5
    new-array v5, v10, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;

    iget-object v4, v4, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifier:Ljava/lang/String;

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaSignatureCodec;

    invoke-direct {v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/DsaSignatureCodec;-><init>()V

    invoke-direct {v6, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$EntryCodec;)V

    aput-object v6, v5, v2

    invoke-virtual {p0, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;

    goto :goto_2

    :cond_6
    new-array v5, v10, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;

    iget-object v4, v4, Lcom/jscape/inet/ssh/protocol/v2/messages/KeyType;->identifier:Ljava/lang/String;

    new-instance v7, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/RsaSignatureCodec;

    invoke-direct {v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/RsaSignatureCodec;-><init>()V

    invoke-direct {v6, v4, v7}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$EntryCodec;)V

    aput-object v6, v5, v2

    invoke-virtual {p0, v5}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;->set([Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec$Entry;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/SignatureCodec;

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_7
    return-object p0
.end method
