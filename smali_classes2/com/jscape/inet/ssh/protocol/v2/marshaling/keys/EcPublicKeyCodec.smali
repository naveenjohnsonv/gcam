.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Ljava/security/PublicKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:I = 0x4

.field private static final c:[Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x1f

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "JB\u0002JB0Ln\r\u0011p\u000c\u0013|d\u0004AG*@\u007ft\u0002\rk\n@dd\u0019At\u0008\u000czd@\u0008qI\u000e`u@\u0012w\u0019\u0010`s\u0014\u0004fG"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x36

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move/from16 v16, v5

    move v5, v4

    move/from16 v4, v16

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    const/16 v14, 0x7f

    if-eqz v13, :cond_6

    if-eq v13, v8, :cond_5

    if-eq v13, v2, :cond_7

    if-eq v13, v0, :cond_4

    const/4 v15, 0x4

    if-eq v13, v15, :cond_3

    const/4 v15, 0x5

    if-eq v13, v15, :cond_2

    goto :goto_2

    :cond_2
    const/16 v14, 0x76

    goto :goto_2

    :cond_3
    const/16 v14, 0x1d

    goto :goto_2

    :cond_4
    const/16 v14, 0x7e

    goto :goto_2

    :cond_5
    const/16 v14, 0x1e

    goto :goto_2

    :cond_6
    const/16 v14, 0x10

    :cond_7
    :goto_2
    xor-int v13, v7, v14

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->b:Ljava/lang/Object;

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private static a([B)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    aget-byte p0, p0, v0

    const/4 v0, 0x4

    if-ne p0, v0, :cond_0

    return-void

    :cond_0
    new-instance p0, Ljava/io/IOException;

    sget-object v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->c:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
.end method

.method private static b([B)[B
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;->b()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    aget-byte v2, p0, v1

    if-nez v2, :cond_1

    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_2

    if-eqz v0, :cond_0

    :cond_1
    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    array-length v0, p0

    sub-int/2addr v0, v1

    new-array v0, v0, [B

    invoke-static {p0, v1, v0}, Lcom/jscape/util/v;->a([BI[B)[B

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static codecFor(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;
    .locals 1

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;

    invoke-direct {v0, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static codecFor(Ljava/security/Provider;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;
    .locals 1

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;

    invoke-direct {v0, p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static readValue(Ljava/lang/String;Ljava/lang/Object;Ljava/io/InputStream;)Ljava/security/PublicKey;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readValue(Ljava/io/InputStream;)[B

    move-result-object p2

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->a([B)V

    :try_start_0
    array-length v1, p2

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    new-array v3, v1, [B

    invoke-static {p2, v2, v3}, Lcom/jscape/util/v;->a([BI[B)[B

    move-result-object v3

    add-int/lit8 v4, v1, 0x1

    new-array v1, v1, [B

    invoke-static {p2, v4, v1}, Lcom/jscape/util/v;->a([BI[B)[B

    move-result-object p2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6

    :try_start_1
    instance-of v1, p1, Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_2
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->c:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v1, v1, v4

    move-object v4, p1

    check-cast v4, Ljava/lang/String;

    invoke-static {v1, v4}, Ljava/security/AlgorithmParameters;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/AlgorithmParameters;

    move-result-object v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    goto :goto_0

    :cond_0
    :try_start_3
    instance-of v1, p1, Ljava/security/Provider;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6

    :cond_1
    if-eqz v1, :cond_2

    :try_start_4
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->c:[Ljava/lang/String;

    aget-object v1, v1, v2

    move-object v4, p1

    check-cast v4, Ljava/security/Provider;

    invoke-static {v1, v4}, Ljava/security/AlgorithmParameters;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/AlgorithmParameters;

    move-result-object v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_5
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_2
    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->c:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/security/AlgorithmParameters;->getInstance(Ljava/lang/String;)Ljava/security/AlgorithmParameters;

    move-result-object v1

    :goto_0
    new-instance v4, Ljava/security/spec/ECGenParameterSpec;

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->oidFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v4, p0}, Ljava/security/spec/ECGenParameterSpec;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/security/AlgorithmParameters;->init(Ljava/security/spec/AlgorithmParameterSpec;)V

    const-class p0, Ljava/security/spec/ECParameterSpec;

    invoke-virtual {v1, p0}, Ljava/security/AlgorithmParameters;->getParameterSpec(Ljava/lang/Class;)Ljava/security/spec/AlgorithmParameterSpec;

    move-result-object p0

    check-cast p0, Ljava/security/spec/ECParameterSpec;

    new-instance v1, Ljava/security/spec/ECPoint;

    new-instance v4, Ljava/math/BigInteger;

    invoke-direct {v4, v2, v3}, Ljava/math/BigInteger;-><init>(I[B)V

    new-instance v3, Ljava/math/BigInteger;

    invoke-direct {v3, v2, p2}, Ljava/math/BigInteger;-><init>(I[B)V

    invoke-direct {v1, v4, v3}, Ljava/security/spec/ECPoint;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    new-instance p2, Ljava/security/spec/ECPublicKeySpec;

    invoke-direct {p2, v1, p0}, Ljava/security/spec/ECPublicKeySpec;-><init>(Ljava/security/spec/ECPoint;Ljava/security/spec/ECParameterSpec;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6

    :try_start_6
    instance-of p0, p1, Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    if-nez v0, :cond_4

    if-eqz p0, :cond_3

    :try_start_7
    sget-object p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->c:[Ljava/lang/String;

    aget-object p0, p0, v2

    check-cast p1, Ljava/lang/String;

    invoke-static {p0, p1}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object p0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    :cond_3
    :try_start_8
    instance-of p0, p1, Ljava/security/Provider;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    :cond_4
    if-eqz p0, :cond_5

    :try_start_9
    sget-object p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->c:[Ljava/lang/String;

    aget-object p0, p0, v2

    check-cast p1, Ljava/security/Provider;

    invoke-static {p0, p1}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyFactory;

    move-result-object p0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    goto :goto_1

    :catch_1
    move-exception p0

    :try_start_a
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_5
    sget-object p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->c:[Ljava/lang/String;

    aget-object p0, p0, v2

    invoke-static {p0}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object p0

    :goto_1
    invoke-virtual {p0, p2}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object p0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6

    if-eqz v0, :cond_6

    const/4 p1, 0x5

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_6
    return-object p0

    :catch_2
    move-exception p0

    :try_start_b
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3

    :catch_3
    move-exception p0

    :try_start_c
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_6

    :catch_4
    move-exception p0

    :try_start_d
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_5

    :catch_5
    move-exception p0

    :try_start_e
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_6

    :catch_6
    move-exception p0

    new-instance p1, Ljava/io/IOException;

    invoke-direct {p1, p0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw p1
.end method

.method public static writeValue(Ljava/security/PublicKey;Ljava/io/OutputStream;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p0, Ljava/security/interfaces/ECPublicKey;

    invoke-static {}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;->b()[Ljava/lang/String;

    invoke-interface {p0}, Ljava/security/interfaces/ECPublicKey;->getParams()Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/spec/ECParameterSpec;->getCurve()Ljava/security/spec/EllipticCurve;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/spec/EllipticCurve;->getField()Ljava/security/spec/ECField;

    move-result-object v0

    invoke-interface {v0}, Ljava/security/spec/ECField;->getFieldSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x7

    div-int/lit8 v0, v0, 0x8

    invoke-interface {p0}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object p0

    invoke-virtual {p0}, Ljava/security/spec/ECPoint;->getAffineX()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v1

    invoke-static {v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->b([B)[B

    move-result-object v1

    invoke-virtual {p0}, Ljava/security/spec/ECPoint;->getAffineY()Ljava/math/BigInteger;

    move-result-object p0

    invoke-virtual {p0}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->b([B)[B

    move-result-object p0

    mul-int/lit8 v2, v0, 0x2

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [B

    const/4 v3, 0x4

    const/4 v4, 0x0

    :try_start_0
    aput-byte v3, v2, v4

    add-int/lit8 v3, v0, 0x1

    array-length v5, v1

    sub-int v5, v3, v5

    array-length v6, v1

    invoke-static {v1, v4, v2, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v3, v0

    array-length v0, p0

    sub-int/2addr v3, v0

    array-length v0, p0

    invoke-static {p0, v4, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x2

    new-array p0, p0, [Ljava/lang/String;

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/PublicKeyCodec$Entry;->b([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
.end method


# virtual methods
.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->read(Ljava/io/InputStream;)Ljava/security/PublicKey;

    move-result-object p1

    return-object p1
.end method

.method public read(Ljava/io/InputStream;)Ljava/security/PublicKey;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->b:Ljava/lang/Object;

    invoke-static {v0, v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->readValue(Ljava/lang/String;Ljava/lang/Object;Ljava/io/InputStream;)Ljava/security/PublicKey;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Ljava/security/PublicKey;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->write(Ljava/security/PublicKey;Ljava/io/OutputStream;)V

    return-void
.end method

.method public write(Ljava/security/PublicKey;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/NamedEllipticCurveDomainParameters;->identifierFor(Ljava/security/Key;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUsAsciiValue(Ljava/lang/String;Ljava/io/OutputStream;)V

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/keys/EcPublicKeyCodec;->writeValue(Ljava/security/PublicKey;Ljava/io/OutputStream;)V

    return-void
.end method
