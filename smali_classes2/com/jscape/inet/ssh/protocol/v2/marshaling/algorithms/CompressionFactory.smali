.class public interface abstract Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;
.super Ljava/lang/Object;


# virtual methods
.method public abstract clientToServerCompressionFor(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$Mode;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$OperationException;
        }
    .end annotation
.end method

.method public abstract compressions()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract serverToClientCompressionFor(Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$Mode;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$OperationException;
        }
    .end annotation
.end method
