.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestShellCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$RequestCodec;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;IZ)Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestShell;

    invoke-direct {p1, p2, p3}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestShell;-><init>(IZ)V

    return-object p1
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method
