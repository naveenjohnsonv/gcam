.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestSignalCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/marshaling/SshMsgChannelRequestCodec$RequestCodec;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;IZ)Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUtf8Value(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    new-instance p3, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestSignal;

    invoke-direct {p3, p2, p1}, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestSignal;-><init>(ILjava/lang/String;)V

    return-object p3
.end method

.method public write(Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequest;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestSignal;

    iget-object p1, p1, Lcom/jscape/inet/ssh/protocol/v2/messages/SshMsgChannelRequestSignal;->signalName:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V

    return-void
.end method
