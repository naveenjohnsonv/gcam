.class public Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/ByteArrayCodec;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static readValue(ILjava/io/InputStream;)[B
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-array p0, p0, [B

    invoke-static {p0, p1}, Lcom/jscape/util/X;->a([BLjava/io/InputStream;)[B

    return-object p0
.end method

.method public static writeValue([BIILjava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p3, p0, p1, p2}, Ljava/io/OutputStream;->write([BII)V

    return-void
.end method

.method public static writeValue([BLjava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1, p0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method
