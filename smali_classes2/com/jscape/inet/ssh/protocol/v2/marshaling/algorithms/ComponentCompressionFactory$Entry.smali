.class public abstract Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;
.super Ljava/lang/Object;


# static fields
.field public static final NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "\u0011cgv"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry$1;

    invoke-direct {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry$1;-><init>(Ljava/lang/String;)V

    sput-object v1, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;->NONE:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x1b

    goto :goto_1

    :cond_1
    const/16 v4, 0x7d

    goto :goto_1

    :cond_2
    const/16 v4, 0x34

    goto :goto_1

    :cond_3
    const/16 v4, 0x35

    goto :goto_1

    :cond_4
    const/16 v4, 0x2f

    goto :goto_1

    :cond_5
    const/16 v4, 0x2a

    goto :goto_1

    :cond_6
    const/16 v4, 0x59

    :goto_1
    const/16 v5, 0x26

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;->name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public abstract clientToServerCompressionFor(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$Mode;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;
.end method

.method public abstract serverToClientCompressionFor(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory$Mode;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compression;
.end method
