.class public interface abstract Lcom/jscape/inet/ssh/protocol/transport/TransportServer;
.super Ljava/lang/Object;


# virtual methods
.method public abstract close()V
.end method

.method public abstract onMessage(Lcom/jscape/inet/ssh/protocol/messages/Message;)V
.end method

.method public abstract start(Lcom/jscape/inet/ssh/protocol/transport/TransportServer$Listener;Lcom/jscape/util/k/a/w;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/ssh/protocol/transport/TransportServer$Listener;",
            "Lcom/jscape/util/k/a/w<",
            "Lcom/jscape/inet/ssh/protocol/messages/Message;",
            ">;",
            "Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;",
            "Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;",
            ")V"
        }
    .end annotation
.end method
