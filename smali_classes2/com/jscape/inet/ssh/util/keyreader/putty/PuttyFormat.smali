.class public abstract Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;

.field private static final e:Ljava/lang/String;

.field private static final f:Ljava/lang/String;

.field private static final g:Ljava/lang/String; = ":"

.field private static final l:[Ljava/lang/String;


# instance fields
.field private final h:Lcom/jscape/inet/util/b;

.field private final i:Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "GlYS).;HlI\nGlYS).;HlI\u0007fbP\u001e*/=\u000cux_\u001f&\"didS\u0016<\u0014uxi\'\u0016l\u001cVhO^\u0004$0\u0008KT\u001f*l\ru\u007fT\u0005.5,\u0008AT\u001d*2\ru\u007fT\u0005.5,\u0008AT\u001d*2\nDhNAzwdFo^\u000bu\u007fT\u0005.5,\u0008@|0\n`c^\u000161=LbS\rGlYS.-.J\u007fT\u0007\',\n`c^\u000161=LbS\u0014uxi\'\u0016l\u001cVhO^\u0004$0\u0008KT\u001f*l\u000e@cX\u000b?$*QhYS\n\u000e\u000f\u0007fbP\u001e*/=\u000bu\u007fT\u0005.5,\u0008@|0"

    const/16 v5, 0xce

    move v8, v3

    const/4 v6, -0x1

    const/16 v7, 0xa

    :goto_0
    const/16 v9, 0x43

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/16 v15, 0xc

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x1e

    const-string v4, "R_x8\u0001\u0005CNCt1\u001b\u0011CoI{+$--du\u0004\t\u0002\nkD}"

    move v8, v11

    move v7, v15

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x64

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->l:[Ljava/lang/String;

    const/16 v1, 0x9

    aget-object v1, v0, v1

    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->b:Ljava/lang/String;

    const/16 v1, 0x8

    aget-object v1, v0, v1

    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->f:Ljava/lang/String;

    aget-object v1, v0, v15

    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->a:Ljava/lang/String;

    const/16 v1, 0xe

    aget-object v1, v0, v1

    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->c:Ljava/lang/String;

    const/4 v1, 0x6

    aget-object v1, v0, v1

    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->e:Ljava/lang/String;

    const/16 v1, 0x10

    aget-object v0, v0, v1

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->d:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v1, v14, 0x7

    const/4 v2, 0x2

    if-eqz v1, :cond_8

    if-eq v1, v10, :cond_7

    if-eq v1, v2, :cond_6

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    const/4 v2, 0x4

    if-eq v1, v2, :cond_9

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v15, 0xa

    goto :goto_4

    :cond_4
    const/4 v15, 0x2

    goto :goto_4

    :cond_5
    const/16 v15, 0x30

    goto :goto_4

    :cond_6
    const/16 v15, 0x7e

    goto :goto_4

    :cond_7
    const/16 v15, 0x4e

    goto :goto_4

    :cond_8
    const/16 v15, 0x66

    :cond_9
    :goto_4
    xor-int v1, v9, v15

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2
.end method

.method protected constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jscape/inet/util/b;

    invoke-direct {v0}, Lcom/jscape/inet/util/b;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->h:Lcom/jscape/inet/util/b;

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->i:Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->l:[Ljava/lang/String;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    const/16 v3, 0x11

    aget-object v1, v1, v3

    const/16 v3, 0x20

    invoke-virtual {v0, v2, v1, v3}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->add(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method private a([B)Lcom/jscape/inet/ssh/util/keyreader/putty/Record;
    .locals 10

    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/StringReader;

    new-instance v2, Ljava/lang/String;

    sget-object v3, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-direct {v2, p1, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-direct {v1, v2}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->l:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-direct {p0, v2, p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->getSshAlgorithm()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    const/16 v2, 0xa

    aget-object v2, v1, v2

    invoke-static {p1, v2}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object p1

    const/16 v2, 0xb

    aget-object v2, v1, v2

    invoke-direct {p0, v2, p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-direct {p0, v2, p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-direct {p0, v2, p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->h:Lcom/jscape/inet/util/b;

    invoke-direct {p0, v0, p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->a(Ljava/io/BufferedReader;I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/jscape/inet/util/b;->a([C)[B

    move-result-object v7

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-direct {p0, v2, p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->h:Lcom/jscape/inet/util/b;

    invoke-direct {p0, v0, p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->a(Ljava/io/BufferedReader;I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/jscape/inet/util/b;->a([C)[B

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object p1

    const/16 v0, 0xf

    aget-object v0, v1, v0

    invoke-direct {p0, v0, p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance p1, Lcom/jscape/inet/ssh/util/keyreader/putty/Record;

    move-object v3, p1

    invoke-direct/range {v3 .. v9}, Lcom/jscape/inet/ssh/util/keyreader/putty/Record;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->l:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/io/BufferedReader;I)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/putty/Record;->b()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    :cond_0
    if-ge v2, p2, :cond_3

    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_1

    if-nez v1, :cond_4

    if-eqz v3, :cond_2

    :try_start_0
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    if-nez v1, :cond_2

    add-int/lit8 v2, v2, 0x1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_2
    :try_start_1
    new-instance p1, Ljava/io/IOException;

    sget-object p2, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->l:[Ljava/lang/String;

    const/16 v0, 0xd

    aget-object p2, p2, v0

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_4
    return-object v3
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-virtual {p2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->l:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {p1, v0}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    const-string p1, ":"

    const/4 v0, 0x0

    invoke-static {p2, p1, v0}, Lcom/jscape/util/at;->a(Ljava/lang/String;Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object p1

    aget-object p1, p1, v1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/putty/Record;->b()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    move-object v0, p1

    goto :goto_0

    :cond_1
    move-object v0, p0

    :goto_0
    if-nez v0, :cond_2

    const/4 p1, 0x0

    return p1

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method protected abstract getSshAlgorithm()Ljava/lang/String;
.end method

.method protected abstract restoreKeyPair(Lcom/jscape/inet/ssh/util/keyreader/putty/Record;)Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public restoreKeyPair([BLjava/lang/String;)Ljava/security/KeyPair;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/util/keyreader/FormatException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->a([B)Lcom/jscape/inet/ssh/util/keyreader/putty/Record;

    move-result-object p1

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/Record;->isEncrypted()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/putty/KeyFactory;

    invoke-direct {v0, p2}, Lcom/jscape/inet/ssh/util/keyreader/putty/KeyFactory;-><init>(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->i:Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;

    invoke-virtual {p1, p2, v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/Record;->decrypt(Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Lcom/jscape/inet/ssh/util/keyreader/putty/Record;

    move-result-object p1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;->restoreKeyPair(Lcom/jscape/inet/ssh/util/keyreader/putty/Record;)Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;->toKeyPair()Ljava/security/KeyPair;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-virtual {p1}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;)V

    throw p2

    :catch_1
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;)V

    throw p2
.end method
