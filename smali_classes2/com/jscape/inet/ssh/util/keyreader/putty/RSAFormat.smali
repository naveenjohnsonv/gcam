.class public Lcom/jscape/inet/ssh/util/keyreader/putty/RSAFormat;
.super Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;


# static fields
.field private static final j:Ljava/lang/String;

.field private static final k:Ljava/lang/String;

.field private static final m:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x7

    const-string v5, "\u000b{\nb&bA\u0003*[#\u0007\u000b{\nb&bA"

    const/16 v6, 0x13

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x6a

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v2

    invoke-virtual {v5, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    const/4 v15, 0x2

    const/4 v3, 0x3

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v2

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v6, 0x1c

    const-string v5, "\u0016g\u001f\u00183F1\u001d\u000f\rw!M~\u0003\tDndU2\u0014\u0007_u0\\3"

    move v2, v3

    move v8, v11

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v2

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v11

    :goto_3
    const/16 v9, 0x56

    add-int/2addr v7, v10

    add-int v3, v7, v2

    invoke-virtual {v5, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/putty/RSAFormat;->m:[Ljava/lang/String;

    aget-object v0, v1, v15

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/putty/RSAFormat;->k:Ljava/lang/String;

    aget-object v0, v1, v3

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/putty/RSAFormat;->j:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v4, v14, 0x7

    if-eqz v4, :cond_9

    if-eq v4, v10, :cond_8

    if-eq v4, v15, :cond_7

    if-eq v4, v3, :cond_6

    const/4 v3, 0x4

    if-eq v4, v3, :cond_5

    if-eq v4, v0, :cond_4

    const/16 v3, 0x4a

    goto :goto_4

    :cond_4
    const/16 v3, 0x7b

    goto :goto_4

    :cond_5
    const/16 v3, 0x3e

    goto :goto_4

    :cond_6
    const/16 v3, 0x25

    goto :goto_4

    :cond_7
    const/16 v3, 0x8

    goto :goto_4

    :cond_8
    const/16 v3, 0x62

    goto :goto_4

    :cond_9
    const/16 v3, 0x12

    :goto_4
    xor-int/2addr v3, v9

    xor-int v3, v16, v3

    int-to-char v3, v3

    aput-char v3, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;-><init>()V

    return-void
.end method

.method private a([BLjava/security/spec/RSAPublicKeySpec;)Ljava/security/spec/RSAPrivateKeySpec;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object p1

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;->readMpint()Ljava/math/BigInteger;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;->readMpint()Ljava/math/BigInteger;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;->readMpint()Ljava/math/BigInteger;

    new-instance v0, Ljava/security/spec/RSAPrivateKeySpec;

    invoke-virtual {p2}, Ljava/security/spec/RSAPublicKeySpec;->getModulus()Ljava/math/BigInteger;

    move-result-object p2

    invoke-direct {v0, p2, p1}, Ljava/security/spec/RSAPrivateKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    return-object v0
.end method

.method private b([B)Ljava/security/spec/RSAPublicKeySpec;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;->readString()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/putty/RSAFormat;->m:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-static {p1, v1}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object p1

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object v0

    new-instance v1, Ljava/security/spec/RSAPublicKeySpec;

    invoke-direct {v1, v0, p1}, Ljava/security/spec/RSAPublicKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    return-object v1
.end method


# virtual methods
.method protected getSshAlgorithm()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/putty/RSAFormat;->m:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    return-object v0
.end method

.method protected restoreKeyPair(Lcom/jscape/inet/ssh/util/keyreader/putty/Record;)Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/Record;->getPublicKeyData()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/RSAFormat;->b([B)Ljava/security/spec/RSAPublicKeySpec;

    move-result-object v0

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/Record;->getPrivateKeyData()[B

    move-result-object p1

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/RSAFormat;->a([BLjava/security/spec/RSAPublicKeySpec;)Ljava/security/spec/RSAPrivateKeySpec;

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/putty/RSAFormat;->m:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-direct {v1, v2, v0, p1}, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;-><init>(Ljava/lang/String;Ljava/security/spec/KeySpec;Ljava/security/spec/KeySpec;)V

    return-object v1
.end method
