.class public Lcom/jscape/inet/ssh/util/keyreader/putty/DSAFormat;
.super Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;


# static fields
.field private static final j:Ljava/lang/String;

.field private static final k:Ljava/lang/String;

.field private static final m:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const-string v5, "\u0005X}[P\u001b\t\u0017S2EVR\u0010RK~RXI\u000b\u0006B\u007f\u0007\u0001Yz\u0018SH\u0011\u0007\u0001Yz\u0018SH\u0011"

    const/16 v6, 0x28

    const/4 v7, -0x1

    const/16 v8, 0x18

    const/4 v9, 0x0

    :goto_0
    const/16 v10, 0x63

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    const/4 v15, 0x0

    :goto_2
    const/4 v2, 0x2

    const/4 v3, 0x3

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_1

    add-int/lit8 v2, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v2

    goto :goto_0

    :cond_0
    const/4 v6, 0x7

    const-string v5, "M\u0002(\u0003M\u0002("

    move v9, v2

    move v8, v3

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v2

    move v9, v12

    :goto_3
    add-int/2addr v7, v11

    add-int v2, v7, v8

    invoke-virtual {v5, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/16 v10, 0x18

    const/4 v13, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/putty/DSAFormat;->m:[Ljava/lang/String;

    aget-object v0, v1, v2

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/putty/DSAFormat;->k:Ljava/lang/String;

    aget-object v0, v1, v3

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/putty/DSAFormat;->j:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v4, v15, 0x7

    if-eqz v4, :cond_9

    if-eq v4, v11, :cond_8

    if-eq v4, v2, :cond_7

    if-eq v4, v3, :cond_6

    const/4 v2, 0x4

    if-eq v4, v2, :cond_5

    if-eq v4, v0, :cond_4

    move v2, v11

    goto :goto_4

    :cond_4
    const/16 v2, 0x58

    goto :goto_4

    :cond_5
    const/16 v2, 0x54

    goto :goto_4

    :cond_6
    const/16 v2, 0x56

    goto :goto_4

    :cond_7
    const/16 v2, 0x71

    goto :goto_4

    :cond_8
    const/16 v2, 0x49

    goto :goto_4

    :cond_9
    const/16 v2, 0x11

    :goto_4
    xor-int/2addr v2, v10

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v12, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ssh/util/keyreader/putty/PuttyFormat;-><init>()V

    return-void
.end method


# virtual methods
.method protected getSshAlgorithm()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/putty/DSAFormat;->m:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method protected restoreKeyPair(Lcom/jscape/inet/ssh/util/keyreader/putty/Record;)Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/Record;->getPublicKeyData()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/DSAFormat;->restorePublicKeySpec([B)Ljava/security/spec/DSAPublicKeySpec;

    move-result-object v0

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/putty/Record;->getPrivateKeyData()[B

    move-result-object p1

    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/DSAFormat;->restorePrivateKeySpec([BLjava/security/spec/DSAPublicKeySpec;)Ljava/security/spec/DSAPrivateKeySpec;

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/putty/DSAFormat;->m:[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    invoke-direct {v1, v2, v0, p1}, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;-><init>(Ljava/lang/String;Ljava/security/spec/KeySpec;Ljava/security/spec/KeySpec;)V

    return-object v1
.end method

.method protected restorePrivateKeySpec([BLjava/security/spec/DSAPublicKeySpec;)Ljava/security/spec/DSAPrivateKeySpec;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object p1

    new-instance v0, Ljava/security/spec/DSAPrivateKeySpec;

    invoke-virtual {p2}, Ljava/security/spec/DSAPublicKeySpec;->getP()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {p2}, Ljava/security/spec/DSAPublicKeySpec;->getQ()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {p2}, Ljava/security/spec/DSAPublicKeySpec;->getG()Ljava/math/BigInteger;

    move-result-object p2

    invoke-direct {v0, p1, v1, v2, p2}, Ljava/security/spec/DSAPrivateKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    return-object v0
.end method

.method protected restorePublicKeySpec([B)Ljava/security/spec/DSAPublicKeySpec;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;->readString()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/putty/DSAFormat;->m:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {p1, v1}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object p1

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object v0

    new-instance v3, Ljava/security/spec/DSAPublicKeySpec;

    invoke-direct {v3, v0, p1, v1, v2}, Ljava/security/spec/DSAPublicKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    return-object v3
.end method
