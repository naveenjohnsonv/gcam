.class public Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;
.super Ljava/lang/Object;


# static fields
.field public static final NO_ENCRYPTION:Ljava/lang/String;

.field private static final e:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Map;

.field private final d:[B


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x4

    const-string v5, "\\H\u000c\n\u0005BK\u0003\u0006x\u000b\u0012L\u0007\u00166c\u0010[UNO"

    const/16 v6, 0x16

    move v8, v3

    const/4 v7, -0x1

    const/4 v9, 0x0

    :goto_0
    const/16 v10, 0x42

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/16 v13, 0x70

    const/4 v14, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v15, v12

    const/4 v2, 0x0

    :goto_2
    if-gt v15, v2, :cond_3

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v10, v9, 0x1

    if-eqz v14, :cond_1

    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v10

    goto :goto_0

    :cond_0
    const/16 v6, 0xe

    const/16 v2, 0x9

    const-string v5, "e{3/]Q7eq\u0004nz>8"

    move v8, v2

    move v9, v10

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v2

    move v9, v10

    :goto_3
    add-int/2addr v7, v11

    add-int v2, v7, v8

    invoke-virtual {v5, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v10, v13

    const/4 v14, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->e:[Ljava/lang/String;

    aget-object v0, v1, v3

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->NO_ENCRYPTION:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v12, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_9

    if-eq v4, v11, :cond_8

    const/4 v11, 0x2

    if-eq v4, v11, :cond_7

    const/4 v11, 0x3

    if-eq v4, v11, :cond_6

    if-eq v4, v3, :cond_5

    if-eq v4, v0, :cond_4

    const/16 v4, 0x33

    goto :goto_4

    :cond_4
    const/16 v4, 0x51

    goto :goto_4

    :cond_5
    const/16 v4, 0x54

    goto :goto_4

    :cond_6
    const/16 v4, 0x2d

    goto :goto_4

    :cond_7
    const/16 v4, 0x20

    goto :goto_4

    :cond_8
    const/16 v4, 0x65

    goto :goto_4

    :cond_9
    move v4, v13

    :goto_4
    xor-int/2addr v4, v10

    xor-int v4, v16, v4

    int-to-char v4, v4

    aput-char v4, v12, v2

    add-int/lit8 v2, v2, 0x1

    const/4 v11, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;[B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p3}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p4}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->b:Ljava/lang/String;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1, p3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->c:Ljava/util/Map;

    invoke-virtual {p4}, [B->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    check-cast p1, [B

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->d:[B

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public comments()Ljava/util/Iterator;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public decrypt(Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/util/keyreader/FormatException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->isEncrypted()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    return-object p0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->createDecipher(Ljava/lang/String;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Ljavax/crypto/Cipher;

    move-result-object p1

    iget-object p2, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->d:[B

    invoke-virtual {p1, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object p1

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->a:Ljava/lang/String;

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->e:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->c:Ljava/util/Map;

    invoke-direct {p2, v0, v1, v2, p1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;[B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-object p2

    :catch_1
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public encrypt(Ljava/lang/String;Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/util/keyreader/FormatException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p3}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    :try_start_0
    invoke-virtual {p2, p1, p3}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->createEncipher(Ljava/lang/String;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Ljavax/crypto/Cipher;

    move-result-object p2

    iget-object p3, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->d:[B

    invoke-virtual {p2, p3}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object p2

    new-instance p3, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->c:Ljava/util/Map;

    invoke-direct {p3, v0, p1, v1, p2}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p3

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public getAlgorithm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getComment(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public getEncryptionAlgorithm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyPairData()[B
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->d:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public isEncrypted()Z
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->b:Ljava/lang/String;

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->e:[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->e:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v3, v2, v3

    if-nez v0, :cond_1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->isEncrypted()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    aget-object v3, v2, v0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->e:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v0, v0, v2

    goto :goto_1

    :cond_1
    :goto_0
    move-object v0, v3

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
