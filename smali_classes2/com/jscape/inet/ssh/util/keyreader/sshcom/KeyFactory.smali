.class public Lcom/jscape/inet/ssh/util/keyreader/sshcom/KeyFactory;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:I = 0x10

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final c:[B


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, -0x1

    move v4, v0

    move v5, v2

    :goto_0
    const/16 v6, 0x5b

    const/4 v7, 0x1

    add-int/2addr v3, v7

    add-int/2addr v4, v3

    const-string v8, "-uL\u0003-uL\r\u0002P\u001d.\u0008r\u001d@B\u0010t\u00069"

    invoke-virtual {v8, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    array-length v9, v3

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v3}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v6, v5, 0x1

    aput-object v3, v1, v5

    const/16 v3, 0x15

    if-ge v4, v3, :cond_0

    invoke-virtual {v8, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/sshcom/KeyFactory;->d:[Ljava/lang/String;

    aget-object v0, v1, v2

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/KeyFactory;->a:Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v3, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v13, 0x2

    if-eq v12, v13, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x3f

    goto :goto_2

    :cond_2
    const/16 v12, 0x4c

    goto :goto_2

    :cond_3
    const/16 v12, 0x38

    goto :goto_2

    :cond_4
    const/16 v12, 0x55

    goto :goto_2

    :cond_5
    const/16 v12, 0x22

    goto :goto_2

    :cond_6
    const/16 v12, 0x6a

    goto :goto_2

    :cond_7
    const/16 v12, 0x3b

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v3, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/KeyFactory;->c:[B

    return-void
.end method

.method private a(I)I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->b()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x10

    if-nez v0, :cond_1

    if-le p1, v1, :cond_0

    goto :goto_0

    :cond_0
    move p1, v1

    :cond_1
    :goto_0
    rem-int/lit8 v1, p1, 0x10

    if-nez v0, :cond_3

    if-eqz v1, :cond_2

    rsub-int/lit8 v0, v1, 0x10

    add-int/2addr p1, v0

    :cond_2
    move v1, p1

    :cond_3
    return v1
.end method

.method private static a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public createIv(I)[B
    .locals 5

    int-to-long v0, p1

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/sshcom/KeyFactory;->d:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const-wide/16 v3, 0x0

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/w;->a(JJLjava/lang/String;)V

    new-array p1, p1, [B

    return-object p1
.end method

.method public createKey(Ljava/lang/String;I)Ljava/security/Key;
    .locals 8

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/sshcom/KeyFactory;->d:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    invoke-direct {p0, p2}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/KeyFactory;->a(I)I

    move-result v2

    new-array v3, v2, [B

    const/4 v4, 0x0

    move v5, v4

    :cond_0
    const/16 v6, 0x10

    add-int/lit8 v7, v2, -0x10

    if-gt v5, v7, :cond_1

    iget-object v7, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/KeyFactory;->c:[B

    invoke-virtual {v1, v7}, Ljava/security/MessageDigest;->update([B)V

    invoke-virtual {v1, v3, v4, v5}, Ljava/security/MessageDigest;->update([BII)V

    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v7

    invoke-static {v7, v4, v3, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v5, v5, 0x10

    if-eqz v0, :cond_0

    :cond_1
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v0, v3, v4, p2, p1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    new-instance p2, Ljava/lang/RuntimeException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public getPassphrase()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/KeyFactory;->c:[B

    sget-object v2, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v0
.end method
