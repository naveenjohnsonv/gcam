.class public abstract Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String; = ":"

.field private static final d:Ljava/lang/String; = "\\"

.field private static g:Ljava/lang/String;

.field private static final i:[Ljava/lang/String;


# instance fields
.field private final e:Lcom/jscape/inet/util/b;

.field private final f:Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xa

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->b(Ljava/lang/String;)V

    const/4 v4, 0x0

    const-string v5, "\u0008}@\u0005\u0019`5b\u0019#\u0008jq8\u0017p(fzp)u\u0004(l\u0019r\"l\u0006,||\u0002;`\tM\u0005\u0014\u000f](\u0008}@\u0005\u0019g>ap>{q\u0010P`\u001e.z`r$`\u0014Mxkk&d\u0004(\u0008rg)\u0005}@\u0005\u0014\u0014a\u0015>M]G_f\u0012.\u0007wM D4\tAWE\u0017p>\u001e]IR\u001fW$\u0008L\u0019I\u0015\\p\u000bGKO\u0011Q~\u000bg1\t\u0008_M\u0002H1\u0019\u0006(\u0008}@\u0005\u0019g>ap>{q\u0010P`\u001e.z`r$`\u0014Mxkk&d\u0004(\u0008rg)\u0005}@\u0005\u0014\u0008\u00164\u0008[\u0014A\u0012F*\u0008}@\u0005\u0019`5b\u0019#\u0008jq8\u0017p(fzp)u\u0004(l\u0019r\"l\u0006,||\u0002;`\tM\u0005\u0014\u000f]"

    const/16 v6, 0xe9

    move v9, v4

    const/4 v7, -0x1

    const/16 v8, 0x2a

    :goto_0
    const/16 v10, 0x8

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    move v15, v4

    :goto_2
    const/4 v0, 0x5

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_1

    add-int/lit8 v0, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v0

    const/16 v0, 0xa

    goto :goto_0

    :cond_0
    const/16 v6, 0x15

    const-string v5, "I?\u0007\u0006QC\u000cF?\u0017\nI?\u0007\u0006QC\u000cF?\u0017"

    move v9, v0

    const/4 v7, -0x1

    const/16 v8, 0xa

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v8, v0

    move v9, v12

    :goto_3
    const/4 v10, 0x6

    add-int/2addr v7, v11

    add-int v0, v7, v8

    invoke-virtual {v5, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v13, v4

    const/16 v0, 0xa

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->i:[Ljava/lang/String;

    aget-object v2, v1, v4

    sput-object v2, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->a:Ljava/lang/String;

    aget-object v0, v1, v0

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->b:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v2, v15, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v11, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    const/4 v3, 0x4

    if-eq v2, v3, :cond_5

    if-eq v2, v0, :cond_4

    const/16 v0, 0x78

    goto :goto_4

    :cond_4
    const/16 v0, 0x2a

    goto :goto_4

    :cond_5
    const/16 v0, 0x31

    goto :goto_4

    :cond_6
    const/16 v0, 0x20

    goto :goto_4

    :cond_7
    const/16 v0, 0x65

    goto :goto_4

    :cond_8
    const/16 v0, 0x58

    goto :goto_4

    :cond_9
    const/16 v0, 0x2d

    :goto_4
    xor-int/2addr v0, v10

    xor-int v0, v16, v0

    int-to-char v0, v0

    aput-char v0, v12, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method protected constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jscape/inet/util/b;

    invoke-direct {v0}, Lcom/jscape/inet/util/b;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->e:Lcom/jscape/inet/util/b;

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->f:Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->i:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    const/4 v3, 0x2

    aget-object v1, v1, v3

    const/16 v3, 0x18

    invoke-virtual {v0, v2, v1, v3}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->add(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method private a([Ljava/lang/String;Ljava/util/Map;)I
    .locals 7

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v2, p1, v1

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    move v4, v1

    :cond_0
    array-length v5, p1

    if-ge v4, v5, :cond_2

    if-nez v0, :cond_3

    if-nez v0, :cond_3

    if-ltz v2, :cond_2

    aget-object v5, p1, v4

    invoke-virtual {v5, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    :try_start_0
    aget-object v6, p1, v4

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ge v2, v6, :cond_1

    aget-object v6, p1, v4

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v6, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_1
    const-string v2, ""

    :goto_0
    invoke-interface {p2, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    aget-object v2, p1, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eqz v0, :cond_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    move v2, v4

    :cond_3
    return v2
.end method

.method private a([B)Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;
    .locals 5

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    new-instance v1, Ljava/lang/String;

    sget-object v2, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-direct {p0, v1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, v1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->a([Ljava/lang/String;Ljava/util/Map;)I

    move-result v2

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    :cond_0
    array-length v4, p1

    if-ge v2, v4, :cond_1

    aget-object v4, p1, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v2, v2, 0x1

    if-eqz v0, :cond_0

    :cond_1
    iget-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->e:Lcom/jscape/inet/util/b;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/jscape/inet/util/b;->a([C)[B

    move-result-object p1

    new-instance v2, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v2, v3}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->readInt()I

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->readInt()I

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->readString()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->getAlgorithm()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_3

    const/4 v0, -0x1

    if-eq v3, v0, :cond_2

    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    move v0, v3

    :goto_1
    :try_start_2
    sget-object v3, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->i:[Ljava/lang/String;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-static {v0, v3}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->readStringAsByteArray()[B

    move-result-object v2

    new-instance v3, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;

    invoke-direct {v3, p1, v0, v1, v2}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;[B)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    return-object v3

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->i:[Ljava/lang/String;

    const/16 v1, 0x8

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    if-nez p1, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    :try_start_0
    sget-object v3, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->i:[Ljava/lang/String;

    const/4 v4, 0x7

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez p1, :cond_2

    if-eqz v1, :cond_1

    move v1, v2

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_0
    sget-object v3, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->i:[Ljava/lang/String;

    const/4 v4, 0x4

    aget-object v3, v3, v4

    invoke-static {v1, v3}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    :cond_3
    if-eqz v4, :cond_7

    if-nez p1, :cond_8

    if-nez p1, :cond_6

    if-eqz v3, :cond_4

    :try_start_1
    const-string v5, "\\"

    invoke-virtual {v3, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    if-nez p1, :cond_5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    sub-int/2addr v5, v2

    invoke-interface {v1, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_4
    :goto_1
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    move-object v6, v4

    move-object v4, v3

    move-object v3, v6

    goto :goto_2

    :cond_6
    move-object v4, v3

    :goto_2
    if-eqz p1, :cond_3

    :cond_7
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p1

    sub-int/2addr p1, v2

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p1

    move-object v3, p1

    check-cast v3, Ljava/lang/String;

    :cond_8
    sget-object p1, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->i:[Ljava/lang/String;

    aget-object v0, p1, v2

    invoke-virtual {v3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const/16 v2, 0x9

    aget-object p1, p1, v2

    invoke-static {v0, p1}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p1

    new-array p1, p1, [Ljava/lang/String;

    invoke-interface {v1, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    check-cast p1, [Ljava/lang/String;

    return-object p1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->g:Ljava/lang/String;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->g:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    move-object v0, p1

    goto :goto_0

    :cond_1
    move-object v0, p0

    :goto_0
    if-nez v0, :cond_2

    const/4 p1, 0x0

    return p1

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method protected abstract getAlgorithm()Ljava/lang/String;
.end method

.method protected abstract restoreKeyPair(Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;)Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public restoreKeyPair([BLjava/lang/String;)Ljava/security/KeyPair;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/util/keyreader/FormatException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->a([B)Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;

    move-result-object p1

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->isEncrypted()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/KeyFactory;

    invoke-direct {v0, p2}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/KeyFactory;-><init>(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->f:Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;

    invoke-virtual {p1, p2, v0}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->decrypt(Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;

    move-result-object p1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->restoreKeyPair(Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;)Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;->toKeyPair()Ljava/security/KeyPair;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-virtual {p1}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;)V

    throw p2

    :catch_1
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;)V

    throw p2
.end method
