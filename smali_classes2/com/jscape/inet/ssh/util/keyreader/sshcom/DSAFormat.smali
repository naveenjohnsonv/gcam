.class public Lcom/jscape/inet/ssh/util/keyreader/sshcom/DSAFormat;
.super Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;


# static fields
.field private static final h:Ljava/lang/String;

.field private static final j:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x3

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v5, v4

    const-string v8, "%g\u0011\u0003%g\u0011"

    invoke-virtual {v8, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v3

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/4 v4, 0x7

    if-ge v5, v4, :cond_0

    invoke-virtual {v8, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DSAFormat;->j:[Ljava/lang/String;

    aget-object v0, v1, v3

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DSAFormat;->h:Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/4 v13, 0x4

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    if-eq v12, v2, :cond_4

    if-eq v12, v13, :cond_3

    const/4 v14, 0x5

    if-eq v12, v14, :cond_2

    const/16 v12, 0x6d

    goto :goto_2

    :cond_2
    const/16 v12, 0x4b

    goto :goto_2

    :cond_3
    const/16 v12, 0x7a

    goto :goto_2

    :cond_4
    const/16 v12, 0x61

    goto :goto_2

    :cond_5
    const/16 v12, 0x54

    goto :goto_2

    :cond_6
    const/16 v12, 0x30

    goto :goto_2

    :cond_7
    const/16 v12, 0x65

    :goto_2
    xor-int/2addr v12, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;-><init>()V

    return-void
.end method


# virtual methods
.method protected getAlgorithm()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DSAFormat;->j:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method protected restoreKeyPair(Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;)Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/Record;->getKeyPairData()[B

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;-><init>(Ljava/io/InputStream;)V

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->b()Ljava/lang/String;

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->readStringAsByteArray()[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->readMpint()Ljava/math/BigInteger;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object v1

    new-instance v5, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;

    sget-object v6, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DSAFormat;->j:[Ljava/lang/String;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    new-instance v7, Ljava/security/spec/DSAPublicKeySpec;

    invoke-direct {v7, v4, v0, v3, v2}, Ljava/security/spec/DSAPublicKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    new-instance v4, Ljava/security/spec/DSAPrivateKeySpec;

    invoke-direct {v4, v1, v0, v3, v2}, Ljava/security/spec/DSAPrivateKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    invoke-direct {v5, v6, v7, v4}, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;-><init>(Ljava/lang/String;Ljava/security/spec/KeySpec;Ljava/security/spec/KeySpec;)V

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object v5
.end method
