.class public Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;
.super Ljava/lang/Object;


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/io/DataInputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x20

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x17

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\u0013R\u007fX%*\t\u0007IfMq,\u0002UTa\u001dg(\u0002UKrNv9\u000e\u0007Z`X \u0013R\u007fX%*\t\u0007IfMq,\u0002UTa\u001dg(\u0002UKrNv9\u000e\u0007Z`X"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x41

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->b:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/4 v13, 0x4

    if-eqz v12, :cond_6

    if-eq v12, v7, :cond_5

    if-eq v12, v0, :cond_7

    const/4 v14, 0x3

    if-eq v12, v14, :cond_4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v13, 0x71

    goto :goto_2

    :cond_2
    const/16 v13, 0x5e

    goto :goto_2

    :cond_3
    const/16 v13, 0x12

    goto :goto_2

    :cond_4
    const/16 v13, 0x2a

    goto :goto_2

    :cond_5
    const/16 v13, 0x2c

    goto :goto_2

    :cond_6
    const/16 v13, 0x62

    :cond_7
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->a:Ljava/io/DataInputStream;

    return-void
.end method

.method private static a(Ljava/lang/NegativeArraySizeException;)Ljava/lang/NegativeArraySizeException;
    .locals 0

    return-object p0
.end method

.method private a([B)[B
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    :try_start_0
    array-length v1, p1
    :try_end_0
    .catch Ljava/lang/NegativeArraySizeException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x1

    if-lt v1, v2, :cond_2

    const/4 v1, 0x0

    :try_start_1
    aget-byte v3, p1, v1
    :try_end_1
    .catch Ljava/lang/NegativeArraySizeException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_1

    if-ltz v3, :cond_0

    goto :goto_0

    :cond_0
    array-length v0, p1

    add-int/lit8 v3, v0, 0x1

    :cond_1
    new-array v0, v3, [B

    array-length v3, p1

    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->a(Ljava/lang/NegativeArraySizeException;)Ljava/lang/NegativeArraySizeException;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/NegativeArraySizeException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->a(Ljava/lang/NegativeArraySizeException;)Ljava/lang/NegativeArraySizeException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/NegativeArraySizeException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->a(Ljava/lang/NegativeArraySizeException;)Ljava/lang/NegativeArraySizeException;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    return-object p1
.end method


# virtual methods
.method public readInt()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->a:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    return v0
.end method

.method public readMpint()Ljava/math/BigInteger;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->readInt()I

    move-result v0

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/SSHComFormat;->b()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x7

    div-int/lit8 v0, v0, 0x8

    const/4 v2, 0x1

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    :try_start_0
    new-instance v0, Ljava/math/BigInteger;

    new-array v1, v2, [B

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/NegativeArraySizeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->a(Ljava/lang/NegativeArraySizeException;)Ljava/lang/NegativeArraySizeException;

    move-result-object v0

    throw v0

    :cond_0
    :try_start_1
    new-array v0, v0, [B

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->a:Ljava/io/DataInputStream;

    invoke-virtual {v1, v0}, Ljava/io/DataInputStream;->readFully([B)V

    new-instance v1, Ljava/math/BigInteger;

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->a([B)[B

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/math/BigInteger;-><init>([B)V
    :try_end_1
    .catch Ljava/lang/NegativeArraySizeException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    return-object v1

    :catch_1
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->b:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_2
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->b:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public readString()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->readStringAsByteArray()[B

    move-result-object v1

    sget-object v2, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v0
.end method

.method public readStringAsByteArray()[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->a:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    new-array v1, v1, [B

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->a:Ljava/io/DataInputStream;

    invoke-virtual {v2, v1}, Ljava/io/DataInputStream;->readFully([B)V
    :try_end_0
    .catch Ljava/lang/NegativeArraySizeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    new-instance v1, Ljava/io/IOException;

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->b:[Ljava/lang/String;

    aget-object v0, v2, v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_1
    new-instance v1, Ljava/io/IOException;

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DataReader;->b:[Ljava/lang/String;

    aget-object v0, v2, v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
