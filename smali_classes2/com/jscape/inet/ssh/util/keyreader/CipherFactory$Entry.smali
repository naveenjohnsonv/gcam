.class public Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;
.super Ljava/lang/Object;


# static fields
.field private static final a:C = '/'

.field private static final f:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "mjC.]Np/xNtS"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->f:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x74

    goto :goto_1

    :cond_1
    const/16 v4, 0x56

    goto :goto_1

    :cond_2
    const/16 v4, 0x4b

    goto :goto_1

    :cond_3
    const/16 v4, 0x73

    goto :goto_1

    :cond_4
    const/16 v4, 0x5a

    goto :goto_1

    :cond_5
    const/16 v4, 0x76

    goto :goto_1

    :cond_6
    const/16 v4, 0x72

    :goto_1
    const/16 v5, 0x7d

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    int-to-long v0, p3

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->f:Ljava/lang/String;

    const-wide/16 v3, 0x0

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/w;->a(JJLjava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->b:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->d:Ljava/lang/String;

    iput p3, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->e:I

    const/16 p1, 0x2f

    invoke-virtual {p2, p1}, Ljava/lang/String;->indexOf(I)I

    move-result p1

    if-ltz p1, :cond_0

    iget-object p2, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->d:Ljava/lang/String;

    const/4 p3, 0x0

    invoke-virtual {p2, p3, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->d:Ljava/lang/String;

    :goto_0
    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->c:Ljava/lang/String;

    return-void
.end method

.method private static a(Ljava/security/NoSuchAlgorithmException;)Ljava/security/NoSuchAlgorithmException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public createCipher()Ljavax/crypto/Cipher;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljavax/crypto/NoSuchPaddingException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->d:Ljava/lang/String;

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    return-object v0
.end method

.method public createCipher(Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;I)Ljavax/crypto/Cipher;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljavax/crypto/NoSuchPaddingException;,
            Ljava/security/InvalidAlgorithmParameterException;,
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->d:Ljava/lang/String;

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;->b()[Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->c:Ljava/lang/String;

    iget v2, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->e:I

    invoke-interface {p1, v1, v2}, Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;->createKey(Ljava/lang/String;I)Ljava/security/Key;

    move-result-object v1

    :try_start_0
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v3

    invoke-interface {p1, v3}, Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;->createIv(I)[B

    move-result-object p1

    invoke-direct {v2, p1}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v0, p2, v1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/String;

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;->b([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-object v0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->a(Ljava/security/NoSuchAlgorithmException;)Ljava/security/NoSuchAlgorithmException;

    move-result-object p1

    throw p1
.end method

.method public getAlgorithm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getJceName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getKeySize()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->e:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->b:Ljava/lang/String;

    return-object v0
.end method
