.class public Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;

.field private static final c:[Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x14

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x13

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "If tV3-Kc\"yR%t\nn4xV%\u000eD`poF\"e\nl9l[$\u007f\u000eD`poF\"e\nl9l[$\u007f"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x32

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v13, 0x2

    if-eq v12, v13, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x1e

    goto :goto_2

    :cond_2
    const/16 v12, 0x52

    goto :goto_2

    :cond_3
    const/16 v12, 0x20

    goto :goto_2

    :cond_4
    const/16 v12, 0xf

    goto :goto_2

    :cond_5
    const/16 v12, 0x43

    goto :goto_2

    :cond_6
    const/16 v12, 0x1c

    goto :goto_2

    :cond_7
    const/16 v12, 0x39

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->b:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->b:Ljava/util/HashMap;

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->b:Ljava/util/HashMap;

    iget-object p1, p1, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;
    .locals 3

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->contains(Ljava/lang/String;)Z

    move-result v0

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->c:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->b:Ljava/util/HashMap;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;

    return-object p1
.end method

.method public static declared-synchronized getGlobalInstance()Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;
    .locals 3

    const-class v0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;

    monitor-enter v0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;->b()[Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->a:Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;

    if-nez v1, :cond_1

    if-nez v2, :cond_0

    new-instance v1, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;

    invoke-direct {v1}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;-><init>()V

    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->a:Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;

    :cond_0
    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->a:Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit v0

    return-object v2

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public add(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->contains(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->c:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->b:Ljava/util/HashMap;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;

    invoke-direct {v2, p1, p2, p3}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public contains(Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->b:Ljava/util/HashMap;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public createCipher(Ljava/lang/String;)Ljavax/crypto/Cipher;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljavax/crypto/NoSuchPaddingException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->a(Ljava/lang/String;)Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->createCipher()Ljavax/crypto/Cipher;

    move-result-object p1

    return-object p1
.end method

.method public createDecipher(Ljava/lang/String;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Ljavax/crypto/Cipher;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljavax/crypto/NoSuchPaddingException;,
            Ljava/security/InvalidAlgorithmParameterException;,
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->a(Ljava/lang/String;)Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;

    move-result-object p1

    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->createCipher(Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;I)Ljavax/crypto/Cipher;

    move-result-object p1

    return-object p1
.end method

.method public createEncipher(Ljava/lang/String;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Ljavax/crypto/Cipher;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljavax/crypto/NoSuchPaddingException;,
            Ljava/security/InvalidAlgorithmParameterException;,
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->a(Ljava/lang/String;)Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->createCipher(Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;I)Ljavax/crypto/Cipher;

    move-result-object p1

    return-object p1
.end method

.method public getAlgorithm(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->a(Ljava/lang/String;)Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->getAlgorithm()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getKeySize(Ljava/lang/String;)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->a(Ljava/lang/String;)Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory$Entry;->getKeySize()I

    move-result p1

    return p1
.end method

.method public remove(Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->contains(Ljava/lang/String;)Z

    move-result v0

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->c:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->b:Ljava/util/HashMap;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
