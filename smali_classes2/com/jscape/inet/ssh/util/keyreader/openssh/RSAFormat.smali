.class public Lcom/jscape/inet/ssh/util/keyreader/openssh/RSAFormat;
.super Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;


# static fields
.field private static final j:Ljava/lang/String;

.field private static final l:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x3

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x65

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "\r\u0011h\u0003\r\u0011h"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/4 v4, 0x7

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/openssh/RSAFormat;->l:[Ljava/lang/String;

    aget-object v0, v1, v8

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/openssh/RSAFormat;->j:Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    if-eqz v13, :cond_7

    if-eq v13, v8, :cond_6

    if-eq v13, v0, :cond_5

    if-eq v13, v2, :cond_4

    const/4 v14, 0x4

    if-eq v13, v14, :cond_3

    const/4 v14, 0x5

    if-eq v13, v14, :cond_2

    const/16 v13, 0x38

    goto :goto_2

    :cond_2
    move v13, v0

    goto :goto_2

    :cond_3
    const/16 v13, 0x2f

    goto :goto_2

    :cond_4
    const/16 v13, 0x6a

    goto :goto_2

    :cond_5
    const/16 v13, 0x4c

    goto :goto_2

    :cond_6
    const/16 v13, 0x27

    goto :goto_2

    :cond_7
    const/16 v13, 0x3a

    :goto_2
    xor-int/2addr v13, v7

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;-><init>()V

    return-void
.end method


# virtual methods
.method protected getAlgorithm()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/openssh/RSAFormat;->l:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method protected restoreKeyPair(Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;)Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->getKeyPairData()[B

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;-><init>(Ljava/io/InputStream;)V

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b()[Lcom/jscape/util/aq;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->readByte()I

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->readLength()I

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->readByte()I

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->readBigInteger()Ljava/math/BigInteger;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->readByte()I

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->readBigInteger()Ljava/math/BigInteger;

    move-result-object p1

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->readByte()I

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->readBigInteger()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->readByte()I

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->readBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    new-instance v2, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;

    sget-object v3, Lcom/jscape/inet/ssh/util/keyreader/openssh/RSAFormat;->l:[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    new-instance v4, Ljava/security/spec/RSAPublicKeySpec;

    invoke-direct {v4, p1, v1}, Ljava/security/spec/RSAPublicKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    new-instance v1, Ljava/security/spec/RSAPrivateKeySpec;

    invoke-direct {v1, p1, v0}, Ljava/security/spec/RSAPrivateKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    invoke-direct {v2, v3, v4, v1}, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;-><init>(Ljava/lang/String;Ljava/security/spec/KeySpec;Ljava/security/spec/KeySpec;)V

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x3

    new-array p1, p1, [Lcom/jscape/util/aq;

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b([Lcom/jscape/util/aq;)V

    :cond_0
    return-object v2
.end method
