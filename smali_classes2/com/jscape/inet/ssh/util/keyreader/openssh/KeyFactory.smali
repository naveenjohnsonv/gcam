.class public Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field private final a:[B

.field private final b:[B


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xd

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x49

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\\ C\u001cZOM\u001e2NFT\u0004\r\\ C\u001cZOM\u001e2NFT\u0004\u0003\u007f\u0004t"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x1f

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v13, 0x2

    if-eq v12, v13, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x7d

    goto :goto_2

    :cond_2
    const/16 v12, 0x63

    goto :goto_2

    :cond_3
    const/16 v12, 0x78

    goto :goto_2

    :cond_4
    const/16 v12, 0x75

    goto :goto_2

    :cond_5
    const/16 v12, 0x6e

    goto :goto_2

    :cond_6
    const/16 v12, 0x8

    goto :goto_2

    :cond_7
    const/16 v12, 0x77

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;->a:[B

    invoke-virtual {p2}, [B->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;->b:[B

    return-void
.end method

.method private a(II)I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    if-nez v0, :cond_1

    if-le p1, p2, :cond_0

    goto :goto_0

    :cond_0
    move p1, p2

    :cond_1
    :goto_0
    rem-int v1, p1, p2

    if-nez v0, :cond_3

    if-eqz v1, :cond_2

    sub-int/2addr p2, v1

    add-int/2addr p1, p2

    :cond_2
    move v1, p1

    :cond_3
    return v1
.end method


# virtual methods
.method public createIv(I)[B
    .locals 5

    int-to-long v0, p1

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;->c:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const-wide/16 v3, 0x0

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/w;->a(JJLjava/lang/String;)V

    new-array v0, p1, [B

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;->b:[B

    array-length v1, v1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;->b:[B

    const/4 v2, 0x0

    invoke-static {v1, v2, v0, v2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public createKey(Ljava/lang/String;I)Ljava/security/Key;
    .locals 9

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    int-to-long v1, p2

    sget-object v3, Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;->c:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v5, v3, v4

    const-wide/16 v6, 0x0

    invoke-static {v1, v2, v6, v7, v5}, Lcom/jscape/util/w;->a(JJLjava/lang/String;)V

    const/4 v1, 0x2

    aget-object v1, v3, v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/jscape/a/h;->d()Lcom/jscape/a/h;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;->a:[B

    invoke-interface {v0, v1}, Lcom/jscape/a/d;->a([B)Lcom/jscape/a/d;

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;->b:[B

    const/16 v2, 0x8

    invoke-interface {v0, v1, v4, v2}, Lcom/jscape/a/d;->a([BII)Lcom/jscape/a/d;

    invoke-interface {v0}, Lcom/jscape/a/d;->c()[B

    move-result-object v0

    invoke-static {v0, v4, p2}, Lcom/jscape/util/v;->a([BII)[B

    move-result-object p2

    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v0, p2, p1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v0

    :cond_0
    invoke-static {}, Lcom/jscape/a/h;->d()Lcom/jscape/a/h;

    move-result-object v1

    invoke-interface {v1}, Lcom/jscape/a/d;->a()I

    move-result v2

    invoke-direct {p0, p2, v2}, Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;->a(II)I

    move-result v3

    new-array v5, v3, [B

    move v6, v4

    :cond_1
    sub-int v7, v3, v2

    if-gt v6, v7, :cond_2

    iget-object v7, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;->a:[B

    invoke-interface {v1, v7}, Lcom/jscape/a/d;->a([B)Lcom/jscape/a/d;

    iget-object v7, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;->b:[B

    invoke-interface {v1, v7}, Lcom/jscape/a/d;->a([B)Lcom/jscape/a/d;

    invoke-interface {v1}, Lcom/jscape/a/d;->c()[B

    move-result-object v7

    array-length v8, v7

    invoke-static {v7, v4, v5, v6, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v8, v7

    invoke-interface {v1, v7, v4, v8}, Lcom/jscape/a/d;->a([BII)Lcom/jscape/a/d;

    add-int/2addr v6, v2

    if-eqz v0, :cond_1

    :cond_2
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v0, v5, v4, p2, p1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    return-object v0
.end method

.method public getIv()[B
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;->b:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public getPassphrase()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;->a:[B

    sget-object v2, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v0
.end method
