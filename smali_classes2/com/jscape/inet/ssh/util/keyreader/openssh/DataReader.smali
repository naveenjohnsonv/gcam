.class public Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;
.super Ljava/lang/Object;


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/io/DataInputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x20

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x30

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "eb*\u001eo9Aqy3\u000b;?J#d4[-;J#{\'\u0008<*Fqj5\u001e eb*\u001eo9Aqy3\u000b;?J#d4[-;J#{\'\u0008<*Fqj5\u001e"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x41

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->b:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x1e

    goto :goto_2

    :cond_2
    const/16 v12, 0x6a

    goto :goto_2

    :cond_3
    const/16 v12, 0x7f

    goto :goto_2

    :cond_4
    const/16 v12, 0x4b

    goto :goto_2

    :cond_5
    const/16 v12, 0x76

    goto :goto_2

    :cond_6
    const/16 v12, 0x3b

    goto :goto_2

    :cond_7
    const/16 v12, 0x33

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->a:Ljava/io/DataInputStream;

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public readBigInteger()Ljava/math/BigInteger;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->readLength()I

    move-result v0

    :try_start_0
    new-array v0, v0, [B

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->a:Ljava/io/DataInputStream;

    invoke-virtual {v1, v0}, Ljava/io/DataInputStream;->readFully([B)V

    new-instance v1, Ljava/math/BigInteger;

    invoke-direct {v1, v0}, Ljava/math/BigInteger;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/NegativeArraySizeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->b:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->b:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public readByte()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->a:Ljava/io/DataInputStream;

    invoke-virtual {v0}, Ljava/io/DataInputStream;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public readLength()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->readByte()I

    move-result v1

    and-int/lit16 v2, v1, 0x80

    if-nez v0, :cond_3

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    rem-int/lit16 v3, v1, 0x80

    :cond_0
    if-lez v3, :cond_1

    shl-int/lit8 v2, v2, 0x8

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/DataReader;->readByte()I

    move-result v4

    or-int/2addr v2, v4

    add-int/lit8 v3, v3, -0x1

    if-nez v0, :cond_2

    if-eqz v0, :cond_0

    :cond_1
    move v1, v2

    :cond_2
    move v2, v1

    :cond_3
    return v2
.end method
