.class public Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;
.super Ljava/lang/Object;


# static fields
.field public static final NO_ENCRYPTION:Ljava/lang/String;

.field private static f:[Lcom/jscape/util/aq;

.field private static final g:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:[B

.field private final e:[B


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b([Lcom/jscape/util/aq;)V

    const-string v4, "cwft3\tvudo$eCv\u007f\u000b3pbd}eVzi+="

    const/16 v5, 0x1b

    move v7, v0

    const/4 v6, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x33

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    const/4 v15, 0x3

    const/4 v2, 0x4

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v11, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x9

    const-string v4, "\u000c\u0005\u0018\t\u0004\u000c\u0005\u0018\t"

    move v7, v2

    move v8, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v2, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v2

    :goto_3
    const/16 v9, 0x42

    add-int/2addr v6, v10

    add-int v2, v6, v7

    invoke-virtual {v4, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->g:[Ljava/lang/String;

    aget-object v0, v1, v15

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->NO_ENCRYPTION:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v3, v14, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v10, :cond_8

    const/4 v10, 0x2

    if-eq v3, v10, :cond_7

    if-eq v3, v15, :cond_6

    if-eq v3, v2, :cond_5

    if-eq v3, v0, :cond_4

    goto :goto_4

    :cond_4
    const/16 v2, 0x26

    goto :goto_4

    :cond_5
    const/16 v2, 0x6e

    goto :goto_4

    :cond_6
    const/16 v2, 0x2e

    goto :goto_4

    :cond_7
    const/16 v2, 0x34

    goto :goto_4

    :cond_8
    const/16 v2, 0x28

    goto :goto_4

    :cond_9
    const/16 v2, 0x20

    :goto_4
    xor-int/2addr v2, v9

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v10, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p3}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    invoke-static {p4}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p5}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->c:Ljava/lang/String;

    invoke-virtual {p4}, [B->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    check-cast p1, [B

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->d:[B

    invoke-virtual {p5}, [B->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    check-cast p1, [B

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->e:[B

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static b([Lcom/jscape/util/aq;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->f:[Lcom/jscape/util/aq;

    return-void
.end method

.method public static b()[Lcom/jscape/util/aq;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->f:[Lcom/jscape/util/aq;

    return-object v0
.end method


# virtual methods
.method public decrypt(Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/util/keyreader/FormatException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->isEncrypted()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    return-object p0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->createDecipher(Ljava/lang/String;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Ljavax/crypto/Cipher;

    move-result-object p1

    iget-object p2, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->e:[B

    invoke-virtual {p1, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v5

    new-instance p1, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b:Ljava/lang/String;

    sget-object p2, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->g:[Ljava/lang/String;

    const/4 v0, 0x3

    aget-object v3, p2, v0

    iget-object v4, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->d:[B

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :catch_1
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public encrypt(Ljava/lang/String;Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/util/keyreader/FormatException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p3}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    :try_start_0
    invoke-virtual {p2, p1, p3}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->createEncipher(Ljava/lang/String;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Ljavax/crypto/Cipher;

    move-result-object p2

    iget-object p3, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->e:[B

    invoke-virtual {p2, p3}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v5

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->d:[B

    move-object v0, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p2

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public getAlgorithm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getEncryptionAlgorithm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getIv()[B
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->d:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public getKeyPairData()[B
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->e:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public getProcType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b:Ljava/lang/String;

    return-object v0
.end method

.method public isEncrypted()Z
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->c:Ljava/lang/String;

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->g:[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->g:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v3, v2, v3

    if-nez v0, :cond_1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->isEncrypted()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    aget-object v3, v2, v0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->g:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    goto :goto_1

    :cond_1
    :goto_0
    move-object v0, v3

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
