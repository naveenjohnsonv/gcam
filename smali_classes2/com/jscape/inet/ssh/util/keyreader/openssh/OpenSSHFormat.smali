.class public abstract Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String; = ":"

.field private static final e:Ljava/lang/String;

.field private static final f:Ljava/lang/String;

.field private static final g:Ljava/lang/String; = ","

.field private static final k:[Ljava/lang/String;


# instance fields
.field private final h:Lcom/jscape/inet/util/b;

.field private final i:Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x4

    const-string v4, "_(DJ\na5EL\u0019,tA\"\u0010\u0014u\u0002yJP\u001d\"r\u0005i\u0000z\u0017]P#NFZ\u001f\u0011\u0011\u0017xfb9YtgajmU \u001cj\u0007\nS&N\u000fR\u0017\u007f\\&^\u000cu\u0002y\u0002q<H\u0002jimw\tu\u0002a\u0002}\u0016k^}\t\u001cj\u0007\u0002\u0019=Cug\u000bp\u0002y\u0002\u0005J5\u001c\u0004hl\t\u001cj\u0007\u0002\u0019=Cug\nS&N\u000fR\u0017\u007f\\&^\na5EL\u0019,tA\"\u0010\u000bs&N\u000fR\u0017\u007f\\&^\u0001\u0011\u0011\u0017xfb9YtgajmU \u001cj\u0007\u000b\u001cj\u0007\u0002\u0019:Hv\u000ed\u000f\u0011p\u0002y\u0000w:N\u001e\tE\u007fU\u001ciX)M"

    const/16 v5, 0xca

    move v7, v1

    const/4 v6, -0x1

    const/4 v8, 0x0

    :goto_0
    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v7

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x7

    move v13, v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v14, v10

    const/4 v15, 0x0

    :goto_2
    const/4 v2, 0x3

    if-gt v14, v15, :cond_3

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v13}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v12, :cond_1

    add-int/lit8 v2, v8, 0x1

    aput-object v10, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v2

    goto :goto_0

    :cond_0
    const/16 v5, 0x15

    const/16 v4, 0xb

    const-string v6, "\u001dk\u0006\u0003\u0018;Iw\u000fe\u000e\tt\u0003`\u0003|\u0017j_|"

    move v8, v2

    move v7, v4

    move-object v4, v6

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v8, 0x1

    aput-object v10, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v7, v2

    move v8, v12

    :goto_3
    const/4 v13, 0x6

    add-int/2addr v6, v9

    add-int v2, v6, v7

    invoke-virtual {v4, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->k:[Ljava/lang/String;

    aget-object v1, v0, v11

    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->b:Ljava/lang/String;

    aget-object v1, v0, v2

    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->c:Ljava/lang/String;

    const/16 v1, 0x11

    aget-object v1, v0, v1

    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->f:Ljava/lang/String;

    const/16 v1, 0x10

    aget-object v1, v0, v1

    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a:Ljava/lang/String;

    aget-object v0, v0, v9

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->e:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v10, v15

    rem-int/lit8 v3, v15, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v9, :cond_8

    const/4 v9, 0x2

    if-eq v3, v9, :cond_7

    if-eq v3, v2, :cond_6

    if-eq v3, v1, :cond_5

    const/4 v2, 0x5

    if-eq v3, v2, :cond_4

    const/16 v2, 0xa

    goto :goto_4

    :cond_4
    const/16 v2, 0x7f

    goto :goto_4

    :cond_5
    const/16 v2, 0x33

    goto :goto_4

    :cond_6
    const/16 v2, 0x28

    goto :goto_4

    :cond_7
    const/16 v2, 0x2d

    goto :goto_4

    :cond_8
    const/16 v2, 0x40

    goto :goto_4

    :cond_9
    const/16 v2, 0x36

    :goto_4
    xor-int/2addr v2, v13

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v10, v15

    add-int/lit8 v15, v15, 0x1

    const/4 v9, 0x1

    goto/16 :goto_2
.end method

.method protected constructor <init>()V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jscape/inet/util/b;

    invoke-direct {v0}, Lcom/jscape/inet/util/b;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->h:Lcom/jscape/inet/util/b;

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->i:Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->k:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    const/4 v3, 0x2

    aget-object v3, v1, v3

    const/16 v4, 0x18

    invoke-virtual {v0, v2, v3, v4}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->add(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->i:Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    const/16 v3, 0xf

    aget-object v1, v1, v3

    const/16 v3, 0x10

    invoke-virtual {v0, v2, v1, v3}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->add(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method private a([B)Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;
    .locals 11

    const-string v0, ","

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b()[Lcom/jscape/util/aq;

    move-result-object v1

    :try_start_0
    new-instance v2, Ljava/lang/String;

    sget-object v3, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-direct {v2, p1, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-direct {p0, v2}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->k:[Ljava/lang/String;

    const/16 v3, 0xb

    aget-object v2, v2, v3

    invoke-direct {p0, v2, p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Lcom/jscape/util/at;->a(Ljava/lang/String;Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    const/4 v4, 0x2

    if-nez v1, :cond_1

    :try_start_1
    array-length v5, v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-lt v5, v4, :cond_0

    goto :goto_0

    :cond_0
    :try_start_2
    const-string v2, ""

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    aget-object v2, v2, v3

    :goto_1
    move-object v7, v2

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->k:[Ljava/lang/String;

    const/4 v5, 0x6

    aget-object v2, v2, v5

    invoke-direct {p0, v2, p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0, v3}, Lcom/jscape/util/at;->a(Ljava/lang/String;Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    if-nez v1, :cond_3

    :try_start_3
    array-length v2, v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    if-lt v2, v4, :cond_2

    goto :goto_2

    :cond_2
    :try_start_4
    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->k:[Ljava/lang/String;

    aget-object v2, v2, v3

    goto :goto_3

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_2
    aget-object v2, v0, v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    :goto_3
    move-object v8, v2

    :try_start_5
    array-length v2, v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    if-nez v1, :cond_4

    if-lt v2, v4, :cond_5

    const/4 v1, 0x1

    :try_start_6
    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->b(Ljava/lang/String;)[B

    move-result-object v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_4

    :cond_4
    move v3, v2

    :cond_5
    :try_start_7
    new-array v0, v3, [B

    :goto_4
    move-object v9, v0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a([Ljava/lang/String;)[B

    move-result-object v10

    new-instance p1, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->getAlgorithm()Ljava/lang/String;

    move-result-object v6

    move-object v5, p1

    invoke-direct/range {v5 .. v10}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[B)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    return-object p1

    :catch_2
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3

    :catch_3
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    :catch_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->k:[Ljava/lang/String;

    const/16 v1, 0xa

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    array-length v2, p2

    if-ge v1, v2, :cond_3

    :try_start_0
    aget-object v2, p2, v1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_4

    if-nez v0, :cond_2

    :try_start_1
    invoke-virtual {v2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v2, :cond_1

    :try_start_2
    aget-object v2, p2, v1
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_2
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {v2, p1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    const-string v2, ""

    :cond_4
    return-object v2
.end method

.method private a([Ljava/lang/String;)[B
    .locals 6

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b()[Lcom/jscape/util/aq;

    move-result-object v1

    const/4 v2, 0x0

    :cond_0
    array-length v3, p1

    if-ge v2, v3, :cond_2

    :try_start_0
    aget-object v3, p1, v2

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_1

    if-nez v1, :cond_1

    if-ltz v3, :cond_2

    add-int/lit8 v2, v2, 0x1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    move-object v4, p0

    goto :goto_2

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    move-object v3, p0

    :goto_1
    move-object v4, v3

    move v3, v2

    :goto_2
    :try_start_3
    array-length v5, p1

    if-ge v3, v5, :cond_4

    aget-object v3, p1, v2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    add-int/lit8 v2, v2, 0x1

    if-eqz v1, :cond_3

    goto :goto_3

    :cond_3
    move-object v3, v4

    goto :goto_1

    :cond_4
    :goto_3
    iget-object p1, v4, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->h:Lcom/jscape/inet/util/b;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jscape/inet/util/b;->a([C)[B

    move-result-object p1

    return-object p1

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b()[Lcom/jscape/util/aq;

    move-result-object p1

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    if-nez p1, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->k:[Ljava/lang/String;

    const/16 v5, 0xe

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->getAlgorithm()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->k:[Ljava/lang/String;

    const/16 v5, 0xd

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_2

    if-eqz v1, :cond_1

    move v1, v2

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_0
    sget-object v3, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->k:[Ljava/lang/String;

    const/16 v4, 0xc

    aget-object v3, v3, v4

    invoke-static {v1, v3}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    :cond_3
    if-eqz v3, :cond_4

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-nez p1, :cond_5

    if-eqz p1, :cond_3

    :cond_4
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p1

    sub-int/2addr p1, v2

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->k:[Ljava/lang/String;

    const/16 v3, 0x9

    aget-object v3, v2, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->getAlgorithm()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x3

    aget-object v3, v2, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    const/4 v0, 0x4

    aget-object v0, v2, v0

    invoke-static {p1, v0}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    :cond_5
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p1

    new-array p1, p1, [Ljava/lang/String;

    invoke-interface {v1, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    check-cast p1, [Ljava/lang/String;

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private b(Ljava/lang/String;)[B
    .locals 6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    new-array v0, v0, [B

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b()[Lcom/jscape/util/aq;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_1

    if-nez v1, :cond_1

    :try_start_0
    div-int/lit8 v3, v2, 0x2

    add-int/lit8 v4, v2, 0x2

    invoke-virtual {p1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/16 v5, 0x10

    invoke-static {v2, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v0, v3
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    move v2, v4

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    if-nez v0, :cond_1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    move-object v0, p1

    goto :goto_0

    :cond_1
    move-object v0, p0

    :goto_0
    if-nez v0, :cond_2

    const/4 p1, 0x0

    return p1

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method protected abstract getAlgorithm()Ljava/lang/String;
.end method

.method protected abstract restoreKeyPair(Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;)Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public restoreKeyPair([BLjava/lang/String;)Ljava/security/KeyPair;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/util/keyreader/FormatException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->a([B)Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;

    move-result-object p1

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->isEncrypted()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->getIv()[B

    move-result-object v1

    invoke-direct {v0, p2, v1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/KeyFactory;-><init>(Ljava/lang/String;[B)V

    iget-object p2, p0, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->i:Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;

    invoke-virtual {p1, p2, v0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;->decrypt(Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;

    move-result-object p1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/openssh/OpenSSHFormat;->restoreKeyPair(Lcom/jscape/inet/ssh/util/keyreader/openssh/Record;)Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;->toKeyPair()Ljava/security/KeyPair;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-virtual {p1}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;)V

    throw p2

    :catch_1
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;)V

    throw p2
.end method
