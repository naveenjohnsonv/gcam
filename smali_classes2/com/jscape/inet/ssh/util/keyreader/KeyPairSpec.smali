.class public Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/security/spec/KeySpec;

.field private c:Ljava/security/spec/KeySpec;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/security/spec/KeySpec;Ljava/security/spec/KeySpec;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;->b:Ljava/security/spec/KeySpec;

    iput-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;->c:Ljava/security/spec/KeySpec;

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p3}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;->b:Ljava/security/spec/KeySpec;

    iput-object p3, p0, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;->c:Ljava/security/spec/KeySpec;

    return-void
.end method


# virtual methods
.method public getAlgorithm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getPrivateSpec()Ljava/security/spec/KeySpec;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;->c:Ljava/security/spec/KeySpec;

    return-object v0
.end method

.method public getPublicSpec()Ljava/security/spec/KeySpec;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;->b:Ljava/security/spec/KeySpec;

    return-object v0
.end method

.method public toKeyPair()Ljava/security/KeyPair;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/util/keyreader/FormatException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/spec/InvalidKeySpecException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;->b:Ljava/security/spec/KeySpec;

    invoke-virtual {v0, v1}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;->c:Ljava/security/spec/KeySpec;

    invoke-virtual {v0, v2}, Ljava/security/KeyFactory;->generatePrivate(Ljava/security/spec/KeySpec;)Ljava/security/PrivateKey;

    move-result-object v0

    new-instance v2, Ljava/security/KeyPair;

    invoke-direct {v2, v1, v0}, Ljava/security/KeyPair;-><init>(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V

    return-object v2
.end method
