.class public Lcom/jscape/inet/ssh/util/keyreader/Pkcs8KeyPairFormat;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public restoreKeyPair([BLjava/lang/String;)Ljava/security/KeyPair;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/util/keyreader/FormatException;
        }
    .end annotation

    :try_start_0
    new-instance p2, Lcom/jscape/util/h/P;

    invoke-direct {p2, p1}, Lcom/jscape/util/h/P;-><init>([B)V

    const/4 p1, 0x2

    new-array p1, p1, [Lcom/jscape/util/h/K;

    const/4 v0, 0x0

    invoke-static {}, Lcom/jscape/util/i/f;->b()Lcom/jscape/util/i/f;

    move-result-object v1

    aput-object v1, p1, v0

    const/4 v0, 0x1

    invoke-static {}, Lcom/jscape/util/i/i;->b()Lcom/jscape/util/i/i;

    move-result-object v1

    aput-object v1, p1, v0

    invoke-static {p2, p1}, Lcom/jscape/util/h/M;->a(Lcom/jscape/util/h/L;[Lcom/jscape/util/h/K;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/security/KeyPair;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method
