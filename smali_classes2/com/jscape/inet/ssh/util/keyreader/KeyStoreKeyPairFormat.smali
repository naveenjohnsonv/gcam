.class public Lcom/jscape/inet/ssh/util/keyreader/KeyStoreKeyPairFormat;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/security/Provider;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/security/Provider;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/KeyStoreKeyPairFormat;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ssh/util/keyreader/KeyStoreKeyPairFormat;->b:Ljava/security/Provider;

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/security/KeyStore;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-nez v0, :cond_3

    :try_start_0
    invoke-virtual {p1, v2}, Ljava/security/KeyStore;->isKeyEntry(Ljava/lang/String;)Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_1

    return-object v2

    :cond_1
    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/KeyStoreKeyPairFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    const-string v2, ""

    :cond_3
    return-object v2
.end method


# virtual methods
.method public restoreKeyPair([BLjava/lang/String;)Ljava/security/KeyPair;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/util/keyreader/FormatException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;->b()[Ljava/lang/String;

    move-result-object v0

    :try_start_0
    new-instance v1, Lcom/jscape/util/h/e;

    invoke-direct {v1, p1}, Lcom/jscape/util/h/e;-><init>([B)V

    iget-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/KeyStoreKeyPairFormat;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/keyreader/KeyStoreKeyPairFormat;->b:Ljava/security/Provider;

    invoke-static {v1, p1, p2, v2}, Lcom/jscape/util/i/b;->a(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyStore;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/KeyStoreKeyPairFormat;->a(Ljava/security/KeyStore;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object p2

    invoke-virtual {p1, v1, p2}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object p2

    check-cast p2, Ljava/security/PrivateKey;

    invoke-virtual {p1, v1}, Ljava/security/KeyStore;->getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;

    move-result-object p1

    const/4 v1, 0x0

    aget-object p1, p1, v1

    invoke-virtual {p1}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object p1

    new-instance v1, Ljava/security/KeyPair;

    invoke-direct {v1, p1, p2}, Ljava/security/KeyPair;-><init>(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    const/4 p1, 0x3

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object v1

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method
