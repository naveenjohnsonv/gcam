.class public final Lcom/jscape/inet/ssh/util/keyreader/vandyke/DSAFormat;
.super Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;


# static fields
.field private static final f:Ljava/lang/String;

.field private static final g:Ljava/lang/String;

.field private static final i:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x3

    const/4 v3, 0x0

    const-string v4, "Cf\u0004\u0018pG* tdxbLe>r-a\'T))|6zs](\u0003Cf\u0004\u0018pG* tdxbLe>r-a\'T))|6zs]("

    const/16 v5, 0x39

    move v7, v1

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x1f

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x4

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0xf

    const/4 v4, 0x7

    const-string v6, "&\u0014\u007f1%e2\u0007&\u0014\u007f1%e2"

    move v7, v4

    move-object v4, v6

    move v8, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x4d

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DSAFormat;->i:[Ljava/lang/String;

    aget-object v1, v0, v3

    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DSAFormat;->f:Ljava/lang/String;

    aget-object v0, v0, v15

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DSAFormat;->g:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v2, v14, 0x7

    const/16 v17, 0xc

    if-eqz v2, :cond_8

    if-eq v2, v10, :cond_7

    const/4 v3, 0x2

    if-eq v2, v3, :cond_6

    if-eq v2, v1, :cond_5

    if-eq v2, v15, :cond_9

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    goto :goto_4

    :cond_4
    const/16 v17, 0x5b

    goto :goto_4

    :cond_5
    const/16 v17, 0x51

    goto :goto_4

    :cond_6
    const/16 v17, 0x5a

    goto :goto_4

    :cond_7
    const/16 v17, 0x2a

    goto :goto_4

    :cond_8
    const/16 v17, 0x18

    :cond_9
    :goto_4
    xor-int v2, v9, v17

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v3, 0x0

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;-><init>()V

    return-void
.end method

.method private a([BLjava/security/spec/DSAPublicKeySpec;)Ljava/security/spec/DSAPrivateKeySpec;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readString()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DSAFormat;->i:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-static {p1, v1}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readMpint()Ljava/math/BigInteger;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readMpint()Ljava/math/BigInteger;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readMpint()Ljava/math/BigInteger;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object p1

    new-instance v0, Ljava/security/spec/DSAPrivateKeySpec;

    invoke-virtual {p2}, Ljava/security/spec/DSAPublicKeySpec;->getP()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {p2}, Ljava/security/spec/DSAPublicKeySpec;->getQ()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {p2}, Ljava/security/spec/DSAPublicKeySpec;->getG()Ljava/math/BigInteger;

    move-result-object p2

    invoke-direct {v0, p1, v1, v2, p2}, Ljava/security/spec/DSAPrivateKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    return-object v0
.end method

.method private b([B)Ljava/security/spec/DSAPublicKeySpec;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readString()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DSAFormat;->i:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {p1, v1}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object p1

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object v0

    new-instance v3, Ljava/security/spec/DSAPublicKeySpec;

    invoke-direct {v3, v0, p1, v1, v2}, Ljava/security/spec/DSAPublicKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    return-object v3
.end method


# virtual methods
.method protected getAlgorithm()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DSAFormat;->i:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    return-object v0
.end method

.method protected restoreKeyPair(Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;)Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->getPublicKeyData()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DSAFormat;->b([B)Ljava/security/spec/DSAPublicKeySpec;

    move-result-object v0

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->getPrivateKeyData()[B

    move-result-object p1

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DSAFormat;->a([BLjava/security/spec/DSAPublicKeySpec;)Ljava/security/spec/DSAPrivateKeySpec;

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DSAFormat;->i:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-direct {v1, v2, v0, p1}, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;-><init>(Ljava/lang/String;Ljava/security/spec/KeySpec;Ljava/security/spec/KeySpec;)V

    return-object v1
.end method
