.class public final Lcom/jscape/inet/ssh/util/keyreader/vandyke/RSAFormat;
.super Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;


# static fields
.field private static final f:Ljava/lang/String;

.field private static final g:Ljava/lang/String;

.field private static final i:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x7

    const/4 v3, 0x0

    const-string v4, "?\u0017E\u0016AH\u0008\u0007?\u0017E\u0016AH\u0008\u0018;\u0016BUT\u001b\u0002)\u001d\rKRR\u001bl\u0005A\\\\I\u00008\u000c@\u0003\u001e7l"

    const/16 v5, 0x2c

    move v7, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x76

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v1

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v3

    :goto_2
    const/4 v14, 0x4

    const/4 v15, 0x3

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v1

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v5, 0x1c

    const-string v4, "Kb9\u0018nC\u0017\u0000\u0001NW|HX\u001e\u0007\u0007N9P\u0014\t\t\u001cUmY\u0015"

    move v7, v10

    move v1, v15

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v1

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v10

    :goto_3
    const/16 v8, 0x23

    add-int/2addr v6, v9

    add-int v10, v6, v1

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/RSAFormat;->i:[Ljava/lang/String;

    aget-object v1, v0, v3

    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/vandyke/RSAFormat;->g:Ljava/lang/String;

    aget-object v0, v0, v14

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/RSAFormat;->f:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v10, v13

    rem-int/lit8 v2, v13, 0x7

    const/16 v17, 0x4d

    if-eqz v2, :cond_7

    if-eq v2, v9, :cond_6

    const/4 v3, 0x2

    if-eq v2, v3, :cond_5

    if-eq v2, v15, :cond_8

    if-eq v2, v14, :cond_4

    const/4 v3, 0x5

    if-eq v2, v3, :cond_8

    const/16 v17, 0x1f

    goto :goto_4

    :cond_4
    const/16 v17, 0x45

    goto :goto_4

    :cond_5
    const/16 v17, 0x5b

    goto :goto_4

    :cond_6
    const/16 v17, 0x12

    goto :goto_4

    :cond_7
    const/16 v17, 0x3a

    :cond_8
    :goto_4
    xor-int v2, v8, v17

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v10, v13

    add-int/lit8 v13, v13, 0x1

    const/4 v3, 0x0

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;-><init>()V

    return-void
.end method

.method private b([B)Ljava/security/spec/RSAPublicKeySpec;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readString()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/vandyke/RSAFormat;->i:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-static {p1, v1}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object p1

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object v0

    new-instance v1, Ljava/security/spec/RSAPublicKeySpec;

    invoke-direct {v1, v0, p1}, Ljava/security/spec/RSAPublicKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    return-object v1
.end method

.method private c([B)Ljava/security/spec/RSAPrivateKeySpec;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readString()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/jscape/inet/ssh/util/keyreader/vandyke/RSAFormat;->i:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-static {p1, v1}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object p1

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readMpint()Ljava/math/BigInteger;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readMpint()Ljava/math/BigInteger;

    move-result-object v0

    new-instance v1, Ljava/security/spec/RSAPrivateKeySpec;

    invoke-direct {v1, p1, v0}, Ljava/security/spec/RSAPrivateKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    return-object v1
.end method


# virtual methods
.method protected getAlgorithm()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/RSAFormat;->i:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    return-object v0
.end method

.method protected restoreKeyPair(Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;)Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->getPublicKeyData()[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/RSAFormat;->b([B)Ljava/security/spec/RSAPublicKeySpec;

    move-result-object v0

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->getPrivateKeyData()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/RSAFormat;->c([B)Ljava/security/spec/RSAPrivateKeySpec;

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/vandyke/RSAFormat;->i:[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    invoke-direct {v1, v2, v0, p1}, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;-><init>(Ljava/lang/String;Ljava/security/spec/KeySpec;Ljava/security/spec/KeySpec;)V

    return-object v1
.end method
