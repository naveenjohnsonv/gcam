.class public abstract Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String; = ":"

.field private static e:Ljava/lang/String;

.field private static final h:[Ljava/lang/String;


# instance fields
.field private final d:Lcom/jscape/inet/util/b;


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "ru24Tc"

    invoke-static {v1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->b(Ljava/lang/String;)V

    const/4 v2, 0x0

    const-string v3, "w-\u001e\u0018\u001cF\u0003X-\u000e\u0016)\u0018aW\u0015Zk4r\u00054\u0018,h?\u0015\u0008#s?\t\"f\u0004H\u0018*{8c\r.}Zb4llW\u0015W\u0004\'\u0018aW\u0015Zl?ql,y4\t5l\u0007?\u0018)z9\u0007l*j3\u007f0a\tZs?pQ\u0018aW\u0015)\u0018aW\u0015Zk4r\u00054\u0018,h?\u0015\u0008#s?\t\"f\u0004H\u0018*{8c\r.}Zb4llW\u0015W\u0004\nW-\u001e\u0018\u001cF\u0003X-\u000e\u001cV-\u0014V\u0015]QG)\tL\u0015[\u0014\u0015)\u0019J\u0003Y\u0005P(ZS\u001fP\u0002"

    const/16 v4, 0xaf

    const/16 v5, 0xb

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x5e

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    const/4 v14, 0x3

    const/4 v15, 0x2

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x32

    const/16 v3, 0x27

    const-string v5, "n\u0017!c,\u001aI\u0007\u001aZ\u000fB\u007fC\u001aqIn_\u000cOq\u001a\\\u001cE\tF\u0017\u007f,\u0005I\u0006\'n\u0017!c\n![hnj0u.[x"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v18, v5

    move v5, v3

    move-object/from16 v3, v18

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x28

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->h:[Ljava/lang/String;

    aget-object v1, v0, v14

    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->a:Ljava/lang/String;

    aget-object v0, v0, v15

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->b:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v10, v13

    rem-int/lit8 v1, v13, 0x7

    const/16 v17, 0x24

    if-eqz v1, :cond_7

    if-eq v1, v9, :cond_6

    if-eq v1, v15, :cond_8

    if-eq v1, v14, :cond_5

    const/4 v14, 0x4

    if-eq v1, v14, :cond_8

    const/4 v14, 0x5

    if-eq v1, v14, :cond_4

    const/16 v17, 0x2f

    goto :goto_4

    :cond_4
    const/16 v17, 0x77

    goto :goto_4

    :cond_5
    const/16 v17, 0x66

    goto :goto_4

    :cond_6
    const/16 v17, 0x12

    goto :goto_4

    :cond_7
    const/16 v17, 0x6b

    :cond_8
    :goto_4
    xor-int v1, v8, v17

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jscape/inet/util/b;

    invoke-direct {v0}, Lcom/jscape/inet/util/b;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->d:Lcom/jscape/inet/util/b;

    return-void
.end method

.method private a([Ljava/lang/String;Ljava/util/Map;)I
    .locals 7

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v2, p1, v1

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    move v4, v1

    :cond_0
    array-length v5, p1

    if-ge v4, v5, :cond_2

    if-eqz v0, :cond_3

    if-eqz v0, :cond_3

    if-ltz v2, :cond_2

    aget-object v5, p1, v4

    invoke-virtual {v5, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    :try_start_0
    aget-object v6, p1, v4

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ge v2, v6, :cond_1

    aget-object v6, p1, v4

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v6, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_1
    const-string v2, ""

    :goto_0
    invoke-interface {p2, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    aget-object v2, p1, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-nez v0, :cond_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    move v2, v4

    :cond_3
    return v2
.end method

.method private a([B)Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;
    .locals 8

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    :try_start_0
    new-instance v1, Ljava/lang/String;

    sget-object v2, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-direct {v1, p1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-direct {p0, v1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, v4}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->a([Ljava/lang/String;Ljava/util/Map;)I

    move-result v1

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    :cond_0
    array-length v3, p1

    if-ge v1, v3, :cond_1

    aget-object v3, p1, v1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_0

    :cond_1
    iget-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->d:Lcom/jscape/inet/util/b;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jscape/inet/util/b;->a([C)[B

    move-result-object p1

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readInt()I

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readStringAsByteArray()[B

    move-result-object v5

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readStringAsByteArray()[B

    move-result-object v6

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DataReader;->readStringAsByteArray()[B

    move-result-object v7

    new-instance p1, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->getAlgorithm()Ljava/lang/String;

    move-result-object v2

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;[B[B[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->h:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz p1, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    :try_start_0
    sget-object v4, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->h:[Ljava/lang/String;

    aget-object v4, v4, v3

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_2

    if-eqz v1, :cond_1

    move v1, v3

    goto :goto_0

    :cond_1
    move v1, v2

    :cond_2
    :goto_0
    sget-object v4, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->h:[Ljava/lang/String;

    aget-object v2, v4, v2

    invoke-static {v1, v2}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    :cond_3
    if-eqz v2, :cond_4

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz p1, :cond_5

    if-nez p1, :cond_3

    :cond_4
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p1

    sub-int/2addr p1, v3

    invoke-interface {v1, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->h:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v2, v0, v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    const/4 v2, 0x7

    aget-object v0, v0, v2

    invoke-static {p1, v0}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    :cond_5
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p1

    new-array p1, p1, [Ljava/lang/String;

    invoke-interface {v1, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    check-cast p1, [Ljava/lang/String;

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->e:Ljava/lang/String;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    move-object v0, p1

    goto :goto_0

    :cond_1
    move-object v0, p0

    :goto_0
    if-nez v0, :cond_2

    const/4 p1, 0x0

    return p1

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method protected abstract getAlgorithm()Ljava/lang/String;
.end method

.method protected abstract restoreKeyPair(Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;)Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public restoreKeyPair([BLjava/lang/String;)Ljava/security/KeyPair;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/util/keyreader/FormatException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->a([B)Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;

    move-result-object p1

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->isEncrypted()Z

    move-result p2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p2, :cond_0

    :try_start_1
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->restoreKeyPair(Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;)Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/ssh/util/keyreader/KeyPairSpec;->toKeyPair()Ljava/security/KeyPair;

    move-result-object p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_0

    return-object p1

    :cond_0
    :try_start_2
    new-instance p1, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    sget-object p2, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->h:[Ljava/lang/String;

    const/4 v0, 0x5

    aget-object p2, p2, v0

    invoke-direct {p1, p2}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/security/GeneralSecurityException; {:try_start_3 .. :try_end_3} :catch_0

    :goto_0
    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-virtual {p1}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;)V

    throw p2

    :catch_2
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;)V

    throw p2
.end method
