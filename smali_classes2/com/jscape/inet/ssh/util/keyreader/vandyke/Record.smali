.class public Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;
.super Ljava/lang/Object;


# static fields
.field public static final NO_ENCRYPTION:Ljava/lang/String;

.field private static final g:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Map;

.field private final d:[B

.field private final e:[B

.field private final f:[B


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x4

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x1d

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "f\u000fT[\u0004f\u000fT["

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x9

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move/from16 v16, v5

    move v5, v4

    move/from16 v4, v16

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->g:[Ljava/lang/String;

    aget-object v0, v1, v8

    sput-object v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->NO_ENCRYPTION:Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    const/16 v14, 0x15

    if-eqz v13, :cond_6

    if-eq v13, v8, :cond_5

    if-eq v13, v0, :cond_4

    const/4 v15, 0x3

    if-eq v13, v15, :cond_3

    if-eq v13, v2, :cond_2

    const/4 v15, 0x5

    if-eq v13, v15, :cond_6

    move v14, v8

    goto :goto_2

    :cond_2
    const/16 v14, 0x1b

    goto :goto_2

    :cond_3
    const/16 v14, 0x23

    goto :goto_2

    :cond_4
    const/16 v14, 0x27

    goto :goto_2

    :cond_5
    const/16 v14, 0x7d

    :cond_6
    :goto_2
    xor-int v13, v7, v14

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;[B[B[B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p3}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p4}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p5}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p6}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->b:Ljava/lang/String;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1, p3}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->c:Ljava/util/Map;

    invoke-virtual {p4}, [B->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    check-cast p1, [B

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->d:[B

    invoke-virtual {p5}, [B->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    check-cast p1, [B

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->e:[B

    invoke-virtual {p6}, [B->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    check-cast p1, [B

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->f:[B

    if-nez v0, :cond_0

    const/4 p1, 0x2

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public comments()Ljava/util/Iterator;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public decrypt(Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/util/keyreader/FormatException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->isEncrypted()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    return-object p0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->createDecipher(Ljava/lang/String;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Ljavax/crypto/Cipher;

    move-result-object p1

    iget-object p2, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->e:[B

    invoke-virtual {p1, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v5

    new-instance p1, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->a:Ljava/lang/String;

    sget-object p2, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->g:[Ljava/lang/String;

    const/4 v0, 0x1

    aget-object v2, p2, v0

    iget-object v3, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->c:Ljava/util/Map;

    iget-object v4, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->d:[B

    iget-object v6, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->f:[B

    move-object v0, p1

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;[B[B[B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :catch_1
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public encrypt(Ljava/lang/String;Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/util/keyreader/FormatException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->b()Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p3}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    :try_start_0
    invoke-virtual {p2, p1, p3}, Lcom/jscape/inet/ssh/util/keyreader/CipherFactory;->createEncipher(Ljava/lang/String;Lcom/jscape/inet/ssh/util/keyreader/IKeyFactory;)Ljavax/crypto/Cipher;

    move-result-object p2

    iget-object p3, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->e:[B

    invoke-virtual {p2, p3}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v5

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->c:Ljava/util/Map;

    iget-object v4, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->d:[B

    iget-object v6, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->f:[B

    move-object v0, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;[B[B[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_0

    const-string p1, "L7FyYb"

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->b(Ljava/lang/String;)V

    :cond_0
    return-object p2

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public getAlgorithm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getComment(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public getEncryptionAlgorithm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getMac()[B
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->f:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public getPrivateKeyData()[B
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->e:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public getPublicKeyData()[B
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->d:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public isEncrypted()Z
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/VanDykeFormat;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->b:Ljava/lang/String;

    sget-object v2, Lcom/jscape/inet/ssh/util/keyreader/vandyke/Record;->g:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    const/4 v0, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v3, v1

    :cond_1
    :goto_0
    return v3
.end method
