.class public Lcom/jscape/inet/ssh/util/KeyPairAssembler;
.super Ljava/lang/Object;


# static fields
.field private static final a:I = 0x80

.field private static final c:[Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "-$# GB?*\'#(GRfk*5)CR\u0003\u0001\u0000\u0002"

    const/16 v5, 0x18

    const/16 v6, 0x14

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x6f

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x1e

    const/4 v4, 0x6

    const-string v6, "MVDHAR\u0017~|iu\u001f\u0014ioxto\u001f\u0012,=vbbP\u0010(to"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v17, v6

    move v6, v4

    move-object/from16 v4, v17

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x39

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->c:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    const/16 v16, 0x24

    if-eqz v2, :cond_8

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v16, 0x70

    goto :goto_4

    :cond_4
    const/16 v16, 0x59

    goto :goto_4

    :cond_5
    const/16 v16, 0x49

    goto :goto_4

    :cond_6
    const/16 v16, 0x22

    goto :goto_4

    :cond_7
    const/16 v16, 0x3e

    :cond_8
    :goto_4
    xor-int v2, v9, v16

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->b:Ljava/util/List;

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/openssh/RSAFormat;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/RSAFormat;-><init>()V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->addFormat(Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;)V

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/openssh/DSAFormat;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/util/keyreader/openssh/DSAFormat;-><init>()V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->addFormat(Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;)V

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/putty/RSAFormat;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/RSAFormat;-><init>()V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->addFormat(Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;)V

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/putty/DSAFormat;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/util/keyreader/putty/DSAFormat;-><init>()V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->addFormat(Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;)V

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/RSAFormat;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/RSAFormat;-><init>()V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->addFormat(Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;)V

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DSAFormat;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/util/keyreader/sshcom/DSAFormat;-><init>()V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->addFormat(Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;)V

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/RSAFormat;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/RSAFormat;-><init>()V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->addFormat(Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;)V

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DSAFormat;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/util/keyreader/vandyke/DSAFormat;-><init>()V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->addFormat(Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;)V

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/Pkcs8KeyPairFormat;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/util/keyreader/Pkcs8KeyPairFormat;-><init>()V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->addFormat(Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;)V

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/KeyStoreKeyPairFormat;

    sget-object v1, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->c:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/jscape/inet/ssh/util/keyreader/KeyStoreKeyPairFormat;-><init>(Ljava/lang/String;Ljava/security/Provider;)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->addFormat(Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;)V

    new-instance v0, Lcom/jscape/inet/ssh/util/keyreader/KeyStoreKeyPairFormat;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/jscape/inet/ssh/util/keyreader/KeyStoreKeyPairFormat;-><init>(Ljava/lang/String;Ljava/security/Provider;)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->addFormat(Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;)V

    return-void
.end method

.method private static a(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public addFormat(Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;)V
    .locals 3

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->containsFormat(Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    sget-object v1, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->c:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public containsFormat(Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;)Z
    .locals 1

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public removeFormat(Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public restoreKeyPair(Ljava/io/File;Ljava/lang/String;)Ljava/security/KeyPair;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/nio/file/OpenOption;

    invoke-static {p1, v0}, Ljava/nio/file/Files;->newInputStream(Ljava/nio/file/Path;[Ljava/nio/file/OpenOption;)Ljava/io/InputStream;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->b()Z

    move-result v0

    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->restoreKeyPair(Ljava/io/InputStream;Ljava/lang/String;)Ljava/security/KeyPair;

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz p1, :cond_0

    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    return-object p2

    :catchall_1
    move-exception p2

    :try_start_2
    throw p2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :catchall_2
    move-exception v1

    if-eqz p1, :cond_1

    :try_start_3
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v2

    :try_start_4
    invoke-virtual {p2, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    goto :goto_1

    :catchall_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    throw v1
.end method

.method public restoreKeyPair(Ljava/io/InputStream;Ljava/lang/String;)Ljava/security/KeyPair;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/util/keyreader/FormatException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->c()Z

    move-result v0

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v2, 0x80

    new-array v2, v2, [B

    invoke-virtual {p1, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    :cond_0
    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    invoke-virtual {p1, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-eqz v0, :cond_0

    :cond_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->restoreKeyPair([BLjava/lang/String;)Ljava/security/KeyPair;

    move-result-object p1

    return-object p1
.end method

.method public restoreKeyPair(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyPair;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/util/keyreader/FormatException;
        }
    .end annotation

    :try_start_0
    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->restoreKeyPair([BLjava/lang/String;)Ljava/security/KeyPair;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/ssh/util/keyreader/FormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    throw p1
.end method

.method public restoreKeyPair([BLjava/lang/String;)Ljava/security/KeyPair;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/util/keyreader/FormatException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->b()Z

    move-result v0

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :cond_0
    if-eqz v0, :cond_1

    :try_start_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2
    :try_end_0
    .catch Lcom/jscape/inet/ssh/util/keyreader/FormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    move-object v2, v1

    :goto_0
    check-cast v2, Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;

    :try_start_1
    invoke-interface {v2, p1, p2}, Lcom/jscape/inet/ssh/util/keyreader/KeyPairFormat;->restoreKeyPair([BLjava/lang/String;)Ljava/security/KeyPair;

    move-result-object p1
    :try_end_1
    .catch Lcom/jscape/inet/ssh/util/keyreader/FormatException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :catch_1
    move-exception v2

    :try_start_2
    new-instance v3, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4
    :try_end_2
    .catch Lcom/jscape/inet/ssh/util/keyreader/FormatException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-direct {v3, v4, v2}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v2, v3

    goto :goto_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catch_3
    move-exception v2

    :goto_2
    if-nez v0, :cond_0

    :cond_3
    if-eqz v0, :cond_5

    if-eqz v2, :cond_4

    goto :goto_3

    :cond_4
    new-instance v2, Lcom/jscape/inet/ssh/util/keyreader/FormatException;

    sget-object p1, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->c:[Ljava/lang/String;

    const/4 p2, 0x3

    aget-object p1, p1, p2

    invoke-direct {v2, p1}, Lcom/jscape/inet/ssh/util/keyreader/FormatException;-><init>(Ljava/lang/String;)V

    :cond_5
    :goto_3
    throw v2
.end method
