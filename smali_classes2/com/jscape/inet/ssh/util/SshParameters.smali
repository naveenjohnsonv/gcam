.class public Lcom/jscape/inet/ssh/util/SshParameters;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final a:I = 0x16

.field private static final b:I = 0x7530

.field private static final c:Lcom/jscape/inet/ssh/util/KeyPairAssembler;

.field private static final t:[Ljava/lang/String;


# instance fields
.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:I

.field private k:J

.field private l:J

.field private m:Ljava/lang/Integer;

.field private n:Ljava/lang/Boolean;

.field private o:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/security/KeyPair;

.field private s:Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "d;\rPw\u001b_C!\u001bA\'\u0000UHu\u0018Ju\u0006QE{\u0017\u001du\u001dIn\u000e^E\u0014\u000bQo\u000e^E<\u001dDs\u0002__h\u0011\u001du\u000eWh\u0013Id&\u001bWi\n]ThY\u0005\u001b\u007fT\u000f-\r\u001du\u000eWh\u0013Iy:\rQ:L\u0015\u001du\rJd\u0000UE\u0001\u000cDa\rYR\u0016\u0012Dt\u0018\r1d;\rPw\u001b_C!\u001bA\'\u0000UHu\u0018Ju\u0006QEu\u0011W\'\u001bQB&\tJu\u000f\u0010F4\r\u0005n\u0005S^\'\u000c@d\u001f\u001e\u0007\u001du\u000eJu\u001f\r\u0005\u001b\u007fT\u000f-\u0011\u001du\u000eWh\u0013Ia4\rVp\u0004BUhY\u0012\u001du\u0016Jt\u001f{T,(@u\u0002VX0\u000c\u0018\u0011\u001du\u000c@f\u000fY_2*Lj\u000e_D!C\u001ab&\u0016uf\u0019Q\\0\n@u\u0018\u0010J%\u000cJ\u007f\u0012dH%\u001b\u0018 \u0004_ \u0012I\n\u001du\u0015@~;QX\'C\u000c\u001du\u000eDt\u0018G^\'\u001a\u0018 \r\u001du\nFw%_u0\u0012D~V\u000c\u001du\u0016Jt\u001f^P8\u001b\u0018 \u000c\u001du\u000bVb\u0019^P8\u001b\u0018 \u0014\u001du\u001dJi\u0005UR!\u0017Ji?Y\\0\u0011PsV"

    const/16 v4, 0x157

    const/16 v5, 0x17

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x50

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x11

    const/16 v3, 0xc

    const-string v5, "R:A\u0018\'\\\u0006.uC\u001eu\u0004\u0010o]\u0006"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x1f

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/ssh/util/KeyPairAssembler;

    invoke-direct {v0}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;-><init>()V

    sput-object v0, Lcom/jscape/inet/ssh/util/SshParameters;->c:Lcom/jscape/inet/ssh/util/KeyPairAssembler;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    const/4 v1, 0x5

    if-eqz v15, :cond_8

    if-eq v15, v9, :cond_9

    const/4 v2, 0x2

    if-eq v15, v2, :cond_7

    const/4 v2, 0x3

    if-eq v15, v2, :cond_6

    const/4 v2, 0x4

    if-eq v15, v2, :cond_5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x60

    goto :goto_4

    :cond_4
    const/16 v1, 0x3b

    goto :goto_4

    :cond_5
    const/16 v1, 0x57

    goto :goto_4

    :cond_6
    const/16 v1, 0x75

    goto :goto_4

    :cond_7
    const/16 v1, 0x2e

    goto :goto_4

    :cond_8
    const/16 v1, 0x61

    :cond_9
    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/util/SshParameters;)V
    .locals 21

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iget-object v2, v0, Lcom/jscape/inet/ssh/util/SshParameters;->d:Ljava/lang/String;

    iget-object v3, v0, Lcom/jscape/inet/ssh/util/SshParameters;->e:Ljava/lang/String;

    iget v4, v0, Lcom/jscape/inet/ssh/util/SshParameters;->f:I

    iget-object v5, v0, Lcom/jscape/inet/ssh/util/SshParameters;->g:Ljava/lang/String;

    iget-object v6, v0, Lcom/jscape/inet/ssh/util/SshParameters;->h:Ljava/lang/String;

    iget-object v7, v0, Lcom/jscape/inet/ssh/util/SshParameters;->i:Ljava/lang/String;

    iget v8, v0, Lcom/jscape/inet/ssh/util/SshParameters;->j:I

    iget-wide v9, v0, Lcom/jscape/inet/ssh/util/SshParameters;->k:J

    iget-wide v11, v0, Lcom/jscape/inet/ssh/util/SshParameters;->l:J

    iget-object v13, v0, Lcom/jscape/inet/ssh/util/SshParameters;->m:Ljava/lang/Integer;

    iget-object v14, v0, Lcom/jscape/inet/ssh/util/SshParameters;->n:Ljava/lang/Boolean;

    iget-object v15, v0, Lcom/jscape/inet/ssh/util/SshParameters;->o:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    move-object/from16 v20, v1

    iget-object v1, v0, Lcom/jscape/inet/ssh/util/SshParameters;->p:Ljava/lang/String;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/jscape/inet/ssh/util/SshParameters;->q:Ljava/lang/String;

    move-object/from16 v17, v1

    iget-object v1, v0, Lcom/jscape/inet/ssh/util/SshParameters;->r:Ljava/security/KeyPair;

    move-object/from16 v18, v1

    iget-object v0, v0, Lcom/jscape/inet/ssh/util/SshParameters;->s:Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    move-object/from16 v19, v0

    move-object/from16 v1, v20

    invoke-direct/range {v1 .. v19}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLjava/lang/Integer;Ljava/lang/Boolean;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/io/File;)V
    .locals 6

    const-string v5, ""

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/io/File;Ljava/lang/String;)V
    .locals 7

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v5, 0x0

    const-string v6, ""

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 7

    const-string v6, ""

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V
    .locals 6

    if-eqz p5, :cond_0

    invoke-static {p5, p6}, Lcom/jscape/inet/ssh/util/SshParameters;->a(Ljava/io/File;Ljava/lang/String;)Ljava/security/KeyPair;

    move-result-object p5

    goto :goto_0

    :cond_0
    const/4 p5, 0x0

    :goto_0
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;)V
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v15, p3

    move-object/from16 v16, p4

    move-object/from16 v17, p5

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    sget-object v14, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;->NULL:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v8, 0x7530

    const-wide/16 v10, 0x7530

    const/4 v12, 0x0

    const/16 v18, 0x0

    invoke-direct/range {v0 .. v18}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLjava/lang/Integer;Ljava/lang/Boolean;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLjava/lang/Integer;Ljava/lang/Boolean;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V
    .locals 20

    if-eqz p17, :cond_0

    invoke-static/range {p17 .. p18}, Lcom/jscape/inet/ssh/util/SshParameters;->a(Ljava/io/File;Ljava/lang/String;)Ljava/security/KeyPair;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object/from16 v18, v0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-wide/from16 v9, p8

    move-wide/from16 v11, p10

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v19, p19

    invoke-direct/range {v1 .. v19}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLjava/lang/Integer;Ljava/lang/Boolean;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLjava/lang/Integer;Ljava/lang/Boolean;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V
    .locals 4

    move-object v1, p0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v0, p1

    iput-object v0, v1, Lcom/jscape/inet/ssh/util/SshParameters;->d:Ljava/lang/String;

    move-object v0, p2

    iput-object v0, v1, Lcom/jscape/inet/ssh/util/SshParameters;->e:Ljava/lang/String;

    invoke-static {}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->b()Z

    move-result v0

    move v2, p3

    iput v2, v1, Lcom/jscape/inet/ssh/util/SshParameters;->f:I

    move-object v2, p4

    iput-object v2, v1, Lcom/jscape/inet/ssh/util/SshParameters;->g:Ljava/lang/String;

    move-object v2, p5

    iput-object v2, v1, Lcom/jscape/inet/ssh/util/SshParameters;->h:Ljava/lang/String;

    invoke-static {p6}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object v2, p6

    iput-object v2, v1, Lcom/jscape/inet/ssh/util/SshParameters;->i:Ljava/lang/String;

    move v2, p7

    :try_start_0
    iput v2, v1, Lcom/jscape/inet/ssh/util/SshParameters;->j:I

    move-wide v2, p8

    iput-wide v2, v1, Lcom/jscape/inet/ssh/util/SshParameters;->k:J

    move-wide v2, p10

    iput-wide v2, v1, Lcom/jscape/inet/ssh/util/SshParameters;->l:J

    move-object/from16 v2, p12

    iput-object v2, v1, Lcom/jscape/inet/ssh/util/SshParameters;->m:Ljava/lang/Integer;

    move-object/from16 v2, p13

    iput-object v2, v1, Lcom/jscape/inet/ssh/util/SshParameters;->n:Ljava/lang/Boolean;

    invoke-static/range {p14 .. p14}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object/from16 v2, p14

    iput-object v2, v1, Lcom/jscape/inet/ssh/util/SshParameters;->o:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    invoke-static/range {p15 .. p15}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object/from16 v2, p15

    iput-object v2, v1, Lcom/jscape/inet/ssh/util/SshParameters;->p:Ljava/lang/String;

    move-object/from16 v2, p16

    iput-object v2, v1, Lcom/jscape/inet/ssh/util/SshParameters;->q:Ljava/lang/String;

    move-object/from16 v2, p17

    iput-object v2, v1, Lcom/jscape/inet/ssh/util/SshParameters;->r:Ljava/security/KeyPair;

    move-object/from16 v2, p18

    iput-object v2, v1, Lcom/jscape/inet/ssh/util/SshParameters;->s:Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v2, :cond_1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->b(Z)V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/inet/ssh/util/SshParameters;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/util/SshParameters;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 1

    const/16 v0, 0x16

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/io/File;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;)V
    .locals 6

    const/16 v2, 0x16

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x16

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 6

    const/16 v2, 0x16

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;)V
    .locals 6

    const/16 v2, 0x16

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ssh/util/SshParameters;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/security/KeyPair;)V

    return-void
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method

.method private static a(Ljava/io/File;Ljava/lang/String;)Ljava/security/KeyPair;
    .locals 2

    :try_start_0
    sget-object v0, Lcom/jscape/inet/ssh/util/SshParameters;->c:Lcom/jscape/inet/ssh/util/KeyPairAssembler;

    invoke-virtual {v0, p0, p1}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->restoreKeyPair(Ljava/io/File;Ljava/lang/String;)Ljava/security/KeyPair;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    invoke-direct {p1, v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
.end method


# virtual methods
.method public getClientAuthentication()Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->s:Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    return-object v0
.end method

.method public getConnectionTimeout()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->k:J

    return-wide v0
.end method

.method public getHostKeyVerifier()Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->o:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    return-object v0
.end method

.method public getHostname()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyPair()Ljava/security/KeyPair;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->r:Ljava/security/KeyPair;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->q:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->j:I

    return v0
.end method

.method public getProxyHost()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyPassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyPort()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->f:I

    return v0
.end method

.method public getProxyType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyUserId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getReadingTimeout()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->l:J

    return-wide v0
.end method

.method public getSocketTrafficClass()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->m:Ljava/lang/Integer;

    return-object v0
.end method

.method public getSshHostname()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getSshPassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->q:Ljava/lang/String;

    return-object v0
.end method

.method public getSshPort()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->j:I

    return v0
.end method

.method public getSshUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->p:Ljava/lang/String;

    return-object v0
.end method

.method public getTcpNoDelay()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->n:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshParameters;->p:Ljava/lang/String;

    return-object v0
.end method

.method public setClientAuthentication(Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->s:Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    return-void
.end method

.method public setConnectionTimeout(J)V
    .locals 0

    iput-wide p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->k:J

    return-void
.end method

.method public setHostKeyVerifier(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->o:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    return-void
.end method

.method public setHostname(Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->i:Ljava/lang/String;

    return-void
.end method

.method public setKeyPair(Ljava/security/KeyPair;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->r:Ljava/security/KeyPair;

    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->q:Ljava/lang/String;

    return-void
.end method

.method public setPort(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->j:I

    return-void
.end method

.method public setPrivateKey(Ljava/io/File;)V
    .locals 1

    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ssh/util/SshParameters;->setPrivateKey(Ljava/io/File;Ljava/lang/String;)V

    return-void
.end method

.method public setPrivateKey(Ljava/io/File;Ljava/lang/String;)V
    .locals 0

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/util/SshParameters;->a(Ljava/io/File;Ljava/lang/String;)Ljava/security/KeyPair;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->r:Ljava/security/KeyPair;

    return-void
.end method

.method public setPrivateKey(Ljava/lang/String;)V
    .locals 1

    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ssh/util/SshParameters;->setPrivateKey(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setPrivateKey(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    :try_start_0
    sget-object v0, Lcom/jscape/inet/ssh/util/SshParameters;->c:Lcom/jscape/inet/ssh/util/KeyPairAssembler;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ssh/util/KeyPairAssembler;->restoreKeyPair(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyPair;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->r:Ljava/security/KeyPair;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-direct {p2, v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public setProxyHost(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->e:Ljava/lang/String;

    return-void
.end method

.method public setProxyPassword(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->h:Ljava/lang/String;

    return-void
.end method

.method public setProxyPort(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->f:I

    return-void
.end method

.method public setProxyType(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->d:Ljava/lang/String;

    return-void
.end method

.method public setProxyUserId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->g:Ljava/lang/String;

    return-void
.end method

.method public setProxyUsername(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->g:Ljava/lang/String;

    return-void
.end method

.method public setReadingTimeout(J)V
    .locals 0

    iput-wide p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->l:J

    return-void
.end method

.method public setSocketTrafficClass(Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->m:Ljava/lang/Integer;

    return-void
.end method

.method public setSshHostname(Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->i:Ljava/lang/String;

    return-void
.end method

.method public setSshPassword(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->q:Ljava/lang/String;

    return-void
.end method

.method public setSshPort(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->j:I

    return-void
.end method

.method public setSshUsername(Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->p:Ljava/lang/String;

    return-void
.end method

.method public setTcpNoDelay(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->n:Ljava/lang/Boolean;

    return-void
.end method

.method public setUsername(Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshParameters;->p:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->b()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/16 v3, 0xc

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/util/SshParameters;->d:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x27

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v4, 0x4

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/ssh/util/SshParameters;->e:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v4, 0x14

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/jscape/inet/ssh/util/SshParameters;->f:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v4, 0x2

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/ssh/util/SshParameters;->g:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v4, 0x9

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/ssh/util/SshParameters;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    if-eqz v4, :cond_0

    const/4 v4, 0x3

    aget-object v4, v2, v4

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/16 v4, 0x15

    aget-object v2, v2, v4

    goto :goto_1

    :cond_1
    :goto_0
    move-object v2, v4

    :goto_1
    :try_start_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/16 v4, 0x11

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/SshParameters;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/4 v4, 0x7

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/util/SshParameters;->j:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/16 v4, 0x13

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/jscape/inet/ssh/util/SshParameters;->k:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/16 v4, 0xb

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/jscape/inet/ssh/util/SshParameters;->l:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/4 v4, 0x5

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/SshParameters;->m:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/16 v5, 0x10

    aget-object v2, v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/SshParameters;->n:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/16 v5, 0xa

    aget-object v2, v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/SshParameters;->o:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/16 v5, 0x12

    aget-object v2, v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/SshParameters;->p:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/16 v5, 0xf

    aget-object v2, v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/SshParameters;->q:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_3

    if-eqz v2, :cond_2

    sget-object v2, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/16 v5, 0x8

    aget-object v2, v2, v5

    goto :goto_2

    :cond_2
    sget-object v2, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/16 v5, 0xd

    aget-object v2, v2, v5

    :cond_3
    :goto_2
    :try_start_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/16 v3, 0xe

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/SshParameters;->r:Ljava/security/KeyPair;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ssh/util/SshParameters;->t:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/SshParameters;->s:Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_4

    new-array v0, v4, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_4
    return-object v1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/util/SshParameters;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/util/SshParameters;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0
.end method
