.class public Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static b:Z


# instance fields
.field public final fingerprint:Ljava/lang/String;

.field public final host:Ljava/net/InetAddress;

.field public final key:Ljava/security/PublicKey;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v2}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->b(Z)V

    const/4 v3, 0x0

    const/16 v4, 0xf

    const/4 v5, -0x1

    move v6, v3

    :goto_0
    const/16 v7, 0x10

    add-int/2addr v5, v2

    add-int/2addr v4, v5

    const-string v8, ";H\u0018eK\u0003+e\u0018\u000ceK\u0010s0\u000bR\u0006\n~\\D5|\r\u00071"

    invoke-virtual {v8, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    array-length v9, v5

    move v10, v3

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v5}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v7, v6, 0x1

    aput-object v5, v1, v6

    const/16 v5, 0x1b

    if-ge v4, v5, :cond_0

    invoke-virtual {v8, v4}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v6, v7

    move v14, v5

    move v5, v4

    move v4, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v5, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v2, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x5e

    goto :goto_2

    :cond_2
    const/16 v12, 0x74

    goto :goto_2

    :cond_3
    const/16 v12, 0x35

    goto :goto_2

    :cond_4
    const/16 v12, 0x1c

    goto :goto_2

    :cond_5
    const/16 v12, 0x6e

    goto :goto_2

    :cond_6
    const/16 v12, 0x78

    goto :goto_2

    :cond_7
    const/4 v12, 0x7

    :goto_2
    xor-int/2addr v12, v7

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v5, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/net/InetAddress;Ljava/security/PublicKey;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->host:Ljava/net/InetAddress;

    iput-object p2, p0, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->key:Ljava/security/PublicKey;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->fingerprint:Ljava/lang/String;

    return-void
.end method

.method public static b(Z)V
    .locals 0

    sput-boolean p0, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->b:Z

    return-void
.end method

.method public static b()Z
    .locals 1

    sget-boolean v0, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->b:Z

    return v0
.end method

.method public static c()Z
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public static entryFor(Ljava/net/InetAddress;Ljava/security/PublicKey;)Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;
    .locals 2

    new-instance v0, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->format(Ljava/security/PublicKey;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;-><init>(Ljava/net/InetAddress;Ljava/security/PublicKey;Ljava/lang/String;)V

    return-object v0
.end method

.method public static entrySha256For(Ljava/net/InetAddress;Ljava/security/PublicKey;)Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;
    .locals 2

    new-instance v0, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->formatSha256(Ljava/security/PublicKey;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;-><init>(Ljava/net/InetAddress;Ljava/security/PublicKey;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->key:Ljava/security/PublicKey;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->fingerprint:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
