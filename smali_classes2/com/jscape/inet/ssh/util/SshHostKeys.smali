.class public Lcom/jscape/inet/ssh/util/SshHostKeys;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/net/InetAddress;",
            "Ljava/util/List<",
            "Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "[e2]b\u001c\u001bCs#f-\u0014\nfb(|h\u001cR"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/util/SshHostKeys;->b:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v5, 0xb

    if-eqz v4, :cond_5

    const/4 v6, 0x1

    if-eq v4, v6, :cond_4

    const/4 v6, 0x2

    if-eq v4, v6, :cond_3

    const/4 v6, 0x3

    if-eq v4, v6, :cond_2

    const/4 v6, 0x4

    if-eq v4, v6, :cond_1

    const/4 v6, 0x5

    goto :goto_1

    :cond_1
    const/16 v5, 0x69

    goto :goto_1

    :cond_2
    const/16 v5, 0x71

    goto :goto_1

    :cond_3
    const/16 v5, 0x3e

    goto :goto_1

    :cond_4
    const/16 v5, 0x72

    goto :goto_1

    :cond_5
    const/16 v5, 0x6c

    :goto_1
    const/16 v4, 0x64

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public varargs constructor <init>([Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ssh/util/SshHostKeys;->a:Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/util/SshHostKeys;->set([Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;)Lcom/jscape/inet/ssh/util/SshHostKeys;

    return-void
.end method

.method private static a(Ljava/net/InetAddress;)Ljava/util/List;
    .locals 0

    new-instance p0, Ljava/util/LinkedList;

    invoke-direct {p0}, Ljava/util/LinkedList;-><init>()V

    return-object p0
.end method


# virtual methods
.method public addKey(Ljava/net/InetAddress;Ljava/lang/String;)Lcom/jscape/inet/ssh/util/SshHostKeys;
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/util/SshHostKeys;->put(Ljava/net/InetAddress;Ljava/lang/String;)Lcom/jscape/inet/ssh/util/SshHostKeys;

    move-result-object p1

    return-object p1
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshHostKeys;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public contains(Ljava/net/InetAddress;Ljava/security/PublicKey;)Z
    .locals 2

    new-instance v0, Lcom/jscape/inet/ssh/util/SshHostKeys$Fingerprint;

    invoke-direct {v0, p2}, Lcom/jscape/inet/ssh/util/SshHostKeys$Fingerprint;-><init>(Ljava/security/PublicKey;)V

    invoke-static {}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->b()Z

    move-result p2

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/util/SshHostKeys;->entriesOf(Ljava/net/InetAddress;)Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ssh/util/SshHostKeys$Fingerprint;->sameAsIn(Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;)Z

    move-result v1

    if-eqz p2, :cond_4

    if-eqz p2, :cond_2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    if-nez p2, :cond_0

    goto :goto_1

    :cond_2
    :goto_0
    return v1

    :cond_3
    :goto_1
    const/4 v1, 0x0

    :cond_4
    return v1
.end method

.method public entriesOf(Ljava/net/InetAddress;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/InetAddress;",
            ")",
            "Ljava/util/Collection<",
            "Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->c()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/SshHostKeys;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    :cond_1
    :goto_0
    return-object p1
.end method

.method public fingerprintsOf(Ljava/net/InetAddress;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/InetAddress;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->b()Z

    move-result v1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/util/SshHostKeys;->entriesOf(Ljava/net/InetAddress;)Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;

    if-eqz v1, :cond_1

    iget-object v2, v2, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->fingerprint:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-nez v1, :cond_0

    :cond_1
    return-object v0
.end method

.method public getHosts()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lcom/jscape/inet/ssh/util/SshHostKeys;->hosts()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getKeys(Ljava/net/InetAddress;)Ljava/util/Iterator;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/InetAddress;",
            ")",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/util/SshHostKeys;->fingerprintsOf(Ljava/net/InetAddress;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    return-object p1
.end method

.method public hosts()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshHostKeys;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public keysOf(Ljava/net/InetAddress;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/InetAddress;",
            ")",
            "Ljava/util/List<",
            "Ljava/security/PublicKey;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->c()Z

    move-result v1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/util/SshHostKeys;->entriesOf(Ljava/net/InetAddress;)Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;

    iget-object v3, v2, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->key:Ljava/security/PublicKey;

    if-eqz v3, :cond_1

    iget-object v2, v2, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->key:Ljava/security/PublicKey;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz v1, :cond_0

    :cond_2
    return-object v0
.end method

.method public put(Ljava/net/InetAddress;Ljava/lang/String;)Lcom/jscape/inet/ssh/util/SshHostKeys;
    .locals 2

    new-instance v0, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, p2}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;-><init>(Ljava/net/InetAddress;Ljava/security/PublicKey;Ljava/lang/String;)V

    const/4 p1, 0x1

    new-array p1, p1, [Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;

    const/4 p2, 0x0

    aput-object v0, p1, p2

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ssh/util/SshHostKeys;->set([Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;)Lcom/jscape/inet/ssh/util/SshHostKeys;

    return-object p0
.end method

.method public put(Ljava/net/InetAddress;Ljava/security/PublicKey;)Lcom/jscape/inet/ssh/util/SshHostKeys;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->entryFor(Ljava/net/InetAddress;Ljava/security/PublicKey;)Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->entrySha256For(Ljava/net/InetAddress;Ljava/security/PublicKey;)Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v0, p2

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ssh/util/SshHostKeys;->set([Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;)Lcom/jscape/inet/ssh/util/SshHostKeys;

    return-object p0
.end method

.method public remove(Ljava/net/InetAddress;Ljava/lang/String;)Lcom/jscape/inet/ssh/util/SshHostKeys;
    .locals 5

    invoke-static {}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->c()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/SshHostKeys;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    return-object p0

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;

    iget-object v4, v3, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->fingerprint:Ljava/lang/String;

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v0, :cond_5

    if-nez v0, :cond_2

    if-eqz v4, :cond_3

    invoke-interface {v1, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_2
    if-eqz v0, :cond_4

    :cond_3
    if-eqz v0, :cond_1

    :cond_4
    if-nez v0, :cond_6

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    :cond_5
    if-eqz v4, :cond_6

    iget-object p2, p0, Lcom/jscape/inet/ssh/util/SshHostKeys;->a:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    return-object p0
.end method

.method public remove(Ljava/net/InetAddress;Ljava/security/PublicKey;)Lcom/jscape/inet/ssh/util/SshHostKeys;
    .locals 5

    invoke-static {}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->b()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/SshHostKeys;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-eqz v0, :cond_0

    if-nez v1, :cond_0

    return-object p0

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;

    iget-object v4, v3, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->key:Ljava/security/PublicKey;

    if-eqz v0, :cond_6

    if-eqz v0, :cond_2

    if-eqz v4, :cond_4

    iget-object v4, v3, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->key:Ljava/security/PublicKey;

    :cond_2
    invoke-virtual {v4, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v0, :cond_3

    if-eqz v4, :cond_4

    invoke-interface {v1, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_3
    if-nez v0, :cond_5

    :cond_4
    if-nez v0, :cond_1

    :cond_5
    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_6

    iget-object p2, p0, Lcom/jscape/inet/ssh/util/SshHostKeys;->a:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    return-object p0
.end method

.method public removeKey(Ljava/net/InetAddress;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ssh/util/SshHostKeys;->remove(Ljava/net/InetAddress;Ljava/lang/String;)Lcom/jscape/inet/ssh/util/SshHostKeys;

    return-void
.end method

.method public varargs set([Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;)Lcom/jscape/inet/ssh/util/SshHostKeys;
    .locals 7

    invoke-static {}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->b()Z

    move-result v0

    array-length v1, p1

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v1, :cond_1

    aget-object v3, p1, v2

    if-eqz v0, :cond_1

    iget-object v4, p0, Lcom/jscape/inet/ssh/util/SshHostKeys;->a:Ljava/util/Map;

    iget-object v5, v3, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->host:Ljava/net/InetAddress;

    nop

    nop

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->computeIfAbsent(Ljava/lang/Object;Ljava/util/function/Function;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_0

    :cond_1
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/util/SshHostKeys;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/SshHostKeys;->a:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
