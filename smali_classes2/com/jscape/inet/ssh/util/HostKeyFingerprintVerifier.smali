.class public Lcom/jscape/inet/ssh/util/HostKeyFingerprintVerifier;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/inet/ssh/util/SshHostKeys;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "V8k_t~eX>vLZill>v_i~nw1qNM;gu9w\\QPyg$%"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ssh/util/HostKeyFingerprintVerifier;->b:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x55

    goto :goto_1

    :cond_1
    const/16 v4, 0x52

    goto :goto_1

    :cond_2
    const/16 v4, 0x76

    goto :goto_1

    :cond_3
    const/16 v4, 0x62

    goto :goto_1

    :cond_4
    const/16 v4, 0x51

    goto :goto_1

    :cond_5
    const/16 v4, 0x1e

    goto :goto_1

    :cond_6
    const/16 v4, 0x57

    :goto_1
    const/16 v5, 0x49

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/util/SshHostKeys;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/HostKeyFingerprintVerifier;->a:Lcom/jscape/inet/ssh/util/SshHostKeys;

    return-void
.end method


# virtual methods
.method public assertKeyValid(Ljava/net/InetAddress;Ljava/security/PublicKey;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$OperationException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/util/HostKeyFingerprintVerifier;->a:Lcom/jscape/inet/ssh/util/SshHostKeys;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ssh/util/SshHostKeys;->contains(Ljava/net/InetAddress;Ljava/security/PublicKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$InvalidHostKeyException;

    invoke-direct {v0, p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$InvalidHostKeyException;-><init>(Ljava/net/InetAddress;Ljava/security/PublicKey;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService$OperationException;

    move-result-object p1

    throw p1
.end method

.method public getKnownKeys()Lcom/jscape/inet/ssh/util/SshHostKeys;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ssh/util/HostKeyFingerprintVerifier;->a:Lcom/jscape/inet/ssh/util/SshHostKeys;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/util/HostKeyFingerprintVerifier;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/HostKeyFingerprintVerifier;->a:Lcom/jscape/inet/ssh/util/SshHostKeys;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
