.class public Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcom/jscape/util/i/m;

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "\u001fh\nTTN\'8r\u001cE\u0004U-3&\rXT[f\u0015\u001fh\nTTN\'8r\u001cE\u0004U-3&\rXT[f\t\u0019N8\u0013\u0011\u0008rou"

    const/16 v5, 0x35

    const/16 v6, 0x15

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x66

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0xe

    const/4 v4, 0x6

    const-string v6, "\n]+\u0000\u0002\u001b\u0007\n]+\u0000\u0002\u001ba"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v16, v6

    move v6, v4

    move-object/from16 v4, v16

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x75

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->b:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/util/i/m;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/jscape/util/i/m;-><init>(Ljava/security/Provider;)V

    sput-object v0, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->a:Lcom/jscape/util/i/m;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    const/4 v3, 0x4

    if-eq v2, v3, :cond_5

    if-eq v2, v0, :cond_4

    const/16 v2, 0x2e

    goto :goto_4

    :cond_4
    const/16 v2, 0x58

    goto :goto_4

    :cond_5
    const/16 v2, 0x42

    goto :goto_4

    :cond_6
    const/16 v2, 0x47

    goto :goto_4

    :cond_7
    const/16 v2, 0x1f

    goto :goto_4

    :cond_8
    const/16 v2, 0x60

    goto :goto_4

    :cond_9
    const/16 v2, 0x2c

    :goto_4
    xor-int/2addr v2, v9

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method

.method public static format(Ljava/security/PublicKey;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->md5HashOf(Ljava/security/PublicKey;)[B

    move-result-object p0

    const-string v0, ":"

    invoke-static {p0, v0}, Lcom/jscape/util/W;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formatSha256(Ljava/security/PublicKey;)Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->sha256HashOf(Ljava/security/PublicKey;)[B

    move-result-object p0

    sget-object v0, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->b:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Lcom/jscape/util/Base64Format;->formatData([B)Ljava/lang/String;

    move-result-object p0

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static md5HashOf(Ljava/security/PublicKey;)[B
    .locals 2

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->a:Lcom/jscape/util/i/m;

    invoke-virtual {v1, p0, v0}, Lcom/jscape/util/i/m;->b(Ljava/security/PublicKey;Ljava/io/OutputStream;)V

    invoke-static {}, Lcom/jscape/a/h;->d()Lcom/jscape/a/h;

    move-result-object p0

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jscape/a/h;->b([B)[B

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->b:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static parseMd5(Ljava/lang/String;)[B
    .locals 1

    const-string v0, ":"

    invoke-static {p0, v0}, Lcom/jscape/util/W;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object p0

    return-object p0
.end method

.method public static parseSha256(Ljava/lang/String;)[B
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->c()Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    sget-object v0, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->b:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    :try_start_1
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :cond_0
    :goto_0
    invoke-static {p0}, Lcom/jscape/util/Base64Format;->parseData(Ljava/lang/String;)[B

    move-result-object p0

    return-object p0
.end method

.method public static sha256HashOf(Ljava/security/PublicKey;)[B
    .locals 2

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v1, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->a:Lcom/jscape/util/i/m;

    invoke-virtual {v1, p0, v0}, Lcom/jscape/util/i/m;->b(Ljava/security/PublicKey;Ljava/io/OutputStream;)V

    sget-object p0, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->b:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object p0, p0, v1

    invoke-static {p0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object p0

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->b:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
