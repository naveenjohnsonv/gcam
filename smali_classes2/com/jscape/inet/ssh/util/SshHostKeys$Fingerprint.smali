.class public Lcom/jscape/inet/ssh/util/SshHostKeys$Fingerprint;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/security/PublicKey;


# direct methods
.method public constructor <init>(Ljava/security/PublicKey;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ssh/util/SshHostKeys$Fingerprint;->a:Ljava/security/PublicKey;

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->c()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    return v1

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ssh/util/SshHostKeys$Fingerprint;->a:Ljava/security/PublicKey;

    invoke-static {v0}, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->format(Ljava/security/PublicKey;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, p1

    :goto_0
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    return v1
.end method

.method private b(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ssh/util/SshHostKeys$Fingerprint;->a:Ljava/security/PublicKey;

    invoke-static {v1}, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->sha256HashOf(Ljava/security/PublicKey;)[B

    move-result-object v1

    invoke-static {p1}, Lcom/jscape/inet/ssh/util/KeyFingerprintFormat;->parseSha256(Ljava/lang/String;)[B

    move-result-object p1

    invoke-static {v1, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    return v0
.end method


# virtual methods
.method public sameAsIn(Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;)Z
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->c()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/ssh/util/SshHostKeys$Fingerprint;->a:Ljava/security/PublicKey;

    iget-object v2, p1, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->key:Ljava/security/PublicKey;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v0, :cond_2

    if-nez v1, :cond_1

    iget-object v1, p1, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->fingerprint:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/jscape/inet/ssh/util/SshHostKeys$Fingerprint;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v0, :cond_2

    if-nez v1, :cond_1

    iget-object p1, p1, Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;->fingerprint:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/jscape/inet/ssh/util/SshHostKeys$Fingerprint;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v0, :cond_2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move p1, v1

    :goto_1
    return p1
.end method
