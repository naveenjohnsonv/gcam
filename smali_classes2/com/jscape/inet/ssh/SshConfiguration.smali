.class public Lcom/jscape/inet/ssh/SshConfiguration;
.super Ljava/lang/Object;


# static fields
.field public static final DEFAULT_PACKET_SIZE:I = 0x8000

.field public static final DEFAULT_WINDOW_SIZE:I = 0x200000

.field private static final a:[Ljava/lang/String;

.field private static b:Z


# instance fields
.field public final compressionFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;

.field public final encryptionFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;

.field public final identificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

.field public final initialWindowSize:I

.field public final keyExchangeFactory:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;

.field public final macFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;

.field public final maxPacketSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/jscape/inet/ssh/SshConfiguration;->b(Z)V

    const/4 v3, 0x0

    const-string v4, "\u001aCk8kD<U\u0017i+q?\n|0E\u0018XGp\u0004M6\u0006|0E\u0018XG\'e\u0010n\u001agl;_\u0004s+iv4Y\r&\"af8X\u0017o?aa<B\ni7[v/_\rad\u0014\u001aCo7av4W\u000fQ0ff2A0o#m?\u0010\u001aCk8pR<U\u0008c-[k\'S^\u0014\u001aCc7kp$F\u0017o6fD<U\u0017i+q?\u0015\u001aCm<qG%U\u000bg7og\u001bW\u0000r6z{`\u0003\u0004M6"

    const/16 v5, 0x9c

    const/16 v6, 0xd

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x30

    add-int/2addr v7, v1

    add-int v10, v7, v6

    invoke-virtual {v4, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v3

    :goto_2
    const/4 v14, 0x6

    if-gt v12, v13, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v10, v8, 0x1

    if-eqz v11, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v10

    goto :goto_0

    :cond_0
    const/16 v5, 0x1c

    const-string v4, "6z\u000fR\u0012\r\u0015P\t/|/8e\u0019Z?z-&Q\u001dJ8|01*"

    move v8, v10

    move v6, v14

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v10

    :goto_3
    const/16 v9, 0x7a

    add-int/2addr v7, v1

    add-int v10, v7, v6

    invoke-virtual {v4, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ssh/SshConfiguration;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v10, v13

    rem-int/lit8 v2, v13, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v1, :cond_8

    const/4 v14, 0x2

    if-eq v2, v14, :cond_7

    const/4 v14, 0x3

    if-eq v2, v14, :cond_6

    const/4 v14, 0x4

    if-eq v2, v14, :cond_5

    const/4 v14, 0x5

    if-eq v2, v14, :cond_4

    const/16 v14, 0x6d

    goto :goto_4

    :cond_4
    const/16 v14, 0x32

    goto :goto_4

    :cond_5
    const/16 v14, 0x38

    goto :goto_4

    :cond_6
    const/16 v14, 0x69

    goto :goto_4

    :cond_7
    const/16 v14, 0x36

    goto :goto_4

    :cond_8
    const/16 v14, 0x53

    :cond_9
    :goto_4
    xor-int v2, v9, v14

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;II)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    sget-object v1, Lcom/jscape/inet/ssh/SshConfiguration;->a:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1, p1, p2}, Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/jscape/inet/ssh/SshConfiguration;->identificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/ssh/SshConfiguration;->keyExchangeFactory:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/ssh/SshConfiguration;->encryptionFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/inet/ssh/SshConfiguration;->macFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;

    invoke-static {}, Lcom/jscape/inet/ssh/SshConfiguration;->c()Z

    move-result p1

    invoke-static {p6}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p6, p0, Lcom/jscape/inet/ssh/SshConfiguration;->compressionFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput p7, p0, Lcom/jscape/inet/ssh/SshConfiguration;->initialWindowSize:I

    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput p8, p0, Lcom/jscape/inet/ssh/SshConfiguration;->maxPacketSize:I

    if-eqz p1, :cond_0

    const/4 p1, 0x3

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-void
.end method

.method public static b(Z)V
    .locals 0

    sput-boolean p0, Lcom/jscape/inet/ssh/SshConfiguration;->b:Z

    return-void
.end method

.method public static b()Z
    .locals 1

    sget-boolean v0, Lcom/jscape/inet/ssh/SshConfiguration;->b:Z

    return v0
.end method

.method public static c()Z
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/SshConfiguration;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public static configurationFor(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;)Lcom/jscape/inet/ssh/SshConfiguration;
    .locals 10

    new-instance v9, Lcom/jscape/inet/ssh/SshConfiguration;

    sget-object v0, Lcom/jscape/inet/ssh/SshConfiguration;->a:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v1, v0, v1

    const/4 v2, 0x0

    const/high16 v7, 0x200000

    const v8, 0x8000

    move-object v0, v9

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v8}, Lcom/jscape/inet/ssh/SshConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;II)V

    return-object v9
.end method

.method public static defaultClientConfiguration()Lcom/jscape/inet/ssh/SshConfiguration;
    .locals 2

    sget-object v0, Lcom/jscape/inet/ssh/SshConfiguration;->a:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/jscape/inet/ssh/SshConfiguration;->defaultClientConfiguration(Ljava/lang/String;)Lcom/jscape/inet/ssh/SshConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public static defaultClientConfiguration(Ljava/lang/String;)Lcom/jscape/inet/ssh/SshConfiguration;
    .locals 10

    new-instance v9, Lcom/jscape/inet/ssh/SshConfiguration;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    const/4 v1, 0x0

    new-array v2, v1, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;-><init>([Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;)V

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initAllClient(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object v3

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    new-array v2, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;)V

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->initAllClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object v4

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    new-array v2, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;)V

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->initAllClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object v5

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;

    new-array v1, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;)V

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compressions;->initAllClient(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;

    move-result-object v6

    const/4 v2, 0x0

    const/high16 v7, 0x200000

    const v8, 0x8000

    move-object v0, v9

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/jscape/inet/ssh/SshConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;II)V

    return-object v9
.end method

.method public static defaultServerConfiguration()Lcom/jscape/inet/ssh/SshConfiguration;
    .locals 2

    sget-object v0, Lcom/jscape/inet/ssh/SshConfiguration;->a:[Ljava/lang/String;

    const/16 v1, 0x9

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/jscape/inet/ssh/SshConfiguration;->defaultServerConfiguration(Ljava/lang/String;)Lcom/jscape/inet/ssh/SshConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public static defaultServerConfiguration(Ljava/lang/String;)Lcom/jscape/inet/ssh/SshConfiguration;
    .locals 10

    new-instance v9, Lcom/jscape/inet/ssh/SshConfiguration;

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    const/4 v1, 0x0

    new-array v2, v1, [Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;-><init>([Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory$Entry;)V

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchanges;->initAll(Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;)Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ComponentClientKeyExchangeFactory;

    move-result-object v3

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    new-array v2, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory$Entry;)V

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptions;->initAll(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceEncryptionFactory;

    move-result-object v4

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    new-array v2, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;

    invoke-direct {v0, v2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory$Entry;)V

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacs;->initAll(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/JceMacFactory;

    move-result-object v5

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;

    new-array v1, v1, [Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;-><init>([Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory$Entry;)V

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/Compressions;->initAll(Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;)Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/ComponentCompressionFactory;

    move-result-object v6

    const/4 v2, 0x0

    const/high16 v7, 0x200000

    const v8, 0x8000

    move-object v0, v9

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/jscape/inet/ssh/SshConfiguration;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;II)V

    return-object v9
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ssh/SshConfiguration;->c()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ssh/SshConfiguration;->a:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/SshConfiguration;->identificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x7

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/SshConfiguration;->keyExchangeFactory:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x6

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/SshConfiguration;->encryptionFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/SshConfiguration;->macFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v3, 0xa

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ssh/SshConfiguration;->compressionFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x4

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/jscape/inet/ssh/SshConfiguration;->initialWindowSize:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ssh/SshConfiguration;->maxPacketSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v2

    if-nez v2, :cond_0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/jscape/inet/ssh/SshConfiguration;->b(Z)V

    :cond_0
    return-object v1
.end method
