.class final Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionOutputStream;
.super Ljava/io/OutputStream;


# instance fields
.field private final a:Lcom/jscape/inet/ftp/FtpBaseImplementation;

.field private final b:Lcom/jscape/inet/ftp/FtpConnection;

.field private final c:Ljava/io/OutputStream;


# direct methods
.method private constructor <init>(Lcom/jscape/inet/ftp/FtpBaseImplementation;Lcom/jscape/inet/ftp/FtpConnection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionOutputStream;->a:Lcom/jscape/inet/ftp/FtpBaseImplementation;

    iput-object p2, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionOutputStream;->b:Lcom/jscape/inet/ftp/FtpConnection;

    invoke-interface {p2}, Lcom/jscape/inet/ftp/FtpConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionOutputStream;->c:Ljava/io/OutputStream;

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftp/FtpBaseImplementation;Lcom/jscape/inet/ftp/FtpConnection;Lcom/jscape/inet/ftp/FtpBaseImplementation$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionOutputStream;-><init>(Lcom/jscape/inet/ftp/FtpBaseImplementation;Lcom/jscape/inet/ftp/FtpConnection;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionOutputStream;->c:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionOutputStream;->b:Lcom/jscape/inet/ftp/FtpConnection;

    invoke-interface {v0}, Lcom/jscape/inet/ftp/FtpConnection;->close()V

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionOutputStream;->a:Lcom/jscape/inet/ftp/FtpBaseImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpBaseImplementation;->readResponse()V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionOutputStream;->c:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

.method public write(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionOutputStream;->c:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method public write([B)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionOutputStream;->c:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public write([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionOutputStream;->c:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    return-void
.end method
