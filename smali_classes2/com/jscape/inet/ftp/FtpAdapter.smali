.class public Lcom/jscape/inet/ftp/FtpAdapter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ftp/FtpListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public changeDir(Lcom/jscape/inet/ftp/FtpChangeDirEvent;)V
    .locals 0

    return-void
.end method

.method public commandSent(Lcom/jscape/inet/ftp/FtpCommandEvent;)V
    .locals 0

    return-void
.end method

.method public connected(Lcom/jscape/inet/ftp/FtpConnectedEvent;)V
    .locals 0

    return-void
.end method

.method public connectionLost(Lcom/jscape/inet/ftp/FtpConnectionLostEvent;)V
    .locals 0

    return-void
.end method

.method public createDir(Lcom/jscape/inet/ftp/FtpCreateDirEvent;)V
    .locals 0

    return-void
.end method

.method public deleteDir(Lcom/jscape/inet/ftp/FtpDeleteDirEvent;)V
    .locals 0

    return-void
.end method

.method public deleteFile(Lcom/jscape/inet/ftp/FtpDeleteFileEvent;)V
    .locals 0

    return-void
.end method

.method public disconnected(Lcom/jscape/inet/ftp/FtpDisconnectedEvent;)V
    .locals 0

    return-void
.end method

.method public download(Lcom/jscape/inet/ftp/FtpDownloadEvent;)V
    .locals 0

    return-void
.end method

.method public listing(Lcom/jscape/inet/ftp/FtpListingEvent;)V
    .locals 0

    return-void
.end method

.method public progress(Lcom/jscape/inet/ftp/FtpProgressEvent;)V
    .locals 0

    return-void
.end method

.method public renameFile(Lcom/jscape/inet/ftp/FtpRenameFileEvent;)V
    .locals 0

    return-void
.end method

.method public responseReceived(Lcom/jscape/inet/ftp/FtpResponseEvent;)V
    .locals 0

    return-void
.end method

.method public upload(Lcom/jscape/inet/ftp/FtpUploadEvent;)V
    .locals 0

    return-void
.end method
