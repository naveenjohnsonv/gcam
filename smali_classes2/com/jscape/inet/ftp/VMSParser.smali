.class public Lcom/jscape/inet/ftp/VMSParser;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ftp/FtpFileParser;


# static fields
.field private static final a:I = 0x200

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "\u0012v|\u0000^kN\u000fk(\u0005\"}%,\u007f\n;_|)wkN\u000fk(\u0010\u0012?\u001c\u0000^kN\u000fk(m[\u000e\r\u001b\u007f\u0004XV\u0018\u001f"

    const/16 v4, 0x31

    const/16 v5, 0xa

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x32

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    const/4 v14, 0x4

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0xe

    const-string v3, ",\"lk\tF\u000fW\\\u0004F,p\u001f"

    move v7, v10

    move v5, v14

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x46

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ftp/VMSParser;->b:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v10, v13

    rem-int/lit8 v1, v13, 0x7

    const/4 v2, 0x5

    if-eqz v1, :cond_9

    if-eq v1, v9, :cond_8

    const/4 v9, 0x2

    if-eq v1, v9, :cond_7

    const/4 v9, 0x3

    if-eq v1, v9, :cond_6

    if-eq v1, v14, :cond_5

    if-eq v1, v2, :cond_4

    goto :goto_4

    :cond_4
    const/16 v2, 0x74

    goto :goto_4

    :cond_5
    const/16 v2, 0x21

    goto :goto_4

    :cond_6
    const/16 v2, 0x7f

    goto :goto_4

    :cond_7
    const/16 v2, 0x63

    goto :goto_4

    :cond_8
    const/16 v2, 0x20

    goto :goto_4

    :cond_9
    const/16 v2, 0x44

    :goto_4
    xor-int v1, v8, v2

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    const/4 v9, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/jscape/inet/ftp/FtpFile;
    .locals 13

    const-string v0, ","

    const-string v1, ""

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, ";"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, -0x1

    if-ne v3, v5, :cond_0

    return-object v4

    :cond_0
    const/4 v5, 0x1

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p1, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Lcom/jscape/inet/ftp/VMSParser;->b:[Ljava/lang/String;

    const/4 v10, 0x4

    aget-object v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v2, :cond_2

    if-eqz v8, :cond_1

    move v8, v5

    goto :goto_0

    :cond_1
    move v8, v6

    goto :goto_1

    :cond_2
    :goto_0
    :try_start_2
    sget-object v9, Lcom/jscape/inet/ftp/VMSParser;->b:[Ljava/lang/String;

    const/4 v10, 0x5

    aget-object v9, v9, v10

    invoke-virtual {v7, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v7, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    :goto_1
    new-instance v9, Lcom/jscape/inet/ftp/FtpFile;

    invoke-direct {v9, v7, p1, p0}, Lcom/jscape/inet/ftp/FtpFile;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ftp/FtpFileParser;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :try_start_3
    invoke-virtual {v9, v8}, Lcom/jscape/inet/ftp/FtpFile;->setDirectory(Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    :catch_0
    move-exception v7

    :try_start_4
    invoke-static {v7}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v7

    throw v7
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-object v9, v4

    :catch_2
    :goto_2
    add-int/2addr v3, v5

    :try_start_5
    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-object v7, v1

    move p1, v6

    move v3, p1

    :cond_3
    :try_start_6
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    if-ge p1, v8, :cond_7

    invoke-virtual {v4, p1}, Ljava/lang/String;->charAt(I)C

    move-result v8
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    invoke-static {v8}, Ljava/lang/Character;->isDigit(C)Z

    move-result v10
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v2, :cond_8

    if-eqz v2, :cond_5

    if-eqz v10, :cond_4

    :try_start_8
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-nez v2, :cond_6

    :cond_4
    add-int/lit8 v3, p1, 0x1

    goto :goto_3

    :cond_5
    move v3, v10

    :goto_3
    if-nez v2, :cond_7

    :cond_6
    add-int/lit8 p1, p1, 0x1

    if-nez v2, :cond_3

    goto :goto_4

    :catch_3
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_4
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_8
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_7
    :goto_4
    move v10, v3

    :cond_8
    if-eqz v2, :cond_9

    if-nez v10, :cond_a

    :try_start_b
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result p1
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    sub-int/2addr p1, v5

    move v3, p1

    goto :goto_5

    :catch_5
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :cond_9
    move v3, v10

    :cond_a
    :goto_5
    if-eqz v2, :cond_b

    if-eqz v4, :cond_b

    :try_start_d
    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_6

    move-object v4, p1

    goto :goto_6

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_b
    :goto_6
    move v3, v6

    goto :goto_8

    :catchall_0
    move-exception p1

    move v6, v3

    goto :goto_7

    :catchall_1
    move-exception p1

    :goto_7
    if-eqz v2, :cond_c

    if-eqz v4, :cond_c

    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    :cond_c
    throw p1

    :catch_7
    move v3, v6

    :catch_8
    if-eqz v2, :cond_d

    if-eqz v4, :cond_b

    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_6

    :cond_d
    :goto_8
    move-object v8, v1

    move p1, v6

    move v7, p1

    :cond_e
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v10

    if-ge p1, v10, :cond_1b

    invoke-virtual {v4, p1}, Ljava/lang/String;->charAt(I)C

    move-result v10

    if-eqz v2, :cond_1d

    if-eqz v2, :cond_16

    if-nez v7, :cond_15

    if-eqz v2, :cond_10

    const/16 v11, 0x2f

    if-ne v10, v11, :cond_f

    move v7, v5

    if-nez v2, :cond_19

    :cond_f
    :try_start_e
    invoke-static {v10}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v11
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_9

    goto :goto_9

    :catch_9
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_10
    move v11, v10

    :goto_9
    if-eqz v2, :cond_12

    if-eqz v11, :cond_11

    :try_start_f
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_a

    if-eqz v2, :cond_12

    if-lez v11, :cond_11

    move v3, p1

    if-nez v2, :cond_1b

    goto :goto_a

    :catch_a
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_11
    :goto_a
    :try_start_10
    invoke-static {v10}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v11
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_b

    goto :goto_b

    :catch_b
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_12
    :goto_b
    if-eqz v2, :cond_14

    if-eqz v11, :cond_13

    goto :goto_e

    :cond_13
    invoke-static {v10}, Ljava/lang/Character;->isDigit(C)Z

    move-result v11

    :cond_14
    if-eqz v11, :cond_19

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    if-nez v2, :cond_19

    :cond_15
    :try_start_11
    invoke-static {v10}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v10
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_c

    move-object v12, v8

    move v8, v7

    move v7, v10

    move-object v10, v12

    goto :goto_c

    :catch_c
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_16
    move-object v10, v8

    move v8, v7

    :goto_c
    if-eqz v2, :cond_17

    if-eqz v7, :cond_18

    move v3, p1

    goto :goto_d

    :cond_17
    move v3, v7

    :goto_d
    if-nez v2, :cond_1a

    :cond_18
    move v7, v8

    move-object v8, v10

    :cond_19
    :goto_e
    add-int/lit8 p1, p1, 0x1

    if-nez v2, :cond_e

    goto :goto_f

    :cond_1a
    move-object v8, v10

    :cond_1b
    :goto_f
    if-eqz v9, :cond_1c

    :try_start_12
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    mul-int/lit16 p1, p1, 0x200

    int-to-long v7, p1

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v9, p1}, Lcom/jscape/inet/ftp/FtpFile;->setFilesize(Ljava/lang/String;)V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_d

    goto :goto_10

    :catch_d
    return-object v9

    :cond_1c
    :goto_10
    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    move v3, v6

    move v7, v3

    :cond_1d
    move-object p1, v1

    :cond_1e
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v7, v8, :cond_23

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    :try_start_13
    invoke-static {v8}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v10
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_e

    if-eqz v2, :cond_24

    if-eqz v2, :cond_1f

    if-eqz v10, :cond_21

    :try_start_14
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v10
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_10

    :cond_1f
    if-eqz v2, :cond_20

    if-lez v10, :cond_22

    move v3, v7

    goto :goto_11

    :cond_20
    move v3, v10

    :goto_11
    if-nez v2, :cond_23

    :cond_21
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_22
    add-int/lit8 v7, v7, 0x1

    if-nez v2, :cond_1e

    goto :goto_12

    :catch_e
    move-exception p1

    :try_start_15
    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_f

    :catch_f
    move-exception p1

    :try_start_16
    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_10

    :catch_10
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_23
    :goto_12
    invoke-virtual {v9, p1}, Lcom/jscape/inet/ftp/FtpFile;->setDate(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    move v3, v6

    move v10, v3

    :cond_24
    move-object p1, v1

    :cond_25
    :try_start_17
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v10, v7, :cond_2a

    invoke-virtual {v4, v10}, Ljava/lang/String;->charAt(I)C

    move-result v7
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_14
    .catchall {:try_start_17 .. :try_end_17} :catchall_2

    :try_start_18
    invoke-static {v7}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v8
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_11
    .catchall {:try_start_18 .. :try_end_18} :catchall_2

    if-eqz v2, :cond_2b

    if-eqz v2, :cond_26

    if-eqz v8, :cond_28

    :try_start_19
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_13
    .catchall {:try_start_19 .. :try_end_19} :catchall_2

    :cond_26
    if-eqz v2, :cond_27

    if-lez v8, :cond_29

    move v3, v10

    goto :goto_13

    :cond_27
    move v3, v8

    :goto_13
    if-nez v2, :cond_2a

    :cond_28
    :try_start_1a
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_14
    .catchall {:try_start_1a .. :try_end_1a} :catchall_2

    :cond_29
    add-int/lit8 v10, v10, 0x1

    if-nez v2, :cond_25

    goto :goto_14

    :catch_11
    move-exception p1

    :try_start_1b
    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_12
    .catchall {:try_start_1b .. :try_end_1b} :catchall_2

    :catch_12
    move-exception p1

    :try_start_1c
    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_13
    .catchall {:try_start_1c .. :try_end_1c} :catchall_2

    :catch_13
    move-exception p1

    :try_start_1d
    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2a
    :goto_14
    invoke-virtual {v9, p1}, Lcom/jscape/inet/ftp/FtpFile;->setTime(Ljava/lang/String;)V
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_14
    .catchall {:try_start_1d .. :try_end_1d} :catchall_2

    :catch_14
    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    move v8, v6

    goto :goto_15

    :catchall_2
    move-exception p1

    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    throw p1

    :cond_2b
    :goto_15
    move-object v3, v1

    move p1, v6

    :cond_2c
    :try_start_1e
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-ge p1, v7, :cond_31

    invoke-virtual {v4, p1}, Ljava/lang/String;->charAt(I)C

    move-result v7
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_18
    .catchall {:try_start_1e .. :try_end_1e} :catchall_3

    :try_start_1f
    invoke-static {v7}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v10
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_15
    .catchall {:try_start_1f .. :try_end_1f} :catchall_3

    if-eqz v2, :cond_32

    if-eqz v2, :cond_2d

    if-eqz v10, :cond_2f

    :try_start_20
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v10
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_17
    .catchall {:try_start_20 .. :try_end_20} :catchall_3

    :cond_2d
    if-eqz v2, :cond_2e

    if-lez v10, :cond_30

    move v8, p1

    goto :goto_16

    :cond_2e
    move v8, v10

    :goto_16
    if-nez v2, :cond_31

    :cond_2f
    :try_start_21
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_18
    .catchall {:try_start_21 .. :try_end_21} :catchall_3

    :cond_30
    add-int/lit8 p1, p1, 0x1

    if-nez v2, :cond_2c

    goto :goto_17

    :catch_15
    move-exception p1

    :try_start_22
    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_16
    .catchall {:try_start_22 .. :try_end_22} :catchall_3

    :catch_16
    move-exception p1

    :try_start_23
    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_17
    .catchall {:try_start_23 .. :try_end_23} :catchall_3

    :catch_17
    move-exception p1

    :try_start_24
    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_31
    :goto_17
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result p1

    sub-int/2addr p1, v5

    invoke-virtual {v3, v5, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p1, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, v5

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v9, v3}, Lcom/jscape/inet/ftp/FtpFile;->setGroup(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Lcom/jscape/inet/ftp/FtpFile;->setOwner(Ljava/lang/String;)V
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_24} :catch_18
    .catchall {:try_start_24 .. :try_end_24} :catchall_3

    :catch_18
    invoke-virtual {v4, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    move v10, v6

    goto :goto_18

    :catchall_3
    move-exception p1

    invoke-virtual {v4, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    throw p1

    :cond_32
    :goto_18
    :try_start_25
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result p1

    if-ge v6, p1, :cond_37

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result p1
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_25} :catch_1c
    .catchall {:try_start_25 .. :try_end_25} :catchall_4

    :try_start_26
    invoke-static {p1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_26} :catch_19
    .catchall {:try_start_26 .. :try_end_26} :catchall_4

    if-eqz v2, :cond_38

    if-eqz v2, :cond_33

    if-eqz v0, :cond_35

    :try_start_27
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_27} :catch_1b
    .catchall {:try_start_27 .. :try_end_27} :catchall_4

    :cond_33
    if-eqz v2, :cond_34

    if-lez v0, :cond_36

    move v10, v6

    goto :goto_19

    :cond_34
    move v10, v0

    :goto_19
    if-nez v2, :cond_37

    :cond_35
    :try_start_28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_28
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_28} :catch_1c
    .catchall {:try_start_28 .. :try_end_28} :catchall_4

    move-object v1, p1

    :cond_36
    add-int/lit8 v6, v6, 0x1

    if-nez v2, :cond_32

    goto :goto_1a

    :catch_19
    move-exception p1

    :try_start_29
    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_29} :catch_1a
    .catchall {:try_start_29 .. :try_end_29} :catchall_4

    :catch_1a
    move-exception p1

    :try_start_2a
    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_2a} :catch_1b
    .catchall {:try_start_2a .. :try_end_2a} :catchall_4

    :catch_1b
    move-exception p1

    :try_start_2b
    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_37
    :goto_1a
    invoke-virtual {v9, v1}, Lcom/jscape/inet/ftp/FtpFile;->setPermission(Ljava/lang/String;)V
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_2b} :catch_1c
    .catchall {:try_start_2b .. :try_end_2b} :catchall_4

    goto :goto_1b

    :catchall_4
    move-exception p1

    invoke-virtual {v4, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    throw p1

    :catch_1c
    :goto_1b
    invoke-virtual {v4, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    :cond_38
    return-object v9
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public getDateTime(Lcom/jscape/inet/ftp/FtpFile;)Ljava/util/Date;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0}, Lcom/jscape/inet/ftp/VMSParser;->getDateTime(Lcom/jscape/inet/ftp/FtpFile;Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    return-object p1
.end method

.method public getDateTime(Lcom/jscape/inet/ftp/FtpFile;Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Date;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/ftp/VMSParser;->b:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getTime()Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_0

    if-eqz v2, :cond_1

    move-object v2, v3

    :cond_0
    if-nez v2, :cond_2

    :cond_1
    const/4 p1, 0x0

    return-object p1

    :cond_2
    new-instance v0, Ljava/text/SimpleDateFormat;

    if-eqz p3, :cond_3

    invoke-direct {v0, v1, p3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    :cond_3
    sget-object p3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, p3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    :goto_0
    if-eqz p2, :cond_4

    :try_start_0
    invoke-virtual {v0, p2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_4
    :goto_1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getDate()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getTime()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    return-object p1
.end method

.method public getFileDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/jscape/inet/ftp/VMSParser;->b:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/jscape/inet/ftp/VMSParser;->b:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public parse(Ljava/io/BufferedReader;)Ljava/util/Enumeration;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, ")"

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v2

    :cond_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    :goto_0
    if-eqz v4, :cond_b

    :try_start_0
    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    if-eqz v2, :cond_1

    if-nez v4, :cond_0

    sget-object v4, Lcom/jscape/inet/ftp/VMSParser;->b:[Ljava/lang/String;

    const/4 v5, 0x6

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    :cond_1
    if-eqz v2, :cond_2

    if-nez v4, :cond_0

    sget-object v4, Lcom/jscape/inet/ftp/VMSParser;->b:[Ljava/lang/String;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    :cond_2
    if-eqz v2, :cond_4

    if-nez v4, :cond_0

    const-string v4, "]"

    if-eqz v2, :cond_3

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    goto :goto_1

    :cond_3
    move-object v6, v4

    move-object v4, v3

    move-object v3, v6

    goto :goto_0

    :cond_4
    :goto_1
    if-nez v4, :cond_0

    :try_start_1
    invoke-virtual {v3, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v2, :cond_5

    if-nez v4, :cond_6

    :try_start_2
    const-string v4, ">"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_5
    if-eqz v4, :cond_7

    :cond_6
    :goto_2
    invoke-direct {p0, v3}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/String;)Lcom/jscape/inet/ftp/FtpFile;

    move-result-object v3

    goto :goto_3

    :cond_7
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_9

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v3, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_9
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :goto_3
    if-eqz v3, :cond_a

    :try_start_3
    invoke-virtual {v1, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_4

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_a
    :goto_4
    if-nez v2, :cond_0

    goto :goto_5

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/VMSParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_b
    :goto_5
    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    return-object p1
.end method
