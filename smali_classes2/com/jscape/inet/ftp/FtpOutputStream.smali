.class public Lcom/jscape/inet/ftp/FtpOutputStream;
.super Ljava/io/OutputStream;


# instance fields
.field private a:Z

.field private b:Ljava/io/OutputStream;

.field private c:Lcom/jscape/inet/ftp/Ftp;


# direct methods
.method public constructor <init>(Lcom/jscape/inet/ftp/Ftp;ZZLjava/lang/String;JZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;,
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpOutputStream;->c:Lcom/jscape/inet/ftp/Ftp;

    :try_start_0
    iput-boolean p3, p0, Lcom/jscape/inet/ftp/FtpOutputStream;->a:Z
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    :try_start_1
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/Ftp;->connect()Lcom/jscape/inet/ftp/Ftp;
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    invoke-virtual {p1, p4, p5, p6, p7}, Lcom/jscape/inet/ftp/Ftp;->getOutputStream(Ljava/lang/String;JZ)Ljava/io/OutputStream;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpOutputStream;->b:Ljava/io/OutputStream;

    :cond_1
    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpOutputStream;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpOutputStream;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object p1

    throw p1
.end method

.method private static a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public close()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ftp/FtpOutputStream;->b:Ljava/io/OutputStream;

    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/FtpOutputStream;->a:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpOutputStream;->c:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->disconnect()V

    :cond_1
    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpOutputStream;->b:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

.method public write(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpOutputStream;->b:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method public write([B)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpOutputStream;->b:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public write([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpOutputStream;->b:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    return-void
.end method
