.class public Lcom/jscape/inet/ftp/FtpProgressEvent;
.super Lcom/jscape/inet/ftp/FtpEvent;


# static fields
.field public static final DOWNLOAD:I = 0x1

.field public static final UPLOAD:I


# instance fields
.field private a:J

.field private c:J

.field private d:J

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:I


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;IJJJ)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/FtpEvent;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->e:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->f:Ljava/lang/String;

    iput p4, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->h:I

    iput-wide p5, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->a:J

    iput-wide p7, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->d:J

    iput-wide p9, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->c:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJ)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/FtpEvent;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->e:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->f:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->g:Ljava/lang/String;

    iput p5, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->h:I

    iput-wide p6, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->a:J

    iput-wide p8, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->d:J

    iput-wide p10, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->c:J

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/inet/ftp/FtpListener;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    invoke-interface {p1, p0}, Lcom/jscape/inet/ftp/FtpListener;->progress(Lcom/jscape/inet/ftp/FtpProgressEvent;)V

    :cond_1
    return-void
.end method

.method public getAbsolutePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getBytes()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->a:J

    return-wide v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalFilePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getMode()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->h:I

    return v0
.end method

.method public getReadBytes()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->d:J

    return-wide v0
.end method

.method public getTotalBytes()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ftp/FtpProgressEvent;->c:J

    return-wide v0
.end method
