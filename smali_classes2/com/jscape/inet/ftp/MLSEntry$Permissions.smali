.class public final Lcom/jscape/inet/ftp/MLSEntry$Permissions;
.super Ljava/lang/Object;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/inet/ftp/MLSEntry$Permissions;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-boolean v0, p1, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->a:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->a:Z

    iget-boolean v0, p1, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->b:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->b:Z

    iget-boolean v0, p1, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->c:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->c:Z

    iget-boolean v0, p1, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->d:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->d:Z

    iget-boolean v0, p1, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->e:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->e:Z

    iget-boolean v0, p1, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->f:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->f:Z

    iget-boolean v0, p1, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->g:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->g:Z

    iget-boolean v0, p1, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->h:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->h:Z

    iget-boolean v0, p1, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->i:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->i:Z

    iget-boolean p1, p1, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->j:Z

    iput-boolean p1, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->j:Z

    return-void
.end method


# virtual methods
.method public isDeletionAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->c:Z

    return v0
.end method

.method public isDirectoryCreationAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->g:Z

    return v0
.end method

.method public isDirectoryEnteringAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->d:Z

    return v0
.end method

.method public isDirectoryPurgingAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->h:Z

    return v0
.end method

.method public isFileAppendingAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->a:Z

    return v0
.end method

.method public isFileCreationAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->b:Z

    return v0
.end method

.method public isFileReadingAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->i:Z

    return v0
.end method

.method public isFileWritingAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->j:Z

    return v0
.end method

.method public isListingAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->f:Z

    return v0
.end method

.method public isRenamingAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->e:Z

    return v0
.end method

.method public setDeletionAllowed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->c:Z

    return-void
.end method

.method public setDirectoryCreationAllowed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->g:Z

    return-void
.end method

.method public setDirectoryEnteringAllowed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->d:Z

    return-void
.end method

.method public setDirectoryPurgingAllowed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->h:Z

    return-void
.end method

.method public setFileAppendingAllowed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->a:Z

    return-void
.end method

.method public setFileCreationAllowed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->b:Z

    return-void
.end method

.method public setFileReadingAllowed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->i:Z

    return-void
.end method

.method public setFileWritingAllowed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->j:Z

    return-void
.end method

.method public setListingAllowed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->f:Z

    return-void
.end method

.method public setRenamingAllowed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->e:Z

    return-void
.end method
