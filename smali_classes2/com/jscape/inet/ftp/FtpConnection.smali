.class public interface abstract Lcom/jscape/inet/ftp/FtpConnection;
.super Ljava/lang/Object;


# virtual methods
.method public abstract close()V
.end method

.method public abstract getHost()Ljava/lang/String;
.end method

.method public abstract getInputStream()Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract getOutputStream()Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract isOpen()Z
.end method

.method public abstract openIncoming(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract openOutgoing(Ljava/lang/String;IIIZZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract openOutgoing(Ljava/lang/String;IIZZILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract prepareIncoming(Lcom/jscape/inet/util/i;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract setCompression(Z)V
.end method

.method public abstract setReceiveBufferSize(I)V
.end method

.method public abstract setSendBufferSize(I)V
.end method
