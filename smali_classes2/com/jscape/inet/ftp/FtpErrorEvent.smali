.class public Lcom/jscape/inet/ftp/FtpErrorEvent;
.super Lcom/jscape/inet/ftp/FtpEvent;


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "e\u0015\u0008`\u0015O Q$\u000e@\tIu\u0003D\u000b\tG\u0018<\u000fA]VK\u001djG"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ftp/FtpErrorEvent;->f:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x39

    goto :goto_1

    :cond_1
    const/16 v4, 0x4b

    goto :goto_1

    :cond_2
    const/16 v4, 0x11

    goto :goto_1

    :cond_3
    const/16 v4, 0x53

    goto :goto_1

    :cond_4
    const/16 v4, 0xe

    goto :goto_1

    :cond_5
    const/16 v4, 0x17

    goto :goto_1

    :cond_6
    const/16 v4, 0x55

    :goto_1
    const/16 v5, 0x76

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/FtpEvent;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ftp/FtpErrorEvent;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/ftp/FtpErrorEvent;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/inet/ftp/FtpErrorEvent;->d:Ljava/lang/String;

    iput p5, p0, Lcom/jscape/inet/ftp/FtpErrorEvent;->e:I

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/inet/ftp/FtpListener;)V
    .locals 0

    return-void
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpErrorEvent;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpErrorEvent;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getMode()J
    .locals 2

    iget v0, p0, Lcom/jscape/inet/ftp/FtpErrorEvent;->e:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpErrorEvent;->c:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/jscape/inet/ftp/FtpErrorEvent;->f:Ljava/lang/String;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/jscape/inet/ftp/FtpErrorEvent;->a:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/jscape/inet/ftp/FtpErrorEvent;->c:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/jscape/inet/ftp/FtpErrorEvent;->d:Ljava/lang/String;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    iget v2, p0, Lcom/jscape/inet/ftp/FtpErrorEvent;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
