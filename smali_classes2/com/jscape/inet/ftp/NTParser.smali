.class public Lcom/jscape/inet/ftp/NTParser;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ftp/FtpFileParser;


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private a:Lcom/jscape/inet/ftp/UnixParser;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "{^XZd1\tf\u0002\u00120\u0008RwXsM1\tf\u000fRwXsM1\tf\u001a\u001d\u007f\u0013q\u001d~\u000fRwXsM1\tf\u001a\u001d\u007f\u0013q\u001d~"

    const/16 v5, 0x34

    move v8, v3

    const/4 v6, -0x1

    const/16 v7, 0x8

    :goto_0
    const/16 v9, 0x27

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x5

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const-string v4, "-p2K\u0019\u0002\u001c>"

    move v8, v11

    move v7, v15

    const/16 v5, 0x8

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x29

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ftp/NTParser;->b:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v10, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    if-eq v1, v15, :cond_4

    const/16 v1, 0x57

    goto :goto_4

    :cond_4
    const/16 v1, 0x3b

    goto :goto_4

    :cond_5
    const/16 v1, 0xe

    goto :goto_4

    :cond_6
    const/16 v1, 0x30

    goto :goto_4

    :cond_7
    const/16 v1, 0x52

    goto :goto_4

    :cond_8
    const/16 v1, 0x1d

    goto :goto_4

    :cond_9
    const/16 v1, 0x38

    :goto_4
    xor-int/2addr v1, v9

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/ftp/NTParser;->a:Lcom/jscape/inet/ftp/UnixParser;

    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/jscape/inet/ftp/FtpFile;
    .locals 5

    new-instance v0, Ljava/util/StringTokenizer;

    invoke-direct {v0, p1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/jscape/inet/ftp/FtpFile;

    const-string v2, ""

    invoke-direct {v1, v2, p1, p0}, Lcom/jscape/inet/ftp/FtpFile;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ftp/FtpFileParser;)V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/jscape/inet/ftp/FtpFile;->setDate(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/jscape/inet/ftp/FtpFile;->setTime(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_0

    sget-object v3, Lcom/jscape/inet/ftp/NTParser;->b:[Ljava/lang/String;

    const/4 v4, 0x5

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/jscape/inet/ftp/FtpFile;->setDirectory(Z)V

    :cond_0
    if-nez v2, :cond_2

    :cond_1
    invoke-virtual {v1, v0}, Lcom/jscape/inet/ftp/FtpFile;->setFilesize(Ljava/lang/String;)V

    :cond_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/NTParser;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/jscape/inet/ftp/FtpFile;->setFilename(Ljava/lang/String;)V

    return-object v1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/NTParser;->c(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private c(Ljava/lang/String;)I
    .locals 8

    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, " "

    const/4 v2, 0x1

    invoke-direct {v0, p1, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object p1

    const/4 v3, 0x0

    move v4, v3

    :cond_0
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz p1, :cond_7

    if-eqz p1, :cond_1

    if-nez v6, :cond_4

    add-int/lit8 v3, v3, 0x1

    move v6, v3

    :cond_1
    const/4 v7, 0x4

    if-eqz p1, :cond_3

    if-ne v6, v7, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    goto :goto_0

    :cond_3
    move v4, v7

    :goto_0
    add-int/2addr v4, v6

    if-nez p1, :cond_5

    :cond_4
    add-int/2addr v4, v2

    :cond_5
    if-nez p1, :cond_0

    :cond_6
    :goto_1
    move v6, v4

    :cond_7
    return v6
.end method


# virtual methods
.method public getDateTime(Lcom/jscape/inet/ftp/FtpFile;)Ljava/util/Date;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0}, Lcom/jscape/inet/ftp/NTParser;->getDateTime(Lcom/jscape/inet/ftp/FtpFile;Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    return-object p1
.end method

.method public getDateTime(Lcom/jscape/inet/ftp/FtpFile;Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Date;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftp/NTParser;->a:Lcom/jscape/inet/ftp/UnixParser;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_3

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftp/NTParser;->a:Lcom/jscape/inet/ftp/UnixParser;
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_2

    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    if-eqz p3, :cond_1

    sget-object v1, Lcom/jscape/inet/ftp/NTParser;->b:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-direct {v0, v1, p3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    goto :goto_0

    :cond_1
    sget-object p3, Lcom/jscape/inet/ftp/NTParser;->b:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object p3, p3, v1

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, p3, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    :goto_0
    if-eqz p2, :cond_2

    :try_start_2
    invoke-virtual {v0, p2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/NTParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getDate()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getTime()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    return-object p1

    :cond_3
    :goto_2
    invoke-virtual {v1, p1, p2, p3}, Lcom/jscape/inet/ftp/UnixParser;->getDateTime(Lcom/jscape/inet/ftp/FtpFile;Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    return-object p1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftp/NTParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/text/ParseException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/NTParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public getFileDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftp/NTParser;->a:Lcom/jscape/inet/ftp/UnixParser;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftp/NTParser;->a:Lcom/jscape/inet/ftp/UnixParser;
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    :cond_0
    :try_start_2
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/jscape/inet/ftp/NTParser;->b:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/jscape/inet/ftp/NTParser;->b:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    :goto_1
    invoke-virtual {v1, p1}, Lcom/jscape/inet/ftp/UnixParser;->getFileDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    return-object p1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftp/NTParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/text/ParseException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/NTParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public parse(Ljava/io/BufferedReader;)Ljava/util/Enumeration;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v1, :cond_5

    if-eqz v2, :cond_4

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, v2}, Lcom/jscape/inet/ftp/NTParser;->a(Ljava/lang/String;)Lcom/jscape/inet/ftp/FtpFile;

    move-result-object v3

    if-eqz v3, :cond_0

    :try_start_0
    invoke-virtual {v0, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/NTParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    if-nez v1, :cond_4

    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v2, Lcom/jscape/inet/ftp/NTParser;->b:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v2, Lcom/jscape/inet/ftp/NTParser;->b:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-nez v1, :cond_2

    :cond_3
    new-instance p1, Ljava/io/StringReader;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/jscape/inet/ftp/UnixParser;

    invoke-direct {v0}, Lcom/jscape/inet/ftp/UnixParser;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ftp/NTParser;->a:Lcom/jscape/inet/ftp/UnixParser;

    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, p1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ftp/UnixParser;->parse(Ljava/io/BufferedReader;)Ljava/util/Enumeration;

    move-result-object p1

    return-object p1

    :cond_4
    move-object v3, p0

    goto :goto_3

    :cond_5
    move-object v3, p0

    :goto_1
    if-eqz v2, :cond_8

    invoke-direct {v3, v2}, Lcom/jscape/inet/ftp/NTParser;->a(Ljava/lang/String;)Lcom/jscape/inet/ftp/FtpFile;

    move-result-object v2

    if-eqz v2, :cond_6

    :try_start_1
    invoke-virtual {v0, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/NTParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_6
    :goto_2
    if-nez v1, :cond_7

    goto :goto_4

    :cond_7
    :goto_3
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_8
    :goto_4
    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    return-object p1
.end method
