.class public Lcom/jscape/inet/ftp/FtpFile;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private a:Ljava/lang/String;

.field private b:J

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:I

.field private l:Lcom/jscape/inet/ftp/FtpFileParser;

.field private m:Ljava/lang/String;

.field private n:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpFile;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpFile;->a:Ljava/lang/String;

    iput-wide p2, p0, Lcom/jscape/inet/ftp/FtpFile;->b:J

    iput-object p4, p0, Lcom/jscape/inet/ftp/FtpFile;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/jscape/inet/ftp/FtpFile;->d:Ljava/lang/String;

    iput-object p6, p0, Lcom/jscape/inet/ftp/FtpFile;->e:Ljava/lang/String;

    iput-object p7, p0, Lcom/jscape/inet/ftp/FtpFile;->f:Ljava/lang/String;

    iput-object p8, p0, Lcom/jscape/inet/ftp/FtpFile;->g:Ljava/lang/String;

    iput-boolean p9, p0, Lcom/jscape/inet/ftp/FtpFile;->h:Z

    iput-boolean p10, p0, Lcom/jscape/inet/ftp/FtpFile;->i:Z

    iput-object p11, p0, Lcom/jscape/inet/ftp/FtpFile;->j:Ljava/lang/String;

    iput p12, p0, Lcom/jscape/inet/ftp/FtpFile;->k:I

    iput-object p13, p0, Lcom/jscape/inet/ftp/FtpFile;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ILjava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpFile;->a:Ljava/lang/String;

    iput-wide p2, p0, Lcom/jscape/inet/ftp/FtpFile;->b:J

    iput-object p4, p0, Lcom/jscape/inet/ftp/FtpFile;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/jscape/inet/ftp/FtpFile;->d:Ljava/lang/String;

    iput-object p6, p0, Lcom/jscape/inet/ftp/FtpFile;->e:Ljava/lang/String;

    iput-object p7, p0, Lcom/jscape/inet/ftp/FtpFile;->f:Ljava/lang/String;

    iput-object p8, p0, Lcom/jscape/inet/ftp/FtpFile;->g:Ljava/lang/String;

    iput-boolean p9, p0, Lcom/jscape/inet/ftp/FtpFile;->h:Z

    iput-boolean p10, p0, Lcom/jscape/inet/ftp/FtpFile;->i:Z

    iput-object p11, p0, Lcom/jscape/inet/ftp/FtpFile;->j:Ljava/lang/String;

    iput p12, p0, Lcom/jscape/inet/ftp/FtpFile;->k:I

    iput-object p13, p0, Lcom/jscape/inet/ftp/FtpFile;->m:Ljava/lang/String;

    iput-boolean p14, p0, Lcom/jscape/inet/ftp/FtpFile;->n:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/jscape/inet/ftp/FtpFileParser;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/FtpFile;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/jscape/inet/ftp/FtpFile;->l:Lcom/jscape/inet/ftp/FtpFileParser;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ftp/FtpFileParser;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/FtpFile;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/jscape/inet/ftp/FtpFile;->m:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/ftp/FtpFile;->l:Lcom/jscape/inet/ftp/FtpFileParser;

    return-void
.end method

.method private static a(Ljava/lang/NumberFormatException;)Ljava/lang/NumberFormatException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public getDate()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpFile;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpFile;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getFilesize()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ftp/FtpFile;->b:J

    return-wide v0
.end method

.method public getFtpFileParser()Lcom/jscape/inet/ftp/FtpFileParser;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpFile;->l:Lcom/jscape/inet/ftp/FtpFileParser;

    return-object v0
.end method

.method public getGroup()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpFile;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getLine()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpFile;->m:Ljava/lang/String;

    return-object v0
.end method

.method public getLinkTarget()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpFile;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getOwner()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpFile;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getPermission()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpFile;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpFile;->g:Ljava/lang/String;

    return-object v0
.end method

.method public isDirectory()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/FtpFile;->h:Z

    return v0
.end method

.method public isHidden()Z
    .locals 2

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isLink()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/FtpFile;->i:Z

    return v0
.end method

.method public isYearSet()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/FtpFile;->n:Z

    return v0
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpFile;->f:Ljava/lang/String;

    return-void
.end method

.method public setDirectory(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftp/FtpFile;->h:Z

    return-void
.end method

.method public setFilename(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpFile;->a:Ljava/lang/String;

    return-void
.end method

.method public setFilesize(Ljava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/jscape/inet/ftp/FtpFile;->b:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/jscape/inet/ftp/FtpFile;->b:J

    :goto_0
    return-void
.end method

.method public setFtpFileParser(Lcom/jscape/inet/ftp/FtpFileParser;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpFile;->l:Lcom/jscape/inet/ftp/FtpFileParser;

    return-void
.end method

.method public setGroup(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpFile;->d:Ljava/lang/String;

    return-void
.end method

.method public setLine(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpFile;->m:Ljava/lang/String;

    return-void
.end method

.method public setLinkTarget(Ljava/lang/String;)V
    .locals 1

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpFile;->j:Ljava/lang/String;

    return-void
.end method

.method public setOwner(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpFile;->c:Ljava/lang/String;

    return-void
.end method

.method public setPermission(Ljava/lang/String;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x64

    :try_start_0
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, 0x1

    const/4 v3, -0x1

    if-eqz v0, :cond_1

    if-eq v1, v3, :cond_0

    :try_start_1
    iput-boolean v2, p0, Lcom/jscape/inet/ftp/FtpFile;->h:Z
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    const/16 v0, 0x6c

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    :cond_1
    if-eq v1, v3, :cond_2

    :try_start_2
    iput-boolean v2, p0, Lcom/jscape/inet/ftp/FtpFile;->i:Z
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpFile;->a(Ljava/lang/NumberFormatException;)Ljava/lang/NumberFormatException;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpFile;->e:Ljava/lang/String;

    return-void

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpFile;->a(Ljava/lang/NumberFormatException;)Ljava/lang/NumberFormatException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpFile;->a(Ljava/lang/NumberFormatException;)Ljava/lang/NumberFormatException;

    move-result-object p1

    throw p1
.end method

.method public setTime(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpFile;->g:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpFile;->m:Ljava/lang/String;

    return-object v0
.end method
