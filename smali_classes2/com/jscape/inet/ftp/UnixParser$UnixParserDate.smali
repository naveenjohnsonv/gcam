.class Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/util/Date;

.field private b:Ljava/lang/String;

.field private c:Z

.field final d:Lcom/jscape/inet/ftp/UnixParser;


# direct methods
.method private constructor <init>(Lcom/jscape/inet/ftp/UnixParser;Ljava/util/Date;Ljava/lang/String;Z)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;->d:Lcom/jscape/inet/ftp/UnixParser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;->a:Ljava/util/Date;

    iput-object p3, p0, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;->b:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;->c:Z

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftp/UnixParser;Ljava/util/Date;Ljava/lang/String;ZLcom/jscape/inet/ftp/UnixParser$1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;-><init>(Lcom/jscape/inet/ftp/UnixParser;Ljava/util/Date;Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public getDate()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;->a:Ljava/util/Date;

    return-object v0
.end method

.method public getFormat()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getFormattedDateAsString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;->getFormat()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;->getDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFormattedDateAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-direct {v0, p1, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;->getDate()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public isYearSet()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;->c:Z

    return v0
.end method

.method public setDate(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;->a:Ljava/util/Date;

    return-void
.end method

.method public setFormat(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;->b:Ljava/lang/String;

    return-void
.end method
