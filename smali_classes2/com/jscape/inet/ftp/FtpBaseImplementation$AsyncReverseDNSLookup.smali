.class Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;
.super Ljava/lang/Thread;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z

.field final c:Lcom/jscape/inet/ftp/FtpBaseImplementation;


# direct methods
.method private constructor <init>(Lcom/jscape/inet/ftp/FtpBaseImplementation;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;->c:Lcom/jscape/inet/ftp/FtpBaseImplementation;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;->a:Ljava/lang/String;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;->b:Z

    iput-object p2, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftp/FtpBaseImplementation;Ljava/lang/String;Lcom/jscape/inet/ftp/FtpBaseImplementation$1;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;-><init>(Lcom/jscape/inet/ftp/FtpBaseImplementation;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method static a(Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;->b:Z

    return p0
.end method

.method static b(Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;->a:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public run()V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    :try_start_0
    iget-object v3, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;->a:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    iput-boolean v2, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;->b:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_1
    :try_start_2
    iput-boolean v1, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;->b:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_0
    :goto_0
    return-void

    :catchall_1
    move-exception v0

    move v1, v2

    :goto_1
    if-nez v1, :cond_1

    :try_start_3
    iput-boolean v2, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;->b:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/FtpBaseImplementation$AsyncReverseDNSLookup;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_2
    throw v0
.end method
