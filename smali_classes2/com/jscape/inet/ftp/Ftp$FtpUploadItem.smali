.class Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;
.super Ljava/lang/Thread;


# static fields
.field private static final n:[Ljava/lang/String;


# instance fields
.field private a:Lcom/jscape/inet/ftp/Ftp;

.field private b:Lcom/jscape/inet/ftp/Ftp;

.field private c:Ljava/lang/String;

.field private d:Ljava/util/Vector;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Ljava/io/File;

.field private h:Ljava/io/File;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Ljava/lang/String;

.field final m:Lcom/jscape/inet/ftp/Ftp;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x4

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x6e

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "&UZ\u001d\u0017=~m=gzc\u00136z;x|d\u0010sl~jhz\rs"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x1c

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move/from16 v16, v5

    move v5, v4

    move/from16 v4, v16

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->n:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    const/16 v14, 0x78

    if-eqz v13, :cond_6

    if-eq v13, v8, :cond_7

    if-eq v13, v0, :cond_5

    const/4 v15, 0x3

    if-eq v13, v15, :cond_4

    if-eq v13, v2, :cond_3

    const/4 v15, 0x5

    if-eq v13, v15, :cond_2

    goto :goto_2

    :cond_2
    const/16 v14, 0x67

    goto :goto_2

    :cond_3
    const/16 v14, 0x62

    goto :goto_2

    :cond_4
    const/16 v14, 0x30

    goto :goto_2

    :cond_5
    const/16 v14, 0x66

    goto :goto_2

    :cond_6
    const/16 v14, 0x10

    :cond_7
    :goto_2
    xor-int v13, v7, v14

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;Ljava/util/Vector;Ljava/lang/String;Ljava/io/File;Z)V
    .locals 1

    iput-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->m:Lcom/jscape/inet/ftp/Ftp;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a:Lcom/jscape/inet/ftp/Ftp;

    iput-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    iput-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->c:Ljava/lang/String;

    iput-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->d:Ljava/util/Vector;

    iput-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->e:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->f:Z

    iput-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->g:Ljava/io/File;

    iput-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->h:Ljava/io/File;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->i:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->j:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->k:Z

    iput-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->l:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a:Lcom/jscape/inet/ftp/Ftp;

    iput-object p3, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->d:Ljava/util/Vector;

    iput-object p5, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->h:Ljava/io/File;

    iput-boolean p7, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->i:Z

    new-instance p1, Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getHostname()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getUsername()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getPassword()Ljava/lang/String;

    move-result-object p5

    invoke-direct {p1, p3, p4, p5}, Lcom/jscape/inet/ftp/Ftp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getTimeZone()Ljava/util/TimeZone;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setTimezone(Ljava/util/TimeZone;)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getPreserveUploadTimestamp()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setPreserveUploadTimestamp(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getPreserveDownloadTimestamp()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setPreserveDownloadTimestamp(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getPassive()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setPassive(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getPort()I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setPort(I)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getConnectBeforeCommand()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setConnectBeforeCommand(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getBlockTransferSize()I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setBlockTransferSize(I)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getCompression()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setCompression(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getDataPortStart()I

    move-result p3

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getDataPortEnd()I

    move-result p4

    invoke-virtual {p1, p3, p4}, Lcom/jscape/inet/ftp/Ftp;->setDataPortRange(II)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getErrorOnSizeCommand()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setErrorOnSizeCommand(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getFtpFileParser()Lcom/jscape/inet/ftp/FtpFileParser;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setFtpFileParser(Lcom/jscape/inet/ftp/FtpFileParser;)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getLinger()I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setLinger(I)V

    :try_start_0
    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getNATAddress()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setNATAddress(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getPortAddress()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setPortAddress(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Lcom/jscape/inet/ftp/Ftp;)Lcom/jscape/inet/ftp/FtpImplementation;

    move-result-object p3

    invoke-virtual {p3}, Lcom/jscape/inet/ftp/FtpImplementation;->getProxyUsername()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Lcom/jscape/inet/ftp/Ftp;)Lcom/jscape/inet/ftp/FtpImplementation;

    move-result-object p4

    invoke-virtual {p4}, Lcom/jscape/inet/ftp/FtpImplementation;->getProxyPassword()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p1, p3, p4}, Lcom/jscape/inet/ftp/Ftp;->setProxyAuthentication(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Lcom/jscape/inet/ftp/Ftp;)Lcom/jscape/inet/ftp/FtpImplementation;

    move-result-object p3

    invoke-virtual {p3}, Lcom/jscape/inet/ftp/FtpImplementation;->getProxyHostname()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Lcom/jscape/inet/ftp/Ftp;)Lcom/jscape/inet/ftp/FtpImplementation;

    move-result-object p4

    invoke-virtual {p4}, Lcom/jscape/inet/ftp/FtpImplementation;->getProxyPort()I

    move-result p4

    invoke-virtual {p1, p3, p4}, Lcom/jscape/inet/ftp/Ftp;->setProxyHost(Ljava/lang/String;I)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Lcom/jscape/inet/ftp/Ftp;)Lcom/jscape/inet/ftp/FtpImplementation;

    move-result-object p3

    invoke-virtual {p3}, Lcom/jscape/inet/ftp/FtpImplementation;->getProxyType()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setProxyType(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getTimeout()I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setTimeout(I)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getUseEPRT()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setUseEPRT(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getUseEPSV()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setUseEPSV(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Lcom/jscape/inet/ftp/Ftp;)Lcom/jscape/inet/ftp/FtpImplementation;

    move-result-object p1

    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Lcom/jscape/inet/ftp/Ftp;)Lcom/jscape/inet/ftp/FtpImplementation;

    move-result-object p2

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/FtpImplementation;->getListeners()Ljava/util/Vector;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->setListeners(Ljava/util/Vector;)V

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;Ljava/util/Vector;Ljava/lang/String;Ljava/io/File;ZLcom/jscape/inet/ftp/Ftp$1;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;-><init>(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;Ljava/util/Vector;Ljava/lang/String;Ljava/io/File;Z)V

    return-void
.end method

.method private static a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public cancelTransfer()V
    .locals 1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->j:Z

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->interrupt()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public error()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->k:Z

    return v0
.end method

.method public getConnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->f:Z

    return v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->l:Ljava/lang/String;

    return-object v0
.end method

.method public interruptTransfer()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->interrupt()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public run()V
    .locals 15

    const-string v0, "/"

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v3}, Lcom/jscape/inet/ftp/Ftp;->connect()Lcom/jscape/inet/ftp/Ftp;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->f:Z

    iget-object v4, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/Ftp;->getMode()I

    move-result v4
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_12
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_16
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    if-ne v4, v3, :cond_0

    :try_start_1
    iget-object v4, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/Ftp;->setAscii()V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_14
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_16
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_4

    :cond_0
    :try_start_2
    iget-object v4, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/Ftp;->getMode()I

    move-result v4
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_15
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_16
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_3

    const/4 v5, 0x2

    goto :goto_0

    :cond_1
    move v5, v3

    :goto_0
    if-ne v4, v5, :cond_2

    :try_start_3
    iget-object v4, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/Ftp;->setBinary()V
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_16
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v1, :cond_4

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :cond_2
    :goto_1
    if-eqz v1, :cond_4

    iget-object v4, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/Ftp;->getMode()I

    move-result v4
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_16
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_3
    if-nez v4, :cond_4

    :try_start_5
    iget-object v4, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v4, v3}, Lcom/jscape/inet/ftp/Ftp;->setAuto(Z)V
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_16
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    :catch_1
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :cond_4
    :goto_2
    iget-object v4, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->d:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v4

    :cond_5
    :goto_3
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_16
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_4
    if-eqz v5, :cond_16

    if-eqz v1, :cond_17

    if-eqz v1, :cond_17

    :try_start_7
    iget-boolean v5, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->j:Z
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_11
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_16
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-nez v5, :cond_16

    :try_start_8
    iget-object v5, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v5}, Lcom/jscape/inet/ftp/Ftp;->reset()V

    iget-object v5, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    iget-object v6, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->e:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->g:Ljava/io/File;

    iget-object v5, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->h:Ljava/io/File;

    invoke-static {v5, v6}, Lcom/jscape/util/Q;->d(Ljava/io/File;Ljava/io/File;)Ljava/lang/String;

    move-result-object v5
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_16
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v1, :cond_6

    if-eqz v5, :cond_6

    if-eqz v1, :cond_6

    :try_start_9
    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_16
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-nez v6, :cond_6

    :try_start_a
    iget-object v6, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-static {v6, v5}, Lcom/jscape/inet/ftp/Ftp;->a(Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;)V

    goto :goto_5

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_16
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_4
    move-exception v0

    :try_start_b
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_16
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_6
    :goto_5
    :try_start_c
    iget-object v5, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->g:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v5
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_10
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_16
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    if-eqz v1, :cond_7

    if-ne v5, v3, :cond_15

    :try_start_d
    iget-object v5, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    iget-object v6, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->c:Ljava/lang/String;

    iget-object v7, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->g:Ljava/io/File;

    invoke-virtual {v5, v6, v7}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/lang/String;Ljava/io/File;)V

    iget-object v5, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->g:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_5
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_16
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    goto :goto_6

    :catch_5
    move-exception v5

    goto/16 :goto_9

    :cond_7
    :goto_6
    if-eqz v1, :cond_8

    if-lez v5, :cond_15

    :try_start_e
    iget-boolean v5, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->i:Z
    :try_end_e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_e .. :try_end_e} :catch_6
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_16
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_7

    :catch_6
    move-exception v5

    :try_start_f
    invoke-static {v5}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v5

    throw v5
    :try_end_f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_f .. :try_end_f} :catch_5
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_16
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :cond_8
    :goto_7
    if-eqz v1, :cond_9

    if-eqz v5, :cond_15

    :try_start_10
    iget-object v5, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a:Lcom/jscape/inet/ftp/Ftp;

    if-eqz v1, :cond_a

    sget-object v6, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->n:[Ljava/lang/String;

    aget-object v6, v6, v2

    invoke-virtual {v5, v6}, Lcom/jscape/inet/ftp/Ftp;->isFeatureSupported(Ljava/lang/String;)Z

    move-result v5
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_7
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_16
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto :goto_8

    :catch_7
    move-exception v5

    :try_start_11
    invoke-static {v5}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v5

    throw v5
    :try_end_11
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_11 .. :try_end_11} :catch_5
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_16
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :cond_9
    :goto_8
    if-eqz v5, :cond_15

    :try_start_12
    iget-object v5, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;
    :try_end_12
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_12 .. :try_end_12} :catch_c
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_16
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :cond_a
    :try_start_13
    invoke-virtual {v5}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v5
    :try_end_13
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_13 .. :try_end_13} :catch_5
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_16
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :try_start_14
    invoke-virtual {v5, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6
    :try_end_14
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_14 .. :try_end_14} :catch_9
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_16
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    if-eqz v1, :cond_c

    if-nez v6, :cond_b

    :try_start_15
    const-string v6, "\\"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6
    :try_end_15
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_15 .. :try_end_15} :catch_b
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_16
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    if-eqz v1, :cond_c

    if-nez v6, :cond_b

    :try_start_16
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_b
    iget-object v6, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    iget-object v7, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->g:Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->g:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v7, v5}, Lcom/jscape/inet/ftp/Ftp;->checksum(Ljava/io/File;Ljava/lang/String;)Z

    move-result v6
    :try_end_16
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_16 .. :try_end_16} :catch_5
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    :cond_c
    if-eqz v6, :cond_d

    goto/16 :goto_b

    :cond_d
    :try_start_17
    new-instance v5, Lcom/jscape/inet/ftp/FtpException;

    sget-object v6, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->n:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-direct {v5, v6}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_17
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_17 .. :try_end_17} :catch_8
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_16
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    :catch_8
    move-exception v5

    :try_start_18
    invoke-static {v5}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v5

    throw v5
    :try_end_18
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_18 .. :try_end_18} :catch_5
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_16
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    :catch_9
    move-exception v5

    :try_start_19
    invoke-static {v5}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v5

    throw v5
    :try_end_19
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_19 .. :try_end_19} :catch_a
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_16
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    :catch_a
    move-exception v5

    :try_start_1a
    invoke-static {v5}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v5

    throw v5
    :try_end_1a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1a .. :try_end_1a} :catch_b
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_16
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    :catch_b
    move-exception v5

    :try_start_1b
    invoke-static {v5}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v5

    throw v5

    :catch_c
    move-exception v5

    invoke-static {v5}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v5

    throw v5
    :try_end_1b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1b .. :try_end_1b} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_16
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    :goto_9
    :try_start_1c
    iget-object v6, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;
    :try_end_1c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1c .. :try_end_1c} :catch_e
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_16
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    if-eqz v1, :cond_f

    :try_start_1d
    invoke-virtual {v6}, Lcom/jscape/inet/ftp/Ftp;->interrupted()Z

    move-result v6
    :try_end_1d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1d .. :try_end_1d} :catch_f
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_16
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    if-eqz v6, :cond_e

    goto/16 :goto_3

    :cond_e
    :try_start_1e
    iput-boolean v3, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->k:Z

    invoke-virtual {v5}, Lcom/jscape/inet/ftp/FtpException;->getMessage()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->l:Ljava/lang/String;

    iget-object v6, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->m:Lcom/jscape/inet/ftp/Ftp;

    :cond_f
    invoke-static {v6}, Lcom/jscape/inet/ftp/Ftp;->a(Lcom/jscape/inet/ftp/Ftp;)Lcom/jscape/inet/ftp/FtpImplementation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/jscape/inet/ftp/FtpImplementation;->getListeners()Ljava/util/Vector;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_10
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_14

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/jscape/inet/ftp/FtpListener;
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_16
    .catchall {:try_start_1e .. :try_end_1e} :catchall_0

    if-eqz v1, :cond_12

    :try_start_1f
    instance-of v8, v7, Lcom/jscape/inet/ftp/FtpErrorListener;
    :try_end_1f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1f .. :try_end_1f} :catch_d
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_16
    .catchall {:try_start_1f .. :try_end_1f} :catchall_0

    if-eqz v1, :cond_11

    if-eqz v8, :cond_13

    goto :goto_a

    :cond_11
    move v5, v8

    goto/16 :goto_4

    :catch_d
    move-exception v0

    :try_start_20
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :cond_12
    :goto_a
    check-cast v7, Lcom/jscape/inet/ftp/FtpErrorListener;

    new-instance v14, Lcom/jscape/inet/ftp/FtpErrorEvent;

    iget-object v8, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->g:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    iget-object v8, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v8}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v11

    iget-object v8, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->g:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move-object v8, v14

    move-object v9, p0

    invoke-direct/range {v8 .. v13}, Lcom/jscape/inet/ftp/FtpErrorEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v7, v14}, Lcom/jscape/inet/ftp/FtpErrorListener;->error(Lcom/jscape/inet/ftp/FtpErrorEvent;)V

    :cond_13
    if-nez v1, :cond_10

    :cond_14
    invoke-virtual {v5}, Lcom/jscape/inet/ftp/FtpException;->printStackTrace()V
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_16
    .catchall {:try_start_20 .. :try_end_20} :catchall_0

    goto :goto_b

    :catch_e
    move-exception v0

    :try_start_21
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_21
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_21 .. :try_end_21} :catch_f
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_16
    .catchall {:try_start_21 .. :try_end_21} :catchall_0

    :catch_f
    move-exception v0

    :try_start_22
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :cond_15
    :goto_b
    if-nez v1, :cond_5

    goto :goto_d

    :catch_10
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :catch_11
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_16
    .catchall {:try_start_22 .. :try_end_22} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_c

    :catch_12
    move-exception v0

    :try_start_23
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_23
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_23 .. :try_end_23} :catch_13
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_16
    .catchall {:try_start_23 .. :try_end_23} :catchall_0

    :catch_13
    move-exception v0

    :try_start_24
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_24
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_24 .. :try_end_24} :catch_14
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_24} :catch_16
    .catchall {:try_start_24 .. :try_end_24} :catchall_0

    :catch_14
    move-exception v0

    :try_start_25
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_25
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_25 .. :try_end_25} :catch_15
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_25} :catch_16
    .catchall {:try_start_25 .. :try_end_25} :catchall_0

    :catch_15
    move-exception v0

    :try_start_26
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_26} :catch_16
    .catchall {:try_start_26 .. :try_end_26} :catchall_0

    :goto_c
    iget-object v1, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/Ftp;->disconnect()V

    iput-boolean v2, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->f:Z

    throw v0

    :catch_16
    :cond_16
    :goto_d
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->b:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->disconnect()V

    :cond_17
    iput-boolean v2, p0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->f:Z

    return-void
.end method
