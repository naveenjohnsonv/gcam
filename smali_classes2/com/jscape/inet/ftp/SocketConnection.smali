.class public Lcom/jscape/inet/ftp/SocketConnection;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ftp/FtpConnection;


# static fields
.field private static final h:[Ljava/lang/String;


# instance fields
.field private a:Ljava/net/Socket;

.field private b:Ljava/net/ServerSocket;

.field private c:Z

.field private d:Ljava/util/zip/DeflaterOutputStream;

.field private e:Ljava/util/zip/InflaterInputStream;

.field private f:I

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "0P\u0015;w\u00154j\u0015%+r\u00168n#\u000e$qM\u000e0P\u0003;r\u001c<h\u0015\u0015\u0011a\u0004`#u\u001e\u00041y\u00193{P\u00041z\u001e8\u007f\u0004\u000e1zP4oP\t1`P-n\u0015\u0017?f\u00159\u0019O\u001f\u00045q\u0004\u001es\u001e\t;w\u00044s\u001eG%g\u001f>w\u0015\u0013c\u0011\u007f\u001f\t0q\u0013)u\u001f\t~w\u001c2o\u0015\u0003\t0P\u0014;f\u00068nM\u000e0P\u00041y\u0000/y\u0003\u00147{\u001e`\r0P\u000e0r\u001c<h\u0015\u0015\u0017zM"

    const/16 v4, 0x9a

    const/16 v5, 0x14

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x25

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x21

    const/16 v3, 0xf

    const-string v5, "T4\"\u001bZ8\u0002^4\"UP+\u0013Y\u0011\u001b{?\u0010Q?4B=*\u0010M\u0008\u001fM>q"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v17, v5

    move v5, v3

    move-object/from16 v3, v17

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0xe

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ftp/SocketConnection;->h:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    const/16 v16, 0x55

    if-eqz v15, :cond_7

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_6

    const/4 v1, 0x3

    if-eq v15, v1, :cond_5

    const/4 v1, 0x4

    if-eq v15, v1, :cond_4

    const/4 v1, 0x5

    if-eq v15, v1, :cond_8

    const/16 v16, 0x78

    goto :goto_4

    :cond_4
    const/16 v16, 0x31

    goto :goto_4

    :cond_5
    const/16 v16, 0x7b

    goto :goto_4

    :cond_6
    const/16 v16, 0x42

    goto :goto_4

    :cond_7
    const/16 v16, 0x39

    :cond_8
    :goto_4
    xor-int v1, v8, v16

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->c:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->d:Ljava/util/zip/DeflaterOutputStream;

    iput-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->e:Ljava/util/zip/InflaterInputStream;

    const/4 v0, -0x1

    iput v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->f:I

    iput v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->g:I

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->b:Ljava/net/ServerSocket;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/net/ServerSocket;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->b:Ljava/net/ServerSocket;

    return-void
.end method

.method private b()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    return-void
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->d:Ljava/util/zip/DeflaterOutputStream;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->d:Ljava/util/zip/DeflaterOutputStream;

    iget-object v1, p0, Lcom/jscape/inet/ftp/SocketConnection;->e:Ljava/util/zip/InflaterInputStream;

    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->e:Ljava/util/zip/InflaterInputStream;

    return-void
.end method

.method private d()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->b:Ljava/net/ServerSocket;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lcom/jscape/inet/ftp/SocketConnection;->h:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private e()V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/SocketConnection;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lcom/jscape/inet/ftp/SocketConnection;->h:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private f()V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/SocketConnection;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lcom/jscape/inet/ftp/SocketConnection;->h:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public close()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ftp/SocketConnection;->c()V

    invoke-direct {p0}, Lcom/jscape/inet/ftp/SocketConnection;->a()V

    invoke-direct {p0}, Lcom/jscape/inet/ftp/SocketConnection;->b()V

    return-void
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    :try_start_0
    invoke-static {}, Ljava/net/InetAddress;->getLocalHost()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const-string v0, ""

    return-object v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/inet/ftp/SocketConnection;->e()V

    if-eqz v0, :cond_1

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/util/zip/InflaterInputStream;

    iget-object v1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->e:Ljava/util/zip/InflaterInputStream;

    :cond_1
    iget-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->e:Ljava/util/zip/InflaterInputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/inet/ftp/SocketConnection;->e()V

    if-eqz v0, :cond_1

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/util/zip/DeflaterOutputStream;

    iget-object v1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->d:Ljava/util/zip/DeflaterOutputStream;

    :cond_1
    iget-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->d:Ljava/util/zip/DeflaterOutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getServerSocket()Ljava/net/ServerSocket;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->b:Ljava/net/ServerSocket;

    return-object v0
.end method

.method public getSocket()Ljava/net/Socket;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    return-object v0
.end method

.method public isOpen()Z
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public openIncoming(II)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ftp/SocketConnection;->d()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/inet/ftp/SocketConnection;->f()V

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftp/SocketConnection;->b:Ljava/net/ServerSocket;

    invoke-virtual {v1}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/net/Socket;->setReuseAddress(Z)V

    iget-object v1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {v1, p1}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-lez p2, :cond_0

    :try_start_2
    iget-object p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    invoke-virtual {p1, v2, p2}, Ljava/net/Socket;->setSoLinger(ZI)V

    if-nez v0, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move p1, v3

    :cond_1
    :try_start_3
    invoke-virtual {v1, p1, v3}, Ljava/net/Socket;->setSoLinger(ZI)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_2
    return-void

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    :catch_3
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public openIncoming(IIZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ftp/SocketConnection;->d()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/inet/ftp/SocketConnection;->f()V

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftp/SocketConnection;->b:Ljava/net/ServerSocket;

    invoke-virtual {v1}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/net/Socket;->setReuseAddress(Z)V

    iget-object v1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    invoke-virtual {v1, p1}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {p1, p3}, Ljava/net/Socket;->setKeepAlive(Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-lez p2, :cond_0

    :try_start_2
    iget-object p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    invoke-virtual {p1, v2, p2}, Ljava/net/Socket;->setSoLinger(ZI)V

    if-nez v0, :cond_2

    :cond_0
    iget-object p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move p3, v1

    :cond_1
    :try_start_3
    invoke-virtual {p1, p3, v1}, Ljava/net/Socket;->setSoLinger(ZI)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_2
    return-void

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    :catch_3
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public openOutgoing(Ljava/lang/String;III)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/inet/ftp/SocketConnection;->f()V

    int-to-long v3, p3

    :try_start_0
    iget v5, p0, Lcom/jscape/inet/ftp/SocketConnection;->f:I

    iget v6, p0, Lcom/jscape/inet/ftp/SocketConnection;->g:I

    move-object v1, p1

    move v2, p2

    invoke-static/range {v1 .. v6}, Lcom/jscape/inet/util/k;->a(Ljava/lang/String;IJII)Ljava/net/Socket;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Ljava/net/Socket;->setReuseAddress(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {p1, p3}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-lez p4, :cond_0

    :try_start_2
    iget-object p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    invoke-virtual {p1, p2, p4}, Ljava/net/Socket;->setSoLinger(ZI)V

    if-nez v0, :cond_2

    :cond_0
    iget-object p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    move p3, v1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {p1, p3, v1}, Ljava/net/Socket;->setSoLinger(ZI)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    return-void

    :catch_2
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public openOutgoing(Ljava/lang/String;IIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object v1, p0

    move/from16 v0, p3

    move/from16 v2, p4

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/jscape/inet/ftp/SocketConnection;->f()V

    int-to-long v6, v0

    move-object v4, p1

    move v5, p2

    move-object/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    :try_start_0
    invoke-static/range {v4 .. v12}, Lcom/jscape/inet/util/k;->a(Ljava/lang/String;IJLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/net/Socket;

    move-result-object v4

    iput-object v4, v1, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/net/Socket;->setReuseAddress(Z)V

    iget-object v4, v1, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v6, 0x0

    if-eqz v3, :cond_1

    :try_start_1
    invoke-virtual {v4, v0}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-lez v2, :cond_0

    :try_start_2
    iget-object v0, v1, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    invoke-virtual {v0, v5, v2}, Ljava/net/Socket;->setSoLinger(ZI)V

    if-nez v3, :cond_2

    :cond_0
    iget-object v4, v1, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    move v0, v6

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    invoke-virtual {v4, v0, v6}, Ljava/net/Socket;->setSoLinger(ZI)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    return-void

    :catch_2
    move-exception v0

    new-instance v2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public openOutgoing(Ljava/lang/String;IIIZZ)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/inet/ftp/SocketConnection;->f()V

    int-to-long v3, p3

    :try_start_0
    iget v5, p0, Lcom/jscape/inet/ftp/SocketConnection;->f:I

    iget v6, p0, Lcom/jscape/inet/ftp/SocketConnection;->g:I

    move-object v1, p1

    move v2, p2

    invoke-static/range {v1 .. v6}, Lcom/jscape/inet/util/k;->a(Ljava/lang/String;IJII)Ljava/net/Socket;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Ljava/net/Socket;->setReuseAddress(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    invoke-virtual {p1, p3}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    invoke-virtual {p1, p5}, Ljava/net/Socket;->setKeepAlive(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    const/4 p3, 0x0

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {p1, p6}, Ljava/net/Socket;->setTcpNoDelay(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-lez p4, :cond_0

    :try_start_2
    iget-object p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    invoke-virtual {p1, p2, p4}, Ljava/net/Socket;->setSoLinger(ZI)V

    if-nez v0, :cond_2

    :cond_0
    iget-object p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    move p6, p3

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {p1, p6, p3}, Ljava/net/Socket;->setSoLinger(ZI)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    return-void

    :catch_2
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public openOutgoing(Ljava/lang/String;IIZZILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object v1, p0

    move/from16 v0, p3

    move/from16 v2, p6

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/jscape/inet/ftp/SocketConnection;->f()V

    int-to-long v6, v0

    move-object v4, p1

    move v5, p2

    move-object/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    :try_start_0
    invoke-static/range {v4 .. v12}, Lcom/jscape/inet/util/k;->a(Ljava/lang/String;IJLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/net/Socket;

    move-result-object v4

    iput-object v4, v1, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/net/Socket;->setReuseAddress(Z)V

    iget-object v4, v1, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    invoke-virtual {v4, v0}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object v0, v1, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    move/from16 v4, p4

    invoke-virtual {v0, v4}, Ljava/net/Socket;->setKeepAlive(Z)V

    iget-object v0, v1, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    move/from16 v6, p5

    :try_start_1
    invoke-virtual {v0, v6}, Ljava/net/Socket;->setTcpNoDelay(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-lez v2, :cond_0

    :try_start_2
    iget-object v0, v1, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    invoke-virtual {v0, v5, v2}, Ljava/net/Socket;->setSoLinger(ZI)V

    if-nez v3, :cond_2

    :cond_0
    iget-object v0, v1, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    move v6, v4

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/ftp/SocketConnection;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    move/from16 v6, p5

    :goto_0
    invoke-virtual {v0, v6, v4}, Ljava/net/Socket;->setSoLinger(ZI)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    return-void

    :catch_2
    move-exception v0

    new-instance v2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public prepareIncoming(Lcom/jscape/inet/util/i;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ftp/SocketConnection;->f()V

    :try_start_0
    invoke-static {p1}, Lcom/jscape/inet/util/h;->a(Lcom/jscape/inet/util/i;)Ljava/net/ServerSocket;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->b:Ljava/net/ServerSocket;

    invoke-virtual {p1, p2}, Ljava/net/ServerSocket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public setCompression(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->c:Z

    return-void
.end method

.method public setReceiveBufferSize(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->g:I

    return-void
.end method

.method public setSendBufferSize(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/ftp/SocketConnection;->f:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftp/SocketConnection;->h:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ftp/SocketConnection;->a:Ljava/net/Socket;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ftp/SocketConnection;->b:Ljava/net/ServerSocket;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/ftp/SocketConnection;->c:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ftp/SocketConnection;->d:Ljava/util/zip/DeflaterOutputStream;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ftp/SocketConnection;->e:Ljava/util/zip/InflaterInputStream;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/ftp/SocketConnection;->f:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/ftp/SocketConnection;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
