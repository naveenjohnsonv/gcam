.class public Lcom/jscape/inet/ftp/Fxp;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcom/jscape/util/m/g;

.field private static final c:[Ljava/lang/String;


# instance fields
.field private b:Ljava/util/Vector;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x4

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x28

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "\u0004\u00075\u001b\u0005\u0004\t4\u0019a"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0xa

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ftp/Fxp;->c:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/util/m/d;

    invoke-direct {v0}, Lcom/jscape/util/m/d;-><init>()V

    sput-object v0, Lcom/jscape/inet/ftp/Fxp;->a:Lcom/jscape/util/m/g;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    if-eqz v13, :cond_7

    if-eq v13, v8, :cond_6

    if-eq v13, v0, :cond_5

    const/4 v14, 0x3

    if-eq v13, v14, :cond_4

    if-eq v13, v2, :cond_3

    const/4 v14, 0x5

    if-eq v13, v14, :cond_2

    const/16 v13, 0x4b

    goto :goto_2

    :cond_2
    const/16 v13, 0x13

    goto :goto_2

    :cond_3
    const/16 v13, 0x69

    goto :goto_2

    :cond_4
    const/16 v13, 0x65

    goto :goto_2

    :cond_5
    const/16 v13, 0x4e

    goto :goto_2

    :cond_6
    const/16 v13, 0x6e

    goto :goto_2

    :cond_7
    const/16 v13, 0x7c

    :goto_2
    xor-int/2addr v13, v7

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ftp/Fxp;->b:Ljava/util/Vector;

    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/jscape/util/m/f;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jscape/inet/ftp/Fxp;->a:Lcom/jscape/util/m/g;

    invoke-interface {v0, p1}, Lcom/jscape/util/m/g;->a(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    :try_start_1
    sget-object p1, Lcom/jscape/util/m/f;->a:Lcom/jscape/util/m/f;

    :goto_0
    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Fxp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-direct {v0, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/Ftp;->isConnected()Z

    move-result v1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    :try_start_1
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/Ftp;->connect()Lcom/jscape/inet/ftp/Ftp;
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    if-eqz v0, :cond_2

    :try_start_2
    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->isConnected()Z

    move-result v1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Fxp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    if-nez v1, :cond_2

    :try_start_3
    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->connect()Lcom/jscape/inet/ftp/Ftp;
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Fxp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    return-void

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftp/Fxp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Fxp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method


# virtual methods
.method public addFxpListener(Lcom/jscape/inet/ftp/FxpListener;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Fxp;->b:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    monitor-enter p0

    :try_start_0
    iput-object v0, p0, Lcom/jscape/inet/ftp/Fxp;->b:Ljava/util/Vector;

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method protected fireFxpEnd(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/jscape/inet/ftp/FxpEndEvent;

    invoke-direct {v0, p1, p2, p3}, Lcom/jscape/inet/ftp/FxpEndEvent;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    :cond_0
    iget-object p3, p0, Lcom/jscape/inet/ftp/Fxp;->b:Ljava/util/Vector;

    invoke-virtual {p3}, Ljava/util/Vector;->size()I

    move-result p3

    if-ge p2, p3, :cond_1

    iget-object p3, p0, Lcom/jscape/inet/ftp/Fxp;->b:Ljava/util/Vector;

    invoke-virtual {p3, p2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/jscape/inet/ftp/FxpListener;

    invoke-interface {p3, v0}, Lcom/jscape/inet/ftp/FxpListener;->fxpEnd(Lcom/jscape/inet/ftp/FxpEndEvent;)V

    add-int/lit8 p2, p2, 0x1

    if-nez p1, :cond_0

    :cond_1
    return-void
.end method

.method protected fireFxpFailed(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    new-instance v0, Lcom/jscape/inet/ftp/FxpFailedEvent;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/jscape/inet/ftp/FxpFailedEvent;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    :cond_0
    iget-object p3, p0, Lcom/jscape/inet/ftp/Fxp;->b:Ljava/util/Vector;

    invoke-virtual {p3}, Ljava/util/Vector;->size()I

    move-result p3

    if-ge p2, p3, :cond_1

    iget-object p3, p0, Lcom/jscape/inet/ftp/Fxp;->b:Ljava/util/Vector;

    invoke-virtual {p3, p2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/jscape/inet/ftp/FxpListener;

    invoke-interface {p3, v0}, Lcom/jscape/inet/ftp/FxpListener;->fxpFailed(Lcom/jscape/inet/ftp/FxpFailedEvent;)V

    add-int/lit8 p2, p2, 0x1

    if-nez p1, :cond_0

    :cond_1
    return-void
.end method

.method protected fireFxpStart(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/jscape/inet/ftp/FxpStartEvent;

    invoke-direct {v0, p1, p2, p3}, Lcom/jscape/inet/ftp/FxpStartEvent;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    :cond_0
    iget-object p3, p0, Lcom/jscape/inet/ftp/Fxp;->b:Ljava/util/Vector;

    invoke-virtual {p3}, Ljava/util/Vector;->size()I

    move-result p3

    if-ge p2, p3, :cond_1

    iget-object p3, p0, Lcom/jscape/inet/ftp/Fxp;->b:Ljava/util/Vector;

    invoke-virtual {p3, p2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/jscape/inet/ftp/FxpListener;

    invoke-interface {p3, v0}, Lcom/jscape/inet/ftp/FxpListener;->fxpStart(Lcom/jscape/inet/ftp/FxpStartEvent;)V

    add-int/lit8 p2, p2, 0x1

    if-nez p1, :cond_0

    :cond_1
    return-void
.end method

.method public mtransfer(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0, p3}, Lcom/jscape/inet/ftp/Fxp;->a(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p3

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/Ftp;->getDirListing()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ftp/FtpFile;

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v2
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    if-nez v2, :cond_2

    :try_start_1
    invoke-interface {p3, v3}, Lcom/jscape/util/m/f;->a(Ljava/lang/String;)Z

    move-result v2
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_1
    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {p0, p1, p2, v3}, Lcom/jscape/inet/ftp/Fxp;->transfer(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Fxp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    if-nez v0, :cond_0

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftp/Fxp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Fxp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    return-void
.end method

.method public removeFxpListener(Lcom/jscape/inet/ftp/FxpListener;)V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ftp/Fxp;->b:Ljava/util/Vector;

    if-eqz v0, :cond_1

    invoke-virtual {v1, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Fxp;->b:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Vector;

    :cond_1
    invoke-virtual {v1, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    monitor-enter p0

    :try_start_0
    iput-object v1, p0, Lcom/jscape/inet/ftp/Fxp;->b:Ljava/util/Vector;

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public transfer(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ftp/Fxp;->a(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;)V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/inet/ftp/Fxp;->fireFxpStart(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;)V

    sget-object v1, Lcom/jscape/inet/ftp/Fxp;->c:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ftp/Ftp;->issueCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/util/StringTokenizer;

    invoke-direct {v3, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x4

    if-ge v2, v1, :cond_1

    :try_start_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_2

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Fxp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    :cond_2
    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/jscape/inet/ftp/Fxp;->c:[Ljava/lang/String;

    aget-object v3, v4, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/jscape/inet/ftp/Ftp;->issueCommand(Ljava/lang/String;)Ljava/lang/String;

    new-instance v1, Lcom/jscape/inet/ftp/FxpRetr;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/jscape/inet/ftp/FxpRetr;-><init>(Lcom/jscape/inet/ftp/Fxp;Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/FxpRetr;->start()V

    new-instance v1, Lcom/jscape/inet/ftp/FxpStor;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/jscape/inet/ftp/FxpStor;-><init>(Lcom/jscape/inet/ftp/Fxp;Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/FxpStor;->start()V

    :cond_3
    :goto_1
    invoke-virtual {v1}, Lcom/jscape/inet/ftp/FxpStor;->isAlive()Z

    move-result v2

    if-eqz v2, :cond_4

    const-wide/16 v2, 0x64

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v0, :cond_5

    goto :goto_1

    :catch_1
    if-nez v0, :cond_3

    :cond_4
    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/inet/ftp/Fxp;->fireFxpEnd(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method public transferDir(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ftp/Fxp;->a(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;)V

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p2, p3}, Lcom/jscape/inet/ftp/Ftp;->makeDir(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    invoke-virtual {p2, p3}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    :try_start_1
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/Ftp;->getDirListing()Ljava/util/Enumeration;

    move-result-object p3

    :cond_0
    invoke-interface {p3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {p3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/ftp/FtpFile;
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_5

    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p1, p2, v4}, Lcom/jscape/inet/ftp/Fxp;->transfer(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_1
    move-exception p3

    :try_start_3
    invoke-static {p3}, Lcom/jscape/inet/ftp/Fxp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p3

    throw p3
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    :goto_0
    if-nez v1, :cond_3

    :cond_2
    :try_start_4
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p1, p2, v3}, Lcom/jscape/inet/ftp/Fxp;->transferDir(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_2
    move-exception p3

    :try_start_5
    invoke-static {p3}, Lcom/jscape/inet/ftp/Fxp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p3

    throw p3
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_3
    :goto_1
    if-nez v1, :cond_0

    :cond_4
    invoke-virtual {p1, v0}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    :cond_5
    const/4 p1, 0x0

    goto :goto_2

    :catchall_0
    move-exception p3

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    throw p3

    :catch_3
    move-exception p3

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    move-object p1, p3

    :goto_2
    if-eqz v1, :cond_6

    if-nez p1, :cond_6

    return-void

    :cond_6
    throw p1
.end method
