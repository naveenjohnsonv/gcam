.class public final Lcom/jscape/inet/ftp/MLSTRequest;
.super Lcom/jscape/inet/ftp/AbstractRequest;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lcom/jscape/inet/ftp/MLSDParser;

.field private static final f:[Ljava/lang/String;


# instance fields
.field private c:Ljava/lang/String;

.field private d:Lcom/jscape/inet/ftp/FtpFile;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "\u0014J\u0000U\u0005\u0014J\u0000UZ"

    const/16 v5, 0xa

    move v7, v0

    const/4 v6, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x61

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    const/4 v15, 0x3

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x16

    const/16 v4, 0x11

    const-string v6, "b\u001aW\u0012d;t\u000b\u0006D\u0000x=~X\u0011\u000f\u0004f8r\'"

    move v7, v4

    move-object v4, v6

    move v8, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x13

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ftp/MLSTRequest;->f:[Ljava/lang/String;

    aget-object v0, v1, v15

    sput-object v0, Lcom/jscape/inet/ftp/MLSTRequest;->a:Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/ftp/MLSDParser;

    invoke-direct {v0}, Lcom/jscape/inet/ftp/MLSDParser;-><init>()V

    sput-object v0, Lcom/jscape/inet/ftp/MLSTRequest;->b:Lcom/jscape/inet/ftp/MLSDParser;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    if-eq v2, v15, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    goto :goto_4

    :cond_4
    const/16 v15, 0x41

    goto :goto_4

    :cond_5
    const/16 v15, 0x1b

    goto :goto_4

    :cond_6
    const/16 v15, 0x60

    goto :goto_4

    :cond_7
    const/16 v15, 0x32

    goto :goto_4

    :cond_8
    const/16 v15, 0x67

    goto :goto_4

    :cond_9
    const/16 v15, 0x38

    :goto_4
    xor-int v2, v9, v15

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ftp/AbstractRequest;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public execute(Lcom/jscape/inet/ftp/ClientProtocolInterpreter;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/MLSTRequest;->assertIsValid()V

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftp/MLSTRequest;->c:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftp/MLSTRequest;->f:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ftp/MLSTRequest;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/jscape/inet/ftp/MLSTRequest;->f:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    :cond_1
    :goto_0
    invoke-interface {p1, v1}, Lcom/jscape/inet/ftp/ClientProtocolInterpreter;->sendCommand(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/jscape/inet/ftp/ClientProtocolInterpreter;->receiveReply()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/MLSTRequest;->assertIsPositive(Ljava/lang/String;)V

    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object p1

    if-eqz v0, :cond_3

    if-eqz p1, :cond_2

    sget-object v0, Lcom/jscape/inet/ftp/MLSTRequest;->b:Lcom/jscape/inet/ftp/MLSDParser;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/MLSDParser;->parse(Ljava/lang/String;)Lcom/jscape/inet/ftp/FtpFile;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ftp/MLSTRequest;->d:Lcom/jscape/inet/ftp/FtpFile;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :cond_2
    :try_start_3
    new-instance p1, Ljava/lang/Exception;

    sget-object v0, Lcom/jscape/inet/ftp/MLSTRequest;->f:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSTRequest;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :cond_3
    :goto_1
    return-void

    :catch_1
    move-exception p1

    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catch_2
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSTRequest;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSTRequest;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public getFile()Lcom/jscape/inet/ftp/FtpFile;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSTRequest;->d:Lcom/jscape/inet/ftp/FtpFile;

    return-object v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSTRequest;->c:Ljava/lang/String;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSTRequest;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setFilename(Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ftp/MLSTRequest;->c:Ljava/lang/String;

    return-void
.end method
