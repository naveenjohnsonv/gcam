.class public Lcom/jscape/inet/ftp/FtpInputStream;
.super Ljava/io/InputStream;


# instance fields
.field private a:Lcom/jscape/inet/ftp/Ftp;

.field private b:Z

.field private c:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Lcom/jscape/inet/ftp/Ftp;ZZLjava/lang/String;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpInputStream;->a:Lcom/jscape/inet/ftp/Ftp;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/Ftp;->connect()Lcom/jscape/inet/ftp/Ftp;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpInputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    iget-object p1, p0, Lcom/jscape/inet/ftp/FtpInputStream;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p1, p4, p5, p6}, Lcom/jscape/inet/ftp/Ftp;->getInputStream(Ljava/lang/String;J)Ljava/io/InputStream;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpInputStream;->c:Ljava/io/InputStream;

    iput-boolean p3, p0, Lcom/jscape/inet/ftp/FtpInputStream;->b:Z

    :cond_1
    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public declared-synchronized available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpInputStream;->c:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public close()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ftp/FtpInputStream;->c:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/FtpInputStream;->b:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpInputStream;->a:Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->disconnect()V

    :cond_1
    return-void
.end method

.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpInputStream;->c:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    return v0
.end method

.method public read([B)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpInputStream;->c:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I

    move-result p1

    return p1
.end method

.method public read([BII)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpInputStream;->c:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result p1

    return p1
.end method

.method public declared-synchronized skip(J)J
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpInputStream;->c:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
