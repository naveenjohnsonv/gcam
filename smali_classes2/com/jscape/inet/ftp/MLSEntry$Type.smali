.class public final Lcom/jscape/inet/ftp/MLSEntry$Type;
.super Ljava/lang/Object;


# static fields
.field public static final CURRENT_DIRECTORY:Lcom/jscape/inet/ftp/MLSEntry$Type;

.field public static final DIRECTORY:Lcom/jscape/inet/ftp/MLSEntry$Type;

.field public static final FILE:Lcom/jscape/inet/ftp/MLSEntry$Type;

.field public static final PARENT_DIRECTORY:Lcom/jscape/inet/ftp/MLSEntry$Type;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x3

    const/4 v4, 0x0

    const-string v5, "e:%\u0004q7>C"

    const/16 v6, 0x8

    move v8, v3

    move v9, v4

    const/4 v7, -0x1

    :goto_0
    const/16 v10, 0x38

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    move v15, v4

    :goto_2
    const/16 v16, 0x9

    const/4 v2, 0x2

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_1

    add-int/lit8 v2, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v2

    goto :goto_0

    :cond_0
    const-string v5, "\u0016CJ7\u0004\u0013NO "

    move v8, v0

    move v9, v2

    move/from16 v6, v16

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v2

    move v9, v12

    :goto_3
    const/16 v10, 0x4c

    add-int/2addr v7, v11

    add-int v2, v7, v8

    invoke-virtual {v5, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v13, v4

    goto :goto_1

    :cond_2
    new-instance v0, Lcom/jscape/inet/ftp/MLSEntry$Type;

    aget-object v3, v1, v3

    invoke-direct {v0, v3}, Lcom/jscape/inet/ftp/MLSEntry$Type;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ftp/MLSEntry$Type;->FILE:Lcom/jscape/inet/ftp/MLSEntry$Type;

    new-instance v0, Lcom/jscape/inet/ftp/MLSEntry$Type;

    aget-object v2, v1, v2

    invoke-direct {v0, v2}, Lcom/jscape/inet/ftp/MLSEntry$Type;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ftp/MLSEntry$Type;->CURRENT_DIRECTORY:Lcom/jscape/inet/ftp/MLSEntry$Type;

    new-instance v0, Lcom/jscape/inet/ftp/MLSEntry$Type;

    aget-object v2, v1, v11

    invoke-direct {v0, v2}, Lcom/jscape/inet/ftp/MLSEntry$Type;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ftp/MLSEntry$Type;->PARENT_DIRECTORY:Lcom/jscape/inet/ftp/MLSEntry$Type;

    new-instance v0, Lcom/jscape/inet/ftp/MLSEntry$Type;

    aget-object v1, v1, v4

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/MLSEntry$Type;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ftp/MLSEntry$Type;->DIRECTORY:Lcom/jscape/inet/ftp/MLSEntry$Type;

    return-void

    :cond_3
    aget-char v17, v12, v15

    rem-int/lit8 v4, v15, 0x7

    if-eqz v4, :cond_8

    if-eq v4, v11, :cond_7

    if-eq v4, v2, :cond_6

    if-eq v4, v3, :cond_9

    if-eq v4, v0, :cond_5

    const/4 v2, 0x5

    if-eq v4, v2, :cond_4

    const/16 v16, 0x2d

    goto :goto_4

    :cond_4
    const/16 v16, 0x4b

    goto :goto_4

    :cond_5
    const/16 v16, 0xb

    goto :goto_4

    :cond_6
    const/16 v16, 0x6f

    goto :goto_4

    :cond_7
    const/16 v16, 0x6b

    goto :goto_4

    :cond_8
    const/16 v16, 0x39

    :cond_9
    :goto_4
    xor-int v2, v10, v16

    xor-int v2, v17, v2

    int-to-char v2, v2

    aput-char v2, v12, v15

    add-int/lit8 v15, v15, 0x1

    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, ""

    invoke-direct {p0, v0, p1}, Lcom/jscape/inet/ftp/MLSEntry$Type;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/inet/ftp/MLSEntry$Type;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ftp/MLSEntry$Type;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Type;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getOsName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSEntry$Type;->a:Ljava/lang/String;

    return-object v0
.end method
