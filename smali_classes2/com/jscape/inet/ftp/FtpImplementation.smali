.class public abstract Lcom/jscape/inet/ftp/FtpImplementation;
.super Ljava/lang/Object;


# static fields
.field protected static final DEFAULT_FTP_PORT:I = 0x15

.field protected static final DEFAULT_KEEPALIVE:Z = false

.field protected static final DEFAULT_SFTP_PORT:I = 0x16

.field protected static final DEFAULT_TIMEOUT:I = 0x7530

.field private static final L:[Ljava/lang/String;

.field private static final a:I = 0x2000

.field private static final b:I = -0x1

.field private static final d:I = 0x438

.field private static final e:Ljava/lang/String;


# instance fields
.field protected account:Ljava/lang/String;

.field protected autoDetectIpv6:Z

.field protected autoMode:Z

.field protected blockSize:I

.field private c:Ljava/util/Vector;

.field protected debug:Z

.field protected debugStream:Ljava/io/PrintStream;

.field protected encoding:Ljava/lang/String;

.field protected encodingDisk:Ljava/lang/String;

.field protected encodingWire:Ljava/lang/String;

.field private f:Z

.field protected ftp:Lcom/jscape/inet/ftp/Ftp;

.field private g:Z

.field private h:Ljava/util/TimeZone;

.field protected host:Ljava/lang/String;

.field protected volatile interrupted:Z

.field protected keepAlive:Z

.field protected linger:I

.field protected password:Ljava/lang/String;

.field protected port:I

.field protected portAddress:Ljava/lang/String;

.field protected proxyHostname:Ljava/lang/String;

.field protected proxyPassword:Ljava/lang/String;

.field protected proxyPort:I

.field protected proxyType:Ljava/lang/String;

.field protected proxyUsername:Ljava/lang/String;

.field protected targetFile:Ljava/lang/String;

.field protected targetPath:Ljava/lang/String;

.field protected tcpNoDelay:Z

.field protected timeout:I

.field protected user:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "gjdu!\u000c\u0006gjdu!\u000c\u000bddtmR\u0013\u0015\u001e\u000f\r\u0014\u00029/\u0003shs\u0002\u001a\u000b\u0002\u001a\u000b"

    const/16 v4, 0x26

    const/4 v5, 0x6

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x16

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x1e

    const/16 v3, 0x19

    const-string v5, "6\"=%`l(-?1 \'?#;5r;f?9$4hk\u0004\u0011\u0011\u0001\u0018"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x63

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ftp/FtpImplementation;->L:[Ljava/lang/String;

    aget-object v0, v0, v2

    sput-object v0, Lcom/jscape/inet/ftp/FtpImplementation;->e:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x29

    goto :goto_4

    :cond_4
    const/16 v1, 0x2f

    goto :goto_4

    :cond_5
    const/16 v1, 0x64

    goto :goto_4

    :cond_6
    const/16 v1, 0x28

    goto :goto_4

    :cond_7
    const/16 v1, 0x31

    goto :goto_4

    :cond_8
    const/16 v1, 0x33

    goto :goto_4

    :cond_9
    const/16 v1, 0x22

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->c:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->ftp:Lcom/jscape/inet/ftp/Ftp;

    const/16 v1, 0x7530

    iput v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->timeout:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->keepAlive:Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/jscape/inet/ftp/FtpImplementation;->tcpNoDelay:Z

    const/4 v2, -0x1

    iput v2, p0, Lcom/jscape/inet/ftp/FtpImplementation;->linger:I

    const/16 v2, 0x2000

    iput v2, p0, Lcom/jscape/inet/ftp/FtpImplementation;->blockSize:I

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->host:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->portAddress:Ljava/lang/String;

    iput v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->port:I

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->user:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->password:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->account:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->encoding:Ljava/lang/String;

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v2

    iput-boolean v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted:Z

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->targetFile:Ljava/lang/String;

    :try_start_0
    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->targetPath:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->debug:Z

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iput-object v3, p0, Lcom/jscape/inet/ftp/FtpImplementation;->debugStream:Ljava/io/PrintStream;

    iput-boolean v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->autoMode:Z

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->encodingWire:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->encodingDisk:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->autoDetectIpv6:Z

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->proxyUsername:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->proxyPassword:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->proxyHostname:Ljava/lang/String;

    const/16 v0, 0x438

    iput v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->proxyPort:I

    sget-object v0, Lcom/jscape/inet/ftp/FtpImplementation;->L:[Ljava/lang/String;

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->proxyType:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->f:Z

    iput-boolean v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->g:Z

    sget-object v0, Lcom/jscape/inet/ftp/FtpImplementation;->L:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->h:Ljava/util/TimeZone;

    if-nez v2, :cond_0

    new-array v0, v1, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private static b(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public addListener(Lcom/jscape/inet/ftp/FtpListener;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    monitor-enter p0

    :try_start_0
    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->c:Ljava/util/Vector;

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public abstract changePassword(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public clearProxySettings()V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Lcom/jscape/inet/ftp/FtpImplementation;->setProxyAuthentication(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x438

    invoke-virtual {p0, v0, v1}, Lcom/jscape/inet/ftp/FtpImplementation;->setProxyHost(Ljava/lang/String;I)V

    sget-object v0, Lcom/jscape/inet/ftp/FtpImplementation;->L:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftp/FtpImplementation;->setProxyType(Ljava/lang/String;)V

    return-void
.end method

.method public abstract connect()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract connect(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method protected declared-synchronized debugPrint(Ljava/lang/String;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->debug:Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_3

    if-eqz v0, :cond_0

    if-eqz p1, :cond_3

    :try_start_2
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :cond_0
    move-object v1, p1

    :goto_0
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_3

    :try_start_4
    sget-object v1, Lcom/jscape/inet/ftp/FtpImplementation;->L:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_1
    if-eqz v1, :cond_2

    :try_start_5
    iget-object v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->debugStream:Ljava/io/PrintStream;

    sget-object v2, Lcom/jscape/inet/ftp/FtpImplementation;->L:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-nez v0, :cond_3

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->debugStream:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    :catch_1
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catch_2
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catch_3
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :cond_3
    :goto_2
    monitor-exit p0

    return-void

    :catch_4
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Ljava/lang/NullPointerException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_5
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Ljava/lang/NullPointerException; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catch_6
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public abstract deleteDir(Ljava/lang/String;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method protected declared-synchronized deleteDirRecursive(Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDir()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setDir(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    const-string v2, ""

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDirListing(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v2

    const/4 v3, 0x0

    move v4, v3

    :cond_0
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v4, v5, :cond_5

    invoke-virtual {v2, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/jscape/inet/ftp/FtpFile;

    invoke-virtual {v5}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v5}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v5
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_6

    if-eqz v1, :cond_2

    if-nez v5, :cond_1

    :try_start_3
    invoke-virtual {p0, v6}, Lcom/jscape/inet/ftp/FtpImplementation;->deleteFile(Ljava/lang/String;)V

    if-nez v1, :cond_4

    :cond_1
    const-string v5, "."

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    goto :goto_3

    :cond_2
    :goto_0
    if-eqz v1, :cond_3

    if-nez v5, :cond_4

    :try_start_4
    sget-object v5, Lcom/jscape/inet/ftp/FtpImplementation;->L:[Ljava/lang/String;

    const/4 v7, 0x5

    aget-object v5, v5, v7

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_3
    :goto_1
    if-nez v5, :cond_4

    const/4 v5, 0x1

    :try_start_6
    invoke-virtual {p0, v6, v5}, Lcom/jscape/inet/ftp/FtpImplementation;->deleteDir(Ljava/lang/String;Z)V
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    :catch_2
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_4
    :goto_2
    add-int/lit8 v4, v4, 0x1

    if-nez v1, :cond_0

    goto :goto_4

    :catch_3
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catch_4
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_5
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :goto_3
    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_5
    :goto_4
    :try_start_c
    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftp/FtpImplementation;->setDir(Ljava/lang/String;)V

    const-string v0, "."

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :cond_6
    if-eqz v1, :cond_7

    if-nez v5, :cond_8

    :try_start_d
    sget-object v0, Lcom/jscape/inet/ftp/FtpImplementation;->L:[Ljava/lang/String;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto :goto_5

    :catch_6
    move-exception p1

    :try_start_e
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    :cond_7
    :goto_5
    if-nez v5, :cond_8

    :try_start_f
    invoke-virtual {p0, p1, v3}, Lcom/jscape/inet/ftp/FtpImplementation;->deleteDir(Ljava/lang/String;Z)V
    :try_end_f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_f .. :try_end_f} :catch_7
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    goto :goto_6

    :catch_7
    move-exception p1

    :try_start_10
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    :cond_8
    :goto_6
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    :try_start_11
    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftp/FtpImplementation;->setDir(Ljava/lang/String;)V

    throw p1
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public abstract deleteFile(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract dirUp()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract disconnect()V
.end method

.method public abstract download(Ljava/io/OutputStream;Ljava/lang/String;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method protected declared-synchronized fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->c:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ftp/FtpListener;

    invoke-virtual {p1, v2}, Lcom/jscape/inet/ftp/FtpEvent;->accept(Lcom/jscape/inet/ftp/FtpListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getAccount()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->account:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAutoDetectIpv6()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->autoDetectIpv6:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getBlockTransferSize()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->blockSize:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract getCompression()Z
.end method

.method public abstract getConnectBeforeCommand()Z
.end method

.method public abstract getDataPort()I
.end method

.method public abstract getDataPortEnd()I
.end method

.method public abstract getDataPortStart()I
.end method

.method public declared-synchronized getDebug()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->debug:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDebugStream()Ljava/io/PrintStream;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->debugStream:Ljava/io/PrintStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract getDir()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract getDirListing(Ljava/lang/String;)Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public declared-synchronized getDirListingAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDirListing(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    const/4 v2, 0x0

    :cond_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v2, v3, :cond_1

    :try_start_1
    invoke-virtual {p1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v3, Lcom/jscape/inet/ftp/FtpImplementation;->L:[Ljava/lang/String;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_1

    add-int/lit8 v2, v2, 0x1

    if-nez v1, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getDiskEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->encodingDisk:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getEncoding()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDiskEncoding()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract getErrorOnSizeCommand()Z
.end method

.method public abstract getFeatures()Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract getFileSize(Ljava/lang/String;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract getFileTimeStamp(Ljava/lang/String;)Ljava/util/Date;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public declared-synchronized getHost()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->host:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract getInputStream(Ljava/lang/String;J)Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method protected declared-synchronized getInputStreamReader(Ljava/io/InputStream;)Ljava/io/InputStreamReader;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->encodingWire:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->encodingWire:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/io/InputStreamReader;

    iget-object v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->encodingWire:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :cond_1
    :try_start_4
    new-instance v0, Ljava/io/InputStreamReader;

    sget-object v1, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v0, p1, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :catch_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_2
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_3
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :goto_1
    :try_start_8
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getKeepAlive()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->keepAlive:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLinger()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->linger:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getListeners()Ljava/util/Vector;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->c:Ljava/util/Vector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract getMachineDirListing(Ljava/lang/String;)Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract getMachineDirListing(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract getMachineFileListing(Ljava/lang/String;)Lcom/jscape/inet/ftp/FtpFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract getNATAddress()Ljava/lang/String;
.end method

.method public abstract getNameListing(Ljava/lang/String;)Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract getOutputStream(Ljava/lang/String;ZJ)Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method protected declared-synchronized getOutputStreamWriter(Ljava/io/OutputStream;)Ljava/io/OutputStreamWriter;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->encodingWire:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->encodingWire:Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/io/OutputStreamWriter;

    iget-object v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->encodingWire:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :cond_1
    :try_start_4
    new-instance v0, Ljava/io/OutputStreamWriter;

    invoke-direct {v0, p1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :catch_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_2
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_3
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :goto_1
    :try_start_8
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getPassword()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->password:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPort()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->port:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPortAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->portAddress:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getPreserveDownloadTimestamp()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPreserveUploadTimestamp()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getProxyHostname()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->proxyHostname:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyPassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->proxyPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyPort()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->proxyPort:I

    return v0
.end method

.method public getProxyType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->proxyType:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->proxyUsername:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getResponseCode()I
.end method

.method public abstract getSystemType()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public getTcpNoDelay()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->tcpNoDelay:Z

    return v0
.end method

.method public declared-synchronized getTimeZone()Ljava/util/TimeZone;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->h:Ljava/util/TimeZone;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTimeout()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->timeout:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract getTransferMode()I
.end method

.method public abstract getUseEPRT()Z
.end method

.method public abstract getUseEPSV()Z
.end method

.method public declared-synchronized getUser()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->user:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getWireEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->encodingWire:Ljava/lang/String;

    return-object v0
.end method

.method protected declared-synchronized hasListeners()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->c:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    monitor-exit p0

    return v0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public interrupt()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted:Z

    return-void
.end method

.method public interrupted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted:Z

    return v0
.end method

.method public abstract isConnected()Z
.end method

.method public abstract isFeatureSupported(Ljava/lang/String;)Z
.end method

.method public abstract isPassive()Z
.end method

.method public abstract issueCommand(Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract issueCommandCheck(Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract login()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract makeDir(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract noop()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract readResponse()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public removeListener(Lcom/jscape/inet/ftp/FtpListener;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    monitor-enter p0

    :try_start_0
    iput-object v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->c:Ljava/util/Vector;

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public abstract renameFile(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted:Z

    return-void
.end method

.method public declared-synchronized setAccount(Ljava/lang/String;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->account:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setAuto(Z)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->autoMode:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setAutoDetectIpv6(Z)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->autoDetectIpv6:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setBlockTransferSize(I)V
    .locals 4

    monitor-enter p0

    if-lez p1, :cond_0

    :try_start_0
    iput p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->blockSize:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftp/FtpImplementation;->L:[Ljava/lang/String;

    const/4 v3, 0x7

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    monitor-exit p0

    throw p1
.end method

.method public abstract setCompression(Z)V
.end method

.method public abstract setConnectBeforeCommand(Z)V
.end method

.method public abstract setDataPort(I)V
.end method

.method public abstract setDataPortEnd(I)V
.end method

.method public abstract setDataPortStart(I)V
.end method

.method public declared-synchronized setDebug(Z)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->debug:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setDebugStream(Ljava/io/PrintStream;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->debugStream:Ljava/io/PrintStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public abstract setDir(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public setDiskEncoding(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->encodingDisk:Ljava/lang/String;

    return-void
.end method

.method public declared-synchronized setEncoding(Ljava/lang/String;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setDiskEncoding(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public abstract setErrorOnSizeCommand(Z)V
.end method

.method public abstract setFileCreationTime(Ljava/lang/String;Ljava/util/Date;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract setFileModificationTime(Ljava/lang/String;Ljava/util/Date;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public declared-synchronized setFtp(Lcom/jscape/inet/ftp/Ftp;)V
    .locals 0

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->ftp:Lcom/jscape/inet/ftp/Ftp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance p1, Ljava/lang/NullPointerException;

    invoke-direct {p1}, Ljava/lang/NullPointerException;-><init>()V

    throw p1
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setHost(Ljava/lang/String;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->host:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setKeepAlive(Z)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->keepAlive:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setLinger(I)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->linger:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setListeners(Ljava/util/Vector;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->c:Ljava/util/Vector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public abstract setNATAddress(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract setPassive(Z)V
.end method

.method public declared-synchronized setPassword(Ljava/lang/String;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->password:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setPort(I)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->port:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setPortAddress(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->portAddress:Ljava/lang/String;

    return-void
.end method

.method public declared-synchronized setPreserveDownloadTimestamp(Z)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setPreserveUploadTimestamp(Z)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setProxyAuthentication(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->proxyUsername:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ftp/FtpImplementation;->proxyPassword:Ljava/lang/String;

    return-void
.end method

.method public setProxyHost(Ljava/lang/String;I)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->proxyHostname:Ljava/lang/String;

    iput p2, p0, Lcom/jscape/inet/ftp/FtpImplementation;->proxyPort:I

    return-void
.end method

.method public setProxyType(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->proxyType:Ljava/lang/String;

    return-void
.end method

.method public abstract setReceiveBufferSize(I)V
.end method

.method public abstract setSendBufferSize(I)V
.end method

.method public declared-synchronized setTargetFile(Ljava/lang/String;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->targetFile:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setTargetPath(Ljava/lang/String;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->targetPath:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setTcpNoDelay(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->tcpNoDelay:Z

    return-void
.end method

.method public declared-synchronized setTimeout(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    if-lez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    iput p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->timeout:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setTimezone(Ljava/util/TimeZone;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->h:Ljava/util/TimeZone;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public abstract setTransferMode(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract setUseEPRT(Z)V
.end method

.method public abstract setUseEPSV(Z)V
.end method

.method public declared-synchronized setUser(Ljava/lang/String;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->user:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setWireEncoding(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpImplementation;->encodingWire:Ljava/lang/String;

    return-void
.end method

.method protected skip(Ljava/io/InputStream;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p1, p2, p3}, Ljava/io/InputStream;->skip(J)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public abstract upload(Ljava/io/InputStream;Ljava/lang/String;ZJJZ)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract uploadUnique(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method
