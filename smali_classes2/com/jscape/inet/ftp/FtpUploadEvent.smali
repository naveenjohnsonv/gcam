.class public Lcom/jscape/inet/ftp/FtpUploadEvent;
.super Lcom/jscape/inet/ftp/FtpEvent;


# static fields
.field private static final g:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:J

.field private final f:J


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x2

    const/4 v4, 0x0

    const-string v5, "kF\ng\u00040Y\u0015E\u0004.\u0008i"

    const/16 v6, 0xd

    move v8, v2

    move v9, v4

    const/4 v7, -0x1

    :goto_0
    const/16 v10, 0x6e

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    move v15, v4

    :goto_2
    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    add-int/lit8 v12, v9, 0x1

    if-eqz v13, :cond_1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v12

    goto :goto_0

    :cond_0
    const/16 v6, 0x2d

    const/16 v5, 0xe

    const-string v7, "lh[<o,?]8Q:b-z\u001e\u0019uD}.<5\u0019jR>a<?\u0019|^!k+.VjNs~).Q8"

    move v8, v5

    move-object v5, v7

    move v9, v12

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v12

    :goto_3
    const/16 v10, 0x10

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v13, v4

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ftp/FtpUploadEvent;->g:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v3, v15, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v11, :cond_8

    if-eq v3, v2, :cond_7

    const/4 v2, 0x3

    if-eq v3, v2, :cond_6

    if-eq v3, v0, :cond_5

    const/4 v2, 0x5

    if-eq v3, v2, :cond_4

    const/16 v2, 0x4a

    goto :goto_4

    :cond_4
    const/16 v2, 0x58

    goto :goto_4

    :cond_5
    const/16 v2, 0x1e

    goto :goto_4

    :cond_6
    const/16 v2, 0x43

    goto :goto_4

    :cond_7
    const/16 v2, 0x27

    goto :goto_4

    :cond_8
    const/16 v2, 0x8

    goto :goto_4

    :cond_9
    const/16 v2, 0x29

    :goto_4
    xor-int/2addr v2, v10

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v12, v15

    add-int/lit8 v15, v15, 0x1

    const/4 v2, 0x2

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/FtpEvent;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ftp/FtpUploadEvent;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/ftp/FtpUploadEvent;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/inet/ftp/FtpUploadEvent;->d:Ljava/lang/String;

    iput-wide p5, p0, Lcom/jscape/inet/ftp/FtpUploadEvent;->e:J

    iput-wide p7, p0, Lcom/jscape/inet/ftp/FtpUploadEvent;->f:J

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/inet/ftp/FtpListener;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    invoke-interface {p1, p0}, Lcom/jscape/inet/ftp/FtpListener;->upload(Lcom/jscape/inet/ftp/FtpUploadEvent;)V

    :cond_1
    return-void
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpUploadEvent;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpUploadEvent;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpUploadEvent;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ftp/FtpUploadEvent;->e:J

    return-wide v0
.end method

.method public getTime()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ftp/FtpUploadEvent;->f:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftp/FtpUploadEvent;->g:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ftp/FtpUploadEvent;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/ftp/FtpUploadEvent;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/ftp/FtpUploadEvent;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ftp/FtpUploadEvent;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
