.class public interface abstract Lcom/jscape/inet/ftp/FtpListener;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/EventListener;


# virtual methods
.method public abstract changeDir(Lcom/jscape/inet/ftp/FtpChangeDirEvent;)V
.end method

.method public abstract commandSent(Lcom/jscape/inet/ftp/FtpCommandEvent;)V
.end method

.method public abstract connected(Lcom/jscape/inet/ftp/FtpConnectedEvent;)V
.end method

.method public abstract connectionLost(Lcom/jscape/inet/ftp/FtpConnectionLostEvent;)V
.end method

.method public abstract createDir(Lcom/jscape/inet/ftp/FtpCreateDirEvent;)V
.end method

.method public abstract deleteDir(Lcom/jscape/inet/ftp/FtpDeleteDirEvent;)V
.end method

.method public abstract deleteFile(Lcom/jscape/inet/ftp/FtpDeleteFileEvent;)V
.end method

.method public abstract disconnected(Lcom/jscape/inet/ftp/FtpDisconnectedEvent;)V
.end method

.method public abstract download(Lcom/jscape/inet/ftp/FtpDownloadEvent;)V
.end method

.method public abstract listing(Lcom/jscape/inet/ftp/FtpListingEvent;)V
.end method

.method public abstract progress(Lcom/jscape/inet/ftp/FtpProgressEvent;)V
.end method

.method public abstract renameFile(Lcom/jscape/inet/ftp/FtpRenameFileEvent;)V
.end method

.method public abstract responseReceived(Lcom/jscape/inet/ftp/FtpResponseEvent;)V
.end method

.method public abstract upload(Lcom/jscape/inet/ftp/FtpUploadEvent;)V
.end method
