.class public Lcom/jscape/inet/ftp/FxpRetr;
.super Ljava/lang/Thread;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private a:Lcom/jscape/inet/ftp/Fxp;

.field private b:Lcom/jscape/inet/ftp/Ftp;

.field private c:Lcom/jscape/inet/ftp/Ftp;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "\u0000 D:\u0018"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ftp/FxpRetr;->e:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/4 v5, 0x2

    if-eqz v4, :cond_5

    const/4 v6, 0x1

    if-eq v4, v6, :cond_4

    if-eq v4, v5, :cond_3

    const/4 v6, 0x3

    if-eq v4, v6, :cond_2

    const/4 v6, 0x4

    if-eq v4, v6, :cond_1

    const/4 v6, 0x5

    if-eq v4, v6, :cond_6

    const/16 v5, 0x30

    goto :goto_1

    :cond_1
    const/16 v5, 0x71

    goto :goto_1

    :cond_2
    const/16 v5, 0x21

    goto :goto_1

    :cond_3
    const/16 v5, 0x59

    goto :goto_1

    :cond_4
    const/16 v5, 0x2c

    goto :goto_1

    :cond_5
    const/16 v5, 0x1b

    :cond_6
    :goto_1
    const/16 v4, 0x49

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/jscape/inet/ftp/Fxp;Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ftp/FxpRetr;->a:Lcom/jscape/inet/ftp/Fxp;

    iput-object p2, p0, Lcom/jscape/inet/ftp/FxpRetr;->b:Lcom/jscape/inet/ftp/Ftp;

    iput-object p3, p0, Lcom/jscape/inet/ftp/FxpRetr;->c:Lcom/jscape/inet/ftp/Ftp;

    iput-object p4, p0, Lcom/jscape/inet/ftp/FxpRetr;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/FxpRetr;->b:Lcom/jscape/inet/ftp/Ftp;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftp/FxpRetr;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ftp/FxpRetr;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ftp/Ftp;->issueCommand(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/inet/ftp/FxpRetr;->a:Lcom/jscape/inet/ftp/Fxp;

    iget-object v2, p0, Lcom/jscape/inet/ftp/FxpRetr;->b:Lcom/jscape/inet/ftp/Ftp;

    iget-object v3, p0, Lcom/jscape/inet/ftp/FxpRetr;->c:Lcom/jscape/inet/ftp/Ftp;

    iget-object v4, p0, Lcom/jscape/inet/ftp/FxpRetr;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/jscape/inet/ftp/Fxp;->fireFxpFailed(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method
