.class public abstract enum Lcom/jscape/inet/ftp/Checksum;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/ftp/Checksum;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CRC32:Lcom/jscape/inet/ftp/Checksum;

.field public static final enum MD5:Lcom/jscape/inet/ftp/Checksum;

.field private static final a:I = 0x2000

.field private static final b:[Lcom/jscape/inet/ftp/Checksum;


# instance fields
.field public final code:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "`\t\u0010PZ\u0003n\u001fF"

    move v8, v4

    const/4 v6, -0x1

    const/4 v7, 0x5

    :goto_0
    const/16 v9, 0x5f

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v5, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    const/16 v13, 0x9

    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v14, v11

    move v15, v4

    :goto_2
    const/4 v3, 0x3

    const/4 v2, 0x2

    if-gt v14, v15, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v2, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v13, :cond_0

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v2

    goto :goto_0

    :cond_0
    const-string v5, "\u0017f\u001f\u0005\u0019pi\t\u0003"

    move v8, v2

    move v7, v3

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v13, :cond_2

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v7, v2

    move v8, v11

    :goto_3
    const/4 v9, 0x6

    add-int/2addr v6, v10

    add-int v2, v6, v7

    invoke-virtual {v5, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v4

    goto :goto_1

    :cond_2
    new-instance v0, Lcom/jscape/inet/ftp/Checksum$1;

    aget-object v3, v1, v3

    aget-object v5, v1, v4

    invoke-direct {v0, v3, v4, v5}, Lcom/jscape/inet/ftp/Checksum$1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ftp/Checksum;->CRC32:Lcom/jscape/inet/ftp/Checksum;

    new-instance v0, Lcom/jscape/inet/ftp/Checksum$2;

    aget-object v3, v1, v2

    aget-object v1, v1, v10

    invoke-direct {v0, v3, v10, v1}, Lcom/jscape/inet/ftp/Checksum$2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/ftp/Checksum;->MD5:Lcom/jscape/inet/ftp/Checksum;

    new-array v1, v2, [Lcom/jscape/inet/ftp/Checksum;

    sget-object v2, Lcom/jscape/inet/ftp/Checksum;->CRC32:Lcom/jscape/inet/ftp/Checksum;

    aput-object v2, v1, v4

    aput-object v0, v1, v10

    sput-object v1, Lcom/jscape/inet/ftp/Checksum;->b:[Lcom/jscape/inet/ftp/Checksum;

    return-void

    :cond_3
    aget-char v16, v11, v15

    rem-int/lit8 v4, v15, 0x7

    if-eqz v4, :cond_9

    if-eq v4, v10, :cond_8

    if-eq v4, v2, :cond_7

    const/4 v2, 0x5

    if-eq v4, v3, :cond_6

    if-eq v4, v0, :cond_5

    if-eq v4, v2, :cond_4

    const/16 v3, 0x79

    goto :goto_4

    :cond_4
    const/16 v3, 0x78

    goto :goto_4

    :cond_5
    const/16 v3, 0x37

    goto :goto_4

    :cond_6
    const/16 v3, 0x3c

    goto :goto_4

    :cond_7
    const/4 v2, 0x5

    const/16 v3, 0x2c

    goto :goto_4

    :cond_8
    const/4 v2, 0x5

    const/16 v3, 0x24

    goto :goto_4

    :cond_9
    const/4 v2, 0x5

    const/16 v3, 0x5c

    :goto_4
    xor-int/2addr v3, v9

    xor-int v3, v16, v3

    int-to-char v3, v3

    aput-char v3, v11, v15

    add-int/lit8 v15, v15, 0x1

    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/jscape/inet/ftp/Checksum;->code:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/jscape/inet/ftp/Checksum$1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/inet/ftp/Checksum;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/ftp/Checksum;
    .locals 1

    const-class v0, Lcom/jscape/inet/ftp/Checksum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/ftp/Checksum;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/ftp/Checksum;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ftp/Checksum;->b:[Lcom/jscape/inet/ftp/Checksum;

    invoke-virtual {v0}, [Lcom/jscape/inet/ftp/Checksum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/ftp/Checksum;

    return-object v0
.end method


# virtual methods
.method protected abstract hash()Lcom/jscape/a/d;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public of(Ljava/io/InputStream;J)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Checksum;->hash()Lcom/jscape/a/d;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/jscape/inet/ftp/Checksum;->of(Ljava/io/InputStream;JLcom/jscape/a/d;)[B

    move-result-object p1

    return-object p1
.end method

.method protected of(Ljava/io/InputStream;JLcom/jscape/a/d;)[B
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/16 v0, 0x2000

    new-array v1, v0, [B

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v2

    int-to-long v3, v0

    invoke-static {v3, v4, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v5

    long-to-int v0, v5

    :cond_0
    const/4 v5, 0x0

    invoke-virtual {p1, v1, v5, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    const/4 v6, -0x1

    if-eq v0, v6, :cond_2

    const-wide/16 v6, 0x0

    cmp-long v6, p2, v6

    if-eqz v2, :cond_1

    if-lez v6, :cond_2

    int-to-long v6, v0

    sub-long/2addr p2, v6

    invoke-interface {p4, v1, v5, v0}, Lcom/jscape/a/d;->a([BII)Lcom/jscape/a/d;

    invoke-static {v3, v4, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v5

    long-to-int v0, v5

    goto :goto_0

    :cond_1
    move v0, v6

    :goto_0
    if-nez v2, :cond_0

    :cond_2
    invoke-interface {p4}, Lcom/jscape/a/d;->c()[B

    move-result-object p1

    return-object p1
.end method
