.class public abstract Lcom/jscape/inet/ftp/AbstractRequest;
.super Ljava/lang/Object;


# static fields
.field private static final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "?R}K\"1YvNn[;=N\""

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ftp/AbstractRequest;->e:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x11

    goto :goto_1

    :cond_1
    const/16 v4, 0x74

    goto :goto_1

    :cond_2
    const/16 v4, 0x62

    goto :goto_1

    :cond_3
    const/4 v4, 0x6

    goto :goto_1

    :cond_4
    const/16 v4, 0x27

    goto :goto_1

    :cond_5
    const/16 v4, 0x10

    goto :goto_1

    :cond_6
    const/16 v4, 0x7a

    :goto_1
    const/16 v5, 0x2c

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method protected assertIsPositive(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    const-string v1, "4"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :try_start_1
    const-string v0, "5"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    if-nez v1, :cond_1

    return-void

    :cond_1
    :try_start_2
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-direct {v0, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/AbstractRequest;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftp/AbstractRequest;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/AbstractRequest;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object p1

    throw p1
.end method

.method protected assertIsValid()V
    .locals 2

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/AbstractRequest;->isValid()Z

    move-result v0

    sget-object v1, Lcom/jscape/inet/ftp/AbstractRequest;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/jscape/util/w;->c(ZLjava/lang/String;)V

    return-void
.end method

.method public abstract execute(Lcom/jscape/inet/ftp/ClientProtocolInterpreter;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract isValid()Z
.end method
