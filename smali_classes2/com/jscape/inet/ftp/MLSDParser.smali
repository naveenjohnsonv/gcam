.class public final Lcom/jscape/inet/ftp/MLSDParser;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ftp/FtpFileParser;


# static fields
.field private static final a:Lcom/jscape/inet/ftp/MLSEntryAssembler;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;

.field private static final e:Ljava/lang/String;

.field private static final f:Ljava/lang/String;

.field private static final g:Ljava/lang/String;

.field private static final h:Ljava/lang/String;

.field private static final i:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const/16 v2, 0x9

    const/4 v3, 0x0

    const-string v4, "\u000b\u007f\u0019\u0003\u0010s2\u000e%\tT%\u0019\\JsmT%\u00081@\u000e\u001c\nd3\n\u00081@\u000e\u001c\nd3\n\tT\u007f\u0019\\\u0010sm\u000e%\t\u000b%\u0019\u0003Js2T%\t\u000b%\u0019\u0003Js2T%\n4E\u001b\u0015\u0003q9\u0000qM\u00134E\u001b\u0015\u0003q9\u0000qMQ/\u0016z\u0014e\u000e\u0002\u0014\n4E\u001b\u0015\u0003q9\u0000qM\tT%\u0019\\JsmT%\tT\u007f\u0019\\\u0010sm\u000e%\u00134E\u001b\u0015\u0003q9\u0000qMQ/\u0016z\u0014e\u000e\u0002\u0014"

    const/16 v5, 0x95

    move v7, v2

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x6a

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v1, 0x4

    const/4 v15, 0x2

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v1, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v1

    goto :goto_0

    :cond_0
    const-string v4, "T F\\O,mQz\u0003B>\u0019"

    move v8, v1

    move v7, v2

    const/16 v5, 0xd

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v1

    move v8, v11

    :goto_3
    const/16 v9, 0x35

    add-int/2addr v6, v10

    add-int v1, v6, v7

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ftp/MLSDParser;->i:[Ljava/lang/String;

    const/16 v4, 0xc

    aget-object v4, v0, v4

    sput-object v4, Lcom/jscape/inet/ftp/MLSDParser;->h:Ljava/lang/String;

    const/4 v4, 0x6

    aget-object v4, v0, v4

    sput-object v4, Lcom/jscape/inet/ftp/MLSDParser;->b:Ljava/lang/String;

    aget-object v3, v0, v3

    sput-object v3, Lcom/jscape/inet/ftp/MLSDParser;->d:Ljava/lang/String;

    aget-object v3, v0, v15

    sput-object v3, Lcom/jscape/inet/ftp/MLSDParser;->g:Ljava/lang/String;

    aget-object v1, v0, v1

    sput-object v1, Lcom/jscape/inet/ftp/MLSDParser;->c:Ljava/lang/String;

    aget-object v1, v0, v2

    sput-object v1, Lcom/jscape/inet/ftp/MLSDParser;->f:Ljava/lang/String;

    const/16 v1, 0xa

    aget-object v0, v0, v1

    sput-object v0, Lcom/jscape/inet/ftp/MLSDParser;->e:Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/ftp/MLSEntryAssembler;

    invoke-direct {v0}, Lcom/jscape/inet/ftp/MLSEntryAssembler;-><init>()V

    sput-object v0, Lcom/jscape/inet/ftp/MLSDParser;->a:Lcom/jscape/inet/ftp/MLSEntryAssembler;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    if-eq v2, v15, :cond_7

    const/4 v15, 0x3

    if-eq v2, v15, :cond_6

    if-eq v2, v1, :cond_5

    const/4 v1, 0x5

    if-eq v2, v1, :cond_4

    const/16 v15, 0x2a

    goto :goto_4

    :cond_4
    const/16 v15, 0x34

    goto :goto_4

    :cond_5
    const/16 v15, 0xd

    goto :goto_4

    :cond_6
    const/16 v15, 0x1b

    goto :goto_4

    :cond_7
    const/16 v15, 0x5e

    goto :goto_4

    :cond_8
    const/16 v15, 0x62

    goto :goto_4

    :cond_9
    const/16 v15, 0x13

    :goto_4
    xor-int v1, v9, v15

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/16 v2, 0x9

    goto/16 :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/inet/ftp/MLSEntry;)Ljava/lang/String;
    .locals 6

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getPermissions()Lcom/jscape/inet/ftp/MLSEntry$Permissions;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/MLSDParser;->b(Lcom/jscape/inet/ftp/MLSEntry;)Z

    move-result p1

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_a

    if-eqz p1, :cond_9

    const/16 p1, 0x64

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isDirectoryEnteringAllowed()Z

    move-result p1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isListingAllowed()Z

    move-result p1

    :cond_0
    if-eqz v1, :cond_2

    if-eqz p1, :cond_1

    move p1, v4

    goto :goto_0

    :cond_1
    move p1, v3

    :cond_2
    :goto_0
    invoke-virtual {v0}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isDirectoryEnteringAllowed()Z

    move-result v5

    if-eqz v1, :cond_3

    if-eqz v5, :cond_7

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isDirectoryCreationAllowed()Z

    move-result v5

    :cond_3
    if-eqz v1, :cond_4

    if-eqz v5, :cond_7

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isFileCreationAllowed()Z

    move-result v5

    :cond_4
    if-eqz v1, :cond_5

    if-eqz v5, :cond_7

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isDirectoryPurgingAllowed()Z

    move-result v5

    :cond_5
    if-eqz v1, :cond_6

    if-eqz v5, :cond_7

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isDeletionAllowed()Z

    move-result v5

    :cond_6
    if-eqz v1, :cond_8

    if-eqz v5, :cond_7

    move v5, v4

    goto :goto_1

    :cond_7
    move v5, v3

    :cond_8
    :goto_1
    if-nez v1, :cond_10

    :cond_9
    const/16 p1, 0x63

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isFileReadingAllowed()Z

    move-result p1

    :cond_a
    invoke-virtual {v0}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isFileWritingAllowed()Z

    move-result v5

    if-eqz v1, :cond_b

    if-eqz v5, :cond_f

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isFileAppendingAllowed()Z

    move-result v5

    :cond_b
    if-eqz v1, :cond_c

    if-eqz v5, :cond_f

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isDeletionAllowed()Z

    move-result v5

    :cond_c
    if-eqz v1, :cond_d

    if-eqz v5, :cond_f

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isRenamingAllowed()Z

    move-result v5

    :cond_d
    if-eqz v1, :cond_e

    if-eqz v5, :cond_f

    move v3, v4

    goto :goto_2

    :cond_e
    move v3, v5

    :cond_f
    :goto_2
    move v5, v3

    :cond_10
    if-eqz v1, :cond_12

    if-eqz p1, :cond_12

    if-eqz v1, :cond_11

    if-eqz v5, :cond_12

    sget-object v0, Lcom/jscape/inet/ftp/MLSDParser;->i:[Ljava/lang/String;

    const/16 v3, 0xd

    aget-object v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez v1, :cond_16

    goto :goto_3

    :cond_11
    move p1, v5

    :cond_12
    :goto_3
    if-eqz v1, :cond_13

    if-eqz p1, :cond_14

    sget-object p1, Lcom/jscape/inet/ftp/MLSDParser;->i:[Ljava/lang/String;

    const/4 v0, 0x5

    aget-object p1, p1, v0

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez v1, :cond_16

    goto :goto_4

    :cond_13
    move v5, p1

    :cond_14
    :goto_4
    if-eqz v5, :cond_15

    sget-object p1, Lcom/jscape/inet/ftp/MLSDParser;->i:[Ljava/lang/String;

    const/16 v0, 0xb

    aget-object p1, p1, v0

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez v1, :cond_16

    :cond_15
    sget-object p1, Lcom/jscape/inet/ftp/MLSDParser;->i:[Ljava/lang/String;

    aget-object p1, p1, v4

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private b(Lcom/jscape/inet/ftp/MLSEntry;)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getType()Lcom/jscape/inet/ftp/MLSEntry$Type;

    move-result-object p1

    sget-object v1, Lcom/jscape/inet/ftp/MLSEntry$Type;->CURRENT_DIRECTORY:Lcom/jscape/inet/ftp/MLSEntry$Type;

    if-eqz v0, :cond_0

    if-eq v1, p1, :cond_2

    sget-object v1, Lcom/jscape/inet/ftp/MLSEntry$Type;->PARENT_DIRECTORY:Lcom/jscape/inet/ftp/MLSEntry$Type;

    :cond_0
    if-eqz v0, :cond_1

    if-eq v1, p1, :cond_2

    sget-object v1, Lcom/jscape/inet/ftp/MLSEntry$Type;->DIRECTORY:Lcom/jscape/inet/ftp/MLSEntry$Type;

    :cond_1
    if-ne v1, p1, :cond_3

    :cond_2
    const/4 p1, 0x1

    goto :goto_0

    :cond_3
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public getDateTime(Lcom/jscape/inet/ftp/FtpFile;)Ljava/util/Date;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/jscape/inet/ftp/MLSDParser;->i:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getTime()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getFileDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 3

    :try_start_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/jscape/inet/ftp/MLSDParser;->i:[Ljava/lang/String;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public parse(Ljava/lang/String;)Lcom/jscape/inet/ftp/FtpFile;
    .locals 5

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/ftp/MLSDParser;->a:Lcom/jscape/inet/ftp/MLSEntryAssembler;

    invoke-virtual {v1, p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->restoreEntry(Ljava/lang/String;)Lcom/jscape/inet/ftp/MLSEntry;

    move-result-object v1

    new-instance v2, Lcom/jscape/inet/ftp/FtpFile;

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/MLSEntry;->getPathname()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, p1, p0}, Lcom/jscape/inet/ftp/FtpFile;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ftp/FtpFileParser;)V

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/MLSEntry;->getSize()Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/MLSEntry;->getSize()Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/jscape/inet/ftp/FtpFile;->setFilesize(Ljava/lang/String;)V

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/MLSEntry;->getType()Lcom/jscape/inet/ftp/MLSEntry$Type;

    move-result-object p1

    if-eqz p1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/MLSEntry;->getType()Lcom/jscape/inet/ftp/MLSEntry$Type;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry$Type;->getName()Ljava/lang/String;

    move-result-object p1

    sget-object v3, Lcom/jscape/inet/ftp/MLSDParser;->i:[Ljava/lang/String;

    const/16 v4, 0xe

    aget-object v3, v3, v4

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    invoke-virtual {v2, p1}, Lcom/jscape/inet/ftp/FtpFile;->setDirectory(Z)V

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/MLSEntry;->getPermissions()Lcom/jscape/inet/ftp/MLSEntry$Permissions;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-direct {p0, v1}, Lcom/jscape/inet/ftp/MLSDParser;->a(Lcom/jscape/inet/ftp/MLSEntry;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/jscape/inet/ftp/FtpFile;->setPermission(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v1}, Lcom/jscape/inet/ftp/MLSEntry;->getLastModificationDate()Ljava/util/Date;

    move-result-object p1

    if-eqz v0, :cond_3

    if-eqz p1, :cond_4

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/MLSEntry;->getLastModificationDate()Ljava/util/Date;

    move-result-object p1

    :cond_3
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/jscape/inet/ftp/MLSDParser;->i:[Ljava/lang/String;

    const/4 v3, 0x7

    aget-object v3, v1, v3

    invoke-direct {v0, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/jscape/inet/ftp/FtpFile;->setDate(Ljava/lang/String;)V

    new-instance v0, Ljava/text/SimpleDateFormat;

    const/4 v3, 0x3

    aget-object v1, v1, v3

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/jscape/inet/ftp/FtpFile;->setTime(Ljava/lang/String;)V

    :cond_4
    return-object v2
.end method

.method public parse(Ljava/io/BufferedReader;)Ljava/util/Enumeration;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    :cond_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftp/MLSDParser;->parse(Ljava/lang/String;)Lcom/jscape/inet/ftp/FtpFile;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSDParser;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    return-object p1
.end method
