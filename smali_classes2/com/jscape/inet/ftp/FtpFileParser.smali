.class public interface abstract Lcom/jscape/inet/ftp/FtpFileParser;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getDateTime(Lcom/jscape/inet/ftp/FtpFile;)Ljava/util/Date;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation
.end method

.method public abstract getFileDate(Ljava/lang/String;)Ljava/util/Date;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation
.end method

.method public abstract parse(Ljava/io/BufferedReader;)Ljava/util/Enumeration;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
