.class public Lcom/jscape/inet/ftp/FtpBaseImplementation$CurrentDirectoryFormat;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "4Z\u000e\u0006_X\u0001b\u0004=U&)\nR\\YQ\u001f\u0013P1}y\u0002Xm"

    const/16 v5, 0x8

    move v7, v3

    const/4 v6, -0x1

    const/16 v8, 0x1b

    :goto_0
    const/16 v9, 0x3f

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v5

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v7, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v8, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v11

    goto :goto_0

    :cond_0
    const/16 v4, 0xe

    const/16 v5, 0xa

    const-string v6, "863;uy:[\u0017\u0013\u00032\u0007{"

    move v8, v4

    move-object v4, v6

    move v7, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v8, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v11

    :goto_3
    const/16 v9, 0x55

    add-int/2addr v6, v10

    add-int v11, v6, v5

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ftp/FtpBaseImplementation$CurrentDirectoryFormat;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v10, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v1, 0x1b

    goto :goto_4

    :cond_4
    const/16 v1, 0x42

    goto :goto_4

    :cond_5
    const/16 v1, 0x45

    goto :goto_4

    :cond_6
    const/16 v1, 0x1c

    goto :goto_4

    :cond_7
    const/16 v1, 0x14

    goto :goto_4

    :cond_8
    const/16 v1, 0x16

    goto :goto_4

    :cond_9
    const/16 v1, 0x2e

    :goto_4
    xor-int/2addr v1, v9

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    new-instance v0, Ljava/util/Scanner;

    invoke-direct {v0, p0}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    sget-object p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$CurrentDirectoryFormat;->a:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object p0, p0, v1

    invoke-virtual {v0, p0}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    move-result-object p0

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    move-object v3, v2

    :cond_0
    invoke-virtual {p0}, Ljava/util/Scanner;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {p0}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    if-eqz v0, :cond_6

    sget-object v5, Lcom/jscape/inet/ftp/FtpBaseImplementation$CurrentDirectoryFormat;->a:[Ljava/lang/String;

    const/4 v6, 0x3

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v0, :cond_2

    if-eqz v5, :cond_1

    invoke-static {v4}, Lcom/jscape/inet/ftp/FtpBaseImplementation$CurrentDirectoryFormat;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v0, :cond_4

    :cond_1
    if-eqz v0, :cond_3

    sget-object v5, Lcom/jscape/inet/ftp/FtpBaseImplementation$CurrentDirectoryFormat;->a:[Ljava/lang/String;

    const/4 v6, 0x4

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    :cond_2
    if-eqz v5, :cond_4

    invoke-static {v4}, Lcom/jscape/inet/ftp/FtpBaseImplementation$CurrentDirectoryFormat;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_3
    move-object v3, v4

    :cond_4
    if-nez v0, :cond_0

    :cond_5
    sget-object p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$CurrentDirectoryFormat;->a:[Ljava/lang/String;

    const/4 v0, 0x0

    aget-object p0, p0, v0

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v3, v4, v0

    aput-object v2, v4, v1

    invoke-static {p0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :cond_6
    return-object v4
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/16 v0, 0x3a

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move v3, v2

    move v4, v3

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v3, v5, :cond_a

    if-eqz v1, :cond_b

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x20

    if-eqz v1, :cond_4

    if-ne v5, v6, :cond_2

    if-eqz v1, :cond_1

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-eqz v1, :cond_3

    if-lez v6, :cond_2

    goto :goto_3

    :cond_1
    move v6, v4

    goto :goto_0

    :cond_2
    move v6, v5

    :cond_3
    :goto_0
    if-eqz v1, :cond_6

    const/16 v7, 0x22

    goto :goto_1

    :cond_4
    move v7, v6

    move v6, v5

    :goto_1
    if-ne v6, v7, :cond_5

    if-eqz v1, :cond_9

    if-eqz v4, :cond_7

    :cond_5
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v6, v2

    :cond_6
    if-nez v1, :cond_8

    :cond_7
    const/4 v4, 0x1

    goto :goto_2

    :cond_8
    move v4, v6

    :cond_9
    :goto_2
    add-int/lit8 v3, v3, 0x1

    if-nez v1, :cond_0

    :cond_a
    :goto_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_b
    return-object p0
.end method

.method public static parse(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/jscape/inet/ftp/FtpBaseImplementation$CurrentDirectoryFormat;->a:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    aget-object v0, v1, v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/jscape/inet/ftp/FtpBaseImplementation$CurrentDirectoryFormat;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lcom/jscape/inet/ftp/FtpBaseImplementation$CurrentDirectoryFormat;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method
