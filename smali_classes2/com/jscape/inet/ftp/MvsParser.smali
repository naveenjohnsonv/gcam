.class public Lcom/jscape/inet/ftp/MvsParser;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ftp/FtpFileParser;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xa

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x2d

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "DN)s@#|\u0012S4\u0002\u001d\u001c\u0008pz\u007fn\u000bAHD"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    const/4 v11, 0x2

    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x16

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ftp/MvsParser;->b:[Ljava/lang/String;

    new-array v0, v11, [Ljava/lang/String;

    aget-object v3, v1, v2

    aput-object v3, v0, v2

    aget-object v1, v1, v11

    aput-object v1, v0, v7

    sput-object v0, Lcom/jscape/inet/ftp/MvsParser;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v10

    rem-int/lit8 v13, v10, 0x7

    if-eqz v13, :cond_7

    if-eq v13, v7, :cond_6

    if-eq v13, v11, :cond_5

    if-eq v13, v0, :cond_4

    const/4 v11, 0x4

    if-eq v13, v11, :cond_3

    const/4 v11, 0x5

    if-eq v13, v11, :cond_2

    const/16 v11, 0x1c

    goto :goto_2

    :cond_2
    const/16 v11, 0x43

    goto :goto_2

    :cond_3
    const/16 v11, 0x42

    goto :goto_2

    :cond_4
    const/16 v11, 0x27

    goto :goto_2

    :cond_5
    const/16 v11, 0x7d

    goto :goto_2

    :cond_6
    const/16 v11, 0x1a

    goto :goto_2

    :cond_7
    const/16 v11, 0x10

    :goto_2
    xor-int/2addr v11, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;
    .locals 15

    sget-object v0, Lcom/jscape/inet/ftp/MvsParser;->b:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    move-object/from16 v3, p1

    invoke-virtual {v3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    new-instance v14, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;

    const/4 v2, 0x0

    aget-object v4, v0, v2

    aget-object v5, v0, v1

    const/4 v1, 0x2

    aget-object v6, v0, v1

    const/4 v1, 0x3

    aget-object v7, v0, v1

    const/4 v1, 0x4

    aget-object v8, v0, v1

    const/4 v1, 0x5

    aget-object v9, v0, v1

    const/4 v1, 0x6

    aget-object v10, v0, v1

    const/4 v1, 0x7

    aget-object v11, v0, v1

    const/16 v1, 0x8

    aget-object v12, v0, v1

    const/16 v1, 0x9

    aget-object v13, v0, v1

    move-object v2, v14

    invoke-direct/range {v2 .. v13}, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v14
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, p2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    return-object p1
.end method

.method private a(Ljava/util/List;)Ljava/util/Vector;
    .locals 3

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->asFile()Lcom/jscape/inet/ftp/FtpFile;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/jscape/inet/ftp/FtpFile;->setFtpFileParser(Lcom/jscape/inet/ftp/FtpFileParser;)V

    if-eqz v1, :cond_1

    invoke-virtual {v0, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    if-nez v1, :cond_0

    :cond_1
    return-object v0
.end method

.method private a(Ljava/io/BufferedReader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    return-void
.end method

.method private b(Ljava/io/BufferedReader;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    :cond_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-direct {p0, v2}, Lcom/jscape/inet/ftp/MvsParser;->a(Ljava/lang/String;)Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;

    move-result-object v2

    if-eqz v0, :cond_1

    :try_start_0
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MvsParser;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method


# virtual methods
.method public getDateTime(Lcom/jscape/inet/ftp/FtpFile;)Ljava/util/Date;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getDate()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/MvsParser;->getFileDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    return-object p1
.end method

.method public getFileDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    sget-object v2, Lcom/jscape/inet/ftp/MvsParser;->a:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_1

    :try_start_0
    aget-object v2, v2, v1

    invoke-direct {p0, p1, v2}, Lcom/jscape/inet/ftp/MvsParser;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public parse(Ljava/io/BufferedReader;)Ljava/util/Enumeration;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/MvsParser;->a(Ljava/io/BufferedReader;)V

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/MvsParser;->b(Ljava/io/BufferedReader;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/MvsParser;->a(Ljava/util/List;)Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method
