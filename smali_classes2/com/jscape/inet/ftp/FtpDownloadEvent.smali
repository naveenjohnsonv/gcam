.class public Lcom/jscape/inet/ftp/FtpDownloadEvent;
.super Lcom/jscape/inet/ftp/FtpEvent;


# static fields
.field private static final g:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:J

.field private final f:J


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "\u0005\u0003\u0001n^O0J\u0003R2\u001bD-Q\u000bR$\u0017[\'F\u001a\u001d2\u0007\t2D\u001a\u001a`\u0010a\u0001\u0005.\u0012F#A\u000b\u0016`\u0018@.@N"

    const/16 v5, 0x31

    const/16 v6, 0x20

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x7f

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    const/16 v15, 0xd

    const/4 v2, 0x2

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v11, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const-string v4, "\u0013T\n\u0013\u001a\u001d\"\rLtZ\u0016D"

    move v6, v2

    move v8, v11

    move v5, v15

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v2, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v2

    :goto_3
    const/16 v9, 0x69

    add-int/2addr v7, v10

    add-int v2, v7, v6

    invoke-virtual {v4, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ftp/FtpDownloadEvent;->g:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v3, v14, 0x7

    if-eqz v3, :cond_8

    if-eq v3, v10, :cond_7

    if-eq v3, v2, :cond_9

    const/4 v2, 0x3

    if-eq v3, v2, :cond_6

    if-eq v3, v0, :cond_5

    const/4 v2, 0x5

    if-eq v3, v2, :cond_4

    const/16 v15, 0x3d

    goto :goto_4

    :cond_4
    const/16 v15, 0x56

    goto :goto_4

    :cond_5
    move v15, v10

    goto :goto_4

    :cond_6
    const/16 v15, 0x3f

    goto :goto_4

    :cond_7
    const/16 v15, 0x11

    goto :goto_4

    :cond_8
    const/16 v15, 0x5a

    :cond_9
    :goto_4
    xor-int v2, v9, v15

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/FtpEvent;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ftp/FtpDownloadEvent;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/ftp/FtpDownloadEvent;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/inet/ftp/FtpDownloadEvent;->d:Ljava/lang/String;

    iput-wide p5, p0, Lcom/jscape/inet/ftp/FtpDownloadEvent;->e:J

    iput-wide p7, p0, Lcom/jscape/inet/ftp/FtpDownloadEvent;->f:J

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/inet/ftp/FtpListener;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    invoke-interface {p1, p0}, Lcom/jscape/inet/ftp/FtpListener;->download(Lcom/jscape/inet/ftp/FtpDownloadEvent;)V

    :cond_1
    return-void
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpDownloadEvent;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpDownloadEvent;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpDownloadEvent;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ftp/FtpDownloadEvent;->e:J

    return-wide v0
.end method

.method public getTime()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/ftp/FtpDownloadEvent;->f:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftp/FtpDownloadEvent;->g:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ftp/FtpDownloadEvent;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/ftp/FtpDownloadEvent;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/ftp/FtpDownloadEvent;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ftp/FtpDownloadEvent;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
