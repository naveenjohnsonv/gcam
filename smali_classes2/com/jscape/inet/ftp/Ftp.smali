.class public Lcom/jscape/inet/ftp/Ftp;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/AutoCloseable;


# static fields
.field public static final ASCII:I = 0x1

.field public static final AUTO:I = 0x0

.field public static final BINARY:I = 0x2

.field public static final CURRENT_DIR:Ljava/lang/String; = "."

.field public static final UP_DIR:Ljava/lang/String;

.field private static final a:Lcom/jscape/util/m/g;

.field private static final i:[Ljava/lang/String;


# instance fields
.field private b:Ljava/io/File;

.field private c:Lcom/jscape/inet/ftp/FtpFileParser;

.field private d:Lcom/jscape/inet/ftp/FtpImplementation;

.field private e:Ljava/util/Vector;

.field private f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/jscape/inet/ftp/Ftp$FtpDownloadItem;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/Vector;

.field private h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x27

    new-array v0, v0, [Ljava/lang/String;

    const/16 v1, 0x1d

    const/4 v3, 0x0

    const-string v4, "\u001b.\u00188\u0015?\u0017r6\u001c/\u0017?\u001fr!\u001b(\u001c*[$#\u0018(\u0017~Ar\u0015r!\u001b(\u001e:[<-\u0000}\u0010;[1*\u00153\u0015;\u001f\u0004\n\u0001&\u001e\u001d|b9(\u0001*[0\'T:\u0000;\u001a&*\u0011/R*\u00133,T\'\u0017,\u0014|\u0015r!\u001b(\u001e:[<-\u0000}\u0010;[1*\u00153\u0015;\u001f\u001d\u0006+\u00198\u0001*\u001a?2T2\u0014~\u0017=!\u00151R:\u0012 \'\u0017)\u001d,\u0002r\u0002 5\u001d\u0006+\u00198\u0001*\u001a?2T2\u0014~\u0017=!\u00151R:\u0012 \'\u0017)\u001d,\u0002r\u0004\n\u0001&\u001e\u0019\u0006+\u00198\u0001*\u001a?2T2\u0014~\t7/\u001b)\u0017~\u001d;.\u0011}\u0016r!\u001b(\u001e:[<-\u0000}\u0010;[1*\u00153\u0015;\u001f| >-\u0017<\u001e~\u001f;0\u0011>\u00061\t+b\u00102\u0017-[<-\u0000}\u0017&\u0012!6N}\u0018\u001e-\u0017<\u001e~\u001d;.\u0011}\u001d,[6+\u00068\u0011*\u0014 ;T\u001d\u0011-\u00190\u00130\u001fr\u001a7\u000f1~\u0012!b\u001a2\u0006~\u0008\'2\u00042\u0000*\u001e6\u0016r!\u001b(\u001c*[<-\u0000}\u0010;[1*\u00153\u0015;\u001f|\u0010r&\u001b8\u0001~\u0015=6T8\n7\u0008&l\u0016r!\u001c<\u001c9\u001e6b\u0007(\u0011=\u001e!1\u0012(\u001e2\u0002|\u0018\u0006+\u00198\u0001*\u001a?2T2\u0014~\u0017=!\u00151R8\u0012>\'T\u0016r!\u001c<\u001c9\u001e6b\u0007(\u0011=\u001e!1\u0012(\u001e2\u0002|\u0002_H\u001e\u0006+\u00198\u0001*\u001a?2T;\u001d,[>-\u0017<\u001e~\u001f;0\u0011>\u00061\t+b\u0002|l\u0017\u0011*\u0011>\u0019-\u000e?b\u00068\u0006+\t<\'\u0010}\u0014?\u0017!\'\u001e\u0006+\u00198\u0001*\u001a?2T2\u0014~\t7/\u001b)\u0017~\u001f;0\u0011>\u00061\t+b\u001d|b9(\u0001*[0\'T:\u0000;\u001a&*\u0011/R*\u00133,T\'\u0017,\u0014|\u0019\u0006+\u00198\u0001*\u001a?2T2\u0014~\t7/\u001b)\u0017~\u001d;.\u0011}\u0017\u0011*\u0011>\u0019-\u000e?b\u00068\u0006+\t<\'\u0010}\u0014?\u0017!\'\u001e\u0006+\u00198\u0001*\u001a?2T2\u0014~\t7/\u001b)\u0017~\u001f;0\u0011>\u00061\t+b\u0004|6\u0019-\u0002|l\u0010r&\u001b8\u0001~\u0015=6T8\n7\u0008&l\u0016r!\u001b(\u001e:[<-\u0000}\u0010;[1*\u00153\u0015;\u001f|\u001d\u001b.\u00188\u0015?\u0017r6\u001c/\u0017?\u001fr!\u001b(\u001c*[$#\u0018(\u0017~Ar\u0018\u001e-\u0017<\u001e~\u001d;.\u0011}\u001d,[6+\u00068\u0011*\u0014 ;T\u001e\u0006+\u00198\u0001*\u001a?2T;\u001d,[>-\u0017<\u001e~\u001f;0\u0011>\u00061\t+b\u0018\u0006+\u00198\u0001*\u001a?2T2\u0014~\u0017=!\u00151R8\u0012>\'T\u0018r!\u001c<\u001c9\u001e6b\u0007(\u0011=\u001e!1\u0012(\u001e2\u0002|b/"

    const/16 v5, 0x32b

    move v7, v1

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x6d

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x5

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x1e

    const-string v4, "\u0008\u0003$\u001cP\u0018p#\u001e>\u001e;\u001c4`\u0005*\u0013?\u001c#3\u0010*\u001c0\u0000~`-"

    move v8, v11

    move v7, v15

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x6f

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v0, v0, v1

    sput-object v0, Lcom/jscape/inet/ftp/Ftp;->UP_DIR:Ljava/lang/String;

    new-instance v0, Lcom/jscape/util/m/d;

    invoke-direct {v0}, Lcom/jscape/util/m/d;-><init>()V

    sput-object v0, Lcom/jscape/inet/ftp/Ftp;->a:Lcom/jscape/util/m/g;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v10, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    if-eq v1, v15, :cond_4

    const/16 v1, 0x16

    goto :goto_4

    :cond_4
    const/16 v1, 0x33

    goto :goto_4

    :cond_5
    const/16 v1, 0x1f

    goto :goto_4

    :cond_6
    const/16 v1, 0x30

    goto :goto_4

    :cond_7
    const/16 v1, 0x19

    goto :goto_4

    :cond_8
    const/16 v1, 0x2f

    goto :goto_4

    :cond_9
    const/16 v1, 0x3f

    :goto_4
    xor-int/2addr v1, v9

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/16 v1, 0x1d

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 1

    new-instance v0, Lcom/jscape/inet/ftp/FtpBaseImplementation;

    invoke-direct {v0}, Lcom/jscape/inet/ftp/FtpBaseImplementation;-><init>()V

    invoke-direct {p0, v0}, Lcom/jscape/inet/ftp/Ftp;-><init>(Lcom/jscape/inet/ftp/FtpImplementation;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/inet/ftp/FtpImplementation;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lcom/jscape/inet/ftp/Ftp;->e:Ljava/util/Vector;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/jscape/inet/ftp/Ftp;->f:Ljava/util/HashMap;

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lcom/jscape/inet/ftp/Ftp;->g:Ljava/util/Vector;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/jscape/inet/ftp/Ftp;->h:Ljava/util/HashMap;

    new-instance v1, Lcom/jscape/util/d;

    invoke-direct {v1}, Lcom/jscape/util/d;-><init>()V

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {v1}, Lcom/jscape/util/d;->a()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p1, p0}, Lcom/jscape/inet/ftp/FtpImplementation;->setFtp(Lcom/jscape/inet/ftp/Ftp;)V

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-virtual {v1}, Lcom/jscape/util/d;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/jscape/inet/ftp/FtpBaseImplementation;

    invoke-direct {v0}, Lcom/jscape/inet/ftp/FtpBaseImplementation;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/jscape/inet/ftp/Ftp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ftp/FtpImplementation;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    new-instance v5, Lcom/jscape/inet/ftp/FtpBaseImplementation;

    invoke-direct {v5}, Lcom/jscape/inet/ftp/FtpBaseImplementation;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ftp/Ftp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/jscape/inet/ftp/FtpImplementation;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/jscape/inet/ftp/FtpImplementation;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p5}, Lcom/jscape/inet/ftp/Ftp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ftp/FtpImplementation;)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p1, p4}, Lcom/jscape/inet/ftp/FtpImplementation;->setPort(I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ftp/FtpImplementation;)V
    .locals 0

    invoke-direct {p0, p4}, Lcom/jscape/inet/ftp/Ftp;-><init>(Lcom/jscape/inet/ftp/FtpImplementation;)V

    iget-object p4, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p4, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setHost(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->setUser(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftp/FtpImplementation;->setPassword(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 6

    new-instance v5, Lcom/jscape/inet/ftp/FtpBaseImplementation;

    invoke-direct {v5}, Lcom/jscape/inet/ftp/FtpBaseImplementation;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ftp/Ftp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Lcom/jscape/inet/ftp/FtpImplementation;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Lcom/jscape/inet/ftp/FtpImplementation;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p5}, Lcom/jscape/inet/ftp/Ftp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ftp/FtpImplementation;)V

    iput-object p4, p0, Lcom/jscape/inet/ftp/Ftp;->b:Ljava/io/File;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/jscape/inet/ftp/FtpBaseImplementation;

    invoke-direct {v0}, Lcom/jscape/inet/ftp/FtpBaseImplementation;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/jscape/inet/ftp/Ftp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ftp/FtpImplementation;)V

    invoke-virtual {p0, p4}, Lcom/jscape/inet/ftp/Ftp;->setAccount(Ljava/lang/String;)V

    return-void
.end method

.method static a(Lcom/jscape/inet/ftp/Ftp;)Lcom/jscape/inet/ftp/FtpImplementation;
    .locals 0

    iget-object p0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    return-object p0
.end method

.method private a()Ljava/lang/String;
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const-string v0, "."

    return-object v0
.end method

.method private a(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    sget-object v0, Lcom/jscape/inet/ftp/Checksum;->CRC32:Lcom/jscape/inet/ftp/Checksum;

    const-wide v1, 0x7fffffffffffffffL

    invoke-virtual {v0, p1, v1, v2}, Lcom/jscape/inet/ftp/Checksum;->of(Ljava/io/InputStream;J)[B

    move-result-object p1

    const-string v0, ""

    invoke-static {p1, v0}, Lcom/jscape/util/W;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const-string v0, "\\"

    const-string v1, "/"

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    if-eqz v2, :cond_8

    :try_start_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_7

    if-nez v3, :cond_8

    :try_start_1
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_8

    if-eqz v2, :cond_1

    if-eqz v3, :cond_0

    goto/16 :goto_3

    :cond_0
    if-eqz v2, :cond_3

    :try_start_2
    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    if-nez v3, :cond_2

    :try_start_3
    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1

    if-eqz v2, :cond_4

    if-eqz v3, :cond_3

    goto :goto_1

    :catch_1
    move-exception p0

    :try_start_4
    invoke-static {p0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p0

    :try_start_5
    invoke-static {p0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_2
    :goto_1
    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_6

    :cond_3
    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    :cond_4
    const/4 v4, -0x1

    if-eqz v2, :cond_6

    if-eq v3, v4, :cond_5

    :try_start_6
    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_3

    return-object p0

    :catch_3
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_5
    if-eqz v2, :cond_7

    :try_start_7
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_2

    :catch_4
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_6
    :goto_2
    if-eq v3, v4, :cond_7

    :try_start_8
    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_5

    return-object p0

    :catch_5
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_7
    return-object p0

    :catch_6
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :catch_7
    move-exception p0

    :try_start_9
    invoke-static {p0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_8

    :catch_8
    move-exception p0

    :try_start_a
    invoke-static {p0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_9

    :catch_9
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_8
    :goto_3
    return-object p0
.end method

.method private static a(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    return-object p0
.end method

.method static a(Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->c(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/io/File;ZLjava/util/Vector;)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p4}, Ljava/util/Vector;->size()I

    move-result v2

    if-lez v2, :cond_a

    :try_start_0
    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_c

    if-nez v2, :cond_a

    :try_start_1
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V

    invoke-virtual {p4, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_d

    :cond_1
    check-cast v2, Ljava/lang/String;

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v4}, Lcom/jscape/util/Q;->d(Ljava/io/File;Ljava/io/File;)Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_3

    if-eqz v2, :cond_2

    :try_start_2
    const-string v5, ""

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    if-nez v5, :cond_2

    :try_start_3
    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftp/Ftp;->makeLocalDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftp/Ftp;->download(Ljava/lang/String;)Ljava/io/File;

    :cond_3
    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_9

    :try_start_4
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v7
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7

    const-wide/16 v9, 0x0

    cmp-long v5, v7, v9

    if-lez v5, :cond_8

    if-eqz v0, :cond_4

    if-eqz p3, :cond_8

    if-eqz v0, :cond_9

    :try_start_5
    sget-object v5, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/4 v7, 0x2

    aget-object v5, v5, v7

    invoke-virtual {p0, v5}, Lcom/jscape/inet/ftp/Ftp;->isFeatureSupported(Ljava/lang/String;)Z

    move-result v5
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_b

    goto :goto_1

    :cond_4
    move v5, p3

    :goto_1
    if-eqz v5, :cond_8

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v5

    :try_start_6
    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    if-eqz v0, :cond_6

    if-nez v7, :cond_5

    :try_start_7
    const-string v7, "\\"

    invoke-virtual {v5, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    if-eqz v0, :cond_6

    if-nez v7, :cond_5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_5
    if-eqz v0, :cond_9

    :try_start_8
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v2, v4}, Lcom/jscape/inet/ftp/Ftp;->checksum(Ljava/io/File;Ljava/lang/String;)Z

    move-result v7
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_6
    :goto_2
    if-eqz v7, :cond_7

    goto :goto_3

    :cond_7
    :try_start_9
    new-instance p1, Lcom/jscape/inet/ftp/FtpException;

    sget-object p2, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 p3, 0x16

    aget-object p2, p2, p3

    invoke-direct {p1, p2}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catch_4
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    :catch_5
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_8
    :goto_3
    invoke-virtual {p4, v3}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    goto :goto_4

    :catch_7
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    :catch_8
    move-exception p1

    :try_start_d
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_9

    :catch_9
    move-exception p1

    :try_start_e
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_a

    :catch_a
    move-exception p1

    :try_start_f
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_b

    :catch_b
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_9
    :goto_4
    if-nez v0, :cond_0

    goto :goto_5

    :catch_c
    move-exception p1

    :try_start_10
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_d

    :catch_d
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_a
    :goto_5
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/io/File;ZLjava/util/Vector;I)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    invoke-virtual/range {p4 .. p4}, Ljava/util/Vector;->size()I

    move-result v0

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    move/from16 v5, p5

    const/4 v6, 0x0

    :cond_0
    if-ge v6, v5, :cond_1

    :try_start_0
    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    invoke-virtual {v3, v7}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v6, v6, 0x1

    if-eqz v2, :cond_2

    if-nez v2, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    const/4 v6, 0x0

    :cond_2
    const/4 v5, 0x0

    :goto_1
    if-ge v5, v0, :cond_6

    if-eqz v2, :cond_4

    :try_start_1
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v7
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v2, :cond_3

    if-lt v6, v7, :cond_4

    const/4 v6, 0x0

    goto :goto_2

    :cond_3
    move-object/from16 v0, p1

    move-object v15, v1

    move v8, v7

    move v7, v6

    move v6, v5

    move-object v5, v3

    move-object v3, v2

    move/from16 v2, p3

    goto :goto_5

    :catch_1
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_4
    :goto_2
    invoke-virtual {v3, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Vector;

    move-object/from16 v8, p4

    invoke-virtual {v8, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v6, 0x1

    invoke-virtual {v3, v7, v6}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    add-int/lit8 v5, v5, 0x1

    if-nez v2, :cond_5

    goto :goto_3

    :cond_5
    move v6, v9

    goto :goto_1

    :cond_6
    :goto_3
    move-object/from16 v0, p1

    move-object v7, v1

    move-object v5, v3

    const/4 v6, 0x0

    move-object v3, v2

    move/from16 v2, p3

    :goto_4
    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v8

    move-object v15, v7

    move v7, v6

    :goto_5
    if-ge v7, v8, :cond_d

    invoke-virtual {v5, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Vector;

    :try_start_2
    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v8
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    if-eqz v3, :cond_c

    if-nez v8, :cond_7

    move-object v4, v15

    goto :goto_6

    :cond_7
    new-instance v8, Lcom/jscape/inet/ftp/Ftp$FtpDownloadItem;

    iget-object v14, v15, Lcom/jscape/inet/ftp/Ftp;->b:Ljava/io/File;

    const/16 v16, 0x0

    move-object v9, v8

    move-object v10, v15

    move-object v11, v15

    move-object v12, v7

    move v13, v2

    move-object/from16 v17, v14

    move-object v14, v0

    move-object v4, v15

    move-object/from16 v15, v17

    invoke-direct/range {v9 .. v16}, Lcom/jscape/inet/ftp/Ftp$FtpDownloadItem;-><init>(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/util/Vector;ZLjava/lang/String;Ljava/io/File;Lcom/jscape/inet/ftp/Ftp$1;)V

    iget-object v9, v4, Lcom/jscape/inet/ftp/Ftp;->e:Ljava/util/Vector;

    invoke-virtual {v9, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const/4 v9, 0x0

    :cond_8
    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v10

    if-ge v9, v10, :cond_9

    :try_start_3
    iget-object v10, v4, Lcom/jscape/inet/ftp/Ftp;->f:Ljava/util/HashMap;

    invoke-virtual {v7, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    add-int/lit8 v9, v9, 0x1

    if-eqz v3, :cond_a

    if-nez v3, :cond_8

    goto :goto_6

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_9
    :goto_6
    add-int/lit8 v6, v6, 0x1

    :cond_a
    if-nez v3, :cond_b

    goto :goto_7

    :cond_b
    move-object v7, v4

    goto :goto_4

    :cond_c
    move-object v4, v15

    goto :goto_8

    :catch_3
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_d
    move-object v4, v15

    :goto_7
    const/4 v8, 0x0

    :cond_e
    :goto_8
    iget-object v0, v4, Lcom/jscape/inet/ftp/Ftp;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v8, v0, :cond_f

    :try_start_4
    iget-object v0, v4, Lcom/jscape/inet/ftp/Ftp;->e:Ljava/util/Vector;

    invoke-virtual {v0, v8}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/ftp/Ftp$FtpDownloadItem;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp$FtpDownloadItem;->start()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    add-int/lit8 v8, v8, 0x1

    if-eqz v3, :cond_10

    if-nez v3, :cond_e

    goto :goto_9

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_f
    :goto_9
    const/4 v8, 0x0

    :cond_10
    const/4 v0, 0x0

    const/4 v2, 0x0

    :cond_11
    iget-object v5, v4, Lcom/jscape/inet/ftp/Ftp;->e:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v2, v5, :cond_17

    iget-object v5, v4, Lcom/jscape/inet/ftp/Ftp;->e:Ljava/util/Vector;

    invoke-virtual {v5, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/jscape/inet/ftp/Ftp$FtpDownloadItem;

    :try_start_5
    invoke-virtual {v5}, Lcom/jscape/inet/ftp/Ftp$FtpDownloadItem;->error()Z

    move-result v6
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7

    if-eqz v3, :cond_18

    const/4 v7, 0x1

    if-eqz v3, :cond_13

    if-eqz v6, :cond_12

    invoke-virtual {v5}, Lcom/jscape/inet/ftp/Ftp$FtpDownloadItem;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    move v8, v7

    :cond_12
    invoke-virtual {v4}, Lcom/jscape/inet/ftp/Ftp;->interrupted()Z

    move-result v6

    :cond_13
    if-eqz v3, :cond_15

    if-ne v6, v7, :cond_14

    :try_start_6
    invoke-virtual {v4}, Lcom/jscape/inet/ftp/Ftp;->abortDownloadThreads()V

    if-nez v3, :cond_17

    :cond_14
    invoke-virtual {v5}, Lcom/jscape/inet/ftp/Ftp$FtpDownloadItem;->getConnected()Z

    move-result v6
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_a

    :catch_5
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_15
    :goto_a
    if-ne v6, v7, :cond_16

    const-wide/16 v5, 0x64

    :try_start_7
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    :catch_6
    add-int/lit8 v2, v2, -0x1

    :cond_16
    add-int/2addr v2, v7

    if-nez v3, :cond_11

    goto :goto_b

    :catch_7
    move-exception v0

    move-object v2, v0

    :try_start_8
    invoke-static {v2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_17
    :goto_b
    move v6, v8

    :cond_18
    if-nez v6, :cond_19

    return-void

    :cond_19
    :try_start_9
    new-instance v2, Lcom/jscape/inet/ftp/FtpException;

    invoke-direct {v2, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method private a(Ljava/lang/String;ZLjava/lang/String;Ljava/util/Vector;Ljava/io/File;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;,
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "/"

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-virtual {p4}, Ljava/util/Vector;->size()I

    move-result v2

    if-lez v2, :cond_8

    :try_start_0
    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    if-eqz v1, :cond_1

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v2
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_e

    if-nez v2, :cond_8

    :try_start_1
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {p4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v2
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_f

    :cond_1
    check-cast v2, Ljava/lang/String;

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {p5, v3}, Lcom/jscape/util/Q;->d(Ljava/io/File;Ljava/io/File;)Ljava/lang/String;

    move-result-object v4

    if-eqz v1, :cond_2

    if-eqz v4, :cond_2

    if-eqz v1, :cond_2

    :try_start_2
    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_0

    if-nez v5, :cond_2

    :try_start_3
    invoke-direct {p0, v4}, Lcom/jscape/inet/ftp/Ftp;->c(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    :try_start_5
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v4
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_7

    if-eqz v1, :cond_7

    if-eqz v4, :cond_6

    :try_start_6
    invoke-virtual {p0, p3, v3}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/lang/String;Ljava/io/File;)V

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_8

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v1, :cond_7

    if-lez v4, :cond_6

    if-eqz v1, :cond_7

    if-eqz p2, :cond_6

    :try_start_7
    sget-object v4, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    invoke-virtual {p0, v4}, Lcom/jscape/inet/ftp/Ftp;->isFeatureSupported(Ljava/lang/String;)Z

    move-result v4
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_c

    if-eqz v1, :cond_7

    if-eqz v4, :cond_6

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v4

    :try_start_8
    invoke-virtual {v4, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_4

    if-eqz v1, :cond_4

    if-nez v5, :cond_3

    :try_start_9
    const-string v5, "\\"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_5

    if-eqz v1, :cond_4

    if-nez v5, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/jscape/inet/ftp/Ftp;->checksum(Ljava/io/File;Ljava/lang/String;)Z

    move-result v5

    :cond_4
    if-eqz v1, :cond_7

    if-eqz v5, :cond_5

    goto :goto_1

    :cond_5
    :try_start_a
    new-instance p1, Lcom/jscape/inet/ftp/FtpException;

    sget-object p2, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 p3, 0x1a

    aget-object p2, p2, p3

    invoke-direct {p1, p2}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catch_4
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_5

    :catch_5
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_6

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_6
    :goto_1
    invoke-virtual {p4, v2}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    :cond_7
    if-nez v1, :cond_0

    goto :goto_2

    :catch_7
    move-exception p1

    :try_start_d
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_8

    :catch_8
    move-exception p1

    :try_start_e
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_e .. :try_end_e} :catch_9

    :catch_9
    move-exception p1

    :try_start_f
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_f .. :try_end_f} :catch_a

    :catch_a
    move-exception p1

    :try_start_10
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_b

    :catch_b
    move-exception p1

    :try_start_11
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_11
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_11 .. :try_end_11} :catch_c

    :catch_c
    move-exception p1

    :try_start_12
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_12
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_12 .. :try_end_12} :catch_d

    :catch_d
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catch_e
    move-exception p1

    :try_start_13
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_13
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_13 .. :try_end_13} :catch_f

    :catch_f
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_8
    :goto_2
    return-void
.end method

.method private a(Ljava/lang/String;ZLjava/lang/String;Ljava/util/Vector;Ljava/io/File;I)V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;,
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->g:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual/range {p4 .. p4}, Ljava/util/Vector;->size()I

    move-result v2

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    move/from16 v5, p6

    const/4 v6, 0x0

    :cond_0
    if-ge v6, v5, :cond_1

    :try_start_0
    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    invoke-virtual {v3, v7}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v6, v6, 0x1

    if-eqz v0, :cond_2

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    const/4 v6, 0x0

    :cond_2
    const/4 v5, 0x0

    :goto_1
    if-ge v5, v2, :cond_6

    if-eqz v0, :cond_4

    :try_start_1
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v7
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v0, :cond_3

    if-lt v6, v7, :cond_4

    const/4 v6, 0x0

    goto :goto_2

    :cond_3
    move/from16 v2, p2

    move-object v15, v1

    move-object v9, v3

    move v8, v6

    move v10, v7

    move-object/from16 v3, p3

    move-object/from16 v6, p5

    move-object v7, v0

    move-object/from16 v0, p1

    goto :goto_5

    :catch_1
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_4
    :goto_2
    invoke-virtual {v3, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Vector;

    move-object/from16 v8, p4

    invoke-virtual {v8, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v6, 0x1

    invoke-virtual {v3, v7, v6}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    add-int/lit8 v5, v5, 0x1

    if-nez v0, :cond_5

    goto :goto_3

    :cond_5
    move v6, v9

    goto :goto_1

    :cond_6
    :goto_3
    move/from16 v2, p2

    move-object/from16 v5, p5

    move-object v6, v0

    move-object v9, v1

    move-object v7, v3

    const/4 v8, 0x0

    move-object/from16 v0, p1

    move-object/from16 v3, p3

    :goto_4
    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v10

    move-object v15, v9

    move-object v9, v7

    move-object v7, v6

    move-object v6, v5

    move v5, v8

    :goto_5
    if-ge v8, v10, :cond_d

    invoke-virtual {v9, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Vector;

    :try_start_2
    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v10
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    if-eqz v7, :cond_c

    if-nez v10, :cond_7

    move-object v4, v15

    goto :goto_6

    :cond_7
    new-instance v10, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;

    const/16 v19, 0x0

    move-object v11, v10

    move-object v12, v15

    move-object v13, v15

    move-object v14, v3

    move-object v4, v15

    move-object v15, v8

    move-object/from16 v16, v0

    move-object/from16 v17, v6

    move/from16 v18, v2

    invoke-direct/range {v11 .. v19}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;-><init>(Lcom/jscape/inet/ftp/Ftp;Lcom/jscape/inet/ftp/Ftp;Ljava/lang/String;Ljava/util/Vector;Ljava/lang/String;Ljava/io/File;ZLcom/jscape/inet/ftp/Ftp$1;)V

    iget-object v11, v4, Lcom/jscape/inet/ftp/Ftp;->g:Ljava/util/Vector;

    invoke-virtual {v11, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const/4 v11, 0x0

    :cond_8
    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v12

    if-ge v11, v12, :cond_9

    :try_start_3
    iget-object v12, v4, Lcom/jscape/inet/ftp/Ftp;->h:Ljava/util/HashMap;

    invoke-virtual {v8, v11}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    add-int/lit8 v11, v11, 0x1

    if-eqz v7, :cond_a

    if-nez v7, :cond_8

    goto :goto_6

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_9
    :goto_6
    add-int/lit8 v5, v5, 0x1

    :cond_a
    move v8, v5

    if-nez v7, :cond_b

    goto :goto_7

    :cond_b
    move-object v5, v6

    move-object v6, v7

    move-object v7, v9

    move-object v9, v4

    goto :goto_4

    :cond_c
    move-object v4, v15

    goto :goto_8

    :catch_3
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_d
    move-object v4, v15

    :goto_7
    const/4 v10, 0x0

    :cond_e
    :goto_8
    iget-object v0, v4, Lcom/jscape/inet/ftp/Ftp;->g:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v10, v0, :cond_f

    :try_start_4
    iget-object v0, v4, Lcom/jscape/inet/ftp/Ftp;->g:Ljava/util/Vector;

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->start()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    add-int/lit8 v10, v10, 0x1

    if-eqz v7, :cond_10

    if-nez v7, :cond_e

    goto :goto_9

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_f
    :goto_9
    const/4 v10, 0x0

    :cond_10
    const/4 v0, 0x0

    const/4 v2, 0x0

    :cond_11
    iget-object v3, v4, Lcom/jscape/inet/ftp/Ftp;->g:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_17

    iget-object v3, v4, Lcom/jscape/inet/ftp/Ftp;->g:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;

    :try_start_5
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->error()Z

    move-result v5
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7

    if-eqz v7, :cond_18

    const/4 v6, 0x1

    if-eqz v7, :cond_13

    if-eqz v5, :cond_12

    invoke-virtual {v3}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    move v10, v6

    :cond_12
    invoke-virtual {v4}, Lcom/jscape/inet/ftp/Ftp;->interrupted()Z

    move-result v5

    :cond_13
    if-eqz v7, :cond_15

    if-ne v5, v6, :cond_14

    :try_start_6
    invoke-virtual {v4}, Lcom/jscape/inet/ftp/Ftp;->abortUploadThreads()V

    if-nez v7, :cond_17

    :cond_14
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->getConnected()Z

    move-result v5
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_a

    :catch_5
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_15
    :goto_a
    if-ne v5, v6, :cond_16

    const-wide/16 v8, 0x64

    :try_start_7
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    :catch_6
    add-int/lit8 v2, v2, -0x1

    :cond_16
    add-int/2addr v2, v6

    if-nez v7, :cond_11

    goto :goto_b

    :catch_7
    move-exception v0

    move-object v2, v0

    :try_start_8
    invoke-static {v2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_17
    :goto_b
    move v5, v10

    :cond_18
    if-nez v5, :cond_19

    return-void

    :cond_19
    :try_start_9
    new-instance v2, Lcom/jscape/inet/ftp/FtpException;

    invoke-direct {v2, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method private a(Ljava/util/Vector;Ljava/io/File;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    const-string v0, "/"

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    :try_start_1
    invoke-virtual {v2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v1, :cond_1

    if-nez v3, :cond_0

    :try_start_2
    const-string v3, "\\"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    if-eqz v1, :cond_1

    if-nez v3, :cond_0

    :try_start_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    :cond_0
    move-object v0, p0

    goto :goto_1

    :cond_1
    move-object v0, p0

    :goto_0
    if-eqz v3, :cond_3

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-static {p2, v4, v3}, Lcom/jscape/util/Q;->a(Ljava/io/File;Ljava/io/File;Z)Ljava/lang/String;

    move-result-object v3
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    :try_start_4
    invoke-direct {v0, v3}, Lcom/jscape/inet/ftp/Ftp;->c(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    if-eqz v1, :cond_3

    if-nez v1, :cond_2

    goto :goto_2

    :cond_2
    :goto_1
    :try_start_5
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :cond_3
    :goto_2
    return-void

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    :catch_2
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    :catch_3
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method private static a(Lcom/jscape/inet/ftp/FtpFile;)Z
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object p0

    :try_start_0
    const-string v1, "."

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_2

    if-nez v1, :cond_1

    :try_start_1
    sget-object v1, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v2, 0x1d

    aget-object v1, v1, v2

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v0, :cond_2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move p0, v1

    :goto_1
    return p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    :try_start_2
    invoke-static {p0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
.end method

.method private static a(Ljava/io/File;)Z
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p0

    :try_start_0
    const-string v1, "."

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_2

    if-nez v1, :cond_1

    :try_start_1
    sget-object v1, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v2, 0x1d

    aget-object v1, v1, v2

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v0, :cond_2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move p0, v1

    :goto_1
    return p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    :try_start_2
    invoke-static {p0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
.end method

.method private a(Ljava/io/InputStream;Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2}, Lcom/jscape/inet/ftp/Ftp;->getRemoteFileChecksum(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method private static b(Ljava/io/File;)Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/nio/file/OpenOption;

    invoke-static {p0, v0}, Ljava/nio/file/Files;->newInputStream(Ljava/nio/file/Path;[Ljava/nio/file/OpenOption;)Ljava/io/InputStream;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-direct {v0, p0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private b(Ljava/lang/String;)Ljava/util/Vector;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    const-string v0, "/"

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDirListing()Ljava/util/Enumeration;

    move-result-object v3

    :try_start_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_5

    if-eqz v2, :cond_1

    if-nez v4, :cond_0

    :try_start_1
    const-string v4, "\\"

    invoke-virtual {p1, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_6

    if-eqz v2, :cond_1

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    move-object v0, p0

    goto/16 :goto_3

    :cond_1
    move-object v0, p0

    :goto_0
    if-eqz v4, :cond_7

    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jscape/inet/ftp/FtpFile;

    :try_start_2
    invoke-virtual {v4}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v5
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_3

    if-eqz v2, :cond_2

    if-eqz v5, :cond_5

    :try_start_3
    invoke-static {v4}, Lcom/jscape/inet/ftp/Ftp;->a(Lcom/jscape/inet/ftp/FtpFile;)Z

    move-result v5
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_4

    :cond_2
    if-eqz v2, :cond_3

    if-nez v5, :cond_5

    if-eqz v2, :cond_4

    :try_start_4
    invoke-direct {v0, v4}, Lcom/jscape/inet/ftp/Ftp;->b(Lcom/jscape/inet/ftp/FtpFile;)Z

    move-result v5
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    if-eqz v5, :cond_4

    :try_start_5
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_1

    if-nez v2, :cond_5

    goto :goto_2

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_4
    :goto_2
    invoke-virtual {v0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/jscape/inet/ftp/Ftp;->b(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0, v5}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    :cond_5
    if-nez v2, :cond_6

    goto :goto_4

    :cond_6
    :goto_3
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    goto :goto_0

    :catch_3
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_7
    :goto_4
    return-object v1

    :catch_5
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_6

    :catch_6
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_7

    :catch_7
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method private b()V
    .locals 4

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v1, "."

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v1, "."
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x5c

    const/16 v3, 0x2f

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    :try_start_2
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->b:Ljava/io/File;

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method private b(Ljava/util/Vector;Ljava/io/File;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p2, v2, v1}, Lcom/jscape/util/Q;->a(Ljava/io/File;Ljava/io/File;Z)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_2

    :cond_1
    :try_start_1
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftp/Ftp;->makeLocalDir(Ljava/lang/String;)Ljava/io/File;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_3
    :goto_0
    return-void
.end method

.method private b(Lcom/jscape/inet/ftp/FtpFile;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDirListing()Ljava/util/Enumeration;

    move-result-object p1

    const/4 v2, 0x1

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/ftp/FtpFile;

    :try_start_0
    invoke-static {v3}, Lcom/jscape/inet/ftp/Ftp;->a(Lcom/jscape/inet/ftp/FtpFile;)Z

    move-result v3
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_4

    if-eqz v0, :cond_1

    if-nez v3, :cond_2

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v3

    :goto_0
    if-nez v0, :cond_3

    :cond_2
    if-nez v0, :cond_0

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    move v3, v2

    :cond_4
    return v3
.end method

.method private static c(Ljava/io/File;)Ljava/io/OutputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p0

    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    :cond_1
    if-eqz v0, :cond_2

    if-eqz p1, :cond_3

    :cond_2
    :try_start_2
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->makeDir(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    :cond_3
    :goto_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    return-void

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method private d(Ljava/lang/String;)Lcom/jscape/util/m/f;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jscape/inet/ftp/Ftp;->a:Lcom/jscape/util/m/g;

    invoke-interface {v0, p1}, Lcom/jscape/util/m/g;->a(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    :try_start_1
    sget-object p1, Lcom/jscape/util/m/f;->a:Lcom/jscape/util/m/f;

    :goto_0
    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private d(Ljava/io/File;)V
    .locals 2

    invoke-virtual {p1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    :try_start_1
    invoke-direct {p0, v1}, Lcom/jscape/inet/ftp/Ftp;->d(Ljava/io/File;)V

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    :cond_0
    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method


# virtual methods
.method public abortDownloadThread(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/ftp/Ftp$FtpDownloadItem;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/Ftp$FtpDownloadItem;->interruptTransfer()V

    return-void
.end method

.method public abortDownloadThreads()V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->e:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->e:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ftp/Ftp$FtpDownloadItem;

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/Ftp$FtpDownloadItem;->cancelTransfer()V

    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public abortUploadThread(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->interruptTransfer()V

    return-void
.end method

.method public abortUploadThreads()V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->g:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->g:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/Ftp$FtpUploadItem;->cancelTransfer()V

    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public declared-synchronized addFtpListener(Lcom/jscape/inet/ftp/FtpListener;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->addListener(Lcom/jscape/inet/ftp/FtpListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized changePassword(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->changePassword(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized checksum(Ljava/io/File;Ljava/lang/String;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_5

    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/nio/file/OpenOption;

    invoke-static {p1, v1}, Ljava/nio/file/Files;->newInputStream(Ljava/nio/file/Path;[Ljava/nio/file/OpenOption;)Ljava/io/InputStream;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    :try_start_2
    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/io/InputStream;Ljava/lang/String;)Z

    move-result p2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz p1, :cond_0

    :try_start_3
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_5

    :cond_0
    :goto_0
    monitor-exit p0

    return p2

    :catchall_1
    move-exception p2

    :try_start_5
    throw p2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception v1

    if-eqz p1, :cond_1

    :try_start_6
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v2

    :try_start_7
    invoke-virtual {p2, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    goto :goto_1

    :catchall_4
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    throw v1
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    :catch_0
    move-exception p1

    :try_start_9
    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    :catchall_5
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized checksum([BLjava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {p0, v0, p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/io/InputStream;Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public clearProxySettings()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->clearProxySettings()V

    return-void
.end method

.method public close()V
    .locals 0

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->disconnect()V

    return-void
.end method

.method public declared-synchronized connect()Lcom/jscape/inet/ftp/Ftp;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->connect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized connect(Z)Lcom/jscape/inet/ftp/Ftp;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->connect(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized deleteDir(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ftp/Ftp;->deleteDir(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized deleteDir(Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->deleteDir(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized deleteFile(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->deleteFile(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized disconnect()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->disconnect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized download(Ljava/lang/String;)Ljava/io/File;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p1}, Lcom/jscape/inet/ftp/Ftp;->download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/ftp/Ftp;->d(Ljava/io/File;)V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->c(Ljava/io/File;)Ljava/io/OutputStream;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetPath(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftp/Ftp;->a()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {p0, v1, p2}, Lcom/jscape/inet/ftp/Ftp;->download(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    iget-object v4, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/FtpImplementation;->getPreserveDownloadTimestamp()Z

    move-result v4
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p1, :cond_0

    if-eqz v4, :cond_7

    :try_start_2
    iget-object v4, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v4
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    if-eqz p1, :cond_1

    if-nez v4, :cond_7

    :try_start_3
    iget-object v4, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/FtpImplementation;->isConnected()Z

    move-result v4
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p2

    :try_start_4
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_9
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_1
    :goto_0
    if-eqz p1, :cond_3

    if-nez v4, :cond_2

    :try_start_5
    iget-object v4, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/FtpImplementation;->connect()V

    iget-object v4, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/FtpImplementation;->reset()V

    iget-object v4, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v4, v2}, Lcom/jscape/inet/ftp/FtpImplementation;->setDir(Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :catch_1
    move-exception p2

    :try_start_6
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2

    :cond_2
    :goto_1
    invoke-virtual {p0, p2}, Lcom/jscape/inet/ftp/Ftp;->getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/io/File;->setLastModified(J)Z

    move-result v4
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_9
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_3
    if-eqz p1, :cond_5

    if-eqz v4, :cond_4

    :try_start_7
    iget-object p2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v4
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz p1, :cond_5

    if-eqz v4, :cond_4

    :try_start_8
    iget-object p2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object p2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v5, 0x11

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v5, 0x26

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v4, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, "]"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-nez p1, :cond_7

    goto :goto_2

    :catch_2
    move-exception p2

    :try_start_9
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_3
    move-exception p2

    :try_start_a
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_4
    move-exception p2

    :try_start_b
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2

    :cond_4
    :goto_2
    iget-object p2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    if-eqz p1, :cond_6

    invoke-virtual {p2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v4
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_3

    :catch_5
    move-exception p2

    :try_start_c
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_9
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :cond_5
    :goto_3
    if-eqz v4, :cond_7

    :try_start_d
    iget-object p2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :cond_6
    :try_start_e
    invoke-virtual {p2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object p2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v5, 0x23

    aget-object v5, v4, v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v5, 0xe

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_4

    :catch_6
    move-exception p2

    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_e .. :try_end_e} :catch_9
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :cond_7
    :goto_4
    :try_start_f
    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p1, v3}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetPath(Ljava/lang/String;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    goto :goto_5

    :catch_7
    move-exception p2

    :try_start_10
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_8
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :catch_8
    move-exception p2

    :try_start_11
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_11
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_11 .. :try_end_11} :catch_9
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :catch_9
    move-exception p2

    :try_start_12
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4
    :try_end_12
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_12 .. :try_end_12} :catch_a
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-eqz p1, :cond_8

    if-nez v2, :cond_8

    :try_start_13
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_13
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_13 .. :try_end_13} :catch_b
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :cond_8
    :try_start_14
    throw p2
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    :catch_a
    move-exception p1

    :try_start_15
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_15
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_15 .. :try_end_15} :catch_b
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    :catch_b
    move-exception p1

    :try_start_16
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    :goto_5
    :try_start_17
    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    iget-object p2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p2, v3}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetPath(Ljava/lang/String;)V

    throw p1
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized download(Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, p1, p2, v2, v3}, Lcom/jscape/inet/ftp/FtpImplementation;->download(Ljava/io/OutputStream;Ljava/lang/String;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    :try_start_3
    iget-object p2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p2, v0}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized downloadDir(Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/16 v3, 0x14

    const/4 v4, 0x1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    sget-object v6, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v7, 0x1d

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {p0, v5}, Lcom/jscape/inet/ftp/Ftp;->makeLocalDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_11
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDirListing()Ljava/util/Enumeration;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v7, :cond_5

    :try_start_4
    iget-object v7, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v7}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v7
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v1, :cond_6

    if-eqz v1, :cond_6

    if-nez v7, :cond_5

    :try_start_5
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/jscape/inet/ftp/FtpFile;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {v7}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v8
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v1, :cond_3

    if-nez v8, :cond_2

    :try_start_7
    invoke-virtual {v7}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/jscape/inet/ftp/Ftp;->download(Ljava/lang/String;)Ljava/io/File;

    if-nez v1, :cond_4

    :cond_2
    invoke-static {v7}, Lcom/jscape/inet/ftp/Ftp;->a(Lcom/jscape/inet/ftp/FtpFile;)Z

    move-result v8
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v5

    goto :goto_2

    :cond_3
    :goto_0
    if-nez v8, :cond_4

    :try_start_8
    invoke-virtual {v7}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/jscape/inet/ftp/Ftp;->downloadDir(Ljava/lang/String;)V
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    :catch_1
    move-exception v5

    :try_start_9
    invoke-static {v5}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v5

    throw v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :cond_4
    :goto_1
    if-nez v1, :cond_1

    goto :goto_3

    :catch_2
    move-exception v5

    :try_start_a
    invoke-static {v5}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v5

    throw v5
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_3
    move-exception v5

    :try_start_b
    invoke-static {v5}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v5

    throw v5
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :goto_2
    :try_start_c
    invoke-static {v5}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v5

    throw v5
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :catch_4
    move-exception v5

    :try_start_d
    invoke-static {v5}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v5

    throw v5
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_5
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catch_5
    move-exception v5

    :try_start_e
    invoke-static {v5}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v5

    throw v5
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :cond_5
    :goto_3
    :try_start_f
    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    :try_start_10
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->interrupted()Z

    move-result v7
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_e
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    :cond_6
    if-eqz v1, :cond_7

    if-nez v7, :cond_e

    :try_start_11
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getPreserveDownloadTimestamp()Z

    move-result v7
    :try_end_11
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_11 .. :try_end_11} :catch_6
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_e
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto :goto_4

    :catch_6
    move-exception p1

    :try_start_12
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_e
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    :cond_7
    :goto_4
    if-eqz v1, :cond_8

    if-ne v7, v4, :cond_e

    if-eqz v1, :cond_9

    :try_start_13
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->isConnected()Z

    move-result v7
    :try_end_13
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_13 .. :try_end_13} :catch_7
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_e
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    goto :goto_5

    :catch_7
    move-exception p1

    :try_start_14
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_14
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_14 .. :try_end_14} :catch_b
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_e
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    :cond_8
    :goto_5
    if-nez v7, :cond_a

    :try_start_15
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->connect()Lcom/jscape/inet/ftp/Ftp;
    :try_end_15
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_15 .. :try_end_15} :catch_8
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_e
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    :cond_9
    :try_start_16
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->reset()V

    goto :goto_6

    :catch_8
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_a
    :goto_6
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_16
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_16 .. :try_end_16} :catch_b
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_e
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    :try_start_17
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Ljava/io/File;->setLastModified(J)Z

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_17
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_17 .. :try_end_17} :catch_9
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_e
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    if-eqz v1, :cond_b

    :try_start_18
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result p1

    if-eqz p1, :cond_e

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_18
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_18 .. :try_end_18} :catch_a
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_e
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    :cond_b
    :try_start_19
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object p1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v7, 0x22

    aget-object v7, v6, v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x10

    aget-object v0, v6, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_19
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_19 .. :try_end_19} :catch_b
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_e
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    goto :goto_7

    :catch_9
    move-exception p1

    :try_start_1a
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1a .. :try_end_1a} :catch_a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_e
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1

    :catch_a
    move-exception p1

    :try_start_1b
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1b .. :try_end_1b} :catch_b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_e
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    :catch_b
    :try_start_1c
    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_1c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1c .. :try_end_1c} :catch_c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_e
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    if-eqz v1, :cond_c

    :try_start_1d
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result p1

    if-eqz p1, :cond_e

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_1d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1d .. :try_end_1d} :catch_d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_e
    .catchall {:try_start_1d .. :try_end_1d} :catchall_1

    :cond_c
    :try_start_1e
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v3, v2, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v2, v2, v4

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_1

    goto :goto_7

    :catch_c
    move-exception p1

    :try_start_1f
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1f .. :try_end_1f} :catch_d
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_e
    .catchall {:try_start_1f .. :try_end_1f} :catchall_1

    :catch_d
    move-exception p1

    :try_start_20
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_e
    .catchall {:try_start_20 .. :try_end_20} :catchall_1

    :catch_e
    :try_start_21
    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_21
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_21 .. :try_end_21} :catch_f
    .catchall {:try_start_21 .. :try_end_21} :catchall_1

    if-eqz v1, :cond_d

    :try_start_22
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result p1

    if-eqz p1, :cond_e

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_22
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_22 .. :try_end_22} :catch_10
    .catchall {:try_start_22 .. :try_end_22} :catchall_1

    :cond_d
    :try_start_23
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v1, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_1

    :cond_e
    :goto_7
    monitor-exit p0

    return-void

    :catch_f
    move-exception p1

    :try_start_24
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_24
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_24 .. :try_end_24} :catch_10
    .catchall {:try_start_24 .. :try_end_24} :catchall_1

    :catch_10
    move-exception p1

    :try_start_25
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_1

    :catch_11
    move-exception v5

    :try_start_26
    invoke-static {v5}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v5

    throw v5
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_0

    :catchall_0
    move-exception v5

    :try_start_27
    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_1

    :try_start_28
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->interrupted()Z

    move-result v2
    :try_end_28
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_28 .. :try_end_28} :catch_19
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_28} :catch_1b
    .catchall {:try_start_28 .. :try_end_28} :catchall_1

    if-eqz v1, :cond_f

    if-nez v2, :cond_16

    :try_start_29
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getPreserveDownloadTimestamp()Z

    move-result v2
    :try_end_29
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_29 .. :try_end_29} :catch_1a
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_29} :catch_1b
    .catchall {:try_start_29 .. :try_end_29} :catchall_1

    :cond_f
    if-eqz v1, :cond_10

    if-ne v2, v4, :cond_16

    if-eqz v1, :cond_11

    :try_start_2a
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->isConnected()Z

    move-result v2
    :try_end_2a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2a .. :try_end_2a} :catch_12
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_2a} :catch_1b
    .catchall {:try_start_2a .. :try_end_2a} :catchall_1

    goto :goto_8

    :catch_12
    move-exception p1

    :try_start_2b
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2b .. :try_end_2b} :catch_16
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_2b} :catch_1b
    .catchall {:try_start_2b .. :try_end_2b} :catchall_1

    :cond_10
    :goto_8
    if-nez v2, :cond_12

    :try_start_2c
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->connect()Lcom/jscape/inet/ftp/Ftp;
    :try_end_2c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2c .. :try_end_2c} :catch_13
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_2c} :catch_1b
    .catchall {:try_start_2c .. :try_end_2c} :catchall_1

    :cond_11
    :try_start_2d
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->reset()V

    goto :goto_9

    :catch_13
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_12
    :goto_9
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_2d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2d .. :try_end_2d} :catch_16
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_2d} :catch_1b
    .catchall {:try_start_2d .. :try_end_2d} :catchall_1

    :try_start_2e
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/io/File;->setLastModified(J)Z

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_2e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2e .. :try_end_2e} :catch_14
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_2e} :catch_1b
    .catchall {:try_start_2e .. :try_end_2e} :catchall_1

    if-eqz v1, :cond_13

    :try_start_2f
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result p1

    if-eqz p1, :cond_16

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_2f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2f .. :try_end_2f} :catch_15
    .catch Ljava/lang/Exception; {:try_start_2f .. :try_end_2f} :catch_1b
    .catchall {:try_start_2f .. :try_end_2f} :catchall_1

    :cond_13
    :try_start_30
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object p1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v8, v7, v3

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x12

    aget-object v2, v7, v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_30
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_30 .. :try_end_30} :catch_16
    .catch Ljava/lang/Exception; {:try_start_30 .. :try_end_30} :catch_1b
    .catchall {:try_start_30 .. :try_end_30} :catchall_1

    goto/16 :goto_a

    :catch_14
    move-exception p1

    :try_start_31
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_31
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_31 .. :try_end_31} :catch_15
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_31} :catch_1b
    .catchall {:try_start_31 .. :try_end_31} :catchall_1

    :catch_15
    move-exception p1

    :try_start_32
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_32
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_32 .. :try_end_32} :catch_16
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_32} :catch_1b
    .catchall {:try_start_32 .. :try_end_32} :catchall_1

    :catch_16
    :try_start_33
    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_33
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_33 .. :try_end_33} :catch_17
    .catch Ljava/lang/Exception; {:try_start_33 .. :try_end_33} :catch_1b
    .catchall {:try_start_33 .. :try_end_33} :catchall_1

    if-eqz v1, :cond_14

    :try_start_34
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result p1

    if-eqz p1, :cond_16

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_34
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_34 .. :try_end_34} :catch_18
    .catch Ljava/lang/Exception; {:try_start_34 .. :try_end_34} :catch_1b
    .catchall {:try_start_34 .. :try_end_34} :catchall_1

    :cond_14
    :try_start_35
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object p1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v3, v6, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, v6, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_35
    .catch Ljava/lang/Exception; {:try_start_35 .. :try_end_35} :catch_1b
    .catchall {:try_start_35 .. :try_end_35} :catchall_1

    goto :goto_a

    :catch_17
    move-exception p1

    :try_start_36
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_36
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_36 .. :try_end_36} :catch_18
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_36} :catch_1b
    .catchall {:try_start_36 .. :try_end_36} :catchall_1

    :catch_18
    move-exception p1

    :try_start_37
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_37
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_37} :catch_1b
    .catchall {:try_start_37 .. :try_end_37} :catchall_1

    :catch_19
    move-exception p1

    :try_start_38
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_38
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_38 .. :try_end_38} :catch_1a
    .catch Ljava/lang/Exception; {:try_start_38 .. :try_end_38} :catch_1b
    .catchall {:try_start_38 .. :try_end_38} :catchall_1

    :catch_1a
    move-exception p1

    :try_start_39
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_39
    .catch Ljava/lang/Exception; {:try_start_39 .. :try_end_39} :catch_1b
    .catchall {:try_start_39 .. :try_end_39} :catchall_1

    :catch_1b
    :try_start_3a
    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    if-eqz v1, :cond_15

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result p1
    :try_end_3a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3a .. :try_end_3a} :catch_1c
    .catchall {:try_start_3a .. :try_end_3a} :catchall_1

    if-eqz p1, :cond_16

    :try_start_3b
    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_3b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3b .. :try_end_3b} :catch_1d
    .catchall {:try_start_3b .. :try_end_3b} :catchall_1

    :cond_15
    :try_start_3c
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, v2, v4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_16
    :goto_a
    throw v5
    :try_end_3c
    .catchall {:try_start_3c .. :try_end_3c} :catchall_1

    :catch_1c
    move-exception p1

    :try_start_3d
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3d .. :try_end_3d} :catch_1d
    .catchall {:try_start_3d .. :try_end_3d} :catchall_1

    :catch_1d
    move-exception p1

    :try_start_3e
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3e
    .catchall {:try_start_3e .. :try_end_3e} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized downloadDir(Ljava/lang/String;IIZ)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v4, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v5, 0x15

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftp/Ftp;->makeLocalDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v3

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->b(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v4

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v4, v5}, Lcom/jscape/inet/ftp/Ftp;->b(Ljava/util/Vector;Ljava/io/File;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v5, 0x0

    move v6, v5

    :cond_1
    if-gt v5, p2, :cond_8

    :try_start_3
    iget-object v7, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v7}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v7
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v2, :cond_6

    if-nez v7, :cond_8

    :try_start_4
    invoke-direct {p0, p1, v4, p4, v3}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/String;Ljava/io/File;ZLjava/util/Vector;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/4 v7, 0x1

    goto :goto_2

    :catch_0
    move-exception v7

    if-eqz v2, :cond_3

    if-ge v5, p2, :cond_2

    goto :goto_0

    :cond_2
    :try_start_5
    new-instance p1, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_3
    :goto_0
    if-eqz v2, :cond_4

    if-ge v5, p2, :cond_5

    const/16 v7, 0x3e8

    move v8, p3

    goto :goto_1

    :cond_4
    move v7, p2

    move v8, v5

    :goto_1
    mul-int/2addr v8, v7

    int-to-long v7, v8

    :try_start_7
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catch_2
    :cond_5
    :try_start_8
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->disconnect()V

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->connect()Lcom/jscape/inet/ftp/Ftp;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3

    :cond_6
    :goto_2
    move v6, v7

    :goto_3
    if-eqz v6, :cond_7

    :try_start_9
    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-nez v2, :cond_8

    goto :goto_4

    :catch_3
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_4
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_7
    :goto_4
    add-int/lit8 v5, v5, 0x1

    if-nez v2, :cond_1

    goto :goto_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_8
    :goto_5
    monitor-exit p0

    return-void

    :catch_6
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized downloadDir(Ljava/lang/String;IIZI)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v7, p0

    move/from16 v8, p2

    move/from16 v9, p5

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v10, :cond_0

    if-eqz v9, :cond_1

    :cond_0
    const/4 v11, 0x1

    if-eqz v10, :cond_2

    if-ne v9, v11, :cond_2

    :cond_1
    :try_start_1
    invoke-virtual/range {p0 .. p4}, Lcom/jscape/inet/ftp/Ftp;->downloadDir(Ljava/lang/String;IIZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_2
    invoke-static {v1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_2
    const/4 v0, 0x0

    if-ltz v9, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p0 .. p1}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v13

    invoke-static/range {p1 .. p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    sget-object v2, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v3, 0x1d

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v7, v1}, Lcom/jscape/inet/ftp/Ftp;->makeLocalDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_3
    :try_start_4
    invoke-virtual/range {p0 .. p1}, Lcom/jscape/inet/ftp/Ftp;->getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v14

    invoke-direct/range {p0 .. p1}, Lcom/jscape/inet/ftp/Ftp;->b(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    move-object/from16 v3, p1

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v1, v2}, Lcom/jscape/inet/ftp/Ftp;->b(Ljava/util/Vector;Ljava/io/File;)V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v16
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v6, v0

    move/from16 v17, v6

    :goto_0
    if-gt v6, v8, :cond_b

    :try_start_5
    iget-object v0, v7, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v10, :cond_8

    if-nez v0, :cond_b

    move-object/from16 v1, p0

    move-object v2, v15

    move-object/from16 v3, v16

    move/from16 v4, p4

    move-object v5, v14

    move v11, v6

    move/from16 v6, p5

    :try_start_6
    invoke-direct/range {v1 .. v6}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/String;Ljava/io/File;ZLjava/util/Vector;I)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const/4 v0, 0x1

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v1, v0

    :try_start_7
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->abortDownloadThreads()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v10, :cond_5

    if-ge v11, v8, :cond_4

    goto :goto_1

    :cond_4
    :try_start_8
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_5
    :goto_1
    if-eqz v10, :cond_6

    if-ge v11, v8, :cond_7

    const/16 v0, 0x3e8

    move/from16 v6, p3

    goto :goto_2

    :cond_6
    move v0, v8

    move v6, v11

    :goto_2
    mul-int/2addr v6, v0

    int-to-long v0, v6

    :try_start_9
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_2
    :cond_7
    :try_start_a
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->disconnect()V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->connect()Lcom/jscape/inet/ftp/Ftp;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_4

    :catch_3
    move-exception v0

    :try_start_b
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catch_4
    move-exception v0

    :try_start_c
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :cond_8
    move v11, v6

    :goto_3
    move/from16 v17, v0

    :goto_4
    if-eqz v17, :cond_9

    :try_start_d
    invoke-virtual {v7, v12}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {v7, v13}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_5
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    if-nez v10, :cond_b

    goto :goto_5

    :catch_5
    move-exception v0

    :try_start_e
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_6
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :catch_6
    move-exception v0

    :try_start_f
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_9
    :goto_5
    add-int/lit8 v6, v11, 0x1

    if-nez v10, :cond_a

    goto :goto_6

    :cond_a
    const/4 v11, 0x1

    goto :goto_0

    :catch_7
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :cond_b
    :goto_6
    monitor-exit p0

    return-void

    :catch_8
    move-exception v0

    :try_start_10
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :cond_c
    :try_start_11
    new-instance v1, Lcom/jscape/inet/ftp/FtpException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v0, v3, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_9
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :catch_9
    move-exception v0

    :try_start_12
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized downloadDir(Ljava/lang/String;IZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/jscape/inet/ftp/Ftp;->downloadDir(Ljava/lang/String;IIZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public downloadDir(Ljava/lang/String;IZI)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/jscape/inet/ftp/Ftp;->downloadDir(Ljava/lang/String;IIZI)V

    return-void
.end method

.method public declared-synchronized getAccount()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getAccount()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAutoDetectIpv6()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getAutoDetectIpv6()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getBlockTransferSize()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getBlockTransferSize()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCharacterEncoding()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getEncoding()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCompression()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getCompression()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getConnectBeforeCommand()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getConnectBeforeCommand()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getDataPort()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDataPort()I

    move-result v0

    return v0
.end method

.method public declared-synchronized getDataPortEnd()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDataPortEnd()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDataPortStart()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDataPortStart()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDebug()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDebugStream()Ljava/io/PrintStream;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDir()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDir()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDirListing()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftp/Ftp;->getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDirListing(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getDirListingAsString()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftp/Ftp;->getDirListingAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDirListingAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDirListingAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getDirListingRegex(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->d(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    const-string v3, ""

    invoke-virtual {v2, v3}, Lcom/jscape/inet/ftp/FtpImplementation;->getDirListing(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/ftp/FtpFile;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Lcom/jscape/util/m/f;->a(Ljava/lang/String;)Z

    move-result v4
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    if-eqz v4, :cond_1

    :try_start_2
    invoke-virtual {v1, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_1
    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_3
    monitor-exit p0

    return-object v2

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getDirListingRegexAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->getDirListingRegex(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ftp/FtpFile;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v3, 0x13

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_1

    if-nez v1, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getErrorOnSizeCommand()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getErrorOnSizeCommand()Z

    move-result v0

    return v0
.end method

.method public getFeatures()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getFeatures()Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getFileTimeStamp(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getFilesize(Ljava/lang/String;)J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getFileSize(Ljava/lang/String;)J

    move-result-wide v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getFtpFileParser()Lcom/jscape/inet/ftp/FtpFileParser;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->c:Lcom/jscape/inet/ftp/FtpFileParser;

    return-object v0
.end method

.method public declared-synchronized getHostname()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getHost()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getInputStream(Ljava/lang/String;J)Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/inet/ftp/FtpImplementation;->getInputStream(Ljava/lang/String;J)Ljava/io/InputStream;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getKeepAlive()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getKeepAlive()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLinger()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getLinger()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getLocalChecksum(Ljava/io/File;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/nio/file/OpenOption;

    invoke-static {p1, v1}, Ljava/nio/file/Files;->newInputStream(Ljava/nio/file/Path;[Ljava/nio/file/OpenOption;)Ljava/io/InputStream;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz p1, :cond_0

    :try_start_2
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_0
    :goto_0
    return-object v0

    :catchall_1
    move-exception v1

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_2
    move-exception v2

    if-eqz p1, :cond_1

    :try_start_5
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v3

    :try_start_6
    invoke-virtual {v1, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    goto :goto_1

    :catchall_4
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    throw v2
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    :catch_0
    move-exception p1

    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public declared-synchronized getLocalDir()Ljava/io/File;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftp/Ftp;->b:Ljava/io/File;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    :try_start_2
    invoke-direct {p0}, Lcom/jscape/inet/ftp/Ftp;->b()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    iget-object v1, p0, Lcom/jscape/inet/ftp/Ftp;->b:Ljava/io/File;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    monitor-exit p0

    return-object v1

    :catch_0
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLocalDirListing()Ljava/util/Enumeration;
    .locals 7

    monitor-enter p0

    :try_start_0
    new-instance v6, Lcom/jscape/util/J;

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "*"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/jscape/util/J;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v6

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getMachineDirListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getMachineDirListing(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    return-object p1
.end method

.method public getMachineDirListing(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->getMachineDirListing(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    return-object p1
.end method

.method public getMachineFileListing(Ljava/lang/String;)Lcom/jscape/inet/ftp/FtpFile;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getMachineFileListing(Ljava/lang/String;)Lcom/jscape/inet/ftp/FtpFile;

    move-result-object p1

    return-object p1
.end method

.method public declared-synchronized getMode()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getTransferMode()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getNATAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getNATAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getNameListing()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftp/Ftp;->getNameListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNameListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->getNameListing(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getOutputStream(Ljava/lang/String;JZ)Ljava/io/OutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1, p4, p2, p3}, Lcom/jscape/inet/ftp/FtpImplementation;->getOutputStream(Ljava/lang/String;ZJ)Ljava/io/OutputStream;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getPassive()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->isPassive()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPassword()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getPassword()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPort()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getPort()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPortAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getPortAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getPreserveDownloadTimestamp()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getPreserveDownloadTimestamp()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPreserveUploadTimestamp()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getPreserveUploadTimestamp()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getRecursiveDirectoryFileCount(Ljava/lang/String;)I
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    const-string p1, ""

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v0

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    :catch_0
    monitor-exit p0

    return v0
.end method

.method public declared-synchronized getRecursiveDirectorySize(Ljava/lang/String;)J
    .locals 7

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v1, 0x0

    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    const-string p1, ""

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    const/4 v4, 0x0

    :cond_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v4, v5, :cond_1

    invoke-virtual {p1, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/jscape/inet/ftp/Ftp;->getFilesize(Ljava/lang/String;)J

    move-result-wide v5

    add-long/2addr v1, v5

    add-int/lit8 v4, v4, 0x1

    if-eqz v0, :cond_2

    if-nez v0, :cond_0

    :cond_1
    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    monitor-exit p0

    return-wide v1

    :catch_0
    monitor-exit p0

    return-wide v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getRemoteFileChecksum(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftp/Ftp;->isFeatureSupported(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x25

    aget-object v1, v1, v3

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->issueCommandCheck(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v2, " "

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {v4, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    const/4 v4, -0x1

    if-eq v2, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {p1, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {p1, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->issueCommandCheck(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    move-object p1, v4

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    new-instance v0, Ljava/util/StringTokenizer;

    invoke-direct {v0, p1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    :try_start_2
    throw v1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_2
    new-instance p1, Lcom/jscape/inet/ftp/FtpException;

    sget-object v0, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v1, 0xd

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public declared-synchronized getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDirListing()Ljava/util/Enumeration;

    move-result-object p1

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_1

    if-nez v3, :cond_0

    :try_start_2
    const-string v3, "\\"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_1

    if-nez v3, :cond_0

    :try_start_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_0
    move-object v4, p0

    goto :goto_2

    :cond_1
    move-object v4, p0

    :goto_0
    if-eqz v3, :cond_5

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/ftp/FtpFile;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v5
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v1, :cond_3

    if-eqz v5, :cond_2

    :try_start_5
    invoke-static {v3}, Lcom/jscape/inet/ftp/Ftp;->a(Lcom/jscape/inet/ftp/FtpFile;)Z

    move-result v5
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-nez v5, :cond_3

    :try_start_6
    invoke-virtual {v4}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Lcom/jscape/inet/ftp/Ftp;->getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v4, v5}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    if-nez v1, :cond_3

    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    if-nez v1, :cond_4

    goto :goto_3

    :cond_4
    :goto_2
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_0

    :catch_1
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_2
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_5
    :goto_3
    monitor-exit p0

    return-object v0

    :catch_3
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catch_4
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :catch_5
    move-exception p1

    :try_start_d
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getRemoteFileObjectList(Ljava/lang/String;)Ljava/util/Vector;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Vector<",
            "Lcom/jscape/inet/ftp/FtpFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDirListing()Ljava/util/Enumeration;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    if-nez v3, :cond_0

    :try_start_2
    const-string v3, "\\"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_1

    if-nez v3, :cond_0

    :try_start_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_0
    move-object v4, p0

    goto :goto_2

    :cond_1
    move-object v4, p0

    :goto_0
    if-eqz v3, :cond_5

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/ftp/FtpFile;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v5
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_3

    if-eqz v5, :cond_2

    :try_start_5
    invoke-static {v3}, Lcom/jscape/inet/ftp/Ftp;->a(Lcom/jscape/inet/ftp/FtpFile;)Z

    move-result v5
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-nez v5, :cond_3

    :try_start_6
    invoke-virtual {v4}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Lcom/jscape/inet/ftp/Ftp;->getRemoteFileObjectList(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v4, v5}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {v1, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    if-nez v0, :cond_4

    goto :goto_3

    :cond_4
    :goto_2
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_0

    :catch_1
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_2
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_5
    :goto_3
    monitor-exit p0

    return-object v1

    :catch_3
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catch_4
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :catch_5
    move-exception p1

    :try_start_d
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getResponseCode()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getResponseCode()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSystemType()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getSystemType()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getTcpNoDelay()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getTcpNoDelay()Z

    move-result v0

    return v0
.end method

.method public declared-synchronized getTimeZone()Ljava/util/TimeZone;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTimeout()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getTimeout()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getUseEPRT()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getUseEPRT()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getUseEPSV()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getUseEPSV()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getUsername()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getUser()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getWireEncoding()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getWireEncoding()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public interrupt()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupt()V

    return-void
.end method

.method public interrupted()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v0

    return v0
.end method

.method public declared-synchronized isConnected()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->isConnected()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isFeatureSupported(Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->isFeatureSupported(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public declared-synchronized issueCommand(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->issueCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized issueCommandCheck(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->issueCommandCheck(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized login()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->login()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized makeDir(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->makeDir(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized makeDirRecursive(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    :cond_1
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v3, "/"

    invoke-direct {v2, p1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_4

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_2
    :try_start_5
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->makeDir(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :goto_1
    if-nez v0, :cond_2

    goto :goto_2

    :catch_3
    move-exception p1

    :try_start_6
    throw p1

    :cond_3
    :goto_2
    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_4
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized makeLocalDir(Ljava/lang/String;)Ljava/io/File;
    .locals 2

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0}, Lcom/jscape/inet/ftp/Ftp;->d(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result p1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez p1, :cond_0

    :try_start_2
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized mdelete(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->d(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDirListing()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_5

    :try_start_1
    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v2
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_5

    :try_start_2
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :try_start_3
    check-cast v2, Lcom/jscape/inet/ftp/FtpFile;

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-interface {p1, v3}, Lcom/jscape/util/m/f;->a(Ljava/lang/String;)Z

    move-result v4
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_2

    if-eqz v4, :cond_4

    :try_start_5
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v4
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    if-eqz v4, :cond_3

    :try_start_6
    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftp/Ftp;->deleteDir(Ljava/lang/String;)V
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-nez v0, :cond_4

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_3
    :goto_0
    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftp/Ftp;->deleteFile(Ljava/lang/String;)V
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_4
    if-nez v0, :cond_0

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catch_2
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_3
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_4
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catch_5
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :cond_5
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized mdownload(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->d(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDirListing()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_4

    :try_start_1
    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v2
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_4

    :try_start_2
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :try_start_3
    check-cast v2, Lcom/jscape/inet/ftp/FtpFile;

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v2
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_2

    if-nez v2, :cond_3

    :try_start_5
    invoke-interface {p1, v3}, Lcom/jscape/util/m/f;->a(Ljava/lang/String;)Z

    move-result v2
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    if-eqz v2, :cond_3

    :try_start_6
    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftp/Ftp;->download(Ljava/lang/String;)Ljava/io/File;
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_3
    :goto_0
    if-nez v0, :cond_0

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catch_2
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_3
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_4
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_4
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized mdownload(Ljava/util/Enumeration;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_3

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_3

    :try_start_2
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftp/Ftp;->download(Ljava/lang/String;)Ljava/io/File;
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    :goto_0
    if-nez v0, :cond_0

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_2
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_3
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized mupload(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->d(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    const/4 v2, 0x0

    :cond_0
    array-length v3, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v2, v3, :cond_3

    :try_start_1
    iget-object v3, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v3
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    if-nez v3, :cond_3

    :try_start_2
    aget-object v3, v1, v2

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/jscape/util/m/f;->a(Ljava/lang/String;)Z

    move-result v3
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    if-eqz v3, :cond_2

    :try_start_3
    aget-object v3, v1, v2

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/io/File;)V
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_2
    :goto_0
    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_0

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_2
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_3
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized mupload(Ljava/util/Enumeration;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_3

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_3

    :try_start_2
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :try_start_3
    check-cast v1, Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/lang/String;)V
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    :goto_0
    if-nez v0, :cond_0

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_2
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_3
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized noop()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->noop()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized readResponse()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->readResponse()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeFtpListener(Lcom/jscape/inet/ftp/FtpListener;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->removeListener(Lcom/jscape/inet/ftp/FtpListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized renameFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->renameFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public reset()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->reset()V

    return-void
.end method

.method public declared-synchronized resumeDownload(Ljava/lang/String;J)Ljava/io/File;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p1, p2, p3}, Lcom/jscape/inet/ftp/Ftp;->resumeDownload(Ljava/lang/String;Ljava/lang/String;J)Ljava/io/File;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized resumeDownload(Ljava/lang/String;Ljava/lang/String;J)Ljava/io/File;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_5

    const-wide/16 v1, 0x0

    cmp-long v1, p3, v1

    if-nez v1, :cond_0

    :try_start_1
    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object p1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    monitor-exit p0

    return-object p1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance p1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v4, 0x1c

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    const/4 v2, 0x0

    :try_start_3
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :try_start_4
    iget-object v4, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetPath(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v4, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :try_start_5
    iget-object v4, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v4, v3, p2, p3, p4}, Lcom/jscape/inet/ftp/FtpImplementation;->download(Ljava/io/OutputStream;Ljava/lang/String;J)V
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catch_1
    :try_start_6
    new-instance p2, Ljava/io/RandomAccessFile;

    sget-object v4, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/4 v5, 0x6

    aget-object v4, v4, v5

    invoke-direct {p2, v1, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    invoke-virtual {p2, p3, p4}, Ljava/io/RandomAccessFile;->seek(J)V

    new-instance p3, Ljava/io/BufferedInputStream;

    new-instance p4, Ljava/io/FileInputStream;

    invoke-direct {p4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {p3, p4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    const/16 p4, 0x7800

    :try_start_8
    new-array p4, p4, [B

    :cond_1
    invoke-virtual {p3, p4}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v4
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    const/4 v5, 0x0

    :try_start_9
    invoke-virtual {p2, p4, v5, v4}, Ljava/io/RandomAccessFile;->write([BII)V
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-eqz v0, :cond_3

    if-nez v0, :cond_1

    goto :goto_0

    :catch_2
    move-exception p4

    :try_start_a
    invoke-static {p4}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p4

    throw p4
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_2
    :goto_0
    :try_start_b
    iget-object p4, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p4, v2}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetPath(Ljava/lang/String;)V

    iget-object p4, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p4, v2}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    invoke-static {p3}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    invoke-static {p2}, Lcom/jscape/util/X;->a(Ljava/io/RandomAccessFile;)V

    invoke-virtual {p1}, Ljava/io/File;->delete()Z
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    :cond_3
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p4

    move-object v6, p3

    move-object p3, p2

    move-object p2, p4

    move-object p4, v6

    goto :goto_2

    :catch_3
    move-exception p4

    move-object v6, p3

    move-object p3, p2

    move-object p2, p4

    move-object p4, v6

    goto :goto_1

    :catchall_1
    move-exception p3

    move-object p4, v2

    move-object v6, p3

    move-object p3, p2

    move-object p2, v6

    goto :goto_2

    :catch_4
    move-exception p3

    move-object p4, v2

    move-object v6, p3

    move-object p3, p2

    move-object p2, v6

    goto :goto_1

    :catchall_2
    move-exception p2

    move-object p3, v2

    move-object p4, p3

    goto :goto_2

    :catch_5
    move-exception p2

    move-object p3, v2

    move-object p4, p3

    goto :goto_1

    :catchall_3
    move-exception p2

    move-object p3, v2

    move-object p4, p3

    move-object v3, p4

    goto :goto_2

    :catch_6
    move-exception p2

    move-object p3, v2

    move-object p4, p3

    move-object v3, p4

    :goto_1
    :try_start_c
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    :catchall_4
    move-exception p2

    :goto_2
    :try_start_d
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, v2}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetPath(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, v2}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    invoke-static {p4}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    invoke-static {p3}, Lcom/jscape/util/X;->a(Ljava/io/RandomAccessFile;)V

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    throw p2
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    :catchall_5
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized resumeUpload(Ljava/io/File;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/jscape/inet/ftp/Ftp;->resumeUpload(Ljava/io/File;Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized resumeUpload(Ljava/io/File;Ljava/lang/String;J)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v11, p2

    monitor-enter p0

    :try_start_0
    invoke-static/range {p1 .. p1}, Lcom/jscape/inet/ftp/Ftp;->b(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v12

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v13

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetPath(Ljava/lang/String;)V

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, v11}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    const/16 v15, 0x26

    const/16 v16, 0x19

    const/4 v10, 0x0

    :try_start_1
    iget-object v2, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    const/4 v5, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v8
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v0, 0x0

    move-object v3, v12

    move-object/from16 v4, p2

    move-wide/from16 v6, p3

    move-object v14, v10

    move v10, v0

    :try_start_2
    invoke-virtual/range {v2 .. v10}, Lcom/jscape/inet/ftp/FtpImplementation;->upload(Ljava/io/InputStream;Ljava/lang/String;ZJJZ)Ljava/lang/String;
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_9
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, v14}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetPath(Ljava/lang/String;)V

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, v14}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V

    invoke-static {v12}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    if-eqz v13, :cond_0

    :try_start_4
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->getPreserveUploadTimestamp()Z

    move-result v14
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v2, v0

    :try_start_5
    invoke-static {v2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    :cond_0
    const/4 v14, 0x0

    :goto_0
    if-eqz v13, :cond_1

    if-eqz v14, :cond_7

    :try_start_6
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v14
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    :cond_1
    :goto_1
    if-eqz v13, :cond_2

    if-nez v14, :cond_7

    :try_start_8
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    if-eqz v13, :cond_3

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->isConnected()Z

    move-result v14
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    goto :goto_2

    :catch_2
    move-exception v0

    :try_start_9
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    :cond_2
    :goto_2
    if-nez v14, :cond_4

    :try_start_a
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->connect()V

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    :cond_3
    :try_start_b
    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->reset()V

    goto :goto_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_4
    :goto_3
    new-instance v0, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    :try_start_c
    invoke-virtual {v1, v11, v0}, Lcom/jscape/inet/ftp/Ftp;->setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)V

    iget-object v2, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    if-eqz v13, :cond_5

    :try_start_d
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_5
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :cond_5
    :try_start_e
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v5, 0x9

    aget-object v5, v4, v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v4, v4, v15

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_e .. :try_end_e} :catch_6
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    goto :goto_4

    :catch_4
    move-exception v0

    :try_start_f
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_f .. :try_end_f} :catch_5
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    :catch_5
    move-exception v0

    :try_start_10
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    :catch_6
    :try_start_11
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_11
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_11 .. :try_end_11} :catch_7
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    if-eqz v13, :cond_6

    :try_start_12
    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_12
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_12 .. :try_end_12} :catch_8
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    :cond_6
    :try_start_13
    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v4, v3, v16

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x1f

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    :cond_7
    :goto_4
    monitor-exit p0

    return-void

    :catch_7
    move-exception v0

    move-object v2, v0

    :try_start_14
    invoke-static {v2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_14
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_14 .. :try_end_14} :catch_8
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    :catch_8
    move-exception v0

    :try_start_15
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_3

    :catchall_0
    move-exception v0

    goto :goto_5

    :catch_9
    move-exception v0

    goto :goto_6

    :catchall_1
    move-exception v0

    move-object v14, v10

    :goto_5
    move-object v2, v0

    const/16 v17, 0x0

    goto :goto_7

    :catch_a
    move-exception v0

    move-object v14, v10

    :goto_6
    const/4 v2, 0x1

    :try_start_16
    throw v0
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_2

    :catchall_2
    move-exception v0

    move/from16 v17, v2

    move-object v2, v0

    :goto_7
    :try_start_17
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, v14}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetPath(Ljava/lang/String;)V

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, v14}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V

    invoke-static {v12}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_3

    if-eqz v13, :cond_8

    if-nez v17, :cond_f

    :try_start_18
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->getPreserveUploadTimestamp()Z

    move-result v17
    :try_end_18
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_18 .. :try_end_18} :catch_b
    .catchall {:try_start_18 .. :try_end_18} :catchall_3

    goto :goto_8

    :catch_b
    move-exception v0

    move-object v3, v0

    :try_start_19
    invoke-static {v3}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_19
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_19 .. :try_end_19} :catch_11
    .catchall {:try_start_19 .. :try_end_19} :catchall_3

    :cond_8
    :goto_8
    if-eqz v13, :cond_9

    if-eqz v17, :cond_f

    :try_start_1a
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v17
    :try_end_1a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1a .. :try_end_1a} :catch_c
    .catchall {:try_start_1a .. :try_end_1a} :catchall_3

    goto :goto_9

    :catch_c
    move-exception v0

    :try_start_1b
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_1b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1b .. :try_end_1b} :catch_11
    .catchall {:try_start_1b .. :try_end_1b} :catchall_3

    :cond_9
    :goto_9
    if-eqz v13, :cond_a

    if-nez v17, :cond_f

    :try_start_1c
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    if-eqz v13, :cond_b

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->isConnected()Z

    move-result v17
    :try_end_1c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1c .. :try_end_1c} :catch_d
    .catchall {:try_start_1c .. :try_end_1c} :catchall_3

    goto :goto_a

    :catch_d
    move-exception v0

    :try_start_1d
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_1d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1d .. :try_end_1d} :catch_11
    .catchall {:try_start_1d .. :try_end_1d} :catchall_3

    :cond_a
    :goto_a
    if-nez v17, :cond_c

    :try_start_1e
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->connect()V

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_1e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1e .. :try_end_1e} :catch_e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_3

    :cond_b
    :try_start_1f
    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->reset()V

    goto :goto_b

    :catch_e
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_c
    :goto_b
    new-instance v0, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    invoke-direct {v0, v3, v4}, Ljava/util/Date;-><init>(J)V
    :try_end_1f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1f .. :try_end_1f} :catch_11
    .catchall {:try_start_1f .. :try_end_1f} :catchall_3

    :try_start_20
    invoke-virtual {v1, v11, v0}, Lcom/jscape/inet/ftp/Ftp;->setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)V

    iget-object v3, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_20
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_20 .. :try_end_20} :catch_f
    .catchall {:try_start_20 .. :try_end_20} :catchall_3

    if-eqz v13, :cond_d

    :try_start_21
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v3

    if-eqz v3, :cond_f

    iget-object v3, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_21
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_21 .. :try_end_21} :catch_10
    .catchall {:try_start_21 .. :try_end_21} :catchall_3

    :cond_d
    :try_start_22
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v6, v5, v16

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v5, v5, v15

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_22
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_22 .. :try_end_22} :catch_11
    .catchall {:try_start_22 .. :try_end_22} :catchall_3

    goto :goto_c

    :catch_f
    move-exception v0

    :try_start_23
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_23
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_23 .. :try_end_23} :catch_10
    .catchall {:try_start_23 .. :try_end_23} :catchall_3

    :catch_10
    move-exception v0

    :try_start_24
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_24
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_24 .. :try_end_24} :catch_11
    .catchall {:try_start_24 .. :try_end_24} :catchall_3

    :catch_11
    :try_start_25
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    if-eqz v13, :cond_e

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v0
    :try_end_25
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_25 .. :try_end_25} :catch_12
    .catchall {:try_start_25 .. :try_end_25} :catchall_3

    if-eqz v0, :cond_f

    :try_start_26
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_26
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_26 .. :try_end_26} :catch_13
    .catchall {:try_start_26 .. :try_end_26} :catchall_3

    :cond_e
    :try_start_27
    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v5, v4, v16

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v5, 0xa

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_f
    :goto_c
    throw v2
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_3

    :catch_12
    move-exception v0

    :try_start_28
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_28
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_28 .. :try_end_28} :catch_13
    .catchall {:try_start_28 .. :try_end_28} :catchall_3

    :catch_13
    move-exception v0

    :try_start_29
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_29
    .catchall {:try_start_29 .. :try_end_29} :catchall_3

    :catchall_3
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized resumeUpload(Ljava/lang/String;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p1, p2, p3}, Lcom/jscape/inet/ftp/Ftp;->resumeUpload(Ljava/lang/String;Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized resumeUpload(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/jscape/inet/ftp/Ftp;->resumeUpload(Ljava/io/File;Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setAccount(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setAccount(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setAscii()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ftp/FtpImplementation;->setTransferMode(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setAuto(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setAuto(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setAutoDetectIpv6(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setAutoDetectIpv6(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setBinary()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ftp/FtpImplementation;->setTransferMode(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setBlockTransferSize(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setBlockTransferSize(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setCharacterEncoding(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setEncoding(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setCompression(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setCompression(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setConnectBeforeCommand(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setConnectBeforeCommand(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setDataPort(I)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setDataPortStart(I)V

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setDataPortEnd(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public declared-synchronized setDataPortRange(II)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->setDataPortEnd(I)V

    iget-object p2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p2, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setDataPortStart(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setDebug(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setDebug(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setDebugStream(Ljava/io/PrintStream;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setDebugStream(Ljava/io/PrintStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setDir(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setDir(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setDirUp()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->dirUp()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setErrorOnSizeCommand(Z)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setErrorOnSizeCommand(Z)V

    return-void
.end method

.method public declared-synchronized setFileCreationTime(Ljava/lang/String;Ljava/util/Date;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->setFileCreationTime(Ljava/lang/String;Ljava/util/Date;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setFileModificationTime(Ljava/lang/String;Ljava/util/Date;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->setFileModificationTime(Ljava/lang/String;Ljava/util/Date;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->setFileModificationTime(Ljava/lang/String;Ljava/util/Date;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setFtpFileParser(Lcom/jscape/inet/ftp/FtpFileParser;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->c:Lcom/jscape/inet/ftp/FtpFileParser;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setHostname(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setHost(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setKeepAlive(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setKeepAlive(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setLinger(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setLinger(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setLocalDir(Ljava/io/File;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    if-eqz v1, :cond_1

    :try_start_3
    iput-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->b:Ljava/io/File;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :try_start_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v3, 0xb

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_0
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_2
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setNATAddress(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setNATAddress(Ljava/lang/String;)V

    return-void
.end method

.method public declared-synchronized setPassive(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setPassive(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setPassword(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setPassword(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setPort(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setPort(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setPortAddress(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setPortAddress(Ljava/lang/String;)V

    return-void
.end method

.method public declared-synchronized setPreserveDownloadTimestamp(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setPreserveDownloadTimestamp(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setPreserveUploadTimestamp(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setPreserveUploadTimestamp(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setProxyAuthentication(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->setProxyAuthentication(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setProxyHost(Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->setProxyHost(Ljava/lang/String;I)V

    return-void
.end method

.method public setProxyType(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setProxyType(Ljava/lang/String;)V

    return-void
.end method

.method public declared-synchronized setReceiveBufferSize(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setReceiveBufferSize(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setSendBufferSize(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setSendBufferSize(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setTcpNoDelay(Z)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setTcpNoDelay(Z)V

    return-void
.end method

.method public declared-synchronized setTimeout(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setTimeout(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setTimezone(Ljava/util/TimeZone;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setTimezone(Ljava/util/TimeZone;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setUseEPRT(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setUseEPRT(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setUseEPSV(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setUseEPSV(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setUsername(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setUser(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setWireEncoding(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/FtpImplementation;->setWireEncoding(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/io/File;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/io/File;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/io/File;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/io/File;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/io/File;Ljava/lang/String;Z)V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v11, p2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    if-eqz v12, :cond_0

    :try_start_1
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->isFile()Z

    move-result v0
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    if-nez v0, :cond_0

    :try_start_2
    invoke-virtual/range {p0 .. p1}, Lcom/jscape/inet/ftp/Ftp;->uploadDir(Ljava/io/File;)V

    if-nez v12, :cond_9

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    invoke-static/range {p1 .. p1}, Lcom/jscape/inet/ftp/Ftp;->b(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v14

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetPath(Ljava/lang/String;)V

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, v11}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDir()Ljava/lang/String;

    move-result-object v15
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    const/16 v16, 0xa

    const/16 v17, 0x26

    const/4 v10, 0x0

    const/16 v18, 0x19

    :try_start_4
    iget-object v2, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    const-wide/16 v6, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v8
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_f
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    const/4 v0, 0x0

    move-object v3, v14

    move-object/from16 v4, p2

    move/from16 v5, p3

    move-object v13, v10

    move v10, v0

    :try_start_5
    invoke-virtual/range {v2 .. v10}, Lcom/jscape/inet/ftp/FtpImplementation;->upload(Ljava/io/InputStream;Ljava/lang/String;ZJJZ)Ljava/lang/String;
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_e
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, v13}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetPath(Ljava/lang/String;)V

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, v13}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V

    invoke-static {v14}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    if-eqz v12, :cond_1

    :try_start_7
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getPreserveUploadTimestamp()Z

    move-result v13
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :cond_1
    const/4 v13, 0x0

    :goto_1
    if-eqz v12, :cond_2

    if-eqz v13, :cond_9

    :try_start_9
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v13
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    goto :goto_2

    :catch_3
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_8
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    :cond_2
    :goto_2
    if-eqz v12, :cond_3

    if-nez v13, :cond_9

    :try_start_b
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    if-eqz v12, :cond_4

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->isConnected()Z

    move-result v13
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    goto :goto_3

    :catch_4
    move-exception v0

    :try_start_c
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_8
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :cond_3
    :goto_3
    if-nez v13, :cond_5

    :try_start_d
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->connect()V

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->reset()V

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_5
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :cond_4
    :try_start_e
    invoke-virtual {v0, v15}, Lcom/jscape/inet/ftp/FtpImplementation;->setDir(Ljava/lang/String;)V

    goto :goto_4

    :catch_5
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_5
    :goto_4
    new-instance v0, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V
    :try_end_e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_e .. :try_end_e} :catch_8
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    :try_start_f
    iget-object v2, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v2, v11, v0}, Lcom/jscape/inet/ftp/FtpImplementation;->setFileModificationTime(Ljava/lang/String;Ljava/util/Date;)V

    iget-object v2, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_f .. :try_end_f} :catch_6
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    if-eqz v12, :cond_6

    :try_start_10
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_7
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    :cond_6
    :try_start_11
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v5, v4, v18

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v4, v4, v17

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_11
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_11 .. :try_end_11} :catch_8
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    goto/16 :goto_5

    :catch_6
    move-exception v0

    :try_start_12
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_12
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_12 .. :try_end_12} :catch_7
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    :catch_7
    move-exception v0

    :try_start_13
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_13
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_13 .. :try_end_13} :catch_8
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    :catch_8
    :try_start_14
    new-instance v0, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V
    :try_end_14
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_14 .. :try_end_14} :catch_b
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    :try_start_15
    iget-object v2, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v2, v11, v0}, Lcom/jscape/inet/ftp/FtpImplementation;->setFileModificationTime(Ljava/lang/String;Ljava/util/Date;)V

    iget-object v2, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_15
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_15 .. :try_end_15} :catch_9
    .catchall {:try_start_15 .. :try_end_15} :catchall_3

    if-eqz v12, :cond_7

    :try_start_16
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_16
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_16 .. :try_end_16} :catch_a
    .catchall {:try_start_16 .. :try_end_16} :catchall_3

    :cond_7
    :try_start_17
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v5, v4, v18

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v4, v4, v17

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_17
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_17 .. :try_end_17} :catch_b
    .catchall {:try_start_17 .. :try_end_17} :catchall_3

    goto :goto_5

    :catch_9
    move-exception v0

    :try_start_18
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_18
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_18 .. :try_end_18} :catch_a
    .catchall {:try_start_18 .. :try_end_18} :catchall_3

    :catch_a
    move-exception v0

    :try_start_19
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_19
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_19 .. :try_end_19} :catch_b
    .catchall {:try_start_19 .. :try_end_19} :catchall_3

    :catch_b
    :try_start_1a
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_1a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1a .. :try_end_1a} :catch_c
    .catchall {:try_start_1a .. :try_end_1a} :catchall_3

    if-eqz v12, :cond_8

    :try_start_1b
    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_1b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1b .. :try_end_1b} :catch_d
    .catchall {:try_start_1b .. :try_end_1b} :catchall_3

    :cond_8
    :try_start_1c
    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v4, v3, v18

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, v3, v16

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_3

    :cond_9
    :goto_5
    monitor-exit p0

    return-void

    :catch_c
    move-exception v0

    move-object v2, v0

    :try_start_1d
    invoke-static {v2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_1d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1d .. :try_end_1d} :catch_d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_3

    :catch_d
    move-exception v0

    :try_start_1e
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_3

    :catchall_0
    move-exception v0

    goto :goto_6

    :catch_e
    move-exception v0

    goto :goto_7

    :catchall_1
    move-exception v0

    move-object v13, v10

    :goto_6
    move-object v2, v0

    const/16 v19, 0x0

    goto :goto_8

    :catch_f
    move-exception v0

    move-object v13, v10

    :goto_7
    const/4 v2, 0x1

    :try_start_1f
    throw v0
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_2

    :catchall_2
    move-exception v0

    move/from16 v19, v2

    move-object v2, v0

    :goto_8
    :try_start_20
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, v13}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetPath(Ljava/lang/String;)V

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, v13}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V

    invoke-static {v14}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_3

    if-eqz v12, :cond_a

    if-nez v19, :cond_12

    :try_start_21
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getPreserveUploadTimestamp()Z

    move-result v19
    :try_end_21
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_21 .. :try_end_21} :catch_10
    .catchall {:try_start_21 .. :try_end_21} :catchall_3

    goto :goto_9

    :catch_10
    move-exception v0

    :try_start_22
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_22
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_22 .. :try_end_22} :catch_16
    .catchall {:try_start_22 .. :try_end_22} :catchall_3

    :cond_a
    :goto_9
    if-eqz v12, :cond_b

    if-eqz v19, :cond_12

    :try_start_23
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v19
    :try_end_23
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_23 .. :try_end_23} :catch_11
    .catchall {:try_start_23 .. :try_end_23} :catchall_3

    goto :goto_a

    :catch_11
    move-exception v0

    :try_start_24
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_24
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_24 .. :try_end_24} :catch_16
    .catchall {:try_start_24 .. :try_end_24} :catchall_3

    :cond_b
    :goto_a
    if-eqz v12, :cond_c

    if-nez v19, :cond_12

    :try_start_25
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    if-eqz v12, :cond_d

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->isConnected()Z

    move-result v19
    :try_end_25
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_25 .. :try_end_25} :catch_12
    .catchall {:try_start_25 .. :try_end_25} :catchall_3

    goto :goto_b

    :catch_12
    move-exception v0

    :try_start_26
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_26
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_26 .. :try_end_26} :catch_16
    .catchall {:try_start_26 .. :try_end_26} :catchall_3

    :cond_c
    :goto_b
    if-nez v19, :cond_e

    :try_start_27
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->connect()V

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->reset()V

    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_27
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_27 .. :try_end_27} :catch_13
    .catchall {:try_start_27 .. :try_end_27} :catchall_3

    :cond_d
    :try_start_28
    invoke-virtual {v0, v15}, Lcom/jscape/inet/ftp/FtpImplementation;->setDir(Ljava/lang/String;)V

    goto :goto_c

    :catch_13
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_e
    :goto_c
    new-instance v0, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    invoke-direct {v0, v3, v4}, Ljava/util/Date;-><init>(J)V
    :try_end_28
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_28 .. :try_end_28} :catch_16
    .catchall {:try_start_28 .. :try_end_28} :catchall_3

    :try_start_29
    iget-object v3, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v3, v11, v0}, Lcom/jscape/inet/ftp/FtpImplementation;->setFileModificationTime(Ljava/lang/String;Ljava/util/Date;)V

    iget-object v3, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_29
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_29 .. :try_end_29} :catch_14
    .catchall {:try_start_29 .. :try_end_29} :catchall_3

    if-eqz v12, :cond_f

    :try_start_2a
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v3

    if-eqz v3, :cond_12

    iget-object v3, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_2a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2a .. :try_end_2a} :catch_15
    .catchall {:try_start_2a .. :try_end_2a} :catchall_3

    :cond_f
    :try_start_2b
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v6, v5, v18

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v5, v5, v17

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2b .. :try_end_2b} :catch_16
    .catchall {:try_start_2b .. :try_end_2b} :catchall_3

    goto/16 :goto_d

    :catch_14
    move-exception v0

    :try_start_2c
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_2c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2c .. :try_end_2c} :catch_15
    .catchall {:try_start_2c .. :try_end_2c} :catchall_3

    :catch_15
    move-exception v0

    :try_start_2d
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_2d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2d .. :try_end_2d} :catch_16
    .catchall {:try_start_2d .. :try_end_2d} :catchall_3

    :catch_16
    :try_start_2e
    new-instance v0, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    invoke-direct {v0, v3, v4}, Ljava/util/Date;-><init>(J)V
    :try_end_2e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2e .. :try_end_2e} :catch_19
    .catchall {:try_start_2e .. :try_end_2e} :catchall_3

    :try_start_2f
    iget-object v3, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v3, v11, v0}, Lcom/jscape/inet/ftp/FtpImplementation;->setFileModificationTime(Ljava/lang/String;Ljava/util/Date;)V

    iget-object v3, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_2f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2f .. :try_end_2f} :catch_17
    .catchall {:try_start_2f .. :try_end_2f} :catchall_3

    if-eqz v12, :cond_10

    :try_start_30
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v3

    if-eqz v3, :cond_12

    iget-object v3, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_30
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_30 .. :try_end_30} :catch_18
    .catchall {:try_start_30 .. :try_end_30} :catchall_3

    :cond_10
    :try_start_31
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v6, v5, v18

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v5, v5, v17

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_31
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_31 .. :try_end_31} :catch_19
    .catchall {:try_start_31 .. :try_end_31} :catchall_3

    goto :goto_d

    :catch_17
    move-exception v0

    :try_start_32
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_32
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_32 .. :try_end_32} :catch_18
    .catchall {:try_start_32 .. :try_end_32} :catchall_3

    :catch_18
    move-exception v0

    :try_start_33
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_33
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_33 .. :try_end_33} :catch_19
    .catchall {:try_start_33 .. :try_end_33} :catchall_3

    :catch_19
    :try_start_34
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    if-eqz v12, :cond_11

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v0
    :try_end_34
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_34 .. :try_end_34} :catch_1a
    .catchall {:try_start_34 .. :try_end_34} :catchall_3

    if-eqz v0, :cond_12

    :try_start_35
    iget-object v0, v1, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_35
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_35 .. :try_end_35} :catch_1b
    .catchall {:try_start_35 .. :try_end_35} :catchall_3

    :cond_11
    :try_start_36
    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v5, v4, v18

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v4, v4, v16

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_12
    :goto_d
    throw v2
    :try_end_36
    .catchall {:try_start_36 .. :try_end_36} :catchall_3

    :catch_1a
    move-exception v0

    :try_start_37
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_37
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_37 .. :try_end_37} :catch_1b
    .catchall {:try_start_37 .. :try_end_37} :catchall_3

    :catch_1b
    move-exception v0

    :try_start_38
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_38
    .catchall {:try_start_38 .. :try_end_38} :catchall_3

    :catchall_3
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized upload(Ljava/io/File;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/io/File;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/io/InputStream;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/io/InputStream;Ljava/lang/String;Z)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetPath(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    const/4 v0, -0x1

    :goto_0
    :try_start_2
    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    const-wide/16 v6, 0x0

    int-to-long v8, v0

    const/4 v10, 0x0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    invoke-virtual/range {v2 .. v10}, Lcom/jscape/inet/ftp/FtpImplementation;->upload(Ljava/io/InputStream;Ljava/lang/String;ZJJZ)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetPath(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    :try_start_4
    iget-object p2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p2, v1}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetPath(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p2, v1}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/lang/String;Ljava/io/File;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {p1}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_7

    if-eqz v0, :cond_2

    :try_start_2
    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    if-eqz v1, :cond_1

    :try_start_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v0, :cond_3

    :cond_1
    :try_start_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_2
    move-object v1, p1

    :cond_3
    :try_start_5
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {p0, p2, v1}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->isConnected()Z

    move-result v2
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v0, :cond_5

    if-nez v2, :cond_4

    :try_start_7
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->connect()Lcom/jscape/inet/ftp/Ftp;

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->reset()V

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_4
    :try_start_8
    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v2

    :cond_5
    if-nez v2, :cond_6

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->deleteFile(Ljava/lang/String;)V
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_0
    :try_start_a
    invoke-virtual {p0, v1, p1}, Lcom/jscape/inet/ftp/Ftp;->renameFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_1
    :cond_6
    if-nez v0, :cond_8

    :cond_7
    const/4 p1, 0x0

    :try_start_b
    invoke-virtual {p0, p2, p1}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/io/File;Z)V
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_0

    :catch_2
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :cond_8
    :goto_0
    monitor-exit p0

    return-void

    :catch_3
    move-exception p1

    :try_start_d
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catch_4
    move-exception p1

    :try_start_e
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :catch_6
    move-exception p1

    :try_start_f
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_f .. :try_end_f} :catch_7
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :catch_7
    move-exception p1

    :try_start_10
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_8
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :catch_8
    move-exception p1

    :try_start_11
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2, p3}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/io/File;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p1, p2}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload([BLjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/ftp/Ftp;->upload([BLjava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload([BLjava/lang/String;Z)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v9, Ljava/io/ByteArrayInputStream;

    invoke-direct {v9, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v10, 0x0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    const-wide/16 v4, 0x0

    array-length p1, p1

    int-to-long v6, p1

    const/4 v8, 0x0

    move-object v1, v9

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v8}, Lcom/jscape/inet/ftp/FtpImplementation;->upload(Ljava/io/InputStream;Ljava/lang/String;ZJJZ)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p1, v10}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    :try_start_3
    iget-object p2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p2, v10}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ftp/Ftp;->uploadDir(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;IIZLjava/lang/String;)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v7, p0

    move/from16 v8, p2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v0
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v1, 0x0

    if-eqz v9, :cond_1

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    :try_start_2
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v3, 0xc

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v3, 0x1e

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_0
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Lcom/jscape/inet/ftp/Ftp;->makeDir(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_0
    :try_start_4
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v11

    invoke-virtual/range {p0 .. p1}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V

    invoke-static/range {p1 .. p1}, Lcom/jscape/util/Q;->g(Ljava/io/File;)Ljava/util/Vector;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v13

    invoke-static/range {p1 .. p1}, Lcom/jscape/util/Q;->f(Ljava/io/File;)Ljava/util/Vector;

    move-result-object v2

    move-object/from16 v14, p1

    invoke-direct {v7, v2, v14}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/util/Vector;Ljava/io/File;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v15, v0

    move v6, v1

    :goto_1
    if-gt v6, v8, :cond_7

    :try_start_5
    iget-object v0, v7, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v0
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v9, :cond_4

    if-nez v0, :cond_7

    move-object/from16 v1, p0

    move-object v2, v13

    move/from16 v3, p4

    move-object/from16 v4, p5

    move-object v5, v12

    move-object/from16 v16, v12

    move v12, v6

    move-object/from16 v6, p1

    :try_start_6
    invoke-direct/range {v1 .. v6}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/util/Vector;Ljava/io/File;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const/4 v0, 0x1

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v1, v0

    if-eqz v9, :cond_3

    if-ge v12, v8, :cond_2

    const/16 v0, 0x3e8

    move/from16 v6, p3

    goto :goto_2

    :cond_2
    :try_start_7
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_3
    move v0, v8

    move v6, v12

    :goto_2
    mul-int/2addr v6, v0

    int-to-long v0, v6

    :try_start_9
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_3
    :try_start_a
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->disconnect()V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->connect()Lcom/jscape/inet/ftp/Ftp;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_4

    :cond_4
    move-object/from16 v16, v12

    move v12, v6

    :goto_3
    move v15, v0

    :goto_4
    if-eqz v15, :cond_5

    :try_start_b
    invoke-virtual {v7, v10}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {v7, v11}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    if-nez v9, :cond_7

    goto :goto_5

    :catch_4
    move-exception v0

    :try_start_c
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :catch_5
    move-exception v0

    :try_start_d
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_5
    :goto_5
    add-int/lit8 v6, v12, 0x1

    if-nez v9, :cond_6

    goto :goto_6

    :cond_6
    move-object/from16 v12, v16

    goto :goto_1

    :catch_6
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :cond_7
    :goto_6
    monitor-exit p0

    return-void

    :catch_7
    move-exception v0

    move-object v1, v0

    :try_start_e
    invoke-static {v1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_e .. :try_end_e} :catch_8
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :catch_8
    move-exception v0

    :try_start_f
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;IIZLjava/lang/String;I)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v8, p0

    move/from16 v9, p2

    move/from16 v10, p6

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v11, :cond_0

    if-eqz v10, :cond_1

    :cond_0
    const/4 v12, 0x1

    if-eqz v11, :cond_2

    if-ne v10, v12, :cond_2

    :cond_1
    :try_start_1
    invoke-virtual/range {p0 .. p5}, Lcom/jscape/inet/ftp/Ftp;->uploadDir(Ljava/io/File;IIZLjava/lang/String;)V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_2
    invoke-static {v1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_2
    if-ltz v10, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v13
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v0
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v1, 0x0

    if-eqz v11, :cond_4

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    :try_start_4
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v3, 0xc

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v3, 0x1e

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_a
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_4
    :goto_0
    :try_start_5
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/jscape/inet/ftp/Ftp;->makeDir(Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_1
    :try_start_6
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v14

    invoke-virtual/range {p0 .. p1}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V

    invoke-static/range {p1 .. p1}, Lcom/jscape/util/Q;->g(Ljava/io/File;)Ljava/util/Vector;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {p1 .. p1}, Lcom/jscape/util/Q;->f(Ljava/io/File;)Ljava/util/Vector;

    move-result-object v2

    move-object/from16 v7, p1

    invoke-direct {v8, v2, v7}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/util/Vector;Ljava/io/File;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move/from16 v17, v0

    move v6, v1

    :goto_1
    if-gt v6, v9, :cond_a

    :try_start_7
    iget-object v0, v8, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v0
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_8
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v11, :cond_7

    if-nez v0, :cond_a

    move-object/from16 v1, p0

    move-object/from16 v2, v16

    move/from16 v3, p4

    move-object/from16 v4, p5

    move-object v5, v15

    move v12, v6

    move-object/from16 v6, p1

    move/from16 v7, p6

    :try_start_8
    invoke-direct/range {v1 .. v7}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/util/Vector;Ljava/io/File;I)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    const/4 v0, 0x1

    goto :goto_3

    :catch_2
    move-exception v0

    move-object v1, v0

    :try_start_9
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->abortUploadThreads()V
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-eqz v11, :cond_6

    if-ge v12, v9, :cond_5

    const/16 v0, 0x3e8

    move/from16 v6, p3

    goto :goto_2

    :cond_5
    :try_start_a
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_6
    move v0, v9

    move v6, v12

    :goto_2
    mul-int/2addr v6, v0

    int-to-long v0, v6

    :try_start_b
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catch_3
    :try_start_c
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->disconnect()V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftp/Ftp;->connect()Lcom/jscape/inet/ftp/Ftp;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_4

    :catch_4
    move-exception v0

    :try_start_d
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_5
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catch_5
    move-exception v0

    :try_start_e
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :cond_7
    move v12, v6

    :goto_3
    move/from16 v17, v0

    :goto_4
    if-eqz v17, :cond_8

    :try_start_f
    invoke-virtual {v8, v13}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {v8, v14}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V
    :try_end_f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_f .. :try_end_f} :catch_6
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    if-nez v11, :cond_a

    goto :goto_5

    :catch_6
    move-exception v0

    :try_start_10
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_7
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :catch_7
    move-exception v0

    :try_start_11
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_8
    :goto_5
    add-int/lit8 v6, v12, 0x1

    if-nez v11, :cond_9

    goto :goto_6

    :cond_9
    move-object/from16 v7, p1

    const/4 v12, 0x1

    goto :goto_1

    :catch_8
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :cond_a
    :goto_6
    monitor-exit p0

    return-void

    :catch_9
    move-exception v0

    move-object v1, v0

    :try_start_12
    invoke-static {v1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_12
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_12 .. :try_end_12} :catch_a
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :catch_a
    move-exception v0

    :try_start_13
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :cond_b
    :try_start_14
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v3, 0x20

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v3, 0x18

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_14
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_14 .. :try_end_14} :catch_b
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    :catch_b
    move-exception v0

    :try_start_15
    invoke-static {v0}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;IZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/jscape/inet/ftp/Ftp;->uploadDir(Ljava/io/File;IIZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;IZLjava/lang/String;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    :try_start_0
    invoke-virtual/range {v0 .. v6}, Lcom/jscape/inet/ftp/Ftp;->uploadDir(Ljava/io/File;IIZLjava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;Ljava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getDir()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v3
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v3, :cond_0

    :try_start_2
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftp/Ftp;->makeDir(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    :cond_0
    :try_start_3
    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v2, 0x21

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p1, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v1, 0xf

    aget-object p1, p1, v1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catch_1
    :cond_1
    :goto_0
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    const/16 v4, 0x26

    const/4 v5, 0x1

    const/16 v6, 0x1b

    :try_start_5
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getLocalDirListing()Ljava/util/Enumeration;

    move-result-object v7

    :cond_2
    invoke-interface {v7}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_18
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v8, :cond_6

    :try_start_6
    iget-object v8, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v8}, Lcom/jscape/inet/ftp/FtpImplementation;->interrupted()Z

    move-result v8
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v0, :cond_7

    if-eqz v0, :cond_7

    if-nez v8, :cond_6

    :try_start_7
    invoke-interface {v7}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/io/File;
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_18
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    invoke-virtual {v8}, Ljava/io/File;->isFile()Z

    move-result v9
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v0, :cond_4

    if-eqz v9, :cond_3

    :try_start_9
    invoke-virtual {p0, p2, v8}, Lcom/jscape/inet/ftp/Ftp;->upload(Ljava/lang/String;Ljava/io/File;)V

    if-nez v0, :cond_5

    :cond_3
    invoke-static {v8}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/io/File;)Z

    move-result v9
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_1

    :catch_2
    move-exception p2

    goto :goto_3

    :cond_4
    :goto_1
    if-nez v9, :cond_5

    :try_start_a
    invoke-virtual {p0, v8, p2}, Lcom/jscape/inet/ftp/Ftp;->uploadDir(Ljava/io/File;Ljava/lang/String;)V
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_2

    :catch_3
    move-exception p2

    :try_start_b
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_18
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_5
    :goto_2
    if-nez v0, :cond_2

    goto :goto_4

    :catch_4
    move-exception p2

    :try_start_c
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :catch_5
    move-exception p2

    :try_start_d
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :goto_3
    :try_start_e
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_e .. :try_end_e} :catch_18
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :catch_6
    move-exception p2

    :try_start_f
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_f .. :try_end_f} :catch_7
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :catch_7
    move-exception p2

    :try_start_10
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_18
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :cond_6
    :goto_4
    :try_start_11
    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    :try_start_12
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getPreserveUploadTimestamp()Z

    move-result v8
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_d
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    :cond_7
    if-eqz v0, :cond_8

    if-eqz v8, :cond_1b

    :try_start_13
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->interrupted()Z

    move-result v8
    :try_end_13
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_13 .. :try_end_13} :catch_8
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_d
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    goto :goto_5

    :catch_8
    move-exception p2

    :try_start_14
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_d
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    :cond_8
    :goto_5
    if-eqz v0, :cond_9

    if-nez v8, :cond_1b

    if-eqz v0, :cond_a

    :try_start_15
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->isConnected()Z

    move-result v8
    :try_end_15
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_15 .. :try_end_15} :catch_9
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_d
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    goto :goto_6

    :catch_9
    move-exception p2

    :try_start_16
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_d
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    :cond_9
    :goto_6
    if-nez v8, :cond_b

    :try_start_17
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->connect()Lcom/jscape/inet/ftp/Ftp;
    :try_end_17
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_17 .. :try_end_17} :catch_a
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_d
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    :cond_a
    :try_start_18
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->reset()V

    goto :goto_7

    :catch_a
    move-exception p2

    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2

    :cond_b
    :goto_7
    new-instance p2, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-direct {p2, v2, v3}, Ljava/util/Date;-><init>(J)V
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_d
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    :try_start_19
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, p2}, Lcom/jscape/inet/ftp/Ftp;->setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)V

    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_19
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_19 .. :try_end_19} :catch_b
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_d
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    if-eqz v0, :cond_c

    :try_start_1a
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v2

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_1a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1a .. :try_end_1a} :catch_c
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_d
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1

    :cond_c
    :try_start_1b
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    const/16 v5, 0x17

    aget-object v5, v4, v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v5, 0x24

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "]"

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_d
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    goto/16 :goto_10

    :catch_b
    move-exception p2

    :try_start_1c
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_1c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1c .. :try_end_1c} :catch_c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_d
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    :catch_c
    move-exception p2

    :try_start_1d
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_1

    :catch_d
    :try_start_1e
    iget-object p2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_1e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1e .. :try_end_1e} :catch_e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_1

    if-eqz v0, :cond_d

    :try_start_1f
    invoke-virtual {p2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result p2

    if-eqz p2, :cond_1b

    iget-object p2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_1f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1f .. :try_end_1f} :catch_f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_1

    :cond_d
    :try_start_20
    invoke-virtual {p2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object p2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v4, v3, v6

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 p1, 0x4

    aget-object p1, v3, p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_8
    invoke-virtual {p2, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_1

    goto/16 :goto_10

    :catch_e
    move-exception p1

    :try_start_21
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_21
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_21 .. :try_end_21} :catch_f
    .catchall {:try_start_21 .. :try_end_21} :catchall_1

    :catch_f
    move-exception p1

    :try_start_22
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catchall_0
    move-exception p2

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_1

    :try_start_23
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getPreserveUploadTimestamp()Z

    move-result v1
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_15
    .catchall {:try_start_23 .. :try_end_23} :catchall_1

    if-eqz v0, :cond_e

    if-eqz v1, :cond_14

    :try_start_24
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->interrupted()Z

    move-result v1
    :try_end_24
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_24 .. :try_end_24} :catch_10
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_24} :catch_15
    .catchall {:try_start_24 .. :try_end_24} :catchall_1

    goto :goto_9

    :catch_10
    move-exception v1

    :try_start_25
    invoke-static {v1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    throw v1
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_25} :catch_15
    .catchall {:try_start_25 .. :try_end_25} :catchall_1

    :cond_e
    :goto_9
    if-eqz v0, :cond_f

    if-nez v1, :cond_14

    if-eqz v0, :cond_10

    :try_start_26
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->isConnected()Z

    move-result v1
    :try_end_26
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_26 .. :try_end_26} :catch_11
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_26} :catch_15
    .catchall {:try_start_26 .. :try_end_26} :catchall_1

    goto :goto_a

    :catch_11
    move-exception v1

    :try_start_27
    invoke-static {v1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    throw v1
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_27} :catch_15
    .catchall {:try_start_27 .. :try_end_27} :catchall_1

    :cond_f
    :goto_a
    if-nez v1, :cond_11

    :try_start_28
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->connect()Lcom/jscape/inet/ftp/Ftp;
    :try_end_28
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_28 .. :try_end_28} :catch_12
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_28} :catch_15
    .catchall {:try_start_28 .. :try_end_28} :catchall_1

    :cond_10
    :try_start_29
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->reset()V

    goto :goto_b

    :catch_12
    move-exception v1

    invoke-static {v1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    throw v1

    :cond_11
    :goto_b
    new-instance v1, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_29} :catch_15
    .catchall {:try_start_29 .. :try_end_29} :catchall_1

    :try_start_2a
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v1}, Lcom/jscape/inet/ftp/Ftp;->setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)V

    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_2a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2a .. :try_end_2a} :catch_13
    .catch Ljava/lang/Exception; {:try_start_2a .. :try_end_2a} :catch_15
    .catchall {:try_start_2a .. :try_end_2a} :catchall_1

    if-eqz v0, :cond_12

    :try_start_2b
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v2

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_2b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2b .. :try_end_2b} :catch_14
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_2b} :catch_15
    .catchall {:try_start_2b .. :try_end_2b} :catchall_1

    :cond_12
    :try_start_2c
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v8, v7, v6

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v4, v7, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_2c} :catch_15
    .catchall {:try_start_2c .. :try_end_2c} :catchall_1

    goto :goto_c

    :catch_13
    move-exception v1

    :try_start_2d
    invoke-static {v1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    throw v1
    :try_end_2d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2d .. :try_end_2d} :catch_14
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_2d} :catch_15
    .catchall {:try_start_2d .. :try_end_2d} :catchall_1

    :catch_14
    move-exception v1

    :try_start_2e
    invoke-static {v1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    throw v1
    :try_end_2e
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_2e} :catch_15
    .catchall {:try_start_2e .. :try_end_2e} :catchall_1

    :catch_15
    :try_start_2f
    iget-object v1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    if-eqz v0, :cond_13

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v0
    :try_end_2f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2f .. :try_end_2f} :catch_16
    .catchall {:try_start_2f .. :try_end_2f} :catchall_1

    if-eqz v0, :cond_14

    :try_start_30
    iget-object v1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_30
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_30 .. :try_end_30} :catch_17
    .catchall {:try_start_30 .. :try_end_30} :catchall_1

    :cond_13
    :try_start_31
    invoke-virtual {v1}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v3, v2, v6

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object p1, v2, v5

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_14
    :goto_c
    throw p2
    :try_end_31
    .catchall {:try_start_31 .. :try_end_31} :catchall_1

    :catch_16
    move-exception p1

    :try_start_32
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_32
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_32 .. :try_end_32} :catch_17
    .catchall {:try_start_32 .. :try_end_32} :catchall_1

    :catch_17
    move-exception p1

    :try_start_33
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catch_18
    move-exception p2

    move-object v1, p2

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftp/Ftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftp/Ftp;->setLocalDir(Ljava/io/File;)V
    :try_end_33
    .catchall {:try_start_33 .. :try_end_33} :catchall_1

    :try_start_34
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->getPreserveUploadTimestamp()Z

    move-result p2
    :try_end_34
    .catch Ljava/lang/Exception; {:try_start_34 .. :try_end_34} :catch_1e
    .catchall {:try_start_34 .. :try_end_34} :catchall_1

    if-eqz v0, :cond_15

    if-eqz p2, :cond_1b

    :try_start_35
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->interrupted()Z

    move-result p2
    :try_end_35
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_35 .. :try_end_35} :catch_19
    .catch Ljava/lang/Exception; {:try_start_35 .. :try_end_35} :catch_1e
    .catchall {:try_start_35 .. :try_end_35} :catchall_1

    goto :goto_d

    :catch_19
    move-exception p2

    :try_start_36
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_36
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_36} :catch_1e
    .catchall {:try_start_36 .. :try_end_36} :catchall_1

    :cond_15
    :goto_d
    if-eqz v0, :cond_16

    if-nez p2, :cond_1b

    if-eqz v0, :cond_17

    :try_start_37
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->isConnected()Z

    move-result p2
    :try_end_37
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_37 .. :try_end_37} :catch_1a
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_37} :catch_1e
    .catchall {:try_start_37 .. :try_end_37} :catchall_1

    goto :goto_e

    :catch_1a
    move-exception p2

    :try_start_38
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_38
    .catch Ljava/lang/Exception; {:try_start_38 .. :try_end_38} :catch_1e
    .catchall {:try_start_38 .. :try_end_38} :catchall_1

    :cond_16
    :goto_e
    if-nez p2, :cond_18

    :try_start_39
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->connect()Lcom/jscape/inet/ftp/Ftp;
    :try_end_39
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_39 .. :try_end_39} :catch_1b
    .catch Ljava/lang/Exception; {:try_start_39 .. :try_end_39} :catch_1e
    .catchall {:try_start_39 .. :try_end_39} :catchall_1

    :cond_17
    :try_start_3a
    invoke-virtual {p0}, Lcom/jscape/inet/ftp/Ftp;->reset()V

    goto :goto_f

    :catch_1b
    move-exception p2

    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2

    :cond_18
    :goto_f
    new-instance p2, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-direct {p2, v2, v3}, Ljava/util/Date;-><init>(J)V
    :try_end_3a
    .catch Ljava/lang/Exception; {:try_start_3a .. :try_end_3a} :catch_1e
    .catchall {:try_start_3a .. :try_end_3a} :catchall_1

    :try_start_3b
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, p2}, Lcom/jscape/inet/ftp/Ftp;->setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)V

    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_3b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3b .. :try_end_3b} :catch_1c
    .catch Ljava/lang/Exception; {:try_start_3b .. :try_end_3b} :catch_1e
    .catchall {:try_start_3b .. :try_end_3b} :catchall_1

    if-eqz v0, :cond_19

    :try_start_3c
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result v2

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_3c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3c .. :try_end_3c} :catch_1d
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_3c} :catch_1e
    .catchall {:try_start_3c .. :try_end_3c} :catchall_1

    :cond_19
    :try_start_3d
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v8, v7, v6

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v4, v7, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "]"

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3d
    .catch Ljava/lang/Exception; {:try_start_3d .. :try_end_3d} :catch_1e
    .catchall {:try_start_3d .. :try_end_3d} :catchall_1

    goto :goto_10

    :catch_1c
    move-exception p2

    :try_start_3e
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_3e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3e .. :try_end_3e} :catch_1d
    .catch Ljava/lang/Exception; {:try_start_3e .. :try_end_3e} :catch_1e
    .catchall {:try_start_3e .. :try_end_3e} :catchall_1

    :catch_1d
    move-exception p2

    :try_start_3f
    invoke-static {p2}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_3f
    .catch Ljava/lang/Exception; {:try_start_3f .. :try_end_3f} :catch_1e
    .catchall {:try_start_3f .. :try_end_3f} :catchall_1

    :catch_1e
    :try_start_40
    iget-object p2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_40
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_40 .. :try_end_40} :catch_1f
    .catchall {:try_start_40 .. :try_end_40} :catchall_1

    if-eqz v0, :cond_1a

    :try_start_41
    invoke-virtual {p2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebug()Z

    move-result p2

    if-eqz p2, :cond_1b

    iget-object p2, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;
    :try_end_41
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_41 .. :try_end_41} :catch_20
    .catchall {:try_start_41 .. :try_end_41} :catchall_1

    :cond_1a
    :try_start_42
    invoke-virtual {p2}, Lcom/jscape/inet/ftp/FtpImplementation;->getDebugStream()Ljava/io/PrintStream;

    move-result-object p2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/ftp/Ftp;->i:[Ljava/lang/String;

    aget-object v4, v3, v6

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object p1, v3, v5

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_42
    .catchall {:try_start_42 .. :try_end_42} :catchall_1

    goto/16 :goto_8

    :cond_1b
    :goto_10
    if-eqz v0, :cond_1c

    if-nez v1, :cond_1c

    monitor-exit p0

    return-void

    :cond_1c
    :try_start_43
    throw v1
    :try_end_43
    .catchall {:try_start_43 .. :try_end_43} :catchall_1

    :catch_1f
    move-exception p1

    :try_start_44
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_44
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_44 .. :try_end_44} :catch_20
    .catchall {:try_start_44 .. :try_end_44} :catchall_1

    :catch_20
    move-exception p1

    :try_start_45
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_45
    .catchall {:try_start_45 .. :try_end_45} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadUnique(Ljava/io/File;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ftp/Ftp;->uploadUnique(Ljava/io/File;Z)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadUnique(Ljava/io/File;Z)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->b(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_0

    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/jscape/inet/ftp/Ftp;->uploadUnique(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftp/Ftp;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    const/4 p1, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/jscape/inet/ftp/Ftp;->uploadUnique(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadUnique(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v0, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v0, 0x0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {v1, p1, p2}, Lcom/jscape/inet/ftp/FtpImplementation;->uploadUnique(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit p0

    return-object p2

    :catchall_0
    move-exception p2

    :try_start_3
    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    iget-object p1, p0, Lcom/jscape/inet/ftp/Ftp;->d:Lcom/jscape/inet/ftp/FtpImplementation;

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ftp/FtpImplementation;->setTargetFile(Ljava/lang/String;)V

    throw p2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadUnique(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ftp/Ftp;->uploadUnique(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadUnique(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/jscape/inet/ftp/Ftp;->uploadUnique(Ljava/io/File;Z)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
