.class public final Lcom/jscape/inet/ftp/FeatRequest;
.super Lcom/jscape/inet/ftp/AbstractRequest;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final d:[Ljava/lang/String;


# instance fields
.field private c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x7

    const/4 v4, 0x0

    const-string v5, "\u0010\u000c7P#\u0004}\u0007\u0010\u000c7P#\u0004}"

    const/16 v6, 0xf

    move v8, v4

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x10

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v2

    invoke-virtual {v5, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v4

    :goto_2
    const/16 v15, 0x9

    const/4 v3, 0x2

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v3, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v2

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v3

    goto :goto_0

    :cond_0
    const-string v5, "\u001a\u00069Z\u0004\u001a\u00069Z"

    move v2, v0

    move v8, v3

    move v6, v15

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v2

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v11

    :goto_3
    const/16 v9, 0x6e

    add-int/2addr v7, v10

    add-int v3, v7, v2

    invoke-virtual {v5, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v4

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/ftp/FeatRequest;->d:[Ljava/lang/String;

    aget-object v0, v1, v4

    sput-object v0, Lcom/jscape/inet/ftp/FeatRequest;->b:Ljava/lang/String;

    aget-object v0, v1, v3

    sput-object v0, Lcom/jscape/inet/ftp/FeatRequest;->a:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v4, v14, 0x7

    if-eqz v4, :cond_9

    if-eq v4, v10, :cond_8

    if-eq v4, v3, :cond_7

    const/4 v3, 0x3

    if-eq v4, v3, :cond_6

    if-eq v4, v0, :cond_5

    const/4 v3, 0x5

    if-eq v4, v3, :cond_4

    goto :goto_4

    :cond_4
    const/16 v15, 0x7a

    goto :goto_4

    :cond_5
    const/16 v15, 0x56

    goto :goto_4

    :cond_6
    const/16 v15, 0x60

    goto :goto_4

    :cond_7
    const/16 v15, 0x16

    goto :goto_4

    :cond_8
    const/16 v15, 0x2d

    goto :goto_4

    :cond_9
    const/16 v15, 0x32

    :goto_4
    xor-int v3, v9, v15

    xor-int v3, v16, v3

    int-to-char v3, v3

    aput-char v3, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v4, 0x0

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ftp/AbstractRequest;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public execute(Lcom/jscape/inet/ftp/ClientProtocolInterpreter;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/FeatRequest;->assertIsValid()V

    sget-object v1, Lcom/jscape/inet/ftp/FeatRequest;->d:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {p1, v1}, Lcom/jscape/inet/ftp/ClientProtocolInterpreter;->sendCommand(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/jscape/inet/ftp/ClientProtocolInterpreter;->receiveReply()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftp/FeatRequest;->assertIsPositive(Ljava/lang/String;)V

    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    :cond_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v2, :cond_2

    :try_start_1
    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/jscape/inet/ftp/FeatRequest;->d:[Ljava/lang/String;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v0, :cond_1

    if-nez v3, :cond_2

    :try_start_2
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/FeatRequest;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftp/FeatRequest;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    check-cast p1, [Ljava/lang/String;

    iput-object p1, p0, Lcom/jscape/inet/ftp/FeatRequest;->c:[Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    return-void

    :catch_2
    move-exception p1

    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public getFeatures()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FeatRequest;->c:[Ljava/lang/String;

    invoke-virtual {v0}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
