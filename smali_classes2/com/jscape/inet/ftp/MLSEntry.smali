.class public final Lcom/jscape/inet/ftp/MLSEntry;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Long;

.field private c:Ljava/util/Date;

.field private d:Ljava/util/Date;

.field private e:Lcom/jscape/inet/ftp/MLSEntry$Type;

.field private f:Ljava/lang/String;

.field private g:Lcom/jscape/inet/ftp/MLSEntry$Permissions;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/util/Map;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ftp/MLSEntry;->a:Ljava/lang/String;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ftp/MLSEntry;->k:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public getAdditionalFact(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSEntry;->k:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public getAdditionalFacts()Ljava/util/Map;
    .locals 2

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/jscape/inet/ftp/MLSEntry;->k:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public getCharset()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSEntry;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getCreationDate()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSEntry;->d:Ljava/util/Date;

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSEntry;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getLastModificationDate()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSEntry;->c:Ljava/util/Date;

    return-object v0
.end method

.method public getMediaType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSEntry;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getPathname()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSEntry;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getPermissions()Lcom/jscape/inet/ftp/MLSEntry$Permissions;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ftp/MLSEntry;->g:Lcom/jscape/inet/ftp/MLSEntry$Permissions;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/jscape/inet/ftp/MLSEntry$Permissions;

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSEntry;->g:Lcom/jscape/inet/ftp/MLSEntry$Permissions;

    invoke-direct {v1, v0}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;-><init>(Lcom/jscape/inet/ftp/MLSEntry$Permissions;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move-object v0, v1

    :goto_1
    return-object v0
.end method

.method public getSize()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSEntry;->b:Ljava/lang/Long;

    return-object v0
.end method

.method public getType()Lcom/jscape/inet/ftp/MLSEntry$Type;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSEntry;->e:Lcom/jscape/inet/ftp/MLSEntry$Type;

    return-object v0
.end method

.method public getUniqueId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSEntry;->f:Ljava/lang/String;

    return-object v0
.end method

.method public setAdditionalFact(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/MLSEntry;->k:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setCharset(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/MLSEntry;->j:Ljava/lang/String;

    return-void
.end method

.method public setCreationDate(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/MLSEntry;->d:Ljava/util/Date;

    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/MLSEntry;->h:Ljava/lang/String;

    return-void
.end method

.method public setLastModificationDate(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/MLSEntry;->c:Ljava/util/Date;

    return-void
.end method

.method public setMediaType(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/MLSEntry;->i:Ljava/lang/String;

    return-void
.end method

.method public setPermissions(Lcom/jscape/inet/ftp/MLSEntry$Permissions;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    new-instance v0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;

    invoke-direct {v0, p1}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;-><init>(Lcom/jscape/inet/ftp/MLSEntry$Permissions;)V

    move-object p1, v0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/jscape/inet/ftp/MLSEntry;->g:Lcom/jscape/inet/ftp/MLSEntry$Permissions;

    return-void
.end method

.method public setSize(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/MLSEntry;->b:Ljava/lang/Long;

    return-void
.end method

.method public setType(Lcom/jscape/inet/ftp/MLSEntry$Type;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/MLSEntry;->e:Lcom/jscape/inet/ftp/MLSEntry$Type;

    return-void
.end method

.method public setUniqueId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/MLSEntry;->f:Ljava/lang/String;

    return-void
.end method
