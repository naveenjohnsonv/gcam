.class public final Lcom/jscape/inet/ftp/MLSEntryAssembler;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;

.field private static final e:Ljava/lang/String;

.field private static final f:Ljava/lang/String;

.field private static final g:Ljava/lang/String;

.field private static final h:Ljava/lang/String;

.field private static final i:Ljava/lang/String;

.field private static final j:C = 'a'

.field private static final k:C = 'c'

.field private static final l:C = 'd'

.field private static final m:C = 'e'

.field private static final n:C = 'f'

.field private static final o:C = 'l'

.field private static final p:C = 'm'

.field private static final q:C = 'p'

.field private static final r:C = 'r'

.field private static final s:C = 'w'

.field private static final t:C = ' '

.field private static final u:C = '='

.field private static final v:Ljava/lang/String; = ";"

.field private static final w:Ljava/lang/String;

.field private static final x:Ljava/lang/String;

.field private static final y:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x1b

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x6

    const/4 v3, 0x0

    const-string v4, "gm&\u001eD\u007f\u0008fc,\u0010Wg:o\u0004yk8\u0012\u000es{;\u000eoK9nJ\n\u001aOu.\u0008fc,\u0010Wg:o\ngg&\u001eC+)sr\'\ngg&\u001eC+)sr\'\u0006ip\'\u0016Vc\u000es{;\u000eoK9nJ\n\u001aOu.\u0006\u007fl+\u0006Wc\u0006gm&\u001eD\u007f\u0007ij#\u0005Qc)\u0008hc&WFg)k\u0012s{;\u000eoK9nJ\n\u001aOu.$Q\u0011$\u0003_V\u0001\u0007ij#\u0005Qc)\u000ecl4\u0016No9*d-\u0005Og)\u0006\u007fl+\u0006Wc\u0006ip\'\u0016Vc\u0004~{2\u0012\u0003_V\u0001\u0004yk8\u0012\u0004zg0\u001a\u0004~{2\u0012\u0012s{;\u000eoK9nJ\n\u001aOu.$Q\u0011$"

    const/16 v5, 0xde

    move v7, v1

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x45

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/16 v15, 0x18

    const/4 v2, 0x2

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v2, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v2

    goto :goto_0

    :cond_0
    const/16 v5, 0x13

    const-string v4, "\u0012\u000fXr\u000e\u000b\u0004\\~&\u0007QB\u000cEm\'\u000fA"

    move v8, v2

    const/4 v6, -0x1

    const/4 v7, 0x4

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v7, v2

    move v8, v11

    :goto_3
    const/16 v9, 0x2d

    add-int/2addr v6, v10

    add-int v2, v6, v7

    invoke-virtual {v4, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v4, 0x17

    aget-object v4, v0, v4

    sput-object v4, Lcom/jscape/inet/ftp/MLSEntryAssembler;->d:Ljava/lang/String;

    const/16 v4, 0xb

    aget-object v4, v0, v4

    sput-object v4, Lcom/jscape/inet/ftp/MLSEntryAssembler;->i:Ljava/lang/String;

    aget-object v4, v0, v10

    sput-object v4, Lcom/jscape/inet/ftp/MLSEntryAssembler;->g:Ljava/lang/String;

    aget-object v2, v0, v2

    sput-object v2, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a:Ljava/lang/String;

    const/16 v2, 0x12

    aget-object v2, v0, v2

    sput-object v2, Lcom/jscape/inet/ftp/MLSEntryAssembler;->c:Ljava/lang/String;

    aget-object v1, v0, v1

    sput-object v1, Lcom/jscape/inet/ftp/MLSEntryAssembler;->h:Ljava/lang/String;

    const/16 v1, 0x11

    aget-object v1, v0, v1

    sput-object v1, Lcom/jscape/inet/ftp/MLSEntryAssembler;->e:Ljava/lang/String;

    const/16 v1, 0x16

    aget-object v1, v0, v1

    sput-object v1, Lcom/jscape/inet/ftp/MLSEntryAssembler;->f:Ljava/lang/String;

    const/16 v1, 0x8

    aget-object v1, v0, v1

    sput-object v1, Lcom/jscape/inet/ftp/MLSEntryAssembler;->w:Ljava/lang/String;

    aget-object v1, v0, v3

    sput-object v1, Lcom/jscape/inet/ftp/MLSEntryAssembler;->b:Ljava/lang/String;

    aget-object v0, v0, v15

    sput-object v0, Lcom/jscape/inet/ftp/MLSEntryAssembler;->x:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v10, :cond_8

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    goto :goto_4

    :cond_4
    const/16 v15, 0x43

    goto :goto_4

    :cond_5
    const/16 v15, 0x67

    goto :goto_4

    :cond_6
    const/16 v15, 0x32

    goto :goto_4

    :cond_7
    const/4 v15, 0x7

    goto :goto_4

    :cond_8
    const/16 v15, 0x47

    goto :goto_4

    :cond_9
    const/16 v15, 0x4f

    :goto_4
    xor-int v1, v9, v15

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v1, 0x6

    goto/16 :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/inet/ftp/MLSEntry$Permissions;)Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isFileAppendingAllowed()Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_9

    if-eqz v0, :cond_1

    if-eqz v2, :cond_0

    const/16 v2, 0x61

    :try_start_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_a

    :cond_0
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isFileCreationAllowed()Z

    move-result v2

    :cond_1
    if-eqz v0, :cond_3

    if-eqz v2, :cond_2

    const/16 v2, 0x63

    :try_start_2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isDeletionAllowed()Z

    move-result v2

    :cond_3
    if-eqz v0, :cond_5

    if-eqz v2, :cond_4

    const/16 v2, 0x64

    :try_start_3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_4
    :goto_1
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isDirectoryEnteringAllowed()Z

    move-result v2

    :cond_5
    if-eqz v0, :cond_7

    if-eqz v2, :cond_6

    const/16 v2, 0x65

    :try_start_4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_6
    :goto_2
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isRenamingAllowed()Z

    move-result v2

    :cond_7
    if-eqz v0, :cond_9

    if-eqz v2, :cond_8

    const/16 v2, 0x66

    :try_start_5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_8
    :goto_3
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isListingAllowed()Z

    move-result v2

    :cond_9
    if-eqz v0, :cond_b

    if-eqz v2, :cond_a

    const/16 v2, 0x6c

    :try_start_6
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_a
    :goto_4
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isDirectoryCreationAllowed()Z

    move-result v2

    :cond_b
    if-eqz v0, :cond_d

    if-eqz v2, :cond_c

    const/16 v2, 0x6d

    :try_start_7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_5

    goto :goto_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_c
    :goto_5
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isDirectoryPurgingAllowed()Z

    move-result v2

    :cond_d
    if-eqz v0, :cond_f

    if-eqz v2, :cond_e

    const/16 v2, 0x70

    :try_start_8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_6

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_e
    :goto_6
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isFileReadingAllowed()Z

    move-result v2

    :cond_f
    if-eqz v0, :cond_11

    if-eqz v2, :cond_10

    const/16 v0, 0x72

    :try_start_9
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_7

    goto :goto_7

    :catch_7
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_10
    :goto_7
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->isFileWritingAllowed()Z

    move-result v2

    :cond_11
    if-eqz v2, :cond_12

    const/16 p1, 0x77

    :try_start_a
    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_8

    goto :goto_8

    :catch_8
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_12
    :goto_8
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catch_9
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_a

    :catch_a
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
.end method

.method private a(Ljava/util/Date;)Ljava/lang/String;
    .locals 5

    :try_start_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    rem-long/2addr v1, v3

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    sget-object v1, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v1, v1, v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v2, 0x18

    aget-object v1, v1, v2

    :goto_0
    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
.end method

.method private a(Ljava/lang/String;)Ljava/util/Date;
    .locals 3

    :try_start_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v2, 0xd

    aget-object v2, v1, v2

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x14

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    :try_start_1
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :catch_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v1, 0x10

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuffer;)V
    .locals 0

    invoke-virtual {p3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 p1, 0x3d

    invoke-virtual {p3, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string p1, ";"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    return-void
.end method

.method private a(Ljava/util/Map;Ljava/lang/StringBuffer;)V
    .locals 4

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-eqz v3, :cond_1

    :try_start_0
    invoke-direct {p0, v2, v3, p2}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuffer;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    if-nez v1, :cond_0

    :cond_2
    return-void
.end method

.method private b(Ljava/lang/String;)Lcom/jscape/inet/ftp/MLSEntry$Type;
    .locals 7

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    sget-object v1, Lcom/jscape/inet/ftp/MLSEntry$Type;->FILE:Lcom/jscape/inet/ftp/MLSEntry$Type;

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/MLSEntry$Type;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    sget-object p1, Lcom/jscape/inet/ftp/MLSEntry$Type;->FILE:Lcom/jscape/inet/ftp/MLSEntry$Type;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4

    return-object p1

    :cond_0
    sget-object v1, Lcom/jscape/inet/ftp/MLSEntry$Type;->CURRENT_DIRECTORY:Lcom/jscape/inet/ftp/MLSEntry$Type;

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/MLSEntry$Type;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    :cond_1
    if-eqz v0, :cond_3

    if-eqz v1, :cond_2

    :try_start_2
    sget-object p1, Lcom/jscape/inet/ftp/MLSEntry$Type;->CURRENT_DIRECTORY:Lcom/jscape/inet/ftp/MLSEntry$Type;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_2
    sget-object v1, Lcom/jscape/inet/ftp/MLSEntry$Type;->PARENT_DIRECTORY:Lcom/jscape/inet/ftp/MLSEntry$Type;

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/MLSEntry$Type;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    :cond_3
    if-eqz v0, :cond_5

    if-eqz v1, :cond_4

    :try_start_3
    sget-object p1, Lcom/jscape/inet/ftp/MLSEntry$Type;->PARENT_DIRECTORY:Lcom/jscape/inet/ftp/MLSEntry$Type;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    return-object p1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_4
    sget-object v1, Lcom/jscape/inet/ftp/MLSEntry$Type;->DIRECTORY:Lcom/jscape/inet/ftp/MLSEntry$Type;

    invoke-virtual {v1}, Lcom/jscape/inet/ftp/MLSEntry$Type;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    :cond_5
    if-eqz v0, :cond_7

    if-eqz v1, :cond_6

    :try_start_4
    sget-object p1, Lcom/jscape/inet/ftp/MLSEntry$Type;->DIRECTORY:Lcom/jscape/inet/ftp/MLSEntry$Type;
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    return-object p1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_6
    const/16 v0, 0x3d

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    :cond_7
    int-to-long v2, v1

    const-wide/16 v4, 0x0

    sget-object v0, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v6, 0x1a

    aget-object v0, v0, v6

    invoke-static {v2, v3, v4, v5, v0}, Lcom/jscape/util/w;->a(JJLjava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/ftp/MLSEntry$Type;

    invoke-direct {v1, v0, p1}, Lcom/jscape/inet/ftp/MLSEntry$Type;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :catch_3
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
.end method

.method private c(Ljava/lang/String;)Lcom/jscape/inet/ftp/MLSEntry$Permissions;
    .locals 7

    new-instance v0, Lcom/jscape/inet/ftp/MLSEntry$Permissions;

    invoke-direct {v0}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v2, :cond_14

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x61

    const/4 v6, 0x1

    if-eqz v1, :cond_2

    if-ne v5, v4, :cond_1

    :try_start_0
    invoke-virtual {v0, v6}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->setFileAppendingAllowed(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_13

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    const/16 v5, 0x63

    :cond_2
    if-eqz v1, :cond_4

    if-ne v5, v4, :cond_3

    :try_start_2
    invoke-virtual {v0, v6}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->setFileCreationAllowed(Z)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    if-nez v1, :cond_13

    goto :goto_1

    :catch_2
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    const/16 v5, 0x64

    :cond_4
    if-eqz v1, :cond_6

    if-ne v5, v4, :cond_5

    :try_start_4
    invoke-virtual {v0, v6}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->setDeletionAllowed(Z)V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_4

    if-nez v1, :cond_13

    goto :goto_2

    :catch_4
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_5
    :goto_2
    const/16 v5, 0x65

    :cond_6
    if-eqz v1, :cond_8

    if-ne v5, v4, :cond_7

    :try_start_6
    invoke-virtual {v0, v6}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->setDirectoryEnteringAllowed(Z)V
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_6

    if-nez v1, :cond_13

    goto :goto_3

    :catch_6
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_7
    :goto_3
    const/16 v5, 0x66

    :cond_8
    if-eqz v1, :cond_a

    if-ne v5, v4, :cond_9

    :try_start_8
    invoke-virtual {v0, v6}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->setRenamingAllowed(Z)V
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_8

    if-nez v1, :cond_13

    goto :goto_4

    :catch_8
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_9
    :goto_4
    const/16 v5, 0x6c

    :cond_a
    if-eqz v1, :cond_c

    if-ne v5, v4, :cond_b

    :try_start_a
    invoke-virtual {v0, v6}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->setListingAllowed(Z)V
    :try_end_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_a

    if-nez v1, :cond_13

    goto :goto_5

    :catch_a
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_b

    :catch_b
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_b
    :goto_5
    const/16 v5, 0x6d

    :cond_c
    if-eqz v1, :cond_e

    if-ne v5, v4, :cond_d

    :try_start_c
    invoke-virtual {v0, v6}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->setDirectoryCreationAllowed(Z)V
    :try_end_c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_c} :catch_c

    if-nez v1, :cond_13

    goto :goto_6

    :catch_c
    move-exception p1

    :try_start_d
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_d .. :try_end_d} :catch_d

    :catch_d
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_d
    :goto_6
    const/16 v5, 0x70

    :cond_e
    if-eqz v1, :cond_10

    if-ne v5, v4, :cond_f

    :try_start_e
    invoke-virtual {v0, v6}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->setDirectoryPurgingAllowed(Z)V
    :try_end_e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_e} :catch_e

    if-nez v1, :cond_13

    goto :goto_7

    :catch_e
    move-exception p1

    :try_start_f
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_f .. :try_end_f} :catch_f

    :catch_f
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_f
    :goto_7
    const/16 v5, 0x72

    :cond_10
    if-eqz v1, :cond_12

    if-ne v5, v4, :cond_11

    :try_start_10
    invoke-virtual {v0, v6}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->setFileReadingAllowed(Z)V
    :try_end_10
    .catch Ljava/lang/IllegalArgumentException; {:try_start_10 .. :try_end_10} :catch_10

    if-nez v1, :cond_13

    goto :goto_8

    :catch_10
    move-exception p1

    :try_start_11
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_11
    .catch Ljava/lang/IllegalArgumentException; {:try_start_11 .. :try_end_11} :catch_11

    :catch_11
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_11
    :goto_8
    const/16 v5, 0x77

    :cond_12
    if-ne v5, v4, :cond_13

    :try_start_12
    invoke-virtual {v0, v6}, Lcom/jscape/inet/ftp/MLSEntry$Permissions;->setFileWritingAllowed(Z)V
    :try_end_12
    .catch Ljava/lang/IllegalArgumentException; {:try_start_12 .. :try_end_12} :catch_12

    goto :goto_9

    :catch_12
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_13
    :goto_9
    add-int/lit8 v3, v3, 0x1

    if-nez v1, :cond_0

    :cond_14
    return-object v0
.end method


# virtual methods
.method public restoreEntry(Ljava/lang/String;)Lcom/jscape/inet/ftp/MLSEntry;
    .locals 13

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    int-to-long v1, v0

    sget-object v3, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v4, 0xc

    aget-object v3, v3, v4

    const-wide/16 v4, 0x0

    invoke-static {v1, v2, v4, v5, v3}, Lcom/jscape/util/w;->b(JJLjava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    add-int/2addr v0, v3

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v6, Lcom/jscape/inet/ftp/MLSEntry;

    invoke-direct {v6, p1}, Lcom/jscape/inet/ftp/MLSEntry;-><init>(Ljava/lang/String;)V

    const-string p1, ";"

    invoke-static {v2, p1, v1}, Lcom/jscape/util/at;->a(Ljava/lang/String;Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object p1

    move v2, v1

    :cond_0
    array-length v7, p1

    if-ge v2, v7, :cond_13

    aget-object v7, p1, v2

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x3d

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    int-to-long v9, v8

    sget-object v11, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v12, 0x10

    aget-object v12, v11, v12

    invoke-static {v9, v10, v4, v5, v12}, Lcom/jscape/util/w;->a(JJLjava/lang/String;)V

    invoke-virtual {v7, v1, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    add-int/2addr v8, v3

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    :try_start_0
    aget-object v8, v11, v8

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_10

    if-eqz v0, :cond_2

    if-eqz v8, :cond_1

    :try_start_1
    new-instance v8, Ljava/lang/Long;

    invoke-direct {v8, v7}, Ljava/lang/Long;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v8}, Lcom/jscape/inet/ftp/MLSEntry;->setSize(Ljava/lang/Long;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_11

    if-nez v0, :cond_12

    :cond_1
    :try_start_2
    sget-object v8, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    aget-object v8, v8, v1

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_12

    :cond_2
    if-eqz v0, :cond_4

    if-eqz v8, :cond_3

    :try_start_3
    invoke-direct {p0, v7}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/jscape/inet/ftp/MLSEntry;->setLastModificationDate(Ljava/util/Date;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0

    if-nez v0, :cond_12

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_3
    :goto_0
    sget-object v8, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v10, 0x12

    aget-object v8, v8, v10

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_4
    :goto_1
    if-eqz v0, :cond_6

    if-eqz v8, :cond_5

    :try_start_5
    invoke-direct {p0, v7}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/jscape/inet/ftp/MLSEntry;->setCreationDate(Ljava/util/Date;)V
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_2

    if-nez v0, :cond_12

    goto :goto_2

    :catch_2
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_5
    :goto_2
    sget-object v8, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v10, 0x17

    aget-object v8, v8, v10

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_6
    :goto_3
    if-eqz v0, :cond_8

    if-eqz v8, :cond_7

    :try_start_7
    invoke-direct {p0, v7}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->b(Ljava/lang/String;)Lcom/jscape/inet/ftp/MLSEntry$Type;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/jscape/inet/ftp/MLSEntry;->setType(Lcom/jscape/inet/ftp/MLSEntry$Type;)V
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_4

    if-nez v0, :cond_12

    goto :goto_4

    :catch_4
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_7
    :goto_4
    sget-object v8, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v10, 0x11

    aget-object v8, v8, v10

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_8
    :goto_5
    if-eqz v0, :cond_a

    if-eqz v8, :cond_9

    :try_start_9
    invoke-virtual {v6, v7}, Lcom/jscape/inet/ftp/MLSEntry;->setUniqueId(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_6

    if-nez v0, :cond_12

    goto :goto_6

    :catch_6
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_9
    :goto_6
    sget-object v8, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v10, 0x16

    aget-object v8, v8, v10

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8
    :try_end_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_7

    goto :goto_7

    :catch_7
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_a
    :goto_7
    if-eqz v0, :cond_c

    if-eqz v8, :cond_b

    :try_start_b
    sget-object v8, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->c(Ljava/lang/String;)Lcom/jscape/inet/ftp/MLSEntry$Permissions;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/jscape/inet/ftp/MLSEntry;->setPermissions(Lcom/jscape/inet/ftp/MLSEntry$Permissions;)V
    :try_end_b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_8

    if-nez v0, :cond_12

    goto :goto_8

    :catch_8
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_b
    :goto_8
    sget-object v8, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    aget-object v8, v8, v3

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8
    :try_end_c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_c} :catch_9

    goto :goto_9

    :catch_9
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_c
    :goto_9
    if-eqz v0, :cond_e

    if-eqz v8, :cond_d

    :try_start_d
    invoke-virtual {v6, v7}, Lcom/jscape/inet/ftp/MLSEntry;->setLanguage(Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_d .. :try_end_d} :catch_a

    if-nez v0, :cond_12

    goto :goto_a

    :catch_a
    move-exception p1

    :try_start_e
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_d
    :goto_a
    sget-object v8, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/4 v10, 0x6

    aget-object v8, v8, v10

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8
    :try_end_e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_e} :catch_b

    goto :goto_b

    :catch_b
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_e
    :goto_b
    if-eqz v0, :cond_10

    if-eqz v8, :cond_f

    :try_start_f
    invoke-virtual {v6, v7}, Lcom/jscape/inet/ftp/MLSEntry;->setMediaType(Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_f .. :try_end_f} :catch_c

    if-nez v0, :cond_12

    goto :goto_c

    :catch_c
    move-exception p1

    :try_start_10
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_f
    :goto_c
    sget-object v8, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v10, 0xb

    aget-object v8, v8, v10

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8
    :try_end_10
    .catch Ljava/lang/IllegalArgumentException; {:try_start_10 .. :try_end_10} :catch_d

    goto :goto_d

    :catch_d
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_10
    :goto_d
    if-eqz v8, :cond_11

    :try_start_11
    invoke-virtual {v6, v7}, Lcom/jscape/inet/ftp/MLSEntry;->setCharset(Ljava/lang/String;)V
    :try_end_11
    .catch Ljava/lang/IllegalArgumentException; {:try_start_11 .. :try_end_11} :catch_e

    if-nez v0, :cond_12

    goto :goto_e

    :catch_e
    move-exception p1

    :try_start_12
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_11
    :goto_e
    invoke-virtual {v6, v9, v7}, Lcom/jscape/inet/ftp/MLSEntry;->setAdditionalFact(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_12
    .catch Ljava/lang/IllegalArgumentException; {:try_start_12 .. :try_end_12} :catch_f

    :cond_12
    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_0

    goto :goto_f

    :catch_f
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :catch_10
    move-exception p1

    :try_start_13
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_13
    .catch Ljava/lang/IllegalArgumentException; {:try_start_13 .. :try_end_13} :catch_11

    :catch_11
    move-exception p1

    :try_start_14
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_14
    .catch Ljava/lang/IllegalArgumentException; {:try_start_14 .. :try_end_14} :catch_12

    :catch_12
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_13
    :goto_f
    return-object v6
.end method

.method public storeEntry(Lcom/jscape/inet/ftp/MLSEntry;)Ljava/lang/String;
    .locals 7

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getSize()Ljava/lang/Long;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_0

    :try_start_1
    sget-object v2, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v3, 0x15

    aget-object v2, v2, v3

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getSize()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3, v1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuffer;)V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    :try_start_2
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getLastModificationDate()Ljava/util/Date;

    move-result-object v2
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_e

    if-eqz v0, :cond_2

    if-eqz v2, :cond_1

    :try_start_3
    sget-object v2, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v3, 0xa

    aget-object v2, v2, v3

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getLastModificationDate()Ljava/util/Date;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3, v1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuffer;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_f

    :cond_1
    if-eqz v0, :cond_3

    :try_start_4
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getCreationDate()Ljava/util/Date;

    move-result-object v2
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    if-eqz v2, :cond_3

    :try_start_5
    sget-object v2, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/4 v3, 0x7

    aget-object v2, v2, v3

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getCreationDate()Ljava/util/Date;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3, v1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuffer;)V
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_2

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_3
    :goto_2
    if-eqz v0, :cond_6

    :try_start_6
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getType()Lcom/jscape/inet/ftp/MLSEntry$Type;

    move-result-object v2
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_6

    if-eqz v2, :cond_6

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getType()Lcom/jscape/inet/ftp/MLSEntry$Type;

    move-result-object v2

    :try_start_7
    sget-object v3, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v4, 0x13

    aget-object v3, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/MLSEntry$Type;->getOsName()Ljava/lang/String;

    move-result-object v5

    if-eqz v0, :cond_5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_4

    if-lez v5, :cond_4

    :try_start_8
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/MLSEntry$Type;->getOsName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v6, 0x3d

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5
    :try_end_8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_3

    :cond_4
    const-string v5, ""

    :cond_5
    :goto_3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/MLSEntry$Type;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v3, v2, v1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuffer;)V

    goto :goto_4

    :catch_4
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_6
    :goto_4
    if-eqz v0, :cond_7

    :try_start_a
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getUniqueId()Ljava/lang/String;

    move-result-object v2
    :try_end_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_7

    if-eqz v2, :cond_7

    :try_start_b
    sget-object v2, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v3, 0x9

    aget-object v2, v2, v3

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getUniqueId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3, v1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuffer;)V

    goto :goto_5

    :catch_7
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_8

    :catch_8
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_7
    :goto_5
    if-eqz v0, :cond_8

    :try_start_c
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getPermissions()Lcom/jscape/inet/ftp/MLSEntry$Permissions;

    move-result-object v2
    :try_end_c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_c} :catch_9

    if-eqz v2, :cond_8

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getPermissions()Lcom/jscape/inet/ftp/MLSEntry$Permissions;

    move-result-object v2

    sget-object v3, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v4, 0x19

    aget-object v3, v3, v4

    invoke-direct {p0, v2}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Lcom/jscape/inet/ftp/MLSEntry$Permissions;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v3, v2, v1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuffer;)V

    goto :goto_6

    :catch_9
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_8
    :goto_6
    :try_start_d
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getLanguage()Ljava/lang/String;

    move-result-object v2
    :try_end_d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_d .. :try_end_d} :catch_c

    if-eqz v0, :cond_a

    if-eqz v2, :cond_9

    :try_start_e
    sget-object v2, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3, v1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuffer;)V
    :try_end_e
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_e} :catch_d

    :cond_9
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getMediaType()Ljava/lang/String;

    move-result-object v2

    :cond_a
    if-eqz v0, :cond_c

    if-eqz v2, :cond_b

    :try_start_f
    sget-object v2, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getMediaType()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3, v1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuffer;)V
    :try_end_f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_f .. :try_end_f} :catch_a

    goto :goto_7

    :catch_a
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_b
    :goto_7
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getCharset()Ljava/lang/String;

    move-result-object v2

    :cond_c
    if-eqz v0, :cond_e

    if-eqz v2, :cond_d

    :try_start_10
    sget-object v0, Lcom/jscape/inet/ftp/MLSEntryAssembler;->y:[Ljava/lang/String;

    const/16 v2, 0xf

    aget-object v0, v0, v2

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getCharset()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2, v1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuffer;)V
    :try_end_10
    .catch Ljava/lang/IllegalArgumentException; {:try_start_10 .. :try_end_10} :catch_b

    goto :goto_8

    :catch_b
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_d
    :goto_8
    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getAdditionalFacts()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/util/Map;Ljava/lang/StringBuffer;)V

    const/16 v0, 0x20

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/MLSEntry;->getPathname()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_e
    return-object v2

    :catch_c
    move-exception p1

    :try_start_11
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_11
    .catch Ljava/lang/IllegalArgumentException; {:try_start_11 .. :try_end_11} :catch_d

    :catch_d
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :catch_e
    move-exception p1

    :try_start_12
    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
    :try_end_12
    .catch Ljava/lang/IllegalArgumentException; {:try_start_12 .. :try_end_12} :catch_f

    :catch_f
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/MLSEntryAssembler;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
.end method
