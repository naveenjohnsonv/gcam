.class public final Lcom/jscape/inet/ftp/CRCComputer;
.super Ljava/lang/Object;


# static fields
.field private static final a:I = 0x1000


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public compute(Ljava/io/InputStream;J)J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/util/zip/CheckedInputStream;

    new-instance v1, Ljava/util/zip/CRC32;

    invoke-direct {v1}, Ljava/util/zip/CRC32;-><init>()V

    invoke-direct {v0, p1, v1}, Ljava/util/zip/CheckedInputStream;-><init>(Ljava/io/InputStream;Ljava/util/zip/Checksum;)V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object p1

    const/16 v1, 0x1000

    new-array v2, v1, [B

    int-to-long v3, v1

    cmp-long v5, v3, p2

    if-eqz p1, :cond_1

    if-lez v5, :cond_0

    move-wide v5, p2

    goto :goto_0

    :cond_0
    move v5, v1

    :cond_1
    int-to-long v5, v5

    :cond_2
    :goto_0
    const/4 v7, 0x0

    long-to-int v5, v5

    invoke-virtual {v0, v2, v7, v5}, Ljava/util/zip/CheckedInputStream;->read([BII)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_5

    if-eqz p1, :cond_6

    if-eqz p1, :cond_6

    const-wide/16 v6, 0x0

    cmp-long v6, p2, v6

    if-lez v6, :cond_5

    int-to-long v5, v5

    sub-long/2addr p2, v5

    cmp-long v5, v3, p2

    if-eqz p1, :cond_4

    if-lez v5, :cond_3

    move-wide v5, p2

    goto :goto_1

    :cond_3
    move v5, v1

    :cond_4
    int-to-long v5, v5

    :goto_1
    if-nez p1, :cond_2

    :cond_5
    invoke-virtual {v0}, Ljava/util/zip/CheckedInputStream;->getChecksum()Ljava/util/zip/Checksum;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/zip/Checksum;->getValue()J

    move-result-wide p2

    :cond_6
    return-wide p2
.end method
