.class public Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final blksize:Ljava/lang/String;

.field public final dsname:Ljava/lang/String;

.field public final dsorg:Ljava/lang/String;

.field public final ext:Ljava/lang/String;

.field public final line:Ljava/lang/String;

.field public final lrecl:Ljava/lang/String;

.field public final recfm:Ljava/lang/String;

.field public final referred:Ljava/lang/String;

.field public final unit:Ljava/lang/String;

.field public final used:Ljava/lang/String;

.field public final volume:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "0okk/\tm!h\u00070o|v8R\'\u000c0okk*\nrn*}3k\t0ou|)\u000cl!h\t0o}}#\u001dg!h\u00080ol})\u000b=;\u000b0o{b\'\u001cif*$)\u00080ol`%\u001b=;"

    const/16 v4, 0x50

    const/16 v5, 0x9

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/4 v8, 0x1

    add-int/2addr v6, v8

    add-int v9, v6, v5

    invoke-virtual {v3, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0x31

    move v12, v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    array-length v13, v9

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v9}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v12}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v11, :cond_1

    add-int/lit8 v11, v7, 0x1

    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v11

    goto :goto_0

    :cond_0
    const/16 v4, 0x22

    const/16 v3, 0x17

    const-string v5, "s\u001bHj\u0007!Gl\u0008XC\u001c)\u0002E\u001bT@\u001b G\u0003J\n\u0012M__\u0000,O[P\u001c"

    move v7, v11

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v7, 0x1

    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v11

    :goto_3
    const/16 v12, 0x13

    add-int/2addr v6, v8

    add-int v9, v6, v5

    invoke-virtual {v3, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v9, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v8, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    move v1, v10

    goto :goto_4

    :cond_4
    const/16 v1, 0x5e

    goto :goto_4

    :cond_5
    const/16 v1, 0x7d

    goto :goto_4

    :cond_6
    const/16 v1, 0x3f

    goto :goto_4

    :cond_7
    const/16 v1, 0x28

    goto :goto_4

    :cond_8
    const/16 v1, 0x7e

    goto :goto_4

    :cond_9
    const/16 v1, 0x2d

    :goto_4
    xor-int/2addr v1, v12

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v9, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->line:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->volume:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->unit:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->referred:Ljava/lang/String;

    iput-object p5, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->ext:Ljava/lang/String;

    iput-object p6, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->used:Ljava/lang/String;

    iput-object p7, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->recfm:Ljava/lang/String;

    iput-object p8, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->lrecl:Ljava/lang/String;

    iput-object p9, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->blksize:Ljava/lang/String;

    iput-object p10, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->dsorg:Ljava/lang/String;

    iput-object p11, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->dsname:Ljava/lang/String;

    return-void
.end method

.method private a()J
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->blksize:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    :catch_0
    const-wide/16 v0, 0x0

    return-wide v0
.end method


# virtual methods
.method public asFile()Lcom/jscape/inet/ftp/FtpFile;
    .locals 15

    new-instance v14, Lcom/jscape/inet/ftp/FtpFile;

    iget-object v1, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->dsname:Ljava/lang/String;

    invoke-direct {p0}, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->a()J

    move-result-wide v2

    iget-object v13, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->line:Ljava/lang/String;

    const-string v4, ""

    const-string v5, ""

    const-string v6, ""

    const-string v7, ""

    const-string v8, ""

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-string v11, ""

    const/4 v12, 0x0

    move-object v0, v14

    invoke-direct/range {v0 .. v13}, Lcom/jscape/inet/ftp/FtpFile;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ILjava/lang/String;)V

    return-object v14
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->a:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->volume:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x7

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->unit:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->referred:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->ext:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->used:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->recfm:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x3

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->lrecl:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x6

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->blksize:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->dsorg:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v3, 0x9

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ftp/MvsParser$MvsFileRecord;->dsname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "QVL4hb"

    invoke-static {v1}, Lcom/jscape/inet/ftp/FtpEvent;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method
