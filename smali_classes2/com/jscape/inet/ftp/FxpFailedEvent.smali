.class public Lcom/jscape/inet/ftp/FxpFailedEvent;
.super Ljava/util/EventObject;


# instance fields
.field private a:Ljava/lang/Exception;

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    invoke-direct {p0, p1}, Ljava/util/EventObject;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ftp/FxpFailedEvent;->b:Ljava/lang/Object;

    iput-object p2, p0, Lcom/jscape/inet/ftp/FxpFailedEvent;->c:Ljava/lang/Object;

    iput-object p3, p0, Lcom/jscape/inet/ftp/FxpFailedEvent;->d:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/inet/ftp/FxpFailedEvent;->a:Ljava/lang/Exception;

    return-void
.end method


# virtual methods
.method public getDestination()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FxpFailedEvent;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public getException()Ljava/lang/Exception;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FxpFailedEvent;->a:Ljava/lang/Exception;

    return-object v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FxpFailedEvent;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getSource()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FxpFailedEvent;->b:Ljava/lang/Object;

    return-object v0
.end method
