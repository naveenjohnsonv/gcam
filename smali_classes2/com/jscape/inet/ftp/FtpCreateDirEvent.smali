.class public Lcom/jscape/inet/ftp/FtpCreateDirEvent;
.super Lcom/jscape/inet/ftp/FtpEvent;


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field private a:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x1c

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x1

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    const-string v7, "\u007f:r\tpWB:1o\u0012xWT6.e\u0005i\u0018B&|p\u0007i\u001f\u0010\u0012\u001c.e\u0007i\u0012T\u007f8i\u0014x\u0014D0.yF"

    invoke-virtual {v7, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v8, v4

    move v9, v2

    :goto_1
    if-gt v8, v9, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x2f

    if-ge v3, v4, :cond_0

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ftp/FtpCreateDirEvent;->d:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v10, v4, v9

    rem-int/lit8 v11, v9, 0x7

    const/4 v12, 0x5

    const/16 v13, 0x63

    if-eqz v11, :cond_6

    if-eq v11, v6, :cond_5

    if-eq v11, v0, :cond_4

    const/4 v14, 0x3

    if-eq v11, v14, :cond_7

    const/4 v14, 0x4

    if-eq v11, v14, :cond_3

    if-eq v11, v12, :cond_2

    const/16 v12, 0x53

    goto :goto_2

    :cond_2
    const/16 v12, 0x14

    goto :goto_2

    :cond_3
    const/16 v12, 0x7e

    goto :goto_2

    :cond_4
    move v12, v13

    goto :goto_2

    :cond_5
    const/16 v12, 0x3f

    goto :goto_2

    :cond_6
    const/16 v12, 0x3c

    :cond_7
    :goto_2
    xor-int v11, v13, v12

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v4, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/FtpEvent;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ftp/FtpCreateDirEvent;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/ftp/FtpCreateDirEvent;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/inet/ftp/FtpListener;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    invoke-interface {p1, p0}, Lcom/jscape/inet/ftp/FtpListener;->createDir(Lcom/jscape/inet/ftp/FtpCreateDirEvent;)V

    :cond_1
    return-void
.end method

.method public getDirectory()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpCreateDirEvent;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpCreateDirEvent;->c:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftp/FtpCreateDirEvent;->d:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/ftp/FtpCreateDirEvent;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/ftp/FtpCreateDirEvent;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
