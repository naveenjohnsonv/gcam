.class Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionInputStream;
.super Ljava/io/InputStream;


# instance fields
.field private final a:Lcom/jscape/inet/ftp/FtpBaseImplementation;

.field private final b:Lcom/jscape/inet/ftp/FtpConnection;

.field private final c:Ljava/io/InputStream;


# direct methods
.method private constructor <init>(Lcom/jscape/inet/ftp/FtpBaseImplementation;Lcom/jscape/inet/ftp/FtpConnection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionInputStream;->a:Lcom/jscape/inet/ftp/FtpBaseImplementation;

    iput-object p2, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionInputStream;->b:Lcom/jscape/inet/ftp/FtpConnection;

    invoke-interface {p2}, Lcom/jscape/inet/ftp/FtpConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionInputStream;->c:Ljava/io/InputStream;

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftp/FtpBaseImplementation;Lcom/jscape/inet/ftp/FtpConnection;Lcom/jscape/inet/ftp/FtpBaseImplementation$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionInputStream;-><init>(Lcom/jscape/inet/ftp/FtpBaseImplementation;Lcom/jscape/inet/ftp/FtpConnection;)V

    return-void
.end method


# virtual methods
.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionInputStream;->c:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    return v0
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionInputStream;->c:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionInputStream;->b:Lcom/jscape/inet/ftp/FtpConnection;

    invoke-interface {v0}, Lcom/jscape/inet/ftp/FtpConnection;->close()V

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionInputStream;->a:Lcom/jscape/inet/ftp/FtpBaseImplementation;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpBaseImplementation;->readResponse()V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public mark(I)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionInputStream;->c:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->mark(I)V

    return-void
.end method

.method public markSupported()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionInputStream;->c:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    return v0
.end method

.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionInputStream;->c:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    return v0
.end method

.method public read([B)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionInputStream;->c:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->read([B)I

    move-result p1

    return p1
.end method

.method public read([BII)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionInputStream;->c:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result p1

    return p1
.end method

.method public reset()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionInputStream;->c:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    return-void
.end method

.method public skip(J)J
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpBaseImplementation$DataConnectionInputStream;->c:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide p1

    return-wide p1
.end method
