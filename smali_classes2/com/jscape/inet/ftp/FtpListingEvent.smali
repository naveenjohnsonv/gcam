.class public Lcom/jscape/inet/ftp/FtpListingEvent;
.super Lcom/jscape/inet/ftp/FtpEvent;


# instance fields
.field private a:Ljava/util/Enumeration;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/util/Enumeration;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/FtpEvent;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/ftp/FtpListingEvent;->a:Ljava/util/Enumeration;

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/inet/ftp/FtpListener;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    invoke-interface {p1, p0}, Lcom/jscape/inet/ftp/FtpListener;->listing(Lcom/jscape/inet/ftp/FtpListingEvent;)V

    :cond_1
    return-void
.end method

.method public getListing()Ljava/util/Enumeration;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpListingEvent;->a:Ljava/util/Enumeration;

    return-object v0
.end method
