.class public Lcom/jscape/inet/ftp/UnixParser;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ftp/FtpFileParser;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:I = 0xc

.field private static final c:I = 0x7

.field private static final e:[Ljava/lang/String;


# instance fields
.field private d:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x1a

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x3

    const/4 v3, 0x0

    const-string v4, "\u000b-\u0010;hJ\t&9\u00136\u0002\u000f>qc5y(\u0014/0%X<\u0015\u00053;,\u00046\u000f$su\u001a\u001fl#@\u0019>=\u00139\u0012\t0:i;s5\u0013<8,V_\u0002Nw\u000f\u0013\u000e8\'9\u0013u2\u00059\u007f\u000c9Ph\u000b\u000b-\u0010\u007f-V^\u000eZ02\u0003\t#\t\u0003\u0015%\r\u0003\u00075\u001a\u0003\u000b!\u0004\u0003\u0008/\u000b\u0013\u0013\u000e.*9\u0006y4\u00148;i\u001by(\u00145ei\u0003\u000c5\u0011\u0003\u000c!\u0013\u0005\u000b-\u0010\u007f-\u0003\u0000%\u001f\u0010\u000b-\u0010\u007f-Vo?\u0019$\u007f\u0001>,+\r\u0003\u000b!\u000f\u0010\u000b-\u0010\u007f-Vo?\u0019$\u007f\u0001>,+\r\u0006\u000b-\u0010\u007f-\u0012\u000b\u000b-\u0010\u007f-\u00126?\u0019$&\u000b\u000b-\u0010\u007f-V^\u000eZ02\u0003\u0002%\u001e\u0004fMc\u007f\u0005\u000e(g2$\u0003\u000c5\u0013"

    const/16 v5, 0xed

    move v7, v2

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x29

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v14, v11

    move v15, v3

    :goto_2
    const/4 v1, 0x2

    const/4 v12, 0x5

    if-gt v14, v15, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    const/16 v11, 0xa

    if-eqz v13, :cond_1

    add-int/lit8 v1, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v1

    goto :goto_0

    :cond_0
    const-string v4, "*\u001d\"\u0006&\u0000=R\u0000?"

    move v8, v1

    move v7, v2

    move v5, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v13, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v1

    move v8, v13

    :goto_3
    add-int/2addr v6, v10

    add-int v1, v6, v7

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v13, v3

    const/4 v9, 0x4

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ftp/UnixParser;->e:[Ljava/lang/String;

    const/16 v4, 0xc

    new-array v4, v4, [Ljava/lang/String;

    const/16 v5, 0xb

    aget-object v6, v0, v5

    aput-object v6, v4, v3

    const/16 v3, 0xd

    aget-object v3, v0, v3

    aput-object v3, v4, v10

    const/16 v3, 0xf

    aget-object v3, v0, v3

    aput-object v3, v4, v1

    const/16 v1, 0x18

    aget-object v1, v0, v1

    aput-object v1, v4, v2

    const/4 v1, 0x7

    aget-object v2, v0, v1

    const/4 v3, 0x4

    aput-object v2, v4, v3

    const/16 v2, 0x17

    aget-object v2, v0, v2

    aput-object v2, v4, v12

    aget-object v2, v0, v11

    const/4 v3, 0x6

    aput-object v2, v4, v3

    aget-object v2, v0, v3

    aput-object v2, v4, v1

    aget-object v1, v0, v12

    const/16 v2, 0x8

    aput-object v1, v4, v2

    const/16 v1, 0x9

    const/4 v3, 0x4

    aget-object v3, v0, v3

    aput-object v3, v4, v1

    aget-object v1, v0, v2

    aput-object v1, v4, v11

    const/16 v1, 0x14

    aget-object v0, v0, v1

    aput-object v0, v4, v5

    sput-object v4, Lcom/jscape/inet/ftp/UnixParser;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v15

    rem-int/lit8 v3, v15, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v10, :cond_8

    if-eq v3, v1, :cond_7

    const/4 v1, 0x4

    if-eq v3, v2, :cond_6

    if-eq v3, v1, :cond_5

    if-eq v3, v12, :cond_4

    const/16 v3, 0x3f

    goto :goto_4

    :cond_4
    const/16 v3, 0x5f

    goto :goto_4

    :cond_5
    const/16 v3, 0x60

    goto :goto_4

    :cond_6
    const/16 v3, 0x76

    goto :goto_4

    :cond_7
    const/4 v1, 0x4

    const/16 v3, 0x74

    goto :goto_4

    :cond_8
    const/4 v1, 0x4

    const/16 v3, 0x49

    goto :goto_4

    :cond_9
    const/4 v1, 0x4

    const/16 v3, 0x6f

    :goto_4
    xor-int/2addr v3, v9

    xor-int v3, v16, v3

    int-to-char v3, v3

    aput-char v3, v11, v15

    add-int/lit8 v15, v15, 0x1

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    iput-object v0, p0, Lcom/jscape/inet/ftp/UnixParser;->d:Ljava/util/Locale;

    return-void
.end method

.method private a(Ljava/io/StringReader;)C
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1}, Ljava/io/StringReader;->read()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    int-to-char v2, v1

    :try_start_0
    invoke-static {v2}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    if-eqz v0, :cond_1

    if-eqz v2, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    int-to-char v2, v1

    :cond_1
    return v2
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/io/StringReader;Z)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    :cond_0
    invoke-virtual {p1}, Ljava/io/StringReader;->read()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4

    int-to-char v2, v2

    :try_start_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_4

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_3

    :try_start_1
    sget-object v3, Lcom/jscape/inet/ftp/UnixParser;->e:[Ljava/lang/String;

    const/16 v4, 0x15

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v3, :cond_2

    const/4 p1, 0x0

    :try_start_2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result p2

    add-int/lit8 p2, p2, -0x4

    invoke-virtual {v2, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    if-nez v0, :cond_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_0
    return-object v2

    :catch_2
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_4
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Ljava/util/Date;I)Ljava/util/Date;
    .locals 5

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    :try_start_0
    invoke-virtual {v2, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/4 v3, 0x1

    const/4 v4, 0x2

    if-eqz v1, :cond_0

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    if-le v1, v0, :cond_1

    move v4, v3

    :cond_0
    sub-int/2addr p2, v3

    invoke-virtual {v2, v4, p2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    :cond_1
    return-object p1

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private a(Ljava/util/Date;Ljava/util/Calendar;I)Ljava/util/Date;
    .locals 3

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    if-eqz v1, :cond_0

    :try_start_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {p1, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    sub-int/2addr p3, p1

    invoke-virtual {p2, p1, p3}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    return-object p1
.end method

.method private a(Ljava/lang/String;)Z
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/util/StringTokenizer;

    invoke-direct {v2, p1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move p1, v1

    :cond_0
    const/4 v3, 0x5

    if-ge p1, v3, :cond_1

    :try_start_1
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v0, :cond_2

    add-int/lit8 p1, p1, 0x1

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object p1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    :cond_2
    :try_start_3
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    if-eqz v0, :cond_3

    const/4 v2, 0x3

    if-ne p1, v2, :cond_5

    :try_start_4
    invoke-direct {p0, v3}, Lcom/jscape/inet/ftp/UnixParser;->b(Ljava/lang/String;)Z

    move-result p1

    :cond_3
    if-eqz v0, :cond_4

    if-eqz p1, :cond_5

    const/4 p1, 0x1

    :cond_4
    move v1, p1

    :cond_5
    return v1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    return v1
.end method

.method private b(Ljava/io/StringReader;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/io/StringReader;)C

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_0
    invoke-virtual {p1}, Ljava/io/StringReader;->read()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    int-to-char v2, v2

    :try_start_0
    invoke-static {v2}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private b(Ljava/lang/String;)Z
    .locals 7

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/jscape/inet/ftp/UnixParser;->e:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    new-instance v4, Ljava/text/SimpleDateFormat;

    iget-object v5, p0, Lcom/jscape/inet/ftp/UnixParser;->d:Ljava/util/Locale;

    invoke-direct {v4, v2, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    const/4 v2, 0x5

    const/4 v5, 0x1

    invoke-virtual {v0, v2, v5}, Ljava/util/Calendar;->set(II)V

    move v2, v3

    :cond_0
    const/16 v6, 0xc

    if-ge v2, v6, :cond_4

    const/4 v6, 0x2

    invoke-virtual {v0, v6, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    if-eqz v1, :cond_3

    :try_start_0
    invoke-virtual {p1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_2

    if-eqz v6, :cond_1

    return v5

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v3, v6

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_0
    if-nez v1, :cond_0

    :cond_4
    :goto_1
    return v3
.end method

.method private c(Ljava/io/StringReader;)J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/io/StringReader;)C

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_0
    invoke-virtual {p1}, Ljava/io/StringReader;->read()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    int-to-char v4, v2

    :try_start_0
    invoke-static {v4}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_1

    if-eqz v1, :cond_1

    if-nez v5, :cond_2

    :try_start_1
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    if-nez v1, :cond_0

    goto :goto_0

    :cond_1
    move v2, v5

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    if-eq v2, v3, :cond_3

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0

    :cond_3
    :try_start_4
    new-instance p1, Ljava/lang/Exception;

    sget-object v0, Lcom/jscape/inet/ftp/UnixParser;->e:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private c(Ljava/lang/String;)Z
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/StringTokenizer;

    const-string v2, " "

    const/4 v3, 0x0

    invoke-direct {v1, p1, v2, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    :try_start_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->countTokens()I

    move-result p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    if-lt p1, v0, :cond_1

    const/4 p1, 0x1

    :cond_0
    move v3, p1

    :cond_1
    return v3

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private d(Ljava/io/StringReader;)Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/UnixParser;->b(Ljava/io/StringReader;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/UnixParser;->b(Ljava/io/StringReader;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftp/UnixParser;->b(Ljava/io/StringReader;)Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-direct {p0, v0}, Lcom/jscape/inet/ftp/UnixParser;->e(Ljava/lang/String;)I

    move-result v0

    const/4 v4, 0x2

    invoke-virtual {v3, v4, v0}, Ljava/util/Calendar;->set(II)V

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/4 v2, 0x5

    invoke-virtual {v3, v2, v0}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0x3a

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v2, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v3, v4, p1}, Ljava/util/Calendar;->set(II)V

    sget-object p1, Lcom/jscape/inet/ftp/UnixParser;->e:[Ljava/lang/String;

    const/16 v0, 0x12

    aget-object p1, p1, v0

    move-object v7, p1

    move v8, v4

    goto :goto_0

    :cond_0
    const/16 v1, 0xb

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v1, v5}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xc

    add-int/2addr v0, v4

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    invoke-virtual {v3, v1, p1}, Ljava/util/Calendar;->set(II)V

    :cond_1
    sget-object p1, Lcom/jscape/inet/ftp/UnixParser;->e:[Ljava/lang/String;

    const/16 v0, 0x19

    aget-object p1, p1, v0

    move-object v7, p1

    move v8, v2

    :goto_0
    new-instance p1, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    const/4 v9, 0x0

    move-object v4, p1

    move-object v5, p0

    invoke-direct/range {v4 .. v9}, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;-><init>(Lcom/jscape/inet/ftp/UnixParser;Ljava/util/Date;Ljava/lang/String;ZLcom/jscape/inet/ftp/UnixParser$1;)V

    return-object p1
.end method

.method private d(Ljava/lang/String;)Ljava/lang/Long;
    .locals 0

    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private e(Ljava/lang/String;)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    sget-object v2, Lcom/jscape/inet/ftp/UnixParser;->a:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_2

    :try_start_0
    aget-object v2, v2, v1

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_1
    return v1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    new-instance v0, Ljava/lang/Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftp/UnixParser;->e:[Ljava/lang/String;

    const/16 v3, 0x9

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private f(Ljava/lang/String;)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    const-string v1, "d"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_2

    if-nez v1, :cond_1

    :try_start_1
    const-string v1, "l"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v0, :cond_2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move p1, v1

    :goto_1
    return p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private g(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "l"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method


# virtual methods
.method public getDateTime(Lcom/jscape/inet/ftp/FtpFile;)Ljava/util/Date;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0}, Lcom/jscape/inet/ftp/UnixParser;->getDateTime(Lcom/jscape/inet/ftp/FtpFile;Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    return-object p1
.end method

.method public getDateTime(Lcom/jscape/inet/ftp/FtpFile;Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Date;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const/4 v2, 0x0

    if-eqz p3, :cond_0

    :try_start_0
    new-instance v3, Ljava/text/SimpleDateFormat;

    sget-object v4, Lcom/jscape/inet/ftp/UnixParser;->e:[Ljava/lang/String;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    invoke-direct {v3, v4, p3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    :goto_0
    move-object v2, v3

    goto :goto_1

    :cond_0
    new-instance v3, Ljava/text/SimpleDateFormat;

    sget-object v4, Lcom/jscape/inet/ftp/UnixParser;->e:[Ljava/lang/String;

    const/16 v5, 0x13

    aget-object v4, v4, v5

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v3, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :goto_1
    if-eqz p2, :cond_1

    :try_start_1
    invoke-virtual {v2, p2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception p2

    :try_start_2
    invoke-static {p2}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p2

    throw p2

    :cond_1
    :goto_2
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getDate()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getTime()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p2

    new-instance v3, Ljava/util/GregorianCalendar;

    invoke-direct {v3}, Ljava/util/GregorianCalendar;-><init>()V

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v3, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p2

    invoke-direct {p0, p2, v3, v5}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/util/Date;Ljava/util/Calendar;I)Ljava/util/Date;

    move-result-object p1
    :try_end_2
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4

    :catch_1
    if-eqz v0, :cond_4

    if-eqz p3, :cond_3

    new-instance p2, Ljava/text/SimpleDateFormat;

    sget-object v2, Lcom/jscape/inet/ftp/UnixParser;->e:[Ljava/lang/String;

    const/16 v3, 0xe

    aget-object v2, v2, v3

    invoke-direct {p2, v2, p3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getTime()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p2

    if-nez v0, :cond_2

    goto :goto_3

    :cond_2
    move-object p1, p2

    goto :goto_4

    :cond_3
    :goto_3
    new-instance v2, Ljava/text/SimpleDateFormat;

    sget-object p2, Lcom/jscape/inet/ftp/UnixParser;->e:[Ljava/lang/String;

    const/16 p3, 0x10

    aget-object p2, p2, p3

    sget-object p3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, p2, p3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    :cond_4
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getDate()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getTime()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    :goto_4
    return-object p1
.end method

.method public getFileDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const/4 v0, 0x1

    :try_start_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    sget-object v2, Lcom/jscape/inet/ftp/UnixParser;->e:[Ljava/lang/String;

    const/16 v3, 0xc

    aget-object v2, v2, v3

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-direct {p0, v3, v2, v1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/util/Date;Ljava/util/Calendar;I)Ljava/util/Date;

    move-result-object p1
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    sget-object v2, Lcom/jscape/inet/ftp/UnixParser;->e:[Ljava/lang/String;

    const/16 v3, 0x11

    aget-object v2, v2, v3

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object p1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result p1

    invoke-virtual {v1, v0, p1}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-direct {p0, v0, v1, p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/util/Date;Ljava/util/Calendar;I)Ljava/util/Date;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public parse(Ljava/io/BufferedReader;)Ljava/util/Enumeration;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v1, :cond_0

    if-nez v2, :cond_0

    :try_start_0
    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    sget-object v3, Lcom/jscape/inet/ftp/UnixParser;->e:[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v0, Lcom/jscape/inet/ftp/LiaisonVANParser;

    invoke-direct {v0}, Lcom/jscape/inet/ftp/LiaisonVANParser;-><init>()V

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/LiaisonVANParser;->parse(Ljava/io/BufferedReader;)Ljava/util/Enumeration;

    move-result-object p1

    return-object p1

    :cond_1
    move-object v3, p0

    :goto_0
    invoke-virtual {v3, v2}, Lcom/jscape/inet/ftp/UnixParser;->parseLine(Ljava/lang/String;)Lcom/jscape/inet/ftp/FtpFile;

    move-result-object v2

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    :goto_1
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    return-object p1

    :cond_3
    :goto_2
    invoke-virtual {v0, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_1

    :cond_4
    move-object v2, v4

    goto :goto_0
.end method

.method public parseLine(Ljava/lang/String;)Lcom/jscape/inet/ftp/FtpFile;
    .locals 20

    move-object/from16 v1, p0

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    :try_start_0
    invoke-direct/range {p0 .. p1}, Lcom/jscape/inet/ftp/UnixParser;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    return-object v2

    :cond_0
    new-instance v3, Ljava/io/StringReader;

    move-object/from16 v15, p1

    invoke-direct {v3, v15}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v3}, Lcom/jscape/inet/ftp/UnixParser;->b(Ljava/io/StringReader;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v1, v3}, Lcom/jscape/inet/ftp/UnixParser;->b(Ljava/io/StringReader;)Ljava/lang/String;

    invoke-direct {v1, v3}, Lcom/jscape/inet/ftp/UnixParser;->b(Ljava/io/StringReader;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v3}, Lcom/jscape/inet/ftp/UnixParser;->b(Ljava/io/StringReader;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3}, Lcom/jscape/inet/ftp/UnixParser;->b(Ljava/io/StringReader;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/jscape/inet/ftp/UnixParser;->d(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    if-eqz v6, :cond_1

    move-object v9, v4

    goto :goto_0

    :cond_1
    const-string v7, ""

    move-object v9, v7

    :goto_0
    if-eqz v0, :cond_3

    if-eqz v6, :cond_2

    goto :goto_1

    :cond_2
    invoke-direct {v1, v4}, Lcom/jscape/inet/ftp/UnixParser;->d(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    goto :goto_2

    :cond_3
    :goto_1
    move-object v0, v6

    :goto_2
    if-eqz v6, :cond_4

    goto :goto_3

    :cond_4
    new-instance v4, Ljava/io/StringReader;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v3}, Lcom/jscape/util/X;->c(Ljava/io/Reader;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    move-object v3, v4

    :goto_3
    invoke-direct {v1, v3}, Lcom/jscape/inet/ftp/UnixParser;->d(Ljava/io/StringReader;)Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;

    move-result-object v4

    invoke-direct {v1, v10}, Lcom/jscape/inet/ftp/UnixParser;->g(Ljava/lang/String;)Z

    move-result v5

    invoke-direct {v1, v3, v5}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/io/StringReader;Z)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v1, v3, v6}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/io/StringReader;Z)Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    new-instance v19, Lcom/jscape/inet/ftp/FtpFile;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    :cond_5
    const-wide/16 v6, 0x0

    :goto_4
    :try_start_2
    invoke-virtual {v4}, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;->getFormattedDateAsString()Ljava/lang/String;

    move-result-object v11

    sget-object v0, Lcom/jscape/inet/ftp/UnixParser;->e:[Ljava/lang/String;

    const/16 v12, 0x16

    aget-object v0, v0, v12

    invoke-virtual {v4, v0}, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;->getFormattedDateAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v1, v10}, Lcom/jscape/inet/ftp/UnixParser;->f(Ljava/lang/String;)Z

    move-result v13

    invoke-direct {v1, v10}, Lcom/jscape/inet/ftp/UnixParser;->g(Ljava/lang/String;)Z

    move-result v14

    const/16 v16, 0x0

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/UnixParser$UnixParserDate;->isYearSet()Z

    move-result v18

    move-object/from16 v4, v19

    move-object v15, v3

    move-object/from16 v17, p1

    invoke-direct/range {v4 .. v18}, Lcom/jscape/inet/ftp/FtpFile;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ILjava/lang/String;Z)V

    return-object v19

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftp/UnixParser;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    return-object v2
.end method

.method public setLocale(Ljava/util/Locale;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftp/UnixParser;->d:Ljava/util/Locale;

    return-void
.end method
