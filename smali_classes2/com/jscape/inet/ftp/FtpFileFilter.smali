.class public Lcom/jscape/inet/ftp/FtpFileFilter;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String; = "*"


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, "*"

    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/jscape/inet/ftp/FtpFileFilter;->b:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ftp/FtpFileFilter;->b:Ljava/lang/String;

    const-string v2, "*"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eqz v0, :cond_4

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    iget-object v2, p0, Lcom/jscape/inet/ftp/FtpFileFilter;->b:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/jscape/inet/ftp/FtpFileFilter;->b:Ljava/lang/String;

    const/4 v5, 0x1

    add-int/2addr v1, v5

    invoke-virtual {v4, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v0, :cond_0

    if-eqz v2, :cond_2

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    :cond_0
    if-eqz v0, :cond_1

    if-eqz v2, :cond_2

    move v3, v5

    goto :goto_0

    :cond_1
    move v3, v2

    :cond_2
    :goto_0
    return v3

    :cond_3
    iget-object v0, p0, Lcom/jscape/inet/ftp/FtpFileFilter;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    :cond_4
    return v1
.end method


# virtual methods
.method public applyTo(Ljava/util/Vector;)Ljava/util/Vector;
    .locals 5

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftp/FtpEvent;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    :cond_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    if-eqz v1, :cond_4

    invoke-virtual {p1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/ftp/FtpFile;

    if-eqz v1, :cond_2

    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/jscape/inet/ftp/FtpFileFilter;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_1
    add-int/lit8 v2, v2, 0x1

    :cond_2
    if-nez v1, :cond_0

    :cond_3
    move-object p1, v0

    :cond_4
    return-object p1
.end method
