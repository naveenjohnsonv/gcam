.class final Lcom/jscape/inet/ftps/Ftps$DataConnectionOutputStream;
.super Ljava/io/OutputStream;


# instance fields
.field private final a:Lcom/jscape/inet/ftps/Ftps;

.field private final b:Ljava/io/OutputStream;

.field private final c:Ljava/net/Socket;


# direct methods
.method private constructor <init>(Lcom/jscape/inet/ftps/Ftps;Ljava/net/Socket;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$DataConnectionOutputStream;->a:Lcom/jscape/inet/ftps/Ftps;

    iput-object p2, p0, Lcom/jscape/inet/ftps/Ftps$DataConnectionOutputStream;->c:Ljava/net/Socket;

    invoke-virtual {p2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$DataConnectionOutputStream;->b:Ljava/io/OutputStream;

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftps/Ftps;Ljava/net/Socket;Lcom/jscape/inet/ftps/Ftps$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ftps/Ftps$DataConnectionOutputStream;-><init>(Lcom/jscape/inet/ftps/Ftps;Ljava/net/Socket;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps$DataConnectionOutputStream;->b:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps$DataConnectionOutputStream;->c:Ljava/net/Socket;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps$DataConnectionOutputStream;->a:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->readResponse()Ljava/lang/String;
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps$DataConnectionOutputStream;->b:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

.method public write(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps$DataConnectionOutputStream;->b:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method public write([B)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps$DataConnectionOutputStream;->b:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public write([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps$DataConnectionOutputStream;->b:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    return-void
.end method
