.class final Lcom/jscape/inet/ftps/FtpsProtocolInterpreter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ftp/ClientProtocolInterpreter;


# instance fields
.field private a:Lcom/jscape/inet/ftps/FtpsClient;


# direct methods
.method constructor <init>(Lcom/jscape/inet/ftps/FtpsClient;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsProtocolInterpreter;->a:Lcom/jscape/inet/ftps/FtpsClient;

    return-void
.end method


# virtual methods
.method public receiveReply()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsProtocolInterpreter;->a:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient;->readResponse()Lcom/jscape/inet/ftps/Response;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Response;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public sendCommand(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsProtocolInterpreter;->a:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendCommand(Ljava/lang/String;)V

    return-void
.end method
