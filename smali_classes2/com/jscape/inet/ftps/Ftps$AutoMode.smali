.class Lcom/jscape/inet/ftps/Ftps$AutoMode;
.super Lcom/jscape/inet/ftps/Ftps$TransferMode;


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field final c:Lcom/jscape/inet/ftps/Ftps;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "g}/D"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ftps/Ftps$AutoMode;->d:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/4 v5, 0x5

    if-eqz v4, :cond_6

    const/4 v6, 0x1

    if-eq v4, v6, :cond_5

    const/4 v6, 0x2

    if-eq v4, v6, :cond_4

    const/4 v6, 0x3

    if-eq v4, v6, :cond_3

    const/4 v6, 0x4

    if-eq v4, v6, :cond_2

    if-eq v4, v5, :cond_1

    goto :goto_1

    :cond_1
    const/16 v5, 0x59

    goto :goto_1

    :cond_2
    const/16 v5, 0xf

    goto :goto_1

    :cond_3
    const/16 v5, 0x40

    goto :goto_1

    :cond_4
    const/16 v5, 0x27

    goto :goto_1

    :cond_5
    const/16 v5, 0x68

    goto :goto_1

    :cond_6
    const/16 v5, 0x63

    :goto_1
    const/16 v4, 0x70

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Lcom/jscape/inet/ftps/Ftps;)V
    .locals 2

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$AutoMode;->c:Lcom/jscape/inet/ftps/Ftps;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/jscape/inet/ftps/Ftps$TransferMode;-><init>(Lcom/jscape/inet/ftps/Ftps;ILcom/jscape/inet/ftps/Ftps$1;)V

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps$AutoMode;-><init>(Lcom/jscape/inet/ftps/Ftps;)V

    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/jscape/inet/ftps/Ftps$TransferMode;
    .locals 2

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps$AutoMode;->b(Ljava/lang/String;)Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    new-instance p1, Lcom/jscape/inet/ftps/Ftps$TextMode;

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps$AutoMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-direct {p1, v1, v0}, Lcom/jscape/inet/ftps/Ftps$TextMode;-><init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps$1;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/jscape/inet/ftps/Ftps$BinaryMode;

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps$AutoMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-direct {p1, v1, v0}, Lcom/jscape/inet/ftps/Ftps$BinaryMode;-><init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps$1;)V

    :goto_0
    return-object p1
.end method

.method private b(Ljava/lang/String;)Z
    .locals 1

    new-instance v0, Ljavax/activation/MimetypesFileTypeMap;

    invoke-direct {v0}, Ljavax/activation/MimetypesFileTypeMap;-><init>()V

    invoke-virtual {v0, p1}, Ljavax/activation/MimetypesFileTypeMap;->getContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :try_start_0
    new-instance v0, Ljavax/activation/MimeType;

    invoke-direct {v0, p1}, Ljavax/activation/MimeType;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljavax/activation/MimeType;->getPrimaryType()Ljava/lang/String;

    move-result-object p1

    sget-object v0, Lcom/jscape/inet/ftps/Ftps$AutoMode;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1
    :try_end_0
    .catch Ljavax/activation/MimeTypeParseException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public transmit(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;IJJ)J
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object v0, p0

    move-object v4, p3

    invoke-direct {p0, p3}, Lcom/jscape/inet/ftps/Ftps$AutoMode;->a(Ljava/lang/String;)Lcom/jscape/inet/ftps/Ftps$TransferMode;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move v5, p4

    move-wide v6, p5

    move-wide/from16 v8, p7

    invoke-virtual/range {v1 .. v9}, Lcom/jscape/inet/ftps/Ftps$TransferMode;->transmit(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;IJJ)J

    move-result-wide v1

    return-wide v1
.end method
