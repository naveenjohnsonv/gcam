.class Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ftps/FtpsClient$DataConnector;


# static fields
.field private static final g:[Ljava/lang/String;


# instance fields
.field private a:Lcom/jscape/inet/util/ConnectionParameters;

.field private final b:Z

.field private c:Ljava/lang/String;

.field private d:Lcom/jscape/inet/ftps/FtpsClient;

.field private e:Z

.field final f:Lcom/jscape/inet/ftps/FtpsClient;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x1f

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x19

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\nkT\u00197\u0011~P\"\u0000?6\u001acCk\u0007/0\u0005hVk\u0015.&\u0001hW8T\u001aq%\u0015(.\u0016-P$T)-\u001dcA(\u0000j6\u001c-L$\u0007>b\u001aq%\u0015(.\u0016-P$T)-\u001dcA(\u0000j6\u001c-L$\u0007>b"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x55

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->g:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v13, 0x2

    if-eq v12, v13, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x14

    goto :goto_2

    :cond_2
    const/16 v12, 0x6a

    goto :goto_2

    :cond_3
    const/16 v12, 0x5b

    goto :goto_2

    :cond_4
    const/16 v12, 0x53

    goto :goto_2

    :cond_5
    const/16 v12, 0x6d

    goto :goto_2

    :cond_6
    const/16 v12, 0x52

    goto :goto_2

    :cond_7
    const/16 v12, 0x3d

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Lcom/jscape/inet/ftps/FtpsClient;Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;IIZZZ)V
    .locals 2

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->f:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->e:Z

    iput-object p2, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->d:Lcom/jscape/inet/ftps/FtpsClient;

    new-instance p1, Lcom/jscape/inet/util/ConnectionParameters;

    int-to-long v0, p6

    invoke-direct {p1, p3, p5, v0, v1}, Lcom/jscape/inet/util/ConnectionParameters;-><init>(Ljava/lang/String;IJ)V

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-static {p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jscape/inet/util/ConnectionParameters;->setTcpNoDelay(Ljava/lang/Boolean;)V

    iput-boolean p8, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->b:Z

    iput-object p4, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->c:Ljava/lang/String;

    iput-boolean p9, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->e:Z

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftps/FtpsClient;Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;IIZZZLcom/jscape/inet/ftps/FtpsClient$1;)V
    .locals 0

    invoke-direct/range {p0 .. p9}, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;-><init>(Lcom/jscape/inet/ftps/FtpsClient;Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;IIZZZ)V

    return-void
.end method

.method private constructor <init>(Lcom/jscape/inet/ftps/FtpsClient;Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;IIZZZLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    move-object v0, p0

    move-object v1, p1

    iput-object v1, v0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->f:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->e:Z

    move-object v1, p2

    iput-object v1, v0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->d:Lcom/jscape/inet/ftps/FtpsClient;

    new-instance v11, Lcom/jscape/inet/util/ConnectionParameters;

    move/from16 v1, p6

    int-to-long v4, v1

    move-object v1, v11

    move-object v2, p3

    move/from16 v3, p5

    move-object/from16 v6, p10

    move/from16 v7, p11

    move-object/from16 v8, p12

    move-object/from16 v9, p13

    move-object/from16 v10, p14

    invoke-direct/range {v1 .. v10}, Lcom/jscape/inet/util/ConnectionParameters;-><init>(Ljava/lang/String;IJLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v11, v0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-static/range {p7 .. p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v11, v1}, Lcom/jscape/inet/util/ConnectionParameters;->setTcpNoDelay(Ljava/lang/Boolean;)V

    move/from16 v1, p8

    iput-boolean v1, v0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->b:Z

    move-object/from16 v1, p4

    iput-object v1, v0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->c:Ljava/lang/String;

    move/from16 v1, p9

    iput-boolean v1, v0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->e:Z

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftps/FtpsClient;Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;IIZZZLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ftps/FtpsClient$1;)V
    .locals 0

    invoke-direct/range {p0 .. p14}, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;-><init>(Lcom/jscape/inet/ftps/FtpsClient;Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;IIZZZLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a()Ljava/net/Socket;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a:Lcom/jscape/inet/util/ConnectionParameters;

    iget-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->f:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-static {v2}, Lcom/jscape/inet/ftps/FtpsClient;->a(Lcom/jscape/inet/ftps/FtpsClient;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jscape/inet/util/ConnectionParameters;->setSendBuffer(I)V

    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a:Lcom/jscape/inet/util/ConnectionParameters;

    iget-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->f:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-static {v2}, Lcom/jscape/inet/ftps/FtpsClient;->b(Lcom/jscape/inet/ftps/FtpsClient;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jscape/inet/util/ConnectionParameters;->setReceiveBuffer(I)V

    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->createSocket()Ljava/net/Socket;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v1

    :try_start_1
    iget-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/util/ConnectionParameters;->getHost()Ljava/lang/String;

    move-result-object v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    if-eqz v0, :cond_1

    :try_start_2
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->c:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->f:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->c(Lcom/jscape/inet/ftps/FtpsClient;)Ljava/util/logging/Logger;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->g:[Ljava/lang/String;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v3}, Lcom/jscape/inet/util/ConnectionParameters;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->g:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->d:Lcom/jscape/inet/ftps/FtpsClient;

    iget-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/jscape/inet/ftps/FtpsClient;->a(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->g:[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v3}, Lcom/jscape/inet/util/ConnectionParameters;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_1
    :goto_0
    :try_start_3
    new-instance v0, Lcom/jscape/inet/util/ConnectionParameters;

    iget-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v3}, Lcom/jscape/inet/util/ConnectionParameters;->getPort()I

    move-result v3

    iget-object v4, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v4}, Lcom/jscape/inet/util/ConnectionParameters;->getConnectionTimeout()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/jscape/inet/util/ConnectionParameters;-><init>(Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a:Lcom/jscape/inet/util/ConnectionParameters;

    iget-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->f:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-static {v2}, Lcom/jscape/inet/ftps/FtpsClient;->a(Lcom/jscape/inet/ftps/FtpsClient;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/jscape/inet/util/ConnectionParameters;->setSendBuffer(I)V

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a:Lcom/jscape/inet/util/ConnectionParameters;

    iget-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->f:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-static {v2}, Lcom/jscape/inet/ftps/FtpsClient;->b(Lcom/jscape/inet/ftps/FtpsClient;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/jscape/inet/util/ConnectionParameters;->setReceiveBuffer(I)V

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v0}, Lcom/jscape/inet/util/ConnectionParameters;->createSocket()Ljava/net/Socket;

    move-result-object v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    return-object v0

    :catch_1
    move-exception v0

    new-instance v2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catch_2
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public close()V
    .locals 0

    return-void
.end method

.method public openConnection(Ljava/lang/String;)Ljava/net/Socket;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->e:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->f:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v1, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendCommand(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a()Ljava/net/Socket;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->f:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-static {v2, v1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Lcom/jscape/inet/ftps/FtpsClient;Ljava/net/Socket;)V

    iget-boolean v2, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->b:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_2

    if-eqz v2, :cond_1

    :try_start_3
    iget-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->f:Lcom/jscape/inet/ftps/FtpsClient;

    const/4 v3, 0x1

    invoke-static {v2, v1, v3}, Lcom/jscape/inet/ftps/FtpsClient;->a(Lcom/jscape/inet/ftps/FtpsClient;Ljava/net/Socket;Z)Ljava/net/Socket;

    move-result-object v1

    check-cast v1, Ljavax/net/ssl/SSLSocket;

    iget-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->f:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-static {v2, v1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Lcom/jscape/inet/ftps/FtpsClient;Ljavax/net/ssl/SSLSocket;)V

    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocket;->startHandshake()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    if-eqz v0, :cond_3

    :try_start_4
    iget-boolean v2, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->e:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    if-eqz v2, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->f:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendCommand(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_4
    iget-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->f:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {p1}, Lcom/jscape/inet/ftps/FtpsClient;->readResponse()Lcom/jscape/inet/ftps/Response;

    return-object v1

    :catch_2
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_3
    move-exception p1

    :try_start_7
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :goto_2
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->f:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient;->readResponse()Lcom/jscape/inet/ftps/Response;

    throw p1
.end method

.method public setConnectBeforeCommand(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->e:Z

    return-void
.end method
