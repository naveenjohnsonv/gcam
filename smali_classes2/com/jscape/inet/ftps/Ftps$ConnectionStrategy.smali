.class public interface abstract Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;
.super Ljava/lang/Object;


# virtual methods
.method public abstract authenticate(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method

.method public abstract createClient(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/util/ConnectionParameters;Ljavax/net/ssl/SSLContext;[Ljava/lang/String;Ljava/util/logging/Logger;)Lcom/jscape/inet/ftps/FtpsClient;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation
.end method
