.class Lcom/jscape/inet/ftps/Ftps$TextMode;
.super Lcom/jscape/inet/ftps/Ftps$TransferMode;


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field final c:Lcom/jscape/inet/ftps/Ftps;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xe

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x1f

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "mp\u001d\\\u001b>6qx\u0001XA\"!\u0002\u000c\u0013\u0002\u000c\u0013"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x14

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ftps/Ftps$TextMode;->d:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v13, 0x2

    if-eq v12, v13, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x4c

    goto :goto_2

    :cond_2
    const/16 v12, 0x52

    goto :goto_2

    :cond_3
    const/16 v12, 0x2a

    goto :goto_2

    :cond_4
    const/16 v12, 0x26

    goto :goto_2

    :cond_5
    const/16 v12, 0x6c

    goto :goto_2

    :cond_6
    const/4 v12, 0x6

    goto :goto_2

    :cond_7
    const/16 v12, 0x1e

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Lcom/jscape/inet/ftps/Ftps;)V
    .locals 2

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/jscape/inet/ftps/Ftps$TransferMode;-><init>(Lcom/jscape/inet/ftps/Ftps;ILcom/jscape/inet/ftps/Ftps$1;)V

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps$TextMode;-><init>(Lcom/jscape/inet/ftps/Ftps;)V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public transmit(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;IJJ)J
    .locals 29
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v13, p0

    move-object/from16 v0, p2

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v14

    const-wide/16 v15, 0x0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->getProperties()Ljava/util/Properties;

    move-result-object v1

    sget-object v2, Lcom/jscape/inet/ftps/Ftps$TextMode;->d:[Ljava/lang/String;

    const/4 v11, 0x0

    aget-object v3, v2, v11

    invoke-virtual {v1, v3}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez p4, :cond_0

    sget-object v3, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    const/4 v3, 0x1

    aget-object v2, v2, v3

    sget-object v3, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    move-object v12, v1

    move-object v9, v2

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/jscape/inet/ftps/Ftps$TextMode;->d:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    sget-object v3, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    sget-object v3, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_b

    move-object v9, v1

    move-object v12, v2

    :goto_0
    cmp-long v1, p5, v15

    if-eqz v14, :cond_1

    if-nez v1, :cond_1

    :try_start_1
    iget-object v1, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;

    new-instance v2, Lcom/jscape/inet/ftp/FtpProgressEvent;

    iget-object v3, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;

    iget-object v4, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-static {v4}, Lcom/jscape/inet/ftps/Ftps;->d(Lcom/jscape/inet/ftps/Ftps;)Ljava/lang/String;

    move-result-object v20

    iget-object v4, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-static {v4}, Lcom/jscape/inet/ftps/Ftps;->d(Lcom/jscape/inet/ftps/Ftps;)Ljava/lang/String;

    move-result-object v21

    add-long v23, p7, v15

    const-wide/16 v25, 0x0

    move-object/from16 v17, v2

    move-object/from16 v18, v3

    move-object/from16 v19, p3

    move/from16 v22, p4

    move-wide/from16 v27, p5

    invoke-direct/range {v17 .. v28}, Lcom/jscape/inet/ftp/FtpProgressEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJ)V

    invoke-virtual {v1, v2}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$TextMode;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    :goto_1
    iget-object v1, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->b(Lcom/jscape/inet/ftps/Ftps;)I

    move-result v1

    int-to-long v1, v1

    add-long/2addr v1, v15

    new-instance v10, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v10}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const-string v3, ""
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b

    :try_start_3
    iget-object v4, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-static {v4}, Lcom/jscape/inet/ftps/Ftps;->c(Lcom/jscape/inet/ftps/Ftps;)Ljava/io/File;

    move-result-object v4
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9

    if-eqz v14, :cond_3

    if-eqz v4, :cond_2

    :try_start_4
    iget-object v3, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-static {v3}, Lcom/jscape/inet/ftps/Ftps;->c(Lcom/jscape/inet/ftps/Ftps;)Ljava/io/File;

    move-result-object v4
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_a

    goto :goto_3

    :cond_2
    :goto_2
    move-object/from16 v17, v3

    move v3, v11

    move-wide v4, v15

    goto :goto_4

    :cond_3
    :goto_3
    :try_start_5
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :goto_4
    invoke-virtual/range {p1 .. p1}, Ljava/io/InputStream;->read()I

    move-result v6
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_b

    move v7, v6

    :goto_5
    const/4 v8, -0x1

    if-eq v6, v8, :cond_15

    :try_start_6
    iget-object v6, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-static {v6}, Lcom/jscape/inet/ftps/Ftps;->e(Lcom/jscape/inet/ftps/Ftps;)Z

    move-result v6
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7

    if-eqz v14, :cond_4

    if-nez v6, :cond_15

    move v6, v7

    :cond_4
    if-eqz v14, :cond_9

    :try_start_7
    aget-byte v8, v12, v3
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    if-ne v6, v8, :cond_8

    add-int/lit8 v3, v3, 0x1

    if-eqz v14, :cond_7

    :try_start_8
    array-length v6, v12
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    if-ne v3, v6, :cond_6

    :try_start_9
    invoke-virtual {v10, v9}, Ljava/io/ByteArrayOutputStream;->write([B)V

    array-length v3, v9
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_b

    move-object/from16 v19, v12

    int-to-long v11, v3

    add-long/2addr v4, v11

    if-nez v14, :cond_5

    const/4 v3, 0x0

    goto :goto_6

    :cond_5
    move-object/from16 v11, v19

    const/4 v3, 0x0

    goto/16 :goto_b

    :cond_6
    move-object v11, v12

    goto/16 :goto_b

    :cond_7
    move v6, v3

    move/from16 v19, v6

    move-wide/from16 v20, v4

    move-object v11, v12

    goto/16 :goto_c

    :cond_8
    move-object/from16 v19, v12

    :goto_6
    move v6, v3

    goto :goto_7

    :catch_1
    move-exception v0

    move-object v1, v0

    :try_start_a
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps$TextMode;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    :catch_2
    move-exception v0

    :try_start_b
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$TextMode;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_b

    :cond_9
    move-object/from16 v19, v12

    :goto_7
    move-object/from16 v11, v19

    if-eqz v14, :cond_c

    if-lez v6, :cond_b

    const/4 v12, 0x0

    :try_start_c
    invoke-virtual {v10, v11, v12, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3

    int-to-long v12, v3

    add-long/2addr v4, v12

    const/4 v3, 0x0

    if-nez v14, :cond_a

    goto :goto_8

    :cond_a
    move-object/from16 v13, p0

    goto :goto_a

    :catch_3
    move-exception v0

    move-object/from16 v13, p0

    goto/16 :goto_12

    :cond_b
    :goto_8
    move/from16 v6, p4

    :cond_c
    move-object/from16 v13, p0

    if-eqz v14, :cond_e

    if-nez v6, :cond_d

    :try_start_d
    iget-object v6, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v6}, Lcom/jscape/inet/ftps/Ftps;->getDiskEncoding()Ljava/lang/String;

    move-result-object v6

    iget-object v8, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v8}, Lcom/jscape/inet/ftps/Ftps;->getWireEncoding()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v6, v8}, Lcom/jscape/inet/ftps/Ftps;->b(ILjava/lang/String;Ljava/lang/String;)I

    move-result v7
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_b

    if-nez v14, :cond_f

    :cond_d
    :try_start_e
    iget-object v6, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v6}, Lcom/jscape/inet/ftps/Ftps;->getWireEncoding()Ljava/lang/String;

    move-result-object v6

    iget-object v8, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v8}, Lcom/jscape/inet/ftps/Ftps;->getDiskEncoding()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v6, v8}, Lcom/jscape/inet/ftps/Ftps;->b(ILjava/lang/String;Ljava/lang/String;)I

    move-result v6
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_4

    goto :goto_9

    :catch_4
    move-exception v0

    :try_start_f
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$TextMode;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_e
    :goto_9
    move v7, v6

    :cond_f
    :goto_a
    invoke-virtual {v10, v7}, Ljava/io/ByteArrayOutputStream;->write(I)V

    const-wide/16 v19, 0x1

    add-long v4, v4, v19

    :goto_b
    cmp-long v6, v4, v1

    move/from16 v19, v3

    move-wide/from16 v20, v4

    :goto_c
    if-eqz v14, :cond_14

    if-ltz v6, :cond_13

    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_b

    :try_start_10
    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual/range {p2 .. p2}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->reset()V

    iget-object v4, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_5

    if-eqz v14, :cond_11

    :try_start_11
    invoke-static {v4}, Lcom/jscape/inet/ftps/Ftps;->f(Lcom/jscape/inet/ftps/Ftps;)Z

    move-result v4

    if-eqz v4, :cond_10

    iget-object v4, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_6

    goto :goto_d

    :cond_10
    move-object/from16 v22, v9

    move-object/from16 v16, v10

    move-object/from16 v18, v11

    const/16 v23, 0x0

    goto :goto_e

    :cond_11
    :goto_d
    move-object v12, v4

    :try_start_12
    new-instance v7, Lcom/jscape/inet/ftp/FtpProgressEvent;

    iget-object v1, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->d(Lcom/jscape/inet/ftps/Ftps;)Ljava/lang/String;

    move-result-object v4

    add-long v22, v20, p7

    array-length v1, v3

    int-to-long v5, v1

    move-object v1, v7

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    move-wide/from16 v24, v5

    move-object/from16 v5, v17

    move/from16 v6, p4

    move-object v15, v7

    move-wide/from16 v7, v22

    move-object/from16 v22, v9

    move-object/from16 v16, v10

    move-wide/from16 v9, v24

    move-object/from16 v18, v11

    move-object v0, v12

    const/16 v23, 0x0

    move-wide/from16 v11, p5

    invoke-direct/range {v1 .. v12}, Lcom/jscape/inet/ftp/FtpProgressEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJ)V

    invoke-virtual {v0, v15}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V

    iget-object v0, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->b(Lcom/jscape/inet/ftps/Ftps;)I

    move-result v0
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_b

    int-to-long v0, v0

    add-long v0, v20, v0

    move-wide v1, v0

    :goto_e
    if-nez v14, :cond_12

    goto :goto_10

    :cond_12
    move-object/from16 v0, p2

    move-object/from16 v10, v16

    move-object/from16 v12, v18

    move/from16 v3, v19

    move-wide/from16 v4, v20

    move-object/from16 v9, v22

    goto :goto_f

    :catch_5
    move-exception v0

    :try_start_13
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$TextMode;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_6

    :catch_6
    move-exception v0

    :try_start_14
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$TextMode;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_13
    const/16 v23, 0x0

    move-object/from16 v0, p2

    move-object v12, v11

    move/from16 v3, v19

    move-wide/from16 v4, v20

    :goto_f
    move/from16 v11, v23

    const-wide/16 v15, 0x0

    goto/16 :goto_4

    :cond_14
    const/16 v23, 0x0

    move-object/from16 v0, p2

    move-object v12, v11

    move/from16 v3, v19

    move-wide/from16 v4, v20

    move/from16 v11, v23

    const-wide/16 v15, 0x0

    goto/16 :goto_5

    :catch_7
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$TextMode;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_15
    move-object/from16 v16, v10

    move-wide/from16 v20, v4

    :goto_10
    invoke-virtual/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_b

    if-eqz v14, :cond_17

    if-eqz v0, :cond_16

    :try_start_15
    array-length v1, v0

    if-lez v1, :cond_16

    move-object/from16 v1, p2

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;->reset()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_8

    goto :goto_11

    :catch_8
    move-exception v0

    :try_start_16
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$TextMode;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_16
    move-object/from16 v1, p2

    :goto_11
    invoke-virtual/range {p2 .. p2}, Ljava/io/OutputStream;->flush()V

    iget-object v14, v13, Lcom/jscape/inet/ftps/Ftps$TextMode;->c:Lcom/jscape/inet/ftps/Ftps;

    new-instance v15, Lcom/jscape/inet/ftp/FtpProgressEvent;

    add-long v7, v20, p7

    array-length v0, v0

    int-to-long v9, v0

    move-object v1, v15

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p3

    move-object/from16 v5, v17

    move/from16 v6, p4

    move-wide/from16 v11, p5

    invoke-direct/range {v1 .. v12}, Lcom/jscape/inet/ftp/FtpProgressEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJ)V

    invoke-virtual {v14, v15}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_b

    :cond_17
    return-wide v20

    :catch_9
    move-exception v0

    :try_start_17
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$TextMode;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_a

    :catch_a
    move-exception v0

    :try_start_18
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$TextMode;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_b

    :catch_b
    move-exception v0

    :goto_12
    :try_start_19
    instance-of v1, v0, Ljavax/net/ssl/SSLHandshakeException;
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_c

    if-eqz v1, :cond_18

    const-wide/16 v1, 0x0

    return-wide v1

    :cond_18
    new-instance v1, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_c
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps$TextMode;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method
