.class public Lcom/jscape/inet/ftps/Ftps;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/AutoCloseable;


# static fields
.field public static final ASCII:I = 0x1

.field public static final AUTH_SSL:Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;

.field public static final AUTH_TLS:Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;

.field public static final AUTO:I = 0x0

.field public static final BINARY:I = 0x2

.field public static final CURRENT_DIR:Ljava/lang/String; = "."

.field public static final IMPLICIT_SSL:Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;

.field public static final UP_DIR:Ljava/lang/String;

.field private static final a:I = 0x2000

.field private static final b:Lcom/jscape/inet/ftp/FtpFileParser;

.field private static final bb:[Ljava/lang/String;

.field private static final c:Lcom/jscape/util/m/g;

.field private static final d:I = 0x438

.field private static final e:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:Z

.field private C:I

.field private D:I

.field private E:Lcom/jscape/inet/util/i;

.field private F:I

.field private G:I

.field private H:I

.field private I:I

.field private J:Z

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:Ljava/util/zip/DeflaterOutputStream;

.field private O:Ljava/util/zip/InflaterInputStream;

.field private P:Ljava/lang/String;

.field private Q:Ljava/lang/String;

.field private R:Ljava/lang/String;

.field private S:I

.field private T:Ljava/lang/String;

.field private U:I

.field private V:I

.field private W:Ljava/util/Vector;

.field private X:Z

.field private Y:Z

.field private Z:Z

.field private aa:Z

.field private ab:Z

.field private ac:Z

.field private ad:Ljava/util/TimeZone;

.field private ae:Ljava/io/File;

.field private af:Ljavax/net/ssl/HandshakeCompletedListener;

.field ag:Ljava/util/Vector;

.field private ah:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;",
            ">;"
        }
    .end annotation
.end field

.field private ai:Ljava/util/Vector;

.field private aj:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/jscape/inet/ftps/ContextFactory;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;

.field private k:Ljava/io/File;

.field private l:Lcom/jscape/inet/ftp/FtpFileParser;

.field private m:Lcom/jscape/inet/ftps/FtpsClient;

.field private n:Ljava/util/LinkedList;

.field private o:Lcom/jscape/inet/ftps/FtpsCertificateVerifier;

.field private p:Lcom/jscape/util/j/b;

.field private q:Ljavax/net/ssl/SSLContext;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Lcom/jscape/inet/ftps/Ftps$TransferMode;

.field private y:[Ljava/lang/String;

.field private volatile z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x45

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "M\n:zK\u0006,cB-|T\u0000+`\u0007;9F\u00145}\u0007\u000fl\u0003;9D\u001c+k\u0001+vR\u000cc.\u000b]+\u000b\\\u0000%\nY&\u007f;\u0018B\r<xLU?g\u000e:9O\u0007yj\u000b-|C\u00016|\u001b\u007f\u0002|\u0015\u0002 LCC\u0017,m\u0000\u0010*z\u0003=uI\u00061.\u00010wN\u0010:z\u000b0w\u0000\u00016.$\u000bI\u0000\u0006<|\u0014:k\u0000\u0017<h\r-|\u0000\u0006<z\u00166wGU\u0017O6\u007fxD\u0011+k\u0011,7\u0015.\u00010lL\u0011y`\r+9B\u0010ym\n>wG\u0010=\u0012l\u0003;9L\u001c7i\u0007-9V\u00145{\u0007e9\u0017m\u000e6|N\u0001yg\u0011\u007fwO\u0001ym\r1wE\u0016-k\u0006\u001cK\u0010-vRU*k\u0016+pN\u0012yz\u0010>wS\u0013<|B2vD\u0010w\u0018.\u00017xN\u0012<jB,lC\u0016<}\u00119lL\u0019  B\u0004\nSB>m\u0000\u00056|\u0016\u007f\u0004V!\rZ\u0005C$\u0012M\u0000\u0005C$\u001cM\u0000\u0018Z\u000b2|S\u00018c\u0012\u007fvFU5a\u0001>u\u0000\u00130b\u0007\u007f\u001eZ\u000b2|S\u00018c\u0012\u007f\u007fO\u0007yb\r<xLU=g\u0010:zT\u001a+wB\u0003I/\u000b\u000ew\u001b&`m8=j*\u0017tM\u0006*\u0006C-\u001b\\\u0000/\u0005V!\rZ\u0000\u001c B\u0012lS\u0001yl\u0007\u007f~R\u00108z\u0007-9T\u001d8`B%|R\u001aw\u0005]+\u0005\\\u0000\u001dG\u000e3|G\u00145.\u00167kE\u0014=.\u00010lN\u0001yx\u00033lEUc.\u0019Z\u000b2|S\u00018c\u0012\u007fvFU+k\u000f0mEU?g\u000e:9\u0013l\u0003;9T\u001c4k\r*m\u0000\u00038b\u0017:#\u0000\u0002\u0003h\u0004z\u0007\'m\u001dG\u000e3|G\u00145.\u00167kE\u0014=.\u00010lN\u0001yx\u00033lEUc.\u0016.\u00017xN\u0012<jB,lC\u0016<}\u00119lL\u0019  \u001eZ\u000b2|S\u00018c\u0012\u007f\u007fO\u0007yb\r<xLU=g\u0010:zT\u001a+wB\u0002 L\u0013y\u00100wGU4a\u0006:9P\u0014*}\u0007;#\u0000\u0004V!\rZ\u001c B\u0012lS\u0001yl\u0007\u007f~R\u00108z\u0007-9T\u001d8`B%|R\u001aw\u0017M\n:zK\u0006,cB-|T\u0000+`\u0007;9F\u00145}\u0007\u0006C-\u001b\\\u0000/\u0018Z\u000b2|S\u00018c\u0012\u007fvFU5a\u0001>u\u0000\u00130b\u0007\u007f\u0016.\u00010lN\u0001y`\r+9B\u0010ym\n>wG\u0010= \u0015.\u00010lL\u0011y`\r+9B\u0010ym\n>wG\u0010=\u000fM\r1wE\u0016-g\u000c89T\u001ayU\u0002\u0003h\u0006]-\u001cRs@\u001eZ\u000b2|S\u00018c\u0012\u007fvFU+k\u000f0mEU=g\u0010:zT\u001a+wB\u000ew\u001b&`m8=j*\u0017tM\u0006*\u001dM\r2tA\u001b=.:\u001cKcU0}B1vTU*{\u0012/vR\u0001<jCC\u0017,m\u0000\u0010*z\u0003=uI\u00061.\u00010wN\u0010:z\u000b0w\u0000\u00016.$\u000bI\u0000\u0006<|\u0014:k\u0000\u0017<h\r-|\u0000\u0006<z\u00166wGU\u0017O6\u007fxD\u0011+k\u0011,7SG\u000c)xL\u001c=.\u0003,jI\u00124k\u000c+7\u0000!1kB,mA\u0007-.\u0006>mAU)a\u0010+9M\u0000*zB=|\u0000\u0019<}\u0011\u007fvRU<\u007f\u0017>u\u0000\u00011o\u000c\u007fmH\u0010yk\u000c;9D\u0014-oB/vR\u0001\u0019Z\u000b2|S\u00018c\u0012\u007fvFU+k\u000f0mEU?g\u000e:9\u0010.\u00060|SU7a\u0016\u007f|X\u001c*zL\u0006A1p-\u0010E\u0003,B}\u0004 \u00162i([\u000c>{L\u0010yz\r\u007fkE\u0001+g\u0007)|\u0000\u0007<c\r+|\u0000\u00110|\u0007<mO\u0007 .\u0012>mH\u0005C&\u000bT\u0000\u0005C&\u000bT\u0000\u0016.\u00010lL\u0011y`\r+9B\u0010ym\n>wG\u0010= \u000fl\u0003;9D\u001c+k\u0001+vR\u000cc.\u0010l\u0003;9B\u00196m\t\u007fjI\u000f<4B\u0006]-\u001cRs@\u0013m\u000e6|N\u0001yg\u0011\u007fzO\u001b7k\u0001+|D\u0003X/\u000c\u0016.\u00010lN\u0001y`\r+9B\u0010ym\n>wG\u0010= \u001eZ\u000b2|S\u00018c\u0012\u007fvFU+k\u000f0mEU=g\u0010:zT\u001a+wB\u0016.\u00010lL\u0011y`\r+9B\u0010ym\n>wG\u0010= VG\u000c)xL\u001c=.\u0003,jI\u00124k\u000c+7\u0000!1kB:wDU=o\u0016>9P\u001a+zB2lS\u0001yl\u0007\u007f~R\u00108z\u0007-9O\u0007yk\u0013*xLU-f\u000319T\u001d<.\u0011+xR\u0001yj\u0003+x\u0000\u00056|\u0016"

    const/16 v4, 0x553

    const/16 v5, 0x17

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x7a

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    const/16 v14, 0x23

    const/16 v15, 0x18

    const/4 v1, 0x5

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    if-eqz v11, :cond_1

    add-int/lit8 v1, v7, 0x1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v1

    goto :goto_0

    :cond_0
    const-string v3, "AnX\u0017!}S\u0005-C\u0003,yS\u0012~V\u0003#vOO-k\n6d^\u0012 mE>Cd"

    move v7, v1

    move v4, v14

    move v5, v15

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v10, v7, 0x1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v5, v1

    move v7, v10

    :goto_3
    const/16 v8, 0x15

    add-int/2addr v6, v9

    add-int v1, v6, v5

    invoke-virtual {v3, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object v1, v0, v1

    sput-object v1, Lcom/jscape/inet/ftps/Ftps;->UP_DIR:Ljava/lang/String;

    const/16 v1, 0x3c

    aget-object v0, v0, v1

    sput-object v0, Lcom/jscape/inet/ftps/Ftps;->e:Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/ftps/Ftps$ImplicitSslStrategy;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftps/Ftps$ImplicitSslStrategy;-><init>(Lcom/jscape/inet/ftps/Ftps$1;)V

    sput-object v0, Lcom/jscape/inet/ftps/Ftps;->IMPLICIT_SSL:Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;

    new-instance v0, Lcom/jscape/inet/ftps/Ftps$AuthTlsStrategy;

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftps/Ftps$AuthTlsStrategy;-><init>(Lcom/jscape/inet/ftps/Ftps$1;)V

    sput-object v0, Lcom/jscape/inet/ftps/Ftps;->AUTH_TLS:Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;

    new-instance v0, Lcom/jscape/inet/ftps/Ftps$AuthSslStrategy;

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftps/Ftps$AuthSslStrategy;-><init>(Lcom/jscape/inet/ftps/Ftps$1;)V

    sput-object v0, Lcom/jscape/inet/ftps/Ftps;->AUTH_SSL:Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;

    new-instance v0, Lcom/jscape/inet/ftp/MLSDParser;

    invoke-direct {v0}, Lcom/jscape/inet/ftp/MLSDParser;-><init>()V

    sput-object v0, Lcom/jscape/inet/ftps/Ftps;->b:Lcom/jscape/inet/ftp/FtpFileParser;

    new-instance v0, Lcom/jscape/util/m/d;

    invoke-direct {v0}, Lcom/jscape/util/m/d;-><init>()V

    sput-object v0, Lcom/jscape/inet/ftps/Ftps;->c:Lcom/jscape/util/m/g;

    return-void

    :cond_3
    aget-char v16, v10, v13

    rem-int/lit8 v2, v13, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v9, :cond_8

    const/4 v15, 0x2

    if-eq v2, v15, :cond_7

    const/4 v15, 0x3

    if-eq v2, v15, :cond_6

    const/4 v15, 0x4

    if-eq v2, v15, :cond_5

    if-eq v2, v1, :cond_4

    goto :goto_4

    :cond_4
    const/16 v14, 0xf

    goto :goto_4

    :cond_5
    const/16 v14, 0x5a

    goto :goto_4

    :cond_6
    const/16 v14, 0x63

    goto :goto_4

    :cond_7
    const/16 v14, 0x25

    goto :goto_4

    :cond_8
    move v14, v15

    goto :goto_4

    :cond_9
    const/16 v14, 0x74

    :goto_4
    xor-int v1, v8, v14

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_2
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, ""

    invoke-direct {p0, v0, v0, v0}, Lcom/jscape/inet/ftps/Ftps;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jscape/inet/ftps/ContextFactory;

    invoke-direct {v0}, Lcom/jscape/inet/ftps/ContextFactory;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->g:Ljava/lang/String;

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->i:Ljava/lang/String;

    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->AUTH_TLS:Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;

    iput-object v2, p0, Lcom/jscape/inet/ftps/Ftps;->j:Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->k:Ljava/io/File;

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->l:Lcom/jscape/inet/ftp/FtpFileParser;

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/jscape/inet/ftps/Ftps;->n:Ljava/util/LinkedList;

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->o:Lcom/jscape/inet/ftps/FtpsCertificateVerifier;

    sget-object v2, Ljava/util/logging/Level;->OFF:Ljava/util/logging/Level;

    invoke-static {v2}, Lcom/jscape/util/j/b;->a(Ljava/util/logging/Level;)Lcom/jscape/util/j/b;

    move-result-object v2

    iput-object v2, p0, Lcom/jscape/inet/ftps/Ftps;->p:Lcom/jscape/util/j/b;

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->q:Ljavax/net/ssl/SSLContext;

    sget-object v2, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v2}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/jscape/inet/ftps/Ftps;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->s:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->t:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->u:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->w:Ljava/lang/String;

    new-instance v2, Lcom/jscape/inet/ftps/Ftps$BinaryMode;

    invoke-direct {v2, p0, v0}, Lcom/jscape/inet/ftps/Ftps$BinaryMode;-><init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps$1;)V

    iput-object v2, p0, Lcom/jscape/inet/ftps/Ftps;->x:Lcom/jscape/inet/ftps/Ftps$TransferMode;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/jscape/inet/ftps/Ftps;->z:Z

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/jscape/inet/ftps/Ftps;->A:Z

    iput-boolean v3, p0, Lcom/jscape/inet/ftps/Ftps;->B:Z

    const/16 v4, 0x2000

    iput v4, p0, Lcom/jscape/inet/ftps/Ftps;->C:I

    iput v2, p0, Lcom/jscape/inet/ftps/Ftps;->D:I

    new-instance v4, Lcom/jscape/inet/util/i;

    invoke-direct {v4, v2, v2}, Lcom/jscape/inet/util/i;-><init>(II)V

    iput-object v4, p0, Lcom/jscape/inet/ftps/Ftps;->E:Lcom/jscape/inet/util/i;

    iput v2, p0, Lcom/jscape/inet/ftps/Ftps;->F:I

    const/16 v4, 0x15

    iput v4, p0, Lcom/jscape/inet/ftps/Ftps;->H:I

    const/16 v4, 0x7530

    iput v4, p0, Lcom/jscape/inet/ftps/Ftps;->I:I

    iput-boolean v3, p0, Lcom/jscape/inet/ftps/Ftps;->J:Z

    iput-boolean v2, p0, Lcom/jscape/inet/ftps/Ftps;->K:Z

    iput-boolean v2, p0, Lcom/jscape/inet/ftps/Ftps;->L:Z

    iput-boolean v2, p0, Lcom/jscape/inet/ftps/Ftps;->M:Z

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->N:Ljava/util/zip/DeflaterOutputStream;

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->O:Ljava/util/zip/InflaterInputStream;

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->P:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->Q:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->R:Ljava/lang/String;

    const/16 v3, 0x438

    iput v3, p0, Lcom/jscape/inet/ftps/Ftps;->S:I

    sget-object v3, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v4, 0x2b

    aget-object v4, v3, v4

    iput-object v4, p0, Lcom/jscape/inet/ftps/Ftps;->T:Ljava/lang/String;

    const/4 v4, -0x1

    iput v4, p0, Lcom/jscape/inet/ftps/Ftps;->U:I

    iput v4, p0, Lcom/jscape/inet/ftps/Ftps;->V:I

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->W:Ljava/util/Vector;

    iput-boolean v2, p0, Lcom/jscape/inet/ftps/Ftps;->Y:Z

    iput-boolean v2, p0, Lcom/jscape/inet/ftps/Ftps;->Z:Z

    iput-boolean v2, p0, Lcom/jscape/inet/ftps/Ftps;->aa:Z

    iput-boolean v2, p0, Lcom/jscape/inet/ftps/Ftps;->ab:Z

    iput-boolean v2, p0, Lcom/jscape/inet/ftps/Ftps;->ac:Z

    const/16 v2, 0x12

    aget-object v2, v3, v2

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    iput-object v2, p0, Lcom/jscape/inet/ftps/Ftps;->ad:Ljava/util/TimeZone;

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->ae:Ljava/io/File;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->ag:Ljava/util/Vector;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->ah:Ljava/util/HashMap;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->ai:Ljava/util/Vector;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->aj:Ljava/util/HashMap;

    new-instance v0, Lcom/jscape/util/d;

    invoke-direct {v0}, Lcom/jscape/util/d;-><init>()V

    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {v0}, Lcom/jscape/util/d;->a()Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->s:Ljava/lang/String;

    iput p2, p0, Lcom/jscape/inet/ftps/Ftps;->H:I

    iput-object p3, p0, Lcom/jscape/inet/ftps/Ftps;->w:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/inet/ftps/Ftps;->t:Ljava/lang/String;

    iput-object p5, p0, Lcom/jscape/inet/ftps/Ftps;->k:Ljava/io/File;

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Lcom/jscape/util/d;->b()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x15

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/jscape/inet/ftps/Ftps;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    new-instance v5, Ljava/io/File;

    const-string v0, "."

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move v2, p4

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ftps/Ftps;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 6

    const/16 v2, 0x15

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ftps/Ftps;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/16 v0, 0x15

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/jscape/inet/ftps/Ftps;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p0, p4}, Lcom/jscape/inet/ftps/Ftps;->setAccount(Ljava/lang/String;)V

    return-void
.end method

.method private static a(ILjava/lang/String;Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    move-object v1, p2

    goto :goto_0

    :cond_0
    move-object v1, p1

    :goto_0
    if-eqz v0, :cond_3

    if-nez v1, :cond_2

    :cond_1
    return p0

    :cond_2
    move-object v1, p1

    :cond_3
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_8

    if-eqz v1, :cond_9

    :try_start_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v0, :cond_5

    if-nez v1, :cond_4

    goto :goto_1

    :cond_4
    invoke-virtual {p1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    :cond_5
    if-eqz v0, :cond_7

    if-eqz v1, :cond_6

    return p0

    :cond_6
    const/4 v1, 0x1

    :cond_7
    new-array v0, v1, [B

    int-to-byte p0, p0

    const/4 v1, 0x0

    aput-byte p0, v0, v1

    new-instance p0, Ljava/lang/String;

    invoke-static {p1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-static {p2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p0

    aget-byte p0, p0, v1

    return p0

    :cond_8
    move p0, v1

    :cond_9
    :goto_1
    return p0

    :catch_0
    move-exception p0

    :try_start_2
    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p0

    :try_start_3
    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
.end method

.method private a(Ljava/net/Socket;)Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->i()Z

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-virtual {p1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :cond_0
    new-instance v0, Ljava/util/zip/InflaterInputStream;

    invoke-virtual {p1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->O:Ljava/util/zip/InflaterInputStream;

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->O:Ljava/util/zip/InflaterInputStream;

    return-object p1
.end method

.method private a(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    sget-object v0, Lcom/jscape/inet/ftp/Checksum;->CRC32:Lcom/jscape/inet/ftp/Checksum;

    const-wide v1, 0x7fffffffffffffffL

    invoke-virtual {v0, p1, v1, v2}, Lcom/jscape/inet/ftp/Checksum;->of(Ljava/io/InputStream;J)[B

    move-result-object p1

    const-string v0, ""

    invoke-static {p1, v0}, Lcom/jscape/util/W;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private a(Ljava/io/InputStream;Ljava/lang/String;ZJJZ)Ljava/lang/String;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object v9, p0

    move-object/from16 v0, p2

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v10

    :try_start_0
    iget-object v1, v9, Lcom/jscape/inet/ftps/Ftps;->x:Lcom/jscape/inet/ftps/Ftps$TransferMode;

    instance-of v1, v1, Lcom/jscape/inet/ftps/Ftps$AutoMode;
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_10

    if-eqz v10, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-direct {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->f(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_11

    :cond_0
    const/4 v1, 0x0

    :cond_1
    move v11, v1

    const/4 v12, 0x1

    const/4 v1, 0x0

    :try_start_2
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->k()V
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_9
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v10, :cond_3

    if-eqz p8, :cond_2

    :try_start_3
    iget-object v2, v9, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v2, v0}, Lcom/jscape/inet/ftps/FtpsClient;->storeUnique(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object v1
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_b
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :cond_2
    move/from16 v2, p3

    goto :goto_0

    :cond_3
    move/from16 v2, p8

    :goto_0
    if-eqz v2, :cond_4

    :try_start_4
    iget-object v2, v9, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v2, v0}, Lcom/jscape/inet/ftps/FtpsClient;->append(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object v1
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_4
    iget-object v2, v9, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v2, v0}, Lcom/jscape/inet/ftps/FtpsClient;->store(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object v1
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_b
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_9
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :goto_1
    move-object v13, v1

    :try_start_6
    invoke-virtual {v13}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    if-eqz v10, :cond_8

    if-eqz p8, :cond_7

    iget-object v1, v9, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/FtpsClient;->getLastResponse()Lcom/jscape/inet/ftps/Response;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Response;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/jscape/inet/ftps/Ftps;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    move-object v2, p1

    move-wide/from16 v5, p4

    move-wide/from16 v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;JJ)V

    iget-object v1, v9, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/FtpsClient;->getLastResponse()Lcom/jscape/inet/ftps/Response;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Response;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/jscape/inet/ftps/Ftps;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_8
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v10, :cond_6

    :try_start_7
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->i()Z

    move-result v1
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_1

    if-eqz v1, :cond_5

    if-eqz v10, :cond_6

    :try_start_8
    iget-object v1, v9, Lcom/jscape/inet/ftps/Ftps;->N:Ljava/util/zip/DeflaterOutputStream;

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->c()V
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_3

    :cond_5
    invoke-static {v13}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v1, v0

    :try_start_9
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_2

    :catch_2
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_6
    :goto_2
    invoke-direct {p0, v11}, Lcom/jscape/inet/ftps/Ftps;->a(Z)V

    return-object v0

    :cond_7
    move-object v1, p0

    move-object v2, p1

    move-object/from16 v4, p2

    move-wide/from16 v5, p4

    move-wide/from16 v7, p6

    :try_start_b
    invoke-direct/range {v1 .. v8}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;JJ)V

    :cond_8
    iget-object v0, v9, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient;->getLastResponse()Lcom/jscape/inet/ftps/Response;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Response;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_8
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    if-eqz v10, :cond_a

    :try_start_c
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->i()Z

    move-result v1
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_4

    if-eqz v1, :cond_9

    if-eqz v10, :cond_a

    :try_start_d
    iget-object v1, v9, Lcom/jscape/inet/ftps/Ftps;->N:Ljava/util/zip/DeflaterOutputStream;

    if-eqz v1, :cond_9

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->c()V
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_6

    :cond_9
    invoke-static {v13}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    goto :goto_3

    :catch_4
    move-exception v0

    move-object v1, v0

    :try_start_e
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_e .. :try_end_e} :catch_5

    :catch_5
    move-exception v0

    :try_start_f
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_f .. :try_end_f} :catch_6

    :catch_6
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_a
    :goto_3
    invoke-direct {p0, v11}, Lcom/jscape/inet/ftps/Ftps;->a(Z)V

    return-object v0

    :catchall_0
    move-exception v0

    move-object v1, v13

    goto :goto_6

    :catch_7
    move-exception v0

    move-object v1, v13

    goto :goto_4

    :catch_8
    move-exception v0

    move-object v1, v13

    goto :goto_5

    :catchall_1
    move-exception v0

    goto :goto_6

    :catch_9
    move-exception v0

    goto :goto_4

    :catch_a
    move-exception v0

    move-object v2, v0

    :try_start_10
    invoke-static {v2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_b
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_9
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    :goto_4
    :try_start_11
    new-instance v2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catchall_2
    move-exception v0

    move v11, v12

    goto :goto_6

    :catch_b
    move-exception v0

    :goto_5
    throw v0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    :goto_6
    if-eqz v10, :cond_c

    :try_start_12
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->i()Z

    move-result v2
    :try_end_12
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_12 .. :try_end_12} :catch_c

    if-eqz v2, :cond_b

    if-eqz v10, :cond_c

    :try_start_13
    iget-object v2, v9, Lcom/jscape/inet/ftps/Ftps;->N:Ljava/util/zip/DeflaterOutputStream;
    :try_end_13
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_13 .. :try_end_13} :catch_e

    if-eqz v2, :cond_b

    :try_start_14
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->c()V
    :try_end_14
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_14 .. :try_end_14} :catch_f

    :cond_b
    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    goto :goto_7

    :catch_c
    move-exception v0

    move-object v1, v0

    :try_start_15
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_15
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_15 .. :try_end_15} :catch_d

    :catch_d
    move-exception v0

    :try_start_16
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_16
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_16 .. :try_end_16} :catch_e

    :catch_e
    move-exception v0

    :try_start_17
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_17
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_17 .. :try_end_17} :catch_f

    :catch_f
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_c
    :goto_7
    invoke-direct {p0, v11}, Lcom/jscape/inet/ftps/Ftps;->a(Z)V

    throw v0

    :catch_10
    move-exception v0

    :try_start_18
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_18
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_18 .. :try_end_18} :catch_11

    :catch_11
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "/"

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object p1
.end method

.method private static a(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    return-object p0
.end method

.method static a(Lcom/jscape/inet/ftps/Ftps;)Ljava/util/LinkedList;
    .locals 0

    iget-object p0, p0, Lcom/jscape/inet/ftps/Ftps;->n:Ljava/util/LinkedList;

    return-object p0
.end method

.method private a()V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->W:Ljava/util/Vector;

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getFeatures()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v2, :cond_1

    :try_start_1
    iget-object v2, p0, Lcom/jscape/inet/ftps/Ftps;->W:Ljava/util/Vector;

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v0, :cond_2

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->X:Z
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->X:Z

    :cond_2
    :goto_1
    return-void
.end method

.method private declared-synchronized a(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_3

    goto :goto_0

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    const-string v2, "A"

    invoke-virtual {v1, v2}, Lcom/jscape/inet/ftps/FtpsClient;->representationType(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_2

    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    const-string v2, "I"

    invoke-virtual {v1, v2}, Lcom/jscape/inet/ftps/FtpsClient;->representationType(Ljava/lang/String;)V

    if-nez v0, :cond_2

    goto :goto_2

    :catch_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_1
    :goto_2
    if-eqz v0, :cond_3

    :cond_2
    :try_start_5
    iput p1, p0, Lcom/jscape/inet/ftps/Ftps;->G:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit p0

    return-void

    :cond_3
    :try_start_6
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v3, 0x21

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_3
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method static a(Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->e(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;JJ)V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v10, p0

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->i()Z

    move-result v1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_1

    new-instance v1, Ljava/util/zip/DeflaterOutputStream;

    move-object/from16 v2, p2

    invoke-direct {v1, v2}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v1, v10, Lcom/jscape/inet/ftps/Ftps;->N:Ljava/util/zip/DeflaterOutputStream;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-object v11, v10, Lcom/jscape/inet/ftps/Ftps;->x:Lcom/jscape/inet/ftps/Ftps$TransferMode;

    iget-object v13, v10, Lcom/jscape/inet/ftps/Ftps;->N:Ljava/util/zip/DeflaterOutputStream;

    const/4 v15, 0x0

    move-object/from16 v12, p1

    move-object/from16 v14, p3

    move-wide/from16 v16, p4

    move-wide/from16 v18, p6

    invoke-virtual/range {v11 .. v19}, Lcom/jscape/inet/ftps/Ftps$TransferMode;->transmit(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;IJJ)J

    move-result-wide v5

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    move-wide v6, v5

    goto :goto_2

    :cond_1
    move-object/from16 v2, p2

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_2
    move-object/from16 v2, p2

    const-wide/16 v3, 0x0

    :goto_1
    iget-object v11, v10, Lcom/jscape/inet/ftps/Ftps;->x:Lcom/jscape/inet/ftps/Ftps$TransferMode;

    const/4 v15, 0x0

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    move-wide/from16 v16, p4

    move-wide/from16 v18, p6

    invoke-virtual/range {v11 .. v19}, Lcom/jscape/inet/ftps/Ftps$TransferMode;->transmit(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;IJJ)J

    move-result-wide v1

    move-wide v6, v1

    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    sub-long v8, v1, v3

    const-string v1, ""

    if-eqz v0, :cond_3

    :try_start_1
    iget-object v0, v10, Lcom/jscape/inet/ftps/Ftps;->ae:Ljava/io/File;
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v0, :cond_3

    iget-object v0, v10, Lcom/jscape/inet/ftps/Ftps;->ae:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_3
    :goto_3
    move-object v5, v1

    new-instance v0, Lcom/jscape/inet/ftp/FtpUploadEvent;

    iget-object v4, v10, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    move-object v1, v0

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    invoke-direct/range {v1 .. v9}, Lcom/jscape/inet/ftp/FtpUploadEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    invoke-virtual {v10, v0}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V

    return-void
.end method

.method private a(Ljava/io/OutputStream;Ljava/lang/String;J)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v10, p0

    move-object/from16 v11, p2

    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_0

    :try_start_0
    iget-object v0, v10, Lcom/jscape/inet/ftps/Ftps;->x:Lcom/jscape/inet/ftps/Ftps$TransferMode;

    instance-of v0, v0, Lcom/jscape/inet/ftps/Ftps$AutoMode;

    if-eqz v0, :cond_0

    invoke-direct {v10, v11}, Lcom/jscape/inet/ftps/Ftps;->f(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    const-wide/16 v1, 0x0

    :try_start_1
    invoke-virtual {v10, v11}, Lcom/jscape/inet/ftps/Ftps;->getFilesize(Ljava/lang/String;)J

    move-result-wide v3
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v3, v0

    :try_start_2
    iget-boolean v0, v10, Lcom/jscape/inet/ftps/Ftps;->K:Z
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_d

    if-eqz v12, :cond_3

    if-nez v0, :cond_2

    move-wide v3, v1

    :goto_1
    if-eqz v12, :cond_1

    :try_start_3
    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->k()V
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_2

    cmp-long v0, p3, v1

    move-wide v1, v3

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_1
    move-wide v6, v3

    goto :goto_4

    :cond_2
    :try_start_4
    throw v3
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_e

    :cond_3
    :goto_2
    if-lez v0, :cond_4

    :try_start_5
    iget-object v0, v10, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/jscape/inet/ftps/FtpsClient;->restart(Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_4
    :goto_3
    move-wide v6, v1

    :goto_4
    iget-object v0, v10, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0, v11}, Lcom/jscape/inet/ftps/FtpsClient;->retrieve(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object v13

    const/4 v14, 0x0

    :try_start_6
    invoke-direct {v10, v13}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/net/Socket;)Ljava/io/InputStream;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    iget-object v1, v10, Lcom/jscape/inet/ftps/Ftps;->x:Lcom/jscape/inet/ftps/Ftps$TransferMode;

    const/4 v5, 0x1

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-wide/from16 v8, p3

    invoke-virtual/range {v1 .. v9}, Lcom/jscape/inet/ftps/Ftps$TransferMode;->transmit(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;IJJ)J

    move-result-wide v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long v8, v0, v15

    const-string v0, ""
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v12, :cond_6

    :try_start_7
    iget-object v1, v10, Lcom/jscape/inet/ftps/Ftps;->ae:Ljava/io/File;
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_8
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v1, :cond_5

    :try_start_8
    iget-object v0, v10, Lcom/jscape/inet/ftps/Ftps;->ae:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    :cond_5
    move-object v5, v0

    new-instance v0, Lcom/jscape/inet/ftp/FtpDownloadEvent;

    iget-object v4, v10, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    move-object v1, v0

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v9}, Lcom/jscape/inet/ftp/FtpDownloadEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    invoke-virtual {v10, v0}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V

    goto :goto_5

    :catch_4
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_6
    :goto_5
    if-eqz v12, :cond_8

    :try_start_9
    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->i()Z

    move-result v0
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_5

    if-eqz v0, :cond_7

    if-eqz v12, :cond_8

    :try_start_a
    iget-object v0, v10, Lcom/jscape/inet/ftps/Ftps;->O:Ljava/util/zip/InflaterInputStream;

    if-eqz v0, :cond_7

    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->d()V
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_7

    :cond_7
    invoke-static {v13}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    goto :goto_6

    :catch_5
    move-exception v0

    move-object v1, v0

    :try_start_b
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_6

    :catch_6
    move-exception v0

    :try_start_c
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_7

    :catch_7
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_8
    :goto_6
    invoke-direct {v10, v14}, Lcom/jscape/inet/ftps/Ftps;->a(Z)V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_7

    :catch_8
    move-exception v0

    const/4 v14, 0x1

    :try_start_d
    new-instance v1, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :goto_7
    if-eqz v12, :cond_a

    :try_start_e
    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->i()Z

    move-result v1
    :try_end_e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_e .. :try_end_e} :catch_9

    if-eqz v1, :cond_9

    if-eqz v12, :cond_a

    :try_start_f
    iget-object v1, v10, Lcom/jscape/inet/ftps/Ftps;->O:Ljava/util/zip/InflaterInputStream;
    :try_end_f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_f .. :try_end_f} :catch_b

    if-eqz v1, :cond_9

    :try_start_10
    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->d()V
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_c

    :cond_9
    invoke-static {v13}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    goto :goto_8

    :catch_9
    move-exception v0

    move-object v1, v0

    :try_start_11
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_11
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_11 .. :try_end_11} :catch_a

    :catch_a
    move-exception v0

    :try_start_12
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_12
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_12 .. :try_end_12} :catch_b

    :catch_b
    move-exception v0

    :try_start_13
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_13
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_13 .. :try_end_13} :catch_c

    :catch_c
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_a
    :goto_8
    invoke-direct {v10, v14}, Lcom/jscape/inet/ftps/Ftps;->a(Z)V

    throw v0

    :catch_d
    move-exception v0

    move-object v1, v0

    :try_start_14
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_14
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_14 .. :try_end_14} :catch_e

    :catch_e
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method private static a(Ljava/io/RandomAccessFile;)V
    .locals 0

    :try_start_0
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/io/File;ZLjava/util/Vector;)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p4}, Ljava/util/Vector;->size()I

    move-result v2

    if-lez v2, :cond_a

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v3, :cond_a

    :try_start_1
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V

    invoke-virtual {p4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    move-object v3, p0

    :goto_0
    check-cast v3, Ljava/lang/String;

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v4}, Lcom/jscape/util/Q;->d(Ljava/io/File;Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_3

    if-eqz v3, :cond_2

    :try_start_2
    const-string v5, ""

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    if-nez v5, :cond_2

    :try_start_3
    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftps/Ftps;->makeLocalDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftps/Ftps;->download(Ljava/lang/String;)Ljava/io/File;

    :cond_3
    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_9

    :try_start_4
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v7
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9

    const-wide/16 v9, 0x0

    cmp-long v5, v7, v9

    if-lez v5, :cond_8

    if-eqz v0, :cond_4

    if-eqz p3, :cond_8

    if-eqz v0, :cond_9

    :try_start_5
    sget-object v5, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v7, 0xd

    aget-object v5, v5, v7

    invoke-virtual {p0, v5}, Lcom/jscape/inet/ftps/Ftps;->isFeatureSupported(Ljava/lang/String;)Z

    move-result v5
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_d

    goto :goto_2

    :cond_4
    move v5, p3

    :goto_2
    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    :try_start_6
    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    if-eqz v0, :cond_6

    if-nez v7, :cond_5

    :try_start_7
    const-string v7, "\\"

    invoke-virtual {v5, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    if-eqz v0, :cond_6

    if-nez v7, :cond_5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_5
    if-eqz v0, :cond_9

    :try_start_8
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/jscape/inet/ftps/Ftps;->checksum(Ljava/io/File;Ljava/lang/String;)Z

    move-result v7
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_3

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_6
    :goto_3
    if-eqz v7, :cond_7

    goto :goto_4

    :cond_7
    :try_start_9
    new-instance p1, Lcom/jscape/inet/ftp/FtpException;

    sget-object p2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 p3, 0x24

    aget-object p2, p2, p3

    invoke-direct {p1, p2}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catch_6
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    :catch_7
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    :catch_8
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_8
    :goto_4
    invoke-virtual {p4, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    goto :goto_5

    :catch_9
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_a

    :catch_a
    move-exception p1

    :try_start_d
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_b

    :catch_b
    move-exception p1

    :try_start_e
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_c

    :catch_c
    move-exception p1

    :try_start_f
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_d

    :catch_d
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_9
    :goto_5
    if-nez v0, :cond_0

    :cond_a
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/io/File;ZLjava/util/Vector;I)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/jscape/inet/ftps/Ftps;->ag:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    iget-object v2, v1, Lcom/jscape/inet/ftps/Ftps;->ah:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    invoke-virtual/range {p4 .. p4}, Ljava/util/Vector;->size()I

    move-result v2

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    move/from16 v5, p5

    const/4 v6, 0x0

    :cond_0
    if-ge v6, v5, :cond_1

    :try_start_0
    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    invoke-virtual {v3, v7}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v6, v6, 0x1

    if-eqz v0, :cond_2

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    const/4 v6, 0x0

    :cond_2
    const/4 v5, 0x0

    :goto_1
    if-ge v5, v2, :cond_6

    if-eqz v0, :cond_4

    :try_start_1
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v7
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v0, :cond_3

    if-lt v6, v7, :cond_4

    const/4 v6, 0x0

    goto :goto_2

    :cond_3
    move/from16 v2, p3

    move-object v15, v1

    move v8, v7

    move v7, v6

    move v6, v5

    move-object v5, v3

    move-object v3, v0

    move-object/from16 v0, p1

    goto :goto_5

    :catch_1
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_4
    :goto_2
    invoke-virtual {v3, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Vector;

    move-object/from16 v8, p4

    invoke-virtual {v8, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v6, 0x1

    invoke-virtual {v3, v7, v6}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    add-int/lit8 v5, v5, 0x1

    if-nez v0, :cond_5

    goto :goto_3

    :cond_5
    move v6, v9

    goto :goto_1

    :cond_6
    :goto_3
    move/from16 v2, p3

    move-object v7, v1

    move-object v5, v3

    const/4 v6, 0x0

    move-object v3, v0

    move-object/from16 v0, p1

    :goto_4
    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v8

    move-object v15, v7

    move v7, v6

    :goto_5
    if-ge v7, v8, :cond_d

    invoke-virtual {v5, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Vector;

    :try_start_2
    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v8
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    if-eqz v3, :cond_c

    if-nez v8, :cond_7

    move-object v4, v15

    goto :goto_6

    :cond_7
    new-instance v8, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;

    iget-object v14, v15, Lcom/jscape/inet/ftps/Ftps;->k:Ljava/io/File;

    const/16 v16, 0x0

    move-object v9, v8

    move-object v10, v15

    move-object v11, v15

    move-object v12, v7

    move v13, v2

    move-object/from16 v17, v14

    move-object v14, v0

    move-object v4, v15

    move-object/from16 v15, v17

    invoke-direct/range {v9 .. v16}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;-><init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/util/Vector;ZLjava/lang/String;Ljava/io/File;Lcom/jscape/inet/ftps/Ftps$1;)V

    iget-object v9, v4, Lcom/jscape/inet/ftps/Ftps;->ag:Ljava/util/Vector;

    invoke-virtual {v9, v8}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const/4 v9, 0x0

    :cond_8
    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v10

    if-ge v9, v10, :cond_9

    :try_start_3
    iget-object v10, v4, Lcom/jscape/inet/ftps/Ftps;->ah:Ljava/util/HashMap;

    invoke-virtual {v7, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    add-int/lit8 v9, v9, 0x1

    if-eqz v3, :cond_a

    if-nez v3, :cond_8

    goto :goto_6

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_9
    :goto_6
    add-int/lit8 v6, v6, 0x1

    :cond_a
    if-nez v3, :cond_b

    goto :goto_7

    :cond_b
    move-object v7, v4

    goto :goto_4

    :cond_c
    move-object v4, v15

    goto :goto_8

    :catch_3
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_d
    move-object v4, v15

    :goto_7
    const/4 v8, 0x0

    :cond_e
    :goto_8
    iget-object v0, v4, Lcom/jscape/inet/ftps/Ftps;->ag:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v8, v0, :cond_f

    :try_start_4
    iget-object v0, v4, Lcom/jscape/inet/ftps/Ftps;->ag:Ljava/util/Vector;

    invoke-virtual {v0, v8}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->start()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    add-int/lit8 v8, v8, 0x1

    if-eqz v3, :cond_10

    if-nez v3, :cond_e

    goto :goto_9

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_f
    :goto_9
    const/4 v8, 0x0

    :cond_10
    const/4 v0, 0x0

    const/4 v2, 0x0

    :cond_11
    iget-object v5, v4, Lcom/jscape/inet/ftps/Ftps;->ag:Ljava/util/Vector;

    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v2, v5, :cond_17

    iget-object v5, v4, Lcom/jscape/inet/ftps/Ftps;->ag:Ljava/util/Vector;

    invoke-virtual {v5, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;

    :try_start_5
    invoke-virtual {v5}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->error()Z

    move-result v6
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7

    if-eqz v3, :cond_18

    const/4 v7, 0x1

    if-eqz v3, :cond_13

    if-eqz v6, :cond_12

    invoke-virtual {v5}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    move v8, v7

    :cond_12
    invoke-virtual {v4}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v6

    :cond_13
    if-eqz v3, :cond_15

    if-ne v6, v7, :cond_14

    :try_start_6
    invoke-virtual {v4}, Lcom/jscape/inet/ftps/Ftps;->abortDownloadThreads()V

    if-nez v3, :cond_17

    :cond_14
    invoke-virtual {v5}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->getConnected()Z

    move-result v6
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_a

    :catch_5
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_15
    :goto_a
    if-ne v6, v7, :cond_16

    const-wide/16 v5, 0x64

    :try_start_7
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    :catch_6
    add-int/lit8 v2, v2, -0x1

    :cond_16
    add-int/2addr v2, v7

    if-nez v3, :cond_11

    goto :goto_b

    :catch_7
    move-exception v0

    move-object v2, v0

    :try_start_8
    invoke-static {v2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_17
    :goto_b
    move v6, v8

    :cond_18
    if-nez v6, :cond_19

    return-void

    :cond_19
    :try_start_9
    new-instance v2, Lcom/jscape/inet/ftp/FtpException;

    invoke-direct {v2, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method private a(Ljava/lang/String;ZLjava/lang/String;Ljava/util/Vector;Ljava/io/File;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;,
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "/"

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-virtual {p4}, Ljava/util/Vector;->size()I

    move-result v2

    if-lez v2, :cond_8

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v3

    if-nez v3, :cond_8

    invoke-virtual {p4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    move-object v3, p0

    :goto_0
    check-cast v3, Ljava/lang/String;

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {p5, v4}, Lcom/jscape/util/Q;->d(Ljava/io/File;Ljava/io/File;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, p1, v5}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v1, :cond_2

    if-eqz v5, :cond_2

    if-eqz v1, :cond_2

    :try_start_1
    const-string v7, ""

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    if-nez v7, :cond_2

    :try_start_2
    invoke-virtual {p0, v6}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    :try_start_3
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-direct {p0, v5}, Lcom/jscape/inet/ftps/Ftps;->e(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    :try_start_4
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v5
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_8

    if-eqz v1, :cond_7

    if-eqz v5, :cond_6

    :try_start_5
    invoke-virtual {p0, p3, v4}, Lcom/jscape/inet/ftps/Ftps;->upload(Ljava/lang/String;Ljava/io/File;)V

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v5
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_9

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v1, :cond_7

    if-lez v5, :cond_6

    if-eqz v1, :cond_7

    if-eqz p2, :cond_6

    :try_start_6
    sget-object v5, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v6, 0x22

    aget-object v5, v5, v6

    invoke-virtual {p0, v5}, Lcom/jscape/inet/ftps/Ftps;->isFeatureSupported(Ljava/lang/String;)Z

    move-result v5
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_d

    if-eqz v1, :cond_7

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    :try_start_7
    invoke-virtual {v5, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    if-eqz v1, :cond_4

    if-nez v6, :cond_3

    :try_start_8
    const-string v6, "\\"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    if-eqz v1, :cond_4

    if-nez v6, :cond_3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/jscape/inet/ftps/Ftps;->checksum(Ljava/io/File;Ljava/lang/String;)Z

    move-result v6

    :cond_4
    if-eqz v1, :cond_7

    if-eqz v6, :cond_5

    goto :goto_2

    :cond_5
    :try_start_9
    new-instance p1, Lcom/jscape/inet/ftp/FtpException;

    sget-object p2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object p2, p2, v2

    invoke-direct {p1, p2}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catch_5
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6

    :catch_6
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_7

    :catch_7
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_6
    :goto_2
    invoke-virtual {p4, v3}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    :cond_7
    if-nez v1, :cond_0

    goto :goto_3

    :catch_8
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_9

    :catch_9
    move-exception p1

    :try_start_d
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_a

    :catch_a
    move-exception p1

    :try_start_e
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_b

    :catch_b
    move-exception p1

    :try_start_f
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_c

    :catch_c
    move-exception p1

    :try_start_10
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_d

    :catch_d
    move-exception p1

    :try_start_11
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_e

    :catch_e
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_8
    :goto_3
    return-void
.end method

.method private a(Ljava/lang/String;ZLjava/lang/String;Ljava/util/Vector;Ljava/io/File;I)V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;,
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/jscape/inet/ftps/Ftps;->ai:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    iget-object v0, v1, Lcom/jscape/inet/ftps/Ftps;->aj:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual/range {p4 .. p4}, Ljava/util/Vector;->size()I

    move-result v2

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3}, Ljava/util/Vector;-><init>()V

    move/from16 v5, p6

    const/4 v6, 0x0

    :cond_0
    if-ge v6, v5, :cond_1

    :try_start_0
    new-instance v7, Ljava/util/Vector;

    invoke-direct {v7}, Ljava/util/Vector;-><init>()V

    invoke-virtual {v3, v7}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v6, v6, 0x1

    if-eqz v0, :cond_2

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    const/4 v6, 0x0

    :cond_2
    const/4 v5, 0x0

    :goto_1
    if-ge v5, v2, :cond_6

    if-eqz v0, :cond_4

    :try_start_1
    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v7
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v0, :cond_3

    if-lt v6, v7, :cond_4

    const/4 v6, 0x0

    goto :goto_2

    :cond_3
    move/from16 v2, p2

    move-object v15, v1

    move-object v9, v3

    move v8, v6

    move v10, v7

    move-object/from16 v3, p3

    move-object/from16 v6, p5

    move-object v7, v0

    move-object/from16 v0, p1

    goto :goto_5

    :catch_1
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_4
    :goto_2
    invoke-virtual {v3, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Vector;

    move-object/from16 v8, p4

    invoke-virtual {v8, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v6, 0x1

    invoke-virtual {v3, v7, v6}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    add-int/lit8 v5, v5, 0x1

    if-nez v0, :cond_5

    goto :goto_3

    :cond_5
    move v6, v9

    goto :goto_1

    :cond_6
    :goto_3
    move/from16 v2, p2

    move-object/from16 v5, p5

    move-object v6, v0

    move-object v9, v1

    move-object v7, v3

    const/4 v8, 0x0

    move-object/from16 v0, p1

    move-object/from16 v3, p3

    :goto_4
    invoke-virtual {v7}, Ljava/util/Vector;->size()I

    move-result v10

    move-object v15, v9

    move-object v9, v7

    move-object v7, v6

    move-object v6, v5

    move v5, v8

    :goto_5
    if-ge v8, v10, :cond_d

    invoke-virtual {v9, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Vector;

    :try_start_2
    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v10
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    if-eqz v7, :cond_c

    if-nez v10, :cond_7

    move-object v4, v15

    goto :goto_6

    :cond_7
    new-instance v10, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;

    const/16 v19, 0x0

    move-object v11, v10

    move-object v12, v15

    move-object v13, v15

    move-object v14, v3

    move-object v4, v15

    move-object v15, v8

    move-object/from16 v16, v0

    move-object/from16 v17, v6

    move/from16 v18, v2

    invoke-direct/range {v11 .. v19}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;-><init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;Ljava/util/Vector;Ljava/lang/String;Ljava/io/File;ZLcom/jscape/inet/ftps/Ftps$1;)V

    iget-object v11, v4, Lcom/jscape/inet/ftps/Ftps;->ai:Ljava/util/Vector;

    invoke-virtual {v11, v10}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const/4 v11, 0x0

    :cond_8
    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v12

    if-ge v11, v12, :cond_9

    :try_start_3
    iget-object v12, v4, Lcom/jscape/inet/ftps/Ftps;->aj:Ljava/util/HashMap;

    invoke-virtual {v8, v11}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    add-int/lit8 v11, v11, 0x1

    if-eqz v7, :cond_a

    if-nez v7, :cond_8

    goto :goto_6

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_9
    :goto_6
    add-int/lit8 v5, v5, 0x1

    :cond_a
    move v8, v5

    if-nez v7, :cond_b

    goto :goto_7

    :cond_b
    move-object v5, v6

    move-object v6, v7

    move-object v7, v9

    move-object v9, v4

    goto :goto_4

    :cond_c
    move-object v4, v15

    goto :goto_8

    :catch_3
    move-exception v0

    move-object v2, v0

    invoke-static {v2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_d
    move-object v4, v15

    :goto_7
    const/4 v10, 0x0

    :cond_e
    :goto_8
    iget-object v0, v4, Lcom/jscape/inet/ftps/Ftps;->ai:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v10, v0, :cond_f

    :try_start_4
    iget-object v0, v4, Lcom/jscape/inet/ftps/Ftps;->ai:Ljava/util/Vector;

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->start()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    add-int/lit8 v10, v10, 0x1

    if-eqz v7, :cond_10

    if-nez v7, :cond_e

    goto :goto_9

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_f
    :goto_9
    const/4 v10, 0x0

    :cond_10
    const/4 v0, 0x0

    const/4 v2, 0x0

    :cond_11
    iget-object v3, v4, Lcom/jscape/inet/ftps/Ftps;->ai:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_17

    iget-object v3, v4, Lcom/jscape/inet/ftps/Ftps;->ai:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;

    :try_start_5
    invoke-virtual {v3}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->error()Z

    move-result v5
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7

    if-eqz v7, :cond_18

    const/4 v6, 0x1

    if-eqz v7, :cond_13

    if-eqz v5, :cond_12

    invoke-virtual {v3}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    move v10, v6

    :cond_12
    invoke-virtual {v4}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v5

    :cond_13
    if-eqz v7, :cond_15

    if-ne v5, v6, :cond_14

    :try_start_6
    invoke-virtual {v4}, Lcom/jscape/inet/ftps/Ftps;->abortUploadThreads()V

    if-nez v7, :cond_17

    :cond_14
    invoke-virtual {v3}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->getConnected()Z

    move-result v5
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_a

    :catch_5
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_15
    :goto_a
    if-ne v5, v6, :cond_16

    const-wide/16 v8, 0x64

    :try_start_7
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    :catch_6
    add-int/lit8 v2, v2, -0x1

    :cond_16
    add-int/2addr v2, v6

    if-nez v7, :cond_11

    goto :goto_b

    :catch_7
    move-exception v0

    move-object v2, v0

    :try_start_8
    invoke-static {v2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_8

    :catch_8
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_17
    :goto_b
    move v5, v10

    :cond_18
    if-nez v5, :cond_19

    return-void

    :cond_19
    :try_start_9
    new-instance v2, Lcom/jscape/inet/ftp/FtpException;

    invoke-direct {v2, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method private a(Ljava/util/Vector;Ljava/io/File;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    const-string v0, "/"

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    :try_start_1
    invoke-virtual {v2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v1, :cond_1

    if-nez v3, :cond_0

    :try_start_2
    const-string v3, "\\"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    if-eqz v1, :cond_1

    if-nez v3, :cond_0

    :try_start_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    :cond_0
    move-object v0, p0

    goto :goto_1

    :cond_1
    move-object v0, p0

    :goto_0
    if-eqz v3, :cond_3

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-static {p2, v4, v3}, Lcom/jscape/util/Q;->a(Ljava/io/File;Ljava/io/File;Z)Ljava/lang/String;

    move-result-object v3
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    :try_start_4
    invoke-direct {v0, v3}, Lcom/jscape/inet/ftps/Ftps;->e(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    if-eqz v1, :cond_3

    if-nez v1, :cond_2

    goto :goto_2

    :cond_2
    :goto_1
    :try_start_5
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :cond_3
    :goto_2
    return-void

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    :catch_2
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    :catch_3
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method private a(Ljava/util/Vector;Ljava/io/File;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    const-string v0, "/"

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    :try_start_1
    invoke-virtual {p3, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v1, :cond_1

    if-nez v2, :cond_0

    :try_start_2
    const-string v2, "\\"

    invoke-virtual {p3, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    if-eqz v1, :cond_1

    if-nez v2, :cond_0

    :try_start_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    :cond_0
    move-object v0, p0

    goto :goto_1

    :cond_1
    move-object v0, p0

    :goto_0
    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-static {p2, v3, v2}, Lcom/jscape/util/Q;->a(Ljava/io/File;Ljava/io/File;Z)Ljava/lang/String;

    move-result-object v2
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    :try_start_4
    invoke-direct {v0, v2}, Lcom/jscape/inet/ftps/Ftps;->e(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    if-eqz v1, :cond_3

    if-nez v1, :cond_2

    goto :goto_2

    :cond_2
    :goto_1
    :try_start_5
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :cond_3
    :goto_2
    return-void

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    :catch_2
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    :catch_3
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method private a(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/FtpsClient;->getLastResponse()Lcom/jscape/inet/ftps/Response;

    move-result-object v1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_2

    :try_start_1
    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Response;->getCode()I

    move-result v0
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_3

    const/16 v1, 0xc8

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient;->readResponse()Lcom/jscape/inet/ftps/Response;
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    :try_start_3
    throw v0
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    return-void

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method private static a(Lcom/jscape/inet/ftp/FtpFile;)Z
    .locals 0

    invoke-virtual {p0}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method private static a(Ljava/io/File;)Z
    .locals 0

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method private a(Ljava/io/InputStream;Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2}, Lcom/jscape/inet/ftps/Ftps;->getRemoteFileChecksum(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    const-string v1, "."

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_2

    if-nez v1, :cond_1

    :try_start_1
    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v0, :cond_2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move p0, v1

    :goto_1
    return p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    :try_start_2
    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
.end method

.method static b(ILjava/lang/String;Ljava/lang/String;)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0, p1, p2}, Lcom/jscape/inet/ftps/Ftps;->a(ILjava/lang/String;Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method static b(Lcom/jscape/inet/ftps/Ftps;)I
    .locals 0

    iget p0, p0, Lcom/jscape/inet/ftps/Ftps;->C:I

    return p0
.end method

.method private static b(Ljava/io/File;)Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/nio/file/OpenOption;

    invoke-static {p0, v0}, Ljava/nio/file/Files;->newInputStream(Ljava/nio/file/Path;[Ljava/nio/file/OpenOption;)Ljava/io/InputStream;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-direct {v0, p0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private b(Ljava/io/InputStream;)Ljava/io/InputStreamReader;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->h:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->h:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/io/InputStreamReader;

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->h:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    return-object v0

    :cond_1
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :goto_0
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const-string v0, "\""

    const-string v1, "\\"

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    :try_start_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_a

    const/4 v4, 0x1

    const-string v5, "/"

    if-eqz v2, :cond_1

    if-eqz v3, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_0
    if-eqz v2, :cond_a

    :try_start_1
    invoke-virtual {p0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    if-nez v3, :cond_a

    :try_start_2
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_8

    if-eqz v2, :cond_3

    if-eqz v0, :cond_2

    goto/16 :goto_4

    :cond_2
    if-eqz v2, :cond_5

    :try_start_3
    invoke-virtual {p0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_3
    :goto_1
    if-nez v0, :cond_4

    :try_start_4
    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_2

    if-eqz v2, :cond_6

    if-eqz v0, :cond_5

    goto :goto_2

    :catch_2
    move-exception p0

    :try_start_5
    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    move-exception p0

    :try_start_6
    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_4
    :goto_2
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v3, v4

    invoke-virtual {p0, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_7

    :cond_5
    invoke-virtual {p0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    :cond_6
    const/4 v3, -0x1

    if-eqz v2, :cond_8

    if-eq v0, v3, :cond_7

    :try_start_7
    invoke-virtual {p0, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, v4

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_4

    return-object p0

    :catch_4
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_7
    if-eqz v2, :cond_9

    :try_start_8
    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_3

    :catch_5
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_8
    :goto_3
    if-eq v0, v3, :cond_9

    :try_start_9
    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, v4

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_6

    return-object p0

    :catch_6
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_9
    return-object p0

    :catch_7
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :catch_8
    move-exception p0

    :try_start_a
    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_9

    :catch_9
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_a
    :goto_4
    return-object p0

    :catch_a
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
.end method

.method private b(Ljava/net/Socket;)Ljava/util/Vector;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/net/Socket;)Ljava/io/InputStream;

    move-result-object v3

    new-instance v4, Ljava/io/BufferedReader;

    invoke-direct {p0, v3}, Lcom/jscape/inet/ftps/Ftps;->b(Ljava/io/InputStream;)Ljava/io/InputStreamReader;

    move-result-object v3

    invoke-direct {v4, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v1, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-nez v0, :cond_0

    :cond_1
    if-eqz v0, :cond_3

    :try_start_2
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->i()Z

    move-result v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    if-eqz v3, :cond_2

    if-eqz v0, :cond_3

    :try_start_3
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->O:Ljava/util/zip/InflaterInputStream;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->d()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_2
    invoke-static {v4}, Lcom/jscape/util/X;->a(Ljava/io/Reader;)V

    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_3
    :goto_0
    invoke-direct {p0, v2}, Lcom/jscape/inet/ftps/Ftps;->a(Z)V

    return-object v1

    :catch_3
    move-exception v1

    goto :goto_1

    :catchall_0
    move-exception v3

    move-object v4, v1

    move-object v1, v3

    goto :goto_4

    :catch_4
    move-exception v3

    move-object v4, v1

    move-object v1, v3

    :goto_1
    :try_start_6
    nop

    instance-of v3, v1, Ljavax/net/ssl/SSLHandshakeException;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v0, :cond_7

    if-eqz v3, :cond_6

    :try_start_7
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v0, :cond_5

    :try_start_8
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->i()Z

    move-result v3
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    if-eqz v3, :cond_4

    if-eqz v0, :cond_5

    :try_start_9
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->O:Ljava/util/zip/InflaterInputStream;

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->d()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    :cond_4
    invoke-static {v4}, Lcom/jscape/util/X;->a(Ljava/io/Reader;)V

    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    goto :goto_2

    :catch_5
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    :catch_6
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    :catch_7
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_5
    :goto_2
    invoke-direct {p0, v2}, Lcom/jscape/inet/ftps/Ftps;->a(Z)V

    return-object v1

    :cond_6
    const/4 v2, 0x1

    goto :goto_3

    :cond_7
    move v2, v3

    :goto_3
    :try_start_c
    new-instance v3, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    :catchall_1
    move-exception v1

    goto :goto_4

    :catch_8
    move-exception v1

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    throw v1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :goto_4
    if-eqz v0, :cond_9

    :try_start_d
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->i()Z

    move-result v3
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_9

    if-eqz v3, :cond_8

    if-eqz v0, :cond_9

    :try_start_e
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->O:Ljava/util/zip/InflaterInputStream;
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_b

    if-eqz v0, :cond_8

    :try_start_f
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->d()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_c

    :cond_8
    invoke-static {v4}, Lcom/jscape/util/X;->a(Ljava/io/Reader;)V

    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    goto :goto_5

    :catch_9
    move-exception p1

    :try_start_10
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_a

    :catch_a
    move-exception p1

    :try_start_11
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_b

    :catch_b
    move-exception p1

    :try_start_12
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_c

    :catch_c
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_9
    :goto_5
    invoke-direct {p0, v2}, Lcom/jscape/inet/ftps/Ftps;->a(Z)V

    throw v1
.end method

.method private declared-synchronized b(I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getDataPortStart()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lt p1, v0, :cond_0

    :try_start_1
    new-instance v0, Lcom/jscape/inet/util/i;

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->E:Lcom/jscape/inet/util/i;

    iget v1, v1, Lcom/jscape/inet/util/i;->a:I

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/util/i;-><init>(II)V

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->E:Lcom/jscape/inet/util/i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v1, 0x42

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_0
    monitor-exit p0

    throw p1
.end method

.method private b(Ljava/util/Vector;Ljava/io/File;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p2, v2, v1}, Lcom/jscape/util/Q;->a(Ljava/io/File;Ljava/io/File;Z)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_2

    :cond_1
    :try_start_1
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftps/Ftps;->makeLocalDir(Ljava/lang/String;)Ljava/io/File;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_3
    :goto_0
    return-void
.end method

.method private declared-synchronized b()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->n:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    monitor-exit p0

    return v0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Lcom/jscape/inet/ftp/FtpFile;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getDirListing()Ljava/util/Enumeration;

    move-result-object p1

    const/4 v2, 0x1

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/ftp/FtpFile;

    :try_start_0
    invoke-static {v3}, Lcom/jscape/inet/ftps/Ftps;->a(Lcom/jscape/inet/ftp/FtpFile;)Z

    move-result v3
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_4

    if-eqz v0, :cond_1

    if-nez v3, :cond_2

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v3

    :goto_0
    if-nez v0, :cond_3

    :cond_2
    if-nez v0, :cond_0

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    move v3, v2

    :cond_4
    return v3
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_a

    const/4 v2, 0x1

    const-string v3, "/"

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    return v2

    :cond_0
    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    :cond_1
    const/4 v4, 0x0

    if-eqz v0, :cond_5

    if-eqz v1, :cond_4

    :try_start_1
    invoke-virtual {p2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v0, :cond_5

    if-nez v1, :cond_4

    :try_start_2
    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v0, :cond_2

    if-eqz p1, :cond_3

    return v2

    :cond_2
    move v4, p1

    :cond_3
    return v4

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_4
    invoke-virtual {p2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    :cond_5
    if-eqz v0, :cond_8

    if-eqz v1, :cond_9

    :try_start_7
    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_5

    if-eqz v0, :cond_8

    if-nez v1, :cond_9

    :try_start_8
    invoke-virtual {p2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_7

    if-eqz v0, :cond_6

    if-eqz p1, :cond_7

    return v2

    :cond_6
    move v4, p1

    :cond_7
    return v4

    :catch_5
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_6

    :catch_6
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_7

    :catch_7
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_8

    :catch_8
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_9

    :catch_9
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_8
    move v4, v1

    :cond_9
    return v4

    :catch_a
    move-exception p1

    :try_start_d
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_d
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_b

    :catch_b
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method static c(Lcom/jscape/inet/ftps/Ftps;)Ljava/io/File;
    .locals 0

    iget-object p0, p0, Lcom/jscape/inet/ftps/Ftps;->ae:Ljava/io/File;

    return-object p0
.end method

.method private static c(Ljava/io/File;)Ljava/io/OutputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p0

    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private declared-synchronized c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    if-eqz v0, :cond_8

    if-eqz p2, :cond_8

    if-eqz v0, :cond_7

    if-eqz p1, :cond_8

    invoke-direct {p0, p2}, Lcom/jscape/inet/ftps/Ftps;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    new-instance v2, Ljava/util/StringTokenizer;

    invoke-direct {v2, p1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {v3, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    move-object v1, v3

    if-nez v0, :cond_3

    :cond_2
    if-nez v0, :cond_0

    :cond_3
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    :cond_4
    if-eqz v4, :cond_6

    new-instance v2, Ljava/util/StringTokenizer;

    invoke-direct {v2, p1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v0, :cond_a

    if-nez v0, :cond_5

    :cond_6
    if-nez v0, :cond_a

    goto :goto_1

    :cond_7
    move-object p2, p1

    :cond_8
    :goto_1
    if-eqz v0, :cond_b

    if-nez p2, :cond_a

    if-eqz v0, :cond_c

    if-eqz p1, :cond_a

    new-instance p2, Ljava/util/StringTokenizer;

    invoke-direct {p2, p1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p2}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result p1

    if-eqz p1, :cond_a

    invoke-virtual {p2}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_d

    if-nez v0, :cond_9

    :cond_a
    move-object p1, v1

    move-object v1, p1

    goto :goto_2

    :cond_b
    move-object p1, p2

    :cond_c
    :goto_2
    if-eqz v0, :cond_e

    :try_start_5
    const-string p2, "."

    invoke-virtual {p1, p2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p1
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz p1, :cond_d

    const/4 p1, 0x0

    :try_start_6
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    invoke-virtual {v1, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :cond_d
    move-object p1, v1

    goto :goto_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_e
    :goto_3
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private c(Ljava/lang/String;)Ljava/util/Vector;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    const-string v0, "/"

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getDirListing()Ljava/util/Enumeration;

    move-result-object v3

    :try_start_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_5

    if-eqz v2, :cond_1

    if-nez v4, :cond_0

    :try_start_1
    const-string v4, "\\"

    invoke-virtual {p1, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_6

    if-eqz v2, :cond_1

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    move-object v0, p0

    goto/16 :goto_3

    :cond_1
    move-object v0, p0

    :goto_0
    if-eqz v4, :cond_7

    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jscape/inet/ftp/FtpFile;

    :try_start_2
    invoke-virtual {v4}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v5
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_3

    if-eqz v2, :cond_2

    if-eqz v5, :cond_5

    :try_start_3
    invoke-static {v4}, Lcom/jscape/inet/ftps/Ftps;->a(Lcom/jscape/inet/ftp/FtpFile;)Z

    move-result v5
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_4

    :cond_2
    if-eqz v2, :cond_3

    if-nez v5, :cond_5

    if-eqz v2, :cond_4

    :try_start_4
    invoke-direct {v0, v4}, Lcom/jscape/inet/ftps/Ftps;->b(Lcom/jscape/inet/ftp/FtpFile;)Z

    move-result v5
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    if-eqz v5, :cond_4

    :try_start_5
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_1

    if-nez v2, :cond_5

    goto :goto_2

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_4
    :goto_2
    iget-object v5, v0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/jscape/inet/ftps/Ftps;->c(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0, v5}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    :cond_5
    if-nez v2, :cond_6

    goto :goto_4

    :cond_6
    :goto_3
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    goto :goto_0

    :catch_3
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_7
    :goto_4
    return-object v1

    :catch_5
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_6

    :catch_6
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_7

    :catch_7
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method private c()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->N:Ljava/util/zip/DeflaterOutputStream;

    invoke-virtual {v0}, Ljava/util/zip/DeflaterOutputStream;->finish()V

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->N:Ljava/util/zip/DeflaterOutputStream;

    invoke-virtual {v0}, Ljava/util/zip/DeflaterOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->N:Ljava/util/zip/DeflaterOutputStream;

    return-void
.end method

.method private declared-synchronized c(I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getDataPortEnd()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gt p1, v0, :cond_0

    :try_start_1
    new-instance v0, Lcom/jscape/inet/util/i;

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->E:Lcom/jscape/inet/util/i;

    iget v1, v1, Lcom/jscape/inet/util/i;->b:I

    invoke-direct {v0, p1, v1}, Lcom/jscape/inet/util/i;-><init>(II)V

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->E:Lcom/jscape/inet/util/i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v1, 0x30

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_0
    monitor-exit p0

    throw p1
.end method

.method static d(Lcom/jscape/inet/ftps/Ftps;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    return-object p0
.end method

.method private d()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->O:Ljava/util/zip/InflaterInputStream;

    invoke-virtual {v0}, Ljava/util/zip/InflaterInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->O:Ljava/util/zip/InflaterInputStream;

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    :try_start_0
    const-string v2, ""

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftps/Ftps;->getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/ftp/FtpFile;

    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v5
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_5

    if-eqz v1, :cond_2

    if-nez v5, :cond_1

    :try_start_2
    invoke-virtual {p0, v4}, Lcom/jscape/inet/ftps/Ftps;->deleteFile(Ljava/lang/String;)V

    if-nez v1, :cond_3

    :cond_1
    invoke-static {v3}, Lcom/jscape/inet/ftps/Ftps;->a(Lcom/jscape/inet/ftp/FtpFile;)Z

    move-result v5
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    goto :goto_2

    :cond_2
    :goto_0
    if-nez v5, :cond_3

    const/4 v3, 0x1

    :try_start_3
    invoke-virtual {p0, v4, v3}, Lcom/jscape/inet/ftps/Ftps;->deleteDir(Ljava/lang/String;Z)V
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_3
    :goto_1
    if-nez v1, :cond_0

    goto :goto_3

    :catch_2
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_3
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_4
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :goto_2
    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_4
    :goto_3
    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/String;)Z

    move-result v5

    :cond_5
    if-nez v5, :cond_6

    :try_start_9
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->deleteDir(Ljava/lang/String;)V
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_5

    goto :goto_4

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_6
    :goto_4
    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    throw p1
.end method

.method private e()V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    :try_start_2
    invoke-virtual {v1}, Lcom/jscape/inet/ftps/FtpsClient;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method private e(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->e(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    :cond_1
    if-eqz v0, :cond_2

    if-eqz p1, :cond_3

    :cond_2
    :try_start_2
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->makeDir(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    :cond_3
    :goto_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    return-void

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method static e(Lcom/jscape/inet/ftps/Ftps;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/jscape/inet/ftps/Ftps;->z:Z

    return p0
.end method

.method private f()V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v2, 0x3d

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method private declared-synchronized f(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljavax/activation/MimetypesFileTypeMap;

    invoke-direct {v0}, Ljavax/activation/MimetypesFileTypeMap;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1}, Ljavax/activation/MimetypesFileTypeMap;->getContentType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v0, Ljavax/activation/MimeType;

    invoke-direct {v0, p1}, Ljavax/activation/MimeType;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljavax/activation/MimeType;->getPrimaryType()Ljava/lang/String;

    move-result-object p1

    sget-object v0, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v2, 0x1c

    aget-object v0, v0, v2

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x2

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    :try_start_2
    iget v2, p0, Lcom/jscape/inet/ftps/Ftps;->G:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_0

    if-ne v2, v0, :cond_1

    const/4 v2, 0x1

    :try_start_3
    invoke-direct {p0, v2}, Lcom/jscape/inet/ftps/Ftps;->a(I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v1, :cond_4

    goto :goto_0

    :cond_0
    move p1, v2

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_2
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    if-nez p1, :cond_4

    if-eqz v1, :cond_3

    :try_start_7
    iget p1, p0, Lcom/jscape/inet/ftps/Ftps;->G:I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    :catch_3
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    if-eq p1, v0, :cond_4

    :cond_3
    invoke-direct {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->a(I)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_4
    monitor-exit p0

    return-void

    :catch_4
    move-exception p1

    :try_start_9
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method static f(Lcom/jscape/inet/ftps/Ftps;)Z
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->b()Z

    move-result p0

    return p0
.end method

.method private declared-synchronized g(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v1, v1, -0x1

    :cond_0
    if-lez v1, :cond_2

    add-int/lit8 v2, v1, -0x1

    if-eqz v0, :cond_3

    :try_start_1
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/16 v3, 0x2f

    if-ne v2, v3, :cond_1

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v1, -0x1

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    move v2, v1

    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private g()V
    .locals 4

    const-string v0, "."

    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x5c

    const/16 v3, 0x2f

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->k:Ljava/io/File;

    return-void
.end method

.method private h(Ljava/lang/String;)Lcom/jscape/util/m/f;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jscape/inet/ftps/Ftps;->c:Lcom/jscape/util/m/g;

    invoke-interface {v0, p1}, Lcom/jscape/util/m/g;->a(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    :try_start_1
    sget-object p1, Lcom/jscape/util/m/f;->a:Lcom/jscape/util/m/f;

    :goto_0
    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-direct {v0, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private h()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient;->quit()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private i()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/ftps/Ftps;->L:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/inet/ftps/Ftps;->M:Z

    :cond_0
    if-eqz v0, :cond_2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move v0, v1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method private j()Z
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->R:Ljava/lang/String;

    if-eqz v0, :cond_0

    if-nez v1, :cond_2

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->w:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    if-eqz v1, :cond_3

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->w:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_1
    :goto_1
    :try_start_2
    const-string v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v0, :cond_4

    const/4 v0, -0x1

    if-eq v1, v0, :cond_3

    :cond_2
    const/4 v1, 0x1

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    :goto_2
    move v0, v1

    :goto_3
    return v0

    :catch_2
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method private k()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/ftps/Ftps;->A:Z
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->j()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jscape/inet/ftps/FtpsClient;->passive(Z)V

    if-nez v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->E:Lcom/jscape/inet/util/i;

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ftps/FtpsClient;->dataPort(Lcom/jscape/inet/util/i;)V

    :cond_1
    return-void
.end method

.method private l()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    sget-object v0, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v1, 0x25

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->isFeatureSupported(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v2, 0x14

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ftps/FtpsClient;->sendCommand(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->readResponse()Ljava/lang/String;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->M:Z

    :cond_1
    return-void
.end method

.method private m()V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->l:Lcom/jscape/inet/ftp/FtpFileParser;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getSystemType()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/StringTokenizer;

    invoke-direct {v2, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6

    :try_start_2
    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v3, 0x44

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    if-eqz v0, :cond_2

    if-eqz v2, :cond_1

    :try_start_3
    new-instance v2, Lcom/jscape/inet/ftp/NTParser;

    invoke-direct {v2}, Lcom/jscape/inet/ftp/NTParser;-><init>()V

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftps/Ftps;->setFtpFileParser(Lcom/jscape/inet/ftp/FtpFileParser;)V

    if-nez v0, :cond_6

    :cond_1
    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v3, 0x3e

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5

    :cond_2
    if-eqz v0, :cond_4

    if-eqz v2, :cond_3

    :try_start_4
    new-instance v2, Lcom/jscape/inet/ftp/VMSParser;

    invoke-direct {v2}, Lcom/jscape/inet/ftp/VMSParser;-><init>()V

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftps/Ftps;->setFtpFileParser(Lcom/jscape/inet/ftp/FtpFileParser;)V

    if-nez v0, :cond_6

    :cond_3
    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v3, 0x33

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6

    :cond_4
    :goto_0
    if-eqz v2, :cond_5

    :try_start_6
    new-instance v1, Lcom/jscape/inet/ftp/AS400Parser;

    invoke-direct {v1}, Lcom/jscape/inet/ftp/AS400Parser;-><init>()V

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftps/Ftps;->setFtpFileParser(Lcom/jscape/inet/ftp/FtpFileParser;)V

    if-nez v0, :cond_6

    :cond_5
    new-instance v0, Lcom/jscape/inet/ftp/UnixParser;

    invoke-direct {v0}, Lcom/jscape/inet/ftp/UnixParser;-><init>()V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->setFtpFileParser(Lcom/jscape/inet/ftp/FtpFileParser;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    :catch_3
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    move-exception v0

    :try_start_9
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5

    :catch_5
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6

    :catch_6
    new-instance v0, Lcom/jscape/inet/ftp/UnixParser;

    invoke-direct {v0}, Lcom/jscape/inet/ftp/UnixParser;-><init>()V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->setFtpFileParser(Lcom/jscape/inet/ftp/FtpFileParser;)V

    :cond_6
    :goto_1
    return-void
.end method


# virtual methods
.method public abortDownloadThread(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->ah:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;

    invoke-virtual {p1}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->interruptTransfer()V

    return-void
.end method

.method public abortDownloadThreads()V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    iget-object v2, p0, Lcom/jscape/inet/ftps/Ftps;->ag:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/jscape/inet/ftps/Ftps;->ag:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;

    invoke-virtual {v2}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->cancelTransfer()V

    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public abortUploadThread(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->aj:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;

    invoke-virtual {p1}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->interruptTransfer()V

    return-void
.end method

.method public abortUploadThreads()V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    iget-object v2, p0, Lcom/jscape/inet/ftps/Ftps;->ai:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    iget-object v2, p0, Lcom/jscape/inet/ftps/Ftps;->ai:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;

    invoke-virtual {v2}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->cancelTransfer()V

    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public addFtpListener(Lcom/jscape/inet/ftp/FtpListener;)V
    .locals 1

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->n:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    monitor-enter p0

    :try_start_0
    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->n:Ljava/util/LinkedList;

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public addFtpsCertificateVerifier(Lcom/jscape/inet/ftps/FtpsCertificateVerifier;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->setFtpsCertificateVerifier(Lcom/jscape/inet/ftps/FtpsCertificateVerifier;)V

    return-void
.end method

.method public declared-synchronized changePassword(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x34

    aget-object p1, v1, p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\""

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->issueCommandCheck(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized checksum(Ljava/io/File;Ljava/lang/String;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_5

    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/nio/file/OpenOption;

    invoke-static {p1, v1}, Ljava/nio/file/Files;->newInputStream(Ljava/nio/file/Path;[Ljava/nio/file/OpenOption;)Ljava/io/InputStream;

    move-result-object p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    :try_start_2
    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/io/InputStream;Ljava/lang/String;)Z

    move-result p2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz p1, :cond_0

    :try_start_3
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_5

    :cond_0
    :goto_0
    monitor-exit p0

    return p2

    :catchall_1
    move-exception p2

    :try_start_5
    throw p2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception v1

    if-eqz p1, :cond_1

    :try_start_6
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v2

    :try_start_7
    invoke-virtual {p2, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    goto :goto_1

    :catchall_4
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    throw v1
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    :catch_0
    move-exception p1

    :try_start_9
    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    :catchall_5
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized checksum([BLjava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {p0, v0, p2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/io/InputStream;Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public clearCommandChannel()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient;->clear()V

    return-void
.end method

.method public declared-synchronized clearProxySettings()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0, v0}, Lcom/jscape/inet/ftps/Ftps;->setProxyAuthentication(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x438

    invoke-virtual {p0, v0, v1}, Lcom/jscape/inet/ftps/Ftps;->setProxyHost(Ljava/lang/String;I)V

    sget-object v0, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v1, 0x3c

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->setProxyType(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public close()V
    .locals 0

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->disconnect()V

    return-void
.end method

.method public declared-synchronized connect()Lcom/jscape/inet/ftps/Ftps;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->connect(Z)Lcom/jscape/inet/ftps/Ftps;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized connect(Z)Lcom/jscape/inet/ftps/Ftps;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v7, p0

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->isConnected()Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_1

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_2
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->disconnect()V

    :cond_1
    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps;->R:Ljava/lang/String;

    if-nez v1, :cond_2

    new-instance v1, Lcom/jscape/inet/util/ConnectionParameters;

    iget-object v2, v7, Lcom/jscape/inet/ftps/Ftps;->s:Ljava/lang/String;

    iget v3, v7, Lcom/jscape/inet/ftps/Ftps;->H:I

    iget v4, v7, Lcom/jscape/inet/ftps/Ftps;->I:I

    int-to-long v4, v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/jscape/inet/util/ConnectionParameters;-><init>(Ljava/lang/String;IJ)V

    :goto_1
    move-object v3, v1

    goto :goto_2

    :cond_2
    new-instance v1, Lcom/jscape/inet/util/ConnectionParameters;

    iget-object v9, v7, Lcom/jscape/inet/ftps/Ftps;->s:Ljava/lang/String;

    iget v10, v7, Lcom/jscape/inet/ftps/Ftps;->H:I

    iget v2, v7, Lcom/jscape/inet/ftps/Ftps;->I:I

    int-to-long v11, v2

    iget-object v13, v7, Lcom/jscape/inet/ftps/Ftps;->R:Ljava/lang/String;

    iget v14, v7, Lcom/jscape/inet/ftps/Ftps;->S:I

    iget-object v15, v7, Lcom/jscape/inet/ftps/Ftps;->P:Ljava/lang/String;

    iget-object v2, v7, Lcom/jscape/inet/ftps/Ftps;->Q:Ljava/lang/String;

    iget-object v3, v7, Lcom/jscape/inet/ftps/Ftps;->T:Ljava/lang/String;

    move-object v8, v1

    move-object/from16 v16, v2

    move-object/from16 v17, v3

    invoke-direct/range {v8 .. v17}, Lcom/jscape/inet/util/ConnectionParameters;-><init>(Ljava/lang/String;IJLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :goto_2
    :try_start_3
    iget-boolean v1, v7, Lcom/jscape/inet/ftps/Ftps;->J:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/jscape/inet/util/ConnectionParameters;->setTcpNoDelay(Ljava/lang/Boolean;)V

    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps;->p:Lcom/jscape/util/j/b;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v5, 0x29

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v7, Lcom/jscape/inet/ftps/Ftps;->s:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v5, 0xc

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, v7, Lcom/jscape/inet/ftps/Ftps;->H:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jscape/util/j/b;->fine(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_3

    :try_start_4
    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps;->q:Ljavax/net/ssl/SSLContext;

    if-eqz v1, :cond_3

    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps;->q:Ljavax/net/ssl/SSLContext;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_a
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    :cond_3
    :try_start_5
    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/ContextFactory;->getContext()Ljavax/net/ssl/SSLContext;

    move-result-object v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :goto_3
    move-object v4, v1

    :try_start_6
    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps;->j:Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;

    iget-object v5, v7, Lcom/jscape/inet/ftps/Ftps;->y:[Ljava/lang/String;

    iget-object v6, v7, Lcom/jscape/inet/ftps/Ftps;->p:Lcom/jscape/util/j/b;

    move-object/from16 v2, p0

    invoke-interface/range {v1 .. v6}, Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;->createClient(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/util/ConnectionParameters;Ljavax/net/ssl/SSLContext;[Ljava/lang/String;Ljava/util/logging/Logger;)Lcom/jscape/inet/ftps/FtpsClient;

    move-result-object v1

    iput-object v1, v7, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    iget v2, v7, Lcom/jscape/inet/ftps/Ftps;->U:I

    invoke-virtual {v1, v2}, Lcom/jscape/inet/ftps/FtpsClient;->setSendBufferSize(I)V

    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    iget v2, v7, Lcom/jscape/inet/ftps/Ftps;->V:I

    invoke-virtual {v1, v2}, Lcom/jscape/inet/ftps/FtpsClient;->setReceiveBufferSize(I)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_7
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v0, :cond_4

    :try_start_7
    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps;->i:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    iget-object v2, v7, Lcom/jscape/inet/ftps/Ftps;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/jscape/inet/ftps/FtpsClient;->setPortAddress(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_8
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_4
    :try_start_8
    new-instance v1, Lcom/jscape/inet/ftp/FtpConnectedEvent;

    iget-object v2, v7, Lcom/jscape/inet/ftps/Ftps;->s:Ljava/lang/String;

    invoke-direct {v1, v7, v2}, Lcom/jscape/inet/ftp/FtpConnectedEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v0, :cond_6

    if-eqz p1, :cond_5

    :try_start_9
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->login()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :cond_5
    :try_start_a
    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->a()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :try_start_b
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catch_1
    :cond_6
    if-eqz v0, :cond_7

    :try_start_c
    iget-boolean v1, v7, Lcom/jscape/inet/ftps/Ftps;->L:Z
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    if-eqz v1, :cond_7

    :try_start_d
    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->l()V

    if-nez v0, :cond_8

    goto :goto_4

    :catch_2
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catch_3
    move-exception v0

    :try_start_e
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_4
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :catch_4
    move-exception v0

    :try_start_f
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_7
    :goto_4
    const/4 v0, 0x0

    iput-boolean v0, v7, Lcom/jscape/inet/ftps/Ftps;->M:Z
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :cond_8
    monitor-exit p0

    return-object v7

    :catch_5
    move-exception v0

    :try_start_10
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :catch_6
    move-exception v0

    :try_start_11
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :catch_7
    move-exception v0

    :try_start_12
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_8
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :catch_8
    move-exception v0

    :try_start_13
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :catch_9
    move-exception v0

    :try_start_14
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_a
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    :catch_a
    move-exception v0

    :try_start_15
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized deleteDir(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ftps/Ftps;->deleteDir(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized deleteDir(Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    :try_start_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->d(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v0, :cond_2

    :cond_0
    :try_start_3
    iget-object p2, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {p2, p1}, Lcom/jscape/inet/ftps/FtpsClient;->removeDirectory(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    :try_start_4
    new-instance p2, Lcom/jscape/inet/ftp/FtpDeleteDirEvent;

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    invoke-direct {p2, p0, p1, v0}, Lcom/jscape/inet/ftp/FtpDeleteDirEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_2
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized deleteFile(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->delete(Ljava/lang/String;)V

    new-instance v0, Lcom/jscape/inet/ftp/FtpDeleteFileEvent;

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/inet/ftp/FtpDeleteFileEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized disconnect()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_2

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    if-eqz v0, :cond_1

    :try_start_3
    invoke-virtual {v1}, Lcom/jscape/inet/ftps/FtpsClient;->isClosed()Z

    move-result v0
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v0, :cond_2

    :try_start_4
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->h()V

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    invoke-virtual {v1}, Lcom/jscape/inet/ftps/FtpsClient;->close()V

    new-instance v0, Lcom/jscape/inet/ftp/FtpDisconnectedEvent;

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->s:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/jscape/inet/ftp/FtpDisconnectedEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catch_2
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_3
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized download(Ljava/lang/String;)Ljava/io/File;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p1}, Lcom/jscape/inet/ftps/Ftps;->download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->c(Ljava/io/File;)Ljava/io/OutputStream;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/16 v2, 0x10

    :try_start_1
    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->ae:Ljava/io/File;

    invoke-virtual {p0, v1, p2}, Lcom/jscape/inet/ftps/Ftps;->download(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getPreserveDownloadTimestamp()Z

    move-result v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz p1, :cond_0

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {p0, p2}, Lcom/jscape/inet/ftps/Ftps;->getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/io/File;->setLastModified(J)Z

    move-result v1
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    if-eqz v1, :cond_2

    :try_start_5
    iget-object p2, p0, Lcom/jscape/inet/ftps/Ftps;->p:Lcom/jscape/util/j/b;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v4, 0x26

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v4, 0xb

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, "]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/jscape/util/j/b;->fine(Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :cond_1
    :goto_1
    if-nez p1, :cond_3

    :cond_2
    :try_start_7
    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->p:Lcom/jscape/util/j/b;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v2, 0x3f

    aget-object v1, v1, v2

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jscape/util/j/b;->fine(Ljava/lang/String;)V
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2

    :catch_2
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :cond_3
    :goto_2
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v3

    goto :goto_3

    :catch_3
    move-exception v3

    :try_start_9
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz p1, :cond_4

    if-nez v4, :cond_4

    :try_start_a
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_4
    :try_start_b
    throw v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catch_4
    move-exception v3

    :try_start_c
    invoke-static {v3}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v3

    throw v3
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :catch_5
    move-exception v3

    :try_start_d
    invoke-static {v3}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v3

    throw v3
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :goto_3
    :try_start_e
    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getPreserveDownloadTimestamp()Z

    move-result v1
    :try_end_e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_e .. :try_end_e} :catch_8
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    if-eqz p1, :cond_5

    if-eqz v1, :cond_8

    :try_start_f
    invoke-virtual {p0, p2}, Lcom/jscape/inet/ftps/Ftps;->getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/io/File;->setLastModified(J)Z

    move-result v1
    :try_end_f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_f .. :try_end_f} :catch_9
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    :cond_5
    if-eqz p1, :cond_6

    if-eqz v1, :cond_7

    :try_start_10
    iget-object p2, p0, Lcom/jscape/inet/ftps/Ftps;->p:Lcom/jscape/util/j/b;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object v4, v4, v2

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v5, 0x43

    aget-object v4, v4, v5

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v4, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, "]"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/jscape/util/j/b;->fine(Ljava/lang/String;)V
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    goto :goto_4

    :catch_6
    move-exception p1

    :try_start_11
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    :cond_6
    :goto_4
    if-nez p1, :cond_8

    :cond_7
    :try_start_12
    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->p:Lcom/jscape/util/j/b;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v1, 0x27

    aget-object v0, v0, v1

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jscape/util/j/b;->fine(Ljava/lang/String;)V
    :try_end_12
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_12 .. :try_end_12} :catch_7
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    goto :goto_5

    :catch_7
    move-exception p1

    :try_start_13
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_8
    :goto_5
    throw v3
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    :catch_8
    move-exception p1

    :try_start_14
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_14
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_14 .. :try_end_14} :catch_9
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    :catch_9
    move-exception p1

    :try_start_15
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized download(Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/io/OutputStream;Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized downloadDir(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    sget-object v3, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v4, 0x20

    aget-object v3, v3, v4

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->makeLocalDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getDirListing()Ljava/util/Enumeration;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v3, :cond_5

    if-eqz v1, :cond_6

    if-eqz v1, :cond_6

    :try_start_4
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v3
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-nez v3, :cond_5

    :try_start_5
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/ftp/FtpFile;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v4
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v1, :cond_3

    if-nez v4, :cond_2

    :try_start_7
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/jscape/inet/ftps/Ftps;->download(Ljava/lang/String;)Ljava/io/File;

    if-nez v1, :cond_4

    :cond_2
    invoke-static {v3}, Lcom/jscape/inet/ftps/Ftps;->a(Lcom/jscape/inet/ftp/FtpFile;)Z

    move-result v4
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    goto :goto_2

    :cond_3
    :goto_0
    if-nez v4, :cond_4

    :try_start_8
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftps/Ftps;->downloadDir(Ljava/lang/String;)V
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :cond_4
    :goto_1
    if-nez v1, :cond_1

    goto :goto_3

    :catch_2
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_3
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :goto_2
    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :cond_5
    :goto_3
    :try_start_d
    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    monitor-exit p0

    return-void

    :catch_5
    move-exception p1

    :try_start_e
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :catchall_0
    move-exception p1

    :try_start_f
    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V

    throw p1
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized downloadDir(Ljava/lang/String;IIZ)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v2

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v4, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/4 v5, 0x5

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftps/Ftps;->makeLocalDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v3

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->c(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v4

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v4, v5}, Lcom/jscape/inet/ftps/Ftps;->b(Ljava/util/Vector;Ljava/io/File;)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v5, 0x0

    move v6, v5

    :cond_1
    if-gt v5, p2, :cond_8

    :try_start_3
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v7
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_6

    if-nez v7, :cond_8

    :try_start_4
    invoke-direct {p0, p1, v4, p4, v3}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/String;Ljava/io/File;ZLjava/util/Vector;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/4 v7, 0x1

    goto :goto_2

    :catch_0
    move-exception v7

    if-eqz v0, :cond_3

    if-ge v5, p2, :cond_2

    goto :goto_0

    :cond_2
    :try_start_5
    new-instance p1, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_3
    :goto_0
    if-eqz v0, :cond_4

    if-ge v5, p2, :cond_5

    const/16 v7, 0x3e8

    move v8, p3

    goto :goto_1

    :cond_4
    move v7, p2

    move v8, v5

    :goto_1
    mul-int/2addr v8, v7

    int-to-long v7, v8

    :try_start_7
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catch_2
    :cond_5
    :try_start_8
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->disconnect()V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->connect()Lcom/jscape/inet/ftps/Ftps;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3

    :cond_6
    :goto_2
    move v6, v7

    :goto_3
    if-eqz v6, :cond_7

    :try_start_9
    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-nez v0, :cond_8

    goto :goto_4

    :catch_3
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_4
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_7
    :goto_4
    add-int/lit8 v5, v5, 0x1

    if-nez v0, :cond_1

    goto :goto_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_8
    :goto_5
    monitor-exit p0

    return-void

    :catch_6
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized downloadDir(Ljava/lang/String;IIZI)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v7, p0

    move-object/from16 v0, p1

    move/from16 v8, p2

    move/from16 v9, p5

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v10, :cond_0

    if-eqz v9, :cond_1

    :cond_0
    const/4 v11, 0x1

    if-eqz v10, :cond_2

    if-ne v9, v11, :cond_2

    :cond_1
    :try_start_1
    invoke-virtual/range {p0 .. p4}, Lcom/jscape/inet/ftps/Ftps;->downloadDir(Ljava/lang/String;IIZ)V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_2
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_2
    if-ltz v9, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p0 .. p1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v13

    invoke-static/range {p1 .. p1}, Lcom/jscape/inet/ftps/Ftps;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v10, :cond_3

    :try_start_3
    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v7, v1}, Lcom/jscape/inet/ftps/Ftps;->makeLocalDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v7, v2}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_3
    :goto_0
    :try_start_5
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p1}, Lcom/jscape/inet/ftps/Ftps;->getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/io/File;->setLastModified(J)Z

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v6, 0x11

    aget-object v6, v5, v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x1e

    aget-object v2, v5, v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :catch_2
    :try_start_6
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v5, 0x1f

    aget-object v5, v4, v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x28

    aget-object v1, v4, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_4
    :goto_1
    invoke-virtual/range {p0 .. p1}, Lcom/jscape/inet/ftps/Ftps;->getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v14

    invoke-direct/range {p0 .. p1}, Lcom/jscape/inet/ftps/Ftps;->c(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v1, v2}, Lcom/jscape/inet/ftps/Ftps;->b(Ljava/util/Vector;Ljava/io/File;)V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v16
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const/4 v0, 0x0

    move v6, v0

    move/from16 v17, v6

    :goto_2
    if-gt v6, v8, :cond_c

    :try_start_7
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v0
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v10, :cond_9

    if-nez v0, :cond_c

    move-object/from16 v1, p0

    move-object v2, v15

    move-object/from16 v3, v16

    move/from16 v4, p4

    move-object v5, v14

    move v11, v6

    move/from16 v6, p5

    :try_start_8
    invoke-direct/range {v1 .. v6}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/String;Ljava/io/File;ZLjava/util/Vector;I)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    const/4 v0, 0x1

    goto :goto_5

    :catch_3
    move-exception v0

    move-object v1, v0

    :try_start_9
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->abortDownloadThreads()V
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-eqz v10, :cond_6

    if-ge v11, v8, :cond_5

    goto :goto_3

    :cond_5
    :try_start_a
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_6
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_6
    :goto_3
    if-eqz v10, :cond_7

    if-ge v11, v8, :cond_8

    const/16 v0, 0x3e8

    move/from16 v6, p3

    goto :goto_4

    :cond_7
    move v0, v8

    move v6, v11

    :goto_4
    mul-int/2addr v6, v0

    int-to-long v0, v6

    :try_start_b
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catch_4
    :cond_8
    :try_start_c
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->disconnect()V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->connect()Lcom/jscape/inet/ftps/Ftps;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_6

    :catch_5
    move-exception v0

    move-object v1, v0

    :try_start_d
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catch_6
    move-exception v0

    :try_start_e
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :cond_9
    move v11, v6

    :goto_5
    move/from16 v17, v0

    :goto_6
    if-eqz v17, :cond_a

    :try_start_f
    invoke-virtual {v7, v12}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual {v7, v13}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V
    :try_end_f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_f .. :try_end_f} :catch_7
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    if-nez v10, :cond_c

    goto :goto_7

    :catch_7
    move-exception v0

    :try_start_10
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_8
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :catch_8
    move-exception v0

    :try_start_11
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_a
    :goto_7
    add-int/lit8 v6, v11, 0x1

    if-nez v10, :cond_b

    goto :goto_8

    :cond_b
    const/4 v11, 0x1

    goto :goto_2

    :catch_9
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :cond_c
    :goto_8
    monitor-exit p0

    return-void

    :cond_d
    :try_start_12
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v3, 0x1d

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v3, 0x16

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_12
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_12 .. :try_end_12} :catch_a
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :catch_a
    move-exception v0

    :try_start_13
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized downloadDir(Ljava/lang/String;IZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/jscape/inet/ftps/Ftps;->downloadDir(Ljava/lang/String;IIZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public downloadDir(Ljava/lang/String;IZI)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/jscape/inet/ftps/Ftps;->downloadDir(Ljava/lang/String;IIZI)V

    return-void
.end method

.method protected fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->n:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ftp/FtpListener;

    invoke-virtual {p1, v2}, Lcom/jscape/inet/ftp/FtpEvent;->accept(Lcom/jscape/inet/ftp/FtpListener;)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public declared-synchronized getAccount()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->u:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAutoDetectIpv6()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->aa:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getBlockTransferSize()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/jscape/inet/ftps/Ftps;->C:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCharacterEncoding()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->r:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCompression()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->L:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getConnectBeforeCommand()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient;->getConnectBeforeCommand()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getConnectionType()Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->j:Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getContext()Ljavax/net/ssl/SSLContext;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->q:Ljavax/net/ssl/SSLContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getContextAlgorithm()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/ContextFactory;->getAlgorithm()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getContextAlgorithmProvider()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/ContextFactory;->getAlgorithmProvider()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getContextKeystoreProvider()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/ContextFactory;->getKeystoreProvider()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getContextProtocol()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/ContextFactory;->getProtocol()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getContextProtocolProvider()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/ContextFactory;->getProtocolProvider()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDataPort()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/jscape/inet/ftps/Ftps;->D:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDataPortEnd()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->E:Lcom/jscape/inet/util/i;

    iget v0, v0, Lcom/jscape/inet/util/i;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDataPortStart()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->E:Lcom/jscape/inet/util/i;

    iget v0, v0, Lcom/jscape/inet/util/i;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDebug()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->p:Lcom/jscape/util/j/b;

    invoke-virtual {v0}, Lcom/jscape/util/j/b;->getLevel()Ljava/util/logging/Level;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDebugStream()Ljava/io/PrintStream;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->p:Lcom/jscape/util/j/b;

    invoke-virtual {v0}, Lcom/jscape/util/j/b;->a()Ljava/io/OutputStream;

    move-result-object v0

    check-cast v0, Ljava/io/PrintStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDir()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient;->printWorkingDirectory()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v1, Lcom/jscape/inet/ftp/FtpException;

    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v3, 0x36

    aget-object v2, v2, v3

    invoke-direct {v1, v2, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDirListing()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/StringReader;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->getDirListingAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    new-instance p1, Ljava/io/BufferedReader;

    invoke-direct {p1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v2, p0, Lcom/jscape/inet/ftps/Ftps;->l:Lcom/jscape/inet/ftp/FtpFileParser;

    invoke-interface {v2, p1}, Lcom/jscape/inet/ftp/FtpFileParser;->parse(Ljava/io/BufferedReader;)Ljava/util/Enumeration;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_1

    :try_start_2
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_2

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    new-instance p1, Lcom/jscape/inet/ftp/FtpListingEvent;

    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    invoke-direct {p1, p0, v0}, Lcom/jscape/inet/ftp/FtpListingEvent;-><init>(Ljava/lang/Object;Ljava/util/Enumeration;)V

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V

    :cond_2
    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object p1

    :catch_1
    move-exception p1

    :try_start_4
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getDirListingAsString()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->getDirListingAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDirListingAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->k()V

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v1, p1}, Lcom/jscape/inet/ftps/FtpsClient;->list(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->b(Ljava/net/Socket;)Ljava/util/Vector;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v2, 0x0

    :cond_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v2, v3, :cond_1

    :try_start_1
    invoke-virtual {p1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    sget-object v3, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v4, 0x1b

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getDirListingRegex(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->h(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getDirListing()Ljava/util/Enumeration;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/ftp/FtpFile;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Lcom/jscape/util/m/f;->a(Ljava/lang/String;)Z

    move-result v4
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    if-eqz v4, :cond_1

    :try_start_2
    invoke-virtual {v1, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_1
    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_3
    monitor-exit p0

    return-object v2

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getDirListingRegexAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->getDirListingRegex(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ftp/FtpFile;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v3, 0x2a

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_1

    if-nez v1, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getDiskEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getEnabledCiphers()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->y:[Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getErrorOnSizeCommand()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->K:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getFeatures()Ljava/util/Enumeration;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ftp/FeatRequest;

    invoke-direct {v0}, Lcom/jscape/inet/ftp/FeatRequest;-><init>()V

    new-instance v1, Lcom/jscape/inet/ftps/FtpsProtocolInterpreter;

    iget-object v2, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-direct {v1, v2}, Lcom/jscape/inet/ftps/FtpsProtocolInterpreter;-><init>(Lcom/jscape/inet/ftps/FtpsClient;)V

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ftp/FeatRequest;->execute(Lcom/jscape/inet/ftp/ClientProtocolInterpreter;)V

    new-instance v1, Ljava/util/Vector;

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FeatRequest;->getFeatures()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/Vector;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v2, 0x37

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->issueCommandCheck(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/util/StringTokenizer;

    invoke-direct {v0, p1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    new-instance v2, Ljava/text/SimpleDateFormat;

    const/16 v3, 0x2d

    aget-object v1, v1, v3

    invoke-direct {v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :catch_0
    :try_start_2
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-direct {v0, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getFilesize(Ljava/lang/String;)J
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v2, 0x17

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->issueCommandCheck(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/util/StringTokenizer;

    invoke-direct {v0, p1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-wide v0

    :catch_0
    :try_start_2
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-direct {v0, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getFtpFileParser()Lcom/jscape/inet/ftp/FtpFileParser;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->l:Lcom/jscape/inet/ftp/FtpFileParser;

    return-object v0
.end method

.method public getFtpsCertificateVerifier()Lcom/jscape/inet/ftps/FtpsCertificateVerifier;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->o:Lcom/jscape/inet/ftps/FtpsCertificateVerifier;

    return-object v0
.end method

.method public declared-synchronized getHostname()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->s:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getInputStream(Ljava/lang/String;J)Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->k()V

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/jscape/inet/ftps/FtpsClient;->restart(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    iget-object p2, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {p2, p1}, Lcom/jscape/inet/ftps/FtpsClient;->retrieve(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object p1

    new-instance p2, Lcom/jscape/inet/ftps/Ftps$DataConnectionInputStream;

    const/4 p3, 0x0

    invoke-direct {p2, p0, p1, p3}, Lcom/jscape/inet/ftps/Ftps$DataConnectionInputStream;-><init>(Lcom/jscape/inet/ftps/Ftps;Ljava/net/Socket;Lcom/jscape/inet/ftps/Ftps$1;)V
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object p2

    :catch_1
    move-exception p1

    :try_start_3
    new-instance p2, Ljava/io/IOException;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getLinger()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/jscape/inet/ftps/Ftps;->F:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getListeners()Ljava/util/LinkedList;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->n:Ljava/util/LinkedList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getLocalChecksum(Ljava/io/File;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/nio/file/OpenOption;

    invoke-static {p1, v1}, Ljava/nio/file/Files;->newInputStream(Ljava/nio/file/Path;[Ljava/nio/file/OpenOption;)Ljava/io/InputStream;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz p1, :cond_0

    :try_start_2
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_0
    :goto_0
    return-object v0

    :catchall_1
    move-exception v1

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_2
    move-exception v2

    if-eqz p1, :cond_1

    :try_start_5
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v3

    :try_start_6
    invoke-virtual {v1, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    if-nez v0, :cond_1

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    goto :goto_1

    :catchall_4
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    throw v2
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    :catch_0
    move-exception p1

    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public declared-synchronized getLocalDir()Ljava/io/File;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->k:Ljava/io/File;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    :try_start_2
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->g()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->k:Ljava/io/File;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    monitor-exit p0

    return-object v1

    :catch_0
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLocalDirListing()Ljava/util/Enumeration;
    .locals 7

    monitor-enter p0

    :try_start_0
    new-instance v6, Lcom/jscape/util/J;

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "*"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/jscape/util/J;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v6

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getMachineDirListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ftps/Ftps;->getMachineDirListing(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    return-object p1
.end method

.method public getMachineDirListing(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/jscape/inet/ftps/Ftps;->h(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p2

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->k()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v3, p1}, Lcom/jscape/inet/ftps/FtpsClient;->mlsd(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/net/Socket;)Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/jscape/inet/ftps/Ftps;->b(Ljava/io/InputStream;)Ljava/io/InputStreamReader;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    sget-object v4, Lcom/jscape/inet/ftps/Ftps;->b:Lcom/jscape/inet/ftp/FtpFileParser;

    invoke-interface {v4, v3}, Lcom/jscape/inet/ftp/FtpFileParser;->parse(Ljava/io/BufferedReader;)Ljava/util/Enumeration;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/jscape/inet/ftp/FtpFile;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v5}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p2, v6}, Lcom/jscape/util/m/f;->a(Ljava/lang/String;)Z

    move-result v6
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_3

    if-eqz v6, :cond_1

    :try_start_4
    invoke-virtual {v1, v5}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_1
    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p2

    invoke-static {p2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    move-exception p2

    :try_start_5
    invoke-static {p2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2

    :cond_2
    :goto_0
    new-instance p2, Lcom/jscape/inet/ftp/FtpListingEvent;

    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v4

    invoke-direct {p2, p0, v4}, Lcom/jscape/inet/ftp/FtpListingEvent;-><init>(Ljava/lang/Object;Ljava/util/Enumeration;)V

    invoke-virtual {p0, p2}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V

    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v4
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v0, :cond_5

    :try_start_6
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->i()Z

    move-result v6
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    :cond_3
    if-eqz v6, :cond_4

    if-eqz v0, :cond_5

    :try_start_7
    iget-object p2, p0, Lcom/jscape/inet/ftps/Ftps;->O:Ljava/util/zip/InflaterInputStream;

    if-eqz p2, :cond_4

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->d()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_4
    :goto_1
    invoke-static {v3}, Lcom/jscape/util/X;->a(Ljava/io/Reader;)V

    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    goto :goto_2

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_5
    :goto_2
    :try_start_8
    invoke-direct {p0, v2}, Lcom/jscape/inet/ftps/Ftps;->a(Z)V

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_6

    const-string p1, "kLbBmc"

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->f(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    :cond_6
    return-object v4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catchall_0
    move-exception p2

    goto :goto_3

    :catch_5
    move-exception p2

    goto :goto_4

    :catchall_1
    move-exception p2

    move-object v3, v1

    :goto_3
    move-object v1, p1

    goto :goto_6

    :catch_6
    move-exception p2

    move-object v3, v1

    :goto_4
    move-object v1, p1

    goto :goto_5

    :catchall_2
    move-exception p2

    move-object v3, v1

    goto :goto_6

    :catch_7
    move-exception p2

    move-object v3, v1

    :goto_5
    const/4 v2, 0x1

    :try_start_9
    new-instance p1, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p1, v4, p2}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    :catchall_3
    move-exception p2

    :goto_6
    if-eqz v0, :cond_8

    :try_start_a
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->i()Z

    move-result p1
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_8

    if-eqz p1, :cond_7

    if-eqz v0, :cond_8

    :try_start_b
    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->O:Ljava/util/zip/InflaterInputStream;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_a

    if-eqz p1, :cond_7

    :try_start_c
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->d()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_b

    :cond_7
    invoke-static {v3}, Lcom/jscape/util/X;->a(Ljava/io/Reader;)V

    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    goto :goto_7

    :catch_8
    move-exception p1

    :try_start_d
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_9

    :catch_9
    move-exception p1

    :try_start_e
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_a

    :catch_a
    move-exception p1

    :try_start_f
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_b

    :catch_b
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_8
    :goto_7
    invoke-direct {p0, v2}, Lcom/jscape/inet/ftps/Ftps;->a(Z)V

    throw p2
.end method

.method public getMachineFileListing(Ljava/lang/String;)Lcom/jscape/inet/ftp/FtpFile;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/ftp/MLSTRequest;

    invoke-direct {v0}, Lcom/jscape/inet/ftp/MLSTRequest;-><init>()V

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/MLSTRequest;->setFilename(Ljava/lang/String;)V

    new-instance p1, Lcom/jscape/inet/ftps/FtpsProtocolInterpreter;

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-direct {p1, v1}, Lcom/jscape/inet/ftps/FtpsProtocolInterpreter;-><init>(Lcom/jscape/inet/ftps/FtpsClient;)V

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftp/MLSTRequest;->execute(Lcom/jscape/inet/ftp/ClientProtocolInterpreter;)V

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/MLSTRequest;->getFile()Lcom/jscape/inet/ftp/FtpFile;

    move-result-object p1

    return-object p1
.end method

.method public declared-synchronized getMode()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->x:Lcom/jscape/inet/ftps/Ftps$TransferMode;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps$TransferMode;->getCode()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getNATAddress()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Lcom/jscape/inet/ftps/FtpsClient;->getNATAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method public declared-synchronized getNameListing()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->getNameListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNameListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->k()V

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->nameList(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->b(Ljava/net/Socket;)Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getOutputStream(Ljava/lang/String;JZ)Ljava/io/OutputStream;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_0

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->k()V

    if-eqz p4, :cond_0

    iget-object p2, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {p2, p1}, Lcom/jscape/inet/ftps/FtpsClient;->append(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object p1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    iget-object p2, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {p2, p1}, Lcom/jscape/inet/ftps/FtpsClient;->store(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object p1

    :goto_0
    new-instance p2, Lcom/jscape/inet/ftps/Ftps$DataConnectionOutputStream;

    const/4 p3, 0x0

    invoke-direct {p2, p0, p1, p3}, Lcom/jscape/inet/ftps/Ftps$DataConnectionOutputStream;-><init>(Lcom/jscape/inet/ftps/Ftps;Ljava/net/Socket;Lcom/jscape/inet/ftps/Ftps$1;)V
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object p2

    :catch_1
    move-exception p1

    :try_start_3
    new-instance p2, Ljava/io/IOException;

    invoke-virtual {p1}, Lcom/jscape/inet/ftp/FtpException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getPassive()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->A:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPassword()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->t:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPort()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/jscape/inet/ftps/Ftps;->H:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPortAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->i:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getPreserveDownloadTimestamp()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->ac:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPreserveUploadTimestamp()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->ab:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getProxyHostname()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->R:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getProxyPassword()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->Q:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getProxyPort()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/jscape/inet/ftps/Ftps;->S:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getProxyType()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->T:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getProxyUsername()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->P:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getRecursiveDirectoryFileCount(Ljava/lang/String;)I
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    const-string p1, ""

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v0

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    :catch_0
    monitor-exit p0

    return v0
.end method

.method public declared-synchronized getRecursiveDirectorySize(Ljava/lang/String;)J
    .locals 7

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v1, 0x0

    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    const-string p1, ""

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    const/4 v4, 0x0

    :cond_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v4, v5, :cond_1

    invoke-virtual {p1, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/jscape/inet/ftps/Ftps;->getFilesize(Ljava/lang/String;)J

    move-result-wide v5

    add-long/2addr v1, v5

    add-int/lit8 v4, v4, 0x1

    if-eqz v0, :cond_2

    if-nez v0, :cond_0

    :cond_1
    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    monitor-exit p0

    return-wide v1

    :catch_0
    monitor-exit p0

    return-wide v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getRemoteFileChecksum(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v2, 0xd

    aget-object v2, v1, v2

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftps/Ftps;->isFeatureSupported(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x15

    aget-object v1, v1, v3

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->issueCommandCheck(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v2, " "

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {v4, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    const/4 v4, -0x1

    if-eq v2, v4, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {p1, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {p1, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->issueCommandCheck(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    move-object p1, v4

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    new-instance v0, Ljava/util/StringTokenizer;

    invoke-direct {v0, p1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    :try_start_2
    throw v1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_2
    new-instance p1, Lcom/jscape/inet/ftp/FtpException;

    sget-object v0, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v1, 0x2e

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public declared-synchronized getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getDirListing()Ljava/util/Enumeration;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iget-object v2, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    if-nez v3, :cond_0

    :try_start_2
    const-string v3, "\\"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_1

    if-nez v3, :cond_0

    :try_start_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_0
    move-object v4, p0

    goto :goto_2

    :cond_1
    move-object v4, p0

    :goto_0
    if-eqz v3, :cond_5

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/ftp/FtpFile;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v5
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_3

    if-eqz v5, :cond_2

    :try_start_5
    invoke-static {v3}, Lcom/jscape/inet/ftps/Ftps;->a(Lcom/jscape/inet/ftp/FtpFile;)Z

    move-result v5
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-nez v5, :cond_3

    :try_start_6
    iget-object v5, v4, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Lcom/jscape/inet/ftps/Ftps;->getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/Vector;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v4, v5}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    if-nez v0, :cond_3

    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    if-nez v0, :cond_4

    goto :goto_3

    :cond_4
    :goto_2
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_0

    :catch_1
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_2
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_5
    :goto_3
    monitor-exit p0

    return-object v1

    :catch_3
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catch_4
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :catch_5
    move-exception p1

    :try_start_d
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getResponseCode()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient;->getLastResponse()Lcom/jscape/inet/ftps/Response;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Response;->getCode()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    const/4 v0, -0x1

    monitor-exit p0

    return v0
.end method

.method public getServerFeaturesCommandSuccessful()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->X:Z

    return v0
.end method

.method public getShutdownCCC()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->B:Z

    return v0
.end method

.method public getSslHandshakeCompletedListener()Ljavax/net/ssl/HandshakeCompletedListener;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->af:Ljavax/net/ssl/HandshakeCompletedListener;

    return-object v0
.end method

.method public declared-synchronized getSystemType()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient;->system()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getTcpNoDelay()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->J:Z

    return v0
.end method

.method public declared-synchronized getTimeZone()Ljava/util/TimeZone;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->ad:Ljava/util/TimeZone;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTimeout()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/jscape/inet/ftps/Ftps;->I:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getUseEPRT()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->Z:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getUseEPSV()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->Y:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getUsername()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->w:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getWireEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->h:Ljava/lang/String;

    return-object v0
.end method

.method public interrupt()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->z:Z

    return-void
.end method

.method public interrupted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->z:Z

    return v0
.end method

.method public declared-synchronized isConnected()Z
    .locals 7

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-virtual {v1}, Lcom/jscape/inet/ftps/FtpsClient;->isClosed()Z

    move-result v1
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_2

    if-nez v1, :cond_1

    move v1, v3

    goto :goto_0

    :cond_1
    move v1, v2

    :cond_2
    :goto_0
    if-eqz v0, :cond_4

    if-eqz v1, :cond_3

    :try_start_4
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->noop()Ljava/lang/String;
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_1
    move v4, v3

    goto :goto_2

    :catch_0
    move-exception v4

    :try_start_5
    invoke-virtual {v4}, Lcom/jscape/inet/ftp/FtpException;->getMessage()Ljava/lang/String;

    move-result-object v4

    const-string v5, "5"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v0, :cond_5

    if-eqz v4, :cond_3

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_3
    move v4, v2

    :goto_2
    move v5, v4

    move v4, v1

    goto :goto_3

    :cond_4
    move v4, v1

    :cond_5
    move v5, v2

    :goto_3
    if-eqz v0, :cond_7

    if-eqz v4, :cond_8

    if-eqz v0, :cond_6

    if-nez v5, :cond_8

    :try_start_7
    new-instance v4, Lcom/jscape/inet/ftp/FtpConnectionLostEvent;

    iget-object v6, p0, Lcom/jscape/inet/ftps/Ftps;->s:Ljava/lang/String;

    invoke-direct {v4, p0, v6}, Lcom/jscape/inet/ftp/FtpConnectionLostEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_4

    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_6
    move v1, v5

    goto :goto_4

    :cond_7
    move v1, v4

    :cond_8
    :goto_4
    if-eqz v0, :cond_9

    if-eqz v1, :cond_b

    goto :goto_5

    :cond_9
    move v5, v1

    :goto_5
    if-eqz v0, :cond_a

    if-eqz v5, :cond_b

    move v2, v3

    goto :goto_6

    :cond_a
    move v2, v5

    :cond_b
    :goto_6
    monitor-exit p0

    return v2

    :catch_3
    move-exception v0

    :try_start_9
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_4
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_5
    move-exception v0

    :try_start_b
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isFeatureSupported(Ljava/lang/String;)Z
    .locals 4

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    move v2, v1

    :cond_0
    iget-object v3, p0, Lcom/jscape/inet/ftps/Ftps;->W:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v2, v3, :cond_4

    iget-object v3, p0, Lcom/jscape/inet/ftps/Ftps;->W:Ljava/util/Vector;

    invoke-virtual {v3, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_3

    :try_start_0
    invoke-virtual {v3, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_2

    if-eqz v3, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_3
    :goto_0
    if-nez v0, :cond_0

    :cond_4
    :goto_1
    return v1
.end method

.method public declared-synchronized issueCommand(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    new-instance v0, Lcom/jscape/inet/ftp/FtpCommandEvent;

    invoke-direct {v0, p0, p1}, Lcom/jscape/inet/ftp/FtpCommandEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->issueCommandCheck(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized issueCommandCheck(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/ftps/Response;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public login()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->j:Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    iget-object v2, p0, Lcom/jscape/inet/ftps/Ftps;->w:Ljava/lang/String;

    iget-object v3, p0, Lcom/jscape/inet/ftps/Ftps;->t:Ljava/lang/String;

    iget-object v4, p0, Lcom/jscape/inet/ftps/Ftps;->u:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;->authenticate(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->m()V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->setBinary()V

    return-void
.end method

.method public declared-synchronized makeDir(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->makeDirectory(Ljava/lang/String;)V

    new-instance v0, Lcom/jscape/inet/ftp/FtpCreateDirEvent;

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/inet/ftp/FtpCreateDirEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized makeDirRecursive(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    :cond_1
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v3, "/"

    invoke-direct {v2, p1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_4

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catch_2
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->makeDir(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    :goto_1
    if-nez v0, :cond_2

    :cond_3
    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_4
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized makeLocalDir(Ljava/lang/String;)Ljava/io/File;
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result p1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez p1, :cond_0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized mdelete(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->h(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getDirListing()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_5

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v2
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_5

    :try_start_2
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    move-object v2, p0

    :goto_0
    check-cast v2, Lcom/jscape/inet/ftp/FtpFile;

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-interface {p1, v3}, Lcom/jscape/util/m/f;->a(Ljava/lang/String;)Z

    move-result v4
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_2

    if-eqz v4, :cond_4

    :try_start_5
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v4
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    if-eqz v4, :cond_3

    :try_start_6
    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftps/Ftps;->deleteDir(Ljava/lang/String;)V
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-nez v0, :cond_4

    goto :goto_1

    :catch_2
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftps/Ftps;->deleteFile(Ljava/lang/String;)V
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_4
    if-nez v0, :cond_0

    goto :goto_2

    :catch_3
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catch_4
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_5
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_5
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_5
    :goto_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized mdownload(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->h(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getDirListing()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_4

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v2
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_4

    :try_start_2
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    move-object v2, p0

    :goto_0
    check-cast v2, Lcom/jscape/inet/ftp/FtpFile;

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v2
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_2

    if-nez v2, :cond_3

    :try_start_5
    invoke-interface {p1, v3}, Lcom/jscape/util/m/f;->a(Ljava/lang/String;)Z

    move-result v2
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    if-eqz v2, :cond_3

    :try_start_6
    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftps/Ftps;->download(Ljava/lang/String;)Ljava/io/File;
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    :catch_2
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_3
    :goto_1
    if-nez v0, :cond_0

    goto :goto_2

    :catch_3
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catch_4
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :cond_4
    :goto_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized mdownload(Ljava/util/Enumeration;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_3

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_3

    :try_start_2
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    move-object v1, p0

    :goto_0
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftps/Ftps;->download(Ljava/lang/String;)Ljava/io/File;
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_2
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    :goto_1
    if-nez v0, :cond_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized mupload(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->h(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p1

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    :cond_0
    array-length v3, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v2, v3, :cond_3

    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v3
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_1

    if-nez v3, :cond_3

    :try_start_2
    aget-object v3, v0, v2

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/jscape/util/m/f;->a(Ljava/lang/String;)Z

    move-result v3
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    if-eqz v3, :cond_2

    :try_start_3
    aget-object v3, v0, v2

    invoke-virtual {p0, v3}, Lcom/jscape/inet/ftps/Ftps;->upload(Ljava/io/File;)V
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_2
    :goto_0
    add-int/lit8 v2, v2, 0x1

    if-nez v1, :cond_0

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_2
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_3
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized mupload(Ljava/util/Enumeration;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_3

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_3

    :try_start_2
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    move-object v1, p0

    :goto_0
    check-cast v1, Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftps/Ftps;->upload(Ljava/lang/String;)V
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_2
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    :goto_1
    if-nez v0, :cond_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized noop()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient;->noop()Lcom/jscape/inet/ftps/Response;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Response;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public readResponse()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient;->readResponse()Lcom/jscape/inet/ftps/Response;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Response;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public removeFtpListener(Lcom/jscape/inet/ftp/FtpListener;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->n:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    monitor-enter p0

    :try_start_0
    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->n:Ljava/util/LinkedList;

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public declared-synchronized renameFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftps/FtpsClient;->rename(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/jscape/inet/ftp/FtpRenameFileEvent;

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/jscape/inet/ftp/FtpRenameFileEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps;->z:Z

    return-void
.end method

.method public declared-synchronized resumeDownload(Ljava/lang/String;J)Ljava/io/File;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p1, p2, p3}, Lcom/jscape/inet/ftps/Ftps;->resumeDownload(Ljava/lang/String;Ljava/lang/String;J)Ljava/io/File;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized resumeDownload(Ljava/lang/String;Ljava/lang/String;J)Ljava/io/File;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_5

    const-wide/16 v1, 0x0

    cmp-long v1, p3, v1

    if-nez v1, :cond_0

    :try_start_1
    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ftps/Ftps;->download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object p1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    monitor-exit p0

    return-object p1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->ae:Ljava/io/File;

    new-instance p1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v4, 0x35

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    const/4 v2, 0x0

    :try_start_3
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :try_start_4
    invoke-direct {p0, v3, p2, p3, p4}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/io/OutputStream;Ljava/lang/String;J)V
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p2

    move-object p3, v2

    move-object p4, p3

    :goto_0
    move-object v2, v3

    goto/16 :goto_3

    :catch_1
    move-exception p2

    move-object p3, v2

    move-object p4, p3

    move-object v2, v3

    goto/16 :goto_2

    :catch_2
    :goto_1
    :try_start_5
    new-instance p2, Ljava/io/RandomAccessFile;

    sget-object v4, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    invoke-direct {p2, v1, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {p2, p3, p4}, Ljava/io/RandomAccessFile;->seek(J)V

    new-instance p3, Ljava/io/BufferedInputStream;

    new-instance p4, Ljava/io/FileInputStream;

    invoke-direct {p4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {p3, p4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getBlockTransferSize()I

    move-result p4

    new-array p4, p4, [B

    invoke-virtual {p3, p4}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v2

    :cond_1
    const/4 v4, -0x1

    if-eq v2, v4, :cond_2

    const/4 v4, 0x0

    invoke-virtual {p2, p4, v4, v2}, Ljava/io/RandomAccessFile;->write([BII)V

    invoke-virtual {p3, p4}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v2
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v0, :cond_3

    if-nez v0, :cond_1

    :cond_2
    :try_start_8
    invoke-static {v3}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    invoke-static {p3}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    invoke-static {p2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/io/RandomAccessFile;)V

    invoke-virtual {p1}, Ljava/io/File;->delete()Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    :cond_3
    monitor-exit p0

    return-object v1

    :catchall_1
    move-exception p4

    move-object v2, v3

    move-object v6, p4

    move-object p4, p2

    move-object p2, v6

    goto :goto_3

    :catch_3
    move-exception p4

    move-object v2, v3

    move-object v6, p3

    move-object p3, p2

    move-object p2, p4

    move-object p4, v6

    goto :goto_2

    :catchall_2
    move-exception p3

    move-object p4, p2

    move-object p2, p3

    move-object p3, v2

    goto :goto_0

    :catch_4
    move-exception p3

    move-object p4, v2

    move-object v2, v3

    move-object v6, p3

    move-object p3, p2

    move-object p2, v6

    goto :goto_2

    :catchall_3
    move-exception p2

    move-object p3, v2

    move-object p4, p3

    goto :goto_3

    :catch_5
    move-exception p2

    move-object p3, v2

    move-object p4, p3

    :goto_2
    :try_start_9
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    :catchall_4
    move-exception p2

    move-object v6, p4

    move-object p4, p3

    move-object p3, v6

    :goto_3
    :try_start_a
    invoke-static {v2}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    invoke-static {p3}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    invoke-static {p4}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/io/RandomAccessFile;)V

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    throw p2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    :catchall_5
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized resumeUpload(Ljava/io/File;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/jscape/inet/ftps/Ftps;->resumeUpload(Ljava/io/File;Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized resumeUpload(Ljava/io/File;Ljava/lang/String;J)V
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    move-wide/from16 v7, p3

    monitor-enter p0

    :try_start_0
    invoke-static/range {p1 .. p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    move-object/from16 v12, p1

    iput-object v12, v9, Lcom/jscape/inet/ftps/Ftps;->ae:Ljava/io/File;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v0

    sub-long v5, v0, v7

    invoke-static/range {p1 .. p1}, Lcom/jscape/inet/ftps/Ftps;->b(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    const/4 v14, 0x0

    const/16 v15, 0x43

    const/16 v16, 0x41

    const/16 v17, 0x1

    const/4 v1, 0x0

    const/16 v18, 0x31

    :try_start_1
    invoke-virtual {v13, v7, v8}, Ljava/io/InputStream;->skip(J)J

    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->k()V

    iget-object v0, v9, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/jscape/inet/ftps/FtpsClient;->restart(Ljava/lang/String;)V

    iget-object v0, v9, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0, v10}, Lcom/jscape/inet/ftps/FtpsClient;->store(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object v19
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    invoke-virtual/range {v19 .. v19}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v20
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object/from16 v1, p0

    move-object v2, v13

    move-object/from16 v3, v20

    move-object/from16 v4, p2

    move-wide/from16 v7, p3

    :try_start_3
    invoke-direct/range {v1 .. v8}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;JJ)V
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-static {v13}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    invoke-static/range {v20 .. v20}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    invoke-static/range {v19 .. v19}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    invoke-direct {v9, v14}, Lcom/jscape/inet/ftps/Ftps;->a(Z)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    if-eqz v11, :cond_0

    :try_start_5
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getPreserveUploadTimestamp()Z

    move-result v14
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_6
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    :cond_0
    :goto_0
    if-eqz v11, :cond_1

    if-eqz v14, :cond_5

    :try_start_7
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v14
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v1, v0

    :try_start_8
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    :cond_1
    :goto_1
    if-eqz v11, :cond_2

    if-nez v14, :cond_5

    if-eqz v11, :cond_3

    :try_start_9
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->isConnected()Z

    move-result v14
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v1, v0

    :try_start_a
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    :cond_2
    :goto_2
    if-nez v14, :cond_4

    :try_start_b
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->connect()Lcom/jscape/inet/ftps/Ftps;
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    :cond_3
    :try_start_c
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->reset()V

    goto :goto_3

    :catch_3
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_4
    :goto_3
    new-instance v0, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->lastModified()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v9, v10, v0}, Lcom/jscape/inet/ftps/Ftps;->setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object v4, v3, v18

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, v3, v15

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    goto :goto_4

    :catch_4
    :try_start_d
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object v3, v2, v18

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v2, v2, v16

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    :cond_5
    :goto_4
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    move-object v2, v0

    goto :goto_9

    :catch_5
    move-exception v0

    goto :goto_5

    :catch_6
    move-exception v0

    goto :goto_6

    :catchall_1
    move-exception v0

    move-object v2, v0

    goto :goto_a

    :catch_7
    move-exception v0

    move-object/from16 v20, v1

    :goto_5
    move-object/from16 v1, v19

    goto :goto_7

    :catch_8
    move-exception v0

    move-object/from16 v20, v1

    :goto_6
    move-object/from16 v1, v19

    goto :goto_8

    :catchall_2
    move-exception v0

    move-object v2, v0

    move-object/from16 v19, v1

    goto :goto_a

    :catch_9
    move-exception v0

    move-object/from16 v20, v1

    :goto_7
    :try_start_e
    new-instance v2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catch_a
    move-exception v0

    move-object/from16 v20, v1

    :goto_8
    throw v0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    :catchall_3
    move-exception v0

    move-object v2, v0

    move-object/from16 v19, v1

    move/from16 v14, v17

    :goto_9
    move-object/from16 v1, v20

    :goto_a
    :try_start_f
    invoke-static {v13}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    invoke-static/range {v19 .. v19}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    invoke-direct {v9, v14}, Lcom/jscape/inet/ftps/Ftps;->a(Z)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    if-eqz v11, :cond_6

    if-nez v14, :cond_b

    :try_start_10
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getPreserveUploadTimestamp()Z

    move-result v14
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_b
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    goto :goto_b

    :catch_b
    move-exception v0

    move-object v1, v0

    :try_start_11
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_11
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_11 .. :try_end_11} :catch_f
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    :cond_6
    :goto_b
    if-eqz v11, :cond_7

    if-eqz v14, :cond_b

    :try_start_12
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v14
    :try_end_12
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_12 .. :try_end_12} :catch_c
    .catchall {:try_start_12 .. :try_end_12} :catchall_4

    goto :goto_c

    :catch_c
    move-exception v0

    move-object v1, v0

    :try_start_13
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_13
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_13 .. :try_end_13} :catch_f
    .catchall {:try_start_13 .. :try_end_13} :catchall_4

    :cond_7
    :goto_c
    if-eqz v11, :cond_8

    if-nez v14, :cond_b

    if-eqz v11, :cond_9

    :try_start_14
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->isConnected()Z

    move-result v14
    :try_end_14
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_14 .. :try_end_14} :catch_d
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    goto :goto_d

    :catch_d
    move-exception v0

    move-object v1, v0

    :try_start_15
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_15
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_15 .. :try_end_15} :catch_f
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    :cond_8
    :goto_d
    if-nez v14, :cond_a

    :try_start_16
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->connect()Lcom/jscape/inet/ftps/Ftps;
    :try_end_16
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_16 .. :try_end_16} :catch_e
    .catchall {:try_start_16 .. :try_end_16} :catchall_4

    :cond_9
    :try_start_17
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->reset()V

    goto :goto_e

    :catch_e
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_a
    :goto_e
    new-instance v0, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    invoke-direct {v0, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v9, v10, v0}, Lcom/jscape/inet/ftps/Ftps;->setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object v5, v4, v18

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v4, v4, v15

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_17
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_17 .. :try_end_17} :catch_f
    .catchall {:try_start_17 .. :try_end_17} :catchall_4

    goto :goto_f

    :catch_f
    :try_start_18
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object v4, v3, v18

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, v3, v16

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_b
    :goto_f
    throw v2
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_4

    :catchall_4
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized resumeUpload(Ljava/lang/String;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p1, p2, p3}, Lcom/jscape/inet/ftps/Ftps;->resumeUpload(Ljava/lang/String;Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized resumeUpload(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/jscape/inet/ftps/Ftps;->resumeUpload(Ljava/io/File;Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setAccount(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->u:Ljava/lang/String;

    return-void
.end method

.method public declared-synchronized setAscii()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->a(I)V

    new-instance v0, Lcom/jscape/inet/ftps/Ftps$TextMode;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/jscape/inet/ftps/Ftps$TextMode;-><init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps$1;)V

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->x:Lcom/jscape/inet/ftps/Ftps$TransferMode;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setAuto(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    :try_start_1
    new-instance p1, Lcom/jscape/inet/ftps/Ftps$AutoMode;

    const/4 v1, 0x0

    invoke-direct {p1, p0, v1}, Lcom/jscape/inet/ftps/Ftps$AutoMode;-><init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps$1;)V

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->x:Lcom/jscape/inet/ftps/Ftps$TransferMode;
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->setBinary()V
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setAutoDetectIpv6(Z)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iput-boolean p1, p0, Lcom/jscape/inet/ftps/Ftps;->aa:Z

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-virtual {v1, p1}, Lcom/jscape/inet/ftps/FtpsClient;->setAutoDetectIpv6(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setBinary()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->a(I)V

    new-instance v0, Lcom/jscape/inet/ftps/Ftps$BinaryMode;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/jscape/inet/ftps/Ftps$BinaryMode;-><init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps$1;)V

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->x:Lcom/jscape/inet/ftps/Ftps$TransferMode;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setBlockTransferSize(I)V
    .locals 7

    monitor-enter p0

    int-to-long v0, p1

    const-wide/16 v2, 0x0

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v6, 0x3b

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/jscape/util/w;->a(JJLjava/lang/String;)V

    iput p1, p0, Lcom/jscape/inet/ftps/Ftps;->C:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setCharacterEncoding(Ljava/lang/String;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->r:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setClientCertificates(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftps/ContextFactory;->setKeys(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->q:Ljavax/net/ssl/SSLContext;

    return-void
.end method

.method public setClientCertificates(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0, p3}, Lcom/jscape/inet/ftps/ContextFactory;->setStoreType(Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ftps/Ftps;->setClientCertificates(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setClientCertificates(Ljava/security/KeyStore;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftps/ContextFactory;->setKeys(Ljava/security/KeyStore;Ljava/lang/String;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->q:Ljavax/net/ssl/SSLContext;

    return-void
.end method

.method public declared-synchronized setCompression(Z)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/jscape/inet/ftps/Ftps;->L:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setConnectBeforeCommand(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->setConnectBeforeCommand(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setConnectionType(Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->j:Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setContext(Ljavax/net/ssl/SSLContext;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->f()V

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->q:Ljavax/net/ssl/SSLContext;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setContextAlgorithm(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftps/ContextFactory;->setAlgorithm(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setContextAlgorithmProvider(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftps/ContextFactory;->setAlgorithmProvider(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setContextKeystoreProvider(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftps/ContextFactory;->setKeystoreProvider(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setContextProtocol(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftps/ContextFactory;->setProtocol(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setContextProtocolProvider(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftps/ContextFactory;->setProtocolProvider(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setDataPort(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/jscape/inet/ftps/Ftps;->D:I

    new-instance v0, Lcom/jscape/inet/util/i;

    invoke-direct {v0, p1, p1}, Lcom/jscape/inet/util/i;-><init>(II)V

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->E:Lcom/jscape/inet/util/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setDataPortRange(II)V
    .locals 0

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p2}, Lcom/jscape/inet/ftps/Ftps;->b(I)V

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->c(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setDebug(Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->p:Lcom/jscape/util/j/b;

    if-eqz p1, :cond_0

    sget-object p1, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    :try_start_1
    sget-object p1, Ljava/util/logging/Level;->OFF:Ljava/util/logging/Level;

    :goto_0
    invoke-virtual {v0, p1}, Lcom/jscape/util/j/b;->setLevel(Ljava/util/logging/Level;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setDebugStream(Ljava/io/PrintStream;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->p:Lcom/jscape/util/j/b;

    invoke-virtual {v0, p1}, Lcom/jscape/util/j/b;->a(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setDir(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->changeWorkingDirectory(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    new-instance p1, Lcom/jscape/inet/ftp/FtpChangeDirEvent;

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    invoke-direct {p1, p0, v0}, Lcom/jscape/inet/ftp/FtpChangeDirEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setDirUp()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->e()V

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient;->changeToParentDirectory()V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/ftp/FtpChangeDirEvent;

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/jscape/inet/ftp/FtpChangeDirEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setDiskEncoding(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->g:Ljava/lang/String;

    return-void
.end method

.method public setEnabledCiphers([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->y:[Ljava/lang/String;

    return-void
.end method

.method public declared-synchronized setErrorOnSizeCommand(Z)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/jscape/inet/ftps/Ftps;->K:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setFileCreationTime(Ljava/lang/String;Ljava/util/Date;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v2, 0x13

    aget-object v2, v1, v2

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->issueCommandCheck(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setFileModificationTime(Ljava/lang/String;Ljava/util/Date;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v2, 0x13

    aget-object v2, v1, v2

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    invoke-virtual {v0, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->issueCommandCheck(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v2, 0x13

    aget-object v2, v1, v2

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x38

    aget-object v1, v1, v3

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " "

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->issueCommandCheck(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setFtpFileParser(Lcom/jscape/inet/ftp/FtpFileParser;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->l:Lcom/jscape/inet/ftp/FtpFileParser;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setFtpsCertificateVerifier(Lcom/jscape/inet/ftps/FtpsCertificateVerifier;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->o:Lcom/jscape/inet/ftps/FtpsCertificateVerifier;

    return-void
.end method

.method public declared-synchronized setHostname(Ljava/lang/String;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->f()V

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->s:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setHostname(Ljava/lang/String;Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    :try_start_2
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->f()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->s:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setLinger(I)V
    .locals 7

    monitor-enter p0

    int-to-long v0, p1

    const-wide/16 v2, 0x0

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v6, 0x8

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/jscape/util/w;->b(JJLjava/lang/String;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->f()V

    iput p1, p0, Lcom/jscape/inet/ftps/Ftps;->F:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setListeners(Ljava/util/LinkedList;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->n:Ljava/util/LinkedList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setLocalDir(Ljava/io/File;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v3, 0x3a

    aget-object v4, v2, v3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->k:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setNATAddress(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance p1, Lcom/jscape/inet/ftp/FtpException;

    sget-object v0, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v1, 0x2f

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_0
    invoke-virtual {v1, p1}, Lcom/jscape/inet/ftps/FtpsClient;->setNATAddress(Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method public declared-synchronized setPassive(Z)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/jscape/inet/ftps/Ftps;->A:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setPassword(Ljava/lang/String;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->f()V

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->t:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setPort(I)V
    .locals 0

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->f()V

    iput p1, p0, Lcom/jscape/inet/ftps/Ftps;->H:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setPortAddress(Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->i:Ljava/lang/String;

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :try_start_1
    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ftps/FtpsClient;->setPortAddress(Ljava/lang/String;)V

    :cond_1
    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method public declared-synchronized setPreserveDownloadTimestamp(Z)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/jscape/inet/ftps/Ftps;->ac:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setPreserveUploadTimestamp(Z)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/jscape/inet/ftps/Ftps;->ab:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setProtectionLevel(C)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->protectionLevel(C)V

    return-void
.end method

.method public setProtocol(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftps/ContextFactory;->setProtocol(Ljava/lang/String;)V

    return-void
.end method

.method public declared-synchronized setProxyAuthentication(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->P:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ftps/Ftps;->Q:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setProxyHost(Ljava/lang/String;I)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->R:Ljava/lang/String;

    iput p2, p0, Lcom/jscape/inet/ftps/Ftps;->S:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setProxyType(Ljava/lang/String;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->T:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setReceiveBufferSize(I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iput p1, p0, Lcom/jscape/inet/ftps/Ftps;->V:I

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-virtual {v1, p1}, Lcom/jscape/inet/ftps/FtpsClient;->setReceiveBufferSize(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setSendBufferSize(I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iput p1, p0, Lcom/jscape/inet/ftps/Ftps;->U:I

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-virtual {v1, p1}, Lcom/jscape/inet/ftps/FtpsClient;->setSendBufferSize(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setServerCertificates(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ftps/ContextFactory;->setCertificates(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->q:Ljavax/net/ssl/SSLContext;

    return-void
.end method

.method public setServerCertificates(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0, p3}, Lcom/jscape/inet/ftps/ContextFactory;->setStoreType(Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/ftps/Ftps;->setServerCertificates(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setServerCertificates(Ljava/security/KeyStore;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->f:Lcom/jscape/inet/ftps/ContextFactory;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ftps/ContextFactory;->setCertificates(Ljava/security/KeyStore;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->q:Ljavax/net/ssl/SSLContext;

    return-void
.end method

.method public setShutdownCCC(Z)V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iput-boolean p1, p0, Lcom/jscape/inet/ftps/Ftps;->B:Z

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    invoke-virtual {v1, p1}, Lcom/jscape/inet/ftps/FtpsClient;->setShutdownCCC(Z)V

    :cond_1
    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method public setSslHandshakeCompletedListener(Ljavax/net/ssl/HandshakeCompletedListener;)V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->af:Ljavax/net/ssl/HandshakeCompletedListener;

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    invoke-virtual {v1, p1}, Lcom/jscape/inet/ftps/FtpsClient;->setSslHandshakeCompletedListener(Ljavax/net/ssl/HandshakeCompletedListener;)V

    :cond_1
    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method public setTcpNoDelay(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftps/Ftps;->J:Z

    return-void
.end method

.method public declared-synchronized setTimeout(I)V
    .locals 7

    monitor-enter p0

    int-to-long v0, p1

    const-wide/16 v2, 0x0

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v6, 0x1a

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/jscape/util/w;->b(JJLjava/lang/String;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->f()V

    iput p1, p0, Lcom/jscape/inet/ftps/Ftps;->I:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setTimezone(Ljava/util/TimeZone;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->ad:Ljava/util/TimeZone;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setUseEPRT(Z)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iput-boolean p1, p0, Lcom/jscape/inet/ftps/Ftps;->Z:Z

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-virtual {v1, p1}, Lcom/jscape/inet/ftps/FtpsClient;->setUseEPRT(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setUseEPSV(Z)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iput-boolean p1, p0, Lcom/jscape/inet/ftps/Ftps;->Y:Z

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->m:Lcom/jscape/inet/ftps/FtpsClient;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-virtual {v1, p1}, Lcom/jscape/inet/ftps/FtpsClient;->setUseEPSV(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setUsername(Ljava/lang/String;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps;->f()V

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->w:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setWireEncoding(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps;->h:Ljava/lang/String;

    return-void
.end method

.method public declared-synchronized upload(Ljava/io/File;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ftps/Ftps;->upload(Ljava/io/File;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/io/File;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/ftps/Ftps;->upload(Ljava/io/File;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/io/File;Ljava/lang/String;Z)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v10, p0

    move-object/from16 v11, p2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v13, 0x0

    if-eqz v12, :cond_0

    :try_start_1
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->isFile()Z

    move-result v0
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-nez v0, :cond_0

    :try_start_2
    invoke-virtual/range {p0 .. p1}, Lcom/jscape/inet/ftps/Ftps;->uploadDir(Ljava/io/File;)V

    if-nez v12, :cond_6

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    invoke-static/range {p1 .. p1}, Lcom/jscape/inet/ftps/Ftps;->b(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object v15
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/16 v16, 0x43

    const/16 v17, 0x31

    move-object/from16 v9, p1

    :try_start_4
    iput-object v9, v10, Lcom/jscape/inet/ftps/Ftps;->ae:Ljava/io/File;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    const/4 v0, 0x0

    move-object/from16 v1, p0

    move-object v2, v14

    move-object/from16 v3, p2

    move/from16 v4, p3

    move v9, v0

    invoke-direct/range {v1 .. v9}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/io/InputStream;Ljava/lang/String;ZJJZ)Ljava/lang/String;
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_8
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-static {v14}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-eqz v12, :cond_1

    :try_start_6
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getPreserveUploadTimestamp()Z

    move-result v13
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v1, v0

    :try_start_7
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :cond_1
    :goto_1
    if-eqz v12, :cond_2

    if-eqz v13, :cond_6

    :try_start_8
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v13
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_2

    :catch_3
    move-exception v0

    move-object v1, v0

    :try_start_9
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :cond_2
    :goto_2
    if-eqz v12, :cond_3

    if-nez v13, :cond_6

    if-eqz v12, :cond_4

    :try_start_a
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->isConnected()Z

    move-result v13
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto :goto_3

    :catch_4
    move-exception v0

    move-object v1, v0

    :try_start_b
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :cond_3
    :goto_3
    if-nez v13, :cond_5

    :try_start_c
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->connect()Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->reset()V
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :cond_4
    :try_start_d
    invoke-virtual {v10, v15}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    goto :goto_4

    :catch_5
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_5
    :goto_4
    new-instance v0, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->lastModified()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v10, v11, v0}, Lcom/jscape/inet/ftps/Ftps;->setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v4, 0x19

    aget-object v4, v3, v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, v3, v16

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto :goto_5

    :catch_6
    :try_start_e
    new-instance v0, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->lastModified()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v10, v11, v0}, Lcom/jscape/inet/ftps/Ftps;->setFileModificationTime(Ljava/lang/String;Ljava/util/Date;)V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object v4, v3, v17

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, v3, v16

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_e .. :try_end_e} :catch_7
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    goto :goto_5

    :catch_7
    :try_start_f
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object v3, v2, v17

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x39

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    :cond_6
    :goto_5
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    move-object v1, v0

    goto :goto_6

    :catch_8
    move-exception v0

    const/4 v13, 0x1

    :try_start_10
    throw v0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :goto_6
    :try_start_11
    invoke-static {v14}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    if-eqz v12, :cond_7

    if-nez v13, :cond_c

    :try_start_12
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getPreserveUploadTimestamp()Z

    move-result v13
    :try_end_12
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_12 .. :try_end_12} :catch_9
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    goto :goto_7

    :catch_9
    move-exception v0

    move-object v2, v0

    :try_start_13
    invoke-static {v2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_13
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_13 .. :try_end_13} :catch_d
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    :cond_7
    :goto_7
    if-eqz v12, :cond_8

    if-eqz v13, :cond_c

    :try_start_14
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v13
    :try_end_14
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_14 .. :try_end_14} :catch_a
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    goto :goto_8

    :catch_a
    move-exception v0

    move-object v2, v0

    :try_start_15
    invoke-static {v2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_15
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_15 .. :try_end_15} :catch_d
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    :cond_8
    :goto_8
    if-eqz v12, :cond_9

    if-nez v13, :cond_c

    if-eqz v12, :cond_a

    :try_start_16
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->isConnected()Z

    move-result v13
    :try_end_16
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_16 .. :try_end_16} :catch_b
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    goto :goto_9

    :catch_b
    move-exception v0

    move-object v2, v0

    :try_start_17
    invoke-static {v2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_17
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_17 .. :try_end_17} :catch_d
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    :cond_9
    :goto_9
    if-nez v13, :cond_b

    :try_start_18
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->connect()Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->reset()V
    :try_end_18
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_18 .. :try_end_18} :catch_c
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    :cond_a
    :try_start_19
    invoke-virtual {v10, v15}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    goto :goto_a

    :catch_c
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_b
    :goto_a
    new-instance v0, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v10, v11, v0}, Lcom/jscape/inet/ftps/Ftps;->setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object v5, v4, v17

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v4, v4, v16

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_19
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_19 .. :try_end_19} :catch_d
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    goto :goto_b

    :catch_d
    :try_start_1a
    new-instance v0, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v10, v11, v0}, Lcom/jscape/inet/ftps/Ftps;->setFileModificationTime(Ljava/lang/String;Ljava/util/Date;)V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object v5, v4, v17

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v4, v4, v16

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1a .. :try_end_1a} :catch_e
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1

    goto :goto_b

    :catch_e
    :try_start_1b
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDebugStream()Ljava/io/PrintStream;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object v4, v3, v17

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x41

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_c
    :goto_b
    throw v1
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized upload(Ljava/io/File;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/jscape/inet/ftps/Ftps;->upload(Ljava/io/File;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/ftps/Ftps;->upload(Ljava/io/InputStream;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/io/InputStream;Ljava/lang/String;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    const/4 v0, -0x1

    :goto_0
    int-to-long v5, v0

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    :try_start_1
    invoke-direct/range {v1 .. v9}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/io/InputStream;Ljava/lang/String;ZJJZ)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :goto_1
    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ftps/Ftps;->upload(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/lang/String;Ljava/io/File;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {p1}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_7

    if-eqz v0, :cond_2

    :try_start_2
    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    if-eqz v1, :cond_1

    :try_start_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v0, :cond_3

    :cond_1
    :try_start_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_2
    move-object v1, p1

    :cond_3
    :try_start_5
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {p0, p2, v1}, Lcom/jscape/inet/ftps/Ftps;->upload(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->isConnected()Z

    move-result v2
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v0, :cond_5

    if-nez v2, :cond_4

    :try_start_7
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->connect()Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->reset()V

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_4
    :try_start_8
    iget-boolean v2, p0, Lcom/jscape/inet/ftps/Ftps;->z:Z

    :cond_5
    if-nez v2, :cond_6

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->deleteFile(Ljava/lang/String;)V
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_0
    :try_start_a
    invoke-virtual {p0, v1, p1}, Lcom/jscape/inet/ftps/Ftps;->renameFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_1
    :cond_6
    if-nez v0, :cond_8

    :cond_7
    const/4 p1, 0x0

    :try_start_b
    invoke-virtual {p0, p2, p1}, Lcom/jscape/inet/ftps/Ftps;->upload(Ljava/io/File;Z)V
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_0

    :catch_2
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :cond_8
    :goto_0
    monitor-exit p0

    return-void

    :catch_3
    move-exception p1

    :try_start_d
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catch_4
    move-exception p1

    :try_start_e
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :catch_6
    move-exception p1

    :try_start_f
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_f .. :try_end_f} :catch_7
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :catch_7
    move-exception p1

    :try_start_10
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_8
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :catch_8
    move-exception p1

    :try_start_11
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/ftps/Ftps;->upload(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2, p3}, Lcom/jscape/inet/ftps/Ftps;->upload(Ljava/io/File;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p1, p2}, Lcom/jscape/inet/ftps/Ftps;->upload(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload([BLjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/ftps/Ftps;->upload([BLjava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload([BLjava/lang/String;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length p1, p1

    int-to-long v4, p1

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v8}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/io/InputStream;Ljava/lang/String;ZJJZ)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ftps/Ftps;->uploadDir(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;IIZLjava/lang/String;)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move/from16 v9, p2

    monitor-enter p0

    :try_start_0
    invoke-static/range {p1 .. p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/4 v11, 0x1

    aget-object v3, v2, v11

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x3a

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    iget-object v12, v7, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/jscape/inet/ftps/Ftps;->makeDir(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v13

    invoke-virtual/range {p0 .. p1}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V

    invoke-static/range {p1 .. p1}, Lcom/jscape/util/Q;->g(Ljava/io/File;)Ljava/util/Vector;

    move-result-object v14

    invoke-static/range {p1 .. p1}, Lcom/jscape/util/Q;->f(Ljava/io/File;)Ljava/util/Vector;

    move-result-object v0

    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    invoke-direct {v7, v0, v8, v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/util/Vector;Ljava/io/File;Ljava/lang/String;)V

    iget-object v15, v7, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v0, 0x0

    move v6, v0

    move/from16 v16, v6

    :goto_0
    if-gt v6, v9, :cond_5

    :try_start_3
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v0
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v10, :cond_2

    if-nez v0, :cond_5

    move-object/from16 v1, p0

    move-object v2, v15

    move/from16 v3, p4

    move-object/from16 v4, p5

    move-object v5, v14

    move v11, v6

    move-object/from16 v6, p1

    :try_start_4
    invoke-direct/range {v1 .. v6}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/util/Vector;Ljava/io/File;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/4 v0, 0x1

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v1, v0

    if-eqz v10, :cond_1

    if-ge v11, v9, :cond_0

    const/16 v0, 0x3e8

    move/from16 v6, p3

    goto :goto_1

    :cond_0
    :try_start_5
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_2
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_1
    move v0, v9

    move v6, v11

    :goto_1
    mul-int/2addr v6, v0

    int-to-long v0, v6

    :try_start_7
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catch_3
    :try_start_8
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->disconnect()V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->connect()Lcom/jscape/inet/ftps/Ftps;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3

    :cond_2
    move v11, v6

    :goto_2
    move/from16 v16, v0

    :goto_3
    if-eqz v16, :cond_3

    :try_start_9
    invoke-virtual {v7, v12}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual {v7, v13}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-nez v10, :cond_5

    goto :goto_4

    :catch_4
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_5
    move-exception v0

    :try_start_b
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_3
    :goto_4
    add-int/lit8 v6, v11, 0x1

    if-nez v10, :cond_4

    goto :goto_5

    :cond_4
    const/4 v11, 0x1

    goto :goto_0

    :catch_6
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_5
    :goto_5
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;IIZLjava/lang/String;I)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v8, p0

    move/from16 v9, p2

    move/from16 v10, p6

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v11, :cond_0

    if-eqz v10, :cond_1

    :cond_0
    const/4 v12, 0x1

    if-eqz v11, :cond_2

    if-ne v10, v12, :cond_2

    :cond_1
    :try_start_1
    invoke-virtual/range {p0 .. p5}, Lcom/jscape/inet/ftps/Ftps;->uploadDir(Ljava/io/File;IIZLjava/lang/String;)V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_2
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_2
    if-ltz v10, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object v13
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v0
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const/4 v1, 0x0

    if-eqz v11, :cond_4

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    :try_start_4
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v3, 0x32

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_a
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_4
    :goto_0
    :try_start_5
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/jscape/inet/ftps/Ftps;->makeDir(Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_1
    :try_start_6
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v14

    invoke-virtual/range {p0 .. p1}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V

    invoke-static/range {p1 .. p1}, Lcom/jscape/util/Q;->g(Ljava/io/File;)Ljava/util/Vector;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {p1 .. p1}, Lcom/jscape/util/Q;->f(Ljava/io/File;)Ljava/util/Vector;

    move-result-object v2

    move-object/from16 v7, p1

    invoke-direct {v8, v2, v7}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/util/Vector;Ljava/io/File;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move/from16 v17, v0

    move v6, v1

    :goto_1
    if-gt v6, v9, :cond_a

    :try_start_7
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v0
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_8
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v11, :cond_7

    if-nez v0, :cond_a

    move-object/from16 v1, p0

    move-object/from16 v2, v16

    move/from16 v3, p4

    move-object/from16 v4, p5

    move-object v5, v15

    move v12, v6

    move-object/from16 v6, p1

    move/from16 v7, p6

    :try_start_8
    invoke-direct/range {v1 .. v7}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/String;ZLjava/lang/String;Ljava/util/Vector;Ljava/io/File;I)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    const/4 v0, 0x1

    goto :goto_3

    :catch_2
    move-exception v0

    move-object v1, v0

    :try_start_9
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->abortUploadThreads()V
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-eqz v11, :cond_6

    if-ge v12, v9, :cond_5

    const/16 v0, 0x3e8

    move/from16 v6, p3

    goto :goto_2

    :cond_5
    :try_start_a
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_6
    move v0, v9

    move v6, v12

    :goto_2
    mul-int/2addr v6, v0

    int-to-long v0, v6

    :try_start_b
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catch_3
    :try_start_c
    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->disconnect()V

    invoke-virtual/range {p0 .. p0}, Lcom/jscape/inet/ftps/Ftps;->connect()Lcom/jscape/inet/ftps/Ftps;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_4

    :catch_4
    move-exception v0

    move-object v1, v0

    :try_start_d
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_5
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catch_5
    move-exception v0

    :try_start_e
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :cond_7
    move v12, v6

    :goto_3
    move/from16 v17, v0

    :goto_4
    if-eqz v17, :cond_8

    :try_start_f
    invoke-virtual {v8, v13}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual {v8, v14}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V
    :try_end_f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_f .. :try_end_f} :catch_6
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    if-nez v11, :cond_a

    goto :goto_5

    :catch_6
    move-exception v0

    :try_start_10
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_7
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :catch_7
    move-exception v0

    :try_start_11
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_8
    :goto_5
    add-int/lit8 v6, v12, 0x1

    if-nez v11, :cond_9

    goto :goto_6

    :cond_9
    move-object/from16 v7, p1

    const/4 v12, 0x1

    goto :goto_1

    :catch_8
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :cond_a
    :goto_6
    monitor-exit p0

    return-void

    :catch_9
    move-exception v0

    move-object v1, v0

    :try_start_12
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_12
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_12 .. :try_end_12} :catch_a
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :catch_a
    move-exception v0

    :try_start_13
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :cond_b
    :try_start_14
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v3, 0x18

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v3, 0x23

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_14
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_14 .. :try_end_14} :catch_b
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    :catch_b
    move-exception v0

    :try_start_15
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;IZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/jscape/inet/ftps/Ftps;->uploadDir(Ljava/io/File;IZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;IZLjava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    move-object v5, p4

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/jscape/inet/ftps/Ftps;->uploadDir(Ljava/io/File;IIZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;IZLjava/lang/String;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    :try_start_0
    invoke-virtual/range {v0 .. v6}, Lcom/jscape/inet/ftps/Ftps;->uploadDir(Ljava/io/File;IIZLjava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v4, 0x3a

    aget-object v5, v3, v4

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftps/Ftps;->makeDir(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catch_0
    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->v:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/16 v3, 0x43

    const/4 v4, 0x7

    const/16 v5, 0x2c

    :try_start_3
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getLocalDirListing()Ljava/util/Enumeration;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v7, :cond_4

    :try_start_4
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v7
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_5

    if-eqz v0, :cond_5

    if-nez v7, :cond_4

    :try_start_5
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/io/File;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {v7}, Ljava/io/File;->isFile()Z

    move-result v8
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-eqz v0, :cond_2

    if-eqz v8, :cond_1

    :try_start_7
    invoke-virtual {p0, p2, v7}, Lcom/jscape/inet/ftps/Ftps;->upload(Ljava/lang/String;Ljava/io/File;)V

    if-nez v0, :cond_3

    :cond_1
    invoke-static {v7}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/io/File;)Z

    move-result v8
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    :catch_1
    move-exception p2

    goto :goto_2

    :cond_2
    :goto_0
    if-nez v8, :cond_3

    :try_start_8
    invoke-virtual {p0, v7, p2}, Lcom/jscape/inet/ftps/Ftps;->uploadDir(Ljava/io/File;Ljava/lang/String;)V
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    :catch_2
    move-exception p2

    :try_start_9
    invoke-static {p2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :cond_3
    :goto_1
    if-nez v0, :cond_0

    goto :goto_3

    :catch_3
    move-exception p2

    :try_start_a
    invoke-static {p2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_4
    move-exception p2

    :try_start_b
    invoke-static {p2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :goto_2
    :try_start_c
    invoke-static {p2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :catch_5
    move-exception p2

    :try_start_d
    invoke-static {p2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catch_6
    move-exception p2

    :try_start_e
    invoke-static {p2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :cond_4
    :goto_3
    :try_start_f
    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    :try_start_10
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getPreserveUploadTimestamp()Z

    move-result v7
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_9
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    :cond_5
    if-eqz v0, :cond_6

    if-eqz v7, :cond_9

    if-eqz v0, :cond_7

    :try_start_11
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->isConnected()Z

    move-result v7
    :try_end_11
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_11 .. :try_end_11} :catch_7
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_9
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto :goto_4

    :catch_7
    move-exception p2

    :try_start_12
    invoke-static {p2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_9
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    :cond_6
    :goto_4
    if-nez v7, :cond_8

    :try_start_13
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->connect()Lcom/jscape/inet/ftps/Ftps;
    :try_end_13
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_13 .. :try_end_13} :catch_8
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_9
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    :cond_7
    :try_start_14
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->reset()V

    goto :goto_5

    :catch_8
    move-exception p2

    invoke-static {p2}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2

    :cond_8
    :goto_5
    new-instance p2, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    invoke-direct {p2, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/jscape/inet/ftps/Ftps;->setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)V

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->p:Lcom/jscape/util/j/b;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    const/16 v6, 0x40

    aget-object v6, v2, v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "]"

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/jscape/util/j/b;->fine(Ljava/lang/String;)V
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_9
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    goto :goto_6

    :catch_9
    :try_start_15
    iget-object p2, p0, Lcom/jscape/inet/ftps/Ftps;->p:Lcom/jscape/util/j/b;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object v2, v1, v5

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object p1, v1, v4

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/jscape/util/j/b;->fine(Ljava/lang/String;)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    :cond_9
    :goto_6
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p2

    :try_start_16
    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    :try_start_17
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->getPreserveUploadTimestamp()Z

    move-result v1
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_c
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    if-eqz v0, :cond_a

    if-eqz v1, :cond_d

    if-eqz v0, :cond_b

    :try_start_18
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->isConnected()Z

    move-result v1
    :try_end_18
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_18 .. :try_end_18} :catch_a
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_c
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    goto :goto_7

    :catch_a
    move-exception v0

    :try_start_19
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_c
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    :cond_a
    :goto_7
    if-nez v1, :cond_c

    :try_start_1a
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->connect()Lcom/jscape/inet/ftps/Ftps;
    :try_end_1a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1a .. :try_end_1a} :catch_b
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_c
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1

    :cond_b
    :try_start_1b
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Ftps;->reset()V

    goto :goto_8

    :catch_b
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_c
    :goto_8
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/jscape/inet/ftps/Ftps;->setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)V

    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps;->p:Lcom/jscape/util/j/b;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object v7, v6, v5

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, v6, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/jscape/util/j/b;->fine(Ljava/lang/String;)V
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_c
    .catchall {:try_start_1b .. :try_end_1b} :catchall_1

    goto :goto_9

    :catch_c
    :try_start_1c
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps;->p:Lcom/jscape/util/j/b;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftps/Ftps;->bb:[Ljava/lang/String;

    aget-object v3, v2, v5

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object p1, v2, v4

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jscape/util/j/b;->fine(Ljava/lang/String;)V

    :cond_d
    :goto_9
    throw p2
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadUnique(Ljava/io/File;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/ftps/Ftps;->uploadUnique(Ljava/io/File;Z)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadUnique(Ljava/io/File;Z)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->b(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_0

    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/jscape/inet/ftps/Ftps;->uploadUnique(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    const/4 p1, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/jscape/inet/ftps/Ftps;->uploadUnique(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadUnique(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    :try_start_0
    invoke-direct/range {v0 .. v8}, Lcom/jscape/inet/ftps/Ftps;->a(Ljava/io/InputStream;Ljava/lang/String;ZJJZ)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadUnique(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/Ftps;->uploadUnique(Ljava/io/File;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadUnique(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/jscape/inet/ftps/Ftps;->uploadUnique(Ljava/io/File;Z)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
