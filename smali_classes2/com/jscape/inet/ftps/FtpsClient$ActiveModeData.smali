.class Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field final c:Lcom/jscape/inet/ftps/FtpsClient;


# direct methods
.method private constructor <init>(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;->c:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;->a:Ljava/lang/String;

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;->b:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;->b:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ftps/FtpsClient$1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;-><init>(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getCommandData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getHostname()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;->a:Ljava/lang/String;

    return-object v0
.end method

.method public isEPRTRequired()Z
    .locals 2

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;->getCommandData()Ljava/lang/String;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
