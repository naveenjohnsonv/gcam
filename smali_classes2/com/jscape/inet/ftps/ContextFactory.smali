.class public Lcom/jscape/inet/ftps/ContextFactory;
.super Ljava/lang/Object;


# static fields
.field protected static final DEFAULT_ALGORITHM:Ljava/lang/String;

.field protected static final DEFAULT_PROTOCOL:Ljava/lang/String;

.field protected static final DEFAULT_PROVIDER:Ljava/lang/String;

.field protected static final DEFAULT_TYPE:Ljava/lang/String;

.field private static final a:[Ljava/lang/String;


# instance fields
.field protected algorithm:Ljava/lang/String;

.field protected algorithmProvider:Ljava/lang/String;

.field protected keyManagers:[Ljavax/net/ssl/KeyManager;

.field protected keystoreProvider:Ljava/lang/String;

.field protected protocol:Ljava/lang/String;

.field protected protocolProvider:Ljava/lang/String;

.field protected storeType:Ljava/lang/String;

.field protected trustManagers:[Ljavax/net/ssl/TrustManager;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x3

    const-string v4, "&%-\u0007!\u001c\u0010f^dZ\u000c\u0010\u0008\u001a\u001e\r=\u000f\u0017\u0007\u001fS\u000e\u00038\"-\u0003&%-\u0007!\u001c\u0010f^dZ\u00038\"-\u000c\u0010\u0008\u001a\u001e\r=\u000f\u0017\u0007\u001fS\u000e"

    const/16 v5, 0x39

    move v7, v1

    const/4 v6, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x56

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    const/4 v15, 0x5

    const/4 v2, 0x4

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v2, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v2

    goto :goto_0

    :cond_0
    const/16 v5, 0x18

    const/4 v4, 0x7

    const-string v6, "\u0002?3W\u001b$\u0005\u00103+9=;\u0003/#/}i1\u0007%kj"

    move v8, v2

    move v7, v4

    move-object v4, v6

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v7, v2

    move v8, v11

    :goto_3
    const/16 v9, 0x75

    add-int/2addr v6, v10

    add-int v2, v6, v7

    invoke-virtual {v4, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ftps/ContextFactory;->a:[Ljava/lang/String;

    const/16 v1, 0x8

    aget-object v1, v0, v1

    sput-object v1, Lcom/jscape/inet/ftps/ContextFactory;->DEFAULT_PROVIDER:Ljava/lang/String;

    const/4 v1, 0x6

    aget-object v1, v0, v1

    sput-object v1, Lcom/jscape/inet/ftps/ContextFactory;->DEFAULT_TYPE:Ljava/lang/String;

    aget-object v1, v0, v2

    sput-object v1, Lcom/jscape/inet/ftps/ContextFactory;->DEFAULT_PROTOCOL:Ljava/lang/String;

    aget-object v0, v0, v15

    sput-object v0, Lcom/jscape/inet/ftps/ContextFactory;->DEFAULT_ALGORITHM:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v3, v14, 0x7

    const/4 v15, 0x2

    if-eqz v3, :cond_8

    if-eq v3, v10, :cond_7

    if-eq v3, v15, :cond_6

    if-eq v3, v1, :cond_5

    if-eq v3, v2, :cond_4

    const/4 v2, 0x5

    if-eq v3, v2, :cond_9

    const/16 v15, 0x35

    goto :goto_4

    :cond_4
    const/16 v15, 0x3d

    goto :goto_4

    :cond_5
    const/16 v15, 0x68

    goto :goto_4

    :cond_6
    const/16 v15, 0x28

    goto :goto_4

    :cond_7
    const/16 v15, 0x3f

    goto :goto_4

    :cond_8
    const/16 v15, 0x24

    :cond_9
    :goto_4
    xor-int v2, v9, v15

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2
.end method

.method public constructor <init>()V
    .locals 3

    sget-object v0, Lcom/jscape/inet/ftps/ContextFactory;->a:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v1, v0, v1

    const/4 v2, 0x1

    aget-object v0, v0, v2

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/jscape/inet/ftps/ContextFactory;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    sget-object v0, Lcom/jscape/inet/ftps/ContextFactory;->a:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object v8, v0, v1

    invoke-static {}, Lcom/jscape/inet/ftps/ContextFactory;->getTrustAllManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v10

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    invoke-direct/range {v2 .. v10}, Lcom/jscape/inet/ftps/ContextFactory;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ftps/ContextFactory;->protocol:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ftps/ContextFactory;->algorithm:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/ftps/ContextFactory;->keystoreProvider:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/inet/ftps/ContextFactory;->protocolProvider:Ljava/lang/String;

    iput-object p5, p0, Lcom/jscape/inet/ftps/ContextFactory;->algorithmProvider:Ljava/lang/String;

    iput-object p6, p0, Lcom/jscape/inet/ftps/ContextFactory;->storeType:Ljava/lang/String;

    iput-object p7, p0, Lcom/jscape/inet/ftps/ContextFactory;->keyManagers:[Ljavax/net/ssl/KeyManager;

    iput-object p8, p0, Lcom/jscape/inet/ftps/ContextFactory;->trustManagers:[Ljavax/net/ssl/TrustManager;

    return-void
.end method

.method private static a(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    return-object p0
.end method

.method protected static getTrustAllManagers()[Ljavax/net/ssl/TrustManager;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljavax/net/ssl/TrustManager;

    new-instance v1, Lcom/jscape/inet/ftps/ContextFactory$1;

    invoke-direct {v1}, Lcom/jscape/inet/ftps/ContextFactory$1;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method


# virtual methods
.method public getAlgorithm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/ContextFactory;->algorithm:Ljava/lang/String;

    return-object v0
.end method

.method public getAlgorithmProvider()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/ContextFactory;->algorithmProvider:Ljava/lang/String;

    return-object v0
.end method

.method public getContext()Ljavax/net/ssl/SSLContext;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/ContextFactory;->keyManagers:[Ljavax/net/ssl/KeyManager;

    iget-object v1, p0, Lcom/jscape/inet/ftps/ContextFactory;->trustManagers:[Ljavax/net/ssl/TrustManager;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Z)Ljavax/net/ssl/SSLContext;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public getKeystoreProvider()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/ContextFactory;->keystoreProvider:Ljava/lang/String;

    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/ContextFactory;->protocol:Ljava/lang/String;

    return-object v0
.end method

.method public getProtocolProvider()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/ContextFactory;->protocolProvider:Ljava/lang/String;

    return-object v0
.end method

.method public getStoreType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/ContextFactory;->storeType:Ljava/lang/String;

    return-object v0
.end method

.method public setAlgorithm(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftps/ContextFactory;->algorithm:Ljava/lang/String;

    return-void
.end method

.method public setAlgorithmProvider(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftps/ContextFactory;->algorithmProvider:Ljava/lang/String;

    return-void
.end method

.method public setCertificates(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_8

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v2

    :cond_1
    :goto_0
    sget-object v3, Lcom/jscape/inet/ftps/ContextFactory;->a:[Ljava/lang/String;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    invoke-static {v1, v3}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    :try_start_1
    new-array v1, v2, [Ljava/lang/String;

    invoke-static {p1, v1}, Ljava/nio/file/Paths;->get(Ljava/lang/String;[Ljava/lang/String;)Ljava/nio/file/Path;

    move-result-object p1

    new-array v1, v2, [Ljava/nio/file/OpenOption;

    invoke-static {p1, v1}, Ljava/nio/file/Files;->newInputStream(Ljava/nio/file/Path;[Ljava/nio/file/OpenOption;)Ljava/io/InputStream;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftps/ContextFactory;->keystoreProvider:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    if-eqz v0, :cond_3

    :try_start_3
    invoke-static {v1}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jscape/inet/ftps/ContextFactory;->storeType:Ljava/lang/String;

    iget-object v2, p0, Lcom/jscape/inet/ftps/ContextFactory;->keystoreProvider:Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    goto :goto_1

    :cond_2
    :try_start_4
    iget-object v1, p0, Lcom/jscape/inet/ftps/ContextFactory;->storeType:Ljava/lang/String;

    :cond_3
    invoke-static {v1}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :goto_1
    :try_start_5
    iget-object v2, p0, Lcom/jscape/inet/ftps/ContextFactory;->algorithmProvider:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v0, :cond_5

    :try_start_6
    invoke-static {v2}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/inet/ftps/ContextFactory;->algorithmProvider:Ljava/lang/String;

    invoke-static {v2, v3}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    :cond_4
    :try_start_7
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v2

    :cond_5
    invoke-static {v2}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v2

    :goto_2
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object p2

    invoke-virtual {v1, p1, p2}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    invoke-virtual {v2, v1}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    invoke-virtual {v2}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object p2

    iput-object p2, p0, Lcom/jscape/inet/ftps/ContextFactory;->trustManagers:[Ljavax/net/ssl/TrustManager;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    if-eqz p1, :cond_6

    :try_start_8
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    :cond_6
    return-void

    :catchall_0
    move-exception p2

    :try_start_9
    invoke-static {p2}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catchall_1
    move-exception p2

    :try_start_a
    invoke-static {p2}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :catchall_2
    move-exception p2

    goto :goto_3

    :catchall_3
    move-exception p2

    :try_start_b
    invoke-static {p2}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    :catchall_4
    move-exception p2

    :try_start_c
    invoke-static {p2}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :goto_3
    :try_start_d
    throw p2
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    :catchall_5
    move-exception v1

    if-eqz p1, :cond_7

    :try_start_e
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    goto :goto_4

    :catchall_6
    move-exception v2

    :try_start_f
    invoke-virtual {p2, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    if-nez v0, :cond_7

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_7

    goto :goto_4

    :catchall_7
    move-exception p1

    :try_start_10
    invoke-static {p1}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_7
    :goto_4
    throw v1
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_0

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-direct {p2, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :catchall_8
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method public setCertificates(Ljava/security/KeyStore;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftps/ContextFactory;->algorithmProvider:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    :try_start_1
    invoke-static {v1}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/ftps/ContextFactory;->algorithm:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/ftps/ContextFactory;->algorithmProvider:Ljava/lang/String;

    invoke-static {v0, v1}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :cond_0
    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftps/ContextFactory;->algorithm:Ljava/lang/String;

    :cond_1
    invoke-static {v1}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v0

    :goto_0
    invoke-virtual {v0, p1}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    invoke-virtual {v0}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ftps/ContextFactory;->trustManagers:[Ljavax/net/ssl/TrustManager;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    return-void

    :catch_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :goto_1
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public setKeys(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_8

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v2

    :cond_1
    :goto_0
    sget-object v3, Lcom/jscape/inet/ftps/ContextFactory;->a:[Ljava/lang/String;

    const/4 v4, 0x7

    aget-object v3, v3, v4

    invoke-static {v1, v3}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    :try_start_1
    new-array v1, v2, [Ljava/lang/String;

    invoke-static {p1, v1}, Ljava/nio/file/Paths;->get(Ljava/lang/String;[Ljava/lang/String;)Ljava/nio/file/Path;

    move-result-object p1

    new-array v1, v2, [Ljava/nio/file/OpenOption;

    invoke-static {p1, v1}, Ljava/nio/file/Files;->newInputStream(Ljava/nio/file/Path;[Ljava/nio/file/OpenOption;)Ljava/io/InputStream;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftps/ContextFactory;->keystoreProvider:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    if-eqz v0, :cond_3

    :try_start_3
    invoke-static {v1}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jscape/inet/ftps/ContextFactory;->storeType:Ljava/lang/String;

    iget-object v2, p0, Lcom/jscape/inet/ftps/ContextFactory;->keystoreProvider:Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    goto :goto_1

    :cond_2
    :try_start_4
    iget-object v1, p0, Lcom/jscape/inet/ftps/ContextFactory;->storeType:Ljava/lang/String;

    :cond_3
    invoke-static {v1}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :goto_1
    :try_start_5
    iget-object v2, p0, Lcom/jscape/inet/ftps/ContextFactory;->algorithmProvider:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v0, :cond_5

    :try_start_6
    invoke-static {v2}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {}, Ljavax/net/ssl/KeyManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/inet/ftps/ContextFactory;->algorithmProvider:Ljava/lang/String;

    invoke-static {v2, v3}, Ljavax/net/ssl/KeyManagerFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/net/ssl/KeyManagerFactory;

    move-result-object v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_2

    :cond_4
    :try_start_7
    invoke-static {}, Ljavax/net/ssl/KeyManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v2

    :cond_5
    invoke-static {v2}, Ljavax/net/ssl/KeyManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/KeyManagerFactory;

    move-result-object v2

    :goto_2
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-virtual {v1, p1, v3}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object p2

    invoke-virtual {v2, v1, p2}, Ljavax/net/ssl/KeyManagerFactory;->init(Ljava/security/KeyStore;[C)V

    invoke-virtual {v2}, Ljavax/net/ssl/KeyManagerFactory;->getKeyManagers()[Ljavax/net/ssl/KeyManager;

    move-result-object p2

    iput-object p2, p0, Lcom/jscape/inet/ftps/ContextFactory;->keyManagers:[Ljavax/net/ssl/KeyManager;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    if-eqz p1, :cond_6

    :try_start_8
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    :cond_6
    return-void

    :catchall_0
    move-exception p2

    :try_start_9
    invoke-static {p2}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catchall_1
    move-exception p2

    :try_start_a
    invoke-static {p2}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :catchall_2
    move-exception p2

    goto :goto_3

    :catchall_3
    move-exception p2

    :try_start_b
    invoke-static {p2}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    :catchall_4
    move-exception p2

    :try_start_c
    invoke-static {p2}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p2

    throw p2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :goto_3
    :try_start_d
    throw p2
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    :catchall_5
    move-exception v1

    if-eqz p1, :cond_7

    :try_start_e
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    goto :goto_4

    :catchall_6
    move-exception v2

    :try_start_f
    invoke-virtual {p2, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    if-nez v0, :cond_7

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_7

    goto :goto_4

    :catchall_7
    move-exception p1

    :try_start_10
    invoke-static {p1}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_7
    :goto_4
    throw v1
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_0

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catchall_8
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method public setKeys(Ljava/security/KeyStore;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftps/ContextFactory;->algorithmProvider:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-static {v1}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v1, :cond_0

    :try_start_1
    invoke-static {}, Ljavax/net/ssl/KeyManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/ftps/ContextFactory;->algorithmProvider:Ljava/lang/String;

    invoke-static {v1, v2}, Ljavax/net/ssl/KeyManagerFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/net/ssl/KeyManagerFactory;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    if-nez v0, :cond_2

    :cond_0
    :try_start_2
    invoke-static {}, Ljavax/net/ssl/KeyManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-static {v1}, Ljavax/net/ssl/KeyManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/KeyManagerFactory;

    move-result-object v1

    :cond_2
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object p2

    invoke-virtual {v1, p1, p2}, Ljavax/net/ssl/KeyManagerFactory;->init(Ljava/security/KeyStore;[C)V

    invoke-virtual {v1}, Ljavax/net/ssl/KeyManagerFactory;->getKeyManagers()[Ljavax/net/ssl/KeyManager;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ftps/ContextFactory;->keyManagers:[Ljavax/net/ssl/KeyManager;

    return-void

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public setKeystoreProvider(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftps/ContextFactory;->keystoreProvider:Ljava/lang/String;

    return-void
.end method

.method public setProtocol(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftps/ContextFactory;->protocol:Ljava/lang/String;

    return-void
.end method

.method public setProtocolProvider(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftps/ContextFactory;->protocolProvider:Ljava/lang/String;

    return-void
.end method

.method public setStoreType(Ljava/lang/String;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftps/ContextFactory;->keystoreProvider:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_1

    :try_start_1
    invoke-static {v1}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v1, :cond_0

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftps/ContextFactory;->keystoreProvider:Ljava/lang/String;

    invoke-static {p1, v1}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;

    if-nez v0, :cond_2

    :cond_0
    move-object v1, p1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/ftps/ContextFactory;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    invoke-static {v1}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    :cond_2
    iput-object p1, p0, Lcom/jscape/inet/ftps/ContextFactory;->storeType:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    return-void

    :catch_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftps/ContextFactory;->a:[Ljava/lang/String;

    const/16 v3, 0x9

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
