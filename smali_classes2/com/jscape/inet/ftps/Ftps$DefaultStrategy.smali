.class Lcom/jscape/inet/ftps/Ftps$DefaultStrategy;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftps/Ftps$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps$DefaultStrategy;-><init>()V

    return-void
.end method

.method private static b(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public authenticate(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ftps/FtpsClient;->userName(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    move-result-object p2

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Response;->getCode()I

    move-result p2

    const/16 v0, 0x12c

    if-le p2, v0, :cond_0

    :try_start_0
    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/FtpsClient;->password(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps$DefaultStrategy;->b(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    if-eqz p4, :cond_1

    :try_start_1
    invoke-virtual {p1, p4}, Lcom/jscape/inet/ftps/FtpsClient;->account(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps$DefaultStrategy;->b(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    return-void
.end method

.method public createClient(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/util/ConnectionParameters;Ljavax/net/ssl/SSLContext;[Ljava/lang/String;Ljava/util/logging/Logger;)Lcom/jscape/inet/ftps/FtpsClient;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {p1, p2, p3, p4, p5}, Lcom/jscape/inet/ftps/FtpsClient;->openPlain(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/util/ConnectionParameters;Ljavax/net/ssl/SSLContext;[Ljava/lang/String;Ljava/util/logging/Logger;)Lcom/jscape/inet/ftps/FtpsClient;

    move-result-object p1

    return-object p1
.end method
