.class public Lcom/jscape/inet/ftps/Fxps;
.super Ljava/lang/Object;


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private a:Ljava/util/Vector;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x4

    const-string v4, "l@)8\u0004oR9 \u0004l@)8\u0008l@)8\u0005g\u0000y"

    const/16 v5, 0x17

    move v7, v1

    const/4 v6, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x7c

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0xd

    const/4 v4, 0x7

    const-string v6, "\u0001-DUh\ne\u0005\u00021UOh"

    move v7, v4

    move-object v4, v6

    move v8, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x11

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ftps/Fxps;->b:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    if-eq v2, v1, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v2, 0x3a

    goto :goto_4

    :cond_4
    const/16 v2, 0x54

    goto :goto_4

    :cond_5
    const/16 v2, 0x59

    goto :goto_4

    :cond_6
    const/16 v2, 0xa

    goto :goto_4

    :cond_7
    const/16 v2, 0x16

    goto :goto_4

    :cond_8
    const/16 v2, 0x6f

    goto :goto_4

    :cond_9
    const/16 v2, 0x43

    :goto_4
    xor-int/2addr v2, v9

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/ftps/Fxps;->a:Ljava/util/Vector;

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/inet/ftps/Ftps;->isConnected()Z

    move-result v1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    :try_start_1
    invoke-virtual {p1}, Lcom/jscape/inet/ftps/Ftps;->connect()Lcom/jscape/inet/ftps/Ftps;
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    if-eqz v0, :cond_2

    :try_start_2
    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->isConnected()Z

    move-result v1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Fxps;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    if-nez v1, :cond_2

    :try_start_3
    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->connect()Lcom/jscape/inet/ftps/Ftps;
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Fxps;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    return-void

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Fxps;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Fxps;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v0, :cond_4

    if-nez v2, :cond_3

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v0, :cond_4

    if-nez v2, :cond_3

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v0, :cond_1

    if-eqz v2, :cond_0

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v0, :cond_4

    if-nez v2, :cond_3

    :cond_0
    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    :cond_1
    if-eqz v0, :cond_2

    if-eqz v2, :cond_5

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    move v2, p0

    :cond_2
    if-eqz v0, :cond_4

    if-eqz v2, :cond_5

    :cond_3
    move v2, v4

    :cond_4
    move v3, v2

    :cond_5
    return v3
.end method


# virtual methods
.method public declared-synchronized addFxpListener(Lcom/jscape/inet/ftp/FxpListener;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Fxps;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected fireFxpEnd(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/jscape/inet/ftp/FxpEndEvent;

    invoke-direct {v0, p1, p2, p3}, Lcom/jscape/inet/ftp/FxpEndEvent;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object p1

    monitor-enter p0

    :try_start_0
    iget-object p2, p0, Lcom/jscape/inet/ftps/Fxps;->a:Ljava/util/Vector;

    invoke-virtual {p2}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Vector;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p3, 0x0

    :cond_0
    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge p3, v1, :cond_1

    invoke-virtual {p2, p3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/ftp/FxpListener;

    invoke-interface {v1, v0}, Lcom/jscape/inet/ftp/FxpListener;->fxpEnd(Lcom/jscape/inet/ftp/FxpEndEvent;)V

    add-int/lit8 p3, p3, 0x1

    if-nez p1, :cond_0

    :cond_1
    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method protected fireFxpFailed(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    new-instance v0, Lcom/jscape/inet/ftp/FxpFailedEvent;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/jscape/inet/ftp/FxpFailedEvent;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object p1

    monitor-enter p0

    :try_start_0
    iget-object p2, p0, Lcom/jscape/inet/ftps/Fxps;->a:Ljava/util/Vector;

    invoke-virtual {p2}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Vector;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p3, 0x0

    :cond_0
    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    move-result p4

    if-ge p3, p4, :cond_1

    invoke-virtual {p2, p3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/jscape/inet/ftp/FxpListener;

    invoke-interface {p4, v0}, Lcom/jscape/inet/ftp/FxpListener;->fxpFailed(Lcom/jscape/inet/ftp/FxpFailedEvent;)V

    add-int/lit8 p3, p3, 0x1

    if-nez p1, :cond_0

    :cond_1
    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method protected fireFxpStart(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/jscape/inet/ftp/FxpStartEvent;

    invoke-direct {v0, p1, p2, p3}, Lcom/jscape/inet/ftp/FxpStartEvent;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object p1

    monitor-enter p0

    :try_start_0
    iget-object p2, p0, Lcom/jscape/inet/ftps/Fxps;->a:Ljava/util/Vector;

    invoke-virtual {p2}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Vector;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p3, 0x0

    :cond_0
    invoke-virtual {p2}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge p3, v1, :cond_1

    invoke-virtual {p2, p3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/ftp/FxpListener;

    invoke-interface {v1, v0}, Lcom/jscape/inet/ftp/FxpListener;->fxpStart(Lcom/jscape/inet/ftp/FxpStartEvent;)V

    add-int/lit8 p3, p3, 0x1

    if-nez p1, :cond_0

    :cond_1
    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public mtransfer(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/jscape/inet/ftps/Ftps;->getDirListing()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/ftp/FtpFile;

    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    invoke-virtual {v2}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v2
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    if-nez v2, :cond_2

    :try_start_1
    invoke-static {v3, p3}, Lcom/jscape/inet/ftps/Fxps;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_1
    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {p0, p1, p2, v3}, Lcom/jscape/inet/ftps/Fxps;->transfer(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Fxps;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    if-nez v0, :cond_0

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/Fxps;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Fxps;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    return-void
.end method

.method public declared-synchronized removeFxpListener(Lcom/jscape/inet/ftp/FxpListener;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ftps/Fxps;->a:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/ftps/Fxps;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public transfer(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ftps/Fxps;->a(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;)V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    :try_start_0
    sget-object v3, Lcom/jscape/inet/ftps/Fxps;->b:[Ljava/lang/String;

    aget-object v4, v3, v1

    invoke-virtual {p1, v4}, Lcom/jscape/inet/ftps/Ftps;->issueCommand(Ljava/lang/String;)Ljava/lang/String;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    invoke-virtual {p2, v3}, Lcom/jscape/inet/ftps/Ftps;->issueCommand(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    :catch_0
    if-eqz v1, :cond_0

    :try_start_1
    sget-object v3, Lcom/jscape/inet/ftps/Fxps;->b:[Ljava/lang/String;

    const/4 v4, 0x4

    aget-object v3, v3, v4

    invoke-virtual {p1, v3}, Lcom/jscape/inet/ftps/Ftps;->issueCommand(Ljava/lang/String;)Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    const/16 v3, 0x43

    invoke-virtual {p1, v3}, Lcom/jscape/inet/ftps/Ftps;->setProtectionLevel(C)V

    invoke-virtual {p2, v3}, Lcom/jscape/inet/ftps/Ftps;->setProtectionLevel(C)V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_4

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/inet/ftps/Fxps;->fireFxpStart(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;)V

    sget-object v3, Lcom/jscape/inet/ftps/Fxps;->b:[Ljava/lang/String;

    aget-object v4, v3, v2

    invoke-virtual {p1, v4}, Lcom/jscape/inet/ftps/Ftps;->issueCommand(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v5, v2

    const-string v2, ")"

    invoke-virtual {v4, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v4, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x5

    aget-object v3, v3, v5

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/jscape/inet/ftps/Ftps;->issueCommand(Ljava/lang/String;)Ljava/lang/String;

    new-instance v2, Lcom/jscape/inet/ftps/FxpsRetr;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/jscape/inet/ftps/FxpsRetr;-><init>(Lcom/jscape/inet/ftps/Fxps;Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/jscape/inet/ftps/FxpsRetr;->start()V

    new-instance v2, Lcom/jscape/inet/ftps/FxpsStor;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/jscape/inet/ftps/FxpsStor;-><init>(Lcom/jscape/inet/ftps/Fxps;Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/jscape/inet/ftps/FxpsStor;->start()V

    :cond_2
    :goto_0
    invoke-virtual {v2}, Lcom/jscape/inet/ftps/FxpsStor;->isAlive()Z

    move-result v3

    if-eqz v3, :cond_3

    const-wide/16 v3, 0x64

    :try_start_2
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz v0, :cond_4

    goto :goto_0

    :catch_1
    if-nez v0, :cond_2

    :cond_3
    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/inet/ftps/Fxps;->fireFxpEnd(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;)V

    :cond_4
    const/16 p3, 0x50

    if-eqz v0, :cond_6

    if-eqz v1, :cond_5

    :try_start_3
    sget-object v1, Lcom/jscape/inet/ftps/Fxps;->b:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ftps/Ftps;->issueCommand(Ljava/lang/String;)Ljava/lang/String;
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_2

    if-nez v0, :cond_7

    goto :goto_1

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/Fxps;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_5
    :goto_1
    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setProtectionLevel(C)V
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_2

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Fxps;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_6
    :goto_2
    invoke-virtual {p2, p3}, Lcom/jscape/inet/ftps/Ftps;->setProtectionLevel(C)V

    :cond_7
    return-void

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Fxps;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public transferDir(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ftps/Fxps;->a(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;)V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p2, p3}, Lcom/jscape/inet/ftps/Ftps;->makeDir(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    invoke-virtual {p2, p3}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    :try_start_1
    invoke-virtual {p1}, Lcom/jscape/inet/ftps/Ftps;->getDirListing()Ljava/util/Enumeration;

    move-result-object p3

    :cond_0
    invoke-interface {p3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {p3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/ftp/FtpFile;
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_5

    if-eqz v0, :cond_1

    :try_start_2
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p1, p2, v4}, Lcom/jscape/inet/ftps/Fxps;->transfer(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_1
    move-exception p3

    :try_start_3
    invoke-static {p3}, Lcom/jscape/inet/ftps/Fxps;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p3

    throw p3
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    :goto_0
    if-nez v0, :cond_3

    :cond_2
    :try_start_4
    invoke-virtual {v3}, Lcom/jscape/inet/ftp/FtpFile;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, p1, p2, v3}, Lcom/jscape/inet/ftps/Fxps;->transferDir(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_2
    move-exception p3

    :try_start_5
    invoke-static {p3}, Lcom/jscape/inet/ftps/Fxps;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p3

    throw p3
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_3
    :goto_1
    if-nez v0, :cond_0

    :cond_4
    invoke-virtual {p1, v1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    :cond_5
    const/4 p1, 0x0

    goto :goto_2

    :catchall_0
    move-exception p3

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    throw p3

    :catch_3
    move-exception p3

    invoke-virtual {p1, v1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    move-object p1, p3

    :goto_2
    if-eqz v0, :cond_6

    if-nez p1, :cond_6

    return-void

    :cond_6
    throw p1
.end method
