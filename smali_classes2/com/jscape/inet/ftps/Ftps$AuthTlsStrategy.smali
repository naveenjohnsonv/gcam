.class Lcom/jscape/inet/ftps/Ftps$AuthTlsStrategy;
.super Lcom/jscape/inet/ftps/Ftps$DefaultStrategy;


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x33

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x1d

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, ":g\u0008\u0001M\u0018a\u0016|]\u000c\\Lg\u001cf\t\u0004JY{\u001c(\n\u0005LV/\u0011i\u000eMGW{Yj\u0018\u0008G\u0018n\u000c|\u0015\u0002[Qu\u001cl\u0003-D."

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x37

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ftps/Ftps$AuthTlsStrategy;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x12

    goto :goto_2

    :cond_2
    const/16 v12, 0x25

    goto :goto_2

    :cond_3
    const/16 v12, 0x34

    goto :goto_2

    :cond_4
    const/16 v12, 0x70

    goto :goto_2

    :cond_5
    const/16 v12, 0x60

    goto :goto_2

    :cond_6
    const/16 v12, 0x15

    goto :goto_2

    :cond_7
    const/16 v12, 0x64

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/jscape/inet/ftps/Ftps$DefaultStrategy;-><init>(Lcom/jscape/inet/ftps/Ftps$1;)V

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftps/Ftps$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps$AuthTlsStrategy;-><init>()V

    return-void
.end method

.method private static a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public authenticate(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/ftps/Ftps$AuthTlsStrategy;->a:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Lcom/jscape/inet/ftps/FtpsClient;->authorize(Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/inet/ftps/FtpsClient;->getFtpsCertificateVerifier()Lcom/jscape/inet/ftps/FtpsCertificateVerifier;

    move-result-object v2
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_3

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    if-eqz v2, :cond_1

    :try_start_1
    invoke-virtual {p1}, Lcom/jscape/inet/ftps/FtpsClient;->getFtpsCertificateVerifier()Lcom/jscape/inet/ftps/FtpsCertificateVerifier;

    move-result-object v2
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_4

    :cond_0
    :try_start_2
    invoke-interface {v2}, Lcom/jscape/inet/ftps/FtpsCertificateVerifier;->authorized()Z

    move-result v2
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v0, :cond_2

    if-nez v2, :cond_1

    move v1, v3

    :cond_1
    move v2, v1

    :cond_2
    if-eqz v2, :cond_3

    :try_start_3
    invoke-super {p0, p1, p2, p3, p4}, Lcom/jscape/inet/ftps/Ftps$DefaultStrategy;->authenticate(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/jscape/inet/ftps/FtpsClient;->bufferSize()V
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_1

    if-eqz v0, :cond_3

    const/16 p2, 0x50

    :try_start_4
    invoke-virtual {p1, p2}, Lcom/jscape/inet/ftps/FtpsClient;->protectionLevel(C)V
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    :catch_0
    const/16 p2, 0x43

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ftps/FtpsClient;->protectionLevel(C)V

    :goto_0
    return-void

    :cond_3
    :try_start_5
    new-instance p1, Lcom/jscape/inet/ftp/FtpException;

    sget-object p2, Lcom/jscape/inet/ftps/Ftps$AuthTlsStrategy;->a:[Ljava/lang/String;

    aget-object p2, p2, v3

    invoke-direct {p1, p2}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps$AuthTlsStrategy;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object p1

    throw p1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps$AuthTlsStrategy;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object p1

    throw p1

    :catch_3
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps$AuthTlsStrategy;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps$AuthTlsStrategy;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object p1

    throw p1
.end method
