.class public Lcom/jscape/inet/ftps/FtpsClient;
.super Ljava/lang/Object;


# static fields
.field private static final A:[Ljava/lang/String;

.field public static final DATA_CLEAR:C = 'C'

.field public static final DATA_CONFIDENTIAL:C = 'E'

.field public static final DATA_PRIVATE:C = 'P'

.field public static final DATA_SAFE:C = 'S'

.field public static final DEFAULT_DATA_PORT:I = 0x0

.field public static final DEFAULT_PORT:I = 0x15

.field public static final EOL:Ljava/lang/String;

.field private static final a:I = 0x4

.field private static final b:I = 0x10

.field private static z:Ljava/lang/String;


# instance fields
.field private final c:Lcom/jscape/inet/ftps/Ftps;

.field private final d:Lcom/jscape/inet/util/ConnectionParameters;

.field private final e:Ljavax/net/ssl/SSLContext;

.field private final f:[Ljava/lang/String;

.field private final g:Ljava/util/logging/Logger;

.field private h:Ljava/net/Socket;

.field private i:Ljava/net/Socket;

.field private j:Ljava/net/Socket;

.field private k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:Z

.field private s:Lcom/jscape/inet/ftps/FtpsCertificateVerifier;

.field private t:Lcom/jscape/inet/ftps/Response;

.field private u:I

.field private v:I

.field private w:Ljava/lang/String;

.field private x:Z

.field private y:Ljavax/net/ssl/HandshakeCompletedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x48

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "VsAoDb"

    invoke-static {v1}, Lcom/jscape/inet/ftps/FtpsClient;->f(Ljava/lang/String;)V

    const/4 v2, 0x4

    const-string v4, "A\u001bM2\u0003mkb\u0003mhb\u0005Y\u001fR42\u0005A\u0015L02\u001dX4h\u0005~l\u00191\u0014_02L-1;z\u0000``\u000ebzx\u000b`h\u001ce\u0005P\u000fJ,2\u0004T\nM2\u0005B\u000eL12\u001cX*hR2m\u0012b.p\u0005\u007f`RX\n>\u0000wq\u0018r.{\u00002?]\u0004P\u0018Q6\u0005C\u0014J+2\u0005P\u0016R+2\u001dX4h\u0005~l\u00191\u0014_02L-1;z\u0000``\u000ebzx\u000b`h\u001ce\u00031\u0008>\u0005C\u001fM02\u0004B\u000eQ1\u0005B\u000e_02\u0002+`\u0004C\u0017ZD C?h\u0001`v\u00181\u001eP72i\u0012~1k\u00142p\u0013b/}\u0007wv\u000ew/rD\u0005P\nN!2\u0004T\nM2\u0005P\u0019]02\u0004B\u0013J!\u0004\\\u0011ZD\u0005C\u0014X62\u0004R\u001eK4\u0004_\u0016M0\u0004]\u0013M0\u0015X4h\u0005~l\u00191\u001fN7D%\u000ft)n\u000b|v\u0018\u0004A\u001bM7\u0004@\u000fW0\u000b9.w\twa]~/jM\u0002\u001cP\u0005\\\u0015Z!2\u0003$j,\u001aT(l\u000b`%\u000et4z\r|b]W\u000eNDqj\u0010|;p\u0000<\u000517mDI C?s\u000bf`]w3r\u0001|d\u0010tzs\u0005k%\u0013~.>\u0006w%\u0013d6rJ F(q\nu%\u0015~)jD4%\r~(jDtj\u000f19q\n|`\u001ee3q\n\u0005E\u0003N!2\u0005B\u000eQ62\u0010r6w\u0001|q]x)>\u0007~j\u000et>\u0004R\rZD\u0005C\u001fJ62\u0004T\nL0\u0005T\nL02\u0004B\u0003M0\u0007+`X\"TCG\u0005P\u0016R+2\u0005U\u001fR!2IR6w\u0001|q]x)>\u000b|%\u001c\u007fzW4d3]\u007f?j\u0013}w\u001618k\u00102@-C\u000e>\u0007}h\u0010p4zD{v]\u007f5jDap\ra5l\u0010wa]s#>\u0017ww\u000bt(0\u000bA\u001bM72/W;p4NxR5s\tsk\u001919v\u0005|k\u0018}zM7^%\u000et)m\r}k]c?k\u0017{k\u001a13mD|j\t1)k\u0014bj\u000fe?zD{k]r/l\u0016wk\t1\u0010L!<%<14{\u00132V.]zm\u0001av\u0014~4>\u0002}w]u;j\u00052f\u0015p4p\u0001~%\nx6rDp`]r({\u0005f`\u0019?\u0003R\u0019]\u0004C\u001fW*\u0007+`x\u0002tcG\u0004_\u0015Q4\u00109\u0001\u007fItDPW\u0007!M:Y\u0019.s\u0005A\u001bM72\u0005B\u000eQ12\u0004\\\u0016M \u0005A\u0008Q02\u0005B\u0017P02\u0002+` C?s\u000bf`]w3r\u0001|d\u0010tzs\u0005k%\u0013~.>\u0006w%\u0013d6rJ\u0005D\t[62\u0002\u001cP\u0006A\u0018M>25"

    const/16 v5, 0x330

    move v7, v2

    const/4 v6, -0x1

    const/4 v8, 0x0

    :goto_0
    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v7

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v12, v9

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v13, v10

    const/4 v14, 0x0

    :goto_2
    const/4 v15, 0x3

    if-gt v13, v14, :cond_3

    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v12}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v11, :cond_1

    add-int/lit8 v11, v8, 0x1

    aput-object v10, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x2c

    const-string v4, "\u0016Z\r(\u0014h?V7!OfI\u0007`e3N\"\u007f,@6rF)b\"F5rI)`9_ &O\"- ]e"

    move v8, v11

    move v7, v15

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v10, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v12, 0x56

    add-int/2addr v6, v9

    add-int v10, v6, v7

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v1, 0x44

    aget-object v0, v0, v1

    sput-object v0, Lcom/jscape/inet/ftps/FtpsClient;->EOL:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v10, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v9, :cond_8

    const/4 v3, 0x2

    if-eq v1, v3, :cond_7

    if-eq v1, v15, :cond_6

    if-eq v1, v2, :cond_5

    const/4 v3, 0x5

    if-eq v1, v3, :cond_4

    const/16 v1, 0x7c

    goto :goto_4

    :cond_4
    move v1, v2

    goto :goto_4

    :cond_5
    const/16 v1, 0x13

    goto :goto_4

    :cond_6
    const/16 v1, 0x65

    goto :goto_4

    :cond_7
    const/16 v1, 0x1f

    goto :goto_4

    :cond_8
    const/16 v1, 0x5b

    goto :goto_4

    :cond_9
    const/16 v1, 0x10

    :goto_4
    xor-int/2addr v1, v12

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v10, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method private constructor <init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/util/ConnectionParameters;Ljavax/net/ssl/SSLContext;[Ljava/lang/String;Ljava/util/logging/Logger;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    iput-object p2, p0, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    iput-object p3, p0, Lcom/jscape/inet/ftps/FtpsClient;->e:Ljavax/net/ssl/SSLContext;

    iput-object p4, p0, Lcom/jscape/inet/ftps/FtpsClient;->f:[Ljava/lang/String;

    iput-object p5, p0, Lcom/jscape/inet/ftps/FtpsClient;->g:Ljava/util/logging/Logger;

    invoke-virtual {p1}, Lcom/jscape/inet/ftps/Ftps;->getUseEPSV()Z

    move-result p1

    iput-boolean p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->m:Z

    iget-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p1}, Lcom/jscape/inet/ftps/Ftps;->getUseEPRT()Z

    move-result p1

    iput-boolean p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->n:Z

    iget-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p1}, Lcom/jscape/inet/ftps/Ftps;->getAutoDetectIpv6()Z

    move-result p1

    iput-boolean p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->x:Z

    iget-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p1}, Lcom/jscape/inet/ftps/Ftps;->getFtpsCertificateVerifier()Lcom/jscape/inet/ftps/FtpsCertificateVerifier;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->s:Lcom/jscape/inet/ftps/FtpsCertificateVerifier;

    iget-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p1}, Lcom/jscape/inet/ftps/Ftps;->getShutdownCCC()Z

    move-result p1

    iput-boolean p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->q:Z

    iget-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p1}, Lcom/jscape/inet/ftps/Ftps;->getSslHandshakeCompletedListener()Ljavax/net/ssl/HandshakeCompletedListener;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->y:Ljavax/net/ssl/HandshakeCompletedListener;

    return-void
.end method

.method static a(Lcom/jscape/inet/ftps/FtpsClient;)I
    .locals 0

    iget p0, p0, Lcom/jscape/inet/ftps/FtpsClient;->u:I

    return p0
.end method

.method private a([B)Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;
    .locals 5

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    const/4 v2, 0x0

    :cond_0
    array-length v3, p1

    const-string v4, ","

    if-ge v2, v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-byte v1, p1, v2

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    if-eqz v0, :cond_2

    if-nez v0, :cond_0

    :cond_1
    iget-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    check-cast p1, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;

    invoke-virtual {p1}, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->getLocalPort()I

    move-result v2

    :cond_2
    new-instance p1, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    shr-int/lit8 v3, v2, 0x8

    and-int/lit16 v3, v3, 0xff

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    and-int/lit16 v2, v2, 0xff

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {p1, p0, v1, v0, v2}, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;-><init>(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ftps/FtpsClient$1;)V

    return-object p1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method static a(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->p:Ljava/lang/String;

    return-object p1
.end method

.method static a(Lcom/jscape/inet/ftps/FtpsClient;Ljava/net/Socket;Z)Ljava/net/Socket;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/net/Socket;Z)Ljava/net/Socket;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/net/Socket;Z)Ljava/net/Socket;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->e:Ljavax/net/ssl/SSLContext;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/net/Socket;->getPort()I

    move-result v3

    :try_start_0
    invoke-virtual {v0, p1, v2, v3, p2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object p1

    check-cast p1, Ljavax/net/ssl/SSLSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6

    const/4 p2, 0x1

    :try_start_1
    invoke-virtual {p1, p2}, Ljavax/net/ssl/SSLSocket;->setUseClientMode(Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    if-eqz v1, :cond_0

    :try_start_2
    iget-object p2, p0, Lcom/jscape/inet/ftps/FtpsClient;->s:Lcom/jscape/inet/ftps/FtpsCertificateVerifier;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    if-eqz p2, :cond_0

    if-eqz v1, :cond_0

    :try_start_3
    iget-object p2, p0, Lcom/jscape/inet/ftps/FtpsClient;->s:Lcom/jscape/inet/ftps/FtpsCertificateVerifier;

    invoke-interface {p2}, Lcom/jscape/inet/ftps/FtpsCertificateVerifier;->authorized()Z

    move-result p2

    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/jscape/inet/ftps/FtpsClient;->s:Lcom/jscape/inet/ftps/FtpsCertificateVerifier;

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/jscape/inet/ftps/FtpsCertificateVerifier;->verify(Ljavax/net/ssl/SSLSession;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    :cond_0
    if-eqz v1, :cond_1

    :try_start_4
    iget-object p2, p0, Lcom/jscape/inet/ftps/FtpsClient;->f:[Ljava/lang/String;

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/jscape/inet/ftps/FtpsClient;->f:[Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljavax/net/ssl/SSLSocket;->setEnabledCipherSuites([Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    :cond_1
    :goto_0
    :try_start_6
    iget-object p2, p0, Lcom/jscape/inet/ftps/FtpsClient;->y:Ljavax/net/ssl/HandshakeCompletedListener;

    if-eqz p2, :cond_2

    iget-object p2, p0, Lcom/jscape/inet/ftps/FtpsClient;->y:Ljavax/net/ssl/HandshakeCompletedListener;

    invoke-virtual {p1, p2}, Ljavax/net/ssl/SSLSocket;->addHandshakeCompletedListener(Ljavax/net/ssl/HandshakeCompletedListener;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    :cond_2
    return-object p1

    :catch_1
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    :catch_2
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    :catch_3
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    :catch_4
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    :catch_5
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    :catch_6
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method private a()V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/FtpsClient;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x2b

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method static a(Lcom/jscape/inet/ftps/FtpsClient;Ljava/net/Socket;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/net/Socket;)V

    return-void
.end method

.method static a(Lcom/jscape/inet/ftps/FtpsClient;Ljavax/net/ssl/SSLSocket;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljavax/net/ssl/SSLSocket;)V

    return-void
.end method

.method private static a(Lcom/jscape/inet/ftps/Response;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Response;->isNegative()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/Response;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
.end method

.method private a(Ljava/net/Socket;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->getTimeout()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private a(Ljavax/net/ssl/SSLSocket;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    invoke-static {v0, p1}, Lcom/jscape/util/k/c/SslContextFactory;->reuseSession(Ljavax/net/ssl/SSLSocket;Ljavax/net/ssl/SSLSocket;)Ljavax/net/ssl/SSLSocket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    iget-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->g:Ljava/util/logging/Logger;

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v1, 0x36

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private a(Z)V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->createSocket()Ljava/net/Socket;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;

    if-eqz p1, :cond_0

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/net/Socket;Z)Ljava/net/Socket;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->p:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz v1, :cond_1

    :try_start_3
    new-instance v1, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;

    iget-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v2}, Lcom/jscape/inet/ftps/Ftps;->getHostname()Ljava/lang/String;

    move-result-object v5

    iget-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/util/ConnectionParameters;->getHost()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v2}, Lcom/jscape/inet/ftps/Ftps;->getTimeout()I

    move-result v8

    iget-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/util/ConnectionParameters;->getTcpNoDelay()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    iget-boolean v11, p0, Lcom/jscape/inet/ftps/FtpsClient;->r:Z

    const/4 v12, 0x0

    move-object v2, v1

    move-object v3, p0

    move-object v4, p0

    move v10, p1

    invoke-direct/range {v2 .. v12}, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;-><init>(Lcom/jscape/inet/ftps/FtpsClient;Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;IIZZZLcom/jscape/inet/ftps/FtpsClient$1;)V

    iput-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    if-nez v0, :cond_2

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    new-instance v11, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v0}, Lcom/jscape/inet/util/ConnectionParameters;->getHost()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->getHostname()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->getTimeout()I

    move-result v6

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v0}, Lcom/jscape/inet/util/ConnectionParameters;->getTcpNoDelay()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    iget-boolean v9, p0, Lcom/jscape/inet/ftps/FtpsClient;->r:Z

    const/4 v10, 0x0

    move-object v0, v11

    move-object v1, p0

    move-object v2, p0

    move v8, p1

    invoke-direct/range {v0 .. v10}, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;-><init>(Lcom/jscape/inet/ftps/FtpsClient;Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;IIZZZLcom/jscape/inet/ftps/FtpsClient$1;)V

    iput-object v11, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    :cond_2
    iput-boolean p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->l:Z

    iget-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/net/Socket;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/FtpsClient;->readResponse()Lcom/jscape/inet/ftps/Response;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    return-void

    :catch_3
    move-exception p1

    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private a(Ljava/lang/String;)Z
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/StringTokenizer;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    const-string v2, "."

    invoke-direct {v1, p1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->countTokens()I

    move-result p1
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    const/4 p1, 0x1

    :cond_0
    move v3, p1

    :cond_1
    return v3

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method static b(Lcom/jscape/inet/ftps/FtpsClient;)I
    .locals 0

    iget p0, p0, Lcom/jscape/inet/ftps/FtpsClient;->v:I

    return p0
.end method

.method private b([B)Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;
    .locals 6

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, ""

    move v3, v1

    :cond_0
    array-length v4, p1

    const/4 v5, 0x1

    if-ge v3, v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-byte v2, p1, v3

    and-int/lit16 v2, v2, 0xff

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "."

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_2

    add-int/lit8 v3, v3, 0x1

    if-nez v0, :cond_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result p1

    sub-int/2addr p1, v5

    invoke-virtual {v2, v1, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    :cond_2
    new-instance p1, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    aget-object v1, v1, v5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/jscape/inet/ftps/FtpsClient;->d()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p1, p0, v2, v0, v1}, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;-><init>(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ftps/FtpsClient$1;)V

    return-object p1
.end method

.method private b()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/inet/ftps/FtpsClient;->c()Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;

    move-result-object v1

    :try_start_0
    iget-boolean v2, p0, Lcom/jscape/inet/ftps/FtpsClient;->n:Z
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_8

    const/4 v3, 0x1

    if-eqz v0, :cond_1

    if-eq v2, v3, :cond_0

    :try_start_1
    iget-boolean v2, p0, Lcom/jscape/inet/ftps/FtpsClient;->x:Z
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_9

    if-eqz v0, :cond_8

    if-ne v2, v3, :cond_7

    :cond_0
    :try_start_2
    invoke-virtual {v1}, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;->getHostname()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/jscape/inet/ftps/FtpsClient;->b(Ljava/lang/String;)Z

    move-result v2
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_7

    :cond_1
    :goto_0
    if-ne v2, v3, :cond_2

    :try_start_3
    iget-object v4, p0, Lcom/jscape/inet/ftps/FtpsClient;->g:Ljava/util/logging/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v7, 0x9

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;->getHostname()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_2
    :goto_1
    if-eqz v0, :cond_4

    if-nez v2, :cond_3

    :try_start_4
    iget-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    sget-object v4, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v5, 0x2e

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Lcom/jscape/inet/ftps/Ftps;->isFeatureSupported(Ljava/lang/String;)Z

    move-result v2
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_2

    if-eqz v0, :cond_4

    if-eqz v2, :cond_5

    goto :goto_2

    :catch_2
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_3
    :goto_2
    invoke-virtual {v1}, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;->isEPRTRequired()Z

    move-result v2
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_3

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_4
    :goto_3
    if-eqz v0, :cond_6

    if-eqz v2, :cond_5

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    goto :goto_5

    :cond_6
    move v3, v2

    :goto_4
    move v2, v3

    :goto_5
    iput-boolean v2, p0, Lcom/jscape/inet/ftps/FtpsClient;->n:Z

    :cond_7
    if-eqz v0, :cond_9

    :try_start_7
    iget-boolean v2, p0, Lcom/jscape/inet/ftps/FtpsClient;->n:Z
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_7

    :cond_8
    if-eqz v2, :cond_9

    :try_start_8
    invoke-virtual {v1}, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;->getCommandData()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/jscape/inet/ftps/FtpsClient;->c(Ljava/lang/String;)V
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_5

    if-nez v0, :cond_a

    goto :goto_6

    :catch_5
    move-exception v0

    :try_start_9
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_6

    :catch_6
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_7
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_9
    :goto_6
    invoke-virtual {v1}, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;->getCommandData()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/ftps/FtpsClient;->d(Ljava/lang/String;)V

    :cond_a
    return-void

    :catch_8
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_9

    :catch_9
    move-exception v0

    :try_start_b
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_a

    :catch_a
    move-exception v0

    :try_start_c
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_0

    :goto_7
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 8

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x41

    aget-object v1, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_5

    if-nez v1, :cond_1

    :try_start_1
    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v4, 0x31

    aget-object v1, v1, v4
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_6

    if-eqz v0, :cond_2

    :try_start_2
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_7

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    :try_start_3
    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v4, 0x39

    aget-object v1, v1, v4

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_9

    if-eqz v1, :cond_3

    goto :goto_0

    :cond_0
    move-object v1, p1

    goto :goto_1

    :cond_1
    :goto_0
    move-object v1, p1

    :cond_2
    const-string v4, "."

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_4

    new-instance v5, Ljava/util/StringTokenizer;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v1, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_4
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v1
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_0

    const/4 v4, 0x4

    if-ne v1, v4, :cond_3

    return v2

    :cond_3
    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v4, 0x12

    aget-object v1, v1, v4

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_4
    :goto_1
    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    move v5, v3

    :cond_5
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-eqz v6, :cond_6

    add-int/lit8 v5, v5, 0x1

    if-eqz v0, :cond_7

    if-nez v0, :cond_5

    :cond_6
    if-eqz v0, :cond_14

    if-le v5, v2, :cond_7

    goto/16 :goto_7

    :cond_7
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v6, ":"

    invoke-direct {v4, p1, v6, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    :cond_8
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_9

    :try_start_5
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_1

    if-eqz v0, :cond_b

    if-nez v0, :cond_8

    goto :goto_2

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_9
    :goto_2
    if-eqz v0, :cond_c

    if-nez v5, :cond_a

    :try_start_6
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v5
    :try_end_6
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_2

    if-eqz v0, :cond_c

    const/16 v1, 0x8

    if-eq v5, v1, :cond_a

    return v3

    :catch_2
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_7} :catch_3

    :catch_3
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Ljava/lang/IllegalStateException; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_a
    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v4, 0x3b

    aget-object v1, v1, v4

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    :cond_b
    move v5, v3

    :cond_c
    move v4, v5

    :goto_3
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v6

    if-ge v4, v6, :cond_13

    invoke-virtual {p1, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    if-eqz v0, :cond_12

    move v6, v3

    :cond_d
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v6, v7, :cond_11

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v7}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/regex/Matcher;->find()Z

    move-result v7

    if-eqz v0, :cond_10

    if-eqz v0, :cond_f

    if-nez v7, :cond_e

    goto :goto_4

    :cond_e
    add-int/lit8 v6, v6, 0x1

    if-nez v0, :cond_d

    goto :goto_5

    :cond_f
    move v3, v7

    :goto_4
    return v3

    :cond_10
    move v4, v7

    goto :goto_3

    :cond_11
    :goto_5
    add-int/lit8 v5, v5, 0x1

    if-nez v0, :cond_c

    goto :goto_6

    :cond_12
    move v2, v3

    :cond_13
    :goto_6
    return v2

    :cond_14
    move v3, v5

    :goto_7
    return v3

    :catch_5
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Ljava/lang/IllegalStateException; {:try_start_9 .. :try_end_9} :catch_6

    :catch_6
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Ljava/lang/IllegalStateException; {:try_start_a .. :try_end_a} :catch_7

    :catch_7
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Ljava/lang/IllegalStateException; {:try_start_b .. :try_end_b} :catch_8

    :catch_8
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_c
    .catch Ljava/lang/IllegalStateException; {:try_start_c .. :try_end_c} :catch_9

    :catch_9
    move-exception p1

    :try_start_d
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_d
    .catch Ljava/lang/IllegalStateException; {:try_start_d .. :try_end_d} :catch_a

    :catch_a
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private c()Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/inet/ftps/FtpsClient;->e()[B

    move-result-object v1

    array-length v2, v1

    const/16 v3, 0x10

    if-eqz v0, :cond_1

    if-ne v2, v3, :cond_1

    :try_start_0
    iget-boolean v4, p0, Lcom/jscape/inet/ftps/FtpsClient;->n:Z
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_2

    if-eqz v4, :cond_0

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x34

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_2

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    move v4, v2

    :cond_2
    const/4 v5, 0x1

    const/4 v6, 0x4

    if-eqz v0, :cond_5

    if-ne v4, v3, :cond_4

    :try_start_4
    iget-boolean v4, p0, Lcom/jscape/inet/ftps/FtpsClient;->n:Z
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_3

    if-eqz v0, :cond_3

    if-ne v4, v5, :cond_4

    invoke-direct {p0, v1}, Lcom/jscape/inet/ftps/FtpsClient;->c([B)Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;

    move-result-object v0

    goto :goto_4

    :cond_3
    move v3, v5

    goto :goto_1

    :catch_3
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_4
    move v4, v2

    move v3, v6

    :cond_5
    :goto_1
    if-eqz v0, :cond_8

    if-ne v4, v3, :cond_7

    :try_start_6
    iget-boolean v3, p0, Lcom/jscape/inet/ftps/FtpsClient;->n:Z
    :try_end_6
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_6 .. :try_end_6} :catch_5

    if-eqz v0, :cond_6

    if-ne v3, v5, :cond_7

    invoke-direct {p0, v1}, Lcom/jscape/inet/ftps/FtpsClient;->b([B)Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;

    move-result-object v0

    goto :goto_4

    :cond_6
    move v2, v3

    goto :goto_2

    :catch_5
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_6

    :catch_6
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_7
    if-eqz v0, :cond_9

    move v5, v6

    goto :goto_2

    :cond_8
    move v5, v3

    move v2, v4

    :goto_2
    if-ne v2, v5, :cond_b

    if-eqz v0, :cond_a

    :try_start_8
    iget-boolean v2, p0, Lcom/jscape/inet/ftps/FtpsClient;->n:Z
    :try_end_8
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_8 .. :try_end_8} :catch_7

    :cond_9
    if-nez v2, :cond_b

    goto :goto_3

    :catch_7
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_a
    :goto_3
    invoke-direct {p0, v1}, Lcom/jscape/inet/ftps/FtpsClient;->a([B)Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;

    move-result-object v0

    goto :goto_4

    :cond_b
    const/4 v0, 0x0

    :goto_4
    return-object v0
.end method

.method private c([B)Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;
    .locals 8

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, ""

    move v3, v1

    :cond_0
    array-length v4, p1

    const/4 v5, 0x1

    if-ge v3, v4, :cond_1

    aget-byte v4, p1, v3

    and-int/lit16 v4, v4, 0xff

    add-int/lit16 v4, v4, 0x100

    const/16 v6, 0x10

    invoke-static {v4, v6}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v3, 0x1

    aget-byte v7, p1, v7

    and-int/lit16 v7, v7, 0xff

    add-int/lit16 v7, v7, 0x100

    invoke-static {v7, v6}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ":"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    if-eqz v0, :cond_2

    add-int/lit8 v3, v3, 0x2

    move-object v2, v4

    if-nez v0, :cond_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result p1

    sub-int/2addr p1, v5

    invoke-virtual {v2, v1, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "|"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/jscape/inet/ftps/FtpsClient;->d()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v4, p1, v1}, Lcom/jscape/inet/ftps/FtpsClient$ActiveModeData;-><init>(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ftps/FtpsClient$1;)V

    return-object v0
.end method

.method static c(Lcom/jscape/inet/ftps/FtpsClient;)Ljava/util/logging/Logger;
    .locals 0

    iget-object p0, p0, Lcom/jscape/inet/ftps/FtpsClient;->g:Ljava/util/logging/Logger;

    return-object p0
.end method

.method private c(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x2f

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    throw p1
.end method

.method private d()I
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    check-cast v0, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->getLocalPort()I

    move-result v0

    shr-int/lit8 v1, v0, 0x8

    and-int/lit16 v1, v1, 0xff

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    and-int/lit16 v0, v0, 0xff

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    mul-int/lit16 v1, v1, 0x100

    add-int/2addr v1, v0

    return v1
.end method

.method static d(Lcom/jscape/inet/ftps/FtpsClient;)Lcom/jscape/inet/ftps/Ftps;
    .locals 0

    iget-object p0, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    return-object p0
.end method

.method private d(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    throw p1
.end method

.method static e(Lcom/jscape/inet/ftps/FtpsClient;)Lcom/jscape/inet/util/ConnectionParameters;
    .locals 0

    iget-object p0, p0, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    return-object p0
.end method

.method private e(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    const-string v0, "("

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_3

    const/4 v3, -0x1

    const/4 v4, 0x0

    const-string v5, "|"

    if-eqz v1, :cond_1

    if-eq v2, v3, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const-string v2, ")"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-nez v1, :cond_3

    goto :goto_0

    :cond_0
    move v0, v4

    :goto_0
    :try_start_1
    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v1, :cond_2

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    if-eq v2, v3, :cond_4

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    :cond_2
    if-eqz v1, :cond_4

    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/util/StringTokenizer;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_4
    :try_start_3
    new-instance p1, Lcom/jscape/inet/ftp/FtpException;

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v1, 0x1e

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private e()[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/FtpsClient;->getPortAddress()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/inet/ftps/FtpsClient;->getPortAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :goto_1
    return-object v0

    :catch_1
    move-exception v0

    new-instance v1, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private f()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->q:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;

    if-eqz v0, :cond_1

    instance-of v1, v1, Ljavax/net/ssl/SSLSocket;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    if-eqz v1, :cond_0

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    :try_start_4
    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->i:Ljava/net/Socket;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :cond_1
    if-eqz v1, :cond_3

    :try_start_5
    invoke-direct {p0}, Lcom/jscape/inet/ftps/FtpsClient;->g()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_1

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_2
    :goto_1
    :try_start_6
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->i:Ljava/net/Socket;

    iput-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    :cond_3
    return-void

    :catch_4
    move-exception v0

    new-instance v1, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static f(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/ftps/FtpsClient;->z:Ljava/lang/String;

    return-void
.end method

.method private g()V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;

    instance-of v0, v0, Ljavax/net/ssl/SSLSocket;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;

    iput-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->j:Ljava/net/Socket;

    :cond_1
    return-void
.end method

.method public static h()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->z:Ljava/lang/String;

    return-object v0
.end method

.method public static openPlain(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/util/ConnectionParameters;Ljavax/net/ssl/SSLContext;[Ljava/lang/String;Ljava/util/logging/Logger;)Lcom/jscape/inet/ftps/FtpsClient;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    new-instance v6, Lcom/jscape/inet/ftps/FtpsClient;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ftps/FtpsClient;-><init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/util/ConnectionParameters;Ljavax/net/ssl/SSLContext;[Ljava/lang/String;Ljava/util/logging/Logger;)V

    const/4 p0, 0x0

    invoke-direct {v6, p0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Z)V

    return-object v6
.end method

.method public static openProtected(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/util/ConnectionParameters;Ljavax/net/ssl/SSLContext;[Ljava/lang/String;Ljava/util/logging/Logger;)Lcom/jscape/inet/ftps/FtpsClient;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    new-instance v6, Lcom/jscape/inet/ftps/FtpsClient;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/ftps/FtpsClient;-><init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/util/ConnectionParameters;Ljavax/net/ssl/SSLContext;[Ljava/lang/String;Ljava/util/logging/Logger;)V

    const/4 p0, 0x1

    invoke-direct {v6, p0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Z)V

    return-object v6
.end method


# virtual methods
.method public abort()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v1, 0xa

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    return-void
.end method

.method public account(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x17

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    return-void
.end method

.method public allocate(I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    return-void
.end method

.method public allocate(II)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x32

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 p1, 0xe

    aget-object p1, v1, p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    return-void
.end method

.method public append(Ljava/lang/String;)Ljava/net/Socket;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v3, 0x15

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/jscape/inet/ftps/FtpsClient$DataConnector;->openConnection(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object p1

    return-object p1
.end method

.method public authorize(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    iget-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->i:Ljava/net/Socket;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/net/Socket;Z)Ljava/net/Socket;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;

    return-void
.end method

.method public bufferSize()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x45

    aget-object v1, v1, v2

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {v1}, Lcom/jscape/inet/ftp/FtpException;->getMessage()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v3, 0x24

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :catch_1
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    throw v1
.end method

.method public changeToParentDirectory()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v1, 0x1b

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    return-void
.end method

.method public changeWorkingDirectory(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x2c

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    return-void
.end method

.method public clear()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v1, 0x37

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    invoke-direct {p0}, Lcom/jscape/inet/ftps/FtpsClient;->f()V

    return-void
.end method

.method public close()V
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->i:Ljava/net/Socket;

    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    invoke-interface {v0}, Lcom/jscape/inet/ftps/FtpsClient$DataConnector;->close()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    iput-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;

    iput-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->i:Ljava/net/Socket;

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    iput-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    return-void
.end method

.method public dataPort(Lcom/jscape/inet/util/i;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    invoke-interface {v0}, Lcom/jscape/inet/ftps/FtpsClient$DataConnector;->close()V

    new-instance v0, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;

    iget-boolean v4, p0, Lcom/jscape/inet/ftps/FtpsClient;->l:Z

    iget-object v5, p0, Lcom/jscape/inet/ftps/FtpsClient;->e:Ljavax/net/ssl/SSLContext;

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p0

    move-object v3, p1

    invoke-direct/range {v1 .. v6}, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;-><init>(Lcom/jscape/inet/ftps/FtpsClient;Lcom/jscape/inet/util/i;ZLjavax/net/ssl/SSLContext;Lcom/jscape/inet/ftps/FtpsClient$1;)V

    iput-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    invoke-direct {p0}, Lcom/jscape/inet/ftps/FtpsClient;->b()V

    return-void
.end method

.method public delete(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x33

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    return-void
.end method

.method public fileStructure(C)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    return-void
.end method

.method public getAutoDetectIpv6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->x:Z

    return v0
.end method

.method public getConnectBeforeCommand()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->r:Z

    return v0
.end method

.method public getFtpsCertificateVerifier()Lcom/jscape/inet/ftps/FtpsCertificateVerifier;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->s:Lcom/jscape/inet/ftps/FtpsCertificateVerifier;

    return-object v0
.end method

.method public getLastResponse()Lcom/jscape/inet/ftps/Response;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->t:Lcom/jscape/inet/ftps/Response;

    return-object v0
.end method

.method public getNATAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->w:Ljava/lang/String;

    return-object v0
.end method

.method public getPortAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->o:Ljava/lang/String;

    return-object v0
.end method

.method public getSslHandshakeCompletedListener()Ljavax/net/ssl/HandshakeCompletedListener;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->y:Ljavax/net/ssl/HandshakeCompletedListener;

    return-object v0
.end method

.method public getUseEPRT()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->n:Z

    return v0
.end method

.method public getUseEPSV()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->m:Z

    return v0
.end method

.method public help(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/ftps/Response;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public isClosed()Z
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public isShutdownCCC()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->q:Z

    return v0
.end method

.method public list(Ljava/lang/String;)Ljava/net/Socket;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v3, 0x1d

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    if-lez v0, :cond_0

    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :cond_0
    const-string p1, ""

    :cond_1
    :goto_0
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    invoke-interface {v0, p1}, Lcom/jscape/inet/ftps/FtpsClient$DataConnector;->openConnection(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public makeDirectory(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x19

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    return-void
.end method

.method public mlsd(Ljava/lang/String;)Ljava/net/Socket;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x3e

    aget-object v1, v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    invoke-interface {v0, p1}, Lcom/jscape/inet/ftps/FtpsClient$DataConnector;->openConnection(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object p1

    return-object p1
.end method

.method public nameList(Ljava/lang/String;)Ljava/net/Socket;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x1c

    aget-object v1, v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    invoke-interface {v0, p1}, Lcom/jscape/inet/ftps/FtpsClient$DataConnector;->openConnection(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object p1

    return-object p1
.end method

.method public noop()Lcom/jscape/inet/ftps/Response;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v1, 0x3a

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    move-result-object v0

    return-object v0
.end method

.method public passive(Z)V
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v15, p0

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v17

    :try_start_0
    iget-boolean v0, v15, Lcom/jscape/inet/ftps/FtpsClient;->m:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1d

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v17, :cond_1

    if-eq v0, v2, :cond_0

    :try_start_1
    iget-boolean v0, v15, Lcom/jscape/inet/ftps/FtpsClient;->x:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v17, :cond_e

    if-ne v0, v2, :cond_d

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v15

    goto/16 :goto_14

    :cond_0
    :goto_0
    :try_start_2
    iget-object v0, v15, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->getHostname()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v15, v0}, Lcom/jscape/inet/ftps/FtpsClient;->b(Ljava/lang/String;)Z

    move-result v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v1, v15

    goto/16 :goto_15

    :cond_1
    :goto_1
    move v3, v0

    iget-object v0, v15, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->getHostname()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v15, v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v17, :cond_2

    if-nez v3, :cond_9

    goto :goto_2

    :cond_2
    move v0, v3

    :goto_2
    if-nez v0, :cond_9

    :try_start_3
    new-instance v0, Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;

    iget-object v4, v15, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v4}, Lcom/jscape/inet/ftps/Ftps;->getHostname()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v0, v15, v4, v5}, Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;-><init>(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Lcom/jscape/inet/ftps/FtpsClient$1;)V

    new-instance v4, Lcom/jscape/util/Timeout;

    const-wide/16 v5, 0x3e8

    invoke-direct {v4, v5, v6}, Lcom/jscape/util/Timeout;-><init>(J)V

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;->start()V

    :cond_3
    invoke-virtual {v4}, Lcom/jscape/util/Timeout;->isExceeded()Z

    move-result v5

    if-nez v5, :cond_5

    const-wide/16 v5, 0x1

    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V

    :cond_4
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;->a(Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, v15, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;->b(Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Lcom/jscape/inet/ftps/Ftps;->setHostname(Ljava/lang/String;Z)V

    iget-object v5, v15, Lcom/jscape/inet/ftps/FtpsClient;->g:Ljava/util/logging/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v8, 0x47

    aget-object v8, v7, v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/jscape/util/Timeout;->getValue()J

    move-result-wide v8

    invoke-virtual {v4}, Lcom/jscape/util/Timeout;->remainedTime()J

    move-result-wide v10

    sub-long/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v8, 0x26

    aget-object v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v15, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v7}, Lcom/jscape/inet/ftps/Ftps;->getHostname()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    if-eqz v17, :cond_4

    :cond_5
    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;->a(Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;)Z

    move-result v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    if-eqz v17, :cond_8

    if-nez v0, :cond_7

    :try_start_5
    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v5, 0x14

    aget-object v5, v0, v5

    invoke-virtual {v4}, Lcom/jscape/util/Timeout;->isExceeded()Z

    move-result v4

    if-eqz v4, :cond_6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v5, 0x21

    aget-object v0, v0, v5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_6
    iget-object v0, v15, Lcom/jscape/inet/ftps/FtpsClient;->g:Ljava/util/logging/Logger;

    invoke-virtual {v0, v5}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    :cond_7
    iget-object v0, v15, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->getHostname()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v15, v0}, Lcom/jscape/inet/ftps/FtpsClient;->b(Ljava/lang/String;)Z

    move-result v0

    :cond_8
    move v3, v0

    goto :goto_3

    :catch_2
    move-exception v0

    move-object v4, v0

    invoke-static {v4}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    :cond_9
    :goto_3
    if-eqz v17, :cond_c

    if-nez v3, :cond_b

    :try_start_6
    iget-object v0, v15, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    sget-object v3, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/4 v4, 0x7

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Lcom/jscape/inet/ftps/Ftps;->isFeatureSupported(Ljava/lang/String;)Z

    move-result v3
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    if-eqz v17, :cond_c

    if-eqz v3, :cond_a

    goto :goto_4

    :cond_a
    move v3, v1

    goto :goto_5

    :catch_4
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    :catch_5
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_b
    :goto_4
    move v3, v2

    :cond_c
    :goto_5
    iput-boolean v3, v15, Lcom/jscape/inet/ftps/FtpsClient;->m:Z

    :cond_d
    if-eqz v17, :cond_f

    :try_start_8
    iget-boolean v0, v15, Lcom/jscape/inet/ftps/FtpsClient;->m:Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7

    :cond_e
    if-nez v0, :cond_f

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    aget-object v0, v0, v1

    invoke-virtual {v15, v0}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    move-result-object v0

    if-nez v17, :cond_10

    const/4 v0, 0x2

    :try_start_9
    new-array v0, v0, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_6

    :catch_6
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_7
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_f
    :goto_6
    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v3, 0x16

    aget-object v0, v0, v3

    invoke-virtual {v15, v0}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    move-result-object v0

    :cond_10
    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Response;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v3, ""

    if-eqz v17, :cond_1f

    :try_start_a
    iget-boolean v4, v15, Lcom/jscape/inet/ftps/FtpsClient;->m:Z
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_e

    if-nez v4, :cond_1f

    const/16 v4, 0x28

    if-eqz v17, :cond_13

    :try_start_b
    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v5
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_10

    const/4 v6, -0x1

    if-ne v5, v6, :cond_12

    new-instance v5, Ljava/util/StringTokenizer;

    const-string v6, ","

    invoke-direct {v5, v0, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_c
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v5

    const/4 v6, 0x6

    if-ne v5, v6, :cond_11

    if-nez v17, :cond_14

    goto :goto_7

    :cond_11
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    aget-object v1, v1, v4

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8

    :catch_8
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_12
    :goto_7
    :try_start_d
    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_9

    add-int/2addr v4, v2

    goto :goto_8

    :catch_9
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_13
    :goto_8
    const/16 v5, 0x29

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_14
    :try_start_e
    iget-object v4, v15, Lcom/jscape/inet/ftps/FtpsClient;->w:Ljava/lang/String;

    invoke-static {v4}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v4
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_b

    const/4 v5, 0x4

    const/16 v6, 0x2c

    if-eqz v17, :cond_19

    if-eqz v4, :cond_18

    :try_start_f
    iget-object v4, v15, Lcom/jscape/inet/ftps/FtpsClient;->w:Ljava/lang/String;

    invoke-direct {v15, v4}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/String;)Z

    move-result v4
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_c

    if-eqz v17, :cond_15

    if-eqz v4, :cond_17

    iget-object v3, v15, Lcom/jscape/inet/ftps/FtpsClient;->w:Ljava/lang/String;

    move v4, v1

    :cond_15
    if-ge v4, v5, :cond_16

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    add-int/2addr v7, v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v4, v4, 0x1

    if-eqz v17, :cond_1c

    if-nez v17, :cond_15

    :cond_16
    if-eqz v17, :cond_17

    goto :goto_9

    :cond_17
    :try_start_10
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_a

    :catch_a
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_18
    move v4, v1

    :cond_19
    if-ge v4, v5, :cond_1c

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    add-int/2addr v7, v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v17, :cond_1b

    const/4 v7, 0x3

    if-eqz v17, :cond_1d

    if-eq v4, v7, :cond_1a

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "."

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_1a
    add-int/lit8 v4, v4, 0x1

    :cond_1b
    if-nez v17, :cond_19

    :cond_1c
    :goto_9
    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    mul-int/lit16 v4, v1, 0x100

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    add-int/2addr v1, v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    :cond_1d
    add-int/2addr v4, v7

    if-nez v17, :cond_1e

    goto :goto_a

    :cond_1e
    move-object v0, v3

    move/from16 v18, v4

    goto :goto_b

    :catch_b
    move-exception v0

    :try_start_11
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_c

    :catch_c
    move-exception v0

    :try_start_12
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_d

    :catch_d
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_e
    move-exception v0

    move-object v1, v0

    :try_start_13
    invoke-static {v1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_f

    :catch_f
    move-exception v0

    :try_start_14
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_10

    :catch_10
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1f
    :goto_a
    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v15, v0}, Lcom/jscape/inet/ftps/FtpsClient;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    move/from16 v18, v0

    move-object v0, v1

    :goto_b
    :try_start_15
    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->p:Ljava/lang/String;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_18

    if-eqz v17, :cond_22

    if-eqz v1, :cond_21

    if-eqz v17, :cond_20

    :try_start_16
    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getProxyHost()Ljava/lang/String;

    move-result-object v1
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_12

    if-nez v1, :cond_20

    :try_start_17
    new-instance v12, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Ftps;->getHostname()Ljava/lang/String;

    move-result-object v4

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Ftps;->getTimeout()I

    move-result v7

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getTcpNoDelay()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    iget-boolean v9, v15, Lcom/jscape/inet/ftps/FtpsClient;->l:Z

    iget-boolean v10, v15, Lcom/jscape/inet/ftps/FtpsClient;->r:Z

    const/4 v11, 0x0

    move-object v1, v12

    move-object/from16 v2, p0

    move-object/from16 v3, p0

    move-object v5, v0

    move/from16 v6, v18

    invoke-direct/range {v1 .. v11}, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;-><init>(Lcom/jscape/inet/ftps/FtpsClient;Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;IIZZZLcom/jscape/inet/ftps/FtpsClient$1;)V

    iput-object v12, v15, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_11

    if-nez v17, :cond_23

    goto :goto_c

    :catch_11
    move-exception v0

    move-object v1, v15

    goto/16 :goto_13

    :catch_12
    move-exception v0

    move-object v1, v15

    goto/16 :goto_12

    :cond_20
    :goto_c
    :try_start_18
    new-instance v14, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Ftps;->getHostname()Ljava/lang/String;

    move-result-object v4

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Ftps;->getTimeout()I

    move-result v7

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getTcpNoDelay()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    iget-boolean v9, v15, Lcom/jscape/inet/ftps/FtpsClient;->l:Z

    iget-boolean v10, v15, Lcom/jscape/inet/ftps/FtpsClient;->r:Z

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getProxyHost()Ljava/lang/String;

    move-result-object v11

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getProxyPort()I

    move-result v12

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getProxyUserId()Ljava/lang/String;

    move-result-object v13

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getProxyPassword()Ljava/lang/String;

    move-result-object v16

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getProxyType()Ljava/lang/String;

    move-result-object v19
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_14

    const/16 v20, 0x0

    move-object v1, v14

    move-object/from16 v2, p0

    move-object/from16 v3, p0

    move-object v5, v0

    move/from16 v6, v18

    move-object/from16 p1, v0

    move-object v0, v14

    move-object/from16 v14, v16

    move-object/from16 v15, v19

    move-object/from16 v16, v20

    :try_start_19
    invoke-direct/range {v1 .. v16}, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;-><init>(Lcom/jscape/inet/ftps/FtpsClient;Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;IIZZZLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ftps/FtpsClient$1;)V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_13

    move-object/from16 v15, p0

    :try_start_1a
    iput-object v0, v15, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_14

    if-nez v17, :cond_23

    goto :goto_e

    :catch_13
    move-exception v0

    move-object/from16 v15, p0

    goto :goto_d

    :catch_14
    move-exception v0

    :goto_d
    :try_start_1b
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_21
    move-object/from16 p1, v0

    :goto_e
    if-eqz v17, :cond_24

    iget-object v0, v15, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v0}, Lcom/jscape/inet/util/ConnectionParameters;->getProxyHost()Ljava/lang/String;

    move-result-object v1
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_15

    goto :goto_f

    :catch_15
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_22
    move-object/from16 p1, v0

    :goto_f
    if-nez v1, :cond_24

    :try_start_1c
    new-instance v0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Ftps;->getHostname()Ljava/lang/String;

    move-result-object v5

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Ftps;->getTimeout()I

    move-result v7

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getTcpNoDelay()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    iget-boolean v9, v15, Lcom/jscape/inet/ftps/FtpsClient;->l:Z

    iget-boolean v10, v15, Lcom/jscape/inet/ftps/FtpsClient;->r:Z

    const/4 v11, 0x0

    move-object v1, v0

    move-object/from16 v2, p0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v6, v18

    invoke-direct/range {v1 .. v11}, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;-><init>(Lcom/jscape/inet/ftps/FtpsClient;Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;IIZZZLcom/jscape/inet/ftps/FtpsClient$1;)V

    iput-object v0, v15, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_16

    if-nez v17, :cond_23

    goto :goto_10

    :cond_23
    move-object v1, v15

    goto :goto_11

    :catch_16
    move-exception v0

    :try_start_1d
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_17

    :catch_17
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_24
    :goto_10
    new-instance v0, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Ftps;->getHostname()Ljava/lang/String;

    move-result-object v5

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Ftps;->getTimeout()I

    move-result v7

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getTcpNoDelay()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    iget-boolean v9, v15, Lcom/jscape/inet/ftps/FtpsClient;->l:Z

    iget-boolean v10, v15, Lcom/jscape/inet/ftps/FtpsClient;->r:Z

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getProxyHost()Ljava/lang/String;

    move-result-object v11

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getProxyPort()I

    move-result v12

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getProxyUserId()Ljava/lang/String;

    move-result-object v13

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getProxyPassword()Ljava/lang/String;

    move-result-object v14

    iget-object v1, v15, Lcom/jscape/inet/ftps/FtpsClient;->d:Lcom/jscape/inet/util/ConnectionParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getProxyType()Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    move-object v1, v0

    move-object/from16 v2, p0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v6, v18

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    invoke-direct/range {v1 .. v16}, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;-><init>(Lcom/jscape/inet/ftps/FtpsClient;Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;IIZZZLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/ftps/FtpsClient$1;)V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    :goto_11
    return-void

    :catch_18
    move-exception v0

    move-object v1, v15

    move-object v2, v0

    :try_start_1e
    invoke-static {v2}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_19

    :catch_19
    move-exception v0

    :try_start_1f
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_1a

    :catch_1a
    move-exception v0

    :goto_12
    :try_start_20
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_1b

    :catch_1b
    move-exception v0

    :goto_13
    :try_start_21
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_1c

    :catch_1c
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_1d
    move-exception v0

    move-object v1, v15

    move-object v2, v0

    :try_start_22
    invoke-static {v2}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_1e

    :catch_1e
    move-exception v0

    :try_start_23
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_1f

    :catch_1f
    move-exception v0

    :goto_14
    :try_start_24
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_24} :catch_20

    :catch_20
    move-exception v0

    :goto_15
    invoke-static {v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public password(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x3c

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    move-result-object p1

    return-object p1
.end method

.method public printWorkingDirectory()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v1, 0x46

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Response;->getMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public protectionLevel(C)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v3, 0x3f

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    const/16 v0, 0x50

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    iput-boolean p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->l:Z

    return-void

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public quit()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v1, 0x20

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public readResponse()Lcom/jscape/inet/ftps/Response;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Ftps;->getWireEncoding()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    iget-object v3, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    iget-object v4, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v4}, Lcom/jscape/inet/ftps/Ftps;->getWireEncoding()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    if-nez v0, :cond_1

    :cond_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    iget-object v3, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    :cond_1
    invoke-static {v1}, Lcom/jscape/inet/ftps/Response;->readFrom(Ljava/io/BufferedReader;)Lcom/jscape/inet/ftps/Response;

    move-result-object v2

    :cond_2
    invoke-virtual {v2}, Lcom/jscape/inet/ftps/Response;->isPreliminary()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/jscape/inet/ftps/FtpsClient;->g:Ljava/util/logging/Logger;

    invoke-virtual {v2}, Lcom/jscape/inet/ftps/Response;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/jscape/inet/ftps/Response;->readFrom(Ljava/io/BufferedReader;)Lcom/jscape/inet/ftps/Response;

    move-result-object v2

    if-eqz v0, :cond_4

    if-nez v0, :cond_2

    :cond_3
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    new-instance v1, Lcom/jscape/inet/ftp/FtpResponseEvent;

    iget-object v3, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v2}, Lcom/jscape/inet/ftps/Response;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/jscape/inet/ftp/FtpResponseEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->g:Ljava/util/logging/Logger;

    invoke-virtual {v2}, Lcom/jscape/inet/ftps/Response;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient;->t:Lcom/jscape/inet/ftps/Response;

    invoke-static {v2}, Lcom/jscape/inet/ftps/FtpsClient;->a(Lcom/jscape/inet/ftps/Response;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    return-object v2

    :catch_0
    move-exception v0

    new-instance v1, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public reinitialize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v1, 0x38

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    return-void
.end method

.method public removeDirectory(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x13

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    return-void
.end method

.method public rename(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x1a

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v0, 0xb

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    return-void
.end method

.method public representationType(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x29

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    return-void
.end method

.method public restart(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    return-void
.end method

.method public retrieve(Ljava/lang/String;)Ljava/net/Socket;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v1, 0x42

    aget-object v1, v0, v1

    invoke-static {p1, v1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x2d

    aget-object v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/jscape/inet/ftps/FtpsClient$DataConnector;->openConnection(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object p1

    return-object p1
.end method

.method public sendCommand(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->h:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v2, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v2}, Lcom/jscape/inet/ftps/Ftps;->getWireEncoding()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v4, 0x22

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v3}, Lcom/jscape/inet/ftps/Ftps;->getWireEncoding()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    :cond_0
    :goto_0
    if-nez v0, :cond_2

    :cond_1
    :try_start_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v4, 0x44

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6

    :cond_2
    :goto_1
    :try_start_5
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    if-eqz v0, :cond_3

    :try_start_6
    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    new-instance v2, Lcom/jscape/inet/ftp/FtpCommandEvent;

    iget-object v3, p0, Lcom/jscape/inet/ftps/FtpsClient;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-direct {v2, v3, p1}, Lcom/jscape/inet/ftp/FtpCommandEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    if-eqz p1, :cond_3

    :try_start_7
    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x1f

    aget-object v1, v1, v2

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    if-eqz v1, :cond_3

    :try_start_8
    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->g:Ljava/util/logging/Logger;

    sget-object v2, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v3, 0x35

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    if-nez v0, :cond_4

    :cond_3
    :try_start_9
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->g:Ljava/util/logging/Logger;

    invoke-virtual {v0, p1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6

    :cond_4
    return-void

    :catch_2
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    :catch_3
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4

    :catch_4
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5

    :catch_5
    move-exception p1

    :try_start_d
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_6

    :catch_6
    move-exception p1

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->g:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    sget-object v2, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v3, 0x25

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/ftps/FtpsClient;->a()V

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendCommand(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jscape/inet/ftps/FtpsClient;->readResponse()Lcom/jscape/inet/ftps/Response;

    move-result-object p1

    return-object p1
.end method

.method public setAutoDetectIpv6(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->x:Z

    return-void
.end method

.method public setConnectBeforeCommand(Z)V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iput-boolean p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->r:Z

    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_0

    if-eqz v1, :cond_2

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    if-eqz v0, :cond_1

    :try_start_2
    instance-of v0, v1, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0

    if-eqz v0, :cond_2

    :try_start_3
    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    check-cast v1, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;

    invoke-virtual {v1, p1}, Lcom/jscape/inet/ftps/FtpsClient$PassiveConnector;->setConnectBeforeCommand(Z)V

    :cond_2
    return-void

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public setFtpsCertificateVerifier(Lcom/jscape/inet/ftps/FtpsCertificateVerifier;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->s:Lcom/jscape/inet/ftps/FtpsCertificateVerifier;

    return-void
.end method

.method public setNATAddress(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    :try_start_2
    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->w:Ljava/lang/String;

    if-eqz v0, :cond_1

    return-void

    :cond_1
    new-instance p1, Lcom/jscape/inet/ftp/FtpException;

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v1, 0xd

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public setPortAddress(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->o:Ljava/lang/String;

    return-void
.end method

.method public setReceiveBufferSize(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->v:I

    return-void
.end method

.method public setSendBufferSize(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->u:I

    return-void
.end method

.method public setShutdownCCC(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->q:Z

    return-void
.end method

.method public setSslHandshakeCompletedListener(Ljavax/net/ssl/HandshakeCompletedListener;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->y:Ljavax/net/ssl/HandshakeCompletedListener;

    return-void
.end method

.method public setUseEPRT(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->n:Z

    return-void
.end method

.method public setUseEPSV(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->m:Z

    return-void
.end method

.method public siteParameters()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v1, 0x18

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Response;->getMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public status(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x11

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/ftps/Response;->getMessage()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public store(Ljava/lang/String;)Ljava/net/Socket;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v1, 0x27

    aget-object v1, v0, v1

    invoke-static {p1, v1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x2a

    aget-object v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/jscape/inet/ftps/FtpsClient$DataConnector;->openConnection(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object p1

    return-object p1
.end method

.method public storeUnique(Ljava/lang/String;)Ljava/net/Socket;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v3, 0x3d

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/jscape/inet/ftps/FtpsClient$DataConnector;->openConnection(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    iget-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient;->k:Lcom/jscape/inet/ftps/FtpsClient$DataConnector;

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v1, 0x10

    aget-object v0, v0, v1

    invoke-interface {p1, v0}, Lcom/jscape/inet/ftps/FtpsClient$DataConnector;->openConnection(Ljava/lang/String;)Ljava/net/Socket;

    move-result-object p1

    return-object p1
.end method

.method public structureMount(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x40

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    return-void
.end method

.method public system()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v1, 0x30

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Response;->getMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public transferMode(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x23

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    return-void
.end method

.method public userName(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/ftps/FtpsClient;->A:[Ljava/lang/String;

    const/16 v2, 0x43

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendRequest(Ljava/lang/String;)Lcom/jscape/inet/ftps/Response;

    move-result-object p1

    return-object p1
.end method
