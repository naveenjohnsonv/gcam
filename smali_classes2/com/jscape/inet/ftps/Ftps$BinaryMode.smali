.class Lcom/jscape/inet/ftps/Ftps$BinaryMode;
.super Lcom/jscape/inet/ftps/Ftps$TransferMode;


# instance fields
.field final c:Lcom/jscape/inet/ftps/Ftps;


# direct methods
.method private constructor <init>(Lcom/jscape/inet/ftps/Ftps;)V
    .locals 2

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$BinaryMode;->c:Lcom/jscape/inet/ftps/Ftps;

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/jscape/inet/ftps/Ftps$TransferMode;-><init>(Lcom/jscape/inet/ftps/Ftps;ILcom/jscape/inet/ftps/Ftps$1;)V

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/ftps/Ftps$BinaryMode;-><init>(Lcom/jscape/inet/ftps/Ftps;)V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public transmit(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;IJJ)J
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    iget-object v2, v1, Lcom/jscape/inet/ftps/Ftps$BinaryMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-static {v2}, Lcom/jscape/inet/ftps/Ftps;->b(Lcom/jscape/inet/ftps/Ftps;)I

    move-result v2

    new-array v2, v2, [B

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    :try_start_0
    const-string v6, ""
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    :try_start_1
    iget-object v7, v1, Lcom/jscape/inet/ftps/Ftps$BinaryMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-static {v7}, Lcom/jscape/inet/ftps/Ftps;->c(Lcom/jscape/inet/ftps/Ftps;)Ljava/io/File;

    move-result-object v7
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    if-eqz v3, :cond_0

    if-eqz v7, :cond_1

    :try_start_2
    iget-object v6, v1, Lcom/jscape/inet/ftps/Ftps$BinaryMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-static {v6}, Lcom/jscape/inet/ftps/Ftps;->c(Lcom/jscape/inet/ftps/Ftps;)Ljava/io/File;

    move-result-object v7
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    :cond_0
    :try_start_3
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    :cond_1
    cmp-long v7, p5, v4

    if-eqz v3, :cond_3

    if-nez v7, :cond_2

    :try_start_4
    iget-object v15, v1, Lcom/jscape/inet/ftps/Ftps$BinaryMode;->c:Lcom/jscape/inet/ftps/Ftps;

    new-instance v13, Lcom/jscape/inet/ftp/FtpProgressEvent;

    iget-object v8, v1, Lcom/jscape/inet/ftps/Ftps$BinaryMode;->c:Lcom/jscape/inet/ftps/Ftps;

    iget-object v7, v1, Lcom/jscape/inet/ftps/Ftps$BinaryMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-static {v7}, Lcom/jscape/inet/ftps/Ftps;->d(Lcom/jscape/inet/ftps/Ftps;)Ljava/lang/String;

    move-result-object v10

    add-long v16, p7, v4

    const-wide/16 v18, 0x0

    move-object v7, v13

    move-object/from16 v9, p3

    move-object v11, v6

    move/from16 v12, p4

    move-object v4, v13

    move-wide/from16 v13, v16

    move-object v5, v15

    move-wide/from16 v15, v18

    move-wide/from16 v17, p5

    invoke-direct/range {v7 .. v18}, Lcom/jscape/inet/ftp/FtpProgressEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJ)V

    invoke-virtual {v5, v4}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$BinaryMode;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_2
    :goto_0
    invoke-virtual {v0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v7
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :cond_3
    const-wide/16 v4, 0x0

    :goto_1
    const/4 v8, -0x1

    if-eq v7, v8, :cond_6

    :try_start_6
    iget-object v8, v1, Lcom/jscape/inet/ftps/Ftps$BinaryMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v8}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v8
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    if-eqz v3, :cond_4

    if-nez v8, :cond_6

    const/4 v8, 0x0

    move-object/from16 v15, p2

    :try_start_7
    invoke-virtual {v15, v2, v8, v7}, Ljava/io/OutputStream;->write([BII)V

    invoke-virtual/range {p2 .. p2}, Ljava/io/OutputStream;->flush()V

    int-to-long v13, v7

    add-long/2addr v4, v13

    iget-object v12, v1, Lcom/jscape/inet/ftps/Ftps$BinaryMode;->c:Lcom/jscape/inet/ftps/Ftps;

    new-instance v11, Lcom/jscape/inet/ftp/FtpProgressEvent;

    iget-object v8, v1, Lcom/jscape/inet/ftps/Ftps$BinaryMode;->c:Lcom/jscape/inet/ftps/Ftps;

    iget-object v7, v1, Lcom/jscape/inet/ftps/Ftps$BinaryMode;->c:Lcom/jscape/inet/ftps/Ftps;

    invoke-static {v7}, Lcom/jscape/inet/ftps/Ftps;->d(Lcom/jscape/inet/ftps/Ftps;)Ljava/lang/String;

    move-result-object v10

    add-long v16, v4, p7

    move-object v7, v11

    move-object/from16 v9, p3

    move-object v1, v11

    move-object v11, v6

    move-wide/from16 v20, v4

    move-object v4, v12

    move/from16 v12, p4

    move-wide/from16 v18, v13

    move-wide/from16 v13, v16

    move-wide/from16 v15, v18

    move-wide/from16 v17, p5

    invoke-direct/range {v7 .. v18}, Lcom/jscape/inet/ftp/FtpProgressEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJJ)V

    invoke-virtual {v4, v1}, Lcom/jscape/inet/ftps/Ftps;->fireEvent(Lcom/jscape/inet/ftp/FtpEvent;)V

    invoke-virtual {v0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v1

    move v7, v1

    move-wide/from16 v4, v20

    goto :goto_2

    :cond_4
    move v7, v8

    :goto_2
    if-nez v3, :cond_5

    goto :goto_3

    :cond_5
    move-object/from16 v1, p0

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$BinaryMode;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :cond_6
    :goto_3
    return-wide v4

    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$BinaryMode;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    :catch_3
    move-exception v0

    :try_start_9
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$BinaryMode;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    :catch_4
    move-exception v0

    :try_start_a
    instance-of v1, v0, Ljavax/net/ssl/SSLHandshakeException;
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    if-eqz v1, :cond_7

    const-wide/16 v1, 0x0

    return-wide v1

    :cond_7
    new-instance v1, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_5
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps$BinaryMode;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method
