.class Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/ftps/FtpsClient$DataConnector;


# instance fields
.field private final a:Ljava/net/ServerSocket;

.field final b:Lcom/jscape/inet/ftps/FtpsClient;


# direct methods
.method private constructor <init>(Lcom/jscape/inet/ftps/FtpsClient;Lcom/jscape/inet/util/i;ZLjavax/net/ssl/SSLContext;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->b:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_1

    if-nez p3, :cond_0

    :try_start_0
    invoke-static {p2}, Lcom/jscape/inet/util/h;->a(Lcom/jscape/inet/util/i;)Ljava/net/ServerSocket;

    move-result-object p3

    iput-object p3, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->a:Ljava/net/ServerSocket;

    if-nez p1, :cond_2

    :cond_0
    invoke-virtual {p4}, Ljavax/net/ssl/SSLContext;->getServerSocketFactory()Ljavax/net/ssl/SSLServerSocketFactory;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/jscape/inet/util/h;->a(Ljavax/net/ServerSocketFactory;Lcom/jscape/inet/util/i;)Ljava/net/ServerSocket;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->a:Ljava/net/ServerSocket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->a:Ljava/net/ServerSocket;

    check-cast p1, Ljavax/net/ssl/SSLServerSocket;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Ljavax/net/ssl/SSLServerSocket;->setUseClientMode(Z)V

    :cond_2
    invoke-direct {p0}, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->a()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-void

    :catch_1
    move-exception p1

    new-instance p2, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method constructor <init>(Lcom/jscape/inet/ftps/FtpsClient;Lcom/jscape/inet/util/i;ZLjavax/net/ssl/SSLContext;Lcom/jscape/inet/ftps/FtpsClient$1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;-><init>(Lcom/jscape/inet/ftps/FtpsClient;Lcom/jscape/inet/util/i;ZLjavax/net/ssl/SSLContext;)V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->a:Ljava/net/ServerSocket;

    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->b:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-static {v1}, Lcom/jscape/inet/ftps/FtpsClient;->d(Lcom/jscape/inet/ftps/FtpsClient;)Lcom/jscape/inet/ftps/Ftps;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Ftps;->getTimeout()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/net/ServerSocket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public close()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->a:Ljava/net/ServerSocket;

    invoke-virtual {v0}, Ljava/net/ServerSocket;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public getLocalPort()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->a:Ljava/net/ServerSocket;

    invoke-virtual {v0}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v0

    return v0
.end method

.method public openConnection(Ljava/lang/String;)Ljava/net/Socket;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->b:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v1, p1}, Lcom/jscape/inet/ftps/FtpsClient;->sendCommand(Ljava/lang/String;)V

    :try_start_0
    iget-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->a:Ljava/net/ServerSocket;

    invoke-virtual {p1}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->b:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-static {v1}, Lcom/jscape/inet/ftps/FtpsClient;->e(Lcom/jscape/inet/ftps/FtpsClient;)Lcom/jscape/inet/util/ConnectionParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jscape/inet/util/ConnectionParameters;->getTcpNoDelay()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v1}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    if-eqz v0, :cond_0

    instance-of v0, p1, Ljavax/net/ssl/SSLSocket;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    :try_start_2
    move-object v0, p1

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    iget-object v1, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->b:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-static {v1, v0}, Lcom/jscape/inet/ftps/FtpsClient;->a(Lcom/jscape/inet/ftps/FtpsClient;Ljavax/net/ssl/SSLSocket;)V

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->startHandshake()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->b:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient;->readResponse()Lcom/jscape/inet/ftps/Response;

    return-object p1

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    :try_start_4
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$ActiveConnector;->b:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/FtpsClient;->readResponse()Lcom/jscape/inet/ftps/Response;

    throw p1
.end method
