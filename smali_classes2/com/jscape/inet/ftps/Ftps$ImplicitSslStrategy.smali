.class Lcom/jscape/inet/ftps/Ftps$ImplicitSslStrategy;
.super Lcom/jscape/inet/ftps/Ftps$DefaultStrategy;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "_\u001b[\u001a4K\rs\u0000\u000e\u0017%\u001f\u000by\u001aZ\u001f3\n\u0017yTY\u001e5\u0005Ct\u0015]V>\u0004\u0017<\u0016K\u0013>K\u0002i\u0000F\u0019\"\u0002\u0019y\u0010"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/ftps/Ftps$ImplicitSslStrategy;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x46

    goto :goto_1

    :cond_1
    const/16 v4, 0x4e

    goto :goto_1

    :cond_2
    const/16 v4, 0x75

    goto :goto_1

    :cond_3
    const/16 v4, 0x53

    goto :goto_1

    :cond_4
    const/16 v4, 0xb

    goto :goto_1

    :cond_5
    const/16 v4, 0x51

    goto :goto_1

    :cond_6
    const/16 v4, 0x39

    :goto_1
    const/16 v5, 0x25

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/jscape/inet/ftps/Ftps$DefaultStrategy;-><init>(Lcom/jscape/inet/ftps/Ftps$1;)V

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftps/Ftps$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/ftps/Ftps$ImplicitSslStrategy;-><init>()V

    return-void
.end method

.method private static a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public authenticate(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/inet/ftps/FtpsClient;->getFtpsCertificateVerifier()Lcom/jscape/inet/ftps/FtpsCertificateVerifier;

    move-result-object v1
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_3

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {p1}, Lcom/jscape/inet/ftps/FtpsClient;->getFtpsCertificateVerifier()Lcom/jscape/inet/ftps/FtpsCertificateVerifier;

    move-result-object v1
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_4

    :cond_0
    :try_start_2
    invoke-interface {v1}, Lcom/jscape/inet/ftps/FtpsCertificateVerifier;->authorized()Z

    move-result v1
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v0, :cond_2

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    :cond_2
    :goto_0
    if-eqz v1, :cond_3

    invoke-super {p0, p1, p2, p3, p4}, Lcom/jscape/inet/ftps/Ftps$DefaultStrategy;->authenticate(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :try_start_3
    invoke-virtual {p1}, Lcom/jscape/inet/ftps/FtpsClient;->bufferSize()V

    const/16 p2, 0x50

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ftps/FtpsClient;->protectionLevel(C)V
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    :catch_0
    if-eqz v0, :cond_3

    :goto_1
    return-void

    :cond_3
    :try_start_4
    new-instance p1, Lcom/jscape/inet/ftp/FtpException;

    sget-object p2, Lcom/jscape/inet/ftps/Ftps$ImplicitSslStrategy;->a:Ljava/lang/String;

    invoke-direct {p1, p2}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps$ImplicitSslStrategy;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object p1

    throw p1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps$ImplicitSslStrategy;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object p1

    throw p1

    :catch_3
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps$ImplicitSslStrategy;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/ftps/Ftps$ImplicitSslStrategy;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object p1

    throw p1
.end method

.method public createClient(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/util/ConnectionParameters;Ljavax/net/ssl/SSLContext;[Ljava/lang/String;Ljava/util/logging/Logger;)Lcom/jscape/inet/ftps/FtpsClient;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/ftp/FtpException;
        }
    .end annotation

    invoke-static {p1, p2, p3, p4, p5}, Lcom/jscape/inet/ftps/FtpsClient;->openProtected(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/util/ConnectionParameters;Ljavax/net/ssl/SSLContext;[Ljava/lang/String;Ljava/util/logging/Logger;)Lcom/jscape/inet/ftps/FtpsClient;

    move-result-object p1

    return-object p1
.end method
