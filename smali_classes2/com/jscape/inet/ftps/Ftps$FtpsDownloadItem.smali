.class Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;
.super Ljava/lang/Thread;


# static fields
.field private static final l:[Ljava/lang/String;


# instance fields
.field private a:Lcom/jscape/inet/ftps/Ftps;

.field private b:Lcom/jscape/inet/ftps/Ftps;

.field private c:Ljava/util/Vector;

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Ljava/io/File;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;

.field final k:Lcom/jscape/inet/ftps/Ftps;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x17

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x63

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "j/%*tm\u0017Dg2,kk\u0010G\"$iy\u007f\u000eZ\"\u0004q\u0004\u0012\n"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x1c

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->l:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    move v12, v7

    goto :goto_2

    :cond_2
    const/16 v12, 0x7d

    goto :goto_2

    :cond_3
    const/16 v12, 0x7c

    goto :goto_2

    :cond_4
    const/16 v12, 0x2a

    goto :goto_2

    :cond_5
    const/16 v12, 0x23

    goto :goto_2

    :cond_6
    const/16 v12, 0x24

    goto :goto_2

    :cond_7
    const/16 v12, 0x4a

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/util/Vector;ZLjava/lang/String;Ljava/io/File;)V
    .locals 2

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->k:Lcom/jscape/inet/ftps/Ftps;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a:Lcom/jscape/inet/ftps/Ftps;

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->c:Ljava/util/Vector;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->d:Z

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->e:Ljava/lang/String;

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->f:Ljava/io/File;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->g:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->h:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->i:Z

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->j:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a:Lcom/jscape/inet/ftps/Ftps;

    iput-object p3, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->c:Ljava/util/Vector;

    iput-boolean p4, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->d:Z

    iput-object p5, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->f:Ljava/io/File;

    new-instance p1, Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getHostname()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getUsername()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getPassword()Ljava/lang/String;

    move-result-object p5

    invoke-direct {p1, p3, p4, p5}, Lcom/jscape/inet/ftps/Ftps;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getPreserveDownloadTimestamp()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setPreserveDownloadTimestamp(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getPreserveUploadTimestamp()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setPreserveUploadTimestamp(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getTimeZone()Ljava/util/TimeZone;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setTimezone(Ljava/util/TimeZone;)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getConnectionType()Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setConnectionType(Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getPassive()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setPassive(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getPort()I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setPort(I)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getBlockTransferSize()I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setBlockTransferSize(I)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getCompression()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setCompression(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getDataPortStart()I

    move-result p3

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getDataPortEnd()I

    move-result p4

    invoke-virtual {p1, p3, p4}, Lcom/jscape/inet/ftps/Ftps;->setDataPortRange(II)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getErrorOnSizeCommand()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setErrorOnSizeCommand(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getFtpFileParser()Lcom/jscape/inet/ftp/FtpFileParser;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setFtpFileParser(Lcom/jscape/inet/ftp/FtpFileParser;)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getLinger()I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setLinger(I)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getPortAddress()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setPortAddress(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getProxyUsername()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getProxyPassword()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p1, p3, p4}, Lcom/jscape/inet/ftps/Ftps;->setProxyAuthentication(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getProxyHostname()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getProxyPort()I

    move-result p4

    invoke-virtual {p1, p3, p4}, Lcom/jscape/inet/ftps/Ftps;->setProxyHost(Ljava/lang/String;I)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getProxyType()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setProxyType(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getTimeout()I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setTimeout(I)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getListeners()Ljava/util/LinkedList;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ftps/Ftps;->setListeners(Ljava/util/LinkedList;)V

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/util/Vector;ZLjava/lang/String;Ljava/io/File;Lcom/jscape/inet/ftps/Ftps$1;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;-><init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/util/Vector;ZLjava/lang/String;Ljava/io/File;)V

    return-void
.end method

.method private static a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public cancelTransfer()V
    .locals 1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->h:Z

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->interrupt()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public error()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->i:Z

    return v0
.end method

.method public getConnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->g:Z

    return v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->j:Ljava/lang/String;

    return-object v0
.end method

.method public interruptTransfer()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->interrupt()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public run()V
    .locals 18

    move-object/from16 v7, p0

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    :try_start_0
    iget-object v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->connect()Lcom/jscape/inet/ftps/Ftps;

    iget-object v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->getMode()I

    move-result v0
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_12
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_11
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v10, 0x1

    if-eqz v8, :cond_1

    if-ne v0, v10, :cond_0

    :try_start_1
    iget-object v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->setAscii()V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_15
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_11
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v8, :cond_4

    :cond_0
    :try_start_2
    iget-object v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->getMode()I

    move-result v0
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_16
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_11
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v8, :cond_3

    const/4 v1, 0x2

    goto :goto_0

    :cond_1
    move v1, v10

    :goto_0
    if-ne v0, v1, :cond_2

    :try_start_3
    iget-object v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->setBinary()V
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_11
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v8, :cond_4

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :cond_2
    :goto_1
    if-eqz v8, :cond_5

    iget-object v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->getMode()I

    move-result v0
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_11
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_3
    if-nez v0, :cond_4

    :try_start_5
    iget-object v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0, v10}, Lcom/jscape/inet/ftps/Ftps;->setAuto(Z)V
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_11
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    :catch_1
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :cond_4
    :goto_2
    iput-boolean v10, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->g:Z

    goto :goto_3

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :cond_5
    :goto_3
    iget-object v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v11

    :goto_4
    invoke-interface {v11}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_11
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_5
    if-eqz v0, :cond_18

    if-eqz v8, :cond_17

    if-eqz v8, :cond_17

    :try_start_7
    iget-boolean v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->h:Z
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_10
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_11
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-nez v0, :cond_18

    :try_start_8
    iget-object v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->reset()V

    iget-object v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    iget-object v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->f:Ljava/io/File;

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V

    invoke-interface {v11}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v12}, Lcom/jscape/util/Q;->d(Ljava/io/File;Ljava/io/File;)Ljava/lang/String;

    move-result-object v13
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_11
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    const-string v14, "/"

    if-eqz v8, :cond_7

    if-eqz v13, :cond_6

    :try_start_9
    const-string v0, ""

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0, v13}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    iget-object v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v1, v13}, Lcom/jscape/inet/ftps/Ftps;->makeLocalDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_11
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_6

    :catch_3
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_11
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_6
    :goto_6
    :try_start_b
    iget-object v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ftps/Ftps;->download(Ljava/lang/String;)Ljava/io/File;

    :cond_7
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v2}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_b .. :try_end_b} :catch_c
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_11
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :try_start_c
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v1
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_a
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_11
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v8, :cond_8

    if-lez v1, :cond_15

    :try_start_d
    iget-boolean v1, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->d:Z
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_b
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_11
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :cond_8
    if-eqz v8, :cond_9

    if-eqz v1, :cond_15

    :try_start_e
    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a:Lcom/jscape/inet/ftps/Ftps;

    if-eqz v8, :cond_a

    sget-object v2, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->l:[Ljava/lang/String;

    aget-object v2, v2, v10

    invoke-virtual {v1, v2}, Lcom/jscape/inet/ftps/Ftps;->isFeatureSupported(Ljava/lang/String;)Z

    move-result v1
    :try_end_e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_e .. :try_end_e} :catch_4
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_11
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_7

    :catch_4
    move-exception v0

    :try_start_f
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_f .. :try_end_f} :catch_c
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_11
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :cond_9
    :goto_7
    if-eqz v1, :cond_15

    :try_start_10
    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_9
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_11
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :cond_a
    :try_start_11
    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object v1
    :try_end_11
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_11 .. :try_end_11} :catch_c
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :try_start_12
    invoke-virtual {v1, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2
    :try_end_12
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_12 .. :try_end_12} :catch_6
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_11
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    if-eqz v8, :cond_c

    if-nez v2, :cond_b

    :try_start_13
    const-string v2, "\\"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2
    :try_end_13
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_13 .. :try_end_13} :catch_8
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_11
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    if-eqz v8, :cond_c

    if-nez v2, :cond_b

    :try_start_14
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_b
    iget-object v2, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/jscape/inet/ftps/Ftps;->checksum(Ljava/io/File;Ljava/lang/String;)Z

    move-result v2
    :try_end_14
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_14 .. :try_end_14} :catch_c
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_11
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    :cond_c
    if-eqz v2, :cond_d

    goto/16 :goto_b

    :cond_d
    :try_start_15
    new-instance v0, Lcom/jscape/inet/ftp/FtpException;

    sget-object v1, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->l:[Ljava/lang/String;

    aget-object v1, v1, v9

    invoke-direct {v0, v1}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_15
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_15 .. :try_end_15} :catch_5
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_11
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    :catch_5
    move-exception v0

    :try_start_16
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_16
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_16 .. :try_end_16} :catch_c
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_11
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    :catch_6
    move-exception v0

    move-object v1, v0

    :try_start_17
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_17
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_17 .. :try_end_17} :catch_7
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_11
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    :catch_7
    move-exception v0

    :try_start_18
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_18
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_18 .. :try_end_18} :catch_8
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_11
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    :catch_8
    move-exception v0

    :try_start_19
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :catch_9
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_19
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_19 .. :try_end_19} :catch_c
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_11
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    :catch_a
    move-exception v0

    move-object v1, v0

    :try_start_1a
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_1a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1a .. :try_end_1a} :catch_b
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_11
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    :catch_b
    move-exception v0

    :try_start_1b
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_1b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1b .. :try_end_1b} :catch_c
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_11
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    :catch_c
    move-exception v0

    :try_start_1c
    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;
    :try_end_1c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1c .. :try_end_1c} :catch_e
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_11
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    if-eqz v8, :cond_f

    :try_start_1d
    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v1
    :try_end_1d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1d .. :try_end_1d} :catch_f
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_11
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    if-eqz v1, :cond_e

    goto/16 :goto_4

    :cond_e
    :try_start_1e
    iput-boolean v10, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->i:Z

    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->j:Ljava/lang/String;

    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->k:Lcom/jscape/inet/ftps/Ftps;

    :cond_f
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps;->a(Lcom/jscape/inet/ftps/Ftps;)Ljava/util/LinkedList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_8
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/ftp/FtpListener;
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_11
    .catchall {:try_start_1e .. :try_end_1e} :catchall_0

    if-eqz v8, :cond_11

    :try_start_1f
    instance-of v2, v1, Lcom/jscape/inet/ftp/FtpErrorListener;
    :try_end_1f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1f .. :try_end_1f} :catch_d
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_11
    .catchall {:try_start_1f .. :try_end_1f} :catchall_0

    if-eqz v8, :cond_10

    if-eqz v2, :cond_12

    goto :goto_9

    :cond_10
    move v0, v2

    goto/16 :goto_5

    :catch_d
    move-exception v0

    move-object v1, v0

    :try_start_20
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :cond_11
    :goto_9
    move-object v6, v1

    check-cast v6, Lcom/jscape/inet/ftp/FtpErrorListener;

    new-instance v5, Lcom/jscape/inet/ftp/FtpErrorEvent;

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->f:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_11
    .catchall {:try_start_20 .. :try_end_20} :catchall_0

    const/16 v17, 0x1

    move-object v1, v5

    move-object/from16 v2, p0

    move-object v10, v5

    move-object/from16 v5, v16

    move-object v9, v6

    move/from16 v6, v17

    :try_start_21
    invoke-direct/range {v1 .. v6}, Lcom/jscape/inet/ftp/FtpErrorEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v9, v10}, Lcom/jscape/inet/ftp/FtpErrorListener;->error(Lcom/jscape/inet/ftp/FtpErrorEvent;)V

    :cond_12
    if-nez v8, :cond_13

    goto :goto_a

    :cond_13
    const/4 v9, 0x0

    const/4 v10, 0x1

    goto :goto_8

    :cond_14
    :goto_a
    invoke-virtual {v0}, Lcom/jscape/inet/ftp/FtpException;->printStackTrace()V
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_13
    .catchall {:try_start_21 .. :try_end_21} :catchall_0

    :cond_15
    :goto_b
    if-nez v8, :cond_16

    goto :goto_c

    :cond_16
    const/4 v9, 0x0

    const/4 v10, 0x1

    goto/16 :goto_4

    :catch_e
    move-exception v0

    move-object v1, v0

    :try_start_22
    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_22
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_22 .. :try_end_22} :catch_f
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_13
    .catchall {:try_start_22 .. :try_end_22} :catchall_0

    :catch_f
    move-exception v0

    :try_start_23
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :catch_10
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_13
    .catchall {:try_start_23 .. :try_end_23} :catchall_0

    :cond_17
    move v1, v9

    goto :goto_f

    :cond_18
    :goto_c
    iget-object v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->disconnect()V

    const/4 v1, 0x0

    goto :goto_f

    :catchall_0
    move-exception v0

    goto :goto_d

    :catch_11
    move v1, v9

    goto :goto_e

    :catch_12
    move-exception v0

    :try_start_24
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_24
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_24 .. :try_end_24} :catch_14
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_24} :catch_13
    .catchall {:try_start_24 .. :try_end_24} :catchall_0

    :catch_13
    const/4 v1, 0x0

    goto :goto_e

    :catch_14
    move-exception v0

    :try_start_25
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_25
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_25 .. :try_end_25} :catch_15
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_25} :catch_13
    .catchall {:try_start_25 .. :try_end_25} :catchall_0

    :catch_15
    move-exception v0

    :try_start_26
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_26
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_26 .. :try_end_26} :catch_16
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_26} :catch_13
    .catchall {:try_start_26 .. :try_end_26} :catchall_0

    :catch_16
    move-exception v0

    :try_start_27
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_27} :catch_13
    .catchall {:try_start_27 .. :try_end_27} :catchall_0

    :goto_d
    iget-object v1, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Ftps;->disconnect()V

    const/4 v1, 0x0

    iput-boolean v1, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->g:Z

    throw v0

    :goto_e
    iget-object v0, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->disconnect()V

    :goto_f
    iput-boolean v1, v7, Lcom/jscape/inet/ftps/Ftps$FtpsDownloadItem;->g:Z

    return-void
.end method
