.class Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;
.super Ljava/lang/Thread;


# static fields
.field private static final n:[Ljava/lang/String;


# instance fields
.field private a:Lcom/jscape/inet/ftps/Ftps;

.field private b:Lcom/jscape/inet/ftps/Ftps;

.field private c:Ljava/lang/String;

.field private d:Ljava/util/Vector;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Ljava/io/File;

.field private h:Ljava/io/File;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Ljava/lang/String;

.field final m:Lcom/jscape/inet/ftps/Ftps;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x17

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x1

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    const-string v7, "CJf\u007fXz@m\u0002qyG|GnGg<UhYsG\u0004XaQ_"

    invoke-virtual {v7, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v8, v4

    move v9, v2

    :goto_1
    if-gt v8, v9, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x1c

    if-ge v3, v4, :cond_0

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->n:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v10, v4, v9

    rem-int/lit8 v11, v9, 0x7

    const/16 v12, 0x13

    if-eqz v11, :cond_7

    if-eq v11, v6, :cond_6

    if-eq v11, v0, :cond_5

    const/4 v13, 0x3

    if-eq v11, v13, :cond_4

    const/4 v13, 0x4

    if-eq v11, v13, :cond_3

    const/4 v13, 0x5

    if-eq v11, v13, :cond_2

    const/16 v11, 0x26

    goto :goto_2

    :cond_2
    const/16 v11, 0x1a

    goto :goto_2

    :cond_3
    const/16 v11, 0x20

    goto :goto_2

    :cond_4
    const/16 v11, 0xf

    goto :goto_2

    :cond_5
    const/16 v11, 0x10

    goto :goto_2

    :cond_6
    const/16 v11, 0x31

    goto :goto_2

    :cond_7
    move v11, v12

    :goto_2
    xor-int/2addr v11, v12

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v4, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;Ljava/util/Vector;Ljava/lang/String;Ljava/io/File;Z)V
    .locals 1

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->m:Lcom/jscape/inet/ftps/Ftps;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a:Lcom/jscape/inet/ftps/Ftps;

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->c:Ljava/lang/String;

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->d:Ljava/util/Vector;

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->e:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->f:Z

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->g:Ljava/io/File;

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->h:Ljava/io/File;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->i:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->j:Z

    iput-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->k:Z

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->l:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a:Lcom/jscape/inet/ftps/Ftps;

    iput-object p3, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->d:Ljava/util/Vector;

    iput-object p5, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->h:Ljava/io/File;

    iput-boolean p7, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->i:Z

    new-instance p1, Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getHostname()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getUsername()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getPassword()Ljava/lang/String;

    move-result-object p5

    invoke-direct {p1, p3, p4, p5}, Lcom/jscape/inet/ftps/Ftps;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getTimeZone()Ljava/util/TimeZone;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setTimezone(Ljava/util/TimeZone;)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getPreserveDownloadTimestamp()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setPreserveDownloadTimestamp(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getPreserveUploadTimestamp()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setPreserveUploadTimestamp(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getPassive()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setPassive(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getPort()I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setPort(I)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getBlockTransferSize()I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setBlockTransferSize(I)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getCompression()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setCompression(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getConnectionType()Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setConnectionType(Lcom/jscape/inet/ftps/Ftps$ConnectionStrategy;)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getDataPortStart()I

    move-result p3

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getDataPortEnd()I

    move-result p4

    invoke-virtual {p1, p3, p4}, Lcom/jscape/inet/ftps/Ftps;->setDataPortRange(II)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getErrorOnSizeCommand()Z

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setErrorOnSizeCommand(Z)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getFtpFileParser()Lcom/jscape/inet/ftp/FtpFileParser;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setFtpFileParser(Lcom/jscape/inet/ftp/FtpFileParser;)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getLocalDir()Ljava/io/File;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setLocalDir(Ljava/io/File;)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getLinger()I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setLinger(I)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getPortAddress()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setPortAddress(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getProxyUsername()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getProxyPassword()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p1, p3, p4}, Lcom/jscape/inet/ftps/Ftps;->setProxyAuthentication(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getProxyHostname()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getProxyPort()I

    move-result p4

    invoke-virtual {p1, p3, p4}, Lcom/jscape/inet/ftps/Ftps;->setProxyHost(Ljava/lang/String;I)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getProxyType()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setProxyType(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getTimeout()I

    move-result p3

    invoke-virtual {p1, p3}, Lcom/jscape/inet/ftps/Ftps;->setTimeout(I)V

    iget-object p1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {p2}, Lcom/jscape/inet/ftps/Ftps;->getListeners()Ljava/util/LinkedList;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ftps/Ftps;->setListeners(Ljava/util/LinkedList;)V

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;Ljava/util/Vector;Ljava/lang/String;Ljava/io/File;ZLcom/jscape/inet/ftps/Ftps$1;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;-><init>(Lcom/jscape/inet/ftps/Ftps;Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;Ljava/util/Vector;Ljava/lang/String;Ljava/io/File;Z)V

    return-void
.end method

.method private static a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public cancelTransfer()V
    .locals 1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->j:Z

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->interrupt()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public error()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->k:Z

    return v0
.end method

.method public getConnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->f:Z

    return v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->l:Ljava/lang/String;

    return-object v0
.end method

.method public interruptTransfer()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->interrupt()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public run()V
    .locals 15

    const-string v0, "/"

    invoke-static {}, Lcom/jscape/inet/ftps/FtpsClient;->h()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v3}, Lcom/jscape/inet/ftps/Ftps;->connect()Lcom/jscape/inet/ftps/Ftps;

    iget-object v3, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v3}, Lcom/jscape/inet/ftps/Ftps;->getMode()I

    move-result v3
    :try_end_0
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_0 .. :try_end_0} :catch_12
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_16
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v4, 0x1

    if-eqz v1, :cond_1

    if-ne v3, v4, :cond_0

    :try_start_1
    iget-object v3, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v3}, Lcom/jscape/inet/ftps/Ftps;->setAscii()V
    :try_end_1
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1 .. :try_end_1} :catch_14
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_16
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_4

    :cond_0
    :try_start_2
    iget-object v3, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v3}, Lcom/jscape/inet/ftps/Ftps;->getMode()I

    move-result v3
    :try_end_2
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_2 .. :try_end_2} :catch_15
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_16
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_3

    const/4 v5, 0x2

    goto :goto_0

    :cond_1
    move v5, v4

    :goto_0
    if-ne v3, v5, :cond_2

    :try_start_3
    iget-object v3, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v3}, Lcom/jscape/inet/ftps/Ftps;->setBinary()V
    :try_end_3
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_16
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v1, :cond_4

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :cond_2
    :goto_1
    if-eqz v1, :cond_5

    iget-object v3, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v3}, Lcom/jscape/inet/ftps/Ftps;->getMode()I

    move-result v3
    :try_end_4
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_16
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_3
    if-nez v3, :cond_4

    :try_start_5
    iget-object v3, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v3, v4}, Lcom/jscape/inet/ftps/Ftps;->setAuto(Z)V
    :try_end_5
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_16
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    :catch_1
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :cond_4
    :goto_2
    iput-boolean v4, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->f:Z

    goto :goto_3

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :cond_5
    :goto_3
    iget-object v3, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->d:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v3

    :cond_6
    :goto_4
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_16
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_5
    if-eqz v5, :cond_17

    if-eqz v1, :cond_18

    if-eqz v1, :cond_18

    :try_start_7
    iget-boolean v5, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->j:Z
    :try_end_7
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_7 .. :try_end_7} :catch_11
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_16
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-nez v5, :cond_17

    :try_start_8
    iget-object v5, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v5}, Lcom/jscape/inet/ftps/Ftps;->reset()V

    iget-object v5, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    iget-object v6, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->e:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/jscape/inet/ftps/Ftps;->setDir(Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->g:Ljava/io/File;

    iget-object v5, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->h:Ljava/io/File;

    invoke-static {v5, v6}, Lcom/jscape/util/Q;->d(Ljava/io/File;Ljava/io/File;)Ljava/lang/String;

    move-result-object v5
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_16
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v1, :cond_7

    if-eqz v5, :cond_7

    if-eqz v1, :cond_7

    :try_start_9
    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6
    :try_end_9
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_16
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-nez v6, :cond_7

    :try_start_a
    iget-object v6, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-static {v6, v5}, Lcom/jscape/inet/ftps/Ftps;->a(Lcom/jscape/inet/ftps/Ftps;Ljava/lang/String;)V

    goto :goto_6

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_16
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_4
    move-exception v0

    :try_start_b
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_16
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_7
    :goto_6
    :try_start_c
    iget-object v5, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->g:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v5
    :try_end_c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_c .. :try_end_c} :catch_10
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_16
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    if-eqz v1, :cond_8

    if-ne v5, v4, :cond_16

    :try_start_d
    iget-object v5, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    iget-object v6, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->c:Ljava/lang/String;

    iget-object v7, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->g:Ljava/io/File;

    invoke-virtual {v5, v6, v7}, Lcom/jscape/inet/ftps/Ftps;->upload(Ljava/lang/String;Ljava/io/File;)V

    iget-object v5, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->g:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5
    :try_end_d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_d .. :try_end_d} :catch_5
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_16
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    goto :goto_7

    :catch_5
    move-exception v5

    goto/16 :goto_a

    :cond_8
    :goto_7
    if-eqz v1, :cond_9

    if-lez v5, :cond_16

    :try_start_e
    iget-boolean v5, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->i:Z
    :try_end_e
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_e .. :try_end_e} :catch_6
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_16
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_8

    :catch_6
    move-exception v5

    :try_start_f
    invoke-static {v5}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v5

    throw v5
    :try_end_f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_f .. :try_end_f} :catch_5
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_16
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :cond_9
    :goto_8
    if-eqz v1, :cond_a

    if-eqz v5, :cond_16

    :try_start_10
    iget-object v5, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a:Lcom/jscape/inet/ftps/Ftps;

    if-eqz v1, :cond_b

    sget-object v6, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->n:[Ljava/lang/String;

    aget-object v6, v6, v4

    invoke-virtual {v5, v6}, Lcom/jscape/inet/ftps/Ftps;->isFeatureSupported(Ljava/lang/String;)Z

    move-result v5
    :try_end_10
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_10 .. :try_end_10} :catch_7
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_16
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto :goto_9

    :catch_7
    move-exception v5

    :try_start_11
    invoke-static {v5}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v5

    throw v5
    :try_end_11
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_11 .. :try_end_11} :catch_5
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_16
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :cond_a
    :goto_9
    if-eqz v5, :cond_16

    :try_start_12
    iget-object v5, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;
    :try_end_12
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_12 .. :try_end_12} :catch_c
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_16
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :cond_b
    :try_start_13
    invoke-virtual {v5}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object v5
    :try_end_13
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_13 .. :try_end_13} :catch_5
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_16
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :try_start_14
    invoke-virtual {v5, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6
    :try_end_14
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_14 .. :try_end_14} :catch_9
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_16
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    if-eqz v1, :cond_d

    if-nez v6, :cond_c

    :try_start_15
    const-string v6, "\\"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6
    :try_end_15
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_15 .. :try_end_15} :catch_b
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_16
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    if-eqz v1, :cond_d

    if-nez v6, :cond_c

    :try_start_16
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_c
    iget-object v6, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    iget-object v7, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->g:Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->g:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v7, v5}, Lcom/jscape/inet/ftps/Ftps;->checksum(Ljava/io/File;Ljava/lang/String;)Z

    move-result v6
    :try_end_16
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_16 .. :try_end_16} :catch_5
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    :cond_d
    if-eqz v6, :cond_e

    goto/16 :goto_c

    :cond_e
    :try_start_17
    new-instance v5, Lcom/jscape/inet/ftp/FtpException;

    sget-object v6, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->n:[Ljava/lang/String;

    aget-object v6, v6, v2

    invoke-direct {v5, v6}, Lcom/jscape/inet/ftp/FtpException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_17
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_17 .. :try_end_17} :catch_8
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_16
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    :catch_8
    move-exception v5

    :try_start_18
    invoke-static {v5}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v5

    throw v5
    :try_end_18
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_18 .. :try_end_18} :catch_5
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_16
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    :catch_9
    move-exception v5

    :try_start_19
    invoke-static {v5}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v5

    throw v5
    :try_end_19
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_19 .. :try_end_19} :catch_a
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_16
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    :catch_a
    move-exception v5

    :try_start_1a
    invoke-static {v5}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v5

    throw v5
    :try_end_1a
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1a .. :try_end_1a} :catch_b
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_16
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    :catch_b
    move-exception v5

    :try_start_1b
    invoke-static {v5}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v5

    throw v5

    :catch_c
    move-exception v5

    invoke-static {v5}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v5

    throw v5
    :try_end_1b
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1b .. :try_end_1b} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_16
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    :goto_a
    :try_start_1c
    iget-object v6, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;
    :try_end_1c
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1c .. :try_end_1c} :catch_e
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_16
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    if-eqz v1, :cond_10

    :try_start_1d
    invoke-virtual {v6}, Lcom/jscape/inet/ftps/Ftps;->interrupted()Z

    move-result v6
    :try_end_1d
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1d .. :try_end_1d} :catch_f
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_16
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    if-ne v6, v4, :cond_f

    goto/16 :goto_4

    :cond_f
    :try_start_1e
    iput-boolean v4, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->k:Z

    invoke-virtual {v5}, Lcom/jscape/inet/ftp/FtpException;->getMessage()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->l:Ljava/lang/String;

    iget-object v6, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->m:Lcom/jscape/inet/ftps/Ftps;

    :cond_10
    invoke-static {v6}, Lcom/jscape/inet/ftps/Ftps;->a(Lcom/jscape/inet/ftps/Ftps;)Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_11
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_15

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/jscape/inet/ftp/FtpListener;
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_16
    .catchall {:try_start_1e .. :try_end_1e} :catchall_0

    if-eqz v1, :cond_13

    :try_start_1f
    instance-of v8, v7, Lcom/jscape/inet/ftp/FtpErrorListener;
    :try_end_1f
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_1f .. :try_end_1f} :catch_d
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_16
    .catchall {:try_start_1f .. :try_end_1f} :catchall_0

    if-eqz v1, :cond_12

    if-eqz v8, :cond_14

    goto :goto_b

    :cond_12
    move v5, v8

    goto/16 :goto_5

    :catch_d
    move-exception v0

    :try_start_20
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :cond_13
    :goto_b
    check-cast v7, Lcom/jscape/inet/ftp/FtpErrorListener;

    new-instance v14, Lcom/jscape/inet/ftp/FtpErrorEvent;

    iget-object v8, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->g:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    iget-object v8, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v8}, Lcom/jscape/inet/ftps/Ftps;->getDir()Ljava/lang/String;

    move-result-object v11

    iget-object v8, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->g:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move-object v8, v14

    move-object v9, p0

    invoke-direct/range {v8 .. v13}, Lcom/jscape/inet/ftp/FtpErrorEvent;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-interface {v7, v14}, Lcom/jscape/inet/ftp/FtpErrorListener;->error(Lcom/jscape/inet/ftp/FtpErrorEvent;)V

    :cond_14
    if-nez v1, :cond_11

    :cond_15
    invoke-virtual {v5}, Lcom/jscape/inet/ftp/FtpException;->printStackTrace()V
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_16
    .catchall {:try_start_20 .. :try_end_20} :catchall_0

    goto :goto_c

    :catch_e
    move-exception v0

    :try_start_21
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_21
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_21 .. :try_end_21} :catch_f
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_16
    .catchall {:try_start_21 .. :try_end_21} :catchall_0

    :catch_f
    move-exception v0

    :try_start_22
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :cond_16
    :goto_c
    if-nez v1, :cond_6

    goto :goto_e

    :catch_10
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0

    :catch_11
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_22} :catch_16
    .catchall {:try_start_22 .. :try_end_22} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_d

    :catch_12
    move-exception v0

    :try_start_23
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_23
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_23 .. :try_end_23} :catch_13
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_23} :catch_16
    .catchall {:try_start_23 .. :try_end_23} :catchall_0

    :catch_13
    move-exception v0

    :try_start_24
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_24
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_24 .. :try_end_24} :catch_14
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_24} :catch_16
    .catchall {:try_start_24 .. :try_end_24} :catchall_0

    :catch_14
    move-exception v0

    :try_start_25
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_25
    .catch Lcom/jscape/inet/ftp/FtpException; {:try_start_25 .. :try_end_25} :catch_15
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_25} :catch_16
    .catchall {:try_start_25 .. :try_end_25} :catchall_0

    :catch_15
    move-exception v0

    :try_start_26
    invoke-static {v0}, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->a(Lcom/jscape/inet/ftp/FtpException;)Lcom/jscape/inet/ftp/FtpException;

    move-result-object v0

    throw v0
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_26} :catch_16
    .catchall {:try_start_26 .. :try_end_26} :catchall_0

    :goto_d
    iget-object v1, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v1}, Lcom/jscape/inet/ftps/Ftps;->disconnect()V

    iput-boolean v2, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->f:Z

    throw v0

    :catch_16
    :cond_17
    :goto_e
    iget-object v0, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->b:Lcom/jscape/inet/ftps/Ftps;

    invoke-virtual {v0}, Lcom/jscape/inet/ftps/Ftps;->disconnect()V

    :cond_18
    iput-boolean v2, p0, Lcom/jscape/inet/ftps/Ftps$FtpsUploadItem;->f:Z

    return-void
.end method
