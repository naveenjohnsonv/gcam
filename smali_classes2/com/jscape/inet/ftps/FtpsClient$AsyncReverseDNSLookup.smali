.class Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;
.super Ljava/lang/Thread;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z

.field final c:Lcom/jscape/inet/ftps/FtpsClient;


# direct methods
.method private constructor <init>(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;->c:Lcom/jscape/inet/ftps/FtpsClient;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p2, p0, Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;Lcom/jscape/inet/ftps/FtpsClient$1;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;-><init>(Lcom/jscape/inet/ftps/FtpsClient;Ljava/lang/String;)V

    return-void
.end method

.method static a(Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;->b:Z

    return p0
.end method

.method static b(Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;->a:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public run()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;->a:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;->b:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/ftps/FtpsClient$AsyncReverseDNSLookup;->b:Z

    :goto_0
    return-void
.end method
