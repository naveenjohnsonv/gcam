.class public Lcom/jscape/inet/util/h;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/jscape/inet/util/i;)Ljava/net/ServerSocket;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/net/ServerSocket;

    invoke-direct {v0}, Ljava/net/ServerSocket;-><init>()V

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/net/ServerSocket;->setReuseAddress(Z)V

    invoke-virtual {p0}, Lcom/jscape/inet/util/i;->a()I

    move-result v3

    move v4, v3

    :goto_0
    :try_start_0
    new-instance v5, Ljava/net/InetSocketAddress;

    invoke-direct {v5, v4}, Ljava/net/InetSocketAddress;-><init>(I)V

    invoke-virtual {v0, v5}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;)V

    invoke-virtual {v0, v2}, Ljava/net/ServerSocket;->setReuseAddress(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v4

    invoke-virtual {p0}, Lcom/jscape/inet/util/i;->a()I

    move-result v5

    if-eqz v1, :cond_1

    if-eq v5, v3, :cond_0

    move v4, v5

    goto :goto_0

    :cond_0
    throw v4

    :cond_1
    :goto_1
    return-object v0
.end method

.method public static a(Ljavax/net/ServerSocketFactory;Lcom/jscape/inet/util/i;)Ljava/net/ServerSocket;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/jscape/inet/util/i;->a()I

    move-result v0

    move v1, v0

    :goto_0
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, v1, v2}, Ljavax/net/ServerSocketFactory;->createServerSocket(II)Ljava/net/ServerSocket;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/net/ServerSocket;->setReuseAddress(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v1

    invoke-virtual {p1}, Lcom/jscape/inet/util/i;->a()I

    move-result v2

    if-eq v2, v0, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    throw v1
.end method
