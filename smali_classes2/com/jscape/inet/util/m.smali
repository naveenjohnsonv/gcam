.class final Lcom/jscape/inet/util/m;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final a:[Ljava/net/Socket;

.field final b:Ljava/lang/String;

.field final c:I

.field final d:[Ljava/lang/Exception;


# direct methods
.method constructor <init>([Ljava/net/Socket;Ljava/lang/String;I[Ljava/lang/Exception;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/util/m;->a:[Ljava/net/Socket;

    iput-object p2, p0, Lcom/jscape/inet/util/m;->b:Ljava/lang/String;

    iput p3, p0, Lcom/jscape/inet/util/m;->c:I

    iput-object p4, p0, Lcom/jscape/inet/util/m;->d:[Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public run()V
    .locals 7

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/util/m;->a:[Ljava/net/Socket;

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-object v3, v1, v2

    :try_start_0
    new-instance v4, Ljava/net/Socket;

    iget-object v5, p0, Lcom/jscape/inet/util/m;->b:Ljava/lang/String;

    iget v6, p0, Lcom/jscape/inet/util/m;->c:I

    invoke-direct {v4, v5, v6}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    aput-object v4, v1, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_1
    iget-object v4, p0, Lcom/jscape/inet/util/m;->d:[Ljava/lang/Exception;

    aput-object v1, v4, v2

    iget-object v1, p0, Lcom/jscape/inet/util/m;->a:[Ljava/net/Socket;

    if-eqz v0, :cond_1

    aget-object v0, v1, v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    if-eqz v0, :cond_0

    :try_start_2
    iget-object v0, p0, Lcom/jscape/inet/util/m;->a:[Ljava/net/Socket;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/util/m;->a:[Ljava/net/Socket;

    :cond_1
    aput-object v3, v1, v2

    :goto_0
    return-void

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/util/m;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method
