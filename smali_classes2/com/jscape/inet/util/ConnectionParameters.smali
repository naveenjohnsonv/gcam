.class public Lcom/jscape/inet/util/ConnectionParameters;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:J = 0x0L

.field private static final b:I = -0x1

.field private static final c:I = 0x438

.field private static final d:Ljava/lang/String;

.field private static final e:I = 0x200000

.field private static final u:[Ljava/lang/String;


# instance fields
.field private f:Ljava/lang/String;

.field private g:I

.field private h:J

.field private i:J

.field private j:I

.field private k:I

.field private l:Ljava/lang/Integer;

.field private m:Ljava/lang/Boolean;

.field private n:Ljava/lang/String;

.field private o:I

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:J

.field private t:J


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "\u000f{\u001du\u0019\u0019as:\u001et\u0001\u000ejGfJ\u000c\u000f{\u001du\u0019\u0019as4\u001fsK\u000f\u000f{\u001du\u0019\u0019av(\u0008u?\u0005%\u0004\u0006p\u0014.L%T\r\u000f{\u001du\u0019\u0019aw\"\u001dbKF\u0015a:\t\'%.8W2\u0000b\u0019\u0014l\u0003-\u000ck\u0003\u00046\u001c`4\u0003i\u0013\u0002lJ4\u0003W\u0017\u0013yN>\u0019b\u0004\u00128X3\u0002t\u0002\\?\u0006p\u0014.L%T\u0010\u000f{\u001fb\u0015\u0004qU>/r\u0010\u0007}Qf\u000f\u000f{\u0019u\u0017\u0007~J8.k\u0017\u0012k\u001e\u000c\u000f{\u001eh\"\u0008uF4\u0018sK\r\u000f{\u0019d\u0006/wg>\u0001f\u000f\\\u0007\u000f{\u001dh\u0004\u0015%\r\u000f{\u001du\u0019\u0019ak4\u001esKF\r\u000f{\u001eb\u0018\u0005ZV=\u000bb\u0004\\\u000fa:\t\'\u0006\u000ejW{\u001bf\u001a\u0014}\r"

    const/16 v4, 0xed

    const/16 v5, 0x11

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x33

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x32

    const/16 v3, 0x14

    const-string v5, "v\u0002w\u0011av\u00049V}\u0011aL\u00087G{\u000b{%\u001d\u0018Cp^lw\u000f4Gw\nfw\u000fzV}\u0013jw\u0014.\u0002b\u001fcm\u0004t"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x4a

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/util/ConnectionParameters;->u:[Ljava/lang/String;

    const/4 v1, 0x7

    aget-object v0, v0, v1

    sput-object v0, Lcom/jscape/inet/util/ConnectionParameters;->d:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x2b

    goto :goto_4

    :cond_4
    const/16 v1, 0x52

    goto :goto_4

    :cond_5
    const/16 v1, 0x45

    goto :goto_4

    :cond_6
    const/16 v1, 0x34

    goto :goto_4

    :cond_7
    const/16 v1, 0x5e

    goto :goto_4

    :cond_8
    const/16 v1, 0x68

    goto :goto_4

    :cond_9
    const/16 v1, 0x10

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/inet/util/ConnectionParameters;)V
    .locals 23

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iget-object v2, v0, Lcom/jscape/inet/util/ConnectionParameters;->f:Ljava/lang/String;

    iget v3, v0, Lcom/jscape/inet/util/ConnectionParameters;->g:I

    iget-wide v4, v0, Lcom/jscape/inet/util/ConnectionParameters;->h:J

    iget-wide v6, v0, Lcom/jscape/inet/util/ConnectionParameters;->i:J

    iget v8, v0, Lcom/jscape/inet/util/ConnectionParameters;->j:I

    iget v9, v0, Lcom/jscape/inet/util/ConnectionParameters;->k:I

    iget-object v10, v0, Lcom/jscape/inet/util/ConnectionParameters;->l:Ljava/lang/Integer;

    iget-object v11, v0, Lcom/jscape/inet/util/ConnectionParameters;->m:Ljava/lang/Boolean;

    iget-object v12, v0, Lcom/jscape/inet/util/ConnectionParameters;->n:Ljava/lang/String;

    iget v13, v0, Lcom/jscape/inet/util/ConnectionParameters;->o:I

    iget-object v14, v0, Lcom/jscape/inet/util/ConnectionParameters;->p:Ljava/lang/String;

    iget-object v15, v0, Lcom/jscape/inet/util/ConnectionParameters;->q:Ljava/lang/String;

    move-object/from16 v21, v1

    iget-object v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->r:Ljava/lang/String;

    move-object/from16 v16, v1

    move-object/from16 v22, v2

    iget-wide v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->s:J

    move-wide/from16 v17, v1

    iget-wide v0, v0, Lcom/jscape/inet/util/ConnectionParameters;->t:J

    move-wide/from16 v19, v0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-direct/range {v1 .. v20}, Lcom/jscape/inet/util/ConnectionParameters;-><init>(Ljava/lang/String;IJJIILjava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/jscape/inet/util/ConnectionParameters;-><init>(Ljava/lang/String;IJ)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;III)V
    .locals 7

    const-wide/16 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/util/ConnectionParameters;-><init>(Ljava/lang/String;IJII)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IJ)V
    .locals 7

    const/4 v5, -0x1

    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-wide v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/util/ConnectionParameters;-><init>(Ljava/lang/String;IJII)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IJII)V
    .locals 16

    sget-object v0, Lcom/jscape/inet/util/ConnectionParameters;->u:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object v15, v0, v1

    const/4 v11, 0x0

    const/16 v12, 0x438

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p2

    move-wide/from16 v5, p3

    move-wide/from16 v7, p3

    move/from16 v9, p5

    move/from16 v10, p6

    invoke-direct/range {v2 .. v15}, Lcom/jscape/inet/util/ConnectionParameters;-><init>(Ljava/lang/String;IJJIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IJJIILjava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    const-wide/32 v16, 0x66666

    const-wide/32 v18, 0x200000

    invoke-direct/range {v0 .. v19}, Lcom/jscape/inet/util/ConnectionParameters;-><init>(Ljava/lang/String;IJJIILjava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IJJIILjava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 3

    move-object v0, p0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object v1, p1

    iput-object v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->f:Ljava/lang/String;

    move v1, p2

    iput v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->g:I

    move-wide v1, p3

    iput-wide v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->h:J

    move-wide v1, p5

    iput-wide v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->i:J

    move v1, p7

    iput v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->j:I

    move v1, p8

    iput v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->k:I

    move-object v1, p9

    iput-object v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->l:Ljava/lang/Integer;

    move-object v1, p10

    iput-object v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->m:Ljava/lang/Boolean;

    move-object v1, p11

    iput-object v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->n:Ljava/lang/String;

    move v1, p12

    iput v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->o:I

    move-object/from16 v1, p13

    iput-object v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->p:Ljava/lang/String;

    move-object/from16 v1, p14

    iput-object v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->q:Ljava/lang/String;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->r:Ljava/lang/String;

    move-wide/from16 v1, p16

    iput-wide v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->s:J

    move-wide/from16 v1, p18

    iput-wide v1, v0, Lcom/jscape/inet/util/ConnectionParameters;->t:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IJJIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 17

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    const/4 v10, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    move-wide/from16 v4, p3

    move-wide/from16 v6, p5

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v12, p9

    move/from16 v13, p10

    move-object/from16 v14, p11

    move-object/from16 v15, p12

    move-object/from16 v16, p13

    invoke-direct/range {v1 .. v16}, Lcom/jscape/inet/util/ConnectionParameters;-><init>(Ljava/lang/String;IJJIILjava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IJLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 14

    const/4 v7, -0x1

    const/4 v8, -0x1

    move-object v0, p0

    move-object v1, p1

    move/from16 v2, p2

    move-wide/from16 v3, p3

    move-wide/from16 v5, p3

    move-object/from16 v9, p5

    move/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    invoke-direct/range {v0 .. v13}, Lcom/jscape/inet/util/ConnectionParameters;-><init>(Ljava/lang/String;IJJIILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private createPlainSocket()Ljava/net/Socket;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->f:Ljava/lang/String;

    iget v1, p0, Lcom/jscape/inet/util/ConnectionParameters;->g:I

    iget-wide v2, p0, Lcom/jscape/inet/util/ConnectionParameters;->h:J

    iget v4, p0, Lcom/jscape/inet/util/ConnectionParameters;->j:I

    iget v5, p0, Lcom/jscape/inet/util/ConnectionParameters;->k:I

    invoke-static/range {v0 .. v5}, Lcom/jscape/inet/util/k;->a(Ljava/lang/String;IJII)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method private createProxySocket()Ljava/net/Socket;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->f:Ljava/lang/String;

    iget v1, p0, Lcom/jscape/inet/util/ConnectionParameters;->g:I

    iget-wide v2, p0, Lcom/jscape/inet/util/ConnectionParameters;->h:J

    iget-object v4, p0, Lcom/jscape/inet/util/ConnectionParameters;->n:Ljava/lang/String;

    iget v5, p0, Lcom/jscape/inet/util/ConnectionParameters;->o:I

    iget-object v6, p0, Lcom/jscape/inet/util/ConnectionParameters;->p:Ljava/lang/String;

    iget-object v7, p0, Lcom/jscape/inet/util/ConnectionParameters;->q:Ljava/lang/String;

    iget-object v8, p0, Lcom/jscape/inet/util/ConnectionParameters;->r:Ljava/lang/String;

    invoke-static/range {v0 .. v8}, Lcom/jscape/inet/util/k;->a(Ljava/lang/String;IJLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method private initSocket(Ljava/net/Socket;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-wide v1, p0, Lcom/jscape/inet/util/ConnectionParameters;->i:J

    long-to-int v1, v1

    invoke-virtual {p1, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->l:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->l:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/net/Socket;->setTrafficClass(I)V
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Ljava/net/Socket;->setTcpNoDelay(Z)V
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/util/ConnectionParameters;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/util/ConnectionParameters;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/util/ConnectionParameters;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method private useProxy()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public createSocket()Ljava/net/Socket;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/util/ConnectionParameters;->useProxy()Z

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/inet/util/ConnectionParameters;->createProxySocket()Ljava/net/Socket;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/util/ConnectionParameters;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/util/ConnectionParameters;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/jscape/inet/util/ConnectionParameters;->createPlainSocket()Ljava/net/Socket;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, Lcom/jscape/inet/util/ConnectionParameters;->initSocket(Ljava/net/Socket;)V

    return-object v0
.end method

.method public getConnectionTimeout()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->h:J

    return-wide v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getMaxIncomingWindowSize()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->t:J

    return-wide v0
.end method

.method public getMinIncomingWindowSize()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->s:J

    return-wide v0
.end method

.method public getPort()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->g:I

    return v0
.end method

.method public getProxyHost()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->n:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyPassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->q:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyPort()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->o:I

    return v0
.end method

.method public getProxyType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->r:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyUserId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->p:Ljava/lang/String;

    return-object v0
.end method

.method public getReceiveBuffer()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->k:I

    return v0
.end method

.method public getSendBuffer()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->j:I

    return v0
.end method

.method public getSoTimeout()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->i:J

    return-wide v0
.end method

.method public getTcpNoDelay()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->m:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getTrafficClass()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/ConnectionParameters;->l:Ljava/lang/Integer;

    return-object v0
.end method

.method public setConnectionTimeout(J)V
    .locals 3

    sget-object v0, Lcom/jscape/inet/util/ConnectionParameters;->u:[Ljava/lang/String;

    const/16 v1, 0x11

    aget-object v0, v0, v1

    const-wide/16 v1, 0x0

    invoke-static {p1, p2, v1, v2, v0}, Lcom/jscape/util/w;->b(JJLjava/lang/String;)V

    iput-wide p1, p0, Lcom/jscape/inet/util/ConnectionParameters;->h:J

    return-void
.end method

.method public setHost(Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/inet/util/ConnectionParameters;->f:Ljava/lang/String;

    return-void
.end method

.method public setMaxIncomingWindowSize(J)V
    .locals 0

    iput-wide p1, p0, Lcom/jscape/inet/util/ConnectionParameters;->t:J

    return-void
.end method

.method public setMinIncomingWindowSize(J)V
    .locals 0

    iput-wide p1, p0, Lcom/jscape/inet/util/ConnectionParameters;->s:J

    return-void
.end method

.method public setPort(I)V
    .locals 7

    int-to-long v0, p1

    sget-object v2, Lcom/jscape/inet/util/ConnectionParameters;->u:[Ljava/lang/String;

    const/16 v3, 0xf

    aget-object v6, v2, v3

    const-wide/16 v2, 0x0

    const-wide/32 v4, 0xffff

    invoke-static/range {v0 .. v6}, Lcom/jscape/util/w;->a(JJJLjava/lang/String;)V

    iput p1, p0, Lcom/jscape/inet/util/ConnectionParameters;->g:I

    return-void
.end method

.method public setProxyHost(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/util/ConnectionParameters;->n:Ljava/lang/String;

    return-void
.end method

.method public setProxyPassword(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/util/ConnectionParameters;->q:Ljava/lang/String;

    return-void
.end method

.method public setProxyPort(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/util/ConnectionParameters;->o:I

    return-void
.end method

.method public setProxyType(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/util/ConnectionParameters;->r:Ljava/lang/String;

    return-void
.end method

.method public setProxyUserId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/util/ConnectionParameters;->p:Ljava/lang/String;

    return-void
.end method

.method public setReceiveBuffer(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/util/ConnectionParameters;->k:I

    return-void
.end method

.method public setSendBuffer(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/util/ConnectionParameters;->j:I

    return-void
.end method

.method public setSoTimeout(J)V
    .locals 3

    sget-object v0, Lcom/jscape/inet/util/ConnectionParameters;->u:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    const-wide/16 v1, 0x0

    invoke-static {p1, p2, v1, v2, v0}, Lcom/jscape/util/w;->b(JJLjava/lang/String;)V

    iput-wide p1, p0, Lcom/jscape/inet/util/ConnectionParameters;->i:J

    return-void
.end method

.method public setTcpNoDelay(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/util/ConnectionParameters;->m:Ljava/lang/Boolean;

    return-void
.end method

.method public setTrafficClass(Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/util/ConnectionParameters;->l:Ljava/lang/Integer;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/util/ConnectionParameters;->u:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/util/ConnectionParameters;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v3, 0xc

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/jscape/inet/util/ConnectionParameters;->g:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v3, 0x10

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lcom/jscape/inet/util/ConnectionParameters;->h:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v3, 0xa

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lcom/jscape/inet/util/ConnectionParameters;->i:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v3, 0xe

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/jscape/inet/util/ConnectionParameters;->j:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v3, 0x8

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/jscape/inet/util/ConnectionParameters;->k:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v3, 0x9

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/util/ConnectionParameters;->l:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v3, 0xb

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/util/ConnectionParameters;->m:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v3, 0xd

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/util/ConnectionParameters;->n:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v4, v1, v3

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/jscape/inet/util/ConnectionParameters;->o:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v4, 0x2

    aget-object v4, v1, v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/util/ConnectionParameters;->p:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    aget-object v4, v1, v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/util/ConnectionParameters;->q:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v4, 0x4

    aget-object v1, v1, v4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/util/ConnectionParameters;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v1

    if-nez v1, :cond_0

    new-array v1, v3, [Ljava/lang/String;

    invoke-static {v1}, Lcom/jscape/inet/util/i;->b([Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method
