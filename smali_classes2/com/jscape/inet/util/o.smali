.class Lcom/jscape/inet/util/o;
.super Ljava/lang/Thread;


# static fields
.field private static final s:[Ljava/lang/String;


# instance fields
.field private volatile a:Ljava/net/Socket;

.field private b:Ljava/net/InetAddress;

.field private c:I

.field private d:Ljava/io/IOException;

.field private e:J

.field private f:Ljava/net/InetAddress;

.field private g:I

.field private h:Z

.field private i:I

.field private j:I

.field private k:Z

.field private l:Ljava/lang/String;

.field private m:I

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Lcom/jscape/inet/util/n;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "9Q!|4g\u0004\"J6g"

    const/16 v5, 0xb

    const/4 v6, 0x6

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x50

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    const/16 v15, 0x37

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x3e

    const-string v4, "G\u0014p2o_l.\nt<{O(z\u0003v6-\u0016^o\u0016o7#Fza\u0002\u007fswOxk\t&2qS2.2R\u0007S\u001a(]5E\u0018P\u0003\u0006]5E\u0018P\u0003"

    move v8, v11

    move v6, v15

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x34

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/util/o;->s:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v2, v14, 0x7

    const/4 v3, 0x2

    if-eqz v2, :cond_8

    if-eq v2, v10, :cond_7

    if-eq v2, v3, :cond_6

    const/4 v3, 0x3

    if-eq v2, v3, :cond_5

    if-eq v2, v0, :cond_9

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v15, 0x3c

    goto :goto_4

    :cond_4
    const/4 v15, 0x2

    goto :goto_4

    :cond_5
    const/16 v15, 0x67

    goto :goto_4

    :cond_6
    const/16 v15, 0x32

    goto :goto_4

    :cond_7
    const/16 v15, 0x4e

    goto :goto_4

    :cond_8
    const/16 v15, 0x3a

    :cond_9
    :goto_4
    xor-int v2, v9, v15

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method constructor <init>(Ljava/lang/String;IJ)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/util/o;->a:Ljava/net/Socket;

    iput-object v0, p0, Lcom/jscape/inet/util/o;->b:Ljava/net/InetAddress;

    const/4 v1, 0x0

    iput v1, p0, Lcom/jscape/inet/util/o;->c:I

    iput-object v0, p0, Lcom/jscape/inet/util/o;->d:Ljava/io/IOException;

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/jscape/inet/util/o;->e:J

    iput-object v0, p0, Lcom/jscape/inet/util/o;->f:Ljava/net/InetAddress;

    iput v1, p0, Lcom/jscape/inet/util/o;->g:I

    iput-boolean v1, p0, Lcom/jscape/inet/util/o;->h:Z

    const/4 v2, -0x1

    iput v2, p0, Lcom/jscape/inet/util/o;->i:I

    iput v2, p0, Lcom/jscape/inet/util/o;->j:I

    iput-boolean v1, p0, Lcom/jscape/inet/util/o;->k:Z

    const/16 v1, 0x438

    iput v1, p0, Lcom/jscape/inet/util/o;->m:I

    sget-object v1, Lcom/jscape/inet/util/o;->s:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/jscape/inet/util/o;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/util/o;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/util/o;->r:Lcom/jscape/inet/util/n;

    iput-object p1, p0, Lcom/jscape/inet/util/o;->q:Ljava/lang/String;

    iput p2, p0, Lcom/jscape/inet/util/o;->c:I

    iput-wide p3, p0, Lcom/jscape/inet/util/o;->e:J

    return-void
.end method

.method constructor <init>(Ljava/lang/String;IJLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/jscape/inet/util/o;-><init>(Ljava/lang/String;IJ)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/jscape/inet/util/o;->k:Z

    iput-object p5, p0, Lcom/jscape/inet/util/o;->l:Ljava/lang/String;

    iput-object p7, p0, Lcom/jscape/inet/util/o;->n:Ljava/lang/String;

    iput p6, p0, Lcom/jscape/inet/util/o;->m:I

    iput-object p8, p0, Lcom/jscape/inet/util/o;->o:Ljava/lang/String;

    iput-object p9, p0, Lcom/jscape/inet/util/o;->p:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/net/InetAddress;IJ)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/util/o;->a:Ljava/net/Socket;

    iput-object v0, p0, Lcom/jscape/inet/util/o;->b:Ljava/net/InetAddress;

    const/4 v1, 0x0

    iput v1, p0, Lcom/jscape/inet/util/o;->c:I

    iput-object v0, p0, Lcom/jscape/inet/util/o;->d:Ljava/io/IOException;

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/jscape/inet/util/o;->e:J

    iput-object v0, p0, Lcom/jscape/inet/util/o;->f:Ljava/net/InetAddress;

    iput v1, p0, Lcom/jscape/inet/util/o;->g:I

    iput-boolean v1, p0, Lcom/jscape/inet/util/o;->h:Z

    const/4 v2, -0x1

    iput v2, p0, Lcom/jscape/inet/util/o;->i:I

    iput v2, p0, Lcom/jscape/inet/util/o;->j:I

    iput-boolean v1, p0, Lcom/jscape/inet/util/o;->k:Z

    const/16 v2, 0x438

    iput v2, p0, Lcom/jscape/inet/util/o;->m:I

    sget-object v2, Lcom/jscape/inet/util/o;->s:[Ljava/lang/String;

    aget-object v1, v2, v1

    iput-object v1, p0, Lcom/jscape/inet/util/o;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/util/o;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/util/o;->r:Lcom/jscape/inet/util/n;

    iput-object p1, p0, Lcom/jscape/inet/util/o;->b:Ljava/net/InetAddress;

    iput p2, p0, Lcom/jscape/inet/util/o;->c:I

    iput-wide p3, p0, Lcom/jscape/inet/util/o;->e:J

    return-void
.end method

.method constructor <init>(Ljava/net/InetAddress;IJII)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/util/o;->a:Ljava/net/Socket;

    iput-object v0, p0, Lcom/jscape/inet/util/o;->b:Ljava/net/InetAddress;

    const/4 v1, 0x0

    iput v1, p0, Lcom/jscape/inet/util/o;->c:I

    iput-object v0, p0, Lcom/jscape/inet/util/o;->d:Ljava/io/IOException;

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/jscape/inet/util/o;->e:J

    iput-object v0, p0, Lcom/jscape/inet/util/o;->f:Ljava/net/InetAddress;

    iput v1, p0, Lcom/jscape/inet/util/o;->g:I

    iput-boolean v1, p0, Lcom/jscape/inet/util/o;->h:Z

    const/4 v2, -0x1

    iput v2, p0, Lcom/jscape/inet/util/o;->i:I

    iput v2, p0, Lcom/jscape/inet/util/o;->j:I

    iput-boolean v1, p0, Lcom/jscape/inet/util/o;->k:Z

    const/16 v1, 0x438

    iput v1, p0, Lcom/jscape/inet/util/o;->m:I

    sget-object v1, Lcom/jscape/inet/util/o;->s:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/jscape/inet/util/o;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/util/o;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/util/o;->r:Lcom/jscape/inet/util/n;

    iput-object p1, p0, Lcom/jscape/inet/util/o;->b:Ljava/net/InetAddress;

    iput p2, p0, Lcom/jscape/inet/util/o;->c:I

    iput-wide p3, p0, Lcom/jscape/inet/util/o;->e:J

    iput p5, p0, Lcom/jscape/inet/util/o;->i:I

    iput p6, p0, Lcom/jscape/inet/util/o;->j:I

    return-void
.end method

.method constructor <init>(Ljava/net/InetAddress;IJLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/jscape/inet/util/o;-><init>(Ljava/net/InetAddress;IJ)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/jscape/inet/util/o;->k:Z

    iput-object p5, p0, Lcom/jscape/inet/util/o;->l:Ljava/lang/String;

    iput-object p7, p0, Lcom/jscape/inet/util/o;->n:Ljava/lang/String;

    iput p6, p0, Lcom/jscape/inet/util/o;->m:I

    iput-object p8, p0, Lcom/jscape/inet/util/o;->o:Ljava/lang/String;

    iput-object p9, p0, Lcom/jscape/inet/util/o;->p:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/net/InetAddress;ILjava/net/InetAddress;IJ)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/util/o;->a:Ljava/net/Socket;

    iput-object v0, p0, Lcom/jscape/inet/util/o;->b:Ljava/net/InetAddress;

    const/4 v1, 0x0

    iput v1, p0, Lcom/jscape/inet/util/o;->c:I

    iput-object v0, p0, Lcom/jscape/inet/util/o;->d:Ljava/io/IOException;

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/jscape/inet/util/o;->e:J

    iput-object v0, p0, Lcom/jscape/inet/util/o;->f:Ljava/net/InetAddress;

    iput v1, p0, Lcom/jscape/inet/util/o;->g:I

    iput-boolean v1, p0, Lcom/jscape/inet/util/o;->h:Z

    const/4 v2, -0x1

    iput v2, p0, Lcom/jscape/inet/util/o;->i:I

    iput v2, p0, Lcom/jscape/inet/util/o;->j:I

    iput-boolean v1, p0, Lcom/jscape/inet/util/o;->k:Z

    const/16 v1, 0x438

    iput v1, p0, Lcom/jscape/inet/util/o;->m:I

    sget-object v1, Lcom/jscape/inet/util/o;->s:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/jscape/inet/util/o;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/util/o;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/util/o;->r:Lcom/jscape/inet/util/n;

    iput-object p1, p0, Lcom/jscape/inet/util/o;->b:Ljava/net/InetAddress;

    iput p2, p0, Lcom/jscape/inet/util/o;->c:I

    iput-object p3, p0, Lcom/jscape/inet/util/o;->f:Ljava/net/InetAddress;

    iput p4, p0, Lcom/jscape/inet/util/o;->g:I

    iput-wide p5, p0, Lcom/jscape/inet/util/o;->e:J

    return-void
.end method

.method constructor <init>(Ljava/net/InetAddress;ILjava/net/InetAddress;IJII)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/util/o;->a:Ljava/net/Socket;

    iput-object v0, p0, Lcom/jscape/inet/util/o;->b:Ljava/net/InetAddress;

    const/4 v1, 0x0

    iput v1, p0, Lcom/jscape/inet/util/o;->c:I

    iput-object v0, p0, Lcom/jscape/inet/util/o;->d:Ljava/io/IOException;

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/jscape/inet/util/o;->e:J

    iput-object v0, p0, Lcom/jscape/inet/util/o;->f:Ljava/net/InetAddress;

    iput v1, p0, Lcom/jscape/inet/util/o;->g:I

    iput-boolean v1, p0, Lcom/jscape/inet/util/o;->h:Z

    const/4 v2, -0x1

    iput v2, p0, Lcom/jscape/inet/util/o;->i:I

    iput v2, p0, Lcom/jscape/inet/util/o;->j:I

    iput-boolean v1, p0, Lcom/jscape/inet/util/o;->k:Z

    const/16 v1, 0x438

    iput v1, p0, Lcom/jscape/inet/util/o;->m:I

    sget-object v1, Lcom/jscape/inet/util/o;->s:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/jscape/inet/util/o;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/util/o;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/util/o;->r:Lcom/jscape/inet/util/n;

    iput-object p1, p0, Lcom/jscape/inet/util/o;->b:Ljava/net/InetAddress;

    iput p2, p0, Lcom/jscape/inet/util/o;->c:I

    iput-object p3, p0, Lcom/jscape/inet/util/o;->f:Ljava/net/InetAddress;

    iput p4, p0, Lcom/jscape/inet/util/o;->g:I

    iput-wide p5, p0, Lcom/jscape/inet/util/o;->e:J

    iput p7, p0, Lcom/jscape/inet/util/o;->i:I

    iput p8, p0, Lcom/jscape/inet/util/o;->j:I

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/inet/util/n;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/util/o;->r:Lcom/jscape/inet/util/n;

    return-void
.end method

.method static a(Lcom/jscape/inet/util/o;Lcom/jscape/inet/util/n;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/util/o;->a(Lcom/jscape/inet/util/n;)V

    return-void
.end method

.method private a(Ljava/net/Socket;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget v1, p0, Lcom/jscape/inet/util/o;->i:I
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    if-lez v1, :cond_0

    :try_start_1
    iget v0, p0, Lcom/jscape/inet/util/o;->i:I

    invoke-virtual {p1, v0}, Ljava/net/Socket;->setSendBufferSize(I)V
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    iget v1, p0, Lcom/jscape/inet/util/o;->j:I

    :cond_1
    if-lez v1, :cond_2

    :try_start_2
    iget v0, p0, Lcom/jscape/inet/util/o;->j:I

    invoke-virtual {p1, v0}, Ljava/net/Socket;->setReceiveBufferSize(I)V
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/util/o;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    return-void

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/util/o;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/util/o;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method


# virtual methods
.method public a()Ljava/net/Socket;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/util/o;->p:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    const/4 v2, 0x3

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    :try_start_1
    sget-object v1, Lcom/jscape/inet/util/o;->s:[Ljava/lang/String;

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/jscape/inet/util/o;->p:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/util/o;->p:Ljava/lang/String;

    :cond_1
    :try_start_2
    sget-object v3, Lcom/jscape/inet/util/o;->s:[Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v0, :cond_3

    if-eqz v1, :cond_2

    new-instance v1, Lcom/jscape/inet/util/f;

    iget-object v2, p0, Lcom/jscape/inet/util/o;->l:Ljava/lang/String;

    iget v3, p0, Lcom/jscape/inet/util/o;->m:I

    invoke-direct {v1, v2, v3}, Lcom/jscape/inet/util/f;-><init>(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/jscape/inet/util/o;->n:Ljava/lang/String;

    iget-object v3, p0, Lcom/jscape/inet/util/o;->o:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/jscape/inet/util/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/jscape/inet/util/o;->p:Ljava/lang/String;

    sget-object v3, Lcom/jscape/inet/util/o;->s:[Ljava/lang/String;

    aget-object v2, v3, v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    :cond_3
    if-eqz v1, :cond_6

    new-instance v1, Lcom/jscape/inet/util/g;

    iget-object v2, p0, Lcom/jscape/inet/util/o;->l:Ljava/lang/String;

    iget v3, p0, Lcom/jscape/inet/util/o;->m:I

    invoke-direct {v1, v2, v3}, Lcom/jscape/inet/util/g;-><init>(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/jscape/inet/util/o;->n:Ljava/lang/String;

    iget-object v3, p0, Lcom/jscape/inet/util/o;->o:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/jscape/inet/util/g;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    :try_start_3
    iget-object v2, p0, Lcom/jscape/inet/util/o;->b:Ljava/net/InetAddress;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    if-eqz v0, :cond_5

    if-nez v2, :cond_4

    iget-object v0, p0, Lcom/jscape/inet/util/o;->q:Ljava/lang/String;

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/jscape/inet/util/o;->b:Ljava/net/InetAddress;

    :cond_5
    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v6, v0

    const/4 v5, 0x0

    :try_start_4
    iget v7, p0, Lcom/jscape/inet/util/o;->c:I

    iget-wide v8, p0, Lcom/jscape/inet/util/o;->e:J

    move-object v4, v1

    invoke-interface/range {v4 .. v9}, Lcom/jscape/inet/util/e;->a(Lcom/jscape/inet/util/j;Ljava/lang/String;IJ)V

    invoke-interface {v1}, Lcom/jscape/inet/util/e;->c()Ljava/net/Socket;

    move-result-object v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    :catch_0
    const/4 v0, 0x0

    :goto_2
    return-object v0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/util/o;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_6
    new-instance v0, Ljava/lang/Exception;

    sget-object v1, Lcom/jscape/inet/util/o;->s:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/util/o;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_3
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/util/o;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/util/o;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/util/o;->h:Z

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/o;->a:Ljava/net/Socket;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/util/o;->h:Z

    return v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/o;->d:Ljava/io/IOException;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public e()Ljava/net/Socket;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/o;->a:Ljava/net/Socket;

    return-object v0
.end method

.method public f()Ljava/io/IOException;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/o;->d:Ljava/io/IOException;

    return-object v0
.end method

.method public run()V
    .locals 9

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/jscape/inet/util/o;->h:Z

    const/4 v2, 0x0

    :catch_0
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/jscape/inet/util/o;->h:Z

    if-eqz v3, :cond_5

    if-eqz v0, :cond_6

    const/4 v3, 0x0

    if-eqz v0, :cond_4

    :try_start_0
    iget-object v4, p0, Lcom/jscape/inet/util/o;->f:Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/ConnectException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6

    if-nez v4, :cond_3

    if-eqz v0, :cond_1

    :try_start_1
    iget-boolean v4, p0, Lcom/jscape/inet/util/o;->k:Z
    :try_end_1
    .catch Ljava/net/ConnectException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6

    if-nez v4, :cond_1

    :try_start_2
    new-instance v4, Ljava/net/Socket;

    iget-object v5, p0, Lcom/jscape/inet/util/o;->b:Ljava/net/InetAddress;

    iget v6, p0, Lcom/jscape/inet/util/o;->c:I

    invoke-direct {v4, v5, v6}, Ljava/net/Socket;-><init>(Ljava/net/InetAddress;I)V

    move-object v2, v4

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/jscape/inet/util/o;->a()Ljava/net/Socket;

    move-result-object v2

    :cond_2
    if-eqz v2, :cond_0

    invoke-direct {p0, v2}, Lcom/jscape/inet/util/o;->a(Ljava/net/Socket;)V

    iput-boolean v3, p0, Lcom/jscape/inet/util/o;->h:Z

    if-nez v0, :cond_5

    :cond_3
    new-instance v4, Ljava/net/Socket;

    iget-object v5, p0, Lcom/jscape/inet/util/o;->b:Ljava/net/InetAddress;

    iget v6, p0, Lcom/jscape/inet/util/o;->c:I

    iget-object v7, p0, Lcom/jscape/inet/util/o;->f:Ljava/net/InetAddress;

    iget v8, p0, Lcom/jscape/inet/util/o;->g:I

    invoke-direct {v4, v5, v6, v7, v8}, Ljava/net/Socket;-><init>(Ljava/net/InetAddress;ILjava/net/InetAddress;I)V
    :try_end_2
    .catch Ljava/net/ConnectException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    :try_start_3
    invoke-direct {p0, v4}, Lcom/jscape/inet/util/o;->a(Ljava/net/Socket;)V
    :try_end_3
    .catch Ljava/net/ConnectException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-object v2, v4

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v2, v4

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v2, v4

    goto :goto_3

    :catch_3
    move-object v2, v4

    goto :goto_0

    :catch_4
    move-exception v4

    :try_start_4
    invoke-static {v4}, Lcom/jscape/inet/util/o;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v4

    throw v4
    :try_end_4
    .catch Ljava/net/ConnectException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6

    :catch_5
    move-exception v4

    :try_start_5
    invoke-static {v4}, Lcom/jscape/inet/util/o;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v4

    throw v4

    :cond_4
    :goto_1
    iput-boolean v3, p0, Lcom/jscape/inet/util/o;->h:Z
    :try_end_5
    .catch Ljava/net/ConnectException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6

    if-nez v0, :cond_5

    goto :goto_0

    :catch_6
    move-exception v0

    :goto_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v4, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/jscape/inet/util/o;->d:Ljava/io/IOException;

    goto :goto_4

    :catch_7
    move-exception v0

    :goto_3
    iput-object v0, p0, Lcom/jscape/inet/util/o;->d:Ljava/io/IOException;

    :goto_4
    iput-boolean v3, p0, Lcom/jscape/inet/util/o;->h:Z

    :cond_5
    iput-object v2, p0, Lcom/jscape/inet/util/o;->a:Ljava/net/Socket;

    :cond_6
    iget-object v0, p0, Lcom/jscape/inet/util/o;->r:Lcom/jscape/inet/util/n;

    invoke-virtual {v0, v1}, Lcom/jscape/inet/util/n;->a(Z)V

    return-void
.end method
