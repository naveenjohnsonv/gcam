.class Lcom/jscape/inet/util/l;
.super Ljava/lang/Object;


# static fields
.field private static final a:[B

.field private static b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "S?\\W\u001cWg<\u001cVZ\u001e\\t<\u0019o ZR\u0010M u<\u0019W\u001aM y<MX\u0017Uio\'\\]A]\rz}0\u007fGT\u0006sr9tNS\u001fhk&mUJ\u0018a`/Xb\u007f+\\_\u0012Qiv$UT\u001bVpm=JM\u0000Owd6C\tD\u000b3(z\u000f\u000eM\u0000+3r"

    const/16 v6, 0xf

    move v8, v4

    const/4 v7, -0x1

    const/16 v9, 0x6b

    :goto_0
    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v5, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    const/16 v13, 0x6b

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v14, v11

    move v15, v4

    :goto_2
    const/4 v2, 0x2

    if-gt v14, v15, :cond_3

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v13}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v11

    if-eqz v12, :cond_1

    add-int/lit8 v2, v8, 0x1

    aput-object v11, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v9, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v2

    goto :goto_0

    :cond_0
    const/16 v5, 0xd

    const/16 v6, 0x9

    const-string v7, "\u000bE7?y/\u0017E\u000c\u0003@\u0013e"

    move v8, v2

    move v9, v5

    move-object v5, v7

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v8, 0x1

    aput-object v11, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v9, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v6, v2

    move v8, v12

    :goto_3
    const/16 v13, 0x8

    add-int/2addr v7, v10

    add-int v2, v7, v6

    invoke-virtual {v5, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v4

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/util/l;->c:[Ljava/lang/String;

    aget-object v0, v1, v2

    sget-object v1, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/util/l;->a:[B

    const-string v1, "0"

    const-string v2, "1"

    const-string v3, "2"

    const-string v4, "3"

    const-string v5, "4"

    const-string v6, "5"

    const-string v7, "6"

    const-string v8, "7"

    const-string v9, "8"

    const-string v10, "9"

    const-string v11, "a"

    const-string v12, "b"

    const-string v13, "c"

    const-string v14, "d"

    const-string v15, "e"

    const-string v16, "f"

    filled-new-array/range {v1 .. v16}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/util/l;->b:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v17, v11, v15

    rem-int/lit8 v3, v15, 0x7

    const/16 v18, 0x52

    if-eqz v3, :cond_6

    if-eq v3, v10, :cond_5

    if-eq v3, v2, :cond_7

    const/4 v2, 0x3

    if-eq v3, v2, :cond_7

    const/4 v2, 0x4

    if-eq v3, v2, :cond_4

    if-eq v3, v0, :cond_7

    const/16 v18, 0x6b

    goto :goto_4

    :cond_4
    const/16 v18, 0x1e

    goto :goto_4

    :cond_5
    const/16 v18, 0x24

    goto :goto_4

    :cond_6
    const/16 v18, 0x77

    :cond_7
    :goto_4
    xor-int v2, v13, v18

    xor-int v2, v17, v2

    int-to-char v2, v2

    aput-char v2, v11, v15

    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(B)B
    .locals 5

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const/16 v2, 0x3d

    if-ne p0, v2, :cond_0

    return v1

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v2, p0

    :cond_2
    :goto_0
    sget-object v3, Lcom/jscape/inet/util/l;->a:[B

    array-length v4, v3

    if-ge v2, v4, :cond_5

    if-eqz v0, :cond_6

    if-eqz v0, :cond_4

    aget-byte v3, v3, v2

    if-ne p0, v3, :cond_3

    int-to-byte p0, v2

    goto :goto_1

    :cond_3
    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_2

    goto :goto_2

    :cond_4
    :goto_1
    return p0

    :cond_5
    :goto_2
    move p0, v1

    :cond_6
    return p0
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method static a(Lcom/jscape/inet/util/d;[B)Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-interface {p0}, Lcom/jscape/inet/util/d;->a()V

    array-length v1, p1

    const/4 v2, 0x0

    invoke-interface {p0, p1, v2, v1}, Lcom/jscape/inet/util/d;->a([BII)V

    invoke-interface {p0}, Lcom/jscape/inet/util/d;->c()[B

    move-result-object p0

    new-instance p1, Ljava/lang/StringBuffer;

    invoke-direct {p1}, Ljava/lang/StringBuffer;-><init>()V

    :cond_0
    array-length v1, p0

    if-ge v2, v1, :cond_2

    aget-byte v1, p0, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    and-int/lit16 v1, v1, 0xff

    :try_start_1
    sget-object v3, Lcom/jscape/inet/util/l;->b:[Ljava/lang/String;

    ushr-int/lit8 v4, v1, 0x4

    and-int/lit8 v4, v4, 0xf

    aget-object v3, v3, v4

    invoke-virtual {p1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v3, Lcom/jscape/inet/util/l;->b:[Ljava/lang/String;

    and-int/lit8 v1, v1, 0xf

    aget-object v1, v3, v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v0, :cond_2

    if-eqz v0, :cond_1

    add-int/lit8 v2, v2, 0x1

    :try_start_2
    array-length v1, p0

    if-ge v2, v1, :cond_1

    const-string v1, ":"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :cond_1
    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_3
    invoke-static {p0}, Lcom/jscape/inet/util/l;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p0

    :try_start_4
    invoke-static {p0}, Lcom/jscape/inet/util/l;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p0

    :try_start_5
    invoke-static {p0}, Lcom/jscape/inet/util/l;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_2
    :goto_0
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    return-object p0

    :catch_3
    sget-object p0, Lcom/jscape/inet/util/l;->c:[Ljava/lang/String;

    const/4 p1, 0x4

    aget-object p0, p0, p1

    return-object p0
.end method

.method static a(Ljava/lang/String;IJ)Ljava/net/Socket;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x0

    cmp-long v1, p2, v1

    if-nez v1, :cond_0

    :try_start_0
    new-instance p2, Ljava/net/Socket;

    invoke-direct {p2, p0, p1}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p2

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance p1, Ljava/lang/Exception;

    invoke-direct {p1, p0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_0
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/net/Socket;

    new-array v3, v1, [Ljava/lang/Exception;

    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/jscape/inet/util/m;

    invoke-direct {v5, v2, p0, p1, v3}, Lcom/jscape/inet/util/m;-><init>([Ljava/net/Socket;Ljava/lang/String;I[Ljava/lang/Exception;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/jscape/inet/util/l;->c:[Ljava/lang/String;

    const/4 v6, 0x0

    aget-object v7, v5, v6

    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    :try_start_1
    invoke-virtual {v4, p2, p3}, Ljava/lang/Thread;->join(J)V

    const/4 p0, 0x3

    aget-object p0, v5, p0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    const-string p0, ""

    :goto_0
    :try_start_2
    aget-object p1, v2, v6
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    if-eqz v0, :cond_4

    if-eqz p1, :cond_1

    :try_start_3
    aget-object p1, v2, v6
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_2

    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p0, Lcom/jscape/inet/util/l;->c:[Ljava/lang/String;

    aget-object p0, p0, v1

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    if-eqz v0, :cond_3

    :try_start_4
    aget-object p1, v3, v6
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    if-eqz p1, :cond_2

    aget-object p0, v3, v6

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_2
    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/util/l;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_3
    :goto_1
    new-instance p1, Ljava/lang/Exception;

    invoke-direct {p1, p0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    :goto_2
    return-object p1

    :catch_3
    move-exception p0

    :try_start_5
    invoke-static {p0}, Lcom/jscape/inet/util/l;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/util/l;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
.end method

.method private static a([BI[BI)Z
    .locals 9

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    array-length v1, p0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    return v2

    :cond_0
    array-length v3, p2

    goto :goto_0

    :cond_1
    move v3, v1

    :cond_2
    :goto_0
    const/4 v4, 0x1

    if-ge p1, v1, :cond_1b

    if-eqz v0, :cond_1a

    if-eqz v0, :cond_1a

    if-ge p3, v3, :cond_1b

    aget-byte v5, p0, p1

    const/16 v6, 0x5c

    const/16 v7, 0x2a

    if-eqz v0, :cond_8

    if-ne v5, v6, :cond_7

    add-int/lit8 v5, p1, 0x1

    if-eqz v0, :cond_5

    if-ne v5, v1, :cond_3

    return v2

    :cond_3
    if-eqz v0, :cond_4

    aget-byte p1, p0, v5

    aget-byte v6, p2, p3

    move v8, v5

    move v5, p1

    move p1, v8

    goto :goto_1

    :cond_4
    move p1, v5

    goto :goto_2

    :cond_5
    move v6, v1

    :goto_1
    if-eq v5, v6, :cond_6

    return v2

    :cond_6
    add-int/2addr p1, v4

    add-int/lit8 p3, p3, 0x1

    :goto_2
    if-nez v0, :cond_2

    :cond_7
    aget-byte v5, p0, p1

    move v6, v7

    :cond_8
    if-eqz v0, :cond_11

    if-ne v5, v6, :cond_10

    if-eqz v0, :cond_a

    add-int/lit8 p1, p1, 0x1

    if-ne v1, p1, :cond_9

    return v4

    :cond_9
    aget-byte v1, p0, p1

    :cond_a
    if-ge p3, v3, :cond_f

    if-eqz v0, :cond_e

    if-eqz v0, :cond_b

    aget-byte v5, p2, p3

    if-ne v1, v5, :cond_c

    invoke-static {p0, p1, p2, p3}, Lcom/jscape/inet/util/l;->a([BI[BI)Z

    move-result v5

    goto :goto_3

    :cond_b
    move v5, v1

    :goto_3
    if-eqz v0, :cond_d

    if-eqz v5, :cond_c

    goto :goto_4

    :cond_c
    add-int/lit8 p3, p3, 0x1

    if-nez v0, :cond_a

    goto :goto_5

    :cond_d
    move v4, v5

    :goto_4
    return v4

    :cond_e
    move v2, v1

    :cond_f
    :goto_5
    return v2

    :cond_10
    aget-byte v5, p0, p1

    const/16 v6, 0x3f

    :cond_11
    if-eqz v0, :cond_13

    if-ne v5, v6, :cond_12

    add-int/lit8 p1, p1, 0x1

    add-int/lit8 p3, p3, 0x1

    if-nez v0, :cond_2

    :cond_12
    aget-byte v5, p0, p1

    aget-byte v6, p2, p3

    :cond_13
    if-eqz v0, :cond_15

    if-eq v5, v6, :cond_14

    return v2

    :cond_14
    add-int/lit8 p1, p1, 0x1

    add-int/lit8 v5, p3, 0x1

    move v6, v3

    move p3, v5

    :cond_15
    if-eqz v0, :cond_16

    if-lt v5, v6, :cond_2

    move v5, p1

    move v6, v1

    :cond_16
    if-eqz v0, :cond_19

    if-lt v5, v6, :cond_17

    return v4

    :cond_17
    aget-byte v5, p0, p1

    if-eqz v0, :cond_18

    goto :goto_6

    :cond_18
    move v4, v5

    goto :goto_7

    :cond_19
    move v7, v6

    :goto_6
    if-ne v5, v7, :cond_2

    :goto_7
    return v4

    :cond_1a
    move p1, p3

    move v1, v3

    goto :goto_8

    :cond_1b
    if-eqz v0, :cond_1d

    :goto_8
    if-ne p1, v1, :cond_1e

    if-eqz v0, :cond_1c

    if-ne p3, v3, :cond_1e

    return v4

    :cond_1c
    move v2, p3

    goto :goto_9

    :cond_1d
    move v2, p1

    :cond_1e
    :goto_9
    return v2
.end method

.method static a([B[B)Z
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0, p1, v0}, Lcom/jscape/inet/util/l;->a([BI[BI)Z

    move-result p0

    return p0
.end method

.method static a([BII)[B
    .locals 10

    new-array v0, p2, [B

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move v3, p1

    move v4, v2

    :cond_0
    add-int v5, p1, p2

    if-ge v3, v5, :cond_5

    aget-byte v5, p0, v3

    invoke-static {v5}, Lcom/jscape/inet/util/l;->a(B)B

    move-result v5

    shl-int/lit8 v5, v5, 0x2

    add-int/lit8 v6, v3, 0x1

    aget-byte v7, p0, v6

    invoke-static {v7}, Lcom/jscape/inet/util/l;->a(B)B

    move-result v7

    and-int/lit8 v7, v7, 0x30

    ushr-int/lit8 v7, v7, 0x4

    or-int/2addr v5, v7

    int-to-byte v5, v5

    aput-byte v5, v0, v4

    add-int/lit8 v5, v3, 0x2

    aget-byte v7, p0, v5

    if-eqz v1, :cond_6

    const/16 v8, 0x3d

    if-eqz v1, :cond_3

    if-ne v7, v8, :cond_1

    add-int/lit8 v4, v4, 0x1

    if-nez v1, :cond_5

    :cond_1
    add-int/lit8 v7, v4, 0x1

    aget-byte v6, p0, v6

    invoke-static {v6}, Lcom/jscape/inet/util/l;->a(B)B

    move-result v6

    and-int/lit8 v6, v6, 0xf

    shl-int/lit8 v6, v6, 0x4

    aget-byte v9, p0, v5

    invoke-static {v9}, Lcom/jscape/inet/util/l;->a(B)B

    move-result v9

    and-int/lit8 v9, v9, 0x3c

    ushr-int/lit8 v9, v9, 0x2

    or-int/2addr v6, v9

    int-to-byte v6, v6

    aput-byte v6, v0, v7

    add-int/lit8 v6, v3, 0x3

    if-eqz v1, :cond_2

    aget-byte v7, p0, v6

    goto :goto_0

    :cond_2
    move-object v7, p0

    goto :goto_1

    :cond_3
    :goto_0
    if-ne v7, v8, :cond_4

    add-int/lit8 v4, v4, 0x2

    if-nez v1, :cond_5

    :cond_4
    add-int/lit8 v6, v4, 0x2

    move-object v7, v0

    :goto_1
    aget-byte v5, p0, v5

    invoke-static {v5}, Lcom/jscape/inet/util/l;->a(B)B

    move-result v5

    and-int/lit8 v5, v5, 0x3

    shl-int/lit8 v5, v5, 0x6

    add-int/lit8 v8, v3, 0x3

    aget-byte v8, p0, v8

    invoke-static {v8}, Lcom/jscape/inet/util/l;->a(B)B

    move-result v8

    and-int/lit8 v8, v8, 0x3f

    or-int/2addr v5, v8

    int-to-byte v5, v5

    aput-byte v5, v7, v6

    add-int/lit8 v4, v4, 0x3

    add-int/lit8 v3, v3, 0x4

    if-nez v1, :cond_0

    :cond_5
    move v7, v4

    :cond_6
    new-array p0, v7, [B

    invoke-static {v0, v2, p0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p0
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 8

    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move v4, v3

    :cond_0
    invoke-virtual {p0, p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v5

    if-ltz v5, :cond_1

    new-instance v6, Ljava/lang/String;

    sub-int v7, v5, v4

    invoke-direct {v6, v0, v4, v7}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v1, v6}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/lit8 v4, v5, 0x1

    if-eqz v2, :cond_2

    if-nez v2, :cond_0

    :cond_1
    new-instance p0, Ljava/lang/String;

    array-length p1, v0

    sub-int/2addr p1, v4

    invoke-direct {p0, v0, v4, p1}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v1, p0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :cond_2
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result p0

    new-array p1, p0, [Ljava/lang/String;

    :cond_3
    if-ge v3, p0, :cond_4

    if-eqz v2, :cond_4

    invoke-virtual {v1, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    check-cast v0, Ljava/lang/String;

    aput-object v0, p1, v3

    add-int/lit8 v3, v3, 0x1

    if-nez v2, :cond_3

    :cond_4
    return-object p1
.end method

.method static b([B[B)Z
    .locals 6

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    array-length v1, p0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    array-length v3, p1

    if-eq v1, v3, :cond_0

    return v2

    :cond_0
    move v3, v2

    goto :goto_0

    :cond_1
    move v3, v1

    :cond_2
    :goto_0
    if-ge v3, v1, :cond_5

    aget-byte v4, p0, v3

    if-eqz v0, :cond_6

    if-eqz v0, :cond_4

    aget-byte v5, p1, v3

    if-eq v4, v5, :cond_3

    goto :goto_1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    if-nez v0, :cond_2

    goto :goto_2

    :cond_4
    move v2, v4

    :goto_1
    return v2

    :cond_5
    :goto_2
    const/4 v4, 0x1

    :cond_6
    return v4
.end method

.method static b([BII)[B
    .locals 11

    mul-int/lit8 v0, p2, 0x2

    new-array v0, v0, [B

    div-int/lit8 v1, p2, 0x3

    mul-int/lit8 v1, v1, 0x3

    add-int/2addr v1, p1

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move v4, p1

    move v5, v3

    :cond_0
    const/4 v6, 0x2

    if-ge v4, v1, :cond_1

    aget-byte v7, p0, v4

    ushr-int/2addr v7, v6

    and-int/lit8 v7, v7, 0x3f

    add-int/lit8 v8, v5, 0x1

    sget-object v9, Lcom/jscape/inet/util/l;->a:[B

    aget-byte v7, v9, v7

    aput-byte v7, v0, v5

    aget-byte v5, p0, v4

    and-int/lit8 v5, v5, 0x3

    shl-int/lit8 v5, v5, 0x4

    add-int/lit8 v7, v4, 0x1

    aget-byte v10, p0, v7

    ushr-int/lit8 v10, v10, 0x4

    and-int/lit8 v10, v10, 0xf

    or-int/2addr v5, v10

    add-int/lit8 v10, v8, 0x1

    aget-byte v5, v9, v5

    aput-byte v5, v0, v8

    aget-byte v5, p0, v7

    and-int/lit8 v5, v5, 0xf

    shl-int/2addr v5, v6

    add-int/lit8 v7, v4, 0x2

    aget-byte v8, p0, v7

    ushr-int/lit8 v8, v8, 0x6

    and-int/lit8 v8, v8, 0x3

    or-int/2addr v5, v8

    add-int/lit8 v8, v10, 0x1

    aget-byte v5, v9, v5

    aput-byte v5, v0, v10

    aget-byte v5, p0, v7

    and-int/lit8 v5, v5, 0x3f

    add-int/lit8 v7, v8, 0x1

    aget-byte v5, v9, v5

    aput-byte v5, v0, v8

    add-int/lit8 v4, v4, 0x3

    if-eqz v2, :cond_2

    move v5, v7

    if-nez v2, :cond_0

    :cond_1
    add-int/2addr p1, p2

    sub-int v1, p1, v1

    move v7, v5

    :cond_2
    const/16 p1, 0x3d

    const/4 p2, 0x1

    if-eqz v2, :cond_4

    if-ne v1, p2, :cond_3

    aget-byte v5, p0, v4

    ushr-int/2addr v5, v6

    and-int/lit8 v5, v5, 0x3f

    add-int/lit8 v8, v7, 0x1

    sget-object v9, Lcom/jscape/inet/util/l;->a:[B

    aget-byte v5, v9, v5

    aput-byte v5, v0, v7

    aget-byte v5, p0, v4

    and-int/lit8 v5, v5, 0x3

    shl-int/lit8 v5, v5, 0x4

    and-int/lit8 v5, v5, 0x3f

    add-int/lit8 v7, v8, 0x1

    aget-byte v5, v9, v5

    aput-byte v5, v0, v8

    add-int/lit8 v5, v7, 0x1

    aput-byte p1, v0, v7

    add-int/lit8 v7, v5, 0x1

    aput-byte p1, v0, v5

    if-nez v2, :cond_5

    :cond_3
    if-eqz v2, :cond_6

    move v2, v6

    goto :goto_0

    :cond_4
    move v2, p2

    :goto_0
    if-ne v1, v2, :cond_5

    aget-byte v1, p0, v4

    ushr-int/2addr v1, v6

    and-int/lit8 v1, v1, 0x3f

    add-int/lit8 v2, v7, 0x1

    sget-object v5, Lcom/jscape/inet/util/l;->a:[B

    aget-byte v1, v5, v1

    aput-byte v1, v0, v7

    aget-byte v1, p0, v4

    and-int/lit8 v1, v1, 0x3

    shl-int/lit8 v1, v1, 0x4

    add-int/2addr v4, p2

    aget-byte p2, p0, v4

    ushr-int/lit8 p2, p2, 0x4

    and-int/lit8 p2, p2, 0xf

    or-int/2addr p2, v1

    add-int/lit8 v1, v2, 0x1

    aget-byte p2, v5, p2

    aput-byte p2, v0, v2

    aget-byte p0, p0, v4

    and-int/lit8 p0, p0, 0xf

    shl-int/2addr p0, v6

    and-int/lit8 p0, p0, 0x3f

    add-int/lit8 p2, v1, 0x1

    aget-byte p0, v5, p0

    aput-byte p0, v0, v1

    add-int/lit8 p0, p2, 0x1

    aput-byte p1, v0, p2

    move v1, p0

    goto :goto_1

    :cond_5
    move v1, v7

    :goto_1
    move v7, v1

    :cond_6
    new-array p0, v1, [B

    invoke-static {v0, v3, p0, v3, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p0
.end method
