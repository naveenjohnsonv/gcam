.class public Lcom/jscape/inet/util/i;
.super Ljava/lang/Object;


# static fields
.field private static d:[Ljava/lang/String;

.field private static final e:[Ljava/lang/String;


# instance fields
.field public final a:I

.field public final b:I

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/String;

    invoke-static {v3}, Lcom/jscape/inet/util/i;->b([Ljava/lang/String;)V

    const/4 v3, 0x0

    const/16 v4, 0x11

    const/4 v5, -0x1

    move v6, v3

    :goto_0
    const/4 v7, 0x1

    add-int/2addr v5, v7

    add-int/2addr v4, v5

    const-string v8, "\u000fFx>XN{8L*1y[t-]7\u0008s\tc$nJmb\u0006s\to$n\u0012"

    invoke-virtual {v8, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    array-length v9, v5

    move v10, v3

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v5}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v7, v6, 0x1

    aput-object v5, v1, v6

    const/16 v5, 0x21

    if-ge v4, v5, :cond_0

    invoke-virtual {v8, v4}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/util/i;->e:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v5, v10

    rem-int/lit8 v12, v10, 0x7

    const/16 v13, 0xd

    if-eqz v12, :cond_5

    if-eq v12, v7, :cond_4

    if-eq v12, v2, :cond_6

    if-eq v12, v0, :cond_3

    const/4 v14, 0x4

    if-eq v12, v14, :cond_6

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v13, 0x12

    goto :goto_2

    :cond_2
    const/16 v13, 0x28

    goto :goto_2

    :cond_3
    const/16 v13, 0x4d

    goto :goto_2

    :cond_4
    const/16 v13, 0x2e

    goto :goto_2

    :cond_5
    const/16 v13, 0x58

    :cond_6
    :goto_2
    const/4 v12, 0x7

    xor-int/2addr v12, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v5, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jscape/inet/util/i;->a:I

    iput p2, p0, Lcom/jscape/inet/util/i;->b:I

    iput p1, p0, Lcom/jscape/inet/util/i;->c:I

    return-void
.end method

.method public static b([Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/util/i;->d:[Ljava/lang/String;

    return-void
.end method

.method public static b()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/inet/util/i;->d:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 3

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/util/i;->c:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/jscape/inet/util/i;->c:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/jscape/inet/util/i;->b:I

    if-le v2, v0, :cond_1

    iget v0, p0, Lcom/jscape/inet/util/i;->a:I

    iput v0, p0, Lcom/jscape/inet/util/i;->c:I

    goto :goto_0

    :cond_0
    move v1, v2

    :cond_1
    :goto_0
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/util/i;->e:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/util/i;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/util/i;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/util/i;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
