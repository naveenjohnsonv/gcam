.class Lcom/jscape/inet/util/n;
.super Ljava/lang/Thread;


# instance fields
.field private a:Z

.field private b:J


# direct methods
.method constructor <init>(ZJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-boolean p1, p0, Lcom/jscape/inet/util/n;->a:Z

    iput-wide p2, p0, Lcom/jscape/inet/util/n;->b:J

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public a(J)V
    .locals 0

    iput-wide p1, p0, Lcom/jscape/inet/util/n;->b:J

    return-void
.end method

.method public a(Z)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iput-boolean p1, p0, Lcom/jscape/inet/util/n;->a:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/inet/util/n;->interrupt()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    :cond_0
    return-void

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/util/n;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/util/n;->a:Z

    return v0
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/util/n;->b:J

    return-wide v0
.end method

.method public run()V
    .locals 3

    const/4 v0, 0x1

    :try_start_0
    iget-wide v1, p0, Lcom/jscape/inet/util/n;->b:J

    invoke-static {v1, v2}, Lcom/jscape/inet/util/n;->sleep(J)V

    iput-boolean v0, p0, Lcom/jscape/inet/util/n;->a:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    iput-boolean v0, p0, Lcom/jscape/inet/util/n;->a:Z

    :goto_0
    return-void
.end method
