.class public Lcom/jscape/inet/util/k;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "g$\n) #p\"*\u000c+(9\u0007$&\r+))w\u000c-(\u0015$b<f5:\n*\"\u0016\u0004&\u0016)(jm(=C&#$m\"*\u0017e*%qg\u0004% \r!\u000f-(\u0015$b$f3g0*/!f3\u0003vgP\u000c-(\u0015$b<f5:\n*\"\u0016-(\u0015$b$f3g0*/!f3\u0008\u0007!>/p4\u001a\u0004&\r+))w.&\re\"%wg,\u00101-(o.:\u000b (\u0003vgR\u0003vgQ\u0014-(\u0015$b$f3g*+)>B#-\u0011 ?9"

    const/16 v4, 0xae

    const/16 v5, 0xd

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x7c

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0x39

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v13, v10

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v12, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x1e

    const/16 v3, 0x1a

    const-string v5, "hmPa\'a#v\"onl{\u0015moMe}N\"f~Csz\u00033\"\u0016"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    add-int/2addr v6, v9

    add-int v8, v6, v5

    invoke-virtual {v3, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v8, v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/util/k;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v10, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v9, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v1, 0x7f

    goto :goto_4

    :cond_4
    const/16 v1, 0x36

    goto :goto_4

    :cond_5
    const/16 v1, 0x30

    goto :goto_4

    :cond_6
    move v1, v11

    goto :goto_4

    :cond_7
    const/16 v1, 0x1f

    goto :goto_4

    :cond_8
    const/16 v1, 0x35

    goto :goto_4

    :cond_9
    const/16 v1, 0x3b

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v10, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private static a(JLcom/jscape/inet/util/o;)Ljava/net/Socket;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/util/n;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0, p1}, Lcom/jscape/inet/util/n;-><init>(ZJ)V

    invoke-static {p2, v0}, Lcom/jscape/inet/util/o;->a(Lcom/jscape/inet/util/o;Lcom/jscape/inet/util/n;)V

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/jscape/inet/util/o;->start()V

    invoke-virtual {v0}, Lcom/jscape/inet/util/n;->start()V

    :goto_0
    :try_start_0
    invoke-virtual {v0}, Lcom/jscape/inet/util/n;->a()Z

    move-result v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v3, :cond_0

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-virtual {p2}, Lcom/jscape/inet/util/o;->b()Z

    move-result v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v2, :cond_2

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/jscape/inet/util/o;->e()Ljava/net/Socket;

    move-result-object p0

    return-object p0

    :cond_1
    if-eqz v2, :cond_3

    :try_start_2
    invoke-virtual {p2}, Lcom/jscape/inet/util/o;->d()Z

    move-result v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/util/k;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_2
    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lcom/jscape/inet/util/o;->f()Ljava/io/IOException;

    move-result-object p0

    invoke-virtual {p2, v1}, Lcom/jscape/inet/util/o;->a(Z)V

    throw p0

    :cond_3
    invoke-virtual {p2, v1}, Lcom/jscape/inet/util/o;->a(Z)V

    new-instance p2, Ljava/io/InterruptedIOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/util/k;->a:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v3, v2, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    aget-object p0, v2, v1

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p2, p0}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    throw p2

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/util/k;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/util/k;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
.end method

.method public static a(Ljava/lang/String;IJ)Ljava/net/Socket;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object p0

    invoke-static {p0, p1, p2, p3}, Lcom/jscape/inet/util/k;->a(Ljava/net/InetAddress;IJ)Ljava/net/Socket;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;IJII)Ljava/net/Socket;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    move v1, p1

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/jscape/inet/util/k;->a(Ljava/net/InetAddress;IJII)Ljava/net/Socket;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;IJLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/net/Socket;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/util/k;->a:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_1

    :try_start_0
    invoke-static {v1}, Lcom/jscape/inet/util/k;->a(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    :try_start_1
    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    new-instance v0, Lcom/jscape/inet/util/o;

    move-object v1, v0

    move v3, p1

    move-wide v4, p2

    move-object/from16 v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v1 .. v10}, Lcom/jscape/inet/util/o;-><init>(Ljava/net/InetAddress;IJLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    new-instance v0, Lcom/jscape/inet/util/o;

    move-object v3, v0

    move-object v4, p0

    move v5, p1

    move-wide v6, p2

    move-object/from16 v8, p4

    move/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    invoke-direct/range {v3 .. v12}, Lcom/jscape/inet/util/o;-><init>(Ljava/lang/String;IJLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    move-wide v4, p2

    invoke-static {v4, v5, v0}, Lcom/jscape/inet/util/k;->a(JLcom/jscape/inet/util/o;)Ljava/net/Socket;

    move-result-object v0

    goto :goto_2

    :cond_0
    move-wide v4, p2

    move-object v1, p0

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/util/k;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    move-wide v4, p2

    :goto_1
    invoke-static {v1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    new-instance v0, Lcom/jscape/inet/util/o;

    move-object v1, v0

    move v3, p1

    move-wide v4, p2

    move-object/from16 v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v1 .. v10}, Lcom/jscape/inet/util/o;-><init>(Ljava/net/InetAddress;IJLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :try_start_2
    invoke-virtual {v0}, Lcom/jscape/inet/util/o;->a()Ljava/net/Socket;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    if-eqz v0, :cond_2

    :goto_2
    return-object v0

    :cond_2
    :try_start_3
    new-instance v0, Ljava/lang/Exception;

    sget-object v1, Lcom/jscape/inet/util/k;->a:[Ljava/lang/String;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/util/k;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static a(Ljava/net/InetAddress;IJ)Ljava/net/Socket;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/util/k;->a:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/jscape/inet/util/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/jscape/inet/util/o;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/jscape/inet/util/o;-><init>(Ljava/net/InetAddress;IJ)V

    invoke-static {p2, p3, v0}, Lcom/jscape/inet/util/k;->a(JLcom/jscape/inet/util/o;)Ljava/net/Socket;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move-wide v2, p2

    invoke-static/range {v0 .. v7}, Lcom/jscape/inet/util/k;->a(Ljava/net/InetAddress;IJIILjava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static a(Ljava/net/InetAddress;IJII)Ljava/net/Socket;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/util/k;->a:[Ljava/lang/String;

    const/4 v1, 0x7

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/jscape/inet/util/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/jscape/inet/util/o;

    move-object v1, v0

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move v6, p4

    move v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/jscape/inet/util/o;-><init>(Ljava/net/InetAddress;IJII)V

    invoke-static {p2, p3, v0}, Lcom/jscape/inet/util/k;->a(JLcom/jscape/inet/util/o;)Ljava/net/Socket;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v7}, Lcom/jscape/inet/util/k;->a(Ljava/net/InetAddress;IJIILjava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private static a(Ljava/net/InetAddress;IJIILjava/net/InetAddress;I)Ljava/net/Socket;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move/from16 v0, p4

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v1

    :try_start_0
    sget-object v2, Lcom/jscape/inet/util/k;->a:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v3, v2, v3

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/Socket;

    const/16 v5, 0x8

    aget-object v5, v2, v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/4 v6, 0x2

    new-array v7, v6, [Ljava/lang/Class;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v10, 0x1

    aput-object v9, v7, v10

    new-array v9, v6, [Ljava/lang/Class;

    const/16 v11, 0xc

    aget-object v11, v2, v11

    invoke-static {v11}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v11

    aput-object v11, v9, v8

    sget-object v11, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v11, v9, v10

    const/16 v11, 0xd

    aget-object v2, v2, v11

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    new-array v9, v6, [Ljava/lang/Object;

    if-eqz v1, :cond_1

    if-eqz p6, :cond_0

    aput-object p6, v9, v8

    new-instance v11, Ljava/lang/Integer;

    move/from16 v12, p7

    invoke-direct {v11, v12}, Ljava/lang/Integer;-><init>(I)V

    aput-object v11, v9, v10

    invoke-virtual {v2, v9}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    new-array v12, v10, [Ljava/lang/Object;

    aput-object v11, v12, v8

    new-array v11, v10, [Ljava/lang/Class;

    aput-object v5, v11, v8

    sget-object v5, Lcom/jscape/inet/util/k;->a:[Ljava/lang/String;

    const/4 v13, 0x4

    aget-object v5, v5, v13

    invoke-virtual {v3, v5, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    invoke-virtual {v5, v4, v12}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    aput-object p0, v9, v8

    new-instance v5, Ljava/lang/Integer;

    move v11, p1

    invoke-direct {v5, p1}, Ljava/lang/Integer;-><init>(I)V

    aput-object v5, v9, v10

    invoke-virtual {v2, v9}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    :cond_1
    move-object/from16 v2, p6

    :goto_0
    new-array v5, v6, [Ljava/lang/Object;

    aput-object v2, v5, v8

    new-instance v2, Ljava/lang/Integer;

    move-wide/from16 v8, p2

    long-to-int v6, v8

    invoke-direct {v2, v6}, Ljava/lang/Integer;-><init>(I)V

    aput-object v2, v5, v10

    sget-object v2, Lcom/jscape/inet/util/k;->a:[Ljava/lang/String;

    aget-object v2, v2, v10

    invoke-virtual {v3, v2, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    :try_start_1
    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    if-eqz v1, :cond_3

    if-lez v0, :cond_2

    :try_start_2
    invoke-virtual {v4, v0}, Ljava/net/Socket;->setSendBufferSize(I)V
    :try_end_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    :cond_2
    move/from16 v0, p5

    :cond_3
    if-lez v0, :cond_4

    move/from16 v0, p5

    :try_start_3
    invoke-virtual {v4, v0}, Ljava/net/Socket;->setReceiveBufferSize(I)V
    :try_end_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_4
    invoke-static {v1}, Lcom/jscape/inet/util/k;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :cond_4
    :goto_1
    return-object v4

    :catch_1
    move-exception v0

    move-object v1, v0

    :try_start_5
    invoke-static {v1}, Lcom/jscape/inet/util/k;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    :catch_2
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/util/k;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getTargetException()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method public static a(Ljava/net/InetAddress;ILjava/net/InetAddress;IJ)Ljava/net/Socket;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/util/k;->a:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/jscape/inet/util/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/jscape/inet/util/o;

    move-object v1, v0

    move-object v2, p0

    move v3, p1

    move-object v4, p2

    move v5, p3

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/jscape/inet/util/o;-><init>(Ljava/net/InetAddress;ILjava/net/InetAddress;IJ)V

    invoke-static {p4, p5, v0}, Lcom/jscape/inet/util/k;->a(JLcom/jscape/inet/util/o;)Ljava/net/Socket;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-wide v2, p4

    move-object v6, p2

    move v7, p3

    invoke-static/range {v0 .. v7}, Lcom/jscape/inet/util/k;->a(Ljava/net/InetAddress;IJIILjava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static a(Ljava/net/InetAddress;ILjava/net/InetAddress;IJII)Ljava/net/Socket;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/util/k;->a:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/jscape/inet/util/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/jscape/inet/util/o;

    move-object v1, v0

    move-object v2, p0

    move v3, p1

    move-object v4, p2

    move v5, p3

    move-wide v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lcom/jscape/inet/util/o;-><init>(Ljava/net/InetAddress;ILjava/net/InetAddress;IJII)V

    move-wide v3, p4

    invoke-static {p4, p5, v0}, Lcom/jscape/inet/util/k;->a(JLcom/jscape/inet/util/o;)Ljava/net/Socket;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-wide v3, p4

    move-object v1, p0

    move v2, p1

    move/from16 v5, p6

    move/from16 v6, p7

    move-object v7, p2

    move v8, p3

    invoke-static/range {v1 .. v8}, Lcom/jscape/inet/util/k;->a(Ljava/net/InetAddress;IJIILjava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 3

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/util/k;->a:[Ljava/lang/String;

    const/16 v2, 0xb

    aget-object v2, v1, v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v0, :cond_2

    if-nez v2, :cond_1

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v0, :cond_2

    if-nez v2, :cond_1

    const/16 v2, 0xe

    aget-object v2, v1, v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v0, :cond_2

    if-nez v2, :cond_1

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz v0, :cond_3

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :cond_2
    move p0, v2

    :cond_3
    :goto_1
    return p0
.end method
