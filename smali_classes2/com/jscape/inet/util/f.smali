.class public Lcom/jscape/inet/util/f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/util/e;


# static fields
.field private static final a:I = 0x50

.field private static final k:[Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Ljava/io/InputStream;

.field private g:Ljava/io/OutputStream;

.field private h:Ljava/net/Socket;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "B`3{H@=gf4lC\u0004\u0006sf5l_W\\Ps/jRM\rG|7m^\u001a\\`w=p^\u0003\u0002\u001f\u0018\u000bB`3{H%(FBf#\rb`3{HM\u0019``3q\u000bM\u000b2Z\u0008WaBM<\"Q\t"

    const/16 v4, 0x52

    const/16 v5, 0x1b

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x3a

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    const/16 v14, 0xb

    const/4 v15, 0x2

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const-string v3, "`g\u0008.\"m2\u000bQWM"

    move v7, v10

    move v4, v14

    move v5, v15

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x45

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/util/f;->k:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v10, v13

    rem-int/lit8 v1, v13, 0x7

    const/16 v17, 0x28

    if-eqz v1, :cond_7

    if-eq v1, v9, :cond_7

    if-eq v1, v15, :cond_6

    const/4 v15, 0x3

    if-eq v1, v15, :cond_5

    const/4 v15, 0x4

    if-eq v1, v15, :cond_8

    const/4 v14, 0x5

    if-eq v1, v14, :cond_4

    const/16 v14, 0x46

    goto :goto_4

    :cond_4
    const/16 v14, 0x57

    goto :goto_4

    :cond_5
    const/16 v14, 0x39

    goto :goto_4

    :cond_6
    const/16 v14, 0x66

    goto :goto_4

    :cond_7
    move/from16 v14, v17

    :cond_8
    :goto_4
    xor-int v1, v8, v14

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/16 v0, 0x3a

    :try_start_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v2, -0x1

    const/16 v3, 0x50

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-object p1, v1

    :catch_1
    :cond_0
    iput-object p1, p0, Lcom/jscape/inet/util/f;->b:Ljava/lang/String;

    iput v3, p0, Lcom/jscape/inet/util/f;->c:I

    goto :goto_0

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/util/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/util/f;->b:Ljava/lang/String;

    iput p2, p0, Lcom/jscape/inet/util/f;->c:I

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static e()I
    .locals 1

    const/16 v0, 0x50

    return v0
.end method


# virtual methods
.method public a()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/f;->f:Ljava/io/InputStream;

    return-object v0
.end method

.method public a(Lcom/jscape/inet/util/j;Ljava/lang/String;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, ":"

    iput-object p2, p0, Lcom/jscape/inet/util/f;->d:Ljava/lang/String;

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v1

    iput p3, p0, Lcom/jscape/inet/util/f;->e:I

    if-eqz v1, :cond_1

    if-nez p1, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/jscape/inet/util/f;->b:Ljava/lang/String;

    iget v3, p0, Lcom/jscape/inet/util/f;->c:I

    invoke-static {v2, v3, p4, p5}, Lcom/jscape/inet/util/l;->a(Ljava/lang/String;IJ)Ljava/net/Socket;

    move-result-object v2

    iput-object v2, p0, Lcom/jscape/inet/util/f;->h:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/jscape/inet/util/f;->f:Ljava/io/InputStream;

    iget-object v2, p0, Lcom/jscape/inet/util/f;->h:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/jscape/inet/util/f;->g:Ljava/io/OutputStream;

    if-nez v1, :cond_2

    :cond_0
    iget-object v2, p0, Lcom/jscape/inet/util/f;->b:Ljava/lang/String;

    iget v3, p0, Lcom/jscape/inet/util/f;->c:I

    invoke-interface {p1, v2, v3}, Lcom/jscape/inet/util/j;->a(Ljava/lang/String;I)Ljava/net/Socket;

    move-result-object v2

    iput-object v2, p0, Lcom/jscape/inet/util/f;->h:Ljava/net/Socket;

    invoke-interface {p1, v2}, Lcom/jscape/inet/util/j;->a(Ljava/net/Socket;)Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, p0, Lcom/jscape/inet/util/f;->f:Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_e

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/util/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/jscape/inet/util/f;->h:Ljava/net/Socket;

    invoke-interface {p1, v2}, Lcom/jscape/inet/util/j;->b(Ljava/net/Socket;)Ljava/io/OutputStream;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/util/f;->g:Ljava/io/OutputStream;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_a

    :cond_2
    const-wide/16 v2, 0x0

    cmp-long p1, p4, v2

    if-lez p1, :cond_3

    :try_start_2
    iget-object p1, p0, Lcom/jscape/inet/util/f;->h:Ljava/net/Socket;

    long-to-int p4, p4

    invoke-virtual {p1, p4}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_e

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/util/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_e
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_a

    :cond_3
    :goto_1
    :try_start_4
    iget-object p1, p0, Lcom/jscape/inet/util/f;->h:Ljava/net/Socket;

    const/4 p4, 0x1

    invoke-virtual {p1, p4}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    iget-object p1, p0, Lcom/jscape/inet/util/f;->g:Ljava/io/OutputStream;

    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/util/f;->k:[Ljava/lang/String;

    const/4 v3, 0x7

    aget-object v2, v2, v3

    invoke-virtual {p5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object p2, Lcom/jscape/inet/util/f;->k:[Ljava/lang/String;

    const/4 p3, 0x5

    aget-object p2, p2, p3

    invoke-virtual {p5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    sget-object p3, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p2, p3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/io/OutputStream;->write([B)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_e

    const/4 p1, 0x0

    if-eqz v1, :cond_5

    :try_start_5
    iget-object p2, p0, Lcom/jscape/inet/util/f;->i:Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_8
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_e

    if-eqz p2, :cond_4

    if-eqz v1, :cond_5

    :try_start_6
    iget-object p2, p0, Lcom/jscape/inet/util/f;->j:Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_9
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_e

    if-eqz p2, :cond_4

    :try_start_7
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p3, p0, Lcom/jscape/inet/util/f;->i:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/jscape/inet/util/f;->j:Ljava/lang/String;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    sget-object p3, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p2, p3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p2

    array-length p3, p2

    invoke-static {p2, p1, p3}, Lcom/jscape/inet/util/l;->b([BII)[B

    move-result-object p2

    iget-object p3, p0, Lcom/jscape/inet/util/f;->g:Ljava/io/OutputStream;

    sget-object p5, Lcom/jscape/inet/util/f;->k:[Ljava/lang/String;

    aget-object v0, p5, p1

    sget-object v2, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/OutputStream;->write([B)V

    iget-object p3, p0, Lcom/jscape/inet/util/f;->g:Ljava/io/OutputStream;

    invoke-virtual {p3, p2}, Ljava/io/OutputStream;->write([B)V

    iget-object p2, p0, Lcom/jscape/inet/util/f;->g:Ljava/io/OutputStream;

    const/4 p3, 0x6

    aget-object p3, p5, p3

    sget-object p5, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p3, p5}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/io/OutputStream;->write([B)V

    :cond_4
    iget-object p2, p0, Lcom/jscape/inet/util/f;->g:Ljava/io/OutputStream;

    sget-object p3, Lcom/jscape/inet/util/f;->k:[Ljava/lang/String;

    const/4 p5, 0x2

    aget-object p3, p3, p5

    sget-object p5, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p3, p5}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/io/OutputStream;->write([B)V

    :cond_5
    iget-object p2, p0, Lcom/jscape/inet/util/f;->g:Ljava/io/OutputStream;

    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    new-instance p2, Ljava/lang/StringBuffer;

    invoke-direct {p2}, Ljava/lang/StringBuffer;-><init>()V

    move p3, p1

    :cond_6
    :goto_2
    const/16 p5, 0xa

    const/16 v0, 0xd

    if-ltz p3, :cond_9

    iget-object p3, p0, Lcom/jscape/inet/util/f;->f:Ljava/io/InputStream;

    invoke-virtual {p3}, Ljava/io/InputStream;->read()I

    move-result p3
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_e
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_a

    if-eqz v1, :cond_9

    if-eqz v1, :cond_8

    if-eq p3, v0, :cond_7

    int-to-char v2, p3

    :try_start_8
    invoke-virtual {p2, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_e

    if-nez v1, :cond_6

    goto :goto_3

    :catch_2
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/util/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_7
    :goto_3
    iget-object p3, p0, Lcom/jscape/inet/util/f;->f:Ljava/io/InputStream;

    invoke-virtual {p3}, Ljava/io/InputStream;->read()I

    move-result p3

    if-eqz v1, :cond_9

    move v2, p5

    goto :goto_4

    :cond_8
    move v2, v0

    :goto_4
    if-eq p3, v2, :cond_9

    goto :goto_2

    :cond_9
    if-ltz p3, :cond_12

    invoke-virtual {p2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p2

    sget-object v2, Lcom/jscape/inet/util/f;->k:[Ljava/lang/String;

    aget-object v2, v2, p4
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_e
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_a

    const/4 v3, -0x1

    const/16 v4, 0x20

    :try_start_a
    invoke-virtual {p2, v4}, Ljava/lang/String;->indexOf(I)I

    move-result p3

    add-int/lit8 v5, p3, 0x1

    invoke-virtual {p2, v4, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    invoke-virtual {p2, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    add-int/2addr v4, p4

    invoke-virtual {p2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_e

    :catch_3
    if-eqz v1, :cond_b

    const/16 p2, 0xc8

    if-ne v3, p2, :cond_a

    goto :goto_5

    :cond_a
    :try_start_b
    new-instance p1, Ljava/io/IOException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p3, Lcom/jscape/inet/util/f;->k:[Ljava/lang/String;

    const/4 p4, 0x4

    aget-object p3, p3, p4

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_e

    :catch_4
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/inet/util/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_b
    :goto_5
    move p2, p1

    :cond_c
    :goto_6
    if-ltz p3, :cond_f

    iget-object p3, p0, Lcom/jscape/inet/util/f;->f:Ljava/io/InputStream;

    invoke-virtual {p3}, Ljava/io/InputStream;->read()I

    move-result p3

    if-eqz v1, :cond_f

    if-eqz v1, :cond_e

    if-eq p3, v0, :cond_d

    add-int/lit8 p2, p2, 0x1

    if-nez v1, :cond_c

    :cond_d
    iget-object p3, p0, Lcom/jscape/inet/util/f;->f:Ljava/io/InputStream;

    invoke-virtual {p3}, Ljava/io/InputStream;->read()I

    move-result p3
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_e
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_a

    if-eqz v1, :cond_f

    move p4, p5

    goto :goto_7

    :cond_e
    move p4, v0

    :goto_7
    if-eq p3, p4, :cond_f

    goto :goto_6

    :cond_f
    if-eqz v1, :cond_11

    if-ltz p3, :cond_10

    goto :goto_8

    :cond_10
    :try_start_d
    new-instance p1, Ljava/io/IOException;

    invoke-direct {p1}, Ljava/io/IOException;-><init>()V

    throw p1
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_5
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_e

    :catch_5
    move-exception p1

    :try_start_e
    invoke-static {p1}, Lcom/jscape/inet/util/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_e
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_a

    :cond_11
    move p2, p3

    :goto_8
    if-nez p2, :cond_b

    return-void

    :cond_12
    :try_start_f
    new-instance p1, Ljava/io/IOException;

    invoke-direct {p1}, Ljava/io/IOException;-><init>()V

    throw p1
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_6
    .catch Ljava/lang/RuntimeException; {:try_start_f .. :try_end_f} :catch_e

    :catch_6
    move-exception p1

    :try_start_10
    invoke-static {p1}, Lcom/jscape/inet/util/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_e
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_a

    :catch_7
    move-exception p1

    :try_start_11
    invoke-static {p1}, Lcom/jscape/inet/util/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_8
    .catch Ljava/lang/RuntimeException; {:try_start_11 .. :try_end_11} :catch_e

    :catch_8
    move-exception p1

    :try_start_12
    invoke-static {p1}, Lcom/jscape/inet/util/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_9
    .catch Ljava/lang/RuntimeException; {:try_start_12 .. :try_end_12} :catch_e

    :catch_9
    move-exception p1

    :try_start_13
    invoke-static {p1}, Lcom/jscape/inet/util/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_13
    .catch Ljava/lang/RuntimeException; {:try_start_13 .. :try_end_13} :catch_e
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_a

    :catch_a
    move-exception p1

    :try_start_14
    iget-object p2, p0, Lcom/jscape/inet/util/f;->h:Ljava/net/Socket;
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_b

    if-eqz v1, :cond_13

    if-eqz p2, :cond_14

    :try_start_15
    iget-object p2, p0, Lcom/jscape/inet/util/f;->h:Ljava/net/Socket;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_c

    :cond_13
    :try_start_16
    invoke-virtual {p2}, Ljava/net/Socket;->close()V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_d

    goto :goto_9

    :catch_b
    move-exception p2

    :try_start_17
    invoke-static {p2}, Lcom/jscape/inet/util/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p2

    throw p2
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_c

    :catch_c
    move-exception p2

    :try_start_18
    invoke-static {p2}, Lcom/jscape/inet/util/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p2

    throw p2
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_d

    :catch_d
    :cond_14
    :goto_9
    new-instance p2, Ljava/lang/Exception;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p4, Lcom/jscape/inet/util/f;->k:[Ljava/lang/String;

    const/4 p5, 0x3

    aget-object p4, p4, p5

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p2

    :catch_e
    move-exception p1

    throw p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/util/f;->i:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/util/f;->j:Ljava/lang/String;

    return-void
.end method

.method public b()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/f;->g:Ljava/io/OutputStream;

    return-object v0
.end method

.method public c()Ljava/net/Socket;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/f;->h:Ljava/net/Socket;

    return-object v0
.end method

.method public d()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/util/f;->f:Ljava/io/InputStream;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jscape/inet/util/f;->f:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/inet/util/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/util/f;->g:Ljava/io/OutputStream;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/util/f;->g:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/util/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    :cond_1
    :goto_1
    :try_start_4
    iget-object v1, p0, Lcom/jscape/inet/util/f;->h:Ljava/net/Socket;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    if-eqz v0, :cond_2

    if-eqz v1, :cond_3

    :try_start_5
    iget-object v1, p0, Lcom/jscape/inet/util/f;->h:Ljava/net/Socket;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    :cond_2
    :try_start_6
    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_2

    :catch_2
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/inet/util/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    :catch_3
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/inet/util/f;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    :cond_3
    :goto_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/util/f;->f:Ljava/io/InputStream;

    iput-object v0, p0, Lcom/jscape/inet/util/f;->g:Ljava/io/OutputStream;

    iput-object v0, p0, Lcom/jscape/inet/util/f;->h:Ljava/net/Socket;

    return-void
.end method
