.class public Lcom/jscape/inet/util/b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/util/c;


# static fields
.field private static b:[C

.field private static c:[B

.field private static final d:[Ljava/lang/String;


# instance fields
.field a:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, -0x1

    const/16 v3, 0x41

    const/4 v4, 0x0

    move v5, v2

    move v6, v3

    move v7, v4

    :goto_0
    const/16 v8, 0x12

    const/4 v9, 0x1

    add-int/2addr v5, v9

    add-int/2addr v6, v5

    const-string v10, "s(JU\u001aAdz#CZ\u0013Jm}:XC\u000cSvd=QH\u0005fAQ\u000elw8oJX\u0001e|1hSC\u0018ze*qTJ\u0013s!n5\u0010\u0006_?&g>\u0008\u001dW!\u007f\u0003zr>k@G\u0006he:c\u0003V\u000b}p\u007fkF\\\r}y\u007f/T@\u0005}t\u007f\u000c\u0012\u0003gb+bBVJfw\u007f"

    invoke-virtual {v10, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    array-length v11, v5

    move v12, v4

    :goto_1
    if-gt v11, v12, :cond_5

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v5}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v8, v7, 0x1

    aput-object v5, v1, v7

    const/16 v5, 0x70

    if-ge v6, v5, :cond_0

    invoke-virtual {v10, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v8

    move/from16 v16, v6

    move v6, v5

    move/from16 v5, v16

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/util/b;->d:[Ljava/lang/String;

    aget-object v0, v1, v4

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/util/b;->b:[C

    const/16 v0, 0x100

    new-array v1, v0, [B

    sput-object v1, Lcom/jscape/inet/util/b;->c:[B

    :goto_2
    if-ge v4, v0, :cond_1

    sget-object v1, Lcom/jscape/inet/util/b;->c:[B

    aput-byte v2, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_1
    :goto_3
    const/16 v0, 0x5a

    if-gt v3, v0, :cond_2

    sget-object v0, Lcom/jscape/inet/util/b;->c:[B

    add-int/lit8 v1, v3, -0x41

    int-to-byte v1, v1

    aput-byte v1, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_2
    const/16 v0, 0x61

    move v1, v0

    :goto_4
    const/16 v2, 0x7a

    if-gt v1, v2, :cond_3

    sget-object v2, Lcom/jscape/inet/util/b;->c:[B

    add-int/lit8 v3, v1, 0x1a

    sub-int/2addr v3, v0

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    const/16 v0, 0x30

    move v1, v0

    :goto_5
    const/16 v2, 0x39

    if-gt v1, v2, :cond_4

    sget-object v2, Lcom/jscape/inet/util/b;->c:[B

    add-int/lit8 v3, v1, 0x34

    sub-int/2addr v3, v0

    int-to-byte v3, v3

    aput-byte v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_4
    sget-object v0, Lcom/jscape/inet/util/b;->c:[B

    const/16 v1, 0x2b

    const/16 v2, 0x3e

    aput-byte v2, v0, v1

    const/16 v1, 0x2f

    const/16 v2, 0x3f

    aput-byte v2, v0, v1

    return-void

    :cond_5
    aget-char v13, v5, v12

    rem-int/lit8 v14, v12, 0x7

    if-eqz v14, :cond_b

    if-eq v14, v9, :cond_a

    const/4 v15, 0x2

    if-eq v14, v15, :cond_9

    if-eq v14, v0, :cond_8

    const/4 v15, 0x4

    if-eq v14, v15, :cond_7

    const/4 v15, 0x5

    if-eq v14, v15, :cond_6

    const/16 v14, 0x31

    goto :goto_6

    :cond_6
    const/16 v14, 0x15

    goto :goto_6

    :cond_7
    const/16 v14, 0x4d

    goto :goto_6

    :cond_8
    move v14, v0

    goto :goto_6

    :cond_9
    const/16 v14, 0x1b

    goto :goto_6

    :cond_a
    const/16 v14, 0x78

    goto :goto_6

    :cond_b
    const/16 v14, 0x20

    :goto_6
    xor-int/2addr v14, v8

    xor-int/2addr v13, v14

    int-to-char v13, v13

    aput-char v13, v5, v12

    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x48

    iput v0, p0, Lcom/jscape/inet/util/b;->a:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/util/b;->a:I

    return v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/util/b;->a:I

    return-void
.end method

.method public a([C)[B
    .locals 11

    array-length v0, p1

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move v3, v2

    :cond_0
    array-length v4, p1

    const/4 v5, 0x3

    const/16 v6, 0xff

    if-ge v3, v4, :cond_5

    aget-char v4, p1, v3

    if-eqz v1, :cond_2

    if-eqz v1, :cond_1

    if-gt v4, v6, :cond_3

    sget-object v4, Lcom/jscape/inet/util/b;->c:[B

    aget-char v7, p1, v3

    aget-byte v4, v4, v7

    goto :goto_0

    :cond_1
    move v5, v6

    goto :goto_1

    :cond_2
    :goto_0
    if-gez v4, :cond_4

    :cond_3
    add-int/lit8 v0, v0, -0x1

    :cond_4
    add-int/lit8 v3, v3, 0x1

    if-nez v1, :cond_0

    :cond_5
    div-int/lit8 v3, v0, 0x4

    mul-int/2addr v3, v5

    rem-int/lit8 v4, v0, 0x4

    :goto_1
    const/4 v7, 0x2

    if-eqz v1, :cond_7

    if-ne v4, v5, :cond_6

    add-int/lit8 v3, v3, 0x2

    :cond_6
    rem-int/lit8 v4, v0, 0x4

    if-eqz v1, :cond_9

    move v5, v7

    :cond_7
    if-ne v4, v5, :cond_8

    add-int/lit8 v3, v3, 0x1

    :cond_8
    move v4, v3

    :cond_9
    new-array v0, v4, [B

    move v3, v2

    move v5, v3

    move v8, v5

    :cond_a
    array-length v9, p1

    if-ge v2, v9, :cond_f

    aget-char v9, p1, v2

    if-eqz v1, :cond_c

    if-eqz v1, :cond_10

    if-le v9, v6, :cond_b

    const/4 v9, -0x1

    goto :goto_2

    :cond_b
    sget-object v9, Lcom/jscape/inet/util/b;->c:[B

    aget-char v10, p1, v2

    aget-byte v9, v9, v10

    :cond_c
    :goto_2
    if-eqz v1, :cond_e

    if-ltz v9, :cond_d

    shl-int/lit8 v5, v5, 0x6

    add-int/lit8 v8, v8, 0x6

    or-int/2addr v5, v9

    if-eqz v1, :cond_e

    const/16 v9, 0x8

    if-lt v8, v9, :cond_d

    add-int/lit8 v8, v8, -0x8

    add-int/lit8 v9, v3, 0x1

    shr-int v10, v5, v8

    and-int/2addr v10, v6

    int-to-byte v10, v10

    aput-byte v10, v0, v3

    move v3, v9

    :cond_d
    add-int/lit8 v2, v2, 0x1

    :cond_e
    if-nez v1, :cond_a

    :cond_f
    move v9, v3

    move v6, v4

    :cond_10
    if-ne v9, v6, :cond_11

    return-object v0

    :cond_11
    new-instance p1, Ljava/lang/Error;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/util/b;->d:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    aget-object v1, v1, v7

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a([B)[C
    .locals 13

    array-length v0, p1

    add-int/lit8 v0, v0, 0x2

    div-int/lit8 v0, v0, 0x3

    mul-int/lit8 v0, v0, 0x6

    new-array v0, v0, [C

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move v3, v2

    move v4, v3

    move v5, v4

    :cond_0
    array-length v6, p1

    if-ge v3, v6, :cond_b

    aget-byte v6, p1, v3

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    add-int/lit8 v7, v3, 0x1

    if-eqz v1, :cond_c

    array-length v8, p1

    const/4 v9, 0x1

    if-eqz v1, :cond_2

    if-ge v7, v8, :cond_1

    aget-byte v7, p1, v7

    and-int/lit16 v7, v7, 0xff

    or-int/2addr v6, v7

    move v7, v9

    goto :goto_0

    :cond_1
    move v7, v2

    :goto_0
    shl-int/lit8 v6, v6, 0x8

    add-int/lit8 v8, v3, 0x2

    array-length v10, p1

    move v12, v8

    move v8, v7

    move v7, v12

    goto :goto_1

    :cond_2
    move v10, v8

    move v8, v2

    :goto_1
    if-eqz v1, :cond_4

    if-ge v7, v10, :cond_3

    add-int/lit8 v7, v3, 0x2

    aget-byte v7, p1, v7

    and-int/lit16 v10, v7, 0xff

    goto :goto_2

    :cond_3
    move v9, v2

    goto :goto_3

    :cond_4
    move v6, v7

    :goto_2
    or-int/2addr v6, v10

    :goto_3
    add-int/lit8 v7, v4, 0x3

    sget-object v10, Lcom/jscape/inet/util/b;->b:[C

    const/16 v11, 0x40

    if-eqz v1, :cond_6

    if-eqz v9, :cond_5

    and-int/lit8 v9, v6, 0x3f

    goto :goto_4

    :cond_5
    move v9, v11

    :cond_6
    :goto_4
    aget-char v9, v10, v9

    aput-char v9, v0, v7

    shr-int/lit8 v6, v6, 0x6

    add-int/lit8 v7, v4, 0x2

    sget-object v9, Lcom/jscape/inet/util/b;->b:[C

    if-eqz v1, :cond_7

    if-eqz v8, :cond_8

    and-int/lit8 v8, v6, 0x3f

    :cond_7
    move v11, v8

    :cond_8
    aget-char v8, v9, v11

    aput-char v8, v0, v7

    shr-int/lit8 v6, v6, 0x6

    add-int/lit8 v8, v4, 0x1

    sget-object v9, Lcom/jscape/inet/util/b;->b:[C

    and-int/lit8 v10, v6, 0x3f

    aget-char v10, v9, v10

    aput-char v10, v0, v8

    shr-int/lit8 v6, v6, 0x6

    add-int/lit8 v8, v4, 0x0

    and-int/lit8 v6, v6, 0x3f

    aget-char v6, v9, v6

    aput-char v6, v0, v8

    add-int/lit8 v5, v5, 0x4

    if-eqz v1, :cond_a

    iget v6, p0, Lcom/jscape/inet/util/b;->a:I

    if-ne v5, v6, :cond_9

    add-int/lit8 v5, v4, 0x4

    const/16 v6, 0xd

    aput-char v6, v0, v5

    add-int/lit8 v4, v4, 0x5

    const/16 v5, 0xa

    aput-char v5, v0, v4

    move v5, v2

    move v4, v7

    :cond_9
    add-int/lit8 v3, v3, 0x3

    add-int/lit8 v4, v4, 0x4

    :cond_a
    if-nez v1, :cond_0

    :cond_b
    move v7, v4

    :cond_c
    new-array p1, v7, [C

    invoke-static {v0, v2, p1, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p1
.end method
