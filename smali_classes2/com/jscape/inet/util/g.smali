.class public Lcom/jscape/inet/util/g;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/util/e;


# static fields
.field private static final a:I = 0x438

.field private static final k:[Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Ljava/io/InputStream;

.field private g:Ljava/io/OutputStream;

.field private h:Ljava/net/Socket;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x14

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x67

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "]S1ZZ{S\u001ba\u0017u1A\u0008\u001bB*Y\u0002k\u001ck@7N\u0003Arxy\u000b\u0003@2N^@.S\u00082O^F-D\u0014a\u001d\rk@7N\u0003Arxy\u000b\u0003@2"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    const/16 v11, 0x3f

    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    if-ge v3, v11, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/util/g;->k:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v10

    rem-int/lit8 v13, v10, 0x7

    if-eqz v13, :cond_6

    if-eq v13, v7, :cond_5

    const/4 v14, 0x2

    if-eq v13, v14, :cond_7

    if-eq v13, v0, :cond_4

    const/4 v11, 0x4

    if-eq v13, v11, :cond_3

    const/4 v11, 0x5

    if-eq v13, v11, :cond_2

    const/16 v11, 0x5a

    goto :goto_2

    :cond_2
    const/16 v11, 0x75

    goto :goto_2

    :cond_3
    const/16 v11, 0x1d

    goto :goto_2

    :cond_4
    const/16 v11, 0x51

    goto :goto_2

    :cond_5
    const/16 v11, 0x55

    goto :goto_2

    :cond_6
    const/16 v11, 0x5c

    :cond_7
    :goto_2
    xor-int/2addr v11, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/16 v0, 0x3a

    :try_start_0
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v2, -0x1

    const/16 v3, 0x438

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-object p1, v1

    :catch_1
    :cond_0
    iput-object p1, p0, Lcom/jscape/inet/util/g;->b:Ljava/lang/String;

    iput v3, p0, Lcom/jscape/inet/util/g;->c:I

    goto :goto_0

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/util/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/util/g;->b:Ljava/lang/String;

    iput p2, p0, Lcom/jscape/inet/util/g;->c:I

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static e()I
    .locals 1

    const/16 v0, 0x438

    return v0
.end method


# virtual methods
.method public a()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/g;->f:Ljava/io/InputStream;

    return-object v0
.end method

.method public a(Lcom/jscape/inet/util/j;Ljava/lang/String;IJ)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iput-object p2, p0, Lcom/jscape/inet/util/g;->d:Ljava/lang/String;

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    iput p3, p0, Lcom/jscape/inet/util/g;->e:I

    const/4 v1, 0x4

    const/4 v2, 0x2

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    :try_start_0
    iget-object v3, p0, Lcom/jscape/inet/util/g;->b:Ljava/lang/String;

    iget v4, p0, Lcom/jscape/inet/util/g;->c:I

    invoke-static {v3, v4, p4, p5}, Lcom/jscape/inet/util/l;->a(Ljava/lang/String;IJ)Ljava/net/Socket;

    move-result-object v3

    iput-object v3, p0, Lcom/jscape/inet/util/g;->h:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    iput-object v3, p0, Lcom/jscape/inet/util/g;->f:Ljava/io/InputStream;

    iget-object v3, p0, Lcom/jscape/inet/util/g;->h:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    iput-object v3, p0, Lcom/jscape/inet/util/g;->g:Ljava/io/OutputStream;

    if-nez v0, :cond_2

    new-array v3, v1, [I

    invoke-static {v3}, Lcom/jscape/util/aq;->b([I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_f

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/util/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/jscape/inet/util/g;->b:Ljava/lang/String;

    iget v4, p0, Lcom/jscape/inet/util/g;->c:I

    invoke-interface {p1, v3, v4}, Lcom/jscape/inet/util/j;->a(Ljava/lang/String;I)Ljava/net/Socket;

    move-result-object v3

    iput-object v3, p0, Lcom/jscape/inet/util/g;->h:Ljava/net/Socket;

    invoke-interface {p1, v3}, Lcom/jscape/inet/util/j;->a(Ljava/net/Socket;)Ljava/io/InputStream;

    move-result-object v3

    iput-object v3, p0, Lcom/jscape/inet/util/g;->f:Ljava/io/InputStream;

    :cond_1
    iget-object v3, p0, Lcom/jscape/inet/util/g;->h:Ljava/net/Socket;

    invoke-interface {p1, v3}, Lcom/jscape/inet/util/j;->b(Ljava/net/Socket;)Ljava/io/OutputStream;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/util/g;->g:Ljava/io/OutputStream;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_f
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_b

    :cond_2
    const-wide/16 v3, 0x0

    cmp-long p1, p4, v3

    const/4 v3, 0x1

    if-eqz v0, :cond_4

    if-lez p1, :cond_3

    :try_start_2
    iget-object p1, p0, Lcom/jscape/inet/util/g;->h:Ljava/net/Socket;

    long-to-int p4, p4

    invoke-virtual {p1, p4}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_f

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/util/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/jscape/inet/util/g;->h:Ljava/net/Socket;

    invoke-virtual {p1, v3}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    const/16 p1, 0x400

    :cond_4
    new-array p1, p1, [B

    const/4 p4, 0x5

    const/4 p5, 0x0

    aput-byte p4, p1, p5

    aput-byte v2, p1, v3

    aput-byte p5, p1, v2

    const/4 v4, 0x3

    aput-byte v2, p1, v4

    iget-object v5, p0, Lcom/jscape/inet/util/g;->g:Ljava/io/OutputStream;

    invoke-virtual {v5, p1, p5, v1}, Ljava/io/OutputStream;->write([BII)V

    iget-object v5, p0, Lcom/jscape/inet/util/g;->f:Ljava/io/InputStream;

    invoke-virtual {v5, p1, p5, v2}, Ljava/io/InputStream;->read([BII)I
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_f
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_b

    :try_start_4
    aget-byte v5, p1, v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_a
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_f

    and-int/lit16 v5, v5, 0xff

    if-eqz v0, :cond_c

    if-eqz v5, :cond_6

    if-eq v5, v2, :cond_5

    move v5, p5

    goto/16 :goto_5

    :cond_5
    move v5, p5

    goto :goto_2

    :cond_6
    if-nez v0, :cond_b

    move v5, v3

    :goto_2
    :try_start_5
    iget-object v6, p0, Lcom/jscape/inet/util/g;->i:Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_f

    if-eqz v0, :cond_7

    if-eqz v6, :cond_c

    :try_start_6
    iget-object v6, p0, Lcom/jscape/inet/util/g;->j:Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_f

    :cond_7
    if-eqz v0, :cond_9

    if-nez v6, :cond_8

    goto :goto_5

    :cond_8
    :try_start_7
    aput-byte v3, p1, p5

    iget-object v6, p0, Lcom/jscape/inet/util/g;->i:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, p1, v3

    iget-object v6, p0, Lcom/jscape/inet/util/g;->i:Ljava/lang/String;

    sget-object v7, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v6, v7}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v6

    iget-object v7, p0, Lcom/jscape/inet/util/g;->i:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-static {v6, p5, p1, v2, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v6, p0, Lcom/jscape/inet/util/g;->i:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v2

    add-int/lit8 v7, v6, 0x1

    iget-object v8, p0, Lcom/jscape/inet/util/g;->j:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    int-to-byte v8, v8

    aput-byte v8, p1, v6

    iget-object v6, p0, Lcom/jscape/inet/util/g;->j:Ljava/lang/String;

    goto :goto_3

    :cond_9
    move v7, v1

    :goto_3
    sget-object v8, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v6, v8}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v6

    iget-object v8, p0, Lcom/jscape/inet/util/g;->j:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-static {v6, p5, p1, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v6, p0, Lcom/jscape/inet/util/g;->j:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_f
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_b

    add-int/2addr v7, v6

    :try_start_8
    iget-object v6, p0, Lcom/jscape/inet/util/g;->g:Ljava/io/OutputStream;

    invoke-virtual {v6, p1, p5, v7}, Ljava/io/OutputStream;->write([BII)V

    iget-object v6, p0, Lcom/jscape/inet/util/g;->f:Ljava/io/InputStream;

    invoke-virtual {v6, p1, p5, v2}, Ljava/io/InputStream;->read([BII)I

    aget-byte v6, p1, v3
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_f

    if-eqz v0, :cond_a

    if-nez v6, :cond_c

    goto :goto_4

    :cond_a
    move v5, v6

    goto :goto_5

    :catch_2
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/util/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_f
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_b

    :catch_3
    move-exception p1

    :try_start_a
    invoke-static {p1}, Lcom/jscape/inet/util/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_f

    :catch_4
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/inet/util/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_f
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_b

    :cond_b
    :goto_4
    move v5, v3

    :cond_c
    :goto_5
    if-eqz v0, :cond_e

    if-eqz v5, :cond_d

    move v5, p5

    goto :goto_6

    :cond_d
    :try_start_c
    iget-object p1, p0, Lcom/jscape/inet/util/g;->h:Ljava/net/Socket;

    invoke-virtual {p1}, Ljava/net/Socket;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_f

    :catch_5
    :try_start_d
    new-instance p1, Ljava/lang/Exception;

    sget-object p2, Lcom/jscape/inet/util/g;->k:[Ljava/lang/String;

    aget-object p2, p2, p5

    invoke-direct {p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_e
    :goto_6
    add-int/lit8 v6, v5, 0x1

    aput-byte p4, p1, v5

    add-int/lit8 p4, v6, 0x1

    aput-byte v3, p1, v6

    add-int/lit8 v5, p4, 0x1

    aput-byte p5, p1, p4

    sget-object p4, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p2, p4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p2

    array-length p4, p2

    add-int/lit8 v6, v5, 0x1

    aput-byte v4, p1, v5

    add-int/lit8 v5, v6, 0x1

    int-to-byte v7, p4

    aput-byte v7, p1, v6

    invoke-static {p2, p5, p1, v5, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_d
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_f
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_b

    add-int/2addr v5, p4

    add-int/lit8 p2, v5, 0x1

    ushr-int/lit8 p4, p3, 0x8

    int-to-byte p4, p4

    :try_start_e
    aput-byte p4, p1, v5

    add-int/lit8 p4, p2, 0x1

    and-int/lit16 p3, p3, 0xff

    int-to-byte p3, p3

    aput-byte p3, p1, p2

    iget-object p2, p0, Lcom/jscape/inet/util/g;->g:Ljava/io/OutputStream;

    invoke-virtual {p2, p1, p5, p4}, Ljava/io/OutputStream;->write([BII)V

    iget-object p2, p0, Lcom/jscape/inet/util/g;->f:Ljava/io/InputStream;

    invoke-virtual {p2, p1, p5, v1}, Ljava/io/InputStream;->read([BII)I

    aget-byte p2, p1, v3
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_9
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_f

    if-eqz v0, :cond_10

    if-nez p2, :cond_f

    :try_start_f
    aget-byte p2, p1, v4
    :try_end_f
    .catch Ljava/lang/RuntimeException; {:try_start_f .. :try_end_f} :catch_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_b

    and-int/lit16 p2, p2, 0xff

    goto :goto_7

    :cond_f
    :try_start_10
    iget-object p2, p0, Lcom/jscape/inet/util/g;->h:Ljava/net/Socket;

    invoke-virtual {p2}, Ljava/net/Socket;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_6
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_f

    :catch_6
    :try_start_11
    new-instance p2, Ljava/lang/Exception;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p4, Lcom/jscape/inet/util/g;->k:[Ljava/lang/String;

    aget-object p4, p4, v3

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-byte p1, p1, v3

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_11
    .catch Ljava/lang/RuntimeException; {:try_start_11 .. :try_end_11} :catch_f
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_b

    :cond_10
    :goto_7
    if-eqz v0, :cond_12

    if-eq p2, v3, :cond_11

    if-eq p2, v4, :cond_13

    if-eq p2, v1, :cond_14

    goto :goto_9

    :cond_11
    :try_start_12
    iget-object p2, p0, Lcom/jscape/inet/util/g;->f:Ljava/io/InputStream;

    const/4 p3, 0x6

    invoke-virtual {p2, p1, p5, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_7
    .catch Ljava/lang/RuntimeException; {:try_start_12 .. :try_end_12} :catch_f

    goto :goto_8

    :catch_7
    move-exception p1

    :try_start_13
    invoke-static {p1}, Lcom/jscape/inet/util/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_13
    .catch Ljava/lang/RuntimeException; {:try_start_13 .. :try_end_13} :catch_f
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_b

    :cond_12
    :goto_8
    if-nez v0, :cond_15

    :cond_13
    :try_start_14
    iget-object p2, p0, Lcom/jscape/inet/util/g;->f:Ljava/io/InputStream;

    invoke-virtual {p2, p1, p5, v3}, Ljava/io/InputStream;->read([BII)I

    iget-object p2, p0, Lcom/jscape/inet/util/g;->f:Ljava/io/InputStream;

    aget-byte p3, p1, p5

    add-int/2addr p3, v2

    invoke-virtual {p2, p1, p5, p3}, Ljava/io/InputStream;->read([BII)I

    if-nez v0, :cond_15

    :cond_14
    iget-object p2, p0, Lcom/jscape/inet/util/g;->f:Ljava/io/InputStream;

    const/16 p3, 0x12

    invoke-virtual {p2, p1, p5, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_8
    .catch Ljava/lang/RuntimeException; {:try_start_14 .. :try_end_14} :catch_f

    goto :goto_9

    :catch_8
    move-exception p1

    :try_start_15
    invoke-static {p1}, Lcom/jscape/inet/util/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_15
    :goto_9
    return-void

    :catch_9
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/util/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :catch_a
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/util/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_15
    .catch Ljava/lang/RuntimeException; {:try_start_15 .. :try_end_15} :catch_f
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_b

    :catch_b
    move-exception p1

    :try_start_16
    iget-object p2, p0, Lcom/jscape/inet/util/g;->h:Ljava/net/Socket;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_c

    if-eqz v0, :cond_16

    if-eqz p2, :cond_17

    :try_start_17
    iget-object p2, p0, Lcom/jscape/inet/util/g;->h:Ljava/net/Socket;
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_d

    :cond_16
    :try_start_18
    invoke-virtual {p2}, Ljava/net/Socket;->close()V
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_e

    goto :goto_a

    :catch_c
    move-exception p2

    :try_start_19
    invoke-static {p2}, Lcom/jscape/inet/util/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p2

    throw p2
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_d

    :catch_d
    move-exception p2

    :try_start_1a
    invoke-static {p2}, Lcom/jscape/inet/util/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p2

    throw p2
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_e

    :catch_e
    :cond_17
    :goto_a
    new-instance p2, Ljava/lang/Exception;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p4, Lcom/jscape/inet/util/g;->k:[Ljava/lang/String;

    aget-object p4, p4, v2

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw p2

    :catch_f
    move-exception p1

    throw p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/util/g;->i:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/util/g;->j:Ljava/lang/String;

    return-void
.end method

.method public b()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/g;->g:Ljava/io/OutputStream;

    return-object v0
.end method

.method public c()Ljava/net/Socket;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/util/g;->h:Ljava/net/Socket;

    return-object v0
.end method

.method public d()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/util/i;->b()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/util/g;->f:Ljava/io/InputStream;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jscape/inet/util/g;->f:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/inet/util/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/util/g;->g:Ljava/io/OutputStream;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/util/g;->g:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/util/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    :cond_1
    :goto_1
    :try_start_4
    iget-object v1, p0, Lcom/jscape/inet/util/g;->h:Ljava/net/Socket;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    if-eqz v0, :cond_2

    if-eqz v1, :cond_3

    :try_start_5
    iget-object v1, p0, Lcom/jscape/inet/util/g;->h:Ljava/net/Socket;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    :cond_2
    :try_start_6
    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_2

    :catch_2
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/inet/util/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    :catch_3
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/inet/util/g;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    :cond_3
    :goto_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/util/g;->f:Ljava/io/InputStream;

    iput-object v0, p0, Lcom/jscape/inet/util/g;->g:Ljava/io/OutputStream;

    iput-object v0, p0, Lcom/jscape/inet/util/g;->h:Ljava/net/Socket;

    return-void
.end method
