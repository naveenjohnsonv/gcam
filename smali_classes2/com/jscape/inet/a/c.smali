.class public Lcom/jscape/inet/a/c;
.super Lcom/jscape/util/n/b;


# static fields
.field private static a:[Lcom/jscape/util/aq; = null

.field private static final serialVersionUID:J = 0x38cfa6558cd32b19L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/a/c;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/jscape/util/aq;

    invoke-static {v0}, Lcom/jscape/inet/a/c;->b([Lcom/jscape/util/aq;)V

    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/util/n/b;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/util/n/b;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/util/n/b;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0, p1}, Lcom/jscape/util/n/b;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static a()[Lcom/jscape/util/aq;
    .locals 1

    sget-object v0, Lcom/jscape/inet/a/c;->a:[Lcom/jscape/util/aq;

    return-object v0
.end method

.method public static b(Ljava/lang/Throwable;)Lcom/jscape/inet/a/c;
    .locals 1

    invoke-static {}, Lcom/jscape/inet/a/c;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    if-nez v0, :cond_1

    instance-of v0, p0, Lcom/jscape/inet/a/c;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/jscape/inet/a/c;

    invoke-direct {v0, p0}, Lcom/jscape/inet/a/c;-><init>(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    :goto_0
    move-object v0, p0

    check-cast v0, Lcom/jscape/inet/a/c;

    :goto_1
    return-object v0
.end method

.method public static b([Lcom/jscape/util/aq;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/a/c;->a:[Lcom/jscape/util/aq;

    return-void
.end method
