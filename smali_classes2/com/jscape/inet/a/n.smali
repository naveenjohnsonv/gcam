.class public Lcom/jscape/inet/a/n;
.super Lcom/jscape/util/n/c;

# interfaces
.implements Lcom/jscape/inet/a/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/util/n/c<",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/jscape/inet/a/k;"
    }
.end annotation


# static fields
.field private static final D:[Ljava/lang/String;

.field private static final c:I = 0x2

.field private static final d:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/a/a/b/n;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Lcom/jscape/inet/a/a/f;

.field private static final f:Lcom/jscape/inet/a/a/c/b;

.field private static final g:Lcom/jscape/inet/a/a/c/g;

.field private static final h:Lcom/jscape/util/h/C;

.field private static final i:Lcom/jscape/util/Time;

.field private static final j:I = 0x2000


# instance fields
.field private A:I

.field private volatile B:Lcom/jscape/inet/a/a/c/a/E;

.field private final k:Lcom/jscape/util/k/a/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/k/a/v<",
            "Lcom/jscape/util/k/a/C;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcom/jscape/util/k/a/s;

.field private final m:Lcom/jscape/inet/a/a/f;

.field private final n:Ljava/lang/String;

.field private final o:I

.field private final p:Lcom/jscape/inet/a/a/c/g;

.field private final q:Lcom/jscape/util/Time;

.field private final r:Lcom/jscape/inet/a/a/c/b;

.field private final s:Lcom/jscape/util/h/C;

.field private final t:Lcom/jscape/util/A;

.field private final u:Lcom/jscape/util/A;

.field private final v:Z

.field private final w:Ljava/util/logging/Logger;

.field private x:Lcom/jscape/util/k/a/B;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/k/a/B<",
            "Lcom/jscape/inet/a/a/b/n;",
            ">;"
        }
    .end annotation
.end field

.field private y:Lcom/jscape/inet/a/a/c/f;

.field private z:Lcom/jscape/util/h/T;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "]f\"+eqYu3\u0017\n^H\u0017\u007fv%?k\u007fR2r\"lQ=D2/{r*=DO)viy6%SU\u0002\u001c*{X|}3/~qX|35>oyCwwllQ=D2/{r*=DO=$SU\u0002\u001c*{X|}3/~qX|35 ekRv)v\u0017/k\u0017.>hl/kj<)[}5#gqYu3\u0017\n^H\u0017\u007fv%?k\u007fR2r\"lQ=D2/{r*=DO)viy6"

    const/16 v4, 0x9e

    const/16 v5, 0x29

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x21

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x2e

    const/16 v3, 0x1d

    const-string v5, "\u0014.`g).\u00075%a?0%\u0007.-lq>k\t$3v~>.^a\u0010\u000e0`m8?\r..%z+9\u000b3n"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x72

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/a/n;->D:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/a/a/a/p;

    new-array v1, v2, [Lcom/jscape/inet/a/a/a/q;

    invoke-direct {v0, v1}, Lcom/jscape/inet/a/a/a/p;-><init>([Lcom/jscape/inet/a/a/a/q;)V

    invoke-static {v0}, Lcom/jscape/inet/a/a/a/r;->a(Lcom/jscape/inet/a/a/a/p;)Lcom/jscape/inet/a/a/a/p;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/n;->d:Lcom/jscape/util/h/I;

    new-instance v0, Lcom/jscape/inet/a/a/e;

    invoke-direct {v0}, Lcom/jscape/inet/a/a/e;-><init>()V

    sput-object v0, Lcom/jscape/inet/a/n;->e:Lcom/jscape/inet/a/a/f;

    new-instance v0, Lcom/jscape/inet/a/a/c/c;

    invoke-direct {v0}, Lcom/jscape/inet/a/a/c/c;-><init>()V

    sput-object v0, Lcom/jscape/inet/a/n;->f:Lcom/jscape/inet/a/a/c/b;

    invoke-static {}, Lcom/jscape/inet/a/a/c/e;->a()Lcom/jscape/inet/a/a/c/e;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/n;->g:Lcom/jscape/inet/a/a/c/g;

    new-instance v0, Lcom/jscape/util/h/W;

    invoke-direct {v0}, Lcom/jscape/util/h/W;-><init>()V

    sput-object v0, Lcom/jscape/inet/a/n;->h:Lcom/jscape/util/h/C;

    const-wide/16 v0, 0x1e

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->seconds(J)Lcom/jscape/util/Time;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/n;->i:Lcom/jscape/util/Time;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x16

    goto :goto_4

    :cond_4
    const/16 v1, 0x39

    goto :goto_4

    :cond_5
    const/16 v1, 0x2b

    goto :goto_4

    :cond_6
    const/16 v1, 0x6d

    goto :goto_4

    :cond_7
    const/16 v1, 0x77

    goto :goto_4

    :cond_8
    const/16 v1, 0x32

    goto :goto_4

    :cond_9
    const/16 v1, 0x33

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_2
.end method

.method public constructor <init>(Lcom/jscape/util/k/a/v;Lcom/jscape/util/k/a/s;Lcom/jscape/inet/a/a/f;Ljava/lang/String;ILcom/jscape/inet/a/a/c/g;Lcom/jscape/util/Time;Lcom/jscape/inet/a/a/c/b;Lcom/jscape/util/h/C;Lcom/jscape/util/A;Lcom/jscape/util/A;ZLjava/util/logging/Logger;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/v<",
            "Lcom/jscape/util/k/a/C;",
            ">;",
            "Lcom/jscape/util/k/a/s;",
            "Lcom/jscape/inet/a/a/f;",
            "Ljava/lang/String;",
            "I",
            "Lcom/jscape/inet/a/a/c/g;",
            "Lcom/jscape/util/Time;",
            "Lcom/jscape/inet/a/a/c/b;",
            "Lcom/jscape/util/h/C;",
            "Lcom/jscape/util/A;",
            "Lcom/jscape/util/A;",
            "Z",
            "Ljava/util/logging/Logger;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/util/n/c;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/a/n;->k:Lcom/jscape/util/k/a/v;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/a/n;->l:Lcom/jscape/util/k/a/s;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/a/n;->m:Lcom/jscape/inet/a/a/f;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/a/n;->n:Ljava/lang/String;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput p5, p0, Lcom/jscape/inet/a/n;->o:I

    invoke-static {p6}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p6, p0, Lcom/jscape/inet/a/n;->p:Lcom/jscape/inet/a/a/c/g;

    invoke-static {p7}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/a/c;->a()[Lcom/jscape/util/aq;

    iput-object p7, p0, Lcom/jscape/inet/a/n;->q:Lcom/jscape/util/Time;

    invoke-static {p8}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p8, p0, Lcom/jscape/inet/a/n;->r:Lcom/jscape/inet/a/a/c/b;

    invoke-static {p9}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p9, p0, Lcom/jscape/inet/a/n;->s:Lcom/jscape/util/h/C;

    invoke-static {p10}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p10, p0, Lcom/jscape/inet/a/n;->t:Lcom/jscape/util/A;

    invoke-static {p11}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p11, p0, Lcom/jscape/inet/a/n;->u:Lcom/jscape/util/A;

    iput-boolean p12, p0, Lcom/jscape/inet/a/n;->v:Z

    invoke-static {p13}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p13, p0, Lcom/jscape/inet/a/n;->w:Ljava/util/logging/Logger;

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    new-array p1, p1, [Lcom/jscape/util/aq;

    invoke-static {p1}, Lcom/jscape/inet/a/c;->b([Lcom/jscape/util/aq;)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;JZ)Lcom/jscape/inet/a/a/b/V;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v7, Lcom/jscape/inet/a/a/b/C;

    iget-object v4, p0, Lcom/jscape/inet/a/n;->t:Lcom/jscape/util/A;

    iget-boolean v5, p0, Lcom/jscape/inet/a/n;->v:Z

    move-object v0, v7

    move-object v1, p1

    move-wide v2, p2

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/a/a/b/C;-><init>(Ljava/lang/String;JLcom/jscape/util/A;ZZ)V

    invoke-direct {p0, v7}, Lcom/jscape/inet/a/n;->b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/a/a/b/V;

    return-object p1
.end method

.method public static a(Lcom/jscape/util/k/a/v;Lcom/jscape/util/k/a/s;Ljava/lang/String;ILcom/jscape/util/A;Lcom/jscape/util/A;ZLjava/util/logging/Logger;)Lcom/jscape/inet/a/n;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/v<",
            "Lcom/jscape/util/k/a/C;",
            ">;",
            "Lcom/jscape/util/k/a/s;",
            "Ljava/lang/String;",
            "I",
            "Lcom/jscape/util/A;",
            "Lcom/jscape/util/A;",
            "Z",
            "Ljava/util/logging/Logger;",
            ")",
            "Lcom/jscape/inet/a/n;"
        }
    .end annotation

    new-instance v14, Lcom/jscape/inet/a/n;

    sget-object v3, Lcom/jscape/inet/a/n;->e:Lcom/jscape/inet/a/a/f;

    sget-object v6, Lcom/jscape/inet/a/n;->g:Lcom/jscape/inet/a/a/c/g;

    sget-object v7, Lcom/jscape/inet/a/n;->i:Lcom/jscape/util/Time;

    sget-object v8, Lcom/jscape/inet/a/n;->f:Lcom/jscape/inet/a/a/c/b;

    sget-object v9, Lcom/jscape/inet/a/n;->h:Lcom/jscape/util/h/C;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v4, p2

    move/from16 v5, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move/from16 v12, p6

    move-object/from16 v13, p7

    invoke-direct/range {v0 .. v13}, Lcom/jscape/inet/a/n;-><init>(Lcom/jscape/util/k/a/v;Lcom/jscape/util/k/a/s;Lcom/jscape/inet/a/a/f;Ljava/lang/String;ILcom/jscape/inet/a/a/c/g;Lcom/jscape/util/Time;Lcom/jscape/inet/a/a/c/b;Lcom/jscape/util/h/C;Lcom/jscape/util/A;Lcom/jscape/util/A;ZLjava/util/logging/Logger;)V

    return-object v14
.end method

.method private a(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 2

    new-instance v0, Ljava/io/BufferedInputStream;

    const/16 v1, 0x2000

    invoke-direct {v0, p1, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    return-object v0
.end method

.method private a(Ljava/io/InputStream;Z)Ljava/io/InputStream;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    if-eqz p2, :cond_0

    :try_start_0
    iget-object p2, p0, Lcom/jscape/inet/a/n;->s:Lcom/jscape/util/h/C;

    invoke-interface {p2, p1}, Lcom/jscape/util/h/C;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/n;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    return-object p1
.end method

.method private a(Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .locals 2

    new-instance v0, Ljava/io/BufferedOutputStream;

    const/16 v1, 0x2000

    invoke-direct {v0, p1, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    return-object v0
.end method

.method private a(Ljava/io/OutputStream;Z)Ljava/io/OutputStream;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    if-eqz p2, :cond_0

    :try_start_0
    iget-object p2, p0, Lcom/jscape/inet/a/n;->s:Lcom/jscape/util/h/C;

    invoke-interface {p2, p1}, Lcom/jscape/util/h/C;->a(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/n;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    return-object p1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x2

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance v1, Lcom/jscape/inet/a/b;

    invoke-direct {v1, v0, p1}, Lcom/jscape/inet/a/b;-><init>(II)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/n;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private a(Lcom/jscape/inet/a/a/b/n;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/n;->d(Lcom/jscape/inet/a/a/b/n;)V

    iget-object v0, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    invoke-virtual {v0, p1}, Lcom/jscape/util/k/a/B;->write(Ljava/lang/Object;)V

    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/a/c;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/n;->w:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/n;->w:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/n;->D:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method private b(Ljava/lang/String;JZ)Lcom/jscape/inet/a/a/b/Z;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/a/a/b/N;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/jscape/inet/a/a/b/N;-><init>(Ljava/lang/String;JZ)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/a/a/b/Z;

    return-object p1
.end method

.method private b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/jscape/inet/a/a/b/n;",
            ">(",
            "Lcom/jscape/inet/a/a/b/n;",
            ")TM;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/n;->a(Lcom/jscape/inet/a/a/b/n;)V

    invoke-direct {p0}, Lcom/jscape/inet/a/n;->r()Lcom/jscape/inet/a/a/b/n;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/n;->c(Lcom/jscape/inet/a/a/b/n;)V

    return-object p1
.end method

.method private b(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/a/n;->z:Lcom/jscape/util/h/T;

    invoke-interface {v0, p1}, Lcom/jscape/util/h/T;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object p1

    return-object p1
.end method

.method private b(Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/a/n;->z:Lcom/jscape/util/h/T;

    invoke-interface {v0, p1}, Lcom/jscape/util/h/T;->b(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object p1

    return-object p1
.end method

.method private c(Lcom/jscape/inet/a/a/b/n;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/c;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    instance-of v0, p1, Lcom/jscape/inet/a/a/b/R;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    return-void

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/a/n;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/n;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    check-cast p1, Lcom/jscape/inet/a/a/b/R;

    new-instance v0, Lcom/jscape/inet/a/d;

    iget-object p1, p1, Lcom/jscape/inet/a/a/b/R;->a:Lcom/jscape/inet/a/a/b/ErrorCode;

    invoke-direct {v0, p1}, Lcom/jscape/inet/a/d;-><init>(Lcom/jscape/inet/a/a/b/ErrorCode;)V

    throw v0
.end method

.method private d(Lcom/jscape/inet/a/a/b/n;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/a/c;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/n;->w:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/n;->w:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/n;->D:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    invoke-virtual {v5}, Lcom/jscape/util/k/a/B;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    invoke-virtual {v5}, Lcom/jscape/util/k/a/B;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v3, 0x2

    aput-object p1, v4, v3

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private e(Lcom/jscape/inet/a/a/b/n;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/a/c;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/n;->w:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/n;->w:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/n;->D:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v0, v0, v3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    invoke-virtual {v5}, Lcom/jscape/util/k/a/B;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    invoke-virtual {v5}, Lcom/jscape/util/k/a/B;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private g()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/a/n;->k:Lcom/jscape/util/k/a/v;

    invoke-interface {v0}, Lcom/jscape/util/k/a/v;->connect()Lcom/jscape/util/k/a/r;

    move-result-object v0

    check-cast v0, Lcom/jscape/util/k/a/C;

    new-instance v1, Lcom/jscape/util/k/a/B;

    sget-object v2, Lcom/jscape/inet/a/n;->d:Lcom/jscape/util/h/I;

    invoke-direct {v1, v0, v2}, Lcom/jscape/util/k/a/B;-><init>(Lcom/jscape/util/k/a/C;Lcom/jscape/util/h/I;)V

    iput-object v1, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    return-void
.end method

.method private o()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/c;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/util/k/a/B;->close()V

    :cond_1
    return-void
.end method

.method private p()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/a/n;->p:Lcom/jscape/inet/a/a/c/g;

    iget v1, p0, Lcom/jscape/inet/a/n;->A:I

    iget-object v2, p0, Lcom/jscape/inet/a/n;->q:Lcom/jscape/util/Time;

    iget-object v3, p0, Lcom/jscape/inet/a/n;->w:Ljava/util/logging/Logger;

    invoke-interface {v0, v1, v2, v3}, Lcom/jscape/inet/a/a/c/g;->a(ILcom/jscape/util/Time;Ljava/util/logging/Logger;)Lcom/jscape/inet/a/a/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/inet/a/n;->y:Lcom/jscape/inet/a/a/c/f;

    iget-object v1, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    invoke-virtual {v1}, Lcom/jscape/util/k/a/B;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jscape/util/k/TransportAddress;->asSocketAddress()Ljava/net/InetSocketAddress;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/jscape/inet/a/a/c/f;->a(Ljava/net/InetSocketAddress;)V

    sget-object v0, Lcom/jscape/util/h/T;->a:Lcom/jscape/util/h/T;

    iput-object v0, p0, Lcom/jscape/inet/a/n;->z:Lcom/jscape/util/h/T;

    return-void
.end method

.method private q()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/c;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/n;->y:Lcom/jscape/inet/a/a/c/f;

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-interface {v1}, Lcom/jscape/inet/a/a/c/f;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/a/n;->y:Lcom/jscape/inet/a/a/c/f;

    sget-object v0, Lcom/jscape/util/h/T;->a:Lcom/jscape/util/h/T;

    iput-object v0, p0, Lcom/jscape/inet/a/n;->z:Lcom/jscape/util/h/T;

    :cond_1
    return-void
.end method

.method private r()Lcom/jscape/inet/a/a/b/n;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    invoke-virtual {v0}, Lcom/jscape/util/k/a/B;->read()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/a/a/b/n;

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->e(Lcom/jscape/inet/a/a/b/n;)V

    return-object v0
.end method

.method private s()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/a/a/b/w;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/jscape/inet/a/a/b/w;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/a/a/b/Y;

    iget v1, v0, Lcom/jscape/inet/a/a/b/Y;->a:I

    invoke-direct {p0, v1}, Lcom/jscape/inet/a/n;->a(I)V

    iget v0, v0, Lcom/jscape/inet/a/a/b/Y;->c:I

    iput v0, p0, Lcom/jscape/inet/a/n;->A:I

    return-void
.end method

.method private t()V
    .locals 1

    :try_start_0
    new-instance v0, Lcom/jscape/inet/a/a/b/B;

    invoke-direct {v0}, Lcom/jscape/inet/a/a/b/B;-><init>()V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->a(Lcom/jscape/inet/a/a/b/n;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method private u()Lcom/jscape/inet/a/a/c/a/i;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/c;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/jscape/inet/a/n;->v:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/a/n;->r:Lcom/jscape/inet/a/a/c/b;

    iget-object v1, p0, Lcom/jscape/inet/a/n;->u:Lcom/jscape/util/A;

    invoke-interface {v0, v1}, Lcom/jscape/inet/a/a/c/b;->b(Lcom/jscape/util/A;)Lcom/jscape/inet/a/a/c/a/i;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/a/n;->r:Lcom/jscape/inet/a/a/c/b;

    iget-object v1, p0, Lcom/jscape/inet/a/n;->u:Lcom/jscape/util/A;

    invoke-interface {v0, v1}, Lcom/jscape/inet/a/a/c/b;->a(Lcom/jscape/util/A;)Lcom/jscape/inet/a/a/c/a/i;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private v()V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/a/n;->r()Lcom/jscape/inet/a/a/b/n;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jscape/inet/a/n;->c(Lcom/jscape/inet/a/a/b/n;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iput-object v0, p0, Lcom/jscape/inet/a/n;->B:Lcom/jscape/inet/a/a/c/a/E;

    goto :goto_1

    :catchall_0
    move-exception v1

    goto :goto_2

    :catch_0
    move-exception v1

    :try_start_1
    invoke-direct {p0, v1}, Lcom/jscape/inet/a/n;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :goto_1
    return-void

    :goto_2
    iput-object v0, p0, Lcom/jscape/inet/a/n;->B:Lcom/jscape/inet/a/a/c/a/E;

    throw v1
.end method

.method private w()V
    .locals 7

    invoke-static {}, Lcom/jscape/inet/a/c;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/n;->w:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/n;->w:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/n;->D:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v0, v0, v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    invoke-virtual {v6}, Lcom/jscape/util/k/a/B;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v6

    aput-object v6, v4, v5

    iget-object v5, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    invoke-virtual {v5}, Lcom/jscape/util/k/a/B;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private x()V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/a/c;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/n;->w:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/n;->w:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/n;->D:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v0, v0, v3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    invoke-virtual {v5}, Lcom/jscape/util/k/a/B;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    invoke-virtual {v5}, Lcom/jscape/util/k/a/B;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/jscape/inet/a/a/b/ac;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/a/a/b/A;

    invoke-direct {v0, p1}, Lcom/jscape/inet/a/a/b/A;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/a/a/b/U;

    iget-object p1, p1, Lcom/jscape/inet/a/a/b/U;->a:Lcom/jscape/inet/a/a/b/ac;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/c;->b(Ljava/lang/Throwable;)Lcom/jscape/inet/a/c;

    move-result-object p1

    throw p1
.end method

.method public a(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/a/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/jscape/inet/a/n;->b(Ljava/lang/String;JZ)Lcom/jscape/inet/a/a/b/Z;

    move-result-object p2

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/n;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object p1

    invoke-direct {p0, p1, p5}, Lcom/jscape/inet/a/n;->a(Ljava/io/InputStream;Z)Ljava/io/InputStream;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/n;->b(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object p1

    iget-object p3, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    invoke-virtual {p3}, Lcom/jscape/util/k/a/B;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object p3

    invoke-virtual {p3}, Lcom/jscape/util/k/TransportAddress;->asSocketAddress()Ljava/net/InetSocketAddress;

    move-result-object p3

    invoke-direct {p0}, Lcom/jscape/inet/a/n;->u()Lcom/jscape/inet/a/a/c/a/i;

    move-result-object p4

    iget-object p5, p0, Lcom/jscape/inet/a/n;->y:Lcom/jscape/inet/a/a/c/f;

    iget p2, p2, Lcom/jscape/inet/a/a/b/Z;->a:I

    invoke-interface {p5, p2, p1, p3, p4}, Lcom/jscape/inet/a/a/c/f;->a(ILjava/io/InputStream;Ljava/net/InetSocketAddress;Lcom/jscape/inet/a/a/c/a/i;)Lcom/jscape/inet/a/a/c/a/E;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/a/n;->B:Lcom/jscape/inet/a/a/c/a/E;

    iget-object p1, p0, Lcom/jscape/inet/a/n;->B:Lcom/jscape/inet/a/a/c/a/E;

    new-instance p2, Lcom/jscape/inet/a/f;

    const/4 p3, 0x0

    invoke-direct {p2, p6, p3}, Lcom/jscape/inet/a/f;-><init>(Lcom/jscape/inet/a/l;Lcom/jscape/inet/a/e;)V

    invoke-interface {p1, p2}, Lcom/jscape/inet/a/a/c/a/E;->a(Lcom/jscape/inet/a/a/c/a/F;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/jscape/inet/a/n;->v()V

    return-void

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/a/c;->b(Ljava/lang/Throwable;)Lcom/jscape/inet/a/c;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/a/n;->v()V

    throw p1
.end method

.method public a(Ljava/lang/String;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/a/a/b/M;

    invoke-direct {v0, p1, p2, p3}, Lcom/jscape/inet/a/a/b/M;-><init>(Ljava/lang/String;J)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/c;->b(Ljava/lang/Throwable;)Lcom/jscape/inet/a/c;

    move-result-object p1

    throw p1
.end method

.method public a(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/a/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/jscape/inet/a/n;->a(Ljava/lang/String;JZ)Lcom/jscape/inet/a/a/b/V;

    move-result-object p1

    invoke-direct {p0, p4}, Lcom/jscape/inet/a/n;->a(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object p2

    invoke-direct {p0, p2, p5}, Lcom/jscape/inet/a/n;->a(Ljava/io/OutputStream;Z)Ljava/io/OutputStream;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/jscape/inet/a/n;->b(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object p2

    iget-object p3, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    invoke-virtual {p3}, Lcom/jscape/util/k/a/B;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object p3

    invoke-virtual {p3}, Lcom/jscape/util/k/TransportAddress;->asSocketAddress()Ljava/net/InetSocketAddress;

    move-result-object p3

    iget-object p4, p0, Lcom/jscape/inet/a/n;->y:Lcom/jscape/inet/a/a/c/f;

    iget p1, p1, Lcom/jscape/inet/a/a/b/V;->a:I

    invoke-interface {p4, p1, p2, p3}, Lcom/jscape/inet/a/a/c/f;->a(ILjava/io/OutputStream;Ljava/net/InetSocketAddress;)Lcom/jscape/inet/a/a/c/a/E;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/a/n;->B:Lcom/jscape/inet/a/a/c/a/E;

    iget-object p1, p0, Lcom/jscape/inet/a/n;->B:Lcom/jscape/inet/a/a/c/a/E;

    new-instance p2, Lcom/jscape/inet/a/f;

    const/4 p3, 0x0

    invoke-direct {p2, p6, p3}, Lcom/jscape/inet/a/f;-><init>(Lcom/jscape/inet/a/l;Lcom/jscape/inet/a/e;)V

    invoke-interface {p1, p2}, Lcom/jscape/inet/a/a/c/a/E;->a(Lcom/jscape/inet/a/a/c/a/F;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/jscape/inet/a/n;->v()V

    return-void

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/a/c;->b(Ljava/lang/Throwable;)Lcom/jscape/inet/a/c;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/a/n;->v()V

    throw p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/a/a/b/z;

    invoke-direct {v0, p1, p2}, Lcom/jscape/inet/a/a/b/z;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/c;->b(Ljava/lang/Throwable;)Lcom/jscape/inet/a/c;

    move-result-object p1

    throw p1
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/a/a/b/E;

    invoke-direct {v0, p1, p2}, Lcom/jscape/inet/a/a/b/E;-><init>(Ljava/lang/String;Z)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/c;->b(Ljava/lang/Throwable;)Lcom/jscape/inet/a/c;

    move-result-object p1

    throw p1
.end method

.method public a()Z
    .locals 3

    invoke-static {}, Lcom/jscape/inet/a/c;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    const/4 v2, 0x0

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    :cond_0
    :try_start_2
    invoke-virtual {v1}, Lcom/jscape/util/k/a/B;->closed()Z

    move-result v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    if-nez v0, :cond_3

    if-eqz v1, :cond_2

    :cond_1
    return v2

    :cond_2
    :try_start_3
    invoke-virtual {p0}, Lcom/jscape/inet/a/n;->e()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    const/4 v1, 0x1

    goto :goto_0

    :catch_0
    return v2

    :cond_3
    :goto_0
    return v1

    :catch_1
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/a/n;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/n;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_3
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/a/n;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/n;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method protected actualStart()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/a/n;->g()V

    invoke-direct {p0}, Lcom/jscape/inet/a/n;->w()V

    invoke-direct {p0}, Lcom/jscape/inet/a/n;->s()V

    invoke-direct {p0}, Lcom/jscape/inet/a/n;->p()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/jscape/inet/a/n;->o()V

    invoke-direct {p0}, Lcom/jscape/inet/a/n;->q()V

    throw v0
.end method

.method protected actualStop()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/a/n;->t()V

    invoke-direct {p0}, Lcom/jscape/inet/a/n;->o()V

    invoke-virtual {p0}, Lcom/jscape/inet/a/n;->f()V

    invoke-direct {p0}, Lcom/jscape/inet/a/n;->q()V

    invoke-direct {p0}, Lcom/jscape/inet/a/n;->x()V

    return-void
.end method

.method public b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/a/a/b/I;

    invoke-direct {v0}, Lcom/jscape/inet/a/a/b/I;-><init>()V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;

    iget-object v0, p0, Lcom/jscape/inet/a/n;->l:Lcom/jscape/util/k/a/s;

    iget-object v1, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    invoke-virtual {v1}, Lcom/jscape/util/k/a/B;->c()Lcom/jscape/util/k/a/C;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/jscape/util/k/a/s;->a(Lcom/jscape/util/k/a/r;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/c;->b(Ljava/lang/Throwable;)Lcom/jscape/inet/a/c;

    move-result-object v0

    throw v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/a/a/b/F;

    invoke-direct {v0, p1, p2}, Lcom/jscape/inet/a/a/b/F;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/c;->b(Ljava/lang/Throwable;)Lcom/jscape/inet/a/c;

    move-result-object p1

    throw p1
.end method

.method public b(Ljava/lang/String;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/a/a/b/u;

    invoke-direct {v0, p1}, Lcom/jscape/inet/a/a/b/u;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/a/a/b/S;

    iget-object p1, p1, Lcom/jscape/inet/a/a/b/S;->a:[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/c;->b(Ljava/lang/Throwable;)Lcom/jscape/inet/a/c;

    move-result-object p1

    throw p1
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/a/a/b/s;

    invoke-direct {v0, p1, p2}, Lcom/jscape/inet/a/a/b/s;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/a/a/b/X;

    iget-object p1, p1, Lcom/jscape/inet/a/a/b/X;->a:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/c;->b(Ljava/lang/Throwable;)Lcom/jscape/inet/a/c;

    move-result-object p1

    throw p1
.end method

.method public c()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/a/a/b/q;

    invoke-direct {v0}, Lcom/jscape/inet/a/a/b/q;-><init>()V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;

    iget-object v0, p0, Lcom/jscape/inet/a/n;->l:Lcom/jscape/util/k/a/s;

    iget-object v1, p0, Lcom/jscape/inet/a/n;->x:Lcom/jscape/util/k/a/B;

    invoke-virtual {v1}, Lcom/jscape/util/k/a/B;->c()Lcom/jscape/util/k/a/C;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/jscape/util/k/a/s;->b(Lcom/jscape/util/k/a/r;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/c;->b(Ljava/lang/Throwable;)Lcom/jscape/inet/a/c;

    move-result-object v0

    throw v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/a/a/b/r;

    invoke-direct {v0, p1}, Lcom/jscape/inet/a/a/b/r;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/c;->b(Ljava/lang/Throwable;)Lcom/jscape/inet/a/c;

    move-result-object p1

    throw p1
.end method

.method public d(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/jscape/inet/a/a/b/ac;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/a/a/b/x;

    invoke-direct {v0, p1}, Lcom/jscape/inet/a/a/b/x;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/a/a/b/Q;

    iget-object p1, p1, Lcom/jscape/inet/a/a/b/Q;->a:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/c;->b(Ljava/lang/Throwable;)Lcom/jscape/inet/a/c;

    move-result-object p1

    throw p1
.end method

.method public d()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/a/n;->m:Lcom/jscape/inet/a/a/f;

    invoke-interface {v0}, Lcom/jscape/inet/a/a/f;->a()Lcom/jscape/inet/a/a/g;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/a/a/b/t;

    iget-object v2, p0, Lcom/jscape/inet/a/n;->n:Ljava/lang/String;

    iget v3, p0, Lcom/jscape/inet/a/n;->o:I

    iget-object v4, v0, Lcom/jscape/inet/a/a/g;->a:[B

    invoke-direct {v1, v2, v3, v4}, Lcom/jscape/inet/a/a/b/t;-><init>(Ljava/lang/String;I[B)V

    invoke-direct {p0, v1}, Lcom/jscape/inet/a/n;->b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/a/a/b/P;

    iget-object v2, p0, Lcom/jscape/inet/a/n;->m:Lcom/jscape/inet/a/a/f;

    iget-object v1, v1, Lcom/jscape/inet/a/a/b/P;->a:[B

    iget v3, p0, Lcom/jscape/inet/a/n;->o:I

    invoke-interface {v2, v0, v1, v3}, Lcom/jscape/inet/a/a/f;->a(Lcom/jscape/inet/a/a/g;[BI)[B

    move-result-object v0

    new-instance v1, Lcom/jscape/util/h/O;

    iget-object v2, p0, Lcom/jscape/inet/a/n;->n:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/jscape/util/h/O;-><init>(Ljava/lang/String;[B)V

    iput-object v1, p0, Lcom/jscape/inet/a/n;->z:Lcom/jscape/util/h/T;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/c;->b(Ljava/lang/Throwable;)Lcom/jscape/inet/a/c;

    move-result-object v0

    throw v0
.end method

.method public e()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/a/a/b/y;

    invoke-direct {v0}, Lcom/jscape/inet/a/a/b/y;-><init>()V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/n;->b(Lcom/jscape/inet/a/a/b/n;)Lcom/jscape/inet/a/a/b/n;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/c;->b(Ljava/lang/Throwable;)Lcom/jscape/inet/a/c;

    move-result-object v0

    throw v0
.end method

.method public f()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/c;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/n;->B:Lcom/jscape/inet/a/a/c/a/E;

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-interface {v1}, Lcom/jscape/inet/a/a/c/a/E;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/a/n;->B:Lcom/jscape/inet/a/a/c/a/E;

    :cond_1
    return-void
.end method
