.class public Lcom/jscape/inet/a/a/a/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/a/a/b/n;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result p1

    and-int/lit16 p1, p1, 0xff

    invoke-static {p1}, Lcom/jscape/inet/a/a/b/ErrorCode;->forCode(I)Lcom/jscape/inet/a/a/b/ErrorCode;

    move-result-object p1

    new-instance v0, Lcom/jscape/inet/a/a/b/R;

    invoke-direct {v0, p1}, Lcom/jscape/inet/a/a/b/R;-><init>(Lcom/jscape/inet/a/a/b/ErrorCode;)V

    return-object v0
.end method

.method public a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/R;

    iget-object p1, p1, Lcom/jscape/inet/a/a/b/R;->a:Lcom/jscape/inet/a/a/b/ErrorCode;

    iget p1, p1, Lcom/jscape/inet/a/a/b/ErrorCode;->code:I

    int-to-byte p1, p1

    invoke-static {p1, p2}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/a/i;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/n;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/a/i;->a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V

    return-void
.end method
