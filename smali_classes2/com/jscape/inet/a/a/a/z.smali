.class public Lcom/jscape/inet/a/a/a/z;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/a/a/b/n;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/inet/a/a/a/x;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jscape/inet/a/a/a/x;

    invoke-direct {v0}, Lcom/jscape/inet/a/a/a/x;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/a/a/a/z;->a:Lcom/jscape/inet/a/a/a/x;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/a/a/a/z;->a:Lcom/jscape/inet/a/a/a/x;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/a/a/a/x;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/ac;

    move-result-object p1

    new-instance v0, Lcom/jscape/inet/a/a/b/U;

    invoke-direct {v0, p1}, Lcom/jscape/inet/a/a/b/U;-><init>(Lcom/jscape/inet/a/a/b/ac;)V

    return-object v0
.end method

.method public a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/U;

    iget-object v0, p0, Lcom/jscape/inet/a/a/a/z;->a:Lcom/jscape/inet/a/a/a/x;

    iget-object p1, p1, Lcom/jscape/inet/a/a/b/U;->a:Lcom/jscape/inet/a/a/b/ac;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/a/a/a/x;->a(Lcom/jscape/inet/a/a/b/ac;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/a/z;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/n;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/a/z;->a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V

    return-void
.end method
