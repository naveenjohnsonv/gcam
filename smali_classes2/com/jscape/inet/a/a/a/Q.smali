.class public Lcom/jscape/inet/a/a/a/Q;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/a/a/b/n;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, p1}, Lcom/jscape/util/h/a/r;->a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/jscape/util/h/a/i;->a(Ljava/io/InputStream;)J

    move-result-wide v3

    invoke-static {p1}, Lcom/jscape/util/h/a/a;->a(Ljava/io/InputStream;)Z

    move-result v5

    invoke-static {p1}, Lcom/jscape/util/h/a/a;->a(Ljava/io/InputStream;)Z

    move-result v6

    new-instance p1, Lcom/jscape/inet/a/a/b/O;

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lcom/jscape/inet/a/a/b/O;-><init>(Ljava/lang/String;JZZ)V

    return-object p1
.end method

.method public a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/O;

    iget-object v0, p1, Lcom/jscape/inet/a/a/b/O;->c:Ljava/lang/String;

    sget-object v1, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, v1, p2}, Lcom/jscape/util/h/a/r;->a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    iget-wide v0, p1, Lcom/jscape/inet/a/a/b/O;->d:J

    invoke-static {v0, v1, p2}, Lcom/jscape/util/h/a/i;->a(JLjava/io/OutputStream;)V

    iget-boolean v0, p1, Lcom/jscape/inet/a/a/b/O;->e:Z

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/a;->a(ZLjava/io/OutputStream;)V

    iget-boolean p1, p1, Lcom/jscape/inet/a/a/b/O;->f:Z

    invoke-static {p1, p2}, Lcom/jscape/util/h/a/a;->a(ZLjava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/a/Q;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/n;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/a/Q;->a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V

    return-void
.end method
