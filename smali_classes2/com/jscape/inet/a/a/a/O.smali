.class public Lcom/jscape/inet/a/a/a/O;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/a/a/b/n;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:I = 0xa


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/a/q;->b()Z

    move-result v0

    invoke-static {p1}, Lcom/jscape/util/h/a/f;->a(Ljava/io/InputStream;)I

    move-result v1

    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    if-lez v2, :cond_0

    :try_start_1
    invoke-static {p1}, Lcom/jscape/util/h/a/f;->a(Ljava/io/InputStream;)I

    move-result v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :cond_0
    const/16 p1, 0xa

    goto :goto_1

    :cond_1
    :goto_0
    move p1, v2

    :goto_1
    new-instance v0, Lcom/jscape/inet/a/a/b/Y;

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/a/a/b/Y;-><init>(II)V

    return-object v0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/a/a/a/O;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/a/a/O;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method public a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/Y;

    iget v0, p1, Lcom/jscape/inet/a/a/b/Y;->a:I

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/f;->a(ILjava/io/OutputStream;)V

    iget p1, p1, Lcom/jscape/inet/a/a/b/Y;->c:I

    invoke-static {p1, p2}, Lcom/jscape/util/h/a/f;->a(ILjava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/a/O;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/n;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/a/O;->a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V

    return-void
.end method
