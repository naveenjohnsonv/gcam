.class public Lcom/jscape/inet/a/a/a/D;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/a/a/b/n;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, p1}, Lcom/jscape/util/h/a/r;->a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/jscape/util/h/a/i;->a(Ljava/io/InputStream;)J

    move-result-wide v3

    invoke-static {p1}, Lcom/jscape/util/h/a/i;->a(Ljava/io/InputStream;)J

    move-result-wide v5

    invoke-static {p1}, Lcom/jscape/util/h/a/i;->a(Ljava/io/InputStream;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/jscape/util/A;->a(J)Lcom/jscape/util/A;

    move-result-object v7

    invoke-static {p1}, Lcom/jscape/util/h/a/a;->a(Ljava/io/InputStream;)Z

    move-result v8

    invoke-static {p1}, Lcom/jscape/util/h/a/a;->a(Ljava/io/InputStream;)Z

    move-result v9

    new-instance p1, Lcom/jscape/inet/a/a/b/D;

    move-object v1, p1

    invoke-direct/range {v1 .. v9}, Lcom/jscape/inet/a/a/b/D;-><init>(Ljava/lang/String;JJLcom/jscape/util/A;ZZ)V

    return-object p1
.end method

.method public a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/D;

    iget-object v0, p1, Lcom/jscape/inet/a/a/b/D;->c:Ljava/lang/String;

    sget-object v1, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, v1, p2}, Lcom/jscape/util/h/a/r;->a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    invoke-static {}, Lcom/jscape/inet/a/a/a/q;->b()Z

    move-result v0

    iget-wide v1, p1, Lcom/jscape/inet/a/a/b/D;->d:J

    invoke-static {v1, v2, p2}, Lcom/jscape/util/h/a/i;->a(JLjava/io/OutputStream;)V

    iget-wide v1, p1, Lcom/jscape/inet/a/a/b/D;->e:J

    invoke-static {v1, v2, p2}, Lcom/jscape/util/h/a/i;->a(JLjava/io/OutputStream;)V

    :try_start_0
    iget-object v1, p1, Lcom/jscape/inet/a/a/b/D;->f:Lcom/jscape/util/A;

    invoke-virtual {v1}, Lcom/jscape/util/A;->a()J

    move-result-wide v1

    invoke-static {v1, v2, p2}, Lcom/jscape/util/h/a/i;->a(JLjava/io/OutputStream;)V

    iget-boolean v1, p1, Lcom/jscape/inet/a/a/b/D;->g:Z

    invoke-static {v1, p2}, Lcom/jscape/util/h/a/a;->a(ZLjava/io/OutputStream;)V

    iget-boolean p1, p1, Lcom/jscape/inet/a/a/b/D;->h:Z

    invoke-static {p1, p2}, Lcom/jscape/util/h/a/a;->a(ZLjava/io/OutputStream;)V

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_1

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    invoke-static {p1}, Lcom/jscape/inet/a/a/a/q;->b(Z)V

    :cond_1
    return-void

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/a/a/a/D;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/a/a/D;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/a/D;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/n;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/a/D;->a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V

    return-void
.end method
