.class public Lcom/jscape/inet/a/a/a/L;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/a/a/b/n;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/f;->a(Ljava/io/InputStream;)I

    move-result v0

    invoke-static {p1}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result p1

    and-int/lit16 p1, p1, 0xff

    new-instance v1, Lcom/jscape/inet/a/a/b/W;

    invoke-static {p1}, Lcom/jscape/inet/a/a/b/c;->a(I)Lcom/jscape/inet/a/a/b/c;

    move-result-object p1

    invoke-direct {v1, v0, p1}, Lcom/jscape/inet/a/a/b/W;-><init>(ILcom/jscape/inet/a/a/b/c;)V

    return-object v1
.end method

.method public a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/W;

    iget v0, p1, Lcom/jscape/inet/a/a/b/W;->a:I

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/f;->a(ILjava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/a/a/b/W;->c:Lcom/jscape/inet/a/a/b/c;

    iget p1, p1, Lcom/jscape/inet/a/a/b/c;->f:I

    int-to-byte p1, p1

    invoke-static {p1, p2}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/a/L;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/n;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/a/L;->a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V

    return-void
.end method
