.class public Lcom/jscape/inet/a/a/a/G;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/a/a/b/n;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, p1}, Lcom/jscape/util/h/a/r;->a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/jscape/util/h/a/i;->a(Ljava/io/InputStream;)J

    move-result-wide v3

    invoke-static {p1}, Lcom/jscape/util/h/a/i;->a(Ljava/io/InputStream;)J

    move-result-wide v5

    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, p1}, Lcom/jscape/util/h/a/r;->a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/jscape/inet/a/a/b/a;->b(Ljava/lang/String;)Lcom/jscape/inet/a/a/b/a;

    move-result-object v7

    new-instance p1, Lcom/jscape/inet/a/a/b/G;

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/jscape/inet/a/a/b/G;-><init>(Ljava/lang/String;JJLcom/jscape/inet/a/a/b/a;)V

    return-object p1
.end method

.method public a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/G;

    iget-object v0, p1, Lcom/jscape/inet/a/a/b/G;->c:Ljava/lang/String;

    sget-object v1, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, v1, p2}, Lcom/jscape/util/h/a/r;->a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    iget-wide v0, p1, Lcom/jscape/inet/a/a/b/G;->d:J

    invoke-static {v0, v1, p2}, Lcom/jscape/util/h/a/i;->a(JLjava/io/OutputStream;)V

    iget-wide v0, p1, Lcom/jscape/inet/a/a/b/G;->e:J

    invoke-static {v0, v1, p2}, Lcom/jscape/util/h/a/i;->a(JLjava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/a/a/b/G;->f:Lcom/jscape/inet/a/a/b/a;

    iget-object p1, p1, Lcom/jscape/inet/a/a/b/a;->d:Ljava/lang/String;

    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {p1, v0, p2}, Lcom/jscape/util/h/a/r;->a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/a/G;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/n;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/a/G;->a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V

    return-void
.end method
