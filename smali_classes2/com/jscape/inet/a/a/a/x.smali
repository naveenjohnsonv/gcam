.class public Lcom/jscape/inet/a/a/a/x;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/a/a/b/ac;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/ac;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, p1}, Lcom/jscape/util/h/a/r;->a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/b/b;->a(I)Lcom/jscape/inet/a/a/b/b;

    move-result-object v3

    invoke-static {}, Lcom/jscape/inet/a/a/a/q;->b()Z

    move-result v0

    invoke-static {p1}, Lcom/jscape/util/h/a/a;->a(Ljava/io/InputStream;)Z

    move-result v4

    invoke-static {p1}, Lcom/jscape/util/h/a/a;->a(Ljava/io/InputStream;)Z

    move-result v5

    invoke-static {p1}, Lcom/jscape/util/h/a/i;->a(Ljava/io/InputStream;)J

    move-result-wide v6

    invoke-static {p1}, Lcom/jscape/util/h/a/i;->a(Ljava/io/InputStream;)J

    move-result-wide v8

    new-instance p1, Lcom/jscape/inet/a/a/b/ac;

    move-object v1, p1

    invoke-direct/range {v1 .. v9}, Lcom/jscape/inet/a/a/b/ac;-><init>(Ljava/lang/String;Lcom/jscape/inet/a/a/b/b;ZZJJ)V

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    new-array v0, v0, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object p1
.end method

.method public a(Lcom/jscape/inet/a/a/b/ac;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p1, Lcom/jscape/inet/a/a/b/ac;->a:Ljava/lang/String;

    sget-object v1, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, v1, p2}, Lcom/jscape/util/h/a/r;->a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    iget-object v0, p1, Lcom/jscape/inet/a/a/b/ac;->b:Lcom/jscape/inet/a/a/b/b;

    invoke-virtual {v0}, Lcom/jscape/inet/a/a/b/b;->ordinal()I

    move-result v0

    int-to-byte v0, v0

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    iget-boolean v0, p1, Lcom/jscape/inet/a/a/b/ac;->c:Z

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/a;->a(ZLjava/io/OutputStream;)V

    iget-boolean v0, p1, Lcom/jscape/inet/a/a/b/ac;->d:Z

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/a;->a(ZLjava/io/OutputStream;)V

    iget-wide v0, p1, Lcom/jscape/inet/a/a/b/ac;->e:J

    invoke-static {v0, v1, p2}, Lcom/jscape/util/h/a/i;->a(JLjava/io/OutputStream;)V

    iget-wide v0, p1, Lcom/jscape/inet/a/a/b/ac;->f:J

    invoke-static {v0, v1, p2}, Lcom/jscape/util/h/a/i;->a(JLjava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/a/x;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/ac;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/ac;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/a/x;->a(Lcom/jscape/inet/a/a/b/ac;Ljava/io/OutputStream;)V

    return-void
.end method
