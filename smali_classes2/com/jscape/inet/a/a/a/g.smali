.class public Lcom/jscape/inet/a/a/a/g;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/a/a/b/n;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/inet/a/a/a/x;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jscape/inet/a/a/a/x;

    invoke-direct {v0}, Lcom/jscape/inet/a/a/a/x;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/a/a/a/g;->a:Lcom/jscape/inet/a/a/a/x;

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/io/InputStream;I)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "I)",
            "Ljava/util/List<",
            "Lcom/jscape/inet/a/a/b/ac;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/a/q;->b()Z

    move-result v0

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    :goto_0
    add-int/lit8 v2, p2, -0x1

    if-lez p2, :cond_1

    iget-object p2, p0, Lcom/jscape/inet/a/a/a/g;->a:Lcom/jscape/inet/a/a/a/x;

    invoke-virtual {p2, p1}, Lcom/jscape/inet/a/a/a/x;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/ac;

    move-result-object p2

    if-nez v0, :cond_1

    :try_start_0
    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    move p2, v2

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/a/a/g;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    return-object v1
.end method

.method private a(Ljava/util/List;Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/jscape/inet/a/a/b/ac;",
            ">;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/a/q;->c()Z

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/a/a/b/ac;

    iget-object v2, p0, Lcom/jscape/inet/a/a/a/g;->a:Lcom/jscape/inet/a/a/a/x;

    invoke-virtual {v2, v1, p2}, Lcom/jscape/inet/a/a/a/x;->a(Lcom/jscape/inet/a/a/b/ac;Ljava/io/OutputStream;)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/f;->a(Ljava/io/InputStream;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/a/a/a/g;->a(Ljava/io/InputStream;I)Ljava/util/List;

    move-result-object p1

    new-instance v0, Lcom/jscape/inet/a/a/b/Q;

    invoke-direct {v0, p1}, Lcom/jscape/inet/a/a/b/Q;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/Q;

    iget-object v0, p1, Lcom/jscape/inet/a/a/b/Q;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/f;->a(ILjava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/a/a/b/Q;->a:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/a/a/a/g;->a(Ljava/util/List;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/a/g;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/n;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/a/g;->a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V

    return-void
.end method
