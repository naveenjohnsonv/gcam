.class public Lcom/jscape/inet/a/a/a/R;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/a/a/b/n;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/f;->a(Ljava/io/InputStream;)I

    move-result p1

    new-instance v0, Lcom/jscape/inet/a/a/b/Z;

    invoke-direct {v0, p1}, Lcom/jscape/inet/a/a/b/Z;-><init>(I)V

    return-object v0
.end method

.method public a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/Z;

    iget p1, p1, Lcom/jscape/inet/a/a/b/Z;->a:I

    invoke-static {p1, p2}, Lcom/jscape/util/h/a/f;->a(ILjava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/a/R;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/b/n;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/b/n;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/a/R;->a(Lcom/jscape/inet/a/a/b/n;Ljava/io/OutputStream;)V

    return-void
.end method
