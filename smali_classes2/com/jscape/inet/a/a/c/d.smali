.class public Lcom/jscape/inet/a/a/c/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/a/a/c/f;
.implements Lcom/jscape/inet/a/a/c/a/r;
.implements Lcom/jscape/inet/a/a/c/a/b/f;


# static fields
.field private static final a:Lcom/jscape/util/Time;

.field private static final b:I = 0x10000

.field private static q:Ljava/lang/String;

.field private static final r:[Ljava/lang/String;


# instance fields
.field private final c:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/a/a/c/a/b/i;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/logging/Logger;

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:Ljava/lang/Integer;

.field private l:Lcom/jscape/util/Time;

.field private m:Ljava/net/InetSocketAddress;

.field private n:Lcom/jscape/util/as;

.field private o:Lcom/jscape/inet/a/a/c/a/w;

.field private p:Lcom/jscape/inet/a/a/c/a/o;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Fe1bo"

    invoke-static {v1}, Lcom/jscape/inet/a/a/c/d;->b(Ljava/lang/String;)V

    const-string v3, "r\rj>\"(O[\tz>32JV\t|>\'&@E\t \u001er\rj><&T\u0010\u001c|{!&^U\u0008.|=(O[\u001f.h0+YUB\u001br\rj>!&O[\tz>5&XQL}w+\"\u000cF\rbk4i\u0015u\u001e|q#g_U\u0002jw? \u000c@\rmu43\u0002 r\rj><&T\u0010\u000fap22^B\t`jq%@_\u000femq1M\\\u0019k0%q*ZNq\u0013^Q\u0002}x45\u000c`\u0003|jq*Y\\\u0018gn=\"TU\u001e.{#5CBB\"q*ZNq\u0013^Q\u0002}x45\u000c`\u0003|jq(\\U\u0002kzq&X\u00107+m\u000ci\u000er\rj>\u001c\u0013y\u0010\u001aor$\"\u0002\u001fr\rj><&T\u0010\u000fas!+ID\tj>3+CS\u0007}>\'&@E\t "

    const/16 v4, 0x102

    const/16 v5, 0x18

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x33

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x3f

    const/16 v3, 0x22

    const-string v5, "\u0008S#7\u0008j\'({\u0004\u0001MLu\u0019z\u0005\u0013\u0008]9&f\u0012\u0003\u0008_!iNR\u0014u\u0010\u001c\u000bt\u0013GX_6\"p\u0003\u0014\u0008N0;5\u0015\u000bG]>ic\u0016\u000b][{"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x4a

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/a/a/c/d;->r:[Ljava/lang/String;

    const-wide/16 v0, 0x1

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->seconds(J)Lcom/jscape/util/Time;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/a/c/d;->a:Lcom/jscape/util/Time;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    const/4 v1, 0x3

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v2, 0x2

    if-eq v15, v2, :cond_7

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x1f

    goto :goto_4

    :cond_4
    const/16 v1, 0x74

    goto :goto_4

    :cond_5
    const/16 v1, 0x62

    goto :goto_4

    :cond_6
    const/16 v1, 0x2d

    goto :goto_4

    :cond_7
    const/16 v1, 0x3d

    goto :goto_4

    :cond_8
    const/16 v1, 0x5f

    :cond_9
    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/util/h/I;IIIIIILjava/lang/Integer;Lcom/jscape/util/Time;Ljava/util/logging/Logger;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/a/a/c/a/b/i;",
            ">;IIIIII",
            "Ljava/lang/Integer;",
            "Lcom/jscape/util/Time;",
            "Ljava/util/logging/Logger;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v4, p5

    move/from16 v5, p6

    move/from16 v6, p7

    move-object/from16 v7, p8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static/range {p1 .. p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/a/a/c/d;->k()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v9, p1

    iput-object v9, v0, Lcom/jscape/inet/a/a/c/d;->c:Lcom/jscape/util/h/I;

    int-to-long v9, v5

    sget-object v11, Lcom/jscape/inet/a/a/c/d;->r:[Ljava/lang/String;

    const/4 v12, 0x4

    aget-object v12, v11, v12

    const-wide/16 v13, 0x0

    invoke-static {v9, v10, v13, v14, v12}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput v5, v0, Lcom/jscape/inet/a/a/c/d;->i:I

    int-to-long v9, v1

    const/4 v5, 0x7

    aget-object v5, v11, v5

    invoke-static {v9, v10, v13, v14, v5}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput v1, v0, Lcom/jscape/inet/a/a/c/d;->e:I

    int-to-long v9, v2

    const/4 v1, 0x2

    aget-object v1, v11, v1

    invoke-static {v9, v10, v13, v14, v1}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput v2, v0, Lcom/jscape/inet/a/a/c/d;->f:I

    int-to-long v1, v3

    const/16 v5, 0xa

    aget-object v5, v11, v5

    invoke-static {v1, v2, v13, v14, v5}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput v3, v0, Lcom/jscape/inet/a/a/c/d;->g:I

    int-to-long v1, v4

    const/4 v3, 0x1

    aget-object v5, v11, v3

    invoke-static {v1, v2, v13, v14, v5}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput v4, v0, Lcom/jscape/inet/a/a/c/d;->h:I

    const/4 v1, 0x0

    if-eqz v8, :cond_3

    int-to-long v4, v6

    const/16 v2, 0x8

    aget-object v2, v11, v2

    invoke-static {v4, v5, v13, v14, v2}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput v6, v0, Lcom/jscape/inet/a/a/c/d;->j:I

    invoke-static/range {p9 .. p9}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object/from16 v2, p9

    iput-object v2, v0, Lcom/jscape/inet/a/a/c/d;->l:Lcom/jscape/util/Time;

    if-eqz v7, :cond_2

    invoke-virtual/range {p8 .. p8}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v8, :cond_1

    if-lez v2, :cond_0

    goto :goto_0

    :cond_0
    move v6, v1

    goto :goto_1

    :cond_1
    move v6, v2

    goto :goto_1

    :cond_2
    :goto_0
    move v6, v3

    :cond_3
    :goto_1
    sget-object v2, Lcom/jscape/inet/a/a/c/d;->r:[Ljava/lang/String;

    aget-object v1, v2, v1

    invoke-static {v6, v1}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    iput-object v7, v0, Lcom/jscape/inet/a/a/c/d;->k:Ljava/lang/Integer;

    invoke-static/range {p10 .. p10}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object/from16 v1, p10

    iput-object v1, v0, Lcom/jscape/inet/a/a/c/d;->d:Ljava/util/logging/Logger;

    if-nez v8, :cond_4

    const/4 v1, 0x5

    new-array v1, v1, [I

    invoke-static {v1}, Lcom/jscape/util/aq;->b([I)V

    :cond_4
    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/d;->o:Lcom/jscape/inet/a/a/c/a/w;

    invoke-interface {v0, p1}, Lcom/jscape/inet/a/a/c/a/w;->a(Lcom/jscape/inet/a/a/c/a/b/i;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/d;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/d;->d:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Ljava/lang/OutOfMemoryError;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/d;->d:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-virtual {v0, v1, p1, p2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/a/a/c/d;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/d;->d:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/d;->d:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/a/c/d;->r:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/a/a/c/d;->q:Ljava/lang/String;

    return-void
.end method

.method private c()V
    .locals 5

    new-instance v0, Lcom/jscape/util/as;

    const-wide/16 v1, 0x0

    const-wide/32 v3, 0x7fffffff

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/jscape/util/as;-><init>(JJ)V

    iput-object v0, p0, Lcom/jscape/inet/a/a/c/d;->n:Lcom/jscape/util/as;

    return-void
.end method

.method private d()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/d;->e()Ljava/net/DatagramSocket;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/a/a/c/a/l;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/d;->c:Lcom/jscape/util/h/I;

    const/high16 v3, 0x10000

    invoke-direct {v1, v0, v2, v3}, Lcom/jscape/inet/a/a/c/a/l;-><init>(Ljava/net/DatagramSocket;Lcom/jscape/util/h/I;I)V

    iput-object v1, p0, Lcom/jscape/inet/a/a/c/d;->o:Lcom/jscape/inet/a/a/c/a/w;

    return-void
.end method

.method private e()Ljava/net/DatagramSocket;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/net/DatagramSocket;

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/d;->m:Ljava/net/InetSocketAddress;

    invoke-direct {v0, v1}, Ljava/net/DatagramSocket;-><init>(Ljava/net/SocketAddress;)V

    invoke-static {}, Lcom/jscape/inet/a/a/c/d;->k()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/net/DatagramSocket;->setReuseAddress(Z)V

    sget-object v2, Lcom/jscape/inet/a/a/c/d;->a:Lcom/jscape/util/Time;

    invoke-virtual {v2}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {v0, v2}, Ljava/net/DatagramSocket;->setSoTimeout(I)V

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/d;->k:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/d;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/net/DatagramSocket;->setSendBufferSize(I)V

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/d;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/net/DatagramSocket;->setReceiveBufferSize(I)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/d;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/d;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/net/DatagramSocket;->getLocalSocketAddress()Ljava/net/SocketAddress;

    move-result-object v1

    check-cast v1, Ljava/net/InetSocketAddress;

    iput-object v1, p0, Lcom/jscape/inet/a/a/c/d;->m:Ljava/net/InetSocketAddress;

    return-object v0
.end method

.method private g()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/d;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/d;->p:Lcom/jscape/inet/a/a/c/a/o;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/inet/a/a/c/a/o;->close()V

    :cond_1
    return-void
.end method

.method private h()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/d;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/d;->o:Lcom/jscape/inet/a/a/c/a/w;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-interface {v1}, Lcom/jscape/inet/a/a/c/a/w;->close()V

    :cond_1
    return-void
.end method

.method private i()V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/a/a/c/d;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/d;->d:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/d;->d:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/a/c/d;->r:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v0, v0, v3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jscape/inet/a/a/c/d;->m:Ljava/net/InetSocketAddress;

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private j()V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/a/a/c/d;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/d;->d:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/d;->d:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/a/c/d;->r:[Ljava/lang/String;

    const/16 v3, 0x9

    aget-object v0, v0, v3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jscape/inet/a/a/c/d;->m:Ljava/net/InetSocketAddress;

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public static k()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/inet/a/a/c/d;->q:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/a/a/c/d;->i:I

    return v0
.end method

.method public a(ILjava/io/InputStream;Ljava/net/InetSocketAddress;Lcom/jscape/inet/a/a/c/a/i;)Lcom/jscape/inet/a/a/c/a/E;
    .locals 15

    move-object v0, p0

    iget v1, v0, Lcom/jscape/inet/a/a/c/d;->e:I

    move-object/from16 v12, p4

    invoke-interface {v12, v1}, Lcom/jscape/inet/a/a/c/a/i;->a(I)V

    new-instance v1, Lcom/jscape/inet/a/a/c/a/A;

    iget-object v6, v0, Lcom/jscape/inet/a/a/c/d;->p:Lcom/jscape/inet/a/a/c/a/o;

    iget v7, v0, Lcom/jscape/inet/a/a/c/d;->f:I

    iget v8, v0, Lcom/jscape/inet/a/a/c/d;->g:I

    iget v9, v0, Lcom/jscape/inet/a/a/c/d;->h:I

    iget v10, v0, Lcom/jscape/inet/a/a/c/d;->i:I

    iget-object v11, v0, Lcom/jscape/inet/a/a/c/d;->l:Lcom/jscape/util/Time;

    iget-object v14, v0, Lcom/jscape/inet/a/a/c/d;->d:Ljava/util/logging/Logger;

    const/4 v13, 0x0

    move-object v2, v1

    move/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-direct/range {v2 .. v14}, Lcom/jscape/inet/a/a/c/a/A;-><init>(ILjava/io/InputStream;Ljava/net/InetSocketAddress;Lcom/jscape/inet/a/a/c/a/o;IIIILcom/jscape/util/Time;Lcom/jscape/inet/a/a/c/a/i;ZLjava/util/logging/Logger;)V

    return-object v1
.end method

.method public a(ILjava/io/OutputStream;Ljava/net/InetSocketAddress;)Lcom/jscape/inet/a/a/c/a/E;
    .locals 12

    new-instance v11, Lcom/jscape/inet/a/a/c/a/x;

    iget-object v5, p0, Lcom/jscape/inet/a/a/c/d;->p:Lcom/jscape/inet/a/a/c/a/o;

    iget v6, p0, Lcom/jscape/inet/a/a/c/d;->i:I

    iget v7, p0, Lcom/jscape/inet/a/a/c/d;->j:I

    iget-object v8, p0, Lcom/jscape/inet/a/a/c/d;->l:Lcom/jscape/util/Time;

    iget-object v10, p0, Lcom/jscape/inet/a/a/c/d;->d:Ljava/util/logging/Logger;

    const/4 v3, 0x0

    const/4 v9, 0x1

    move-object v0, v11

    move v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v10}, Lcom/jscape/inet/a/a/c/a/x;-><init>(ILjava/io/OutputStream;ZLjava/net/InetSocketAddress;Lcom/jscape/inet/a/a/c/a/o;IILcom/jscape/util/Time;ZLjava/util/logging/Logger;)V

    return-object v11
.end method

.method public a(Ljava/io/InputStream;Ljava/net/InetSocketAddress;Lcom/jscape/inet/a/a/c/a/i;)Lcom/jscape/inet/a/a/c/a/E;
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/jscape/inet/a/a/c/d;->n:Lcom/jscape/util/as;

    invoke-virtual {v1}, Lcom/jscape/util/as;->c()J

    move-result-wide v1

    long-to-int v4, v1

    invoke-static {}, Lcom/jscape/inet/a/a/c/d;->k()Ljava/lang/String;

    iget v1, v0, Lcom/jscape/inet/a/a/c/d;->e:I

    move-object/from16 v2, p3

    invoke-interface {v2, v1}, Lcom/jscape/inet/a/a/c/a/i;->a(I)V

    new-instance v1, Lcom/jscape/inet/a/a/c/a/A;

    iget-object v7, v0, Lcom/jscape/inet/a/a/c/d;->p:Lcom/jscape/inet/a/a/c/a/o;

    iget v8, v0, Lcom/jscape/inet/a/a/c/d;->f:I

    iget v9, v0, Lcom/jscape/inet/a/a/c/d;->g:I

    iget v10, v0, Lcom/jscape/inet/a/a/c/d;->h:I

    iget v11, v0, Lcom/jscape/inet/a/a/c/d;->i:I

    iget-object v12, v0, Lcom/jscape/inet/a/a/c/d;->l:Lcom/jscape/util/Time;

    iget-object v15, v0, Lcom/jscape/inet/a/a/c/d;->d:Ljava/util/logging/Logger;

    const/4 v14, 0x1

    move-object v3, v1

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v13, p3

    invoke-direct/range {v3 .. v15}, Lcom/jscape/inet/a/a/c/a/A;-><init>(ILjava/io/InputStream;Ljava/net/InetSocketAddress;Lcom/jscape/inet/a/a/c/a/o;IIIILcom/jscape/util/Time;Lcom/jscape/inet/a/a/c/a/i;ZLjava/util/logging/Logger;)V

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v2

    if-nez v2, :cond_0

    const-string v2, "hgs4Rb"

    invoke-static {v2}, Lcom/jscape/inet/a/a/c/d;->b(Ljava/lang/String;)V

    :cond_0
    return-object v1
.end method

.method public a(Ljava/io/OutputStream;Ljava/net/InetSocketAddress;)Lcom/jscape/inet/a/a/c/a/E;
    .locals 13

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/d;->n:Lcom/jscape/util/as;

    invoke-virtual {v0}, Lcom/jscape/util/as;->c()J

    move-result-wide v0

    long-to-int v3, v0

    new-instance v0, Lcom/jscape/inet/a/a/c/a/x;

    iget-object v7, p0, Lcom/jscape/inet/a/a/c/d;->p:Lcom/jscape/inet/a/a/c/a/o;

    iget v8, p0, Lcom/jscape/inet/a/a/c/d;->i:I

    iget v9, p0, Lcom/jscape/inet/a/a/c/d;->j:I

    iget-object v10, p0, Lcom/jscape/inet/a/a/c/d;->l:Lcom/jscape/util/Time;

    iget-object v12, p0, Lcom/jscape/inet/a/a/c/d;->d:Ljava/util/logging/Logger;

    const/4 v5, 0x1

    const/4 v11, 0x0

    move-object v2, v0

    move-object v4, p1

    move-object v6, p2

    invoke-direct/range {v2 .. v12}, Lcom/jscape/inet/a/a/c/a/x;-><init>(ILjava/io/OutputStream;ZLjava/net/InetSocketAddress;Lcom/jscape/inet/a/a/c/a/o;IILcom/jscape/util/Time;ZLjava/util/logging/Logger;)V

    return-object v0
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/o;)V
    .locals 2

    new-instance v0, Lcom/jscape/inet/a/a/c/a/b/n;

    iget v1, p1, Lcom/jscape/inet/a/a/c/a/b/o;->a:I

    invoke-direct {v0, v1}, Lcom/jscape/inet/a/a/c/a/b/n;-><init>(I)V

    iget-object p1, p1, Lcom/jscape/inet/a/a/c/a/b/o;->b:Ljava/net/InetSocketAddress;

    iput-object p1, v0, Lcom/jscape/inet/a/a/c/a/b/n;->c:Ljava/net/InetSocketAddress;

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/a/c/d;->a(Lcom/jscape/inet/a/a/c/a/b/i;)V

    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/o;Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 0

    :try_start_0
    invoke-virtual {p2, p0}, Lcom/jscape/inet/a/a/c/a/b/i;->a(Lcom/jscape/inet/a/a/c/a/b/u;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/o;Ljava/lang/Throwable;)V
    .locals 1

    sget-object p1, Lcom/jscape/inet/a/a/c/d;->r:[Ljava/lang/String;

    const/4 v0, 0x5

    aget-object p1, p1, v0

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/a/a/c/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public a(Ljava/net/InetSocketAddress;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/d;->m:Ljava/net/InetSocketAddress;

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/d;->c()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/d;->d()V

    invoke-virtual {p0}, Lcom/jscape/inet/a/a/c/d;->f()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/d;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {p0}, Lcom/jscape/inet/a/a/c/d;->close()V

    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method public b()Ljava/net/InetSocketAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/d;->m:Ljava/net/InetSocketAddress;

    return-object v0
.end method

.method public close()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/d;->g()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/d;->h()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/d;->j()V

    return-void
.end method

.method protected f()V
    .locals 2

    new-instance v0, Lcom/jscape/inet/a/a/c/a/o;

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/d;->o:Lcom/jscape/inet/a/a/c/a/w;

    invoke-direct {v0, v1}, Lcom/jscape/inet/a/a/c/a/o;-><init>(Lcom/jscape/inet/a/a/c/a/w;)V

    iput-object v0, p0, Lcom/jscape/inet/a/a/c/d;->p:Lcom/jscape/inet/a/a/c/a/o;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/a/a/c/a/o;->a(Lcom/jscape/inet/a/a/c/a/r;)V

    return-void
.end method
