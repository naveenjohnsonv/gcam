.class public Lcom/jscape/inet/a/a/c/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/a/a/c/g;


# static fields
.field private static final a:Lcom/jscape/inet/a/a/c/a/a/i;

.field private static final b:I = 0x41c

.field private static final c:I

.field private static final d:I = 0x1000

.field private static final e:I = 0x400000

.field private static final f:I = 0x4

.field private static final g:I = 0x4

.field private static final n:[Ljava/lang/String;


# instance fields
.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "oe1\u0019a\u00190\rt\'\\|\u0019:H`u[`\u0017+FwuOm\u0014=H*\u0012\u0001$%Xo\u0013-Yw\u0005\\~:$Bg>\u0004\u001foe1\u0019a\u00190\rg:T|\u0014-Ya1\u0019n\u0014\'No&\u0019z\u0019$Xa{\u001doe1\u0019\u007f\u0017+Fa!\u0019n\r.Ka\'\u0019\u007f\u00112H$#X`\r-\u0003\u000eoe1\u0019A,\u001d\rr4Uy\u001df\u0011\u0001$%Xo\u0013-Y@4Mm+!Wah\u0013\u0001$&Vo\u0013-YF _j\u001d:~m/\\1\u001coe1\u0019|\u0019+Fa!J,\u0008-_$7Uc\u001b#\rr4Uy\u001df"

    const/16 v4, 0xc1

    const/16 v5, 0x1e

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x60

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x29

    const/16 v3, 0xd

    const-string v5, "\u0017\u0019J1\u001fvMq\u0003D1\u00059\u001b\u0013\u0019Me\u0000eW:\u001d]e\u0014e@0XZ,\na\u0014\'\u0019E0\u0015*"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x1c

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/a/a/c/e;->n:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/a/a/c/a/a/i;

    new-array v1, v2, [Lcom/jscape/inet/a/a/c/a/a/j;

    invoke-direct {v0, v1}, Lcom/jscape/inet/a/a/c/a/a/i;-><init>([Lcom/jscape/inet/a/a/c/a/a/j;)V

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/a/k;->a(Lcom/jscape/inet/a/a/c/a/a/i;)Lcom/jscape/inet/a/a/c/a/a/i;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/a/c/e;->a:Lcom/jscape/inet/a/a/c/a/a/i;

    const/16 v0, 0x41c

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/k;->a(I)I

    move-result v0

    sput v0, Lcom/jscape/inet/a/a/c/e;->c:I

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x28

    goto :goto_4

    :cond_4
    const/16 v1, 0x18

    goto :goto_4

    :cond_5
    const/16 v1, 0x6c

    goto :goto_4

    :cond_6
    const/16 v1, 0x59

    goto :goto_4

    :cond_7
    const/16 v1, 0x35

    goto :goto_4

    :cond_8
    const/16 v1, 0x64

    goto :goto_4

    :cond_9
    const/16 v1, 0x4d

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_2
.end method

.method public constructor <init>(IIIIII)V
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    int-to-long v11, v1

    sget-object v14, Lcom/jscape/inet/a/a/c/e;->n:[Ljava/lang/String;

    const/4 v7, 0x4

    aget-object v7, v14, v7

    const-wide/16 v9, 0x0

    invoke-static {v11, v12, v9, v10, v7}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput v1, v0, Lcom/jscape/inet/a/a/c/e;->h:I

    int-to-long v7, v2

    const/16 v1, 0x9

    aget-object v13, v14, v1

    const-wide/16 v15, 0x0

    move-wide v5, v9

    move-wide v9, v15

    invoke-static/range {v7 .. v13}, Lcom/jscape/util/aq;->b(JJJLjava/lang/String;)V

    iput v2, v0, Lcom/jscape/inet/a/a/c/e;->i:I

    int-to-long v1, v3

    const/4 v7, 0x7

    aget-object v7, v14, v7

    invoke-static {v1, v2, v5, v6, v7}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput v3, v0, Lcom/jscape/inet/a/a/c/e;->j:I

    int-to-long v1, v4

    const/4 v3, 0x0

    aget-object v3, v14, v3

    invoke-static {v1, v2, v5, v6, v3}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput v4, v0, Lcom/jscape/inet/a/a/c/e;->k:I

    move/from16 v1, p5

    move-wide v2, v5

    int-to-long v4, v1

    const/4 v6, 0x2

    aget-object v6, v14, v6

    invoke-static {v4, v5, v2, v3, v6}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput v1, v0, Lcom/jscape/inet/a/a/c/e;->l:I

    move/from16 v1, p6

    int-to-long v4, v1

    const/4 v6, 0x3

    aget-object v6, v14, v6

    invoke-static {v4, v5, v2, v3, v6}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput v1, v0, Lcom/jscape/inet/a/a/c/e;->m:I

    return-void
.end method

.method public static a()Lcom/jscape/inet/a/a/c/e;
    .locals 8

    new-instance v7, Lcom/jscape/inet/a/a/c/e;

    sget v2, Lcom/jscape/inet/a/a/c/e;->c:I

    const/16 v1, 0x41c

    const/16 v3, 0x1000

    const/4 v4, 0x4

    const/4 v5, 0x4

    const/high16 v6, 0x400000

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/a/a/c/e;-><init>(IIIIII)V

    return-object v7
.end method


# virtual methods
.method public a(ILcom/jscape/util/Time;Ljava/util/logging/Logger;)Lcom/jscape/inet/a/a/c/f;
    .locals 12

    new-instance v11, Lcom/jscape/inet/a/a/c/d;

    sget-object v1, Lcom/jscape/inet/a/a/c/e;->a:Lcom/jscape/inet/a/a/c/a/a/i;

    iget v2, p0, Lcom/jscape/inet/a/a/c/e;->h:I

    iget v3, p0, Lcom/jscape/inet/a/a/c/e;->i:I

    iget v4, p0, Lcom/jscape/inet/a/a/c/e;->j:I

    iget v5, p0, Lcom/jscape/inet/a/a/c/e;->k:I

    iget v7, p0, Lcom/jscape/inet/a/a/c/e;->l:I

    iget v0, p0, Lcom/jscape/inet/a/a/c/e;->m:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object v0, v11

    move v6, p1

    move-object v9, p2

    move-object v10, p3

    invoke-direct/range {v0 .. v10}, Lcom/jscape/inet/a/a/c/d;-><init>(Lcom/jscape/util/h/I;IIIIIILjava/lang/Integer;Lcom/jscape/util/Time;Ljava/util/logging/Logger;)V

    return-object v11
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/a/a/c/e;->n:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/a/a/c/e;->h:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/a/a/c/e;->i:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/a/a/c/e;->j:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/a/a/c/e;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
