.class public Lcom/jscape/inet/a/a/c/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/a/a/c/b;


# static fields
.field private static final a:Lcom/jscape/util/Time;

.field private static final b:J = 0x186a0L

.field private static final c:Lcom/jscape/util/Time;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, 0xc8

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->millis(J)Lcom/jscape/util/Time;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/a/c/c;->a:Lcom/jscape/util/Time;

    const-wide/16 v0, 0x3c

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->seconds(J)Lcom/jscape/util/Time;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/a/c/c;->c:Lcom/jscape/util/Time;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/util/A;)Lcom/jscape/inet/a/a/c/a/i;
    .locals 2

    new-instance v0, Lcom/jscape/inet/a/a/c/a/j;

    sget-object v1, Lcom/jscape/inet/a/a/c/c;->a:Lcom/jscape/util/Time;

    invoke-direct {v0, p1, v1}, Lcom/jscape/inet/a/a/c/a/j;-><init>(Lcom/jscape/util/A;Lcom/jscape/util/Time;)V

    return-object v0
.end method

.method public b(Lcom/jscape/util/A;)Lcom/jscape/inet/a/a/c/a/i;
    .locals 7

    new-instance v6, Lcom/jscape/inet/a/a/c/a/s;

    sget-object v2, Lcom/jscape/inet/a/a/c/c;->a:Lcom/jscape/util/Time;

    sget-object v5, Lcom/jscape/inet/a/a/c/c;->c:Lcom/jscape/util/Time;

    const-wide/32 v3, 0x186a0

    move-object v0, v6

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/a/a/c/a/s;-><init>(Lcom/jscape/util/A;Lcom/jscape/util/Time;JLcom/jscape/util/Time;)V

    return-object v6
.end method
