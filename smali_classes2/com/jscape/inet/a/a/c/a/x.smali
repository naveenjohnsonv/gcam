.class public Lcom/jscape/inet/a/a/c/a/x;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/a/a/c/a/E;
.implements Lcom/jscape/inet/a/a/c/a/b/d;
.implements Lcom/jscape/inet/a/a/c/a/b/b;
.implements Lcom/jscape/inet/a/a/c/a/b/v;
.implements Lcom/jscape/inet/a/a/c/a/b/f;
.implements Lcom/jscape/inet/a/a/c/a/b/g;


# static fields
.field private static final a:Lcom/jscape/util/Time;

.field private static final b:Lcom/jscape/util/Time;

.field private static final c:I = 0x64

.field private static final t:[Ljava/lang/String;


# instance fields
.field private final d:I

.field private final e:Ljava/io/OutputStream;

.field private f:Z

.field private volatile g:Ljava/net/InetSocketAddress;

.field private final h:Lcom/jscape/inet/a/a/c/a/o;

.field private final i:Lcom/jscape/inet/a/a/c/a/y;

.field private final j:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Lcom/jscape/inet/a/a/c/a/z;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/jscape/util/Time;

.field private final l:Ljava/util/concurrent/atomic/AtomicLong;

.field private final m:Ljava/util/logging/Logger;

.field private volatile n:Z

.field private volatile o:Z

.field private volatile p:Ljava/io/IOException;

.field private volatile q:J

.field private volatile r:J

.field private s:Lcom/jscape/inet/a/a/c/a/F;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, " Pe\u0003\u001eEu\u0000N \u0014\rCs\u001d\u000e\u0017.fT!_U}\u001bA \u0002\u001a_x\nR \u0014\rCs\u001d\u000e\u0015*Rr\u001e\r\u0011o\nNd\u0018\u0011V<\u001fAc\u001a\u001aE2% Ut\u0016\u0010Xr\u0008\u0000A7+a<\u001fAc\u001a\u001aE<4\u0005sQC\u001c\"O\u0005s,E\u00119\u001c\u000e\u0012 Pe\u0003\u001eEu\u0000N \u0005\u0016\\y\u0000Ut_"

    const/16 v4, 0x77

    const/16 v5, 0x10

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/4 v8, 0x1

    add-int/2addr v6, v8

    add-int v9, v6, v5

    invoke-virtual {v3, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0x39

    move v12, v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    array-length v13, v9

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v9}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v12}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v11, :cond_1

    add-int/lit8 v11, v7, 0x1

    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v11

    goto :goto_0

    :cond_0
    const/16 v4, 0x3a

    const/16 v3, 0x14

    const-string v5, "H<\u0001v}3\u0001o&\u0017g-3\u000f~9\u0017w7c%T<\u0011l`*\u0000zr3EY\u0013Nm3\u0011hh7NFw\u0001#1nP=w\u0001^7cKn|"

    move v7, v11

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v7, 0x1

    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v11

    :goto_3
    const/16 v12, 0x4b

    add-int/2addr v6, v8

    add-int v9, v6, v5

    invoke-virtual {v3, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/a/a/c/a/x;->t:[Ljava/lang/String;

    const-wide/16 v0, 0x5

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->millis(J)Lcom/jscape/util/Time;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/a/c/a/x;->a:Lcom/jscape/util/Time;

    const-wide/16 v0, 0xa

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->millis(J)Lcom/jscape/util/Time;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/a/c/a/x;->b:Lcom/jscape/util/Time;

    return-void

    :cond_3
    aget-char v15, v9, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v8, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v1, 0x25

    goto :goto_4

    :cond_4
    const/16 v1, 0x8

    goto :goto_4

    :cond_5
    const/16 v1, 0x46

    goto :goto_4

    :cond_6
    const/16 v1, 0x48

    goto :goto_4

    :cond_7
    move v1, v10

    goto :goto_4

    :cond_8
    const/16 v1, 0x19

    goto :goto_4

    :cond_9
    const/16 v1, 0x56

    :goto_4
    xor-int/2addr v1, v12

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v9, v14

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2
.end method

.method public constructor <init>(ILjava/io/OutputStream;ZLjava/net/InetSocketAddress;Lcom/jscape/inet/a/a/c/a/o;IILcom/jscape/util/Time;ZLjava/util/logging/Logger;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jscape/inet/a/a/c/a/x;->d:I

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object p1

    iput-object p2, p0, Lcom/jscape/inet/a/a/c/a/x;->e:Ljava/io/OutputStream;

    iput-boolean p3, p0, Lcom/jscape/inet/a/a/c/a/x;->f:Z

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/a/a/c/a/x;->g:Ljava/net/InetSocketAddress;

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/inet/a/a/c/a/x;->h:Lcom/jscape/inet/a/a/c/a/o;

    new-instance p2, Lcom/jscape/inet/a/a/c/a/y;

    invoke-direct {p2, p6}, Lcom/jscape/inet/a/a/c/a/y;-><init>(I)V

    iput-object p2, p0, Lcom/jscape/inet/a/a/c/a/x;->i:Lcom/jscape/inet/a/a/c/a/y;

    invoke-static {p8}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p8, p0, Lcom/jscape/inet/a/a/c/a/x;->k:Lcom/jscape/util/Time;

    new-instance p2, Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-direct {p2, p7}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object p2, p0, Lcom/jscape/inet/a/a/c/a/x;->j:Ljava/util/concurrent/BlockingQueue;

    const/4 p2, 0x1

    if-nez p1, :cond_1

    if-nez p9, :cond_0

    move p9, p2

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move p3, p9

    :goto_1
    iput-boolean p3, p0, Lcom/jscape/inet/a/a/c/a/x;->n:Z

    new-instance p3, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {p3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    iput-object p3, p0, Lcom/jscape/inet/a/a/c/a/x;->l:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {p10}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p10, p0, Lcom/jscape/inet/a/a/c/a/x;->m:Ljava/util/logging/Logger;

    if-eqz p1, :cond_2

    new-array p1, p2, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_2
    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a(II)V
    .locals 2

    new-instance v0, Lcom/jscape/inet/a/a/c/a/b/j;

    iget v1, p0, Lcom/jscape/inet/a/a/c/a/x;->d:I

    invoke-direct {v0, v1, p1, p2}, Lcom/jscape/inet/a/a/c/a/b/j;-><init>(III)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/a/c/a/x;->d(Lcom/jscape/inet/a/a/c/a/b/i;)V

    return-void
.end method

.method private a(Lcom/jscape/inet/a/a/c/a/b/k;Lcom/jscape/inet/a/a/c/a/z;)V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/jscape/inet/a/a/c/a/z;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/jscape/inet/a/a/c/a/z;->a()I

    move-result v1

    iget v2, p1, Lcom/jscape/inet/a/a/c/a/b/k;->f:I

    invoke-direct {p0, v1, v2}, Lcom/jscape/inet/a/a/c/a/x;->a(II)V

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/a/a/c/a/x;->b(Lcom/jscape/inet/a/a/c/a/b/k;Lcom/jscape/inet/a/a/c/a/z;)V

    :cond_1
    return-void
.end method

.method private a(Lcom/jscape/inet/a/a/c/a/z;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->e:Ljava/io/OutputStream;

    invoke-virtual {p1, v1}, Lcom/jscape/inet/a/a/c/a/z;->a(Ljava/io/OutputStream;)V

    if-eqz v0, :cond_2

    :cond_1
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/x;->e()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/a/c/a/x;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method private a(Lcom/jscape/inet/a/a/c/a/z;I)V
    .locals 5

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/x;->l:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcom/jscape/inet/a/a/c/a/x;->r:J

    int-to-long v3, p2

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/jscape/inet/a/a/c/a/x;->r:J

    if-nez v0, :cond_0

    iget-object p2, p0, Lcom/jscape/inet/a/a/c/a/x;->l:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    const-wide/16 v2, 0x64

    rem-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long p2, v0, v2

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/jscape/inet/a/a/c/a/z;->c()Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/x;->f()V

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/a/c/a/x;->t:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method private b(Lcom/jscape/inet/a/a/c/a/b/k;)V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->i:Lcom/jscape/inet/a/a/c/a/y;

    iget v2, p1, Lcom/jscape/inet/a/a/c/a/b/k;->e:I

    invoke-virtual {v1, v2}, Lcom/jscape/inet/a/a/c/a/y;->b(I)Z

    move-result v1

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    iget v1, p1, Lcom/jscape/inet/a/a/c/a/b/k;->e:I

    iget v2, p1, Lcom/jscape/inet/a/a/c/a/b/k;->f:I

    invoke-direct {p0, v1, v2}, Lcom/jscape/inet/a/a/c/a/x;->a(II)V

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/x;->i:Lcom/jscape/inet/a/a/c/a/y;

    iget v1, p1, Lcom/jscape/inet/a/a/c/a/b/k;->e:I

    invoke-virtual {v0, v1}, Lcom/jscape/inet/a/a/c/a/y;->c(I)Z

    move-result v1

    :cond_1
    if-eqz v1, :cond_2

    new-instance v0, Lcom/jscape/inet/a/a/c/a/z;

    iget v1, p1, Lcom/jscape/inet/a/a/c/a/b/k;->e:I

    iget v2, p1, Lcom/jscape/inet/a/a/c/a/b/k;->g:I

    invoke-direct {v0, v1, v2}, Lcom/jscape/inet/a/a/c/a/z;-><init>(II)V

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->i:Lcom/jscape/inet/a/a/c/a/y;

    invoke-virtual {v1, v0}, Lcom/jscape/inet/a/a/c/a/y;->a(Lcom/jscape/inet/a/a/c/a/z;)V

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/a/a/c/a/x;->b(Lcom/jscape/inet/a/a/c/a/b/k;Lcom/jscape/inet/a/a/c/a/z;)V

    :cond_2
    return-void
.end method

.method private b(Lcom/jscape/inet/a/a/c/a/b/k;Lcom/jscape/inet/a/a/c/a/z;)V
    .locals 3

    invoke-virtual {p2}, Lcom/jscape/inet/a/a/c/a/z;->d()[I

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/a/a/c/a/b/l;

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/x;->d:I

    invoke-virtual {p2}, Lcom/jscape/inet/a/a/c/a/z;->a()I

    move-result p2

    iget p1, p1, Lcom/jscape/inet/a/a/c/a/b/k;->f:I

    invoke-direct {v1, v2, p2, p1, v0}, Lcom/jscape/inet/a/a/c/a/b/l;-><init>(III[I)V

    invoke-direct {p0, v1}, Lcom/jscape/inet/a/a/c/a/x;->d(Lcom/jscape/inet/a/a/c/a/b/i;)V

    return-void
.end method

.method private b(Ljava/lang/Throwable;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/a/c/a/x;->t:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method private b(Lcom/jscape/inet/a/a/c/a/b/i;)Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/x;->g:Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    iget-object p1, p1, Lcom/jscape/inet/a/a/c/a/b/i;->b:Ljava/net/InetSocketAddress;

    invoke-virtual {p1}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private c(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 0

    :try_start_0
    invoke-virtual {p1, p0}, Lcom/jscape/inet/a/a/c/a/b/i;->a(Lcom/jscape/inet/a/a/c/a/b/u;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/x;->g(Lcom/jscape/inet/a/a/c/a/b/i;)V

    :goto_0
    return-void
.end method

.method private d(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/x;->g:Ljava/net/InetSocketAddress;

    iput-object v0, p1, Lcom/jscape/inet/a/a/c/a/b/i;->c:Ljava/net/InetSocketAddress;

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/x;->f(Lcom/jscape/inet/a/a/c/a/b/i;)V

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/x;->h:Lcom/jscape/inet/a/a/c/a/o;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/a/a/c/a/o;->a(Lcom/jscape/inet/a/a/c/a/b/i;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/x;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method private e()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InterruptedIOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/x;->q:J

    sget-object v2, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v0, v1, v2}, Lcom/jscape/util/am;->b(JLcom/jscape/util/u;)Z

    move-result v0
    :try_end_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/x;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/InterruptedIOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/x;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/x;->n()V

    :cond_1
    return-void
.end method

.method private e(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/a/c/a/x;->t:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v0, v0, v3

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/jscape/inet/a/a/c/a/b/i;->b:Ljava/net/InetSocketAddress;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p1, Lcom/jscape/inet/a/a/c/a/b/i;->c:Ljava/net/InetSocketAddress;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private f()V
    .locals 3

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/x;->s:Lcom/jscape/inet/a/a/c/a/F;

    iget-wide v1, p0, Lcom/jscape/inet/a/a/c/a/x;->r:J

    invoke-interface {v0, p0, v1, v2}, Lcom/jscape/inet/a/a/c/a/F;->a(Lcom/jscape/inet/a/a/c/a/E;J)V

    return-void
.end method

.method private f(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/a/c/a/x;->t:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v0, v0, v3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/jscape/inet/a/a/c/a/b/i;->b:Ljava/net/InetSocketAddress;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p1, Lcom/jscape/inet/a/a/c/a/b/i;->c:Ljava/net/InetSocketAddress;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private g()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/x;->l()V

    :cond_0
    iget-boolean v1, p0, Lcom/jscape/inet/a/a/c/a/x;->o:Z

    if-nez v1, :cond_2

    if-nez v0, :cond_1

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/a/a/c/a/x;->n:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_2

    :try_start_1
    new-instance v1, Lcom/jscape/inet/a/a/c/a/b/q;

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/x;->d:I

    invoke-direct {v1, v2}, Lcom/jscape/inet/a/a/c/a/b/q;-><init>(I)V

    invoke-direct {p0, v1}, Lcom/jscape/inet/a/a/c/a/x;->d(Lcom/jscape/inet/a/a/c/a/b/i;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/x;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/x;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/x;->m()V

    if-eqz v0, :cond_0

    :cond_2
    return-void
.end method

.method private g(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/a/a/c/a/x;->t:[Ljava/lang/String;

    const/4 v4, 0x5

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private h()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/x;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    if-nez v0, :cond_2

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->j:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_2

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/x;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/x;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/x;->i()Lcom/jscape/inet/a/a/c/a/z;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jscape/inet/a/a/c/a/x;->a(Lcom/jscape/inet/a/a/c/a/z;)V

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->i:Lcom/jscape/inet/a/a/c/a/y;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/x;->j:Ljava/util/concurrent/BlockingQueue;

    invoke-virtual {v1, v2}, Lcom/jscape/inet/a/a/c/a/y;->a(Ljava/util/concurrent/BlockingQueue;)V

    if-eqz v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/x;->e:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

.method private i()Lcom/jscape/inet/a/a/c/a/z;
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/x;->j:Ljava/util/concurrent/BlockingQueue;

    sget-object v1, Lcom/jscape/inet/a/a/c/a/x;->b:Lcom/jscape/util/Time;

    iget-wide v1, v1, Lcom/jscape/util/Time;->value:J

    sget-object v3, Lcom/jscape/inet/a/a/c/a/x;->b:Lcom/jscape/util/Time;

    iget-object v3, v3, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/a/a/c/a/z;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private j()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jscape/inet/a/a/c/a/x;->o:Z

    if-nez v0, :cond_2

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/x;->p:Ljava/io/IOException;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move v0, v1

    :goto_1
    return v0
.end method

.method private k()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->p:Ljava/io/IOException;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->p:Ljava/io/IOException;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    throw v1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/x;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/x;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method private l()V
    .locals 3

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/x;->k:Lcom/jscape/util/Time;

    invoke-virtual {v0}, Lcom/jscape/util/Time;->toMicros()J

    move-result-wide v0

    sget-object v2, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v0, v1, v2}, Lcom/jscape/util/am;->c(JLcom/jscape/util/u;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/x;->q:J

    return-void
.end method

.method private m()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    sget-object v1, Lcom/jscape/inet/a/a/c/a/x;->a:Lcom/jscape/util/Time;

    invoke-virtual {v1}, Lcom/jscape/util/Time;->toMicros()J

    move-result-wide v1

    sget-object v3, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v1, v2, v3}, Lcom/jscape/util/am;->f(JLcom/jscape/util/u;)V

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/x;->q:J

    sget-object v2, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v0, v1, v2}, Lcom/jscape/util/am;->b(JLcom/jscape/util/u;)Z

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/x;->n()V

    :cond_1
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/x;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/x;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method private n()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InterruptedIOException;
        }
    .end annotation

    new-instance v0, Ljava/io/InterruptedIOException;

    sget-object v1, Lcom/jscape/inet/a/a/c/a/x;->t:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private o()V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/jscape/inet/a/a/c/a/x;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/x;->e:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/inet/a/a/c/a/F;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/x;->s:Lcom/jscape/inet/a/a/c/a/F;

    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/x;->l:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v0, 0x0

    invoke-virtual {p1, v0, v1}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/x;->h:Lcom/jscape/inet/a/a/c/a/o;

    iget v0, p0, Lcom/jscape/inet/a/a/c/a/x;->d:I

    invoke-virtual {p1, v0, p0}, Lcom/jscape/inet/a/a/c/a/o;->a(ILcom/jscape/inet/a/a/c/a/q;)V

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/x;->g()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/x;->h()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/x;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/x;->h:Lcom/jscape/inet/a/a/c/a/o;

    iget v0, p0, Lcom/jscape/inet/a/a/c/a/x;->d:I

    invoke-virtual {p1, v0}, Lcom/jscape/inet/a/a/c/a/o;->a(I)V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/x;->o()V

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/x;->b(Ljava/lang/Throwable;)V

    new-instance v0, Lcom/jscape/inet/a/a/c/a/b/p;

    iget v1, p0, Lcom/jscape/inet/a/a/c/a/x;->d:I

    invoke-direct {v0, v1}, Lcom/jscape/inet/a/a/c/a/b/p;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/a/c/a/x;->d(Lcom/jscape/inet/a/a/c/a/b/i;)V

    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/x;->h:Lcom/jscape/inet/a/a/c/a/o;

    iget v1, p0, Lcom/jscape/inet/a/a/c/a/x;->d:I

    invoke-virtual {v0, v1}, Lcom/jscape/inet/a/a/c/a/o;->a(I)V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/x;->o()V

    throw p1
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/x;->e(Lcom/jscape/inet/a/a/c/a/b/i;)V

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/x;->b(Lcom/jscape/inet/a/a/c/a/b/i;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/a/a/c/a/x;->n:Z

    iget-object v0, p1, Lcom/jscape/inet/a/a/c/a/b/i;->b:Ljava/net/InetSocketAddress;

    iput-object v0, p0, Lcom/jscape/inet/a/a/c/a/x;->g:Ljava/net/InetSocketAddress;

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/x;->l()V

    :cond_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/x;->c(Lcom/jscape/inet/a/a/c/a/b/i;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/k;)V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->i:Lcom/jscape/inet/a/a/c/a/y;

    iget v2, p1, Lcom/jscape/inet/a/a/c/a/b/k;->e:I

    invoke-virtual {v1, v2}, Lcom/jscape/inet/a/a/c/a/y;->a(I)Lcom/jscape/inet/a/a/c/a/z;

    move-result-object v1

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    invoke-direct {p0, p1, v1}, Lcom/jscape/inet/a/a/c/a/x;->a(Lcom/jscape/inet/a/a/c/a/b/k;Lcom/jscape/inet/a/a/c/a/z;)V

    :cond_0
    if-eqz v0, :cond_2

    :cond_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/x;->b(Lcom/jscape/inet/a/a/c/a/b/k;)V

    :cond_2
    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/m;)V
    .locals 5

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/x;->i:Lcom/jscape/inet/a/a/c/a/y;

    iget v1, p1, Lcom/jscape/inet/a/a/c/a/b/m;->e:I

    invoke-virtual {v0, v1}, Lcom/jscape/inet/a/a/c/a/y;->a(I)Lcom/jscape/inet/a/a/c/a/z;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    iget v2, p1, Lcom/jscape/inet/a/a/c/a/b/m;->f:I

    iget-object v3, p1, Lcom/jscape/inet/a/a/c/a/b/m;->h:[B

    invoke-virtual {v0, v2, v3}, Lcom/jscape/inet/a/a/c/a/z;->a(I[B)Z

    move-result v2

    if-eqz v1, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/jscape/inet/a/a/c/a/x;->i:Lcom/jscape/inet/a/a/c/a/y;

    iget v4, p1, Lcom/jscape/inet/a/a/c/a/b/m;->e:I

    invoke-virtual {v3, v4}, Lcom/jscape/inet/a/a/c/a/y;->c(I)Z

    move-result v3

    if-nez v1, :cond_3

    if-eqz v3, :cond_2

    new-instance v0, Lcom/jscape/inet/a/a/c/a/z;

    iget v1, p1, Lcom/jscape/inet/a/a/c/a/b/m;->e:I

    iget v2, p1, Lcom/jscape/inet/a/a/c/a/b/m;->g:I

    invoke-direct {v0, v1, v2}, Lcom/jscape/inet/a/a/c/a/z;-><init>(II)V

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/x;->i:Lcom/jscape/inet/a/a/c/a/y;

    invoke-virtual {v1, v0}, Lcom/jscape/inet/a/a/c/a/y;->a(Lcom/jscape/inet/a/a/c/a/z;)V

    iget v1, p1, Lcom/jscape/inet/a/a/c/a/b/m;->f:I

    iget-object v2, p1, Lcom/jscape/inet/a/a/c/a/b/m;->h:[B

    invoke-virtual {v0, v1, v2}, Lcom/jscape/inet/a/a/c/a/z;->a(I[B)Z

    move-result v2

    :cond_2
    move v3, v2

    :cond_3
    if-eqz v3, :cond_4

    iget-object p1, p1, Lcom/jscape/inet/a/a/c/a/b/m;->h:[B

    array-length p1, p1

    invoke-direct {p0, v0, p1}, Lcom/jscape/inet/a/a/c/a/x;->a(Lcom/jscape/inet/a/a/c/a/z;I)V

    :cond_4
    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/x;->i:Lcom/jscape/inet/a/a/c/a/y;

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/x;->j:Ljava/util/concurrent/BlockingQueue;

    invoke-virtual {p1, v0}, Lcom/jscape/inet/a/a/c/a/y;->a(Ljava/util/concurrent/BlockingQueue;)V

    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/o;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/x;->i:Lcom/jscape/inet/a/a/c/a/y;

    invoke-virtual {v0}, Lcom/jscape/inet/a/a/c/a/y;->a()Z

    move-result v0

    if-nez p1, :cond_0

    if-eqz v0, :cond_2

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/x;->j:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {p1}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v0

    :cond_0
    if-eqz v0, :cond_2

    new-instance p1, Lcom/jscape/inet/a/a/c/a/b/n;

    iget v0, p0, Lcom/jscape/inet/a/a/c/a/x;->d:I

    invoke-direct {p1, v0}, Lcom/jscape/inet/a/a/c/a/b/n;-><init>(I)V

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/x;->d(Lcom/jscape/inet/a/a/c/a/b/i;)V

    :cond_1
    invoke-virtual {p0}, Lcom/jscape/inet/a/a/c/a/x;->close()V

    :cond_2
    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/p;)V
    .locals 2

    new-instance p1, Ljava/io/IOException;

    sget-object v0, Lcom/jscape/inet/a/a/c/a/x;->t:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/x;->p:Ljava/io/IOException;

    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/r;)V
    .locals 4

    new-instance v0, Lcom/jscape/inet/a/a/c/a/b/s;

    iget v1, p0, Lcom/jscape/inet/a/a/c/a/x;->d:I

    iget-wide v2, p1, Lcom/jscape/inet/a/a/c/a/b/r;->e:J

    invoke-direct {v0, v1, v2, v3}, Lcom/jscape/inet/a/a/c/a/b/s;-><init>(IJ)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/a/c/a/x;->d(Lcom/jscape/inet/a/a/c/a/b/i;)V

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/a/a/c/a/x;->d:I

    return v0
.end method

.method public close()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/a/a/c/a/x;->o:Z

    return-void
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/x;->r:J

    return-wide v0
.end method
