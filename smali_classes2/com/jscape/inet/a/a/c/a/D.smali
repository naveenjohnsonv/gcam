.class public Lcom/jscape/inet/a/a/c/a/D;
.super Ljava/lang/Object;


# static fields
.field private static final l:[Ljava/lang/String;


# instance fields
.field private final a:I

.field private final b:[Lcom/jscape/inet/a/a/c/a/b/m;

.field private final c:Lcom/jscape/inet/a/a/c/a/b/k;

.field private final d:Ljava/util/concurrent/locks/Lock;

.field private e:J

.field private f:I

.field private g:I

.field private h:Lcom/jscape/util/b/m;

.field private i:Z

.field private j:J

.field private k:J


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x8

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, -0x1

    const/4 v4, 0x0

    const-string v5, ".*xr:;\u0013vy&\u007f<>\u0011vb5\u0019.*dr*$2c~iC83\u001dg~[v7$\"kgm.\u0016@kl3;<\u0019aa(z74\u0013z*~r5%\u0013,\u0014Qofw0>\u0011@fgp2p\rkdlv!m\u000e.*d|*$&cicv-#K\u0007.*{z#5K"

    const/16 v6, 0x6e

    move v7, v3

    move v9, v4

    const/16 v8, 0x11

    :goto_0
    const/16 v10, 0x1b

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v13, v3

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    move v15, v4

    :goto_2
    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    add-int/lit8 v12, v9, 0x1

    if-eqz v13, :cond_1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v12

    goto :goto_0

    :cond_0
    const/16 v6, 0x2d

    const/16 v5, 0x1e

    const-string v7, "\\X\u0016\u0000XVG\u001f\u0015\n\rNVa\u0014(\u001b\u0002@Gp#\u001d\u0014\u0015\u007fKi\u0015E\u000e\\X\n\u0000HIa\u00041\u0014\u0005NZ9"

    move v8, v5

    move-object v5, v7

    move v9, v12

    move v7, v3

    goto :goto_3

    :cond_1
    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v12

    :goto_3
    const/16 v10, 0x69

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v13, v4

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/a/a/c/a/D;->l:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v0, v15, 0x7

    if-eqz v0, :cond_9

    if-eq v0, v11, :cond_8

    const/4 v2, 0x2

    if-eq v0, v2, :cond_7

    const/4 v2, 0x3

    if-eq v0, v2, :cond_6

    const/4 v2, 0x4

    if-eq v0, v2, :cond_5

    const/4 v2, 0x5

    if-eq v0, v2, :cond_4

    const/16 v0, 0x6d

    goto :goto_4

    :cond_4
    const/16 v0, 0x4b

    goto :goto_4

    :cond_5
    const/16 v0, 0x42

    goto :goto_4

    :cond_6
    const/16 v0, 0x8

    goto :goto_4

    :cond_7
    const/16 v0, 0x13

    goto :goto_4

    :cond_8
    const/16 v0, 0x11

    goto :goto_4

    :cond_9
    const/16 v0, 0x19

    :goto_4
    xor-int/2addr v0, v10

    xor-int v0, v16, v0

    int-to-char v0, v0

    aput-char v0, v12, v15

    add-int/lit8 v15, v15, 0x1

    const/16 v0, 0x8

    goto :goto_2
.end method

.method public constructor <init>(II[Lcom/jscape/inet/a/a/c/a/b/m;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    int-to-long v0, p2

    sget-object v2, Lcom/jscape/inet/a/a/c/a/D;->l:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const-wide/16 v3, 0x0

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    iput p2, p0, Lcom/jscape/inet/a/a/c/a/D;->a:I

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/a/a/c/a/D;->b:[Lcom/jscape/inet/a/a/c/a/b/m;

    new-instance v0, Lcom/jscape/inet/a/a/c/a/b/k;

    array-length p3, p3

    const/4 v1, -0x1

    invoke-direct {v0, p1, p2, v1, p3}, Lcom/jscape/inet/a/a/c/a/b/k;-><init>(IIII)V

    iput-object v0, p0, Lcom/jscape/inet/a/a/c/a/D;->c:Lcom/jscape/inet/a/a/c/a/b/k;

    iput v1, p0, Lcom/jscape/inet/a/a/c/a/D;->g:I

    new-instance p1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/D;->d:Ljava/util/concurrent/locks/Lock;

    new-instance p1, Lcom/jscape/util/b/n;

    invoke-direct {p1}, Lcom/jscape/util/b/n;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/D;->h:Lcom/jscape/util/b/m;

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/D;->f()V

    return-void
.end method

.method private a(J)Lcom/jscape/inet/a/a/c/a/b/i;
    .locals 3

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcom/jscape/inet/a/a/c/a/D;->j:J

    add-long/2addr v1, p1

    sget-object p1, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v1, v2, p1}, Lcom/jscape/util/am;->b(JLcom/jscape/util/u;)Z

    move-result p1

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/D;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->lock()V

    if-nez v0, :cond_2

    :try_start_0
    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/D;->h:Lcom/jscape/util/b/m;

    invoke-interface {p1}, Lcom/jscape/util/b/m;->a()Z

    move-result p1

    :cond_1
    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/D;->h:Lcom/jscape/util/b/m;

    invoke-interface {p1}, Lcom/jscape/util/b/m;->b()I

    move-result p1

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :cond_2
    iget p1, p0, Lcom/jscape/inet/a/a/c/a/D;->f:I

    add-int/lit8 p2, p1, 0x1

    iput p2, p0, Lcom/jscape/inet/a/a/c/a/D;->f:I

    :goto_0
    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/jscape/inet/a/a/c/a/D;->e()Z

    move-result p2

    if-eqz p2, :cond_3

    iget p2, p0, Lcom/jscape/inet/a/a/c/a/D;->g:I

    const/4 v0, 0x1

    add-int/2addr p2, v0

    iput p2, p0, Lcom/jscape/inet/a/a/c/a/D;->g:I

    iput-boolean v0, p0, Lcom/jscape/inet/a/a/c/a/D;->i:Z

    :cond_3
    sget-object p2, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {p2}, Lcom/jscape/util/am;->a(Lcom/jscape/util/u;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/D;->j:J

    :cond_4
    iget-object p2, p0, Lcom/jscape/inet/a/a/c/a/D;->b:[Lcom/jscape/inet/a/a/c/a/b/m;

    aget-object p1, p2, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p2, p0, Lcom/jscape/inet/a/a/c/a/D;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object p1

    :goto_1
    iget-object p2, p0, Lcom/jscape/inet/a/a/c/a/D;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
.end method

.method private b(J)Lcom/jscape/inet/a/a/c/a/b/i;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/D;->k:J

    add-long/2addr v0, p1

    sget-object p1, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v0, v1, p1}, Lcom/jscape/util/am;->b(JLcom/jscape/util/u;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/D;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/D;->c:Lcom/jscape/inet/a/a/c/a/b/k;

    iget p2, p0, Lcom/jscape/inet/a/a/c/a/D;->g:I

    iput p2, p1, Lcom/jscape/inet/a/a/c/a/b/k;->f:I

    sget-object p1, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {p1}, Lcom/jscape/util/am;->a(Lcom/jscape/util/u;)J

    move-result-wide p1

    iput-wide p1, p0, Lcom/jscape/inet/a/a/c/a/D;->k:J

    :cond_1
    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/D;->c:Lcom/jscape/inet/a/a/c/a/b/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p2, p0, Lcom/jscape/inet/a/a/c/a/D;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object p1

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lcom/jscape/inet/a/a/c/a/D;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
.end method

.method private f()V
    .locals 7

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/D;->e:J

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/D;->b:[Lcom/jscape/inet/a/a/c/a/b/m;

    array-length v3, v2

    if-ge v1, v3, :cond_1

    iget-wide v3, p0, Lcom/jscape/inet/a/a/c/a/D;->e:J

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/jscape/inet/a/a/c/a/b/m;->h:[B

    array-length v2, v2

    int-to-long v5, v2

    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/jscape/inet/a/a/c/a/D;->e:J

    add-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/a/a/c/a/D;->a:I

    return v0
.end method

.method public a(JJ)Lcom/jscape/inet/a/a/c/a/b/i;
    .locals 1

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/jscape/inet/a/a/c/a/D;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p3, p4}, Lcom/jscape/inet/a/a/c/a/D;->b(J)Lcom/jscape/inet/a/a/c/a/b/i;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/a/a/c/a/D;->a(J)Lcom/jscape/inet/a/a/c/a/b/i;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public a(I[I)V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/D;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    if-nez v0, :cond_1

    :try_start_0
    iget v1, p0, Lcom/jscape/inet/a/a/c/a/D;->g:I

    if-ne p1, v1, :cond_0

    if-nez v0, :cond_0

    iget-boolean p1, p0, Lcom/jscape/inet/a/a/c/a/D;->i:Z

    if-eqz p1, :cond_0

    new-instance p1, Lcom/jscape/util/b/k;

    invoke-direct {p1, p2}, Lcom/jscape/util/b/k;-><init>([I)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/D;->h:Lcom/jscape/util/b/m;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/jscape/inet/a/a/c/a/D;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/D;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lcom/jscape/inet/a/a/c/a/D;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method public b()[Lcom/jscape/inet/a/a/c/a/b/m;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/D;->b:[Lcom/jscape/inet/a/a/c/a/b/m;

    return-object v0
.end method

.method public c()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/D;->e:J

    return-wide v0
.end method

.method public d()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/a/a/c/a/D;->f:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/D;->b:[Lcom/jscape/inet/a/a/c/a/b/m;

    array-length v0, v0

    if-lt v1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public e()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/D;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/a/a/c/a/D;->d()Z

    move-result v1

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/D;->h:Lcom/jscape/util/b/m;

    invoke-interface {v1}, Lcom/jscape/util/b/m;->a()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    if-nez v0, :cond_2

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move v0, v1

    :goto_1
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/D;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/D;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/a/a/c/a/D;->l:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/D;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/D;->b:[Lcom/jscape/inet/a/a/c/a/b/m;

    array-length v2, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/D;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/D;->f:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/D;->h:Lcom/jscape/util/b/m;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/D;->j:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/jscape/inet/a/a/c/a/D;->k:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
