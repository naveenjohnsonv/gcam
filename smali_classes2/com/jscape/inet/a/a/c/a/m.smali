.class public Lcom/jscape/inet/a/a/c/a/m;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/jscape/util/h/K;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/K<",
            "Lcom/jscape/inet/a/a/c/a/b/i;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/net/DatagramPacket;

.field private final c:Lcom/jscape/util/h/e;


# direct methods
.method public constructor <init>(Lcom/jscape/util/h/K;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/h/K<",
            "Lcom/jscape/inet/a/a/c/a/b/i;",
            ">;I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/m;->a:Lcom/jscape/util/h/K;

    new-instance p1, Ljava/net/DatagramPacket;

    new-array v0, p2, [B

    invoke-direct {p1, v0, p2}, Ljava/net/DatagramPacket;-><init>([BI)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/m;->b:Ljava/net/DatagramPacket;

    new-instance p1, Lcom/jscape/util/h/e;

    const/4 p2, 0x0

    new-array p2, p2, [B

    invoke-direct {p1, p2}, Lcom/jscape/util/h/e;-><init>([B)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/m;->c:Lcom/jscape/util/h/e;

    return-void
.end method


# virtual methods
.method public a(Ljava/net/DatagramSocket;)Lcom/jscape/inet/a/a/c/a/b/i;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/m;->b:Ljava/net/DatagramPacket;

    invoke-virtual {p1, v0}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/m;->c:Lcom/jscape/util/h/e;

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/m;->b:Ljava/net/DatagramPacket;

    invoke-virtual {v0}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/m;->b:Ljava/net/DatagramPacket;

    invoke-virtual {v1}, Ljava/net/DatagramPacket;->getLength()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Lcom/jscape/util/h/e;->a([BII)V

    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/m;->a:Lcom/jscape/util/h/K;

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/m;->c:Lcom/jscape/util/h/e;

    invoke-interface {p1, v0}, Lcom/jscape/util/h/K;->read(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/a/a/c/a/b/i;

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/m;->b:Ljava/net/DatagramPacket;

    invoke-virtual {v0}, Ljava/net/DatagramPacket;->getSocketAddress()Ljava/net/SocketAddress;

    move-result-object v0

    check-cast v0, Ljava/net/InetSocketAddress;

    iput-object v0, p1, Lcom/jscape/inet/a/a/c/a/b/i;->b:Ljava/net/InetSocketAddress;

    return-object p1
.end method
