.class public Lcom/jscape/inet/a/a/c/a/z;
.super Ljava/lang/Object;


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field private final a:I

.field private final b:[[B

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "4tm\u0010vw_n1{%tq]} lH\u00114to\u0014vySl\'1\u0019p|Ql<\""

    const/16 v5, 0x24

    const/16 v6, 0x12

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x35

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    const/16 v15, 0x2d

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v4, 0x16

    const-string v5, "]2|Rpy^|88\u001b|qTgsn\u0013~`T1\u0016M6{\u0017{cXq4Z\u001e}vZ?(q\u001cvpI\""

    move v6, v4

    move-object v4, v5

    move v8, v11

    move v5, v15

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x32

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/a/a/c/a/z;->d:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v2, v14, 0x7

    const/4 v3, 0x3

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v15, 0x2

    if-eq v2, v15, :cond_7

    if-eq v2, v3, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v15, 0x5

    if-eq v2, v15, :cond_4

    move v15, v3

    goto :goto_4

    :cond_4
    const/16 v15, 0x27

    goto :goto_4

    :cond_5
    const/16 v15, 0x20

    goto :goto_4

    :cond_6
    const/16 v15, 0x40

    goto :goto_4

    :cond_7
    const/16 v15, 0x2a

    goto :goto_4

    :cond_8
    const/16 v15, 0x61

    :cond_9
    :goto_4
    xor-int v2, v9, v15

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(II)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    int-to-long v0, p1

    sget-object v2, Lcom/jscape/inet/a/a/c/a/z;->d:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const-wide/16 v3, 0x0

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    iput p1, p0, Lcom/jscape/inet/a/a/c/a/z;->a:I

    new-array p1, p2, [[B

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/z;->b:[[B

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/a/a/c/a/z;->a:I

    return v0
.end method

.method public a(Ljava/io/OutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/z;->b:[[B

    array-length v3, v2

    if-ge v1, v3, :cond_1

    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    add-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method public a(I[B)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/z;->b:[[B

    if-nez v0, :cond_1

    aget-object v0, v1, p1

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    aput-object p2, v1, p1

    iget p1, p0, Lcom/jscape/inet/a/a/c/a/z;->c:I

    const/4 p2, 0x1

    add-int/2addr p1, p2

    iput p1, p0, Lcom/jscape/inet/a/a/c/a/z;->c:I

    return p2
.end method

.method public b()[[B
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/z;->b:[[B

    return-object v0
.end method

.method public c()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/a/a/c/a/z;->c:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/z;->b:[[B

    array-length v0, v0

    if-ne v1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public d()[I
    .locals 6

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/z;->b:[[B

    array-length v0, v0

    iget v1, p0, Lcom/jscape/inet/a/a/c/a/z;->c:I

    sub-int/2addr v0, v1

    new-array v0, v0, [I

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move v3, v2

    :cond_0
    iget-object v4, p0, Lcom/jscape/inet/a/a/c/a/z;->b:[[B

    array-length v5, v4

    if-ge v2, v5, :cond_2

    aget-object v4, v4, v2

    if-nez v4, :cond_1

    add-int/lit8 v4, v3, 0x1

    aput v2, v0, v3

    move v3, v4

    :cond_1
    add-int/lit8 v2, v2, 0x1

    if-eqz v1, :cond_0

    :cond_2
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/a/a/c/a/z;->d:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/z;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/z;->b:[[B

    array-length v2, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/a/a/c/a/z;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
