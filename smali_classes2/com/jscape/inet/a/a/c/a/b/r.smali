.class public Lcom/jscape/inet/a/a/c/a/b/r;
.super Lcom/jscape/inet/a/a/c/a/b/i;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/a/a/c/a/b/i<",
        "Lcom/jscape/inet/a/a/c/a/b/v;",
        ">;"
    }
.end annotation


# static fields
.field private static final f:[Ljava/lang/String;


# instance fields
.field public final e:J


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "5s`o_:{m2yv\u000f\u00155spcA+aw2`o]1I}7fcA,5"

    const/16 v5, 0x22

    const/16 v6, 0xc

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x1b

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x2c

    const/16 v4, 0x10

    const-string v6, " fr|R8~i\u0007ewU/n\u007f{\u001b^)t}C\u001eoe6SvV?x\u007f2QrD!xxfzzCw"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v16, v6

    move v6, v4

    move-object/from16 v4, v16

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0xe

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/a/a/c/a/b/r;->f:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    const/4 v3, 0x2

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v3, 0x13

    goto :goto_4

    :cond_4
    const/16 v3, 0x44

    goto :goto_4

    :cond_5
    const/16 v3, 0x29

    goto :goto_4

    :cond_6
    const/16 v3, 0x1d

    goto :goto_4

    :cond_7
    const/16 v3, 0xf

    goto :goto_4

    :cond_8
    const/16 v3, 0x48

    :cond_9
    :goto_4
    xor-int v2, v9, v3

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(IJ)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/b/i;-><init>(I)V

    iput-wide p2, p0, Lcom/jscape/inet/a/a/c/a/b/r;->e:J

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/jscape/inet/a/a/c/a/b/u;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/a/a/c/a/b/v;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/c/a/b/r;->a(Lcom/jscape/inet/a/a/c/a/b/v;)V

    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/v;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/jscape/inet/a/a/c/a/b/v;->a(Lcom/jscape/inet/a/a/c/a/b/r;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/a/a/c/a/b/r;->f:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/b/r;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/b/r;->b:Ljava/net/InetSocketAddress;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/b/r;->c:Ljava/net/InetSocketAddress;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/jscape/inet/a/a/c/a/b/r;->e:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
