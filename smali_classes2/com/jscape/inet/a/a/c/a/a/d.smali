.class public Lcom/jscape/inet/a/a/c/a/a/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/a/a/c/a/b/i;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/c/a/b/i;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/f;->a(Ljava/io/InputStream;)I

    move-result v1

    invoke-static {p1}, Lcom/jscape/util/h/a/f;->a(Ljava/io/InputStream;)I

    move-result v2

    invoke-static {p1}, Lcom/jscape/util/h/a/f;->a(Ljava/io/InputStream;)I

    move-result v3

    invoke-static {p1}, Lcom/jscape/util/h/a/f;->a(Ljava/io/InputStream;)I

    move-result v4

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/a/j;->b()[Lcom/jscape/util/aq;

    invoke-static {p1}, Lcom/jscape/util/h/a/p;->a(Ljava/io/InputStream;)[B

    move-result-object v5

    new-instance p1, Lcom/jscape/inet/a/a/c/a/b/m;

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/a/a/c/a/b/m;-><init>(IIII[B)V

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/util/aq;

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/a/j;->b([Lcom/jscape/util/aq;)V

    :cond_0
    return-object p1
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/i;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/c/a/b/m;

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/a/j;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    iget v1, p1, Lcom/jscape/inet/a/a/c/a/b/m;->a:I

    invoke-static {v1, p2}, Lcom/jscape/util/h/a/f;->a(ILjava/io/OutputStream;)V

    :try_start_0
    iget v1, p1, Lcom/jscape/inet/a/a/c/a/b/m;->e:I

    invoke-static {v1, p2}, Lcom/jscape/util/h/a/f;->a(ILjava/io/OutputStream;)V

    iget v1, p1, Lcom/jscape/inet/a/a/c/a/b/m;->f:I

    invoke-static {v1, p2}, Lcom/jscape/util/h/a/f;->a(ILjava/io/OutputStream;)V

    iget v1, p1, Lcom/jscape/inet/a/a/c/a/b/m;->g:I

    invoke-static {v1, p2}, Lcom/jscape/util/h/a/f;->a(ILjava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/a/a/c/a/b/m;->h:[B

    invoke-static {p1, p2}, Lcom/jscape/util/h/a/p;->a([BLjava/io/OutputStream;)V

    if-nez v0, :cond_0

    const/4 p1, 0x1

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/a/c/a/a/d;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/c/a/a/d;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/c/a/b/i;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/c/a/b/i;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/c/a/a/d;->a(Lcom/jscape/inet/a/a/c/a/b/i;Ljava/io/OutputStream;)V

    return-void
.end method
