.class public Lcom/jscape/inet/a/a/c/a/l;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/a/a/c/a/w;


# static fields
.field private static final g:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/net/DatagramSocket;

.field private final b:Ljava/net/InetSocketAddress;

.field private final c:Lcom/jscape/inet/a/a/c/a/n;

.field private final d:Lcom/jscape/inet/a/a/c/a/m;

.field private final e:Ljava/util/concurrent/locks/Lock;

.field private final f:Ljava/util/logging/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "$\u0015\u001e1Uyl\r$\u000b3Yny#\u001c\u000b>\\na@\u000f\u0019?Q`h\u0014I()\u001a\t?_bc\u0007T+\u0016f[-\u0010\u0015\t;W\u007f-\u0001\u0000J\u000b\u0017x-\\YTp\u0017xPZTO#\u001c(/\u0001\u001e7]bc\u0007T+\u0016f[-\u0010\u0015\t;W\u007f-\u0001\u0000J\u000b\u0017x-\\YTp\u0017xPZTO#\u001c\u0015\u0003\u001b\u0007~Xxn\u0001\u0004\u000f~[eh\u0014Z\u000b~S%n"

    const/16 v4, 0x86

    const/16 v5, 0x1e

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x6e

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x4c

    const/16 v3, 0x23

    const-string v5, "N}nX8\u0005\u0004`3}V6\u0007\u000fs3lCu7Ot31\u001akLOtN7\u0017p\u001fD(HfyP:\u0005\u0004`3Lq\u0001<Jwrn\\0\u0018Jfg-lp\u001fJ;>3\u0017p\u001f7=3(D{"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x9

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/a/a/c/a/l;->g:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    const/4 v1, 0x4

    if-eqz v15, :cond_8

    if-eq v15, v9, :cond_7

    const/4 v2, 0x2

    if-eq v15, v2, :cond_9

    const/4 v2, 0x3

    if-eq v15, v2, :cond_6

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x63

    goto :goto_4

    :cond_4
    const/16 v1, 0x65

    goto :goto_4

    :cond_5
    const/16 v1, 0x5c

    goto :goto_4

    :cond_6
    const/16 v1, 0x3e

    goto :goto_4

    :cond_7
    const/16 v1, 0x1a

    goto :goto_4

    :cond_8
    const/16 v1, 0xe

    :cond_9
    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/net/DatagramSocket;Lcom/jscape/util/h/I;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/DatagramSocket;",
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/a/a/c/a/b/i;",
            ">;I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/l;->a:Ljava/net/DatagramSocket;

    invoke-virtual {p1}, Ljava/net/DatagramSocket;->getLocalSocketAddress()Ljava/net/SocketAddress;

    move-result-object p1

    check-cast p1, Ljava/net/InetSocketAddress;

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/l;->b:Ljava/net/InetSocketAddress;

    new-instance p1, Lcom/jscape/inet/a/a/c/a/n;

    invoke-direct {p1, p2, p3}, Lcom/jscape/inet/a/a/c/a/n;-><init>(Lcom/jscape/util/h/N;I)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/l;->c:Lcom/jscape/inet/a/a/c/a/n;

    new-instance p1, Lcom/jscape/inet/a/a/c/a/m;

    invoke-direct {p1, p2, p3}, Lcom/jscape/inet/a/a/c/a/m;-><init>(Lcom/jscape/util/h/K;I)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/l;->d:Lcom/jscape/inet/a/a/c/a/m;

    new-instance p1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/l;->e:Ljava/util/concurrent/locks/Lock;

    sget-object p1, Lcom/jscape/inet/a/a/c/a/l;->g:[Ljava/lang/String;

    const/4 p2, 0x3

    aget-object p1, p1, p2

    invoke-static {p1}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/l;->f:Ljava/util/logging/Logger;

    return-void
.end method

.method private b(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 10

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/l;->f:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x2

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/l;->f:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    sget-object v7, Lcom/jscape/inet/a/a/c/a/l;->g:[Ljava/lang/String;

    const/4 v8, 0x5

    aget-object v7, v7, v8

    new-array v8, v4, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/jscape/inet/a/a/c/a/l;->b:Ljava/net/InetSocketAddress;

    aput-object v9, v8, v3

    iget-object v9, p1, Lcom/jscape/inet/a/a/c/a/b/i;->c:Ljava/net/InetSocketAddress;

    aput-object v9, v8, v2

    aput-object p1, v8, v5

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    if-eqz v0, :cond_4

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/l;->f:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    invoke-virtual {v1, v6}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    :cond_1
    if-nez v0, :cond_2

    if-eqz v1, :cond_4

    if-nez v0, :cond_3

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/l;->d(Lcom/jscape/inet/a/a/c/a/b/i;)Z

    move-result v1

    :cond_2
    if-eqz v1, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/l;->f:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    sget-object v6, Lcom/jscape/inet/a/a/c/a/l;->g:[Ljava/lang/String;

    aget-object v6, v6, v5

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/jscape/inet/a/a/c/a/l;->b:Ljava/net/InetSocketAddress;

    aput-object v7, v4, v3

    iget-object v3, p1, Lcom/jscape/inet/a/a/c/a/b/i;->c:Ljava/net/InetSocketAddress;

    aput-object v3, v4, v2

    aput-object p1, v4, v5

    invoke-static {v6, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_4
    return-void
.end method

.method private c(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 10

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/l;->f:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x1

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/l;->f:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    sget-object v7, Lcom/jscape/inet/a/a/c/a/l;->g:[Ljava/lang/String;

    aget-object v7, v7, v5

    new-array v8, v4, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/jscape/inet/a/a/c/a/l;->b:Ljava/net/InetSocketAddress;

    aput-object v9, v8, v3

    iget-object v9, p1, Lcom/jscape/inet/a/a/c/a/b/i;->b:Ljava/net/InetSocketAddress;

    aput-object v9, v8, v5

    aput-object p1, v8, v2

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    if-eqz v0, :cond_4

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/l;->f:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    invoke-virtual {v1, v6}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    :cond_1
    if-nez v0, :cond_2

    if-eqz v1, :cond_4

    if-nez v0, :cond_3

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/l;->d(Lcom/jscape/inet/a/a/c/a/b/i;)Z

    move-result v1

    :cond_2
    if-eqz v1, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/l;->f:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    sget-object v6, Lcom/jscape/inet/a/a/c/a/l;->g:[Ljava/lang/String;

    const/4 v7, 0x4

    aget-object v6, v6, v7

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/jscape/inet/a/a/c/a/l;->b:Ljava/net/InetSocketAddress;

    aput-object v7, v4, v3

    iget-object v3, p1, Lcom/jscape/inet/a/a/c/a/b/i;->b:Ljava/net/InetSocketAddress;

    aput-object v3, v4, v5

    aput-object p1, v4, v2

    invoke-static {v6, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_4
    return-void
.end method

.method private d(Lcom/jscape/inet/a/a/c/a/b/i;)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    const-class v1, Lcom/jscape/inet/a/a/c/a/b/m;

    if-nez v0, :cond_0

    if-eq p1, v1, :cond_2

    const-class v1, Lcom/jscape/inet/a/a/c/a/b/r;

    :cond_0
    if-nez v0, :cond_1

    if-eq p1, v1, :cond_2

    const-class v1, Lcom/jscape/inet/a/a/c/a/b/s;

    :cond_1
    if-eq p1, v1, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public a()Ljava/net/InetSocketAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/l;->b:Ljava/net/InetSocketAddress;

    return-object v0
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/l;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/l;->b(Lcom/jscape/inet/a/a/c/a/b/i;)V

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/l;->c:Lcom/jscape/inet/a/a/c/a/n;

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/l;->a:Ljava/net/DatagramSocket;

    invoke-virtual {v0, p1, v1}, Lcom/jscape/inet/a/a/c/a/n;->a(Lcom/jscape/inet/a/a/c/a/b/i;Ljava/net/DatagramSocket;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/l;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/l;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
.end method

.method public b()Lcom/jscape/inet/a/a/c/a/b/i;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/l;->d:Lcom/jscape/inet/a/a/c/a/m;

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/l;->a:Ljava/net/DatagramSocket;

    invoke-virtual {v0, v1}, Lcom/jscape/inet/a/a/c/a/m;->a(Ljava/net/DatagramSocket;)Lcom/jscape/inet/a/a/c/a/b/i;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/a/c/a/l;->c(Lcom/jscape/inet/a/a/c/a/b/i;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/l;->a:Ljava/net/DatagramSocket;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/net/DatagramSocket;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/a/a/c/a/l;->g:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/l;->a:Ljava/net/DatagramSocket;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
