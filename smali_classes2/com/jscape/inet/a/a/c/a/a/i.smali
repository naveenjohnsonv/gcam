.class public Lcom/jscape/inet/a/a/c/a/a/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/a/a/c/a/b/i;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/jscape/inet/a/a/c/a/a/j;

.field private static final d:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/jscape/inet/a/a/c/a/a/j;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class;",
            "Lcom/jscape/inet/a/a/c/a/a/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "/s`}`#$\u0008ivl0#*\u0019vv|0\'2\nx)("

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/a/c/a/a/i;->d:Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/a/a/c/a/a/j;

    const/4 v1, -0x1

    const-class v2, Lcom/jscape/inet/a/a/c/a/b/t;

    new-instance v3, Lcom/jscape/inet/a/a/c/a/a/n;

    invoke-direct {v3}, Lcom/jscape/inet/a/a/c/a/a/n;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/jscape/inet/a/a/c/a/a/j;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    sput-object v0, Lcom/jscape/inet/a/a/c/a/a/i;->a:Lcom/jscape/inet/a/a/c/a/a/j;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x2b

    goto :goto_1

    :cond_1
    const/16 v4, 0x33

    goto :goto_1

    :cond_2
    const/16 v4, 0x70

    goto :goto_1

    :cond_3
    const/16 v4, 0x68

    goto :goto_1

    :cond_4
    const/16 v4, 0x73

    goto :goto_1

    :cond_5
    const/16 v4, 0x7d

    goto :goto_1

    :cond_6
    const/16 v4, 0x1a

    :goto_1
    const/16 v5, 0x60

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public varargs constructor <init>([Lcom/jscape/inet/a/a/c/a/a/j;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/a/a/c/a/a/i;->b:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/a/a/c/a/a/i;->c:Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/c/a/a/i;->a([Lcom/jscape/inet/a/a/c/a/a/j;)Lcom/jscape/inet/a/a/c/a/a/i;

    return-void
.end method

.method private a(I)Lcom/jscape/inet/a/a/c/a/a/j;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/a/j;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/a/i;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/a/a/c/a/a/j;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/jscape/inet/a/a/c/a/a/i;->a:Lcom/jscape/inet/a/a/c/a/a/j;

    :cond_1
    :goto_0
    return-object p1
.end method

.method private a(Ljava/lang/Class;)Lcom/jscape/inet/a/a/c/a/a/j;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/a/j;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/a/i;->c:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/a/a/c/a/a/j;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/a/a/c/a/a/i;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/a/c/a/a/i;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public varargs a([Lcom/jscape/inet/a/a/c/a/a/j;)Lcom/jscape/inet/a/a/c/a/a/i;
    .locals 5

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    iget-object v3, p0, Lcom/jscape/inet/a/a/c/a/a/i;->b:Ljava/util/Map;

    iget v4, v2, Lcom/jscape/inet/a/a/c/a/a/j;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/jscape/inet/a/a/c/a/a/i;->c:Ljava/util/Map;

    iget-object v4, v2, Lcom/jscape/inet/a/a/c/a/a/j;->b:Ljava/lang/Class;

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/c/a/b/i;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/a/c/a/a/i;->a(I)Lcom/jscape/inet/a/a/c/a/a/j;

    move-result-object v0

    iget-object v0, v0, Lcom/jscape/inet/a/a/c/a/a/j;->c:Lcom/jscape/util/h/I;

    invoke-interface {v0, p1}, Lcom/jscape/util/h/I;->read(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/a/a/c/a/b/i;

    return-object p1
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/i;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/a/c/a/a/i;->a(Ljava/lang/Class;)Lcom/jscape/inet/a/a/c/a/a/j;

    move-result-object v0

    iget v1, v0, Lcom/jscape/inet/a/a/c/a/a/j;->a:I

    int-to-byte v1, v1

    invoke-static {v1, p2}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    iget-object v0, v0, Lcom/jscape/inet/a/a/c/a/a/j;->c:Lcom/jscape/util/h/I;

    invoke-interface {v0, p1, p2}, Lcom/jscape/util/h/I;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/c/a/a/i;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/c/a/b/i;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/c/a/b/i;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/c/a/a/i;->a(Lcom/jscape/inet/a/a/c/a/b/i;Ljava/io/OutputStream;)V

    return-void
.end method
