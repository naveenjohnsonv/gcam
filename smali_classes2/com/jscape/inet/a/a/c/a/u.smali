.class public Lcom/jscape/inet/a/a/c/a/u;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/jscape/inet/a/a/c/a/u;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field public final a:J

.field public final b:J


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xc

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x76

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "&K\u0017a7\u0015:~\n\u000exg\u000eX\u001f\u0017M4\u0004;sK\u0018z.\u0004t"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x1b

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/a/a/c/a/u;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x3f

    goto :goto_2

    :cond_2
    const/4 v12, 0x6

    goto :goto_2

    :cond_3
    const/16 v12, 0x2c

    goto :goto_2

    :cond_4
    const/16 v12, 0x7e

    goto :goto_2

    :cond_5
    const/16 v12, 0x15

    goto :goto_2

    :cond_6
    const/16 v12, 0x1d

    goto :goto_2

    :cond_7
    const/16 v12, 0x7c

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(J)V
    .locals 2

    sget-object v0, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v0}, Lcom/jscape/util/am;->a(Lcom/jscape/util/u;)J

    move-result-wide v0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/jscape/inet/a/a/c/a/u;-><init>(JJ)V

    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/jscape/inet/a/a/c/a/u;->a:J

    iput-wide p3, p0, Lcom/jscape/inet/a/a/c/a/u;->b:J

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/inet/a/a/c/a/u;)I
    .locals 5

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p1, Lcom/jscape/inet/a/a/c/a/u;->a:J

    iget-wide v3, p0, Lcom/jscape/inet/a/a/c/a/u;->a:J

    cmp-long p1, v1, v3

    if-nez v0, :cond_1

    if-gez p1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    cmp-long p1, v1, v3

    :cond_1
    if-nez v0, :cond_3

    if-nez p1, :cond_2

    const/4 p1, 0x0

    goto :goto_0

    :cond_2
    const/4 p1, 0x1

    :cond_3
    :goto_0
    return p1
.end method

.method public a(Lcom/jscape/util/Time;)Z
    .locals 4

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/u;->b:J

    invoke-virtual {p1}, Lcom/jscape/util/Time;->toMicros()J

    move-result-wide v2

    add-long/2addr v0, v2

    sget-object p1, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v0, v1, p1}, Lcom/jscape/util/am;->b(JLcom/jscape/util/u;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/jscape/inet/a/a/c/a/u;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/c/a/u;->a(Lcom/jscape/inet/a/a/c/a/u;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    if-ne p0, p1, :cond_0

    return v1

    :cond_0
    move-object v2, p1

    goto :goto_0

    :cond_1
    move-object v2, p0

    :goto_0
    const/4 v3, 0x0

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-nez v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v2, v4, :cond_3

    goto :goto_1

    :cond_2
    move-object p1, v2

    :cond_3
    check-cast p1, Lcom/jscape/inet/a/a/c/a/u;

    iget-wide v4, p0, Lcom/jscape/inet/a/a/c/a/u;->a:J

    iget-wide v6, p1, Lcom/jscape/inet/a/a/c/a/u;->a:J

    cmp-long v2, v4, v6

    if-nez v0, :cond_5

    if-eqz v2, :cond_4

    return v3

    :cond_4
    iget-wide v4, p0, Lcom/jscape/inet/a/a/c/a/u;->b:J

    iget-wide v6, p1, Lcom/jscape/inet/a/a/c/a/u;->b:J

    cmp-long v2, v4, v6

    :cond_5
    if-nez v0, :cond_6

    if-eqz v2, :cond_7

    return v3

    :cond_6
    move v1, v2

    :cond_7
    return v1

    :cond_8
    :goto_1
    return v3
.end method

.method public hashCode()I
    .locals 5

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/u;->a:J

    const/16 v2, 0x20

    ushr-long v3, v0, v2

    xor-long/2addr v0, v3

    long-to-int v0, v0

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v3, p0, Lcom/jscape/inet/a/a/c/a/u;->b:J

    ushr-long v1, v3, v2

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/a/a/c/a/u;->c:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/u;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/jscape/inet/a/a/c/a/u;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
