.class public Lcom/jscape/inet/a/a/c/a/a/n;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/a/a/c/a/b/i;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/c/a/b/i;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/f;->a(Ljava/io/InputStream;)I

    move-result v0

    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v1

    new-array v1, v1, [B

    invoke-static {v1, p1}, Lcom/jscape/util/X;->a([BLjava/io/InputStream;)[B

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/a/a/c/a/b/t;

    invoke-direct {v1, v0, p1}, Lcom/jscape/inet/a/a/c/a/b/t;-><init>(I[B)V

    return-object v1
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/i;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/c/a/b/t;

    iget v0, p1, Lcom/jscape/inet/a/a/c/a/b/t;->a:I

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/f;->a(ILjava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/a/a/c/a/b/t;->e:[B

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/c/a/a/n;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/c/a/b/i;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/c/a/b/i;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/c/a/a/n;->a(Lcom/jscape/inet/a/a/c/a/b/i;Ljava/io/OutputStream;)V

    return-void
.end method
