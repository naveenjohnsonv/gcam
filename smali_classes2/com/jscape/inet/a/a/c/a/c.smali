.class abstract enum Lcom/jscape/inet/a/a/c/a/c;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/a/a/c/a/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/jscape/inet/a/a/c/a/c;

.field public static final enum b:Lcom/jscape/inet/a/a/c/a/c;

.field private static final c:[Lcom/jscape/inet/a/a/c/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x6

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x32

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\u000c&#3P{\u0005\u0011=0,E"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0xc

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    new-instance v3, Lcom/jscape/inet/a/a/c/a/d;

    aget-object v4, v1, v7

    invoke-direct {v3, v4, v2}, Lcom/jscape/inet/a/a/c/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/jscape/inet/a/a/c/a/c;->a:Lcom/jscape/inet/a/a/c/a/c;

    new-instance v3, Lcom/jscape/inet/a/a/c/a/e;

    aget-object v1, v1, v2

    invoke-direct {v3, v1, v7}, Lcom/jscape/inet/a/a/c/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/jscape/inet/a/a/c/a/c;->b:Lcom/jscape/inet/a/a/c/a/c;

    new-array v0, v0, [Lcom/jscape/inet/a/a/c/a/c;

    sget-object v1, Lcom/jscape/inet/a/a/c/a/c;->a:Lcom/jscape/inet/a/a/c/a/c;

    aput-object v1, v0, v2

    aput-object v3, v0, v7

    sput-object v0, Lcom/jscape/inet/a/a/c/a/c;->c:[Lcom/jscape/inet/a/a/c/a/c;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/4 v13, 0x5

    if-eqz v12, :cond_6

    if-eq v12, v7, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v14, 0x3

    if-eq v12, v14, :cond_3

    const/4 v14, 0x4

    if-eq v12, v14, :cond_2

    if-eq v12, v13, :cond_7

    const/16 v13, 0x36

    goto :goto_2

    :cond_2
    const/16 v13, 0x23

    goto :goto_2

    :cond_3
    const/16 v13, 0x4c

    goto :goto_2

    :cond_4
    const/16 v13, 0x43

    goto :goto_2

    :cond_5
    const/16 v13, 0x5b

    goto :goto_2

    :cond_6
    const/16 v13, 0x70

    :cond_7
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILcom/jscape/inet/a/a/c/a/t;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/a/a/c/a/c;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/inet/a/a/c/a/c;
    .locals 1

    const-class v0, Lcom/jscape/inet/a/a/c/a/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/a/a/c/a/c;

    return-object p0
.end method

.method public static a()[Lcom/jscape/inet/a/a/c/a/c;
    .locals 1

    sget-object v0, Lcom/jscape/inet/a/a/c/a/c;->c:[Lcom/jscape/inet/a/a/c/a/c;

    invoke-virtual {v0}, [Lcom/jscape/inet/a/a/c/a/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/a/a/c/a/c;

    return-object v0
.end method


# virtual methods
.method public abstract a(Lcom/jscape/inet/a/a/c/a/s;)V
.end method
