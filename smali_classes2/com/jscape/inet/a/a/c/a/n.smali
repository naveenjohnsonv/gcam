.class public Lcom/jscape/inet/a/a/c/a/n;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/jscape/util/h/N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/N<",
            "Lcom/jscape/inet/a/a/c/a/b/i;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/jscape/util/h/o;

.field private final c:Ljava/net/DatagramPacket;


# direct methods
.method public constructor <init>(Lcom/jscape/util/h/N;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/h/N<",
            "Lcom/jscape/inet/a/a/c/a/b/i;",
            ">;I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/n;->a:Lcom/jscape/util/h/N;

    new-instance p1, Lcom/jscape/util/h/o;

    invoke-direct {p1, p2}, Lcom/jscape/util/h/o;-><init>(I)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/n;->b:Lcom/jscape/util/h/o;

    new-instance p1, Ljava/net/DatagramPacket;

    const/4 p2, 0x0

    new-array v0, p2, [B

    invoke-direct {p1, v0, p2}, Ljava/net/DatagramPacket;-><init>([BI)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/n;->c:Ljava/net/DatagramPacket;

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/inet/a/a/c/a/b/i;Ljava/net/DatagramSocket;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/n;->b:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->c()Lcom/jscape/util/h/o;

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/n;->a:Lcom/jscape/util/h/N;

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/n;->b:Lcom/jscape/util/h/o;

    invoke-interface {v0, p1, v1}, Lcom/jscape/util/h/N;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/n;->c:Ljava/net/DatagramPacket;

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/n;->b:Lcom/jscape/util/h/o;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/n;->b:Lcom/jscape/util/h/o;

    invoke-virtual {v2}, Lcom/jscape/util/h/o;->b()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/net/DatagramPacket;->setData([BII)V

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/n;->c:Ljava/net/DatagramPacket;

    iget-object p1, p1, Lcom/jscape/inet/a/a/c/a/b/i;->c:Ljava/net/InetSocketAddress;

    invoke-virtual {v0, p1}, Ljava/net/DatagramPacket;->setSocketAddress(Ljava/net/SocketAddress;)V

    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/n;->c:Ljava/net/DatagramPacket;

    invoke-virtual {p2, p1}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    return-void
.end method
