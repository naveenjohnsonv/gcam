.class public Lcom/jscape/inet/a/a/c/a/a/k;
.super Ljava/lang/Object;


# static fields
.field public static final a:[Lcom/jscape/inet/a/a/c/a/a/j;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/jscape/inet/a/a/c/a/a/j;

    new-instance v1, Lcom/jscape/inet/a/a/c/a/a/j;

    const-class v2, Lcom/jscape/inet/a/a/c/a/b/m;

    new-instance v3, Lcom/jscape/inet/a/a/c/a/a/d;

    invoke-direct {v3}, Lcom/jscape/inet/a/a/c/a/a/d;-><init>()V

    const/4 v4, 0x0

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/a/a/c/a/a/j;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/jscape/inet/a/a/c/a/a/j;

    const-class v2, Lcom/jscape/inet/a/a/c/a/b/k;

    new-instance v3, Lcom/jscape/inet/a/a/c/a/a/b;

    invoke-direct {v3}, Lcom/jscape/inet/a/a/c/a/a/b;-><init>()V

    const/4 v4, 0x1

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/a/a/c/a/a/j;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/jscape/inet/a/a/c/a/a/j;

    const-class v2, Lcom/jscape/inet/a/a/c/a/b/j;

    new-instance v3, Lcom/jscape/inet/a/a/c/a/a/a;

    invoke-direct {v3}, Lcom/jscape/inet/a/a/c/a/a/a;-><init>()V

    const/4 v4, 0x2

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/a/a/c/a/a/j;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/jscape/inet/a/a/c/a/a/j;

    const-class v2, Lcom/jscape/inet/a/a/c/a/b/l;

    new-instance v3, Lcom/jscape/inet/a/a/c/a/a/c;

    invoke-direct {v3}, Lcom/jscape/inet/a/a/c/a/a/c;-><init>()V

    const/4 v4, 0x3

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/a/a/c/a/a/j;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/jscape/inet/a/a/c/a/a/j;

    const-class v2, Lcom/jscape/inet/a/a/c/a/b/r;

    new-instance v3, Lcom/jscape/inet/a/a/c/a/a/l;

    invoke-direct {v3}, Lcom/jscape/inet/a/a/c/a/a/l;-><init>()V

    const/4 v4, 0x4

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/a/a/c/a/a/j;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/jscape/inet/a/a/c/a/a/j;

    const-class v2, Lcom/jscape/inet/a/a/c/a/b/s;

    new-instance v3, Lcom/jscape/inet/a/a/c/a/a/m;

    invoke-direct {v3}, Lcom/jscape/inet/a/a/c/a/a/m;-><init>()V

    const/4 v4, 0x5

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/a/a/c/a/a/j;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/jscape/inet/a/a/c/a/a/j;

    const-class v2, Lcom/jscape/inet/a/a/c/a/b/q;

    new-instance v3, Lcom/jscape/inet/a/a/c/a/a/h;

    invoke-direct {v3}, Lcom/jscape/inet/a/a/c/a/a/h;-><init>()V

    const/4 v4, 0x6

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/a/a/c/a/a/j;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/jscape/inet/a/a/c/a/a/j;

    const-class v2, Lcom/jscape/inet/a/a/c/a/b/p;

    new-instance v3, Lcom/jscape/inet/a/a/c/a/a/g;

    invoke-direct {v3}, Lcom/jscape/inet/a/a/c/a/a/g;-><init>()V

    const/4 v4, 0x7

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/a/a/c/a/a/j;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/jscape/inet/a/a/c/a/a/j;

    const-class v2, Lcom/jscape/inet/a/a/c/a/b/o;

    new-instance v3, Lcom/jscape/inet/a/a/c/a/a/f;

    invoke-direct {v3}, Lcom/jscape/inet/a/a/c/a/a/f;-><init>()V

    const/16 v4, 0x8

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/a/a/c/a/a/j;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/jscape/inet/a/a/c/a/a/j;

    const-class v2, Lcom/jscape/inet/a/a/c/a/b/n;

    new-instance v3, Lcom/jscape/inet/a/a/c/a/a/e;

    invoke-direct {v3}, Lcom/jscape/inet/a/a/c/a/a/e;-><init>()V

    const/16 v4, 0x9

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/a/a/c/a/a/j;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    aput-object v1, v0, v4

    sput-object v0, Lcom/jscape/inet/a/a/c/a/a/k;->a:[Lcom/jscape/inet/a/a/c/a/a/j;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/jscape/inet/a/a/c/a/a/i;)Lcom/jscape/inet/a/a/c/a/a/i;
    .locals 1

    sget-object v0, Lcom/jscape/inet/a/a/c/a/a/k;->a:[Lcom/jscape/inet/a/a/c/a/a/j;

    invoke-virtual {p0, v0}, Lcom/jscape/inet/a/a/c/a/a/i;->a([Lcom/jscape/inet/a/a/c/a/a/j;)Lcom/jscape/inet/a/a/c/a/a/i;

    move-result-object p0

    return-object p0
.end method
