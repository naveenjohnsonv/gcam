.class public Lcom/jscape/inet/a/a/c/a/s;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/a/a/c/a/i;


# static fields
.field private static final a:I = 0x1

.field private static final b:I = 0x5

.field private static final c:I = 0xa

.field private static final d:I = 0x3

.field private static final e:I = 0xa

.field private static final f:D = 1.5

.field private static r:Ljava/lang/String;

.field private static final s:[Ljava/lang/String;


# instance fields
.field private final g:Lcom/jscape/util/A;

.field private final h:J

.field private final i:Lcom/jscape/inet/a/a/c/a/v;

.field private final j:Ljava/util/logging/Logger;

.field private k:I

.field private l:Lcom/jscape/inet/a/a/c/a/c;

.field private m:D

.field private n:D

.field private volatile o:J

.field private volatile p:J

.field private q:D


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/jscape/inet/a/a/c/a/s;->b(Ljava/lang/String;)V

    const/4 v2, 0x0

    const-string v3, "]\"\u000c Ej\u000e\u0000w\u001b\'Eh\u000e\u0003k\u00110\u000c\u0006]\"\u0013 D\u00057!c\u001d?TLK\u0003c\n1\u000c\u001d\rQRQ\'\n\u00189%VCqU\u0018\u0006\u001aqEtb]\u0005\u0015k\u00103\u0011Q\u0005\u0005g\u000c\"PTVTf^9ZK\u0006]\"\u000c E\u0005\r]\"\u000c El\u0019\u0010a\u00151C\u0005\u0012]\"\r1_\\\u0002\u001fe7:E]\u0019\u0007c\u0012i\r]\"\u000e5RS\u000e\u0005P\u001f T\u0005\u001f]\"\r PJ\u001f!j\u001f\'Tt\n\u0002v7:RJ\u000e\u0010q\u001b\u0015\\W\u001e\u001fvC!?c\n=G](\u001el\u00191BL\u0002\u001el=;_L\u0019\u001en^/SQ\u001f#c\n1\u000c"

    const/16 v4, 0xca

    const/16 v5, 0x13

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x17

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x1e

    const/16 v3, 0x8

    const-string v5, "\u000eq]o\u0003\u0018]\u001f\u0015A>@)\u0008\u0018[C!H)\u000b\u0005]V\u007fL)\u0003E["

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x44

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/a/a/c/a/s;->s:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x7c

    goto :goto_4

    :cond_4
    const/16 v1, 0x2f

    goto :goto_4

    :cond_5
    const/16 v1, 0x26

    goto :goto_4

    :cond_6
    const/16 v1, 0x43

    goto :goto_4

    :cond_7
    const/16 v1, 0x69

    goto :goto_4

    :cond_8
    const/16 v1, 0x15

    goto :goto_4

    :cond_9
    const/16 v1, 0x66

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/util/A;Lcom/jscape/util/Time;JLcom/jscape/util/Time;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/s;->g:Lcom/jscape/util/A;

    invoke-virtual {p2}, Lcom/jscape/util/Time;->toMicros()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/jscape/inet/a/a/c/a/s;->h:J

    new-instance p1, Lcom/jscape/inet/a/a/c/a/v;

    invoke-direct {p1, p3, p4, p5}, Lcom/jscape/inet/a/a/c/a/v;-><init>(JLcom/jscape/util/Time;)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/s;->i:Lcom/jscape/inet/a/a/c/a/v;

    sget-object p1, Lcom/jscape/inet/a/a/c/a/s;->s:[Ljava/lang/String;

    const/16 p2, 0xa

    aget-object p1, p1, p2

    invoke-static {p1}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/s;->j:Ljava/util/logging/Logger;

    return-void
.end method

.method static a(Lcom/jscape/inet/a/a/c/a/s;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/s;->f()V

    return-void
.end method

.method static b(Lcom/jscape/inet/a/a/c/a/s;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/s;->g()V

    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/a/a/c/a/s;->r:Ljava/lang/String;

    return-void
.end method

.method private d()V
    .locals 6

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/s;->g:Lcom/jscape/util/A;

    invoke-virtual {v0}, Lcom/jscape/util/A;->a()J

    move-result-wide v0

    long-to-double v0, v0

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/s;->k:I

    int-to-double v2, v2

    const-wide/high16 v4, 0x4020000000000000L    # 8.0

    mul-double/2addr v2, v4

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/s;->m:D

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/s;->n:D

    return-void
.end method

.method private e()V
    .locals 4

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/s;->m:D

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/s;->p:J

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/s;->i()V

    return-void
.end method

.method private f()V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/s;->i:Lcom/jscape/inet/a/a/c/a/v;

    invoke-virtual {v1}, Lcom/jscape/inet/a/a/c/a/v;->a()I

    move-result v1

    const/4 v2, 0x1

    if-nez v0, :cond_1

    if-le v1, v2, :cond_0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/s;->i:Lcom/jscape/inet/a/a/c/a/v;

    invoke-virtual {v1}, Lcom/jscape/inet/a/a/c/a/v;->a()I

    move-result v1

    int-to-double v1, v1

    const-wide/high16 v3, 0x3ff8000000000000L    # 1.5

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-double v1, v1

    iput-wide v1, p0, Lcom/jscape/inet/a/a/c/a/s;->q:D

    iget-wide v3, p0, Lcom/jscape/inet/a/a/c/a/s;->m:D

    add-double/2addr v3, v1

    iput-wide v3, p0, Lcom/jscape/inet/a/a/c/a/s;->m:D

    if-eqz v0, :cond_2

    :cond_0
    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/s;->i:Lcom/jscape/inet/a/a/c/a/v;

    invoke-virtual {v0}, Lcom/jscape/inet/a/a/c/a/v;->b()I

    move-result v1

    const/4 v2, 0x5

    :cond_1
    if-le v1, v2, :cond_2

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/s;->m:D

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/s;->q:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/s;->m:D

    sget-object v0, Lcom/jscape/inet/a/a/c/a/c;->b:Lcom/jscape/inet/a/a/c/a/c;

    iput-object v0, p0, Lcom/jscape/inet/a/a/c/a/s;->l:Lcom/jscape/inet/a/a/c/a/c;

    :cond_2
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/s;->h()V

    :cond_3
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/s;->e()V

    return-void
.end method

.method private g()V
    .locals 12

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/s;->i:Lcom/jscape/inet/a/a/c/a/v;

    invoke-virtual {v0}, Lcom/jscape/inet/a/a/c/a/v;->a()I

    move-result v0

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/s;->i:Lcom/jscape/inet/a/a/c/a/v;

    invoke-virtual {v2}, Lcom/jscape/inet/a/a/c/a/v;->b()I

    move-result v2

    const-wide/high16 v3, 0x4024000000000000L    # 10.0

    const/4 v5, 0x3

    const/16 v6, 0xa

    const/4 v7, 0x1

    if-nez v1, :cond_3

    if-le v0, v7, :cond_2

    iget-wide v8, p0, Lcom/jscape/inet/a/a/c/a/s;->m:D

    if-nez v1, :cond_1

    if-le v0, v6, :cond_0

    div-double v10, v8, v3

    goto :goto_1

    :cond_0
    move v10, v5

    goto :goto_0

    :cond_1
    move v10, v6

    :goto_0
    rem-int/2addr v0, v10

    int-to-double v10, v0

    :goto_1
    add-double/2addr v8, v10

    iput-wide v8, p0, Lcom/jscape/inet/a/a/c/a/s;->m:D

    if-eqz v1, :cond_6

    :cond_2
    move v0, v2

    :cond_3
    if-le v0, v7, :cond_6

    iget-wide v7, p0, Lcom/jscape/inet/a/a/c/a/s;->m:D

    if-nez v1, :cond_4

    if-le v2, v6, :cond_5

    div-double v0, v7, v3

    goto :goto_2

    :cond_4
    move v5, v6

    :cond_5
    rem-int/2addr v2, v5

    int-to-double v0, v2

    :goto_2
    sub-double/2addr v7, v0

    iput-wide v7, p0, Lcom/jscape/inet/a/a/c/a/s;->m:D

    :cond_6
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/s;->h()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/s;->e()V

    return-void
.end method

.method private h()V
    .locals 4

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/s;->m:D

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/s;->n:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/s;->m:D

    return-void
.end method

.method private i()V
    .locals 8

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/s;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/s;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/a/c/a/s;->s:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v0, v0, v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, p0, Lcom/jscape/inet/a/a/c/a/s;->m:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-wide v6, p0, Lcom/jscape/inet/a/a/c/a/s;->o:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    iget-wide v5, p0, Lcom/jscape/inet/a/a/c/a/s;->p:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public static j()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/inet/a/a/c/a/s;->r:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/s;->h:J

    return-wide v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/a/a/c/a/s;->k:I

    sget-object p1, Lcom/jscape/inet/a/a/c/a/c;->a:Lcom/jscape/inet/a/a/c/a/c;

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/s;->l:Lcom/jscape/inet/a/a/c/a/c;

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/s;->d()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/s;->e()V

    return-void
.end method

.method public a(J)V
    .locals 2

    iput-wide p1, p0, Lcom/jscape/inet/a/a/c/a/s;->o:J

    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/s;->i:Lcom/jscape/inet/a/a/c/a/v;

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/s;->o:J

    invoke-virtual {p1, v0, v1}, Lcom/jscape/inet/a/a/c/a/v;->a(J)V

    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/s;->l:Lcom/jscape/inet/a/a/c/a/c;

    invoke-virtual {p1, p0}, Lcom/jscape/inet/a/a/c/a/c;->a(Lcom/jscape/inet/a/a/c/a/s;)V

    return-void
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/s;->p:J

    return-wide v0
.end method

.method public c()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/s;->o:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/a/a/c/a/s;->s:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/s;->g:Lcom/jscape/util/A;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/s;->k:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/s;->h:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/s;->i:Lcom/jscape/inet/a/a/c/a/v;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/s;->l:Lcom/jscape/inet/a/a/c/a/c;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/s;->m:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/s;->o:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/s;->p:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/jscape/inet/a/a/c/a/s;->q:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
