.class public Lcom/jscape/inet/a/a/c/a/a/l;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/a/a/c/a/b/i;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/c/a/b/i;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/f;->a(Ljava/io/InputStream;)I

    move-result v0

    invoke-static {p1}, Lcom/jscape/util/h/a/i;->a(Ljava/io/InputStream;)J

    move-result-wide v1

    new-instance p1, Lcom/jscape/inet/a/a/c/a/b/r;

    invoke-direct {p1, v0, v1, v2}, Lcom/jscape/inet/a/a/c/a/b/r;-><init>(IJ)V

    return-object p1
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/i;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/c/a/b/r;

    iget v0, p1, Lcom/jscape/inet/a/a/c/a/b/r;->a:I

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/f;->a(ILjava/io/OutputStream;)V

    iget-wide v0, p1, Lcom/jscape/inet/a/a/c/a/b/r;->e:J

    invoke-static {v0, v1, p2}, Lcom/jscape/util/h/a/i;->a(JLjava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/c/a/a/l;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/c/a/b/i;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/c/a/b/i;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/c/a/a/l;->a(Lcom/jscape/inet/a/a/c/a/b/i;Ljava/io/OutputStream;)V

    return-void
.end method
