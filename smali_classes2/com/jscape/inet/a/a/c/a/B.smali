.class public Lcom/jscape/inet/a/a/c/a/B;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final a:Lcom/jscape/util/Time;

.field private static final k:[Ljava/lang/String;


# instance fields
.field private final b:I

.field private final c:Ljava/io/InputStream;

.field private final d:I

.field private final e:I

.field private final f:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Lcom/jscape/util/b/d<",
            "[B>;>;"
        }
    .end annotation
.end field

.field private g:I

.field private volatile h:Z

.field private volatile i:Ljava/lang/Throwable;

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x1b

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x4d

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "w\nF%\u001f!K^\u000eV%\u000b!\\TKQl\u0015%\u0008C\nNp\nn\u001cw\nF%\u001f!K^\u000eVvO0MGK@i\u0000#C\u0015\u001dCi\u001a%\u0006\u0018t-vUOg\rFL\u0002V\n.L\\\u0005E%;(ZP\nF"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x51

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/a/a/c/a/B;->k:[Ljava/lang/String;

    const-wide/16 v0, 0xa

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->millis(J)Lcom/jscape/util/Time;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/a/c/a/B;->a:Lcom/jscape/util/Time;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v13, 0x2

    if-eq v12, v13, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x65

    goto :goto_2

    :cond_2
    const/16 v12, 0xd

    goto :goto_2

    :cond_3
    const/16 v12, 0x22

    goto :goto_2

    :cond_4
    const/16 v12, 0x48

    goto :goto_2

    :cond_5
    const/16 v12, 0x6f

    goto :goto_2

    :cond_6
    const/16 v12, 0x26

    goto :goto_2

    :cond_7
    const/16 v12, 0x78

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(ILjava/io/InputStream;III)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jscape/inet/a/a/c/a/B;->b:I

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/a/a/c/a/B;->c:Ljava/io/InputStream;

    int-to-long p1, p3

    sget-object v0, Lcom/jscape/inet/a/a/c/a/B;->k:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v1, v0, v1

    const-wide/16 v2, 0x0

    invoke-static {p1, p2, v2, v3, v1}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p3, p0, Lcom/jscape/inet/a/a/c/a/B;->d:I

    int-to-long p1, p4

    const/4 p3, 0x1

    aget-object v0, v0, p3

    invoke-static {p1, p2, v2, v3, v0}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p4, p0, Lcom/jscape/inet/a/a/c/a/B;->e:I

    new-instance p1, Ljava/util/concurrent/ArrayBlockingQueue;

    sub-int/2addr p5, p3

    invoke-direct {p1, p5}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/B;->f:Ljava/util/concurrent/BlockingQueue;

    return-void
.end method

.method private static a(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/util/b/d;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/b/d<",
            "[B>;)V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    :try_start_0
    iget-boolean v2, p0, Lcom/jscape/inet/a/a/c/a/B;->h:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v2, :cond_1

    if-nez v0, :cond_0

    if-nez v1, :cond_1

    :try_start_1
    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/B;->f:Ljava/util/concurrent/BlockingQueue;

    sget-object v3, Lcom/jscape/inet/a/a/c/a/B;->a:Lcom/jscape/util/Time;

    iget-wide v3, v3, Lcom/jscape/util/Time;->value:J

    sget-object v5, Lcom/jscape/inet/a/a/c/a/B;->a:Lcom/jscape/util/Time;

    iget-object v5, v5, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, p1, v3, v4, v5}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    if-eqz v0, :cond_0

    :cond_1
    return-void

    :catch_1
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/a/a/c/a/B;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/a/c/a/B;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method private b(Lcom/jscape/util/b/d;)Lcom/jscape/inet/a/a/c/a/D;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/b/d<",
            "[B>;)",
            "Lcom/jscape/inet/a/a/c/a/D;"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/B;->c(Lcom/jscape/util/b/d;)[Lcom/jscape/inet/a/a/c/a/b/m;

    move-result-object p1

    new-instance v0, Lcom/jscape/inet/a/a/c/a/D;

    iget v1, p0, Lcom/jscape/inet/a/a/c/a/B;->b:I

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/B;->g:I

    invoke-direct {v0, v1, v2, p1}, Lcom/jscape/inet/a/a/c/a/D;-><init>(II[Lcom/jscape/inet/a/a/c/a/b/m;)V

    iget p1, p0, Lcom/jscape/inet/a/a/c/a/B;->g:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/jscape/inet/a/a/c/a/B;->g:I

    return-object v0
.end method

.method private c(Lcom/jscape/util/b/d;)[Lcom/jscape/inet/a/a/c/a/b/m;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/b/d<",
            "[B>;)[",
            "Lcom/jscape/inet/a/a/c/a/b/m;"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/jscape/util/b/d;->b()I

    move-result v6

    new-array v7, v6, [Lcom/jscape/inet/a/a/c/a/b/m;

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Lcom/jscape/util/b/d;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    move v9, v0

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, [B

    if-nez v8, :cond_1

    new-instance v10, Lcom/jscape/inet/a/a/c/a/b/m;

    iget v1, p0, Lcom/jscape/inet/a/a/c/a/B;->b:I

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/B;->g:I

    move-object v0, v10

    move v3, v9

    move v4, v6

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/a/a/c/a/b/m;-><init>(IIII[B)V

    aput-object v10, v7, v9

    add-int/lit8 v9, v9, 0x1

    if-eqz v8, :cond_0

    :cond_1
    return-object v7
.end method

.method private d()Lcom/jscape/util/b/d;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jscape/util/b/d<",
            "[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/b/d;

    iget v1, p0, Lcom/jscape/inet/a/a/c/a/B;->e:I

    invoke-direct {v0, v1}, Lcom/jscape/util/b/d;-><init>(I)V

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    :cond_0
    iget v3, p0, Lcom/jscape/inet/a/a/c/a/B;->e:I

    if-ge v2, v3, :cond_1

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/B;->f()[B

    move-result-object v3

    if-eqz v3, :cond_1

    :try_start_0
    invoke-virtual {v0, v3}, Lcom/jscape/util/b/d;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    if-eqz v1, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/B;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    :try_start_1
    invoke-virtual {v0}, Lcom/jscape/util/b/d;->c()Z

    move-result v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :cond_2
    return-object v0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/B;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method private e()V
    .locals 2

    new-instance v0, Lcom/jscape/util/b/d;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/jscape/util/b/d;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/a/c/a/B;->a(Lcom/jscape/util/b/d;)V

    return-void
.end method

.method private f()[B
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/jscape/inet/a/a/c/a/B;->d:I

    new-array v1, v0, [B

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move v4, v3

    :cond_0
    const/4 v5, -0x1

    if-lez v0, :cond_1

    iget-object v4, p0, Lcom/jscape/inet/a/a/c/a/B;->c:Ljava/io/InputStream;

    invoke-virtual {v4, v1, v3, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    if-nez v2, :cond_2

    if-nez v2, :cond_2

    if-eq v4, v5, :cond_1

    add-int/2addr v3, v4

    sub-int/2addr v0, v4

    if-eqz v2, :cond_0

    :cond_1
    if-nez v2, :cond_4

    :cond_2
    if-ne v4, v5, :cond_3

    move v4, v3

    goto :goto_0

    :cond_3
    return-object v1

    :cond_4
    :goto_0
    if-nez v2, :cond_6

    if-lez v4, :cond_5

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    :cond_6
    move v0, v4

    :goto_1
    if-lez v0, :cond_7

    :try_start_0
    new-array v0, v3, [B

    invoke-static {v1, v0}, Lcom/jscape/util/v;->a([B[B)[B

    move-result-object v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/B;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_7
    :goto_2
    return-object v1
.end method


# virtual methods
.method public a()V
    .locals 6

    new-instance v0, Ljava/lang/Thread;

    sget-object v1, Lcom/jscape/inet/a/a/c/a/B;->k:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    iget v4, p0, Lcom/jscape/inet/a/a/c/a/B;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/Thread;->setDaemon(Z)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/a/a/c/a/B;->h:Z

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/B;->c:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    return-void
.end method

.method public c()Lcom/jscape/inet/a/a/c/a/D;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/B;->i:Ljava/lang/Throwable;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/B;->i:Ljava/lang/Throwable;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/B;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/inet/a/a/c/a/B;->j:Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_6

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    return-object v2

    :cond_2
    move-object v1, v2

    :cond_3
    :goto_1
    :try_start_2
    iget-boolean v3, p0, Lcom/jscape/inet/a/a/c/a/B;->h:Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_3

    if-nez v3, :cond_4

    if-nez v0, :cond_4

    if-nez v0, :cond_4

    if-nez v1, :cond_4

    :try_start_3
    iget-object v3, p0, Lcom/jscape/inet/a/a/c/a/B;->f:Ljava/util/concurrent/BlockingQueue;

    sget-object v4, Lcom/jscape/inet/a/a/c/a/B;->a:Lcom/jscape/util/Time;

    iget-wide v4, v4, Lcom/jscape/util/Time;->value:J

    sget-object v6, Lcom/jscape/inet/a/a/c/a/B;->a:Lcom/jscape/util/Time;

    iget-object v6, v6, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v4, v5, v6}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/util/b/d;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v1, v3

    goto :goto_1

    :catch_1
    if-eqz v0, :cond_3

    :cond_4
    if-nez v0, :cond_5

    if-eqz v1, :cond_6

    :cond_5
    :try_start_4
    invoke-virtual {v1}, Lcom/jscape/util/b/d;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/a/a/c/a/B;->j:Z
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2

    return-object v2

    :cond_7
    invoke-direct {p0, v1}, Lcom/jscape/inet/a/a/c/a/B;->b(Lcom/jscape/util/b/d;)Lcom/jscape/inet/a/a/c/a/D;

    move-result-object v0

    return-object v0

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/B;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :catch_3
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/B;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/B;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_5

    :catch_5
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/B;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :catch_6
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/B;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method public run()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/a/a/c/a/B;->h:Z

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/B;->d()Lcom/jscape/util/b/d;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v1, :cond_1

    :try_start_1
    invoke-direct {p0, v1}, Lcom/jscape/inet/a/a/c/a/B;->a(Lcom/jscape/util/b/d;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/B;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/B;->e()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    iput-object v0, p0, Lcom/jscape/inet/a/a/c/a/B;->i:Ljava/lang/Throwable;

    :goto_1
    return-void
.end method
