.class public Lcom/jscape/inet/a/a/c/a/a/j;
.super Ljava/lang/Object;


# static fields
.field private static d:[Lcom/jscape/util/aq;

.field private static final e:[Ljava/lang/String;


# instance fields
.field public final a:I

.field public final b:Ljava/lang/Class;

.field public final c:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/a/a/c/a/b/i;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x2

    new-array v3, v2, [Lcom/jscape/util/aq;

    invoke-static {v3}, Lcom/jscape/inet/a/a/c/a/a/j;->b([Lcom/jscape/util/aq;)V

    const/16 v3, 0x8

    const/4 v4, 0x0

    const/4 v5, -0x1

    move v6, v3

    move v7, v4

    :goto_0
    const/4 v8, 0x1

    add-int/2addr v5, v8

    add-int/2addr v6, v5

    const-string v9, "4@oE>\u0013b%\u000c]\u000exX#Vzl\u0019|Og\u000e4@|K9\u001ddl#`K)\u0005<"

    invoke-virtual {v9, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    array-length v10, v5

    move v11, v4

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v5}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v8, v7, 0x1

    aput-object v5, v1, v7

    const/16 v5, 0x24

    if-ge v6, v5, :cond_0

    invoke-virtual {v9, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v8

    move v15, v6

    move v6, v5

    move v5, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/a/a/c/a/a/j;->e:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v5, v11

    rem-int/lit8 v13, v11, 0x7

    const/4 v14, 0x4

    if-eqz v13, :cond_6

    if-eq v13, v8, :cond_5

    if-eq v13, v2, :cond_7

    if-eq v13, v0, :cond_4

    if-eq v13, v14, :cond_3

    const/4 v14, 0x5

    if-eq v13, v14, :cond_2

    const/16 v14, 0x9

    goto :goto_2

    :cond_2
    const/16 v14, 0x7e

    goto :goto_2

    :cond_3
    const/16 v14, 0x52

    goto :goto_2

    :cond_4
    const/16 v14, 0x22

    goto :goto_2

    :cond_5
    const/16 v14, 0x68

    goto :goto_2

    :cond_6
    const/16 v14, 0x10

    :cond_7
    :goto_2
    xor-int v13, v3, v14

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v5, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method public constructor <init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class;",
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/a/a/c/a/b/i;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jscape/inet/a/a/c/a/a/j;->a:I

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/a/a/c/a/a/j;->b:Ljava/lang/Class;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/a/a/c/a/a/j;->c:Lcom/jscape/util/h/I;

    return-void
.end method

.method public static b([Lcom/jscape/util/aq;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/a/a/c/a/a/j;->d:[Lcom/jscape/util/aq;

    return-void
.end method

.method public static b()[Lcom/jscape/util/aq;
    .locals 1

    sget-object v0, Lcom/jscape/inet/a/a/c/a/a/j;->d:[Lcom/jscape/util/aq;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/a/a/c/a/a/j;->e:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/a/j;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/a/j;->b:Ljava/lang/Class;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/a/j;->c:Lcom/jscape/util/h/I;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
