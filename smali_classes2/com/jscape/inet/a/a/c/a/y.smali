.class public Lcom/jscape/inet/a/a/c/a/y;
.super Ljava/lang/Object;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private final a:I

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/jscape/inet/a/a/c/a/z;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/locks/Lock;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "i]!\u0002vI[\u000b_*Lx]QYY+V;JOD_.Q;^BGI \u000c"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/a/c/a/y;->e:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x6d

    goto :goto_1

    :cond_1
    const/16 v4, 0x66

    goto :goto_1

    :cond_2
    const/16 v4, 0x55

    goto :goto_1

    :cond_3
    const/16 v4, 0x6c

    goto :goto_1

    :cond_4
    const/16 v4, 0xb

    goto :goto_1

    :cond_5
    const/16 v4, 0x72

    goto :goto_1

    :cond_6
    const/16 v4, 0x65

    :goto_1
    const/16 v5, 0x4e

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    int-to-long v0, p1

    sget-object v2, Lcom/jscape/inet/a/a/c/a/y;->e:Ljava/lang/String;

    const-wide/16 v3, 0x0

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p1, p0, Lcom/jscape/inet/a/a/c/a/y;->a:I

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/y;->b:Ljava/util/Map;

    new-instance p1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    return-void
.end method


# virtual methods
.method public a(I)Lcom/jscape/inet/a/a/c/a/z;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/y;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/a/a/c/a/z;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object p1

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/z;)V
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/y;->b:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/jscape/inet/a/a/c/a/z;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
.end method

.method public a(Ljava/util/concurrent/BlockingQueue;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/BlockingQueue<",
            "Lcom/jscape/inet/a/a/c/a/z;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :cond_0
    :try_start_0
    iget v1, p0, Lcom/jscape/inet/a/a/c/a/y;->d:I

    invoke-virtual {p0, v1}, Lcom/jscape/inet/a/a/c/a/y;->a(I)Lcom/jscape/inet/a/a/c/a/z;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/jscape/inet/a/a/c/a/z;->c()Z

    move-result v2

    if-nez v0, :cond_1

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/concurrent/BlockingQueue;->remainingCapacity()I

    move-result v2

    :cond_1
    if-nez v0, :cond_2

    if-lez v2, :cond_3

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/y;->b:Ljava/util/Map;

    iget v3, p0, Lcom/jscape/inet/a/a/c/a/y;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/y;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/jscape/inet/a/a/c/a/y;->d:I

    invoke-interface {p1, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    if-eqz v0, :cond_0

    :cond_3
    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
.end method

.method public a()Z
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/y;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public b(I)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v1, p0, Lcom/jscape/inet/a/a/c/a/y;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    if-le v1, p1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move p1, v1

    :goto_1
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return p1

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
.end method

.method public c(I)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v1, p0, Lcom/jscape/inet/a/a/c/a/y;->d:I

    if-nez v0, :cond_0

    if-lt p1, v1, :cond_1

    if-nez v0, :cond_2

    iget v0, p0, Lcom/jscape/inet/a/a/c/a/y;->d:I

    iget v1, p0, Lcom/jscape/inet/a/a/c/a/y;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/2addr v1, v0

    :cond_0
    if-ge p1, v1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return p1

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/y;->c:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw p1
.end method
