.class public Lcom/jscape/inet/a/a/c/a/a/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/a/a/c/a/b/i;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a([ILjava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    array-length v0, p1

    int-to-short v0, v0

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/q;->a(SLjava/io/OutputStream;)V

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/a/j;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    aget v2, p1, v1

    int-to-short v2, v2

    invoke-static {v2, p2}, Lcom/jscape/util/h/a/q;->a(SLjava/io/OutputStream;)V

    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private b(Ljava/io/InputStream;)[I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/q;->a(Ljava/io/InputStream;)S

    move-result v0

    const v1, 0xffff

    and-int/2addr v0, v1

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/a/j;->b()[Lcom/jscape/util/aq;

    move-result-object v2

    new-array v3, v0, [I

    const/4 v4, 0x0

    :cond_0
    if-ge v4, v0, :cond_1

    if-eqz v2, :cond_1

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/h/a/q;->a(Ljava/io/InputStream;)S

    move-result v5

    and-int/2addr v5, v1

    aput v5, v3, v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v4, v4, 0x1

    if-nez v2, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/a/c/a/a/c;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v3
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/c/a/b/i;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/f;->a(Ljava/io/InputStream;)I

    move-result v0

    invoke-static {p1}, Lcom/jscape/util/h/a/f;->a(Ljava/io/InputStream;)I

    move-result v1

    invoke-static {p1}, Lcom/jscape/util/h/a/f;->a(Ljava/io/InputStream;)I

    move-result v2

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/a/c;->b(Ljava/io/InputStream;)[I

    move-result-object p1

    new-instance v3, Lcom/jscape/inet/a/a/c/a/b/l;

    invoke-direct {v3, v0, v1, v2, p1}, Lcom/jscape/inet/a/a/c/a/b/l;-><init>(III[I)V

    return-object v3
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/i;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/c/a/b/l;

    iget v0, p1, Lcom/jscape/inet/a/a/c/a/b/l;->a:I

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/f;->a(ILjava/io/OutputStream;)V

    iget v0, p1, Lcom/jscape/inet/a/a/c/a/b/l;->e:I

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/f;->a(ILjava/io/OutputStream;)V

    iget v0, p1, Lcom/jscape/inet/a/a/c/a/b/l;->f:I

    invoke-static {v0, p2}, Lcom/jscape/util/h/a/f;->a(ILjava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/a/a/c/a/b/l;->g:[I

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/a/a/c/a/a/c;->a([ILjava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/c/a/a/c;->a(Ljava/io/InputStream;)Lcom/jscape/inet/a/a/c/a/b/i;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/a/a/c/a/b/i;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/c/a/a/c;->a(Lcom/jscape/inet/a/a/c/a/b/i;Ljava/io/OutputStream;)V

    return-void
.end method
