.class public Lcom/jscape/inet/a/a/c/a/b/t;
.super Lcom/jscape/inet/a/a/c/a/b/i;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/a/a/c/a/b/i<",
        "Lcom/jscape/inet/a/a/c/a/b/x;",
        ">;"
    }
.end annotation


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field public final e:[B


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "f\u0008l)k|lI\u000clh@1/F\u0008l)\u0015qjL\u000el \u0006"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/a/c/a/b/t;->f:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x6d

    goto :goto_1

    :cond_1
    const/16 v4, 0x7f

    goto :goto_1

    :cond_2
    const/16 v4, 0x59

    goto :goto_1

    :cond_3
    const/16 v4, 0x2a

    goto :goto_1

    :cond_4
    const/16 v4, 0x7a

    goto :goto_1

    :cond_5
    const/16 v4, 0xb

    goto :goto_1

    :cond_6
    const/16 v4, 0x40

    :goto_1
    const/16 v5, 0x62

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(I[B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/b/i;-><init>(I)V

    iput-object p2, p0, Lcom/jscape/inet/a/a/c/a/b/t;->e:[B

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/jscape/inet/a/a/c/a/b/u;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/a/a/c/a/b/x;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/c/a/b/t;->a(Lcom/jscape/inet/a/a/c/a/b/x;)V

    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/x;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/jscape/inet/a/a/c/a/b/x;->a(Lcom/jscape/inet/a/a/c/a/b/t;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/a/a/c/a/b/t;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/b/t;->e:[B

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
