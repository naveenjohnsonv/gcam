.class public Lcom/jscape/inet/a/a/c/a/j;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/a/a/c/a/i;


# static fields
.field private static final h:[Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/util/A;

.field private final b:J

.field private final c:Ljava/util/logging/Logger;

.field private d:I

.field private e:D

.field private volatile f:J

.field private volatile g:J


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "<xDg!=\u0015s7[=?s9q(S=<n?dvW=4.97@9Ux0tzb9Bvh%<0\u0008\u0019`n \u0008D\u000c\u000b61 7{+\r3\u0006e4t1Xtui4d=De4lg5<\u0016~>s\u0006<x[g =\u0012<xEv;d3~?\u007f}!e(f9Z."

    const/16 v4, 0x6e

    const/4 v5, 0x6

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/4 v8, 0x1

    add-int/2addr v6, v8

    add-int v9, v6, v5

    invoke-virtual {v3, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0x2c

    move v12, v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    array-length v13, v9

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v9}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v12}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v11, :cond_1

    add-int/lit8 v11, v7, 0x1

    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v11

    goto :goto_0

    :cond_0
    const/16 v4, 0x37

    const/16 v3, 0x23

    const-string v5, "Z>Qi(h=m\u0012Pt;l m8Pt\u001ff=m#Pv|r1p%m{(ln\u00135qMn([6h$Zi(Y6k8P~a"

    move v7, v11

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v7, 0x1

    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v11

    :goto_3
    const/16 v12, 0x25

    add-int/2addr v6, v8

    add-int v9, v6, v5

    invoke-virtual {v3, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/a/a/c/a/j;->h:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v9, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v8, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v1, 0x76

    goto :goto_4

    :cond_4
    move v1, v10

    goto :goto_4

    :cond_5
    const/16 v1, 0x79

    goto :goto_4

    :cond_6
    const/16 v1, 0x3f

    goto :goto_4

    :cond_7
    const/16 v1, 0x1a

    goto :goto_4

    :cond_8
    const/16 v1, 0x74

    goto :goto_4

    :cond_9
    const/16 v1, 0x3c

    :goto_4
    xor-int/2addr v1, v12

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v9, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/util/A;Lcom/jscape/util/Time;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/j;->a:Lcom/jscape/util/A;

    invoke-virtual {p2}, Lcom/jscape/util/Time;->toMicros()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/jscape/inet/a/a/c/a/j;->b:J

    sget-object p1, Lcom/jscape/inet/a/a/c/a/j;->h:[Ljava/lang/String;

    const/4 p2, 0x1

    aget-object p1, p1, p2

    invoke-static {p1}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/j;->c:Ljava/util/logging/Logger;

    return-void
.end method

.method private d()V
    .locals 6

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/j;->a:Lcom/jscape/util/A;

    invoke-virtual {v0}, Lcom/jscape/util/A;->a()J

    move-result-wide v0

    long-to-double v0, v0

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/j;->d:I

    int-to-double v2, v2

    const-wide/high16 v4, 0x4020000000000000L    # 8.0

    mul-double/2addr v2, v4

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/j;->e:D

    return-void
.end method

.method private e()V
    .locals 4

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/j;->e:D

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/j;->g:J

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/j;->f()V

    return-void
.end method

.method private f()V
    .locals 8

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/j;->c:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/j;->c:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/a/c/a/j;->h:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v0, v0, v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, p0, Lcom/jscape/inet/a/a/c/a/j;->e:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-wide v6, p0, Lcom/jscape/inet/a/a/c/a/j;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    iget-wide v5, p0, Lcom/jscape/inet/a/a/c/a/j;->g:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/j;->b:J

    return-wide v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/a/a/c/a/j;->d:I

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/j;->d()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/j;->e()V

    return-void
.end method

.method public a(J)V
    .locals 0

    iput-wide p1, p0, Lcom/jscape/inet/a/a/c/a/j;->f:J

    return-void
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/j;->g:J

    return-wide v0
.end method

.method public c()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/j;->f:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/a/a/c/a/j;->h:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/j;->a:Lcom/jscape/util/A;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/j;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/j;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/j;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/jscape/inet/a/a/c/a/j;->g:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
