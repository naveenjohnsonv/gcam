.class public Lcom/jscape/inet/a/a/c/a/A;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/a/a/c/a/E;
.implements Lcom/jscape/inet/a/a/c/a/b/a;
.implements Lcom/jscape/inet/a/a/c/a/b/c;
.implements Lcom/jscape/inet/a/a/c/a/b/w;
.implements Lcom/jscape/inet/a/a/c/a/b/h;
.implements Lcom/jscape/inet/a/a/c/a/b/g;
.implements Lcom/jscape/inet/a/a/c/a/b/e;


# static fields
.field private static final a:Lcom/jscape/util/Time;

.field private static final b:Lcom/jscape/util/Time;

.field private static final c:I = 0x64

.field private static final y:[Ljava/lang/String;


# instance fields
.field private final d:I

.field private volatile e:Ljava/net/InetSocketAddress;

.field private final f:Lcom/jscape/inet/a/a/c/a/o;

.field private final g:Lcom/jscape/inet/a/a/c/a/B;

.field private final h:Lcom/jscape/inet/a/a/c/a/C;

.field private final i:Lcom/jscape/util/Time;

.field private final j:Lcom/jscape/inet/a/a/c/a/i;

.field private final k:Ljava/util/logging/Logger;

.field private volatile l:Z

.field private volatile m:Z

.field private volatile n:Ljava/io/IOException;

.field private volatile o:J

.field private volatile p:J

.field private volatile q:Ljava/lang/Long;

.field private volatile r:J

.field private s:Lcom/jscape/inet/a/a/c/a/F;

.field private t:J

.field private u:J

.field private v:Lcom/jscape/inet/a/a/c/a/D;

.field private w:J

.field private x:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "eI\u000cFSlkK\u0007.ojU%\\F\u000cB[q%w\u0002\u001c\t\u0002(;\u000c\u0002\u001ct\u0004% _\t\u0010cW\n[_qlCIOLLwj^\t\u0014yI\u001c\\Nuj^S\nM\u001eudOL\n]\u0004%%cR\u001bNQlkK\u0007.ojU%\\F\u000cB[q%w\u0002\u001c\t\u0002(;\u000c\u0002\u001ct\u0004% _\t\u0012cW\n[_qlCIO]Wh`CR\u001b\u0007"

    const/16 v5, 0x84

    move v8, v3

    const/4 v6, -0x1

    const/16 v7, 0x25

    :goto_0
    const/16 v9, 0x6d

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v4, 0xf

    const-string v5, "\u001e\"l,7\u0013\u0000>gj;,\n\u0017b\u0015\t5}&,E\u0016))k 0\u0002E<&l\";\u0011K"

    move v7, v4

    move-object v4, v5

    move v8, v11

    const/16 v5, 0x25

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0xd

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/a/a/c/a/A;->y:[Ljava/lang/String;

    const-wide/16 v0, 0x1388

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->micros(J)Lcom/jscape/util/Time;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/a/c/a/A;->a:Lcom/jscape/util/Time;

    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->micros(J)Lcom/jscape/util/Time;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/a/c/a/A;->b:Lcom/jscape/util/Time;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v1, v14, 0x7

    const/16 v16, 0x68

    const/4 v2, 0x2

    if-eqz v1, :cond_8

    if-eq v1, v10, :cond_7

    if-eq v1, v2, :cond_6

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    const/4 v2, 0x4

    if-eq v1, v2, :cond_4

    const/4 v2, 0x5

    goto :goto_4

    :cond_4
    const/16 v16, 0x53

    goto :goto_4

    :cond_5
    const/16 v16, 0x44

    goto :goto_4

    :cond_6
    move/from16 v16, v2

    goto :goto_4

    :cond_7
    const/16 v16, 0x4a

    goto :goto_4

    :cond_8
    const/16 v16, 0x41

    :goto_4
    xor-int v1, v9, v16

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(ILjava/io/InputStream;Ljava/net/InetSocketAddress;Lcom/jscape/inet/a/a/c/a/o;IIIILcom/jscape/util/Time;Lcom/jscape/inet/a/a/c/a/i;ZLjava/util/logging/Logger;)V
    .locals 9

    move-object v0, p0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move v1, p1

    iput v1, v0, Lcom/jscape/inet/a/a/c/a/A;->d:I

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object v2, p3

    iput-object v2, v0, Lcom/jscape/inet/a/a/c/a/A;->e:Ljava/net/InetSocketAddress;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object v2, p4

    iput-object v2, v0, Lcom/jscape/inet/a/a/c/a/A;->f:Lcom/jscape/inet/a/a/c/a/o;

    new-instance v8, Lcom/jscape/inet/a/a/c/a/B;

    iget v3, v0, Lcom/jscape/inet/a/a/c/a/A;->d:I

    move-object v2, v8

    move-object v4, p2

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v2 .. v7}, Lcom/jscape/inet/a/a/c/a/B;-><init>(ILjava/io/InputStream;III)V

    iput-object v8, v0, Lcom/jscape/inet/a/a/c/a/A;->g:Lcom/jscape/inet/a/a/c/a/B;

    new-instance v2, Lcom/jscape/inet/a/a/c/a/C;

    move/from16 v3, p8

    invoke-direct {v2, v3}, Lcom/jscape/inet/a/a/c/a/C;-><init>(I)V

    iput-object v2, v0, Lcom/jscape/inet/a/a/c/a/A;->h:Lcom/jscape/inet/a/a/c/a/C;

    invoke-static/range {p9 .. p9}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object/from16 v2, p9

    iput-object v2, v0, Lcom/jscape/inet/a/a/c/a/A;->i:Lcom/jscape/util/Time;

    invoke-static/range {p10 .. p10}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object/from16 v2, p10

    iput-object v2, v0, Lcom/jscape/inet/a/a/c/a/A;->j:Lcom/jscape/inet/a/a/c/a/i;

    if-nez v1, :cond_1

    if-nez p11, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move/from16 v1, p11

    :goto_0
    iput-boolean v1, v0, Lcom/jscape/inet/a/a/c/a/A;->l:Z

    invoke-static/range {p12 .. p12}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    move-object/from16 v1, p12

    iput-object v1, v0, Lcom/jscape/inet/a/a/c/a/A;->k:Ljava/util/logging/Logger;

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v1

    if-nez v1, :cond_2

    const-string v1, "KlHqm"

    invoke-static {v1}, Lcom/jscape/inet/a/a/c/a/s;->b(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/inet/a/a/c/a/D;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/jscape/inet/a/a/c/a/D;->d()Z

    move-result v1

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/A;->p:J

    iget-wide v4, p0, Lcom/jscape/inet/a/a/c/a/A;->o:J

    invoke-virtual {p1, v2, v3, v4, v5}, Lcom/jscape/inet/a/a/c/a/D;->a(JJ)Lcom/jscape/inet/a/a/c/a/b/i;

    move-result-object v2

    if-nez v0, :cond_0

    if-eqz v2, :cond_1

    invoke-direct {p0, v2}, Lcom/jscape/inet/a/a/c/a/A;->d(Lcom/jscape/inet/a/a/c/a/b/i;)V

    :cond_0
    invoke-direct {p0, p1, v2, v1}, Lcom/jscape/inet/a/a/c/a/A;->a(Lcom/jscape/inet/a/a/c/a/D;Lcom/jscape/inet/a/a/c/a/b/i;Z)V

    :cond_1
    return-void
.end method

.method private a(Lcom/jscape/inet/a/a/c/a/D;Lcom/jscape/inet/a/a/c/a/b/i;Z)V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    if-eqz p3, :cond_0

    return-void

    :cond_0
    iget-wide v1, p0, Lcom/jscape/inet/a/a/c/a/A;->t:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/jscape/inet/a/a/c/a/A;->t:J

    iget-wide v1, p0, Lcom/jscape/inet/a/a/c/a/A;->u:J

    check-cast p2, Lcom/jscape/inet/a/a/c/a/b/m;

    iget-object p2, p2, Lcom/jscape/inet/a/a/c/a/b/m;->h:[B

    array-length p2, p2

    int-to-long p2, p2

    add-long/2addr v1, p2

    iput-wide v1, p0, Lcom/jscape/inet/a/a/c/a/A;->u:J

    if-nez v0, :cond_2

    iget-wide p2, p0, Lcom/jscape/inet/a/a/c/a/A;->t:J

    const-wide/16 v0, 0x64

    rem-long/2addr p2, v0

    const-wide/16 v0, 0x0

    cmp-long p3, p2, v0

    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p1}, Lcom/jscape/inet/a/a/c/a/D;->d()Z

    move-result p1

    if-eqz p1, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->s()V

    :cond_3
    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->k:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->k:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/a/c/a/A;->y:[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method private b(Ljava/lang/Throwable;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->k:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->k:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/a/c/a/A;->y:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method private b(Lcom/jscape/inet/a/a/c/a/b/i;)Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/A;->e:Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    iget-object p1, p1, Lcom/jscape/inet/a/a/c/a/b/i;->b:Ljava/net/InetSocketAddress;

    invoke-virtual {p1}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private c(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 0

    :try_start_0
    invoke-virtual {p1, p0}, Lcom/jscape/inet/a/a/c/a/b/i;->a(Lcom/jscape/inet/a/a/c/a/b/u;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/A;->g(Lcom/jscape/inet/a/a/c/a/b/i;)V

    :goto_0
    return-void
.end method

.method private d(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/A;->e:Ljava/net/InetSocketAddress;

    iput-object v0, p1, Lcom/jscape/inet/a/a/c/a/b/i;->c:Ljava/net/InetSocketAddress;

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/A;->f(Lcom/jscape/inet/a/a/c/a/b/i;)V

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/A;->f:Lcom/jscape/inet/a/a/c/a/o;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/a/a/c/a/o;->a(Lcom/jscape/inet/a/a/c/a/b/i;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method private e()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->u()V

    :cond_0
    iget-boolean v1, p0, Lcom/jscape/inet/a/a/c/a/A;->m:Z

    if-nez v1, :cond_2

    if-nez v0, :cond_1

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/a/a/c/a/A;->l:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_2

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->m()V

    if-eqz v0, :cond_0

    :cond_2
    return-void
.end method

.method private e(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->k:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->k:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/a/c/a/A;->y:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/jscape/inet/a/a/c/a/b/i;->b:Ljava/net/InetSocketAddress;

    aput-object v5, v4, v3

    const/4 v3, 0x1

    iget-object v5, p1, Lcom/jscape/inet/a/a/c/a/b/i;->c:Ljava/net/InetSocketAddress;

    aput-object v5, v4, v3

    const/4 v3, 0x2

    aput-object p1, v4, v3

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/A;->j:Lcom/jscape/inet/a/a/c/a/i;

    invoke-interface {v0}, Lcom/jscape/inet/a/a/c/a/i;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/A;->p:J

    sget-object v0, Lcom/jscape/inet/a/a/c/a/A;->b:Lcom/jscape/util/Time;

    invoke-virtual {v0}, Lcom/jscape/util/Time;->toMicros()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/A;->o:J

    return-void
.end method

.method private f(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->k:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->k:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/a/a/c/a/A;->y:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v0, v0, v3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/jscape/inet/a/a/c/a/b/i;->b:Ljava/net/InetSocketAddress;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p1, Lcom/jscape/inet/a/a/c/a/b/i;->c:Ljava/net/InetSocketAddress;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private g()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/A;->g:Lcom/jscape/inet/a/a/c/a/B;

    invoke-virtual {v0}, Lcom/jscape/inet/a/a/c/a/B;->a()V

    return-void
.end method

.method private g(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->k:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->k:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/jscape/inet/a/a/c/a/A;->y:[Ljava/lang/String;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private h()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/A;->g:Lcom/jscape/inet/a/a/c/a/B;

    invoke-virtual {v0}, Lcom/jscape/inet/a/a/c/a/B;->b()V

    return-void
.end method

.method private i()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->v()V

    :cond_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->k()Z

    move-result v1

    if-nez v1, :cond_2

    if-nez v0, :cond_1

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->l()Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_2

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->n()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->o()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->p()V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->q()V

    invoke-static {}, Ljava/lang/Thread;->yield()V

    if-eqz v0, :cond_0

    :cond_2
    return-void
.end method

.method private j()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->k()Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lcom/jscape/inet/a/a/c/a/b/o;

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/A;->d:I

    invoke-direct {v1, v2}, Lcom/jscape/inet/a/a/c/a/b/o;-><init>(I)V

    invoke-direct {p0, v1}, Lcom/jscape/inet/a/a/c/a/A;->d(Lcom/jscape/inet/a/a/c/a/b/i;)V

    iget-wide v1, p0, Lcom/jscape/inet/a/a/c/a/A;->o:J

    sget-object v3, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v1, v2, v3}, Lcom/jscape/util/am;->f(JLcom/jscape/util/u;)V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->q()V

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method private k()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jscape/inet/a/a/c/a/A;->m:Z

    if-nez v0, :cond_2

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/A;->n:Ljava/io/IOException;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move v0, v1

    :goto_1
    return v0
.end method

.method private l()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jscape/inet/a/a/c/a/A;->x:Z

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->h:Lcom/jscape/inet/a/a/c/a/C;

    invoke-virtual {v1}, Lcom/jscape/inet/a/a/c/a/C;->a()Z

    move-result v1

    :cond_0
    if-nez v0, :cond_2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method private m()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/a/a/c/a/A;->a:Lcom/jscape/util/Time;

    invoke-virtual {v0}, Lcom/jscape/util/Time;->toMicros()J

    move-result-wide v0

    sget-object v2, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v0, v1, v2}, Lcom/jscape/util/am;->f(JLcom/jscape/util/u;)V

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->q:Ljava/lang/Long;

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v0, v1, v2}, Lcom/jscape/util/am;->b(JLcom/jscape/util/u;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->w()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method private n()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->r()Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->g:Lcom/jscape/inet/a/a/c/a/B;

    invoke-virtual {v1}, Lcom/jscape/inet/a/a/c/a/B;->c()Lcom/jscape/inet/a/a/c/a/D;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->v:Lcom/jscape/inet/a/a/c/a/D;

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    if-nez v0, :cond_2

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->v:Lcom/jscape/inet/a/a/c/a/D;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v1, :cond_2

    :try_start_3
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->h:Lcom/jscape/inet/a/a/c/a/C;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/A;->v:Lcom/jscape/inet/a/a/c/a/D;

    invoke-virtual {v1, v2}, Lcom/jscape/inet/a/a/c/a/C;->a(Lcom/jscape/inet/a/a/c/a/D;)V

    if-eqz v0, :cond_3

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_2
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/a/a/c/a/A;->x:Z

    :cond_3
    return-void
.end method

.method private o()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->h:Lcom/jscape/inet/a/a/c/a/C;

    invoke-virtual {v1}, Lcom/jscape/inet/a/a/c/a/C;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/a/a/c/a/D;

    invoke-direct {p0, v2}, Lcom/jscape/inet/a/a/c/a/A;->a(Lcom/jscape/inet/a/a/c/a/D;)V

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method private p()V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/A;->w:J

    sget-object v2, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v0, v1, v2}, Lcom/jscape/util/am;->b(JLcom/jscape/util/u;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/jscape/inet/a/a/c/a/b/r;

    iget v1, p0, Lcom/jscape/inet/a/a/c/a/A;->d:I

    sget-object v2, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v2}, Lcom/jscape/util/am;->a(Lcom/jscape/util/u;)J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/jscape/inet/a/a/c/a/b/r;-><init>(IJ)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/a/c/a/A;->d(Lcom/jscape/inet/a/a/c/a/b/i;)V

    :cond_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->t()V

    :cond_1
    return-void
.end method

.method private q()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->q:Ljava/lang/Long;

    if-nez v0, :cond_1

    if-nez v1, :cond_1

    if-nez v0, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/A;->h:Lcom/jscape/inet/a/a/c/a/C;

    invoke-virtual {v2}, Lcom/jscape/inet/a/a/c/a/C;->c()Z

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    :try_start_2
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->u()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v0, :cond_3

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    :goto_1
    if-nez v0, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v0, v1, v2}, Lcom/jscape/util/am;->b(JLcom/jscape/util/u;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->w()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_3
    return-void

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method private r()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jscape/inet/a/a/c/a/A;->x:Z

    if-nez v0, :cond_0

    if-nez v1, :cond_4

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->h:Lcom/jscape/inet/a/a/c/a/C;

    invoke-virtual {v1}, Lcom/jscape/inet/a/a/c/a/C;->b()Z

    move-result v1

    :cond_0
    if-eqz v1, :cond_4

    :cond_1
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->v:Lcom/jscape/inet/a/a/c/a/D;

    if-nez v0, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    invoke-virtual {v1}, Lcom/jscape/inet/a/a/c/a/D;->d()Z

    move-result v1

    if-nez v0, :cond_5

    if-eqz v1, :cond_4

    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    :cond_5
    :goto_0
    return v1
.end method

.method private s()V
    .locals 4

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/A;->u:J

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/A;->r:J

    add-long/2addr v0, v2

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/A;->s:Lcom/jscape/inet/a/a/c/a/F;

    invoke-interface {v2, p0, v0, v1}, Lcom/jscape/inet/a/a/c/a/F;->a(Lcom/jscape/inet/a/a/c/a/E;J)V

    return-void
.end method

.method private t()V
    .locals 3

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/A;->j:Lcom/jscape/inet/a/a/c/a/i;

    invoke-interface {v0}, Lcom/jscape/inet/a/a/c/a/i;->a()J

    move-result-wide v0

    sget-object v2, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v0, v1, v2}, Lcom/jscape/util/am;->c(JLcom/jscape/util/u;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/A;->w:J

    return-void
.end method

.method private u()V
    .locals 3

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/A;->i:Lcom/jscape/util/Time;

    invoke-virtual {v0}, Lcom/jscape/util/Time;->toMicros()J

    move-result-wide v0

    sget-object v2, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v0, v1, v2}, Lcom/jscape/util/am;->c(JLcom/jscape/util/u;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/inet/a/a/c/a/A;->q:Ljava/lang/Long;

    return-void
.end method

.method private v()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/a/a/c/a/A;->q:Ljava/lang/Long;

    return-void
.end method

.method private w()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/InterruptedIOException;

    sget-object v1, Lcom/jscape/inet/a/a/c/a/A;->y:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private x()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->n:Ljava/io/IOException;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->n:Ljava/io/IOException;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    throw v1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/A;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public a(Lcom/jscape/inet/a/a/c/a/F;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/A;->s:Lcom/jscape/inet/a/a/c/a/F;

    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/A;->f:Lcom/jscape/inet/a/a/c/a/o;

    iget v0, p0, Lcom/jscape/inet/a/a/c/a/A;->d:I

    invoke-virtual {p1, v0, p0}, Lcom/jscape/inet/a/a/c/a/o;->a(ILcom/jscape/inet/a/a/c/a/q;)V

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->e()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->f()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->p()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->g()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->i()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->x()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/A;->f:Lcom/jscape/inet/a/a/c/a/o;

    iget v0, p0, Lcom/jscape/inet/a/a/c/a/A;->d:I

    invoke-virtual {p1, v0}, Lcom/jscape/inet/a/a/c/a/o;->a(I)V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->h()V

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/A;->b(Ljava/lang/Throwable;)V

    new-instance v0, Lcom/jscape/inet/a/a/c/a/b/p;

    iget v1, p0, Lcom/jscape/inet/a/a/c/a/A;->d:I

    invoke-direct {v0, v1}, Lcom/jscape/inet/a/a/c/a/b/p;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/a/c/a/A;->d(Lcom/jscape/inet/a/a/c/a/b/i;)V

    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/A;->f:Lcom/jscape/inet/a/a/c/a/o;

    iget v1, p0, Lcom/jscape/inet/a/a/c/a/A;->d:I

    invoke-virtual {v0, v1}, Lcom/jscape/inet/a/a/c/a/o;->a(I)V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->h()V

    throw p1
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/A;->e(Lcom/jscape/inet/a/a/c/a/b/i;)V

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/A;->b(Lcom/jscape/inet/a/a/c/a/b/i;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/jscape/inet/a/a/c/a/b/i;->b:Ljava/net/InetSocketAddress;

    iput-object v0, p0, Lcom/jscape/inet/a/a/c/a/A;->e:Ljava/net/InetSocketAddress;

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->v()V

    :cond_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/A;->c(Lcom/jscape/inet/a/a/c/a/b/i;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/j;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->h:Lcom/jscape/inet/a/a/c/a/C;

    iget p1, p1, Lcom/jscape/inet/a/a/c/a/b/j;->e:I

    invoke-virtual {v1, p1}, Lcom/jscape/inet/a/a/c/a/C;->b(I)Lcom/jscape/inet/a/a/c/a/D;

    move-result-object p1

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/A;->r:J

    invoke-virtual {p1}, Lcom/jscape/inet/a/a/c/a/D;->c()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/A;->r:J

    :cond_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/A;->s()V

    :cond_1
    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/l;)V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/A;->h:Lcom/jscape/inet/a/a/c/a/C;

    iget v2, p1, Lcom/jscape/inet/a/a/c/a/b/l;->e:I

    invoke-virtual {v1, v2}, Lcom/jscape/inet/a/a/c/a/C;->a(I)Lcom/jscape/inet/a/a/c/a/D;

    move-result-object v1

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    iget v0, p1, Lcom/jscape/inet/a/a/c/a/b/l;->f:I

    iget-object p1, p1, Lcom/jscape/inet/a/a/c/a/b/l;->g:[I

    invoke-virtual {v1, v0, p1}, Lcom/jscape/inet/a/a/c/a/D;->a(I[I)V

    :cond_1
    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/n;)V
    .locals 0

    invoke-virtual {p0}, Lcom/jscape/inet/a/a/c/a/A;->close()V

    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/p;)V
    .locals 2

    new-instance p1, Ljava/io/IOException;

    sget-object v0, Lcom/jscape/inet/a/a/c/a/A;->y:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/A;->n:Ljava/io/IOException;

    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/q;)V
    .locals 0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/jscape/inet/a/a/c/a/A;->l:Z

    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/s;)V
    .locals 2

    iget-wide v0, p1, Lcom/jscape/inet/a/a/c/a/b/s;->e:J

    sget-object p1, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    invoke-static {v0, v1, p1}, Lcom/jscape/util/am;->e(JLcom/jscape/util/u;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/A;->o:J

    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/A;->j:Lcom/jscape/inet/a/a/c/a/i;

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/A;->o:J

    invoke-interface {p1, v0, v1}, Lcom/jscape/inet/a/a/c/a/i;->a(J)V

    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/A;->j:Lcom/jscape/inet/a/a/c/a/i;

    invoke-interface {p1}, Lcom/jscape/inet/a/a/c/a/i;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/A;->p:J

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/a/a/c/a/A;->d:I

    return v0
.end method

.method public close()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/a/a/c/a/A;->m:Z

    return-void
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/A;->r:J

    return-wide v0
.end method
