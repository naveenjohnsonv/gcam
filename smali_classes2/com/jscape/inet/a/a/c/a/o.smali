.class public Lcom/jscape/inet/a/a/c/a/o;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/AutoCloseable;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/inet/a/a/c/a/w;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/jscape/inet/a/a/c/a/q;",
            ">;"
        }
    .end annotation
.end field

.field private volatile c:Z

.field private d:Lcom/jscape/inet/a/a/c/a/r;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "M\u0008h+}r^m O\u001d8T\u000c\\!N\u000f}kY`:U\u000b1CTi<\u001c^."

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/a/c/a/o;->e:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/4 v5, 0x3

    if-eqz v4, :cond_6

    const/4 v6, 0x1

    if-eq v4, v6, :cond_5

    const/4 v6, 0x2

    if-eq v4, v6, :cond_4

    if-eq v4, v5, :cond_3

    const/4 v6, 0x4

    if-eq v4, v6, :cond_2

    const/4 v6, 0x5

    if-eq v4, v6, :cond_1

    const/16 v4, 0x2f

    goto :goto_1

    :cond_1
    const/16 v4, 0x25

    goto :goto_1

    :cond_2
    const/16 v4, 0x5e

    goto :goto_1

    :cond_3
    const/16 v4, 0x78

    goto :goto_1

    :cond_4
    const/16 v4, 0x3f

    goto :goto_1

    :cond_5
    const/16 v4, 0x4d

    goto :goto_1

    :cond_6
    const/16 v4, 0xf

    :goto_1
    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/jscape/inet/a/a/c/a/w;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/o;->a:Lcom/jscape/inet/a/a/c/a/w;

    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/o;->b:Ljava/util/Map;

    return-void
.end method

.method private static a(Ljava/io/InterruptedIOException;)Ljava/io/InterruptedIOException;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 7

    new-instance v0, Lcom/jscape/inet/a/a/c/a/p;

    invoke-direct {v0, p0}, Lcom/jscape/inet/a/a/c/a/p;-><init>(Lcom/jscape/inet/a/a/c/a/o;)V

    new-instance v1, Ljava/lang/Thread;

    sget-object v2, Lcom/jscape/inet/a/a/c/a/o;->e:Ljava/lang/String;

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/jscape/inet/a/a/c/a/o;->a:Lcom/jscape/inet/a/a/c/a/w;

    invoke-interface {v5}, Lcom/jscape/inet/a/a/c/a/w;->a()Ljava/net/InetSocketAddress;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/Thread;->setDaemon(Z)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method static a(Lcom/jscape/inet/a/a/c/a/o;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/o;->b()V

    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/jscape/inet/a/a/c/a/o;->c:Z

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/o;->d:Lcom/jscape/inet/a/a/c/a/r;

    invoke-interface {v0, p0, p1}, Lcom/jscape/inet/a/a/c/a/r;->a(Lcom/jscape/inet/a/a/c/a/o;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method private b()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-boolean v1, p0, Lcom/jscape/inet/a/a/c/a/o;->c:Z

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/o;->c()V

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method private c()V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/o;->a:Lcom/jscape/inet/a/a/c/a/w;

    invoke-interface {v1}, Lcom/jscape/inet/a/a/c/a/w;->b()Lcom/jscape/inet/a/a/c/a/b/i;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/o;->b:Ljava/util/Map;

    iget v3, v1, Lcom/jscape/inet/a/a/c/a/b/i;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/a/a/c/a/q;
    :try_end_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    :try_start_1
    invoke-interface {v2, v1}, Lcom/jscape/inet/a/a/c/a/q;->a(Lcom/jscape/inet/a/a/c/a/b/i;)V

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/o;->d:Lcom/jscape/inet/a/a/c/a/r;

    invoke-interface {v0, p0, v1}, Lcom/jscape/inet/a/a/c/a/r;->a(Lcom/jscape/inet/a/a/c/a/o;Lcom/jscape/inet/a/a/c/a/b/i;)V
    :try_end_1
    .catch Ljava/io/InterruptedIOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/a/a/c/a/o;->a(Ljava/io/InterruptedIOException;)Ljava/io/InterruptedIOException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/io/InterruptedIOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/a/a/c/a/o;->a(Ljava/lang/Throwable;)V

    :catch_1
    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/o;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(ILcom/jscape/inet/a/a/c/a/q;)V
    .locals 1

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/o;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/b/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/o;->a:Lcom/jscape/inet/a/a/c/a/w;

    invoke-interface {v0, p1}, Lcom/jscape/inet/a/a/c/a/w;->a(Lcom/jscape/inet/a/a/c/a/b/i;)V

    return-void
.end method

.method public a(Lcom/jscape/inet/a/a/c/a/r;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/o;->d:Lcom/jscape/inet/a/a/c/a/r;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/jscape/inet/a/a/c/a/o;->c:Z

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/o;->a()V

    return-void
.end method

.method public close()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/a/a/c/a/o;->c:Z

    return-void
.end method
