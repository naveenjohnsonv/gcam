.class public Lcom/jscape/inet/a/a/c/a/b/n;
.super Lcom/jscape/inet/a/a/c/a/b/i;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/a/a/c/a/b/i<",
        "Lcom/jscape/inet/a/a/c/a/b/e;",
        ">;"
    }
.end annotation


# static fields
.field private static final e:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x1d

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x1

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    const-string v7, "&DuHY1!\u000c\\\u007fl^=*\u000eN}}j;,\u0008Ng)A3+^\u0010O\u000b`fO(,\u0006jwmH?<\u0010\u0016\u0015O\u000bwlI.&\rJg`U4\u000e\u0007OalI)r"

    invoke-virtual {v7, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v8, v4

    move v9, v2

    :goto_1
    if-gt v8, v9, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x44

    if-ge v3, v4, :cond_0

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/a/a/c/a/b/n;->e:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v10, v4, v9

    rem-int/lit8 v11, v9, 0x7

    const/4 v12, 0x5

    if-eqz v11, :cond_7

    if-eq v11, v6, :cond_6

    const/4 v13, 0x2

    if-eq v11, v13, :cond_5

    if-eq v11, v0, :cond_4

    const/4 v13, 0x4

    if-eq v11, v13, :cond_3

    if-eq v11, v12, :cond_2

    const/16 v11, 0x4a

    goto :goto_2

    :cond_2
    const/16 v11, 0x5f

    goto :goto_2

    :cond_3
    const/16 v11, 0x3f

    goto :goto_2

    :cond_4
    const/16 v11, 0xc

    goto :goto_2

    :cond_5
    const/16 v11, 0x16

    goto :goto_2

    :cond_6
    const/16 v11, 0x2e

    goto :goto_2

    :cond_7
    const/16 v11, 0x66

    :goto_2
    xor-int/2addr v11, v12

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v4, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/a/a/c/a/b/i;-><init>(I)V

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/inet/a/a/c/a/b/e;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/jscape/inet/a/a/c/a/b/e;->a(Lcom/jscape/inet/a/a/c/a/b/n;)V

    return-void
.end method

.method public bridge synthetic a(Lcom/jscape/inet/a/a/c/a/b/u;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/a/a/c/a/b/e;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/a/a/c/a/b/n;->a(Lcom/jscape/inet/a/a/c/a/b/e;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/a/a/c/a/b/n;->e:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/b/n;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/b/n;->b:Ljava/net/InetSocketAddress;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/b/n;->c:Ljava/net/InetSocketAddress;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
