.class public abstract Lcom/jscape/inet/a/a/c/a/b/i;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<H::",
        "Lcom/jscape/inet/a/a/c/a/b/u;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:[Lcom/jscape/util/aq;

.field private static final i:[Ljava/lang/String;


# instance fields
.field public final a:I

.field public b:Ljava/net/InetSocketAddress;

.field public c:Ljava/net/InetSocketAddress;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/jscape/inet/a/a/c/a/b/i;->b([Lcom/jscape/util/aq;)V

    const/4 v2, 0x0

    const/16 v3, 0xb

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x3f

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\'B`\u0010F\u0015Z\u000cJgF\u0015[\u0003g\u001eP\u0015\u0013\u0019Bw\u0012L\u000f;\u0013Gq\u001eP\u0012G\u0010[\u0003p\u0014V\u0013\u0019\u0012bg\u001fQ\u0004\t\u0004\u001e"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x32

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/a/a/c/a/b/i;->i:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/16 v13, 0x1c

    if-eqz v12, :cond_5

    if-eq v12, v7, :cond_6

    const/4 v14, 0x2

    if-eq v12, v14, :cond_4

    if-eq v12, v0, :cond_3

    const/4 v14, 0x4

    if-eq v12, v14, :cond_6

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v13, 0x45

    goto :goto_2

    :cond_2
    const/16 v13, 0x5e

    goto :goto_2

    :cond_3
    const/16 v13, 0x44

    goto :goto_2

    :cond_4
    const/16 v13, 0x3c

    goto :goto_2

    :cond_5
    const/16 v13, 0x48

    :cond_6
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method protected constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jscape/inet/a/a/c/a/b/i;->a:I

    return-void
.end method

.method public static b([Lcom/jscape/util/aq;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/a/a/c/a/b/i;->d:[Lcom/jscape/util/aq;

    return-void
.end method

.method public static b()[Lcom/jscape/util/aq;
    .locals 1

    sget-object v0, Lcom/jscape/inet/a/a/c/a/b/i;->d:[Lcom/jscape/util/aq;

    return-object v0
.end method


# virtual methods
.method public abstract a(Lcom/jscape/inet/a/a/c/a/b/u;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TH;)V"
        }
    .end annotation
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/a/a/c/a/b/i;->i:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/b/i;->a:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/b/i;->b:Ljava/net/InetSocketAddress;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/b/i;->c:Ljava/net/InetSocketAddress;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
