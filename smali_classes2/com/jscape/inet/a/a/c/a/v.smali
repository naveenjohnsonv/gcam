.class public Lcom/jscape/inet/a/a/c/a/v;
.super Ljava/lang/Object;


# static fields
.field private static final a:I = 0x5

.field private static final b:J = 0x2710L

.field private static final k:[Ljava/lang/String;


# instance fields
.field private final c:J

.field private final d:Lcom/jscape/util/Time;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/jscape/inet/a/a/c/a/u;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/jscape/inet/a/a/c/a/f;

.field private g:J

.field private h:J

.field private i:I

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x8

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "\u000f?\u007f\u0003!|[@jh\u00059jwM|n\t.|[P\"\u0017\u000f?\u007f\u0003!|[@jh\u00059jzF|n\t.|[P\"\n\u000f?y\u0002;}WFl!\u0010\u000f?}\u001a*}_DzP\u00038]JW\"\u0014\u000f?y\u0014?fLBku\u0003![WNzs\u0019;2\u001fqkh8=n]HznL4zNSzn8=jMKpp\u0008\r`KM{!"

    const/16 v5, 0x80

    const/16 v6, 0x17

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x19

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x3

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v14, v11

    const/4 v15, 0x0

    :goto_2
    if-gt v14, v15, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v13, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x15

    const-string v4, "\u0015%v\u001e4fA\u0004\u000c\u0015%r\u001e\'pWQjj\u0012h"

    move v6, v0

    move v8, v11

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    add-int/2addr v7, v10

    add-int v9, v7, v6

    invoke-virtual {v4, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v9, v12

    const/4 v13, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/a/a/c/a/v;->k:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v15

    rem-int/lit8 v0, v15, 0x7

    const/4 v2, 0x5

    if-eqz v0, :cond_8

    if-eq v0, v10, :cond_7

    const/4 v3, 0x2

    if-eq v0, v3, :cond_9

    if-eq v0, v12, :cond_6

    const/4 v3, 0x4

    if-eq v0, v3, :cond_5

    if-eq v0, v2, :cond_4

    const/16 v2, 0x27

    goto :goto_4

    :cond_4
    const/16 v2, 0x16

    goto :goto_4

    :cond_5
    const/16 v2, 0x56

    goto :goto_4

    :cond_6
    const/16 v2, 0x75

    goto :goto_4

    :cond_7
    const/4 v2, 0x6

    goto :goto_4

    :cond_8
    const/16 v2, 0x3a

    :cond_9
    :goto_4
    xor-int v0, v9, v2

    xor-int v0, v16, v0

    int-to-char v0, v0

    aput-char v0, v11, v15

    add-int/lit8 v15, v15, 0x1

    const/16 v0, 0x8

    goto :goto_2
.end method

.method public constructor <init>(JLcom/jscape/util/Time;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/jscape/inet/a/a/c/a/v;->c:J

    iput-object p3, p0, Lcom/jscape/inet/a/a/c/a/v;->d:Lcom/jscape/util/Time;

    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/v;->e:Ljava/util/List;

    sget-object p1, Lcom/jscape/inet/a/a/c/a/f;->a:Lcom/jscape/inet/a/a/c/a/f;

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/v;->f:Lcom/jscape/inet/a/a/c/a/f;

    return-void
.end method

.method static a(Lcom/jscape/inet/a/a/c/a/v;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/a/a/c/a/v;->b(J)V

    return-void
.end method

.method private b(J)V
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/v;->e:Ljava/util/List;

    new-instance v1, Lcom/jscape/inet/a/a/c/a/u;

    invoke-direct {v1, p1, p2}, Lcom/jscape/inet/a/a/c/a/u;-><init>(J)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/jscape/inet/a/a/c/a/v;->e:Ljava/util/List;

    invoke-static {p2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/v;->c()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/v;->g()V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/v;->h()V

    :cond_0
    sget-object p1, Lcom/jscape/inet/a/a/c/a/f;->b:Lcom/jscape/inet/a/a/c/a/f;

    iput-object p1, p0, Lcom/jscape/inet/a/a/c/a/v;->f:Lcom/jscape/inet/a/a/c/a/f;

    :cond_1
    return-void
.end method

.method static b(Lcom/jscape/inet/a/a/c/a/v;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/a/a/c/a/v;->c(J)V

    return-void
.end method

.method private c(J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/a/a/c/a/v;->d(J)V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/v;->d()V

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/a/a/c/a/v;->e(J)V

    return-void
.end method

.method private c()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/v;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v0, :cond_1

    const/4 v0, 0x5

    if-ne v1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method private d()V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/LinkedList;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/v;->e:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/a/a/c/a/u;

    iget-object v3, p0, Lcom/jscape/inet/a/a/c/a/v;->d:Lcom/jscape/util/Time;

    invoke-virtual {v2, v3}, Lcom/jscape/inet/a/a/c/a/u;->a(Lcom/jscape/util/Time;)Z

    move-result v3

    if-nez v0, :cond_1

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/jscape/inet/a/a/c/a/v;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    :cond_1
    if-nez v0, :cond_2

    const/4 v4, 0x1

    if-le v3, v4, :cond_2

    iget-object v3, p0, Lcom/jscape/inet/a/a/c/a/v;->e:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_2
    if-eqz v0, :cond_0

    :cond_3
    return-void
.end method

.method private d(J)V
    .locals 7

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/v;->e:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/a/a/c/a/u;

    iget-wide v3, v1, Lcom/jscape/inet/a/a/c/a/u;->a:J

    cmp-long v1, p1, v3

    if-nez v0, :cond_0

    if-gez v1, :cond_2

    iget-wide v3, p0, Lcom/jscape/inet/a/a/c/a/v;->g:J

    iget-wide v5, p0, Lcom/jscape/inet/a/a/c/a/v;->h:J

    add-long/2addr v3, v5

    cmp-long v1, p1, v3

    :cond_0
    if-nez v0, :cond_1

    if-gez v1, :cond_2

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/v;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/v;->e:Ljava/util/List;

    new-instance v1, Lcom/jscape/inet/a/a/c/a/u;

    invoke-direct {v1, p1, p2}, Lcom/jscape/inet/a/a/c/a/u;-><init>(J)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object p1, p0, Lcom/jscape/inet/a/a/c/a/v;->e:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/v;->g()V

    :cond_2
    return-void
.end method

.method private e()V
    .locals 1

    iget v0, p0, Lcom/jscape/inet/a/a/c/a/v;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/jscape/inet/a/a/c/a/v;->j:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/inet/a/a/c/a/v;->i:I

    return-void
.end method

.method private e(J)V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lcom/jscape/inet/a/a/c/a/v;->g:J

    iget-wide v3, p0, Lcom/jscape/inet/a/a/c/a/v;->h:J

    add-long/2addr v1, v3

    cmp-long p1, p1, v1

    if-lez p1, :cond_0

    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/v;->e()V

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/jscape/inet/a/a/c/a/v;->f()V

    :cond_1
    return-void
.end method

.method private f()V
    .locals 1

    iget v0, p0, Lcom/jscape/inet/a/a/c/a/v;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/jscape/inet/a/a/c/a/v;->i:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/inet/a/a/c/a/v;->j:I

    return-void
.end method

.method private g()V
    .locals 8

    invoke-static {}, Lcom/jscape/inet/a/a/c/a/s;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/a/a/c/a/v;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const-wide/16 v2, 0x0

    move-wide v4, v2

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/jscape/inet/a/a/c/a/u;

    iget-wide v6, v6, Lcom/jscape/inet/a/a/c/a/u;->a:J

    add-long/2addr v2, v6

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    if-nez v0, :cond_2

    if-eqz v0, :cond_0

    :cond_1
    div-long/2addr v2, v4

    iput-wide v2, p0, Lcom/jscape/inet/a/a/c/a/v;->g:J

    :cond_2
    return-void
.end method

.method private h()V
    .locals 4

    iget-wide v0, p0, Lcom/jscape/inet/a/a/c/a/v;->g:J

    const-wide/16 v2, 0x5

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/v;->c:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    const-wide/16 v2, 0x2710

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/jscape/inet/a/a/c/a/v;->h:J

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/a/a/c/a/v;->i:I

    return v0
.end method

.method public a(J)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/a/a/c/a/v;->f:Lcom/jscape/inet/a/a/c/a/f;

    invoke-virtual {v0, p0, p1, p2}, Lcom/jscape/inet/a/a/c/a/f;->a(Lcom/jscape/inet/a/a/c/a/v;J)V

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/a/a/c/a/v;->j:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/a/a/c/a/v;->k:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/v;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/v;->d:Lcom/jscape/util/Time;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/v;->e:Ljava/util/List;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/c/a/v;->f:Lcom/jscape/inet/a/a/c/a/f;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/v;->g:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/a/a/c/a/v;->h:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/a/a/c/a/v;->i:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/a/a/c/a/v;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
