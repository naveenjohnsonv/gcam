.class public final enum Lcom/jscape/inet/a/a/b/ErrorCode;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/a/a/b/ErrorCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/jscape/inet/a/a/b/ErrorCode;

.field public static final enum INTERNAL_ERROR:Lcom/jscape/inet/a/a/b/ErrorCode;

.field public static final enum INVALID_PATH:Lcom/jscape/inet/a/a/b/ErrorCode;

.field public static final enum IO_ERROR:Lcom/jscape/inet/a/a/b/ErrorCode;

.field public static final enum NOT_AUTHENTICATED:Lcom/jscape/inet/a/a/b/ErrorCode;

.field public static final enum NO_PERMISSIONS:Lcom/jscape/inet/a/a/b/ErrorCode;

.field public static final enum PROTOCOL_ERROR:Lcom/jscape/inet/a/a/b/ErrorCode;

.field public static final enum RESET_PASSWORD_REQUIRED:Lcom/jscape/inet/a/a/b/ErrorCode;

.field public static final enum SECURED_CONNECTION_REQUIRED:Lcom/jscape/inet/a/a/b/ErrorCode;


# instance fields
.field public final code:I


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const/16 v0, 0x8

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "\u000e\u0015+@\u0013\u000f$\u001d\u0003+R\u0008\u00020\u0003\u0002=T\u0012\u0019&\u0019\u0014\u000e\u0012\u001f\'U\u0002\u00029\u0015\u0003+L\u0008\u001e\'\u000c\u0015\u001e.D\u000b\u00190\u0003\u00009Q\u000f\u0008\u0015\u001f\'@\u0015\u0002;\u000e\u000e\u0015\u001e,@\u0015\u001e5\u0010\u000f=W\u0015\u001f&\u000e\u000c\u00027Q\u0008\u0013;\u0010\u000f=W\u0015\u001f&"

    const/16 v5, 0x5a

    const/16 v6, 0x17

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x18

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x5

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v14, v11

    move v15, v3

    :goto_2
    const/4 v0, 0x3

    const/4 v2, 0x2

    if-gt v14, v15, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v13, :cond_1

    add-int/lit8 v0, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v0

    const/16 v0, 0x8

    goto :goto_0

    :cond_0
    const/16 v5, 0x2d

    const/16 v2, 0x11

    const-string v4, "\u000f\u00021G\u001b\u0018=\t\u0008+L\u0013\u000e(\u0015\u0008!\u001b\u0012\u0008&M\u0008\u0008-\u001e\u000e*V\u0014\u0008*\u0015\u0004*V\u0005\u001f,\u0010\u0018,J\u001f\t"

    move v8, v0

    move v6, v2

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v6, v0

    move v8, v11

    :goto_3
    add-int/2addr v7, v10

    add-int v0, v7, v6

    invoke-virtual {v4, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v13, v3

    move v9, v12

    const/16 v0, 0x8

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/jscape/inet/a/a/b/ErrorCode;

    aget-object v5, v1, v12

    invoke-direct {v4, v5, v3, v3}, Lcom/jscape/inet/a/a/b/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/a/a/b/ErrorCode;->PROTOCOL_ERROR:Lcom/jscape/inet/a/a/b/ErrorCode;

    new-instance v4, Lcom/jscape/inet/a/a/b/ErrorCode;

    const/4 v5, 0x6

    aget-object v6, v1, v5

    invoke-direct {v4, v6, v10, v10}, Lcom/jscape/inet/a/a/b/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/a/a/b/ErrorCode;->NOT_AUTHENTICATED:Lcom/jscape/inet/a/a/b/ErrorCode;

    new-instance v4, Lcom/jscape/inet/a/a/b/ErrorCode;

    const/4 v6, 0x7

    aget-object v7, v1, v6

    invoke-direct {v4, v7, v2, v2}, Lcom/jscape/inet/a/a/b/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/a/a/b/ErrorCode;->SECURED_CONNECTION_REQUIRED:Lcom/jscape/inet/a/a/b/ErrorCode;

    new-instance v4, Lcom/jscape/inet/a/a/b/ErrorCode;

    aget-object v7, v1, v3

    invoke-direct {v4, v7, v0, v0}, Lcom/jscape/inet/a/a/b/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/a/a/b/ErrorCode;->RESET_PASSWORD_REQUIRED:Lcom/jscape/inet/a/a/b/ErrorCode;

    new-instance v4, Lcom/jscape/inet/a/a/b/ErrorCode;

    aget-object v7, v1, v2

    const/4 v8, 0x4

    invoke-direct {v4, v7, v8, v8}, Lcom/jscape/inet/a/a/b/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/a/a/b/ErrorCode;->INVALID_PATH:Lcom/jscape/inet/a/a/b/ErrorCode;

    new-instance v4, Lcom/jscape/inet/a/a/b/ErrorCode;

    aget-object v7, v1, v10

    invoke-direct {v4, v7, v12, v12}, Lcom/jscape/inet/a/a/b/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/a/a/b/ErrorCode;->NO_PERMISSIONS:Lcom/jscape/inet/a/a/b/ErrorCode;

    new-instance v4, Lcom/jscape/inet/a/a/b/ErrorCode;

    aget-object v7, v1, v0

    invoke-direct {v4, v7, v5, v5}, Lcom/jscape/inet/a/a/b/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/a/a/b/ErrorCode;->IO_ERROR:Lcom/jscape/inet/a/a/b/ErrorCode;

    new-instance v4, Lcom/jscape/inet/a/a/b/ErrorCode;

    aget-object v1, v1, v8

    invoke-direct {v4, v1, v6, v6}, Lcom/jscape/inet/a/a/b/ErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/a/a/b/ErrorCode;->INTERNAL_ERROR:Lcom/jscape/inet/a/a/b/ErrorCode;

    const/16 v1, 0x8

    new-array v1, v1, [Lcom/jscape/inet/a/a/b/ErrorCode;

    sget-object v7, Lcom/jscape/inet/a/a/b/ErrorCode;->PROTOCOL_ERROR:Lcom/jscape/inet/a/a/b/ErrorCode;

    aput-object v7, v1, v3

    sget-object v3, Lcom/jscape/inet/a/a/b/ErrorCode;->NOT_AUTHENTICATED:Lcom/jscape/inet/a/a/b/ErrorCode;

    aput-object v3, v1, v10

    sget-object v3, Lcom/jscape/inet/a/a/b/ErrorCode;->SECURED_CONNECTION_REQUIRED:Lcom/jscape/inet/a/a/b/ErrorCode;

    aput-object v3, v1, v2

    sget-object v2, Lcom/jscape/inet/a/a/b/ErrorCode;->RESET_PASSWORD_REQUIRED:Lcom/jscape/inet/a/a/b/ErrorCode;

    aput-object v2, v1, v0

    sget-object v0, Lcom/jscape/inet/a/a/b/ErrorCode;->INVALID_PATH:Lcom/jscape/inet/a/a/b/ErrorCode;

    const/4 v2, 0x4

    aput-object v0, v1, v2

    sget-object v0, Lcom/jscape/inet/a/a/b/ErrorCode;->NO_PERMISSIONS:Lcom/jscape/inet/a/a/b/ErrorCode;

    aput-object v0, v1, v12

    sget-object v0, Lcom/jscape/inet/a/a/b/ErrorCode;->IO_ERROR:Lcom/jscape/inet/a/a/b/ErrorCode;

    aput-object v0, v1, v5

    aput-object v4, v1, v6

    sput-object v1, Lcom/jscape/inet/a/a/b/ErrorCode;->$VALUES:[Lcom/jscape/inet/a/a/b/ErrorCode;

    return-void

    :cond_3
    const/16 v16, 0x8

    aget-char v17, v11, v15

    rem-int/lit8 v3, v15, 0x7

    const/16 v18, 0x48

    if-eqz v3, :cond_7

    if-eq v3, v10, :cond_8

    if-eq v3, v2, :cond_6

    if-eq v3, v0, :cond_5

    const/4 v0, 0x4

    if-eq v3, v0, :cond_4

    if-eq v3, v12, :cond_8

    const/16 v18, 0x6c

    goto :goto_4

    :cond_4
    const/16 v18, 0x5f

    goto :goto_4

    :cond_5
    const/16 v18, 0x1d

    goto :goto_4

    :cond_6
    const/16 v18, 0x60

    goto :goto_4

    :cond_7
    const/16 v18, 0x44

    :cond_8
    :goto_4
    xor-int v0, v9, v18

    xor-int v0, v17, v0

    int-to-char v0, v0

    aput-char v0, v11, v15

    add-int/lit8 v15, v15, 0x1

    move/from16 v0, v16

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/a/a/b/ErrorCode;->code:I

    return-void
.end method

.method public static forCode(I)Lcom/jscape/inet/a/a/b/ErrorCode;
    .locals 6

    invoke-static {}, Lcom/jscape/inet/a/a/b/ErrorCode;->values()[Lcom/jscape/inet/a/a/b/ErrorCode;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/a/a/b/n;->b()I

    move-result v1

    array-length v2, v0

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v2, :cond_3

    aget-object v4, v0, v3

    if-eqz v1, :cond_2

    if-eqz v1, :cond_4

    iget v5, v4, Lcom/jscape/inet/a/a/b/ErrorCode;->code:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    :cond_2
    if-nez v1, :cond_0

    :cond_3
    sget-object v4, Lcom/jscape/inet/a/a/b/ErrorCode;->PROTOCOL_ERROR:Lcom/jscape/inet/a/a/b/ErrorCode;

    :cond_4
    return-object v4
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/a/a/b/ErrorCode;
    .locals 1

    const-class v0, Lcom/jscape/inet/a/a/b/ErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/a/a/b/ErrorCode;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/a/a/b/ErrorCode;
    .locals 1

    sget-object v0, Lcom/jscape/inet/a/a/b/ErrorCode;->$VALUES:[Lcom/jscape/inet/a/a/b/ErrorCode;

    invoke-virtual {v0}, [Lcom/jscape/inet/a/a/b/ErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/a/a/b/ErrorCode;

    return-object v0
.end method
