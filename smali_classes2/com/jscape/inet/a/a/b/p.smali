.class public Lcom/jscape/inet/a/a/b/p;
.super Lcom/jscape/inet/a/a/b/o;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/a/a/b/o<",
        "Lcom/jscape/inet/a/a/b/d;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "i\u001b)5 )\u0003X\u001b)%# %i\u0015*;$+3\n\u0001.2x"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/a/b/p;->d:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v5, 0x7f

    if-eqz v4, :cond_4

    const/4 v6, 0x1

    if-eq v4, v6, :cond_3

    const/4 v6, 0x2

    if-eq v4, v6, :cond_2

    const/4 v6, 0x3

    if-eq v4, v6, :cond_1

    const/4 v6, 0x4

    if-eq v4, v6, :cond_5

    const/4 v6, 0x5

    if-eq v4, v6, :cond_5

    const/16 v5, 0x6d

    goto :goto_1

    :cond_1
    const/16 v5, 0x6c

    goto :goto_1

    :cond_2
    const/16 v5, 0x7d

    goto :goto_1

    :cond_3
    const/16 v5, 0x40

    goto :goto_1

    :cond_4
    const/16 v5, 0x10

    :cond_5
    :goto_1
    const/16 v4, 0x3a

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/a/a/b/o;-><init>()V

    iput p1, p0, Lcom/jscape/inet/a/a/b/p;->c:I

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/inet/a/a/b/d;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/a/a/b/d;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/a/a/b/n;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/a/a/b/d;->a(Lcom/jscape/inet/a/a/b/p;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public bridge synthetic a(Lcom/jscape/inet/a/a/b/f;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/a/a/b/d;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/b/p;->a(Lcom/jscape/inet/a/a/b/d;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/a/a/b/p;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/a/a/b/p;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
