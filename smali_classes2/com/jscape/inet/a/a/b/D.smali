.class public Lcom/jscape/inet/a/a/b/D;
.super Lcom/jscape/inet/a/a/b/o;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/a/a/b/o<",
        "Lcom/jscape/inet/a/a/b/ah;",
        ">;"
    }
.end annotation


# static fields
.field private static final i:[Ljava/lang/String;


# instance fields
.field public final c:Ljava/lang/String;

.field public final d:J

.field public final e:J

.field public final f:Lcom/jscape/util/A;

.field public final g:Z

.field public final h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "\u0006pKjBYzO#[l@GZO!]l]Ll\u0017\u001ex5Iai@dO\u0003Mf[@gD\u0013GhBHfNpSuN]`\u0017w\t\u0006pGcIZm^m\t\u0006pD`AN|Bm"

    const/16 v4, 0x49

    const/16 v5, 0x16

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x55

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x27

    const/16 v3, 0x1c

    const-string v5, "\r{@aJEfR/JaJalO/QaHpfP.J|AF>\n\r{AgPpbU>\u001e"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x5e

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/a/a/b/D;->i:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    const/4 v1, 0x5

    if-eqz v15, :cond_8

    if-eq v15, v9, :cond_9

    const/4 v2, 0x2

    if-eq v15, v2, :cond_7

    const/4 v2, 0x3

    if-eq v15, v2, :cond_6

    const/4 v2, 0x4

    if-eq v15, v2, :cond_5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x5d

    goto :goto_4

    :cond_4
    const/16 v1, 0x7c

    goto :goto_4

    :cond_5
    const/16 v1, 0x7a

    goto :goto_4

    :cond_6
    const/16 v1, 0x50

    goto :goto_4

    :cond_7
    const/16 v1, 0x7d

    goto :goto_4

    :cond_8
    const/16 v1, 0x7f

    :cond_9
    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;JJLcom/jscape/util/A;ZZ)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/a/a/b/o;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/b/D;->c:Ljava/lang/String;

    iput-wide p2, p0, Lcom/jscape/inet/a/a/b/D;->d:J

    iput-wide p4, p0, Lcom/jscape/inet/a/a/b/D;->e:J

    invoke-static {p6}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p6, p0, Lcom/jscape/inet/a/a/b/D;->f:Lcom/jscape/util/A;

    iput-boolean p7, p0, Lcom/jscape/inet/a/a/b/D;->g:Z

    iput-boolean p8, p0, Lcom/jscape/inet/a/a/b/D;->h:Z

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/inet/a/a/b/ah;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/a/a/b/ah;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/a/a/b/n;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/a/a/b/ah;->a(Lcom/jscape/inet/a/a/b/D;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public bridge synthetic a(Lcom/jscape/inet/a/a/b/f;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/a/a/b/ah;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/b/D;->a(Lcom/jscape/inet/a/a/b/ah;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    invoke-static {}, Lcom/jscape/inet/a/a/b/n;->b()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/a/a/b/D;->i:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v4, v2, v3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/a/a/b/D;->c:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x27

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v4, 0x2

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/jscape/inet/a/a/b/D;->d:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v4, 0x3

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/jscape/inet/a/a/b/D;->e:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v4, 0x5

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/a/a/b/D;->f:Lcom/jscape/util/A;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v4, 0x4

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/jscape/inet/a/a/b/D;->g:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/a/a/b/D;->h:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v2

    if-nez v2, :cond_0

    add-int/2addr v0, v3

    invoke-static {v0}, Lcom/jscape/inet/a/a/b/n;->b(I)V

    :cond_0
    return-object v1
.end method
