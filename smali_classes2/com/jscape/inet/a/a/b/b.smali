.class public final enum Lcom/jscape/inet/a/a/b/b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/a/a/b/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/jscape/inet/a/a/b/b;

.field public static final enum b:Lcom/jscape/inet/a/a/b/b;

.field private static final d:[Lcom/jscape/inet/a/a/b/b;

.field private static final e:[Ljava/lang/String;


# instance fields
.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x4

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x58

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "eLO8\tgLQ8-q*q\\\u0017vkp\u0008\u001eU\nQqf\u0019NQ\u001cS`#\u001e\u0001A\u0000\u0019%"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    const/4 v12, 0x2

    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x26

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move v15, v5

    move v5, v4

    move v4, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/a/a/b/b;->e:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/a/a/b/b;

    sget-object v1, Lcom/jscape/inet/a/a/b/b;->e:[Ljava/lang/String;

    aget-object v2, v1, v3

    invoke-direct {v0, v2, v3, v3}, Lcom/jscape/inet/a/a/b/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/jscape/inet/a/a/b/b;->a:Lcom/jscape/inet/a/a/b/b;

    new-instance v0, Lcom/jscape/inet/a/a/b/b;

    aget-object v1, v1, v8

    invoke-direct {v0, v1, v8, v8}, Lcom/jscape/inet/a/a/b/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/jscape/inet/a/a/b/b;->b:Lcom/jscape/inet/a/a/b/b;

    new-array v1, v12, [Lcom/jscape/inet/a/a/b/b;

    sget-object v2, Lcom/jscape/inet/a/a/b/b;->a:Lcom/jscape/inet/a/a/b/b;

    aput-object v2, v1, v3

    aput-object v0, v1, v8

    sput-object v1, Lcom/jscape/inet/a/a/b/b;->d:[Lcom/jscape/inet/a/a/b/b;

    return-void

    :cond_1
    aget-char v13, v4, v11

    rem-int/lit8 v14, v11, 0x7

    if-eqz v14, :cond_7

    if-eq v14, v8, :cond_6

    if-eq v14, v12, :cond_5

    if-eq v14, v0, :cond_4

    if-eq v14, v2, :cond_3

    const/4 v12, 0x5

    if-eq v14, v12, :cond_2

    const/16 v12, 0x3d

    goto :goto_2

    :cond_2
    const/16 v12, 0x7d

    goto :goto_2

    :cond_3
    const/16 v12, 0x36

    goto :goto_2

    :cond_4
    const/16 v12, 0x25

    goto :goto_2

    :cond_5
    const/16 v12, 0x5b

    goto :goto_2

    :cond_6
    const/16 v12, 0x5d

    goto :goto_2

    :cond_7
    const/16 v12, 0x7b

    :goto_2
    xor-int/2addr v12, v7

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/a/a/b/b;->c:I

    return-void
.end method

.method public static a(I)Lcom/jscape/inet/a/a/b/b;
    .locals 6

    invoke-static {}, Lcom/jscape/inet/a/a/b/b;->a()[Lcom/jscape/inet/a/a/b/b;

    move-result-object v0

    array-length v1, v0

    invoke-static {}, Lcom/jscape/inet/a/a/b/n;->c()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    if-nez v2, :cond_1

    :try_start_0
    iget v5, v4, Lcom/jscape/inet/a/a/b/b;->c:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v5, p0, :cond_0

    return-object v4

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/inet/a/a/b/b;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/a/a/b/b;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_1
    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/a/a/b/b;->e:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/inet/a/a/b/b;
    .locals 1

    const-class v0, Lcom/jscape/inet/a/a/b/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/a/a/b/b;

    return-object p0
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method

.method public static a()[Lcom/jscape/inet/a/a/b/b;
    .locals 1

    sget-object v0, Lcom/jscape/inet/a/a/b/b;->d:[Lcom/jscape/inet/a/a/b/b;

    invoke-virtual {v0}, [Lcom/jscape/inet/a/a/b/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/a/a/b/b;

    return-object v0
.end method
