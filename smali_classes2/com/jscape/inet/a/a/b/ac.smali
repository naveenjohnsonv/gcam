.class public Lcom/jscape/inet/a/a/b/ac;
.super Ljava/lang/Object;


# static fields
.field private static final g:[Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/jscape/inet/a/a/b/b;

.field public final c:Z

.field public final d:Z

.field public final e:J

.field public final f:J


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x7

    const-string v4, "1 \u000c$<N8\u000b1 \r(\'Od\u007fl\u001ap\u0010Ma\u000b%\u000fEcr \u0004#\'F` \'\u00071 \u000b46N8"

    const/16 v5, 0x2c

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/4 v8, 0x1

    add-int/2addr v6, v8

    add-int v9, v6, v1

    invoke-virtual {v4, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0x57

    move v12, v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    array-length v13, v9

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v9}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v12}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v11, :cond_1

    add-int/lit8 v11, v7, 0x1

    aput-object v9, v0, v7

    add-int/2addr v6, v1

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x23

    const/16 v1, 0xb

    const-string v4, "BS{L\\,\u0017\u000c\u001fi\u0003\u0017BS`_F,;\u0001\u0017eX\\;\u0017\u001a\u001acPq9\u0002\u000bN"

    move v7, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v7, 0x1

    aput-object v9, v0, v7

    add-int/2addr v6, v1

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v11

    :goto_3
    const/16 v12, 0x24

    add-int/2addr v6, v8

    add-int v9, v6, v1

    invoke-virtual {v4, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/a/a/b/ac;->g:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v9, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v8, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    const/4 v3, 0x4

    if-eq v2, v3, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v2, 0x52

    goto :goto_4

    :cond_4
    const/16 v2, 0x7c

    goto :goto_4

    :cond_5
    const/16 v2, 0x11

    goto :goto_4

    :cond_6
    const/16 v2, 0x1a

    goto :goto_4

    :cond_7
    const/16 v2, 0x28

    goto :goto_4

    :cond_8
    move v2, v10

    goto :goto_4

    :cond_9
    const/16 v2, 0x4a

    :goto_4
    xor-int/2addr v2, v12

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v9, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/jscape/inet/a/a/b/b;ZZJJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/b/ac;->a:Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/a/a/b/ac;->b:Lcom/jscape/inet/a/a/b/b;

    iput-boolean p3, p0, Lcom/jscape/inet/a/a/b/ac;->c:Z

    iput-boolean p4, p0, Lcom/jscape/inet/a/a/b/ac;->d:Z

    iput-wide p5, p0, Lcom/jscape/inet/a/a/b/ac;->e:J

    iput-wide p7, p0, Lcom/jscape/inet/a/a/b/ac;->f:J

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/a/a/b/ac;->b:Lcom/jscape/inet/a/a/b/b;

    sget-object v1, Lcom/jscape/inet/a/a/b/b;->b:Lcom/jscape/inet/a/a/b/b;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    invoke-static {}, Lcom/jscape/inet/a/a/b/n;->c()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/a/a/b/ac;->g:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/a/a/b/ac;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x27

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x3

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/a/a/b/ac;->b:Lcom/jscape/inet/a/a/b/b;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/jscape/inet/a/a/b/ac;->c:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v3, 0x4

    aget-object v4, v2, v3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/jscape/inet/a/a/b/ac;->d:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/jscape/inet/a/a/b/ac;->e:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v4, 0x5

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/jscape/inet/a/a/b/ac;->f:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    new-array v0, v3, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object v1
.end method
