.class public Lcom/jscape/inet/a/a/b/C;
.super Lcom/jscape/inet/a/a/b/o;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/a/a/b/o<",
        "Lcom/jscape/inet/a/a/b/ag;",
        ">;"
    }
.end annotation


# static fields
.field private static final h:[Ljava/lang/String;


# instance fields
.field public final c:Ljava/lang/String;

.field public final d:J

.field public final e:Lcom/jscape/util/A;

.field public final f:Z

.field public final g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const-string v5, "pN<?|UF(S\u0017\u000e\u000b2=\\OO9-<4wGM8N(){RKaI\npN10ntB(\u000bn"

    const/16 v6, 0x2c

    const/4 v7, -0x1

    const/16 v8, 0x9

    const/4 v9, 0x0

    :goto_0
    const/16 v10, 0x50

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    const/4 v15, 0x0

    :goto_2
    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    add-int/lit8 v12, v9, 0x1

    if-eqz v13, :cond_1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v12

    goto :goto_0

    :cond_0
    const/16 v6, 0x33

    const/16 v5, 0x1c

    const-string v7, "\\b\u001c\u001aXmj\u00036\u0016\u001aXI`\u001e6\r\u001aZXj\u00017\u0016\u0007Sn2\u0016\\b\u001c\u001a[z}\u00151\u000c\u001cYd]\u00153\n\u001cDokM"

    move v8, v5

    move-object v5, v7

    move v9, v12

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v12

    :goto_3
    const/16 v10, 0x7c

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/a/a/b/C;->h:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v2, v15, 0x7

    const/4 v3, 0x3

    if-eqz v2, :cond_8

    if-eq v2, v11, :cond_7

    const/4 v4, 0x2

    if-eq v2, v4, :cond_9

    if-eq v2, v3, :cond_6

    const/4 v3, 0x4

    if-eq v2, v3, :cond_5

    if-eq v2, v0, :cond_4

    const/16 v3, 0x73

    goto :goto_4

    :cond_4
    const/16 v3, 0x76

    goto :goto_4

    :cond_5
    const/16 v3, 0x4a

    goto :goto_4

    :cond_6
    const/16 v3, 0x9

    goto :goto_4

    :cond_7
    const/16 v3, 0x3e

    goto :goto_4

    :cond_8
    const/16 v3, 0xc

    :cond_9
    :goto_4
    xor-int v2, v10, v3

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v12, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;JLcom/jscape/util/A;ZZ)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/a/a/b/o;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/a/a/b/C;->c:Ljava/lang/String;

    iput-wide p2, p0, Lcom/jscape/inet/a/a/b/C;->d:J

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/a/a/b/C;->e:Lcom/jscape/util/A;

    iput-boolean p5, p0, Lcom/jscape/inet/a/a/b/C;->f:Z

    iput-boolean p6, p0, Lcom/jscape/inet/a/a/b/C;->g:Z

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/inet/a/a/b/ag;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/a/a/b/ag;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/a/a/b/n;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/a/a/b/ag;->a(Lcom/jscape/inet/a/a/b/C;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public bridge synthetic a(Lcom/jscape/inet/a/a/b/f;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/a/a/b/ag;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/a/a/b/C;->a(Lcom/jscape/inet/a/a/b/ag;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/a/a/b/C;->h:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/b/C;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/a/a/b/C;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/a/a/b/C;->e:Lcom/jscape/util/A;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/a/a/b/C;->f:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/inet/a/a/b/C;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
