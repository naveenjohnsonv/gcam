.class public Lcom/jscape/inet/a/a/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/a/a/f;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/math/BigInteger;

.field private static final c:Ljava/math/BigInteger;

.field private static final d:I = 0xa

.field private static final e:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x5

    const/4 v4, 0x2

    const-string v6, "t=\u0002t="

    move v10, v2

    move v8, v4

    const/4 v7, -0x1

    const/4 v9, 0x0

    :goto_0
    const/16 v11, 0x5b

    const/4 v12, 0x1

    add-int/2addr v7, v12

    add-int v13, v7, v8

    invoke-virtual {v6, v7, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    :goto_1
    invoke-virtual {v13}, Ljava/lang/String;->toCharArray()[C

    move-result-object v13

    array-length v15, v13

    const/4 v3, 0x0

    :goto_2
    if-gt v15, v3, :cond_3

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v13}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v3}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v11, v9, 0x1

    if-eqz v14, :cond_1

    aput-object v3, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v10, :cond_0

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v11

    goto :goto_0

    :cond_0
    const/16 v3, 0x13e

    const/16 v6, 0x100

    const-string v7, "X\u001dTI+wWX\u001dTI+wWX\u001dQ6]wU_\u001a =\\\u0007)]i!;.\u0005R(m 7/\t!Z\u0018#L)\u0000#\'k ;(\u0001)&\u001a$8.r&*k ?/sT_m!M\\\u0002(\\i :\\\u0005P.c%6Ut\"*k&K)tW\'n#6/\u0002RZhS;^\u0000S-k M]p\'Z\u001d :+\u0000%-l&I(\u0000\"+mV9)\u0004 ]i&:(\u0005)+\u0019\'8[\u0007#+\u001e%J.\u0007W*oQ;_t(_m!8(u\'\\kPI+\u0004R\\mT;]\u0007S)\u001eVJ(\u0002)(\u0019TMXp)\'bTNXpT\'\u001d ;\\\u0000&]oP>+t\'*b 7[\u0007$/\u001eQJ[\u0004\"&jTI+wWX\u001dTI+wWX\u001dTI=[)``\u001f\u0011v{5w}\u000cExp<2y\u000c]xz{yj\u0014\u0011a\u007f2`5M_ds9w}M^w>:f{\u0008\\aj(2j\u0015Rt{?wkC"

    move v10, v3

    move v8, v6

    move-object v6, v7

    move v9, v11

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v3, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v10, :cond_2

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v3

    move v8, v3

    move v9, v11

    :goto_3
    const/16 v11, 0x75

    add-int/2addr v7, v12

    add-int v3, v7, v8

    invoke-virtual {v6, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/a/a/e;->e:[Ljava/lang/String;

    aget-object v0, v1, v12

    sput-object v0, Lcom/jscape/inet/a/a/e;->a:Ljava/lang/String;

    const-wide/16 v0, 0x2

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/a/a/e;->b:Ljava/math/BigInteger;

    new-instance v0, Ljava/math/BigInteger;

    sget-object v1, Lcom/jscape/inet/a/a/e;->e:[Ljava/lang/String;

    aget-object v1, v1, v4

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/jscape/inet/a/a/e;->c:Ljava/math/BigInteger;

    return-void

    :cond_3
    aget-char v16, v13, v3

    rem-int/lit8 v5, v3, 0x7

    if-eqz v5, :cond_9

    if-eq v5, v12, :cond_8

    if-eq v5, v4, :cond_7

    const/4 v4, 0x3

    if-eq v5, v4, :cond_6

    if-eq v5, v0, :cond_5

    if-eq v5, v2, :cond_4

    const/16 v4, 0x64

    goto :goto_4

    :cond_4
    const/16 v4, 0x44

    goto :goto_4

    :cond_5
    const/16 v4, 0x18

    goto :goto_4

    :cond_6
    const/16 v4, 0x7a

    goto :goto_4

    :cond_7
    const/16 v4, 0x67

    goto :goto_4

    :cond_8
    const/16 v4, 0x2e

    goto :goto_4

    :cond_9
    const/16 v4, 0x6b

    :goto_4
    xor-int/2addr v4, v11

    xor-int v4, v16, v4

    int-to-char v4, v4

    aput-char v4, v13, v3

    add-int/lit8 v3, v3, 0x1

    const/4 v4, 0x2

    goto/16 :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/math/BigInteger;)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/a/a/g;->b()[Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {p1, v1}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v1

    if-eqz v0, :cond_0

    if-lez v1, :cond_1

    sget-object v1, Lcom/jscape/inet/a/a/e;->c:Ljava/math/BigInteger;

    invoke-virtual {p1, v1}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v1

    :cond_0
    if-eqz v0, :cond_2

    if-gez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move p1, v1

    :goto_1
    return p1
.end method

.method private a(Ljava/math/BigInteger;Ljava/math/BigInteger;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/a/a/e;->c:Ljava/math/BigInteger;

    invoke-virtual {p1, p2, v0}, Ljava/math/BigInteger;->modPow(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p1

    invoke-virtual {p1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object p1

    return-object p1
.end method

.method private a([BI)[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/o;

    invoke-direct {v0, p2}, Lcom/jscape/util/h/o;-><init>(I)V

    invoke-static {}, Lcom/jscape/inet/a/a/g;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/jscape/a/k;->d()Lcom/jscape/a/k;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/jscape/a/d;->b([B)[B

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jscape/util/h/o;->write([B)V

    :cond_0
    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result p1

    const/4 v3, 0x0

    if-ge p1, p2, :cond_1

    invoke-interface {v2}, Lcom/jscape/a/d;->b()Lcom/jscape/a/d;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object p1

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result v4

    invoke-interface {v2, p1, v3, v4}, Lcom/jscape/a/d;->b([BII)[B

    move-result-object p1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {v0, p1}, Lcom/jscape/util/h/o;->write([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/a/e;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object p1

    new-array v0, p2, [B

    invoke-static {p1, v3, v0, v3, p2}, Lcom/jscape/util/v;->a([BI[BII)[B

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public a()Lcom/jscape/inet/a/a/g;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/a/d;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/g;->b()[Ljava/lang/String;

    move-result-object v0

    :try_start_0
    sget-object v1, Lcom/jscape/inet/a/a/e;->e:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    move-result-object v1

    new-instance v3, Ljavax/crypto/spec/DHParameterSpec;

    sget-object v4, Lcom/jscape/inet/a/a/e;->c:Ljava/math/BigInteger;

    sget-object v5, Lcom/jscape/inet/a/a/e;->b:Ljava/math/BigInteger;

    invoke-direct {v3, v4, v5}, Ljavax/crypto/spec/DHParameterSpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    invoke-virtual {v1, v3}, Ljava/security/KeyPairGenerator;->initialize(Ljava/security/spec/AlgorithmParameterSpec;)V

    :goto_0
    const/16 v3, 0xa

    if-ge v2, v3, :cond_3

    invoke-virtual {v1}, Ljava/security/KeyPairGenerator;->genKeyPair()Ljava/security/KeyPair;

    move-result-object v3

    invoke-virtual {v3}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v4

    check-cast v4, Ljavax/crypto/interfaces/DHPublicKey;

    invoke-interface {v4}, Ljavax/crypto/interfaces/DHPublicKey;->getY()Ljava/math/BigInteger;

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    :try_start_1
    invoke-direct {p0, v4}, Lcom/jscape/inet/a/a/e;->a(Ljava/math/BigInteger;)Z

    move-result v5
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v5, :cond_0

    :try_start_2
    new-instance v0, Lcom/jscape/inet/a/a/g;

    invoke-virtual {v4}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v3}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v2

    check-cast v2, Ljavax/crypto/interfaces/DHPrivateKey;

    invoke-interface {v2}, Ljavax/crypto/interfaces/DHPrivateKey;->getX()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/jscape/inet/a/a/g;-><init>([B[B)V

    return-object v0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/e;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    new-array v0, v0, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_3
    new-instance v0, Lcom/jscape/inet/a/a/d;

    sget-object v1, Lcom/jscape/inet/a/a/e;->e:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/inet/a/a/d;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/a/a/d;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/a/a/d;

    move-result-object v0

    throw v0
.end method

.method public a(Lcom/jscape/inet/a/a/g;[BI)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/a/d;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/g;->b()[Ljava/lang/String;

    :try_start_0
    new-instance v0, Ljava/math/BigInteger;

    iget-object p1, p1, Lcom/jscape/inet/a/a/g;->b:[B

    invoke-direct {v0, p1}, Ljava/math/BigInteger;-><init>([B)V

    new-instance p1, Ljava/math/BigInteger;

    invoke-direct {p1, p2}, Ljava/math/BigInteger;-><init>([B)V

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/a/a/e;->a(Ljava/math/BigInteger;Ljava/math/BigInteger;)[B

    move-result-object p1

    invoke-direct {p0, p1, p3}, Lcom/jscape/inet/a/a/e;->a([BI)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p2

    if-nez p2, :cond_0

    const/4 p2, 0x3

    new-array p2, p2, [Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/inet/a/a/g;->b([Ljava/lang/String;)V

    :cond_0
    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/a/a/d;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/a/a/d;

    move-result-object p1

    throw p1
.end method
