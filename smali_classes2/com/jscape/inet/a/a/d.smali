.class public Lcom/jscape/inet/a/a/d;
.super Ljava/io/IOException;


# static fields
.field private static final serialVersionUID:J = -0x2f9a805328f4b74dL


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/io/IOException;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private static a(Lcom/jscape/inet/a/a/d;)Lcom/jscape/inet/a/a/d;
    .locals 0

    return-object p0
.end method

.method public static a(Ljava/lang/Throwable;)Lcom/jscape/inet/a/a/d;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/a/d;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/a/a/g;->b()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    instance-of v0, p0, Lcom/jscape/inet/a/a/d;
    :try_end_0
    .catch Lcom/jscape/inet/a/a/d; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/jscape/inet/a/a/d;

    invoke-direct {v0, p0}, Lcom/jscape/inet/a/a/d;-><init>(Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/inet/a/a/d;->a(Lcom/jscape/inet/a/a/d;)Lcom/jscape/inet/a/a/d;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Lcom/jscape/inet/a/a/d; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/a/a/d;->a(Lcom/jscape/inet/a/a/d;)Lcom/jscape/inet/a/a/d;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    move-object v0, p0

    check-cast v0, Lcom/jscape/inet/a/a/d;

    :goto_1
    throw v0
.end method
