.class public interface abstract Lcom/jscape/inet/a/k;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/n/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/n/d<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract a(Ljava/lang/String;)Lcom/jscape/inet/a/a/b/ac;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation
.end method

.method public abstract a(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/a/l;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/a/l;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation
.end method

.method public abstract a()Z
.end method

.method public abstract b()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/String;)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation
.end method

.method public abstract c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation
.end method

.method public abstract c()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation
.end method

.method public abstract c(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation
.end method

.method public abstract d(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/jscape/inet/a/a/b/ac;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation
.end method

.method public abstract d()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation
.end method

.method public abstract e()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/a/c;
        }
    .end annotation
.end method

.method public abstract f()V
.end method
