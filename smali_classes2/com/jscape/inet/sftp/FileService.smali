.class public interface abstract Lcom/jscape/inet/sftp/FileService;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/n/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/n/d<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract accessTime(Ljava/lang/String;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract accessTimeOf(Ljava/lang/String;Z)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract appendFile(Ljava/io/InputStream;Ljava/lang/String;ZLcom/jscape/inet/sftp/FileService$TransferListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract appendFile(Ljava/io/InputStream;Ljava/lang/String;ZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract available()Z
.end method

.method public abstract cancelTransferOperation()V
.end method

.method public abstract createDirectory(Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract createSymbolicLink(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract createTime(Ljava/lang/String;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract createTimeOf(Ljava/lang/String;Z)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract exists(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public varargs abstract hashOf(Ljava/lang/String;JJI[Ljava/lang/String;)Lcom/jscape/inet/sftp/HashInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract infoAbout(Ljava/lang/String;Z)Lcom/jscape/inet/sftp/PathInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract listDirectory(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/jscape/inet/sftp/PathInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract modificationTime(Ljava/lang/String;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract modificationTimeOf(Ljava/lang/String;Z)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract permissions(Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract permissionsOf(Ljava/lang/String;Z)Lcom/jscape/util/e/PosixFilePermissions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract readFile(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/sftp/FileService$TransferListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract readFile(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract removeDirectory(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract removeFile(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract rename(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract sizeOf(Ljava/lang/String;Z)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract typeOf(Ljava/lang/String;Z)Lcom/jscape/util/e/UnixFileType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation
.end method
