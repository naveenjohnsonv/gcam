.class public Lcom/jscape/inet/sftp/SftpPermissionDeniedException;
.super Lcom/jscape/inet/sftp/RequestException;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private resource:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "@P]MO\u0002)aQ\u001cLQ\u0002ha[\u001c@QGhv]Y\\PG{pMSZQ\u0004l5\u0019\u0019\\\u0004I"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/sftp/SftpPermissionDeniedException;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x59

    goto :goto_1

    :cond_1
    const/16 v4, 0x37

    goto :goto_1

    :cond_2
    const/16 v4, 0x73

    goto :goto_1

    :cond_3
    const/16 v4, 0x7f

    goto :goto_1

    :cond_4
    const/16 v4, 0x6c

    goto :goto_1

    :cond_5
    const/16 v4, 0x6e

    goto :goto_1

    :cond_6
    const/16 v4, 0x45

    :goto_1
    const/16 v5, 0x50

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_PERMISSION_DENIED:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    iget v0, v0, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->code:I

    sget-object v1, Lcom/jscape/inet/sftp/SftpPermissionDeniedException;->a:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/jscape/inet/sftp/RequestException;-><init>(ILjava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/SftpPermissionDeniedException;->resource:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getResource()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpPermissionDeniedException;->resource:Ljava/lang/String;

    return-object v0
.end method
