.class public Lcom/jscape/inet/sftp/SftpException;
.super Ljava/io/IOException;


# static fields
.field private static final serialVersionUID:J = -0x1d1732a49af050feL


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/io/IOException;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    if-nez v0, :cond_1

    instance-of v0, p0, Lcom/jscape/inet/sftp/SftpException;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/jscape/inet/sftp/SftpException;

    invoke-direct {v0, p0}, Lcom/jscape/inet/sftp/SftpException;-><init>(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    :goto_0
    move-object v0, p0

    check-cast v0, Lcom/jscape/inet/sftp/SftpException;

    :goto_1
    return-object v0
.end method
