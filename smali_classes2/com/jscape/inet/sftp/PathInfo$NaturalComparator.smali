.class public Lcom/jscape/inet/sftp/PathInfo$NaturalComparator;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/jscape/inet/sftp/PathInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/jscape/inet/sftp/PathInfo;Lcom/jscape/inet/sftp/PathInfo;)I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    invoke-virtual {p1}, Lcom/jscape/inet/sftp/PathInfo;->directory()Z

    move-result v1

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/jscape/inet/sftp/PathInfo;->directory()Z

    move-result v1

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    invoke-virtual {p2}, Lcom/jscape/inet/sftp/PathInfo;->directory()Z

    move-result v1

    :cond_1
    if-nez v0, :cond_3

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/jscape/inet/sftp/PathInfo;->directory()Z

    move-result v1

    if-nez v0, :cond_3

    if-nez v1, :cond_2

    const/4 p1, 0x1

    return p1

    :cond_2
    iget-object p1, p1, Lcom/jscape/inet/sftp/PathInfo;->name:Ljava/lang/String;

    iget-object p2, p2, Lcom/jscape/inet/sftp/PathInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    :cond_3
    return v1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/jscape/inet/sftp/PathInfo;

    check-cast p2, Lcom/jscape/inet/sftp/PathInfo;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/PathInfo$NaturalComparator;->compare(Lcom/jscape/inet/sftp/PathInfo;Lcom/jscape/inet/sftp/PathInfo;)I

    move-result p1

    return p1
.end method
