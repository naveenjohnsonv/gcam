.class public Lcom/jscape/inet/sftp/Sftp$RemoteDirectory$PathElementsDirectory;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final pathElements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "@]\u00174\u0016*^}Y\r( \u0002RbY\u0000(<4B0G\u0013=\'.~|Y\u000e9=2H-"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/sftp/Sftp$RemoteDirectory$PathElementsDirectory;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x75

    goto :goto_1

    :cond_1
    const/16 v4, 0x8

    goto :goto_1

    :cond_2
    const/16 v4, 0x1d

    goto :goto_1

    :cond_3
    const/16 v4, 0x12

    goto :goto_1

    :cond_4
    const/16 v4, 0x2d

    goto :goto_1

    :cond_5
    const/16 v4, 0x72

    goto :goto_1

    :cond_6
    const/16 v4, 0x5e

    :goto_1
    const/16 v5, 0x4e

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$RemoteDirectory$PathElementsDirectory;->pathElements:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public applyTo(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$RemoteDirectory$PathElementsDirectory;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$RemoteDirectory$PathElementsDirectory;->pathElements:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/jscape/inet/sftp/Sftp;->setDir(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    :cond_1
    return-object p0
.end method

.method public bridge synthetic applyTo(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/Sftp$RemoteDirectory$PathElementsDirectory;->applyTo(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$RemoteDirectory$PathElementsDirectory;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/Sftp$RemoteDirectory$PathElementsDirectory;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$RemoteDirectory$PathElementsDirectory;->pathElements:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
