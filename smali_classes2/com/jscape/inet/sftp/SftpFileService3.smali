.class public Lcom/jscape/inet/sftp/SftpFileService3;
.super Lcom/jscape/util/n/c;

# interfaces
.implements Lcom/jscape/inet/sftp/FileService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/util/n/c<",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/jscape/inet/sftp/FileService;"
    }
.end annotation


# static fields
.field public static final DEFAULT_PIPELINE_WINDOW_SIZE:I = 0x8

.field private static final c:I = 0x3

.field private static final p:[Ljava/lang/String;


# instance fields
.field private final d:Lcom/jscape/util/k/a/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/k/a/v<",
            "Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;",
            ">;"
        }
    .end annotation
.end field

.field private final e:I

.field private final f:Z

.field private final g:I

.field private final h:Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;

.field private final i:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final j:Ljava/util/logging/Logger;

.field private k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

.field private l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

.field private m:Ljava/io/InputStream;

.field private n:Ljava/io/OutputStream;

.field private volatile o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "ms\u0014K{VZC=$bBo\u0014Ix\u0004WwXQ\u0004|\u0003\u0004M\u001aG\u0004!Z\u001a6\u001aGy\'W\u0001e\u0011\u001akm\u0012VwK]KsWJyK\u0014Mp\u0007HsRQJi\u0012@8\u001dqs\u0012\\fZWPx\u0013\u0004\u007fQWKp\u001eJq\u001fYAn\u0004EqZ\u000e\u0004\u0002\n3\u0016f|\u0013\u0004tJRBx\u0005\u0004eVNA=\u0001EzJQ\n)kh\u0003CyVZC=$bBo\u0014Ix\u0004WwXQ\u0004|\u0003\u0004M\u001aG\u0004!Z\u001a6\u001aGy\'W\u0001e\u0011\u001dqs\u0012\\fZWPx\u0013\u0004\u007fQWKp\u001eJq\u001fYAn\u0004EqZ\u000e\u0004\u0019f|\u0013\u0004fVDAq\u001eJs\u001fCMs\u0013Ka\u001fGMg\u0012\n\u0010km\u0012VwK]KsWAdM[V3"

    const/16 v4, 0xef

    const/16 v5, 0x29

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0xf

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x4a

    const/16 v3, 0x24

    const-string v5, "p\\$s1[\\Mt\u0015@eQ\\M:\u0013O~KVG Px4K\u0013\u001f7N\u00034Kn\r%p\\$s1[\\Mt\u0015@eQ\\M:\u0013QtYGF~J\u0003J\u001d@\u0003&]\u001d1\u001d@~4"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v17, v5

    move v5, v3

    move-object/from16 v3, v17

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x8

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/sftp/SftpFileService3;->p:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    const/16 v16, 0x2b

    if-eqz v15, :cond_8

    if-eq v15, v9, :cond_7

    const/4 v1, 0x2

    if-eq v15, v1, :cond_6

    const/4 v1, 0x3

    if-eq v15, v1, :cond_8

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v16, 0x3b

    goto :goto_4

    :cond_4
    const/16 v16, 0x30

    goto :goto_4

    :cond_5
    const/16 v16, 0x19

    goto :goto_4

    :cond_6
    const/16 v16, 0x78

    goto :goto_4

    :cond_7
    const/16 v16, 0x12

    :cond_8
    :goto_4
    xor-int v1, v8, v16

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/util/k/a/v;IZILjava/util/logging/Logger;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/v<",
            "Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;",
            ">;IZI",
            "Ljava/util/logging/Logger;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/util/n/c;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->d:Lcom/jscape/util/k/a/v;

    int-to-long v0, p2

    sget-object p1, Lcom/jscape/inet/sftp/SftpFileService3;->p:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v2, p1, v2

    const-wide/16 v3, 0x0

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p2, p0, Lcom/jscape/inet/sftp/SftpFileService3;->e:I

    iput-boolean p3, p0, Lcom/jscape/inet/sftp/SftpFileService3;->f:Z

    int-to-long p2, p4

    const/4 v0, 0x7

    aget-object p1, p1, v0

    invoke-static {p2, p3, v3, v4, p1}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p4, p0, Lcom/jscape/inet/sftp/SftpFileService3;->g:I

    new-instance p1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;

    const/4 p2, 0x0

    new-array p3, p2, [Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    invoke-direct {p1, p3}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;-><init>([Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;)V

    invoke-static {p1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/Messages3;->init(Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;)Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->h:Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/inet/sftp/SftpFileService3;->j:Ljava/util/logging/Logger;

    return-void
.end method

.method private a()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method static a(Lcom/jscape/inet/sftp/SftpFileService3;Lcom/jscape/inet/sftp/protocol/messages/Message;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/SftpFileService3;->b(Lcom/jscape/inet/sftp/protocol/messages/Message;)V

    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/sftp/SftpFileService3;->p:[Ljava/lang/String;

    const/16 v3, 0x8

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method private a([B)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpClose;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpClose;-><init>(I[B)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    throw p1
.end method

.method static a(Lcom/jscape/inet/sftp/SftpFileService3;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->o:Z

    return p0
.end method

.method private a(Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;)Z
    .locals 3

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;->filename:Ljava/lang/String;

    const-string v2, "."

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    sget-object v1, Lcom/jscape/inet/sftp/SftpFileService3;->p:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iget-object p1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;->filename:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    :cond_0
    if-eqz v0, :cond_2

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move p1, v1

    :goto_1
    return p1
.end method

.method private a(Lcom/jscape/inet/sftp/protocol/messages/Message;)[B
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpHandle;

    iget-object p1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpHandle;->handle:[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    throw p1
.end method

.method static b(Lcom/jscape/inet/sftp/SftpFileService3;)I
    .locals 0

    iget p0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->g:I

    return p0
.end method

.method private b(Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;)Lcom/jscape/inet/sftp/PathInfo;
    .locals 13

    new-instance v12, Lcom/jscape/inet/sftp/PathInfo;

    iget-object v1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;->filename:Ljava/lang/String;

    iget-object v2, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;->attributes:Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    invoke-virtual {v2}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->type()Lcom/jscape/util/e/UnixFileType;

    move-result-object v2

    iget-object v3, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;->attributes:Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    invoke-virtual {v3}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->size()J

    move-result-wide v3

    iget-object v5, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;->attributes:Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    iget-object v5, v5, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->userId:Ljava/lang/Integer;

    const/4 v6, 0x0

    if-eqz v5, :cond_0

    iget-object v5, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;->attributes:Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    iget-object v5, v5, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->userId:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    :cond_0
    move-object v5, v6

    :goto_0
    iget-object v7, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;->attributes:Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    iget-object v7, v7, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->groupId:Ljava/lang/Integer;

    if-eqz v7, :cond_1

    iget-object v6, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;->attributes:Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    iget-object v6, v6, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->groupId:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v6

    :cond_1
    iget-object v7, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;->attributes:Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    iget-object v7, v7, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    iget-object v8, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;->attributes:Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    invoke-virtual {v8}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->accessTime()J

    move-result-wide v8

    iget-object v0, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;->attributes:Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    invoke-virtual {v0}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->modificationTime()J

    move-result-wide v10

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/jscape/inet/sftp/PathInfo;-><init>(Ljava/lang/String;Lcom/jscape/util/e/UnixFileType;JLjava/lang/String;Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;JJ)V

    return-object v12
.end method

.method private b()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    new-instance v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpInit;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const/4 v3, 0x3

    invoke-direct {v1, v3, v2}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpInit;-><init>(ILjava/util/Map;)V

    invoke-direct {p0, v1}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpVersion;

    :try_start_0
    iget v2, v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpVersion;->version:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_0

    if-eqz v2, :cond_1

    :try_start_1
    iget v2, v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpVersion;->version:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    if-ne v2, v3, :cond_2

    :cond_1
    return-void

    :cond_2
    :try_start_2
    new-instance v0, Lcom/jscape/inet/sftp/FileService$UnsupportedVersionException;

    iget v1, v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpVersion;->version:I

    invoke-direct {v0, v3, v1}, Lcom/jscape/inet/sftp/FileService$UnsupportedVersionException;-><init>(II)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private b(Lcom/jscape/inet/sftp/protocol/messages/Message;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/SftpFileService3;->e(Lcom/jscape/inet/sftp/protocol/messages/Message;)V

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->h:Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->n:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, v1}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;->write(Lcom/jscape/inet/sftp/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method

.method private b([B)V
    .locals 2

    :try_start_0
    new-instance v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpClose;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpClose;-><init>(I[B)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method private c()Lcom/jscape/inet/sftp/protocol/messages/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/jscape/inet/sftp/protocol/messages/Message;",
            ">()TM;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->h:Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->m:Ljava/io/InputStream;

    invoke-virtual {v0, v1}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/messages/Message;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/SftpFileService3;->f(Lcom/jscape/inet/sftp/protocol/messages/Message;)V

    return-object v0
.end method

.method static c(Lcom/jscape/inet/sftp/SftpFileService3;)Lcom/jscape/inet/sftp/protocol/messages/Message;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->c()Lcom/jscape/inet/sftp/protocol/messages/Message;

    move-result-object p0

    return-object p0
.end method

.method private c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/jscape/inet/sftp/protocol/messages/Message;",
            ")TM;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/SftpFileService3;->b(Lcom/jscape/inet/sftp/protocol/messages/Message;)V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->c()Lcom/jscape/inet/sftp/protocol/messages/Message;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/SftpFileService3;->d(Lcom/jscape/inet/sftp/protocol/messages/Message;)V

    return-object p1
.end method

.method static d(Lcom/jscape/inet/sftp/SftpFileService3;)I
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result p0

    return p0
.end method

.method private d()V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/sftp/SftpFileService3;->p:[Ljava/lang/String;

    const/16 v3, 0xa

    aget-object v0, v0, v3

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jscape/inet/sftp/SftpFileService3;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/jscape/inet/sftp/SftpFileService3;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private d(Lcom/jscape/inet/sftp/protocol/messages/Message;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    instance-of v1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStatus;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_2

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    check-cast p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStatus;

    :try_start_2
    iget v1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStatus;->code:I

    sget-object v2, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_OK:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    iget v2, v2, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->code:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    if-eqz v0, :cond_1

    if-eq v1, v2, :cond_2

    :try_start_3
    iget v1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStatus;->code:I

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_EOF:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    iget v2, v0, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->code:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    :cond_1
    if-ne v1, v2, :cond_3

    :cond_2
    return-void

    :cond_3
    :try_start_4
    new-instance v0, Lcom/jscape/inet/sftp/FileService$ServerException;

    iget v1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStatus;->code:I

    iget-object v2, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStatus;->message:Ljava/lang/String;

    iget-object p1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStatus;->languageTag:Ljava/lang/String;

    invoke-direct {v0, v1, v2, p1}, Lcom/jscape/inet/sftp/FileService$ServerException;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :catch_3
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method static e(Lcom/jscape/inet/sftp/SftpFileService3;)I
    .locals 0

    iget p0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->e:I

    return p0
.end method

.method private e()V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/sftp/SftpFileService3;->p:[Ljava/lang/String;

    const/16 v3, 0x9

    aget-object v0, v0, v3

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jscape/inet/sftp/SftpFileService3;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/jscape/inet/sftp/SftpFileService3;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private e(Lcom/jscape/inet/sftp/protocol/messages/Message;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/sftp/SftpFileService3;->p:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v0, v0, v3

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jscape/inet/sftp/SftpFileService3;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/jscape/inet/sftp/SftpFileService3;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private f(Lcom/jscape/inet/sftp/protocol/messages/Message;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/sftp/SftpFileService3;->p:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/jscape/inet/sftp/SftpFileService3;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/jscape/inet/sftp/SftpFileService3;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v3, 0x2

    aput-object p1, v4, v3

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public accessTime(Ljava/lang/String;J)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    new-instance v9, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    const-wide/16 v0, 0x3e8

    div-long/2addr p2, v0

    long-to-int p2, p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v8

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/jscape/util/e/UnixFileType;Lcom/jscape/util/e/PosixFilePermissions;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/Map;)V

    invoke-virtual {p0, p1, v9}, Lcom/jscape/inet/sftp/SftpFileService3;->attributes(Ljava/lang/String;Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;)V

    return-void
.end method

.method public accessTimeOf(Ljava/lang/String;Z)J
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/SftpFileService3;->attributesOf(Ljava/lang/String;Z)Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->accessTime()J

    move-result-wide p1

    return-wide p1
.end method

.method protected actualStart()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->d:Lcom/jscape/util/k/a/v;

    invoke-interface {v0}, Lcom/jscape/util/k/a/v;->connect()Lcom/jscape/util/k/a/r;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    iput-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->openSessionChannel(Z)Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    sget-object v1, Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;->SFTP:Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;

    iget-object v1, v1, Lcom/jscape/inet/ssh/protocol/v2/connection/SubsystemType;->name:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/SftpFileService3;->f:Z

    invoke-virtual {v0, v1, v2}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->subsystem(Ljava/lang/String;Z)V

    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v1, Lcom/jscape/util/k/a/a;

    iget-object v2, p0, Lcom/jscape/inet/sftp/SftpFileService3;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-direct {v1, v2}, Lcom/jscape/util/k/a/a;-><init>(Lcom/jscape/util/k/a/C;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->m:Ljava/io/InputStream;

    new-instance v0, Ljava/io/BufferedOutputStream;

    new-instance v1, Lcom/jscape/util/k/a/e;

    iget-object v2, p0, Lcom/jscape/inet/sftp/SftpFileService3;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-direct {v1, v2}, Lcom/jscape/util/k/a/e;-><init>(Lcom/jscape/util/k/a/C;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->n:Ljava/io/OutputStream;

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->i:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->b()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->d()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->close()V

    throw v0
.end method

.method protected actualStop()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->close()V

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->close()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->e()V

    return-void
.end method

.method public appendFile(Ljava/io/InputStream;Ljava/lang/String;ZLcom/jscape/inet/sftp/FileService$TransferListener;)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    move-object/from16 v1, p0

    const/4 v0, 0x0

    iput-boolean v0, v1, Lcom/jscape/inet/sftp/SftpFileService3;->o:Z

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v2

    move-object/from16 v3, p2

    invoke-static {v2, v3}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;->appending(ILjava/lang/String;)Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Lcom/jscape/inet/sftp/protocol/messages/Message;)[B

    move-result-object v2

    :try_start_0
    iget v3, v1, Lcom/jscape/inet/sftp/SftpFileService3;->e:I

    new-array v11, v3, [B

    const-wide/16 v3, 0x0

    move-wide v12, v3

    move-wide v14, v12

    :goto_0
    iget-boolean v3, v1, Lcom/jscape/inet/sftp/SftpFileService3;->o:Z

    if-nez v3, :cond_1

    move-object/from16 v10, p1

    invoke-virtual {v10, v11}, Ljava/io/InputStream;->read([B)I

    move-result v9

    const/4 v3, -0x1

    if-eq v9, v3, :cond_1

    new-instance v8, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpWrite;

    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/16 v16, 0x0

    move-object v3, v8

    move-object v5, v2

    move-wide v6, v12

    move-object/from16 p2, v2

    move-object v2, v8

    move-object v8, v11

    move/from16 p3, v9

    move/from16 v9, v16

    move/from16 v10, p3

    :try_start_1
    invoke-direct/range {v3 .. v10}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpWrite;-><init>(I[BJ[BII)V

    invoke-direct {v1, v2}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;

    move/from16 v2, p3

    int-to-long v2, v2

    add-long/2addr v12, v2

    add-long/2addr v14, v2

    move-object/from16 v2, p4

    invoke-interface {v2, v14, v15}, Lcom/jscape/inet/sftp/FileService$TransferListener;->onTransferProgress(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    move-object/from16 v2, p2

    goto :goto_0

    :catch_0
    move-exception v0

    move-object/from16 v2, p2

    goto :goto_2

    :cond_1
    move-object/from16 p2, v2

    :goto_1
    move-object/from16 v2, p2

    :try_start_2
    invoke-direct {v1, v2}, Lcom/jscape/inet/sftp/SftpFileService3;->a([B)V

    return-void

    :catch_1
    move-exception v0

    :goto_2
    invoke-direct {v1, v2}, Lcom/jscape/inet/sftp/SftpFileService3;->b([B)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object v0

    throw v0
.end method

.method public appendFile(Ljava/io/InputStream;Ljava/lang/String;ZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    const/4 p3, 0x0

    iput-boolean p3, p0, Lcom/jscape/inet/sftp/SftpFileService3;->o:Z

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result p3

    invoke-static {p3, p2}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;->appending(ILjava/lang/String;)Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Lcom/jscape/inet/sftp/protocol/messages/Message;)[B

    move-result-object v3

    new-instance p2, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;

    const-wide/16 v4, 0x0

    move-object v0, p2

    move-object v1, p0

    move-object v2, p1

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;-><init>(Lcom/jscape/inet/sftp/SftpFileService3;Ljava/io/InputStream;[BJLcom/jscape/inet/sftp/FileService$TransferListener;)V

    :try_start_0
    invoke-interface {p5, p2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    invoke-virtual {p2}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->execute()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    throw p1
.end method

.method public attributes(Ljava/lang/String;Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpSetStat;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v1

    invoke-direct {v0, v1, p1, p2}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpSetStat;-><init>(ILjava/lang/String;Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    throw p1
.end method

.method public attributesOf(Ljava/lang/String;Z)Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    if-eqz p2, :cond_0

    :try_start_0
    new-instance p2, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStat;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v0

    invoke-direct {p2, v0, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStat;-><init>(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    new-instance p2, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpLstat;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v0

    invoke-direct {p2, v0, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpLstat;-><init>(ILjava/lang/String;)V

    :goto_0
    invoke-direct {p0, p2}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpAttrs;

    iget-object p1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpAttrs;->attributes:Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    throw p1
.end method

.method public available()Z
    .locals 5

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;
    :try_end_0
    .catch Lcom/jscape/inet/sftp/FileService$ServerException; {:try_start_0 .. :try_end_0} :catch_4

    const/4 v2, 0x0

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;
    :try_end_1
    .catch Lcom/jscape/inet/sftp/FileService$ServerException; {:try_start_1 .. :try_end_1} :catch_5

    :cond_0
    :try_start_2
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->closed()Z

    move-result v1
    :try_end_2
    .catch Lcom/jscape/inet/sftp/FileService$ServerException; {:try_start_2 .. :try_end_2} :catch_2

    const/4 v3, 0x1

    if-nez v0, :cond_3

    if-eqz v1, :cond_2

    :cond_1
    return v2

    :cond_2
    :try_start_3
    new-instance v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRealPath;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v1

    const-string v4, "."

    invoke-direct {v0, v1, v4}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRealPath;-><init>(ILjava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;
    :try_end_3
    .catch Lcom/jscape/inet/sftp/FileService$ServerException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move v1, v3

    goto :goto_0

    :catch_0
    return v2

    :catch_1
    return v3

    :cond_3
    :goto_0
    return v1

    :catch_2
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Lcom/jscape/inet/sftp/FileService$ServerException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_4
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Lcom/jscape/inet/sftp/FileService$ServerException; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public cancelTransferOperation()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->o:Z

    return-void
.end method

.method public createDirectory(Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpMkdir;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v1

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    sget-object v3, Lcom/jscape/util/e/UnixFileType;->DIRECTORY:Lcom/jscape/util/e/UnixFileType;

    const/4 v4, 0x0

    invoke-direct {v2, v4, v3, p2, v4}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;-><init>(Ljava/lang/Long;Lcom/jscape/util/e/UnixFileType;Lcom/jscape/util/e/PosixFilePermissions;Ljava/lang/Integer;)V

    invoke-direct {v0, v1, p1, v2}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpMkdir;-><init>(ILjava/lang/String;Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    throw p1
.end method

.method public createSymbolicLink(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpSymLink;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v1

    invoke-direct {v0, v1, p1, p2}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpSymLink;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    throw p1
.end method

.method public createTime(Ljava/lang/String;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    new-instance p1, Lcom/jscape/inet/sftp/FileService$OperationException;

    sget-object p2, Lcom/jscape/inet/sftp/SftpFileService3;->p:[Ljava/lang/String;

    const/4 p3, 0x1

    aget-object p2, p2, p3

    invoke-direct {p1, p2}, Lcom/jscape/inet/sftp/FileService$OperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public createTimeOf(Ljava/lang/String;Z)J
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    const-wide/16 p1, 0x0

    return-wide p1
.end method

.method public exists(Ljava/lang/String;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    new-instance v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStat;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v2

    invoke-direct {v1, v2, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStat;-><init>(ILjava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/jscape/inet/sftp/FileService$ServerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    if-nez v0, :cond_1

    :try_start_1
    iget v0, p1, Lcom/jscape/inet/sftp/FileService$ServerException;->code:I

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_NO_SUCH_FILE:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    iget v1, v1, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->code:I
    :try_end_1
    .catch Lcom/jscape/inet/sftp/FileService$ServerException; {:try_start_1 .. :try_end_1} :catch_2

    if-ne v0, v1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    goto :goto_0

    :catch_2
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/sftp/FileService$ServerException; {:try_start_2 .. :try_end_2} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    throw p1
.end method

.method public varargs hashOf(Ljava/lang/String;JJI[Ljava/lang/String;)Lcom/jscape/inet/sftp/HashInfo;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    :try_start_0
    invoke-static/range {p7 .. p7}, Lcom/jscape/util/G;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    new-instance v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v2

    move-object v1, v0

    move-object v3, p1

    move-wide v5, p2

    move-wide v7, p4

    move/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;-><init>(ILjava/lang/String;Ljava/util/List;JJI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-object v1, p0

    :try_start_1
    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCheckFileNameCodec;

    invoke-direct {v2}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCheckFileNameCodec;-><init>()V

    new-instance v3, Lcom/jscape/util/h/e;

    iget-object v4, v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;->data:[B

    invoke-direct {v3, v4}, Lcom/jscape/util/h/e;-><init>([B)V

    iget v0, v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;->id:I

    invoke-virtual {v2, v3, v0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCheckFileNameCodec;->read(Ljava/io/InputStream;I)Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyCheckFileName;

    new-instance v2, Lcom/jscape/inet/sftp/HashInfo;

    iget-object v3, v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyCheckFileName;->hashAlgorithmUsed:Ljava/lang/String;

    iget-object v0, v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyCheckFileName;->data:[B

    invoke-direct {v2, v3, v0}, Lcom/jscape/inet/sftp/HashInfo;-><init>(Ljava/lang/String;[B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    return-object v2

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v1, p0

    :goto_0
    invoke-static {v0}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object v0

    throw v0
.end method

.method public infoAbout(Ljava/lang/String;Z)Lcom/jscape/inet/sftp/PathInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    if-eqz p2, :cond_0

    :try_start_0
    new-instance p2, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReadLink;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v0

    invoke-direct {p2, v0, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReadLink;-><init>(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    new-instance p2, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRealPath;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v0

    invoke-direct {p2, v0, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRealPath;-><init>(ILjava/lang/String;)V

    :goto_0
    invoke-direct {p0, p2}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpName;

    iget-object p1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpName;->entries:[Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;

    const/4 p2, 0x0

    aget-object p1, p1, p2

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/SftpFileService3;->b(Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;)Lcom/jscape/inet/sftp/PathInfo;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    throw p1
.end method

.method public listDirectory(Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/jscape/inet/sftp/PathInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->o:Z

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v1

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpendir;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v3

    invoke-direct {v2, v3, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpendir;-><init>(ILjava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Lcom/jscape/inet/sftp/protocol/messages/Message;)[B

    move-result-object p1

    :try_start_0
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    :cond_0
    :try_start_1
    iget-boolean v3, p0, Lcom/jscape/inet/sftp/SftpFileService3;->o:Z

    :goto_0
    if-nez v3, :cond_7

    if-nez v1, :cond_7

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v4

    invoke-direct {v3, v4, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir;-><init>(I[B)V

    invoke-direct {p0, v3}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/sftp/protocol/messages/Message;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    if-nez v1, :cond_1

    :try_start_2
    instance-of v4, v3, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpName;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    if-eqz v4, :cond_7

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_1
    check-cast v3, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpName;

    iget-object v3, v3, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpName;->entries:[Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;

    array-length v4, v3

    move v5, v0

    :cond_2
    if-ge v5, v4, :cond_6

    aget-object v6, v3, v5
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    if-nez v1, :cond_5

    :try_start_4
    invoke-direct {p0, v6}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;)Z

    move-result v7
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    if-nez v1, :cond_4

    if-eqz v7, :cond_3

    :try_start_5
    invoke-direct {p0, v6}, Lcom/jscape/inet/sftp/SftpFileService3;->b(Lcom/jscape/inet/sftp/protocol/v3/messages/NameEntry;)Lcom/jscape/inet/sftp/PathInfo;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_4
    move v3, v7

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    :cond_5
    :goto_2
    if-eqz v1, :cond_2

    :cond_6
    if-eqz v1, :cond_0

    :cond_7
    :try_start_6
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/SftpFileService3;->a([B)V

    return-object v2

    :catch_2
    move-exception v0

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/SftpFileService3;->b([B)V

    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    throw p1
.end method

.method public modificationTime(Ljava/lang/String;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    invoke-static {p2, p3}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->attributesFor(J)Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/SftpFileService3;->attributes(Ljava/lang/String;Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;)V

    return-void
.end method

.method public modificationTimeOf(Ljava/lang/String;Z)J
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/SftpFileService3;->attributesOf(Ljava/lang/String;Z)Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->modificationTime()J

    move-result-wide p1

    return-wide p1
.end method

.method public permissions(Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    invoke-static {p2}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->attributesFor(Lcom/jscape/util/e/PosixFilePermissions;)Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/SftpFileService3;->attributes(Ljava/lang/String;Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;)V

    return-void
.end method

.method public permissionsOf(Ljava/lang/String;Z)Lcom/jscape/util/e/PosixFilePermissions;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/SftpFileService3;->attributesOf(Ljava/lang/String;Z)Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    move-result-object p1

    iget-object p1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    return-object p1
.end method

.method public readFile(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/sftp/FileService$TransferListener;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    const/4 p5, 0x0

    iput-boolean p5, p0, Lcom/jscape/inet/sftp/SftpFileService3;->o:Z

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result p5

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v0

    invoke-static {v0, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;->reading(ILjava/lang/String;)Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Lcom/jscape/inet/sftp/protocol/messages/Message;)[B

    move-result-object p1

    const-wide/16 v0, 0x0

    move-wide v6, v0

    :cond_0
    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->o:Z

    if-nez v0, :cond_2

    if-nez p5, :cond_2

    new-instance v8, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v1

    iget v5, p0, Lcom/jscape/inet/sftp/SftpFileService3;->e:I

    move-object v0, v8

    move-object v2, p1

    move-wide v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;-><init>(I[BJI)V

    invoke-direct {p0, v8}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/sftp/protocol/messages/Message;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez p5, :cond_1

    :try_start_1
    instance-of v1, v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpData;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v1, :cond_2

    goto :goto_0

    :catch_0
    move-exception p2

    :try_start_2
    invoke-static {p2}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p2

    throw p2

    :cond_1
    :goto_0
    check-cast v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpData;

    iget-object v1, v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpData;->data:[B

    invoke-virtual {p4, v1}, Ljava/io/OutputStream;->write([B)V

    iget-object v1, v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpData;->data:[B

    array-length v1, v1

    int-to-long v1, v1

    add-long/2addr p2, v1

    iget-object v0, v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpData;->data:[B

    array-length v0, v0

    int-to-long v0, v0

    add-long/2addr v6, v0

    invoke-interface {p6, v6, v7}, Lcom/jscape/inet/sftp/FileService$TransferListener;->onTransferProgress(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz p5, :cond_0

    :cond_2
    :try_start_3
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/SftpFileService3;->a([B)V

    return-void

    :catch_1
    move-exception p2

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/SftpFileService3;->b([B)V

    throw p2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    throw p1
.end method

.method public readFile(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    const/4 p5, 0x0

    iput-boolean p5, p0, Lcom/jscape/inet/sftp/SftpFileService3;->o:Z

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result p5

    invoke-static {p5, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;->reading(ILjava/lang/String;)Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Lcom/jscape/inet/sftp/protocol/messages/Message;)[B

    move-result-object v2

    new-instance p1, Lcom/jscape/inet/sftp/SftpFileService3$ReadOperation;

    move-object v0, p1

    move-object v1, p0

    move-wide v3, p2

    move-object v5, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/sftp/SftpFileService3$ReadOperation;-><init>(Lcom/jscape/inet/sftp/SftpFileService3;[BJLjava/io/OutputStream;Lcom/jscape/inet/sftp/FileService$TransferListener;)V

    :try_start_0
    invoke-interface {p7, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    invoke-virtual {p1}, Lcom/jscape/inet/sftp/SftpFileService3$ReadOperation;->execute()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    throw p1
.end method

.method public removeDirectory(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRmdir;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRmdir;-><init>(ILjava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    throw p1
.end method

.method public removeFile(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRemove;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v1

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRemove;-><init>(ILjava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    throw p1
.end method

.method public rename(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRename;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v1

    invoke-direct {v0, v1, p1, p2}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRename;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    throw p1
.end method

.method public sessionConnection()Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    return-object v0
.end method

.method public sizeOf(Ljava/lang/String;Z)J
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/SftpFileService3;->attributesOf(Ljava/lang/String;Z)Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->size()J

    move-result-wide p1

    return-wide p1
.end method

.method public typeOf(Ljava/lang/String;Z)Lcom/jscape/util/e/UnixFileType;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/SftpFileService3;->attributesOf(Ljava/lang/String;Z)Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->type()Lcom/jscape/util/e/UnixFileType;

    move-result-object p1

    return-object p1
.end method

.method public writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    move-object/from16 v1, p0

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, v1, Lcom/jscape/inet/sftp/SftpFileService3;->o:Z

    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    const-wide/16 v4, 0x0

    cmp-long v6, p3, v4

    if-eqz v0, :cond_0

    if-nez v6, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    move v2, v6

    :cond_1
    :goto_0
    move-object/from16 v6, p2

    invoke-static {v3, v6, v2}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;->writing(ILjava/lang/String;Z)Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Lcom/jscape/inet/sftp/protocol/messages/Message;)[B

    move-result-object v2

    :try_start_1
    iget v3, v1, Lcom/jscape/inet/sftp/SftpFileService3;->e:I

    new-array v3, v3, [B

    move-wide v14, v4

    move-wide/from16 v4, p3

    :goto_1
    iget-boolean v6, v1, Lcom/jscape/inet/sftp/SftpFileService3;->o:Z

    if-nez v6, :cond_3

    move-object/from16 v13, p1

    invoke-virtual {v13, v3}, Ljava/io/InputStream;->read([B)I

    move-result v12

    const/4 v6, -0x1

    if-eq v12, v6, :cond_3

    new-instance v11, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpWrite;

    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v7
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    const/16 v16, 0x0

    move-object v6, v11

    move-object v8, v2

    move-wide v9, v4

    move-object/from16 p2, v2

    move-object v2, v11

    move-object v11, v3

    move/from16 p3, v12

    move/from16 v12, v16

    move/from16 v13, p3

    :try_start_2
    invoke-direct/range {v6 .. v13}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpWrite;-><init>(I[BJ[BII)V

    invoke-direct {v1, v2}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/protocol/messages/Message;)Ljava/lang/Object;

    move/from16 v2, p3

    int-to-long v6, v2

    add-long/2addr v4, v6

    add-long/2addr v14, v6

    move-object/from16 v2, p6

    invoke-interface {v2, v14, v15}, Lcom/jscape/inet/sftp/FileService$TransferListener;->onTransferProgress(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    move-object/from16 v2, p2

    goto :goto_1

    :catch_0
    move-exception v0

    move-object/from16 v2, p2

    goto :goto_3

    :cond_3
    move-object/from16 p2, v2

    :goto_2
    move-object/from16 v2, p2

    :try_start_3
    invoke-direct {v1, v2}, Lcom/jscape/inet/sftp/SftpFileService3;->a([B)V

    return-void

    :catch_1
    move-exception v0

    :goto_3
    invoke-direct {v1, v2}, Lcom/jscape/inet/sftp/SftpFileService3;->b([B)V

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object v0

    throw v0

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/FileService$OperationException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result p5

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/jscape/inet/sftp/SftpFileService3;->o:Z

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3;->a()I

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const-wide/16 v2, 0x0

    cmp-long v2, p3, v2

    if-nez p5, :cond_0

    if-nez v2, :cond_1

    const/4 p5, 0x1

    move v0, p5

    goto :goto_0

    :cond_0
    move v0, v2

    :cond_1
    :goto_0
    invoke-static {v1, p2, v0}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;->writing(ILjava/lang/String;Z)Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Lcom/jscape/inet/sftp/protocol/messages/Message;)[B

    move-result-object v3

    new-instance p2, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;

    move-object v0, p2

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p3

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;-><init>(Lcom/jscape/inet/sftp/SftpFileService3;Ljava/io/InputStream;[BJLcom/jscape/inet/sftp/FileService$TransferListener;)V

    :try_start_1
    invoke-interface {p7, p2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    invoke-virtual {p2}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->execute()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method
