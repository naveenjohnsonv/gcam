.class public Lcom/jscape/inet/sftp/Sftp$DirectoryService;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "~\u0012R\u007fIvdH\u0006\u0017lAdeC\u0003Zy\u0000y~\r\u000cXh\u0000q-I\u000bEyCdb_\u001b\u0019"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/sftp/Sftp$DirectoryService;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x4a

    goto :goto_1

    :cond_1
    const/16 v4, 0x57

    goto :goto_1

    :cond_2
    const/16 v4, 0x67

    goto :goto_1

    :cond_3
    const/16 v4, 0x5b

    goto :goto_1

    :cond_4
    const/16 v4, 0x70

    goto :goto_1

    :cond_5
    const/16 v4, 0x25

    goto :goto_1

    :cond_6
    const/16 v4, 0x6a

    :goto_1
    const/16 v5, 0x47

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/sftp/Sftp;",
            ")",
            "Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries<",
            "Lcom/jscape/inet/sftp/SftpFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v2

    invoke-virtual {p0}, Lcom/jscape/inet/sftp/Sftp;->getDirListing()Ljava/util/Enumeration;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/sftp/SftpFile;

    :try_start_0
    invoke-virtual {v3}, Lcom/jscape/inet/sftp/SftpFile;->isDirectory()Z

    move-result v4
    :try_end_0
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v2, :cond_2

    if-eqz v4, :cond_1

    :try_start_1
    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_1 .. :try_end_1} :catch_2

    if-eqz v2, :cond_2

    :cond_1
    :try_start_2
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    goto :goto_1

    :cond_2
    :goto_0
    if-eqz v2, :cond_0

    goto :goto_2

    :catch_1
    move-exception p0

    :try_start_3
    invoke-static {p0}, Lcom/jscape/inet/sftp/Sftp$DirectoryService;->a(Lcom/jscape/inet/sftp/SftpException;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p0

    throw p0
    :try_end_3
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p0

    :try_start_4
    invoke-static {p0}, Lcom/jscape/inet/sftp/Sftp$DirectoryService;->a(Lcom/jscape/inet/sftp/SftpException;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p0

    throw p0
    :try_end_4
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_4 .. :try_end_4} :catch_0

    :goto_1
    invoke-static {p0}, Lcom/jscape/inet/sftp/Sftp$DirectoryService;->a(Lcom/jscape/inet/sftp/SftpException;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p0

    throw p0

    :cond_3
    :goto_2
    new-instance p0, Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;-><init>(Ljava/util/Collection;Ljava/util/Collection;Lcom/jscape/inet/sftp/Sftp$1;)V

    return-object p0
.end method

.method private static a(Ljava/io/File;)Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v2

    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p0

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    new-array p0, v3, [Ljava/io/File;

    :cond_1
    :goto_0
    array-length v4, p0

    :cond_2
    if-ge v3, v4, :cond_6

    aget-object v5, p0, v3

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v2, :cond_4

    if-eqz v6, :cond_3

    invoke-interface {v0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    if-nez v2, :cond_5

    :cond_3
    invoke-virtual {v5}, Ljava/io/File;->isFile()Z

    move-result v6

    :cond_4
    if-eqz v2, :cond_5

    if-eqz v6, :cond_5

    invoke-interface {v1, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v3, v3, 0x1

    if-nez v2, :cond_2

    :cond_6
    new-instance p0, Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;-><init>(Ljava/util/Collection;Ljava/util/Collection;Lcom/jscape/inet/sftp/Sftp$1;)V

    return-object p0
.end method

.method private static a(Lcom/jscape/inet/sftp/SftpException;)Lcom/jscape/inet/sftp/SftpException;
    .locals 0

    return-object p0
.end method

.method private static a(Ljava/util/Collection;Lcom/jscape/util/b/s;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/io/File;",
            ">;",
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory<",
            "Ljava/io/File;",
            ">;>;)V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-static {v1}, Lcom/jscape/inet/sftp/Sftp$DirectoryService;->a(Ljava/io/File;)Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;

    move-result-object v2

    new-instance v3, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;

    iget-object v4, v2, Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;->files:Ljava/util/Collection;

    invoke-direct {v3, v1, v4}, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;-><init>(Ljava/lang/Object;Ljava/util/Collection;)V

    invoke-virtual {p1, v3}, Lcom/jscape/util/b/s;->d(Ljava/lang/Object;)Lcom/jscape/util/b/s;

    move-result-object v1

    iget-object v2, v2, Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;->directories:Ljava/util/Collection;

    invoke-static {v2, v1}, Lcom/jscape/inet/sftp/Sftp$DirectoryService;->a(Ljava/util/Collection;Lcom/jscape/util/b/s;)V

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method private static a(Ljava/util/Collection;Lcom/jscape/util/b/s;Lcom/jscape/inet/sftp/Sftp;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/jscape/inet/sftp/SftpFile;",
            ">;",
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory<",
            "Lcom/jscape/inet/sftp/SftpFile;",
            ">;>;",
            "Lcom/jscape/inet/sftp/Sftp;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/sftp/SftpFile;

    invoke-virtual {v1}, Lcom/jscape/inet/sftp/SftpFile;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/jscape/inet/sftp/Sftp;->setDir(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/jscape/inet/sftp/Sftp$DirectoryService;->a(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;

    move-result-object v2

    new-instance v3, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;

    iget-object v4, v2, Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;->files:Ljava/util/Collection;

    invoke-direct {v3, v1, v4}, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;-><init>(Ljava/lang/Object;Ljava/util/Collection;)V

    invoke-virtual {p1, v3}, Lcom/jscape/util/b/s;->d(Ljava/lang/Object;)Lcom/jscape/util/b/s;

    move-result-object v1

    iget-object v2, v2, Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;->directories:Ljava/util/Collection;

    invoke-static {v2, v1, p2}, Lcom/jscape/inet/sftp/Sftp$DirectoryService;->a(Ljava/util/Collection;Lcom/jscape/util/b/s;Lcom/jscape/inet/sftp/Sftp;)V

    invoke-virtual {p2}, Lcom/jscape/inet/sftp/Sftp;->setDirUp()V

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method public static asTree(Ljava/io/File;)Lcom/jscape/util/b/s;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/Sftp$DirectoryService;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    invoke-static {p0}, Lcom/jscape/inet/sftp/Sftp$DirectoryService;->a(Ljava/io/File;)Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;

    iget-object v2, v0, Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;->files:Ljava/util/Collection;

    invoke-direct {v1, p0, v2}, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;-><init>(Ljava/lang/Object;Ljava/util/Collection;)V

    invoke-static {v1}, Lcom/jscape/util/b/s;->a(Ljava/lang/Object;)Lcom/jscape/util/b/s;

    move-result-object p0

    iget-object v0, v0, Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;->directories:Ljava/util/Collection;

    invoke-static {v0, p0}, Lcom/jscape/inet/sftp/Sftp$DirectoryService;->a(Ljava/util/Collection;Lcom/jscape/util/b/s;)V

    return-object p0
.end method

.method public static asTree(Ljava/lang/String;Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/util/b/s;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/jscape/inet/sftp/Sftp;",
            ")",
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory<",
            "Lcom/jscape/inet/sftp/SftpFile;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    move-object/from16 v0, p1

    invoke-virtual/range {p1 .. p1}, Lcom/jscape/inet/sftp/Sftp;->getDir()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v2, p0

    invoke-virtual {v0, v2}, Lcom/jscape/inet/sftp/Sftp;->setDir(Ljava/lang/String;)V

    invoke-static/range {p1 .. p1}, Lcom/jscape/inet/sftp/Sftp$DirectoryService;->a(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;

    move-result-object v3

    new-instance v14, Lcom/jscape/inet/sftp/SftpFile;

    invoke-static/range {p0 .. p0}, Lcom/jscape/inet/sftp/PathTools;->nameOf(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/jscape/util/e/UnixFileType;->DIRECTORY:Lcom/jscape/util/e/UnixFileType;

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->ownerDirectoryPermissions()Lcom/jscape/util/e/PosixFilePermissions;

    move-result-object v11

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v15

    const-wide/16 v7, 0x0

    const-string v9, ""

    const-string v10, ""

    const-wide/16 v12, 0x0

    move-object v4, v14

    move-object v2, v14

    move-wide v14, v15

    invoke-direct/range {v4 .. v15}, Lcom/jscape/inet/sftp/SftpFile;-><init>(Ljava/lang/String;Lcom/jscape/util/e/UnixFileType;JLjava/lang/String;Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;JJ)V

    new-instance v4, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;

    iget-object v5, v3, Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;->files:Ljava/util/Collection;

    invoke-direct {v4, v2, v5}, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;-><init>(Ljava/lang/Object;Ljava/util/Collection;)V

    invoke-static {v4}, Lcom/jscape/util/b/s;->a(Ljava/lang/Object;)Lcom/jscape/util/b/s;

    move-result-object v2

    iget-object v3, v3, Lcom/jscape/inet/sftp/Sftp$DirectoryService$DirectoryEntries;->directories:Ljava/util/Collection;

    invoke-static {v3, v2, v0}, Lcom/jscape/inet/sftp/Sftp$DirectoryService;->a(Ljava/util/Collection;Lcom/jscape/util/b/s;Lcom/jscape/inet/sftp/Sftp;)V

    invoke-virtual {v0, v1}, Lcom/jscape/inet/sftp/Sftp;->setDir(Ljava/lang/String;)V

    return-object v2
.end method
