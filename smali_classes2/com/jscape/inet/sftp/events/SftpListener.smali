.class public interface abstract Lcom/jscape/inet/sftp/events/SftpListener;
.super Ljava/lang/Object;


# virtual methods
.method public abstract changeDir(Lcom/jscape/inet/sftp/events/SftpChangeDirEvent;)V
.end method

.method public abstract connected(Lcom/jscape/inet/sftp/events/SftpConnectedEvent;)V
.end method

.method public abstract createDir(Lcom/jscape/inet/sftp/events/SftpCreateDirEvent;)V
.end method

.method public abstract deleteDir(Lcom/jscape/inet/sftp/events/SftpDeleteDirEvent;)V
.end method

.method public abstract deleteFile(Lcom/jscape/inet/sftp/events/SftpDeleteFileEvent;)V
.end method

.method public abstract dirListing(Lcom/jscape/inet/sftp/events/SftpListingEvent;)V
.end method

.method public abstract disconnected(Lcom/jscape/inet/sftp/events/SftpDisconnectedEvent;)V
.end method

.method public abstract download(Lcom/jscape/inet/sftp/events/SftpDownloadEvent;)V
.end method

.method public abstract progress(Lcom/jscape/inet/sftp/events/SftpProgressEvent;)V
.end method

.method public abstract renameFile(Lcom/jscape/inet/sftp/events/SftpRenameFileEvent;)V
.end method

.method public abstract upload(Lcom/jscape/inet/sftp/events/SftpUploadEvent;)V
.end method
