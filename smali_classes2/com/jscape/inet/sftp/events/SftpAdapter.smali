.class public Lcom/jscape/inet/sftp/events/SftpAdapter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/sftp/events/SftpListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public changeDir(Lcom/jscape/inet/sftp/events/SftpChangeDirEvent;)V
    .locals 0

    return-void
.end method

.method public connected(Lcom/jscape/inet/sftp/events/SftpConnectedEvent;)V
    .locals 0

    return-void
.end method

.method public createDir(Lcom/jscape/inet/sftp/events/SftpCreateDirEvent;)V
    .locals 0

    return-void
.end method

.method public deleteDir(Lcom/jscape/inet/sftp/events/SftpDeleteDirEvent;)V
    .locals 0

    return-void
.end method

.method public deleteFile(Lcom/jscape/inet/sftp/events/SftpDeleteFileEvent;)V
    .locals 0

    return-void
.end method

.method public dirListing(Lcom/jscape/inet/sftp/events/SftpListingEvent;)V
    .locals 0

    return-void
.end method

.method public disconnected(Lcom/jscape/inet/sftp/events/SftpDisconnectedEvent;)V
    .locals 0

    return-void
.end method

.method public download(Lcom/jscape/inet/sftp/events/SftpDownloadEvent;)V
    .locals 0

    return-void
.end method

.method public progress(Lcom/jscape/inet/sftp/events/SftpProgressEvent;)V
    .locals 0

    return-void
.end method

.method public renameFile(Lcom/jscape/inet/sftp/events/SftpRenameFileEvent;)V
    .locals 0

    return-void
.end method

.method public upload(Lcom/jscape/inet/sftp/events/SftpUploadEvent;)V
    .locals 0

    return-void
.end method
