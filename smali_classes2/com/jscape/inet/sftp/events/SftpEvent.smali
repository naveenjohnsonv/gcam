.class public abstract Lcom/jscape/inet/sftp/events/SftpEvent;
.super Ljava/lang/Object;


# static fields
.field private static b:Z


# instance fields
.field protected final source:Lcom/jscape/inet/sftp/Sftp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/events/SftpEvent;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/jscape/inet/sftp/events/SftpEvent;->b(Z)V

    :cond_0
    return-void
.end method

.method protected constructor <init>(Lcom/jscape/inet/sftp/Sftp;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/events/SftpEvent;->source:Lcom/jscape/inet/sftp/Sftp;

    return-void
.end method

.method public static b(Z)V
    .locals 0

    sput-boolean p0, Lcom/jscape/inet/sftp/events/SftpEvent;->b:Z

    return-void
.end method

.method public static b()Z
    .locals 1

    sget-boolean v0, Lcom/jscape/inet/sftp/events/SftpEvent;->b:Z

    return v0
.end method

.method public static c()Z
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/events/SftpEvent;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public abstract accept(Lcom/jscape/inet/sftp/events/SftpListener;)V
.end method

.method public getSource()Lcom/jscape/inet/sftp/Sftp;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/events/SftpEvent;->source:Lcom/jscape/inet/sftp/Sftp;

    return-object v0
.end method
