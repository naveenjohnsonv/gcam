.class public Lcom/jscape/inet/sftp/events/SftpDownloadEvent;
.super Lcom/jscape/inet/sftp/events/SftpTransferCompleteEvent;


# direct methods
.method public constructor <init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 0

    invoke-direct/range {p0 .. p8}, Lcom/jscape/inet/sftp/events/SftpTransferCompleteEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/inet/sftp/events/SftpListener;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/jscape/inet/sftp/events/SftpListener;->download(Lcom/jscape/inet/sftp/events/SftpDownloadEvent;)V

    return-void
.end method
