.class public Lcom/jscape/inet/sftp/events/SftpListingEvent;
.super Lcom/jscape/inet/sftp/events/SftpEvent;


# instance fields
.field private final a:Ljava/util/Enumeration;


# direct methods
.method public constructor <init>(Lcom/jscape/inet/sftp/Sftp;Ljava/util/Enumeration;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/events/SftpEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;)V

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/events/SftpListingEvent;->a:Ljava/util/Enumeration;

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/inet/sftp/events/SftpListener;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/jscape/inet/sftp/events/SftpListener;->dirListing(Lcom/jscape/inet/sftp/events/SftpListingEvent;)V

    return-void
.end method

.method public getFiles()Ljava/util/Enumeration;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/events/SftpListingEvent;->a:Ljava/util/Enumeration;

    return-object v0
.end method
