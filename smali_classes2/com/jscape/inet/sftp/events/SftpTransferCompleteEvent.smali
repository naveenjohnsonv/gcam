.class public abstract Lcom/jscape/inet/sftp/events/SftpTransferCompleteEvent;
.super Lcom/jscape/inet/sftp/events/SftpEvent;


# instance fields
.field private final a:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:J

.field private final f:J


# direct methods
.method protected constructor <init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/events/SftpEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;)V

    invoke-static {}, Lcom/jscape/inet/sftp/events/SftpEvent;->b()Z

    move-result p1

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/events/SftpTransferCompleteEvent;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/sftp/events/SftpTransferCompleteEvent;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/inet/sftp/events/SftpTransferCompleteEvent;->d:Ljava/lang/String;

    iput-wide p5, p0, Lcom/jscape/inet/sftp/events/SftpTransferCompleteEvent;->e:J

    iput-wide p7, p0, Lcom/jscape/inet/sftp/events/SftpTransferCompleteEvent;->f:J

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p2

    if-nez p2, :cond_0

    xor-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/jscape/inet/sftp/events/SftpEvent;->b(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/events/SftpTransferCompleteEvent;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getFilesize()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/sftp/events/SftpTransferCompleteEvent;->e:J

    return-wide v0
.end method

.method public getLocalPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/events/SftpTransferCompleteEvent;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/events/SftpTransferCompleteEvent;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/sftp/events/SftpTransferCompleteEvent;->f:J

    return-wide v0
.end method
