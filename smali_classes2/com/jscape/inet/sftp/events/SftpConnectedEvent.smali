.class public Lcom/jscape/inet/sftp/events/SftpConnectedEvent;
.super Lcom/jscape/inet/sftp/events/SftpEvent;


# instance fields
.field private final a:Ljava/lang/String;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/events/SftpEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/events/SftpConnectedEvent;->a:Ljava/lang/String;

    iput p3, p0, Lcom/jscape/inet/sftp/events/SftpConnectedEvent;->c:I

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/inet/sftp/events/SftpListener;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/jscape/inet/sftp/events/SftpListener;->connected(Lcom/jscape/inet/sftp/events/SftpConnectedEvent;)V

    return-void
.end method

.method public getHostname()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/events/SftpConnectedEvent;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/sftp/events/SftpConnectedEvent;->c:I

    return v0
.end method
