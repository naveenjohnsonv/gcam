.class public Lcom/jscape/inet/sftp/events/SftpDeleteDirEvent;
.super Lcom/jscape/inet/sftp/events/SftpEvent;


# instance fields
.field private final a:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/events/SftpEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/events/SftpDeleteDirEvent;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/sftp/events/SftpDeleteDirEvent;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/inet/sftp/events/SftpListener;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/jscape/inet/sftp/events/SftpListener;->deleteDir(Lcom/jscape/inet/sftp/events/SftpDeleteDirEvent;)V

    return-void
.end method

.method public getDirectory()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/events/SftpDeleteDirEvent;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/events/SftpDeleteDirEvent;->c:Ljava/lang/String;

    return-object v0
.end method
