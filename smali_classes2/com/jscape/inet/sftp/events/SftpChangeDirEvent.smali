.class public Lcom/jscape/inet/sftp/events/SftpChangeDirEvent;
.super Lcom/jscape/inet/sftp/events/SftpEvent;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/events/SftpEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/events/SftpChangeDirEvent;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/inet/sftp/events/SftpListener;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/jscape/inet/sftp/events/SftpListener;->changeDir(Lcom/jscape/inet/sftp/events/SftpChangeDirEvent;)V

    return-void
.end method

.method public getDirectory()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/events/SftpChangeDirEvent;->a:Ljava/lang/String;

    return-object v0
.end method
