.class public Lcom/jscape/inet/sftp/events/SftpProgressEvent;
.super Lcom/jscape/inet/sftp/events/SftpEvent;


# static fields
.field public static final DOWNLOAD:I = 0x1

.field public static final UPLOAD:I


# instance fields
.field private final a:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:J

.field private final g:J


# direct methods
.method public constructor <init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJ)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/events/SftpEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;)V

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/events/SftpProgressEvent;->a:Ljava/lang/String;

    invoke-static {}, Lcom/jscape/inet/sftp/events/SftpEvent;->b()Z

    move-result p1

    iput-object p3, p0, Lcom/jscape/inet/sftp/events/SftpProgressEvent;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/inet/sftp/events/SftpProgressEvent;->d:Ljava/lang/String;

    iput p5, p0, Lcom/jscape/inet/sftp/events/SftpProgressEvent;->e:I

    iput-wide p6, p0, Lcom/jscape/inet/sftp/events/SftpProgressEvent;->f:J

    iput-wide p8, p0, Lcom/jscape/inet/sftp/events/SftpProgressEvent;->g:J

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/inet/sftp/events/SftpListener;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/jscape/inet/sftp/events/SftpListener;->progress(Lcom/jscape/inet/sftp/events/SftpProgressEvent;)V

    return-void
.end method

.method public getBytes()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/sftp/events/SftpProgressEvent;->g:J

    return-wide v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/events/SftpProgressEvent;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalFilePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/events/SftpProgressEvent;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getMode()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/sftp/events/SftpProgressEvent;->e:I

    return v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/events/SftpProgressEvent;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalBytes()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/sftp/events/SftpProgressEvent;->f:J

    return-wide v0
.end method
