.class final enum Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy$3;
.super Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy;


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy;-><init>(Ljava/lang/String;ILcom/jscape/inet/sftp/Sftp$1;)V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->d(Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;)I

    move-result v1

    if-lez v1, :cond_0

    sget-object v1, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy$3;->ALWAYS_RESUME:Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy;

    invoke-virtual {v1, p1}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy;->apply(Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    :cond_0
    :try_start_1
    sget-object v0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy$3;->FROM_SCRATCH:Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy;->apply(Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;)V

    :cond_1
    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy$3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy$3;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method
