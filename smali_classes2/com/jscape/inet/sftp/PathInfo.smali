.class public Lcom/jscape/inet/sftp/PathInfo;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final accessTime:J

.field public final group:Ljava/lang/String;

.field public final modificationTime:J

.field public final name:Ljava/lang/String;

.field public final owner:Ljava/lang/String;

.field public final permissions:Lcom/jscape/util/e/PosixFilePermissions;

.field public final size:J

.field public final type:Lcom/jscape/util/e/UnixFileType;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "m6Yd\u001d\n82e@n\u0001\u0014l\u0010\u0011w]i&\t7.6Ro\u000e\n4|1\u0007m6]x\u001f\u0002l\rm6Hb\u000c\u0002\"2B@l\nZ\u0007m6Zh\u0015\u0002l\tm6Ns\u0000\u0012!|1"

    const/16 v4, 0x47

    const/16 v5, 0xe

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x4c

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    const/16 v14, 0x1d

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v3, 0x13

    const-string v4, "u.\\v\u0013\u0016/0mPm\u001e\u0010\'\rg\\|J\tu.^n\u0019\u001a;d)"

    move v5, v3

    move-object v3, v4

    move v7, v10

    move v4, v14

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x54

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/sftp/PathInfo;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v10, v13

    rem-int/lit8 v1, v13, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v9, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    goto :goto_4

    :cond_4
    const/16 v14, 0x2b

    goto :goto_4

    :cond_5
    const/16 v14, 0x23

    goto :goto_4

    :cond_6
    const/16 v14, 0x4d

    goto :goto_4

    :cond_7
    const/16 v14, 0x65

    goto :goto_4

    :cond_8
    const/16 v14, 0x5a

    goto :goto_4

    :cond_9
    const/16 v14, 0xd

    :goto_4
    xor-int v1, v8, v14

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/jscape/util/e/UnixFileType;JLjava/lang/String;Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/PathInfo;->name:Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/PathInfo;->type:Lcom/jscape/util/e/UnixFileType;

    iput-wide p3, p0, Lcom/jscape/inet/sftp/PathInfo;->size:J

    iput-object p5, p0, Lcom/jscape/inet/sftp/PathInfo;->owner:Ljava/lang/String;

    iput-object p6, p0, Lcom/jscape/inet/sftp/PathInfo;->group:Ljava/lang/String;

    iput-object p7, p0, Lcom/jscape/inet/sftp/PathInfo;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    iput-wide p8, p0, Lcom/jscape/inet/sftp/PathInfo;->accessTime:J

    iput-wide p10, p0, Lcom/jscape/inet/sftp/PathInfo;->modificationTime:J

    return-void
.end method


# virtual methods
.method public directory()Z
    .locals 3

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/PathInfo;->type:Lcom/jscape/util/e/UnixFileType;

    sget-object v2, Lcom/jscape/util/e/UnixFileType;->DIRECTORY:Lcom/jscape/util/e/UnixFileType;

    if-eqz v0, :cond_0

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/sftp/PathInfo;->type:Lcom/jscape/util/e/UnixFileType;

    sget-object v2, Lcom/jscape/util/e/UnixFileType;->SYMBOLIC_LINK:Lcom/jscape/util/e/UnixFileType;

    :cond_0
    if-ne v1, v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public link()Z
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/sftp/PathInfo;->type:Lcom/jscape/util/e/UnixFileType;

    sget-object v1, Lcom/jscape/util/e/UnixFileType;->SYMBOLIC_LINK:Lcom/jscape/util/e/UnixFileType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public readable()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/PathInfo;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    iget-boolean v1, v1, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanRead:Z

    if-eqz v0, :cond_2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/PathInfo;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/PathInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/sftp/PathInfo;->type:Lcom/jscape/util/e/UnixFileType;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lcom/jscape/inet/sftp/PathInfo;->size:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v3, 0x7

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/sftp/PathInfo;->owner:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/sftp/PathInfo;->group:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/PathInfo;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/sftp/PathInfo;->accessTime:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/jscape/inet/sftp/PathInfo;->modificationTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writable()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/PathInfo;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    iget-boolean v1, v1, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanWrite:Z

    if-eqz v0, :cond_2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method
