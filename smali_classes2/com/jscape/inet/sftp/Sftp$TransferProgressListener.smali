.class Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/sftp/FileService$TransferListener;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:J

.field private final f:J

.field final g:Lcom/jscape/inet/sftp/Sftp;


# direct methods
.method private constructor <init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJ)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;->c:Ljava/lang/String;

    iput p5, p0, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;->d:I

    iput-wide p6, p0, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;->e:J

    iput-wide p8, p0, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;->f:J

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLcom/jscape/inet/sftp/Sftp$1;)V
    .locals 0

    invoke-direct/range {p0 .. p9}, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJ)V

    return-void
.end method


# virtual methods
.method public onTransferProgress(J)V
    .locals 12

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;->g:Lcom/jscape/inet/sftp/Sftp;

    new-instance v11, Lcom/jscape/inet/sftp/events/SftpProgressEvent;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;->g:Lcom/jscape/inet/sftp/Sftp;

    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;->c:Ljava/lang/String;

    iget v6, p0, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;->d:I

    iget-wide v7, p0, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;->f:J

    iget-wide v9, p0, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;->e:J

    add-long/2addr v9, p1

    move-object v1, v11

    invoke-direct/range {v1 .. v10}, Lcom/jscape/inet/sftp/events/SftpProgressEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJ)V

    invoke-static {v0, v11}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/Sftp;Lcom/jscape/inet/sftp/events/SftpEvent;)V

    return-void
.end method
