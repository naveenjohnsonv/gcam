.class public final enum Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum SSH_FXF_APPEND:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

.field public static final enum SSH_FXF_CREAT:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

.field public static final enum SSH_FXF_EXCL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

.field public static final enum SSH_FXF_READ:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

.field public static final enum SSH_FXF_TEXT:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

.field public static final enum SSH_FXF_TRUNC:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

.field public static final enum SSH_FXF_WRITE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

.field private static final a:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;


# instance fields
.field public final mask:I


# direct methods
.method static constructor <clinit>()V
    .locals 20

    const/4 v0, 0x7

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Q\u0010YHC\u00055]\u0011TVA\rQ\u0010YHC\u00055]\u0014C^Q\u0018\rQ\u0010YHC\u00055]\u0000CRD\t\u000cQ\u0010YHC\u00055]\u0017TOQ\rQ\u0010YHC\u00055]\u0017CBK\u001e"

    const/16 v5, 0x43

    const/16 v6, 0xc

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x57

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v14, v11

    move v15, v3

    :goto_2
    const/4 v0, 0x5

    const/4 v12, 0x4

    const/4 v2, 0x2

    if-gt v14, v15, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v13, :cond_1

    add-int/lit8 v0, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v0

    const/4 v0, 0x7

    goto :goto_0

    :cond_0
    const/16 v5, 0x1b

    const/16 v2, 0xe

    const-string v4, "&g.?4rB*u607d@\u000c&g.?4rB*q>#>"

    move v8, v0

    move v6, v2

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v6, v0

    move v8, v11

    :goto_3
    add-int/2addr v7, v10

    add-int v0, v7, v6

    invoke-virtual {v4, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v13, v3

    const/4 v0, 0x7

    const/16 v9, 0x20

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    aget-object v5, v1, v3

    invoke-direct {v4, v5, v3, v10}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_READ:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    aget-object v5, v1, v10

    invoke-direct {v4, v5, v10, v2}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_WRITE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    aget-object v5, v1, v0

    invoke-direct {v4, v5, v2, v12}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_APPEND:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    aget-object v5, v1, v2

    const/16 v6, 0x8

    const/4 v7, 0x3

    invoke-direct {v4, v5, v7, v6}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_CREAT:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    aget-object v5, v1, v12

    const/16 v6, 0x10

    invoke-direct {v4, v5, v12, v6}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_TRUNC:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    const/4 v5, 0x6

    aget-object v6, v1, v5

    const/16 v7, 0x20

    invoke-direct {v4, v6, v0, v7}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_EXCL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    const/4 v6, 0x3

    aget-object v1, v1, v6

    const/16 v7, 0x40

    invoke-direct {v4, v1, v5, v7}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_TEXT:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    const/4 v1, 0x7

    new-array v1, v1, [Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    sget-object v7, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_READ:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    aput-object v7, v1, v3

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_WRITE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    aput-object v3, v1, v10

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_APPEND:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    aput-object v3, v1, v2

    sget-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_CREAT:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    aput-object v2, v1, v6

    sget-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_TRUNC:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    aput-object v2, v1, v12

    sget-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_EXCL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    aput-object v2, v1, v0

    aput-object v4, v1, v5

    sput-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->a:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    return-void

    :cond_3
    const/16 v16, 0x20

    const/16 v17, 0x40

    const/16 v18, 0x7

    aget-char v19, v11, v15

    rem-int/lit8 v3, v15, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v10, :cond_8

    if-eq v3, v2, :cond_7

    const/4 v2, 0x3

    if-eq v3, v2, :cond_6

    if-eq v3, v12, :cond_5

    if-eq v3, v0, :cond_4

    const/16 v2, 0x24

    goto :goto_4

    :cond_4
    const/16 v2, 0xa

    goto :goto_4

    :cond_5
    const/16 v2, 0x52

    goto :goto_4

    :cond_6
    move/from16 v2, v17

    goto :goto_4

    :cond_7
    const/16 v2, 0x46

    goto :goto_4

    :cond_8
    const/16 v2, 0x14

    goto :goto_4

    :cond_9
    const/16 v2, 0x55

    :goto_4
    xor-int v0, v9, v2

    xor-int v0, v19, v0

    int-to-char v0, v0

    aput-char v0, v11, v15

    add-int/lit8 v15, v15, 0x1

    move/from16 v0, v18

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->mask:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;
    .locals 1

    const-class v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->a:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    invoke-virtual {v0}, [Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    return-object v0
.end method


# virtual methods
.method public presentIn(I)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->mask:I

    and-int/2addr p1, v1

    if-nez v0, :cond_1

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    return p1
.end method

.method public set(ZI)I
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    iget p1, p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->mask:I

    or-int/2addr p1, p2

    :cond_0
    move p2, p1

    :cond_1
    return p2
.end method
