.class public final enum Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum SSH_FILEXFER_ATTR_ACCESSTIME:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

.field public static final enum SSH_FILEXFER_ATTR_ACL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

.field public static final enum SSH_FILEXFER_ATTR_CREATETIME:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

.field public static final enum SSH_FILEXFER_ATTR_EXTENDED:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

.field public static final enum SSH_FILEXFER_ATTR_MODIFYTIME:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

.field public static final enum SSH_FILEXFER_ATTR_OWNERGROUP:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

.field public static final enum SSH_FILEXFER_ATTR_PERMISSIONS:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

.field public static final enum SSH_FILEXFER_ATTR_SIZE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

.field public static final enum SSH_FILEXFER_ATTR_SUBSECOND_TIMES:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

.field private static final a:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;


# instance fields
.field public final code:I


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const/16 v0, 0x9

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "\u0012R>,YM\u0013\u0004Y06M[\u001e\u0015U$,^G\u0013!\u0012R>,YM\u0013\u0004Y06M[\u001e\u0015U$,LQ\u001d\u0012D5<Q@\u0000\u0015H;6L\u0016\u0012R>,YM\u0013\u0004Y06M[\u001e\u0015U$,LM\u0005\u0004\u001c\u0012R>,YM\u0013\u0004Y06M[\u001e\u0015U$,\\V\u001a\u0000U3\'VI\u001a\u001c\u0012R>,YM\u0013\u0004Y06M[\u001e\u0015U$,PS\u0011\u0004S1!PQ\u000f\u001c\u0012R>,YM\u0013\u0004Y06M[\u001e\u0015U$,^G\u001c\u0004R%\'VI\u001a\u001d\u0012R>,YM\u0013\u0004Y06M[\u001e\u0015U$,OA\r\u000cH% VK\u0011\u0012"

    const/16 v5, 0xc3

    const/16 v6, 0x15

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x7f

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v2, 0x5

    const/4 v15, 0x2

    const/4 v0, 0x4

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v0, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v0

    const/16 v0, 0x9

    goto :goto_0

    :cond_0
    const/16 v5, 0x37

    const/16 v2, 0x1c

    const-string v4, " `\u000c\u001ek\u007f!6k\u0002\u0004\u007fi,\'g\u0016\u001e`y):u\u001d\u0015d{(\u001a `\u000c\u001ek\u007f!6k\u0002\u0004\u007fi,\'g\u0016\u001ehn96}\u0000\u0004i"

    move v8, v0

    move v6, v2

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v6, v0

    move v8, v11

    :goto_3
    const/16 v9, 0x4d

    add-int/2addr v7, v10

    add-int v0, v7, v6

    invoke-virtual {v4, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    const/16 v0, 0x9

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    aget-object v5, v1, v15

    invoke-direct {v4, v5, v3, v10}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_SIZE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    const/4 v5, 0x6

    aget-object v6, v1, v5

    invoke-direct {v4, v6, v10, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_PERMISSIONS:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    aget-object v6, v1, v2

    const/16 v7, 0x8

    invoke-direct {v4, v6, v15, v7}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_ACCESSTIME:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    const/4 v6, 0x3

    aget-object v8, v1, v6

    const/16 v9, 0x10

    invoke-direct {v4, v8, v6, v9}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_CREATETIME:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    const/4 v6, 0x7

    aget-object v8, v1, v6

    const/16 v9, 0x20

    invoke-direct {v4, v8, v0, v9}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_MODIFYTIME:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    aget-object v8, v1, v3

    const/16 v9, 0x40

    invoke-direct {v4, v8, v2, v9}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_ACL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    aget-object v8, v1, v0

    const/16 v9, 0x80

    invoke-direct {v4, v8, v5, v9}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_OWNERGROUP:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    aget-object v8, v1, v10

    const/16 v9, 0x100

    invoke-direct {v4, v8, v6, v9}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_SUBSECOND_TIMES:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    aget-object v1, v1, v7

    const/high16 v8, -0x80000000

    invoke-direct {v4, v1, v7, v8}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_EXTENDED:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    const/16 v1, 0x9

    new-array v1, v1, [Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    sget-object v8, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_SIZE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    aput-object v8, v1, v3

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_PERMISSIONS:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    aput-object v3, v1, v10

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_ACCESSTIME:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    aput-object v3, v1, v15

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_CREATETIME:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    const/4 v8, 0x3

    aput-object v3, v1, v8

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_MODIFYTIME:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    aput-object v3, v1, v0

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_ACL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    aput-object v0, v1, v2

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_OWNERGROUP:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    aput-object v0, v1, v5

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_SUBSECOND_TIMES:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    aput-object v0, v1, v6

    aput-object v4, v1, v7

    sput-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->a:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    return-void

    :cond_3
    const/16 v16, 0x20

    const/16 v17, 0x9

    aget-char v18, v11, v14

    rem-int/lit8 v3, v14, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v10, :cond_8

    if-eq v3, v15, :cond_7

    const/4 v15, 0x3

    if-eq v3, v15, :cond_6

    if-eq v3, v0, :cond_5

    if-eq v3, v2, :cond_4

    move/from16 v15, v16

    goto :goto_4

    :cond_4
    const/16 v15, 0x7b

    goto :goto_4

    :cond_5
    const/16 v15, 0x60

    goto :goto_4

    :cond_6
    const/16 v15, 0xc

    goto :goto_4

    :cond_7
    move/from16 v15, v17

    goto :goto_4

    :cond_8
    const/16 v15, 0x7e

    goto :goto_4

    :cond_9
    const/16 v15, 0x3e

    :goto_4
    xor-int v0, v9, v15

    xor-int v0, v18, v0

    int-to-char v0, v0

    aput-char v0, v11, v14

    add-int/lit8 v14, v14, 0x1

    move/from16 v0, v17

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->code:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;
    .locals 1

    const-class v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->a:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-virtual {v0}, [Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    return-object v0
.end method


# virtual methods
.method public presentIn(I)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->code:I

    and-int/2addr p1, v1

    if-nez v0, :cond_1

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    return p1
.end method

.method public set(ZI)I
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    iget p1, p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->code:I

    or-int/2addr p1, p2

    :cond_0
    move p2, p1

    :cond_1
    return p2
.end method
