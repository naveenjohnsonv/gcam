.class public final enum Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ACE4_ADD_FILE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field public static final enum ACE4_ADD_SUBDIRECTORY:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field public static final enum ACE4_APPEND_DATA:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field public static final enum ACE4_DELETE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field public static final enum ACE4_DELETE_CHILD:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field public static final enum ACE4_EXECUTE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field public static final enum ACE4_LIST_DIRECTORY:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field public static final enum ACE4_READ_ACL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field public static final enum ACE4_READ_ATTRIBUTES:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field public static final enum ACE4_READ_DATA:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field public static final enum ACE4_READ_NAMED_ATTRS:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field public static final enum ACE4_SYNCHRONIZE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field public static final enum ACE4_WRITE_ACL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field public static final enum ACE4_WRITE_ATTRIBUTES:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field public static final enum ACE4_WRITE_DATA:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field public static final enum ACE4_WRITE_NAMED_ATTRS:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field public static final enum ACE4_WRITE_OWNER:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

.field private static final a:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;


# instance fields
.field public final code:I


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const/16 v0, 0x11

    new-array v1, v0, [Ljava/lang/String;

    const/16 v3, 0x10

    const/4 v4, 0x0

    const-string v5, "l<\u0008\u001aB{w}:\u0003jB~fy>\rl<\u0008\u001aB{ci \u000bgQ\u007f\u000bl<\u0008\u001aB~ba:\u0019k\u0010l<\u0008\u001aBi~c<\u0005|Rtnw:\u0014l<\u0008\u001aBhbl;\u0012oInud=\u0018zXi\u0015l<\u0008\u001aBmud+\u0008q\\ns\u007f6\u000f{I\u007ft\u000cl<\u0008\u001aB\u007f\u007fh<\u0018zX\u0015l<\u0008\u001aB{ci \u001e{_~n\u007f:\u000ezRh~\u0015l<\u0008\u001aBhbl;\u0012`\\wbi \u000czIht\u000el<\u0008\u001aBmud+\u0008q\\yk\rl<\u0008\u001aBhbl;\u0012o^v\u0013l<\u0008\u001aBvn~+\u0012jThbn+\u0002|D\u0010l<\u0008\u001aBmud+\u0008qRmih-\u000el<\u0008\u001aBhbl;\u0012j\\nf\u0011l<\u0008\u001aB~ba:\u0019kByod3\t"

    const/16 v6, 0x102

    move v8, v3

    move v9, v4

    const/4 v7, -0x1

    :goto_0
    const/16 v10, 0x7d

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v14, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v15, v12

    move v2, v4

    :goto_2
    const/4 v13, 0x3

    const/4 v0, 0x2

    if-gt v15, v2, :cond_3

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    if-eqz v14, :cond_1

    add-int/lit8 v0, v9, 0x1

    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v0

    const/16 v0, 0x11

    goto :goto_0

    :cond_0
    const/16 v6, 0x26

    const/16 v2, 0x16

    const-string v5, "\u001eNzh0\u001f\u0007\u0016Yz\u0003!\t\u0018\u001aI`\u001d;\u001c\u0007\u000c\u000f\u001eNzh0\u001f\u0007\u0016Yz\u0003+\t\u0001\u001e"

    move v9, v0

    move v8, v2

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v10, v9, 0x1

    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v8, v0

    move v9, v10

    :goto_3
    add-int/2addr v7, v11

    add-int v0, v7, v8

    invoke-virtual {v5, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v14, v4

    const/16 v0, 0x11

    const/16 v10, 0xf

    goto :goto_1

    :cond_2
    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    const/16 v5, 0xd

    aget-object v6, v1, v5

    invoke-direct {v2, v6, v4, v11}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_READ_DATA:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    const/16 v6, 0xb

    aget-object v7, v1, v6

    invoke-direct {v2, v7, v11, v11}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_LIST_DIRECTORY:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aget-object v7, v1, v3

    invoke-direct {v2, v7, v0, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_DATA:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aget-object v7, v1, v11

    invoke-direct {v2, v7, v13, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_ADD_FILE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aget-object v7, v1, v4

    const/4 v8, 0x4

    invoke-direct {v2, v7, v8, v8}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_APPEND_DATA:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    const/4 v7, 0x7

    aget-object v9, v1, v7

    const/4 v10, 0x5

    invoke-direct {v2, v9, v10, v8}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_ADD_SUBDIRECTORY:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    const/16 v8, 0x8

    aget-object v9, v1, v8

    const/4 v10, 0x6

    invoke-direct {v2, v9, v10, v8}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_READ_NAMED_ATTRS:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    const/16 v9, 0xf

    aget-object v12, v1, v9

    invoke-direct {v2, v12, v7, v3}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_NAMED_ATTRS:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aget-object v9, v1, v10

    const/16 v12, 0x20

    invoke-direct {v2, v9, v8, v12}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_EXECUTE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    const/16 v9, 0xe

    aget-object v12, v1, v9

    const/16 v14, 0x40

    const/16 v15, 0x9

    invoke-direct {v2, v12, v15, v14}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_DELETE_CHILD:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    const/4 v12, 0x4

    aget-object v14, v1, v12

    const/16 v12, 0x80

    const/16 v8, 0xa

    invoke-direct {v2, v14, v8, v12}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_READ_ATTRIBUTES:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    const/4 v12, 0x5

    aget-object v14, v1, v12

    const/16 v12, 0x100

    invoke-direct {v2, v14, v6, v12}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_ATTRIBUTES:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aget-object v12, v1, v0

    const/high16 v14, 0x10000

    const/16 v6, 0xc

    invoke-direct {v2, v12, v6, v14}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_DELETE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aget-object v12, v1, v8

    const/high16 v14, 0x20000

    invoke-direct {v2, v12, v5, v14}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_READ_ACL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aget-object v12, v1, v15

    const/high16 v14, 0x40000

    invoke-direct {v2, v12, v9, v14}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_ACL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aget-object v12, v1, v6

    const/high16 v14, 0x80000

    const/16 v9, 0xf

    invoke-direct {v2, v12, v9, v14}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_OWNER:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aget-object v1, v1, v13

    const/high16 v9, 0x100000

    invoke-direct {v2, v1, v3, v9}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_SYNCHRONIZE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    const/16 v1, 0x11

    new-array v1, v1, [Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    sget-object v9, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_READ_DATA:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aput-object v9, v1, v4

    sget-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_LIST_DIRECTORY:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aput-object v4, v1, v11

    sget-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_DATA:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aput-object v4, v1, v0

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_ADD_FILE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aput-object v0, v1, v13

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_APPEND_DATA:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    const/4 v4, 0x4

    aput-object v0, v1, v4

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_ADD_SUBDIRECTORY:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    const/4 v4, 0x5

    aput-object v0, v1, v4

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_READ_NAMED_ATTRS:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aput-object v0, v1, v10

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_NAMED_ATTRS:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aput-object v0, v1, v7

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_EXECUTE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    const/16 v4, 0x8

    aput-object v0, v1, v4

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_DELETE_CHILD:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aput-object v0, v1, v15

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_READ_ATTRIBUTES:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aput-object v0, v1, v8

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_ATTRIBUTES:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    const/16 v4, 0xb

    aput-object v0, v1, v4

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_DELETE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aput-object v0, v1, v6

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_READ_ACL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    aput-object v0, v1, v5

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_ACL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    const/16 v4, 0xe

    aput-object v0, v1, v4

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_OWNER:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    const/16 v16, 0xf

    aput-object v0, v1, v16

    aput-object v2, v1, v3

    sput-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->a:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    return-void

    :cond_3
    const/16 v16, 0xf

    const/16 v17, 0x11

    aget-char v18, v12, v2

    rem-int/lit8 v3, v2, 0x7

    if-eqz v3, :cond_8

    if-eq v3, v11, :cond_9

    if-eq v3, v0, :cond_7

    if-eq v3, v13, :cond_6

    const/4 v0, 0x4

    if-eq v3, v0, :cond_5

    const/4 v0, 0x5

    if-eq v3, v0, :cond_4

    const/16 v0, 0x5a

    goto :goto_4

    :cond_4
    const/16 v0, 0x47

    goto :goto_4

    :cond_5
    const/16 v0, 0x60

    goto :goto_4

    :cond_6
    const/16 v0, 0x53

    goto :goto_4

    :cond_7
    const/16 v0, 0x30

    goto :goto_4

    :cond_8
    const/16 v0, 0x50

    :cond_9
    :goto_4
    xor-int/2addr v0, v10

    xor-int v0, v18, v0

    int-to-char v0, v0

    aput-char v0, v12, v2

    add-int/lit8 v2, v2, 0x1

    const/16 v3, 0x10

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->code:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;
    .locals 1

    const-class v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->a:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v0}, [Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    return-object v0
.end method


# virtual methods
.method public presentIn(I)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->code:I

    and-int/2addr p1, v1

    if-nez v0, :cond_1

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    return p1
.end method

.method public set(ZI)I
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    iget p1, p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->code:I

    or-int/2addr p1, p2

    :cond_0
    move p2, p1

    :cond_1
    return p2
.end method
