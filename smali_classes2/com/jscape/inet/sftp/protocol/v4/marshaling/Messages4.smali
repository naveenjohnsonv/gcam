.class public Lcom/jscape/inet/sftp/protocol/v4/marshaling/Messages4;
.super Ljava/lang/Object;


# static fields
.field public static final ENTRIES:[Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

.field public static final EXTENDED_ENTRIES:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedCodec$Entry;

.field public static final EXTENDED_REPLY_ENTRIES:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec$Entry;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/16 v2, 0x9

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x23

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "kn\u000c\u0003zqBz`\u000f|c\u0011\u0014</Avg\u0011Z9cJz"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    const/4 v12, 0x5

    const/4 v13, 0x3

    const/4 v14, 0x4

    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x19

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move/from16 v16, v5

    move v5, v4

    move/from16 v4, v16

    goto :goto_0

    :cond_0
    new-array v5, v0, [Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedCodec$Entry;

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedCodec$Entry;

    aget-object v7, v1, v8

    new-instance v9, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedCheckFileNameCodec;

    invoke-direct {v9}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedCheckFileNameCodec;-><init>()V

    const-class v10, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;

    invoke-direct {v6, v7, v9, v10}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedCodec$TypeCodec;Ljava/lang/Class;)V

    aput-object v6, v5, v3

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedCodec$Entry;

    aget-object v1, v1, v3

    new-instance v7, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedTextSeekCodec;

    invoke-direct {v7}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedTextSeekCodec;-><init>()V

    const-class v9, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;

    invoke-direct {v6, v1, v7, v9}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedCodec$TypeCodec;Ljava/lang/Class;)V

    aput-object v6, v5, v8

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/Messages4;->EXTENDED_ENTRIES:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedCodec$Entry;

    new-array v1, v8, [Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec$Entry;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec$Entry;

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCheckFileNameCodec;

    invoke-direct {v6}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCheckFileNameCodec;-><init>()V

    const-class v7, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedReplyCheckFileName;

    invoke-direct {v5, v6, v7}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec$Entry;-><init>(Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec$TypeCodec;Ljava/lang/Class;)V

    aput-object v5, v1, v3

    sput-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/Messages4;->EXTENDED_REPLY_ENTRIES:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec$Entry;

    const/16 v1, 0x1b

    new-array v1, v1, [Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpInitCodec;

    invoke-direct {v6}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpInitCodec;-><init>()V

    new-array v7, v8, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpInit;

    aput-object v9, v7, v3

    invoke-direct {v5, v8, v6, v7}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v1, v3

    new-instance v5, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpVersionCodec;

    invoke-direct {v6}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpVersionCodec;-><init>()V

    new-array v7, v8, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpVersion;

    aput-object v9, v7, v3

    invoke-direct {v5, v0, v6, v7}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v1, v8

    new-instance v5, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpOpenCodec;

    invoke-direct {v6}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpOpenCodec;-><init>()V

    new-array v7, v8, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;

    aput-object v9, v7, v3

    invoke-direct {v5, v13, v6, v7}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v1, v0

    new-instance v5, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpCloseCodec;

    invoke-direct {v6}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpCloseCodec;-><init>()V

    new-array v7, v8, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpClose;

    aput-object v9, v7, v3

    invoke-direct {v5, v14, v6, v7}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v1, v13

    new-instance v5, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpReadCodec;

    invoke-direct {v6}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpReadCodec;-><init>()V

    new-array v7, v8, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpRead;

    aput-object v9, v7, v3

    invoke-direct {v5, v12, v6, v7}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v1, v14

    new-instance v5, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpWriteCodec;

    invoke-direct {v6}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpWriteCodec;-><init>()V

    new-array v7, v8, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpWrite;

    aput-object v9, v7, v3

    const/4 v9, 0x6

    invoke-direct {v5, v9, v6, v7}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v1, v12

    new-instance v5, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpLstatCodec;

    invoke-direct {v6}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpLstatCodec;-><init>()V

    new-array v7, v8, [Ljava/lang/Class;

    const-class v10, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpLstat;

    aput-object v10, v7, v3

    const/4 v10, 0x7

    invoke-direct {v5, v10, v6, v7}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v1, v9

    new-instance v5, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpFstatCodec;

    invoke-direct {v6}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpFstatCodec;-><init>()V

    new-array v7, v8, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpFstat;

    aput-object v9, v7, v3

    const/16 v9, 0x8

    invoke-direct {v5, v9, v6, v7}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v1, v10

    new-instance v5, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpSetStatCodec;

    invoke-direct {v6}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpSetStatCodec;-><init>()V

    new-array v7, v8, [Ljava/lang/Class;

    const-class v10, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpSetStat;

    aput-object v10, v7, v3

    invoke-direct {v5, v2, v6, v7}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v1, v9

    new-instance v5, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpFsetStatCodec;

    invoke-direct {v6}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpFsetStatCodec;-><init>()V

    new-array v7, v8, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpFsetStat;

    aput-object v9, v7, v3

    const/16 v9, 0xa

    invoke-direct {v5, v9, v6, v7}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v1, v2

    new-instance v2, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpOpendirCodec;

    invoke-direct {v5}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpOpendirCodec;-><init>()V

    new-array v6, v8, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpendir;

    aput-object v7, v6, v3

    const/16 v7, 0xb

    invoke-direct {v2, v7, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v9

    new-instance v2, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpReaddirCodec;

    invoke-direct {v5}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpReaddirCodec;-><init>()V

    new-array v6, v8, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpReaddir;

    aput-object v9, v6, v3

    const/16 v9, 0xc

    invoke-direct {v2, v9, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpRemoveCodec;

    invoke-direct {v5}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpRemoveCodec;-><init>()V

    new-array v6, v8, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpRemove;

    aput-object v7, v6, v3

    const/16 v7, 0xd

    invoke-direct {v2, v7, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v9

    new-instance v2, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpMkdirCodec;

    invoke-direct {v5}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpMkdirCodec;-><init>()V

    new-array v6, v8, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpMkdir;

    aput-object v9, v6, v3

    const/16 v9, 0xe

    invoke-direct {v2, v9, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpRmdirCodec;

    invoke-direct {v5}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpRmdirCodec;-><init>()V

    new-array v6, v8, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpRmdir;

    aput-object v7, v6, v3

    const/16 v7, 0xf

    invoke-direct {v2, v7, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v9

    new-instance v2, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpRealPathCodec;

    invoke-direct {v5}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpRealPathCodec;-><init>()V

    new-array v6, v8, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpRealPath;

    aput-object v9, v6, v3

    const/16 v9, 0x10

    invoke-direct {v2, v9, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpStatCodec;

    invoke-direct {v5}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpStatCodec;-><init>()V

    new-array v6, v8, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStat;

    aput-object v7, v6, v3

    const/16 v7, 0x11

    invoke-direct {v2, v7, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v9

    new-instance v2, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpRenameCodec;

    invoke-direct {v5}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpRenameCodec;-><init>()V

    new-array v6, v8, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpRename;

    aput-object v9, v6, v3

    const/16 v9, 0x12

    invoke-direct {v2, v9, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpReadLinkCodec;

    invoke-direct {v5}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpReadLinkCodec;-><init>()V

    new-array v6, v8, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpReadLink;

    aput-object v7, v6, v3

    const/16 v7, 0x13

    invoke-direct {v2, v7, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v9

    new-instance v2, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpSymLinkCodec;

    invoke-direct {v5}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpSymLinkCodec;-><init>()V

    new-array v6, v8, [Ljava/lang/Class;

    const-class v9, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpSymLink;

    aput-object v9, v6, v3

    const/16 v9, 0x14

    invoke-direct {v2, v9, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    const/16 v5, 0x65

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpStatusCodec;

    invoke-direct {v6}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpStatusCodec;-><init>()V

    new-array v7, v8, [Ljava/lang/Class;

    const-class v10, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;

    aput-object v10, v7, v3

    invoke-direct {v2, v5, v6, v7}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v9

    const/16 v2, 0x15

    new-instance v5, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    const/16 v6, 0x66

    new-instance v7, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpHandleCodec;

    invoke-direct {v7}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpHandleCodec;-><init>()V

    new-array v9, v8, [Ljava/lang/Class;

    const-class v10, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpHandle;

    aput-object v10, v9, v3

    invoke-direct {v5, v6, v7, v9}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v1, v2

    const/16 v2, 0x16

    new-instance v5, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    const/16 v6, 0x67

    new-instance v7, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpDataCodec;

    invoke-direct {v7}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpDataCodec;-><init>()V

    new-array v9, v8, [Ljava/lang/Class;

    const-class v10, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpData;

    aput-object v10, v9, v3

    invoke-direct {v5, v6, v7, v9}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v1, v2

    const/16 v2, 0x17

    new-instance v5, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    const/16 v6, 0x68

    new-instance v7, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpNameCodec;

    invoke-direct {v7}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpNameCodec;-><init>()V

    new-array v9, v8, [Ljava/lang/Class;

    const-class v10, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpName;

    aput-object v10, v9, v3

    invoke-direct {v5, v6, v7, v9}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v1, v2

    const/16 v2, 0x18

    new-instance v5, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    const/16 v6, 0x69

    new-instance v7, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpAttrsCodec;

    invoke-direct {v7}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpAttrsCodec;-><init>()V

    new-array v9, v8, [Ljava/lang/Class;

    const-class v10, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpAttrs;

    aput-object v10, v9, v3

    invoke-direct {v5, v6, v7, v9}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v5, v1, v2

    new-instance v2, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    const/16 v5, 0xc8

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedCodec;

    sget-object v7, Lcom/jscape/inet/sftp/protocol/v4/marshaling/Messages4;->EXTENDED_ENTRIES:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedCodec$Entry;

    invoke-direct {v6, v7}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedCodec;-><init>([Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedCodec$Entry;)V

    new-array v0, v0, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;

    aput-object v7, v0, v3

    const-class v7, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;

    aput-object v7, v0, v8

    invoke-direct {v2, v5, v6, v0}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v4

    const/16 v0, 0x1a

    new-instance v2, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    const/16 v4, 0xc9

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec;

    sget-object v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/Messages4;->EXTENDED_REPLY_ENTRIES:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec$Entry;

    invoke-direct {v5, v6}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec;-><init>([Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec$Entry;)V

    new-array v6, v8, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedReplyCheckFileName;

    aput-object v7, v6, v3

    invoke-direct {v2, v4, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v2, v1, v0

    sput-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/Messages4;->ENTRIES:[Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    return-void

    :cond_1
    aget-char v15, v4, v11

    rem-int/lit8 v2, v11, 0x7

    if-eqz v2, :cond_7

    if-eq v2, v8, :cond_6

    if-eq v2, v0, :cond_5

    if-eq v2, v13, :cond_4

    if-eq v2, v14, :cond_3

    if-eq v2, v12, :cond_2

    goto :goto_2

    :cond_2
    const/16 v14, 0x21

    goto :goto_2

    :cond_3
    const/16 v14, 0x74

    goto :goto_2

    :cond_4
    const/16 v14, 0x54

    goto :goto_2

    :cond_5
    const/16 v14, 0x57

    goto :goto_2

    :cond_6
    const/16 v14, 0x28

    goto :goto_2

    :cond_7
    const/16 v14, 0x3c

    :goto_2
    xor-int v2, v7, v14

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v4, v11

    add-int/lit8 v11, v11, 0x1

    const/16 v2, 0x9

    goto/16 :goto_1
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init(Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;)Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/Messages4;->ENTRIES:[Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    invoke-virtual {p0, v0}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;->set([Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;

    move-result-object p0

    return-object p0
.end method

.method public static init(Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec;)Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/Messages4;->EXTENDED_REPLY_ENTRIES:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec$Entry;

    invoke-virtual {p0, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec;->set([Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec$Entry;)Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec;

    move-result-object p0

    return-object p0
.end method
