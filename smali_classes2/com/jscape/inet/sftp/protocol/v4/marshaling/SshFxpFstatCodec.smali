.class public Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpFstatCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/sftp/protocol/messages/Message;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/messages/Message;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v0

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readValue(Ljava/io/InputStream;)[B

    move-result-object v1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat;->parse(I)Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;

    move-result-object p1

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpFstat;

    invoke-direct {v2, v0, v1, p1}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpFstat;-><init>(I[BLcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;)V

    return-object v2
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpFstatCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/jscape/inet/sftp/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpFstat;

    iget v0, p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpFstat;->id:I

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-object v0, p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpFstat;->handle:[B

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpFstat;->flags:Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;

    invoke-static {p1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat;->format(Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;)I

    move-result p1

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/sftp/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpFstatCodec;->write(Lcom/jscape/inet/sftp/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
