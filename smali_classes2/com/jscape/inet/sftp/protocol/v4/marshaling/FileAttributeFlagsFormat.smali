.class public Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static format(Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;)I
    .locals 3

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_SIZE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->sizeAttributePresent:Z

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->set(ZI)I

    move-result v0

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_PERMISSIONS:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->permissionsAttributePresent:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_ACCESSTIME:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->accessTimeAttributePresent:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_CREATETIME:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->createTimeAttributePresent:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_MODIFYTIME:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->modifyTimeAttributePresent:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_ACL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->aclAttributePresent:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_OWNERGROUP:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->ownerGroupAttributesPresent:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_SUBSECOND_TIMES:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->subsecondTimesPresent:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_EXTENDED:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    iget-boolean p0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->extendedAttributesPresent:Z

    invoke-virtual {v1, p0, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->set(ZI)I

    move-result p0

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "SjcrDc"

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b(Ljava/lang/String;)V

    :cond_0
    return p0
.end method

.method public static parse(I)Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;
    .locals 11

    new-instance v10, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_SIZE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->presentIn(I)Z

    move-result v1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_PERMISSIONS:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->presentIn(I)Z

    move-result v2

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_ACCESSTIME:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->presentIn(I)Z

    move-result v3

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_CREATETIME:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->presentIn(I)Z

    move-result v4

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_MODIFYTIME:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->presentIn(I)Z

    move-result v5

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_ACL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->presentIn(I)Z

    move-result v6

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_OWNERGROUP:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->presentIn(I)Z

    move-result v7

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_SUBSECOND_TIMES:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->presentIn(I)Z

    move-result v8

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_EXTENDED:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat$Mask;->presentIn(I)Z

    move-result v9

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;-><init>(ZZZZZZZZZ)V

    return-object v10
.end method
