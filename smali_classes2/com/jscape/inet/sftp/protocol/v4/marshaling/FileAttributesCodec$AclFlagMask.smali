.class public final enum Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ACE4_DIRECTORY_INHERIT_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

.field public static final enum ACE4_FAILED_ACCESS_ACE_FLAG:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

.field public static final enum ACE4_FILE_INHERIT_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

.field public static final enum ACE4_IDENTIFIER_GROUP:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

.field public static final enum ACE4_INHERIT_ONLY_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

.field public static final enum ACE4_NO_PROPAGATE_INHERIT_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

.field public static final enum ACE4_SUCCESSFUL_ACCESS_ACE_FLAG:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

.field private static final a:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;


# instance fields
.field public final code:I


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x7

    new-array v1, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, ":\u00125q\u0012f\u00123\u0014\"\u000c\u0019p\u00135\u001d)\u001a\u000cl\u0019\u001f:\u00125q\u0012|\t8\u00125\u0016\u001ei\t7\u000e1\u0006\u000ej\u000f(\u000e1\u0006\u0008p\u001a7\u00107\u001d:\u00125q\u0012a\u0013$\u0001\"\n\u001dn\u001b:\u00055\u001a\u0004a\u0014>\u00039\u0011\u0012n\u001f>\u0015:\u00125q\u0012i\u00157\u0014/\u000c\u0003g\u0019)\u0018$\u001a\u000cl\u0019\u001b:\u00125q\u0012i\u001d2\u001d5\u0001\u0012n\u001f8\u0014#\u0016\u0012n\u001f>\u000e6\t\u000ch"

    const/16 v6, 0x85

    move v9, v4

    const/4 v7, -0x1

    const/16 v8, 0x15

    :goto_0
    const/16 v10, 0x52

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    move v15, v4

    :goto_2
    const/4 v3, 0x4

    const/4 v0, 0x3

    const/4 v2, 0x2

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_1

    add-int/lit8 v0, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v0

    const/4 v0, 0x7

    goto :goto_0

    :cond_0
    const/16 v6, 0x30

    const-string v5, "\u0005-\nN-Y\'\u0001 \u001b34Y&\u00161\u0008(=E3\u001a\u0005-\nN-T*\u0016+\u000c.=B:\u001b\'\u000127B*\u00101\u000e97"

    move v9, v0

    const/4 v7, -0x1

    const/16 v8, 0x15

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v8, v0

    move v9, v12

    :goto_3
    const/16 v10, 0x6d

    add-int/2addr v7, v11

    add-int v0, v7, v8

    invoke-virtual {v5, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v13, v4

    const/4 v0, 0x7

    goto :goto_1

    :cond_2
    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    aget-object v6, v1, v0

    invoke-direct {v5, v6, v4, v11}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_FILE_INHERIT_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    const/4 v6, 0x6

    aget-object v7, v1, v6

    invoke-direct {v5, v7, v11, v2}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_DIRECTORY_INHERIT_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    aget-object v7, v1, v2

    invoke-direct {v5, v7, v2, v3}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_NO_PROPAGATE_INHERIT_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    aget-object v7, v1, v4

    const/16 v8, 0x8

    invoke-direct {v5, v7, v0, v8}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_INHERIT_ONLY_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    aget-object v7, v1, v11

    const/16 v8, 0x10

    invoke-direct {v5, v7, v3, v8}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_SUCCESSFUL_ACCESS_ACE_FLAG:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    aget-object v7, v1, v3

    const/16 v8, 0x20

    const/4 v9, 0x5

    invoke-direct {v5, v7, v9, v8}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_FAILED_ACCESS_ACE_FLAG:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    aget-object v1, v1, v9

    const/16 v7, 0x40

    invoke-direct {v5, v1, v6, v7}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_IDENTIFIER_GROUP:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    const/4 v1, 0x7

    new-array v1, v1, [Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    sget-object v7, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_FILE_INHERIT_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    aput-object v7, v1, v4

    sget-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_DIRECTORY_INHERIT_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    aput-object v4, v1, v11

    sget-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_NO_PROPAGATE_INHERIT_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    aput-object v4, v1, v2

    sget-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_INHERIT_ONLY_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    aput-object v2, v1, v0

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_SUCCESSFUL_ACCESS_ACE_FLAG:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    aput-object v0, v1, v3

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_FAILED_ACCESS_ACE_FLAG:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    const/4 v2, 0x5

    aput-object v0, v1, v2

    aput-object v5, v1, v6

    sput-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->a:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    return-void

    :cond_3
    const/16 v16, 0x7

    aget-char v17, v12, v15

    rem-int/lit8 v4, v15, 0x7

    if-eqz v4, :cond_8

    if-eq v4, v11, :cond_9

    if-eq v4, v2, :cond_7

    if-eq v4, v0, :cond_6

    if-eq v4, v3, :cond_5

    const/4 v0, 0x5

    if-eq v4, v0, :cond_4

    const/16 v0, 0xe

    goto :goto_4

    :cond_4
    const/16 v0, 0x7d

    goto :goto_4

    :cond_5
    const/16 v0, 0x1f

    goto :goto_4

    :cond_6
    const/16 v0, 0x17

    goto :goto_4

    :cond_7
    const/16 v0, 0x22

    goto :goto_4

    :cond_8
    const/16 v0, 0x29

    :cond_9
    :goto_4
    xor-int/2addr v0, v10

    xor-int v0, v17, v0

    int-to-char v0, v0

    aput-char v0, v12, v15

    add-int/lit8 v15, v15, 0x1

    move/from16 v0, v16

    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->code:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;
    .locals 1

    const-class v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->a:[Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    invoke-virtual {v0}, [Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    return-object v0
.end method


# virtual methods
.method public presentIn(I)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->code:I

    and-int/2addr p1, v1

    if-nez v0, :cond_1

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    return p1
.end method

.method public set(ZI)I
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    iget p1, p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->code:I

    or-int/2addr p1, p2

    :cond_0
    move p2, p1

    :cond_1
    return p2
.end method
