.class public Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedTextSeekCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedCodec$TypeCodec;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;I)Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readValue(Ljava/io/InputStream;)[B

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->readValue(Ljava/io/InputStream;)J

    move-result-wide v1

    new-instance p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;

    invoke-direct {p1, p2, v0, v1, v2}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;-><init>(I[BJ)V

    return-object p1
.end method

.method public write(Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;

    iget-object v0, p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;->fileHandle:[B

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    iget-wide v0, p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;->lineNumber:J

    invoke-static {v0, v1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->writeValue(JLjava/io/OutputStream;)V

    return-void
.end method
