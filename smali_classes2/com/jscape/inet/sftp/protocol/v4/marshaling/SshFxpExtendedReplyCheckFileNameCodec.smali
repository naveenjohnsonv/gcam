.class public Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCheckFileNameCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpExtendedReplyCodec$TypeCodec;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;I)Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedReply;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/util/X;->c(Ljava/io/InputStream;)[B

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedReplyCheckFileName;

    invoke-direct {v1, p2, v0, p1}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedReplyCheckFileName;-><init>(ILjava/lang/String;[B)V

    return-object v1
.end method

.method public write(Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedReply;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedReplyCheckFileName;

    iget-object v0, p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedReplyCheckFileName;->hashAlgorithmUsed:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUsAsciiValue(Ljava/lang/String;Ljava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedReplyCheckFileName;->data:[B

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method
