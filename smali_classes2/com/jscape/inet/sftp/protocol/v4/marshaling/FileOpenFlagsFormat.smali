.class public Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static format(Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;)I
    .locals 3

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_READ:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;->read:Z

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_WRITE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;->write:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_APPEND:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;->append:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_CREAT:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;->create:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_TRUNC:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;->truncate:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_EXCL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;->exclusively:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_TEXT:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    iget-boolean p0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;->text:Z

    invoke-virtual {v1, p0, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->set(ZI)I

    move-result p0

    return p0
.end method

.method public static parse(I)Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;
    .locals 9

    new-instance v8, Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_READ:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->presentIn(I)Z

    move-result v1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_WRITE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->presentIn(I)Z

    move-result v2

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_APPEND:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->presentIn(I)Z

    move-result v3

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_CREAT:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->presentIn(I)Z

    move-result v4

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_TRUNC:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->presentIn(I)Z

    move-result v5

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_EXCL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->presentIn(I)Z

    move-result v6

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_TEXT:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileOpenFlagsFormat$Mask;->presentIn(I)Z

    move-result v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;-><init>(ZZZZZZZ)V

    return-object v8
.end method
