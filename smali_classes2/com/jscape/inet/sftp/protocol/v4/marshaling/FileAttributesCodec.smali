.class public Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;
.super Ljava/lang/Object;


# static fields
.field private static b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "BzTkSb"

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;)I
    .locals 3

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_FILE_INHERIT_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->fileInheritAce:Z

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_DIRECTORY_INHERIT_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->directoryInheritAce:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_NO_PROPAGATE_INHERIT_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->noPropagateInheritAce:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_INHERIT_ONLY_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->inheritOnlyAce:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_SUCCESSFUL_ACCESS_ACE_FLAG:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->successfulAccessAceFlag:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_FAILED_ACCESS_ACE_FLAG:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->failedAccessAceFlag:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_IDENTIFIER_GROUP:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    iget-boolean p0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->identifierGroup:Z

    invoke-virtual {v1, p0, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->set(ZI)I

    move-result p0

    return p0
.end method

.method private static a(Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;)I
    .locals 3

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_READ_DATA:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->readData:Z

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_LIST_DIRECTORY:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->listDirectory:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_DATA:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->writeData:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_ADD_FILE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->addFile:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_APPEND_DATA:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->appendData:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_ADD_SUBDIRECTORY:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->addSubdirectory:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_READ_NAMED_ATTRS:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->readNamedAttrs:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_NAMED_ATTRS:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->writeNamedAttrs:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_EXECUTE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->execute:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_DELETE_CHILD:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->deleteChild:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_READ_ATTRIBUTES:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->readAttributes:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_ATTRIBUTES:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->writeAttributes:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_DELETE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->delete:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_READ_ACL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->readAcl:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_ACL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->writeAcl:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_OWNER:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->writeOwner:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_SYNCHRONIZE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    iget-boolean p0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->synchronize:Z

    invoke-virtual {v1, p0, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->set(ZI)I

    move-result p0

    return p0
.end method

.method private static a(I)Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;
    .locals 9

    new-instance v8, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_FILE_INHERIT_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->presentIn(I)Z

    move-result v1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_DIRECTORY_INHERIT_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->presentIn(I)Z

    move-result v2

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_NO_PROPAGATE_INHERIT_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->presentIn(I)Z

    move-result v3

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_INHERIT_ONLY_ACE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->presentIn(I)Z

    move-result v4

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_SUCCESSFUL_ACCESS_ACE_FLAG:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->presentIn(I)Z

    move-result v5

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_FAILED_ACCESS_ACE_FLAG:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->presentIn(I)Z

    move-result v6

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->ACE4_IDENTIFIER_GROUP:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclFlagMask;->presentIn(I)Z

    move-result v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;-><init>(ZZZZZZZ)V

    return-object v8
.end method

.method private static a(Lcom/jscape/util/h/e;)Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v1

    invoke-static {v1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(I)Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;

    move-result-object v1

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v2

    invoke-static {v2}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b(I)Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;

    move-result-object v2

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUtf8Value(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p0

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;

    invoke-direct {v3, v0, v1, v2, p0}, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;-><init>(ILcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;Ljava/lang/String;)V

    return-object v3
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private static a(Ljava/io/InputStream;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v0

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    :goto_0
    add-int/lit8 v3, v0, -0x1

    if-lez v0, :cond_1

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUtf8Value(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUtf8Value(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    if-nez v1, :cond_1

    :try_start_0
    invoke-interface {v2, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    move v0, v3

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_1
    return-object v2
.end method

.method private static a(Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;Lcom/jscape/util/h/o;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;->type:I

    invoke-static {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-object v0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;->flag:Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;)I

    move-result v0

    invoke-static {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-object v0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;->mask:Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;)I

    move-result v0

    invoke-static {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-object p0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;->who:Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V

    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method private static a([Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;)[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/o;

    invoke-direct {v0}, Lcom/jscape/util/h/o;-><init>()V

    array-length v1, p0

    invoke-static {v1, v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    array-length v1, p0

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v1, :cond_1

    aget-object v4, p0, v3

    invoke-static {v4, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;Lcom/jscape/util/h/o;)V

    add-int/lit8 v3, v3, 0x1

    if-eqz v2, :cond_0

    :cond_1
    invoke-virtual {v0}, Lcom/jscape/util/h/o;->d()[B

    move-result-object p0

    return-object p0
.end method

.method private static a([B)[Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/e;

    invoke-direct {v0, p0}, Lcom/jscape/util/h/e;-><init>([B)V

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v1

    new-array v2, v1, [Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v1, :cond_1

    if-nez p0, :cond_1

    :try_start_0
    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Lcom/jscape/util/h/e;)Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;

    move-result-object v4

    aput-object v4, v2, v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v3, v3, 0x1

    if-eqz p0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    return-object v2
.end method

.method private static b(I)Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;
    .locals 21

    move/from16 v0, p0

    new-instance v19, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;

    move-object/from16 v1, v19

    sget-object v2, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_READ_DATA:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v2, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v2

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_LIST_DIRECTORY:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v3, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v3

    sget-object v4, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_DATA:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v4, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v4

    sget-object v5, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_ADD_FILE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v5, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v5

    sget-object v6, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_APPEND_DATA:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v6, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v6

    sget-object v7, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_ADD_SUBDIRECTORY:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v7, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v7

    sget-object v8, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_READ_NAMED_ATTRS:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v8, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v8

    sget-object v9, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_NAMED_ATTRS:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v9, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v9

    sget-object v10, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_EXECUTE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v10, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v10

    sget-object v11, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_DELETE_CHILD:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v11, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v11

    sget-object v12, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_READ_ATTRIBUTES:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v12, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v12

    sget-object v13, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_ATTRIBUTES:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v13, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v13

    sget-object v14, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_DELETE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v14, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v14

    sget-object v15, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_READ_ACL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v15, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v15

    move-object/from16 v20, v1

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_ACL:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v1, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v16

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_WRITE_OWNER:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v1, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v17

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->ACE4_SYNCHRONIZE:Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;

    invoke-virtual {v1, v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec$AclMask;->presentIn(I)Z

    move-result v18

    move-object/from16 v1, v20

    invoke-direct/range {v1 .. v18}, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;-><init>(ZZZZZZZZZZZZZZZZZ)V

    return-object v19
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b:Ljava/lang/String;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b:Ljava/lang/String;

    return-void
.end method

.method public static readValue(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat;->parse(I)Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static/range {p0 .. p0}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result v2

    and-int/lit16 v4, v2, 0xff

    :try_start_0
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->sizeAttributePresent:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    invoke-static/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->readValue(Ljava/io/InputStream;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_12

    move-object v5, v2

    goto :goto_0

    :cond_0
    move-object v5, v3

    :goto_0
    :try_start_1
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->ownerGroupAttributesPresent:Z

    if-eqz v2, :cond_1

    invoke-static/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUtf8Value(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_11

    move-object v6, v2

    goto :goto_1

    :cond_1
    move-object v6, v3

    :goto_1
    :try_start_2
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->ownerGroupAttributesPresent:Z

    if-eqz v2, :cond_2

    invoke-static/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUtf8Value(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_10

    move-object v7, v2

    goto :goto_2

    :cond_2
    move-object v7, v3

    :goto_2
    :try_start_3
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->permissionsAttributePresent:Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_e

    if-nez v1, :cond_4

    if-eqz v2, :cond_3

    :try_start_4
    invoke-static/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_f

    goto :goto_3

    :cond_3
    move-object v8, v3

    goto :goto_4

    :cond_4
    :goto_3
    invoke-static {v2}, Lcom/jscape/util/e/a;->a(I)Lcom/jscape/util/e/PosixFilePermissions;

    move-result-object v2

    move-object v8, v2

    :goto_4
    :try_start_5
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->accessTimeAttributePresent:Z

    if-eqz v2, :cond_5

    invoke-static/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->readValue(Ljava/io/InputStream;)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_d

    move-object v9, v2

    goto :goto_5

    :cond_5
    move-object v9, v3

    :goto_5
    :try_start_6
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->accessTimeAttributePresent:Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_b

    if-nez v1, :cond_6

    if-eqz v2, :cond_7

    :try_start_7
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->subsecondTimesPresent:Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_c

    :cond_6
    if-nez v1, :cond_8

    if-eqz v2, :cond_7

    :try_start_8
    invoke-static/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v2
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_6

    :catch_0
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_7
    move-object v10, v3

    goto :goto_7

    :cond_8
    :goto_6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object v10, v2

    :goto_7
    :try_start_9
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->createTimeAttributePresent:Z

    if-eqz v2, :cond_9

    invoke-static/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->readValue(Ljava/io/InputStream;)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_a

    move-object v11, v2

    goto :goto_8

    :cond_9
    move-object v11, v3

    :goto_8
    :try_start_a
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->createTimeAttributePresent:Z
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    if-nez v1, :cond_a

    if-eqz v2, :cond_b

    :try_start_b
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->subsecondTimesPresent:Z
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_9

    :cond_a
    if-nez v1, :cond_c

    if-eqz v2, :cond_b

    :try_start_c
    invoke-static/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v2
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1

    goto :goto_9

    :catch_1
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_b
    move-object v12, v3

    goto :goto_a

    :cond_c
    :goto_9
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object v12, v2

    :goto_a
    :try_start_d
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->modifyTimeAttributePresent:Z

    if-eqz v2, :cond_d

    invoke-static/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->readValue(Ljava/io/InputStream;)J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_7

    move-object v13, v2

    goto :goto_b

    :cond_d
    move-object v13, v3

    :goto_b
    :try_start_e
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->modifyTimeAttributePresent:Z
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5

    if-nez v1, :cond_e

    if-eqz v2, :cond_f

    :try_start_f
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->subsecondTimesPresent:Z
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_6

    :cond_e
    if-nez v1, :cond_10

    if-eqz v2, :cond_f

    :try_start_10
    invoke-static/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v2
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_2

    goto :goto_c

    :catch_2
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_f
    move-object v14, v3

    goto :goto_d

    :cond_10
    :goto_c
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object v14, v2

    :goto_d
    :try_start_11
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->aclAttributePresent:Z

    if-eqz v2, :cond_11

    invoke-static/range {p0 .. p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readValue(Ljava/io/InputStream;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a([B)[Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;

    move-result-object v2
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_4

    move-object v15, v2

    goto :goto_e

    :cond_11
    move-object v15, v3

    :goto_e
    :try_start_12
    iget-boolean v0, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->extendedAttributesPresent:Z

    if-eqz v0, :cond_12

    invoke-static/range {p0 .. p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/InputStream;)Ljava/util/Map;

    move-result-object v0
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_3

    goto :goto_f

    :cond_12
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    :goto_f
    move-object/from16 v16, v0

    new-instance v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;

    move-object v3, v0

    invoke-direct/range {v3 .. v16}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;-><init>(ILjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;[Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;Ljava/util/Map;)V

    if-eqz v1, :cond_13

    const/4 v1, 0x5

    new-array v1, v1, [I

    invoke-static {v1}, Lcom/jscape/util/aq;->b([I)V

    :cond_13
    return-object v0

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :catch_5
    move-exception v0

    move-object v1, v0

    :try_start_13
    invoke-static {v1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_6

    :catch_6
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :catch_7
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :catch_8
    move-exception v0

    move-object v1, v0

    :try_start_14
    invoke-static {v1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_9

    :catch_9
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :catch_a
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :catch_b
    move-exception v0

    move-object v1, v0

    :try_start_15
    invoke-static {v1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_c

    :catch_c
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :catch_d
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :catch_e
    move-exception v0

    move-object v1, v0

    :try_start_16
    invoke-static {v1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_f

    :catch_f
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :catch_10
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :catch_11
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :catch_12
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method public static writeValue(Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;Ljava/io/OutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->flagsFor(Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;)Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributeFlagsFormat;->format(Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;)I

    move-result v2

    invoke-static {v2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->type:I

    int-to-byte v2, v2

    invoke-static {v2, p1}, Lcom/jscape/util/h/a/c;->a(BLjava/io/OutputStream;)V

    :try_start_0
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->sizeAttributePresent:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7

    if-nez v1, :cond_1

    if-eqz v2, :cond_0

    :try_start_1
    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->size:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->writeValue(JLjava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_8

    :cond_0
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->ownerGroupAttributesPresent:Z

    :cond_1
    if-nez v1, :cond_3

    if-eqz v2, :cond_2

    :try_start_2
    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->owner:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->group:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_2
    :goto_0
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->permissionsAttributePresent:Z

    :cond_3
    if-nez v1, :cond_5

    if-eqz v2, :cond_4

    :try_start_3
    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    invoke-static {v2}, Lcom/jscape/util/e/a;->a(Lcom/jscape/util/e/PosixFilePermissions;)I

    move-result v2

    invoke-static {v2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_4
    :goto_1
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->accessTimeAttributePresent:Z

    :cond_5
    if-nez v1, :cond_7

    if-eqz v2, :cond_6

    :try_start_4
    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->accessTime:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->writeValue(JLjava/io/OutputStream;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_6
    :goto_2
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->createTimeAttributePresent:Z

    :cond_7
    if-nez v1, :cond_9

    if-eqz v2, :cond_8

    :try_start_5
    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->createTime:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->writeValue(JLjava/io/OutputStream;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_3

    :catch_3
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_8
    :goto_3
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->modifyTimeAttributePresent:Z

    :cond_9
    if-nez v1, :cond_b

    if-eqz v2, :cond_a

    :try_start_6
    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->modificationTime:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->writeValue(JLjava/io/OutputStream;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_4

    :catch_4
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_a
    :goto_4
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->aclAttributePresent:Z

    :cond_b
    if-nez v1, :cond_d

    if-eqz v2, :cond_c

    :try_start_7
    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->acls:[Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;

    invoke-static {v1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a([Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;)[B

    move-result-object v1

    invoke-static {v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    goto :goto_5

    :catch_5
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_c
    :goto_5
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->extendedAttributesPresent:Z

    :cond_d
    if-eqz v2, :cond_e

    :try_start_8
    iget-object p0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->extendedData:Ljava/util/Map;

    invoke-static {p0, p1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/util/Map;Ljava/io/OutputStream;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_6

    :catch_6
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_e
    :goto_6
    return-void

    :catch_7
    move-exception p0

    :try_start_9
    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8

    :catch_8
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method
