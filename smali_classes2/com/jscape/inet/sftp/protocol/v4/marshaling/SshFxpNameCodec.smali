.class public Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpNameCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/sftp/protocol/messages/Message;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a([Lcom/jscape/inet/sftp/protocol/v4/messages/NameEntry;Ljava/io/OutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    array-length v0, p1

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object v0

    array-length v1, p1

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v1, :cond_1

    aget-object v3, p1, v2

    iget-object v4, v3, Lcom/jscape/inet/sftp/protocol/v4/messages/NameEntry;->filename:Ljava/lang/String;

    invoke-static {v4, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V

    iget-object v3, v3, Lcom/jscape/inet/sftp/protocol/v4/messages/NameEntry;->attributes:Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;

    invoke-static {v3, p2}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->writeValue(Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;Ljava/io/OutputStream;)V

    add-int/lit8 v2, v2, 0x1

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method private a(Ljava/io/InputStream;)[Lcom/jscape/inet/sftp/protocol/v4/messages/NameEntry;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v0

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->b()Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Lcom/jscape/inet/sftp/protocol/v4/messages/NameEntry;

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v0, :cond_1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUtf8Value(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/FileAttributesCodec;->readValue(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;

    move-result-object v5

    if-nez v1, :cond_1

    :try_start_0
    new-instance v6, Lcom/jscape/inet/sftp/protocol/v4/messages/NameEntry;

    invoke-direct {v6, v4, v5}, Lcom/jscape/inet/sftp/protocol/v4/messages/NameEntry;-><init>(Ljava/lang/String;Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;)V

    aput-object v6, v2, v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v3, v3, 0x1

    if-eqz v1, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpNameCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v2
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/messages/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v0

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpNameCodec;->a(Ljava/io/InputStream;)[Lcom/jscape/inet/sftp/protocol/v4/messages/NameEntry;

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpName;

    invoke-direct {v1, v0, p1}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpName;-><init>(I[Lcom/jscape/inet/sftp/protocol/v4/messages/NameEntry;)V

    return-object v1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpNameCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/jscape/inet/sftp/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpName;

    iget v0, p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpName;->id:I

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpName;->entries:[Lcom/jscape/inet/sftp/protocol/v4/messages/NameEntry;

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpNameCodec;->a([Lcom/jscape/inet/sftp/protocol/v4/messages/NameEntry;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/sftp/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v4/marshaling/SshFxpNameCodec;->write(Lcom/jscape/inet/sftp/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
