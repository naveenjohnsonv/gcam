.class public Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;
.super Ljava/lang/Object;


# static fields
.field public static final TIME_FACTOR:J = 0x3e8L

.field private static final a:[Ljava/lang/String;


# instance fields
.field public final accessTime:Ljava/lang/Long;

.field public final accessTimeNanos:Ljava/lang/Integer;

.field public final acls:[Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;

.field public final createTime:Ljava/lang/Long;

.field public final createTimeNanos:Ljava/lang/Integer;

.field public final extendedData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final group:Ljava/lang/String;

.field public final modificationTime:Ljava/lang/Long;

.field public final modificationTimeNanos:Ljava/lang/Integer;

.field public final owner:Ljava/lang/String;

.field public final permissions:Lcom/jscape/util/e/PosixFilePermissions;

.field public final size:Ljava/lang/Long;

.field public final type:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "\u0000\u0003\u0008E2b_E@\u0004^?dWxJ\u0008Ok\u000f\u0000\u0003\u0000R\"nWHF\u0001n7\u007fX\u0011\u0007\u0000\u0003\u0004I:x\u0004\t\u0000\u0003\u0002X9~I\u0011\u0004\u0012\u0000\u0003\u0006X3jMIw\u000cG3EXBL\u0016\u0017\r\u0000\u0003\u0006X3jMIw\u000cG36\r\u0000\u0003\u0004I5nJ_w\u000cG36\u000e\u0000\u0003\u0015O$fP_P\u000cE8x\u0004\u0012\u0000\u0003\u0004I5nJ_w\u000cG3EXBL\u0016\u0017\u0015jJ\tO\u0017\u007fM^J\u0007_\"nJ\u000cX\u0011S&n\u0004\u0007\u0000\u0003\u0016C,n\u0004"

    const/16 v4, 0xa4

    const/16 v5, 0x13

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x14

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x22

    const/16 v3, 0x9

    const-string v5, "\u0008\u000b\u0002U0fC\u0019\u000c\u0018\u0008\u000b\u0000M:jWMH\u000cV7l_pB\u0000G\u0010b_KXP"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x1c

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x2d

    goto :goto_4

    :cond_4
    const/16 v1, 0x1f

    goto :goto_4

    :cond_5
    const/16 v1, 0x42

    goto :goto_4

    :cond_6
    const/16 v1, 0x3e

    goto :goto_4

    :cond_7
    const/16 v1, 0x71

    goto :goto_4

    :cond_8
    const/16 v1, 0x37

    goto :goto_4

    :cond_9
    const/16 v1, 0x38

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(ILjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;[Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/jscape/util/e/PosixFilePermissions;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "[",
            "Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput p1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->type:I

    iput-object p2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->size:Ljava/lang/Long;

    iput-object p3, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->owner:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->group:Ljava/lang/String;

    iput-object p5, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    iput-object p6, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->accessTime:Ljava/lang/Long;

    iput-object p7, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->accessTimeNanos:Ljava/lang/Integer;

    iput-object p8, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->createTime:Ljava/lang/Long;

    iput-object p9, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->createTimeNanos:Ljava/lang/Integer;

    iput-object p10, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->modificationTime:Ljava/lang/Long;

    iput-object p11, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->modificationTimeNanos:Ljava/lang/Integer;

    iput-object p12, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->acls:[Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;

    invoke-static {p13}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p13, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->extendedData:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;[Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;Ljava/util/Map;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/jscape/util/e/PosixFilePermissions;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "[",
            "Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    move-object v0, p1

    iget v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->code:I

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v8, p7

    move-object/from16 v10, p8

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    invoke-direct/range {v0 .. v13}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;-><init>(ILjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;[Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;Ljava/util/Map;)V

    return-void
.end method

.method public static attributesFor(J)Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;
    .locals 12

    new-instance v11, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_UNKNOWN:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    const-wide/16 v2, 0x3e8

    div-long/2addr p0, v2

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;-><init>(Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;[Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;Ljava/util/Map;)V

    return-object v11
.end method

.method public static attributesFor(Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;Lcom/jscape/util/e/PosixFilePermissions;)Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;
    .locals 12

    new-instance v11, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;

    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, v11

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v10}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;-><init>(Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;[Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;Ljava/util/Map;)V

    return-object v11
.end method

.method public static attributesFor(Lcom/jscape/util/e/PosixFilePermissions;)Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_UNKNOWN:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    invoke-static {v0, p0}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->attributesFor(Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;Lcom/jscape/util/e/PosixFilePermissions;)Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;

    move-result-object p0

    return-object p0
.end method

.method public static empty()Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;
    .locals 12

    new-instance v11, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_UNKNOWN:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;-><init>(Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;[Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;Ljava/util/Map;)V

    return-object v11
.end method


# virtual methods
.method public accessTime()J
    .locals 4

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->accessTime:Ljava/lang/Long;

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v0

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    :goto_1
    return-wide v0
.end method

.method public createTime()J
    .locals 4

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->createTime:Ljava/lang/Long;

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v0

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    :goto_1
    return-wide v0
.end method

.method public modificationTime()J
    .locals 4

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->modificationTime:Ljava/lang/Long;

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v0

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    :goto_1
    return-wide v0
.end method

.method public readable()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    iget-boolean v1, v1, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanRead:Z

    if-nez v0, :cond_2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public size()J
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->size:Ljava/lang/Long;

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_1
    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended;->a()[Lcom/jscape/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->a:[Ljava/lang/String;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->type:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v2, 0xa

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->size:Ljava/lang/Long;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0xb

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->owner:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x3

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->group:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->accessTime:Ljava/lang/Long;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->accessTimeNanos:Ljava/lang/Integer;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->createTime:Ljava/lang/Long;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->createTimeNanos:Ljava/lang/Integer;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->modificationTime:Ljava/lang/Long;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0xc

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->modificationTimeNanos:Ljava/lang/Integer;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->acls:[Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;

    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->extendedData:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v1

    if-nez v1, :cond_0

    new-array v1, v2, [Lcom/jscape/util/aq;

    invoke-static {v1}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended;->b([Lcom/jscape/util/aq;)V

    :cond_0
    return-object v0
.end method

.method public type()Lcom/jscape/util/e/UnixFileType;
    .locals 1

    iget v0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->type:I

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->unixFileTypeFor(I)Lcom/jscape/util/e/UnixFileType;

    move-result-object v0

    return-object v0
.end method

.method public writable()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    iget-boolean v1, v1, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanWrite:Z

    if-nez v0, :cond_2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method
