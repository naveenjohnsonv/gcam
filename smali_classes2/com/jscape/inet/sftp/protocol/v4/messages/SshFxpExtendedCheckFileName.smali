.class public Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;
.super Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended<",
        "Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field public final blockSize:I

.field public final hashAlgorithmList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final length:J

.field public final name:Ljava/lang/String;

.field public final startOffset:J


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "sd\u0017tL\u0014\u0008x\u000csd\u001byN\u0012^\u000c-\u0003p\u001c\u000esd\na@\u0003A\u0010\"\u001ffD\u0005\u0008\u0014sd\u0011tR\u0019t3#\u0016gH\u0005]2\u0008\u0010fUL"

    const/16 v4, 0x39

    const/16 v5, 0x8

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x6e

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    const/16 v14, 0x2a

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v3, 0x9

    const-string v4, "\u0007\u0010a\u0004;b5C\r xCe\'-u\u0004SDh\u000f1`%hXh\u0002>C(GUC\u00008`aPYi\\"

    move v5, v3

    move-object v3, v4

    move v7, v10

    move v4, v14

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x1a

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;->c:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v10, v13

    rem-int/lit8 v1, v13, 0x7

    if-eqz v1, :cond_8

    if-eq v1, v9, :cond_9

    const/4 v14, 0x2

    if-eq v1, v14, :cond_7

    const/4 v14, 0x3

    if-eq v1, v14, :cond_6

    const/4 v14, 0x4

    if-eq v1, v14, :cond_5

    const/4 v14, 0x5

    if-eq v1, v14, :cond_4

    const/16 v14, 0x5b

    goto :goto_4

    :cond_4
    const/16 v14, 0x1f

    goto :goto_4

    :cond_5
    const/16 v14, 0x4f

    goto :goto_4

    :cond_6
    const/16 v14, 0x7b

    goto :goto_4

    :cond_7
    const/16 v14, 0x17

    goto :goto_4

    :cond_8
    const/16 v14, 0x31

    :cond_9
    :goto_4
    xor-int v1, v8, v14

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/util/List;JJI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;JJI)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended;-><init>(I)V

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;->name:Ljava/lang/String;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;->hashAlgorithmList:Ljava/util/List;

    iput-wide p4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;->startOffset:J

    iput-wide p6, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;->length:J

    iput p8, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;->blockSize:I

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/sftp/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;->accept(Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/sftp/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName$Handler;->handle(Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;->c:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;->id:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;->hashAlgorithmList:Ljava/util/List;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;->startOffset:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;->length:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedCheckFileName;->blockSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
