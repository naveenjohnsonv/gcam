.class public Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;
.super Lcom/jscape/inet/sftp/protocol/messages/Message;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/sftp/protocol/messages/Message<",
        "Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final code:I

.field public final id:I

.field public final languageTag:Ljava/lang/String;

.field public final message:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x7

    const-string v5, ",n#0&qO\u0011S=(\u0019:d!t/4*14\ti*}"

    const/16 v6, 0x19

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/4 v9, 0x1

    add-int/2addr v7, v9

    add-int v10, v7, v2

    invoke-virtual {v5, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0x25

    move v13, v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v14, v10

    const/4 v15, 0x0

    :goto_2
    if-gt v14, v15, :cond_3

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v13}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v12, :cond_1

    add-int/lit8 v12, v8, 0x1

    aput-object v10, v1, v8

    add-int/2addr v7, v2

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v12

    goto :goto_0

    :cond_0
    const/16 v6, 0x1b

    const/16 v2, 0xf

    const-string v5, "f$ftf9M+coAi9\u0005m\u000bf$gp{-Y-a72"

    move v8, v12

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v8, 0x1

    aput-object v10, v1, v8

    add-int/2addr v7, v2

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v12

    :goto_3
    const/16 v13, 0x6f

    add-int/2addr v7, v9

    add-int v10, v7, v2

    invoke-virtual {v5, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v10, v15

    rem-int/lit8 v3, v15, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v9, :cond_8

    const/4 v4, 0x2

    if-eq v3, v4, :cond_7

    const/4 v4, 0x3

    if-eq v3, v4, :cond_6

    if-eq v3, v0, :cond_5

    const/4 v4, 0x5

    if-eq v3, v4, :cond_4

    const/16 v3, 0x57

    goto :goto_4

    :cond_4
    const/16 v3, 0x31

    goto :goto_4

    :cond_5
    const/16 v3, 0x67

    goto :goto_4

    :cond_6
    const/16 v3, 0x7a

    goto :goto_4

    :cond_7
    const/16 v3, 0x65

    goto :goto_4

    :cond_8
    const/16 v3, 0x6b

    goto :goto_4

    :cond_9
    move v3, v11

    :goto_4
    xor-int/2addr v3, v13

    xor-int v3, v16, v3

    int-to-char v3, v3

    aput-char v3, v10, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/sftp/protocol/messages/Message;-><init>()V

    iput p1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;->id:I

    iput p2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;->code:I

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;->message:Ljava/lang/String;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;->languageTag:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iget p2, p2, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->code:I

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;-><init>(IILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/sftp/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;->accept(Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/sftp/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus$Handler;->handle(Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;->id:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;->code:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;->message:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;->languageTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
