.class public Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final directoryInheritAce:Z

.field public final failedAccessAceFlag:Z

.field public final fileInheritAce:Z

.field public final identifierGroup:Z

.field public final inheritOnlyAce:Z

.field public final noPropagateInheritAce:Z

.field public final successfulAccessAceFlag:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "NHo&ou\u0005\u000b\u000eo\'x\\\u0003\r\u001dv\u007f\u001aNHu7ix\u0014\u0011\u001b`7fZ\u0012\u0001\ru1Kx\u0014$\u0004g%7\u0011NHo,b~\u0003\u000b\u001cI,fb0\u0001\r;\u0015$\u0004g%*`\u0017\u000b\u0004c\u000bds\u0014\u0010\u0001r\u0003i~L\u0016NH`#cw\u0014\u0006)e!oh\u0002#\u000bc\u0004fz\u0016_"

    const/16 v4, 0x6c

    const/16 v5, 0x12

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x5c

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x2

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v13, v10

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v12, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x2f

    const/16 v3, 0x18

    const-string v5, "\u0010\u00166s\u00047@LW?}  fR^=n=1n_Se\u0016\u0010\u0016<u& LHY*e\u001d+GYD1h\u0015&J\u0001"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    add-int/2addr v6, v9

    add-int v8, v6, v5

    invoke-virtual {v3, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v8, v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v10, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v9, :cond_8

    if-eq v1, v11, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v1, 0x2d

    goto :goto_4

    :cond_4
    const/16 v1, 0x47

    goto :goto_4

    :cond_5
    const/16 v1, 0x56

    goto :goto_4

    :cond_6
    const/16 v1, 0x1e

    goto :goto_4

    :cond_7
    const/16 v1, 0x5a

    goto :goto_4

    :cond_8
    const/16 v1, 0x34

    goto :goto_4

    :cond_9
    const/16 v1, 0x3e

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v10, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(ZZZZZZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->fileInheritAce:Z

    iput-boolean p2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->directoryInheritAce:Z

    iput-boolean p3, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->noPropagateInheritAce:Z

    iput-boolean p4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->inheritOnlyAce:Z

    iput-boolean p5, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->successfulAccessAceFlag:Z

    iput-boolean p6, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->failedAccessAceFlag:Z

    iput-boolean p7, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->identifierGroup:Z

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->a:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->fileInheritAce:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->directoryInheritAce:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->noPropagateInheritAce:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->inheritOnlyAce:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->successfulAccessAceFlag:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->failedAccessAceFlag:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;->identifierGroup:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
