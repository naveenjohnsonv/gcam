.class public Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final addFile:Z

.field public final addSubdirectory:Z

.field public final appendData:Z

.field public final delete:Z

.field public final deleteChild:Z

.field public final execute:Z

.field public final listDirectory:Z

.field public final readAcl:Z

.field public final readAttributes:Z

.field public final readData:Z

.field public final readNamedAttrs:Z

.field public final synchronize:Z

.field public final writeAcl:Z

.field public final writeAttributes:Z

.field public final writeData:Z

.field public final writeNamedAttrs:Z

.field public final writeOwner:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "{C\'\u0002\u000cf~\u0016\u0017$\u0002\u000cpn#\u0006#M\u0012{C1\u0014\u0001An5\u00079\u0002\u0000qo8\u0011)M\n{C1\u0014\u0001Tr;\u0006m\u0011{C\"\u0015\u0004vZ#\u0017\"\u0019\u0007go2\u0010m\r{C\'\u0002\u000cf~\u0018\u0014>\u0015\u0017/\u000b{C\'\u0002\u000cf~\u0016\u0000<M\u0010{C<\u0019\u0016f_>\u00115\u0013\u0011}i.^\u0011{C\"\u0015\u0004vU6\u000e5\u0014$fo%\u0010m\r{C1\u0000\u0015wu3\'1\u0004\u0004/\u000e{C#\t\u000bqs%\u000c>\u0019\u001fw&\n{C5\u0008\u0000qn#\u0006m\t{C4\u0015\two2^\u0012{C\'\u0002\u000cf~\u0019\u0002=\u0015\u0001So#\u0011#M\u000c{C\'\u0002\u000cf~\u0013\u0002$\u0011X\u000e{C4\u0015\two2 8\u0019\tv&"

    const/16 v4, 0xe0

    const/16 v5, 0x12

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x3a

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x1a

    const/16 v3, 0xf

    const-string v5, "umLt*\u0006\u0006]m[[k\t\u0015\u0005\n\u0014,Mzk\u00195[`\u0002"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x55

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x21

    goto :goto_4

    :cond_4
    const/16 v1, 0x28

    goto :goto_4

    :cond_5
    const/16 v1, 0x5f

    goto :goto_4

    :cond_6
    const/16 v1, 0x4a

    goto :goto_4

    :cond_7
    const/16 v1, 0x6a

    goto :goto_4

    :cond_8
    const/16 v1, 0x59

    goto :goto_4

    :cond_9
    const/16 v1, 0x6d

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(ZZZZZZZZZZZZZZZZZ)V
    .locals 2

    move-object v0, p0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move v1, p1

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->readData:Z

    move v1, p2

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->listDirectory:Z

    move v1, p3

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->writeData:Z

    move v1, p4

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->addFile:Z

    move v1, p5

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->appendData:Z

    move v1, p6

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->addSubdirectory:Z

    move v1, p7

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->readNamedAttrs:Z

    move v1, p8

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->writeNamedAttrs:Z

    move v1, p9

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->execute:Z

    move v1, p10

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->deleteChild:Z

    move v1, p11

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->readAttributes:Z

    move v1, p12

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->writeAttributes:Z

    move v1, p13

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->delete:Z

    move/from16 v1, p14

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->readAcl:Z

    move/from16 v1, p15

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->writeAcl:Z

    move/from16 v1, p16

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->writeOwner:Z

    move/from16 v1, p17

    iput-boolean v1, v0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->synchronize:Z

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->a:[Ljava/lang/String;

    const/16 v3, 0xf

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->readData:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v3, 0x6

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->listDirectory:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v3, 0xd

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->writeData:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->addFile:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v3, 0x8

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->appendData:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v4, v2, v3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->addSubdirectory:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v4, 0x7

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->readNamedAttrs:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v4, 0xc

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->writeNamedAttrs:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v4, 0xa

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->execute:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v4, 0xe

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->deleteChild:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v4, 0x3

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->readAttributes:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->writeAttributes:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v4, 0xb

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->delete:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v4, 0x10

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->readAcl:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v4, 0x5

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->writeAcl:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v4, 0x4

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->writeOwner:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v4, 0x9

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;->synchronize:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    new-array v0, v3, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object v1
.end method
