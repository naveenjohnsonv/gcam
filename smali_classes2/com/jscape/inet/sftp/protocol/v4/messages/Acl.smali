.class public Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final flag:Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;

.field public final mask:Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;

.field public final type:I

.field public final who:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x7

    const/4 v4, 0x0

    const-string v5, "\u00003A#e8T\u0007\u00003P\'kbN"

    const/16 v6, 0xf

    move v8, v4

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x19

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v2

    invoke-virtual {v5, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v4

    :goto_2
    const/16 v15, 0xa

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v2

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v6, 0x12

    const-string v5, "\\AkZ@@;mG:\u00071\u0002j\u001bH_\u007f"

    move v8, v11

    move v2, v15

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v2

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v11

    :goto_3
    const/16 v9, 0x28

    add-int/2addr v7, v10

    add-int v11, v7, v2

    invoke-virtual {v5, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v4

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v3, v14, 0x7

    if-eqz v3, :cond_8

    if-eq v3, v10, :cond_9

    const/4 v15, 0x2

    if-eq v3, v15, :cond_7

    const/4 v15, 0x3

    if-eq v3, v15, :cond_6

    if-eq v3, v0, :cond_5

    const/4 v15, 0x5

    if-eq v3, v15, :cond_4

    const/16 v15, 0x6a

    goto :goto_4

    :cond_4
    const/16 v15, 0x1c

    goto :goto_4

    :cond_5
    const/16 v15, 0x13

    goto :goto_4

    :cond_6
    const/16 v15, 0x52

    goto :goto_4

    :cond_7
    const/16 v15, 0x2f

    goto :goto_4

    :cond_8
    const/16 v15, 0x35

    :cond_9
    :goto_4
    xor-int v3, v9, v15

    xor-int v3, v16, v3

    int-to-char v3, v3

    aput-char v3, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(ILcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;->type:I

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;->flag:Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;->mask:Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;->who:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/jscape/inet/sftp/protocol/v4/messages/AceType;Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;Ljava/lang/String;)V
    .locals 0

    iget p1, p1, Lcom/jscape/inet/sftp/protocol/v4/messages/AceType;->code:I

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;-><init>(ILcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;->a:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;->type:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;->flag:Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Flag;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;->mask:Lcom/jscape/inet/sftp/protocol/v4/messages/Acl$Mask;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;->who:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
