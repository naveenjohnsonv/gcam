.class public Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final accessTimeAttributePresent:Z

.field public final aclAttributePresent:Z

.field public final createTimeAttributePresent:Z

.field public final extendedAttributesPresent:Z

.field public final modifyTimeAttributePresent:Z

.field public final ownerGroupAttributesPresent:Z

.field public final permissionsAttributePresent:Z

.field public final sizeAttributePresent:Z

.field public final subsecondTimesPresent:Z


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "]}=\u0015}t>6/=\u0017cP8\u0005/;\u0000fe)\u0002\r \u0007`t\"\u0005`\u001d]}1\u0010vp8\u0014\t;\u000fvP8\u0005/;\u0000fe)!/7\u0011v\u007f8L\u0018]}!\u0017qb)\u00122<\u0006Gx!\u0014.\u0002\u0010vb)\u001f)o\u001d]}?\rwx*\u0008\t;\u000fvP8\u0005/;\u0000fe)!/7\u0011v\u007f8L\u001d]}3\u0001pt?\u0002\t;\u000fvP8\u0005/;\u0000fe)!/7\u0011v\u007f8L\u001c]}7\u001agt\"\u001586#ge>\u0018?\'\u0016vb\u001c\u00038!\u0007}eq\u0016]}3\u0001\u007fP8\u0005/;\u0000fe)!/7\u0011v\u007f8L"

    const/16 v5, 0xc5

    move v8, v3

    const/4 v6, -0x1

    const/16 v7, 0x1e

    :goto_0
    const/16 v9, 0x68

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x48

    const-string v4, ".\u000eQt\u0012\u000fVq]H~\u000e\u0011~vZSx\u0002\u0017Kg~St\u0013\u0007Qv\u0013)DGMt!\u0016KpGCd\u0014\u0007ynOFb@\u0019LkTDP\u0014\u0016MkLTe\u00052Mg]D\u007f\u0014_"

    move v8, v11

    const/4 v6, -0x1

    const/16 v7, 0x1e

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x1b

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v10, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v1, 0x24

    goto :goto_4

    :cond_4
    const/16 v1, 0x79

    goto :goto_4

    :cond_5
    const/16 v1, 0x7b

    goto :goto_4

    :cond_6
    const/16 v1, 0xa

    goto :goto_4

    :cond_7
    const/16 v1, 0x3a

    goto :goto_4

    :cond_8
    const/16 v1, 0x35

    goto :goto_4

    :cond_9
    const/16 v1, 0x19

    :goto_4
    xor-int/2addr v1, v9

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(ZZZZZZZZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->sizeAttributePresent:Z

    iput-boolean p2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->permissionsAttributePresent:Z

    iput-boolean p3, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->accessTimeAttributePresent:Z

    iput-boolean p4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->createTimeAttributePresent:Z

    iput-boolean p5, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->modifyTimeAttributePresent:Z

    iput-boolean p6, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->aclAttributePresent:Z

    iput-boolean p7, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->ownerGroupAttributesPresent:Z

    iput-boolean p8, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->subsecondTimesPresent:Z

    iput-boolean p9, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->extendedAttributesPresent:Z

    return-void
.end method

.method public static empty()Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;
    .locals 11

    new-instance v10, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;-><init>(ZZZZZZZZZ)V

    return-object v10
.end method

.method public static flagsFor(Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;)Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;
    .locals 12

    new-instance v10, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;

    iget-object v0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->size:Ljava/lang/Long;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    move v3, v2

    goto :goto_0

    :cond_0
    move v3, v1

    :goto_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    if-eqz v0, :cond_1

    move v4, v2

    goto :goto_1

    :cond_1
    move v4, v1

    :goto_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->accessTime:Ljava/lang/Long;

    if-eqz v0, :cond_2

    move v5, v2

    goto :goto_2

    :cond_2
    move v5, v1

    :goto_2
    iget-object v0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->createTime:Ljava/lang/Long;

    if-eqz v0, :cond_3

    move v6, v2

    goto :goto_3

    :cond_3
    move v6, v1

    :goto_3
    iget-object v0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->modificationTime:Ljava/lang/Long;

    if-eqz v0, :cond_4

    move v7, v2

    goto :goto_4

    :cond_4
    move v7, v1

    :goto_4
    iget-object v0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->acls:[Lcom/jscape/inet/sftp/protocol/v4/messages/Acl;

    if-eqz v0, :cond_5

    move v8, v2

    goto :goto_5

    :cond_5
    move v8, v1

    :goto_5
    iget-object v0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->owner:Ljava/lang/String;

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->group:Ljava/lang/String;

    if-eqz v0, :cond_6

    goto :goto_6

    :cond_6
    move v9, v1

    goto :goto_7

    :cond_7
    :goto_6
    move v9, v2

    :goto_7
    const/4 v11, 0x0

    iget-object p0, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->extendedData:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result p0

    xor-int/2addr p0, v2

    move-object v0, v10

    move v1, v3

    move v2, v4

    move v3, v5

    move v4, v6

    move v5, v7

    move v6, v8

    move v7, v9

    move v8, v11

    move v9, p0

    invoke-direct/range {v0 .. v9}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;-><init>(ZZZZZZZZZ)V

    return-object v10
.end method

.method public static modificationTime()Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;
    .locals 11

    new-instance v10, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;-><init>(ZZZZZZZZZ)V

    return-object v10
.end method

.method public static permissions()Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;
    .locals 11

    new-instance v10, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;-><init>(ZZZZZZZZZ)V

    return-object v10
.end method

.method public static size()Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;
    .locals 11

    new-instance v10, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, v10

    invoke-direct/range {v0 .. v9}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;-><init>(ZZZZZZZZZ)V

    return-object v10
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->a:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->sizeAttributePresent:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->permissionsAttributePresent:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->accessTimeAttributePresent:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->createTimeAttributePresent:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->modifyTimeAttributePresent:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->aclAttributePresent:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->ownerGroupAttributesPresent:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->subsecondTimesPresent:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributeFlags;->extendedAttributesPresent:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
