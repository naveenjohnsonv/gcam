.class public final enum Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum SSH_FX_BAD_MESSAGE:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

.field public static final enum SSH_FX_EOF:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

.field public static final enum SSH_FX_FAILURE:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

.field public static final enum SSH_FX_FILE_ALREADY_EXISTS:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

.field public static final enum SSH_FX_INVALID_HANDLE:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

.field public static final enum SSH_FX_NO_SUCH_FILE:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

.field public static final enum SSH_FX_NO_SUCH_PATH:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

.field public static final enum SSH_FX_OK:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

.field public static final enum SSH_FX_OP_UNSUPPORTED:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

.field public static final enum SSH_FX_PERMISSION_DENIED:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

.field public static final enum SSH_FX_WRITE_PROTECT:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

.field private static final synthetic a:[Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;


# instance fields
.field public final code:I


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0xb

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "\u0003\r\u0006kb4X\u0016\u001f\u0007xq>B\t\u0003\r\u0006kb4X\u001f\u0015\u0012\u0003\r\u0006kb4X\u0012\u001f\nki)T\u0003\u001f\tq\u0018\u0003\r\u0006kb4X\u0000\u001b\u001cym?T\u0019\u0011\u0000k`)I\u0019\u001b\n\u0013\u0003\r\u0006kb4X\u001e\u0011\u0011gq/O\u000f\u0018\u0007xa\u0014\u0003\r\u0006kb4X\u0007\u000c\u0007`a3W\u0002\u0011\u001aqg8\u0013\u0003\r\u0006kb4X\u001e\u0011\u0011gq/O\u000f\u000e\u000f`l\n\u0003\r\u0006kb4X\u0015\u0011\u0008\u0015\u0003\r\u0006kb4X\u001f\u000e\u0011aj?R\u0000\u000e\u0001fp)C"

    const/16 v5, 0xa2

    const/16 v6, 0xe

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x2f

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v2, 0x4

    const/4 v0, 0x3

    const/4 v15, 0x2

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v0, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v0

    const/16 v0, 0xb

    goto :goto_0

    :cond_0
    const/16 v5, 0x30

    const/16 v2, 0x15

    const-string v4, "\u000c\u0002\tdm;W\u0016\u001f\u0017zg*L\u0000\u0019\u0000uo/M\u001a\u000c\u0002\tdm;W\u0019\u0018\r~t\"D\r\u0014\u0000\u007fr<M\u0007\u0018\u0012ox"

    move v8, v0

    move v6, v2

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v6, v0

    move v8, v11

    :goto_3
    const/16 v9, 0x20

    add-int/2addr v7, v10

    add-int v0, v7, v6

    invoke-virtual {v4, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    const/16 v0, 0xb

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    aget-object v5, v1, v10

    invoke-direct {v4, v5, v3, v3}, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_OK:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    const/4 v5, 0x7

    aget-object v6, v1, v5

    invoke-direct {v4, v6, v10, v10}, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_EOF:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    aget-object v6, v1, v2

    invoke-direct {v4, v6, v15, v15}, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_NO_SUCH_FILE:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    aget-object v6, v1, v0

    invoke-direct {v4, v6, v0, v0}, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_PERMISSION_DENIED:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    aget-object v6, v1, v3

    invoke-direct {v4, v6, v2, v2}, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_FAILURE:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    aget-object v6, v1, v15

    const/4 v7, 0x5

    invoke-direct {v4, v6, v7, v7}, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_BAD_MESSAGE:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    const/16 v6, 0x8

    aget-object v7, v1, v6

    const/4 v8, 0x6

    invoke-direct {v4, v7, v8, v6}, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_OP_UNSUPPORTED:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    const/16 v7, 0x9

    aget-object v9, v1, v7

    invoke-direct {v4, v9, v5, v7}, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_INVALID_HANDLE:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    aget-object v9, v1, v8

    const/16 v11, 0xa

    invoke-direct {v4, v9, v6, v11}, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_NO_SUCH_PATH:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    aget-object v9, v1, v11

    const/16 v12, 0xb

    invoke-direct {v4, v9, v7, v12}, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_FILE_ALREADY_EXISTS:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    new-instance v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    const/4 v9, 0x5

    aget-object v1, v1, v9

    const/16 v9, 0xc

    invoke-direct {v4, v1, v11, v9}, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_WRITE_PROTECT:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    new-array v1, v12, [Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    sget-object v9, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_OK:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    aput-object v9, v1, v3

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_EOF:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    aput-object v3, v1, v10

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_NO_SUCH_FILE:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    aput-object v3, v1, v15

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_PERMISSION_DENIED:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    aput-object v3, v1, v0

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_FAILURE:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    aput-object v0, v1, v2

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_BAD_MESSAGE:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    const/4 v2, 0x5

    aput-object v0, v1, v2

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_OP_UNSUPPORTED:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    aput-object v0, v1, v8

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_INVALID_HANDLE:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    aput-object v0, v1, v5

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_NO_SUCH_PATH:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    aput-object v0, v1, v6

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_FILE_ALREADY_EXISTS:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    aput-object v0, v1, v7

    aput-object v4, v1, v11

    sput-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->a:[Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    return-void

    :cond_3
    const/16 v16, 0xb

    aget-char v17, v11, v14

    rem-int/lit8 v3, v14, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v10, :cond_8

    if-eq v3, v15, :cond_7

    if-eq v3, v0, :cond_6

    if-eq v3, v2, :cond_5

    const/4 v0, 0x5

    if-eq v3, v0, :cond_4

    const/16 v0, 0x28

    goto :goto_4

    :cond_4
    const/16 v0, 0x43

    goto :goto_4

    :cond_5
    move/from16 v0, v16

    goto :goto_4

    :cond_6
    const/16 v0, 0x1b

    goto :goto_4

    :cond_7
    const/16 v0, 0x61

    goto :goto_4

    :cond_8
    const/16 v0, 0x71

    goto :goto_4

    :cond_9
    const/16 v0, 0x7f

    :goto_4
    xor-int/2addr v0, v9

    xor-int v0, v17, v0

    int-to-char v0, v0

    aput-char v0, v11, v14

    add-int/lit8 v14, v14, 0x1

    move/from16 v0, v16

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->code:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;
    .locals 1

    const-class v0, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->a:[Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    invoke-virtual {v0}, [Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    return-object v0
.end method
