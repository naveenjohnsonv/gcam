.class synthetic Lcom/jscape/inet/sftp/protocol/v4/messages/FileType$1;
.super Ljava/lang/Object;


# static fields
.field static final a:[I

.field static final b:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->values()[Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType$1;->b:[I

    const/4 v1, 0x1

    :try_start_0
    sget-object v2, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_REGULAR:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    invoke-virtual {v2}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->ordinal()I

    move-result v2

    aput v1, v0, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x2

    :try_start_1
    sget-object v2, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType$1;->b:[I

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_DIRECTORY:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    invoke-virtual {v3}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->ordinal()I

    move-result v3

    aput v0, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType$1;->b:[I

    sget-object v4, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_SYMLINK:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    invoke-virtual {v4}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    invoke-static {}, Lcom/jscape/util/e/UnixFileType;->values()[Lcom/jscape/util/e/UnixFileType;

    move-result-object v3

    array-length v3, v3

    new-array v3, v3, [I

    sput-object v3, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType$1;->a:[I

    :try_start_3
    sget-object v4, Lcom/jscape/util/e/UnixFileType;->REGULAR:Lcom/jscape/util/e/UnixFileType;

    invoke-virtual {v4}, Lcom/jscape/util/e/UnixFileType;->ordinal()I

    move-result v4

    aput v1, v3, v4
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType$1;->a:[I

    sget-object v3, Lcom/jscape/util/e/UnixFileType;->DIRECTORY:Lcom/jscape/util/e/UnixFileType;

    invoke-virtual {v3}, Lcom/jscape/util/e/UnixFileType;->ordinal()I

    move-result v3

    aput v0, v1, v3
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType$1;->a:[I

    sget-object v1, Lcom/jscape/util/e/UnixFileType;->SYMBOLIC_LINK:Lcom/jscape/util/e/UnixFileType;

    invoke-virtual {v1}, Lcom/jscape/util/e/UnixFileType;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    return-void
.end method
