.class public Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;
.super Lcom/jscape/inet/sftp/protocol/messages/Message;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/sftp/protocol/messages/Message<",
        "Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final attributes:Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;

.field public final filename:Ljava/lang/String;

.field public final id:I

.field public final openFlags:Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "i@\u0019bz\u000cw$\r\u001a61\ri@\u001e\u007fb\u001bp\'\u0015\u000bneT"

    const/16 v5, 0x1a

    const/16 v6, 0xc

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x71

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x1c

    const/16 v4, 0xf

    const-string v6, "\u0003\u0006\u0002X{\u000cC \u0010\u0004>x\u0015hm\u000c|U\u0005nf\u0012J<\u0014\rm>"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v16, v6

    move v6, v4

    move-object/from16 v4, v16

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x64

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v2, 0x68

    goto :goto_4

    :cond_4
    const/16 v2, 0x18

    goto :goto_4

    :cond_5
    const/16 v2, 0x67

    goto :goto_4

    :cond_6
    const/16 v2, 0x7a

    goto :goto_4

    :cond_7
    const/16 v2, 0xe

    goto :goto_4

    :cond_8
    const/16 v2, 0x11

    goto :goto_4

    :cond_9
    const/16 v2, 0x34

    :goto_4
    xor-int/2addr v2, v9

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(ILjava/lang/String;Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/sftp/protocol/messages/Message;-><init>()V

    iput p1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;->id:I

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;->filename:Ljava/lang/String;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;->openFlags:Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;->attributes:Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;

    return-void
.end method

.method public static appending(ILjava/lang/String;Z)Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;
    .locals 10

    new-instance v0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;

    new-instance v9, Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, v9

    move v8, p2

    invoke-direct/range {v1 .. v8}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;-><init>(ZZZZZZZ)V

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->empty()Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;

    move-result-object p2

    invoke-direct {v0, p0, p1, v9, p2}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;-><init>(ILjava/lang/String;Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;)V

    return-object v0
.end method

.method public static reading(ILjava/lang/String;Z)Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;
    .locals 10

    new-instance v0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;

    new-instance v9, Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, v9

    move v8, p2

    invoke-direct/range {v1 .. v8}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;-><init>(ZZZZZZZ)V

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->empty()Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;

    move-result-object p2

    invoke-direct {v0, p0, p1, v9, p2}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;-><init>(ILjava/lang/String;Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;)V

    return-object v0
.end method

.method public static writing(ILjava/lang/String;ZZ)Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;
    .locals 10

    new-instance v0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;

    new-instance v9, Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x0

    move-object v1, v9

    move v6, p2

    move v8, p3

    invoke-direct/range {v1 .. v8}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;-><init>(ZZZZZZZ)V

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;->empty()Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;

    move-result-object p2

    invoke-direct {v0, p0, p1, v9, p2}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;-><init>(ILjava/lang/String;Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/sftp/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;->accept(Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/sftp/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen$Handler;->handle(Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;->a:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;->id:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;->filename:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;->openFlags:Lcom/jscape/inet/sftp/protocol/v4/messages/FileOpenFlags;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpOpen;->attributes:Lcom/jscape/inet/sftp/protocol/v4/messages/FileAttributes;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
