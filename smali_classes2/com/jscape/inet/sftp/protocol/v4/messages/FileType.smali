.class public final enum Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum SSH_FILEXFER_TYPE_DIRECTORY:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

.field public static final enum SSH_FILEXFER_TYPE_REGULAR:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

.field public static final enum SSH_FILEXFER_TYPE_SPECIAL:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

.field public static final enum SSH_FILEXFER_TYPE_SYMLINK:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

.field public static final enum SSH_FILEXFER_TYPE_UNKNOWN:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

.field private static final a:[Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;


# instance fields
.field public final code:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "\u000bSd=B. \u001dXj\'V88\u0001Pi=V\"+\rLm0\u001b\u000bSd=B. \u001dXj\'V88\u0001Pi=@.>\u001dCx-V>\u0019\u000bSd=B. \u001dXj\'V88\u0001Pi=W7)\u001bIm."

    const/16 v6, 0x4f

    move v9, v4

    const/4 v7, -0x1

    const/16 v8, 0x19

    :goto_0
    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v8

    invoke-virtual {v5, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v13, -0x1

    const/16 v14, 0xf

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v15, v11

    move v2, v4

    :goto_2
    const/4 v3, 0x4

    const/4 v12, 0x3

    const/4 v0, 0x2

    if-gt v15, v2, :cond_3

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    if-eqz v13, :cond_1

    add-int/lit8 v0, v9, 0x1

    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v0

    const/4 v0, 0x5

    goto :goto_0

    :cond_0
    const/16 v6, 0x33

    const-string v5, "}%\u0012K4XVk.\u001cQ NNw&\u001fK\'_Q`9\rZ\u0019}%\u0012K4XVk.\u001cQ NNw&\u001fK!HWb?\u0014_"

    move v9, v0

    const/4 v7, -0x1

    const/16 v8, 0x19

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v9, 0x1

    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v8, v0

    move v9, v11

    :goto_3
    const/16 v14, 0x79

    add-int/2addr v7, v10

    add-int v0, v7, v8

    invoke-virtual {v5, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v13, v4

    const/4 v0, 0x5

    goto :goto_1

    :cond_2
    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    aget-object v5, v1, v4

    invoke-direct {v2, v5, v4, v10}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_REGULAR:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    aget-object v5, v1, v10

    invoke-direct {v2, v5, v10, v0}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_DIRECTORY:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    aget-object v5, v1, v3

    invoke-direct {v2, v5, v0, v12}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_SYMLINK:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    aget-object v5, v1, v0

    invoke-direct {v2, v5, v12, v3}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_SPECIAL:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    aget-object v1, v1, v12

    const/4 v5, 0x5

    invoke-direct {v2, v1, v3, v5}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_UNKNOWN:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    new-array v1, v5, [Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    sget-object v5, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_REGULAR:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    aput-object v5, v1, v4

    sget-object v4, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_DIRECTORY:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    aput-object v4, v1, v10

    sget-object v4, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_SYMLINK:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    aput-object v4, v1, v0

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_SPECIAL:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    aput-object v0, v1, v12

    aput-object v2, v1, v3

    sput-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->a:[Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    return-void

    :cond_3
    aget-char v16, v11, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_9

    if-eq v4, v10, :cond_8

    if-eq v4, v0, :cond_7

    const/4 v0, 0x5

    if-eq v4, v12, :cond_6

    if-eq v4, v3, :cond_5

    if-eq v4, v0, :cond_4

    const/16 v3, 0x63

    goto :goto_4

    :cond_4
    const/16 v3, 0x68

    goto :goto_4

    :cond_5
    const/16 v3, 0xb

    goto :goto_4

    :cond_6
    const/16 v3, 0x6d

    goto :goto_4

    :cond_7
    const/4 v0, 0x5

    const/16 v3, 0x23

    goto :goto_4

    :cond_8
    const/4 v0, 0x5

    const/16 v3, 0xf

    goto :goto_4

    :cond_9
    const/4 v0, 0x5

    const/16 v3, 0x57

    :goto_4
    xor-int/2addr v3, v14

    xor-int v3, v16, v3

    int-to-char v3, v3

    aput-char v3, v11, v2

    add-int/lit8 v2, v2, 0x1

    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->code:I

    return-void
.end method

.method public static typeFor(I)Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;
    .locals 6

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->values()[Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended;->a()[Lcom/jscape/util/aq;

    move-result-object v1

    array-length v2, v0

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v2, :cond_3

    aget-object v4, v0, v3

    if-nez v1, :cond_2

    if-nez v1, :cond_4

    iget v5, v4, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->code:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    :cond_2
    if-eqz v1, :cond_0

    :cond_3
    sget-object v4, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_UNKNOWN:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    :cond_4
    return-object v4
.end method

.method public static typeFor(Lcom/jscape/util/e/UnixFileType;)Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType$1;->a:[I

    invoke-virtual {p0}, Lcom/jscape/util/e/UnixFileType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    sget-object p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_SPECIAL:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    return-object p0

    :cond_0
    sget-object p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_SYMLINK:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    return-object p0

    :cond_1
    sget-object p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_DIRECTORY:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    return-object p0

    :cond_2
    sget-object p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->SSH_FILEXFER_TYPE_REGULAR:Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    return-object p0
.end method

.method public static unixFileTypeFor(I)Lcom/jscape/util/e/UnixFileType;
    .locals 1

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->typeFor(I)Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    move-result-object p0

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType$1;->b:[I

    invoke-virtual {p0}, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    sget-object p0, Lcom/jscape/util/e/UnixFileType;->REGULAR:Lcom/jscape/util/e/UnixFileType;

    return-object p0

    :cond_0
    sget-object p0, Lcom/jscape/util/e/UnixFileType;->SYMBOLIC_LINK:Lcom/jscape/util/e/UnixFileType;

    return-object p0

    :cond_1
    sget-object p0, Lcom/jscape/util/e/UnixFileType;->DIRECTORY:Lcom/jscape/util/e/UnixFileType;

    return-object p0

    :cond_2
    sget-object p0, Lcom/jscape/util/e/UnixFileType;->REGULAR:Lcom/jscape/util/e/UnixFileType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;
    .locals 1

    const-class v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->a:[Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    invoke-virtual {v0}, [Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/sftp/protocol/v4/messages/FileType;

    return-object v0
.end method
