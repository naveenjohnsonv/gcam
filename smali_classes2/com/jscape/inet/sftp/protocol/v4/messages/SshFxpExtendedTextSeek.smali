.class public Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;
.super Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended<",
        "Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field public final fileHandle:[B

.field public final lineNumber:J


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xd

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x63

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\u000eDm\u000fa[\u0016W\tc\u0003}\u0003\u001bq\u0017i wN\u001dZ\u0010d\u0008k[<v\u0001y\u0012\\[=IDz\u000fk\u0003\r\u000eDg\u000fc[\u0010C\ne\nj\u0003"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x37

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/4 v13, 0x5

    if-eqz v12, :cond_6

    if-eq v12, v7, :cond_5

    const/4 v14, 0x2

    if-eq v12, v14, :cond_4

    if-eq v12, v0, :cond_7

    const/4 v14, 0x4

    if-eq v12, v14, :cond_3

    if-eq v12, v13, :cond_2

    const/16 v13, 0x3b

    goto :goto_2

    :cond_2
    const/16 v13, 0x5d

    goto :goto_2

    :cond_3
    const/16 v13, 0x6c

    goto :goto_2

    :cond_4
    const/16 v13, 0x62

    goto :goto_2

    :cond_5
    const/4 v13, 0x7

    goto :goto_2

    :cond_6
    const/16 v13, 0x41

    :cond_7
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(I[BJ)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtended;-><init>(I)V

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;->fileHandle:[B

    iput-wide p3, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;->lineNumber:J

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/sftp/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;->accept(Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/sftp/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek$Handler;->handle(Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;->c:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;->id:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;->fileHandle:[B

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpExtendedTextSeek;->lineNumber:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
