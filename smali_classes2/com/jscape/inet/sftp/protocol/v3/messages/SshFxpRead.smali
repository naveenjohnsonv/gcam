.class public Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;
.super Lcom/jscape/inet/sftp/protocol/messages/Message;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/sftp/protocol/messages/Message<",
        "Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final handle:[B

.field public final id:I

.field public final length:I

.field public final offset:J


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "\u0000y&Uj\u0012:Id\u000f\u007f*&r|\u0006\u0004I8*\u0014\u007f\u001f2\u0011"

    const/16 v6, 0x19

    move v9, v4

    const/4 v7, -0x1

    const/16 v8, 0x9

    :goto_0
    const/16 v10, 0x7a

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    move v15, v4

    :goto_2
    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    add-int/lit8 v12, v9, 0x1

    if-eqz v13, :cond_1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v12

    goto :goto_0

    :cond_0
    const/16 v6, 0x13

    const-string v5, "|\u0005^-\u0016m^8\u0018\t|\u0005].\u001eyO$\u0018"

    move v9, v12

    const/4 v7, -0x1

    const/16 v8, 0x9

    goto :goto_3

    :cond_1
    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v12

    :goto_3
    const/4 v10, 0x6

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v13, v4

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v2, v15, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v11, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v2, 0x2c

    goto :goto_4

    :cond_4
    const/16 v2, 0xc

    goto :goto_4

    :cond_5
    const/16 v2, 0x7e

    goto :goto_4

    :cond_6
    const/16 v2, 0x4e

    goto :goto_4

    :cond_7
    const/16 v2, 0x34

    goto :goto_4

    :cond_8
    const/16 v2, 0x23

    goto :goto_4

    :cond_9
    const/16 v2, 0x56

    :goto_4
    xor-int/2addr v2, v10

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v12, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method public constructor <init>(I[BJI)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/sftp/protocol/messages/Message;-><init>()V

    iput p1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;->id:I

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;->handle:[B

    iput-wide p3, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;->offset:J

    iput p5, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;->length:I

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/sftp/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;->accept(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/sftp/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead$Handler;->handle(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;->id:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;->handle:[B

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;->offset:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;->length:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
