.class public Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;
.super Ljava/lang/Object;


# static fields
.field public static final TIME_FACTOR:J = 0x3e8L

.field private static final a:[Ljava/lang/String;


# instance fields
.field public final accessTime:Ljava/lang/Integer;

.field public final extendedData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final fileType:Lcom/jscape/util/e/UnixFileType;

.field public final groupId:Ljava/lang/Integer;

.field public final modificationTime:Ljava/lang/Integer;

.field public final permissions:Lcom/jscape/util/e/PosixFilePermissions;

.field public final size:Ljava/lang/Long;

.field public final userId:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v4, "dDE\u0010i\\#,\u0001D,|M,u\u0015\u000e\rL\r\\M9:\rB\u001di\\>h\u001fS\u0001g\\p\rdDA\u000b~\\>;0I\u0005x\u0004\u000bdDF\u0001q\\\u00191\u0014EU\tdDU\u001bxK\u0004,Y\ndDG\u001arL=\u0001\u0000\u001d"

    const/16 v5, 0x54

    const/4 v6, -0x1

    const/16 v7, 0xf

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x6b

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x22

    const/16 v4, 0x13

    const-string v6, " \u0000\tC=\u0014oeC\u0005X0\u0012gXI\tId\u000e \u0000\u0014I+\u0010`\u007fS\rC7\u000e4"

    move v7, v4

    move-object v4, v6

    move v8, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x2f

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v1, v14, 0x7

    const/4 v2, 0x3

    if-eqz v1, :cond_8

    if-eq v1, v10, :cond_7

    const/4 v3, 0x2

    if-eq v1, v3, :cond_6

    if-eq v1, v2, :cond_9

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v2, 0x26

    goto :goto_4

    :cond_4
    const/16 v2, 0x52

    goto :goto_4

    :cond_5
    const/16 v2, 0x76

    goto :goto_4

    :cond_6
    const/16 v2, 0x4b

    goto :goto_4

    :cond_7
    const/16 v2, 0xf

    goto :goto_4

    :cond_8
    const/16 v2, 0x23

    :cond_9
    :goto_4
    xor-int v1, v9, v2

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/Long;Lcom/jscape/util/e/UnixFileType;Lcom/jscape/util/e/PosixFilePermissions;Ljava/lang/Integer;)V
    .locals 9

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p4

    invoke-direct/range {v0 .. v8}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/jscape/util/e/UnixFileType;Lcom/jscape/util/e/PosixFilePermissions;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/Map;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/jscape/util/e/UnixFileType;Lcom/jscape/util/e/PosixFilePermissions;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lcom/jscape/util/e/UnixFileType;",
            "Lcom/jscape/util/e/PosixFilePermissions;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->size:Ljava/lang/Long;

    iput-object p2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->userId:Ljava/lang/Integer;

    iput-object p3, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->groupId:Ljava/lang/Integer;

    iput-object p4, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->fileType:Lcom/jscape/util/e/UnixFileType;

    iput-object p5, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    iput-object p6, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->accessTime:Ljava/lang/Integer;

    iput-object p7, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->modificationTime:Ljava/lang/Integer;

    invoke-static {p8}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p8, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->extendedData:Ljava/util/Map;

    return-void
.end method

.method public static attributesFor(J)Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;
    .locals 3

    new-instance v0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    const-wide/16 v1, 0x3e8

    div-long/2addr p0, v1

    long-to-int p0, p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const/4 p1, 0x0

    invoke-direct {v0, p1, p1, p1, p0}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;-><init>(Ljava/lang/Long;Lcom/jscape/util/e/UnixFileType;Lcom/jscape/util/e/PosixFilePermissions;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static attributesFor(Lcom/jscape/util/e/PosixFilePermissions;)Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;
    .locals 10

    new-instance v9, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, v9

    move-object v5, p0

    invoke-direct/range {v0 .. v8}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/jscape/util/e/UnixFileType;Lcom/jscape/util/e/PosixFilePermissions;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/Map;)V

    return-object v9
.end method

.method public static empty()Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;
    .locals 2

    new-instance v0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;-><init>(Ljava/lang/Long;Lcom/jscape/util/e/UnixFileType;Lcom/jscape/util/e/PosixFilePermissions;Ljava/lang/Integer;)V

    return-object v0
.end method


# virtual methods
.method public accessTime()J
    .locals 4

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->accessTime:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v0

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    :goto_1
    return-wide v0
.end method

.method public modificationTime()J
    .locals 4

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->modificationTime:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v0

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    :goto_1
    return-wide v0
.end method

.method public readable()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    iget-boolean v1, v1, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanRead:Z

    if-eqz v0, :cond_2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public size()J
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->size:Ljava/lang/Long;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_1
    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->a:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->size:Ljava/lang/Long;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x4

    aget-object v4, v2, v3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->userId:Ljava/lang/Integer;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v4, 0x5

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->groupId:Ljava/lang/Integer;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v4, 0x3

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->fileType:Lcom/jscape/util/e/UnixFileType;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v4, 0x7

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v4, 0x2

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->accessTime:Ljava/lang/Integer;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v4, 0x6

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->modificationTime:Ljava/lang/Integer;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->extendedData:Ljava/util/Map;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_0

    new-array v0, v3, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object v1
.end method

.method public type()Lcom/jscape/util/e/UnixFileType;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->fileType:Lcom/jscape/util/e/UnixFileType;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/jscape/util/e/UnixFileType;->REGULAR:Lcom/jscape/util/e/UnixFileType;

    :cond_1
    :goto_0
    return-object v1
.end method

.method public writable()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    iget-boolean v1, v1, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanWrite:Z

    if-eqz v0, :cond_2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method
