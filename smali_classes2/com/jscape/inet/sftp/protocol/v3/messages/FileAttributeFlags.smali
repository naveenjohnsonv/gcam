.class public Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final accessModificationTimeAttributesPresent:Z

.field public final extendedAttributesPresent:Z

.field public final permissionsAttributePresent:Z

.field public final sizeAttributePresent:Z

.field public final userIdGroupIdAttributesPresent:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "I<PjO\u0004.\u0001[Wv_\u0006.\u0001]QmX\u001f\u0005\u0010h@jz\u0004\u0002\u0016yKm\u0017\u001cI<@a^\u0013\t\u0001yAX^\u0002\u0015\u000c~PmO\u00057\u0017yV|D\u0002Z\u001eI<U|X\u001b\u000e\u0016oLvD\u0005&\u0011hWpH\u0003\u0013\u0000LW|Y\u0013\t\u0011!"

    const/16 v5, 0x5d

    const/16 v6, 0x21

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x69

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x55

    const/16 v4, 0x2a

    const-string v6, "z,\u0010%2[JN,\u001e5\u0007JMz)\u001d\'\u0000\u000fEO,\u0006%2[JN,\u001e5\u0007JnN \u000f%\u001d[\u0003*\u0010e\u001d#\u0010JMO\u0008\u0013$\u001aIW_$\u0008)\u001cAjU(\u0019\u0001\u0007[LU\'\t4\u0016\\nN \u000f%\u001d[\u0003"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v16, v6

    move v6, v4

    move-object/from16 v4, v16

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x30

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    const/4 v3, 0x4

    if-eq v2, v3, :cond_5

    if-eq v2, v0, :cond_4

    const/16 v2, 0xe

    goto :goto_4

    :cond_4
    const/16 v2, 0x1f

    goto :goto_4

    :cond_5
    const/16 v2, 0x43

    goto :goto_4

    :cond_6
    const/16 v2, 0x70

    goto :goto_4

    :cond_7
    const/16 v2, 0x4c

    goto :goto_4

    :cond_8
    const/16 v2, 0x75

    goto :goto_4

    :cond_9
    const/16 v2, 0xc

    :goto_4
    xor-int/2addr v2, v9

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(ZZZZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->sizeAttributePresent:Z

    iput-boolean p2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->userIdGroupIdAttributesPresent:Z

    iput-boolean p3, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->permissionsAttributePresent:Z

    iput-boolean p4, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->accessModificationTimeAttributesPresent:Z

    iput-boolean p5, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->extendedAttributesPresent:Z

    return-void
.end method

.method public static flagsFor(Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;)Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;
    .locals 8

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;

    iget-object v0, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->size:Ljava/lang/Long;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    move v3, v2

    goto :goto_0

    :cond_0
    move v3, v1

    :goto_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->userId:Ljava/lang/Integer;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->groupId:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move v4, v1

    goto :goto_2

    :cond_2
    :goto_1
    move v4, v2

    :goto_2
    iget-object v0, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    if-eqz v0, :cond_3

    move v5, v2

    goto :goto_3

    :cond_3
    move v5, v1

    :goto_3
    iget-object v0, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->accessTime:Ljava/lang/Integer;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->modificationTime:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    goto :goto_4

    :cond_4
    move v7, v1

    goto :goto_5

    :cond_5
    :goto_4
    move v7, v2

    :goto_5
    iget-object p0, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->extendedData:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result p0

    xor-int/2addr p0, v2

    move-object v0, v6

    move v1, v3

    move v2, v4

    move v3, v5

    move v4, v7

    move v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;-><init>(ZZZZZ)V

    return-object v6
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->a:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->sizeAttributePresent:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->userIdGroupIdAttributesPresent:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->permissionsAttributePresent:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->accessModificationTimeAttributesPresent:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->extendedAttributesPresent:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
