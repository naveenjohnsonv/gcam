.class public Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;
.super Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtended;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtended<",
        "Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final blockSize:I

.field public final hashAlgorithmList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final length:J

.field public final name:Ljava/lang/String;

.field public final startOffset:J


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "l\"a)[,\u001c\u000fdt._*U\u000cl\"p1U=\u0003\u0013kh8\u0007\tl\"~8T9\u001c(? \u0013qz\u001bB.-8vw3^;\u000c\u0003jw>Q\u0018\u0001,g\\<W;H;kv`"

    const/16 v5, 0xe

    move v7, v3

    const/4 v6, -0x1

    const/16 v8, 0x46

    :goto_0
    const/16 v9, 0x7c

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v5

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x21

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v14, v11

    move v15, v3

    :goto_2
    const/16 v16, 0x14

    if-gt v14, v15, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v7, 0x1

    if-eqz v13, :cond_1

    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v8, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v11

    goto :goto_0

    :cond_0
    const/16 v4, 0x1d

    const-string v5, "1\u007f\'a\u0014ktq8 r\u000ew]p\u0013&s\u0013>\u00081\u007f!a\nf\u0008:"

    move v8, v4

    move-object v4, v5

    move v7, v11

    move/from16 v5, v16

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v8, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v11

    :goto_3
    add-int/2addr v6, v10

    add-int v9, v6, v5

    invoke-virtual {v4, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v13, v3

    move v9, v12

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v17, v11, v15

    rem-int/lit8 v1, v15, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v10, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    goto :goto_4

    :cond_4
    const/16 v16, 0x22

    goto :goto_4

    :cond_5
    const/16 v16, 0x46

    goto :goto_4

    :cond_6
    move/from16 v16, v12

    goto :goto_4

    :cond_7
    const/16 v16, 0x6e

    goto :goto_4

    :cond_8
    const/16 v16, 0x7e

    goto :goto_4

    :cond_9
    const/16 v16, 0x3c

    :goto_4
    xor-int v1, v9, v16

    xor-int v1, v17, v1

    int-to-char v1, v1

    aput-char v1, v11, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/util/List;JJI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;JJI)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtended;-><init>(I)V

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->name:Ljava/lang/String;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->hashAlgorithmList:Ljava/util/List;

    iput-wide p4, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->startOffset:J

    iput-wide p6, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->length:J

    iput p8, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->blockSize:I

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/sftp/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->accept(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/sftp/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName$Handler;->handle(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;->a()[Lcom/jscape/util/aq;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->a:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->id:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v3, v1, v2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->hashAlgorithmList:Ljava/util/List;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->startOffset:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->length:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->blockSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v1

    if-nez v1, :cond_0

    new-array v1, v2, [Lcom/jscape/util/aq;

    invoke-static {v1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;->b([Lcom/jscape/util/aq;)V

    :cond_0
    return-object v0
.end method
