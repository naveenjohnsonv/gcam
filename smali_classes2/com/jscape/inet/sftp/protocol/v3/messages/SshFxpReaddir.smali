.class public Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir;
.super Lcom/jscape/inet/sftp/protocol/messages/Message;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/sftp/protocol/messages/Message<",
        "Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final handle:[B

.field public final id:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x12

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x43

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "`!(:0/tV3$\u0018!-\u0006H;$A\t\u001fr(\u001d&;JVo"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    const/16 v11, 0x1c

    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    if-ge v3, v11, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v10

    rem-int/lit8 v13, v10, 0x7

    const/4 v14, 0x3

    if-eqz v13, :cond_6

    if-eq v13, v7, :cond_5

    if-eq v13, v0, :cond_4

    if-eq v13, v14, :cond_3

    const/4 v14, 0x4

    if-eq v13, v14, :cond_2

    const/4 v14, 0x5

    if-eq v13, v14, :cond_7

    const/16 v11, 0x65

    goto :goto_2

    :cond_2
    const/16 v11, 0xb

    goto :goto_2

    :cond_3
    const/16 v11, 0x3f

    goto :goto_2

    :cond_4
    move v11, v14

    goto :goto_2

    :cond_5
    const/16 v11, 0x11

    goto :goto_2

    :cond_6
    const/16 v11, 0x70

    :cond_7
    :goto_2
    xor-int/2addr v11, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(I[B)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/sftp/protocol/messages/Message;-><init>()V

    iput p1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir;->id:I

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir;->handle:[B

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/sftp/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir;->accept(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/sftp/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir$Handler;->handle(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir;->a:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir;->id:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir;->handle:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
