.class public final enum Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum SSH_FX_BAD_MESSAGE:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

.field public static final enum SSH_FX_EOF:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

.field public static final enum SSH_FX_FAILURE:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

.field public static final enum SSH_FX_NO_SUCH_FILE:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

.field public static final enum SSH_FX_OK:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

.field public static final enum SSH_FX_OP_UNSUPPORTED:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

.field public static final enum SSH_FX_PERMISSION_DENIED:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

.field private static final synthetic a:[Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;


# instance fields
.field public final code:I


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const/4 v0, 0x7

    new-array v1, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "u7=\tHaQi4*\u0003@j[v4:\u0004Z|J\u0018u7=\tHaQv!\'\u001bGj]o+;\tJ|@o!1\u0013u7=\tHaQh+*\u0005[zFy\"<\u001aK\nu7=\tHaQc+3\u0012u7=\tHaQd%1\tC|]u%2\u0013"

    const/16 v6, 0x60

    move v9, v4

    const/4 v7, -0x1

    const/16 v8, 0x15

    :goto_0
    const/16 v10, 0x72

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    move v15, v4

    :goto_2
    const/4 v2, 0x5

    const/4 v0, 0x3

    const/4 v3, 0x2

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_1

    add-int/lit8 v0, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v0

    const/4 v0, 0x7

    goto :goto_0

    :cond_0
    const/16 v6, 0x18

    const/16 v2, 0xe

    const-string v5, "\u0012PZn/\u00066\u0007B[}<\u000c,\t\u0012PZn/\u00066\u000eH"

    move v9, v0

    move v8, v2

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v8, v0

    move v9, v12

    :goto_3
    add-int/2addr v7, v11

    add-int v0, v7, v8

    invoke-virtual {v5, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v13, v4

    const/4 v0, 0x7

    const/16 v10, 0x15

    goto :goto_1

    :cond_2
    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    const/4 v6, 0x6

    aget-object v7, v1, v6

    invoke-direct {v5, v7, v4, v4}, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_OK:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    aget-object v7, v1, v0

    invoke-direct {v5, v7, v11, v11}, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_EOF:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    aget-object v7, v1, v3

    invoke-direct {v5, v7, v3, v3}, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_NO_SUCH_FILE:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    aget-object v7, v1, v11

    invoke-direct {v5, v7, v0, v0}, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_PERMISSION_DENIED:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    aget-object v7, v1, v2

    const/4 v8, 0x4

    invoke-direct {v5, v7, v8, v8}, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_FAILURE:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    aget-object v7, v1, v8

    invoke-direct {v5, v7, v2, v2}, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_BAD_MESSAGE:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    aget-object v1, v1, v4

    const/16 v7, 0x8

    invoke-direct {v5, v1, v6, v7}, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_OP_UNSUPPORTED:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    const/4 v1, 0x7

    new-array v1, v1, [Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    sget-object v7, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_OK:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    aput-object v7, v1, v4

    sget-object v4, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_EOF:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    aput-object v4, v1, v11

    sget-object v4, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_NO_SUCH_FILE:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    aput-object v4, v1, v3

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_PERMISSION_DENIED:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    aput-object v3, v1, v0

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_FAILURE:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    const/4 v3, 0x4

    aput-object v0, v1, v3

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_BAD_MESSAGE:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    aput-object v0, v1, v2

    aput-object v5, v1, v6

    sput-object v1, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->a:[Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    return-void

    :cond_3
    const/16 v16, 0x7

    aget-char v17, v12, v15

    rem-int/lit8 v4, v15, 0x7

    const/16 v18, 0x7c

    if-eqz v4, :cond_9

    if-eq v4, v11, :cond_8

    if-eq v4, v3, :cond_7

    if-eq v4, v0, :cond_6

    const/4 v0, 0x4

    if-eq v4, v0, :cond_5

    if-eq v4, v2, :cond_4

    goto :goto_4

    :cond_4
    const/16 v0, 0x4b

    goto :goto_5

    :cond_5
    :goto_4
    move/from16 v0, v18

    goto :goto_5

    :cond_6
    const/16 v0, 0x24

    goto :goto_5

    :cond_7
    move/from16 v0, v16

    goto :goto_5

    :cond_8
    const/16 v0, 0x16

    goto :goto_5

    :cond_9
    const/16 v0, 0x54

    :goto_5
    xor-int/2addr v0, v10

    xor-int v0, v17, v0

    int-to-char v0, v0

    aput-char v0, v12, v15

    add-int/lit8 v15, v15, 0x1

    move/from16 v0, v16

    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->code:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;
    .locals 1

    const-class v0, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->a:[Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    invoke-virtual {v0}, [Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    return-object v0
.end method
