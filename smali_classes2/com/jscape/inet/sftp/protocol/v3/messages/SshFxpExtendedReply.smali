.class public abstract Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;
.super Lcom/jscape/inet/sftp/protocol/messages/Message;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<H::",
        "Lcom/jscape/inet/sftp/protocol/messages/Message$HandlerBase;",
        ">",
        "Lcom/jscape/inet/sftp/protocol/messages/Message<",
        "TH;>;"
    }
.end annotation


# static fields
.field private static a:[Lcom/jscape/util/aq;


# instance fields
.field public final id:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;->a()[Lcom/jscape/util/aq;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/jscape/util/aq;

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;->b([Lcom/jscape/util/aq;)V

    :cond_0
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/sftp/protocol/messages/Message;-><init>()V

    iput p1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;->id:I

    return-void
.end method

.method public static a()[Lcom/jscape/util/aq;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;->a:[Lcom/jscape/util/aq;

    return-object v0
.end method

.method public static b([Lcom/jscape/util/aq;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;->a:[Lcom/jscape/util/aq;

    return-void
.end method
