.class public Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final append:Z

.field public final create:Z

.field public final exclusively:Z

.field public final read:Z

.field public final truncate:Z

.field public final write:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "B\u0017B\u0001T\'X\n\n\u000eB\u0017F\tG.C\u001d^U\u0014H;\u000b\u000bB\u0017W\u0003Q,U\u000fCFL\u0014(^O\u0014k2S\u0000qO\u0010C1\u0016\u0015EF\u0010@\u007f"

    const/16 v4, 0x39

    const/16 v5, 0x9

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x35

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x12

    const/16 v3, 0x8

    const-string v5, "#v5b,W22\t#v!b B#jk"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x54

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    const/4 v1, 0x3

    const/4 v2, 0x2

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    if-eq v15, v2, :cond_7

    if-eq v15, v1, :cond_6

    const/4 v2, 0x4

    if-eq v15, v2, :cond_5

    const/4 v2, 0x5

    if-eq v15, v2, :cond_4

    goto :goto_4

    :cond_4
    const/16 v1, 0x77

    goto :goto_4

    :cond_5
    const/16 v1, 0x11

    goto :goto_4

    :cond_6
    const/16 v1, 0x44

    goto :goto_4

    :cond_7
    const/16 v1, 0x16

    goto :goto_4

    :cond_8
    move v1, v2

    goto :goto_4

    :cond_9
    const/16 v1, 0x5b

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(ZZZZZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;->read:Z

    iput-boolean p2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;->write:Z

    iput-boolean p3, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;->append:Z

    iput-boolean p4, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;->create:Z

    iput-boolean p5, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;->truncate:Z

    iput-boolean p6, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;->exclusively:Z

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;->a:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;->read:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;->write:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;->append:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;->create:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;->truncate:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;->exclusively:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
