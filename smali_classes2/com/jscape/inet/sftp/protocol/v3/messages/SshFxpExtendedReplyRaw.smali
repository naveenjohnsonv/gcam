.class public Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;
.super Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply<",
        "Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field public final data:[B


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x7

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v3

    :goto_0
    const/4 v6, 0x1

    add-int/2addr v4, v6

    add-int/2addr v2, v4

    const-string v7, "{Kdo\tQN\u001b\u0004\u0018hH\u0005@6/\u001fe`\u0019U\u0017\u0005\u000epb\u0004b\u0012 K{g\u0019\r"

    invoke-virtual {v7, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v8, v4

    move v9, v3

    :goto_1
    if-gt v8, v9, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x23

    if-ge v2, v4, :cond_0

    invoke-virtual {v7, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v2

    move v2, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v10, v4, v9

    rem-int/lit8 v11, v9, 0x7

    const/16 v12, 0x25

    if-eqz v11, :cond_7

    if-eq v11, v6, :cond_6

    if-eq v11, v0, :cond_5

    const/4 v13, 0x3

    if-eq v11, v13, :cond_4

    const/4 v13, 0x4

    if-eq v11, v13, :cond_3

    const/4 v13, 0x5

    if-eq v11, v13, :cond_2

    const/16 v11, 0x56

    goto :goto_2

    :cond_2
    const/16 v11, 0x15

    goto :goto_2

    :cond_3
    const/16 v11, 0x58

    goto :goto_2

    :cond_4
    const/16 v11, 0x2b

    goto :goto_2

    :cond_5
    move v11, v12

    goto :goto_2

    :cond_6
    const/16 v11, 0x4e

    goto :goto_2

    :cond_7
    const/16 v11, 0x72

    :goto_2
    xor-int/2addr v11, v12

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v4, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public constructor <init>(I[B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;-><init>(I)V

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;->data:[B

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/sftp/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;->accept(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/sftp/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw$Handler;->handle(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;->c:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;->id:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;->data:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
