.class public Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpReadCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/sftp/protocol/messages/Message;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/messages/Message;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readValue(Ljava/io/InputStream;)[B

    move-result-object v2

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->readValue(Ljava/io/InputStream;)J

    move-result-wide v3

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v5

    new-instance p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;-><init>(I[BJI)V

    return-object p1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpReadCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/jscape/inet/sftp/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;

    iget v0, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;->id:I

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-object v0, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;->handle:[B

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeValue([BLjava/io/OutputStream;)V

    iget-wide v0, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;->offset:J

    invoke-static {v0, v1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->writeValue(JLjava/io/OutputStream;)V

    iget p1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;->length:I

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/sftp/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpReadCodec;->write(Lcom/jscape/inet/sftp/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
