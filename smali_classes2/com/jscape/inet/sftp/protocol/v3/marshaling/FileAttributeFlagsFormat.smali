.class public Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static format(Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;)I
    .locals 3

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_SIZE:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->sizeAttributePresent:Z

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_UIDGID:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->userIdGroupIdAttributesPresent:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_PERMISSIONS:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->permissionsAttributePresent:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_ACMODTIME:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->accessModificationTimeAttributesPresent:Z

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->set(ZI)I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_EXTENDED:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    iget-boolean p0, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->extendedAttributesPresent:Z

    invoke-virtual {v1, p0, v0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->set(ZI)I

    move-result p0

    return p0
.end method

.method public static parse(I)Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;
    .locals 7

    new-instance v6, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_SIZE:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->presentIn(I)Z

    move-result v1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_UIDGID:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->presentIn(I)Z

    move-result v2

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_PERMISSIONS:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->presentIn(I)Z

    move-result v3

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_ACMODTIME:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->presentIn(I)Z

    move-result v4

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_EXTENDED:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->presentIn(I)Z

    move-result v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;-><init>(ZZZZZ)V

    return-object v6
.end method
