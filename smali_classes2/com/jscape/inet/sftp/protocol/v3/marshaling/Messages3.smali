.class public Lcom/jscape/inet/sftp/protocol/v3/marshaling/Messages3;
.super Ljava/lang/Object;


# static fields
.field public static final ENTRIES:[Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

.field public static final EXTENDED_ENTRIES:[Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;

.field public static final EXTENDED_REPLY_ENTRIES:[Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const-string v0, "m62?H\u0016Ug22qMZ^k"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    const/16 v4, 0x14

    const/4 v5, 0x5

    const/4 v6, 0x3

    const/4 v7, 0x2

    const/4 v8, 0x4

    const/4 v9, 0x1

    if-gt v1, v3, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    new-array v1, v9, [Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;

    new-instance v10, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCheckFileNameCodec;

    invoke-direct {v10}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCheckFileNameCodec;-><init>()V

    const-class v11, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;

    invoke-direct {v3, v0, v10, v11}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;-><init>(Ljava/lang/String;Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$TypeCodec;Ljava/lang/Class;)V

    aput-object v3, v1, v2

    sput-object v1, Lcom/jscape/inet/sftp/protocol/v3/marshaling/Messages3;->EXTENDED_ENTRIES:[Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;

    new-array v0, v9, [Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;

    new-instance v1, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCheckFileNameCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCheckFileNameCodec;-><init>()V

    const-class v10, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyCheckFileName;

    invoke-direct {v1, v3, v10}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;-><init>(Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$TypeCodec;Ljava/lang/Class;)V

    aput-object v1, v0, v2

    sput-object v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/Messages3;->EXTENDED_REPLY_ENTRIES:[Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;

    const/16 v0, 0x1b

    new-array v0, v0, [Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpInitCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpInitCodec;-><init>()V

    new-array v10, v9, [Ljava/lang/Class;

    const-class v11, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpInit;

    aput-object v11, v10, v2

    invoke-direct {v1, v9, v3, v10}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v2

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpVersionCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpVersionCodec;-><init>()V

    new-array v10, v9, [Ljava/lang/Class;

    const-class v11, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpVersion;

    aput-object v11, v10, v2

    invoke-direct {v1, v7, v3, v10}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v9

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpOpenCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpOpenCodec;-><init>()V

    new-array v10, v9, [Ljava/lang/Class;

    const-class v11, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;

    aput-object v11, v10, v2

    invoke-direct {v1, v6, v3, v10}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpCloseCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpCloseCodec;-><init>()V

    new-array v7, v9, [Ljava/lang/Class;

    const-class v10, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpClose;

    aput-object v10, v7, v2

    invoke-direct {v1, v8, v3, v7}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpReadCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpReadCodec;-><init>()V

    new-array v6, v9, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRead;

    aput-object v7, v6, v2

    invoke-direct {v1, v5, v3, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v8

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpWriteCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpWriteCodec;-><init>()V

    new-array v6, v9, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpWrite;

    aput-object v7, v6, v2

    const/4 v7, 0x6

    invoke-direct {v1, v7, v3, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpLstatCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpLstatCodec;-><init>()V

    new-array v5, v9, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpLstat;

    aput-object v6, v5, v2

    const/4 v6, 0x7

    invoke-direct {v1, v6, v3, v5}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpFstatCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpFstatCodec;-><init>()V

    new-array v5, v9, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpFstat;

    aput-object v7, v5, v2

    const/16 v7, 0x8

    invoke-direct {v1, v7, v3, v5}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpSetStatCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpSetStatCodec;-><init>()V

    new-array v5, v9, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpSetStat;

    aput-object v6, v5, v2

    const/16 v6, 0x9

    invoke-direct {v1, v6, v3, v5}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpFsetStatCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpFsetStatCodec;-><init>()V

    new-array v5, v9, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpFsetStat;

    aput-object v7, v5, v2

    const/16 v7, 0xa

    invoke-direct {v1, v7, v3, v5}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpOpendirCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpOpendirCodec;-><init>()V

    new-array v5, v9, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpendir;

    aput-object v6, v5, v2

    const/16 v6, 0xb

    invoke-direct {v1, v6, v3, v5}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpReaddirCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpReaddirCodec;-><init>()V

    new-array v5, v9, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReaddir;

    aput-object v7, v5, v2

    const/16 v7, 0xc

    invoke-direct {v1, v7, v3, v5}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpRemoveCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpRemoveCodec;-><init>()V

    new-array v5, v9, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRemove;

    aput-object v6, v5, v2

    const/16 v6, 0xd

    invoke-direct {v1, v6, v3, v5}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpMkdirCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpMkdirCodec;-><init>()V

    new-array v5, v9, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpMkdir;

    aput-object v7, v5, v2

    const/16 v7, 0xe

    invoke-direct {v1, v7, v3, v5}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpRmdirCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpRmdirCodec;-><init>()V

    new-array v5, v9, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRmdir;

    aput-object v6, v5, v2

    const/16 v6, 0xf

    invoke-direct {v1, v6, v3, v5}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpRealPathCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpRealPathCodec;-><init>()V

    new-array v5, v9, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRealPath;

    aput-object v7, v5, v2

    const/16 v7, 0x10

    invoke-direct {v1, v7, v3, v5}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpStatCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpStatCodec;-><init>()V

    new-array v5, v9, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStat;

    aput-object v6, v5, v2

    const/16 v6, 0x11

    invoke-direct {v1, v6, v3, v5}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpRenameCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpRenameCodec;-><init>()V

    new-array v5, v9, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpRename;

    aput-object v7, v5, v2

    const/16 v7, 0x12

    invoke-direct {v1, v7, v3, v5}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpReadLinkCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpReadLinkCodec;-><init>()V

    new-array v5, v9, [Ljava/lang/Class;

    const-class v6, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpReadLink;

    aput-object v6, v5, v2

    const/16 v6, 0x13

    invoke-direct {v1, v6, v3, v5}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpSymLinkCodec;

    invoke-direct {v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpSymLinkCodec;-><init>()V

    new-array v5, v9, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpSymLink;

    aput-object v7, v5, v2

    invoke-direct {v1, v4, v3, v5}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    const/16 v3, 0x65

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpStatusCodec;

    invoke-direct {v5}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpStatusCodec;-><init>()V

    new-array v6, v9, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStatus;

    aput-object v7, v6, v2

    invoke-direct {v1, v3, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v1, v0, v4

    const/16 v1, 0x15

    new-instance v3, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    const/16 v4, 0x66

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpHandleCodec;

    invoke-direct {v5}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpHandleCodec;-><init>()V

    new-array v6, v9, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpHandle;

    aput-object v7, v6, v2

    invoke-direct {v3, v4, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v3, v0, v1

    const/16 v1, 0x16

    new-instance v3, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    const/16 v4, 0x67

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpDataCodec;

    invoke-direct {v5}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpDataCodec;-><init>()V

    new-array v6, v9, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpData;

    aput-object v7, v6, v2

    invoke-direct {v3, v4, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v3, v0, v1

    const/16 v1, 0x17

    new-instance v3, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    const/16 v4, 0x68

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpNameCodec;

    invoke-direct {v5}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpNameCodec;-><init>()V

    new-array v6, v9, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpName;

    aput-object v7, v6, v2

    invoke-direct {v3, v4, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v3, v0, v1

    const/16 v1, 0x18

    new-instance v3, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    const/16 v4, 0x69

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpAttrsCodec;

    invoke-direct {v5}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpAttrsCodec;-><init>()V

    new-array v6, v9, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpAttrs;

    aput-object v7, v6, v2

    invoke-direct {v3, v4, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v3, v0, v1

    const/16 v1, 0x19

    new-instance v3, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    const/16 v4, 0xc8

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;

    sget-object v6, Lcom/jscape/inet/sftp/protocol/v3/marshaling/Messages3;->EXTENDED_ENTRIES:[Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;

    invoke-direct {v5, v6}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;-><init>([Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;)V

    new-array v6, v9, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;

    aput-object v7, v6, v2

    invoke-direct {v3, v4, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v3, v0, v1

    const/16 v1, 0x1a

    new-instance v3, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    const/16 v4, 0xc9

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;

    sget-object v6, Lcom/jscape/inet/sftp/protocol/v3/marshaling/Messages3;->EXTENDED_REPLY_ENTRIES:[Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;

    invoke-direct {v5, v6}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;-><init>([Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;)V

    new-array v6, v9, [Ljava/lang/Class;

    const-class v7, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyCheckFileName;

    aput-object v7, v6, v2

    invoke-direct {v3, v4, v5, v6}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;-><init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V

    aput-object v3, v0, v1

    sput-object v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/Messages3;->ENTRIES:[Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    return-void

    :cond_0
    aget-char v10, v0, v3

    rem-int/lit8 v11, v3, 0x7

    if-eqz v11, :cond_6

    if-eq v11, v9, :cond_5

    if-eq v11, v7, :cond_4

    if-eq v11, v6, :cond_3

    if-eq v11, v8, :cond_2

    if-eq v11, v5, :cond_1

    goto :goto_1

    :cond_1
    const/16 v4, 0x1c

    goto :goto_1

    :cond_2
    move v4, v8

    goto :goto_1

    :cond_3
    const/16 v4, 0x7b

    goto :goto_1

    :cond_4
    const/16 v4, 0x70

    goto :goto_1

    :cond_5
    const/16 v4, 0x79

    goto :goto_1

    :cond_6
    const/16 v4, 0x29

    :goto_1
    const/16 v5, 0x27

    xor-int/2addr v4, v5

    xor-int/2addr v4, v10

    int-to-char v4, v4

    aput-char v4, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init(Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;)Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/Messages3;->ENTRIES:[Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;

    invoke-virtual {p0, v0}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;->set([Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec;

    move-result-object p0

    return-object p0
.end method

.method public static init(Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/Messages3;->EXTENDED_REPLY_ENTRIES:[Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;

    invoke-virtual {p0, v0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;->set([Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;

    move-result-object p0

    return-object p0
.end method
