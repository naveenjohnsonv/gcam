.class public Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/sftp/protocol/messages/Message;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class;",
            "Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x14

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x1

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    const-string v7, "PbwZhR>wxaK8O4qdkK\"\u0002\u0015PbwZhR>wxaK8O4v\u007feH}\u0018q"

    invoke-virtual {v7, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v8, v4

    move v9, v2

    :goto_1
    if-gt v8, v9, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x2a

    if-ge v3, v4, :cond_0

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v13, v4

    move v4, v3

    move v3, v13

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v10, v4, v9

    rem-int/lit8 v11, v9, 0x7

    const/4 v12, 0x3

    if-eqz v11, :cond_6

    if-eq v11, v6, :cond_5

    if-eq v11, v0, :cond_7

    if-eq v11, v12, :cond_4

    const/4 v12, 0x4

    if-eq v11, v12, :cond_3

    const/4 v12, 0x5

    if-eq v11, v12, :cond_2

    const/16 v12, 0x56

    goto :goto_2

    :cond_2
    const/16 v12, 0x25

    goto :goto_2

    :cond_3
    const/16 v12, 0x1f

    goto :goto_2

    :cond_4
    const/16 v12, 0x28

    goto :goto_2

    :cond_5
    const/16 v12, 0xb

    goto :goto_2

    :cond_6
    move v12, v0

    :cond_7
    :goto_2
    const/4 v11, 0x7

    xor-int/2addr v11, v12

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v4, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public varargs constructor <init>([Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;->b:Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;->set([Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;

    return-void
.end method

.method private a(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtended;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->b()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;->b:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;->c:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method

.method private a(Ljava/lang/String;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->b()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;->c:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/messages/Message;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v0

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-direct {p0, v1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;->a(Ljava/lang/String;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;

    move-result-object v2

    iget-object v2, v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;->codec:Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$TypeCodec;

    invoke-interface {v2, p1, v0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$TypeCodec;->read(Ljava/io/InputStream;I)Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtended;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    invoke-static {p1}, Lcom/jscape/util/X;->c(Ljava/io/InputStream;)[B

    move-result-object p1

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedUnknown;

    invoke-direct {v2, v0, v1, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedUnknown;-><init>(ILjava/lang/String;[B)V

    return-object v2
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public varargs set([Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;
    .locals 5

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    iget-object v3, p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;->a:Ljava/util/Map;

    iget-object v4, v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;->type:Ljava/lang/String;

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;->b:Ljava/util/Map;

    iget-object v4, v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;->messageClass:Ljava/lang/Class;

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public write(Lcom/jscape/inet/sftp/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtended;

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;->a(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtended;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;

    move-result-object v0

    iget v1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtended;->id:I

    invoke-static {v1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-object v1, v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;->type:Ljava/lang/String;

    invoke-static {v1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUsAsciiValue(Ljava/lang/String;Ljava/io/OutputStream;)V

    iget-object v0, v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$Entry;->codec:Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$TypeCodec;

    invoke-interface {v0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$TypeCodec;->write(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtended;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/sftp/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec;->write(Lcom/jscape/inet/sftp/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
