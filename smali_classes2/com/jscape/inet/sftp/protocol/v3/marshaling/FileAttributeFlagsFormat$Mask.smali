.class public final enum Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum SSH_FILEXFER_ATTR_ACMODTIME:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

.field public static final enum SSH_FILEXFER_ATTR_EXTENDED:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

.field public static final enum SSH_FILEXFER_ATTR_PERMISSIONS:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

.field public static final enum SSH_FILEXFER_ATTR_SIZE:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

.field public static final enum SSH_FILEXFER_ATTR_UIDGID:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

.field private static final a:[Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;


# instance fields
.field public final code:I


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ei~\u0008\u0007\u001e\u0005sbp\u0012\u0013\u0008\u0008bnd\u0008\u0004\u000f\u001dstr\u0012\u0005\u0018ei~\u0008\u0007\u001e\u0005sbp\u0012\u0013\u0008\u0008bnd\u0008\u0014\u001e\rqsr\u0016ei~\u0008\u0007\u001e\u0005sbp\u0012\u0013\u0008\u0008bnd\u0008\u0012\u001e\u0013s"

    const/16 v5, 0x4a

    const/16 v6, 0x1a

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/4 v9, 0x1

    add-int/2addr v7, v9

    add-int v10, v7, v6

    invoke-virtual {v4, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v13, -0x1

    const/16 v14, 0x1b

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v15, v10

    move v2, v3

    :goto_2
    const/4 v11, 0x3

    const/4 v12, 0x4

    const/4 v0, 0x2

    if-gt v15, v2, :cond_3

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    if-eqz v13, :cond_1

    add-int/lit8 v0, v8, 0x1

    aput-object v2, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v0

    const/4 v0, 0x5

    goto :goto_0

    :cond_0
    const/16 v5, 0x39

    const-string v4, ", 7ANWL:+9[ZAA+\'-AI]M07+WE[\u001d, 7ANWL:+9[ZAA+\'-AX[R2:,MAQN,"

    move v8, v0

    const/16 v6, 0x1b

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v10, v8, 0x1

    aput-object v2, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v6, v0

    move v8, v10

    :goto_3
    add-int/2addr v7, v9

    add-int v0, v7, v6

    invoke-virtual {v4, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v13, v3

    const/4 v0, 0x5

    const/16 v14, 0x52

    goto :goto_1

    :cond_2
    new-instance v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    aget-object v4, v1, v0

    invoke-direct {v2, v4, v3, v9}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_SIZE:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    aget-object v4, v1, v9

    invoke-direct {v2, v4, v9, v0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_UIDGID:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    aget-object v4, v1, v12

    invoke-direct {v2, v4, v0, v12}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_PERMISSIONS:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    aget-object v4, v1, v11

    const/16 v5, 0x8

    invoke-direct {v2, v4, v11, v5}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_ACMODTIME:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    new-instance v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    aget-object v1, v1, v3

    const/high16 v4, -0x80000000

    invoke-direct {v2, v1, v12, v4}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_EXTENDED:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    sget-object v4, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_SIZE:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    aput-object v4, v1, v3

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_UIDGID:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    aput-object v3, v1, v9

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_PERMISSIONS:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    aput-object v3, v1, v0

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->SSH_FILEXFER_ATTR_ACMODTIME:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    aput-object v0, v1, v11

    aput-object v2, v1, v12

    sput-object v1, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->a:[Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    return-void

    :cond_3
    aget-char v16, v10, v2

    rem-int/lit8 v3, v2, 0x7

    const/16 v17, 0x4c

    const/16 v18, 0x2d

    if-eqz v3, :cond_6

    if-eq v3, v9, :cond_5

    if-eq v3, v0, :cond_6

    const/4 v0, 0x5

    if-eq v3, v11, :cond_7

    if-eq v3, v12, :cond_4

    if-eq v3, v0, :cond_7

    const/16 v17, 0x52

    goto :goto_4

    :cond_4
    const/16 v17, 0x5a

    goto :goto_4

    :cond_5
    const/4 v0, 0x5

    const/16 v17, 0x21

    goto :goto_4

    :cond_6
    const/4 v0, 0x5

    move/from16 v17, v18

    :cond_7
    :goto_4
    xor-int v3, v14, v17

    xor-int v3, v16, v3

    int-to-char v3, v3

    aput-char v3, v10, v2

    add-int/lit8 v2, v2, 0x1

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->code:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;
    .locals 1

    const-class v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->a:[Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    invoke-virtual {v0}, [Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;

    return-object v0
.end method


# virtual methods
.method public presentIn(I)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->b()[Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->code:I

    and-int/2addr p1, v1

    if-eqz v0, :cond_1

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    return p1
.end method

.method public set(ZI)I
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->b()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget p1, p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat$Mask;->code:I

    or-int/2addr p1, p2

    :cond_0
    move p2, p1

    :cond_1
    return p2
.end method
