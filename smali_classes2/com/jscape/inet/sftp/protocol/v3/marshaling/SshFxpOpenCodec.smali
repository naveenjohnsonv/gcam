.class public Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpOpenCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/sftp/protocol/messages/Message;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/messages/Message;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v0

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUtf8Value(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v2

    invoke-static {v2}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat;->parse(I)Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;

    move-result-object v2

    invoke-static {p1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->readValue(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    move-result-object p1

    new-instance v3, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;

    invoke-direct {v3, v0, v1, v2, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;-><init>(ILjava/lang/String;Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;)V

    return-object v3
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpOpenCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/jscape/inet/sftp/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;

    iget v0, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;->id:I

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-object v0, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;->filename:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V

    iget-object v0, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;->openFlags:Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat;->format(Lcom/jscape/inet/sftp/protocol/v3/messages/FileOpenFlags;)I

    move-result v0

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpOpen;->attributes:Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    invoke-static {p1, p2}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->writeValue(Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/sftp/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpOpenCodec;->write(Lcom/jscape/inet/sftp/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
