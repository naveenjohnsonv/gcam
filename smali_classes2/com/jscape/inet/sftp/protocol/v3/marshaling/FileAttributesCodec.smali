.class public Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;
.super Ljava/lang/Object;


# static fields
.field private static b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->b()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->b([Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private static a(Ljava/io/InputStream;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v0

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->b()[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    :goto_0
    add-int/lit8 v3, v0, -0x1

    if-lez v0, :cond_1

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUtf8Value(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUtf8Value(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    if-eqz v1, :cond_1

    :try_start_0
    invoke-interface {v2, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    move v0, v3

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_1
    return-object v2
.end method

.method private static a(Ljava/util/Map;Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public static b([Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->b:[Ljava/lang/String;

    return-void
.end method

.method public static b()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public static readValue(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v1

    invoke-static {v1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat;->parse(I)Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;

    move-result-object v1

    :try_start_0
    iget-boolean v2, v1, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->sizeAttributePresent:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->readValue(Ljava/io/InputStream;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_b

    goto :goto_0

    :cond_0
    move-object v2, v3

    :goto_0
    :try_start_1
    iget-boolean v4, v1, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->userIdGroupIdAttributesPresent:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9

    if-eqz v0, :cond_2

    if-eqz v4, :cond_1

    :try_start_2
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v4
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_a

    goto :goto_1

    :cond_1
    move-object v4, v3

    goto :goto_2

    :cond_2
    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :goto_2
    :try_start_3
    iget-boolean v5, v1, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->userIdGroupIdAttributesPresent:Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7

    if-eqz v0, :cond_4

    if-eqz v5, :cond_3

    :try_start_4
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v5
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_8

    goto :goto_3

    :cond_3
    move-object v5, v3

    goto :goto_4

    :cond_4
    :goto_3
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    :goto_4
    sget-object v6, Lcom/jscape/util/e/UnixFileType;->REGULAR:Lcom/jscape/util/e/UnixFileType;

    :try_start_5
    iget-boolean v7, v1, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->permissionsAttributePresent:Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    if-eqz v0, :cond_6

    if-eqz v7, :cond_5

    :try_start_6
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v7
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_5

    :cond_5
    move-object v7, v3

    goto :goto_6

    :cond_6
    :goto_5
    invoke-static {v7}, Lcom/jscape/util/e/UnixFileType;->parse(I)Lcom/jscape/util/e/UnixFileType;

    move-result-object v6

    invoke-static {v7}, Lcom/jscape/util/e/a;->a(I)Lcom/jscape/util/e/PosixFilePermissions;

    move-result-object v7

    :goto_6
    :try_start_7
    iget-boolean v8, v1, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->accessModificationTimeAttributesPresent:Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    if-eqz v0, :cond_8

    if-eqz v8, :cond_7

    :try_start_8
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v8
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_7

    :cond_7
    move-object v8, v3

    goto :goto_8

    :cond_8
    :goto_7
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    :goto_8
    :try_start_9
    iget-boolean v9, v1, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->accessModificationTimeAttributesPresent:Z
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    if-eqz v0, :cond_9

    if-eqz v9, :cond_a

    :try_start_a
    invoke-static {p0}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v9
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2

    :cond_9
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :cond_a
    move-object v9, v3

    :try_start_b
    iget-boolean v1, v1, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->extendedAttributesPresent:Z

    if-eqz v1, :cond_b

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/InputStream;)Ljava/util/Map;

    move-result-object p0
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0

    goto :goto_9

    :cond_b
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    :goto_9
    new-instance v10, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;

    move-object v1, v10

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, p0

    invoke-direct/range {v1 .. v9}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/jscape/util/e/UnixFileType;Lcom/jscape/util/e/PosixFilePermissions;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/Map;)V

    if-nez v0, :cond_c

    const/4 p0, 0x2

    new-array p0, p0, [I

    invoke-static {p0}, Lcom/jscape/util/aq;->b([I)V

    :cond_c
    return-object v10

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :catch_1
    move-exception p0

    :try_start_c
    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :catch_3
    move-exception p0

    :try_start_d
    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_4

    :catch_4
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :catch_5
    move-exception p0

    :try_start_e
    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_6

    :catch_6
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :catch_7
    move-exception p0

    :try_start_f
    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_8

    :catch_8
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :catch_9
    move-exception p0

    :try_start_10
    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_a

    :catch_a
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :catch_b
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method

.method public static writeValue(Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;Ljava/io/OutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->flagsFor(Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;)Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributeFlagsFormat;->format(Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;)I

    move-result v2

    invoke-static {v2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    :try_start_0
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->sizeAttributePresent:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_d

    if-eqz v1, :cond_1

    if-eqz v2, :cond_0

    :try_start_1
    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->size:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->writeValue(JLjava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_e

    :cond_0
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->userIdGroupIdAttributesPresent:Z

    :cond_1
    const/4 v3, 0x0

    if-eqz v1, :cond_7

    if-eqz v2, :cond_6

    :try_start_2
    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->userId:Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v1, :cond_3

    if-eqz v2, :cond_2

    :try_start_3
    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->userId:Ljava/lang/Integer;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1

    :cond_3
    :goto_0
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_1
    :try_start_4
    invoke-static {v2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->groupId:Ljava/lang/Integer;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    if-eqz v1, :cond_5

    if-eqz v2, :cond_4

    :try_start_5
    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->groupId:Ljava/lang/Integer;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    :cond_4
    move v2, v3

    goto :goto_3

    :cond_5
    :goto_2
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_3
    invoke-static {v2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    goto :goto_4

    :catch_0
    move-exception p0

    :try_start_6
    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :catch_2
    move-exception p0

    :try_start_7
    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    :catch_3
    move-exception p0

    :try_start_8
    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_6
    :goto_4
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->permissionsAttributePresent:Z

    :cond_7
    if-eqz v1, :cond_b

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    invoke-static {v2}, Lcom/jscape/util/e/a;->a(Lcom/jscape/util/e/PosixFilePermissions;)I

    move-result v2

    :try_start_9
    iget-object v4, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->fileType:Lcom/jscape/util/e/UnixFileType;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    if-eqz v1, :cond_8

    if-eqz v4, :cond_9

    :try_start_a
    iget-object v4, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->fileType:Lcom/jscape/util/e/UnixFileType;
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    :cond_8
    invoke-virtual {v4, v2}, Lcom/jscape/util/e/UnixFileType;->format(I)I

    move-result v2

    :cond_9
    invoke-static {v2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    goto :goto_5

    :catch_5
    move-exception p0

    :try_start_b
    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    :catch_6
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_a
    :goto_5
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->accessModificationTimeAttributesPresent:Z

    :cond_b
    if-eqz v1, :cond_11

    if-eqz v2, :cond_10

    :try_start_c
    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->accessTime:Ljava/lang/Integer;
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    if-eqz v1, :cond_d

    if-eqz v2, :cond_c

    :try_start_d
    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->accessTime:Ljava/lang/Integer;
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_b

    goto :goto_6

    :cond_c
    move v2, v3

    goto :goto_7

    :cond_d
    :goto_6
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_7
    :try_start_e
    invoke-static {v2, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->modificationTime:Ljava/lang/Integer;
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7

    if-eqz v1, :cond_e

    if-eqz v2, :cond_f

    :try_start_f
    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->modificationTime:Ljava/lang/Integer;
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_8

    :cond_e
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    :cond_f
    invoke-static {v3, p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    goto :goto_8

    :catch_7
    move-exception p0

    :try_start_10
    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_8

    :catch_8
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :catch_9
    move-exception p0

    :try_start_11
    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_a

    :catch_a
    move-exception p0

    :try_start_12
    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_b

    :catch_b
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_10
    :goto_8
    iget-boolean v2, v0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributeFlags;->extendedAttributesPresent:Z

    :cond_11
    if-eqz v2, :cond_12

    :try_start_13
    iget-object p0, p0, Lcom/jscape/inet/sftp/protocol/v3/messages/FileAttributes;->extendedData:Ljava/util/Map;

    invoke-static {p0, p1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/util/Map;Ljava/io/OutputStream;)V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_c

    goto :goto_9

    :catch_c
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_12
    :goto_9
    return-void

    :catch_d
    move-exception p0

    :try_start_14
    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_e

    :catch_e
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method
