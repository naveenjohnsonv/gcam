.class public final enum Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum SSH_FXF_APPEND:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

.field public static final enum SSH_FXF_CREAT:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

.field public static final enum SSH_FXF_EXCL:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

.field public static final enum SSH_FXF_READ:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

.field public static final enum SSH_FXF_TRUNC:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

.field public static final enum SSH_FXF_WRITE:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

.field private static final a:[Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;


# instance fields
.field public final mask:I


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x6

    new-array v1, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, ":gTTf\u0014\u00176cNBt\t\u000c:gTTf\u0014\u00176qDHl\u000c:gTTf\u0014\u00176fYJd\r:gTTf\u0014\u00176wNNa\u0018"

    const/16 v6, 0x35

    move v9, v4

    const/4 v7, -0x1

    const/16 v8, 0xd

    :goto_0
    const/16 v10, 0x2f

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    move v15, v4

    :goto_2
    const/4 v2, 0x5

    const/4 v0, 0x4

    const/4 v3, 0x2

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_1

    add-int/lit8 v0, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v0

    const/4 v0, 0x6

    goto :goto_0

    :cond_0
    const/16 v6, 0x1c

    const-string v5, "U\u0008;;\t{xY\u000f!1\u0001`\u000eU\u0008;;\t{xY\u001a#4\nmz"

    move v9, v0

    const/4 v7, -0x1

    const/16 v8, 0xd

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v8, v0

    move v9, v12

    :goto_3
    const/16 v10, 0x40

    add-int/2addr v7, v11

    add-int v0, v7, v8

    invoke-virtual {v5, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v13, v4

    const/4 v0, 0x6

    goto :goto_1

    :cond_2
    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    aget-object v6, v1, v3

    invoke-direct {v5, v6, v4, v11}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_READ:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    aget-object v6, v1, v4

    invoke-direct {v5, v6, v11, v3}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_WRITE:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    aget-object v6, v1, v2

    invoke-direct {v5, v6, v3, v0}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_APPEND:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    const/4 v6, 0x3

    aget-object v7, v1, v6

    const/16 v8, 0x8

    invoke-direct {v5, v7, v6, v8}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_CREAT:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    aget-object v6, v1, v0

    const/16 v7, 0x10

    invoke-direct {v5, v6, v0, v7}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_TRUNC:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    new-instance v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    aget-object v1, v1, v11

    const/16 v6, 0x20

    invoke-direct {v5, v1, v2, v6}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_EXCL:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    const/4 v1, 0x6

    new-array v1, v1, [Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    sget-object v6, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_READ:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    aput-object v6, v1, v4

    sget-object v4, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_WRITE:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    aput-object v4, v1, v11

    sget-object v4, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_APPEND:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    aput-object v4, v1, v3

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_CREAT:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    const/4 v4, 0x3

    aput-object v3, v1, v4

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->SSH_FXF_TRUNC:Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    aput-object v3, v1, v0

    aput-object v5, v1, v2

    sput-object v1, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->a:[Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    return-void

    :cond_3
    const/16 v16, 0x6

    aget-char v17, v12, v15

    rem-int/lit8 v4, v15, 0x7

    if-eqz v4, :cond_9

    if-eq v4, v11, :cond_8

    if-eq v4, v3, :cond_7

    const/4 v3, 0x3

    if-eq v4, v3, :cond_6

    if-eq v4, v0, :cond_5

    if-eq v4, v2, :cond_4

    const/16 v0, 0x7e

    goto :goto_4

    :cond_4
    const/16 v0, 0x63

    goto :goto_4

    :cond_5
    const/16 v0, 0xf

    goto :goto_4

    :cond_6
    const/16 v0, 0x24

    goto :goto_4

    :cond_7
    const/16 v0, 0x33

    goto :goto_4

    :cond_8
    const/16 v0, 0x1b

    goto :goto_4

    :cond_9
    const/16 v0, 0x46

    :goto_4
    xor-int/2addr v0, v10

    xor-int v0, v17, v0

    int-to-char v0, v0

    aput-char v0, v12, v15

    add-int/lit8 v15, v15, 0x1

    move/from16 v0, v16

    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->mask:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;
    .locals 1

    const-class v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->a:[Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    invoke-virtual {v0}, [Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;

    return-object v0
.end method


# virtual methods
.method public presentIn(I)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->b()[Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->mask:I

    and-int/2addr p1, v1

    if-eqz v0, :cond_1

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    return p1
.end method

.method public set(ZI)I
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->b()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget p1, p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileOpenFlagsFormat$Mask;->mask:I

    or-int/2addr p1, p2

    :cond_0
    move p2, p1

    :cond_1
    return p2
.end method
