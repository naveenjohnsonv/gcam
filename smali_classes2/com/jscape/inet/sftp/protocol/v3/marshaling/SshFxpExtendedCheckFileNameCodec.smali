.class public Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCheckFileNameCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedCodec$TypeCodec;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;I)Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtended;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->readUtf8Value(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->readUsAsciiValue(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v3

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->readValue(Ljava/io/InputStream;)J

    move-result-wide v4

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->readValue(Ljava/io/InputStream;)J

    move-result-wide v6

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v8

    new-instance p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;

    move-object v0, p1

    move v1, p2

    invoke-direct/range {v0 .. v8}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;-><init>(ILjava/lang/String;Ljava/util/List;JJI)V

    return-object p1
.end method

.method public write(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtended;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;

    iget-object v0, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->name:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/StringCodec;->writeUtf8Value(Ljava/lang/String;Ljava/io/OutputStream;)V

    iget-object v0, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->hashAlgorithmList:Ljava/util/List;

    invoke-static {v0, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/NameListCodec;->writeUsAsciiValue(Ljava/util/List;Ljava/io/OutputStream;)V

    iget-wide v0, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->startOffset:J

    invoke-static {v0, v1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->writeValue(JLjava/io/OutputStream;)V

    iget-wide v0, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->length:J

    invoke-static {v0, v1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint64Codec;->writeValue(JLjava/io/OutputStream;)V

    iget p1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedCheckFileName;->blockSize:I

    invoke-static {p1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    return-void
.end method
