.class public Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/sftp/protocol/messages/Message;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class;",
            "Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "\r\u001f{\n%>I*\u0005m\u001bu#C+\u0002i\u00180t\u0006"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;->b:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x46

    goto :goto_1

    :cond_1
    const/16 v4, 0x2e

    goto :goto_1

    :cond_2
    const/16 v4, 0x35

    goto :goto_1

    :cond_3
    const/16 v4, 0x1f

    goto :goto_1

    :cond_4
    const/16 v4, 0x68

    goto :goto_1

    :cond_5
    const/16 v4, 0x11

    goto :goto_1

    :cond_6
    const/16 v4, 0x38

    :goto_1
    const/16 v5, 0x60

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public varargs constructor <init>([Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;->a:Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;->set([Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;

    return-void
.end method

.method private a(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;
    .locals 0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;->a(Ljava/lang/Class;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;

    move-result-object p1

    return-object p1
.end method

.method private a(Ljava/lang/Class;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;
    .locals 3

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/FileAttributesCodec;->b()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public convert(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;Ljava/lang/Class;)Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;",
            ">(",
            "Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;",
            "Ljava/lang/Class<",
            "TM;>;)TM;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/e;

    iget-object v1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;->data:[B

    invoke-direct {v0, v1}, Lcom/jscape/util/h/e;-><init>([B)V

    invoke-direct {p0, p2}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;->a(Ljava/lang/Class;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;

    move-result-object p2

    iget-object p2, p2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;->codec:Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$TypeCodec;

    iget p1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;->id:I

    invoke-interface {p2, v0, p1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$TypeCodec;->read(Ljava/io/InputStream;I)Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;

    move-result-object p1

    return-object p1
.end method

.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/messages/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->readValue(Ljava/io/InputStream;)I

    move-result v0

    invoke-static {p1}, Lcom/jscape/util/X;->c(Ljava/io/InputStream;)[B

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;

    invoke-direct {v1, v0, p1}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReplyRaw;-><init>(I[B)V

    return-object v1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/sftp/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public varargs set([Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;
    .locals 5

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    iget-object v3, p0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;->a:Ljava/util/Map;

    iget-object v4, v2, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;->messageClass:Ljava/lang/Class;

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public write(Lcom/jscape/inet/sftp/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;->a(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;)Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;

    move-result-object v0

    iget v1, p1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;->id:I

    invoke-static {v1, p2}, Lcom/jscape/inet/ssh/protocol/v2/marshaling/primitive/Uint32Codec;->writeValue(ILjava/io/OutputStream;)V

    iget-object v0, v0, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$Entry;->codec:Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$TypeCodec;

    invoke-interface {v0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec$TypeCodec;->write(Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpExtendedReply;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/sftp/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/protocol/v3/marshaling/SshFxpExtendedReplyCodec;->write(Lcom/jscape/inet/sftp/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
