.class public abstract Lcom/jscape/inet/sftp/protocol/messages/Message;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<H::",
        "Lcom/jscape/inet/sftp/protocol/messages/Message$HandlerBase;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/messages/Message;->b()I

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x27

    invoke-static {v0}, Lcom/jscape/inet/sftp/protocol/messages/Message;->b(I)V

    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b()I
    .locals 1

    sget v0, Lcom/jscape/inet/sftp/protocol/messages/Message;->b:I

    return v0
.end method

.method public static b(I)V
    .locals 0

    sput p0, Lcom/jscape/inet/sftp/protocol/messages/Message;->b:I

    return-void
.end method

.method public static c()I
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/messages/Message;->b()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x4b

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public abstract accept(Lcom/jscape/inet/sftp/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TH;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/sftp/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation
.end method
