.class public Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage;
.super Lcom/jscape/inet/sftp/protocol/messages/Message;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/sftp/protocol/messages/Message<",
        "Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final data:[B

.field public final id:I

.field public final messageType:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x5

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x36

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "5\u000e[X\u001b\u00075\u000eV]R\'q\u001cL@YRI1\"TKAOG!)9U_YU5-~KfEV#q"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x2a

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move/from16 v16, v5

    move v5, v4

    move/from16 v4, v16

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    const/4 v14, 0x4

    if-eqz v13, :cond_6

    if-eq v13, v8, :cond_5

    const/4 v15, 0x2

    if-eq v13, v15, :cond_7

    if-eq v13, v0, :cond_4

    if-eq v13, v14, :cond_3

    if-eq v13, v2, :cond_2

    const/16 v14, 0x7a

    goto :goto_2

    :cond_2
    const/16 v14, 0x70

    goto :goto_2

    :cond_3
    const/16 v14, 0x10

    goto :goto_2

    :cond_4
    const/16 v14, 0xa

    goto :goto_2

    :cond_5
    const/16 v14, 0x18

    goto :goto_2

    :cond_6
    const/16 v14, 0x2f

    :cond_7
    :goto_2
    xor-int v13, v7, v14

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method public constructor <init>(II[B)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/sftp/protocol/messages/Message;-><init>()V

    iput p1, p0, Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage;->messageType:I

    iput p2, p0, Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage;->id:I

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/messages/Message;->c()I

    move-result p1

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage;->data:[B

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p2

    if-nez p2, :cond_0

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/jscape/inet/sftp/protocol/messages/Message;->b(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/sftp/protocol/messages/Message$HandlerBase;Lcom/jscape/util/k/a/x;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage;->accept(Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage$Handler;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage$Handler;Lcom/jscape/util/k/a/x;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage$Handler;",
            "Lcom/jscape/util/k/a/x<",
            "Lcom/jscape/inet/sftp/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage$Handler;->handle(Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage;Lcom/jscape/util/k/a/x;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/sftp/protocol/messages/Message;->c()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage;->a:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage;->messageType:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage;->id:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/messages/UnknownMessage;->data:[B

    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object v1
.end method
