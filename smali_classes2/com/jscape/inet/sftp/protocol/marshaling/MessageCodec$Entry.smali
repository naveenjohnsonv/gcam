.class public Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static b:[Lcom/jscape/util/aq;


# instance fields
.field public final codec:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/sftp/protocol/messages/Message;",
            ">;"
        }
    .end annotation
.end field

.field public final messageClasses:[Ljava/lang/Class;

.field public final messageCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    new-array v2, v0, [Lcom/jscape/util/aq;

    invoke-static {v2}, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;->b([Lcom/jscape/util/aq;)V

    const/4 v2, 0x0

    const/16 v3, 0x13

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x1

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    const-string v7, "p\u001bvE?4\u0003X\u0010qD\'s\u001dv\u001afR{\u0011\u0019UoR5g\u0019R\u0010A[\'g\u000bP\u0006?\u0008\u0019UaX\"q\u001b\u0008"

    invoke-virtual {v7, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v8, v4

    move v9, v2

    :goto_1
    if-gt v8, v9, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x2e

    if-ge v3, v4, :cond_0

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v13, v4

    move v4, v3

    move v3, v13

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v10, v4, v9

    rem-int/lit8 v11, v9, 0x7

    if-eqz v11, :cond_7

    if-eq v11, v6, :cond_6

    const/4 v12, 0x2

    if-eq v11, v12, :cond_5

    if-eq v11, v0, :cond_4

    const/4 v12, 0x4

    if-eq v11, v12, :cond_3

    const/4 v12, 0x5

    if-eq v11, v12, :cond_2

    const/16 v11, 0x79

    goto :goto_2

    :cond_2
    const/16 v11, 0x15

    goto :goto_2

    :cond_3
    const/16 v11, 0x47

    goto :goto_2

    :cond_4
    const/16 v11, 0x36

    goto :goto_2

    :cond_5
    move v11, v0

    goto :goto_2

    :cond_6
    const/16 v11, 0x74

    goto :goto_2

    :cond_7
    const/16 v11, 0x34

    :goto_2
    xor-int/2addr v11, v6

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v4, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public varargs constructor <init>(ILcom/jscape/util/h/I;[Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/sftp/protocol/messages/Message;",
            ">;[",
            "Ljava/lang/Class;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;->messageCode:I

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;->messageClasses:[Ljava/lang/Class;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;->codec:Lcom/jscape/util/h/I;

    return-void
.end method

.method public static b([Lcom/jscape/util/aq;)V
    .locals 0

    sput-object p0, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;->b:[Lcom/jscape/util/aq;

    return-void
.end method

.method public static b()[Lcom/jscape/util/aq;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;->b:[Lcom/jscape/util/aq;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;->a:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;->messageCode:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;->codec:Lcom/jscape/util/h/I;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/sftp/protocol/marshaling/MessageCodec$Entry;->messageClasses:[Ljava/lang/Class;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
