.class public Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$Listener;


# static fields
.field private static final m:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/io/File;

.field private final b:Ljava/lang/String;

.field private final c:Z

.field private final d:I

.field private final e:Lcom/jscape/util/Time;

.field private final f:Ljava/lang/Integer;

.field private g:Lcom/jscape/inet/sftp/Sftp;

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/io/File;",
            "Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/concurrent/ExecutorService;

.field private j:Lcom/jscape/util/b/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory<",
            "Ljava/io/File;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Ljava/util/concurrent/CountDownLatch;

.field private volatile l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, ";.G mU]\u001c.G zRZ\u0017;\u0003vxQZ\u001ca\u000cUo@aw^J\u0015#Fd$\u0010UoBtmXB\t;sekT@\u001dr),?OoxYk\u0010=FcmR]\u0000\u0000Sek\\[\u0010 M bQ@\u001a.ODpOJ\u001a;Lr`\u0000\u000eUoWhkXN\u001d\u000cLuwI\u0012\"UoEiuX{\u0010\"Fsm\\B\t\u001fQejX]\u000f&MgKX^\u000c&Qe}\u0000\u0013-=Bnj[J\u000bo@aw^J\u0015#Fd7\u000eUoNaa|[\r*NpmN\u0012\u0017;.G t\\WY.Wt|P_\r<\u0003vxQZ\u001ca\u0015UoWetMi\u0010#FEaIJ\u0017<Jow\u0000\u0008"

    const/16 v4, 0xe2

    const/16 v5, 0x17

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/4 v8, 0x1

    add-int/2addr v6, v8

    add-int v9, v6, v5

    invoke-virtual {v3, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0x5d

    move v12, v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    array-length v13, v9

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v9}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v12}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v11, :cond_1

    add-int/lit8 v11, v7, 0x1

    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v11

    goto :goto_0

    :cond_0
    const/16 v4, 0x3c

    const/16 v3, 0x26

    const-string v5, ",9@ev]@\u001a-\u0005v~OA\u0011(Hc?RZ_\'Jr?Z\t\u001b Wc|OF\r0\u000b\u0015SiIi|ZE; Wc|OF\r0qtz^\u0014"

    move v7, v11

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v7, 0x1

    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v11

    :goto_3
    const/16 v12, 0x5b

    add-int/2addr v6, v8

    add-int v9, v6, v5

    invoke-virtual {v3, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->m:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v9, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v8, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v1, 0x72

    goto :goto_4

    :cond_4
    const/16 v1, 0x60

    goto :goto_4

    :cond_5
    const/16 v1, 0x44

    goto :goto_4

    :cond_6
    move v1, v10

    goto :goto_4

    :cond_7
    const/16 v1, 0x7e

    goto :goto_4

    :cond_8
    const/16 v1, 0x12

    goto :goto_4

    :cond_9
    const/16 v1, 0x24

    :goto_4
    xor-int/2addr v1, v12

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v9, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/io/File;Ljava/lang/String;ZILcom/jscape/util/Time;Ljava/lang/Integer;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    sget-object v2, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->m:[Ljava/lang/String;

    const/16 v3, 0xa

    aget-object v3, v2, v3

    invoke-static {v1, v3}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a:Ljava/io/File;

    iput-object p2, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->b:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->c:Z

    const/4 p1, 0x0

    if-eqz v0, :cond_2

    int-to-long p2, p4

    const-wide/16 v3, 0x0

    const/16 v1, 0x8

    aget-object v1, v2, v1

    invoke-static {p2, p3, v3, v4, v1}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p4, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->d:I

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->e:Lcom/jscape/util/Time;

    if-eqz p6, :cond_1

    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result p4

    if-eqz v0, :cond_2

    if-lez p4, :cond_0

    goto :goto_0

    :cond_0
    move p4, p1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p4, 0x1

    :cond_2
    :goto_1
    sget-object p2, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->m:[Ljava/lang/String;

    aget-object p1, p2, p1

    invoke-static {p4, p1}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    iput-object p6, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->f:Ljava/lang/Integer;

    return-void
.end method

.method private a(Ljava/io/File;)Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;

    return-object p1
.end method

.method private a(Ljava/io/File;Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;)Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;
    .locals 11

    new-instance v10, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;

    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->b:Ljava/lang/String;

    sget-object v4, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy;->RESUME_AFTER_FIRST_ATTEMPT:Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy;

    iget-boolean v5, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->c:Z

    iget v6, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->d:I

    iget-object v7, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->e:Lcom/jscape/util/Time;

    const/4 v8, 0x0

    move-object v0, v10

    move-object v1, p1

    move-object v2, p2

    move-object v9, p0

    invoke-direct/range {v0 .. v9}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;-><init>(Ljava/io/File;Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;Ljava/lang/String;Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy;ZILcom/jscape/util/Time;ZLcom/jscape/inet/sftp/Sftp$UploadFileOperation$Listener;)V

    return-object v10
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/jscape/inet/sftp/Sftp;->isConnected()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v0}, Lcom/jscape/inet/sftp/Sftp;->connect()Lcom/jscape/inet/sftp/Sftp;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private a(Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->f()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v0}, Lcom/jscape/inet/sftp/Sftp;->copy()Lcom/jscape/inet/sftp/Sftp;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->f()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->disconnectRequired(Z)V

    invoke-virtual {p1, v0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->applyTo(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;

    return-void
.end method

.method private a(Lcom/jscape/util/b/s;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory<",
            "Ljava/io/File;",
            ">;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    invoke-virtual {p1}, Lcom/jscape/util/b/s;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;

    iget-object v1, v1, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;->path:Ljava/lang/Object;

    check-cast v1, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    if-eqz v0, :cond_1

    invoke-virtual {v2, v1}, Lcom/jscape/inet/sftp/Sftp;->exists(Ljava/lang/String;)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v2, :cond_0

    :try_start_1
    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v2, v1}, Lcom/jscape/inet/sftp/Sftp;->makeDir(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    :cond_1
    invoke-virtual {v2, v1}, Lcom/jscape/inet/sftp/Sftp;->setDir(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/jscape/util/b/s;->c()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/util/b/s;

    :try_start_2
    invoke-direct {p0, v1}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Lcom/jscape/util/b/s;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    if-eqz v0, :cond_4

    if-nez v0, :cond_2

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {p1}, Lcom/jscape/inet/sftp/Sftp;->setDirUp()V

    :cond_4
    return-void

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private a(Lcom/jscape/util/b/s;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory<",
            "Ljava/io/File;",
            ">;>;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/jscape/util/b/s;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;

    iget-object v0, v0, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;->path:Ljava/lang/Object;

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    invoke-virtual {p1}, Lcom/jscape/util/b/s;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;

    iget-object v1, v1, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;->files:Ljava/util/Collection;

    new-instance v2, Lcom/jscape/inet/sftp/Sftp$RemoteDirectory$PathElementsDirectory;

    invoke-direct {v2, p2}, Lcom/jscape/inet/sftp/Sftp$RemoteDirectory$PathElementsDirectory;-><init>(Ljava/util/List;)V

    invoke-direct {p0, v1, v2}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Ljava/util/Collection;Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;)V

    invoke-virtual {p1}, Lcom/jscape/util/b/s;->c()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/util/b/s;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2, p2}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v1, v2}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Lcom/jscape/util/b/s;Ljava/util/List;)V

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method private a(Ljava/util/Collection;Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/io/File;",
            ">;",
            "Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;",
            ")V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    invoke-direct {p0, v1, p2}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Ljava/io/File;Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;)Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->h:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method private b()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a:Ljava/io/File;

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$DirectoryService;->asTree(Ljava/io/File;)Lcom/jscape/util/b/s;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->j:Lcom/jscape/util/b/s;

    return-void
.end method

.method private c()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->j:Lcom/jscape/util/b/s;

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Lcom/jscape/util/b/s;)V

    return-void
.end method

.method private d()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->h:Ljava/util/Map;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->k()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->j:Lcom/jscape/util/b/s;

    invoke-direct {p0, v1, v0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Lcom/jscape/util/b/s;Ljava/util/List;)V

    return-void
.end method

.method private e()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->h:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->k:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method private f()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private g()V
    .locals 6

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->i:Ljava/util/concurrent/ExecutorService;

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->h:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->f()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v3}, Lcom/jscape/inet/sftp/Sftp;->copy()Lcom/jscape/inet/sftp/Sftp;

    move-result-object v3

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    :goto_0
    iget-object v4, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation$1;

    invoke-direct {v5, p0, v2, v3}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation$1;-><init>(Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;Lcom/jscape/inet/sftp/Sftp;)V

    invoke-interface {v4, v5}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    if-nez v0, :cond_0

    :cond_2
    return-void
.end method

.method private h()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->k:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private i()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->l:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/inet/sftp/SftpException;

    sget-object v1, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->m:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/inet/sftp/SftpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private j()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->i:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    iput-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->i:Ljava/util/concurrent/ExecutorService;

    :cond_0
    iput-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->j:Lcom/jscape/util/b/s;

    :cond_1
    iput-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->k:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method private k()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v1}, Lcom/jscape/inet/sftp/Sftp;->getDir()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private l()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->h:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;

    invoke-direct {p0, v2}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;)V

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method


# virtual methods
.method public applyTo(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->l:Z

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->b()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->c()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->d()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->e()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->f()Z

    move-result p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p1, :cond_0

    :try_start_2
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->g()V

    if-eqz v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->l()V

    :cond_1
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->h()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->i()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->j()V

    return-object p0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_2
    move-exception p1

    :try_start_4
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-static {v0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_1
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->j()V

    throw p1
.end method

.method public cancel()V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->l:Z

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->h:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;

    invoke-virtual {v2}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->cancel()V

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method public cancel(Ljava/io/File;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a(Ljava/io/File;)Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;

    move-result-object p1

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->cancel()V

    :cond_1
    return-void
.end method

.method public fileCount()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public onOperationCompleted(Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;)V
    .locals 0

    iget-object p1, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->k:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->m:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->a:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->c:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->e:Lcom/jscape/util/Time;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->f:Ljava/lang/Integer;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0xb

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->j:Lcom/jscape/util/b/s;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->l:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
