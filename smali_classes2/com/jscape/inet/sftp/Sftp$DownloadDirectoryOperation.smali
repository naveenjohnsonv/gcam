.class public Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$Listener;


# static fields
.field private static final n:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z

.field private final c:I

.field private final d:Lcom/jscape/util/Time;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/Integer;

.field private g:Lcom/jscape/inet/sftp/Sftp;

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/concurrent/ExecutorService;

.field private j:Lcom/jscape/util/b/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory<",
            "Lcom/jscape/inet/sftp/SftpFile;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Ljava/util/concurrent/CountDownLatch;

.field private volatile l:Z

.field final m:Lcom/jscape/inet/sftp/Sftp;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "\u0008Y\u00178f_EjY\u0007lnSM>KSnjRH/\u0016\u0016f\u0018\u0001}fQI/|\u001ajn]I%J\nLy[Xw\u000cf\u0018\u0010ye]X&T\u0016|6\u0013\u001eJ\u0012vxXX8\u0018\u0010ye]X&T\u0016|%\u0017\u0008Y\u00178\u007fVO/Y\u00178hQH$LSnjRH/\u0016\u000ef\u0018\u0007py[\\.{\u001cmeJ\u0000\"f\u0018\u0015qg[i#U\u0016k\u007f_P:h\u0001}x[O<Q\u001d\u007fY[L?Q\u0001}o\u0003-\u000eW\u0004vgQ\\.|\u001ajn]I%J\nW{[O+L\u001awe\u001eF8]\u001ew\u007f[y#J\u0016{\u007fQO3\u0005T\u000ef\u0018\u001eys\u007fI>]\u001eh\u007fM\u0000"

    const/16 v4, 0xd6

    const/16 v5, 0x17

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x50

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x22

    const/16 v3, 0x10

    const-string v5, "9GM3 \u0004\u000fe\u0013|\"&\u0008\rqZ\u00119GJ.8\u00041p\u0017M55\u0015\rgZ\u000b"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0xf

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->n:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x6d

    goto :goto_4

    :cond_4
    const/16 v1, 0x6e

    goto :goto_4

    :cond_5
    const/16 v1, 0x5b

    goto :goto_4

    :cond_6
    const/16 v1, 0x48

    goto :goto_4

    :cond_7
    const/16 v1, 0x23

    goto :goto_4

    :cond_8
    const/16 v1, 0x68

    goto :goto_4

    :cond_9
    const/16 v1, 0x1a

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;ZILcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 4

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->m:Lcom/jscape/inet/sftp/Sftp;

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result p1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->b:Z

    const/4 p2, 0x0

    if-nez p1, :cond_1

    int-to-long v0, p4

    const-wide/16 v2, 0x0

    sget-object p3, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->n:[Ljava/lang/String;

    aget-object p3, p3, p2

    invoke-static {v0, v1, v2, v3, p3}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p4, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->c:I

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->d:Lcom/jscape/util/Time;

    invoke-static {p6}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p6, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->e:Ljava/lang/String;

    if-eqz p7, :cond_0

    invoke-virtual {p7}, Ljava/lang/Integer;->intValue()I

    move-result p4

    if-nez p1, :cond_1

    if-lez p4, :cond_2

    :cond_0
    const/4 p4, 0x1

    :cond_1
    move p2, p4

    :cond_2
    sget-object p1, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->n:[Ljava/lang/String;

    const/4 p3, 0x4

    aget-object p1, p1, p3

    invoke-static {p2, p1}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    iput-object p7, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->f:Ljava/lang/Integer;

    return-void
.end method

.method private a(Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;Ljava/io/File;)Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;
    .locals 11

    new-instance v10, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;

    sget-object v4, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;->RESUME_AFTER_FIRST_ATTEMPT:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

    iget-boolean v5, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->b:Z

    iget v6, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->c:I

    iget-object v7, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->d:Lcom/jscape/util/Time;

    const/4 v2, 0x0

    const/4 v8, 0x0

    move-object v0, v10

    move-object v1, p1

    move-object v3, p2

    move-object v9, p0

    invoke-direct/range {v0 .. v9}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;-><init>(Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;Ljava/lang/String;Ljava/io/File;Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;ZILcom/jscape/util/Time;ZLcom/jscape/inet/sftp/Sftp$DownloadFileOperation$Listener;)V

    return-object v10
.end method

.method private a(Ljava/lang/String;)Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;

    return-object p1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v1, :cond_1

    if-eqz v1, :cond_3

    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    if-nez v1, :cond_0

    :cond_2
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_3
    return-object v2
.end method

.method private a()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/jscape/inet/sftp/Sftp;->isConnected()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v0}, Lcom/jscape/inet/sftp/Sftp;->connect()Lcom/jscape/inet/sftp/Sftp;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private a(Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->g()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v0}, Lcom/jscape/inet/sftp/Sftp;->copy()Lcom/jscape/inet/sftp/Sftp;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->g()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->disconnectRequired(Z)V

    invoke-virtual {p1, v0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->applyTo(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;

    return-void
.end method

.method private a(Ljava/io/File;Lcom/jscape/util/b/s;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory<",
            "Lcom/jscape/inet/sftp/SftpFile;",
            ">;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p2}, Lcom/jscape/util/b/s;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;

    iget-object v0, v0, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;->path:Ljava/lang/Object;

    check-cast v0, Lcom/jscape/inet/sftp/SftpFile;

    invoke-virtual {v0}, Lcom/jscape/inet/sftp/SftpFile;->getFilename()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/jscape/util/Q;->b(Ljava/io/File;)Ljava/io/File;

    invoke-virtual {p2}, Lcom/jscape/util/b/s;->c()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/jscape/util/b/s;

    invoke-direct {p0, v2, p2}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Ljava/io/File;Lcom/jscape/util/b/s;)V

    if-eqz v1, :cond_0

    :cond_1
    return-void
.end method

.method private a(Ljava/util/List;Lcom/jscape/util/b/s;Ljava/io/File;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/jscape/util/b/s<",
            "Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory<",
            "Lcom/jscape/inet/sftp/SftpFile;",
            ">;>;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    invoke-virtual {p2}, Lcom/jscape/util/b/s;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;

    iget-object v0, v0, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;->path:Ljava/lang/Object;

    check-cast v0, Lcom/jscape/inet/sftp/SftpFile;

    invoke-virtual {v0}, Lcom/jscape/inet/sftp/SftpFile;->getFilename()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result p3

    invoke-virtual {p2}, Lcom/jscape/util/b/s;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;

    iget-object v0, v0, Lcom/jscape/inet/sftp/Sftp$DirectoryService$Directory;->files:Ljava/util/Collection;

    invoke-direct {p0, p1, v0, v1}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Ljava/util/List;Ljava/util/Collection;Ljava/io/File;)V

    invoke-virtual {p2}, Lcom/jscape/util/b/s;->c()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/util/b/s;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2, p1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v2, v0, v1}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Ljava/util/List;Lcom/jscape/util/b/s;Ljava/io/File;)V

    if-nez p3, :cond_0

    :cond_1
    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/Collection;Ljava/io/File;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Collection<",
            "Lcom/jscape/inet/sftp/SftpFile;",
            ">;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/sftp/Sftp$RemoteDirectory$PathElementsDirectory;

    invoke-direct {v0, p1}, Lcom/jscape/inet/sftp/Sftp$RemoteDirectory$PathElementsDirectory;-><init>(Ljava/util/List;)V

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v1

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/sftp/SftpFile;

    invoke-virtual {v2}, Lcom/jscape/inet/sftp/SftpFile;->getFilename()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p3, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {p0, v0, v3}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;Ljava/io/File;)Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;

    move-result-object v3

    iget-object v4, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->h:Ljava/util/Map;

    invoke-direct {p0, p1, v2}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v1, :cond_0

    :cond_1
    return-void
.end method

.method private b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-static {v0, v1}, Lcom/jscape/inet/sftp/Sftp$DirectoryService;->asTree(Ljava/lang/String;Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/util/b/s;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->j:Lcom/jscape/util/b/s;

    return-void
.end method

.method private c()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v0}, Lcom/jscape/inet/sftp/Sftp;->getLocalDir()Ljava/io/File;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->j:Lcom/jscape/util/b/s;

    invoke-direct {p0, v0, v1}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Ljava/io/File;Lcom/jscape/util/b/s;)V

    return-void
.end method

.method private d()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->h:Ljava/util/Map;

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->e()Ljava/util/LinkedList;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->j:Lcom/jscape/util/b/s;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v2}, Lcom/jscape/inet/sftp/Sftp;->getLocalDir()Ljava/io/File;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Ljava/util/List;Lcom/jscape/util/b/s;Ljava/io/File;)V

    return-void
.end method

.method private e()Ljava/util/LinkedList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v1}, Lcom/jscape/inet/sftp/Sftp;->getDir()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private f()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->h:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->k:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method private g()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private h()V
    .locals 6

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->i:Ljava/util/concurrent/ExecutorService;

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->h:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->g()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v3}, Lcom/jscape/inet/sftp/Sftp;->copy()Lcom/jscape/inet/sftp/Sftp;

    move-result-object v3

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    :goto_0
    iget-object v4, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation$1;

    invoke-direct {v5, p0, v2, v3}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation$1;-><init>(Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;Lcom/jscape/inet/sftp/Sftp;)V

    invoke-interface {v4, v5}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    :cond_2
    return-void
.end method

.method private i()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->k:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private j()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->l:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/inet/sftp/SftpException;

    sget-object v1, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->n:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/inet/sftp/SftpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private k()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->i:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    iput-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->i:Ljava/util/concurrent/ExecutorService;

    :cond_0
    iput-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->j:Lcom/jscape/util/b/s;

    :cond_1
    iput-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->k:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method private l()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->h:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;

    invoke-direct {p0, v2}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method


# virtual methods
.method public applyTo(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result p1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->l:Z

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->b()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->c()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->d()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->f()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->g()Z

    move-result v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    :try_start_2
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->h()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez p1, :cond_1

    :cond_0
    :try_start_3
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->l()V

    :cond_1
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->i()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->j()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->k()V

    return-object p0

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_2
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_3
    move-exception p1

    :try_start_7
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->g:Lcom/jscape/inet/sftp/Sftp;

    invoke-static {v0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->k()V

    throw p1
.end method

.method public cancel()V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->l:Z

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->h:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;

    invoke-virtual {v2}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->cancel()V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public cancel(Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a(Ljava/lang/String;)Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;

    move-result-object p1

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->cancel()V

    :cond_1
    return-void
.end method

.method public fileCount()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public onOperationCompleted(Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;)V
    .locals 0

    iget-object p1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->k:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->n:[Ljava/lang/String;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x6

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->b:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v3, 0x8

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->c:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v3, 0x9

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->d:Lcom/jscape/util/Time;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v3, 0xa

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->f:Ljava/lang/Integer;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->j:Lcom/jscape/util/b/s;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->l:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
