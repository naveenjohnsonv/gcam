.class public Lcom/jscape/inet/sftp/SftpAuthenticationException;
.super Lcom/jscape/inet/sftp/SftpException;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private hostname:Ljava/lang/String;

.field private username:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "=r\u0013ic`\"\u000f|Zdspj\u001e}\u000eleev\u001e3\u000ej&lm\u0008gZ\"#w%[d\u0013qn$w\u0008v\u0008%!!q\\="

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/sftp/SftpAuthenticationException;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x2d

    goto :goto_1

    :cond_1
    const/16 v4, 0x2b

    goto :goto_1

    :cond_2
    const/16 v4, 0x29

    goto :goto_1

    :cond_3
    const/16 v4, 0x2a

    goto :goto_1

    :cond_4
    const/16 v4, 0x55

    goto :goto_1

    :cond_5
    const/16 v4, 0x3c

    goto :goto_1

    :cond_6
    const/16 v4, 0x54

    :goto_1
    const/16 v5, 0x2f

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/jscape/inet/sftp/SftpAuthenticationException;->a:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/SftpException;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/SftpAuthenticationException;->hostname:Ljava/lang/String;

    iput-object p2, p0, Lcom/jscape/inet/sftp/SftpAuthenticationException;->username:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getHostname()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpAuthenticationException;->hostname:Ljava/lang/String;

    return-object v0
.end method

.method public getUsername()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpAuthenticationException;->username:Ljava/lang/String;

    return-object v0
.end method
