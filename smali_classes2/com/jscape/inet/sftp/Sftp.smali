.class public Lcom/jscape/inet/sftp/Sftp;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/AutoCloseable;


# static fields
.field public static final ASCII:I = 0x1

.field public static final AUTO:I = 0x0

.field public static final BINARY:I = 0x2

.field private static C:I = 0x0

.field private static final D:[Ljava/lang/String;

.field public static final DEFAULT_VERSION:I = 0x3

.field private static final a:Ljava/io/File;

.field private static final b:Ljava/lang/String; = "."

.field private static final c:I = 0x7fee

.field private static final d:I = 0x8

.field private static final e:Ljava/lang/String; = "/"

.field private static final f:I = 0x2

.field private static final g:I = 0x3

.field private static final h:[B


# instance fields
.field private volatile A:Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;

.field private volatile B:Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;

.field private i:Lcom/jscape/inet/ssh/util/SshParameters;

.field private j:Lcom/jscape/inet/sftp/SftpConfiguration;

.field private k:I

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;

.field private o:Ljava/nio/file/Path;

.field private p:Ljava/lang/String;

.field private final q:Ljava/util/logging/Logger;

.field private final r:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/jscape/inet/sftp/events/SftpListener;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Lcom/jscape/inet/ssh/util/SshHostKeys;

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private volatile x:Z

.field private y:Ljava/util/concurrent/ExecutorService;

.field private z:Lcom/jscape/inet/sftp/FileService;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x18

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/jscape/inet/sftp/Sftp;->b(I)V

    const/4 v2, -0x1

    const-string v3, "RnO\u0015%)=dz\n\u0006-;<!9\u000f\u0005ko=r>D\u00198o5!zC\u0004), nlSX\u0019C\u007fNV<&$drC\u0018)o#hpN\u0019;o\'hdOX\u0002/5\u000emwD\u0013b<1q\u007fX\u00178 &\u000erqI\u001d?\u001f&nfS&#= \u0017k\u007f\\\u0017b!1u0Y\u0019/$\'/kY\u0013>!5l{\u0014TpY\u0003<?;sjO\u0012l91smC\u0019\"a.GwF\u0013l?1ssC\u0005?&;om\n\u001f\");ssK\u0002% :!wYV\"  !\u007f\\\u0017%#5crOX\u0002\u000c\u0014\u0018Hp\\\u0017 &0!rE\u0015-#tewX\u0013/;;sg\u0004$/4q\u0006\u001c\u0012\u000f`_w-?\u001c\tZmy+\u00178\u0003\\EE9\u0011\u0014&SCq\u0012\u0008\u0012z+$/4q\u0006\u001c\u0012\u000f`_w-?\u001c\tZmy+\u00178\u0003\\EE9\u0011\u0014&SCq\u0012\u0008\u0012z+\u0002\u000c\u0014\u0002/5\u0002/0\u0014C\u007fNV  7`r\n\u0012%=1bjE\u00045a\u0016RX~&l&\'!pE\u0002l,;opO\u00158*0/\u000erqI\u001d?\u001f&nfS>#< \u000fbqGX&<7`nOX9;=m\u0017k\u007f\\\u0017b!1u0Y\u0019/$\'/nK\u0005?8;sz\rRX~&l\u001b&`pY\u0010)=\u0019C\u007fNV<&$drC\u0018)o#hpN\u0019;o\'hdOX"

    const/16 v4, 0x1b8

    const/16 v5, 0x27

    move v7, v1

    move v6, v2

    :goto_0
    const/16 v8, 0x5f

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v1

    :goto_2
    const/4 v14, 0x2

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x24

    const/16 v3, 0x14

    const-string v5, "CgN\u0014+(,d}X\u0005{.&dzT\u000e5v\u000fufPO1+ wyXO.,*z"

    move v6, v2

    move v7, v10

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x48

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v1

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    const-string v1, "."

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/sftp/Sftp;->a:Ljava/io/File;

    new-array v0, v14, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jscape/inet/sftp/Sftp;->h:[B

    return-void

    :cond_3
    aget-char v15, v10, v13

    rem-int/lit8 v1, v13, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v9, :cond_8

    if-eq v1, v14, :cond_7

    const/4 v14, 0x3

    if-eq v1, v14, :cond_6

    const/4 v14, 0x4

    if-eq v1, v14, :cond_5

    const/4 v14, 0x5

    if-eq v1, v14, :cond_4

    const/16 v1, 0xb

    goto :goto_4

    :cond_4
    const/16 v1, 0x10

    goto :goto_4

    :cond_5
    const/16 v1, 0x13

    goto :goto_4

    :cond_6
    const/16 v1, 0x29

    goto :goto_4

    :cond_7
    const/16 v1, 0x75

    goto :goto_4

    :cond_8
    const/16 v1, 0x41

    goto :goto_4

    :cond_9
    const/16 v1, 0x5e

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    const/4 v1, 0x0

    goto/16 :goto_2

    :array_0
    .array-data 1
        0xdt
        0xat
    .end array-data
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/util/SshParameters;)V
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/Sftp;->a:Ljava/io/File;

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/sftp/Sftp;-><init>(Lcom/jscape/inet/ssh/util/SshParameters;Ljava/io/File;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/util/SshParameters;Lcom/jscape/inet/sftp/SftpConfiguration;)V
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/Sftp;->a:Ljava/io/File;

    invoke-direct {p0, p1, v0, p2}, Lcom/jscape/inet/sftp/Sftp;-><init>(Lcom/jscape/inet/ssh/util/SshParameters;Ljava/io/File;Lcom/jscape/inet/sftp/SftpConfiguration;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/util/SshParameters;Lcom/jscape/inet/sftp/SftpConfiguration;ILjava/io/File;Ljava/lang/String;IILjava/util/logging/Logger;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->a()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/Sftp;->j:Lcom/jscape/inet/sftp/SftpConfiguration;

    const/4 p1, 0x0

    if-nez v0, :cond_2

    const/4 p2, 0x3

    if-eq p3, p2, :cond_1

    if-nez v0, :cond_2

    const/4 p2, 0x4

    if-ne p3, p2, :cond_0

    goto :goto_0

    :cond_0
    move p2, p1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p2, 0x1

    goto :goto_1

    :cond_2
    move p2, p3

    :goto_1
    sget-object v0, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/4 v1, 0x6

    aget-object v1, v0, v1

    invoke-static {p2, v1}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    iput p3, p0, Lcom/jscape/inet/sftp/Sftp;->k:I

    invoke-virtual {p4}, Ljava/io/File;->isDirectory()Z

    move-result p2

    const/16 p3, 0xf

    aget-object p3, v0, p3

    invoke-static {p2, p3}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    invoke-virtual {p4}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p2

    invoke-interface {p2}, Ljava/nio/file/Path;->normalize()Ljava/nio/file/Path;

    move-result-object p2

    iput-object p2, p0, Lcom/jscape/inet/sftp/Sftp;->o:Ljava/nio/file/Path;

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    iput p6, p0, Lcom/jscape/inet/sftp/Sftp;->t:I

    iput p6, p0, Lcom/jscape/inet/sftp/Sftp;->u:I

    int-to-long p2, p7

    const-wide/16 p4, 0x0

    const/16 p6, 0x15

    aget-object p6, v0, p6

    invoke-static {p2, p3, p4, p5, p6}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p7, p0, Lcom/jscape/inet/sftp/Sftp;->v:I

    const/4 p2, 0x2

    iput p2, p0, Lcom/jscape/inet/sftp/Sftp;->w:I

    invoke-static {p8}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p8, p0, Lcom/jscape/inet/sftp/Sftp;->q:Ljava/util/logging/Logger;

    new-instance p2, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {p2}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object p2, p0, Lcom/jscape/inet/sftp/Sftp;->r:Ljava/util/Set;

    new-instance p2, Lcom/jscape/inet/ssh/util/SshHostKeys;

    new-array p1, p1, [Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;

    invoke-direct {p2, p1}, Lcom/jscape/inet/ssh/util/SshHostKeys;-><init>([Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/Sftp;->s:Lcom/jscape/inet/ssh/util/SshHostKeys;

    return-void
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/util/SshParameters;Ljava/io/File;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/SftpConfiguration;->defaultConfiguration()Lcom/jscape/inet/sftp/SftpConfiguration;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/jscape/inet/sftp/Sftp;-><init>(Lcom/jscape/inet/ssh/util/SshParameters;Ljava/io/File;Lcom/jscape/inet/sftp/SftpConfiguration;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/util/SshParameters;Ljava/io/File;Lcom/jscape/inet/sftp/SftpConfiguration;)V
    .locals 10

    sget-object v0, Ljava/util/logging/Level;->OFF:Ljava/util/logging/Level;

    invoke-static {v0}, Lcom/jscape/util/j/b;->a(Ljava/util/logging/Level;)Lcom/jscape/util/j/b;

    move-result-object v9

    const/4 v4, 0x3

    const-string v6, "."

    const/16 v7, 0x7fee

    const/16 v8, 0x8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v5, p2

    invoke-direct/range {v1 .. v9}, Lcom/jscape/inet/sftp/Sftp;-><init>(Lcom/jscape/inet/ssh/util/SshParameters;Lcom/jscape/inet/sftp/SftpConfiguration;ILjava/io/File;Ljava/lang/String;IILjava/util/logging/Logger;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;
    .locals 12

    new-instance v11, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;

    const/4 v5, 0x1

    const/4 v10, 0x0

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    invoke-direct/range {v0 .. v10}, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLcom/jscape/inet/sftp/Sftp$1;)V

    return-object v11
.end method

.method private a(Lcom/jscape/inet/sftp/FileService$ServerException;)Lcom/jscape/inet/sftp/SftpException;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    const/4 v1, 0x2

    :try_start_0
    iget v2, p1, Lcom/jscape/inet/sftp/FileService$ServerException;->code:I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    const-string v3, ""

    if-eqz v0, :cond_1

    if-ne v1, v2, :cond_0

    :try_start_1
    new-instance p1, Lcom/jscape/inet/sftp/SftpFileNotFoundException;

    invoke-direct {p1, v3}, Lcom/jscape/inet/sftp/SftpFileNotFoundException;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    return-object p1

    :cond_0
    const/4 v1, 0x3

    iget v2, p1, Lcom/jscape/inet/sftp/FileService$ServerException;->code:I

    :cond_1
    if-ne v1, v2, :cond_2

    :try_start_2
    new-instance p1, Lcom/jscape/inet/sftp/SftpPermissionDeniedException;

    invoke-direct {p1, v3}, Lcom/jscape/inet/sftp/SftpPermissionDeniedException;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    new-instance v0, Lcom/jscape/inet/sftp/RequestException;

    iget v1, p1, Lcom/jscape/inet/sftp/FileService$ServerException;->code:I

    iget-object p1, p1, Lcom/jscape/inet/sftp/FileService$ServerException;->message:Ljava/lang/String;

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/sftp/RequestException;-><init>(ILjava/lang/String;)V

    return-object v0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method static a(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    :try_start_0
    instance-of v1, p1, Lcom/jscape/inet/sftp/FileService$UnsupportedVersionException;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_c

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    new-instance v0, Lcom/jscape/inet/sftp/BadVersionException;

    move-object v1, p1

    check-cast v1, Lcom/jscape/inet/sftp/FileService$UnsupportedVersionException;

    iget v1, v1, Lcom/jscape/inet/sftp/FileService$UnsupportedVersionException;->clientVersion:I

    check-cast p1, Lcom/jscape/inet/sftp/FileService$UnsupportedVersionException;

    iget p1, p1, Lcom/jscape/inet/sftp/FileService$UnsupportedVersionException;->serverVersion:I

    invoke-direct {v0, v1, p1}, Lcom/jscape/inet/sftp/BadVersionException;-><init>(II)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_d

    return-object v0

    :cond_0
    instance-of v1, p1, Lcom/jscape/inet/sftp/FileService$ServerException;

    :cond_1
    if-eqz v0, :cond_3

    if-eqz v1, :cond_2

    :try_start_2
    check-cast p1, Lcom/jscape/inet/sftp/FileService$ServerException;

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/FileService$ServerException;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :try_start_3
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v0, :cond_5

    instance-of v1, v1, Lcom/jscape/inet/sftp/FileService$ServerException;
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_0
    if-eqz v1, :cond_4

    :try_start_4
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/sftp/FileService$ServerException;

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/FileService$ServerException;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_2

    return-object p1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_4
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    :cond_5
    if-eqz v0, :cond_7

    if-eqz v1, :cond_6

    :try_start_5
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_3

    if-eqz v0, :cond_7

    :try_start_6
    instance-of v1, v1, Ljava/net/UnknownHostException;
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_4

    if-eqz v1, :cond_6

    :try_start_7
    new-instance v0, Lcom/jscape/inet/sftp/SftpUnknownHostException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/jscape/inet/sftp/SftpUnknownHostException;-><init>(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_5

    return-object v0

    :catch_3
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_6
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    :cond_7
    if-eqz v0, :cond_9

    if-eqz v1, :cond_8

    :try_start_a
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_6

    if-eqz v0, :cond_9

    :try_start_b
    instance-of v1, v1, Ljava/net/ConnectException;
    :try_end_b
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_7

    if-eqz v1, :cond_8

    :try_start_c
    new-instance v0, Lcom/jscape/inet/sftp/SftpConnectionException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/jscape/inet/sftp/SftpConnectionException;-><init>(Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_8

    return-object v0

    :catch_6
    move-exception p1

    :try_start_d
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_d
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_7

    :catch_7
    move-exception p1

    :try_start_e
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_e
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_8

    :catch_8
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_8
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    :cond_9
    if-eqz v0, :cond_a

    if-eqz v1, :cond_b

    :try_start_f
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1
    :try_end_f
    .catch Ljava/lang/RuntimeException; {:try_start_f .. :try_end_f} :catch_9

    if-eqz v0, :cond_a

    :try_start_10
    instance-of v0, v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$InvalidCredentialsException;
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_a

    if-eqz v0, :cond_b

    :try_start_11
    new-instance p1, Lcom/jscape/inet/sftp/SftpAuthenticationException;

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/SshParameters;->getHostname()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getUsername()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/jscape/inet/sftp/SftpAuthenticationException;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_11
    .catch Ljava/lang/RuntimeException; {:try_start_11 .. :try_end_11} :catch_b

    return-object p1

    :catch_9
    move-exception p1

    :try_start_12
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_12
    .catch Ljava/lang/RuntimeException; {:try_start_12 .. :try_end_12} :catch_a

    :catch_a
    move-exception p1

    :try_start_13
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_13
    .catch Ljava/lang/RuntimeException; {:try_start_13 .. :try_end_13} :catch_b

    :catch_b
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_a
    move-object p1, v1

    :cond_b
    invoke-static {p1}, Lcom/jscape/inet/sftp/SftpException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    return-object p1

    :catch_c
    move-exception p1

    :try_start_14
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_14
    .catch Ljava/lang/RuntimeException; {:try_start_14 .. :try_end_14} :catch_d

    :catch_d
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private a(Lcom/jscape/inet/sftp/PathInfo;)Lcom/jscape/inet/sftp/SftpFile;
    .locals 13

    new-instance v12, Lcom/jscape/inet/sftp/SftpFile;

    iget-object v1, p1, Lcom/jscape/inet/sftp/PathInfo;->name:Ljava/lang/String;

    iget-object v2, p1, Lcom/jscape/inet/sftp/PathInfo;->type:Lcom/jscape/util/e/UnixFileType;

    iget-wide v3, p1, Lcom/jscape/inet/sftp/PathInfo;->size:J

    iget-object v5, p1, Lcom/jscape/inet/sftp/PathInfo;->owner:Ljava/lang/String;

    iget-object v6, p1, Lcom/jscape/inet/sftp/PathInfo;->group:Ljava/lang/String;

    iget-object v7, p1, Lcom/jscape/inet/sftp/PathInfo;->permissions:Lcom/jscape/util/e/PosixFilePermissions;

    iget-wide v8, p1, Lcom/jscape/inet/sftp/PathInfo;->accessTime:J

    iget-wide v10, p1, Lcom/jscape/inet/sftp/PathInfo;->modificationTime:J

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/jscape/inet/sftp/SftpFile;-><init>(Ljava/lang/String;Lcom/jscape/util/e/UnixFileType;JLjava/lang/String;Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;JJ)V

    return-object v12
.end method

.method private a(Ljava/lang/String;)Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->o:Ljava/nio/file/Path;

    invoke-interface {v0, p1}, Ljava/nio/file/Path;->resolve(Ljava/lang/String;)Ljava/nio/file/Path;

    move-result-object p1

    invoke-interface {p1}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object p1

    return-object p1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-static {p1, p2}, Lcom/jscape/inet/sftp/PathTools;->resolve(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a()V
    .locals 2

    new-instance v0, Lcom/jscape/util/d;

    invoke-direct {v0}, Lcom/jscape/util/d;-><init>()V

    :try_start_0
    invoke-virtual {v0}, Lcom/jscape/util/d;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Lcom/jscape/util/d;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method static a(Lcom/jscape/inet/sftp/Sftp;Lcom/jscape/inet/sftp/events/SftpEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/events/SftpEvent;)V

    return-void
.end method

.method private a(Lcom/jscape/inet/sftp/events/SftpEvent;)V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->r:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/sftp/events/SftpListener;

    invoke-virtual {p1, v2}, Lcom/jscape/inet/sftp/events/SftpEvent;->accept(Lcom/jscape/inet/sftp/events/SftpListener;)V

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 11

    move-object v9, p0

    new-instance v10, Lcom/jscape/inet/sftp/events/SftpDownloadEvent;

    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-wide v5, p3

    move-wide/from16 v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/jscape/inet/sftp/events/SftpDownloadEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    invoke-direct {p0, v10}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/events/SftpEvent;)V

    return-void
.end method

.method private a(Lcom/jscape/util/n/b;)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    instance-of v1, p1, Lcom/jscape/inet/sftp/FileService$UnsupportedVersionException;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    check-cast p1, Lcom/jscape/inet/sftp/FileService$UnsupportedVersionException;

    iget v1, p1, Lcom/jscape/inet/sftp/FileService$UnsupportedVersionException;->serverVersion:I

    :cond_0
    if-nez v0, :cond_2

    const/4 p1, 0x3

    if-ne v1, p1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move p1, v1

    :goto_1
    return p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;
    .locals 12

    new-instance v11, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;

    const/4 v5, 0x0

    const/4 v10, 0x0

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    invoke-direct/range {v0 .. v10}, Lcom/jscape/inet/sftp/Sftp$TransferProgressListener;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLcom/jscape/inet/sftp/Sftp$1;)V

    return-object v11
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private b()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->d()V

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/jscape/inet/sftp/FileService;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/jscape/util/n/b; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_1
    invoke-direct {p0, v1}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/util/n/b;)Z

    move-result v2
    :try_end_1
    .catch Lcom/jscape/util/n/b; {:try_start_1 .. :try_end_1} :catch_2

    if-eqz v0, :cond_0

    if-eqz v2, :cond_2

    if-eqz v0, :cond_1

    :try_start_2
    iget-boolean v2, p0, Lcom/jscape/inet/sftp/Sftp;->l:Z
    :try_end_2
    .catch Lcom/jscape/util/n/b; {:try_start_2 .. :try_end_2} :catch_4

    :cond_0
    if-eqz v2, :cond_2

    :cond_1
    :try_start_3
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->c()V

    if-eqz v0, :cond_2

    :goto_0
    return-void

    :cond_2
    throw v1
    :try_end_3
    .catch Lcom/jscape/util/n/b; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_2
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Lcom/jscape/util/n/b; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Lcom/jscape/util/n/b; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public static b(I)V
    .locals 0

    sput p0, Lcom/jscape/inet/sftp/Sftp;->C:I

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    new-instance v6, Lcom/jscape/inet/sftp/events/SftpErrorEvent;

    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    const/4 v5, 0x1

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/sftp/events/SftpErrorEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {p0, v6}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/events/SftpEvent;)V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 11

    move-object v9, p0

    new-instance v10, Lcom/jscape/inet/sftp/events/SftpUploadEvent;

    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p2

    move-object v4, p1

    move-wide v5, p3

    move-wide/from16 v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/jscape/inet/sftp/events/SftpUploadEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    invoke-direct {p0, v10}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/events/SftpEvent;)V

    return-void
.end method

.method private c()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x3

    iput v0, p0, Lcom/jscape/inet/sftp/Sftp;->k:I

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->d()V

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/jscape/inet/sftp/FileService;->a(Ljava/lang/Object;)V

    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    new-instance v6, Lcom/jscape/inet/sftp/events/SftpErrorEvent;

    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, v6

    move-object v1, p0

    move-object v2, p2

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/sftp/events/SftpErrorEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {p0, v6}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/events/SftpEvent;)V

    return-void
.end method

.method public static clearProxySettings()V
    .locals 3

    invoke-static {}, Ljava/lang/System;->getProperties()Ljava/util/Properties;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/16 v2, 0x11

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/util/Properties;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/util/Properties;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/util/Properties;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v2, 0x13

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/util/Properties;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/System;->setProperties(Ljava/util/Properties;)V

    return-void
.end method

.method private d()V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    move-object/from16 v1, p0

    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/sftp/Sftp;->g()Lcom/jscape/util/k/a/v;

    move-result-object v3

    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/sftp/Sftp;->h()Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    move-result-object v10

    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;

    iget-object v2, v1, Lcom/jscape/inet/sftp/Sftp;->j:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-object v2, v2, Lcom/jscape/inet/sftp/SftpConfiguration;->sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

    iget-object v4, v2, Lcom/jscape/inet/ssh/SshConfiguration;->identificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    iget-object v2, v1, Lcom/jscape/inet/sftp/Sftp;->j:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-object v2, v2, Lcom/jscape/inet/sftp/SftpConfiguration;->sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

    iget-object v5, v2, Lcom/jscape/inet/ssh/SshConfiguration;->keyExchangeFactory:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;

    iget-object v2, v1, Lcom/jscape/inet/sftp/Sftp;->j:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-object v2, v2, Lcom/jscape/inet/sftp/SftpConfiguration;->sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

    iget-object v6, v2, Lcom/jscape/inet/ssh/SshConfiguration;->encryptionFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;

    iget-object v2, v1, Lcom/jscape/inet/sftp/Sftp;->j:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-object v2, v2, Lcom/jscape/inet/sftp/SftpConfiguration;->sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

    iget-object v7, v2, Lcom/jscape/inet/ssh/SshConfiguration;->macFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;

    iget-object v2, v1, Lcom/jscape/inet/sftp/Sftp;->j:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-object v2, v2, Lcom/jscape/inet/sftp/SftpConfiguration;->sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

    iget-object v8, v2, Lcom/jscape/inet/ssh/SshConfiguration;->compressionFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;

    iget-object v2, v1, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getHostKeyVerifier()Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    move-result-object v9

    iget-object v2, v1, Lcom/jscape/inet/sftp/Sftp;->j:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-object v2, v2, Lcom/jscape/inet/sftp/SftpConfiguration;->sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

    iget v11, v2, Lcom/jscape/inet/ssh/SshConfiguration;->initialWindowSize:I

    iget-object v2, v1, Lcom/jscape/inet/sftp/Sftp;->j:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-object v2, v2, Lcom/jscape/inet/sftp/SftpConfiguration;->sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

    iget v12, v2, Lcom/jscape/inet/ssh/SshConfiguration;->maxPacketSize:I

    iget-object v13, v1, Lcom/jscape/inet/sftp/Sftp;->q:Ljava/util/logging/Logger;

    move-object v2, v0

    invoke-direct/range {v2 .. v13}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;-><init>(Lcom/jscape/util/k/a/v;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;IILjava/util/logging/Logger;)V

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v2

    iget v3, v1, Lcom/jscape/inet/sftp/Sftp;->u:I

    iget v4, v1, Lcom/jscape/inet/sftp/Sftp;->t:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v6

    if-nez v2, :cond_2

    :try_start_0
    iget v3, v1, Lcom/jscape/inet/sftp/Sftp;->k:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    :try_start_1
    iget-object v3, v1, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    if-nez v2, :cond_1

    if-eqz v3, :cond_0

    :try_start_2
    iget-object v3, v1, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    if-nez v2, :cond_1

    :try_start_3
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5

    if-nez v3, :cond_0

    :try_start_4
    iget-object v3, v1, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    sget-object v4, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6

    goto :goto_0

    :cond_0
    sget-object v3, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/4 v4, 0x3

    aget-object v4, v3, v4

    const/16 v5, 0xc

    aget-object v3, v3, v5

    invoke-static {v4, v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    sget-object v4, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    :goto_0
    move-object v15, v3

    :try_start_5
    new-instance v3, Lcom/jscape/inet/sftp/SftpFileService4;

    iget-object v4, v1, Lcom/jscape/inet/sftp/Sftp;->j:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-boolean v14, v4, Lcom/jscape/inet/sftp/SftpConfiguration;->subsystemResponseRequired:Z

    iget v4, v1, Lcom/jscape/inet/sftp/Sftp;->v:I

    iget-object v5, v1, Lcom/jscape/inet/sftp/Sftp;->q:Ljava/util/logging/Logger;

    move-object v11, v3

    move-object v12, v0

    move v13, v6

    move/from16 v16, v4

    move-object/from16 v17, v5

    invoke-direct/range {v11 .. v17}, Lcom/jscape/inet/sftp/SftpFileService4;-><init>(Lcom/jscape/util/k/a/v;IZ[BILjava/util/logging/Logger;)V

    iput-object v3, v1, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    if-eqz v2, :cond_3

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_1
    move-exception v0

    move-object v2, v0

    :try_start_6
    invoke-static {v2}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    :catch_2
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    :catch_3
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    move-exception v0

    :try_start_9
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_5

    :catch_5
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6

    :catch_6
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_2
    :goto_1
    new-instance v2, Lcom/jscape/inet/sftp/SftpFileService3;

    iget-object v3, v1, Lcom/jscape/inet/sftp/Sftp;->j:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget-boolean v7, v3, Lcom/jscape/inet/sftp/SftpConfiguration;->subsystemResponseRequired:Z

    iget v8, v1, Lcom/jscape/inet/sftp/Sftp;->v:I

    iget-object v9, v1, Lcom/jscape/inet/sftp/Sftp;->q:Ljava/util/logging/Logger;

    move-object v4, v2

    move-object v5, v0

    invoke-direct/range {v4 .. v9}, Lcom/jscape/inet/sftp/SftpFileService3;-><init>(Lcom/jscape/util/k/a/v;IZILjava/util/logging/Logger;)V

    iput-object v2, v1, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    :cond_3
    return-void
.end method

.method private e()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    invoke-interface {v1}, Lcom/jscape/inet/sftp/FileService;->j()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    :cond_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private f()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->y:Ljava/util/concurrent/ExecutorService;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->y:Ljava/util/concurrent/ExecutorService;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->y:Ljava/util/concurrent/ExecutorService;

    :cond_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private g()Lcom/jscape/util/k/a/v;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jscape/util/k/a/v<",
            "Lcom/jscape/util/k/a/C;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/k/g;

    new-instance v1, Lcom/jscape/util/k/TransportAddress;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getHostname()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v3}, Lcom/jscape/inet/ssh/util/SshParameters;->getPort()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getConnectionTimeout()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/jscape/util/k/g;-><init>(Lcom/jscape/util/k/TransportAddress;J)V

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v1

    new-instance v11, Lcom/jscape/util/k/b/j;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getSocketTrafficClass()Ljava/lang/Integer;

    move-result-object v6

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getTcpNoDelay()Ljava/lang/Boolean;

    move-result-object v7

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getReadingTimeout()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/jscape/util/Time;->millis(J)Lcom/jscape/util/Time;

    move-result-object v10

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/jscape/util/k/b/j;-><init>(ZLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/jscape/util/Time;Lcom/jscape/util/Time;)V

    :try_start_0
    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyType()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    if-eqz v2, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyHost()Ljava/lang/String;

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    if-eqz v2, :cond_1

    new-instance v1, Lcom/jscape/inet/c/f;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyHost()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v3}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyPort()I

    move-result v3

    iget-object v4, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v4}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyUsername()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyPassword()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/jscape/inet/c/f;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/jscape/inet/c/d;->a(Ljava/lang/String;)Lcom/jscape/inet/c/e;

    move-result-object v2

    new-instance v3, Lcom/jscape/inet/c/g;

    invoke-direct {v3, v1, v2, v0, v11}, Lcom/jscape/inet/c/g;-><init>(Lcom/jscape/inet/c/f;Lcom/jscape/inet/c/e;Lcom/jscape/util/k/g;Lcom/jscape/util/k/b/j;)V

    return-object v3

    :cond_1
    new-instance v1, Lcom/jscape/util/k/b/i;

    invoke-direct {v1, v0, v11}, Lcom/jscape/util/k/b/i;-><init>(Lcom/jscape/util/k/g;Lcom/jscape/util/k/b/j;)V

    return-object v1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private h()Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;
    .locals 9

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    if-nez v0, :cond_1

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getClientAuthentication()Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_8

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/SshParameters;->getClientAuthentication()Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_9

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    :cond_1
    :try_start_2
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getPassword()Ljava/lang/String;

    move-result-object v1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_4

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v0, :cond_4

    if-eqz v1, :cond_2

    :try_start_3
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_5

    if-nez v0, :cond_3

    :try_start_4
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getKeyPair()Ljava/security/KeyPair;

    move-result-object v1
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_6

    if-eqz v1, :cond_2

    :try_start_5
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    iget-object v5, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/util/SshParameters;->getUsername()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v6}, Lcom/jscape/inet/ssh/util/SshParameters;->getKeyPair()Ljava/security/KeyPair;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->defaultAuthentication(Ljava/lang/String;Ljava/security/KeyPair;)Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;

    move-result-object v5

    aput-object v5, v1, v4

    iget-object v5, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/util/SshParameters;->getUsername()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v6}, Lcom/jscape/inet/ssh/util/SshParameters;->getPassword()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->defaultAuthentication(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;

    move-result-object v5

    aput-object v5, v1, v3

    iget-object v5, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/util/SshParameters;->getUsername()Ljava/lang/String;

    move-result-object v5

    new-array v3, v3, [Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;

    sget-object v7, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/16 v8, 0xa

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v8}, Lcom/jscape/inet/ssh/util/SshParameters;->getPassword()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v3, v4

    invoke-static {v5, v3}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;->defaultAuthentication(Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;)Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;-><init>([Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_7

    return-object v0

    :cond_2
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    :cond_3
    if-nez v0, :cond_6

    :try_start_6
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getPassword()Ljava/lang/String;

    move-result-object v1
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_4
    :goto_0
    if-eqz v1, :cond_5

    :try_start_7
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;

    new-array v1, v2, [Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getUsername()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/util/SshParameters;->getPassword()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->defaultAuthentication(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getUsername()Ljava/lang/String;

    move-result-object v2

    new-array v5, v3, [Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;

    sget-object v7, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/16 v8, 0xb

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v8}, Lcom/jscape/inet/ssh/util/SshParameters;->getPassword()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v5, v4

    invoke-static {v2, v5}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;->defaultAuthentication(Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;)Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;-><init>([Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_1

    return-object v0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_5
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    :cond_6
    if-nez v0, :cond_8

    :try_start_8
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getKeyPair()Ljava/security/KeyPair;

    move-result-object v0
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_2

    if-eqz v0, :cond_7

    :try_start_9
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/SshParameters;->getUsername()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getKeyPair()Ljava/security/KeyPair;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->defaultAuthentication(Ljava/lang/String;Ljava/security/KeyPair;)Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;

    move-result-object v0
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_3

    return-object v0

    :cond_7
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_8
    :goto_1
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getUsername()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/authentication/NoneClientAuthentication;->defaultAuthentication(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/authentication/NoneClientAuthentication;

    move-result-object v0

    return-object v0

    :catch_4
    move-exception v0

    :try_start_b
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_b
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_5

    :catch_5
    move-exception v0

    :try_start_c
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_6

    :catch_6
    move-exception v0

    :try_start_d
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_d
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_7

    :catch_7
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_8
    move-exception v0

    :try_start_e
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_e
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_9

    :catch_9
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private i()V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    :try_start_2
    invoke-interface {v1}, Lcom/jscape/inet/sftp/FileService;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public static j()I
    .locals 1

    sget v0, Lcom/jscape/inet/sftp/Sftp;->C:I

    return v0
.end method

.method public static k()I
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0xb

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public abortDownloadThread(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->B:Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v1, p1}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->cancel(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public abortDownloadThreads()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->B:Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->cancel()V

    :cond_1
    return-void
.end method

.method public abortUploadThread(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->A:Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->cancel(Ljava/io/File;)V

    :cond_1
    return-void
.end method

.method public abortUploadThreads()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->A:Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->cancel()V

    :cond_1
    return-void
.end method

.method public addSftpListener(Lcom/jscape/inet/sftp/events/SftpListener;)V
    .locals 1

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->r:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public close()V
    .locals 0

    invoke-virtual {p0}, Lcom/jscape/inet/sftp/Sftp;->disconnect()V

    return-void
.end method

.method public declared-synchronized connect()Lcom/jscape/inet/sftp/Sftp;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->b()V

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    :try_start_2
    instance-of v0, v1, Lcom/jscape/inet/sftp/SftpFileService4;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    check-cast v0, Lcom/jscape/inet/sftp/SftpFileService4;

    invoke-virtual {v0}, Lcom/jscape/inet/sftp/SftpFileService4;->sessionConnection()Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :cond_0
    :try_start_3
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    :cond_1
    check-cast v1, Lcom/jscape/inet/sftp/SftpFileService3;

    invoke-virtual {v1}, Lcom/jscape/inet/sftp/SftpFileService3;->sessionConnection()Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->s:Lcom/jscape/inet/ssh/util/SshHostKeys;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getHostname()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->hostKey()Ljava/security/PublicKey;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/jscape/inet/ssh/util/SshHostKeys;->put(Ljava/net/InetAddress;Ljava/security/PublicKey;)Lcom/jscape/inet/ssh/util/SshHostKeys;

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/jscape/inet/sftp/FileService;->infoAbout(Ljava/lang/String;Z)Lcom/jscape/inet/sftp/PathInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/jscape/inet/sftp/PathInfo;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    sget-object v0, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/16 v1, 0x14

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/jscape/util/ak;->a(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->y:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/jscape/inet/sftp/events/SftpConnectedEvent;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getHostname()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getPort()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/jscape/inet/sftp/events/SftpConnectedEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;I)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/events/SftpEvent;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object p0

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_2
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :goto_1
    :try_start_6
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->e()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->f()V

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object v0

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public copy()Lcom/jscape/inet/sftp/Sftp;
    .locals 10

    new-instance v9, Lcom/jscape/inet/sftp/Sftp;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp;->j:Lcom/jscape/inet/sftp/SftpConfiguration;

    iget v3, p0, Lcom/jscape/inet/sftp/Sftp;->k:I

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->o:Ljava/nio/file/Path;

    invoke-interface {v0}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object v4

    iget-object v5, p0, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    iget v6, p0, Lcom/jscape/inet/sftp/Sftp;->t:I

    iget v7, p0, Lcom/jscape/inet/sftp/Sftp;->v:I

    iget-object v8, p0, Lcom/jscape/inet/sftp/Sftp;->q:Ljava/util/logging/Logger;

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/jscape/inet/sftp/Sftp;-><init>(Lcom/jscape/inet/ssh/util/SshParameters;Lcom/jscape/inet/sftp/SftpConfiguration;ILjava/io/File;Ljava/lang/String;IILjava/util/logging/Logger;)V

    iget v0, p0, Lcom/jscape/inet/sftp/Sftp;->w:I

    iput v0, v9, Lcom/jscape/inet/sftp/Sftp;->w:I

    return-object v9
.end method

.method public declared-synchronized createSymbolicLink(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p2}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v0, p1, p2}, Lcom/jscape/inet/sftp/FileService;->createSymbolicLink(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized deleteDir(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v1, v0}, Lcom/jscape/inet/sftp/FileService;->removeDirectory(Ljava/lang/String;)V

    new-instance v0, Lcom/jscape/inet/sftp/events/SftpDeleteDirEvent;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/inet/sftp/events/SftpDeleteDirEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/events/SftpEvent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized deleteDir(Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_1
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p2, :cond_0

    :try_start_2
    new-instance p2, Lcom/jscape/inet/sftp/Sftp$RecursiveDeleteOperation;

    invoke-direct {p2, p1}, Lcom/jscape/inet/sftp/Sftp$RecursiveDeleteOperation;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p0}, Lcom/jscape/inet/sftp/Sftp$RecursiveDeleteOperation;->applyTo(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$RecursiveDeleteOperation;

    if-nez v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->deleteDir(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized deleteFile(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v0, p1}, Lcom/jscape/inet/sftp/FileService;->removeFile(Ljava/lang/String;)V

    new-instance v0, Lcom/jscape/inet/sftp/events/SftpDeleteFileEvent;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/inet/sftp/events/SftpDeleteFileEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/events/SftpEvent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public disableFileAccessLogging()V
    .locals 2

    sget-object v0, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/16 v1, 0x12

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->OFF:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    return-void
.end method

.method public declared-synchronized disconnect()V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    if-nez v0, :cond_3

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-interface {v1}, Lcom/jscape/inet/sftp/FileService;->h()Z

    move-result v0
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v0, :cond_2

    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    :try_start_4
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->e()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->f()V

    :cond_3
    new-instance v0, Lcom/jscape/inet/sftp/events/SftpDisconnectedEvent;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getHostname()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getPort()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/jscape/inet/sftp/events/SftpDisconnectedEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;I)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/events/SftpEvent;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_1
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_2
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catch_3
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized download(Ljava/lang/String;)Ljava/io/File;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p1}, Lcom/jscape/inet/sftp/Sftp;->download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/jscape/inet/sftp/Sftp;->resumeDownload(Ljava/lang/String;Ljava/lang/String;J)Ljava/io/File;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized download(Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/jscape/inet/sftp/Sftp;->download(Ljava/io/OutputStream;Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized download(Ljava/io/OutputStream;Ljava/lang/String;J)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    invoke-direct/range {p0 .. p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {v9, v10}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/jscape/inet/sftp/Sftp;->getFilesize(Ljava/lang/String;)J

    move-result-wide v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sub-long v7, v1, p3

    :try_start_2
    iget v1, v9, Lcom/jscape/inet/sftp/Sftp;->w:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    if-ne v1, v2, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    move v12, v1

    :try_start_3
    new-instance v13, Lcom/jscape/util/h/p;

    if-eqz v12, :cond_2

    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    instance-of v1, v1, Lcom/jscape/inet/sftp/SftpFileService3;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v1, :cond_2

    :try_start_4
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    sget-object v2, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lcom/jscape/util/h/q;->a(Ljava/io/OutputStream;[B)Lcom/jscape/util/h/q;

    move-result-object v1

    goto :goto_1

    :cond_2
    move-object/from16 v2, p1

    move-object v1, v2

    :goto_1
    invoke-direct {v13, v1}, Lcom/jscape/util/h/p;-><init>(Ljava/io/OutputStream;)V

    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    const-string v4, ""

    const-wide/16 v5, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v8}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;

    move-result-object v14

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v15
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_3

    :try_start_5
    iget-boolean v1, v9, Lcom/jscape/inet/sftp/Sftp;->m:Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v1, :cond_3

    :try_start_6
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    const-wide/16 v3, 0x0

    iget-object v8, v9, Lcom/jscape/inet/sftp/Sftp;->y:Ljava/util/concurrent/ExecutorService;

    move-object v2, v11

    move-object v5, v13

    move v6, v12

    move-object v7, v14

    invoke-interface/range {v1 .. v8}, Lcom/jscape/inet/sftp/FileService;->readFile(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V

    if-nez v0, :cond_4

    goto :goto_2

    :catch_0
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_1
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_3
    :goto_2
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    const-wide/16 v3, 0x0

    move-object v2, v11

    move-object v5, v13

    move v6, v12

    move-object v7, v14

    invoke-interface/range {v1 .. v7}, Lcom/jscape/inet/sftp/FileService;->readFile(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/sftp/FileService$TransferListener;)V

    :cond_4
    const-string v3, ""

    invoke-virtual {v13}, Lcom/jscape/util/h/p;->a()J

    move-result-wide v4

    invoke-static/range {v15 .. v16}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v7}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    monitor-exit p0

    return-void

    :catch_2
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catch_3
    move-exception v0

    move-object v1, v0

    :try_start_9
    invoke-static {v1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_4
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_5
    move-exception v0

    :try_start_b
    const-string v1, ""

    invoke-direct {v9, v10, v1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object v0

    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized downloadDir(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/sftp/Sftp;->downloadDir(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized downloadDir(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/sftp/Sftp;->downloadDir(Ljava/lang/String;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized downloadDir(Ljava/lang/String;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/jscape/inet/sftp/Sftp;->downloadDir(Ljava/lang/String;III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized downloadDir(Ljava/lang/String;III)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v8, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 p1, 0x1

    add-int/2addr p2, p1

    invoke-static {p2, p1}, Ljava/lang/Math;->max(II)I

    move-result v4

    int-to-long p1, p3

    invoke-static {p1, p2}, Lcom/jscape/util/Time;->seconds(J)Lcom/jscape/util/Time;

    move-result-object v5

    const-string v6, "/"

    if-lez p4, :cond_0

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    move-object v7, p1

    move-object v0, v8

    move-object v1, p0

    :try_start_1
    invoke-direct/range {v0 .. v7}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;ZILcom/jscape/util/Time;Ljava/lang/String;Ljava/lang/Integer;)V

    iput-object v8, p0, Lcom/jscape/inet/sftp/Sftp;->B:Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;

    iget-object p1, p0, Lcom/jscape/inet/sftp/Sftp;->B:Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;

    invoke-virtual {p1, p0}, Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;->applyTo(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$DownloadDirectoryOperation;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    monitor-exit p0

    throw p1
.end method

.method public enableFileAccessLogging()V
    .locals 2

    sget-object v0, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/16 v1, 0x17

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    return-void
.end method

.method public declared-synchronized exists(Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v0, p1}, Lcom/jscape/inet/sftp/FileService;->exists(Ljava/lang/String;)Z

    move-result p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return p1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getBlockSize()I
    .locals 1

    invoke-virtual {p0}, Lcom/jscape/inet/sftp/Sftp;->getUploadBlockSize()I

    move-result v0

    return v0
.end method

.method public getConfiguration()Lcom/jscape/inet/sftp/SftpConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->j:Lcom/jscape/inet/sftp/SftpConfiguration;

    return-object v0
.end method

.method public getDebug()Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->q:Ljava/util/logging/Logger;

    invoke-virtual {v0}, Ljava/util/logging/Logger;->getLevel()Ljava/util/logging/Level;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public getDebugStream()Ljava/io/PrintStream;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->q:Ljava/util/logging/Logger;

    if-eqz v0, :cond_1

    instance-of v0, v1, Lcom/jscape/util/j/b;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->q:Ljava/util/logging/Logger;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    check-cast v1, Lcom/jscape/util/j/b;

    invoke-virtual {v1}, Lcom/jscape/util/j/b;->a()Ljava/io/OutputStream;

    move-result-object v0

    check-cast v0, Ljava/io/PrintStream;

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public declared-synchronized getDir()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDirListing()Ljava/util/Enumeration;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/16 v1, 0xd

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/sftp/Sftp;->getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lcom/jscape/inet/sftp/Sftp;->getDirListing(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    return-object p1
.end method

.method public getDirListing(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v1, p1}, Lcom/jscape/inet/sftp/FileService;->listDirectory(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    new-instance v1, Lcom/jscape/inet/sftp/PathInfo$NaturalComparator;

    invoke-direct {v1}, Lcom/jscape/inet/sftp/PathInfo$NaturalComparator;-><init>()V

    invoke-static {p1, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/sftp/PathInfo;

    iget-object v3, v2, Lcom/jscape/inet/sftp/PathInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, v2}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/PathInfo;)Lcom/jscape/inet/sftp/SftpFile;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_1
    if-nez v0, :cond_0

    :cond_2
    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
.end method

.method public declared-synchronized getDirListingAsString()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/16 v1, 0xd

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/sftp/Sftp;->getDirListingAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDirListingAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/sftp/SftpFile;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_2

    add-int/lit8 v4, v2, 0x1

    if-lez v2, :cond_0

    :try_start_1
    sget-object v2, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/16 v5, 0x8

    aget-object v2, v2, v5
    :try_end_1
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    const-string v2, ""

    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/jscape/inet/sftp/SftpFile;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v1, :cond_1

    goto :goto_2

    :cond_1
    move v2, v4

    goto :goto_0

    :cond_2
    :goto_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getDownloadBlockSize()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/sftp/Sftp;->t:I

    return v0
.end method

.method public declared-synchronized getFileAccessTimestamp(Ljava/lang/String;)Ljava/util/Date;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/jscape/inet/sftp/FileService;->accessTimeOf(Ljava/lang/String;Z)J

    move-result-wide v0

    new-instance p1, Ljava/util/Date;

    invoke-direct {p1, v0, v1}, Ljava/util/Date;-><init>(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getFileCreationTimestamp(Ljava/lang/String;)Ljava/util/Date;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/jscape/inet/sftp/FileService;->createTimeOf(Ljava/lang/String;Z)J

    move-result-wide v0

    new-instance p1, Ljava/util/Date;

    invoke-direct {p1, v0, v1}, Ljava/util/Date;-><init>(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getFilePermissions(Ljava/lang/String;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2}, Lcom/jscape/inet/sftp/FileService;->permissionsOf(Ljava/lang/String;Z)Lcom/jscape/util/e/PosixFilePermissions;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_2
    new-instance p1, Lcom/jscape/inet/sftp/SftpException;

    sget-object v0, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/4 v1, 0x7

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Lcom/jscape/inet/sftp/SftpException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-static {p1}, Lcom/jscape/util/e/a;->a(Lcom/jscape/util/e/PosixFilePermissions;)I

    move-result p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return p1

    :catch_1
    move-exception p1

    :try_start_4
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/jscape/inet/sftp/FileService;->modificationTimeOf(Ljava/lang/String;Z)J

    move-result-wide v0

    new-instance p1, Ljava/util/Date;

    invoke-direct {p1, v0, v1}, Ljava/util/Date;-><init>(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getFilesize(Ljava/lang/String;)J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/jscape/inet/sftp/FileService;->sizeOf(Ljava/lang/String;Z)J

    move-result-wide v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-wide v0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getHostKeys()Lcom/jscape/inet/ssh/util/SshHostKeys;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->s:Lcom/jscape/inet/ssh/util/SshHostKeys;

    return-object v0
.end method

.method public getInputStream(Ljava/lang/String;J)Ljava/io/InputStream;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    :try_start_0
    invoke-static {p0, p1, p2, p3}, Lcom/jscape/inet/sftp/Sftp$SftpPipedInputStream;->streamFor(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;J)Lcom/jscape/inet/sftp/Sftp$SftpPipedInputStream;

    move-result-object p1

    iget-object p2, p0, Lcom/jscape/inet/sftp/Sftp;->y:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p2, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
.end method

.method public getLineTerminator()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalDir()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->o:Ljava/nio/file/Path;

    invoke-interface {v0}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getLocalDirListing()Ljava/util/Enumeration;
    .locals 2

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/sftp/Sftp;->getLocalDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getLocalDirListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 3

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->o:Ljava/nio/file/Path;

    invoke-interface {v1}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object v1

    new-instance v2, Lcom/jscape/inet/sftp/Sftp$1;

    invoke-direct {v2, p0, p1}, Lcom/jscape/inet/sftp/Sftp$1;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object p1

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/util/Vector;

    invoke-direct {p1}, Ljava/util/Vector;-><init>()V

    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    goto :goto_1

    :cond_1
    :goto_0
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->enumeration(Ljava/util/Collection;)Ljava/util/Enumeration;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public getLogLevel()Ljava/util/logging/Level;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->q:Ljava/util/logging/Logger;

    invoke-virtual {v0}, Ljava/util/logging/Logger;->getLevel()Ljava/util/logging/Level;

    move-result-object v0

    return-object v0
.end method

.method public getMode()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/sftp/Sftp;->w:I

    return v0
.end method

.method public declared-synchronized getNameListing()Ljava/util/Enumeration;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/16 v1, 0xd

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/sftp/Sftp;->getNameListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNameListing(Ljava/lang/String;)Ljava/util/Enumeration;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/sftp/SftpFile;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {v2}, Lcom/jscape/inet/sftp/SftpFile;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getOutputStream(Ljava/lang/String;JZ)Ljava/io/OutputStream;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    if-eqz p4, :cond_0

    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->getFilesize(Ljava/lang/String;)J

    move-result-wide p2

    :cond_0
    move-wide v2, p2

    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    :try_start_0
    invoke-static/range {v0 .. v5}, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->streamFor(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;

    move-result-object p1

    iget-object p2, p0, Lcom/jscape/inet/sftp/Sftp;->y:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p2, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
.end method

.method public getParameters()Lcom/jscape/inet/ssh/util/SshParameters;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    return-object v0
.end method

.method public getPipelineWindowSize()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/sftp/Sftp;->v:I

    return v0
.end method

.method public getRealPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/jscape/inet/sftp/FileService;->infoAbout(Ljava/lang/String;Z)Lcom/jscape/inet/sftp/PathInfo;

    move-result-object p1

    iget-object p1, p1, Lcom/jscape/inet/sftp/PathInfo;->name:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
.end method

.method public declared-synchronized getRecursiveDirectoryFileCount(Ljava/lang/String;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    const/4 v1, 0x0

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/sftp/SftpFile;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v2}, Lcom/jscape/inet/sftp/SftpFile;->isDirectory()Z

    move-result v2
    :try_end_1
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_3

    if-nez v2, :cond_1

    add-int/lit8 v1, v1, 0x1

    :cond_1
    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    :goto_0
    move v2, v1

    :cond_3
    monitor-exit p0

    return v2

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getRecursiveDirectorySize(Ljava/lang/String;)J
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    const-wide/16 v1, 0x0

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/sftp/SftpFile;

    invoke-virtual {v3}, Lcom/jscape/inet/sftp/SftpFile;->getFilesize()J

    move-result-wide v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-long/2addr v1, v3

    if-nez v0, :cond_1

    if-eqz v0, :cond_0

    :cond_1
    monitor-exit p0

    return-wide v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getRemoteFileList(Ljava/lang/String;)Ljava/util/Vector;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/Vector;

    new-instance v1, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;

    invoke-direct {v1, p1}, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->applyTo(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;

    move-result-object p1

    invoke-virtual {p1}, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->files()Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/Vector;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getSshParameters()Lcom/jscape/inet/ssh/util/SshParameters;
    .locals 1

    invoke-virtual {p0}, Lcom/jscape/inet/sftp/Sftp;->getParameters()Lcom/jscape/inet/ssh/util/SshParameters;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getSymbolicLinkTargetPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, Lcom/jscape/inet/sftp/FileService;->infoAbout(Ljava/lang/String;Z)Lcom/jscape/inet/sftp/PathInfo;

    move-result-object p1

    iget-object p1, p1, Lcom/jscape/inet/sftp/PathInfo;->name:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getTimeout()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/SshParameters;->getConnectionTimeout()J

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized getType(Ljava/lang/String;)Lcom/jscape/util/e/UnixFileType;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/jscape/inet/sftp/FileService;->typeOf(Ljava/lang/String;Z)Lcom/jscape/util/e/UnixFileType;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object p1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getUploadBlockSize()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/sftp/Sftp;->u:I

    return v0
.end method

.method public getVersion()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/sftp/Sftp;->k:I

    return v0
.end method

.method public interrupt()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp;->x:Z

    return-void
.end method

.method public interrupted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp;->x:Z

    return v0
.end method

.method public isAutomaticVersionLoweringEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp;->l:Z

    return v0
.end method

.method public declared-synchronized isConnected()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_2

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-interface {v1}, Lcom/jscape/inet/sftp/FileService;->h()Z

    move-result v1
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_2

    :try_start_4
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v1}, Lcom/jscape/inet/sftp/FileService;->available()Z

    move-result v1
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_1
    if-eqz v0, :cond_3

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    move v0, v1

    :goto_1
    monitor-exit p0

    return v0

    :catch_0
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_1
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_2
    move-exception v0

    :try_start_7
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catch_3
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isDirectory(Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->getType(Ljava/lang/String;)Lcom/jscape/util/e/UnixFileType;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v0, Lcom/jscape/util/e/UnixFileType;->DIRECTORY:Lcom/jscape/util/e/UnixFileType;
    :try_end_1
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    monitor-exit p0

    return p1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public isPipelinedTransferEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp;->m:Z

    return v0
.end method

.method public declared-synchronized makeDir(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->ownerDirectoryPermissions()Lcom/jscape/util/e/PosixFilePermissions;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/jscape/inet/sftp/FileService;->createDirectory(Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;)V

    new-instance v0, Lcom/jscape/inet/sftp/events/SftpCreateDirEvent;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/inet/sftp/events/SftpCreateDirEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/events/SftpEvent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized makeDirRecursive(Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    const-string v1, ""

    array-length v2, p1

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v2, :cond_3

    aget-object v4, p1, v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_3

    if-eqz v0, :cond_2

    :try_start_2
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v5, :cond_1

    :try_start_3
    invoke-direct {p0, v1, v4}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_2

    :try_start_4
    invoke-virtual {p0, v1}, Lcom/jscape/inet/sftp/Sftp;->exists(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->ownerDirectoryPermissions()Lcom/jscape/util/e/PosixFilePermissions;

    move-result-object v5

    invoke-interface {v4, v1, v5}, Lcom/jscape/inet/sftp/FileService;->createDirectory(Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;)V

    new-instance v4, Lcom/jscape/inet/sftp/events/SftpCreateDirEvent;

    iget-object v5, p0, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    invoke-direct {v4, p0, v1, v5}, Lcom/jscape/inet/sftp/events/SftpCreateDirEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v4}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/events/SftpEvent;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    :goto_1
    if-nez v0, :cond_0

    :cond_3
    monitor-exit p0

    return-void

    :catch_2
    move-exception p1

    :try_start_6
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public makeLocalDir(Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->o:Ljava/nio/file/Path;

    invoke-interface {v0, p1}, Ljava/nio/file/Path;->resolve(Ljava/lang/String;)Ljava/nio/file/Path;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/nio/file/attribute/FileAttribute;

    invoke-static {p1, v0}, Ljava/nio/file/Files;->createDirectories(Ljava/nio/file/Path;[Ljava/nio/file/attribute/FileAttribute;)Ljava/nio/file/Path;

    move-result-object p1

    invoke-interface {p1}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
.end method

.method public declared-synchronized mdelete(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/sftp/SftpFile;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {v1}, Lcom/jscape/inet/sftp/SftpFile;->isDirectory()Z

    move-result v2
    :try_end_1
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {v1}, Lcom/jscape/inet/sftp/SftpFile;->getFilename()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/jscape/inet/sftp/Sftp;->deleteDir(Ljava/lang/String;Z)V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    :goto_0
    if-nez v0, :cond_3

    :cond_2
    :try_start_4
    invoke-virtual {v1}, Lcom/jscape/inet/sftp/SftpFile;->getFilename()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/jscape/inet/sftp/Sftp;->deleteFile(Ljava/lang/String;)V
    :try_end_4
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_2
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_3
    :goto_1
    if-nez v0, :cond_0

    :cond_4
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized mdownload(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->getDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_3

    if-eqz v0, :cond_1

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/inet/sftp/Sftp;->x:Z
    :try_end_1
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_3

    :try_start_2
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    move-object v1, p0

    :goto_0
    check-cast v1, Lcom/jscape/inet/sftp/SftpFile;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v1}, Lcom/jscape/inet/sftp/SftpFile;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/jscape/inet/sftp/SftpFile;->getFilename()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/jscape/inet/sftp/Sftp;->download(Ljava/lang/String;)Ljava/io/File;
    :try_end_4
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_2
    if-nez v0, :cond_0

    goto :goto_1

    :catch_2
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_3
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized mdownload(Ljava/util/Enumeration;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_2

    if-nez v0, :cond_1

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/inet/sftp/Sftp;->x:Z
    :try_end_1
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_2

    :try_start_2
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    move-object v1, p0

    :goto_0
    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/jscape/inet/sftp/Sftp;->download(Ljava/lang/String;)Ljava/io/File;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized mupload(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->getLocalDirListing(Ljava/lang/String;)Ljava/util/Enumeration;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_3

    if-eqz v0, :cond_1

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/inet/sftp/Sftp;->x:Z
    :try_end_1
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_3

    :try_start_2
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    move-object v1, p0

    :goto_0
    check-cast v1, Ljava/io/File;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0, v1}, Lcom/jscape/inet/sftp/Sftp;->upload(Ljava/io/File;)V
    :try_end_4
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_2
    if-nez v0, :cond_0

    goto :goto_1

    :catch_2
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_3
    :goto_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized mupload(Ljava/util/Enumeration;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    :cond_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_2

    if-eqz v0, :cond_1

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/inet/sftp/Sftp;->x:Z
    :try_end_1
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_2

    :try_start_2
    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    move-object v1, p0

    :goto_0
    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/jscape/inet/sftp/Sftp;->upload(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v0, :cond_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public removeSftpListener(Lcom/jscape/inet/sftp/events/SftpListener;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->r:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public declared-synchronized renameFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p2}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v0, p1, p2}, Lcom/jscape/inet/sftp/FileService;->rename(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/jscape/inet/sftp/events/SftpRenameFileEvent;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/jscape/inet/sftp/events/SftpRenameFileEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/events/SftpEvent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp;->x:Z

    return-void
.end method

.method public declared-synchronized resumeDownload(Ljava/lang/String;J)Ljava/io/File;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1, p1, p2, p3}, Lcom/jscape/inet/sftp/Sftp;->resumeDownload(Ljava/lang/String;Ljava/lang/String;J)Ljava/io/File;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized resumeDownload(Ljava/lang/String;Ljava/lang/String;J)Ljava/io/File;
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    move-wide/from16 v11, p3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    const/4 v1, 0x0

    :try_start_1
    invoke-direct {v9, v10}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9, v13}, Lcom/jscape/inet/sftp/Sftp;->getFilesize(Ljava/lang/String;)J

    move-result-wide v2

    sub-long v7, v2, v11

    invoke-direct/range {p0 .. p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v14
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    iget v2, v9, Lcom/jscape/inet/sftp/Sftp;->w:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const/4 v15, 0x1

    if-eqz v0, :cond_1

    if-ne v2, v15, :cond_0

    move v2, v15

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :cond_1
    :goto_0
    move/from16 v16, v2

    :try_start_3
    invoke-static {v14, v11, v12, v15}, Lcom/jscape/util/h/r;->a(Ljava/io/File;JZ)Lcom/jscape/util/h/r;

    move-result-object v2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    new-instance v5, Lcom/jscape/util/h/p;

    if-eqz v16, :cond_2

    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    instance-of v3, v3, Lcom/jscape/inet/sftp/SftpFileService3;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    if-eqz v3, :cond_2

    :try_start_5
    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    sget-object v4, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    invoke-static {v2, v3}, Lcom/jscape/util/h/q;->a(Ljava/io/OutputStream;[B)Lcom/jscape/util/h/q;

    move-result-object v2

    :cond_2
    invoke-direct {v5, v2}, Lcom/jscape/util/h/p;-><init>(Ljava/io/OutputStream;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_8
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v17, v5

    move-wide/from16 v5, p3

    :try_start_7
    invoke-direct/range {v1 .. v8}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;

    move-result-object v18

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v19
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v0, :cond_3

    :try_start_8
    iget-boolean v1, v9, Lcom/jscape/inet/sftp/Sftp;->m:Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v1, :cond_3

    :try_start_9
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    iget-object v8, v9, Lcom/jscape/inet/sftp/Sftp;->y:Ljava/util/concurrent/ExecutorService;

    move-object v2, v13

    move-wide/from16 v3, p3

    move-object/from16 v5, v17

    move/from16 v6, v16

    move-object/from16 v7, v18

    invoke-interface/range {v1 .. v8}, Lcom/jscape/inet/sftp/FileService;->readFile(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V

    if-nez v0, :cond_4

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_1
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_3
    :goto_1
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    move-object v2, v13

    move-wide/from16 v3, p3

    move-object/from16 v5, v17

    move/from16 v6, v16

    move-object/from16 v7, v18

    invoke-interface/range {v1 .. v7}, Lcom/jscape/inet/sftp/FileService;->readFile(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/sftp/FileService$TransferListener;)V

    :cond_4
    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v17 .. v17}, Lcom/jscape/util/h/p;->a()J

    move-result-wide v4

    invoke-static/range {v19 .. v20}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v7}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :try_start_b
    invoke-static/range {v17 .. v17}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v1

    if-nez v1, :cond_5

    add-int/2addr v0, v15

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->b(I)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    :cond_5
    monitor-exit p0

    return-object v14

    :catch_2
    move-exception v0

    :try_start_c
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_3

    :catchall_1
    move-exception v0

    move-object/from16 v17, v5

    :goto_2
    move-object/from16 v1, v17

    goto :goto_5

    :catch_4
    move-exception v0

    move-object/from16 v17, v5

    :goto_3
    move-object/from16 v2, p1

    move-object/from16 v1, v17

    goto :goto_4

    :catch_5
    move-exception v0

    :try_start_d
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_8
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    :catch_6
    move-exception v0

    move-object v2, v0

    :try_start_e
    invoke-static {v2}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_7
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    :catch_7
    move-exception v0

    :try_start_f
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_8
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    :catchall_2
    move-exception v0

    goto :goto_5

    :catch_8
    move-exception v0

    move-object/from16 v2, p1

    :goto_4
    :try_start_10
    invoke-direct {v9, v10, v2}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object v0

    throw v0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    :goto_5
    :try_start_11
    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    throw v0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    :catchall_3
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public resumeDownload(Ljava/io/OutputStream;Ljava/lang/String;J)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    invoke-direct {v9, v10}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/jscape/inet/sftp/Sftp;->getFilesize(Ljava/lang/String;)J

    move-result-wide v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    sub-long v7, v1, p3

    :try_start_1
    iget v1, v9, Lcom/jscape/inet/sftp/Sftp;->w:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    const/4 v2, 0x1

    if-nez v0, :cond_1

    if-ne v1, v2, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    move v12, v1

    :try_start_2
    new-instance v13, Lcom/jscape/util/h/p;

    if-eqz v12, :cond_2

    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    instance-of v1, v1, Lcom/jscape/inet/sftp/SftpFileService3;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v1, :cond_2

    :try_start_3
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    sget-object v2, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lcom/jscape/util/h/q;->a(Ljava/io/OutputStream;[B)Lcom/jscape/util/h/q;

    move-result-object v1

    goto :goto_1

    :cond_2
    move-object/from16 v2, p1

    move-object v1, v2

    :goto_1
    invoke-direct {v13, v1}, Lcom/jscape/util/h/p;-><init>(Ljava/io/OutputStream;)V

    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    const-string v4, ""

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-wide/from16 v5, p3

    invoke-direct/range {v1 .. v8}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;

    move-result-object v14

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v15
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5

    if-nez v0, :cond_3

    :try_start_4
    iget-boolean v1, v9, Lcom/jscape/inet/sftp/Sftp;->m:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    if-eqz v1, :cond_3

    :try_start_5
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    iget-object v8, v9, Lcom/jscape/inet/sftp/Sftp;->y:Ljava/util/concurrent/ExecutorService;

    move-object v2, v11

    move-wide/from16 v3, p3

    move-object v5, v13

    move v6, v12

    move-object v7, v14

    invoke-interface/range {v1 .. v8}, Lcom/jscape/inet/sftp/FileService;->readFile(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V

    if-eqz v0, :cond_4

    goto :goto_2

    :catch_0
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_3
    :goto_2
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    move-object v2, v11

    move-wide/from16 v3, p3

    move-object v5, v13

    move v6, v12

    move-object v7, v14

    invoke-interface/range {v1 .. v7}, Lcom/jscape/inet/sftp/FileService;->readFile(Ljava/lang/String;JLjava/io/OutputStream;ZLcom/jscape/inet/sftp/FileService$TransferListener;)V

    :cond_4
    const-string v3, ""

    invoke-virtual {v13}, Lcom/jscape/util/h/p;->a()J

    move-result-wide v4

    invoke-static/range {v15 .. v16}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v7}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/String;Ljava/lang/String;JJ)V

    return-void

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    :catch_3
    move-exception v0

    move-object v1, v0

    :try_start_7
    invoke-static {v1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    :catch_4
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    :catch_5
    move-exception v0

    const-string v1, ""

    invoke-direct {v9, v10, v1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object v0

    throw v0
.end method

.method public declared-synchronized resumeUpload(Ljava/io/File;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/jscape/inet/sftp/Sftp;->resumeUpload(Ljava/io/File;Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized resumeUpload(Ljava/io/File;Ljava/lang/String;J)V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    move-wide/from16 v11, p3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    const/4 v1, 0x0

    :try_start_1
    invoke-direct {v9, v10}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    sub-long v7, v2, v11

    :try_start_2
    iget v2, v9, Lcom/jscape/inet/sftp/Sftp;->w:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const/4 v3, 0x1

    if-nez v0, :cond_1

    if-ne v2, v3, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :cond_1
    :goto_0
    move-object/from16 v15, p1

    move v14, v2

    :try_start_3
    invoke-static {v15, v11, v12}, Lcom/jscape/util/h/i;->a(Ljava/io/File;J)Lcom/jscape/util/h/i;

    move-result-object v2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    new-instance v5, Lcom/jscape/util/h/f;

    if-eqz v14, :cond_2

    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    instance-of v3, v3, Lcom/jscape/inet/sftp/SftpFileService3;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    if-eqz v3, :cond_2

    :try_start_5
    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    sget-object v4, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    invoke-static {v2, v3}, Lcom/jscape/util/h/h;->a(Ljava/io/InputStream;[B)Lcom/jscape/util/h/h;

    move-result-object v2

    :cond_2
    invoke-direct {v5, v2}, Lcom/jscape/util/h/f;-><init>(Ljava/io/InputStream;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-object/from16 v1, p0

    move-object/from16 v16, v5

    move-wide/from16 v5, p3

    :try_start_7
    invoke-direct/range {v1 .. v8}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;

    move-result-object v17

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v18
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-nez v0, :cond_3

    :try_start_8
    iget-boolean v1, v9, Lcom/jscape/inet/sftp/Sftp;->m:Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v1, :cond_3

    :try_start_9
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    iget-object v8, v9, Lcom/jscape/inet/sftp/Sftp;->y:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v2, v16

    move-object v3, v13

    move-wide/from16 v4, p3

    move v6, v14

    move-object/from16 v7, v17

    invoke-interface/range {v1 .. v8}, Lcom/jscape/inet/sftp/FileService;->writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V

    if-eqz v0, :cond_4

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_1
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_3
    :goto_1
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    move-object/from16 v2, v16

    move-object v3, v13

    move-wide/from16 v4, p3

    move v6, v14

    move-object/from16 v7, v17

    invoke-interface/range {v1 .. v7}, Lcom/jscape/inet/sftp/FileService;->writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;)V

    :cond_4
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {v16 .. v16}, Lcom/jscape/util/h/f;->a()J

    move-result-wide v4

    invoke-static/range {v18 .. v19}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :try_start_b
    invoke-static/range {v16 .. v16}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_3

    :catchall_1
    move-exception v0

    move-object/from16 v16, v5

    :goto_2
    move-object/from16 v1, v16

    goto :goto_5

    :catch_3
    move-exception v0

    move-object/from16 v16, v5

    :goto_3
    move-object/from16 v1, v16

    goto :goto_4

    :catch_4
    move-exception v0

    :try_start_c
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :catch_5
    move-exception v0

    goto :goto_4

    :catch_6
    move-exception v0

    move-object/from16 v15, p1

    move-object v2, v0

    :try_start_d
    invoke-static {v2}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_7
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    :catch_7
    move-exception v0

    :try_start_e
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_5
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    :catchall_2
    move-exception v0

    goto :goto_5

    :catch_8
    move-exception v0

    move-object/from16 v15, p1

    :goto_4
    :try_start_f
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2, v10}, Lcom/jscape/inet/sftp/Sftp;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object v0

    throw v0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    :goto_5
    :try_start_10
    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    throw v0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    :catchall_3
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public resumeUpload(Ljava/io/InputStream;JLjava/lang/String;J)V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p4

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    invoke-direct {v9, v10}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5

    :try_start_1
    iget v1, v9, Lcom/jscape/inet/sftp/Sftp;->w:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    const/4 v2, 0x1

    if-nez v0, :cond_1

    if-ne v1, v2, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    move v12, v1

    :try_start_2
    new-instance v13, Lcom/jscape/util/h/f;

    if-eqz v12, :cond_2

    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    instance-of v1, v1, Lcom/jscape/inet/sftp/SftpFileService3;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v1, :cond_2

    :try_start_3
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    sget-object v2, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lcom/jscape/util/h/h;->a(Ljava/io/InputStream;[B)Lcom/jscape/util/h/h;

    move-result-object v1

    goto :goto_1

    :cond_2
    move-object/from16 v2, p1

    move-object v1, v2

    :goto_1
    invoke-direct {v13, v1}, Lcom/jscape/util/h/f;-><init>(Ljava/io/InputStream;)V

    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    const-string v4, ""

    move-object/from16 v1, p0

    move-object/from16 v2, p4

    move-wide/from16 v5, p5

    move-wide/from16 v7, p2

    invoke-direct/range {v1 .. v8}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;

    move-result-object v14

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v15
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5

    if-nez v0, :cond_3

    :try_start_4
    iget-boolean v1, v9, Lcom/jscape/inet/sftp/Sftp;->m:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    if-eqz v1, :cond_3

    :try_start_5
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    iget-object v8, v9, Lcom/jscape/inet/sftp/Sftp;->y:Ljava/util/concurrent/ExecutorService;

    move-object v2, v13

    move-object v3, v11

    move-wide/from16 v4, p5

    move v6, v12

    move-object v7, v14

    invoke-interface/range {v1 .. v8}, Lcom/jscape/inet/sftp/FileService;->writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V

    if-eqz v0, :cond_4

    goto :goto_2

    :catch_0
    move-exception v0

    move-object v1, v0

    invoke-static {v1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_3
    :goto_2
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    move-object v2, v13

    move-object v3, v11

    move-wide/from16 v4, p5

    move v6, v12

    move-object v7, v14

    invoke-interface/range {v1 .. v7}, Lcom/jscape/inet/sftp/FileService;->writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;)V

    :cond_4
    const-string v2, ""

    invoke-virtual {v13}, Lcom/jscape/util/h/f;->a()J

    move-result-wide v4

    invoke-static/range {v15 .. v16}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v3, p4

    invoke-direct/range {v1 .. v7}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;Ljava/lang/String;JJ)V

    return-void

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    :catch_3
    move-exception v0

    move-object v1, v0

    :try_start_7
    invoke-static {v1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    :catch_4
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    :catch_5
    move-exception v0

    const-string v1, ""

    invoke-direct {v9, v1, v10}, Lcom/jscape/inet/sftp/Sftp;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object v0

    throw v0
.end method

.method public declared-synchronized resumeUpload(Ljava/lang/String;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/inet/sftp/Sftp;->resumeUpload(Ljava/io/File;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized resumeUpload(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/jscape/inet/sftp/Sftp;->resumeUpload(Ljava/io/File;Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized sameChecksum(Ljava/io/File;Ljava/lang/String;)Z
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object v1

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/nio/file/OpenOption;

    invoke-static {v1, v3}, Ljava/nio/file/Files;->newInputStream(Ljava/nio/file/Path;[Ljava/nio/file/OpenOption;)Ljava/io/InputStream;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/sftp/Checksum;->MD5:Lcom/jscape/inet/sftp/Checksum;

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v3

    invoke-virtual {v1, v0, v3, v4, v2}, Lcom/jscape/inet/sftp/Checksum;->of(Ljava/io/InputStream;JI)[B

    move-result-object p1

    invoke-direct {p0, p2}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    const-wide/16 v5, 0x0

    invoke-virtual {p0, p2}, Lcom/jscape/inet/sftp/Sftp;->getFilesize(Ljava/lang/String;)J

    move-result-wide v7

    const/4 v9, 0x0

    const/4 p2, 0x1

    new-array v10, p2, [Ljava/lang/String;

    sget-object p2, Lcom/jscape/inet/sftp/Checksum;->MD5:Lcom/jscape/inet/sftp/Checksum;

    iget-object p2, p2, Lcom/jscape/inet/sftp/Checksum;->code:Ljava/lang/String;

    aput-object p2, v10, v2

    invoke-interface/range {v3 .. v10}, Lcom/jscape/inet/sftp/FileService;->hashOf(Ljava/lang/String;JJI[Ljava/lang/String;)Lcom/jscape/inet/sftp/HashInfo;

    move-result-object p2

    iget-object p2, p2, Lcom/jscape/inet/sftp/HashInfo;->hash:[B

    invoke-static {p1, p2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    :try_start_3
    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setAscii()V
    .locals 1

    const/4 v0, 0x1

    iput v0, p0, Lcom/jscape/inet/sftp/Sftp;->w:I

    return-void
.end method

.method public setAuto(Z)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    :cond_1
    :goto_0
    iput p1, p0, Lcom/jscape/inet/sftp/Sftp;->w:I

    return-void
.end method

.method public setAutomaticVersionLoweringEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/sftp/Sftp;->l:Z

    return-void
.end method

.method public setBinary()V
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, Lcom/jscape/inet/sftp/Sftp;->w:I

    return-void
.end method

.method public setBlockSize(I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->setUploadBlockSize(I)V

    return-void
.end method

.method public setConfiguration(Lcom/jscape/inet/sftp/SftpConfiguration;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp;->j:Lcom/jscape/inet/sftp/SftpConfiguration;

    return-void
.end method

.method public setDebug(Z)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->q:Ljava/util/logging/Logger;

    if-eqz p1, :cond_0

    sget-object p1, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    sget-object p1, Ljava/util/logging/Level;->OFF:Ljava/util/logging/Level;

    :goto_0
    invoke-virtual {v0, p1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public setDebugStream(Ljava/io/PrintStream;)V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->q:Ljava/util/logging/Logger;

    if-eqz v0, :cond_0

    instance-of v0, v1, Lcom/jscape/util/j/b;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->q:Ljava/util/logging/Logger;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    check-cast v1, Lcom/jscape/util/j/b;

    invoke-virtual {v1, p1}, Lcom/jscape/util/j/b;->a(Ljava/io/OutputStream;)V

    :cond_1
    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public declared-synchronized setDir(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    const/4 v3, 0x1

    invoke-interface {v2, v1, v3}, Lcom/jscape/inet/sftp/FileService;->typeOf(Ljava/lang/String;Z)Lcom/jscape/util/e/UnixFileType;

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    :try_start_2
    sget-object v0, Lcom/jscape/util/e/UnixFileType;->DIRECTORY:Lcom/jscape/util/e/UnixFileType;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-ne v2, v0, :cond_0

    :try_start_3
    iput-object v1, p0, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    new-instance p1, Lcom/jscape/inet/sftp/events/SftpChangeDirEvent;

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    invoke-direct {p1, p0, v0}, Lcom/jscape/inet/sftp/events/SftpChangeDirEvent;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/events/SftpEvent;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :cond_0
    :try_start_4
    new-instance v0, Ljava/lang/Exception;

    sget-object v1, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_0
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catch_1
    move-exception p1

    :try_start_6
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setDirUp()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/16 v1, 0xe

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/inet/sftp/Sftp;->setDir(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setDownloadBlockSize(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/sftp/Sftp;->t:I

    return-void
.end method

.method public setFileAccessTimestamp(Ljava/lang/String;Ljava/util/Date;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-interface {v0, p1, v1, v2}, Lcom/jscape/inet/sftp/FileService;->accessTime(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
.end method

.method public setFileCreationTimestamp(Ljava/lang/String;Ljava/util/Date;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-interface {v0, p1, v1, v2}, Lcom/jscape/inet/sftp/FileService;->createTime(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
.end method

.method public declared-synchronized setFilePermissions(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p2}, Lcom/jscape/util/e/a;->a(I)Lcom/jscape/util/e/PosixFilePermissions;

    move-result-object p2

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v0, p1, p2}, Lcom/jscape/inet/sftp/FileService;->permissions(Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp;->i()V

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-interface {v0, p1, v1, v2}, Lcom/jscape/inet/sftp/FileService;->modificationTime(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
.end method

.method public setLineTerminator(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_4

    if-nez v0, :cond_0

    if-eqz p1, :cond_3

    :cond_0
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_1

    if-nez p1, :cond_3

    :try_start_2
    iget-object p1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    if-nez v0, :cond_2

    :try_start_3
    instance-of p1, p1, Lcom/jscape/inet/sftp/SftpFileService4;
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_1
    if-eqz p1, :cond_3

    :try_start_4
    iget-object p1, p0, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0

    :cond_2
    check-cast p1, Lcom/jscape/inet/sftp/SftpFileService4;

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    sget-object v1, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jscape/inet/sftp/SftpFileService4;->systemEol([B)V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_0
    return-void

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :catch_4
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public setLocalDir(Ljava/io/File;)V
    .locals 3

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p1

    invoke-interface {p1}, Ljava/nio/file/Path;->normalize()Ljava/nio/file/Path;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp;->o:Ljava/nio/file/Path;

    return-void
.end method

.method public setLogLevel(Ljava/util/logging/Level;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->q:Ljava/util/logging/Logger;

    invoke-virtual {v0, p1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    return-void
.end method

.method public setParameters(Lcom/jscape/inet/ssh/util/SshParameters;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    return-void
.end method

.method public setPipelineWindowSize(I)V
    .locals 5

    iget v0, p0, Lcom/jscape/inet/sftp/Sftp;->v:I

    int-to-long v0, v0

    sget-object v2, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const-wide/16 v3, 0x0

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p1, p0, Lcom/jscape/inet/sftp/Sftp;->v:I

    return-void
.end method

.method public setPipelinedTransferEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/sftp/Sftp;->m:Z

    return-void
.end method

.method public setProxyAuthentication(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ssh/util/SshParameters;->setProxyUsername(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ssh/util/SshParameters;->setProxyPassword(Ljava/lang/String;)V

    return-void
.end method

.method public setProxyHost(Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ssh/util/SshParameters;->setProxyHost(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {p1, p2}, Lcom/jscape/inet/ssh/util/SshParameters;->setProxyPort(I)V

    return-void
.end method

.method public setProxyType(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/ssh/util/SshParameters;->setProxyType(Ljava/lang/String;)V

    return-void
.end method

.method public setReceiveBufferSize(I)V
    .locals 0

    return-void
.end method

.method public setSendBufferSize(I)V
    .locals 0

    return-void
.end method

.method public setTimeout(J)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ssh/util/SshParameters;->setConnectionTimeout(J)V

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp;->i:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/ssh/util/SshParameters;->setReadingTimeout(J)V

    return-void
.end method

.method public setUploadBlockSize(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/inet/sftp/Sftp;->u:I

    return-void
.end method

.method public setVersion(I)V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x3

    if-eq p1, v1, :cond_1

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    move v0, p1

    :goto_1
    sget-object v1, Lcom/jscape/inet/sftp/Sftp;->D:[Ljava/lang/String;

    const/16 v2, 0x16

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    iput p1, p0, Lcom/jscape/inet/sftp/Sftp;->k:I

    return-void
.end method

.method public declared-synchronized upload(Ljava/io/File;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/sftp/Sftp;->upload(Ljava/io/File;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/io/File;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/sftp/Sftp;->upload(Ljava/io/File;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/io/File;Ljava/lang/String;Z)V
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    const/4 v1, 0x0

    :try_start_1
    invoke-direct {v9, v10}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v7
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_11
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    const-wide/16 v2, 0x0

    const/4 v11, 0x3

    if-nez v0, :cond_0

    if-eqz p3, :cond_3

    :try_start_2
    iget v4, v9, Lcom/jscape/inet/sftp/Sftp;->k:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v2, v0

    :try_start_3
    invoke-static {v2}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_11
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :cond_0
    move/from16 v4, p3

    :goto_0
    if-nez v0, :cond_1

    if-ne v4, v11, :cond_3

    if-nez v0, :cond_2

    :try_start_4
    invoke-virtual {v9, v10}, Lcom/jscape/inet/sftp/Sftp;->exists(Ljava/lang/String;)Z

    move-result v4
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v2, v0

    :try_start_5
    invoke-static {v2}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_1
    if-eqz v4, :cond_3

    :cond_2
    invoke-virtual {v9, v10}, Lcom/jscape/inet/sftp/Sftp;->getFilesize(Ljava/lang/String;)J

    move-result-wide v4
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_11
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-wide/from16 v19, v4

    goto :goto_2

    :cond_3
    move-wide/from16 v19, v2

    :goto_2
    :try_start_6
    iget v4, v9, Lcom/jscape/inet/sftp/Sftp;->w:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_f
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    const/4 v5, 0x1

    if-nez v0, :cond_5

    if-ne v4, v5, :cond_4

    move v4, v5

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    :cond_5
    :goto_3
    move-object/from16 v13, p1

    move v15, v4

    :try_start_7
    invoke-static {v13, v2, v3}, Lcom/jscape/util/h/i;->a(Ljava/io/File;J)Lcom/jscape/util/h/i;

    move-result-object v2
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_11
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    :try_start_8
    new-instance v12, Lcom/jscape/util/h/f;

    if-eqz v15, :cond_6

    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    instance-of v3, v3, Lcom/jscape/inet/sftp/SftpFileService3;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_e
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    if-eqz v3, :cond_6

    :try_start_9
    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    if-eqz v3, :cond_6

    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    sget-object v4, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    invoke-static {v2, v3}, Lcom/jscape/util/h/h;->a(Ljava/io/InputStream;[B)Lcom/jscape/util/h/h;

    move-result-object v2

    :cond_6
    invoke-direct {v12, v2}, Lcom/jscape/util/h/f;-><init>(Ljava/io/InputStream;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_11
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    :try_start_a
    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v8}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;

    move-result-object v8

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v21
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_d
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    if-nez v0, :cond_b

    if-eqz p3, :cond_9

    :try_start_b
    iget v1, v9, Lcom/jscape/inet/sftp/Sftp;->k:I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    if-nez v0, :cond_c

    if-le v1, v11, :cond_9

    if-nez v0, :cond_7

    :try_start_c
    iget-boolean v1, v9, Lcom/jscape/inet/sftp/Sftp;->m:Z
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_6
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    if-eqz v1, :cond_7

    :try_start_d
    iget-object v2, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    iget-object v7, v9, Lcom/jscape/inet/sftp/Sftp;->y:Ljava/util/concurrent/ExecutorService;

    move-object v3, v12

    move-object v4, v14

    move v5, v15

    move-object v6, v8

    invoke-interface/range {v2 .. v7}, Lcom/jscape/inet/sftp/FileService;->appendFile(Ljava/io/InputStream;Ljava/lang/String;ZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V

    if-eqz v0, :cond_8

    const/4 v1, 0x4

    new-array v1, v1, [I

    invoke-static {v1}, Lcom/jscape/util/aq;->b([I)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_7
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :cond_7
    :try_start_e
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v1, v12, v14, v15, v8}, Lcom/jscape/inet/sftp/FileService;->appendFile(Ljava/io/InputStream;Ljava/lang/String;ZLcom/jscape/inet/sftp/FileService$TransferListener;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    if-eqz v0, :cond_8

    goto :goto_4

    :cond_8
    move-object/from16 v23, v12

    goto/16 :goto_8

    :catch_2
    move-exception v0

    :try_start_f
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_8
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :catchall_0
    move-exception v0

    move-object v1, v12

    goto/16 :goto_c

    :catch_3
    move-exception v0

    move-object v1, v0

    :try_start_10
    invoke-static {v1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_4
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :catch_4
    move-exception v0

    :try_start_11
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_5
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :catch_5
    move-exception v0

    :try_start_12
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_6
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :catch_6
    move-exception v0

    :try_start_13
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_7
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :catch_7
    move-exception v0

    :try_start_14
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_9
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    :cond_9
    :goto_4
    if-nez v0, :cond_a

    :try_start_15
    iget-boolean v1, v9, Lcom/jscape/inet/sftp/Sftp;->m:Z
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_8
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    goto :goto_5

    :catch_8
    move-exception v0

    :try_start_16
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_9
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    :catch_9
    move-exception v0

    move-object v1, v12

    goto/16 :goto_b

    :cond_a
    move-object/from16 v23, v12

    move-object v4, v14

    move v7, v15

    goto :goto_7

    :cond_b
    move/from16 v1, p3

    :cond_c
    :goto_5
    if-eqz v1, :cond_a

    :try_start_17
    iget-object v11, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->y:Ljava/util/concurrent/ExecutorService;
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_b
    .catchall {:try_start_17 .. :try_end_17} :catchall_2

    move-object/from16 v23, v12

    move-object v13, v14

    move-object v4, v14

    move v7, v15

    move-wide/from16 v14, v19

    move/from16 v16, v7

    move-object/from16 v17, v8

    move-object/from16 v18, v1

    :try_start_18
    invoke-interface/range {v11 .. v18}, Lcom/jscape/inet/sftp/FileService;->writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_a
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    if-eqz v0, :cond_d

    goto :goto_7

    :catch_a
    move-exception v0

    goto :goto_6

    :catch_b
    move-exception v0

    move-object/from16 v23, v12

    :goto_6
    :try_start_19
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :goto_7
    iget-object v2, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    move-object/from16 v3, v23

    move-wide/from16 v5, v19

    invoke-interface/range {v2 .. v8}, Lcom/jscape/inet/sftp/FileService;->writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;)V

    :cond_d
    :goto_8
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {v23 .. v23}, Lcom/jscape/util/h/f;->a()J

    move-result-wide v4

    invoke-static/range {v21 .. v22}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_c
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    :try_start_1a
    invoke-static/range {v23 .. v23}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_4

    monitor-exit p0

    return-void

    :catchall_1
    move-exception v0

    goto :goto_9

    :catch_c
    move-exception v0

    goto :goto_a

    :catchall_2
    move-exception v0

    move-object/from16 v23, v12

    :goto_9
    move-object/from16 v1, v23

    goto :goto_c

    :catch_d
    move-exception v0

    move-object/from16 v23, v12

    :goto_a
    move-object/from16 v1, v23

    goto :goto_b

    :catch_e
    move-exception v0

    :try_start_1b
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_11
    .catchall {:try_start_1b .. :try_end_1b} :catchall_3

    :catch_f
    move-exception v0

    move-object v2, v0

    :try_start_1c
    invoke-static {v2}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_10
    .catchall {:try_start_1c .. :try_end_1c} :catchall_3

    :catch_10
    move-exception v0

    :try_start_1d
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_11
    .catchall {:try_start_1d .. :try_end_1d} :catchall_3

    :catchall_3
    move-exception v0

    goto :goto_c

    :catch_11
    move-exception v0

    :goto_b
    :try_start_1e
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v9, v2, v10}, Lcom/jscape/inet/sftp/Sftp;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object v0

    throw v0
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_3

    :goto_c
    :try_start_1f
    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    throw v0
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_4

    :catchall_4
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized upload(Ljava/io/File;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/jscape/inet/sftp/Sftp;->upload(Ljava/io/File;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/sftp/Sftp;->upload(Ljava/io/InputStream;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/io/InputStream;Ljava/lang/String;Z)V
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-direct {v9, v10}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_d
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget v1, v9, Lcom/jscape/inet/sftp/Sftp;->w:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    if-ne v1, v2, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    move v12, v1

    :try_start_3
    new-instance v13, Lcom/jscape/util/h/f;

    if-eqz v12, :cond_2

    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    instance-of v1, v1, Lcom/jscape/inet/sftp/SftpFileService3;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_a
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v1, :cond_2

    :try_start_4
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->n:Ljava/lang/String;

    sget-object v2, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lcom/jscape/util/h/h;->a(Ljava/io/InputStream;[B)Lcom/jscape/util/h/h;

    move-result-object v1

    goto :goto_1

    :cond_2
    move-object/from16 v2, p1

    move-object v1, v2

    :goto_1
    invoke-direct {v13, v1}, Lcom/jscape/util/h/f;-><init>(Ljava/io/InputStream;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_d
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/4 v14, 0x3

    if-eqz v0, :cond_3

    if-eqz p3, :cond_6

    :try_start_5
    iget v1, v9, Lcom/jscape/inet/sftp/Sftp;->k:I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_6
    invoke-static {v1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_d
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_3
    move/from16 v1, p3

    :goto_2
    if-eqz v0, :cond_4

    if-ne v1, v14, :cond_6

    if-eqz v0, :cond_5

    :try_start_7
    invoke-virtual {v9, v10}, Lcom/jscape/inet/sftp/Sftp;->exists(Ljava/lang/String;)Z

    move-result v1
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v1, v0

    :try_start_8
    invoke-static {v1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_4
    :goto_3
    if-eqz v1, :cond_6

    :cond_5
    invoke-virtual {v9, v10}, Lcom/jscape/inet/sftp/Sftp;->getFilesize(Ljava/lang/String;)J

    move-result-wide v1

    goto :goto_4

    :cond_6
    const-wide/16 v1, 0x0

    :goto_4
    move-wide v15, v1

    iget-object v3, v9, Lcom/jscape/inet/sftp/Sftp;->p:Ljava/lang/String;

    const-string v4, ""

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v8}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/FileService$TransferListener;

    move-result-object v8

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v17
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_d
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    if-eqz v0, :cond_a

    if-eqz p3, :cond_8

    :try_start_9
    iget v1, v9, Lcom/jscape/inet/sftp/Sftp;->k:I
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-eqz v0, :cond_b

    if-le v1, v14, :cond_8

    if-eqz v0, :cond_7

    :try_start_a
    iget-boolean v1, v9, Lcom/jscape/inet/sftp/Sftp;->m:Z
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_6
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    if-eqz v1, :cond_7

    :try_start_b
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    iget-object v6, v9, Lcom/jscape/inet/sftp/Sftp;->y:Ljava/util/concurrent/ExecutorService;

    move-object v2, v13

    move-object v3, v11

    move v4, v12

    move-object v5, v8

    invoke-interface/range {v1 .. v6}, Lcom/jscape/inet/sftp/FileService;->appendFile(Ljava/io/InputStream;Ljava/lang/String;ZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_7
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    if-nez v0, :cond_c

    :cond_7
    :try_start_c
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    invoke-interface {v1, v13, v11, v12, v8}, Lcom/jscape/inet/sftp/FileService;->appendFile(Ljava/io/InputStream;Ljava/lang/String;ZLcom/jscape/inet/sftp/FileService$TransferListener;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    if-nez v0, :cond_c

    goto :goto_5

    :catch_2
    move-exception v0

    :try_start_d
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_8
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catch_3
    move-exception v0

    move-object v1, v0

    :try_start_e
    invoke-static {v1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_4
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :catch_4
    move-exception v0

    :try_start_f
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_5
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :catch_5
    move-exception v0

    :try_start_10
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    :catch_6
    move-exception v0

    :try_start_11
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_7
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    :catch_7
    move-exception v0

    :try_start_12
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_d
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    :cond_8
    :goto_5
    if-eqz v0, :cond_9

    :try_start_13
    iget-boolean v1, v9, Lcom/jscape/inet/sftp/Sftp;->m:Z
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_8
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    goto :goto_6

    :catch_8
    move-exception v0

    :try_start_14
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_d
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    :cond_9
    move-object/from16 v19, v8

    goto :goto_7

    :cond_a
    move/from16 v1, p3

    :cond_b
    :goto_6
    if-eqz v1, :cond_9

    :try_start_15
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    iget-object v14, v9, Lcom/jscape/inet/sftp/Sftp;->y:Ljava/util/concurrent/ExecutorService;

    move-object v2, v13

    move-object v3, v11

    move-wide v4, v15

    move v6, v12

    move-object v7, v8

    move-object/from16 v19, v8

    move-object v8, v14

    invoke-interface/range {v1 .. v8}, Lcom/jscape/inet/sftp/FileService;->writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;Ljava/util/concurrent/ExecutorService;)V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_9
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    if-nez v0, :cond_c

    goto :goto_7

    :catch_9
    move-exception v0

    :try_start_16
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :goto_7
    iget-object v1, v9, Lcom/jscape/inet/sftp/Sftp;->z:Lcom/jscape/inet/sftp/FileService;

    move-object v2, v13

    move-object v3, v11

    move-wide v4, v15

    move v6, v12

    move-object/from16 v7, v19

    invoke-interface/range {v1 .. v7}, Lcom/jscape/inet/sftp/FileService;->writeFile(Ljava/io/InputStream;Ljava/lang/String;JZLcom/jscape/inet/sftp/FileService$TransferListener;)V

    :cond_c
    const-string v2, ""

    invoke-virtual {v13}, Lcom/jscape/util/h/f;->a()J

    move-result-wide v4

    invoke-static/range {v17 .. v18}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object/from16 v1, p0

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/jscape/inet/sftp/Sftp;->b(Ljava/lang/String;Ljava/lang/String;JJ)V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_d
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    monitor-exit p0

    return-void

    :catch_a
    move-exception v0

    :try_start_17
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_d
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    :catch_b
    move-exception v0

    move-object v1, v0

    :try_start_18
    invoke-static {v1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_c
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    :catch_c
    move-exception v0

    :try_start_19
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_d
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    :catch_d
    move-exception v0

    :try_start_1a
    const-string v1, ""

    invoke-direct {v9, v1, v10}, Lcom/jscape/inet/sftp/Sftp;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v0}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object v0

    throw v0
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized upload(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/sftp/Sftp;->upload(Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/lang/String;Ljava/io/File;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2, p1}, Lcom/jscape/inet/sftp/Sftp;->upload(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/sftp/Sftp;->renameFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/sftp/Sftp;->upload(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/inet/sftp/Sftp;->upload(Ljava/io/File;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload(Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/jscape/inet/sftp/Sftp;->upload(Ljava/io/File;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload([BLjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/sftp/Sftp;->upload([BLjava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized upload([BLjava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {p0, v0, p2, p3}, Lcom/jscape/inet/sftp/Sftp;->upload(Ljava/io/InputStream;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/sftp/Sftp;->uploadDir(Ljava/io/File;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/sftp/Sftp;->uploadDir(Ljava/io/File;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/jscape/inet/sftp/Sftp;->uploadDir(Ljava/io/File;ILjava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;IILjava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/jscape/inet/sftp/Sftp;->uploadDir(Ljava/io/File;IILjava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;IILjava/lang/String;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    new-instance v7, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;

    const/4 v3, 0x0

    const/4 v0, 0x1

    add-int/2addr p2, v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    int-to-long p2, p3

    invoke-static {p2, p3}, Lcom/jscape/util/Time;->seconds(J)Lcom/jscape/util/Time;

    move-result-object v5

    if-lez p5, :cond_0

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2
    :try_end_0
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    move-object v6, p2

    move-object v0, v7

    move-object v1, p1

    move-object v2, p4

    :try_start_1
    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;-><init>(Ljava/io/File;Ljava/lang/String;ZILcom/jscape/util/Time;Ljava/lang/Integer;)V

    iput-object v7, p0, Lcom/jscape/inet/sftp/Sftp;->A:Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;

    iget-object p1, p0, Lcom/jscape/inet/sftp/Sftp;->A:Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;

    invoke-virtual {p1, p0}, Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;->applyTo(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$UploadDirectoryOperation;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/jscape/inet/sftp/Sftp;->uploadDir(Ljava/io/File;ILjava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized uploadDir(Ljava/io/File;ILjava/lang/String;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    monitor-enter p0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move v5, p4

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/jscape/inet/sftp/Sftp;->uploadDir(Ljava/io/File;IILjava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
