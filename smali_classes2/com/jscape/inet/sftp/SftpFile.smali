.class public Lcom/jscape/inet/sftp/SftpFile;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/jscape/util/e/UnixFileType;

.field private final c:J

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/jscape/util/e/PosixFilePermissions;

.field private final g:J

.field private final h:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/jscape/util/e/UnixFileType;JLjava/lang/String;Ljava/lang/String;Lcom/jscape/util/e/PosixFilePermissions;JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/SftpFile;->a:Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/SftpFile;->b:Lcom/jscape/util/e/UnixFileType;

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result p1

    iput-wide p3, p0, Lcom/jscape/inet/sftp/SftpFile;->c:J

    const-string p2, ""

    if-eqz p1, :cond_1

    if-eqz p5, :cond_0

    goto :goto_0

    :cond_0
    move-object p5, p2

    :cond_1
    :goto_0
    iput-object p5, p0, Lcom/jscape/inet/sftp/SftpFile;->d:Ljava/lang/String;

    if-eqz p1, :cond_3

    if-eqz p6, :cond_2

    goto :goto_1

    :cond_2
    move-object p6, p2

    :cond_3
    :goto_1
    iput-object p6, p0, Lcom/jscape/inet/sftp/SftpFile;->e:Ljava/lang/String;

    iput-object p7, p0, Lcom/jscape/inet/sftp/SftpFile;->f:Lcom/jscape/util/e/PosixFilePermissions;

    iput-wide p8, p0, Lcom/jscape/inet/sftp/SftpFile;->g:J

    iput-wide p10, p0, Lcom/jscape/inet/sftp/SftpFile;->h:J

    return-void
.end method


# virtual methods
.method public getAccessTime()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/sftp/SftpFile;->g:J

    return-wide v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFile;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getFilesize()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/sftp/SftpFile;->c:J

    return-wide v0
.end method

.method public getGroup()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFile;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getLongName()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public getModificationTime()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/sftp/SftpFile;->h:J

    return-wide v0
.end method

.method public getOwner()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFile;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getPermission()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFile;->f:Lcom/jscape/util/e/PosixFilePermissions;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFile;->b:Lcom/jscape/util/e/UnixFileType;

    iget-char v1, v1, Lcom/jscape/util/e/UnixFileType;->lsCode:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFile;->f:Lcom/jscape/util/e/PosixFilePermissions;

    invoke-static {v1}, Lcom/jscape/util/e/a;->b(Lcom/jscape/util/e/PosixFilePermissions;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getPermissions()I
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFile;->f:Lcom/jscape/util/e/PosixFilePermissions;

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    invoke-static {v1}, Lcom/jscape/util/e/a;->a(Lcom/jscape/util/e/PosixFilePermissions;)I

    move-result v0

    :goto_1
    return v0
.end method

.method public isDirectory()Z
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFile;->b:Lcom/jscape/util/e/UnixFileType;

    sget-object v1, Lcom/jscape/util/e/UnixFileType;->DIRECTORY:Lcom/jscape/util/e/UnixFileType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFile;->a:Ljava/lang/String;

    return-object v0
.end method
