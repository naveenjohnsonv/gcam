.class public Lcom/jscape/inet/sftp/SftpConfiguration;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

.field public final subsystemResponseRequired:Z


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x1c

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x68

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\u001fRWn6\u00000@\u0006Av\u0006\u0016:C\u001dJh1!,B\u0007Mi1\u0017t$`\u0014Pk\u0017\u001c\'U\u001bCn&\u0012=Z\u001dJ;/\u0000:[1Ku2\u001a.F\u0000Eo=\u001c\'\u000e"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x41

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/sftp/SftpConfiguration;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x21

    goto :goto_2

    :cond_2
    const/16 v12, 0x1b

    goto :goto_2

    :cond_3
    const/16 v12, 0x3c

    goto :goto_2

    :cond_4
    const/16 v12, 0x73

    goto :goto_2

    :cond_5
    const/16 v12, 0x4c

    goto :goto_2

    :cond_6
    const/16 v12, 0x1a

    goto :goto_2

    :cond_7
    const/16 v12, 0x5b

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/SshConfiguration;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/SftpConfiguration;->sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

    iput-boolean p2, p0, Lcom/jscape/inet/sftp/SftpConfiguration;->subsystemResponseRequired:Z

    return-void
.end method

.method public static defaultConfiguration()Lcom/jscape/inet/sftp/SftpConfiguration;
    .locals 3

    new-instance v0, Lcom/jscape/inet/sftp/SftpConfiguration;

    invoke-static {}, Lcom/jscape/inet/ssh/SshConfiguration;->defaultClientConfiguration()Lcom/jscape/inet/ssh/SshConfiguration;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/jscape/inet/sftp/SftpConfiguration;-><init>(Lcom/jscape/inet/ssh/SshConfiguration;Z)V

    return-object v0
.end method

.method public static defaultConfiguration(Ljava/lang/String;)Lcom/jscape/inet/sftp/SftpConfiguration;
    .locals 2

    new-instance v0, Lcom/jscape/inet/sftp/SftpConfiguration;

    invoke-static {p0}, Lcom/jscape/inet/ssh/SshConfiguration;->defaultClientConfiguration(Ljava/lang/String;)Lcom/jscape/inet/ssh/SshConfiguration;

    move-result-object p0

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/jscape/inet/sftp/SftpConfiguration;-><init>(Lcom/jscape/inet/ssh/SshConfiguration;Z)V

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/SftpConfiguration;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/SftpConfiguration;->sshConfiguration:Lcom/jscape/inet/ssh/SshConfiguration;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/SftpConfiguration;->subsystemResponseRequired:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
