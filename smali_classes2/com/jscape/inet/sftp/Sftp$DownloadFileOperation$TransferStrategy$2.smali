.class final enum Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy$2;
.super Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;-><init>(Ljava/lang/String;ILcom/jscape/inet/sftp/Sftp$1;)V

    return-void
.end method


# virtual methods
.method public apply(Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->a(Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->c(Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;)Lcom/jscape/inet/sftp/Sftp;

    move-result-object v2

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b(Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lcom/jscape/inet/sftp/Sftp;->resumeDownload(Ljava/lang/String;J)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    sget-object v0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy$2;->FROM_SCRATCH:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;->apply(Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;)V

    :goto_0
    return-void
.end method
