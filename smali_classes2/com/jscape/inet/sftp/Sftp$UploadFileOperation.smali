.class public Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;
.super Ljava/lang/Object;


# static fields
.field private static final r:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/io/File;

.field private final b:Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy;

.field private final e:Z

.field private final f:Z

.field private final g:I

.field private final h:Lcom/jscape/util/Time;

.field private final i:Z

.field private final j:Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$Listener;

.field private k:Z

.field private l:Lcom/jscape/inet/sftp/Sftp;

.field private m:Ljava/lang/String;

.field private n:I

.field private o:Z

.field private p:Ljava/lang/Exception;

.field private volatile q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "\u0018z3\u0018p\\KQ\u00168\u001e}Uy]62/yHJ](2\u0019!\u000e\u0018z:\u001cdxK@?:\rhJ\u0002\u0008\u0018z2\u000fnVM\t\u000e\u0018z%\u0018qVKQ\u001c>\u0011y\u0004\u0018\u001b\u0018z%\u0018rXRQ\u00082\u0010sMZr3;\u0018N\\NA3%\u0018x\u0004\u0013\u0018z#\u000f}WLR?%.hK^@?0\u0004!\u000c\u0018z$\u0008\u007fZZQ>2\u0019!!g*2\u001eu_VQ>w\r}MWZ;:\u0018<PL\u001448\t<X\u001fR3;\u00182\u001fa*;\u0012}]y]622l\\MU.>\u0012r\u0019DX54\u001cp\u007fVX?j\u0012\u0018z%\u0018qVKQ\u001e>\u000fyZK[(.@\u0013`(6\u0013o_ZFz4\u001crZZX62\u00192\u000c\u0018z4\u001crZZX62\u0019!\u0017v;3]qXG\u0014;#\tyTO@)w\u000b}UJQt\u0010\u0018z6\th\\RD.\u0007\u0018nPPPg"

    const/16 v4, 0x11d

    const/16 v5, 0x1a

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x43

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x2e

    const/16 v3, 0x22

    const-string v5, "F$oJ.\u00025\u0003ilP6\u0006\u000c\u001aT{F1\u0002\u0013\u001cmgD\u0010\u0002\u0010\u001fm{F&Z\u000bF$hW6\u0002\u000c\u001apz\u001e"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x1d

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->r:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x7c

    goto :goto_4

    :cond_4
    const/16 v1, 0x7a

    goto :goto_4

    :cond_5
    const/16 v1, 0x5f

    goto :goto_4

    :cond_6
    const/16 v1, 0x3e

    goto :goto_4

    :cond_7
    const/16 v1, 0x14

    goto :goto_4

    :cond_8
    const/16 v1, 0x19

    goto :goto_4

    :cond_9
    const/16 v1, 0x77

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/io/File;Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;Ljava/lang/String;Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy;ZILcom/jscape/util/Time;ZLcom/jscape/inet/sftp/Sftp$UploadFileOperation$Listener;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iput-object p2, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->b:Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;

    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result p2

    sget-object v1, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->r:[Ljava/lang/String;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-static {p2, v1}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->a:Ljava/io/File;

    invoke-static {p3}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result p2

    const-string v1, "."

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    if-eqz v0, :cond_3

    invoke-virtual {p3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p2

    goto :goto_0

    :cond_0
    const-string p3, ""

    goto :goto_1

    :cond_1
    :goto_0
    if-eqz p2, :cond_2

    goto :goto_1

    :cond_2
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    :cond_3
    :goto_1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->c:Ljava/lang/String;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->d:Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy;

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p1

    if-eqz v0, :cond_5

    if-lez p1, :cond_4

    const/4 p1, 0x1

    goto :goto_2

    :cond_4
    const/4 p1, 0x0

    :cond_5
    :goto_2
    iput-boolean p1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->e:Z

    iput-boolean p5, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->f:Z

    int-to-long p1, p6

    const-wide/16 p3, 0x0

    sget-object p5, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->r:[Ljava/lang/String;

    const/16 v0, 0xc

    aget-object p5, p5, v0

    invoke-static {p1, p2, p3, p4, p5}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p6, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->g:I

    invoke-static {p7}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p7, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->h:Lcom/jscape/util/Time;

    iput-boolean p8, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->i:Z

    invoke-static {p9}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p9, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->j:Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$Listener;

    return-void
.end method

.method static a(Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;)Ljava/io/File;
    .locals 0

    iget-object p0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->a:Ljava/io/File;

    return-object p0
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->p:Ljava/lang/Exception;

    iget p1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->n:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->n:I

    return-void
.end method

.method private a()Z
    .locals 3

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->o:Z

    if-eqz v0, :cond_0

    if-nez v1, :cond_2

    iget v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->n:I

    :cond_0
    if-eqz v0, :cond_1

    iget v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->g:I

    if-ge v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->q:Z

    :cond_1
    if-eqz v0, :cond_3

    if-nez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method private static b(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method static b(Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->c:Ljava/lang/String;

    return-object p0
.end method

.method private b()V
    .locals 1

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->e()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->f()V

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->g()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->h()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->i()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->j()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->k()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->l()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->m()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_1
    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->a(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->n()V

    return-void

    :goto_1
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->n()V

    throw v0
.end method

.method static c(Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;)Lcom/jscape/inet/sftp/Sftp;
    .locals 0

    iget-object p0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->l:Lcom/jscape/inet/sftp/Sftp;

    return-object p0
.end method

.method private c()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->q:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/inet/sftp/SftpException;

    sget-object v1, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->r:[Ljava/lang/String;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/inet/sftp/SftpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method static d(Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;)I
    .locals 0

    iget p0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->n:I

    return p0
.end method

.method private d()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->p:Ljava/lang/Exception;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->p:Ljava/lang/Exception;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    throw v1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private e()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->o:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->p:Ljava/lang/Exception;

    return-void
.end method

.method private f()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->n:I

    if-lez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->h:Lcom/jscape/util/Time;

    invoke-virtual {v0}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/jscape/util/D;->f(J)V

    :cond_1
    return-void
.end method

.method private g()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->l:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v0}, Lcom/jscape/inet/sftp/Sftp;->isConnected()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->l:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v0}, Lcom/jscape/inet/sftp/Sftp;->connect()Lcom/jscape/inet/sftp/Sftp;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->l:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v0}, Lcom/jscape/inet/sftp/Sftp;->getDir()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->m:Ljava/lang/String;

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->b:Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->l:Lcom/jscape/inet/sftp/Sftp;

    invoke-interface {v0, v1}, Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;->applyTo(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;

    return-void
.end method

.method private h()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->d:Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy;->apply(Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;)V

    return-void
.end method

.method private i()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->e:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->l:Lcom/jscape/inet/sftp/Sftp;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/jscape/inet/sftp/Sftp;->renameFile(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private j()V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->f:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->l:Lcom/jscape/inet/sftp/Sftp;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->c:Ljava/lang/String;

    new-instance v2, Ljava/util/Date;

    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->a:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1, v2}, Lcom/jscape/inet/sftp/Sftp;->setFileTimestamp(Ljava/lang/String;Ljava/util/Date;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method

.method private k()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->i:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    :cond_1
    return-void

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private l()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->l:Lcom/jscape/inet/sftp/Sftp;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/jscape/inet/sftp/Sftp;->setDir(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private m()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->o:Z

    return-void
.end method

.method private n()V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->k:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->l:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v0}, Lcom/jscape/inet/sftp/Sftp;->disconnect()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method


# virtual methods
.method public applyTo(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->l:Lcom/jscape/inet/sftp/Sftp;

    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->a()Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_1

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->b()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_2

    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->c()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->d()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    iget-object p1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->j:Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$Listener;

    invoke-interface {p1, p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$Listener;->onOperationCompleted(Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;)V

    return-object p0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_3
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->l:Lcom/jscape/inet/sftp/Sftp;

    invoke-static {v0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->j:Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$Listener;

    invoke-interface {v0, p0}, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$Listener;->onOperationCompleted(Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;)V

    throw p1
.end method

.method public cancel()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->q:Z

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->l:Lcom/jscape/inet/sftp/Sftp;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/inet/sftp/Sftp;->interrupt()V

    :cond_1
    return-void
.end method

.method public disconnectRequired(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->k:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->r:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->a:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->b:Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->d:Lcom/jscape/inet/sftp/Sftp$UploadFileOperation$TransferStrategy;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->e:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v2, 0xe

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->f:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->g:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v2, 0xd

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->h:Lcom/jscape/util/Time;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->i:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v2, 0xf

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->n:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->o:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->p:Ljava/lang/Exception;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/Sftp$UploadFileOperation;->q:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
