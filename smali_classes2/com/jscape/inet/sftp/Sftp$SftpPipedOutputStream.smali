.class public Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;
.super Ljava/io/PipedOutputStream;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/jscape/inet/sftp/Sftp;

.field private final b:Ljava/lang/String;

.field private final c:J

.field private final d:J

.field private final e:Ljava/io/PipedInputStream;

.field private final f:Ljava/util/concurrent/locks/Lock;

.field private final g:Ljava/util/concurrent/locks/Condition;

.field private h:Z

.field private volatile i:Ljava/lang/Exception;


# direct methods
.method private constructor <init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;JJLjava/io/PipedInputStream;)V
    .locals 0

    invoke-direct {p0}, Ljava/io/PipedOutputStream;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->a:Lcom/jscape/inet/sftp/Sftp;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->b:Ljava/lang/String;

    iput-wide p3, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->c:J

    iput-wide p5, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->d:J

    invoke-static {p7}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p7, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->e:Ljava/io/PipedInputStream;

    new-instance p1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->g:Ljava/util/concurrent/locks/Condition;

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :cond_0
    :goto_0
    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->g:Ljava/util/concurrent/locks/Condition;

    const-wide/16 v2, 0x1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/locks/Condition;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_2

    goto :goto_0

    :catch_0
    if-nez v0, :cond_0

    :cond_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->i:Ljava/lang/Exception;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->i:Ljava/lang/Exception;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method public static streamFor(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;JJ)Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v8, Ljava/io/PipedInputStream;

    invoke-direct {v8}, Ljava/io/PipedInputStream;-><init>()V

    new-instance v9, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    move-object v7, v8

    invoke-direct/range {v0 .. v7}, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;-><init>(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/String;JJLjava/io/PipedInputStream;)V

    invoke-virtual {v9, v8}, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->connect(Ljava/io/PipedInputStream;)V

    return-object v9
.end method


# virtual methods
.method public close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0}, Ljava/io/PipedOutputStream;->close()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->a()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->b()V

    return-void
.end method

.method public declared-synchronized flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Ljava/io/PipedOutputStream;->close()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->a()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 8

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->a:Lcom/jscape/inet/sftp/Sftp;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->e:Ljava/io/PipedInputStream;

    iget-wide v3, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->d:J

    iget-object v5, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->b:Ljava/lang/String;

    iget-wide v6, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->c:J

    invoke-virtual/range {v1 .. v7}, Lcom/jscape/inet/sftp/Sftp;->resumeUpload(Ljava/io/InputStream;JLjava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_1
    iput-object v1, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->i:Ljava/lang/Exception;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    iput-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->h:Z

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->g:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :goto_1
    iput-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->h:Z

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->g:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v1
.end method

.method public write(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0, p1}, Ljava/io/PipedOutputStream;->write(I)V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->b()V

    return-void
.end method

.method public write([BII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0, p1, p2, p3}, Ljava/io/PipedOutputStream;->write([BII)V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$SftpPipedOutputStream;->b()V

    return-void
.end method
