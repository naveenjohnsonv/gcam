.class public Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;
.super Ljava/lang/Object;


# static fields
.field private static final q:[Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/io/File;

.field private final d:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

.field private final e:Z

.field private final f:I

.field private final g:Lcom/jscape/util/Time;

.field private final h:Z

.field private final i:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$Listener;

.field private j:Z

.field private k:Lcom/jscape/inet/sftp/Sftp;

.field private l:Ljava/lang/String;

.field private m:I

.field private n:Z

.field private o:Ljava/lang/Exception;

.field private volatile p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0xe

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "}\u0007%8\r\u0010a%B%)\u0001\"(\u0010}\u0007)-\u00014x!S\u0018<\u00078z5\u001a\u0008}\u0007-+\u0007>gl\"}\u0007.0\u00194A8J-*\u00010x!w:<\u00064g\'N&>\'4d$N:<\u0011l\u0013}\u0007,<\u00194a4u-4\u001a%p\u0017N$<H\u0013}\u0007<+\u0014?f7B:\n\u0001#t%B/ H\'\u0015H?7\u0019>t5a!5\u0010\u001ee4U)-\u001c>{q\\:<\u0018>a4c!+\u00102a>U1d\u0017\u0013F,y\u00180mqF<-\u0010<e%Th/\u0014=`4\t\u0013\u0005U)7\u00067p#\u0007+8\u001b2p=K-=[\u000c}\u0007$6\u00160y\u0017N$<H\u000c}\u0007+8\u001b2p=K-=H\u000b}\u0007)-\u00014x!S;d"

    const/16 v5, 0xed

    move v7, v0

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v7

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x4

    move v13, v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v14, v10

    move v15, v3

    :goto_2
    if-gt v14, v15, :cond_3

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v13}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v12, :cond_1

    add-int/lit8 v12, v8, 0x1

    aput-object v10, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v12

    goto :goto_0

    :cond_0
    const/16 v5, 0x19

    const-string v4, "\u001ba\\Z~X\u0007R\u0007GSv\nT\n\u001ba]JpT\u0016D2\u0013"

    move v7, v0

    move v8, v12

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v8, 0x1

    aput-object v10, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v12

    :goto_3
    const/16 v13, 0x62

    add-int/2addr v6, v9

    add-int v10, v6, v7

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->q:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v10, v15

    rem-int/lit8 v0, v15, 0x7

    const/16 v17, 0x55

    if-eqz v0, :cond_8

    if-eq v0, v9, :cond_7

    const/4 v2, 0x2

    if-eq v0, v2, :cond_6

    const/4 v2, 0x3

    if-eq v0, v2, :cond_5

    if-eq v0, v11, :cond_4

    const/4 v2, 0x5

    if-eq v0, v2, :cond_8

    const/16 v17, 0x11

    goto :goto_4

    :cond_4
    const/16 v17, 0x71

    goto :goto_4

    :cond_5
    const/16 v17, 0x5d

    goto :goto_4

    :cond_6
    const/16 v17, 0x4c

    goto :goto_4

    :cond_7
    const/16 v17, 0x23

    :cond_8
    :goto_4
    xor-int v0, v13, v17

    xor-int v0, v16, v0

    int-to-char v0, v0

    aput-char v0, v10, v15

    add-int/lit8 v15, v15, 0x1

    const/16 v0, 0xe

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;Ljava/lang/String;Ljava/io/File;Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;ZILcom/jscape/util/Time;ZLcom/jscape/inet/sftp/Sftp$DownloadFileOperation$Listener;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->a:Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    if-nez v0, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p2

    :cond_2
    :goto_0
    iput-object p2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b:Ljava/lang/String;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->c:Ljava/io/File;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->d:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

    iput-boolean p5, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->e:Z

    int-to-long p1, p6

    const-wide/16 p3, 0x0

    sget-object p5, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->q:[Ljava/lang/String;

    const/4 v0, 0x7

    aget-object p5, p5, v0

    invoke-static {p1, p2, p3, p4, p5}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p6, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->f:I

    invoke-static {p7}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p7, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->g:Lcom/jscape/util/Time;

    iput-boolean p8, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->h:Z

    invoke-static {p9}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p9, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->i:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$Listener;

    return-void
.end method

.method static a(Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;)Ljava/io/File;
    .locals 0

    iget-object p0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->c:Ljava/io/File;

    return-object p0
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->o:Ljava/lang/Exception;

    iget p1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->m:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->m:I

    return-void
.end method

.method private a()Z
    .locals 3

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->n:Z

    if-nez v0, :cond_0

    if-nez v1, :cond_2

    iget v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->m:I

    :cond_0
    if-nez v0, :cond_1

    iget v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->f:I

    if-ge v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->p:Z

    :cond_1
    if-nez v0, :cond_3

    if-nez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method private static b(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method static b(Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b:Ljava/lang/String;

    return-object p0
.end method

.method private b()V
    .locals 1

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->e()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->f()V

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->g()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->h()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->i()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->j()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->k()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->l()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_1
    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->a(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->m()V

    return-void

    :goto_1
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->m()V

    throw v0
.end method

.method static c(Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;)Lcom/jscape/inet/sftp/Sftp;
    .locals 0

    iget-object p0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->k:Lcom/jscape/inet/sftp/Sftp;

    return-object p0
.end method

.method private c()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->p:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/inet/sftp/SftpException;

    sget-object v1, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->q:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/inet/sftp/SftpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method static d(Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;)I
    .locals 0

    iget p0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->m:I

    return p0
.end method

.method private d()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->o:Ljava/lang/Exception;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->o:Ljava/lang/Exception;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    throw v1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private e()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->n:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->o:Ljava/lang/Exception;

    return-void
.end method

.method private f()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->m:I

    if-lez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->g:Lcom/jscape/util/Time;

    invoke-virtual {v0}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/jscape/util/D;->f(J)V

    :cond_1
    return-void
.end method

.method private g()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->k:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v1}, Lcom/jscape/inet/sftp/Sftp;->isConnected()Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->k:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v1}, Lcom/jscape/inet/sftp/Sftp;->connect()Lcom/jscape/inet/sftp/Sftp;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->k:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v1}, Lcom/jscape/inet/sftp/Sftp;->getDir()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->l:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->a:Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->k:Lcom/jscape/inet/sftp/Sftp;

    invoke-interface {v1, v2}, Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;->applyTo(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    :try_start_3
    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->k:Lcom/jscape/inet/sftp/Sftp;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    if-eqz v0, :cond_3

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    new-instance v1, Ljava/io/File;

    const-string v0, "."

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :cond_3
    :goto_1
    invoke-virtual {v2, v1}, Lcom/jscape/inet/sftp/Sftp;->setLocalDir(Ljava/io/File;)V

    return-void

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private h()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->d:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

    invoke-virtual {v0, p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;->apply(Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;)V

    return-void
.end method

.method private i()V
    .locals 4

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->e:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->k:Lcom/jscape/inet/sftp/Sftp;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/jscape/inet/sftp/Sftp;->getFileTimestamp(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/io/File;->setLastModified(J)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method

.method private j()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->h:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->k:Lcom/jscape/inet/sftp/Sftp;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/jscape/inet/sftp/Sftp;->deleteFile(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method

.method private k()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->k:Lcom/jscape/inet/sftp/Sftp;

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/jscape/inet/sftp/Sftp;->setDir(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private l()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->n:Z

    return-void
.end method

.method private m()V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->j:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->k:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v0}, Lcom/jscape/inet/sftp/Sftp;->disconnect()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method


# virtual methods
.method public applyTo(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->k:Lcom/jscape/inet/sftp/Sftp;

    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->a()Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_1

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_2

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->c()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->d()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    iget-object p1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->i:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$Listener;

    invoke-interface {p1, p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$Listener;->onOperationCompleted(Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;)V

    return-object p0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_3
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->k:Lcom/jscape/inet/sftp/Sftp;

    invoke-static {v0, p1}, Lcom/jscape/inet/sftp/Sftp;->a(Lcom/jscape/inet/sftp/Sftp;Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->i:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$Listener;

    invoke-interface {v0, p0}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$Listener;->onOperationCompleted(Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;)V

    throw p1
.end method

.method public cancel()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->p:Z

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->k:Lcom/jscape/inet/sftp/Sftp;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/inet/sftp/Sftp;->interrupt()V

    :cond_1
    return-void
.end method

.method public disconnectRequired(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->j:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->q:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->a:Lcom/jscape/inet/sftp/Sftp$RemoteDirectory;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0xc

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->c:Ljava/io/File;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->d:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->e:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->f:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->g:Lcom/jscape/util/Time;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->h:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v2, 0xb

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->m:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v2, 0xd

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->n:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->o:Ljava/lang/Exception;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;->p:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
