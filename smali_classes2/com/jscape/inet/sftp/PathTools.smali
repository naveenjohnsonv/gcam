.class public Lcom/jscape/inet/sftp/PathTools;
.super Ljava/lang/Object;


# static fields
.field public static final CURRENT_PATH:Ljava/lang/String; = "."

.field public static final FILE_SEPARATOR:Ljava/lang/String; = "/"

.field public static final PARENT_PATH:Ljava/lang/String;

.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x2

    const-string v5, "_f\u0002_g\u0002Kf"

    const/16 v6, 0x8

    move v8, v3

    const/4 v7, -0x1

    const/4 v9, 0x0

    :goto_0
    const/16 v10, 0x56

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    const/4 v15, 0x0

    :goto_2
    const/4 v2, 0x3

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_1

    add-int/lit8 v2, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v2

    goto :goto_0

    :cond_0
    const-string v5, ";\u0003\u0002Iq"

    move v6, v0

    move v9, v2

    move v8, v3

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v2

    move v9, v12

    :goto_3
    const/16 v10, 0x32

    add-int/2addr v7, v11

    add-int v2, v7, v8

    invoke-virtual {v5, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/sftp/PathTools;->a:[Ljava/lang/String;

    aget-object v0, v1, v2

    sput-object v0, Lcom/jscape/inet/sftp/PathTools;->PARENT_PATH:Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v4, v15, 0x7

    if-eqz v4, :cond_9

    if-eq v4, v11, :cond_8

    if-eq v4, v3, :cond_7

    if-eq v4, v2, :cond_6

    const/4 v2, 0x4

    if-eq v4, v2, :cond_5

    if-eq v4, v0, :cond_4

    const/16 v2, 0x14

    goto :goto_4

    :cond_4
    const/16 v2, 0x74

    goto :goto_4

    :cond_5
    const/16 v2, 0x4d

    goto :goto_4

    :cond_6
    const/16 v2, 0x51

    goto :goto_4

    :cond_7
    const/16 v2, 0x3a

    goto :goto_4

    :cond_8
    const/16 v2, 0x1f

    goto :goto_4

    :cond_9
    const/16 v2, 0x27

    :goto_4
    xor-int/2addr v2, v10

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v12, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    sget-object v1, Lcom/jscape/inet/sftp/PathTools;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v3, "/"

    if-nez v0, :cond_3

    if-eqz v1, :cond_2

    if-nez v0, :cond_0

    invoke-virtual {p0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p1

    sub-int/2addr p1, v2

    sub-int/2addr p1, v2

    invoke-virtual {p0, v3, p1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    move-result p1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result p1

    :goto_0
    if-lez p1, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    :cond_1
    return-object v3

    :cond_2
    invoke-virtual {p0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    :cond_3
    if-nez v0, :cond_5

    if-nez v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_4
    if-nez v0, :cond_7

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    :cond_5
    if-eqz v1, :cond_6

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_7
    return-object p1
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 9

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    const-string v1, "/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v0, :cond_0

    if-le v2, v4, :cond_2

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isLetter(C)Z

    move-result v2

    :cond_0
    if-eqz v0, :cond_1

    if-eqz v2, :cond_2

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    :cond_1
    if-eqz v0, :cond_3

    const/16 v5, 0x3a

    if-ne v2, v5, :cond_2

    move v2, v4

    goto :goto_0

    :cond_2
    move v2, v3

    :cond_3
    :goto_0
    sget-object v5, Lcom/jscape/inet/sftp/PathTools;->a:[Ljava/lang/String;

    const/4 v6, 0x4

    aget-object v6, v5, v6

    invoke-virtual {p0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    const/4 v7, 0x2

    aget-object v7, v5, v7

    invoke-virtual {p0, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    const/4 v8, 0x3

    aget-object v5, v5, v8

    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    const-string v8, "."

    invoke-virtual {p0, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz v0, :cond_8

    if-nez v1, :cond_7

    if-eqz v0, :cond_6

    if-nez v2, :cond_7

    if-eqz v0, :cond_5

    if-nez v6, :cond_7

    if-eqz v0, :cond_4

    if-eqz v7, :cond_b

    goto :goto_1

    :cond_4
    move v1, v7

    goto :goto_2

    :cond_5
    move v1, v6

    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_2

    :cond_7
    :goto_1
    move v1, v5

    :cond_8
    :goto_2
    if-eqz v0, :cond_9

    if-nez v1, :cond_b

    goto :goto_3

    :cond_9
    move p0, v1

    :goto_3
    if-eqz v0, :cond_a

    if-nez p0, :cond_b

    move v3, v4

    goto :goto_4

    :cond_a
    move v3, p0

    :cond_b
    :goto_4
    return v3
.end method

.method public static nameOf(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    const-string v1, "/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    if-eqz v0, :cond_0

    if-ltz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    add-int/lit8 v1, v1, 0x1

    if-le v0, v1, :cond_1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    :cond_1
    return-object p0
.end method

.method public static resolve(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    invoke-static {p1}, Lcom/jscape/inet/sftp/PathTools;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    return-object p1

    :cond_0
    sget-object v1, Lcom/jscape/inet/sftp/PathTools;->a:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    if-nez v0, :cond_3

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    :cond_1
    if-nez v1, :cond_4

    const-string v1, "."

    if-nez v0, :cond_3

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v1, Lcom/jscape/inet/sftp/PathTools;->a:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    if-nez v0, :cond_2

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    move-object p0, p1

    move-object p1, v1

    goto :goto_0

    :cond_3
    move-object p0, v1

    :cond_4
    :goto_0
    invoke-static {p0, p1}, Lcom/jscape/inet/sftp/PathTools;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
