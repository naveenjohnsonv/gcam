.class public Lcom/jscape/inet/sftp/SftpFileNotFoundException;
.super Lcom/jscape/inet/sftp/RequestException;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private filename:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "le\u0015X@S0\nh\u0010O\u0005_6E~\u0000\u001d\u000eS6\nj\u0016H\u000eXl"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/sftp/SftpFileNotFoundException;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x59

    goto :goto_1

    :cond_1
    const/16 v4, 0x27

    goto :goto_1

    :cond_2
    const/16 v4, 0x7b

    goto :goto_1

    :cond_3
    const/16 v4, 0x26

    goto :goto_1

    :cond_4
    const/16 v4, 0x62

    goto :goto_1

    :cond_5
    const/16 v4, 0x17

    goto :goto_1

    :cond_6
    const/16 v4, 0x31

    :goto_1
    const/16 v5, 0x1b

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_NO_SUCH_FILE:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    iget v0, v0, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->code:I

    sget-object v1, Lcom/jscape/inet/sftp/SftpFileNotFoundException;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/jscape/inet/sftp/RequestException;-><init>(ILjava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/SftpFileNotFoundException;->filename:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileNotFoundException;->filename:Ljava/lang/String;

    return-object v0
.end method
