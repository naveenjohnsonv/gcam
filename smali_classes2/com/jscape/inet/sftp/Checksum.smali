.class public abstract enum Lcom/jscape/inet/sftp/Checksum;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/sftp/Checksum;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CRC32:Lcom/jscape/inet/sftp/Checksum;

.field public static final enum MD5:Lcom/jscape/inet/sftp/Checksum;

.field public static final enum SHA1:Lcom/jscape/inet/sftp/Checksum;

.field public static final enum SHA224:Lcom/jscape/inet/sftp/Checksum;

.field public static final enum SHA256:Lcom/jscape/inet/sftp/Checksum;

.field public static final enum SHA384:Lcom/jscape/inet/sftp/Checksum;

.field public static final enum SHA512:Lcom/jscape/inet/sftp/Checksum;

.field private static final a:I = 0x2000

.field private static final b:[Lcom/jscape/inet/sftp/Checksum;

.field private static final c:[Ljava/lang/String;


# instance fields
.field public final code:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x3

    const/4 v3, 0x0

    const-string v4, "J>)\u0005d\u0008\u007f\u000b$\u0006t\u0012}\r\'o\u0006T2]\u000b.i\u0006t\u0012}\n#k D\u0015qUy3\u001fo\u001boP6<S`\u0015nQb5R\'\u0014sL6;Pr\u0014x\u0016\u0004T2]\t\u0003j\u001e)\u0006T2]\r\'o\u0006T2]\n$i\u0006t\u0012}\n$i\u0006t\u0012}\u000b.i\u0006T2]\n#k"

    const/16 v5, 0x6b

    move v7, v2

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x76

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/16 v15, 0xc

    const/4 v1, 0x2

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    const/16 v11, 0xa

    if-eqz v12, :cond_1

    add-int/lit8 v1, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v1

    goto :goto_0

    :cond_0
    const-string v4, "d\u0008\u007f+\u0004\u0004T2])"

    move v8, v1

    move v5, v11

    const/4 v6, -0x1

    const/4 v7, 0x5

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v1

    move v8, v12

    :goto_3
    const/16 v9, 0x56

    add-int/2addr v6, v10

    add-int v1, v6, v7

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/sftp/Checksum;->c:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/sftp/Checksum$1;

    sget-object v4, Lcom/jscape/inet/sftp/Checksum;->c:[Ljava/lang/String;

    const/16 v5, 0xd

    aget-object v5, v4, v5

    aget-object v6, v4, v10

    invoke-direct {v0, v5, v3, v6}, Lcom/jscape/inet/sftp/Checksum$1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/sftp/Checksum;->CRC32:Lcom/jscape/inet/sftp/Checksum;

    new-instance v0, Lcom/jscape/inet/sftp/Checksum$2;

    aget-object v5, v4, v3

    const/4 v6, 0x7

    aget-object v7, v4, v6

    invoke-direct {v0, v5, v10, v7}, Lcom/jscape/inet/sftp/Checksum$2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/sftp/Checksum;->MD5:Lcom/jscape/inet/sftp/Checksum;

    new-instance v0, Lcom/jscape/inet/sftp/Checksum$3;

    const/4 v5, 0x6

    aget-object v7, v4, v5

    const/16 v8, 0xe

    aget-object v8, v4, v8

    invoke-direct {v0, v7, v1, v8}, Lcom/jscape/inet/sftp/Checksum$3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/sftp/Checksum;->SHA1:Lcom/jscape/inet/sftp/Checksum;

    new-instance v0, Lcom/jscape/inet/sftp/Checksum$4;

    const/16 v7, 0x9

    aget-object v7, v4, v7

    aget-object v8, v4, v11

    invoke-direct {v0, v7, v2, v8}, Lcom/jscape/inet/sftp/Checksum$4;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/sftp/Checksum;->SHA224:Lcom/jscape/inet/sftp/Checksum;

    new-instance v0, Lcom/jscape/inet/sftp/Checksum$5;

    aget-object v7, v4, v15

    const/4 v8, 0x4

    aget-object v9, v4, v8

    invoke-direct {v0, v7, v8, v9}, Lcom/jscape/inet/sftp/Checksum$5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/sftp/Checksum;->SHA256:Lcom/jscape/inet/sftp/Checksum;

    new-instance v0, Lcom/jscape/inet/sftp/Checksum$6;

    aget-object v7, v4, v2

    const/16 v8, 0xb

    aget-object v8, v4, v8

    const/4 v9, 0x5

    invoke-direct {v0, v7, v9, v8}, Lcom/jscape/inet/sftp/Checksum$6;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/sftp/Checksum;->SHA384:Lcom/jscape/inet/sftp/Checksum;

    new-instance v0, Lcom/jscape/inet/sftp/Checksum$7;

    const/16 v7, 0x8

    aget-object v7, v4, v7

    aget-object v4, v4, v1

    invoke-direct {v0, v7, v5, v4}, Lcom/jscape/inet/sftp/Checksum$7;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/sftp/Checksum;->SHA512:Lcom/jscape/inet/sftp/Checksum;

    new-array v4, v6, [Lcom/jscape/inet/sftp/Checksum;

    sget-object v6, Lcom/jscape/inet/sftp/Checksum;->CRC32:Lcom/jscape/inet/sftp/Checksum;

    aput-object v6, v4, v3

    sget-object v3, Lcom/jscape/inet/sftp/Checksum;->MD5:Lcom/jscape/inet/sftp/Checksum;

    aput-object v3, v4, v10

    sget-object v3, Lcom/jscape/inet/sftp/Checksum;->SHA1:Lcom/jscape/inet/sftp/Checksum;

    aput-object v3, v4, v1

    sget-object v1, Lcom/jscape/inet/sftp/Checksum;->SHA224:Lcom/jscape/inet/sftp/Checksum;

    aput-object v1, v4, v2

    sget-object v1, Lcom/jscape/inet/sftp/Checksum;->SHA256:Lcom/jscape/inet/sftp/Checksum;

    const/4 v2, 0x4

    aput-object v1, v4, v2

    sget-object v1, Lcom/jscape/inet/sftp/Checksum;->SHA384:Lcom/jscape/inet/sftp/Checksum;

    const/4 v2, 0x5

    aput-object v1, v4, v2

    aput-object v0, v4, v5

    sput-object v4, Lcom/jscape/inet/sftp/Checksum;->b:[Lcom/jscape/inet/sftp/Checksum;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v3, v14, 0x7

    if-eqz v3, :cond_8

    if-eq v3, v10, :cond_9

    if-eq v3, v1, :cond_7

    if-eq v3, v2, :cond_6

    const/4 v1, 0x4

    if-eq v3, v1, :cond_5

    const/4 v1, 0x5

    if-eq v3, v1, :cond_4

    const/16 v15, 0x49

    goto :goto_4

    :cond_4
    const/16 v15, 0x2b

    goto :goto_4

    :cond_5
    const/16 v15, 0x60

    goto :goto_4

    :cond_6
    const/16 v15, 0x4e

    goto :goto_4

    :cond_7
    const/16 v15, 0x6a

    goto :goto_4

    :cond_8
    const/16 v15, 0x71

    :cond_9
    :goto_4
    xor-int v1, v9, v15

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/jscape/inet/sftp/Checksum;->code:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/jscape/inet/sftp/Checksum$1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/inet/sftp/Checksum;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static firstSupportedFrom(Ljava/util/List;)Lcom/jscape/inet/sftp/Checksum;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/jscape/inet/sftp/Checksum;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    :goto_1
    if-eqz v1, :cond_5

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {}, Lcom/jscape/inet/sftp/Checksum;->values()[Lcom/jscape/inet/sftp/Checksum;

    move-result-object v2

    array-length v3, v2

    const/4 v4, 0x0

    :cond_0
    if-ge v4, v3, :cond_4

    aget-object v5, v2, v4

    if-nez v0, :cond_3

    :try_start_0
    iget-object v6, v5, Lcom/jscape/inet/sftp/Checksum;->code:Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_2

    if-eqz v6, :cond_1

    return-object v5

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_2
    move v1, v6

    goto :goto_1

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/sftp/Checksum;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_3
    :goto_2
    if-eqz v0, :cond_0

    :cond_4
    if-nez v0, :cond_5

    goto :goto_0

    :cond_5
    new-instance p0, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/jscape/inet/sftp/Checksum;->c:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/sftp/Checksum;
    .locals 1

    const-class v0, Lcom/jscape/inet/sftp/Checksum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/sftp/Checksum;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/sftp/Checksum;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/Checksum;->b:[Lcom/jscape/inet/sftp/Checksum;

    invoke-virtual {v0}, [Lcom/jscape/inet/sftp/Checksum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/sftp/Checksum;

    return-object v0
.end method


# virtual methods
.method protected abstract hash()Lcom/jscape/a/d;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public of(Ljava/io/InputStream;JI)[B
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/inet/sftp/Checksum;->hash()Lcom/jscape/a/d;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/jscape/inet/sftp/Checksum;->of(Ljava/io/InputStream;JILcom/jscape/a/d;)[B

    move-result-object p1

    return-object p1
.end method

.method protected of(Ljava/io/InputStream;JILcom/jscape/a/d;)[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    if-nez p4, :cond_0

    :try_start_0
    invoke-virtual {p0, p1, p2, p3, p5}, Lcom/jscape/inet/sftp/Checksum;->of(Ljava/io/InputStream;JLcom/jscape/a/d;)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Checksum;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    new-instance v1, Lcom/jscape/util/h/o;

    invoke-direct {v1}, Lcom/jscape/util/h/o;-><init>()V

    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-lez v2, :cond_2

    int-to-long v2, p4

    invoke-static {p2, p3, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    int-to-long v2, v2

    invoke-virtual {p0, p1, v2, v3, p5}, Lcom/jscape/inet/sftp/Checksum;->of(Ljava/io/InputStream;JLcom/jscape/a/d;)[B

    move-result-object v4

    sub-long/2addr p2, v2

    if-eqz v0, :cond_2

    :try_start_1
    invoke-virtual {v1, v4}, Lcom/jscape/util/h/o;->write([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_1

    goto :goto_0

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Checksum;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    invoke-virtual {v1}, Lcom/jscape/util/h/o;->d()[B

    move-result-object p1

    return-object p1
.end method

.method protected of(Ljava/io/InputStream;JLcom/jscape/a/d;)[B
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/16 v0, 0x2000

    new-array v1, v0, [B

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v2

    int-to-long v3, v0

    invoke-static {v3, v4, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v5

    long-to-int v0, v5

    :cond_0
    const/4 v5, 0x0

    invoke-virtual {p1, v1, v5, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    const/4 v6, -0x1

    if-eq v0, v6, :cond_2

    const-wide/16 v6, 0x0

    cmp-long v6, p2, v6

    if-nez v2, :cond_1

    if-lez v6, :cond_2

    int-to-long v6, v0

    sub-long/2addr p2, v6

    invoke-interface {p4, v1, v5, v0}, Lcom/jscape/a/d;->a([BII)Lcom/jscape/a/d;

    invoke-static {v3, v4, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v5

    long-to-int v0, v5

    goto :goto_0

    :cond_1
    move v0, v6

    :goto_0
    if-eqz v2, :cond_0

    :cond_2
    invoke-interface {p4}, Lcom/jscape/a/d;->c()[B

    move-result-object p1

    return-object p1
.end method
