.class public Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;
.super Ljava/lang/Object;


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lcom/jscape/inet/sftp/Sftp;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/jscape/inet/sftp/SftpFile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "4}"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->d:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x12

    goto :goto_1

    :cond_1
    const/16 v4, 0x26

    goto :goto_1

    :cond_2
    const/16 v4, 0x14

    goto :goto_1

    :cond_3
    const/16 v4, 0x31

    goto :goto_1

    :cond_4
    const/16 v4, 0x1c

    goto :goto_1

    :cond_5
    const/16 v4, 0x2c

    goto :goto_1

    :cond_6
    const/16 v4, 0x65

    :goto_1
    const/16 v5, 0x7f

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->a:Ljava/lang/String;

    return-void
.end method

.method private static a(Lcom/jscape/inet/sftp/SftpException;)Lcom/jscape/inet/sftp/SftpException;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->b:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v0}, Lcom/jscape/inet/sftp/Sftp;->getDir()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v1

    iget-object v2, p0, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->b:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {v2, p1}, Lcom/jscape/inet/sftp/Sftp;->setDir(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->b:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {p1}, Lcom/jscape/inet/sftp/Sftp;->getDirListing()Ljava/util/Enumeration;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/sftp/SftpFile;

    invoke-virtual {v2}, Lcom/jscape/inet/sftp/SftpFile;->getFilename()Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_7

    :try_start_0
    invoke-virtual {v2}, Lcom/jscape/inet/sftp/SftpFile;->isDirectory()Z

    move-result v4
    :try_end_0
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v1, :cond_3

    if-eqz v4, :cond_2

    :try_start_1
    sget-object v4, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->d:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4
    :try_end_1
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_1 .. :try_end_1} :catch_4

    if-nez v1, :cond_1

    if-nez v4, :cond_0

    const-string v4, "."

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    :cond_1
    if-nez v1, :cond_3

    if-eqz v4, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Lcom/jscape/inet/sftp/SftpFile;->isDirectory()Z

    move-result v4

    :cond_3
    if-nez v1, :cond_5

    if-eqz v4, :cond_4

    :try_start_2
    invoke-direct {p0, v3}, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_2 .. :try_end_2} :catch_0

    if-eqz v1, :cond_5

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->a(Lcom/jscape/inet/sftp/SftpException;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1

    :cond_4
    :goto_1
    iget-object v3, p0, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->c:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->a(Lcom/jscape/inet/sftp/SftpException;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1

    :cond_5
    :goto_2
    if-eqz v1, :cond_0

    goto :goto_3

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->a(Lcom/jscape/inet/sftp/SftpException;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->a(Lcom/jscape/inet/sftp/SftpException;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->a(Lcom/jscape/inet/sftp/SftpException;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Lcom/jscape/inet/sftp/SftpException; {:try_start_6 .. :try_end_6} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->a(Lcom/jscape/inet/sftp/SftpException;)Lcom/jscape/inet/sftp/SftpException;

    move-result-object p1

    throw p1

    :cond_6
    :goto_3
    iget-object p1, p0, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->b:Lcom/jscape/inet/sftp/Sftp;

    invoke-virtual {p1, v0}, Lcom/jscape/inet/sftp/Sftp;->setDir(Ljava/lang/String;)V

    :cond_7
    return-void
.end method


# virtual methods
.method public applyTo(Lcom/jscape/inet/sftp/Sftp;)Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/sftp/SftpException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->b:Lcom/jscape/inet/sftp/Sftp;

    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->c:Ljava/util/List;

    iget-object p1, p0, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->a:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->a(Ljava/lang/String;)V

    return-object p0
.end method

.method public files()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/jscape/inet/sftp/SftpFile;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/sftp/Sftp$RecursiveFileListOperation;->c:Ljava/util/List;

    return-object v0
.end method
