.class public Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Ljava/io/InputStream;

.field private final b:[B

.field private c:J

.field private final d:Lcom/jscape/inet/sftp/FileService$TransferListener;

.field private final e:Ljava/util/concurrent/locks/Lock;

.field private final f:Ljava/util/concurrent/locks/Condition;

.field private final g:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final h:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Ljava/lang/Exception;",
            ">;"
        }
    .end annotation
.end field

.field private final i:[B

.field private j:J

.field private volatile k:Z

.field private volatile l:Z

.field private volatile m:Z

.field final n:Lcom/jscape/inet/sftp/SftpFileService3;


# direct methods
.method public constructor <init>(Lcom/jscape/inet/sftp/SftpFileService3;Ljava/io/InputStream;[BJLcom/jscape/inet/sftp/FileService$TransferListener;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->n:Lcom/jscape/inet/sftp/SftpFileService3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->a:Ljava/io/InputStream;

    iput-object p3, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->b:[B

    iput-wide p4, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->c:J

    iput-object p6, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->d:Lcom/jscape/inet/sftp/FileService$TransferListener;

    new-instance p2, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object p2, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p2}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object p2

    iput-object p2, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->f:Ljava/util/concurrent/locks/Condition;

    new-instance p2, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 p3, 0x0

    invoke-direct {p2, p3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object p2, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance p2, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p2}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object p2, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->h:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {p1}, Lcom/jscape/inet/sftp/SftpFileService3;->e(Lcom/jscape/inet/sftp/SftpFileService3;)I

    move-result p1

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->i:[B

    return-void
.end method

.method private a()V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    iget-object v2, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->n:Lcom/jscape/inet/sftp/SftpFileService3;

    invoke-static {v2}, Lcom/jscape/inet/sftp/SftpFileService3;->b(Lcom/jscape/inet/sftp/SftpFileService3;)I

    move-result v2

    if-le v1, v2, :cond_1

    const-wide/16 v1, 0x1

    invoke-static {v1, v2}, Lcom/jscape/util/Time;->millis(J)Lcom/jscape/util/Time;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jscape/util/Time;->sleepSafe()V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->h:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    return-void
.end method

.method private static b(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private b()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->k:Z

    if-eqz v0, :cond_2

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->h:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move v0, v1

    :goto_1
    return v0
.end method

.method private c()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->a:Ljava/io/InputStream;

    iget-object v2, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->i:[B

    invoke-virtual {v1, v2}, Ljava/io/InputStream;->read([B)I

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v0, :cond_0

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    :try_start_1
    iget-object v2, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->n:Lcom/jscape/inet/sftp/SftpFileService3;

    new-instance v11, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpWrite;

    iget-object v3, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->n:Lcom/jscape/inet/sftp/SftpFileService3;

    invoke-static {v3}, Lcom/jscape/inet/sftp/SftpFileService3;->d(Lcom/jscape/inet/sftp/SftpFileService3;)I

    move-result v4

    iget-object v5, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->b:[B

    iget-wide v6, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->c:J

    iget-object v8, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->i:[B

    const/4 v9, 0x0

    move-object v3, v11

    move v10, v1

    invoke-direct/range {v3 .. v10}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpWrite;-><init>(I[BJ[BII)V

    invoke-static {v2, v11}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Lcom/jscape/inet/sftp/SftpFileService3;Lcom/jscape/inet/sftp/protocol/messages/Message;)V

    iget-object v2, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_0
    :goto_0
    :try_start_3
    iget-wide v2, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->c:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->c:J

    iget-wide v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->j:J

    add-long/2addr v1, v4

    iput-wide v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->j:J

    iget-object v3, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->d:Lcom/jscape/inet/sftp/FileService$TransferListener;

    invoke-interface {v3, v1, v2}, Lcom/jscape/inet/sftp/FileService$TransferListener;->onTransferProgress(J)V

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->k:Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->a(Ljava/lang/Exception;)V

    :cond_2
    :goto_1
    return-void
.end method

.method private d()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->m:Z

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->n:Lcom/jscape/inet/sftp/SftpFileService3;

    new-instance v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpClose;

    iget-object v2, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->n:Lcom/jscape/inet/sftp/SftpFileService3;

    invoke-static {v2}, Lcom/jscape/inet/sftp/SftpFileService3;->d(Lcom/jscape/inet/sftp/SftpFileService3;)I

    move-result v2

    iget-object v3, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->b:[B

    invoke-direct {v1, v2, v3}, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpClose;-><init>(I[B)V

    invoke-static {v0, v1}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Lcom/jscape/inet/sftp/SftpFileService3;Lcom/jscape/inet/sftp/protocol/messages/Message;)V

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->g()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private e()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->m:Z

    if-nez v0, :cond_2

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move v0, v1

    :goto_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private f()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->h:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->h:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    check-cast v1, Ljava/lang/Exception;

    throw v1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private g()V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->l:Z

    if-nez v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->f:Ljava/util/concurrent/locks/Condition;

    const-wide/16 v2, 0x1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/locks/Condition;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method private h()V
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->l:Z

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->f:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method


# virtual methods
.method public execute()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->n:Lcom/jscape/inet/sftp/SftpFileService3;

    invoke-static {v1}, Lcom/jscape/inet/sftp/SftpFileService3;->a(Lcom/jscape/inet/sftp/SftpFileService3;)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    if-eqz v0, :cond_1

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->b()Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_1

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->c()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->a()V

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->d()V

    return-void
.end method

.method public run()V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->n:Lcom/jscape/inet/sftp/SftpFileService3;

    invoke-static {v1}, Lcom/jscape/inet/sftp/SftpFileService3;->c(Lcom/jscape/inet/sftp/SftpFileService3;)Lcom/jscape/inet/sftp/protocol/messages/Message;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    check-cast v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStatus;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_3

    :try_start_1
    iget v2, v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStatus;->code:I

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->SSH_FX_OK:Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;

    iget v3, v3, Lcom/jscape/inet/sftp/protocol/v3/messages/StatusCode;->code:I

    if-eq v2, v3, :cond_1

    new-instance v2, Lcom/jscape/inet/sftp/FileService$ServerException;

    iget v3, v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStatus;->code:I

    iget-object v4, v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStatus;->message:Ljava/lang/String;

    iget-object v1, v1, Lcom/jscape/inet/sftp/protocol/v3/messages/SshFxpStatus;->languageTag:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v1}, Lcom/jscape/inet/sftp/FileService$ServerException;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->a(Ljava/lang/Exception;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_3
    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->a(Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->h()V

    :cond_3
    return-void

    :goto_1
    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService3$WriteOperation;->h()V

    throw v0
.end method
