.class public Lcom/jscape/inet/sftp/FileService$ServerException;
.super Lcom/jscape/inet/sftp/FileService$OperationException;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final serialVersionUID:J = -0x19941ca0d861f355L


# instance fields
.field public final code:I

.field public final languageTag:Ljava/lang/String;

.field public final message:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "w\u0005\u001dhL4\'A\u0012\u001dq[|\'\u0001\u0013O6\u000c5.\u0004HJm\u0000h"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/sftp/FileService$ServerException;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x71

    goto :goto_1

    :cond_1
    const/16 v4, 0x30

    goto :goto_1

    :cond_2
    const/16 v4, 0x5f

    goto :goto_1

    :cond_3
    const/16 v4, 0x68

    goto :goto_1

    :cond_4
    const/16 v4, 0x19

    goto :goto_1

    :cond_5
    const/16 v4, 0x16

    goto :goto_1

    :cond_6
    const/16 v4, 0x52

    :goto_1
    const/16 v5, 0x76

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    sget-object v0, Lcom/jscape/inet/sftp/FileService$ServerException;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/FileService$OperationException;-><init>(Ljava/lang/String;)V

    iput p1, p0, Lcom/jscape/inet/sftp/FileService$ServerException;->code:I

    iput-object p2, p0, Lcom/jscape/inet/sftp/FileService$ServerException;->message:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/inet/sftp/FileService$ServerException;->languageTag:Ljava/lang/String;

    return-void
.end method
