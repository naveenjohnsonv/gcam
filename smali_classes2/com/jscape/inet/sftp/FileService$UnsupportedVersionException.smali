.class public Lcom/jscape/inet/sftp/FileService$UnsupportedVersionException;
.super Lcom/jscape/inet/sftp/FileService$OperationException;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final serialVersionUID:J = -0x45b1c3364fb74e72L


# instance fields
.field public final clientVersion:I

.field public final serverVersion:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "2Ez,\u0003`2\u000fB{/\u000b0#\tDm,\r\u007f?[]|1\u001dy<\u0015X7c-|:\u001eEmyN7v\u0008\u000c5c\u001du!\rNkyN7v\u0008\u000c7"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/sftp/FileService$UnsupportedVersionException;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x1b

    goto :goto_1

    :cond_1
    const/16 v4, 0x58

    goto :goto_1

    :cond_2
    const/16 v4, 0x26

    goto :goto_1

    :cond_3
    const/16 v4, 0xb

    goto :goto_1

    :cond_4
    const/16 v4, 0x51

    goto :goto_1

    :cond_5
    const/16 v4, 0x63

    goto :goto_1

    :cond_6
    const/16 v4, 0x33

    :goto_1
    const/16 v5, 0x48

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(II)V
    .locals 4

    sget-object v0, Lcom/jscape/inet/sftp/FileService$UnsupportedVersionException;->a:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/FileService$OperationException;-><init>(Ljava/lang/String;)V

    iput p1, p0, Lcom/jscape/inet/sftp/FileService$UnsupportedVersionException;->clientVersion:I

    iput p2, p0, Lcom/jscape/inet/sftp/FileService$UnsupportedVersionException;->serverVersion:I

    return-void
.end method
