.class public Lcom/jscape/inet/sftp/FileService$OperationException;
.super Lcom/jscape/util/n/b;


# static fields
.field private static final serialVersionUID:J = -0x16d946e2a284e101L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/util/n/b;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/util/n/b;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/util/n/b;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0, p1}, Lcom/jscape/util/n/b;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/sftp/FileService$OperationException;
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    if-nez v0, :cond_1

    instance-of v0, p0, Lcom/jscape/inet/sftp/FileService$OperationException;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/jscape/inet/sftp/FileService$OperationException;

    invoke-direct {v0, p0}, Lcom/jscape/inet/sftp/FileService$OperationException;-><init>(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    :goto_0
    move-object v0, p0

    check-cast v0, Lcom/jscape/inet/sftp/FileService$OperationException;

    :goto_1
    return-object v0
.end method
