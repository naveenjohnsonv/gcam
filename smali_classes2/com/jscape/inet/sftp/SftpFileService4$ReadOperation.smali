.class public Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:[B

.field private b:J

.field private final c:Ljava/io/OutputStream;

.field private final d:Lcom/jscape/inet/sftp/FileService$TransferListener;

.field private final e:Ljava/util/concurrent/locks/Lock;

.field private final f:Ljava/util/concurrent/locks/Condition;

.field private final g:Ljava/util/concurrent/atomic/AtomicInteger;

.field private h:J

.field private volatile i:Z

.field private volatile j:Z

.field private volatile k:Ljava/lang/Exception;

.field private volatile l:Z

.field final m:Lcom/jscape/inet/sftp/SftpFileService4;


# direct methods
.method public constructor <init>(Lcom/jscape/inet/sftp/SftpFileService4;[BJLjava/io/OutputStream;Lcom/jscape/inet/sftp/FileService$TransferListener;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->m:Lcom/jscape/inet/sftp/SftpFileService4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->a:[B

    iput-wide p3, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->b:J

    iput-object p5, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->c:Ljava/io/OutputStream;

    iput-object p6, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->d:Lcom/jscape/inet/sftp/FileService$TransferListener;

    new-instance p1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {p1}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->f:Ljava/util/concurrent/locks/Condition;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object p1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method private a()V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    iget-object v2, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->m:Lcom/jscape/inet/sftp/SftpFileService4;

    invoke-static {v2}, Lcom/jscape/inet/sftp/SftpFileService4;->b(Lcom/jscape/inet/sftp/SftpFileService4;)I

    move-result v2

    if-le v1, v2, :cond_1

    const-wide/16 v1, 0x1

    invoke-static {v1, v2}, Lcom/jscape/util/Time;->millis(J)Lcom/jscape/util/Time;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jscape/util/Time;->sleepSafe()V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->k:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    :cond_0
    iput-object p1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->k:Ljava/lang/Exception;

    :cond_1
    return-void
.end method

.method private a([B)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->c:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    iget-wide v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->h:J

    array-length p1, p1

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->h:J

    iget-object p1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->d:Lcom/jscape/inet/sftp/FileService$TransferListener;

    invoke-interface {p1, v0, v1}, Lcom/jscape/inet/sftp/FileService$TransferListener;->onTransferProgress(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->a(Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method

.method private static b(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private b()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->m:Lcom/jscape/inet/sftp/SftpFileService4;

    new-instance v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpClose;

    iget-object v2, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->m:Lcom/jscape/inet/sftp/SftpFileService4;

    invoke-static {v2}, Lcom/jscape/inet/sftp/SftpFileService4;->d(Lcom/jscape/inet/sftp/SftpFileService4;)I

    move-result v2

    iget-object v3, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->a:[B

    invoke-direct {v1, v2, v3}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpClose;-><init>(I[B)V

    invoke-static {v0, v1}, Lcom/jscape/inet/sftp/SftpFileService4;->a(Lcom/jscape/inet/sftp/SftpFileService4;Lcom/jscape/inet/sftp/protocol/messages/Message;)V

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->l:Z

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->f()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private c()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    iget-boolean v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->i:Z

    if-nez v0, :cond_2

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->k:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move v0, v1

    :goto_1
    return v0
.end method

.method private d()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->l:Z

    if-eqz v0, :cond_2

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move v0, v1

    :goto_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private e()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->k:Ljava/lang/Exception;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->k:Ljava/lang/Exception;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    throw v1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private f()V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->j:Z

    if-nez v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->f:Ljava/util/concurrent/locks/Condition;

    const-wide/16 v2, 0x1

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Ljava/util/concurrent/locks/Condition;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->j:Z

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->f:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->e:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method


# virtual methods
.method public execute()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->k()I

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->m:Lcom/jscape/inet/sftp/SftpFileService4;

    invoke-static {v1}, Lcom/jscape/inet/sftp/SftpFileService4;->a(Lcom/jscape/inet/sftp/SftpFileService4;)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    if-eqz v0, :cond_1

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->c()Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->sendRequest()V

    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->a()V

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->b()V

    return-void
.end method

.method public run()V
    .locals 5

    invoke-static {}, Lcom/jscape/inet/sftp/Sftp;->j()I

    move-result v0

    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->d()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->m:Lcom/jscape/inet/sftp/SftpFileService4;

    invoke-static {v1}, Lcom/jscape/inet/sftp/SftpFileService4;->c(Lcom/jscape/inet/sftp/SftpFileService4;)Lcom/jscape/inet/sftp/protocol/messages/Message;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v2, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_6

    if-nez v0, :cond_1

    :try_start_2
    instance-of v2, v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpData;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v2, :cond_1

    :try_start_3
    move-object v2, v1

    check-cast v2, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpData;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    iget-object v2, v2, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpData;->data:[B

    invoke-direct {p0, v2}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->a([B)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_4

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    check-cast v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    iget v2, v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;->code:I

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_EOF:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    iget v3, v3, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->code:I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    if-nez v0, :cond_3

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    :try_start_7
    iput-boolean v2, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->i:Z

    if-eqz v0, :cond_4

    :cond_2
    iget v2, v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;->code:I

    sget-object v3, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->SSH_FX_OK:Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;

    iget v3, v3, Lcom/jscape/inet/sftp/protocol/v4/messages/StatusCode;->code:I
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_3
    if-eq v2, v3, :cond_4

    :try_start_8
    new-instance v2, Lcom/jscape/inet/sftp/FileService$ServerException;

    iget v3, v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;->code:I

    iget-object v4, v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;->message:Ljava/lang/String;

    iget-object v1, v1, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpStatus;->languageTag:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v1}, Lcom/jscape/inet/sftp/FileService$ServerException;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->a(Ljava/lang/Exception;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_9
    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_7
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :cond_4
    :goto_1
    if-eqz v0, :cond_0

    goto :goto_2

    :catch_2
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_3
    move-exception v0

    :try_start_b
    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catch_4
    move-exception v0

    :try_start_c
    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_7
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :catch_5
    move-exception v0

    :try_start_d
    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_6
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :catch_6
    move-exception v0

    :try_start_e
    invoke-static {v0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_7
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_3

    :catch_7
    move-exception v0

    :try_start_f
    invoke-direct {p0, v0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->a(Ljava/lang/Exception;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :cond_5
    :goto_2
    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->g()V

    :cond_6
    return-void

    :goto_3
    invoke-direct {p0}, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->g()V

    throw v0
.end method

.method public sendRequest()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->m:Lcom/jscape/inet/sftp/SftpFileService4;

    new-instance v7, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpRead;

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->m:Lcom/jscape/inet/sftp/SftpFileService4;

    invoke-static {v1}, Lcom/jscape/inet/sftp/SftpFileService4;->d(Lcom/jscape/inet/sftp/SftpFileService4;)I

    move-result v2

    iget-object v3, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->a:[B

    iget-wide v4, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->b:J

    iget-object v1, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->m:Lcom/jscape/inet/sftp/SftpFileService4;

    invoke-static {v1}, Lcom/jscape/inet/sftp/SftpFileService4;->e(Lcom/jscape/inet/sftp/SftpFileService4;)I

    move-result v6

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/jscape/inet/sftp/protocol/v4/messages/SshFxpRead;-><init>(I[BJI)V

    invoke-static {v0, v7}, Lcom/jscape/inet/sftp/SftpFileService4;->a(Lcom/jscape/inet/sftp/SftpFileService4;Lcom/jscape/inet/sftp/protocol/messages/Message;)V

    iget-object v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    iget-wide v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->b:J

    iget-object v2, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->m:Lcom/jscape/inet/sftp/SftpFileService4;

    invoke-static {v2}, Lcom/jscape/inet/sftp/SftpFileService4;->e(Lcom/jscape/inet/sftp/SftpFileService4;)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/jscape/inet/sftp/SftpFileService4$ReadOperation;->b:J

    return-void
.end method
