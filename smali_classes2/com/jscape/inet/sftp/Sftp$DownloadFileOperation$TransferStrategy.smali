.class public abstract enum Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ALWAYS_RESUME:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

.field public static final enum FROM_SCRATCH:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

.field public static final enum RESUME_AFTER_FIRST_ATTEMPT:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

.field private static final a:[Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xc

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x22

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "_T\u0017[$L\'KG\u000cU3\rXJ\u000fW\"L;KC\u000bC6Z\u001aKC\u000bC6Z;X@\u000cS)@\"PT\u000bB$^0MC\u0015F/"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    const/4 v11, 0x2

    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x35

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    new-instance v3, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy$1;

    aget-object v4, v1, v2

    invoke-direct {v3, v4, v2}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy$1;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;->FROM_SCRATCH:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

    new-instance v3, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy$2;

    aget-object v4, v1, v7

    invoke-direct {v3, v4, v7}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy$2;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;->ALWAYS_RESUME:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

    new-instance v3, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy$3;

    aget-object v1, v1, v11

    invoke-direct {v3, v1, v11}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy$3;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;->RESUME_AFTER_FIRST_ATTEMPT:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

    new-array v0, v0, [Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

    sget-object v1, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;->FROM_SCRATCH:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;->ALWAYS_RESUME:Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

    aput-object v1, v0, v7

    aput-object v3, v0, v11

    sput-object v0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;->a:[Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

    return-void

    :cond_1
    aget-char v12, v4, v10

    rem-int/lit8 v13, v10, 0x7

    if-eqz v13, :cond_7

    if-eq v13, v7, :cond_6

    if-eq v13, v11, :cond_5

    if-eq v13, v0, :cond_4

    const/4 v11, 0x4

    if-eq v13, v11, :cond_3

    const/4 v11, 0x5

    if-eq v13, v11, :cond_2

    const/16 v11, 0x46

    goto :goto_2

    :cond_2
    const/16 v11, 0x3d

    goto :goto_2

    :cond_3
    const/16 v11, 0x59

    goto :goto_2

    :cond_4
    const/16 v11, 0x34

    goto :goto_2

    :cond_5
    const/16 v11, 0x7a

    goto :goto_2

    :cond_6
    const/16 v11, 0x24

    goto :goto_2

    :cond_7
    const/16 v11, 0x3b

    :goto_2
    xor-int/2addr v11, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILcom/jscape/inet/sftp/Sftp$1;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;
    .locals 1

    const-class v0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;
    .locals 1

    sget-object v0, Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;->a:[Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

    invoke-virtual {v0}, [Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation$TransferStrategy;

    return-object v0
.end method


# virtual methods
.method public abstract apply(Lcom/jscape/inet/sftp/Sftp$DownloadFileOperation;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method
