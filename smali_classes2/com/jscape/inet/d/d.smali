.class public Lcom/jscape/inet/d/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/d/c;


# static fields
.field private static final a:Ljava/lang/String; = ""

.field private static final b:Ljava/lang/String; = "."

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String; = "/"

.field private static final e:Ljava/lang/String; = "\\"

.field private static final f:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, -0x1

    move v4, v0

    move v5, v2

    :goto_0
    const/16 v6, 0x7c

    const/4 v7, 0x1

    add-int/2addr v3, v7

    add-int/2addr v4, v3

    const-string v8, "g\u0017\u0002g\u0017"

    invoke-virtual {v8, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    array-length v9, v3

    move v10, v2

    :goto_1
    const/4 v11, 0x5

    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v3}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v6, v5, 0x1

    aput-object v3, v1, v5

    if-ge v4, v11, :cond_0

    invoke-virtual {v8, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    move v5, v6

    move/from16 v16, v4

    move v4, v3

    move/from16 v3, v16

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/d/d;->f:[Ljava/lang/String;

    aget-object v0, v1, v2

    sput-object v0, Lcom/jscape/inet/d/d;->c:Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v3, v10

    rem-int/lit8 v13, v10, 0x7

    const/16 v14, 0x13

    if-eqz v13, :cond_6

    if-eq v13, v7, :cond_5

    if-eq v13, v0, :cond_7

    const/4 v15, 0x3

    if-eq v13, v15, :cond_4

    const/4 v15, 0x4

    if-eq v13, v15, :cond_3

    if-eq v13, v11, :cond_2

    goto :goto_2

    :cond_2
    const/16 v14, 0x6c

    goto :goto_2

    :cond_3
    const/16 v14, 0x66

    goto :goto_2

    :cond_4
    const/16 v14, 0x26

    goto :goto_2

    :cond_5
    const/16 v14, 0x45

    goto :goto_2

    :cond_6
    const/16 v14, 0x35

    :cond_7
    :goto_2
    xor-int v11, v6, v14

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v3, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/jscape/inet/d/a;)Lcom/jscape/inet/d/a;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/d/a;->c()I

    move-result v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v1, :cond_1

    if-nez v1, :cond_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/d/a;->b()I

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_1
    :goto_0
    return-object p1
.end method

.method private i(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method private j(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "\\"

    const-string v1, "/"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private k(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/d/a;
        }
    .end annotation

    const-string v0, "/"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/jscape/util/at;->a(Ljava/lang/String;Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/d/a;->c()I

    move-result v2

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    array-length v4, v0

    :cond_0
    if-ge v1, v4, :cond_7

    aget-object v5, v0, v1

    :try_start_0
    invoke-virtual {p0, v5}, Lcom/jscape/inet/d/d;->d(Ljava/lang/String;)Z

    move-result v6
    :try_end_0
    .catch Lcom/jscape/inet/d/a; {:try_start_0 .. :try_end_0} :catch_4

    if-eqz v2, :cond_2

    if-eqz v6, :cond_1

    invoke-virtual {p0, v5}, Lcom/jscape/inet/d/d;->c(Ljava/lang/String;)Z

    move-result v6

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v0, Lcom/jscape/inet/d/a;

    invoke-direct {v0, p1}, Lcom/jscape/inet/d/a;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/jscape/inet/d/a; {:try_start_1 .. :try_end_1} :catch_5

    :cond_2
    :goto_0
    if-eqz v2, :cond_4

    if-eqz v6, :cond_3

    :try_start_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6
    :try_end_2
    .catch Lcom/jscape/inet/d/a; {:try_start_2 .. :try_end_2} :catch_0

    if-eqz v2, :cond_6

    if-lez v6, :cond_5

    :try_start_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v3, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;
    :try_end_3
    .catch Lcom/jscape/inet/d/a; {:try_start_3 .. :try_end_3} :catch_2

    if-nez v2, :cond_5

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/d/d;->a(Lcom/jscape/inet/d/a;)Lcom/jscape/inet/d/a;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/inet/d/a; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/d/d;->a(Lcom/jscape/inet/d/a;)Lcom/jscape/inet/d/a;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/inet/d/a; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/d/d;->a(Lcom/jscape/inet/d/a;)Lcom/jscape/inet/d/a;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    invoke-virtual {p0, v5}, Lcom/jscape/inet/d/d;->b(Ljava/lang/String;)Z

    move-result v6
    :try_end_6
    .catch Lcom/jscape/inet/d/a; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_2

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/d/d;->a(Lcom/jscape/inet/d/a;)Lcom/jscape/inet/d/a;

    move-result-object p1

    throw p1

    :cond_4
    :goto_2
    if-eqz v2, :cond_6

    if-eqz v6, :cond_5

    goto :goto_3

    :cond_5
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    :goto_3
    add-int/lit8 v1, v1, 0x1

    if-nez v2, :cond_0

    goto :goto_4

    :catch_4
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/d/d;->a(Lcom/jscape/inet/d/a;)Lcom/jscape/inet/d/a;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Lcom/jscape/inet/d/a; {:try_start_7 .. :try_end_7} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/d/d;->a(Lcom/jscape/inet/d/a;)Lcom/jscape/inet/d/a;

    move-result-object p1

    throw p1

    :cond_7
    :goto_4
    return-object v3
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/d/a;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/d/d;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/d/a;->b()I

    move-result v0

    invoke-direct {p0, p2}, Lcom/jscape/inet/d/d;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    if-nez v0, :cond_0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/d/d;->a(Ljava/lang/String;)Z

    move-result v1
    :try_end_0
    .catch Lcom/jscape/inet/d/a; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-virtual {p0, p2}, Lcom/jscape/inet/d/d;->a(Ljava/lang/String;)Z

    move-result v1
    :try_end_1
    .catch Lcom/jscape/inet/d/a; {:try_start_1 .. :try_end_1} :catch_2

    if-eqz v1, :cond_0

    :try_start_2
    const-string p1, "/"
    :try_end_2
    .catch Lcom/jscape/inet/d/a; {:try_start_2 .. :try_end_2} :catch_3

    return-object p1

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/d/d;->a(Lcom/jscape/inet/d/a;)Lcom/jscape/inet/d/a;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/inet/d/a; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/d/d;->a(Lcom/jscape/inet/d/a;)Lcom/jscape/inet/d/a;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/inet/d/a; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/d/d;->a(Lcom/jscape/inet/d/a;)Lcom/jscape/inet/d/a;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/inet/d/a; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/d/d;->a(Lcom/jscape/inet/d/a;)Lcom/jscape/inet/d/a;

    move-result-object p1

    throw p1

    :cond_0
    if-nez v0, :cond_2

    :try_start_6
    invoke-direct {p0, p2}, Lcom/jscape/inet/d/d;->i(Ljava/lang/String;)Z

    move-result v1
    :try_end_6
    .catch Lcom/jscape/inet/d/a; {:try_start_6 .. :try_end_6} :catch_4

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0, p1}, Lcom/jscape/inet/d/d;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :catch_4
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/d/d;->a(Lcom/jscape/inet/d/a;)Lcom/jscape/inet/d/a;

    move-result-object p1

    throw p1
    :try_end_7
    .catch Lcom/jscape/inet/d/a; {:try_start_7 .. :try_end_7} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/d/d;->a(Lcom/jscape/inet/d/a;)Lcom/jscape/inet/d/a;

    move-result-object p1

    throw p1

    :cond_2
    move-object p1, p2

    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/d/d;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    :goto_1
    :try_start_8
    invoke-direct {p0, p2}, Lcom/jscape/inet/d/d;->k(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/d/d;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    if-eqz v0, :cond_3

    const/4 p2, 0x1

    new-array p2, p2, [I

    invoke-static {p2}, Lcom/jscape/util/aq;->b([I)V
    :try_end_8
    .catch Lcom/jscape/inet/d/a; {:try_start_8 .. :try_end_8} :catch_6

    :cond_3
    return-object p1

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/d/d;->a(Lcom/jscape/inet/d/a;)Lcom/jscape/inet/d/a;

    move-result-object p1

    throw p1
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/d/a;->c()I

    move-result v0

    const-string v1, ""

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v0, :cond_2

    if-nez v1, :cond_1

    const-string v1, "/"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v0, :cond_2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move p1, v1

    :goto_1
    return p1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const-string v0, "."

    return-object v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "."

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/d/a;
        }
    .end annotation

    invoke-virtual {p0, p2}, Lcom/jscape/inet/d/d;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1}, Lcom/jscape/inet/d/d;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/d/d;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public c()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/jscape/inet/d/d;->f:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 2

    sget-object v0, Lcom/jscape/inet/d/d;->f:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public d()Ljava/lang/String;
    .locals 1

    const-string v0, "/"

    return-object v0
.end method

.method public d(Ljava/lang/String;)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/d/a;->b()I

    move-result v0

    invoke-static {p1}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Lcom/jscape/inet/d/d;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    :cond_0
    if-nez v0, :cond_2

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move p1, v1

    :goto_1
    return p1
.end method

.method public e(Ljava/lang/String;)Z
    .locals 0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/d/d;->f(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/jscape/inet/d/a; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    const/4 p1, 0x0

    return p1
.end method

.method public f(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/d/a;
        }
    .end annotation

    const-string v0, ""

    invoke-virtual {p0, v0, p1}, Lcom/jscape/inet/d/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public g(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/d/a;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/d/d;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/d/a;->b()I

    move-result v0

    invoke-direct {p0, p1}, Lcom/jscape/inet/d/d;->k(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    if-nez v0, :cond_1

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0
    :try_end_0
    .catch Lcom/jscape/inet/d/a; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    :try_start_1
    const-string p1, ""
    :try_end_1
    .catch Lcom/jscape/inet/d/a; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/d/d;->a(Lcom/jscape/inet/d/a;)Lcom/jscape/inet/d/a;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/d/a; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/d/d;->a(Lcom/jscape/inet/d/a;)Lcom/jscape/inet/d/a;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    check-cast p1, Ljava/lang/String;

    :goto_1
    return-object p1
.end method

.method public h(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/d/a;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/d/d;->f:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {p0, p1, v0}, Lcom/jscape/inet/d/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
