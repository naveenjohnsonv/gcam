.class public final Lcom/jscape/inet/d/b;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String; = "."

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String; = "/"

.field private static final d:C = '/'

.field private static final e:C = '\\'

.field private static final f:Ljava/lang/String; = "\\"

.field private static final g:Ljava/lang/String; = ""

.field private static final h:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, -0x1

    move v4, v0

    move v5, v2

    :goto_0
    const/4 v6, 0x6

    const/4 v7, 0x1

    add-int/2addr v3, v7

    add-int/2addr v4, v3

    const-string v8, "&\u0013\u0002&\u0013"

    invoke-virtual {v8, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    array-length v9, v3

    move v10, v2

    :goto_1
    const/4 v11, 0x5

    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v3}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v6, v5, 0x1

    aput-object v3, v1, v5

    if-ge v4, v11, :cond_0

    invoke-virtual {v8, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/d/b;->h:[Ljava/lang/String;

    aget-object v0, v1, v2

    sput-object v0, Lcom/jscape/inet/d/b;->b:Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v3, v10

    rem-int/lit8 v13, v10, 0x7

    if-eqz v13, :cond_7

    if-eq v13, v7, :cond_6

    if-eq v13, v0, :cond_5

    const/4 v14, 0x3

    if-eq v13, v14, :cond_4

    const/4 v14, 0x4

    if-eq v13, v14, :cond_3

    if-eq v13, v11, :cond_2

    const/4 v11, 0x7

    goto :goto_2

    :cond_2
    const/16 v11, 0x2a

    goto :goto_2

    :cond_3
    const/16 v11, 0x65

    goto :goto_2

    :cond_4
    const/16 v11, 0x20

    goto :goto_2

    :cond_5
    const/16 v11, 0x46

    goto :goto_2

    :cond_6
    const/16 v11, 0x3b

    goto :goto_2

    :cond_7
    const/16 v11, 0xe

    :goto_2
    xor-int/2addr v11, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v3, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/d/a;->c()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/jscape/inet/d/b;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/jscape/inet/d/b;->d()Ljava/lang/String;

    move-result-object p0

    :goto_0
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_1
    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_2

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/jscape/inet/d/a;->b(I)V

    :cond_2
    return-object p0
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/d/a;->c()I

    move-result v1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v2, :cond_1

    invoke-static {}, Lcom/jscape/inet/d/b;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v1, :cond_1

    add-int/lit8 v3, v3, 0x1

    if-nez v1, :cond_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/d/b;->f(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/d/a;->c()I

    move-result v0

    const/4 v1, 0x0

    :cond_0
    array-length v2, p1

    if-ge v1, v2, :cond_7

    aget-object v2, p1, v1

    invoke-static {v2}, Lcom/jscape/inet/d/b;->d(Ljava/lang/String;)Z

    move-result v3

    if-eqz v0, :cond_2

    if-nez v3, :cond_1

    const/4 p0, 0x0

    return-object p0

    :cond_1
    invoke-static {}, Lcom/jscape/inet/d/b;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    :cond_2
    if-eqz v0, :cond_5

    if-eqz v3, :cond_4

    if-eqz v0, :cond_3

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_6

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {p0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_3
    if-nez v0, :cond_6

    :cond_4
    invoke-static {}, Lcom/jscape/inet/d/b;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    :cond_5
    if-eqz v0, :cond_6

    if-nez v3, :cond_6

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_0

    :cond_7
    return-object p0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/d/a;->c()I

    move-result v0

    const-string v1, ""

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v0, :cond_2

    if-nez v1, :cond_1

    const-string v1, "/"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v0, :cond_2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move p0, v1

    :goto_1
    return p0
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    const-string v0, "."

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/jscape/inet/d/a;->b()I

    move-result v0

    const-string v1, ""

    if-nez v0, :cond_2

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object p0, v1

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {}, Lcom/jscape/inet/d/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/jscape/inet/d/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/inet/d/b;->f(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    if-nez v0, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p0

    if-lez p0, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    invoke-interface {v1, p0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_1
    invoke-static {v1}, Lcom/jscape/inet/d/b;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object p0

    :cond_2
    :goto_0
    return-object p0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/jscape/inet/d/a;->c()I

    move-result v0

    const-string v1, ""

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v0, :cond_1

    if-eqz v2, :cond_0

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v0, :cond_1

    if-eqz v2, :cond_0

    return-object v1

    :cond_0
    invoke-static {v1, p0}, Lcom/jscape/inet/d/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    :cond_1
    if-eqz v0, :cond_4

    if-nez v2, :cond_2

    return-object v1

    :cond_2
    const/16 v1, 0x5c

    invoke-static {}, Lcom/jscape/inet/d/b;->e()C

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p1

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/jscape/inet/d/b;->e(Ljava/lang/String;)Z

    move-result v2

    goto :goto_0

    :cond_3
    move-object p0, p1

    goto :goto_1

    :cond_4
    :goto_0
    if-eqz v2, :cond_5

    new-instance p0, Ljava/util/LinkedList;

    invoke-direct {p0}, Ljava/util/LinkedList;-><init>()V

    goto :goto_2

    :cond_5
    :goto_1
    invoke-static {p0}, Lcom/jscape/inet/d/b;->f(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/util/G;->b([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    :goto_2
    invoke-static {p0, p1}, Lcom/jscape/inet/d/b;->a(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object p0

    if-eqz v0, :cond_7

    if-eqz p0, :cond_6

    goto :goto_3

    :cond_6
    const/4 p0, 0x0

    goto :goto_4

    :cond_7
    :goto_3
    invoke-static {p0}, Lcom/jscape/inet/d/b;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object p0

    :goto_4
    return-object p0
.end method

.method public static c()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/jscape/inet/d/b;->h:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/jscape/inet/d/a;->b()I

    move-result v0

    const-string v1, ""

    if-nez v0, :cond_3

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/jscape/inet/d/b;->d()Ljava/lang/String;

    move-result-object v2

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move-object v2, p0

    move-object p0, v1

    :cond_1
    invoke-static {p0, v2}, Lcom/jscape/inet/d/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/inet/d/b;->f(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    aget-object p0, p0, v0

    return-object p0

    :cond_2
    :goto_0
    move-object p0, v1

    :cond_3
    return-object p0
.end method

.method public static d()Ljava/lang/String;
    .locals 1

    const-string v0, "/"

    return-object v0
.end method

.method public static d(Ljava/lang/String;)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/d/a;->b()I

    move-result v0

    invoke-static {p0}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v0, :cond_0

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/jscape/inet/d/b;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    :cond_0
    if-nez v0, :cond_1

    if-nez v1, :cond_2

    const-string v1, "\\"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    :cond_1
    if-nez v0, :cond_3

    if-nez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    move p0, v1

    :goto_1
    return p0
.end method

.method public static e()C
    .locals 1

    const/16 v0, 0x2f

    return v0
.end method

.method private static e(Ljava/lang/String;)Z
    .locals 1

    invoke-static {}, Lcom/jscape/inet/d/b;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static f(Ljava/lang/String;)[Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/d/b;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/jscape/util/at;->a(Ljava/lang/String;Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
