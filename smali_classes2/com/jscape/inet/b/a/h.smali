.class public Lcom/jscape/inet/b/a/h;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/AutoCloseable;


# static fields
.field private static final f:[Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/jscape/inet/b/a/b/h;

.field public final d:Ljava/io/InputStream;

.field public final e:Lcom/jscape/inet/b/a/b/h;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x7

    const-string v5, "\u0008W\u0007\u001c\u001eL\u0015\n\u0008W\u001a\u000b\u0016\u0015WV\u0004O\u001av\u0012\u0003\u001b\u0012\u0002F\u0004\u000c\u0002\u001c\u0018\u0005]G\u0018\u001e8\u0012\u0003AM\u0018\u001cSP"

    const/16 v6, 0x2d

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x12

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v2

    invoke-virtual {v5, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v2

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v6, 0x19

    const/16 v2, 0x11

    const-string v5, "E\u001aKQ[U\u0013\u000cHwF[X\u001a\u001bI\u0002\u0007E\u001a]L^EB"

    move v8, v11

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v2

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v11

    :goto_3
    const/16 v9, 0x5f

    add-int/2addr v7, v10

    add-int v11, v7, v2

    invoke-virtual {v5, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/b/a/h;->f:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v3, v14, 0x7

    const/16 v16, 0x65

    if-eqz v3, :cond_7

    if-eq v3, v10, :cond_8

    const/4 v4, 0x2

    if-eq v3, v4, :cond_6

    const/4 v4, 0x3

    if-eq v3, v4, :cond_5

    const/4 v4, 0x4

    if-eq v3, v4, :cond_8

    if-eq v3, v0, :cond_4

    const/16 v16, 0x20

    goto :goto_4

    :cond_4
    const/16 v16, 0x63

    goto :goto_4

    :cond_5
    const/16 v16, 0x7c

    goto :goto_4

    :cond_6
    const/16 v16, 0x60

    goto :goto_4

    :cond_7
    const/16 v16, 0x36

    :cond_8
    :goto_4
    xor-int v3, v9, v16

    xor-int/2addr v3, v15

    int-to-char v3, v3

    aput-char v3, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/b/a/b/h;Ljava/io/InputStream;)V
    .locals 6

    new-instance v5, Lcom/jscape/inet/b/a/b/h;

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/jscape/inet/b/a/b/g;

    invoke-direct {v5, v0}, Lcom/jscape/inet/b/a/b/h;-><init>([Lcom/jscape/inet/b/a/b/g;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/b/a/h;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/b/a/b/h;Ljava/io/InputStream;Lcom/jscape/inet/b/a/b/h;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/b/a/b/h;Ljava/io/InputStream;Lcom/jscape/inet/b/a/b/h;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/b/a/h;->a:Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/jscape/inet/b/a/h;->b:Ljava/lang/String;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/b/a/h;->c:Lcom/jscape/inet/b/a/b/h;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/b/a/h;->d:Ljava/io/InputStream;

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/inet/b/a/h;->e:Lcom/jscape/inet/b/a/b/h;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/b/a/h;->d:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/b/a/h;->f:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/b/a/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/inet/b/a/h;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/b/a/h;->c:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/b/a/h;->d:Ljava/io/InputStream;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/b/a/h;->e:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
