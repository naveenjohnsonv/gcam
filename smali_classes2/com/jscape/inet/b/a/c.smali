.class public Lcom/jscape/inet/b/a/c;
.super Ljava/io/IOException;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/io/IOException;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static a(Ljava/lang/Throwable;)Lcom/jscape/inet/b/a/c;
    .locals 1

    invoke-static {}, Lcom/jscape/inet/b/a/e;->j()Z

    move-result v0

    if-nez v0, :cond_1

    instance-of v0, p0, Lcom/jscape/inet/b/a/c;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/jscape/inet/b/a/c;

    invoke-direct {v0, p0}, Lcom/jscape/inet/b/a/c;-><init>(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    :goto_0
    move-object v0, p0

    check-cast v0, Lcom/jscape/inet/b/a/c;

    :goto_1
    return-object v0
.end method
