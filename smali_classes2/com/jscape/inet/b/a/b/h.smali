.class public Lcom/jscape/inet/b/a/b/h;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "Lcom/jscape/inet/b/a/b/g;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/jscape/inet/b/a/b/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "GlS7c5\u0000/rW=r5\u001ajz\u000f"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/b/a/b/h;->b:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/4 v5, 0x1

    if-eqz v4, :cond_5

    if-eq v4, v5, :cond_4

    const/4 v6, 0x2

    if-eq v4, v6, :cond_3

    const/4 v6, 0x3

    if-eq v4, v6, :cond_2

    const/4 v6, 0x4

    if-eq v4, v6, :cond_1

    const/4 v6, 0x5

    if-eq v4, v6, :cond_6

    const/16 v5, 0x35

    goto :goto_1

    :cond_1
    const/16 v5, 0x40

    goto :goto_1

    :cond_2
    const/16 v5, 0x15

    goto :goto_1

    :cond_3
    const/16 v5, 0x74

    goto :goto_1

    :cond_4
    const/16 v5, 0x4f

    goto :goto_1

    :cond_5
    const/16 v5, 0x49

    :cond_6
    :goto_1
    const/16 v4, 0x46

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/jscape/inet/b/a/b/h;)V
    .locals 0

    iget-object p1, p1, Lcom/jscape/inet/b/a/b/h;->a:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/jscape/inet/b/a/b/h;-><init>(Ljava/util/List;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/jscape/inet/b/a/b/g;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/jscape/inet/b/a/b/h;->a:Ljava/util/List;

    return-void
.end method

.method public varargs constructor <init>([Lcom/jscape/inet/b/a/b/g;)V
    .locals 0

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/inet/b/a/b/h;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Ljava/lang/Integer;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/b/h;->c(Ljava/lang/String;)I

    move-result p1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/b/a/b/h;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/b/a/b/g;

    invoke-virtual {p1, p2}, Lcom/jscape/inet/b/a/b/g;->a(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public a(Lcom/jscape/inet/b/a/b/g;)V
    .locals 1

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jscape/inet/b/a/b/h;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/jscape/inet/b/a/b/g;

    invoke-direct {v0, p1, p2}, Lcom/jscape/inet/b/a/b/g;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/b/a/b/h;->b(Lcom/jscape/inet/b/a/b/g;)V

    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    invoke-static {}, Lcom/jscape/inet/b/a/b/i;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/b/h;->c(Ljava/lang/String;)I

    move-result p1

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    return p1
.end method

.method public a()[Lcom/jscape/inet/b/a/b/g;
    .locals 2

    iget-object v0, p0, Lcom/jscape/inet/b/a/b/h;->a:Ljava/util/List;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/jscape/inet/b/a/b/g;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/b/a/b/g;

    return-object v0
.end method

.method public b(Ljava/lang/String;I)Ljava/lang/Long;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/b/h;->c(Ljava/lang/String;)I

    move-result p1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/b/a/b/h;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/b/a/b/g;

    invoke-virtual {p1, p2}, Lcom/jscape/inet/b/a/b/g;->b(I)Ljava/lang/Long;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/b/h;->c(Ljava/lang/String;)I

    move-result p1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/jscape/inet/b/a/b/h;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/b/a/b/g;

    iget-object p1, p1, Lcom/jscape/inet/b/a/b/g;->b:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public b()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {}, Lcom/jscape/inet/b/a/b/i;->b()[Lcom/jscape/util/aq;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/b/a/b/h;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/b/a/b/g;

    if-eqz v1, :cond_2

    iget-object v4, v3, Lcom/jscape/inet/b/a/b/g;->a:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    if-eqz v1, :cond_1

    if-nez v4, :cond_1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    iget-object v3, v3, Lcom/jscape/inet/b/a/b/g;->b:Ljava/lang/String;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-nez v1, :cond_0

    :cond_2
    return-object v0
.end method

.method public b(Lcom/jscape/inet/b/a/b/g;)V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/b/a/b/i;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p1, Lcom/jscape/inet/b/a/b/g;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/jscape/inet/b/a/b/h;->c(Ljava/lang/String;)I

    move-result v1

    if-eqz v0, :cond_1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget-object v2, p0, Lcom/jscape/inet/b/a/b/h;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v2, p0, Lcom/jscape/inet/b/a/b/h;->a:Ljava/util/List;

    invoke-interface {v2, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/inet/b/a/b/h;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method public c(Ljava/lang/String;)I
    .locals 3

    invoke-static {}, Lcom/jscape/inet/b/a/b/i;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/b/a/b/h;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :cond_0
    if-ltz v1, :cond_3

    iget-object v2, p0, Lcom/jscape/inet/b/a/b/h;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/b/a/b/g;

    if-eqz v0, :cond_2

    iget-object v2, v2, Lcom/jscape/inet/b/a/b/g;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v0, :cond_4

    if-eqz v2, :cond_1

    return v1

    :cond_1
    add-int/lit8 v1, v1, -0x1

    :cond_2
    if-nez v0, :cond_0

    :cond_3
    const/4 v2, -0x1

    :cond_4
    return v2
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/jscape/inet/b/a/b/g;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/b/a/b/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/b/a/b/h;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/b/a/b/h;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
