.class public Lcom/jscape/inet/b/a/b/A;
.super Ljava/lang/Object;


# static fields
.field public static final A:I = 0x194

.field public static final B:I = 0x195

.field public static final C:I = 0x196

.field public static final D:I = 0x197

.field public static final E:I = 0x198

.field public static final F:I = 0x199

.field public static final G:I = 0x19a

.field public static final H:I = 0x19b

.field public static final I:I = 0x19c

.field public static final J:I = 0x19d

.field public static final K:I = 0x19e

.field public static final L:I = 0x19f

.field public static final M:I = 0x1a0

.field public static final N:I = 0x1a1

.field public static final O:I = 0x1a2

.field public static final P:I = 0x1a5

.field public static final Q:I = 0x1a6

.field public static final R:I = 0x1a7

.field public static final S:I = 0x1a8

.field public static final T:I = 0x1aa

.field public static final U:I = 0x1ac

.field public static final V:I = 0x1ad

.field public static final W:I = 0x1af

.field public static final X:I = 0x1c3

.field public static final Y:I = 0x1f4

.field public static final Z:I = 0x1f5

.field public static final a:I = 0x64

.field public static final aa:I = 0x1f6

.field public static final ab:I = 0x1f7

.field public static final ac:I = 0x1f8

.field public static final ad:I = 0x1f9

.field public static final ae:I = 0x1fa

.field public static final af:I = 0x1fb

.field public static final ag:I = 0x1fc

.field public static final ah:I = 0x1fe

.field public static final ai:I = 0x1ff

.field public static final b:I = 0x65

.field public static final c:I = 0x66

.field public static final d:I = 0xc8

.field public static final e:I = 0xc9

.field public static final f:I = 0xca

.field public static final g:I = 0xcb

.field public static final h:I = 0xcc

.field public static final i:I = 0xcd

.field public static final j:I = 0xce

.field public static final k:I = 0xcf

.field public static final l:I = 0xd0

.field public static final m:I = 0xe2

.field public static final n:I = 0x12c

.field public static final o:I = 0x12d

.field public static final p:I = 0x12e

.field public static final q:I = 0x12f

.field public static final r:I = 0x130

.field public static final s:I = 0x131

.field public static final t:I = 0x132

.field public static final u:I = 0x133

.field public static final v:I = 0x134

.field public static final w:I = 0x190

.field public static final x:I = 0x191

.field public static final y:I = 0x192

.field public static final z:I = 0x193


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/b/a/b/i;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    invoke-static {p0}, Lcom/jscape/inet/b/a/b/A;->b(I)Z

    move-result v1

    if-eqz v0, :cond_2

    if-nez v1, :cond_1

    if-eqz v0, :cond_3

    const/16 v1, 0xcc

    if-eq p0, v1, :cond_1

    if-eqz v0, :cond_3

    const/16 v0, 0x130

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    goto :goto_1

    :cond_2
    move p0, v1

    :cond_3
    :goto_1
    return p0
.end method

.method public static b(I)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/b/a/b/i;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    const/16 v1, 0x64

    if-eqz v0, :cond_0

    if-gt v1, p0, :cond_1

    if-eqz v0, :cond_2

    const/16 v0, 0xc7

    goto :goto_0

    :cond_0
    move v0, p0

    move p0, v1

    :goto_0
    if-gt p0, v0, :cond_1

    const/4 p0, 0x1

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    :cond_2
    :goto_1
    return p0
.end method

.method public static c(I)Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/b/a/b/i;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    const/16 v1, 0x190

    if-eqz v0, :cond_0

    if-gt v1, p0, :cond_1

    if-eqz v0, :cond_2

    const/16 v0, 0x258

    goto :goto_0

    :cond_0
    move v0, p0

    move p0, v1

    :goto_0
    if-gt p0, v0, :cond_1

    const/4 p0, 0x1

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    :cond_2
    :goto_1
    return p0
.end method
