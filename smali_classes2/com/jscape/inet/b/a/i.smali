.class public Lcom/jscape/inet/b/a/i;
.super Ljava/lang/Object;


# static fields
.field private static final g:[Ljava/lang/String;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:Lcom/jscape/inet/b/a/b/h;

.field public final e:Ljava/io/InputStream;

.field public final f:Lcom/jscape/inet/b/a/b/h;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x7

    const/4 v3, 0x0

    const-string v4, "(FnMExN\u001bV\u0003\u007fRNo\u0000aFwRSn\u0007k\u0005cNwd\u0001w\u000fcL\u001c&\r(F\u007fV@u\u0006w%cFD<\u0010(F~G@r\u001cj6dP@r\u00169A"

    const/16 v5, 0x42

    move v7, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x59

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v1

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v3

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v1

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v5, 0x1c

    const/16 v1, 0xa

    const-string v4, "\u0014zX{|Y*J)\r\u0011\u0014zDl|T#](x{|Y*J)\r"

    move v7, v10

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v1

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v10

    :goto_3
    const/16 v8, 0x65

    add-int/2addr v6, v9

    add-int v10, v6, v1

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/b/a/i;->g:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v2, 0x2

    if-eq v15, v2, :cond_7

    const/4 v2, 0x3

    if-eq v15, v2, :cond_6

    const/4 v2, 0x4

    if-eq v15, v2, :cond_5

    const/4 v2, 0x5

    if-eq v15, v2, :cond_4

    const/16 v2, 0x2a

    goto :goto_4

    :cond_4
    const/16 v2, 0x58

    goto :goto_4

    :cond_5
    const/16 v2, 0x78

    goto :goto_4

    :cond_6
    const/16 v2, 0x7b

    goto :goto_4

    :cond_7
    const/16 v2, 0x55

    goto :goto_4

    :cond_8
    const/16 v2, 0x3f

    goto :goto_4

    :cond_9
    const/16 v2, 0x5d

    :goto_4
    xor-int/2addr v2, v8

    xor-int/2addr v2, v14

    int-to-char v2, v2

    aput-char v2, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/jscape/inet/b/a/b/h;Ljava/io/InputStream;)V
    .locals 7

    new-instance v6, Lcom/jscape/inet/b/a/b/h;

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/jscape/inet/b/a/b/g;

    invoke-direct {v6, v0}, Lcom/jscape/inet/b/a/b/h;-><init>([Lcom/jscape/inet/b/a/b/g;)V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/b/a/i;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/jscape/inet/b/a/b/h;Ljava/io/InputStream;Lcom/jscape/inet/b/a/b/h;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/jscape/inet/b/a/b/h;Ljava/io/InputStream;Lcom/jscape/inet/b/a/b/h;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/b/a/i;->a:Ljava/lang/String;

    iput p2, p0, Lcom/jscape/inet/b/a/i;->b:I

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/b/a/i;->c:Ljava/lang/String;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/b/a/i;->d:Lcom/jscape/inet/b/a/b/h;

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/inet/b/a/i;->e:Ljava/io/InputStream;

    invoke-static {p6}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p6, p0, Lcom/jscape/inet/b/a/i;->f:Lcom/jscape/inet/b/a/b/h;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/b/a/e;->i()Z

    move-result v0

    const/16 v1, 0xc8

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/jscape/inet/b/a/i;->b:I

    if-ne v1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public b()Z
    .locals 1

    iget v0, p0, Lcom/jscape/inet/b/a/i;->b:I

    invoke-static {v0}, Lcom/jscape/inet/b/a/b/A;->b(I)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    iget v0, p0, Lcom/jscape/inet/b/a/i;->b:I

    invoke-static {v0}, Lcom/jscape/inet/b/a/b/A;->c(I)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    invoke-static {}, Lcom/jscape/inet/b/a/e;->j()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/b/a/i;->g:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v4, v2, v3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/b/a/i;->a:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x27

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v5, 0x2

    aget-object v5, v2, v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p0, Lcom/jscape/inet/b/a/i;->b:I

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v5, 0x3

    aget-object v5, v2, v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/jscape/inet/b/a/i;->c:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v4, 0x4

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/b/a/i;->d:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/b/a/i;->e:Ljava/io/InputStream;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v4, 0x5

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/b/a/i;->f:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v2

    if-nez v2, :cond_0

    xor-int/2addr v0, v3

    invoke-static {v0}, Lcom/jscape/inet/b/a/e;->b(Z)V

    :cond_0
    return-object v1
.end method
