.class public Lcom/jscape/inet/b/a/a/a;
.super Ljava/io/InputStream;


# instance fields
.field private final a:Ljava/io/InputStream;

.field private final b:Lcom/jscape/inet/b/a/b/h;

.field private final c:Lcom/jscape/util/h/e;

.field private d:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/jscape/inet/b/a/b/h;)V
    .locals 0

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/b/a/a/a;->a:Ljava/io/InputStream;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/b/a/a/a;->b:Lcom/jscape/inet/b/a/b/h;

    invoke-static {}, Lcom/jscape/util/h/e;->a()Lcom/jscape/util/h/e;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/b/a/a/a;->c:Lcom/jscape/util/h/e;

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/b/a/a/t;->b()[Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/b/a/a/a;->c:Lcom/jscape/util/h/e;

    invoke-virtual {v1}, Lcom/jscape/util/h/e;->available()I

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_1

    if-gtz v1, :cond_2

    if-eqz v0, :cond_0

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/inet/b/a/a/a;->d:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_0

    :cond_0
    move-object v1, p0

    goto :goto_1

    :cond_1
    :goto_0
    if-eqz v1, :cond_3

    :cond_2
    return-void

    :cond_3
    iget-object v1, p0, Lcom/jscape/inet/b/a/a/a;->a:Ljava/io/InputStream;

    :goto_1
    invoke-static {v1}, Lcom/jscape/inet/b/a/a/j;->a(Ljava/io/InputStream;)Lcom/jscape/inet/b/a/a/i;

    move-result-object v1

    if-eqz v0, :cond_4

    if-eqz v1, :cond_5

    :try_start_2
    iget-object v2, p0, Lcom/jscape/inet/b/a/a/a;->c:Lcom/jscape/util/h/e;

    iget-object v3, v1, Lcom/jscape/inet/b/a/a/i;->a:[B

    iget v4, v1, Lcom/jscape/inet/b/a/a/i;->b:I

    iget v1, v1, Lcom/jscape/inet/b/a/a/i;->c:I

    invoke-virtual {v2, v3, v4, v1}, Lcom/jscape/util/h/e;->a([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/b/a/a/a;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_4
    :goto_2
    if-nez v0, :cond_8

    :cond_5
    iget-object v1, p0, Lcom/jscape/inet/b/a/a/a;->a:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/jscape/inet/b/a/a/o;->a(Ljava/io/InputStream;)[Lcom/jscape/inet/b/a/b/g;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :cond_6
    if-ge v3, v2, :cond_7

    aget-object v4, v1, v3

    :try_start_3
    iget-object v5, p0, Lcom/jscape/inet/b/a/a/a;->b:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {v5, v4}, Lcom/jscape/inet/b/a/b/h;->a(Lcom/jscape/inet/b/a/b/g;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    add-int/lit8 v3, v3, 0x1

    if-eqz v0, :cond_8

    if-nez v0, :cond_6

    goto :goto_3

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/b/a/a/a;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_7
    :goto_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/b/a/a/a;->d:Z

    :cond_8
    return-void

    :catch_2
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/b/a/a/a;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/b/a/a/a;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/b/a/a/a;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/b/a/a/a;->a()V

    iget-object v0, p0, Lcom/jscape/inet/b/a/a/a;->c:Lcom/jscape/util/h/e;

    invoke-virtual {v0}, Lcom/jscape/util/h/e;->read()I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/b/a/a/a;->a()V

    iget-object v0, p0, Lcom/jscape/inet/b/a/a/a;->c:Lcom/jscape/util/h/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/util/h/e;->read([BII)I

    move-result p1

    return p1
.end method
