.class public Lcom/jscape/inet/b/a/a/s;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/b/a/b/i;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:I = 0x10000

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/jscape/inet/b/a/a/t;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class;",
            "Lcom/jscape/inet/b/a/a/t;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x8

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x30

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "L\u0016WAl!_\u001a\u001e<\u000b\u0004\u0011oq\u0015\u001b\u0011\u0012\u0000?l\u001f\u001a\u0016\u0016\u0003z!\u0019\u0005\u0004\u0004\u0017%!_\u001aK"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x27

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/b/a/a/s;->d:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x4a

    goto :goto_2

    :cond_2
    const/16 v12, 0x31

    goto :goto_2

    :cond_3
    const/16 v12, 0x2f

    goto :goto_2

    :cond_4
    const/16 v12, 0x54

    goto :goto_2

    :cond_5
    const/16 v12, 0x47

    goto :goto_2

    :cond_6
    const/16 v12, 0x55

    goto :goto_2

    :cond_7
    const/16 v12, 0x59

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public varargs constructor <init>([Lcom/jscape/inet/b/a/a/t;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/b/a/a/s;->b:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/inet/b/a/a/s;->c:Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/a/s;->a([Lcom/jscape/inet/b/a/a/t;)Lcom/jscape/inet/b/a/a/s;

    return-void
.end method

.method private a(Lcom/jscape/inet/b/a/b/j;)Lcom/jscape/inet/b/a/a/t;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/b/a/a/t;->b()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/b/a/a/s;->c:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/inet/b/a/a/t;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lcom/jscape/inet/b/a/a/s;->d:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/b/a/a/s;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method

.method private a(Ljava/lang/String;)Lcom/jscape/inet/b/a/a/t;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/b/a/a/s;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/b/a/a/t;

    return-object p1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public varargs a([Lcom/jscape/inet/b/a/a/t;)Lcom/jscape/inet/b/a/a/s;
    .locals 5

    iget-object v0, p0, Lcom/jscape/inet/b/a/a/s;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/jscape/inet/b/a/a/s;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    iget-object v3, p0, Lcom/jscape/inet/b/a/a/s;->b:Ljava/util/Map;

    iget-object v4, v2, Lcom/jscape/inet/b/a/a/t;->a:Ljava/lang/String;

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/jscape/inet/b/a/a/s;->c:Ljava/util/Map;

    iget-object v4, v2, Lcom/jscape/inet/b/a/a/t;->b:Ljava/lang/Class;

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/b/a/b/i;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-static {v0, p1}, Lcom/jscape/inet/b/a/a/r;->b(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/b/a/a/t;->b()[Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/jscape/inet/b/a/a/s;->a(Ljava/lang/String;)Lcom/jscape/inet/b/a/a/t;

    move-result-object v0

    sget-object v2, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-static {v2, p1}, Lcom/jscape/inet/b/a/a/r;->b(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-static {v3, p1}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/jscape/inet/b/a/a/o;->a(Ljava/io/InputStream;)[Lcom/jscape/inet/b/a/b/g;

    move-result-object v4

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    const-class v0, Lcom/jscape/inet/b/a/b/t;

    goto :goto_1

    :cond_1
    :goto_0
    iget-object v0, v0, Lcom/jscape/inet/b/a/a/t;->b:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    const/4 v5, 0x4

    :try_start_1
    new-array v5, v5, [Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v6, 0x0

    if-eqz v1, :cond_3

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    :try_start_2
    const-string v2, ""

    :cond_3
    :goto_2
    aput-object v2, v5, v6

    const/4 v2, 0x1

    aput-object v3, v5, v2

    const/4 v2, 0x2

    aput-object v4, v5, v2

    const/4 v2, 0x3

    aput-object p1, v5, v2

    invoke-static {v0, v5}, Lcom/jscape/util/l/c;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/b/a/b/i;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    if-nez v1, :cond_4

    const/4 v0, 0x5

    new-array v0, v0, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_4
    return-object p1

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/b/a/a/s;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p1

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public a(Lcom/jscape/inet/b/a/b/i;Ljava/io/OutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/b/a/b/j;

    invoke-static {}, Lcom/jscape/inet/b/a/a/t;->b()[Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/jscape/inet/b/a/a/s;->a(Lcom/jscape/inet/b/a/b/j;)Lcom/jscape/inet/b/a/a/t;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/b/a/a/s;->d:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/jscape/inet/b/a/a/t;->a:Ljava/lang/String;

    aput-object v0, v3, v2

    iget-object v0, p1, Lcom/jscape/inet/b/a/b/j;->c:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v0, v3, v2

    iget-object v0, p1, Lcom/jscape/inet/b/a/b/j;->a:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v0, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    sget-object v1, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-static {v0, v1, p2}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    iget-object v0, p1, Lcom/jscape/inet/b/a/b/j;->d:[Lcom/jscape/inet/b/a/b/g;

    invoke-static {v0, p2}, Lcom/jscape/inet/b/a/a/o;->a([Lcom/jscape/inet/b/a/b/g;Ljava/io/OutputStream;)V

    iget-object p1, p1, Lcom/jscape/inet/b/a/b/j;->e:Ljava/io/InputStream;

    const/high16 v0, 0x10000

    invoke-static {p1, p2, v0}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;Ljava/io/OutputStream;I)J

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x4

    new-array p1, p1, [Ljava/lang/String;

    invoke-static {p1}, Lcom/jscape/inet/b/a/a/t;->b([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/b/a/a/s;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/a/s;->a(Ljava/io/InputStream;)Lcom/jscape/inet/b/a/b/i;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/b/a/b/i;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/b/a/a/s;->a(Lcom/jscape/inet/b/a/b/i;Ljava/io/OutputStream;)V

    return-void
.end method
