.class final enum Lcom/jscape/inet/b/a/a/f;
.super Lcom/jscape/inet/b/a/a/c;


# static fields
.field private static final g:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x7

    const-string v5, "\u0001`\u001c\u0000N$q\u0007\u0001`\u001c\u0000N$q"

    const/16 v6, 0xf

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x1b

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v2

    invoke-virtual {v5, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v2

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v6, 0x23

    const/16 v2, 0x11

    const-string v5, "e)[S\u0005t#Cv\u007fS\u0015}\"X5]\u0011e)[S\u0005t#Cv\u007fS\u0015}\"X5]"

    move v8, v11

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v2

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v11

    :goto_3
    const/16 v9, 0x48

    add-int/2addr v7, v10

    add-int v11, v7, v2

    invoke-virtual {v5, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/b/a/a/f;->g:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v3, v14, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v10, :cond_8

    const/4 v4, 0x2

    if-eq v3, v4, :cond_7

    const/4 v4, 0x3

    if-eq v3, v4, :cond_6

    if-eq v3, v0, :cond_5

    const/4 v4, 0x5

    if-eq v3, v4, :cond_4

    const/16 v3, 0xe

    goto :goto_4

    :cond_4
    const/16 v3, 0x5a

    goto :goto_4

    :cond_5
    const/16 v3, 0x3e

    goto :goto_4

    :cond_6
    const/16 v3, 0x75

    goto :goto_4

    :cond_7
    const/16 v3, 0x72

    goto :goto_4

    :cond_8
    const/16 v3, 0x13

    goto :goto_4

    :cond_9
    const/16 v3, 0x79

    :goto_4
    xor-int/2addr v3, v9

    xor-int/2addr v3, v15

    int-to-char v3, v3

    aput-char v3, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/jscape/inet/b/a/a/c;-><init>(Ljava/lang/String;ILcom/jscape/inet/b/a/a/d;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/inet/b/a/b/h;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/jscape/inet/b/a/b/h;",
            ")TT;"
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/b/a/a/f;->g:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v1, v0, v1

    invoke-virtual {p1, v1}, Lcom/jscape/inet/b/a/b/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/jscape/inet/b/a/b/h;Ljava/lang/Object;)V
    .locals 2

    sget-object p2, Lcom/jscape/inet/b/a/a/f;->g:[Ljava/lang/String;

    const/4 v0, 0x3

    aget-object v0, p2, v0

    const/4 v1, 0x1

    aget-object p2, p2, v1

    invoke-virtual {p1, v0, p2}, Lcom/jscape/inet/b/a/b/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
