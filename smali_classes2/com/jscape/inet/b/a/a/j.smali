.class public Lcom/jscape/inet/b/a/a/j;
.super Ljava/lang/Object;


# static fields
.field private static final a:[B

.field private static final b:I = 0x10


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jscape/inet/b/a/a/j;->a:[B

    return-void

    nop

    :array_0
    .array-data 1
        0xdt
        0xat
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/io/InputStream;)Lcom/jscape/inet/b/a/a/i;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/b/a/a/t;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/j;->b(Ljava/io/InputStream;)Lcom/jscape/inet/b/a/a/l;

    move-result-object v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    iget v0, v1, Lcom/jscape/inet/b/a/a/l;->a:I

    new-array v0, v0, [B

    invoke-static {v0, p0}, Lcom/jscape/util/X;->a([BLjava/io/InputStream;)[B

    move-result-object v0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/j;->c(Ljava/io/InputStream;)V

    new-instance p0, Lcom/jscape/inet/b/a/a/i;

    const/4 v2, 0x0

    array-length v3, v0

    iget-object v1, v1, Lcom/jscape/inet/b/a/a/l;->b:[Ljava/lang/String;

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/jscape/inet/b/a/a/i;-><init>([BII[Ljava/lang/String;)V

    return-object p0
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method public static a(Lcom/jscape/inet/b/a/a/i;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/b/a/a/t;->b()[Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {p0, p1}, Lcom/jscape/inet/b/a/a/j;->b(Lcom/jscape/inet/b/a/a/i;Ljava/io/OutputStream;)V

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/jscape/inet/b/a/a/i;->c:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-lez v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/b/a/a/i;->a:[B

    iget v1, p0, Lcom/jscape/inet/b/a/a/i;->b:I

    iget p0, p0, Lcom/jscape/inet/b/a/a/i;->c:I

    invoke-virtual {p1, v0, v1, p0}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    sget-object p0, Lcom/jscape/inet/b/a/a/j;->a:[B

    invoke-virtual {p1, p0}, Ljava/io/OutputStream;->write([B)V

    :cond_1
    return-void

    :catch_0
    move-exception p0

    :try_start_2
    invoke-static {p0}, Lcom/jscape/inet/b/a/a/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method

.method private static b(Ljava/io/InputStream;)Lcom/jscape/inet/b/a/a/l;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/b/a/a/t;->b()[Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/b/a/a/j;->a:[B

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;[BZ)[B

    move-result-object p0

    const/4 v1, 0x0

    if-nez p0, :cond_0

    return-object v1

    :cond_0
    new-instance v3, Ljava/lang/String;

    sget-object v4, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-direct {v3, p0, v4}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    const-string p0, ";"

    invoke-static {v3, p0, v2}, Lcom/jscape/util/at;->a(Ljava/lang/String;Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/util/at;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    aget-object v3, p0, v2

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    :try_start_0
    array-length v4, p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    if-le v4, v0, :cond_2

    :try_start_1
    array-length v2, p0

    sub-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    invoke-static {p0, v0, v2}, Lcom/jscape/util/v;->a([Ljava/lang/Object;I[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :cond_1
    move v2, v4

    :cond_2
    new-array p0, v2, [Ljava/lang/String;

    :goto_0
    new-instance v0, Lcom/jscape/inet/b/a/a/l;

    invoke-direct {v0, v3, p0, v1}, Lcom/jscape/inet/b/a/a/l;-><init>(I[Ljava/lang/String;Lcom/jscape/inet/b/a/a/k;)V

    return-object v0

    :catch_0
    move-exception p0

    :try_start_2
    invoke-static {p0}, Lcom/jscape/inet/b/a/a/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method

.method private static b(Lcom/jscape/inet/b/a/a/i;Ljava/io/OutputStream;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/b/a/a/i;->c:I

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/b/a/a/t;->b()[Ljava/lang/String;

    move-result-object v1

    iget-object p0, p0, Lcom/jscape/inet/b/a/a/i;->d:[Ljava/lang/String;

    array-length v2, p0

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v2, :cond_1

    aget-object v4, p0, v3

    const/16 v5, 0x3b

    :try_start_0
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_1

    add-int/lit8 v3, v3, 0x1

    if-nez v1, :cond_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    sget-object v0, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/io/OutputStream;->write([B)V

    sget-object p0, Lcom/jscape/inet/b/a/a/j;->a:[B

    invoke-virtual {p1, p0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method private static c(Ljava/io/InputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/b/a/a/t;->b()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    sget-object v2, Lcom/jscape/inet/b/a/a/j;->a:[B

    array-length v2, v2

    if-ge v1, v2, :cond_1

    invoke-static {p0}, Lcom/jscape/util/X;->b(Ljava/io/InputStream;)I

    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method
