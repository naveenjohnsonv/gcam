.class public Lcom/jscape/inet/b/a/a/v;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/b/a/b/i;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:I = 0x10000

.field private static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "\u0004+Z\u00017+ZR"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/b/a/a/v;->b:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x69

    goto :goto_1

    :cond_1
    const/16 v4, 0x1d

    goto :goto_1

    :cond_2
    const/16 v4, 0x52

    goto :goto_1

    :cond_3
    const/16 v4, 0x32

    goto :goto_1

    :cond_4
    const/16 v4, 0x6c

    goto :goto_1

    :cond_5
    const/16 v4, 0x4e

    goto :goto_1

    :cond_6
    const/16 v4, 0x37

    :goto_1
    const/16 v5, 0x16

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/io/InputStream;)Lcom/jscape/inet/b/a/b/u;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-static {v0, p0}, Lcom/jscape/inet/b/a/a/r;->b(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-static {v0, p0}, Lcom/jscape/inet/b/a/a/r;->b(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    sget-object v0, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-static {v0, p0}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/o;->a(Ljava/io/InputStream;)[Lcom/jscape/inet/b/a/b/g;

    move-result-object v5

    new-instance v0, Lcom/jscape/inet/b/a/b/u;

    move-object v1, v0

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Lcom/jscape/inet/b/a/b/u;-><init>(Ljava/lang/String;ILjava/lang/String;[Lcom/jscape/inet/b/a/b/g;Ljava/io/InputStream;)V

    return-object v0
.end method

.method public static a(Lcom/jscape/inet/b/a/b/u;Ljava/io/OutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/b/a/a/v;->b:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/jscape/inet/b/a/b/u;->a:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget v2, p0, Lcom/jscape/inet/b/a/b/u;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/jscape/inet/b/a/b/u;->d:Ljava/lang/String;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-static {v0, v1, p1}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    iget-object v0, p0, Lcom/jscape/inet/b/a/b/u;->e:[Lcom/jscape/inet/b/a/b/g;

    invoke-static {v0, p1}, Lcom/jscape/inet/b/a/a/o;->a([Lcom/jscape/inet/b/a/b/g;Ljava/io/OutputStream;)V

    iget-object p0, p0, Lcom/jscape/inet/b/a/b/u;->f:Ljava/io/InputStream;

    const/high16 v0, 0x10000

    invoke-static {p0, p1, v0}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;Ljava/io/OutputStream;I)J

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/inet/b/a/b/i;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/b/a/b/u;

    invoke-static {p1, p2}, Lcom/jscape/inet/b/a/a/v;->a(Lcom/jscape/inet/b/a/b/u;Ljava/io/OutputStream;)V

    return-void
.end method

.method public b(Ljava/io/InputStream;)Lcom/jscape/inet/b/a/b/i;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/b/a/a/v;->a(Ljava/io/InputStream;)Lcom/jscape/inet/b/a/b/u;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/a/v;->b(Ljava/io/InputStream;)Lcom/jscape/inet/b/a/b/i;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/b/a/b/i;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/b/a/a/v;->a(Lcom/jscape/inet/b/a/b/i;Ljava/io/OutputStream;)V

    return-void
.end method
