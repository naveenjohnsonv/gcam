.class final enum Lcom/jscape/inet/b/a/a/e;
.super Lcom/jscape/inet/b/a/a/c;


# static fields
.field private static final g:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "V0\u000be\u0008 @8\r\u0004\u007f\n+\u00070,E;BkG\rV0\u000be\u0008 @8\r\u0004\u007f\n+"

    const/16 v5, 0x23

    const/16 v6, 0xd

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x7e

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x16

    const/16 v4, 0xb

    const-string v6, "?#J;\u0011l\u001ei\u007fOm\n?#J;\u0011l\u001ei\u007f@"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v16, v6

    move v6, v4

    move-object/from16 v4, v16

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x71

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/b/a/a/e;->g:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    const/4 v3, 0x4

    if-eq v2, v3, :cond_5

    if-eq v2, v0, :cond_4

    const/16 v2, 0x4a

    goto :goto_4

    :cond_4
    const/16 v2, 0x30

    goto :goto_4

    :cond_5
    const/16 v2, 0x13

    goto :goto_4

    :cond_6
    const/16 v2, 0x6f

    goto :goto_4

    :cond_7
    const/16 v2, 0x1b

    goto :goto_4

    :cond_8
    const/16 v2, 0x21

    goto :goto_4

    :cond_9
    const/16 v2, 0x6b

    :goto_4
    xor-int/2addr v2, v9

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/jscape/inet/b/a/a/c;-><init>(Ljava/lang/String;ILcom/jscape/inet/b/a/a/d;)V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public a(Lcom/jscape/inet/b/a/b/h;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/jscape/inet/b/a/b/h;",
            ")TT;"
        }
    .end annotation

    const-string v0, "*"

    invoke-static {}, Lcom/jscape/inet/b/a/a/t;->b()[Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/jscape/inet/b/a/a/e;->g:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {p1, v2}, Lcom/jscape/inet/b/a/b/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    if-nez p1, :cond_0

    return-object v2

    :cond_0
    :try_start_0
    new-instance v3, Ljava/util/Scanner;

    invoke-direct {v3, p1}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    const-string p1, " "

    invoke-virtual {v3, p1}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    invoke-virtual {v3}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object p1

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    invoke-virtual {v3}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    const/16 v6, 0xa

    if-nez v5, :cond_1

    new-instance v5, Ljava/util/Scanner;

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    const-string v4, "-"

    invoke-virtual {v5, v4}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    invoke-virtual {v5}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v5}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v5

    goto :goto_0

    :cond_1
    move-object v4, v2

    move-object v5, v4

    :goto_0
    invoke-virtual {v3}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v1, :cond_3

    :try_start_1
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_2
    move-object v0, v2

    goto :goto_2

    :catch_0
    move-exception p1

    goto :goto_3

    :cond_3
    :goto_1
    :try_start_2
    invoke-static {v0, v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v0

    :goto_2
    new-instance v1, Lcom/jscape/inet/b/a/b/c;

    invoke-direct {v1, p1, v4, v5, v0}, Lcom/jscape/inet/b/a/b/c;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;)V

    return-object v1

    :goto_3
    invoke-static {p1}, Lcom/jscape/inet/b/a/a/e;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    return-object v2
.end method

.method public a(Lcom/jscape/inet/b/a/b/h;Ljava/lang/Object;)V
    .locals 10

    invoke-static {}, Lcom/jscape/inet/b/a/a/t;->b()[Ljava/lang/String;

    move-result-object v0

    check-cast p2, Lcom/jscape/inet/b/a/b/c;

    iget-object v1, p2, Lcom/jscape/inet/b/a/b/c;->b:Ljava/lang/Long;

    const/4 v2, 0x4

    const/4 v3, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x2

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    iget-object v1, p2, Lcom/jscape/inet/b/a/b/c;->d:Ljava/lang/Long;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/jscape/inet/b/a/a/e;->g:[Ljava/lang/String;

    aget-object v7, v1, v3

    new-array v8, v2, [Ljava/lang/Object;

    iget-object v9, p2, Lcom/jscape/inet/b/a/b/c;->a:Ljava/lang/String;

    aput-object v9, v8, v4

    iget-object v9, p2, Lcom/jscape/inet/b/a/b/c;->b:Ljava/lang/Long;

    aput-object v9, v8, v5

    iget-object v9, p2, Lcom/jscape/inet/b/a/b/c;->c:Ljava/lang/Long;

    aput-object v9, v8, v6

    iget-object v9, p2, Lcom/jscape/inet/b/a/b/c;->d:Ljava/lang/Long;

    aput-object v9, v8, v3

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aget-object v1, v1, v6

    invoke-virtual {p1, v1, v7}, Lcom/jscape/inet/b/a/b/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_4

    :cond_0
    iget-object v1, p2, Lcom/jscape/inet/b/a/b/c;->b:Ljava/lang/Long;

    :cond_1
    if-eqz v0, :cond_3

    if-eqz v1, :cond_2

    sget-object v1, Lcom/jscape/inet/b/a/a/e;->g:[Ljava/lang/String;

    aget-object v2, v1, v2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v7, p2, Lcom/jscape/inet/b/a/b/c;->a:Ljava/lang/String;

    aput-object v7, v3, v4

    iget-object v7, p2, Lcom/jscape/inet/b/a/b/c;->b:Ljava/lang/Long;

    aput-object v7, v3, v5

    iget-object v7, p2, Lcom/jscape/inet/b/a/b/c;->c:Ljava/lang/Long;

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aget-object v1, v1, v6

    invoke-virtual {p1, v1, v2}, Lcom/jscape/inet/b/a/b/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_4

    :cond_2
    iget-object v1, p2, Lcom/jscape/inet/b/a/b/c;->d:Ljava/lang/Long;

    :cond_3
    if-eqz v1, :cond_4

    sget-object v0, Lcom/jscape/inet/b/a/a/e;->g:[Ljava/lang/String;

    aget-object v1, v0, v5

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p2, Lcom/jscape/inet/b/a/b/c;->a:Ljava/lang/String;

    aput-object v3, v2, v4

    iget-object p2, p2, Lcom/jscape/inet/b/a/b/c;->d:Ljava/lang/Long;

    aput-object p2, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    aget-object v0, v0, v6

    invoke-virtual {p1, v0, p2}, Lcom/jscape/inet/b/a/b/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return-void
.end method
