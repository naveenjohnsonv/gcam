.class public Lcom/jscape/inet/b/a/a/b;
.super Ljava/io/InputStream;


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field private final a:Ljava/io/InputStream;

.field private final b:[B

.field private final c:Lcom/jscape/inet/b/a/b/h;

.field private final d:Lcom/jscape/util/h/o;

.field private final e:Lcom/jscape/util/h/e;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "q\u000b| t~0\u0018\u0006b4v|tK\u000cp$8a5T\u0010oo"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/b/a/a/b;->g:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/4 v5, 0x3

    if-eqz v4, :cond_6

    const/4 v6, 0x1

    if-eq v4, v6, :cond_5

    const/4 v6, 0x2

    if-eq v4, v6, :cond_4

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v5, 0x6f

    goto :goto_1

    :cond_1
    const/16 v5, 0x2c

    goto :goto_1

    :cond_2
    const/16 v5, 0x23

    goto :goto_1

    :cond_3
    const/16 v5, 0x7a

    goto :goto_1

    :cond_4
    const/16 v5, 0x31

    goto :goto_1

    :cond_5
    const/16 v5, 0x5e

    :cond_6
    :goto_1
    const/16 v4, 0x3b

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/io/InputStream;ILcom/jscape/inet/b/a/b/h;)V
    .locals 4

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/b/a/a/b;->a:Ljava/io/InputStream;

    int-to-long v0, p2

    sget-object p1, Lcom/jscape/inet/b/a/a/b;->g:Ljava/lang/String;

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3, p1}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    new-array p1, p2, [B

    iput-object p1, p0, Lcom/jscape/inet/b/a/a/b;->b:[B

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/b/a/a/b;->c:Lcom/jscape/inet/b/a/b/h;

    new-instance p1, Lcom/jscape/util/h/o;

    invoke-direct {p1}, Lcom/jscape/util/h/o;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/b/a/a/b;->d:Lcom/jscape/util/h/o;

    invoke-static {}, Lcom/jscape/util/h/e;->a()Lcom/jscape/util/h/e;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/b/a/a/b;->e:Lcom/jscape/util/h/e;

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/b/a/a/t;->b()[Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/b/a/a/b;->e:Lcom/jscape/util/h/e;

    invoke-virtual {v1}, Lcom/jscape/util/h/e;->available()I

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    if-gtz v1, :cond_1

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/inet/b/a/a/b;->f:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    if-eqz v0, :cond_3

    if-eqz v1, :cond_2

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/jscape/inet/b/a/a/b;->d:Lcom/jscape/util/h/o;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->c()Lcom/jscape/util/h/o;

    iget-object v1, p0, Lcom/jscape/inet/b/a/a/b;->a:Ljava/io/InputStream;

    iget-object v2, p0, Lcom/jscape/inet/b/a/a/b;->b:[B

    invoke-virtual {v1, v2}, Ljava/io/InputStream;->read([B)I

    move-result v1

    :cond_3
    const/4 v2, 0x0

    if-eqz v0, :cond_5

    const/4 v3, -0x1

    if-eq v1, v3, :cond_4

    :try_start_2
    new-instance v3, Lcom/jscape/inet/b/a/a/i;

    iget-object v4, p0, Lcom/jscape/inet/b/a/a/b;->b:[B

    new-array v5, v2, [Ljava/lang/String;

    invoke-direct {v3, v4, v2, v1, v5}, Lcom/jscape/inet/b/a/a/i;-><init>([BII[Ljava/lang/String;)V

    iget-object v1, p0, Lcom/jscape/inet/b/a/a/b;->d:Lcom/jscape/util/h/o;

    invoke-static {v3, v1}, Lcom/jscape/inet/b/a/a/j;->a(Lcom/jscape/inet/b/a/a/i;Ljava/io/OutputStream;)V

    iget-object v1, p0, Lcom/jscape/inet/b/a/a/b;->e:Lcom/jscape/util/h/e;

    iget-object v3, p0, Lcom/jscape/inet/b/a/a/b;->d:Lcom/jscape/util/h/o;

    invoke-virtual {v3}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v3

    iget-object v4, p0, Lcom/jscape/inet/b/a/a/b;->d:Lcom/jscape/util/h/o;

    invoke-virtual {v4}, Lcom/jscape/util/h/o;->b()I

    move-result v4

    invoke-virtual {v1, v3, v2, v4}, Lcom/jscape/util/h/e;->a([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    if-nez v0, :cond_6

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/b/a/a/b;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_4
    :goto_0
    move v1, v2

    :cond_5
    new-array v0, v1, [Ljava/lang/String;

    invoke-static {v0}, Lcom/jscape/inet/b/a/a/i;->a([Ljava/lang/String;)Lcom/jscape/inet/b/a/a/i;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/b/a/a/b;->d:Lcom/jscape/util/h/o;

    invoke-static {v0, v1}, Lcom/jscape/inet/b/a/a/j;->a(Lcom/jscape/inet/b/a/a/i;Ljava/io/OutputStream;)V

    iget-object v0, p0, Lcom/jscape/inet/b/a/a/b;->e:Lcom/jscape/util/h/e;

    iget-object v1, p0, Lcom/jscape/inet/b/a/a/b;->d:Lcom/jscape/util/h/o;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v1

    iget-object v3, p0, Lcom/jscape/inet/b/a/a/b;->d:Lcom/jscape/util/h/o;

    invoke-virtual {v3}, Lcom/jscape/util/h/o;->b()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/jscape/util/h/e;->a([BII)V

    iget-object v0, p0, Lcom/jscape/inet/b/a/a/b;->c:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {v0}, Lcom/jscape/inet/b/a/b/h;->a()[Lcom/jscape/inet/b/a/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/b/a/a/b;->d:Lcom/jscape/util/h/o;

    invoke-static {v0, v1}, Lcom/jscape/inet/b/a/a/o;->a([Lcom/jscape/inet/b/a/b/g;Ljava/io/OutputStream;)V

    iget-object v0, p0, Lcom/jscape/inet/b/a/a/b;->e:Lcom/jscape/util/h/e;

    iget-object v1, p0, Lcom/jscape/inet/b/a/a/b;->d:Lcom/jscape/util/h/o;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v1

    iget-object v3, p0, Lcom/jscape/inet/b/a/a/b;->d:Lcom/jscape/util/h/o;

    invoke-virtual {v3}, Lcom/jscape/util/h/o;->b()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/jscape/util/h/e;->a([BII)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/inet/b/a/a/b;->f:Z

    :cond_6
    return-void

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/b/a/a/b;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/b/a/a/b;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/b/a/a/b;->a()V

    iget-object v0, p0, Lcom/jscape/inet/b/a/a/b;->e:Lcom/jscape/util/h/e;

    invoke-virtual {v0}, Lcom/jscape/util/h/e;->read()I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/b/a/a/b;->a()V

    iget-object v0, p0, Lcom/jscape/inet/b/a/a/b;->e:Lcom/jscape/util/h/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/util/h/e;->read([BII)I

    move-result p1

    return p1
.end method
