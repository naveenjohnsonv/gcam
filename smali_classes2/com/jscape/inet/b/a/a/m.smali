.class public Lcom/jscape/inet/b/a/a/m;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/b/a/b/i;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/inet/b/a/a/v;

.field private final b:Lcom/jscape/inet/b/a/a/s;


# direct methods
.method public constructor <init>(Lcom/jscape/inet/b/a/a/v;Lcom/jscape/inet/b/a/a/s;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/b/a/a/m;->a:Lcom/jscape/inet/b/a/a/v;

    iput-object p2, p0, Lcom/jscape/inet/b/a/a/m;->b:Lcom/jscape/inet/b/a/a/s;

    return-void
.end method

.method public static a()Lcom/jscape/inet/b/a/a/m;
    .locals 4

    new-instance v0, Lcom/jscape/inet/b/a/a/m;

    new-instance v1, Lcom/jscape/inet/b/a/a/v;

    invoke-direct {v1}, Lcom/jscape/inet/b/a/a/v;-><init>()V

    new-instance v2, Lcom/jscape/inet/b/a/a/s;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/jscape/inet/b/a/a/t;

    invoke-direct {v2, v3}, Lcom/jscape/inet/b/a/a/s;-><init>([Lcom/jscape/inet/b/a/a/t;)V

    invoke-static {v2}, Lcom/jscape/inet/b/a/a/u;->a(Lcom/jscape/inet/b/a/a/s;)Lcom/jscape/inet/b/a/a/s;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/jscape/inet/b/a/a/m;-><init>(Lcom/jscape/inet/b/a/a/v;Lcom/jscape/inet/b/a/a/s;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/b/a/b/i;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/b/a/a/m;->a:Lcom/jscape/inet/b/a/a/v;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/b/a/a/v;->b(Ljava/io/InputStream;)Lcom/jscape/inet/b/a/b/i;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/jscape/inet/b/a/b/i;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/b/a/a/m;->b:Lcom/jscape/inet/b/a/a/s;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/b/a/a/s;->a(Lcom/jscape/inet/b/a/b/i;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/a/m;->a(Ljava/io/InputStream;)Lcom/jscape/inet/b/a/b/i;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/b/a/b/i;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/b/a/a/m;->a(Lcom/jscape/inet/b/a/b/i;Ljava/io/OutputStream;)V

    return-void
.end method
