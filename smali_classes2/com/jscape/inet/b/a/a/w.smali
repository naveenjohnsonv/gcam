.class public Lcom/jscape/inet/b/a/a/w;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Lcom/jscape/inet/b/a/b/i;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/inet/b/a/a/s;

.field private final b:Lcom/jscape/inet/b/a/a/v;


# direct methods
.method public constructor <init>(Lcom/jscape/inet/b/a/a/s;Lcom/jscape/inet/b/a/a/v;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/b/a/a/w;->a:Lcom/jscape/inet/b/a/a/s;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/b/a/a/w;->b:Lcom/jscape/inet/b/a/a/v;

    return-void
.end method

.method public static a()Lcom/jscape/inet/b/a/a/w;
    .locals 3

    new-instance v0, Lcom/jscape/inet/b/a/a/w;

    new-instance v1, Lcom/jscape/inet/b/a/a/s;

    const/4 v2, 0x0

    new-array v2, v2, [Lcom/jscape/inet/b/a/a/t;

    invoke-direct {v1, v2}, Lcom/jscape/inet/b/a/a/s;-><init>([Lcom/jscape/inet/b/a/a/t;)V

    invoke-static {v1}, Lcom/jscape/inet/b/a/a/u;->a(Lcom/jscape/inet/b/a/a/s;)Lcom/jscape/inet/b/a/a/s;

    move-result-object v1

    new-instance v2, Lcom/jscape/inet/b/a/a/v;

    invoke-direct {v2}, Lcom/jscape/inet/b/a/a/v;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/jscape/inet/b/a/a/w;-><init>(Lcom/jscape/inet/b/a/a/s;Lcom/jscape/inet/b/a/a/v;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Lcom/jscape/inet/b/a/b/i;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/b/a/a/w;->a:Lcom/jscape/inet/b/a/a/s;

    invoke-virtual {v0, p1}, Lcom/jscape/inet/b/a/a/s;->a(Ljava/io/InputStream;)Lcom/jscape/inet/b/a/b/i;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/jscape/inet/b/a/b/i;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/b/a/a/w;->b:Lcom/jscape/inet/b/a/a/v;

    invoke-virtual {v0, p1, p2}, Lcom/jscape/inet/b/a/a/v;->a(Lcom/jscape/inet/b/a/b/i;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/a/w;->a(Ljava/io/InputStream;)Lcom/jscape/inet/b/a/b/i;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/b/a/b/i;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/b/a/a/w;->a(Lcom/jscape/inet/b/a/b/i;Ljava/io/OutputStream;)V

    return-void
.end method
