.class public Lcom/jscape/inet/b/a/a/r;
.super Ljava/lang/Object;


# static fields
.field private static final a:[B

.field private static final b:[B

.field private static final c:I = 0x10000


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jscape/inet/b/a/a/r;->a:[B

    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x20

    aput-byte v2, v0, v1

    sput-object v0, Lcom/jscape/inet/b/a/a/r;->b:[B

    return-void

    nop

    :array_0
    .array-data 1
        0xdt
        0xat
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method public static a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/io/InputStream;)[B

    move-result-object p1

    if-eqz p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p0}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public static a(Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/b/a/a/r;->a:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/b/a/a/r;->a:[B

    invoke-static {p0, p1, v0, p2}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/lang/String;Ljava/nio/charset/Charset;[BLjava/io/OutputStream;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/nio/charset/Charset;[BLjava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p0

    invoke-static {p0, p2, p3}, Lcom/jscape/inet/b/a/a/r;->a([B[BLjava/io/OutputStream;)V

    return-void
.end method

.method public static a([BLjava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/b/a/a/r;->a:[B

    invoke-static {p0, v0, p1}, Lcom/jscape/inet/b/a/a/r;->a([B[BLjava/io/OutputStream;)V

    return-void
.end method

.method public static a([B[BLjava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p2, p0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public static a(Ljava/io/InputStream;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/b/a/a/r;->a:[B

    invoke-static {v0, p0}, Lcom/jscape/inet/b/a/a/r;->a([BLjava/io/InputStream;)[B

    move-result-object p0

    return-object p0
.end method

.method public static a([BLjava/io/InputStream;)[B
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/o;

    invoke-direct {v0}, Lcom/jscape/util/h/o;-><init>()V

    invoke-static {}, Lcom/jscape/inet/b/a/a/t;->b()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move v3, v2

    :cond_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    :try_start_0
    invoke-virtual {v0, v4}, Lcom/jscape/util/h/o;->write(I)V

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v6

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result v7

    invoke-static {v6, p0, v7}, Lcom/jscape/util/v;->b([B[BI)Z

    move-result v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v1, :cond_2

    if-eqz v6, :cond_1

    const/4 v3, 0x1

    if-nez v1, :cond_3

    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result v6
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_2
    :goto_0
    const/high16 v7, 0x10000

    if-lt v6, v7, :cond_0

    goto :goto_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_3
    :goto_1
    if-eqz v1, :cond_5

    if-ne v4, v5, :cond_4

    :try_start_2
    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result v4
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v1, :cond_5

    if-nez v4, :cond_4

    const/4 p0, 0x0

    return-object p0

    :catch_2
    move-exception p0

    :try_start_3
    invoke-static {p0}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception p0

    :try_start_4
    invoke-static {p0}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_4
    move v4, v3

    :cond_5
    if-eqz v1, :cond_7

    if-eqz v4, :cond_6

    :try_start_5
    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result p1

    array-length p0, p0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    sub-int v4, p1, p0

    goto :goto_2

    :catch_5
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_6
    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result p0

    goto :goto_3

    :cond_7
    :goto_2
    move p0, v4

    :goto_3
    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object p1

    new-array p0, p0, [B

    invoke-static {p1, v2, p0}, Lcom/jscape/util/v;->a([BI[B)[B

    move-result-object p0

    return-object p0
.end method

.method public static a([[BLjava/io/InputStream;)[B
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/o;

    invoke-direct {v0}, Lcom/jscape/util/h/o;-><init>()V

    invoke-static {}, Lcom/jscape/inet/b/a/a/t;->b()[Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v5, 0x0

    if-eq v2, v4, :cond_6

    invoke-virtual {v0, v2}, Lcom/jscape/util/h/o;->write(I)V

    array-length v6, p0

    move v7, v3

    :cond_1
    if-ge v7, v6, :cond_4

    aget-object v8, p0, v7

    if-eqz v1, :cond_3

    :try_start_0
    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v9

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result v10

    invoke-static {v9, v8, v10}, Lcom/jscape/util/v;->b([B[BI)Z

    move-result v9
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_5

    if-eqz v9, :cond_2

    goto :goto_1

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_3
    :goto_0
    if-nez v1, :cond_1

    :cond_4
    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result v9

    :cond_5
    const/high16 v6, 0x10000

    if-lt v9, v6, :cond_0

    :cond_6
    move-object v8, v5

    :goto_1
    if-eqz v1, :cond_7

    if-ne v2, v4, :cond_8

    :try_start_2
    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_7
    :goto_2
    if-nez v2, :cond_8

    return-object v5

    :cond_8
    if-eqz v8, :cond_9

    :try_start_3
    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result p0

    array-length p1, v8
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    sub-int/2addr p0, p1

    goto :goto_3

    :catch_3
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_9
    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result p0

    :goto_3
    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object p1

    new-array p0, p0, [B

    invoke-static {p1, v3, p0}, Lcom/jscape/util/v;->a([BI[B)[B

    move-result-object p0

    return-object p0
.end method

.method public static b(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/inet/b/a/a/r;->b(Ljava/io/InputStream;)[B

    move-result-object p1

    if-eqz p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p0}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public static b(Ljava/io/InputStream;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/b/a/a/r;->b:[B

    invoke-static {v0, p0}, Lcom/jscape/inet/b/a/a/r;->a([BLjava/io/InputStream;)[B

    move-result-object p0

    return-object p0
.end method
