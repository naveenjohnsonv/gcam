.class public abstract enum Lcom/jscape/inet/b/a/a/c;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/b/a/a/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/jscape/inet/b/a/a/c;

.field public static final enum b:Lcom/jscape/inet/b/a/a/c;

.field public static final enum c:Lcom/jscape/inet/b/a/a/c;

.field public static final enum d:Lcom/jscape/inet/b/a/a/c;

.field public static final enum e:Lcom/jscape/inet/b/a/a/c;

.field private static final f:[Lcom/jscape/inet/b/a/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "2UT\u0012:\u000eK.H[\u00088\u0005\u000c2UT\u0012:\u000eK.NC\u0016:\u000e2UT\u0012:\u000eK.V_\u00088\u0014W"

    const/16 v5, 0x29

    const/16 v6, 0xd

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x2e

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x4

    const/4 v2, 0x3

    const/4 v0, 0x2

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v0, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v0

    const/4 v0, 0x5

    goto :goto_0

    :cond_0
    const/16 v5, 0x2a

    const/16 v2, 0x10

    const-string v4, "!FG\u0001)\u001dX=LG\u0016#\u0017E,N\u00196[H\u001b?\u0015I0VL\u001b/\u001cH+GN\n/\u001bY,BL\u0011"

    move v8, v0

    move v6, v2

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v6, v0

    move v8, v11

    :goto_3
    const/16 v9, 0x3d

    add-int/2addr v7, v10

    add-int v0, v7, v6

    invoke-virtual {v4, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    const/4 v0, 0x5

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/jscape/inet/b/a/a/d;

    aget-object v5, v1, v0

    invoke-direct {v4, v5, v3}, Lcom/jscape/inet/b/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/jscape/inet/b/a/a/c;->a:Lcom/jscape/inet/b/a/a/c;

    new-instance v4, Lcom/jscape/inet/b/a/a/e;

    aget-object v5, v1, v3

    invoke-direct {v4, v5, v10}, Lcom/jscape/inet/b/a/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/jscape/inet/b/a/a/c;->b:Lcom/jscape/inet/b/a/a/c;

    new-instance v4, Lcom/jscape/inet/b/a/a/f;

    aget-object v5, v1, v15

    invoke-direct {v4, v5, v0}, Lcom/jscape/inet/b/a/a/f;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/jscape/inet/b/a/a/c;->c:Lcom/jscape/inet/b/a/a/c;

    new-instance v4, Lcom/jscape/inet/b/a/a/g;

    aget-object v5, v1, v2

    invoke-direct {v4, v5, v2}, Lcom/jscape/inet/b/a/a/g;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/jscape/inet/b/a/a/c;->d:Lcom/jscape/inet/b/a/a/c;

    new-instance v4, Lcom/jscape/inet/b/a/a/h;

    aget-object v1, v1, v10

    invoke-direct {v4, v1, v15}, Lcom/jscape/inet/b/a/a/h;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/jscape/inet/b/a/a/c;->e:Lcom/jscape/inet/b/a/a/c;

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/jscape/inet/b/a/a/c;

    sget-object v5, Lcom/jscape/inet/b/a/a/c;->a:Lcom/jscape/inet/b/a/a/c;

    aput-object v5, v1, v3

    sget-object v3, Lcom/jscape/inet/b/a/a/c;->b:Lcom/jscape/inet/b/a/a/c;

    aput-object v3, v1, v10

    sget-object v3, Lcom/jscape/inet/b/a/a/c;->c:Lcom/jscape/inet/b/a/a/c;

    aput-object v3, v1, v0

    sget-object v0, Lcom/jscape/inet/b/a/a/c;->d:Lcom/jscape/inet/b/a/a/c;

    aput-object v0, v1, v2

    aput-object v4, v1, v15

    sput-object v1, Lcom/jscape/inet/b/a/a/c;->f:[Lcom/jscape/inet/b/a/a/c;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v3, v14, 0x7

    const/16 v17, 0x34

    if-eqz v3, :cond_8

    if-eq v3, v10, :cond_7

    if-eq v3, v0, :cond_7

    const/4 v0, 0x5

    if-eq v3, v2, :cond_6

    if-eq v3, v15, :cond_5

    if-eq v3, v0, :cond_4

    const/16 v17, 0x31

    goto :goto_4

    :cond_4
    const/16 v17, 0x6e

    goto :goto_4

    :cond_5
    const/16 v17, 0x51

    goto :goto_4

    :cond_6
    const/16 v17, 0x68

    goto :goto_4

    :cond_7
    const/4 v0, 0x5

    goto :goto_4

    :cond_8
    const/4 v0, 0x5

    const/16 v17, 0x5f

    :goto_4
    xor-int v2, v9, v17

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILcom/jscape/inet/b/a/a/d;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/b/a/a/c;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/inet/b/a/a/c;
    .locals 1

    const-class v0, Lcom/jscape/inet/b/a/a/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/b/a/a/c;

    return-object p0
.end method

.method public static a()[Lcom/jscape/inet/b/a/a/c;
    .locals 1

    sget-object v0, Lcom/jscape/inet/b/a/a/c;->f:[Lcom/jscape/inet/b/a/a/c;

    invoke-virtual {v0}, [Lcom/jscape/inet/b/a/a/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/b/a/a/c;

    return-object v0
.end method


# virtual methods
.method public abstract a(Lcom/jscape/inet/b/a/b/h;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/jscape/inet/b/a/b/h;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/jscape/inet/b/a/b/h;Ljava/lang/Object;)V
.end method
