.class public Lcom/jscape/inet/b/a/a/o;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const-string v0, "*E({Rz"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/b/a/a/o;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/4 v5, 0x5

    const/4 v6, 0x3

    if-eqz v4, :cond_5

    const/4 v7, 0x1

    if-eq v4, v7, :cond_4

    const/4 v7, 0x2

    if-eq v4, v7, :cond_3

    if-eq v4, v6, :cond_2

    const/4 v6, 0x4

    if-eq v4, v6, :cond_1

    if-eq v4, v5, :cond_6

    const/16 v5, 0x2d

    goto :goto_1

    :cond_1
    const/16 v5, 0x7b

    goto :goto_1

    :cond_2
    const/16 v5, 0x57

    goto :goto_1

    :cond_3
    const/16 v5, 0x1e

    goto :goto_1

    :cond_4
    const/16 v5, 0x3a

    goto :goto_1

    :cond_5
    move v5, v6

    :cond_6
    :goto_1
    const/16 v4, 0xc

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method public static a([Lcom/jscape/inet/b/a/b/g;Ljava/io/OutputStream;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/b/a/a/t;->b()[Ljava/lang/String;

    move-result-object v0

    array-length v1, p0

    const/4 v2, 0x0

    move v3, v2

    :cond_0
    if-ge v3, v1, :cond_1

    aget-object v4, p0, v3

    sget-object v5, Lcom/jscape/inet/b/a/a/o;->a:Ljava/lang/String;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, v4, Lcom/jscape/inet/b/a/b/g;->a:Ljava/lang/String;

    aput-object v7, v6, v2

    iget-object v4, v4, Lcom/jscape/inet/b/a/b/g;->b:Ljava/lang/String;

    const/4 v7, 0x1

    aput-object v4, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :try_start_0
    sget-object v5, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-static {v4, v5, p1}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v3, v3, 0x1

    if-eqz v0, :cond_2

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/o;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    invoke-static {p1}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/io/OutputStream;)V

    :cond_2
    return-void
.end method

.method public static a(Ljava/io/InputStream;)[Lcom/jscape/inet/b/a/b/g;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-static {}, Lcom/jscape/inet/b/a/a/t;->b()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    move-object v3, v2

    :cond_0
    :goto_0
    sget-object v4, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-static {v4, p0}, Lcom/jscape/inet/b/a/a/r;->a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    if-eqz v4, :cond_6

    :try_start_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v1, :cond_7

    if-eqz v1, :cond_7

    if-lez v6, :cond_6

    if-eqz v3, :cond_3

    :try_start_1
    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    if-eqz v1, :cond_1

    if-nez v6, :cond_2

    :try_start_2
    const-string v6, "\t"

    invoke-virtual {v4, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6

    :cond_1
    if-eqz v1, :cond_4

    if-eqz v6, :cond_3

    :cond_2
    :try_start_3
    iget-object v6, v3, Lcom/jscape/inet/b/a/a/q;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    if-nez v1, :cond_0

    goto :goto_1

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/o;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_3
    :goto_1
    const/16 v3, 0x3a

    invoke-virtual {v4, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    :cond_4
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Lcom/jscape/inet/b/a/a/q;

    invoke-direct {v6, v3, v4, v2}, Lcom/jscape/inet/b/a/a/q;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/jscape/inet/b/a/a/p;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-nez v1, :cond_5

    goto :goto_2

    :cond_5
    move-object v3, v6

    goto :goto_0

    :catch_1
    move-exception p0

    :try_start_4
    invoke-static {p0}, Lcom/jscape/inet/b/a/a/o;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p0

    :try_start_5
    invoke-static {p0}, Lcom/jscape/inet/b/a/a/o;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    move-exception p0

    :try_start_6
    invoke-static {p0}, Lcom/jscape/inet/b/a/a/o;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    :catch_4
    move-exception p0

    :try_start_7
    invoke-static {p0}, Lcom/jscape/inet/b/a/a/o;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    :catch_5
    move-exception p0

    :try_start_8
    invoke-static {p0}, Lcom/jscape/inet/b/a/a/o;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    :catch_6
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/o;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_6
    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    :cond_7
    new-array p0, v6, [Lcom/jscape/inet/b/a/b/g;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/b/a/a/q;

    if-eqz v1, :cond_9

    add-int/lit8 v3, v5, 0x1

    :try_start_9
    invoke-virtual {v2}, Lcom/jscape/inet/b/a/a/q;->a()Lcom/jscape/inet/b/a/b/g;

    move-result-object v2

    aput-object v2, p0, v5
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    if-nez v1, :cond_8

    goto :goto_4

    :cond_8
    move v5, v3

    goto :goto_3

    :catch_7
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/b/a/a/o;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_9
    :goto_4
    return-object p0
.end method
