.class public Lcom/jscape/inet/b/a/e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/AutoCloseable;


# static fields
.field protected static final a:I = 0x10000

.field protected static final b:I = 0x1000

.field private static j:Z

.field private static final k:[Ljava/lang/String;


# instance fields
.field protected final c:Lcom/jscape/util/k/a/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/k/a/v<",
            "Lcom/jscape/util/k/a/C;",
            ">;"
        }
    .end annotation
.end field

.field protected final d:Lcom/jscape/util/k/a/s;

.field protected final e:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/b/a/b/i;",
            ">;"
        }
    .end annotation
.end field

.field protected final f:Ljava/util/logging/Logger;

.field protected g:Lcom/jscape/util/k/a/B;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/k/a/B<",
            "Lcom/jscape/inet/b/a/b/i;",
            ">;"
        }
    .end annotation
.end field

.field protected h:Ljava/io/InputStream;

.field protected i:Ljava/io/OutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v2}, Lcom/jscape/inet/b/a/e;->b(Z)V

    const/4 v4, 0x0

    const-string v5, "\u0010?) gDD6\u0005\u0018\u00133ND6K\u001e\u0002\"F_=\u000fGP\u001c\u0002XxWPNg\u0002X\u0005E$\u0010?) gDD6\u0005\u0018\u00133ND6K\u001e\u001c(TN<Q]+bT\u000bdFCPbTvv"

    const/16 v6, 0x4a

    const/16 v7, 0x25

    move v9, v4

    const/4 v8, -0x1

    :goto_0
    const/16 v10, 0x15

    add-int/2addr v8, v2

    add-int v11, v8, v7

    invoke-virtual {v5, v8, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v4

    :goto_2
    const/16 v15, 0x4d

    if-gt v13, v14, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    add-int/lit8 v11, v9, 0x1

    if-eqz v12, :cond_1

    aput-object v10, v1, v9

    add-int/2addr v8, v7

    if-ge v8, v6, :cond_0

    invoke-virtual {v5, v8}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v9, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x26

    const-string v6, "@TON{\u001f\u0014n\u001aduB&Zd__Rw\u0011\u001f)a\tR6JW7\u001a\tRKLZ,I\u0002&FOXFy\u001f\u0014n\u001aduB&Zd__Rw\u0011\u001f)a\tR6JW7\u001a\tRKLZ,I\u0002"

    move v7, v5

    move-object v5, v6

    move v9, v11

    move v6, v15

    const/4 v8, -0x1

    goto :goto_3

    :cond_1
    aput-object v10, v1, v9

    add-int/2addr v8, v7

    if-ge v8, v6, :cond_2

    invoke-virtual {v5, v8}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v9, v11

    :goto_3
    const/16 v10, 0x44

    add-int/2addr v8, v2

    add-int v11, v8, v7

    invoke-virtual {v5, v8, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v4

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/b/a/e;->k:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v3, v14, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v2, :cond_8

    const/4 v15, 0x2

    if-eq v3, v15, :cond_7

    const/4 v15, 0x3

    if-eq v3, v15, :cond_6

    if-eq v3, v0, :cond_5

    const/4 v15, 0x5

    if-eq v3, v15, :cond_4

    const/16 v15, 0x3e

    goto :goto_4

    :cond_4
    const/16 v15, 0x32

    goto :goto_4

    :cond_5
    const/16 v15, 0x52

    goto :goto_4

    :cond_6
    const/16 v15, 0x65

    goto :goto_4

    :cond_7
    const/16 v15, 0x68

    goto :goto_4

    :cond_8
    const/16 v15, 0x7e

    :cond_9
    :goto_4
    xor-int v3, v10, v15

    xor-int v3, v16, v3

    int-to-char v3, v3

    aput-char v3, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/util/k/a/v;Lcom/jscape/util/k/a/s;Lcom/jscape/util/h/I;Ljava/util/logging/Logger;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/v<",
            "Lcom/jscape/util/k/a/C;",
            ">;",
            "Lcom/jscape/util/k/a/s;",
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/b/a/b/i;",
            ">;",
            "Ljava/util/logging/Logger;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/b/a/e;->c:Lcom/jscape/util/k/a/v;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/b/a/e;->d:Lcom/jscape/util/k/a/s;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/b/a/e;->e:Lcom/jscape/util/h/I;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/inet/b/a/e;->f:Ljava/util/logging/Logger;

    return-void
.end method

.method public static a(Lcom/jscape/util/k/a/v;Lcom/jscape/util/k/a/s;Ljava/util/logging/Logger;)Lcom/jscape/inet/b/a/e;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/v<",
            "Lcom/jscape/util/k/a/C;",
            ">;",
            "Lcom/jscape/util/k/a/s;",
            "Ljava/util/logging/Logger;",
            ")",
            "Lcom/jscape/inet/b/a/e;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/b/a/e;

    invoke-static {}, Lcom/jscape/inet/b/a/a/m;->a()Lcom/jscape/inet/b/a/a/m;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/jscape/inet/b/a/e;-><init>(Lcom/jscape/util/k/a/v;Lcom/jscape/util/k/a/s;Lcom/jscape/util/h/I;Ljava/util/logging/Logger;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static b(Z)V
    .locals 0

    sput-boolean p0, Lcom/jscape/inet/b/a/e;->j:Z

    return-void
.end method

.method public static i()Z
    .locals 1

    sget-boolean v0, Lcom/jscape/inet/b/a/e;->j:Z

    return v0
.end method

.method public static j()Z
    .locals 1

    invoke-static {}, Lcom/jscape/inet/b/a/e;->i()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method protected a(Lcom/jscape/inet/b/a/b/j;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/b/j;)V

    invoke-static {}, Lcom/jscape/inet/b/a/e;->j()Z

    move-result v0

    invoke-virtual {p0}, Lcom/jscape/inet/b/a/e;->f()Lcom/jscape/inet/b/a/i;

    move-result-object v1

    if-nez v0, :cond_0

    :try_start_0
    invoke-virtual {v1}, Lcom/jscape/inet/b/a/i;->b()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    invoke-interface {p2, p0, v1}, Lcom/jscape/inet/b/a/f;->a(Lcom/jscape/inet/b/a/e;Lcom/jscape/inet/b/a/i;)V

    iget-object p1, p1, Lcom/jscape/inet/b/a/b/j;->e:Ljava/io/InputStream;

    iget-object p2, p0, Lcom/jscape/inet/b/a/e;->i:Ljava/io/OutputStream;

    const/high16 v0, 0x10000

    invoke-static {p1, p2, v0}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;Ljava/io/OutputStream;I)J

    iget-object p1, p0, Lcom/jscape/inet/b/a/e;->i:Ljava/io/OutputStream;

    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {p0}, Lcom/jscape/inet/b/a/e;->f()Lcom/jscape/inet/b/a/i;

    move-result-object p1

    move-object v1, p1

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/b/a/e;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    return-object v1
.end method

.method protected a(Lcom/jscape/inet/b/a/b/u;)Lcom/jscape/inet/b/a/i;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v4, Lcom/jscape/inet/b/a/b/h;

    iget-object v0, p1, Lcom/jscape/inet/b/a/b/u;->e:[Lcom/jscape/inet/b/a/b/g;

    invoke-direct {v4, v0}, Lcom/jscape/inet/b/a/b/h;-><init>([Lcom/jscape/inet/b/a/b/g;)V

    invoke-static {}, Lcom/jscape/inet/b/a/e;->j()Z

    move-result v0

    new-instance v6, Lcom/jscape/inet/b/a/b/h;

    const/4 v1, 0x0

    new-array v2, v1, [Lcom/jscape/inet/b/a/b/g;

    invoke-direct {v6, v2}, Lcom/jscape/inet/b/a/b/h;-><init>([Lcom/jscape/inet/b/a/b/g;)V

    iget-object v2, p1, Lcom/jscape/inet/b/a/b/u;->f:Ljava/io/InputStream;

    sget-object v3, Lcom/jscape/inet/b/a/a/c;->a:Lcom/jscape/inet/b/a/a/c;

    invoke-virtual {v3, v4}, Lcom/jscape/inet/b/a/a/c;->a(Lcom/jscape/inet/b/a/b/h;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    iget v5, p1, Lcom/jscape/inet/b/a/b/u;->c:I

    invoke-static {v5}, Lcom/jscape/inet/b/a/b/A;->a(I)Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v2, Lcom/jscape/util/h/k;

    invoke-direct {v2}, Lcom/jscape/util/h/k;-><init>()V

    if-eqz v0, :cond_3

    const/4 v5, 0x5

    :try_start_0
    new-array v5, v5, [I

    invoke-static {v5}, Lcom/jscape/util/aq;->b([I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/b/a/e;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    if-nez v0, :cond_2

    if-eqz v3, :cond_1

    new-instance v5, Lcom/jscape/util/h/j;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-direct {v5, v2, v7, v8, v1}, Lcom/jscape/util/h/j;-><init>(Ljava/io/InputStream;JZ)V

    if-eqz v0, :cond_4

    move-object v2, v5

    :cond_1
    :try_start_1
    sget-object v0, Lcom/jscape/inet/b/a/a/c;->c:Lcom/jscape/inet/b/a/a/c;

    invoke-virtual {v0, v4}, Lcom/jscape/inet/b/a/a/c;->a(Lcom/jscape/inet/b/a/b/h;)Ljava/lang/Object;

    move-result-object v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/b/a/e;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/jscape/inet/b/a/a/a;

    invoke-direct {v0, v2, v6}, Lcom/jscape/inet/b/a/a/a;-><init>(Ljava/io/InputStream;Lcom/jscape/inet/b/a/b/h;)V

    move-object v5, v0

    goto :goto_2

    :cond_3
    move-object v5, v2

    :cond_4
    :goto_2
    new-instance v7, Lcom/jscape/inet/b/a/i;

    iget-object v1, p1, Lcom/jscape/inet/b/a/b/u;->a:Ljava/lang/String;

    iget v2, p1, Lcom/jscape/inet/b/a/b/u;->c:I

    iget-object v3, p1, Lcom/jscape/inet/b/a/b/u;->d:Ljava/lang/String;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/b/a/i;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/jscape/inet/b/a/b/h;Ljava/io/InputStream;Lcom/jscape/inet/b/a/b/h;)V

    return-object v7
.end method

.method public a(Lcom/jscape/inet/b/a/h;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/h;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/b/a/b/k;

    iget-object v2, p1, Lcom/jscape/inet/b/a/h;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/jscape/inet/b/a/h;->a:Ljava/lang/String;

    iget-object p1, p1, Lcom/jscape/inet/b/a/h;->c:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {p1}, Lcom/jscape/inet/b/a/b/h;->a()[Lcom/jscape/inet/b/a/b/g;

    move-result-object p1

    invoke-direct {v1, v2, v3, p1, v0}, Lcom/jscape/inet/b/a/b/k;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/jscape/inet/b/a/b/g;Ljava/io/InputStream;)V

    invoke-virtual {p0, v1, p2}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/b/j;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/b/a/c;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/b/a/c;

    move-result-object p1

    throw p1
.end method

.method public a()Lcom/jscape/util/k/TransportAddress;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/b/a/e;->j()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/b/a/e;->g:Lcom/jscape/util/k/a/B;

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Lcom/jscape/util/k/a/B;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method protected a(Lcom/jscape/inet/b/a/h;)Ljava/io/InputStream;
    .locals 4

    invoke-static {}, Lcom/jscape/inet/b/a/e;->i()Z

    move-result v0

    sget-object v1, Lcom/jscape/inet/b/a/a/c;->a:Lcom/jscape/inet/b/a/a/c;

    iget-object v2, p1, Lcom/jscape/inet/b/a/h;->c:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {v1, v2}, Lcom/jscape/inet/b/a/a/c;->a(Lcom/jscape/inet/b/a/b/h;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/jscape/util/h/j;

    iget-object p1, p1, Lcom/jscape/inet/b/a/h;->d:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const/4 v3, 0x0

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/jscape/util/h/j;-><init>(Ljava/io/InputStream;JZ)V

    return-object v0

    :cond_0
    sget-object v0, Lcom/jscape/inet/b/a/a/c;->c:Lcom/jscape/inet/b/a/a/c;

    iget-object v1, p1, Lcom/jscape/inet/b/a/h;->c:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {v0, v1}, Lcom/jscape/inet/b/a/a/c;->a(Lcom/jscape/inet/b/a/b/h;)Ljava/lang/Object;

    move-result-object v1

    :cond_1
    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/jscape/inet/b/a/a/b;

    iget-object v1, p1, Lcom/jscape/inet/b/a/h;->d:Ljava/io/InputStream;

    const/16 v2, 0x1000

    iget-object p1, p1, Lcom/jscape/inet/b/a/h;->e:Lcom/jscape/inet/b/a/b/h;

    invoke-direct {v0, v1, v2, p1}, Lcom/jscape/inet/b/a/a/b;-><init>(Ljava/io/InputStream;ILcom/jscape/inet/b/a/b/h;)V

    return-object v0

    :cond_2
    iget-object p1, p1, Lcom/jscape/inet/b/a/h;->d:Ljava/io/InputStream;

    return-object p1
.end method

.method protected a(Lcom/jscape/inet/b/a/b/i;)V
    .locals 7

    invoke-static {}, Lcom/jscape/inet/b/a/e;->i()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/b/a/e;->f:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/b/a/e;->f:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/b/a/e;->k:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v0, v0, v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/jscape/inet/b/a/e;->g:Lcom/jscape/util/k/a/B;

    invoke-virtual {v6}, Lcom/jscape/util/k/a/B;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/jscape/inet/b/a/e;->g:Lcom/jscape/util/k/a/B;

    invoke-virtual {v6}, Lcom/jscape/util/k/a/B;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v6

    aput-object v6, v4, v5

    aput-object p1, v4, v3

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected a(Lcom/jscape/inet/b/a/b/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/e;->b(Lcom/jscape/inet/b/a/b/i;)V

    iget-object v0, p0, Lcom/jscape/inet/b/a/e;->g:Lcom/jscape/util/k/a/B;

    invoke-virtual {v0, p1}, Lcom/jscape/util/k/a/B;->write(Ljava/lang/Object;)V

    return-void
.end method

.method protected a(Lcom/jscape/inet/b/a/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/b/a/d;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/inet/b/a/i;->c()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/inet/b/a/d;

    invoke-direct {v0, p1}, Lcom/jscape/inet/b/a/d;-><init>(Lcom/jscape/inet/b/a/i;)V

    throw v0
    :try_end_0
    .catch Lcom/jscape/inet/b/a/d; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/b/a/e;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public b(Lcom/jscape/inet/b/a/h;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/h;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/b/a/b/n;

    iget-object v2, p1, Lcom/jscape/inet/b/a/h;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/jscape/inet/b/a/h;->a:Ljava/lang/String;

    iget-object p1, p1, Lcom/jscape/inet/b/a/h;->c:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {p1}, Lcom/jscape/inet/b/a/b/h;->a()[Lcom/jscape/inet/b/a/b/g;

    move-result-object p1

    invoke-direct {v1, v2, v3, p1, v0}, Lcom/jscape/inet/b/a/b/n;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/jscape/inet/b/a/b/g;Ljava/io/InputStream;)V

    invoke-virtual {p0, v1, p2}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/b/j;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;

    move-result-object p1

    new-instance p2, Lcom/jscape/inet/b/a/i;

    iget-object v1, p1, Lcom/jscape/inet/b/a/i;->a:Ljava/lang/String;

    iget v2, p1, Lcom/jscape/inet/b/a/i;->b:I

    iget-object v3, p1, Lcom/jscape/inet/b/a/i;->c:Ljava/lang/String;

    iget-object v4, p1, Lcom/jscape/inet/b/a/i;->d:Lcom/jscape/inet/b/a/b/h;

    new-instance v5, Lcom/jscape/util/h/k;

    invoke-direct {v5}, Lcom/jscape/util/h/k;-><init>()V

    move-object v0, p2

    invoke-direct/range {v0 .. v5}, Lcom/jscape/inet/b/a/i;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/jscape/inet/b/a/b/h;Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p2

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/b/a/c;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/b/a/c;

    move-result-object p1

    throw p1
.end method

.method public b()Lcom/jscape/util/k/TransportAddress;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/b/a/e;->i()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/b/a/e;->g:Lcom/jscape/util/k/a/B;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Lcom/jscape/util/k/a/B;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method protected b(Lcom/jscape/inet/b/a/b/i;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/b/a/e;->j()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/b/a/e;->f:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/b/a/e;->f:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/b/a/e;->k:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v0, v0, v3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jscape/inet/b/a/e;->g:Lcom/jscape/util/k/a/B;

    invoke-virtual {v5}, Lcom/jscape/util/k/a/B;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/jscape/inet/b/a/e;->g:Lcom/jscape/util/k/a/B;

    invoke-virtual {v5}, Lcom/jscape/util/k/a/B;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public c()Lcom/jscape/inet/b/a/e;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/b/a/c;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/b/a/e;->c:Lcom/jscape/util/k/a/v;

    invoke-interface {v1}, Lcom/jscape/util/k/a/v;->connect()Lcom/jscape/util/k/a/r;

    move-result-object v1

    check-cast v1, Lcom/jscape/util/k/a/C;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/b/a/e;->d:Lcom/jscape/util/k/a/s;

    invoke-interface {v0, v1}, Lcom/jscape/util/k/a/s;->a(Lcom/jscape/util/k/a/r;)V

    new-instance v0, Lcom/jscape/util/k/a/B;

    iget-object v2, p0, Lcom/jscape/inet/b/a/e;->e:Lcom/jscape/util/h/I;

    invoke-direct {v0, v1, v2}, Lcom/jscape/util/k/a/B;-><init>(Lcom/jscape/util/k/a/C;Lcom/jscape/util/h/I;)V

    iput-object v0, p0, Lcom/jscape/inet/b/a/e;->g:Lcom/jscape/util/k/a/B;

    new-instance v0, Lcom/jscape/util/k/a/a;

    invoke-direct {v0, v1}, Lcom/jscape/util/k/a/a;-><init>(Lcom/jscape/util/k/a/C;)V

    iput-object v0, p0, Lcom/jscape/inet/b/a/e;->h:Ljava/io/InputStream;

    new-instance v0, Lcom/jscape/util/k/a/e;

    invoke-direct {v0, v1}, Lcom/jscape/util/k/a/e;-><init>(Lcom/jscape/util/k/a/C;)V

    iput-object v0, p0, Lcom/jscape/inet/b/a/e;->i:Ljava/io/OutputStream;

    invoke-virtual {p0}, Lcom/jscape/inet/b/a/e;->g()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_0
    invoke-static {v1}, Lcom/jscape/util/k/a/u;->a(Lcom/jscape/util/k/a/r;)V

    invoke-static {v0}, Lcom/jscape/inet/b/a/c;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/b/a/c;

    move-result-object v0

    throw v0
.end method

.method public c(Lcom/jscape/inet/b/a/h;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/h;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/b/a/b/o;

    iget-object v2, p1, Lcom/jscape/inet/b/a/h;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/jscape/inet/b/a/h;->a:Ljava/lang/String;

    iget-object p1, p1, Lcom/jscape/inet/b/a/h;->c:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {p1}, Lcom/jscape/inet/b/a/b/h;->a()[Lcom/jscape/inet/b/a/b/g;

    move-result-object p1

    invoke-direct {v1, v2, v3, p1, v0}, Lcom/jscape/inet/b/a/b/o;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/jscape/inet/b/a/b/g;Ljava/io/InputStream;)V

    invoke-virtual {p0, v1, p2}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/b/j;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/b/a/c;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/b/a/c;

    move-result-object p1

    throw p1
.end method

.method public close()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/b/a/e;->j()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/b/a/e;->g:Lcom/jscape/util/k/a/B;

    if-nez v0, :cond_0

    if-eqz v1, :cond_2

    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {v1}, Lcom/jscape/util/k/a/B;->closed()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v1, p0, Lcom/jscape/inet/b/a/e;->g:Lcom/jscape/util/k/a/B;

    :cond_1
    invoke-virtual {v1}, Lcom/jscape/util/k/a/B;->close()V

    invoke-virtual {p0}, Lcom/jscape/inet/b/a/e;->h()V

    :cond_2
    return-void
.end method

.method public d(Lcom/jscape/inet/b/a/h;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/h;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/b/a/b/s;

    iget-object v2, p1, Lcom/jscape/inet/b/a/h;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/jscape/inet/b/a/h;->a:Ljava/lang/String;

    iget-object p1, p1, Lcom/jscape/inet/b/a/h;->c:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {p1}, Lcom/jscape/inet/b/a/b/h;->a()[Lcom/jscape/inet/b/a/b/g;

    move-result-object p1

    invoke-direct {v1, v2, v3, p1, v0}, Lcom/jscape/inet/b/a/b/s;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/jscape/inet/b/a/b/g;Ljava/io/InputStream;)V

    invoke-virtual {p0, v1, p2}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/b/j;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/b/a/c;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/b/a/c;

    move-result-object p1

    throw p1
.end method

.method public d()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/b/a/e;->h:Ljava/io/InputStream;

    return-object v0
.end method

.method public e(Lcom/jscape/inet/b/a/h;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/h;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/b/a/b/m;

    iget-object v2, p1, Lcom/jscape/inet/b/a/h;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/jscape/inet/b/a/h;->a:Ljava/lang/String;

    iget-object p1, p1, Lcom/jscape/inet/b/a/h;->c:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {p1}, Lcom/jscape/inet/b/a/b/h;->a()[Lcom/jscape/inet/b/a/b/g;

    move-result-object p1

    invoke-direct {v1, v2, v3, p1, v0}, Lcom/jscape/inet/b/a/b/m;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/jscape/inet/b/a/b/g;Ljava/io/InputStream;)V

    invoke-virtual {p0, v1, p2}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/b/j;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/b/a/c;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/b/a/c;

    move-result-object p1

    throw p1
.end method

.method public e()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/b/a/e;->i:Ljava/io/OutputStream;

    return-object v0
.end method

.method protected f()Lcom/jscape/inet/b/a/i;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/b/a/e;->g:Lcom/jscape/util/k/a/B;

    invoke-virtual {v0}, Lcom/jscape/util/k/a/B;->read()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/b/a/b/u;

    invoke-virtual {p0, v0}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/b/i;)V

    invoke-virtual {p0, v0}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/b/u;)Lcom/jscape/inet/b/a/i;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/i;)V

    return-object v0
.end method

.method public f(Lcom/jscape/inet/b/a/h;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/h;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/b/a/b/p;

    iget-object v2, p1, Lcom/jscape/inet/b/a/h;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/jscape/inet/b/a/h;->a:Ljava/lang/String;

    iget-object p1, p1, Lcom/jscape/inet/b/a/h;->c:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {p1}, Lcom/jscape/inet/b/a/b/h;->a()[Lcom/jscape/inet/b/a/b/g;

    move-result-object p1

    invoke-direct {v1, v2, v3, p1, v0}, Lcom/jscape/inet/b/a/b/p;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/jscape/inet/b/a/b/g;Ljava/io/InputStream;)V

    invoke-virtual {p0, v1, p2}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/b/j;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/b/a/c;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/b/a/c;

    move-result-object p1

    throw p1
.end method

.method public g(Lcom/jscape/inet/b/a/h;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/h;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/b/a/b/r;

    iget-object v2, p1, Lcom/jscape/inet/b/a/h;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/jscape/inet/b/a/h;->a:Ljava/lang/String;

    iget-object p1, p1, Lcom/jscape/inet/b/a/h;->c:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {p1}, Lcom/jscape/inet/b/a/b/h;->a()[Lcom/jscape/inet/b/a/b/g;

    move-result-object p1

    invoke-direct {v1, v2, v3, p1, v0}, Lcom/jscape/inet/b/a/b/r;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/jscape/inet/b/a/b/g;Ljava/io/InputStream;)V

    invoke-virtual {p0, v1, p2}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/b/j;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/b/a/c;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/b/a/c;

    move-result-object p1

    throw p1
.end method

.method protected g()V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/b/a/e;->j()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/b/a/e;->f:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/b/a/e;->f:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/b/a/e;->k:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/jscape/inet/b/a/e;->g:Lcom/jscape/util/k/a/B;

    invoke-virtual {v5}, Lcom/jscape/util/k/a/B;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/jscape/inet/b/a/e;->g:Lcom/jscape/util/k/a/B;

    invoke-virtual {v5}, Lcom/jscape/util/k/a/B;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public h(Lcom/jscape/inet/b/a/h;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/h;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/b/a/b/q;

    iget-object v2, p1, Lcom/jscape/inet/b/a/h;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/jscape/inet/b/a/h;->a:Ljava/lang/String;

    iget-object p1, p1, Lcom/jscape/inet/b/a/h;->c:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {p1}, Lcom/jscape/inet/b/a/b/h;->a()[Lcom/jscape/inet/b/a/b/g;

    move-result-object p1

    invoke-direct {v1, v2, v3, p1, v0}, Lcom/jscape/inet/b/a/b/q;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/jscape/inet/b/a/b/g;Ljava/io/InputStream;)V

    invoke-virtual {p0, v1, p2}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/b/j;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/b/a/c;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/b/a/c;

    move-result-object p1

    throw p1
.end method

.method protected h()V
    .locals 7

    invoke-static {}, Lcom/jscape/inet/b/a/e;->j()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/inet/b/a/e;->f:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/b/a/e;->f:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/b/a/e;->k:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v0, v0, v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/jscape/inet/b/a/e;->g:Lcom/jscape/util/k/a/B;

    invoke-virtual {v6}, Lcom/jscape/util/k/a/B;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v6

    aput-object v6, v4, v5

    iget-object v5, p0, Lcom/jscape/inet/b/a/e;->g:Lcom/jscape/util/k/a/B;

    invoke-virtual {v5}, Lcom/jscape/util/k/a/B;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public i(Lcom/jscape/inet/b/a/h;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/h;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/b/a/b/l;

    iget-object v2, p1, Lcom/jscape/inet/b/a/h;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/jscape/inet/b/a/h;->a:Ljava/lang/String;

    iget-object p1, p1, Lcom/jscape/inet/b/a/h;->c:Lcom/jscape/inet/b/a/b/h;

    invoke-virtual {p1}, Lcom/jscape/inet/b/a/b/h;->a()[Lcom/jscape/inet/b/a/b/g;

    move-result-object p1

    invoke-direct {v1, v2, v3, p1, v0}, Lcom/jscape/inet/b/a/b/l;-><init>(Ljava/lang/String;Ljava/lang/String;[Lcom/jscape/inet/b/a/b/g;Ljava/io/InputStream;)V

    invoke-virtual {p0, v1, p2}, Lcom/jscape/inet/b/a/e;->a(Lcom/jscape/inet/b/a/b/j;Lcom/jscape/inet/b/a/f;)Lcom/jscape/inet/b/a/i;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/b/a/c;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/b/a/c;

    move-result-object p1

    throw p1
.end method
