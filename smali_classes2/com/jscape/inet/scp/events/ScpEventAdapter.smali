.class public Lcom/jscape/inet/scp/events/ScpEventAdapter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/scp/events/ScpEventListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public connected(Lcom/jscape/inet/scp/events/ScpConnectedEvent;)V
    .locals 0

    return-void
.end method

.method public disconnected(Lcom/jscape/inet/scp/events/ScpDisconnectedEvent;)V
    .locals 0

    return-void
.end method

.method public download(Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;)V
    .locals 0

    return-void
.end method

.method public progress(Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;)V
    .locals 0

    return-void
.end method

.method public upload(Lcom/jscape/inet/scp/events/ScpFileUploadedEvent;)V
    .locals 0

    return-void
.end method
