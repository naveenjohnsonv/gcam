.class public Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;
.super Lcom/jscape/inet/scp/events/ScpEvent;


# static fields
.field private static final f:[Ljava/lang/String;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:J


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x22

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x53

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "|9\"zEsJk5%R@pNK?6yZzA[z)ZEsJA;?Y\u00118\u0007\u0003z&UAz\u0012\u0007\u0003z!UVz\u0012"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x32

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;->f:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/16 v13, 0x7c

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v14, 0x2

    if-eq v12, v14, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v14, 0x4

    if-eq v12, v14, :cond_3

    const/4 v14, 0x5

    if-eq v12, v14, :cond_2

    goto :goto_2

    :cond_2
    const/16 v13, 0x4c

    goto :goto_2

    :cond_3
    const/16 v13, 0x7f

    goto :goto_2

    :cond_4
    const/16 v13, 0x6f

    goto :goto_2

    :cond_5
    move v13, v7

    goto :goto_2

    :cond_6
    const/16 v13, 0x9

    :cond_7
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lcom/jscape/inet/scp/Scp;Ljava/lang/String;JJ)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/scp/events/ScpEvent;-><init>(Lcom/jscape/inet/scp/Scp;)V

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;->c:Ljava/lang/String;

    iput-wide p3, p0, Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;->d:J

    iput-wide p5, p0, Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;->e:J

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/inet/scp/events/ScpEventListener;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/jscape/inet/scp/events/ScpEventListener;->download(Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;)V

    return-void
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;->d:J

    return-wide v0
.end method

.method public getTime()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;->e:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;->f:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;->e:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
