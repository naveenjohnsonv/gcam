.class public Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;
.super Lcom/jscape/inet/scp/events/ScpEvent;


# static fields
.field public static final DOWNLOAD:I = 0x1

.field public static final UPLOAD:I

.field private static final h:[Ljava/lang/String;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:J

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v5, "\u0001X1\\z\u0001t\u0013\u0001X(A\u007f\n:K\u001d.A{\u0000\u000bT\u000c9@#"

    const/16 v6, 0x1b

    const/4 v7, -0x1

    const/4 v8, 0x7

    const/4 v9, 0x0

    :goto_0
    const/16 v10, 0x79

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/16 v13, 0x1d

    const/4 v14, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v15, v12

    const/4 v2, 0x0

    :goto_2
    if-gt v15, v2, :cond_3

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v10, v9, 0x1

    if-eqz v14, :cond_1

    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v10

    goto :goto_0

    :cond_0
    const/16 v6, 0x2c

    const-string v5, "e<K>\u0000e\u0010$\u001a\u007fH\u0003\u0008aC:z]%*rB.n]$\tE[,rLw\u0001fD%yV6\u0017e\u0010n"

    move v9, v10

    const/4 v7, -0x1

    const/4 v8, 0x7

    goto :goto_3

    :cond_1
    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v2

    move v9, v10

    :goto_3
    add-int/2addr v7, v11

    add-int v2, v7, v8

    invoke-virtual {v5, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v10, v13

    const/4 v14, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;->h:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v12, v2

    rem-int/lit8 v3, v2, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v11, :cond_8

    const/4 v4, 0x2

    if-eq v3, v4, :cond_7

    const/4 v4, 0x3

    if-eq v3, v4, :cond_6

    if-eq v3, v0, :cond_5

    const/4 v4, 0x5

    if-eq v3, v4, :cond_4

    const/16 v3, 0x30

    goto :goto_4

    :cond_4
    move v3, v13

    goto :goto_4

    :cond_5
    const/16 v3, 0x67

    goto :goto_4

    :cond_6
    const/16 v3, 0x4a

    goto :goto_4

    :cond_7
    const/16 v3, 0x25

    goto :goto_4

    :cond_8
    move v3, v11

    goto :goto_4

    :cond_9
    const/16 v3, 0x54

    :goto_4
    xor-int/2addr v3, v10

    xor-int v3, v16, v3

    int-to-char v3, v3

    aput-char v3, v12, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/inet/scp/Scp;Ljava/lang/String;JJI)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/scp/events/ScpEvent;-><init>(Lcom/jscape/inet/scp/Scp;)V

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;->c:Ljava/lang/String;

    iput-wide p3, p0, Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;->d:J

    iput-wide p5, p0, Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;->e:J

    iput p7, p0, Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;->f:I

    return-void
.end method


# virtual methods
.method public accept(Lcom/jscape/inet/scp/events/ScpEventListener;)V
    .locals 0

    invoke-interface {p1, p0}, Lcom/jscape/inet/scp/events/ScpEventListener;->progress(Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;)V

    return-void
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getMode()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;->f:I

    return v0
.end method

.method public getSize()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;->e:J

    return-wide v0
.end method

.method public getTransferredBytes()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;->d:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    invoke-static {}, Lcom/jscape/inet/scp/events/ScpEvent;->c()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;->h:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v4, v2, v3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;->c:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x27

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v4, 0x1

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;->d:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v4, 0x2

    aget-object v4, v2, v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v4, p0, Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;->e:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    new-array v0, v3, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object v1
.end method
