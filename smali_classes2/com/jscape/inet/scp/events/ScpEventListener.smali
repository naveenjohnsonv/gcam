.class public interface abstract Lcom/jscape/inet/scp/events/ScpEventListener;
.super Ljava/lang/Object;


# virtual methods
.method public abstract connected(Lcom/jscape/inet/scp/events/ScpConnectedEvent;)V
.end method

.method public abstract disconnected(Lcom/jscape/inet/scp/events/ScpDisconnectedEvent;)V
.end method

.method public abstract download(Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;)V
.end method

.method public abstract progress(Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;)V
.end method

.method public abstract upload(Lcom/jscape/inet/scp/events/ScpFileUploadedEvent;)V
.end method
