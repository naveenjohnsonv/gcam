.class public abstract Lcom/jscape/inet/scp/events/ScpEvent;
.super Ljava/lang/Object;


# static fields
.field private static b:Z

.field private static final g:Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/inet/scp/Scp;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/jscape/inet/scp/events/ScpEvent;->b(Z)V

    const-string v1, "z\u0011/\u0007NjX]R$1WzDJ\u0017b"

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-gt v2, v3, :cond_0

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/scp/events/ScpEvent;->g:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v4, v1, v3

    rem-int/lit8 v5, v3, 0x7

    if-eqz v5, :cond_6

    if-eq v5, v0, :cond_5

    const/4 v6, 0x2

    if-eq v5, v6, :cond_4

    const/4 v6, 0x3

    if-eq v5, v6, :cond_3

    const/4 v6, 0x4

    if-eq v5, v6, :cond_2

    const/4 v6, 0x5

    if-eq v5, v6, :cond_1

    const/16 v5, 0x18

    goto :goto_1

    :cond_1
    const/16 v5, 0x21

    goto :goto_1

    :cond_2
    const/16 v5, 0x16

    goto :goto_1

    :cond_3
    const/16 v5, 0x6c

    goto :goto_1

    :cond_4
    const/16 v5, 0x71

    goto :goto_1

    :cond_5
    const/16 v5, 0x5c

    goto :goto_1

    :cond_6
    const/4 v5, 0x7

    :goto_1
    const/16 v6, 0x2e

    xor-int/2addr v5, v6

    xor-int/2addr v4, v5

    int-to-char v4, v4

    aput-char v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method protected constructor <init>(Lcom/jscape/inet/scp/Scp;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/scp/events/ScpEvent;->a:Lcom/jscape/inet/scp/Scp;

    return-void
.end method

.method public static b(Z)V
    .locals 0

    sput-boolean p0, Lcom/jscape/inet/scp/events/ScpEvent;->b:Z

    return-void
.end method

.method public static b()Z
    .locals 1

    sget-boolean v0, Lcom/jscape/inet/scp/events/ScpEvent;->b:Z

    return v0
.end method

.method public static c()Z
    .locals 1

    invoke-static {}, Lcom/jscape/inet/scp/events/ScpEvent;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public abstract accept(Lcom/jscape/inet/scp/events/ScpEventListener;)V
.end method

.method public getSource()Lcom/jscape/inet/scp/Scp;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/scp/events/ScpEvent;->a:Lcom/jscape/inet/scp/Scp;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/scp/events/ScpEvent;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/scp/events/ScpEvent;->a:Lcom/jscape/inet/scp/Scp;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
