.class public interface abstract Lcom/jscape/inet/scp/FileService;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/n/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/n/d<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract available()Z
.end method

.method public abstract cancelTransferOperation()V
.end method

.method public abstract readDirectory(Ljava/lang/String;Ljava/io/File;ZLcom/jscape/inet/scp/FileService$TransferListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/scp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract readFile(Ljava/lang/String;Ljava/io/OutputStream;ZLcom/jscape/inet/scp/FileService$TransferListener;)Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/scp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract readFiles(Ljava/lang/String;Ljava/io/File;ZLcom/jscape/inet/scp/FileService$TransferListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/scp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract writeDirectory(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/jscape/inet/scp/FileService$TransferListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/scp/FileService$OperationException;
        }
    .end annotation
.end method

.method public abstract writeFile(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/Long;Lcom/jscape/inet/scp/FileService$TransferListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/scp/FileService$OperationException;
        }
    .end annotation
.end method
