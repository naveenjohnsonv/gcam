.class Lcom/jscape/inet/scp/Scp$DownloadListener;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/inet/scp/FileService$TransferListener;


# instance fields
.field private a:Ljava/lang/String;

.field private b:J

.field private c:J

.field final d:Lcom/jscape/inet/scp/Scp;


# direct methods
.method private constructor <init>(Lcom/jscape/inet/scp/Scp;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/scp/Scp$DownloadListener;->d:Lcom/jscape/inet/scp/Scp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(Lcom/jscape/inet/scp/Scp;Lcom/jscape/inet/scp/Scp$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/scp/Scp$DownloadListener;-><init>(Lcom/jscape/inet/scp/Scp;)V

    return-void
.end method


# virtual methods
.method public onTransferCompleted(J)V
    .locals 9

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp$DownloadListener;->d:Lcom/jscape/inet/scp/Scp;

    new-instance v8, Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp$DownloadListener;->d:Lcom/jscape/inet/scp/Scp;

    iget-object v3, p0, Lcom/jscape/inet/scp/Scp$DownloadListener;->a:Ljava/lang/String;

    iget-wide v4, p0, Lcom/jscape/inet/scp/Scp$DownloadListener;->c:J

    invoke-static {v4, v5}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v6

    move-object v1, v8

    move-wide v4, p1

    invoke-direct/range {v1 .. v7}, Lcom/jscape/inet/scp/events/ScpFileDownloadedEvent;-><init>(Lcom/jscape/inet/scp/Scp;Ljava/lang/String;JJ)V

    invoke-static {v0, v8}, Lcom/jscape/inet/scp/Scp;->a(Lcom/jscape/inet/scp/Scp;Lcom/jscape/inet/scp/events/ScpEvent;)V

    return-void
.end method

.method public onTransferProgress(J)V
    .locals 10

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp$DownloadListener;->d:Lcom/jscape/inet/scp/Scp;

    new-instance v9, Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp$DownloadListener;->d:Lcom/jscape/inet/scp/Scp;

    iget-object v3, p0, Lcom/jscape/inet/scp/Scp$DownloadListener;->a:Ljava/lang/String;

    iget-wide v6, p0, Lcom/jscape/inet/scp/Scp$DownloadListener;->b:J

    const/4 v8, 0x1

    move-object v1, v9

    move-wide v4, p1

    invoke-direct/range {v1 .. v8}, Lcom/jscape/inet/scp/events/ScpTransferProgressEvent;-><init>(Lcom/jscape/inet/scp/Scp;Ljava/lang/String;JJI)V

    invoke-static {v0, v9}, Lcom/jscape/inet/scp/Scp;->a(Lcom/jscape/inet/scp/Scp;Lcom/jscape/inet/scp/events/ScpEvent;)V

    return-void
.end method

.method public onTransferStarted(Ljava/lang/String;J)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/scp/Scp$DownloadListener;->a:Ljava/lang/String;

    iput-wide p2, p0, Lcom/jscape/inet/scp/Scp$DownloadListener;->b:J

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/jscape/inet/scp/Scp$DownloadListener;->c:J

    return-void
.end method
