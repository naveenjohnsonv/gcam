.class public Lcom/jscape/inet/scp/protocol/marshaling/DirectoryMessageCodec;
.super Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "P\u001beSOy\t"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/scp/protocol/marshaling/DirectoryMessageCodec;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x4e

    goto :goto_1

    :cond_1
    const/16 v4, 0x68

    goto :goto_1

    :cond_2
    const/16 v4, 0x5b

    goto :goto_1

    :cond_3
    const/16 v4, 0x57

    goto :goto_1

    :cond_4
    const/16 v4, 0x71

    goto :goto_1

    :cond_5
    const/16 v4, 0x5c

    goto :goto_1

    :cond_6
    const/16 v4, 0x41

    :goto_1
    const/16 v5, 0x34

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/scp/protocol/messages/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, " "

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/scp/protocol/marshaling/DirectoryMessageCodec;->readLine(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    new-instance v1, Ljava/util/Scanner;

    invoke-direct {v1, p1}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/Scanner;->next()Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/Scanner;->skip(Ljava/lang/String;)Ljava/util/Scanner;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;

    invoke-direct {v0, p1, v1}, Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/scp/protocol/marshaling/DirectoryMessageCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/scp/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/jscape/inet/scp/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;

    sget-object v0, Lcom/jscape/inet/scp/protocol/marshaling/DirectoryMessageCodec;->a:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p1, Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;->directoryMode:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object p1, p1, Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;->directoryName:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write([B)V

    const/16 p1, 0xa

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/scp/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/scp/protocol/marshaling/DirectoryMessageCodec;->write(Lcom/jscape/inet/scp/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
