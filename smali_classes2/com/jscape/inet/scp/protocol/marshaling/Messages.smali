.class public Lcom/jscape/inet/scp/protocol/marshaling/Messages;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;

    new-instance v1, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;

    const-class v2, Lcom/jscape/inet/scp/protocol/messages/OkMessage;

    new-instance v3, Lcom/jscape/inet/scp/protocol/marshaling/OkMessageCodec;

    invoke-direct {v3}, Lcom/jscape/inet/scp/protocol/marshaling/OkMessageCodec;-><init>()V

    const/4 v4, 0x0

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;

    const-class v2, Lcom/jscape/inet/scp/protocol/messages/WarningMessage;

    new-instance v3, Lcom/jscape/inet/scp/protocol/marshaling/WarningMessageCodec;

    invoke-direct {v3}, Lcom/jscape/inet/scp/protocol/marshaling/WarningMessageCodec;-><init>()V

    const/4 v4, 0x1

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;

    const-class v2, Lcom/jscape/inet/scp/protocol/messages/FatalErrorMessage;

    new-instance v3, Lcom/jscape/inet/scp/protocol/marshaling/FatalErrorMessageCodec;

    invoke-direct {v3}, Lcom/jscape/inet/scp/protocol/marshaling/FatalErrorMessageCodec;-><init>()V

    const/4 v4, 0x2

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;

    const-class v2, Lcom/jscape/inet/scp/protocol/messages/FileMessage;

    new-instance v3, Lcom/jscape/inet/scp/protocol/marshaling/FileMessageCodec;

    invoke-direct {v3}, Lcom/jscape/inet/scp/protocol/marshaling/FileMessageCodec;-><init>()V

    const/16 v4, 0x43

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;

    const-class v2, Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;

    new-instance v3, Lcom/jscape/inet/scp/protocol/marshaling/DirectoryMessageCodec;

    invoke-direct {v3}, Lcom/jscape/inet/scp/protocol/marshaling/DirectoryMessageCodec;-><init>()V

    const/16 v4, 0x44

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;

    const-class v2, Lcom/jscape/inet/scp/protocol/messages/EndDirectoryMessage;

    new-instance v3, Lcom/jscape/inet/scp/protocol/marshaling/EndDirectoryMessageCodec;

    invoke-direct {v3}, Lcom/jscape/inet/scp/protocol/marshaling/EndDirectoryMessageCodec;-><init>()V

    const/16 v4, 0x45

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;

    const-class v2, Lcom/jscape/inet/scp/protocol/messages/TimeMessage;

    new-instance v3, Lcom/jscape/inet/scp/protocol/marshaling/TimeMessageCodec;

    invoke-direct {v3}, Lcom/jscape/inet/scp/protocol/marshaling/TimeMessageCodec;-><init>()V

    const/16 v4, 0x54

    invoke-direct {v1, v4, v2, v3}, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sput-object v0, Lcom/jscape/inet/scp/protocol/marshaling/Messages;->a:[Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init(Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec;)Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec;
    .locals 1

    sget-object v0, Lcom/jscape/inet/scp/protocol/marshaling/Messages;->a:[Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;

    invoke-virtual {p0, v0}, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec;->set([Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;)Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec;

    move-result-object p0

    return-object p0
.end method
