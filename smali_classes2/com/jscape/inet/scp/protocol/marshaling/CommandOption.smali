.class public final enum Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum PRESERVE_FILE_ATTRIBUTES:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

.field public static final enum READ:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

.field public static final enum RECURSIVE:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

.field public static final enum TARGET_DIRECTORY:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

.field public static final enum WRITE:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

.field private static final synthetic a:[Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

.field private static final b:[Ljava/lang/String;


# instance fields
.field public final value:C


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "ZQM\u0004pc?JYM\u0006vc/\\I\t\\U\\\u0016gd)XU\u0005YBV\u0017p\u0013[~l6EG\u000f|dz\'\u0015A\u0001bezy\u0015"

    const/16 v4, 0x34

    const/16 v5, 0x10

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x6e

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    const/4 v14, 0x5

    const/4 v1, 0x4

    const/4 v15, 0x2

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    if-eqz v11, :cond_1

    add-int/lit8 v1, v7, 0x1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v1

    goto :goto_0

    :cond_0
    const/16 v4, 0x1d

    const/16 v3, 0x18

    const-string v5, "JVN\u0004dq\"_[M\u001emf+[P_\u0005ha!NAX\u0004HAJ\u0013"

    move v7, v1

    const/4 v6, -0x1

    move-object/from16 v17, v5

    move v5, v3

    move-object/from16 v3, v17

    goto :goto_3

    :cond_1
    add-int/lit8 v10, v7, 0x1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v5, v1

    move v7, v10

    :goto_3
    const/16 v8, 0x7a

    add-int/2addr v6, v9

    add-int v1, v6, v5

    invoke-virtual {v3, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->b:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    sget-object v3, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->b:[Ljava/lang/String;

    aget-object v4, v3, v1

    const/16 v5, 0x70

    invoke-direct {v0, v4, v2, v5}, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;-><init>(Ljava/lang/String;IC)V

    sput-object v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->PRESERVE_FILE_ATTRIBUTES:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    new-instance v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    aget-object v4, v3, v9

    const/16 v5, 0x72

    invoke-direct {v0, v4, v9, v5}, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;-><init>(Ljava/lang/String;IC)V

    sput-object v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->RECURSIVE:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    new-instance v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    aget-object v4, v3, v15

    const/16 v5, 0x74

    invoke-direct {v0, v4, v15, v5}, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;-><init>(Ljava/lang/String;IC)V

    sput-object v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->WRITE:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    new-instance v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    aget-object v4, v3, v14

    const/16 v5, 0x66

    const/4 v6, 0x3

    invoke-direct {v0, v4, v6, v5}, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;-><init>(Ljava/lang/String;IC)V

    sput-object v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->READ:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    new-instance v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    aget-object v3, v3, v2

    const/16 v4, 0x64

    invoke-direct {v0, v3, v1, v4}, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;-><init>(Ljava/lang/String;IC)V

    sput-object v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->TARGET_DIRECTORY:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    new-array v3, v14, [Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    sget-object v4, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->PRESERVE_FILE_ATTRIBUTES:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    aput-object v4, v3, v2

    sget-object v2, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->RECURSIVE:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    aput-object v2, v3, v9

    sget-object v2, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->WRITE:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    aput-object v2, v3, v15

    sget-object v2, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->READ:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    const/4 v4, 0x3

    aput-object v2, v3, v4

    aput-object v0, v3, v1

    sput-object v3, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->a:[Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    return-void

    :cond_3
    const/4 v2, 0x3

    aget-char v16, v10, v13

    rem-int/lit8 v14, v13, 0x7

    if-eqz v14, :cond_9

    if-eq v14, v9, :cond_8

    if-eq v14, v15, :cond_7

    if-eq v14, v2, :cond_6

    if-eq v14, v1, :cond_5

    const/4 v1, 0x5

    if-eq v14, v1, :cond_4

    const/16 v1, 0xe

    goto :goto_4

    :cond_4
    const/16 v1, 0x59

    goto :goto_4

    :cond_5
    const/16 v1, 0x5b

    goto :goto_4

    :cond_6
    const/16 v1, 0x2d

    goto :goto_4

    :cond_7
    const/16 v1, 0x71

    goto :goto_4

    :cond_8
    const/16 v1, 0x7e

    goto :goto_4

    :cond_9
    const/16 v1, 0x60

    :goto_4
    xor-int/2addr v1, v8

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    const/4 v2, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;IC)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(C)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-char p3, p0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->value:C

    return-void
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method

.method public static optionFor(C)Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;
    .locals 6

    invoke-static {}, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->values()[Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    move-result-object v0

    invoke-static {}, Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;->b()[Lcom/jscape/util/aq;

    move-result-object v1

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v0, v3

    if-nez v1, :cond_1

    :try_start_0
    iget-char v5, v4, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->value:C
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v5, p0, :cond_0

    return-object v4

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_1
    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->b:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;
    .locals 1

    const-class v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    return-object p0
.end method

.method public static values()[Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;
    .locals 1

    sget-object v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->a:[Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    invoke-virtual {v0}, [Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    return-object v0
.end method
