.class public Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final codec:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/scp/protocol/messages/Message;",
            ">;"
        }
    .end annotation
.end field

.field public final messageClass:Ljava/lang/Class;

.field public final messageId:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xf

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x58

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\u0016mf-\u001d)1](H$\u000f)#\u0007\u0011\u007f#\u007f:\u0017z+W(x;\u000f=5s)6\u0008\u0016mh\'\n?3\u0007"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x2a

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/4 v13, 0x2

    if-eqz v12, :cond_6

    if-eq v12, v7, :cond_5

    if-eq v12, v13, :cond_4

    if-eq v12, v0, :cond_3

    const/4 v14, 0x4

    if-eq v12, v14, :cond_2

    const/4 v14, 0x5

    if-eq v12, v14, :cond_7

    const/16 v13, 0x8

    goto :goto_2

    :cond_2
    const/16 v13, 0x36

    goto :goto_2

    :cond_3
    const/16 v13, 0x10

    goto :goto_2

    :cond_4
    const/16 v13, 0x53

    goto :goto_2

    :cond_5
    const/16 v13, 0x15

    goto :goto_2

    :cond_6
    const/16 v13, 0x62

    :cond_7
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class;",
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/scp/protocol/messages/Message;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;->messageId:I

    iput-object p2, p0, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;->messageClass:Ljava/lang/Class;

    iput-object p3, p0, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;->codec:Lcom/jscape/util/h/I;

    return-void
.end method

.method public static entry(ILjava/lang/Class;Lcom/jscape/util/h/I;)Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class;",
            "Lcom/jscape/util/h/I<",
            "Lcom/jscape/inet/scp/protocol/messages/Message;",
            ">;)",
            "Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;

    invoke-direct {v0, p0, p1, p2}, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;-><init>(ILjava/lang/Class;Lcom/jscape/util/h/I;)V

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;->messageId:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;->messageClass:Ljava/lang/Class;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;->codec:Lcom/jscape/util/h/I;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
