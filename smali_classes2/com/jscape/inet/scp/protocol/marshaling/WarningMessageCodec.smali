.class public Lcom/jscape/inet/scp/protocol/marshaling/WarningMessageCodec;
.super Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/scp/protocol/messages/Message;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/inet/scp/protocol/marshaling/WarningMessageCodec;->readLine(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Lcom/jscape/inet/scp/protocol/messages/WarningMessage;

    invoke-direct {v0, p1}, Lcom/jscape/inet/scp/protocol/messages/WarningMessage;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/scp/protocol/marshaling/WarningMessageCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/scp/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/jscape/inet/scp/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/scp/protocol/messages/WarningMessage;

    iget-object p1, p1, Lcom/jscape/inet/scp/protocol/messages/WarningMessage;->message:Ljava/lang/String;

    sget-object v0, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write([B)V

    const/16 p1, 0xa

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/scp/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/scp/protocol/marshaling/WarningMessageCodec;->write(Lcom/jscape/inet/scp/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
