.class public Lcom/jscape/inet/scp/protocol/marshaling/OkMessageCodec;
.super Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/scp/protocol/messages/Message;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance p1, Lcom/jscape/inet/scp/protocol/messages/OkMessage;

    invoke-direct {p1}, Lcom/jscape/inet/scp/protocol/messages/OkMessage;-><init>()V

    return-object p1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/scp/protocol/marshaling/OkMessageCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/scp/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/jscape/inet/scp/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/scp/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/scp/protocol/marshaling/OkMessageCodec;->write(Lcom/jscape/inet/scp/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
