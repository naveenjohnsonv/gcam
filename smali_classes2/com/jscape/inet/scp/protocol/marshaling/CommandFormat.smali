.class public Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x1a

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x18

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\u0003z1P\u0018f2$`\'AHu2;y#K\u000c61?z\'\u001fH\u0015\u0003z1P\u0018f2$`\'AHu2;y#K\u000c,}"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x30

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x45

    goto :goto_2

    :cond_2
    const/16 v12, 0xe

    goto :goto_2

    :cond_3
    const/16 v12, 0x70

    goto :goto_2

    :cond_4
    const/16 v12, 0x3d

    goto :goto_2

    :cond_5
    const/16 v12, 0x5a

    goto :goto_2

    :cond_6
    const/16 v12, 0xc

    goto :goto_2

    :cond_7
    const/16 v12, 0x4e

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/jscape/inet/scp/protocol/messages/Command;)Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    :try_start_0
    instance-of v1, p0, Lcom/jscape/inet/scp/protocol/messages/ReadFileCommand;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-nez v0, :cond_2

    if-eqz v1, :cond_1

    check-cast p0, Lcom/jscape/inet/scp/protocol/messages/ReadFileCommand;

    :try_start_1
    new-instance v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;

    iget-object v1, p0, Lcom/jscape/inet/scp/protocol/messages/ReadFileCommand;->path:Ljava/lang/String;

    new-array v4, v4, [Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    sget-object v6, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->READ:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    aput-object v6, v4, v3

    iget-boolean p0, p0, Lcom/jscape/inet/scp/protocol/messages/ReadFileCommand;->preserveAttributes:Z

    if-eqz p0, :cond_0

    sget-object v2, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->PRESERVE_FILE_ATTRIBUTES:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    aput-object v2, v4, v5

    invoke-direct {v0, v1, v4}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;-><init>(Ljava/lang/String;[Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;)V

    return-object v0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_1
    instance-of v1, p0, Lcom/jscape/inet/scp/protocol/messages/ReadFilesCommand;

    :cond_2
    if-nez v0, :cond_5

    if-eqz v1, :cond_4

    check-cast p0, Lcom/jscape/inet/scp/protocol/messages/ReadFilesCommand;

    :try_start_2
    new-instance v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;

    iget-object v1, p0, Lcom/jscape/inet/scp/protocol/messages/ReadFilesCommand;->path:Ljava/lang/String;

    new-array v4, v4, [Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    sget-object v6, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->READ:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    aput-object v6, v4, v3

    iget-boolean p0, p0, Lcom/jscape/inet/scp/protocol/messages/ReadFilesCommand;->preserveAttributes:Z

    if-eqz p0, :cond_3

    sget-object v2, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->PRESERVE_FILE_ATTRIBUTES:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_3
    aput-object v2, v4, v5

    invoke-direct {v0, v1, v4}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;-><init>(Ljava/lang/String;[Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;)V

    return-object v0

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_4
    instance-of v1, p0, Lcom/jscape/inet/scp/protocol/messages/ReadDirectoryCommand;

    :cond_5
    const/4 v6, 0x3

    if-nez v0, :cond_8

    if-eqz v1, :cond_7

    check-cast p0, Lcom/jscape/inet/scp/protocol/messages/ReadDirectoryCommand;

    :try_start_3
    new-instance v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;

    iget-object v1, p0, Lcom/jscape/inet/scp/protocol/messages/ReadDirectoryCommand;->path:Ljava/lang/String;

    new-array v6, v6, [Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    sget-object v7, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->READ:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    aput-object v7, v6, v3

    sget-object v3, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->RECURSIVE:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    aput-object v3, v6, v5

    iget-boolean p0, p0, Lcom/jscape/inet/scp/protocol/messages/ReadDirectoryCommand;->preserveAttributes:Z

    if-eqz p0, :cond_6

    sget-object v2, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->PRESERVE_FILE_ATTRIBUTES:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_6
    aput-object v2, v6, v4

    invoke-direct {v0, v1, v6}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;-><init>(Ljava/lang/String;[Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;)V

    return-object v0

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_7
    instance-of v1, p0, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;

    :cond_8
    if-nez v0, :cond_b

    if-eqz v1, :cond_a

    check-cast p0, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;

    :try_start_4
    new-instance v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;

    iget-object v1, p0, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;->path:Ljava/lang/String;

    new-array v4, v4, [Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    sget-object v6, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->WRITE:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    aput-object v6, v4, v3

    iget-boolean p0, p0, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;->preserveAttributes:Z

    if-eqz p0, :cond_9

    sget-object v2, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->PRESERVE_FILE_ATTRIBUTES:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_9
    aput-object v2, v4, v5

    invoke-direct {v0, v1, v4}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;-><init>(Ljava/lang/String;[Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;)V

    return-object v0

    :catch_3
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_a
    if-nez v0, :cond_c

    :try_start_5
    instance-of v1, p0, Lcom/jscape/inet/scp/protocol/messages/WriteDirectoryCommand;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_0

    :catch_4
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_b
    :goto_0
    if-eqz v1, :cond_e

    :cond_c
    check-cast p0, Lcom/jscape/inet/scp/protocol/messages/WriteDirectoryCommand;

    :try_start_6
    new-instance v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;

    iget-object v1, p0, Lcom/jscape/inet/scp/protocol/messages/WriteDirectoryCommand;->path:Ljava/lang/String;

    new-array v6, v6, [Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    sget-object v7, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->WRITE:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    aput-object v7, v6, v3

    sget-object v3, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->RECURSIVE:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    aput-object v3, v6, v5

    iget-boolean p0, p0, Lcom/jscape/inet/scp/protocol/messages/WriteDirectoryCommand;->preserveAttributes:Z

    if-eqz p0, :cond_d

    sget-object v2, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->PRESERVE_FILE_ATTRIBUTES:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    :cond_d
    aput-object v2, v6, v4

    invoke-direct {v0, v1, v6}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;-><init>(Ljava/lang/String;[Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;)V

    return-object v0

    :catch_5
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_e
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_6
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method public static format(Lcom/jscape/inet/scp/protocol/messages/Command;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a(Lcom/jscape/inet/scp/protocol/messages/Command;)Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;

    move-result-object p0

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->format(Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;Z)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static parse(Ljava/lang/String;)Lcom/jscape/inet/scp/protocol/messages/Command;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->parse(Ljava/lang/String;)Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;

    move-result-object p0

    invoke-static {}, Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    sget-object v1, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->PRESERVE_FILE_ATTRIBUTES:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    invoke-virtual {p0, v1}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;->contains(Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;)Z

    move-result v1

    :try_start_0
    sget-object v2, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->READ:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    invoke-virtual {p0, v2}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;->contains(Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;)Z

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    if-nez v0, :cond_4

    if-eqz v2, :cond_3

    :try_start_1
    iget-object v2, p0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;->path:Ljava/lang/String;

    invoke-static {v2}, Lcom/jscape/util/aw;->a(Ljava/lang/String;)Z

    move-result v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    if-nez v0, :cond_1

    if-eqz v2, :cond_0

    :try_start_2
    new-instance v0, Lcom/jscape/inet/scp/protocol/messages/ReadFilesCommand;

    iget-object p0, p0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;->path:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/jscape/inet/scp/protocol/messages/ReadFilesCommand;-><init>(Ljava/lang/String;Z)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->RECURSIVE:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    invoke-virtual {p0, v0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;->contains(Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;)Z

    move-result v2

    :cond_1
    if-eqz v2, :cond_2

    :try_start_3
    new-instance v0, Lcom/jscape/inet/scp/protocol/messages/ReadDirectoryCommand;

    iget-object p0, p0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;->path:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/jscape/inet/scp/protocol/messages/ReadDirectoryCommand;-><init>(Ljava/lang/String;Z)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_2
    new-instance v0, Lcom/jscape/inet/scp/protocol/messages/ReadFileCommand;

    iget-object p0, p0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;->path:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/jscape/inet/scp/protocol/messages/ReadFileCommand;-><init>(Ljava/lang/String;Z)V

    :goto_0
    return-object v0

    :cond_3
    sget-object v2, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->WRITE:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    invoke-virtual {p0, v2}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;->contains(Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;)Z

    move-result v2

    :cond_4
    if-nez v0, :cond_6

    if-eqz v2, :cond_5

    :try_start_4
    sget-object v2, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->RECURSIVE:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    invoke-virtual {p0, v2}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;->contains(Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;)Z

    move-result v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_5
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    :goto_1
    if-nez v0, :cond_7

    if-nez v2, :cond_8

    :try_start_5
    sget-object v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->TARGET_DIRECTORY:Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    invoke-virtual {p0, v0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;->contains(Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;)Z

    move-result v2
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_7
    :goto_2
    if-eqz v2, :cond_9

    :cond_8
    :try_start_6
    new-instance v0, Lcom/jscape/inet/scp/protocol/messages/WriteDirectoryCommand;

    iget-object p0, p0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;->path:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/jscape/inet/scp/protocol/messages/WriteDirectoryCommand;-><init>(Ljava/lang/String;Z)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_3

    :catch_3
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_9
    new-instance v0, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;

    iget-object p0, p0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;->path:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;-><init>(Ljava/lang/String;Z)V

    :goto_3
    return-object v0

    :catch_4
    move-exception p0

    :try_start_7
    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    :catch_5
    move-exception p0

    :try_start_8
    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    :catch_6
    move-exception p0

    :try_start_9
    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    :catch_7
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method
