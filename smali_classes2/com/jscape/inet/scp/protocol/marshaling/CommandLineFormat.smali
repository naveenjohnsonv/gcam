.class public Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "\u0016%\u000eYr\u0004_ `\u0010\\o\u001fU*3_Bt\u0002\u001a42\u001a_~\u0018Nj\u0011\u0014!\u000bD;\u0018U0`\u000f^~\u0005_*4Q\t7#\u000f\u000c>\u0005\u001aa3\u0002dm\u00037#\u000f"

    const/16 v4, 0x40

    const/16 v5, 0x1d

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x60

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x33

    const/16 v3, 0x14

    const-string v5, "HwC\r2J\u0000i|BU!@\u000epxH\u0011x\u000f\u001eHwC\r2J\u0000i|BU!@\u000epxH\u0011bC\ns|\u0006\u0005#]\u0017\'9"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x39

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x5a

    goto :goto_4

    :cond_4
    const/16 v1, 0x16

    goto :goto_4

    :cond_5
    const/16 v1, 0x7b

    goto :goto_4

    :cond_6
    const/16 v1, 0x4c

    goto :goto_4

    :cond_7
    const/16 v1, 0x1f

    goto :goto_4

    :cond_8
    const/16 v1, 0x20

    goto :goto_4

    :cond_9
    const/16 v1, 0x24

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private static a(Ljava/io/StringReader;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    invoke-virtual {p0}, Ljava/io/StringReader;->read()I

    move-result v2

    :try_start_0
    invoke-static {v2}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(I)Z

    move-result v3

    if-nez v3, :cond_0

    int-to-char v3, v2

    invoke-static {v3}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v0, :cond_1

    if-nez v0, :cond_1

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {v2}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(I)Z

    move-result v3

    :cond_1
    const/4 v4, 0x0

    if-nez v0, :cond_2

    if-eqz v3, :cond_5

    return-object v4

    :cond_2
    :goto_1
    if-nez v3, :cond_3

    int-to-char v3, v2

    :try_start_1
    invoke-static {v3}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_3
    :goto_2
    if-nez v0, :cond_6

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    if-nez v0, :cond_4

    if-lez v3, :cond_7

    goto :goto_4

    :cond_4
    :goto_3
    if-eqz v3, :cond_5

    goto :goto_2

    :cond_5
    int-to-char v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/io/StringReader;->read()I

    move-result v2

    invoke-static {v2}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(I)Z

    move-result v3

    goto :goto_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_6
    :goto_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_7
    return-object v4

    :catch_2
    move-exception p0

    :try_start_3
    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception p0

    :try_start_4
    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method

.method private static a(Ljava/lang/String;Ljava/io/StringReader;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;->b()[Lcom/jscape/util/aq;

    move-result-object p0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p1}, Ljava/io/StringReader;->read()I

    move-result v1

    invoke-static {v1}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(I)Z

    move-result v2

    if-nez v2, :cond_1

    int-to-char v1, v1

    :try_start_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p0, :cond_1

    if-eqz p0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    const-string p1, "\""

    invoke-static {p0, p1, p1}, Lcom/jscape/util/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static a(Ljava/util/List;Z)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;",
            ">;Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;->b()[Lcom/jscape/util/aq;

    move-result-object v1

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v2, 0x0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    if-nez v1, :cond_2

    add-int/lit8 v4, v2, 0x1

    if-lez v2, :cond_1

    if-eqz p1, :cond_1

    sget-object v2, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a:[Ljava/lang/String;

    const/4 v5, 0x3

    aget-object v2, v2, v5

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-char v2, v3, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->value:C

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v2, v4

    :cond_2
    if-eqz v1, :cond_0

    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static a(Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    :try_start_0
    invoke-static {v2}, Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;->optionFor(C)Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    add-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/jscape/inet/scp/protocol/marshaling/CommandOption;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_0

    return-void

    :cond_0
    new-instance p0, Ljava/io/IOException;

    sget-object v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method

.method private static a(I)Z
    .locals 1

    invoke-static {}, Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :cond_1
    :goto_0
    return p0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "-"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method private static b(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    sget-object v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method

.method private static c(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p0, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance p0, Ljava/io/IOException;

    sget-object v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method

.method private static d(Ljava/lang/String;)Z
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, " "

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static format(Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;Z)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;->options:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/util/List;Z)Ljava/lang/String;

    move-result-object p1

    iget-object p0, p0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;->path:Ljava/lang/String;

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    sget-object v0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 p1, 0x1

    aput-object p0, v1, p1

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static parse(Ljava/lang/String;)Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;->b()[Lcom/jscape/util/aq;

    move-result-object p0

    invoke-static {v0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/io/StringReader;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->b(Ljava/lang/String;)V

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    const/4 v2, 0x0

    :cond_0
    invoke-static {v0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/io/StringReader;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->d(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    if-nez p0, :cond_6

    if-nez p0, :cond_1

    :try_start_0
    invoke-static {v3}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/lang/String;)Z

    move-result v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v4, :cond_2

    :try_start_1
    invoke-static {v3, v1}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/lang/String;Ljava/util/List;)V

    if-eqz p0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_1
    move-object v2, v3

    :cond_2
    :goto_0
    if-nez p0, :cond_4

    if-nez v2, :cond_3

    invoke-static {v3, v0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/lang/String;Ljava/io/StringReader;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_3
    :try_start_2
    new-instance p0, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_4
    :goto_1
    if-eqz p0, :cond_0

    :cond_5
    invoke-static {v1}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLineFormat;->a(Ljava/util/List;)V

    move-object v3, v2

    :cond_6
    if-nez p0, :cond_7

    if-nez v3, :cond_8

    const-string p0, ""

    move-object v2, p0

    goto :goto_2

    :cond_7
    move-object v2, v3

    :cond_8
    :goto_2
    new-instance p0, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;

    invoke-direct {p0, v2, v1}, Lcom/jscape/inet/scp/protocol/marshaling/CommandLine;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object p0
.end method
