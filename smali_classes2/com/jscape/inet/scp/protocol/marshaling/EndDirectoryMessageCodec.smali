.class public Lcom/jscape/inet/scp/protocol/marshaling/EndDirectoryMessageCodec;
.super Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "e\u0014B}R4YE\u0007T{Ns\u000bM\u0003CaAsN\u000e"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/scp/protocol/marshaling/EndDirectoryMessageCodec;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v5, 0x61

    if-eqz v4, :cond_5

    const/4 v6, 0x1

    if-eq v4, v6, :cond_4

    const/4 v6, 0x2

    if-eq v4, v6, :cond_3

    const/4 v6, 0x3

    if-eq v4, v6, :cond_2

    const/4 v6, 0x4

    if-eq v4, v6, :cond_5

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v5, 0x6a

    goto :goto_1

    :cond_1
    const/16 v5, 0x55

    goto :goto_1

    :cond_2
    const/16 v5, 0x53

    goto :goto_1

    :cond_3
    const/16 v5, 0x71

    goto :goto_1

    :cond_4
    const/16 v5, 0x27

    :cond_5
    :goto_1
    const/16 v4, 0x41

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/scp/protocol/marshaling/CodecBase;-><init>()V

    return-void
.end method

.method private static b(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public read(Ljava/io/InputStream;)Lcom/jscape/inet/scp/protocol/messages/Message;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/h/a/c;->a(Ljava/io/InputStream;)B

    move-result p1

    and-int/lit16 p1, p1, 0xff

    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    new-instance p1, Lcom/jscape/inet/scp/protocol/messages/EndDirectoryMessage;

    invoke-direct {p1}, Lcom/jscape/inet/scp/protocol/messages/EndDirectoryMessage;-><init>()V

    return-object p1

    :cond_0
    :try_start_0
    new-instance p1, Ljava/io/IOException;

    sget-object v0, Lcom/jscape/inet/scp/protocol/marshaling/EndDirectoryMessageCodec;->a:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/protocol/marshaling/EndDirectoryMessageCodec;->b(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/inet/scp/protocol/marshaling/EndDirectoryMessageCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/scp/protocol/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/jscape/inet/scp/protocol/messages/Message;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 p1, 0xa

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/jscape/inet/scp/protocol/messages/Message;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/scp/protocol/marshaling/EndDirectoryMessageCodec;->write(Lcom/jscape/inet/scp/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method
