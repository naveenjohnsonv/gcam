.class public Lcom/jscape/inet/scp/protocol/messages/EndDirectoryMessage;
.super Lcom/jscape/inet/scp/protocol/messages/Message;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "9[\u0001\u0000q\u0015\"\u001fA\n6a*\"\u000fF\u0004#}G<\u0001"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/scp/protocol/messages/EndDirectoryMessage;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x14

    goto :goto_1

    :cond_1
    const/16 v4, 0x34

    goto :goto_1

    :cond_2
    const/16 v4, 0x4b

    goto :goto_1

    :cond_3
    const/16 v4, 0x17

    goto :goto_1

    :cond_4
    const/16 v4, 0x36

    goto :goto_1

    :cond_5
    const/16 v4, 0x66

    goto :goto_1

    :cond_6
    const/16 v4, 0x2f

    :goto_1
    const/16 v5, 0x53

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/scp/protocol/messages/Message;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/inet/scp/protocol/messages/EndDirectoryMessage;->a:Ljava/lang/String;

    return-object v0
.end method
