.class public Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;
.super Lcom/jscape/inet/scp/protocol/messages/Command;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/inet/scp/protocol/messages/Command<",
        "Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand$Handler;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final path:Ljava/lang/String;

.field public final preserveAttributes:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/16 v2, 0x18

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v2

    move v6, v3

    :goto_0
    const/16 v7, 0x4e

    const/4 v8, 0x1

    add-int/2addr v4, v8

    add-int/2addr v5, v4

    const-string v9, "[$d\u0016wJ:`3N\r\u007fa2b2-\u0019bm\'dk*\u0015 v}\u0010w\u007f6~ h#fx!e4x\u0016w\u007fn"

    invoke-virtual {v9, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v10, v4

    move v11, v3

    :goto_1
    if-gt v10, v11, :cond_1

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v7, v6, 0x1

    aput-object v4, v1, v6

    const/16 v4, 0x2e

    if-ge v5, v4, :cond_0

    invoke-virtual {v9, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v6, v7

    move/from16 v16, v5

    move v5, v4

    move/from16 v4, v16

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v12, v4, v11

    rem-int/lit8 v13, v11, 0x7

    const/16 v14, 0x42

    if-eqz v13, :cond_6

    if-eq v13, v8, :cond_5

    if-eq v13, v0, :cond_4

    const/4 v15, 0x3

    if-eq v13, v15, :cond_3

    const/4 v15, 0x4

    if-eq v13, v15, :cond_2

    const/4 v15, 0x5

    if-eq v13, v15, :cond_6

    const/16 v14, 0x1d

    goto :goto_2

    :cond_2
    const/16 v14, 0x5c

    goto :goto_2

    :cond_3
    const/16 v14, 0x2c

    goto :goto_2

    :cond_4
    const/16 v14, 0x43

    goto :goto_2

    :cond_5
    move v14, v2

    :cond_6
    :goto_2
    xor-int v13, v7, v14

    xor-int/2addr v12, v13

    int-to-char v12, v12

    aput-char v12, v4, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/scp/protocol/messages/Command;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;->path:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;->preserveAttributes:Z

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Lcom/jscape/inet/scp/protocol/messages/Command$HandlerBase;Lcom/jscape/util/k/a/C;)V
    .locals 0

    check-cast p1, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand$Handler;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;->accept(Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand$Handler;Lcom/jscape/util/k/a/C;)V

    return-void
.end method

.method public accept(Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand$Handler;Lcom/jscape/util/k/a/C;)V
    .locals 0

    invoke-interface {p1, p0, p2}, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand$Handler;->handle(Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;Lcom/jscape/util/k/a/C;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;->a:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;->path:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;->preserveAttributes:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
