.class public interface abstract Lcom/jscape/inet/scp/FileService$TransferListener;
.super Ljava/lang/Object;


# static fields
.field public static final NULL:Lcom/jscape/inet/scp/FileService$TransferListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jscape/inet/scp/FileService$TransferListener$1;

    invoke-direct {v0}, Lcom/jscape/inet/scp/FileService$TransferListener$1;-><init>()V

    sput-object v0, Lcom/jscape/inet/scp/FileService$TransferListener;->NULL:Lcom/jscape/inet/scp/FileService$TransferListener;

    return-void
.end method


# virtual methods
.method public abstract onTransferCompleted(J)V
.end method

.method public abstract onTransferProgress(J)V
.end method

.method public abstract onTransferStarted(Ljava/lang/String;J)V
.end method
