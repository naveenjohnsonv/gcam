.class public Lcom/jscape/inet/scp/Scp;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/AutoCloseable;


# static fields
.field public static final DEFAULT_BUFFER_SIZE:I = 0x20000

.field public static final DEFAULT_LOCAL_DIRECTORY:Ljava/io/File;

.field public static final DEFAULT_PRESERVE_FILE_ATTRIBUTES:Z = true

.field private static final a:Lcom/jscape/util/m/g;

.field private static final k:[Ljava/lang/String;


# instance fields
.field private b:Lcom/jscape/inet/ssh/util/SshParameters;

.field private c:Lcom/jscape/inet/ssh/SshConfiguration;

.field private d:Ljava/nio/file/Path;

.field private e:I

.field private f:Z

.field private final g:Ljava/util/logging/Logger;

.field private final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/jscape/inet/scp/events/ScpEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/jscape/inet/ssh/util/SshHostKeys;

.field private volatile j:Lcom/jscape/inet/scp/FileService;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "?C@\tb\u007fRVAY\u000boz\u0016\u0012DD\rmbY\u0004T\u0018\u001f4LRHzdW\u0018^P\r|6T\u0003KP\r|6E\u001fWSHxwZ\u0003H\u0018\u0010%NFHufW\u0004L[\rzsD\u0005\u0010\u0010Z\rU\u0007`p_\u0011XD\tz\u007fY\u0018\u0010$X\u0007m\u0018^Km\u0017lk3}Ek-^e5Uaa+vY\'SMD$pm\u000cJK\u0018\\\u0018?C@\tb\u007fRVAY\u000boz\u0016\u0012DD\rmbY\u0004T\u0018\u001aZ\rB\u001aoxE\u0010HD*{pP\u0013_e\u0001tst\u000fYS\u001b3#Z\rP\u0001bsw\u0002YD\u0001lcB\u0013^f\u001akeS\u0004[_\u0006iDS\u0007X_\u001akr\u000b\u001f4LRHzdW\u0018^P\r|6T\u0003KP\r|6E\u001fWSHxwZ\u0003H\u0018"

    const/16 v5, 0xf7

    move v8, v3

    const/4 v6, -0x1

    const/16 v7, 0x18

    :goto_0
    const/16 v9, 0x78

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x3d

    const-string v4, "\u001dJ\u0012Z;#\u0014_\u001e=@*0\u001du\u0003\u0003J*%\u001eC\u0013L$\u001f@*_\u0019\u000c*P+,t:\u0002,j\u0019\"r\u0012&&l1\u001e`\u0014\n\u0003c7*K\r\u000c_\u001b"

    move v8, v11

    const/4 v6, -0x1

    const/16 v7, 0x18

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x3f

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/scp/Scp;->k:[Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    const-string v1, "."

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/jscape/inet/scp/Scp;->DEFAULT_LOCAL_DIRECTORY:Ljava/io/File;

    new-instance v0, Lcom/jscape/util/m/h;

    invoke-direct {v0}, Lcom/jscape/util/m/h;-><init>()V

    sput-object v0, Lcom/jscape/inet/scp/Scp;->a:Lcom/jscape/util/m/g;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v1, v14, 0x7

    const/16 v16, 0x4e

    if-eqz v1, :cond_8

    if-eq v1, v10, :cond_7

    const/4 v2, 0x2

    if-eq v1, v2, :cond_9

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    goto :goto_4

    :cond_4
    const/16 v16, 0x6e

    goto :goto_4

    :cond_5
    const/16 v16, 0x76

    goto :goto_4

    :cond_6
    const/16 v16, 0x10

    goto :goto_4

    :cond_7
    const/16 v16, 0x55

    goto :goto_4

    :cond_8
    const/16 v16, 0xe

    :cond_9
    :goto_4
    xor-int v1, v9, v16

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/util/SshParameters;)V
    .locals 1

    invoke-static {}, Lcom/jscape/inet/ssh/SshConfiguration;->defaultClientConfiguration()Lcom/jscape/inet/ssh/SshConfiguration;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/jscape/inet/scp/Scp;-><init>(Lcom/jscape/inet/ssh/util/SshParameters;Lcom/jscape/inet/ssh/SshConfiguration;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/util/SshParameters;Lcom/jscape/inet/ssh/SshConfiguration;)V
    .locals 7

    sget-object v3, Lcom/jscape/inet/scp/Scp;->DEFAULT_LOCAL_DIRECTORY:Ljava/io/File;

    sget-object v0, Ljava/util/logging/Level;->OFF:Ljava/util/logging/Level;

    invoke-static {v0}, Lcom/jscape/util/j/b;->a(Ljava/util/logging/Level;)Lcom/jscape/util/j/b;

    move-result-object v6

    const/high16 v4, 0x20000

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/scp/Scp;-><init>(Lcom/jscape/inet/ssh/util/SshParameters;Lcom/jscape/inet/ssh/SshConfiguration;Ljava/io/File;IZLjava/util/logging/Logger;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/util/SshParameters;Lcom/jscape/inet/ssh/SshConfiguration;Ljava/io/File;IZLjava/util/logging/Logger;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0}, Lcom/jscape/inet/scp/Scp;->a()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/inet/scp/Scp;->c:Lcom/jscape/inet/ssh/SshConfiguration;

    invoke-virtual {p3}, Ljava/io/File;->isDirectory()Z

    move-result p1

    sget-object p2, Lcom/jscape/inet/scp/Scp;->k:[Ljava/lang/String;

    const/4 v0, 0x0

    aget-object v1, p2, v0

    invoke-static {p1, v1}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    invoke-virtual {p3}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p1

    invoke-interface {p1}, Ljava/nio/file/Path;->normalize()Ljava/nio/file/Path;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/scp/Scp;->d:Ljava/nio/file/Path;

    int-to-long v1, p4

    const/4 p1, 0x1

    aget-object p1, p2, p1

    const-wide/16 p2, 0x0

    invoke-static {v1, v2, p2, p3, p1}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    iput p4, p0, Lcom/jscape/inet/scp/Scp;->e:I

    iput-boolean p5, p0, Lcom/jscape/inet/scp/Scp;->f:Z

    :try_start_0
    invoke-static {p6}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p6, p0, Lcom/jscape/inet/scp/Scp;->g:Ljava/util/logging/Logger;

    new-instance p1, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {p1}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object p1, p0, Lcom/jscape/inet/scp/Scp;->h:Ljava/util/Set;

    new-instance p1, Lcom/jscape/inet/ssh/util/SshHostKeys;

    new-array p2, v0, [Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;

    invoke-direct {p1, p2}, Lcom/jscape/inet/ssh/util/SshHostKeys;-><init>([Lcom/jscape/inet/ssh/util/SshHostKeys$Entry;)V

    iput-object p1, p0, Lcom/jscape/inet/scp/Scp;->i:Lcom/jscape/inet/ssh/util/SshHostKeys;

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x5

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/inet/scp/ScpException;->b([I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method public constructor <init>(Lcom/jscape/inet/ssh/util/SshParameters;Ljava/util/logging/Logger;)V
    .locals 7

    invoke-static {}, Lcom/jscape/inet/ssh/SshConfiguration;->defaultClientConfiguration()Lcom/jscape/inet/ssh/SshConfiguration;

    move-result-object v2

    sget-object v3, Lcom/jscape/inet/scp/Scp;->DEFAULT_LOCAL_DIRECTORY:Ljava/io/File;

    const/high16 v4, 0x20000

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/jscape/inet/scp/Scp;-><init>(Lcom/jscape/inet/ssh/util/SshParameters;Lcom/jscape/inet/ssh/SshConfiguration;Ljava/io/File;IZLjava/util/logging/Logger;)V

    return-void
.end method

.method private a(Ljava/lang/Throwable;)Lcom/jscape/inet/scp/ScpException;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    :try_start_1
    instance-of v0, v1, Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication$InvalidCredentialsException;

    if-eqz v0, :cond_1

    new-instance p1, Lcom/jscape/inet/scp/ScpAuthenticationException;

    invoke-direct {p1}, Lcom/jscape/inet/scp/ScpAuthenticationException;-><init>()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/jscape/inet/scp/ScpException;

    invoke-direct {v0, p1}, Lcom/jscape/inet/scp/ScpException;-><init>(Ljava/lang/Throwable;)V

    move-object p1, v0

    :goto_1
    return-object p1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method private a(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/scp/Scp;->c(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p1

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    new-instance v1, Lcom/jscape/inet/scp/Scp$1;

    invoke-direct {v1, p0, p1}, Lcom/jscape/inet/scp/Scp$1;-><init>(Lcom/jscape/inet/scp/Scp;Lcom/jscape/util/m/f;)V

    iget-object p1, p0, Lcom/jscape/inet/scp/Scp;->d:Ljava/nio/file/Path;

    invoke-static {p1, v1}, Ljava/nio/file/Files;->newDirectoryStream(Ljava/nio/file/Path;Ljava/nio/file/DirectoryStream$Filter;)Ljava/nio/file/DirectoryStream;

    move-result-object p1

    :try_start_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {p1}, Ljava/nio/file/DirectoryStream;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/nio/file/Path;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-eqz v0, :cond_1

    :try_start_1
    invoke-interface {v3}, Ljava/nio/file/Path;->getFileName()Ljava/nio/file/Path;

    move-result-object v3

    invoke-interface {v3}, Ljava/nio/file/Path;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_0

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_2
    invoke-static {v1}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    :try_start_3
    invoke-interface {p1}, Ljava/nio/file/DirectoryStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    return-object v1

    :catchall_2
    move-exception v1

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :catchall_3
    move-exception v2

    if-eqz p1, :cond_3

    :try_start_5
    invoke-interface {p1}, Ljava/nio/file/DirectoryStream;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    goto :goto_2

    :catchall_4
    move-exception v3

    :try_start_6
    invoke-virtual {v1, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    if-nez v0, :cond_3

    invoke-interface {p1}, Ljava/nio/file/DirectoryStream;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    goto :goto_2

    :catchall_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_3
    :goto_2
    throw v2
.end method

.method private a()V
    .locals 2

    new-instance v0, Lcom/jscape/util/d;

    invoke-direct {v0}, Lcom/jscape/util/d;-><init>()V

    :try_start_0
    invoke-virtual {v0}, Lcom/jscape/util/d;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Lcom/jscape/util/d;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method static a(Lcom/jscape/inet/scp/Scp;Lcom/jscape/inet/scp/events/ScpEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/inet/scp/Scp;->a(Lcom/jscape/inet/scp/events/ScpEvent;)V

    return-void
.end method

.method private a(Lcom/jscape/inet/scp/events/ScpEvent;)V
    .locals 3

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->h:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/inet/scp/events/ScpEventListener;

    invoke-virtual {p1, v2}, Lcom/jscape/inet/scp/events/ScpEvent;->accept(Lcom/jscape/inet/scp/events/ScpEventListener;)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->d:Ljava/nio/file/Path;

    invoke-interface {v0, p1}, Ljava/nio/file/Path;->resolve(Ljava/lang/String;)Ljava/nio/file/Path;

    move-result-object p1

    invoke-interface {p1}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object p1

    return-object p1
.end method

.method private static b(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    return-object p0
.end method

.method private c(Ljava/lang/String;)Lcom/jscape/util/m/f;
    .locals 1

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/at;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jscape/inet/scp/Scp;->a:Lcom/jscape/util/m/g;

    invoke-interface {v0, p1}, Lcom/jscape/util/m/g;->a(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    :try_start_1
    sget-object p1, Lcom/jscape/util/m/f;->a:Lcom/jscape/util/m/f;

    :goto_0
    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method


# virtual methods
.method public addListener(Lcom/jscape/inet/scp/events/ScpEventListener;)V
    .locals 1

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected authentication()Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;
    .locals 9

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getClientAuthentication()Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_8

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/SshParameters;->getClientAuthentication()Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_9

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    :cond_1
    :try_start_2
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getPassword()Ljava/lang/String;

    move-result-object v1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_4

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v0, :cond_4

    if-eqz v1, :cond_2

    :try_start_3
    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_5

    if-eqz v0, :cond_3

    :try_start_4
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getKeyPair()Ljava/security/KeyPair;

    move-result-object v1
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_6

    if-eqz v1, :cond_2

    :try_start_5
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    iget-object v5, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/util/SshParameters;->getUsername()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v6}, Lcom/jscape/inet/ssh/util/SshParameters;->getKeyPair()Ljava/security/KeyPair;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->defaultAuthentication(Ljava/lang/String;Ljava/security/KeyPair;)Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;

    move-result-object v5

    aput-object v5, v1, v4

    iget-object v5, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/util/SshParameters;->getUsername()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v6}, Lcom/jscape/inet/ssh/util/SshParameters;->getPassword()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->defaultAuthentication(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;

    move-result-object v5

    aput-object v5, v1, v3

    iget-object v5, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/util/SshParameters;->getUsername()Ljava/lang/String;

    move-result-object v5

    new-array v3, v3, [Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;

    sget-object v7, Lcom/jscape/inet/scp/Scp;->k:[Ljava/lang/String;

    const/4 v8, 0x4

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v8}, Lcom/jscape/inet/ssh/util/SshParameters;->getPassword()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v3, v4

    invoke-static {v5, v3}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;->defaultAuthentication(Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;)Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;-><init>([Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_7

    return-object v0

    :cond_2
    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    :cond_3
    if-eqz v0, :cond_6

    :try_start_6
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getPassword()Ljava/lang/String;

    move-result-object v1
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_4
    :goto_0
    if-eqz v1, :cond_5

    :try_start_7
    new-instance v0, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;

    new-array v1, v2, [Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getUsername()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/util/SshParameters;->getPassword()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;->defaultAuthentication(Ljava/lang/String;Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/authentication/PasswordClientAuthentication;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getUsername()Ljava/lang/String;

    move-result-object v2

    new-array v5, v3, [Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;

    new-instance v6, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;

    sget-object v7, Lcom/jscape/inet/scp/Scp;->k:[Ljava/lang/String;

    const/16 v8, 0xa

    aget-object v7, v7, v8

    iget-object v8, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v8}, Lcom/jscape/inet/ssh/util/SshParameters;->getPassword()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v5, v4

    invoke-static {v2, v5}, Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;->defaultAuthentication(Ljava/lang/String;[Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication$Entry;)Lcom/jscape/inet/ssh/protocol/v2/authentication/KeyboardInteractiveRegexClientAuthentication;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-direct {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/ComponentClientAuthentication;-><init>([Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;)V
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_1

    return-object v0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_5
    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    :cond_6
    if-eqz v0, :cond_8

    :try_start_8
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getKeyPair()Ljava/security/KeyPair;

    move-result-object v0
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_2

    if-eqz v0, :cond_7

    :try_start_9
    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/SshParameters;->getUsername()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getKeyPair()Ljava/security/KeyPair;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;->defaultAuthentication(Ljava/lang/String;Ljava/security/KeyPair;)Lcom/jscape/inet/ssh/protocol/v2/authentication/PublicKeyClientAuthentication;

    move-result-object v0
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_3

    return-object v0

    :cond_7
    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    goto :goto_1

    :catch_2
    move-exception v0

    :try_start_a
    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_8
    :goto_1
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getUsername()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/jscape/inet/ssh/protocol/v2/authentication/NoneClientAuthentication;->defaultAuthentication(Ljava/lang/String;)Lcom/jscape/inet/ssh/protocol/v2/authentication/NoneClientAuthentication;

    move-result-object v0

    return-object v0

    :catch_4
    move-exception v0

    :try_start_b
    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_b
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_5

    :catch_5
    move-exception v0

    :try_start_c
    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_6

    :catch_6
    move-exception v0

    :try_start_d
    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_d
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_7

    :catch_7
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :catch_8
    move-exception v0

    :try_start_e
    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_e
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_9

    :catch_9
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method public close()V
    .locals 0

    invoke-virtual {p0}, Lcom/jscape/inet/scp/Scp;->disconnect()V

    return-void
.end method

.method public declared-synchronized connect()Lcom/jscape/inet/scp/Scp;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/scp/ScpException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/inet/scp/Scp;->initService()V

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->j:Lcom/jscape/inet/scp/FileService;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/jscape/inet/scp/FileService;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->i:Lcom/jscape/inet/ssh/util/SshHostKeys;

    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getHostname()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->j:Lcom/jscape/inet/scp/FileService;

    check-cast v2, Lcom/jscape/inet/scp/ScpFileService;

    invoke-virtual {v2}, Lcom/jscape/inet/scp/ScpFileService;->sessionConnection()Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->hostKey()Ljava/security/PublicKey;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/jscape/inet/ssh/util/SshHostKeys;->put(Ljava/net/InetAddress;Ljava/security/PublicKey;)Lcom/jscape/inet/ssh/util/SshHostKeys;

    new-instance v0, Lcom/jscape/inet/scp/events/ScpConnectedEvent;

    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getHostname()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getPort()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/jscape/inet/scp/events/ScpConnectedEvent;-><init>(Lcom/jscape/inet/scp/Scp;Ljava/lang/String;I)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/scp/Scp;->a(Lcom/jscape/inet/scp/events/ScpEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p0

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-direct {p0, v0}, Lcom/jscape/inet/scp/Scp;->a(Ljava/lang/Throwable;)Lcom/jscape/inet/scp/ScpException;

    move-result-object v0

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized disconnect()V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/inet/scp/Scp;->isConnected()Z

    move-result v0
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->j:Lcom/jscape/inet/scp/FileService;

    invoke-interface {v0}, Lcom/jscape/inet/scp/FileService;->j()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/scp/Scp;->j:Lcom/jscape/inet/scp/FileService;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_1
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    new-instance v0, Lcom/jscape/inet/scp/events/ScpDisconnectedEvent;

    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getHostname()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getPort()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/jscape/inet/scp/events/ScpDisconnectedEvent;-><init>(Lcom/jscape/inet/scp/Scp;Ljava/lang/String;I)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/scp/Scp;->a(Lcom/jscape/inet/scp/events/ScpEvent;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p2}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/scp/Scp;->download(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object p1

    return-object p1
.end method

.method public download(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/jscape/inet/scp/Scp;->j:Lcom/jscape/inet/scp/FileService;

    iget-boolean v1, p0, Lcom/jscape/inet/scp/Scp;->f:Z

    new-instance v2, Lcom/jscape/inet/scp/Scp$DownloadListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/jscape/inet/scp/Scp$DownloadListener;-><init>(Lcom/jscape/inet/scp/Scp;Lcom/jscape/inet/scp/Scp$1;)V

    invoke-interface {p2, p1, v0, v1, v2}, Lcom/jscape/inet/scp/FileService;->readFile(Ljava/lang/String;Ljava/io/OutputStream;ZLcom/jscape/inet/scp/FileService$TransferListener;)Ljava/lang/Long;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/inet/scp/FileService$OperationException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    iget-boolean p2, p0, Lcom/jscape/inet/scp/Scp;->f:Z
    :try_end_1
    .catch Lcom/jscape/inet/scp/FileService$OperationException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    :try_start_2
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    invoke-virtual {p3, p1, p2}, Ljava/io/File;->setLastModified(J)Z
    :try_end_2
    .catch Lcom/jscape/inet/scp/FileService$OperationException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    return-object p3

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/inet/scp/FileService$OperationException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/inet/scp/FileService$OperationException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_2
    move-exception p1

    :try_start_5
    new-instance p2, Lcom/jscape/inet/scp/ScpException;

    invoke-direct {p2, p1}, Lcom/jscape/inet/scp/ScpException;-><init>(Ljava/lang/Throwable;)V

    throw p2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :goto_0
    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    throw p1
.end method

.method public download(Ljava/io/OutputStream;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    :try_start_0
    iget-object p3, p0, Lcom/jscape/inet/scp/Scp;->j:Lcom/jscape/inet/scp/FileService;

    const/4 v0, 0x0

    new-instance v1, Lcom/jscape/inet/scp/Scp$DownloadListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/jscape/inet/scp/Scp$DownloadListener;-><init>(Lcom/jscape/inet/scp/Scp;Lcom/jscape/inet/scp/Scp$1;)V

    invoke-interface {p3, p2, p1, v0, v1}, Lcom/jscape/inet/scp/FileService;->readFile(Ljava/lang/String;Ljava/io/OutputStream;ZLcom/jscape/inet/scp/FileService$TransferListener;)Ljava/lang/Long;
    :try_end_0
    .catch Lcom/jscape/inet/scp/FileService$OperationException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/scp/ScpException;

    invoke-direct {p2, p1}, Lcom/jscape/inet/scp/ScpException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public downloadDir(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :try_start_0
    iget-object p2, p0, Lcom/jscape/inet/scp/Scp;->j:Lcom/jscape/inet/scp/FileService;

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->d:Ljava/nio/file/Path;

    invoke-interface {v0}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jscape/inet/scp/Scp;->f:Z

    new-instance v2, Lcom/jscape/inet/scp/Scp$DownloadListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/jscape/inet/scp/Scp$DownloadListener;-><init>(Lcom/jscape/inet/scp/Scp;Lcom/jscape/inet/scp/Scp$1;)V

    invoke-interface {p2, p1, v0, v1, v2}, Lcom/jscape/inet/scp/FileService;->readDirectory(Ljava/lang/String;Ljava/io/File;ZLcom/jscape/inet/scp/FileService$TransferListener;)V
    :try_end_0
    .catch Lcom/jscape/inet/scp/FileService$OperationException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/scp/ScpException;

    invoke-direct {p2, p1}, Lcom/jscape/inet/scp/ScpException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public getConfiguration()Lcom/jscape/inet/ssh/SshConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->c:Lcom/jscape/inet/ssh/SshConfiguration;

    return-object v0
.end method

.method public getDebug()Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->g:Ljava/util/logging/Logger;

    invoke-virtual {v0}, Ljava/util/logging/Logger;->getLevel()Ljava/util/logging/Level;

    move-result-object v0

    sget-object v1, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method public getDebugStream()Ljava/io/PrintStream;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->g:Ljava/util/logging/Logger;

    if-eqz v0, :cond_1

    instance-of v0, v1, Lcom/jscape/util/j/b;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->g:Ljava/util/logging/Logger;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    check-cast v1, Lcom/jscape/util/j/b;

    invoke-virtual {v1}, Lcom/jscape/util/j/b;->a()Ljava/io/OutputStream;

    move-result-object v0

    check-cast v0, Ljava/io/PrintStream;

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method public getDownloadBlockSize()I
    .locals 1

    invoke-virtual {p0}, Lcom/jscape/inet/scp/Scp;->getTransferBufferSizeBytes()I

    move-result v0

    return v0
.end method

.method public getHostKeys()Lcom/jscape/inet/ssh/util/SshHostKeys;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->i:Lcom/jscape/inet/ssh/util/SshHostKeys;

    return-object v0
.end method

.method public getLocalDir()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->d:Ljava/nio/file/Path;

    invoke-interface {v0}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public getLogLevel()Ljava/util/logging/Level;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->g:Ljava/util/logging/Logger;

    invoke-virtual {v0}, Ljava/util/logging/Logger;->getLevel()Ljava/util/logging/Level;

    move-result-object v0

    return-object v0
.end method

.method public getParameters()Lcom/jscape/inet/ssh/util/SshParameters;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    return-object v0
.end method

.method public getPreserveTime()Z
    .locals 1

    invoke-virtual {p0}, Lcom/jscape/inet/scp/Scp;->isFileAttributesPreservingRequired()Z

    move-result v0

    return v0
.end method

.method public getTransferBufferSizeBytes()I
    .locals 1

    iget v0, p0, Lcom/jscape/inet/scp/Scp;->e:I

    return v0
.end method

.method public getUploadBlockSize()I
    .locals 1

    invoke-virtual {p0}, Lcom/jscape/inet/scp/Scp;->getTransferBufferSizeBytes()I

    move-result v0

    return v0
.end method

.method protected initService()V
    .locals 13

    invoke-virtual {p0}, Lcom/jscape/inet/scp/Scp;->rawConnector()Lcom/jscape/util/k/a/v;

    move-result-object v1

    invoke-virtual {p0}, Lcom/jscape/inet/scp/Scp;->authentication()Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;

    move-result-object v8

    new-instance v12, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->c:Lcom/jscape/inet/ssh/SshConfiguration;

    iget-object v2, v0, Lcom/jscape/inet/ssh/SshConfiguration;->identificationString:Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->c:Lcom/jscape/inet/ssh/SshConfiguration;

    iget-object v3, v0, Lcom/jscape/inet/ssh/SshConfiguration;->keyExchangeFactory:Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->c:Lcom/jscape/inet/ssh/SshConfiguration;

    iget-object v4, v0, Lcom/jscape/inet/ssh/SshConfiguration;->encryptionFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->c:Lcom/jscape/inet/ssh/SshConfiguration;

    iget-object v5, v0, Lcom/jscape/inet/ssh/SshConfiguration;->macFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->c:Lcom/jscape/inet/ssh/SshConfiguration;

    iget-object v6, v0, Lcom/jscape/inet/ssh/SshConfiguration;->compressionFactory:Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/util/SshParameters;->getHostKeyVerifier()Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;

    move-result-object v7

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->c:Lcom/jscape/inet/ssh/SshConfiguration;

    iget v9, v0, Lcom/jscape/inet/ssh/SshConfiguration;->initialWindowSize:I

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->c:Lcom/jscape/inet/ssh/SshConfiguration;

    iget v10, v0, Lcom/jscape/inet/ssh/SshConfiguration;->maxPacketSize:I

    iget-object v11, p0, Lcom/jscape/inet/scp/Scp;->g:Ljava/util/logging/Logger;

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnector;-><init>(Lcom/jscape/util/k/a/v;Lcom/jscape/inet/ssh/protocol/messages/IdentificationString;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/ClientKeyExchangeFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/EncryptionFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/MacFactory;Lcom/jscape/inet/ssh/protocol/v2/marshaling/algorithms/CompressionFactory;Lcom/jscape/inet/ssh/protocol/v2/transport/keyexchange/HostKeyVerificationService;Lcom/jscape/inet/ssh/protocol/v2/authentication/ClientAuthentication;IILjava/util/logging/Logger;)V

    new-instance v0, Lcom/jscape/inet/scp/ScpFileService;

    iget v1, p0, Lcom/jscape/inet/scp/Scp;->e:I

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->g:Ljava/util/logging/Logger;

    invoke-direct {v0, v12, v1, v2}, Lcom/jscape/inet/scp/ScpFileService;-><init>(Lcom/jscape/util/k/a/v;ILjava/util/logging/Logger;)V

    iput-object v0, p0, Lcom/jscape/inet/scp/Scp;->j:Lcom/jscape/inet/scp/FileService;

    return-void
.end method

.method public interrupt()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->j:Lcom/jscape/inet/scp/FileService;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-interface {v1}, Lcom/jscape/inet/scp/FileService;->cancelTransferOperation()V

    :cond_1
    return-void
.end method

.method public declared-synchronized isConnected()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->j:Lcom/jscape/inet/scp/FileService;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :try_start_2
    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->j:Lcom/jscape/inet/scp/FileService;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-interface {v1}, Lcom/jscape/inet/scp/FileService;->h()Z

    move-result v1
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move v0, v1

    :goto_1
    monitor-exit p0

    return v0

    :catch_0
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_2
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isFileAttributesPreservingRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/inet/scp/Scp;->f:Z

    return v0
.end method

.method public mdownload(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :try_start_0
    iget-object p2, p0, Lcom/jscape/inet/scp/Scp;->j:Lcom/jscape/inet/scp/FileService;

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->d:Ljava/nio/file/Path;

    invoke-interface {v0}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object v0

    iget-boolean v1, p0, Lcom/jscape/inet/scp/Scp;->f:Z

    new-instance v2, Lcom/jscape/inet/scp/Scp$DownloadListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/jscape/inet/scp/Scp$DownloadListener;-><init>(Lcom/jscape/inet/scp/Scp;Lcom/jscape/inet/scp/Scp$1;)V

    invoke-interface {p2, p1, v0, v1, v2}, Lcom/jscape/inet/scp/FileService;->readFiles(Ljava/lang/String;Ljava/io/File;ZLcom/jscape/inet/scp/FileService$TransferListener;)V
    :try_end_0
    .catch Lcom/jscape/inet/scp/FileService$OperationException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/scp/ScpException;

    invoke-direct {p2, p1}, Lcom/jscape/inet/scp/ScpException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public mdownload(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, p1, v1}, Lcom/jscape/inet/scp/Scp;->download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public mdownload(Ljava/lang/String;Ljava/util/Enumeration;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Enumeration<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    :cond_0
    invoke-interface {p2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, p1, v1}, Lcom/jscape/inet/scp/Scp;->download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public mdownload(Ljava/lang/String;Ljava/util/Iterator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, p1, v1}, Lcom/jscape/inet/scp/Scp;->download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public varargs mdownload(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    array-length v1, p2

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v1, :cond_1

    aget-object v3, p2, v2

    invoke-virtual {p0, p1, v3}, Lcom/jscape/inet/scp/Scp;->download(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public mupload(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p2}, Lcom/jscape/inet/scp/Scp;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/jscape/inet/scp/Scp;->mupload(Ljava/lang/String;Ljava/util/Iterator;)V

    return-void
.end method

.method public mupload(Ljava/lang/String;Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, p1, v1}, Lcom/jscape/inet/scp/Scp;->upload(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public mupload(Ljava/lang/String;Ljava/util/Enumeration;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Enumeration<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    :cond_0
    invoke-interface {p2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, p1, v1}, Lcom/jscape/inet/scp/Scp;->upload(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public mupload(Ljava/lang/String;Ljava/util/Iterator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, p1, v1}, Lcom/jscape/inet/scp/Scp;->upload(Ljava/lang/String;Ljava/lang/String;)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public varargs mupload(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    array-length v1, p2

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v1, :cond_1

    aget-object v3, p2, v2

    invoke-virtual {p0, p1, v3}, Lcom/jscape/inet/scp/Scp;->upload(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method protected rawConnector()Lcom/jscape/util/k/a/v;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jscape/util/k/a/v<",
            "Lcom/jscape/util/k/a/C;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/k/g;

    new-instance v1, Lcom/jscape/util/k/TransportAddress;

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getHostname()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v3}, Lcom/jscape/inet/ssh/util/SshParameters;->getPort()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getConnectionTimeout()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/jscape/util/k/g;-><init>(Lcom/jscape/util/k/TransportAddress;J)V

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v1

    new-instance v11, Lcom/jscape/util/k/b/j;

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getSocketTrafficClass()Ljava/lang/Integer;

    move-result-object v6

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getTcpNoDelay()Ljava/lang/Boolean;

    move-result-object v7

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getReadingTimeout()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/jscape/util/Time;->millis(J)Lcom/jscape/util/Time;

    move-result-object v10

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v2, v11

    invoke-direct/range {v2 .. v10}, Lcom/jscape/util/k/b/j;-><init>(ZLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/jscape/util/Time;Lcom/jscape/util/Time;)V

    :try_start_0
    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyType()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyHost()Ljava/lang/String;

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    if-eqz v2, :cond_1

    new-instance v1, Lcom/jscape/inet/c/f;

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyHost()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v3}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyPort()I

    move-result v3

    iget-object v4, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v4}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyUsername()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyPassword()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/jscape/inet/c/f;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v2}, Lcom/jscape/inet/ssh/util/SshParameters;->getProxyType()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/jscape/inet/c/d;->a(Ljava/lang/String;)Lcom/jscape/inet/c/e;

    move-result-object v2

    new-instance v3, Lcom/jscape/inet/c/g;

    invoke-direct {v3, v1, v2, v0, v11}, Lcom/jscape/inet/c/g;-><init>(Lcom/jscape/inet/c/f;Lcom/jscape/inet/c/e;Lcom/jscape/util/k/g;Lcom/jscape/util/k/b/j;)V

    return-object v3

    :cond_1
    new-instance v1, Lcom/jscape/util/k/b/i;

    invoke-direct {v1, v0, v11}, Lcom/jscape/util/k/b/i;-><init>(Lcom/jscape/util/k/g;Lcom/jscape/util/k/b/j;)V

    return-object v1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method public removeListener(Lcom/jscape/inet/scp/events/ScpEventListener;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public setConfiguration(Lcom/jscape/inet/ssh/SshConfiguration;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/scp/Scp;->c:Lcom/jscape/inet/ssh/SshConfiguration;

    return-void
.end method

.method public setDebug(Z)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->g:Ljava/util/logging/Logger;

    if-eqz p1, :cond_0

    sget-object p1, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    sget-object p1, Ljava/util/logging/Level;->OFF:Ljava/util/logging/Level;

    :goto_0
    invoke-virtual {v0, p1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method public setDebugStream(Ljava/io/PrintStream;)V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->g:Ljava/util/logging/Logger;

    if-eqz v0, :cond_0

    instance-of v0, v1, Lcom/jscape/util/j/b;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/inet/scp/Scp;->g:Ljava/util/logging/Logger;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    check-cast v1, Lcom/jscape/util/j/b;

    invoke-virtual {v1, p1}, Lcom/jscape/util/j/b;->a(Ljava/io/OutputStream;)V

    :cond_1
    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
.end method

.method public setDownloadBlockSize(I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/jscape/inet/scp/Scp;->setTransferBufferSizeBytes(I)V

    return-void
.end method

.method public setFileAttributesPreservingRequired(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jscape/inet/scp/Scp;->f:Z

    return-void
.end method

.method public setLocalDir(Ljava/io/File;)V
    .locals 3

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    sget-object v1, Lcom/jscape/inet/scp/Scp;->k:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p1

    invoke-interface {p1}, Ljava/nio/file/Path;->normalize()Ljava/nio/file/Path;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/inet/scp/Scp;->d:Ljava/nio/file/Path;

    return-void
.end method

.method public setLogLevel(Ljava/util/logging/Level;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->g:Ljava/util/logging/Logger;

    invoke-virtual {v0, p1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    return-void
.end method

.method public setParameters(Lcom/jscape/inet/ssh/util/SshParameters;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    return-void
.end method

.method public setPreserveTime(Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/jscape/inet/scp/Scp;->setFileAttributesPreservingRequired(Z)V

    return-void
.end method

.method public setTransferBufferSizeBytes(I)V
    .locals 5

    int-to-long v0, p1

    sget-object v2, Lcom/jscape/inet/scp/Scp;->k:[Ljava/lang/String;

    const/16 v3, 0x8

    aget-object v2, v2, v3

    const-wide/16 v3, 0x0

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p1, p0, Lcom/jscape/inet/scp/Scp;->e:I

    return-void
.end method

.method public setUploadBlockSize(I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/jscape/inet/scp/Scp;->setTransferBufferSizeBytes(I)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/scp/Scp;->k:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->b:Lcom/jscape/inet/ssh/util/SshParameters;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->c:Lcom/jscape/inet/ssh/SshConfiguration;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/scp/Scp;->d:Ljava/nio/file/Path;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/inet/scp/Scp;->e:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/inet/scp/Scp;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public upload(Ljava/io/File;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/scp/Scp;->upload(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public upload(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/inet/scp/Scp;->f:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0
    :try_end_0
    .catch Lcom/jscape/inet/scp/FileService$OperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v7, v0

    goto :goto_0

    :cond_0
    move-object v7, v1

    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->j:Lcom/jscape/inet/scp/FileService;

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v4

    const/4 v6, 0x0

    new-instance v8, Lcom/jscape/inet/scp/Scp$UploadListener;

    invoke-direct {v8, p0, v1}, Lcom/jscape/inet/scp/Scp$UploadListener;-><init>(Lcom/jscape/inet/scp/Scp;Lcom/jscape/inet/scp/Scp$1;)V

    move-object v1, v9

    move-object v2, p2

    move-object v3, p3

    invoke-interface/range {v0 .. v8}, Lcom/jscape/inet/scp/FileService;->writeFile(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/Long;Lcom/jscape/inet/scp/FileService$TransferListener;)V
    :try_end_1
    .catch Lcom/jscape/inet/scp/FileService$OperationException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v9}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    return-void

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Lcom/jscape/inet/scp/FileService$OperationException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception p1

    :try_start_3
    new-instance p2, Lcom/jscape/inet/scp/ScpException;

    invoke-direct {p2, p1}, Lcom/jscape/inet/scp/ScpException;-><init>(Ljava/lang/Throwable;)V

    throw p2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :goto_1
    invoke-static {v9}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    throw p1
.end method

.method public upload(Ljava/io/InputStream;JLjava/lang/String;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->j:Lcom/jscape/inet/scp/FileService;

    const/4 v6, 0x0

    const/4 v7, 0x0

    new-instance v8, Lcom/jscape/inet/scp/Scp$UploadListener;

    const/4 v1, 0x0

    invoke-direct {v8, p0, v1}, Lcom/jscape/inet/scp/Scp$UploadListener;-><init>(Lcom/jscape/inet/scp/Scp;Lcom/jscape/inet/scp/Scp$1;)V

    move-object v1, p1

    move-object v2, p4

    move-object v3, p5

    move-wide v4, p2

    invoke-interface/range {v0 .. v8}, Lcom/jscape/inet/scp/FileService;->writeFile(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/Long;Lcom/jscape/inet/scp/FileService$TransferListener;)V
    :try_end_0
    .catch Lcom/jscape/inet/scp/FileService$OperationException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/scp/ScpException;

    invoke-direct {p2, p1}, Lcom/jscape/inet/scp/ScpException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public upload(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v0

    int-to-long v3, v0

    move-object v1, p0

    move-object v2, p1

    move-object v5, p2

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, Lcom/jscape/inet/scp/Scp;->upload(Ljava/io/InputStream;JLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public upload(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p2}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object p2

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2, p1, v0}, Lcom/jscape/inet/scp/Scp;->upload(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public upload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/scp/Scp;->b(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/inet/scp/Scp;->upload(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public upload([BLjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length p1, p1

    int-to-long v2, p1

    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/jscape/inet/scp/Scp;->upload(Ljava/io/InputStream;JLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public uploadDir(Ljava/io/File;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/inet/scp/Scp;->uploadDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public uploadDir(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/inet/scp/Scp;->j:Lcom/jscape/inet/scp/FileService;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/jscape/inet/scp/Scp;->f:Z

    new-instance v6, Lcom/jscape/inet/scp/Scp$UploadListener;

    const/4 v1, 0x0

    invoke-direct {v6, p0, v1}, Lcom/jscape/inet/scp/Scp$UploadListener;-><init>(Lcom/jscape/inet/scp/Scp;Lcom/jscape/inet/scp/Scp$1;)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-interface/range {v0 .. v6}, Lcom/jscape/inet/scp/FileService;->writeDirectory(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/jscape/inet/scp/FileService$TransferListener;)V
    :try_end_0
    .catch Lcom/jscape/inet/scp/FileService$OperationException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/inet/scp/ScpException;

    invoke-direct {p2, p1}, Lcom/jscape/inet/scp/ScpException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method
