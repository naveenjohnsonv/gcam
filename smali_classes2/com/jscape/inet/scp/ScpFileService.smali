.class public Lcom/jscape/inet/scp/ScpFileService;
.super Lcom/jscape/util/n/c;

# interfaces
.implements Lcom/jscape/inet/scp/FileService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/util/n/c<",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/jscape/inet/scp/FileService;"
    }
.end annotation


# static fields
.field private static final c:Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec;

.field private static final d:Ljava/lang/String;

.field private static final e:Ljava/lang/String;

.field private static final f:I = 0x3

.field private static final g:I = 0x2000

.field private static final o:[Ljava/lang/String;


# instance fields
.field private final h:Lcom/jscape/util/k/a/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/k/a/v<",
            "Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;",
            ">;"
        }
    .end annotation
.end field

.field private final i:I

.field private final j:Ljava/util/logging/Logger;

.field private k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

.field private volatile l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

.field private m:Ljava/io/InputStream;

.field private n:Ljava/io/OutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "\'\u0018\\K6\u007f6\u000fM{o\t6;\u0007\u0000EM7rx\t\u0019\u0008w|exT@\u0016\u000c|e\u0005RM\r_w$;.x\u000c:y6\u0006\u0008KX0y6H\u000eZI8b=\u000cW\u0008w|exT@\u0016\u000c|e\u0005F\u001f*\u000cL\u000c-d9\u0006\u001eNI+6:\u001d\u000bNI+6+\u0001\u0017M\u000c/w4\u001d\u0008\u0006(!\u0003KC4\u007f6\u000fM{o\t65\r\u001e[M>sx\t\u0019\u0008w|exT@\u0016\u000c|e\u0005RM\r_w(\'\u0018\\K6\u007f6\u000fM{o\t65\r\u001e[M>sx\t\u0019\u0008w|exT@\u0016\u000c|e\u0005RM\r_w\u001aDM\\^8x+\u000e\u0008Zn,p>\r\u001f{E#s\u001a\u0011\u0019M_d\u0004XZ\u001d\u0019\u0004X[\u001c\u0018\u0004XZ\u001d\u0019\u001a;\u000eXj0z=;\u0008ZZ0u=H\u0016KC7x=\u000b\u0019G^d#;.x\u000c:y6\u0006\u0008KX0y6H\u000eDC*s<RMs\t*6dES\u0008\t*Kv"

    const/16 v4, 0x128

    const/16 v5, 0x28

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x34

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    const/16 v14, 0x22

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v3, 0x1d

    const-string v4, "\u001a$js\u000eT\u001c;/k+\u0017_\u001c \'fe\u0019\u0011\u0012*9|j\u0019TEo\u0004\u007f|;?"

    move v5, v3

    move-object v3, v4

    move v7, v10

    move v4, v14

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x13

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/inet/scp/ScpFileService;->o:[Ljava/lang/String;

    const/16 v1, 0x8

    aget-object v1, v0, v1

    sput-object v1, Lcom/jscape/inet/scp/ScpFileService;->d:Ljava/lang/String;

    const/4 v1, 0x7

    aget-object v0, v0, v1

    sput-object v0, Lcom/jscape/inet/scp/ScpFileService;->e:Ljava/lang/String;

    new-instance v0, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec;

    new-array v1, v2, [Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;

    invoke-direct {v0, v1}, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec;-><init>([Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec$Entry;)V

    invoke-static {v0}, Lcom/jscape/inet/scp/protocol/marshaling/Messages;->init(Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec;)Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec;

    move-result-object v0

    sput-object v0, Lcom/jscape/inet/scp/ScpFileService;->c:Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec;

    return-void

    :cond_3
    aget-char v15, v10, v13

    rem-int/lit8 v1, v13, 0x7

    if-eqz v1, :cond_8

    if-eq v1, v9, :cond_7

    const/4 v2, 0x2

    if-eq v1, v2, :cond_6

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    const/4 v2, 0x4

    if-eq v1, v2, :cond_4

    const/4 v2, 0x5

    if-eq v1, v2, :cond_9

    const/16 v14, 0x6c

    goto :goto_4

    :cond_4
    const/16 v14, 0x6d

    goto :goto_4

    :cond_5
    const/16 v14, 0x18

    goto :goto_4

    :cond_6
    const/16 v14, 0x1c

    goto :goto_4

    :cond_7
    const/16 v14, 0x59

    goto :goto_4

    :cond_8
    const/16 v14, 0x5c

    :cond_9
    :goto_4
    xor-int v1, v8, v14

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    const/4 v2, 0x0

    goto/16 :goto_2
.end method

.method public constructor <init>(Lcom/jscape/util/k/a/v;ILjava/util/logging/Logger;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/v<",
            "Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;",
            ">;I",
            "Ljava/util/logging/Logger;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/util/n/c;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/inet/scp/ScpFileService;->h:Lcom/jscape/util/k/a/v;

    int-to-long v0, p2

    sget-object p1, Lcom/jscape/inet/scp/ScpFileService;->o:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object p1, p1, v2

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3, p1}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p2, p0, Lcom/jscape/inet/scp/ScpFileService;->i:I

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/inet/scp/ScpFileService;->j:Ljava/util/logging/Logger;

    return-void
.end method

.method private a(Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;Ljava/io/File;Lcom/jscape/inet/scp/FileService$TransferListener;)Ljava/io/File;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/io/File;

    iget-object p1, p1, Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;->directoryName:Ljava/lang/String;

    invoke-direct {v0, p2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/jscape/util/Q;->b(Ljava/io/File;)Ljava/io/File;

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->c()V

    const/4 p1, 0x1

    invoke-direct {p0, v0, p1, p3}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/io/File;ZLcom/jscape/inet/scp/FileService$TransferListener;)V

    return-object v0
.end method

.method private a(Lcom/jscape/inet/scp/protocol/messages/FileMessage;Ljava/io/File;Lcom/jscape/inet/scp/FileService$TransferListener;)Ljava/io/File;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p1, Lcom/jscape/inet/scp/protocol/messages/FileMessage;->fileName:Ljava/lang/String;

    invoke-direct {v1, p2, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance p2, Ljava/io/FileOutputStream;

    invoke-direct {p2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {p0, p2}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->c()V

    iget-object p2, p1, Lcom/jscape/inet/scp/protocol/messages/FileMessage;->fileName:Ljava/lang/String;

    iget-wide v2, p1, Lcom/jscape/inet/scp/protocol/messages/FileMessage;->fileLength:J

    invoke-interface {p3, p2, v2, v3}, Lcom/jscape/inet/scp/FileService$TransferListener;->onTransferStarted(Ljava/lang/String;J)V

    iget-object v3, p0, Lcom/jscape/inet/scp/ScpFileService;->m:Ljava/io/InputStream;

    iget-wide v5, p1, Lcom/jscape/inet/scp/protocol/messages/FileMessage;->fileLength:J

    move-object v2, p0

    move-object v4, v0

    move-object v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/io/InputStream;Ljava/io/OutputStream;JLcom/jscape/inet/scp/FileService$TransferListener;)V

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->e()V

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    return-object v1

    :catchall_0
    move-exception p1

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    throw p1
.end method

.method private a(Ljava/io/InputStream;)Ljava/io/InputStream;
    .locals 3

    new-instance v0, Ljava/io/BufferedInputStream;

    iget v1, p0, Lcom/jscape/inet/scp/ScpFileService;->i:I

    mul-int/lit8 v1, v1, 0x3

    const/16 v2, 0x2000

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-direct {v0, p1, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    return-object v0
.end method

.method private a(Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .locals 3

    new-instance v0, Ljava/io/BufferedOutputStream;

    iget v1, p0, Lcom/jscape/inet/scp/ScpFileService;->i:I

    mul-int/lit8 v1, v1, 0x3

    const/16 v2, 0x2000

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-direct {v0, p1, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    return-object v0
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Z)Ljava/lang/Long;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->d()Lcom/jscape/inet/scp/protocol/messages/Message;

    move-result-object p1

    check-cast p1, Lcom/jscape/inet/scp/protocol/messages/TimeMessage;

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->c()V

    iget-wide v0, p1, Lcom/jscape/inet/scp/protocol/messages/TimeMessage;->modificationDate:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method

.method private a()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/scp/ScpFileService;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->openSessionChannel(Z)Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/inet/scp/ScpFileService;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v1, Lcom/jscape/util/k/a/a;

    iget-object v2, p0, Lcom/jscape/inet/scp/ScpFileService;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-direct {v1, v2}, Lcom/jscape/util/k/a/a;-><init>(Lcom/jscape/util/k/a/C;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/jscape/inet/scp/ScpFileService;->m:Ljava/io/InputStream;

    new-instance v0, Ljava/io/BufferedOutputStream;

    new-instance v1, Lcom/jscape/util/k/a/e;

    iget-object v2, p0, Lcom/jscape/inet/scp/ScpFileService;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-direct {v1, v2}, Lcom/jscape/util/k/a/e;-><init>(Lcom/jscape/util/k/a/C;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/jscape/inet/scp/ScpFileService;->n:Ljava/io/OutputStream;

    return-void
.end method

.method private a(Lcom/jscape/inet/scp/protocol/messages/Command;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/scp/ScpFileService;->b(Lcom/jscape/inet/scp/protocol/messages/Command;)V

    invoke-static {p1}, Lcom/jscape/inet/scp/protocol/marshaling/CommandFormat;->format(Lcom/jscape/inet/scp/protocol/messages/Command;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/inet/scp/ScpFileService;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->exec(Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Lcom/jscape/inet/scp/protocol/messages/Message;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/scp/ScpFileService;->d(Lcom/jscape/inet/scp/protocol/messages/Message;)V

    sget-object v0, Lcom/jscape/inet/scp/ScpFileService;->c:Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec;

    iget-object v1, p0, Lcom/jscape/inet/scp/ScpFileService;->n:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, v1}, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec;->write(Lcom/jscape/inet/scp/protocol/messages/Message;Ljava/io/OutputStream;)V

    return-void
.end method

.method private a(Ljava/io/EOFException;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    if-nez p2, :cond_0

    return-void

    :cond_0
    :try_start_0
    throw p1
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private a(Ljava/io/File;Lcom/jscape/inet/scp/FileService$TransferListener;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v0

    :try_start_0
    new-instance v1, Lcom/jscape/inet/scp/protocol/messages/FileMessage;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v3

    sget-object v5, Lcom/jscape/inet/scp/ScpFileService;->o:[Ljava/lang/String;

    const/4 v6, 0x7

    aget-object v5, v5, v6

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/jscape/inet/scp/protocol/messages/FileMessage;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/jscape/inet/scp/ScpFileService;->c(Lcom/jscape/inet/scp/protocol/messages/Message;)V

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-interface {p2, v1, v2, v3}, Lcom/jscape/inet/scp/FileService$TransferListener;->onTransferStarted(Ljava/lang/String;J)V

    iget-object v3, p0, Lcom/jscape/inet/scp/ScpFileService;->n:Ljava/io/OutputStream;

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v4

    move-object v1, p0

    move-object v2, v0

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/io/InputStream;Ljava/io/OutputStream;JLcom/jscape/inet/scp/FileService$TransferListener;)V

    new-instance p1, Lcom/jscape/inet/scp/protocol/messages/OkMessage;

    invoke-direct {p1}, Lcom/jscape/inet/scp/protocol/messages/OkMessage;-><init>()V

    invoke-direct {p0, p1}, Lcom/jscape/inet/scp/ScpFileService;->c(Lcom/jscape/inet/scp/protocol/messages/Message;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    return-void

    :catchall_0
    move-exception p1

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    throw p1
.end method

.method private a(Ljava/io/File;Ljava/lang/Long;)V
    .locals 2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljava/io/File;->setLastModified(J)Z

    :cond_0
    return-void
.end method

.method private a(Ljava/io/File;ZLcom/jscape/inet/scp/FileService$TransferListener;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    const/4 v2, 0x1

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->d()Lcom/jscape/inet/scp/protocol/messages/Message;

    move-result-object v3

    instance-of v4, v3, Lcom/jscape/inet/scp/protocol/messages/TimeMessage;

    const/4 v5, 0x0

    if-eqz v4, :cond_1

    move-object v1, v3

    check-cast v1, Lcom/jscape/inet/scp/protocol/messages/TimeMessage;

    iget-wide v6, v1, Lcom/jscape/inet/scp/protocol/messages/TimeMessage;->modificationDate:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->c()V
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_4

    move p2, v2

    if-eqz v0, :cond_3

    if-nez v0, :cond_7

    :cond_1
    :try_start_1
    instance-of v4, v3, Lcom/jscape/inet/scp/protocol/messages/FileMessage;
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_3

    if-eqz v0, :cond_4

    if-eqz v4, :cond_3

    :try_start_2
    move-object p2, v3

    check-cast p2, Lcom/jscape/inet/scp/protocol/messages/FileMessage;

    invoke-direct {p0, p2, p1, p3}, Lcom/jscape/inet/scp/ScpFileService;->a(Lcom/jscape/inet/scp/protocol/messages/FileMessage;Ljava/io/File;Lcom/jscape/inet/scp/FileService$TransferListener;)Ljava/io/File;

    move-result-object p2

    invoke-direct {p0, p2, v1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/io/File;Ljava/lang/Long;)V
    :try_end_2
    .catch Ljava/io/EOFException; {:try_start_2 .. :try_end_2} :catch_0

    if-nez v0, :cond_2

    move p2, v5

    goto :goto_0

    :cond_2
    move p2, v5

    goto :goto_3

    :cond_3
    :goto_0
    :try_start_3
    instance-of v4, v3, Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;
    :try_end_3
    .catch Ljava/io/EOFException; {:try_start_3 .. :try_end_3} :catch_4

    :cond_4
    if-eqz v0, :cond_6

    if-eqz v4, :cond_5

    :try_start_4
    move-object p2, v3

    check-cast p2, Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;

    invoke-direct {p0, p2, p1, p3}, Lcom/jscape/inet/scp/ScpFileService;->a(Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;Ljava/io/File;Lcom/jscape/inet/scp/FileService$TransferListener;)Ljava/io/File;

    move-result-object p2

    invoke-direct {p0, p2, v1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/io/File;Ljava/lang/Long;)V
    :try_end_4
    .catch Ljava/io/EOFException; {:try_start_4 .. :try_end_4} :catch_0

    if-nez v0, :cond_2

    move p2, v5

    goto :goto_1

    :catch_0
    move-exception p1

    move p2, v2

    goto :goto_4

    :cond_5
    :goto_1
    :try_start_5
    instance-of v4, v3, Lcom/jscape/inet/scp/protocol/messages/EndDirectoryMessage;
    :try_end_5
    .catch Ljava/io/EOFException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/io/EOFException; {:try_start_6 .. :try_end_6} :catch_4

    :cond_6
    :goto_2
    if-eqz v4, :cond_7

    :try_start_7
    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->c()V
    :try_end_7
    .catch Ljava/io/EOFException; {:try_start_7 .. :try_end_7} :catch_2

    if-nez v0, :cond_8

    goto :goto_3

    :catch_2
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_7
    :goto_3
    if-nez v0, :cond_0

    goto :goto_5

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Ljava/io/EOFException; {:try_start_8 .. :try_end_8} :catch_4

    :catch_4
    move-exception p1

    :goto_4
    invoke-direct {p0, p1, p2}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/io/EOFException;Z)V

    :cond_8
    :goto_5
    return-void
.end method

.method private a(Ljava/io/InputStream;Ljava/io/OutputStream;JLcom/jscape/inet/scp/FileService$TransferListener;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget v0, p0, Lcom/jscape/inet/scp/ScpFileService;->i:I

    new-array v1, v0, [B

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v2

    int-to-long v3, v0

    invoke-static {v3, v4, p3, p4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    :cond_0
    cmp-long v7, v5, p3

    if-gez v7, :cond_2

    long-to-int v3, v3

    const/4 v4, 0x0

    invoke-virtual {p1, v1, v4, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    :try_start_0
    invoke-direct {p0, v3}, Lcom/jscape/inet/scp/ScpFileService;->a(I)Z

    move-result v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_1

    if-nez v7, :cond_2

    invoke-virtual {p2, v1, v4, v3}, Ljava/io/OutputStream;->write([BII)V

    int-to-long v3, v3

    add-long/2addr v5, v3

    invoke-interface {p5, v5, v6}, Lcom/jscape/inet/scp/FileService$TransferListener;->onTransferProgress(J)V

    move v7, v0

    :cond_1
    int-to-long v3, v7

    sub-long v7, p3, v5

    invoke-static {v3, v4, v7, v8}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    if-nez v2, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    invoke-interface {p5, v5, v6}, Lcom/jscape/inet/scp/FileService$TransferListener;->onTransferCompleted(J)V

    return-void
.end method

.method private a(I)Z
    .locals 1

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    return p1
.end method

.method private b()V
    .locals 2

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/scp/ScpFileService;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/scp/ScpFileService;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/inet/scp/ScpFileService;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    :cond_1
    return-void
.end method

.method private b(Lcom/jscape/inet/scp/protocol/messages/Command;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/scp/ScpFileService;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/scp/ScpFileService;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/scp/ScpFileService;->o:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/jscape/inet/scp/ScpFileService;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/jscape/inet/scp/ScpFileService;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v4, v3

    const/4 v3, 0x2

    aput-object p1, v4, v3

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private b(Lcom/jscape/inet/scp/protocol/messages/Message;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/scp/ScpException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    :try_start_0
    instance-of v1, p1, Lcom/jscape/inet/scp/protocol/messages/WarningMessage;
    :try_end_0
    .catch Lcom/jscape/inet/scp/ScpException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/jscape/inet/scp/protocol/messages/FatalErrorMessage;

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/jscape/inet/scp/ScpException;

    check-cast p1, Lcom/jscape/inet/scp/protocol/messages/WarningMessage;

    iget-object p1, p1, Lcom/jscape/inet/scp/protocol/messages/WarningMessage;->message:Ljava/lang/String;

    invoke-direct {v0, p1}, Lcom/jscape/inet/scp/ScpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/jscape/inet/scp/ScpException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_1
    :goto_0
    if-nez v1, :cond_2

    return-void

    :cond_2
    :try_start_2
    new-instance v0, Lcom/jscape/inet/scp/ScpException;

    check-cast p1, Lcom/jscape/inet/scp/protocol/messages/FatalErrorMessage;

    iget-object p1, p1, Lcom/jscape/inet/scp/protocol/messages/FatalErrorMessage;->message:Ljava/lang/String;

    invoke-direct {v0, p1}, Lcom/jscape/inet/scp/ScpException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lcom/jscape/inet/scp/ScpException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/inet/scp/ScpException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method private b(Ljava/io/File;ZLcom/jscape/inet/scp/FileService$TransferListener;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p1

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    return-void

    :cond_0
    array-length v1, p1

    const/4 v2, 0x0

    :cond_1
    if-ge v2, v1, :cond_6

    aget-object v3, p1, v2

    if-eqz v0, :cond_7

    if-eqz v0, :cond_3

    if-eqz p2, :cond_2

    :try_start_0
    new-instance v4, Lcom/jscape/inet/scp/protocol/messages/TimeMessage;

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v7

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/jscape/inet/scp/protocol/messages/TimeMessage;-><init>(JJ)V

    invoke-direct {p0, v4}, Lcom/jscape/inet/scp/ScpFileService;->c(Lcom/jscape/inet/scp/protocol/messages/Message;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    goto :goto_1

    :cond_3
    move v4, p2

    :goto_1
    if-eqz v4, :cond_4

    :try_start_1
    invoke-direct {p0, v3, p2, p3}, Lcom/jscape/inet/scp/ScpFileService;->c(Ljava/io/File;ZLcom/jscape/inet/scp/FileService$TransferListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_5

    goto :goto_2

    :catch_1
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_4
    :goto_2
    invoke-direct {p0, v3, p3}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/io/File;Lcom/jscape/inet/scp/FileService$TransferListener;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :cond_5
    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_1

    goto :goto_3

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_6
    :goto_3
    new-instance p1, Lcom/jscape/inet/scp/protocol/messages/EndDirectoryMessage;

    invoke-direct {p1}, Lcom/jscape/inet/scp/protocol/messages/EndDirectoryMessage;-><init>()V

    invoke-direct {p0, p1}, Lcom/jscape/inet/scp/ScpFileService;->c(Lcom/jscape/inet/scp/protocol/messages/Message;)V

    :cond_7
    return-void
.end method

.method private c()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/scp/protocol/messages/OkMessage;

    invoke-direct {v0}, Lcom/jscape/inet/scp/protocol/messages/OkMessage;-><init>()V

    invoke-direct {p0, v0}, Lcom/jscape/inet/scp/ScpFileService;->a(Lcom/jscape/inet/scp/protocol/messages/Message;)V

    return-void
.end method

.method private c(Lcom/jscape/inet/scp/protocol/messages/Message;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Lcom/jscape/inet/scp/protocol/messages/Message;)V

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->e()V

    return-void
.end method

.method private c(Ljava/io/File;ZLcom/jscape/inet/scp/FileService$TransferListener;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/jscape/inet/scp/ScpFileService;->o:[Ljava/lang/String;

    const/16 v3, 0x8

    aget-object v2, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/scp/ScpFileService;->c(Lcom/jscape/inet/scp/protocol/messages/Message;)V

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/inet/scp/ScpFileService;->b(Ljava/io/File;ZLcom/jscape/inet/scp/FileService$TransferListener;)V

    return-void
.end method

.method private d()Lcom/jscape/inet/scp/protocol/messages/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/jscape/inet/scp/protocol/messages/Message;",
            ">()TM;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sget-object v0, Lcom/jscape/inet/scp/ScpFileService;->c:Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec;

    iget-object v1, p0, Lcom/jscape/inet/scp/ScpFileService;->m:Ljava/io/InputStream;

    invoke-virtual {v0, v1}, Lcom/jscape/inet/scp/protocol/marshaling/MessageCodec;->read(Ljava/io/InputStream;)Lcom/jscape/inet/scp/protocol/messages/Message;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/inet/scp/ScpFileService;->e(Lcom/jscape/inet/scp/protocol/messages/Message;)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/scp/ScpFileService;->b(Lcom/jscape/inet/scp/protocol/messages/Message;)V

    return-object v0
.end method

.method private d(Lcom/jscape/inet/scp/protocol/messages/Message;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/scp/ScpFileService;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/scp/ScpFileService;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/scp/ScpFileService;->o:[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v0, v0, v3

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jscape/inet/scp/ScpFileService;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/jscape/inet/scp/ScpFileService;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private e()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->d()Lcom/jscape/inet/scp/protocol/messages/Message;

    return-void
.end method

.method private e(Lcom/jscape/inet/scp/protocol/messages/Message;)V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/scp/ScpFileService;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/scp/ScpFileService;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/scp/ScpFileService;->o:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v0, v0, v3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jscape/inet/scp/ScpFileService;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/jscape/inet/scp/ScpFileService;->l:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionChannelConnection;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private f()V
    .locals 7

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/scp/ScpFileService;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/scp/ScpFileService;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/scp/ScpFileService;->o:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v0, v0, v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/jscape/inet/scp/ScpFileService;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    invoke-virtual {v6}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v6

    aput-object v6, v4, v5

    iget-object v5, p0, Lcom/jscape/inet/scp/ScpFileService;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private g()V
    .locals 6

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/scp/ScpFileService;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/inet/scp/ScpFileService;->j:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/inet/scp/ScpFileService;->o:[Ljava/lang/String;

    const/16 v3, 0xa

    aget-object v0, v0, v3

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jscape/inet/scp/ScpFileService;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/jscape/inet/scp/ScpFileService;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    invoke-virtual {v5}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected actualStart()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/inet/scp/ScpFileService;->h:Lcom/jscape/util/k/a/v;

    invoke-interface {v0}, Lcom/jscape/util/k/a/v;->connect()Lcom/jscape/util/k/a/r;

    move-result-object v0

    check-cast v0, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    iput-object v0, p0, Lcom/jscape/inet/scp/ScpFileService;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->f()V

    return-void
.end method

.method protected actualStop()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/scp/ScpFileService;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    invoke-virtual {v0}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->close()V

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->g()V

    return-void
.end method

.method public available()Z
    .locals 2

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/inet/scp/ScpFileService;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    invoke-virtual {v1}, Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;->closed()Z

    move-result v1

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public cancelTransferOperation()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->b()V

    return-void
.end method

.method public readDirectory(Ljava/lang/String;Ljava/io/File;ZLcom/jscape/inet/scp/FileService$TransferListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/scp/FileService$OperationException;
        }
    .end annotation

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->a()V

    new-instance v0, Lcom/jscape/inet/scp/protocol/messages/ReadDirectoryCommand;

    invoke-direct {v0, p1, p3}, Lcom/jscape/inet/scp/protocol/messages/ReadDirectoryCommand;-><init>(Ljava/lang/String;Z)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/scp/ScpFileService;->a(Lcom/jscape/inet/scp/protocol/messages/Command;)V

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->c()V

    invoke-direct {p0, p3}, Lcom/jscape/inet/scp/ScpFileService;->a(Z)Ljava/lang/Long;

    move-result-object p1

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->d()Lcom/jscape/inet/scp/protocol/messages/Message;

    move-result-object p3

    check-cast p3, Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;

    invoke-direct {p0, p3, p2, p4}, Lcom/jscape/inet/scp/ScpFileService;->a(Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;Ljava/io/File;Lcom/jscape/inet/scp/FileService$TransferListener;)Ljava/io/File;

    move-result-object p2

    invoke-direct {p0, p2, p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/io/File;Ljava/lang/Long;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->b()V

    return-void

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/scp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/scp/FileService$OperationException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->b()V

    throw p1
.end method

.method public readFile(Ljava/lang/String;Ljava/io/OutputStream;ZLcom/jscape/inet/scp/FileService$TransferListener;)Ljava/lang/Long;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/scp/FileService$OperationException;
        }
    .end annotation

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->a()V

    new-instance v0, Lcom/jscape/inet/scp/protocol/messages/ReadFileCommand;

    invoke-direct {v0, p1, p3}, Lcom/jscape/inet/scp/protocol/messages/ReadFileCommand;-><init>(Ljava/lang/String;Z)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/scp/ScpFileService;->a(Lcom/jscape/inet/scp/protocol/messages/Command;)V

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->c()V

    invoke-direct {p0, p3}, Lcom/jscape/inet/scp/ScpFileService;->a(Z)Ljava/lang/Long;

    move-result-object p1

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->d()Lcom/jscape/inet/scp/protocol/messages/Message;

    move-result-object p3

    check-cast p3, Lcom/jscape/inet/scp/protocol/messages/FileMessage;

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->c()V

    iget-object v0, p3, Lcom/jscape/inet/scp/protocol/messages/FileMessage;->fileName:Ljava/lang/String;

    iget-wide v1, p3, Lcom/jscape/inet/scp/protocol/messages/FileMessage;->fileLength:J

    invoke-interface {p4, v0, v1, v2}, Lcom/jscape/inet/scp/FileService$TransferListener;->onTransferStarted(Ljava/lang/String;J)V

    iget-object v4, p0, Lcom/jscape/inet/scp/ScpFileService;->m:Ljava/io/InputStream;

    iget-wide v6, p3, Lcom/jscape/inet/scp/protocol/messages/FileMessage;->fileLength:J

    move-object v3, p0

    move-object v5, p2

    move-object v8, p4

    invoke-direct/range {v3 .. v8}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/io/InputStream;Ljava/io/OutputStream;JLcom/jscape/inet/scp/FileService$TransferListener;)V

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->e()V

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->c()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->b()V

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/scp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/scp/FileService$OperationException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->b()V

    throw p1
.end method

.method public readFiles(Ljava/lang/String;Ljava/io/File;ZLcom/jscape/inet/scp/FileService$TransferListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/scp/FileService$OperationException;
        }
    .end annotation

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->a()V

    new-instance v0, Lcom/jscape/inet/scp/protocol/messages/ReadFilesCommand;

    invoke-direct {v0, p1, p3}, Lcom/jscape/inet/scp/protocol/messages/ReadFilesCommand;-><init>(Ljava/lang/String;Z)V

    invoke-direct {p0, v0}, Lcom/jscape/inet/scp/ScpFileService;->a(Lcom/jscape/inet/scp/protocol/messages/Command;)V

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->c()V

    const/4 p1, 0x0

    invoke-direct {p0, p2, p1, p4}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/io/File;ZLcom/jscape/inet/scp/FileService$TransferListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->b()V

    return-void

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/inet/scp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/scp/FileService$OperationException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->b()V

    throw p1
.end method

.method public sessionConnection()Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;
    .locals 1

    iget-object v0, p0, Lcom/jscape/inet/scp/ScpFileService;->k:Lcom/jscape/inet/ssh/protocol/v2/connection/SessionConnection;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/inet/scp/ScpFileService;->o:[Ljava/lang/String;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/inet/scp/ScpFileService;->h:Lcom/jscape/util/k/a/v;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/inet/scp/ScpFileService;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeDirectory(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/jscape/inet/scp/FileService$TransferListener;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/scp/FileService$OperationException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->a()V

    new-instance v1, Lcom/jscape/inet/scp/protocol/messages/WriteDirectoryCommand;

    invoke-direct {v1, p2, p5}, Lcom/jscape/inet/scp/protocol/messages/WriteDirectoryCommand;-><init>(Ljava/lang/String;Z)V

    invoke-direct {p0, v1}, Lcom/jscape/inet/scp/ScpFileService;->a(Lcom/jscape/inet/scp/protocol/messages/Command;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->e()V

    if-eqz p5, :cond_0

    new-instance p2, Lcom/jscape/inet/scp/protocol/messages/TimeMessage;

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    invoke-direct {p2, v0, v1, v2, v3}, Lcom/jscape/inet/scp/protocol/messages/TimeMessage;-><init>(JJ)V

    invoke-direct {p0, p2}, Lcom/jscape/inet/scp/ScpFileService;->c(Lcom/jscape/inet/scp/protocol/messages/Message;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    new-instance p2, Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz p4, :cond_1

    goto :goto_0

    :cond_1
    :try_start_3
    sget-object p4, Lcom/jscape/inet/scp/ScpFileService;->o:[Ljava/lang/String;

    const/4 v0, 0x6

    aget-object p4, p4, v0

    :goto_0
    invoke-direct {p2, p3, p4}, Lcom/jscape/inet/scp/protocol/messages/DirectoryMessage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/jscape/inet/scp/ScpFileService;->c(Lcom/jscape/inet/scp/protocol/messages/Message;)V

    invoke-direct {p0, p1, p5, p6}, Lcom/jscape/inet/scp/ScpFileService;->b(Ljava/io/File;ZLcom/jscape/inet/scp/FileService$TransferListener;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->b()V

    return-void

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_2
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_3
    move-exception p1

    :try_start_7
    invoke-static {p1}, Lcom/jscape/inet/scp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/scp/FileService$OperationException;

    move-result-object p1

    throw p1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :goto_1
    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->b()V

    throw p1
.end method

.method public writeFile(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/Long;Lcom/jscape/inet/scp/FileService$TransferListener;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/inet/scp/FileService$OperationException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->a()V

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v1, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string p2, "."
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    if-eqz p7, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    :try_start_2
    invoke-direct {v1, p2, v2}, Lcom/jscape/inet/scp/protocol/messages/WriteFileCommand;-><init>(Ljava/lang/String;Z)V

    invoke-direct {p0, v1}, Lcom/jscape/inet/scp/ScpFileService;->a(Lcom/jscape/inet/scp/protocol/messages/Command;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_2

    :try_start_3
    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->e()V

    if-eqz p7, :cond_2

    new-instance p2, Lcom/jscape/inet/scp/protocol/messages/TimeMessage;

    invoke-virtual {p7}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p7}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-direct {p2, v1, v2, v3, v4}, Lcom/jscape/inet/scp/protocol/messages/TimeMessage;-><init>(JJ)V

    invoke-direct {p0, p2}, Lcom/jscape/inet/scp/ScpFileService;->c(Lcom/jscape/inet/scp/protocol/messages/Message;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_2
    :goto_1
    :try_start_5
    new-instance p2, Lcom/jscape/inet/scp/protocol/messages/FileMessage;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz p6, :cond_3

    goto :goto_2

    :cond_3
    :try_start_6
    sget-object p6, Lcom/jscape/inet/scp/ScpFileService;->o:[Ljava/lang/String;

    const/16 p7, 0xc

    aget-object p6, p6, p7

    :goto_2
    invoke-direct {p2, p3, p4, p5, p6}, Lcom/jscape/inet/scp/protocol/messages/FileMessage;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/jscape/inet/scp/ScpFileService;->c(Lcom/jscape/inet/scp/protocol/messages/Message;)V

    invoke-interface {p8, p3, p4, p5}, Lcom/jscape/inet/scp/FileService$TransferListener;->onTransferStarted(Ljava/lang/String;J)V

    iget-object v3, p0, Lcom/jscape/inet/scp/ScpFileService;->n:Ljava/io/OutputStream;

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p4

    move-object v6, p8

    invoke-direct/range {v1 .. v6}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/io/InputStream;Ljava/io/OutputStream;JLcom/jscape/inet/scp/FileService$TransferListener;)V

    new-instance p1, Lcom/jscape/inet/scp/protocol/messages/OkMessage;

    invoke-direct {p1}, Lcom/jscape/inet/scp/protocol/messages/OkMessage;-><init>()V

    invoke-direct {p0, p1}, Lcom/jscape/inet/scp/ScpFileService;->c(Lcom/jscape/inet/scp/protocol/messages/Message;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->b()V

    if-nez v0, :cond_4

    const/4 p1, 0x2

    :try_start_7
    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_3

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_4
    :goto_3
    return-void

    :catch_2
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/ScpFileService;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_4
    move-exception p1

    :try_start_9
    invoke-static {p1}, Lcom/jscape/inet/scp/FileService$OperationException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/inet/scp/FileService$OperationException;

    move-result-object p1

    throw p1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :goto_4
    invoke-direct {p0}, Lcom/jscape/inet/scp/ScpFileService;->b()V

    throw p1
.end method
