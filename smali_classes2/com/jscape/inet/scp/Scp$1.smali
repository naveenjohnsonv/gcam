.class Lcom/jscape/inet/scp/Scp$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/nio/file/DirectoryStream$Filter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/nio/file/DirectoryStream$Filter<",
        "Ljava/nio/file/Path;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lcom/jscape/util/m/f;

.field final b:Lcom/jscape/inet/scp/Scp;


# direct methods
.method constructor <init>(Lcom/jscape/inet/scp/Scp;Lcom/jscape/util/m/f;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/inet/scp/Scp$1;->b:Lcom/jscape/inet/scp/Scp;

    iput-object p2, p0, Lcom/jscape/inet/scp/Scp$1;->a:Lcom/jscape/util/m/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Ljava/nio/file/Path;

    invoke-virtual {p0, p1}, Lcom/jscape/inet/scp/Scp$1;->accept(Ljava/nio/file/Path;)Z

    move-result p1

    return p1
.end method

.method public accept(Ljava/nio/file/Path;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/scp/ScpException;->b()[I

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    new-array v2, v1, [Ljava/nio/file/LinkOption;

    invoke-static {p1, v2}, Ljava/nio/file/Files;->isRegularFile(Ljava/nio/file/Path;[Ljava/nio/file/LinkOption;)Z

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    if-eqz v2, :cond_2

    :try_start_1
    iget-object v2, p0, Lcom/jscape/inet/scp/Scp$1;->a:Lcom/jscape/util/m/f;

    invoke-interface {p1}, Ljava/nio/file/Path;->getFileName()Ljava/nio/file/Path;

    move-result-object p1

    invoke-interface {p1}, Ljava/nio/file/Path;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v2, p1}, Lcom/jscape/util/m/f;->a(Ljava/lang/String;)Z

    move-result v2

    :cond_0
    if-eqz v0, :cond_1

    if-eqz v2, :cond_2

    const/4 p1, 0x1

    move v1, p1

    goto :goto_0

    :cond_1
    move v1, v2

    :cond_2
    :goto_0
    return v1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/Scp$1;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/inet/scp/Scp$1;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method
