.class public Lcom/jscape/util/J;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Enumeration;


# static fields
.field public static final a:Z = false

.field public static final b:I = 0x64

.field public static final c:I = 0x3e8

.field private static final p:[Ljava/lang/String;


# instance fields
.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:I

.field g:[I

.field h:Z

.field i:Z

.field j:Z

.field k:Ljava/lang/String;

.field l:Z

.field m:Ljava/io/File;

.field n:Z

.field o:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x3e

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0xa

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\t4\u000cX[__\u00008\u001bzVaQ\u001a9Xx[DC\u000b5XkH^R\u00024\u0015;S_\u0010*8\u000bpsEU\u001c0\u000ctH\u001d\u0010\u001b\"\u0011u]\u0011W\u000b%(zNY>\t4\u000cX[__\u00008\u001bzVaQ\u001a9Xx[DC\u000b5XkH^R\u00024\u0015;S_\u0010*8\u000bpsEU\u001c0\u000ctH\u001d\u0010\u001b\"\u0011u]\u0011W\u000b%(zNY"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x7d

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/J;->p:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x3a

    goto :goto_2

    :cond_2
    const/16 v12, 0x3b

    goto :goto_2

    :cond_3
    const/16 v12, 0x30

    goto :goto_2

    :cond_4
    const/16 v12, 0x11

    goto :goto_2

    :cond_5
    const/16 v12, 0x72

    goto :goto_2

    :cond_6
    const/16 v12, 0x5b

    goto :goto_2

    :cond_7
    const/16 v12, 0x64

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZZ)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/util/J;->d:Ljava/lang/String;

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v1

    iput-object v0, p0, Lcom/jscape/util/J;->e:Ljava/lang/String;

    const/4 v2, 0x0

    iput v2, p0, Lcom/jscape/util/J;->f:I

    const/16 v3, 0x64

    new-array v4, v3, [I

    iput-object v4, p0, Lcom/jscape/util/J;->g:[I

    iput-boolean v2, p0, Lcom/jscape/util/J;->l:Z

    iput-object v0, p0, Lcom/jscape/util/J;->m:Ljava/io/File;

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/jscape/util/J;->n:Z

    iput-object v0, p0, Lcom/jscape/util/J;->o:[Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/util/J;->d:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    sget-object p1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    sget-object v5, Lcom/jscape/util/J;->p:[Ljava/lang/String;

    aget-object v4, v5, v4

    invoke-virtual {p1, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/util/J;->d:Ljava/lang/String;

    :goto_0
    iget-object p1, p0, Lcom/jscape/util/J;->d:Ljava/lang/String;

    iput-object p1, p0, Lcom/jscape/util/J;->e:Ljava/lang/String;

    move p1, v2

    :cond_0
    if-ge p1, v3, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/jscape/util/J;->g:[I

    aput v2, v0, p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    add-int/lit8 p1, p1, 0x1

    if-nez v1, :cond_2

    if-eqz v1, :cond_0

    goto :goto_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    iput-object p2, p0, Lcom/jscape/util/J;->k:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/jscape/util/J;->h:Z

    iput-boolean p4, p0, Lcom/jscape/util/J;->i:Z

    iput-boolean p5, p0, Lcom/jscape/util/J;->j:Z

    :cond_2
    new-instance p1, Ljava/io/File;

    iget-object p2, p0, Lcom/jscape/util/J;->d:Ljava/lang/String;

    invoke-direct {p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/util/J;->o:[Ljava/lang/String;

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 9

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    :cond_0
    iget v1, p0, Lcom/jscape/util/J;->f:I

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eq v1, v2, :cond_10

    :try_start_0
    iget-boolean v5, p0, Lcom/jscape/util/J;->n:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_10

    if-nez v0, :cond_f

    if-nez v0, :cond_f

    if-ne v5, v4, :cond_10

    if-nez v0, :cond_4

    :try_start_1
    iget-object v5, p0, Lcom/jscape/util/J;->g:[I

    aget v5, v5, v1

    iget-object v6, p0, Lcom/jscape/util/J;->o:[Ljava/lang/String;

    array-length v6, v6
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_14

    if-ne v5, v6, :cond_4

    add-int/lit8 v1, v1, -0x1

    :try_start_2
    iput v1, p0, Lcom/jscape/util/J;->f:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_15

    if-nez v0, :cond_2

    if-gez v1, :cond_1

    goto/16 :goto_8

    :cond_1
    if-nez v0, :cond_3

    :try_start_3
    iget-object v1, p0, Lcom/jscape/util/J;->e:Ljava/lang/String;

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_2
    :goto_0
    if-eqz v1, :cond_3

    :try_start_4
    iget-object v1, p0, Lcom/jscape/util/J;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/jscape/util/J;->e:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v1, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/util/J;->e:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/jscape/util/J;->e:Ljava/lang/String;

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/util/J;->e:Ljava/lang/String;

    new-instance v1, Ljava/io/File;

    iget-object v5, p0, Lcom/jscape/util/J;->e:Ljava/lang/String;

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/util/J;->o:[Ljava/lang/String;

    if-eqz v0, :cond_0

    :cond_4
    iget-object v1, p0, Lcom/jscape/util/J;->o:[Ljava/lang/String;

    iget-object v5, p0, Lcom/jscape/util/J;->g:[I

    iget v6, p0, Lcom/jscape/util/J;->f:I

    aget v5, v5, v6

    aget-object v1, v1, v5

    new-instance v1, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/jscape/util/J;->e:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/jscape/util/J;->o:[Ljava/lang/String;

    iget-object v7, p0, Lcom/jscape/util/J;->g:[I

    iget v8, p0, Lcom/jscape/util/J;->f:I

    aget v7, v7, v8

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_5
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v5
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_a

    if-nez v0, :cond_a

    if-eqz v5, :cond_a

    :try_start_6
    iget-boolean v5, p0, Lcom/jscape/util/J;->h:Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_c

    if-nez v0, :cond_6

    if-nez v5, :cond_5

    :try_start_7
    iget-boolean v5, p0, Lcom/jscape/util/J;->i:Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_e

    if-nez v0, :cond_6

    if-nez v5, :cond_5

    :try_start_8
    iget-object v5, p0, Lcom/jscape/util/J;->g:[I

    iget v6, p0, Lcom/jscape/util/J;->f:I

    iget-object v7, p0, Lcom/jscape/util/J;->g:[I

    iget v8, p0, Lcom/jscape/util/J;->f:I

    aget v7, v7, v8

    add-int/2addr v7, v4

    aput v7, v5, v6
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_f

    if-eqz v0, :cond_0

    :cond_5
    iget-boolean v5, p0, Lcom/jscape/util/J;->h:Z

    :cond_6
    if-nez v0, :cond_8

    if-eqz v5, :cond_7

    :try_start_9
    iget-boolean v5, p0, Lcom/jscape/util/J;->i:Z
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    if-nez v0, :cond_8

    if-nez v5, :cond_7

    :try_start_a
    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/jscape/util/J;->e:Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2

    goto :goto_2

    :catch_2
    sget-object v5, Ljava/lang/System;->err:Ljava/io/PrintStream;

    sget-object v6, Lcom/jscape/util/J;->p:[Ljava/lang/String;

    aget-object v6, v6, v3

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/jscape/util/J;->e:Ljava/lang/String;

    :goto_2
    iget-object v5, p0, Lcom/jscape/util/J;->g:[I

    iget v6, p0, Lcom/jscape/util/J;->f:I

    aget v7, v5, v6

    add-int/2addr v7, v4

    aput v7, v5, v6

    add-int/2addr v6, v4

    iput v6, p0, Lcom/jscape/util/J;->f:I

    aput v3, v5, v6

    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lcom/jscape/util/J;->e:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/jscape/util/J;->o:[Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_7
    :goto_3
    if-nez v0, :cond_9

    :try_start_b
    iget-boolean v5, p0, Lcom/jscape/util/J;->h:Z
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4

    goto :goto_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_8
    :goto_4
    if-nez v5, :cond_9

    if-nez v0, :cond_9

    :try_start_c
    iget-boolean v5, p0, Lcom/jscape/util/J;->i:Z

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/jscape/util/J;->g:[I

    iget v6, p0, Lcom/jscape/util/J;->f:I

    iget-object v7, p0, Lcom/jscape/util/J;->g:[I

    iget v8, p0, Lcom/jscape/util/J;->f:I

    aget v7, v7, v8

    add-int/2addr v7, v4

    aput v7, v5, v6

    iput-object v1, p0, Lcom/jscape/util/J;->m:Ljava/io/File;

    iput-boolean v3, p0, Lcom/jscape/util/J;->n:Z

    iput-boolean v4, p0, Lcom/jscape/util/J;->l:Z
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5

    if-eqz v0, :cond_e

    goto :goto_5

    :catch_5
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_9
    :goto_5
    :try_start_d
    invoke-virtual {v1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/util/J;->e:Ljava/lang/String;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_6

    goto :goto_6

    :catch_6
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/util/J;->e:Ljava/lang/String;

    :goto_6
    iget-object v0, p0, Lcom/jscape/util/J;->g:[I

    iget v2, p0, Lcom/jscape/util/J;->f:I

    aget v5, v0, v2

    add-int/2addr v5, v4

    aput v5, v0, v2

    iput-object v1, p0, Lcom/jscape/util/J;->m:Ljava/io/File;

    iput-boolean v3, p0, Lcom/jscape/util/J;->n:Z

    iput-boolean v4, p0, Lcom/jscape/util/J;->l:Z

    add-int/2addr v2, v4

    iput v2, p0, Lcom/jscape/util/J;->f:I

    aput v3, v0, v2

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/jscape/util/J;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/util/J;->o:[Ljava/lang/String;

    return-void

    :cond_a
    :try_start_e
    iget-boolean v5, p0, Lcom/jscape/util/J;->j:Z
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_9

    if-nez v0, :cond_c

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/jscape/util/J;->k:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/jscape/util/J;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v0, :cond_c

    :cond_b
    :try_start_f
    iget-object v5, p0, Lcom/jscape/util/J;->k:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/jscape/util/J;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_7

    goto :goto_7

    :catch_7
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_c
    :goto_7
    if-eqz v5, :cond_d

    :try_start_10
    iget-object v0, p0, Lcom/jscape/util/J;->g:[I

    iget v2, p0, Lcom/jscape/util/J;->f:I

    iget-object v5, p0, Lcom/jscape/util/J;->g:[I

    iget v6, p0, Lcom/jscape/util/J;->f:I

    aget v5, v5, v6

    add-int/2addr v5, v4

    aput v5, v0, v2

    iput-object v1, p0, Lcom/jscape/util/J;->m:Ljava/io/File;

    iput-boolean v3, p0, Lcom/jscape/util/J;->n:Z

    iput-boolean v4, p0, Lcom/jscape/util/J;->l:Z
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_8

    return-void

    :catch_8
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_d
    iget-object v1, p0, Lcom/jscape/util/J;->g:[I

    iget v5, p0, Lcom/jscape/util/J;->f:I

    aget v6, v1, v5

    add-int/2addr v6, v4

    aput v6, v1, v5

    if-eqz v0, :cond_0

    :cond_e
    if-eqz v0, :cond_0

    goto :goto_8

    :catch_9
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_a
    move-exception v0

    :try_start_11
    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_b

    :catch_b
    move-exception v0

    :try_start_12
    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_c

    :catch_c
    move-exception v0

    :try_start_13
    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_d

    :catch_d
    move-exception v0

    :try_start_14
    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_e

    :catch_e
    move-exception v0

    :try_start_15
    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_f

    :catch_f
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_f
    move v2, v4

    goto :goto_9

    :catch_10
    move-exception v0

    :try_start_16
    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_11

    :catch_11
    move-exception v0

    :try_start_17
    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_12

    :catch_12
    move-exception v0

    :try_start_18
    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_13

    :catch_13
    move-exception v0

    :try_start_19
    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_14

    :catch_14
    move-exception v0

    :try_start_1a
    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_15

    :catch_15
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_10
    :goto_8
    if-nez v0, :cond_11

    :try_start_1b
    iget v5, p0, Lcom/jscape/util/J;->f:I
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_17

    :goto_9
    if-ne v5, v2, :cond_12

    const/4 v0, 0x0

    :try_start_1c
    iput-object v0, p0, Lcom/jscape/util/J;->m:Ljava/io/File;

    iput-boolean v3, p0, Lcom/jscape/util/J;->l:Z
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_16

    goto :goto_a

    :catch_16
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_17
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/J;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_11
    :goto_a
    iput-boolean v3, p0, Lcom/jscape/util/J;->n:Z

    :cond_12
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :cond_0
    :goto_0
    move v3, v1

    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-lt v3, v4, :cond_2

    move v4, v5

    goto :goto_1

    :cond_2
    move v4, v1

    :goto_1
    const/16 v6, 0x7c

    if-nez v0, :cond_5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v2, v7, :cond_4

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-nez v0, :cond_6

    if-ne v7, v6, :cond_3

    goto :goto_2

    :cond_3
    move v7, v1

    goto :goto_3

    :cond_4
    :goto_2
    move v7, v5

    goto :goto_3

    :cond_5
    move v7, v2

    :cond_6
    :goto_3
    if-nez v0, :cond_8

    if-eqz v4, :cond_8

    if-nez v0, :cond_7

    if-eqz v7, :cond_8

    return v5

    :cond_7
    move v4, v7

    :cond_8
    if-nez v0, :cond_15

    if-nez v4, :cond_14

    if-nez v0, :cond_a

    if-eqz v7, :cond_9

    goto :goto_7

    :cond_9
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    :cond_a
    const/16 v4, 0x3f

    if-nez v0, :cond_c

    if-ne v7, v4, :cond_b

    goto :goto_6

    :cond_b
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v4, 0x2a

    :cond_c
    if-nez v0, :cond_12

    if-ne v7, v4, :cond_11

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    :cond_d
    if-lt v4, v3, :cond_14

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lcom/jscape/util/J;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v0, :cond_10

    if-nez v0, :cond_f

    if-eqz v7, :cond_e

    goto :goto_4

    :cond_e
    add-int/lit8 v4, v4, -0x1

    if-eqz v0, :cond_d

    goto :goto_5

    :cond_f
    move v5, v7

    :goto_4
    return v5

    :cond_10
    move v4, v7

    goto :goto_8

    :cond_11
    :goto_5
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {p2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    :cond_12
    if-eq v7, v4, :cond_13

    goto :goto_7

    :cond_13
    :goto_6
    add-int/2addr v2, v5

    add-int/lit8 v3, v3, 0x1

    if-eqz v0, :cond_1

    :cond_14
    :goto_7
    invoke-virtual {p1, v6, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    if-nez v0, :cond_0

    move v4, v2

    :cond_15
    :goto_8
    const/4 v3, -0x1

    if-ne v4, v3, :cond_16

    return v1

    :cond_16
    add-int/2addr v2, v5

    goto/16 :goto_0
.end method

.method public hasMoreElements()Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    iget-boolean v1, p0, Lcom/jscape/util/J;->n:Z

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/jscape/util/J;->a()V

    :cond_0
    iget-boolean v1, p0, Lcom/jscape/util/J;->l:Z

    :cond_1
    return v1
.end method

.method public nextElement()Ljava/lang/Object;
    .locals 1

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/jscape/util/J;->n:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/jscape/util/J;->a()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/util/J;->n:Z

    :cond_1
    iget-object v0, p0, Lcom/jscape/util/J;->m:Ljava/io/File;

    return-object v0
.end method
