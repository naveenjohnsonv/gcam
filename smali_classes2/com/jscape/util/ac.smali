.class public Lcom/jscape/util/ac;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/K;


# static fields
.field public static final a:Lcom/jscape/util/K;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jscape/util/ac;

    invoke-direct {v0}, Lcom/jscape/util/ac;-><init>()V

    sput-object v0, Lcom/jscape/util/ac;->a:Lcom/jscape/util/K;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a([BI)Z
    .locals 1

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    aget-byte p1, p1, p2

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    return p1
.end method

.method public a(C[BI)[B
    .locals 1

    int-to-byte v0, p1

    aput-byte v0, p2, p3

    add-int/lit8 p3, p3, 0x1

    ushr-int/lit8 p1, p1, 0x8

    int-to-byte p1, p1

    aput-byte p1, p2, p3

    return-object p2
.end method

.method public a(D[BI)[B
    .locals 0

    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide p1

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/jscape/util/ac;->a(J[BI)[B

    move-result-object p1

    return-object p1
.end method

.method public a(F[BI)[B
    .locals 0

    invoke-static {p1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result p1

    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/util/ac;->a(I[BI)[B

    move-result-object p1

    return-object p1
.end method

.method public a(I[BI)[B
    .locals 2

    int-to-byte v0, p1

    aput-byte v0, p2, p3

    add-int/lit8 v0, p3, 0x1

    ushr-int/lit8 v1, p1, 0x8

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    add-int/lit8 v0, p3, 0x2

    ushr-int/lit8 v1, p1, 0x10

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    add-int/lit8 p3, p3, 0x3

    ushr-int/lit8 p1, p1, 0x18

    int-to-byte p1, p1

    aput-byte p1, p2, p3

    return-object p2
.end method

.method public a(J[BI)[B
    .locals 3

    long-to-int v0, p1

    int-to-byte v0, v0

    aput-byte v0, p3, p4

    add-int/lit8 v0, p4, 0x1

    const/16 v1, 0x8

    ushr-long v1, p1, v1

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p3, v0

    add-int/lit8 v0, p4, 0x2

    const/16 v1, 0x10

    ushr-long v1, p1, v1

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p3, v0

    add-int/lit8 v0, p4, 0x3

    const/16 v1, 0x18

    ushr-long v1, p1, v1

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p3, v0

    add-int/lit8 v0, p4, 0x4

    const/16 v1, 0x20

    ushr-long v1, p1, v1

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p3, v0

    add-int/lit8 v0, p4, 0x5

    const/16 v1, 0x28

    ushr-long v1, p1, v1

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p3, v0

    add-int/lit8 v0, p4, 0x6

    const/16 v1, 0x30

    ushr-long v1, p1, v1

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, p3, v0

    add-int/lit8 p4, p4, 0x7

    const/16 v0, 0x38

    ushr-long/2addr p1, v0

    long-to-int p1, p1

    int-to-byte p1, p1

    aput-byte p1, p3, p4

    return-object p3
.end method

.method public a(S[BI)[B
    .locals 1

    int-to-byte v0, p1

    aput-byte v0, p2, p3

    add-int/lit8 p3, p3, 0x1

    ushr-int/lit8 p1, p1, 0x8

    int-to-byte p1, p1

    aput-byte p1, p2, p3

    return-object p2
.end method

.method public a(Z[BI)[B
    .locals 1

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    int-to-byte p1, p1

    aput-byte p1, p2, p3

    return-object p2
.end method

.method public b([BI)C
    .locals 1

    aget-byte v0, p1, p2

    and-int/lit16 v0, v0, 0xff

    int-to-char v0, v0

    add-int/lit8 p2, p2, 0x1

    aget-byte p1, p1, p2

    and-int/lit16 p1, p1, 0xff

    shl-int/lit8 p1, p1, 0x8

    or-int/2addr p1, v0

    int-to-char p1, p1

    return p1
.end method

.method public c([BI)S
    .locals 1

    aget-byte v0, p1, p2

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    add-int/lit8 p2, p2, 0x1

    aget-byte p1, p1, p2

    and-int/lit16 p1, p1, 0xff

    shl-int/lit8 p1, p1, 0x8

    or-int/2addr p1, v0

    int-to-short p1, p1

    return p1
.end method

.method public d([BI)I
    .locals 2

    aget-byte v0, p1, p2

    and-int/lit16 v0, v0, 0xff

    add-int/lit8 v1, p2, 0x1

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    add-int/lit8 v1, p2, 0x2

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    add-int/lit8 p2, p2, 0x3

    aget-byte p1, p1, p2

    and-int/lit16 p1, p1, 0xff

    shl-int/lit8 p1, p1, 0x18

    or-int/2addr p1, v0

    return p1
.end method

.method public e([BI)J
    .locals 8

    aget-byte v0, p1, p2

    int-to-long v0, v0

    const-wide/16 v2, 0xff

    and-long/2addr v0, v2

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v4

    add-int/lit8 v5, p2, 0x6

    aget-byte v5, p1, v5

    int-to-long v5, v5

    and-long/2addr v5, v2

    const/16 v7, 0x8

    shl-long/2addr v5, v7

    or-long/2addr v0, v5

    add-int/lit8 v5, p2, 0x5

    aget-byte v5, p1, v5

    int-to-long v5, v5

    and-long/2addr v5, v2

    const/16 v7, 0x10

    shl-long/2addr v5, v7

    or-long/2addr v0, v5

    add-int/lit8 v5, p2, 0x4

    aget-byte v5, p1, v5

    int-to-long v5, v5

    and-long/2addr v5, v2

    const/16 v7, 0x18

    shl-long/2addr v5, v7

    or-long/2addr v0, v5

    add-int/lit8 v5, p2, 0x3

    aget-byte v5, p1, v5

    int-to-long v5, v5

    and-long/2addr v5, v2

    const/16 v7, 0x20

    shl-long/2addr v5, v7

    or-long/2addr v0, v5

    add-int/lit8 v5, p2, 0x2

    aget-byte v5, p1, v5

    int-to-long v5, v5

    and-long/2addr v5, v2

    const/16 v7, 0x28

    shl-long/2addr v5, v7

    or-long/2addr v0, v5

    add-int/lit8 v5, p2, 0x1

    aget-byte v5, p1, v5

    int-to-long v5, v5

    and-long/2addr v5, v2

    const/16 v7, 0x30

    shl-long/2addr v5, v7

    or-long/2addr v0, v5

    add-int/lit8 p2, p2, 0x7

    aget-byte p1, p1, p2

    int-to-long p1, p1

    and-long/2addr p1, v2

    const/16 v2, 0x38

    shl-long/2addr p1, v2

    or-long/2addr p1, v0

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-wide p1
.end method

.method public f([BI)F
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/jscape/util/ac;->d([BI)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result p1

    return p1
.end method

.method public g([BI)D
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/jscape/util/ac;->e([BI)J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide p1

    return-wide p1
.end method
