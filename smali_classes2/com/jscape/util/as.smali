.class public Lcom/jscape/util/as;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private a:Lcom/jscape/util/I;


# direct methods
.method public constructor <init>(JJ)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v6, Lcom/jscape/util/I;

    const/4 v5, 0x1

    move-object v0, v6

    move-wide v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/jscape/util/I;-><init>(JJZ)V

    iput-object v6, p0, Lcom/jscape/util/as;->a:Lcom/jscape/util/I;

    return-void
.end method

.method public constructor <init>(Lcom/jscape/util/as;)V
    .locals 4

    invoke-virtual {p1}, Lcom/jscape/util/as;->a()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/jscape/util/as;->b()J

    move-result-wide v2

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/jscape/util/as;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/as;->a:Lcom/jscape/util/I;

    invoke-virtual {v0}, Lcom/jscape/util/I;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public b()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/as;->a:Lcom/jscape/util/I;

    invoke-virtual {v0}, Lcom/jscape/util/I;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized c()J
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/as;->a:Lcom/jscape/util/I;

    invoke-virtual {v0}, Lcom/jscape/util/I;->d()J

    move-result-wide v0

    iget-object v2, p0, Lcom/jscape/util/as;->a:Lcom/jscape/util/I;

    invoke-virtual {v2}, Lcom/jscape/util/I;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()J
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/as;->a:Lcom/jscape/util/I;

    invoke-virtual {v0}, Lcom/jscape/util/I;->d()J

    move-result-wide v0

    iget-object v2, p0, Lcom/jscape/util/as;->a:Lcom/jscape/util/I;

    invoke-virtual {v2}, Lcom/jscape/util/I;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    if-ne p0, p1, :cond_0

    return v1

    :cond_0
    move-object v2, p1

    goto :goto_0

    :cond_1
    move-object v2, p0

    :goto_0
    const/4 v3, 0x0

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_1

    :cond_2
    move-object p1, v2

    :cond_3
    check-cast p1, Lcom/jscape/util/as;

    iget-object v2, p0, Lcom/jscape/util/as;->a:Lcom/jscape/util/I;

    iget-object p1, p1, Lcom/jscape/util/as;->a:Lcom/jscape/util/I;

    invoke-virtual {v2, p1}, Lcom/jscape/util/I;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz v0, :cond_4

    if-nez p1, :cond_5

    return v3

    :cond_4
    move v1, p1

    :cond_5
    return v1

    :cond_6
    :goto_1
    return v3
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/as;->a:Lcom/jscape/util/I;

    invoke-virtual {v0}, Lcom/jscape/util/I;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/as;->a:Lcom/jscape/util/I;

    invoke-virtual {v0}, Lcom/jscape/util/I;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
