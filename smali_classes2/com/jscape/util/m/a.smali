.class public final enum Lcom/jscape/util/m/a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/util/m/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/jscape/util/m/a;

.field public static final enum b:Lcom/jscape/util/m/a;

.field private static final e:[Lcom/jscape/util/m/a;

.field private static final f:[Ljava/lang/String;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Lcom/jscape/util/m/g;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "\u0015NM\u0007\u0018_i\u0018ZW\u0007\u0015Ri\u0007_I\u0003\u0014^e\u0010A\u0008\u0008fw\"$l^;\u0008\u0008FW\u0002\u0004L~\u001b"

    const/16 v5, 0x29

    const/16 v6, 0x17

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/4 v9, 0x1

    add-int/2addr v7, v9

    add-int v10, v7, v6

    invoke-virtual {v4, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x3

    move v13, v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v14, v10

    move v15, v3

    :goto_2
    const/4 v2, 0x4

    const/4 v0, 0x2

    if-gt v14, v15, :cond_3

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v13}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v12, :cond_1

    add-int/lit8 v0, v8, 0x1

    aput-object v10, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v0

    const/4 v0, 0x5

    goto :goto_0

    :cond_0
    const/16 v5, 0x35

    const/16 v2, 0x22

    const-string v4, "\u0003ha:>tJ$rw+na]&tw<=mJ8&f6>a\u00058g\u007f*t$\u0012\u0004cu:\"eWvCj?<aV%o}!"

    move v8, v0

    move v6, v2

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v8, 0x1

    aput-object v10, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v6, v0

    move v8, v12

    :goto_3
    const/16 v13, 0xa

    add-int/2addr v7, v9

    add-int v0, v7, v6

    invoke-virtual {v4, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v12, v3

    const/4 v0, 0x5

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/m/a;->f:[Ljava/lang/String;

    new-instance v1, Lcom/jscape/util/m/a;

    sget-object v4, Lcom/jscape/util/m/a;->f:[Ljava/lang/String;

    aget-object v5, v4, v3

    aget-object v2, v4, v2

    new-instance v6, Lcom/jscape/util/m/d;

    invoke-direct {v6}, Lcom/jscape/util/m/d;-><init>()V

    invoke-virtual {v6}, Lcom/jscape/util/m/d;->a()Lcom/jscape/util/m/d;

    move-result-object v6

    invoke-direct {v1, v5, v3, v2, v6}, Lcom/jscape/util/m/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/jscape/util/m/g;)V

    sput-object v1, Lcom/jscape/util/m/a;->a:Lcom/jscape/util/m/a;

    new-instance v1, Lcom/jscape/util/m/a;

    aget-object v2, v4, v0

    aget-object v4, v4, v9

    new-instance v5, Lcom/jscape/util/m/h;

    invoke-direct {v5}, Lcom/jscape/util/m/h;-><init>()V

    invoke-direct {v1, v2, v9, v4, v5}, Lcom/jscape/util/m/a;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/jscape/util/m/g;)V

    sput-object v1, Lcom/jscape/util/m/a;->b:Lcom/jscape/util/m/a;

    new-array v0, v0, [Lcom/jscape/util/m/a;

    sget-object v2, Lcom/jscape/util/m/a;->a:Lcom/jscape/util/m/a;

    aput-object v2, v0, v3

    aput-object v1, v0, v9

    sput-object v0, Lcom/jscape/util/m/a;->e:[Lcom/jscape/util/m/a;

    return-void

    :cond_3
    aget-char v16, v10, v15

    rem-int/lit8 v3, v15, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v9, :cond_8

    if-eq v3, v0, :cond_7

    const/4 v0, 0x5

    if-eq v3, v11, :cond_6

    if-eq v3, v2, :cond_5

    if-eq v3, v0, :cond_4

    const/16 v2, 0x2f

    goto :goto_4

    :cond_4
    const/16 v2, 0xe

    goto :goto_4

    :cond_5
    const/16 v2, 0x44

    goto :goto_4

    :cond_6
    const/16 v2, 0x45

    goto :goto_4

    :cond_7
    const/4 v0, 0x5

    const/16 v2, 0x18

    goto :goto_4

    :cond_8
    const/4 v0, 0x5

    const/16 v2, 0xc

    goto :goto_4

    :cond_9
    const/4 v0, 0x5

    const/16 v2, 0x5c

    :goto_4
    xor-int/2addr v2, v13

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v10, v15

    add-int/lit8 v15, v15, 0x1

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/jscape/util/m/g;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/jscape/util/m/g;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/jscape/util/m/a;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/jscape/util/m/a;->d:Lcom/jscape/util/m/g;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/util/m/a;
    .locals 1

    const-class v0, Lcom/jscape/util/m/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/util/m/a;

    return-object p0
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method

.method public static a()[Lcom/jscape/util/m/a;
    .locals 1

    sget-object v0, Lcom/jscape/util/m/a;->e:[Lcom/jscape/util/m/a;

    invoke-virtual {v0}, [Lcom/jscape/util/m/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/util/m/a;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lcom/jscape/util/m/a;
    .locals 7

    invoke-static {}, Lcom/jscape/util/m/a;->a()[Lcom/jscape/util/m/a;

    move-result-object v0

    array-length v1, v0

    invoke-static {}, Lcom/jscape/util/m/d;->b()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x3

    if-ge v3, v1, :cond_3

    aget-object v5, v0, v3

    if-nez v2, :cond_1

    :try_start_0
    iget-object v6, v5, Lcom/jscape/util/m/a;->c:Ljava/lang/String;

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v6, :cond_0

    return-object v5

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/util/m/a;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/m/a;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_1
    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    new-array v0, v4, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/util/m/a;->f:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 0

    :try_start_0
    invoke-virtual {p0, p2}, Lcom/jscape/util/m/a;->c(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/jscape/util/m/f;->a(Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Lcom/jscape/util/m/b; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    const/4 p1, 0x0

    return p1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/m/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/jscape/util/m/f;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/m/b;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/m/a;->d:Lcom/jscape/util/m/g;

    invoke-interface {v0, p1}, Lcom/jscape/util/m/g;->a(Ljava/lang/String;)Lcom/jscape/util/m/f;

    move-result-object p1

    return-object p1
.end method
