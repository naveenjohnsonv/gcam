.class public Lcom/jscape/util/m/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/FileFilter;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/util/m/f;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "@Tx\u000e\u001e|\u0014eUq\u0019\u0015t\u000crXfK(p\u0001r^|\u000e! "

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/m/c;->b:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x51

    goto :goto_1

    :cond_1
    const/16 v4, 0x2c

    goto :goto_1

    :cond_2
    const/16 v4, 0x62

    goto :goto_1

    :cond_3
    const/16 v4, 0x5a

    goto :goto_1

    :cond_4
    const/16 v4, 0x25

    goto :goto_1

    :cond_5
    const/16 v4, 0xc

    goto :goto_1

    :cond_6
    const/16 v4, 0x37

    :goto_1
    const/16 v5, 0x31

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/jscape/util/m/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/m/c;->a:Lcom/jscape/util/m/f;

    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;)Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/m/d;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v1

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jscape/util/m/c;->a:Lcom/jscape/util/m/f;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/jscape/util/m/f;->a(Ljava/lang/String;)Z

    move-result v1

    :cond_0
    if-nez v0, :cond_2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move p1, v1

    :goto_1
    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/util/m/d;->b()[Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/m/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/m/c;->a:Lcom/jscape/util/m/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v1}, Lcom/jscape/util/m/d;->b([Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method
