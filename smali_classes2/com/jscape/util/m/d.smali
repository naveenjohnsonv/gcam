.class public Lcom/jscape/util/m/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/m/g;


# static fields
.field private static b:[Ljava/lang/String;


# instance fields
.field private a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/util/m/d;->b()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {v0}, Lcom/jscape/util/m/d;->b([Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/jscape/util/m/d;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jscape/util/m/d;->a:I

    return-void
.end method

.method public static b([Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/util/m/d;->b:[Ljava/lang/String;

    return-void
.end method

.method public static b()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/util/m/d;->b:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/jscape/util/m/d;
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, Lcom/jscape/util/m/d;->a:I

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/jscape/util/m/f;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/m/b;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lcom/jscape/util/m/e;

    iget v1, p0, Lcom/jscape/util/m/d;->a:I

    invoke-static {p1, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/jscape/util/m/e;-><init>(Ljava/util/regex/Pattern;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    new-instance v0, Lcom/jscape/util/m/b;

    invoke-direct {v0, p1}, Lcom/jscape/util/m/b;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method
