.class public Lcom/jscape/util/B;
.super Ljava/lang/Object;


# instance fields
.field private a:[B

.field private b:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/jscape/util/B;->a:[B

    const/4 p1, 0x0

    iput p1, p0, Lcom/jscape/util/B;->b:I

    return-void
.end method


# virtual methods
.method public a(IILjava/nio/charset/Charset;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/util/B;->a:[B

    invoke-direct {v0, v1, p1, p2, p3}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    return-object v0
.end method

.method public a(ILjava/nio/charset/Charset;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/jscape/util/B;->b()I

    move-result v0

    sub-int/2addr v0, p1

    invoke-virtual {p0, p1, v0, p2}, Lcom/jscape/util/B;->a(IILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/nio/charset/Charset;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/jscape/util/B;->b()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0, p1}, Lcom/jscape/util/B;->a(IILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a(B)V
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/jscape/util/B;->b(I)V

    iget-object v1, p0, Lcom/jscape/util/B;->a:[B

    iget v2, p0, Lcom/jscape/util/B;->b:I

    aput-byte p1, v1, v2

    add-int/2addr v2, v0

    iput v2, p0, Lcom/jscape/util/B;->b:I

    return-void
.end method

.method public a(BI)V
    .locals 2

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    iget v1, p0, Lcom/jscape/util/B;->b:I

    if-ge p2, v1, :cond_0

    iget-object v1, p0, Lcom/jscape/util/B;->a:[B

    aput-byte p1, v1, p2

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/jscape/util/B;->a(B)V

    :cond_1
    return-void
.end method

.method public a(I)V
    .locals 1

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jscape/util/B;->a:[B

    array-length v0, v0

    if-ge v0, p1, :cond_0

    iget v0, p0, Lcom/jscape/util/B;->b:I

    sub-int v0, p1, v0

    invoke-virtual {p0, v0}, Lcom/jscape/util/B;->b(I)V

    :cond_0
    iput p1, p0, Lcom/jscape/util/B;->b:I

    return-void
.end method

.method public a(II)V
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/B;->a:[B

    iget v1, p0, Lcom/jscape/util/B;->b:I

    sub-int/2addr v1, p2

    invoke-static {v0, p2, v0, p1, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lcom/jscape/util/B;->b:I

    sub-int/2addr p2, p1

    sub-int/2addr v0, p2

    iput v0, p0, Lcom/jscape/util/B;->b:I

    return-void
.end method

.method public a(I[BII)V
    .locals 4

    add-int v0, p1, p4

    iget v1, p0, Lcom/jscape/util/B;->b:I

    sub-int v2, v1, p1

    add-int v3, p4, v2

    add-int/2addr v3, p1

    sub-int/2addr v3, v1

    invoke-virtual {p0, v3}, Lcom/jscape/util/B;->b(I)V

    iget-object v1, p0, Lcom/jscape/util/B;->a:[B

    invoke-static {v1, p1, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/jscape/util/B;->a:[B

    invoke-static {p2, p3, v0, p1, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method public a([B)V
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/jscape/util/B;->a([BII)V

    return-void
.end method

.method public a([BII)V
    .locals 2

    invoke-virtual {p0, p3}, Lcom/jscape/util/B;->b(I)V

    iget-object v0, p0, Lcom/jscape/util/B;->a:[B

    iget v1, p0, Lcom/jscape/util/B;->b:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p1, p0, Lcom/jscape/util/B;->b:I

    add-int/2addr p1, p3

    iput p1, p0, Lcom/jscape/util/B;->b:I

    return-void
.end method

.method public a([BIII)V
    .locals 2

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    iget v1, p0, Lcom/jscape/util/B;->b:I

    sub-int/2addr v1, p4

    sub-int v1, p3, v1

    if-nez v0, :cond_1

    if-lez v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/jscape/util/B;->b(I)V

    :cond_0
    iget-object v0, p0, Lcom/jscape/util/B;->a:[B

    invoke-static {p1, p2, v0, p4, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p4, p3

    iput p4, p0, Lcom/jscape/util/B;->b:I

    :cond_1
    return-void
.end method

.method public a()[B
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/B;->a:[B

    return-object v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/jscape/util/B;->b:I

    return v0
.end method

.method public b(B)I
    .locals 4

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    const/4 v1, 0x0

    :cond_0
    iget-object v2, p0, Lcom/jscape/util/B;->a:[B

    array-length v3, v2

    if-ge v1, v3, :cond_3

    if-nez v0, :cond_4

    if-nez v0, :cond_2

    aget-byte v2, v2, v1

    if-ne p1, v2, :cond_1

    move p1, v1

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_2
    :goto_0
    return p1

    :cond_3
    :goto_1
    const/4 p1, -0x1

    :cond_4
    return p1
.end method

.method public b(I)V
    .locals 5

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    invoke-virtual {p0}, Lcom/jscape/util/B;->d()I

    move-result v1

    if-nez v0, :cond_1

    if-le v1, p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/jscape/util/B;->d()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, p1

    move p1, v1

    :goto_0
    sub-int/2addr p1, v0

    iget-object v0, p0, Lcom/jscape/util/B;->a:[B

    array-length v1, v0

    add-int/2addr v1, p1

    int-to-double v1, v1

    const-wide/high16 v3, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v1, v3

    double-to-int p1, v1

    new-array p1, p1, [B

    iget v1, p0, Lcom/jscape/util/B;->b:I

    const/4 v2, 0x0

    invoke-static {v0, v2, p1, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object p1, p0, Lcom/jscape/util/B;->a:[B

    return-void
.end method

.method public b([B)Z
    .locals 5

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    invoke-virtual {p0}, Lcom/jscape/util/B;->b()I

    move-result v1

    const/4 v2, 0x0

    if-nez v0, :cond_1

    array-length v3, p1

    if-ge v1, v3, :cond_0

    return v2

    :cond_0
    move v1, v2

    :cond_1
    array-length v3, p1

    if-ge v1, v3, :cond_4

    aget-byte v3, p1, v1

    if-nez v0, :cond_5

    if-nez v0, :cond_3

    invoke-virtual {p0, v1}, Lcom/jscape/util/B;->c(I)B

    move-result v4

    if-eq v3, v4, :cond_2

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_3
    move v2, v3

    :goto_0
    return v2

    :cond_4
    :goto_1
    const/4 v3, 0x1

    :cond_5
    return v3
.end method

.method public b(II)[B
    .locals 3

    new-array v0, p2, [B

    iget-object v1, p0, Lcom/jscape/util/B;->a:[B

    const/4 v2, 0x0

    invoke-static {v1, p1, v0, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public c(I)B
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/B;->a:[B

    aget-byte p1, v0, p1

    return p1
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/B;->a:[B

    array-length v0, v0

    return v0
.end method

.method public c(B)I
    .locals 4

    iget-object v0, p0, Lcom/jscape/util/B;->a:[B

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v1

    move v2, v0

    :cond_0
    if-lt v2, v0, :cond_3

    if-nez v1, :cond_4

    if-nez v1, :cond_2

    iget-object v3, p0, Lcom/jscape/util/B;->a:[B

    aget-byte v3, v3, v2

    if-ne p1, v3, :cond_1

    move p1, v2

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v2, -0x1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_2
    :goto_0
    return p1

    :cond_3
    :goto_1
    const/4 p1, -0x1

    :cond_4
    return p1
.end method

.method public c([B)Z
    .locals 6

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    invoke-virtual {p0}, Lcom/jscape/util/B;->b()I

    move-result v1

    const/4 v2, 0x0

    if-nez v0, :cond_1

    array-length v3, p1

    if-ge v1, v3, :cond_0

    return v2

    :cond_0
    move v1, v2

    :cond_1
    invoke-virtual {p0}, Lcom/jscape/util/B;->b()I

    move-result v3

    array-length v4, p1

    sub-int/2addr v3, v4

    :cond_2
    array-length v4, p1

    if-ge v1, v4, :cond_5

    aget-byte v4, p1, v1

    if-nez v0, :cond_6

    if-nez v0, :cond_4

    invoke-virtual {p0, v3}, Lcom/jscape/util/B;->c(I)B

    move-result v5

    if-eq v4, v5, :cond_3

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v3, v3, 0x1

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_4
    move v2, v4

    :goto_0
    return v2

    :cond_5
    :goto_1
    const/4 v4, 0x1

    :cond_6
    return v4
.end method

.method public d()I
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/B;->a:[B

    array-length v0, v0

    iget v1, p0, Lcom/jscape/util/B;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public d(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/util/B;->b:I

    return-void
.end method

.method public e()V
    .locals 6

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    iget v1, p0, Lcom/jscape/util/B;->b:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v1, :cond_1

    iget-object v3, p0, Lcom/jscape/util/B;->a:[B

    aget-byte v4, v3, v2

    aget-byte v5, v3, v1

    aput-byte v5, v3, v2

    aput-byte v4, v3, v1

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, -0x1

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method public f()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/util/B;->b:I

    return-void
.end method

.method public g()[B
    .locals 2

    iget v0, p0, Lcom/jscape/util/B;->b:I

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/jscape/util/B;->b(II)[B

    move-result-object v0

    return-object v0
.end method
