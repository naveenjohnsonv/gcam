.class public Lcom/jscape/util/ab;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "TT;>;"
    }
.end annotation


# static fields
.field private static final a:I = -0x1

.field private static final d:[Ljava/lang/String;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x8

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0xd

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "C\u0000|afAdR\u0010-Aq/kJx\nX5ycHi\n\u000e\u0016#If{KPy\u001dAa`p\u0004g\u0000B\u007fjaPoR"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x30

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/ab;->d:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/4 v13, 0x2

    if-eqz v12, :cond_6

    if-eq v12, v7, :cond_5

    if-eq v12, v13, :cond_4

    if-eq v12, v0, :cond_7

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v13, 0x11

    goto :goto_2

    :cond_2
    const/16 v13, 0x29

    goto :goto_2

    :cond_3
    const/16 v13, 0xf

    goto :goto_2

    :cond_4
    const/16 v13, 0x18

    goto :goto_2

    :cond_5
    const/16 v13, 0x2d

    goto :goto_2

    :cond_6
    const/16 v13, 0x62

    :cond_7
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "TT;>;)V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz v0, :cond_1

    :try_start_0
    instance-of v0, p1, Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/ab;->a(Ljava/lang/UnsupportedOperationException;)Ljava/lang/UnsupportedOperationException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    move-object v0, p1

    check-cast v0, Ljava/util/ArrayList;

    :goto_1
    iput-object v0, p0, Lcom/jscape/util/ab;->b:Ljava/util/List;

    const/4 p1, -0x1

    iput p1, p0, Lcom/jscape/util/ab;->c:I

    return-void
.end method

.method private static a(Ljava/lang/UnsupportedOperationException;)Ljava/lang/UnsupportedOperationException;
    .locals 0

    return-object p0
.end method

.method private d()V
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/util/ab;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/ab;->a(Ljava/lang/UnsupportedOperationException;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/jscape/util/ab;->c:I

    return v0
.end method

.method public a(I)V
    .locals 7

    int-to-long v0, p1

    iget-object v2, p0, Lcom/jscape/util/ab;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v4, v2

    sget-object v2, Lcom/jscape/util/ab;->d:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v6, v2, v3

    const-wide/16 v2, -0x1

    invoke-static/range {v0 .. v6}, Lcom/jscape/util/aq;->a(JJJLjava/lang/String;)V

    iput p1, p0, Lcom/jscape/util/ab;->c:I

    return-void
.end method

.method public b()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/ab;->b:Ljava/util/List;

    iget v1, p0, Lcom/jscape/util/ab;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public c()V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/jscape/util/ab;->a(I)V

    return-void
.end method

.method public hasNext()Z
    .locals 4

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    :try_start_0
    iget v1, p0, Lcom/jscape/util/ab;->c:I
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v2, -0x1

    const/4 v3, 0x1

    if-nez v0, :cond_2

    if-ne v1, v2, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/util/ab;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_2

    if-nez v0, :cond_1

    if-gtz v1, :cond_4

    :cond_0
    :try_start_2
    iget v1, p0, Lcom/jscape/util/ab;->c:I
    :try_end_2
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_2 .. :try_end_2} :catch_0

    if-nez v0, :cond_1

    :try_start_3
    iget-object v0, p0, Lcom/jscape/util/ab;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0
    :try_end_3
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_3 .. :try_end_3} :catch_4

    add-int/lit8 v2, v0, -0x1

    goto :goto_0

    :cond_1
    move v3, v1

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2

    :cond_2
    :goto_0
    if-ge v1, v2, :cond_3

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    :cond_4
    :goto_1
    return v3

    :catch_1
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/util/ab;->a(Ljava/lang/UnsupportedOperationException;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/util/ab;->a(Ljava/lang/UnsupportedOperationException;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/util/ab;->a(Ljava/lang/UnsupportedOperationException;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_6 .. :try_end_6} :catch_0

    :goto_2
    :try_start_7
    invoke-static {v0}, Lcom/jscape/util/ab;->a(Ljava/lang/UnsupportedOperationException;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
    :try_end_7
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_7 .. :try_end_7} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/ab;->a(Ljava/lang/UnsupportedOperationException;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/util/ab;->d()V

    iget v0, p0, Lcom/jscape/util/ab;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/jscape/util/ab;->c:I

    invoke-virtual {p0}, Lcom/jscape/util/ab;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/ab;->d:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/ab;->b:Ljava/util/List;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/util/ab;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
