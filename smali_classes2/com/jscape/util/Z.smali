.class Lcom/jscape/util/Z;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final a:Lcom/jscape/util/Y;


# direct methods
.method constructor <init>(Lcom/jscape/util/Y;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/util/Z;->a:Lcom/jscape/util/Y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/Z;->a:Lcom/jscape/util/Y;

    invoke-static {v0}, Lcom/jscape/util/Y;->a(Lcom/jscape/util/Y;)Ljava/util/Enumeration;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/Z;->a:Lcom/jscape/util/Y;

    invoke-static {v0}, Lcom/jscape/util/Y;->a(Lcom/jscape/util/Y;)Ljava/util/Enumeration;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
