.class public Lcom/jscape/util/w;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "hHt<(K_@Xj5fZ_\u0006TkpfVN\u0006\\t<gN_B\u0013\u001bd\\|piKHGD8?n_ICI8?z\u0019VCS\u007f$`\u0017\u0011d\\|piKHGD8?n_ICI6\u000bd\\|p{MHOS\u007f~"

    const/16 v4, 0x58

    const/16 v5, 0x1e

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x19

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    const/4 v13, 0x0

    :goto_2
    const/16 v14, 0x11

    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x21

    const-string v3, " \u001884-\u000f\u000c\u0003\u0000|x)\u0013\u0019\u0016\u0011r\u000f \u001884<\u0012\u000c\u0016Y*u \u0008\u001bL"

    move v7, v10

    move v5, v14

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x5d

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/util/w;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v10, v13

    rem-int/lit8 v1, v13, 0x7

    if-eqz v1, :cond_8

    if-eq v1, v9, :cond_7

    const/4 v2, 0x2

    if-eq v1, v2, :cond_6

    const/4 v2, 0x3

    if-eq v1, v2, :cond_5

    const/4 v2, 0x4

    if-eq v1, v2, :cond_9

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v14, 0x23

    goto :goto_4

    :cond_4
    const/16 v14, 0x20

    goto :goto_4

    :cond_5
    const/16 v14, 0x49

    goto :goto_4

    :cond_6
    move v14, v9

    goto :goto_4

    :cond_7
    const/16 v14, 0x24

    goto :goto_4

    :cond_8
    const/16 v14, 0x3f

    :cond_9
    :goto_4
    xor-int v1, v8, v14

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/NullPointerException;)Ljava/lang/NullPointerException;
    .locals 0

    return-object p0
.end method

.method public static a(I)V
    .locals 7

    int-to-long v0, p0

    sget-object p0, Lcom/jscape/util/w;->a:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v6, p0, v2

    const-wide/16 v2, 0x1

    const-wide/32 v4, 0xffff

    invoke-static/range {v0 .. v6}, Lcom/jscape/util/w;->a(JJJLjava/lang/String;)V

    return-void
.end method

.method public static a(JJJLjava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p6}, Lcom/jscape/util/w;->b(JJLjava/lang/String;)V

    invoke-static {p0, p1, p4, p5, p6}, Lcom/jscape/util/w;->d(JJLjava/lang/String;)V

    return-void
.end method

.method public static a(JJLjava/lang/String;)V
    .locals 0

    cmp-long p0, p0, p2

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0, p4}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/Object;)V
    .locals 2

    sget-object v0, Lcom/jscape/util/w;->a:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {p0, v0}, Lcom/jscape/util/w;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p0

    invoke-static {p0, p2}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0

    if-eqz p0, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance p0, Ljava/lang/NullPointerException;

    invoke-direct {p0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/w;->a(Ljava/lang/NullPointerException;)Ljava/lang/NullPointerException;

    move-result-object p0

    throw p0
.end method

.method public static a(Ljava/lang/Object;[Ljava/lang/Object;Ljava/lang/String;)V
    .locals 4

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_4

    aget-object v2, p1, v1

    if-nez v0, :cond_1

    if-nez v2, :cond_0

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    return-void

    :cond_0
    move-object v3, p0

    goto :goto_1

    :cond_1
    move-object v3, v2

    :goto_1
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    move-object v3, p0

    :cond_2
    :try_start_0
    invoke-virtual {v3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_3

    return-void

    :cond_3
    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_4

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/w;->a(Ljava/lang/NullPointerException;)Ljava/lang/NullPointerException;

    move-result-object p0

    throw p0

    :cond_4
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 4

    invoke-static {p0}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p0

    int-to-long v0, p0

    sget-object p0, Lcom/jscape/util/w;->a:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object p0, p0, v2

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3, p0}, Lcom/jscape/util/w;->a(JJLjava/lang/String;)V

    return-void
.end method

.method public static a(ZLjava/lang/String;)V
    .locals 0

    if-eqz p0, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/w;->a(Ljava/lang/NullPointerException;)Ljava/lang/NullPointerException;

    move-result-object p0

    throw p0
.end method

.method public static a([BII)V
    .locals 8

    int-to-long v0, p1

    array-length v2, p0

    int-to-long v4, v2

    sget-object v7, Lcom/jscape/util/w;->a:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v6, v7, v2

    const-wide/16 v2, 0x0

    invoke-static/range {v0 .. v6}, Lcom/jscape/util/w;->a(JJJLjava/lang/String;)V

    int-to-long v0, p2

    const/4 v2, 0x4

    aget-object v2, v7, v2

    const-wide/16 v3, 0x0

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/w;->b(JJLjava/lang/String;)V

    add-int/2addr p1, p2

    int-to-long p1, p1

    array-length p0, p0

    int-to-long v0, p0

    const/4 p0, 0x1

    aget-object p0, v7, p0

    invoke-static {p1, p2, v0, v1, p0}, Lcom/jscape/util/w;->d(JJLjava/lang/String;)V

    return-void
.end method

.method public static b(JJJLjava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p6}, Lcom/jscape/util/w;->a(JJLjava/lang/String;)V

    invoke-static {p0, p1, p4, p5, p6}, Lcom/jscape/util/w;->c(JJLjava/lang/String;)V

    return-void
.end method

.method public static b(JJLjava/lang/String;)V
    .locals 0

    cmp-long p0, p0, p2

    if-ltz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0, p4}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public static b(ZLjava/lang/String;)V
    .locals 0

    if-nez p0, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/w;->a(Ljava/lang/NullPointerException;)Ljava/lang/NullPointerException;

    move-result-object p0

    throw p0
.end method

.method public static c(JJLjava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    cmp-long p0, p0, p2

    if-nez v0, :cond_1

    if-gez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :cond_1
    :goto_0
    invoke-static {p0, p4}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public static c(ZLjava/lang/String;)V
    .locals 0

    if-eqz p0, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance p0, Ljava/lang/IllegalStateException;

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/w;->a(Ljava/lang/NullPointerException;)Ljava/lang/NullPointerException;

    move-result-object p0

    throw p0
.end method

.method public static d(JJLjava/lang/String;)V
    .locals 0

    cmp-long p0, p0, p2

    if-gtz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0, p4}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public static e(JJLjava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    cmp-long p0, p0, p2

    if-eqz v0, :cond_1

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :cond_1
    :goto_0
    invoke-static {p0, p4}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    return-void
.end method
