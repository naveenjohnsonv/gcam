.class public abstract Lcom/jscape/util/a/b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/a/l;


# static fields
.field protected static final b:Ljava/lang/String; = "["

.field protected static final c:Ljava/lang/String; = "]"

.field protected static final d:Ljava/lang/String; = "<"

.field protected static final e:Ljava/lang/String; = ">"

.field private static i:I

.field private static final l:[Ljava/lang/String;


# instance fields
.field protected f:Ljava/lang/String;

.field protected g:Z

.field protected h:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/jscape/util/a/b;->b(I)V

    const/16 v3, 0x1b

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x1

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    const-string v7, "~@EziwU\u0013[Sxup@VM\u0016fpm[\\G\u001ez)7\u000cQHR)pvAZ]_fn"

    invoke-virtual {v7, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v8, v4

    move v9, v2

    :goto_1
    if-gt v8, v9, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x28

    if-ge v3, v4, :cond_0

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/a/b;->l:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v10, v4, v9

    rem-int/lit8 v11, v9, 0x7

    const/16 v12, 0x65

    if-eqz v11, :cond_7

    if-eq v11, v6, :cond_6

    if-eq v11, v0, :cond_5

    const/4 v13, 0x3

    if-eq v11, v13, :cond_4

    const/4 v13, 0x4

    if-eq v11, v13, :cond_3

    const/4 v13, 0x5

    if-eq v11, v13, :cond_2

    const/16 v11, 0x57

    goto :goto_2

    :cond_2
    const/16 v11, 0x7c

    goto :goto_2

    :cond_3
    move v11, v12

    goto :goto_2

    :cond_4
    const/16 v11, 0x6c

    goto :goto_2

    :cond_5
    const/16 v11, 0x53

    goto :goto_2

    :cond_6
    const/16 v11, 0x4c

    goto :goto_2

    :cond_7
    const/16 v11, 0x56

    :goto_2
    xor-int/2addr v11, v12

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v4, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method protected constructor <init>(Ljava/lang/String;ZI)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    int-to-long v0, p3

    sget-object v2, Lcom/jscape/util/a/b;->l:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v6, v2, v3

    const-wide/16 v2, 0x0

    const-wide/32 v4, 0x7fffffff

    invoke-static/range {v0 .. v6}, Lcom/jscape/util/w;->a(JJJLjava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/util/a/b;->f:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/jscape/util/a/b;->g:Z

    iput p3, p0, Lcom/jscape/util/a/b;->h:I

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static b(I)V
    .locals 0

    sput p0, Lcom/jscape/util/a/b;->i:I

    return-void
.end method

.method public static f()I
    .locals 1

    sget v0, Lcom/jscape/util/a/b;->i:I

    return v0
.end method

.method public static g()I
    .locals 1

    invoke-static {}, Lcom/jscape/util/a/b;->f()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0xf

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public a(Lcom/jscape/util/a/l;)I
    .locals 1

    invoke-virtual {p0}, Lcom/jscape/util/a/b;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1}, Lcom/jscape/util/a/l;->c()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result p1

    return p1
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/a/b;->f:Ljava/lang/String;

    return-object v0
.end method

.method protected a(Lcom/jscape/util/a/h;Lcom/jscape/util/ab;)Ljava/lang/String;
    .locals 0

    const/4 p2, 0x1

    iput-boolean p2, p1, Lcom/jscape/util/a/h;->b:Z

    iget-object p1, p1, Lcom/jscape/util/a/h;->a:Ljava/lang/String;

    return-object p1
.end method

.method public a(Lcom/jscape/util/ab;)Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/util/a/b;->f()I

    move-result v0

    :try_start_0
    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    if-nez v0, :cond_0

    iget v0, p0, Lcom/jscape/util/a/b;->h:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const v1, 0x7fffffff

    if-eq v0, v1, :cond_0

    :try_start_1
    invoke-virtual {p0, p1}, Lcom/jscape/util/a/b;->c(Lcom/jscape/util/ab;)Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/jscape/util/a/b;->b(Lcom/jscape/util/ab;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method protected a(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/a/b;->f:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method protected b(Lcom/jscape/util/ab;)Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/jscape/util/a/b;->f()I

    move-result v0

    invoke-virtual {p1}, Lcom/jscape/util/ab;->c()V

    :cond_0
    invoke-virtual {p1}, Lcom/jscape/util/ab;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/jscape/util/ab;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/util/a/h;

    if-nez v0, :cond_3

    :try_start_0
    iget-object v2, v1, Lcom/jscape/util/a/h;->a:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/jscape/util/a/b;->a(Ljava/lang/String;)Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_1

    if-eqz v2, :cond_2

    :try_start_1
    iget-boolean v2, v1, Lcom/jscape/util/a/h;->b:Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_1
    if-nez v2, :cond_2

    :try_start_2
    invoke-virtual {p0, v1, p1}, Lcom/jscape/util/a/b;->a(Lcom/jscape/util/a/h;Lcom/jscape/util/ab;)Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    if-eqz v0, :cond_0

    goto :goto_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/jscape/util/a/b;->d()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/util/a/b;->g:Z

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/jscape/util/a/b;->h:I

    return v0
.end method

.method protected c(Lcom/jscape/util/ab;)Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/util/a/b;->f()I

    move-result v0

    :try_start_0
    iget v1, p0, Lcom/jscape/util/a/b;->h:I

    invoke-virtual {p1, v1}, Lcom/jscape/util/ab;->a(I)V

    invoke-virtual {p1}, Lcom/jscape/util/ab;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/util/a/h;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_0

    :try_start_1
    iget-object v0, v1, Lcom/jscape/util/a/h;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/jscape/util/a/b;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, v1, Lcom/jscape/util/a/h;->b:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_0

    :try_start_2
    invoke-virtual {p0, v1, p1}, Lcom/jscape/util/a/b;->a(Lcom/jscape/util/a/h;Lcom/jscape/util/ab;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    invoke-virtual {p0}, Lcom/jscape/util/a/b;->d()Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    return-object p1

    :catch_1
    invoke-virtual {p0}, Lcom/jscape/util/a/b;->d()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/jscape/util/a/l;

    invoke-virtual {p0, p1}, Lcom/jscape/util/a/b;->a(Lcom/jscape/util/a/l;)I

    move-result p1

    return p1
.end method

.method protected d()Ljava/lang/String;
    .locals 3

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/util/a/b;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lcom/jscape/util/a/b;->l:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/jscape/util/a/b;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    invoke-static {}, Lcom/jscape/util/a/b;->f()I

    move-result v0

    if-nez v0, :cond_1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    move-object v1, p1

    goto :goto_0

    :cond_1
    move-object v1, p0

    :goto_0
    const/4 v2, 0x0

    if-eqz v1, :cond_8

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3

    if-nez v0, :cond_2

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4

    if-eq v1, v3, :cond_3

    goto :goto_2

    :cond_2
    move-object p1, v1

    :cond_3
    check-cast p1, Lcom/jscape/util/a/b;

    :try_start_2
    iget v1, p0, Lcom/jscape/util/a/b;->h:I

    iget v3, p1, Lcom/jscape/util/a/b;->h:I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    if-nez v0, :cond_5

    if-eq v1, v3, :cond_4

    return v2

    :cond_4
    :try_start_3
    iget-boolean v1, p0, Lcom/jscape/util/a/b;->g:Z

    if-nez v0, :cond_7

    iget-boolean v3, p1, Lcom/jscape/util/a/b;->g:Z
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_5
    :goto_1
    if-eq v1, v3, :cond_6

    return v2

    :cond_6
    iget-object v0, p0, Lcom/jscape/util/a/b;->f:Ljava/lang/String;

    iget-object p1, p1, Lcom/jscape/util/a/b;->f:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    :cond_7
    return v1

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :catch_3
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_8
    :goto_2
    return v2
.end method

.method public hashCode()I
    .locals 3

    invoke-static {}, Lcom/jscape/util/a/b;->g()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/util/a/b;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1d

    :try_start_0
    iget-boolean v2, p0, Lcom/jscape/util/a/b;->g:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :cond_1
    :goto_0
    add-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1d

    iget v2, p0, Lcom/jscape/util/a/b;->h:I

    add-int/2addr v1, v2

    if-nez v0, :cond_2

    const/4 v0, 0x4

    :try_start_1
    new-array v0, v0, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_2
    :goto_1
    return v1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/util/a/b;->g()I

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/util/a/b;->g:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/jscape/util/a/b;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/a/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/jscape/util/a/b;->e()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0
.end method
