.class public Lcom/jscape/util/a/k;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final a:Ljava/lang/String; = "\u0008"

.field private static final b:Ljava/lang/String;

.field private static final h:[Ljava/lang/String;


# instance fields
.field private final c:Ljava/io/PrintStream;

.field private final d:Ljava/lang/String;

.field private e:Ljava/lang/Thread;

.field private f:I

.field private volatile g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x14

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x5a

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "JxR\u0012mCk)^R\u0011w[.DvO\ng]\u0014JxR\u0012mCk)^R\u0011w[.DvO\ng]"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x29

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/a/k;->h:[Ljava/lang/String;

    aget-object v0, v1, v7

    sput-object v0, Lcom/jscape/util/a/k;->b:Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x54

    goto :goto_2

    :cond_2
    const/16 v12, 0x75

    goto :goto_2

    :cond_3
    const/16 v12, 0x58

    goto :goto_2

    :cond_4
    const/16 v12, 0x3b

    goto :goto_2

    :cond_5
    const/16 v12, 0x66

    goto :goto_2

    :cond_6
    const/16 v12, 0x4d

    goto :goto_2

    :cond_7
    const/16 v12, 0x53

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/io/PrintStream;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/a/k;->c:Ljava/io/PrintStream;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "\u0008"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/util/a/k;->d:Ljava/lang/String;

    return-void
.end method

.method private c()V
    .locals 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getPriority()I

    move-result v0

    iput v0, p0, Lcom/jscape/util/a/k;->f:I

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    return-void
.end method

.method private d()V
    .locals 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget v1, p0, Lcom/jscape/util/a/k;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    return-void
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/a/k;->c:Ljava/io/PrintStream;

    iget-object v1, p0, Lcom/jscape/util/a/k;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    return-void
.end method

.method private f()V
    .locals 2

    const-wide/16 v0, 0x1

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/util/a/k;->g:Z

    :goto_0
    return-void
.end method

.method private g()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/util/a/k;->e:Ljava/lang/Thread;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    new-instance v0, Ljava/lang/Thread;

    sget-object v1, Lcom/jscape/util/a/k;->h:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/jscape/util/a/k;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/util/a/k;->g:Z

    return-void
.end method

.method public run()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/util/a/k;->g:Z

    invoke-static {}, Lcom/jscape/util/a/b;->f()I

    move-result v0

    invoke-direct {p0}, Lcom/jscape/util/a/k;->c()V

    :cond_0
    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/util/a/k;->g:Z

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/jscape/util/a/k;->e()V

    invoke-direct {p0}, Lcom/jscape/util/a/k;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    if-eqz v0, :cond_0

    :cond_1
    invoke-direct {p0}, Lcom/jscape/util/a/k;->d()V

    invoke-direct {p0}, Lcom/jscape/util/a/k;->g()V

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/jscape/util/a/k;->d()V

    invoke-direct {p0}, Lcom/jscape/util/a/k;->g()V

    throw v0
.end method
