.class public Lcom/jscape/util/a/d;
.super Lcom/jscape/util/a/b;


# static fields
.field private static final k:Ljava/lang/String;


# instance fields
.field private j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "#/,\u0018{rPN)/\u001f{sYN0>\u0007gy\u0019"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/a/d;->k:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x4e

    goto :goto_1

    :cond_1
    const/16 v4, 0x65

    goto :goto_1

    :cond_2
    const/16 v4, 0x6b

    goto :goto_1

    :cond_3
    const/16 v4, 0x12

    goto :goto_1

    :cond_4
    const/16 v4, 0x26

    goto :goto_1

    :cond_5
    const/16 v4, 0x3f

    goto :goto_1

    :cond_6
    const/16 v4, 0x17

    :goto_1
    const/16 v5, 0x79

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZI)V
    .locals 0

    invoke-direct {p0, p1, p3, p4}, Lcom/jscape/util/a/b;-><init>(Ljava/lang/String;ZI)V

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/a/d;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZI)V
    .locals 1

    const-string v0, ""

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/jscape/util/a/d;-><init>(Ljava/lang/String;Ljava/lang/String;ZI)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/jscape/util/a/h;Lcom/jscape/util/ab;)Ljava/lang/String;
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/jscape/util/a/b;->a(Lcom/jscape/util/a/h;Lcom/jscape/util/ab;)Ljava/lang/String;

    invoke-virtual {p2}, Lcom/jscape/util/ab;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/util/a/h;

    iget-boolean p2, p1, Lcom/jscape/util/a/h;->b:Z

    sget-object v0, Lcom/jscape/util/a/d;->k:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/jscape/util/w;->b(ZLjava/lang/String;)V

    const/4 p2, 0x1

    iput-boolean p2, p1, Lcom/jscape/util/a/h;->b:Z

    iget-object p1, p1, Lcom/jscape/util/a/h;->a:Ljava/lang/String;

    return-object p1
.end method

.method protected e()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/jscape/util/a/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/a/d;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/a/d;->j:Ljava/lang/String;

    return-object v0
.end method
