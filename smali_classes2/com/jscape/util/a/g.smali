.class public Lcom/jscape/util/a/g;
.super Lcom/jscape/util/a/f;


# static fields
.field private static final j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "|eC|\u0006F\u0008)dXf\u0000^\u0008"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/a/g;->j:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/4 v5, 0x2

    if-eqz v4, :cond_5

    const/4 v6, 0x1

    if-eq v4, v6, :cond_4

    if-eq v4, v5, :cond_3

    const/4 v6, 0x3

    if-eq v4, v6, :cond_2

    const/4 v6, 0x4

    if-eq v4, v6, :cond_6

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v5, 0xd

    goto :goto_1

    :cond_1
    const/16 v5, 0x5a

    goto :goto_1

    :cond_2
    const/16 v5, 0x79

    goto :goto_1

    :cond_3
    const/16 v5, 0x43

    goto :goto_1

    :cond_4
    const/16 v5, 0x60

    goto :goto_1

    :cond_5
    const/16 v5, 0x62

    :cond_6
    :goto_1
    const/16 v4, 0x6b

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/jscape/util/a/f;-><init>(Ljava/lang/String;ZI)V

    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;)Z
    .locals 6

    invoke-static {}, Lcom/jscape/util/a/b;->g()I

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-ge v2, v3, :cond_4

    iget-object v3, p0, Lcom/jscape/util/a/g;->f:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-eqz v0, :cond_3

    if-eqz v0, :cond_2

    if-ltz v3, :cond_1

    move v3, v4

    goto :goto_0

    :cond_1
    move v3, v1

    :cond_2
    :goto_0
    sget-object v5, Lcom/jscape/util/a/g;->j:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/jscape/util/w;->a(ZLjava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_0

    goto :goto_1

    :cond_3
    move v4, v3

    :cond_4
    :goto_1
    return v4
.end method
