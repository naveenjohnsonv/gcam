.class public Lcom/jscape/util/a/a/s;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/a/a/p;


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x1f

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x25

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\u0007r5\u0016MRI17+\u0013PIC;7\u001fFW}\u000c;x0CBOY;sj,\u001ag0\nKN\u000c\u000e27>\u0004DC0dd\rKT\u000c=v2\u0006\u0004RI$b-\u0011AD\u000c4e#\u0016IEB!9\"\u001cy2\u0002HIHux4\u0017MOBuLa\u0010y\u0000M\'p1\u000eANXuc=\u0013A\u000e"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x6f

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/a/a/s;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/4 v13, 0x5

    if-eqz v12, :cond_6

    if-eq v12, v7, :cond_5

    const/4 v14, 0x2

    if-eq v12, v14, :cond_4

    if-eq v12, v0, :cond_3

    const/4 v14, 0x4

    if-eq v12, v14, :cond_2

    if-eq v12, v13, :cond_7

    const/16 v13, 0x9

    goto :goto_2

    :cond_2
    move v13, v7

    goto :goto_2

    :cond_3
    const/16 v13, 0x46

    goto :goto_2

    :cond_4
    const/16 v13, 0x61

    goto :goto_2

    :cond_5
    const/16 v13, 0x32

    goto :goto_2

    :cond_6
    const/16 v13, 0x70

    :cond_7
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/util/a/a/q;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/a/a/f;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/util/a/a/q;->d()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/util/a/a/f;

    sget-object v1, Lcom/jscape/util/a/a/s;->a:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/jscape/util/a/a/q;->c()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/jscape/util/a/a/f;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/jscape/util/a/a/f; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/a/s;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1
.end method

.method private a(Lcom/jscape/util/a/a/q;Lcom/jscape/util/a/a/i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/a/a/f;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p2}, Lcom/jscape/util/a/a/i;->b()Z

    move-result p2

    if-nez p2, :cond_0

    return-void

    :cond_0
    new-instance p2, Lcom/jscape/util/a/a/f;

    sget-object v0, Lcom/jscape/util/a/a/s;->a:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/jscape/util/a/a/q;->c()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/util/a/a/f;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_0
    .catch Lcom/jscape/util/a/a/f; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/a/s;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1
.end method

.method private a(Lcom/jscape/util/a/a/q;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/a/a/f;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p1, p2}, Lcom/jscape/util/a/a/q;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    new-instance p2, Lcom/jscape/util/a/a/f;

    sget-object v0, Lcom/jscape/util/a/a/s;->a:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/jscape/util/a/a/q;->c()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/util/a/a/f;-><init>(Ljava/lang/String;)V

    throw p2
.end method


# virtual methods
.method public a(Lcom/jscape/util/a/a/m;Lcom/jscape/util/a/a/j;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/a/a/f;
        }
    .end annotation

    check-cast p1, Lcom/jscape/util/a/a/q;

    invoke-static {}, Lcom/jscape/util/a/a/q;->g()[Lcom/jscape/util/aq;

    move-result-object v0

    invoke-virtual {p1}, Lcom/jscape/util/a/a/q;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/jscape/util/a/a/j;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/util/a/a/i;

    if-eqz v0, :cond_6

    :try_start_0
    invoke-virtual {v2}, Lcom/jscape/util/a/a/i;->b()Z

    move-result v3
    :try_end_0
    .catch Lcom/jscape/util/a/a/f; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_1

    if-nez v3, :cond_4

    :try_start_1
    invoke-virtual {v2}, Lcom/jscape/util/a/a/i;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3
    :try_end_1
    .catch Lcom/jscape/util/a/a/f; {:try_start_1 .. :try_end_1} :catch_4

    :cond_1
    if-eqz v0, :cond_2

    if-eqz v3, :cond_4

    if-eqz v0, :cond_3

    :try_start_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3
    :try_end_2
    .catch Lcom/jscape/util/a/a/f; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/a/s;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    if-eqz v3, :cond_4

    :try_start_3
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2
    :try_end_3
    .catch Lcom/jscape/util/a/a/f; {:try_start_3 .. :try_end_3} :catch_1

    :cond_3
    check-cast p2, Lcom/jscape/util/a/a/i;

    invoke-direct {p0, p1, p2}, Lcom/jscape/util/a/a/s;->a(Lcom/jscape/util/a/a/q;Lcom/jscape/util/a/a/i;)V

    invoke-virtual {p2}, Lcom/jscape/util/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/jscape/util/a/a/s;->a(Lcom/jscape/util/a/a/q;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/jscape/util/a/a/i;->c()V

    invoke-virtual {p2}, Lcom/jscape/util/a/a/i;->c()V

    return-void

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/a/s;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1

    :cond_4
    if-nez v0, :cond_0

    goto :goto_1

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/util/a/a/s;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/util/a/a/f; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/util/a/a/s;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/util/a/a/f; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/a/s;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1

    :cond_5
    :goto_1
    invoke-direct {p0, p1}, Lcom/jscape/util/a/a/s;->a(Lcom/jscape/util/a/a/q;)V

    :cond_6
    return-void
.end method
