.class public Lcom/jscape/util/a/a/n;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Lcom/jscape/util/a/a/o;

.field private static final c:[Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class;",
            "Lcom/jscape/util/a/a/o;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x17

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x59

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "C\u0007{cX?2m\u0014{eE(Tw\u0012a~E8\u0011\u007fJ\u0014Y\u0019|\u007fG!\u001b~\u0003jn\u0017>\u0004x\u001e`d\rq"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    const/4 v11, 0x4

    const/4 v12, 0x3

    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x2c

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/a/a/n;->c:[Ljava/lang/String;

    new-array v1, v11, [Lcom/jscape/util/a/a/o;

    new-instance v3, Lcom/jscape/util/a/a/o;

    const-class v4, Lcom/jscape/util/a/a/t;

    new-instance v5, Lcom/jscape/util/a/a/u;

    invoke-direct {v5}, Lcom/jscape/util/a/a/u;-><init>()V

    invoke-direct {v3, v4, v5}, Lcom/jscape/util/a/a/o;-><init>(Ljava/lang/Class;Lcom/jscape/util/a/a/p;)V

    aput-object v3, v1, v2

    new-instance v2, Lcom/jscape/util/a/a/o;

    const-class v3, Lcom/jscape/util/a/a/k;

    new-instance v4, Lcom/jscape/util/a/a/l;

    invoke-direct {v4}, Lcom/jscape/util/a/a/l;-><init>()V

    invoke-direct {v2, v3, v4}, Lcom/jscape/util/a/a/o;-><init>(Ljava/lang/Class;Lcom/jscape/util/a/a/p;)V

    aput-object v2, v1, v7

    new-instance v2, Lcom/jscape/util/a/a/o;

    const-class v3, Lcom/jscape/util/a/a/q;

    new-instance v4, Lcom/jscape/util/a/a/s;

    invoke-direct {v4}, Lcom/jscape/util/a/a/s;-><init>()V

    invoke-direct {v2, v3, v4}, Lcom/jscape/util/a/a/o;-><init>(Ljava/lang/Class;Lcom/jscape/util/a/a/p;)V

    aput-object v2, v1, v0

    new-instance v0, Lcom/jscape/util/a/a/o;

    const-class v2, Lcom/jscape/util/a/a/g;

    new-instance v3, Lcom/jscape/util/a/a/h;

    invoke-direct {v3}, Lcom/jscape/util/a/a/h;-><init>()V

    invoke-direct {v0, v2, v3}, Lcom/jscape/util/a/a/o;-><init>(Ljava/lang/Class;Lcom/jscape/util/a/a/p;)V

    aput-object v0, v1, v12

    sput-object v1, Lcom/jscape/util/a/a/n;->a:[Lcom/jscape/util/a/a/o;

    return-void

    :cond_1
    aget-char v13, v4, v10

    rem-int/lit8 v14, v10, 0x7

    if-eqz v14, :cond_7

    if-eq v14, v7, :cond_6

    if-eq v14, v0, :cond_5

    if-eq v14, v12, :cond_4

    if-eq v14, v11, :cond_3

    const/4 v11, 0x5

    if-eq v14, v11, :cond_2

    const/16 v11, 0x2d

    goto :goto_2

    :cond_2
    const/16 v11, 0x8

    goto :goto_2

    :cond_3
    const/16 v11, 0x6e

    goto :goto_2

    :cond_4
    const/16 v11, 0x53

    goto :goto_2

    :cond_5
    const/16 v11, 0x56

    goto :goto_2

    :cond_6
    const/16 v11, 0x2e

    goto :goto_2

    :cond_7
    const/16 v11, 0x55

    :goto_2
    xor-int/2addr v11, v6

    xor-int/2addr v11, v13

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1
.end method

.method public varargs constructor <init>([Lcom/jscape/util/a/a/o;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/util/a/a/n;->b:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/jscape/util/a/a/n;->a([Lcom/jscape/util/a/a/o;)V

    return-void
.end method

.method private a([Ljava/lang/String;)Lcom/jscape/util/a/a/j;
    .locals 6

    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {}, Lcom/jscape/util/a/a/q;->g()[Lcom/jscape/util/aq;

    move-result-object v1

    array-length v2, p1

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v2, :cond_1

    aget-object v4, p1, v3

    new-instance v5, Lcom/jscape/util/a/a/i;

    invoke-direct {v5, v4}, Lcom/jscape/util/a/a/i;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    if-nez v1, :cond_0

    :cond_1
    new-instance p1, Lcom/jscape/util/a/a/j;

    invoke-direct {p1, v0}, Lcom/jscape/util/a/a/j;-><init>(Ljava/util/List;)V

    return-object p1
.end method

.method public static a()Lcom/jscape/util/a/a/n;
    .locals 2

    new-instance v0, Lcom/jscape/util/a/a/n;

    sget-object v1, Lcom/jscape/util/a/a/n;->a:[Lcom/jscape/util/a/a/o;

    invoke-direct {v0, v1}, Lcom/jscape/util/a/a/n;-><init>([Lcom/jscape/util/a/a/o;)V

    return-object v0
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/util/a/a/m;Lcom/jscape/util/a/a/j;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/a/a/f;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/util/a/a/n;->b(Lcom/jscape/util/a/a/m;)Lcom/jscape/util/a/a/o;

    move-result-object v0

    iget-object v0, v0, Lcom/jscape/util/a/a/o;->b:Lcom/jscape/util/a/a/p;

    invoke-interface {v0, p1, p2}, Lcom/jscape/util/a/a/p;->a(Lcom/jscape/util/a/a/m;Lcom/jscape/util/a/a/j;)V

    return-void
.end method

.method private a([Lcom/jscape/util/a/a/m;Lcom/jscape/util/a/a/j;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/a/a/f;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/a/a/q;->g()[Lcom/jscape/util/aq;

    move-result-object v0

    array-length v1, p1

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v1, :cond_1

    aget-object v3, p1, v2

    invoke-direct {p0, v3, p2}, Lcom/jscape/util/a/a/n;->a(Lcom/jscape/util/a/a/m;Lcom/jscape/util/a/a/j;)V

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private a([Lcom/jscape/util/a/a/o;)V
    .locals 6

    invoke-static {}, Lcom/jscape/util/a/a/q;->g()[Lcom/jscape/util/aq;

    move-result-object v0

    array-length v1, p1

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v1, :cond_1

    aget-object v3, p1, v2

    iget-object v4, p0, Lcom/jscape/util/a/a/n;->b:Ljava/util/Map;

    iget-object v5, v3, Lcom/jscape/util/a/a/o;->a:Ljava/lang/Class;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private b(Lcom/jscape/util/a/a/m;)Lcom/jscape/util/a/a/o;
    .locals 4

    invoke-static {}, Lcom/jscape/util/a/a/q;->g()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/a/a/n;->b:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/util/a/a/o;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/util/a/a/n;->c:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/a/n;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method


# virtual methods
.method public varargs a([Ljava/lang/String;[Lcom/jscape/util/a/a/m;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/a/a/f;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/util/a/a/n;->a([Ljava/lang/String;)Lcom/jscape/util/a/a/j;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lcom/jscape/util/a/a/n;->a([Lcom/jscape/util/a/a/m;Lcom/jscape/util/a/a/j;)V

    return-void
.end method

.method public a(Lcom/jscape/util/a/a/m;)Z
    .locals 0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/util/a/a/n;->a(Ljava/lang/Class;)Z

    move-result p1

    return p1
.end method

.method public a(Ljava/lang/Class;)Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/a/a/n;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/a/a/n;->c:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/a/a/n;->b:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
