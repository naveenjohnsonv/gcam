.class public abstract enum Lcom/jscape/util/a/a/a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/util/a/a/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/jscape/util/a/a/a;

.field public static final enum b:Lcom/jscape/util/a/a/a;

.field public static final enum c:Lcom/jscape/util/a/a/a;

.field public static final enum d:Lcom/jscape/util/a/a/a;

.field private static final e:[Lcom/jscape/util/a/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "](O\u0018W8\u0016\u0004S(N\u0013"

    const/16 v6, 0xc

    move v9, v4

    const/4 v7, -0x1

    const/4 v8, 0x7

    :goto_0
    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v8

    invoke-virtual {v5, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v13, -0x1

    const/16 v14, 0x51

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v15, v11

    move v2, v4

    :goto_2
    const/4 v3, 0x3

    const/4 v12, 0x2

    if-gt v15, v2, :cond_3

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    if-eqz v13, :cond_1

    add-int/lit8 v3, v9, 0x1

    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v3

    goto :goto_0

    :cond_0
    const/16 v6, 0xe

    const-string v5, "\u0000\u007f\u0002G\u0003j\\\u0006\u001ae\u0004K\nh"

    move v9, v3

    const/4 v7, -0x1

    const/4 v8, 0x7

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v9, 0x1

    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v2

    move v9, v11

    :goto_3
    add-int/2addr v7, v10

    add-int v2, v7, v8

    invoke-virtual {v5, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v13, v4

    const/4 v14, 0x7

    goto :goto_1

    :cond_2
    new-instance v2, Lcom/jscape/util/a/a/b;

    aget-object v5, v1, v3

    invoke-direct {v2, v5, v4}, Lcom/jscape/util/a/a/b;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/jscape/util/a/a/a;->a:Lcom/jscape/util/a/a/a;

    new-instance v2, Lcom/jscape/util/a/a/c;

    aget-object v5, v1, v12

    invoke-direct {v2, v5, v10}, Lcom/jscape/util/a/a/c;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/jscape/util/a/a/a;->b:Lcom/jscape/util/a/a/a;

    new-instance v2, Lcom/jscape/util/a/a/d;

    aget-object v5, v1, v10

    invoke-direct {v2, v5, v12}, Lcom/jscape/util/a/a/d;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/jscape/util/a/a/a;->c:Lcom/jscape/util/a/a/a;

    new-instance v2, Lcom/jscape/util/a/a/e;

    aget-object v1, v1, v4

    invoke-direct {v2, v1, v3}, Lcom/jscape/util/a/a/e;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/jscape/util/a/a/a;->d:Lcom/jscape/util/a/a/a;

    new-array v0, v0, [Lcom/jscape/util/a/a/a;

    sget-object v1, Lcom/jscape/util/a/a/a;->a:Lcom/jscape/util/a/a/a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/jscape/util/a/a/a;->b:Lcom/jscape/util/a/a/a;

    aput-object v1, v0, v10

    sget-object v1, Lcom/jscape/util/a/a/a;->c:Lcom/jscape/util/a/a/a;

    aput-object v1, v0, v12

    aput-object v2, v0, v3

    sput-object v0, Lcom/jscape/util/a/a/a;->e:[Lcom/jscape/util/a/a/a;

    return-void

    :cond_3
    aget-char v16, v11, v2

    rem-int/lit8 v4, v2, 0x7

    const/4 v0, 0x5

    if-eqz v4, :cond_9

    if-eq v4, v10, :cond_8

    if-eq v4, v12, :cond_7

    if-eq v4, v3, :cond_6

    const/4 v3, 0x4

    if-eq v4, v3, :cond_5

    if-eq v4, v0, :cond_4

    const/16 v0, 0x9

    goto :goto_4

    :cond_4
    const/16 v0, 0x28

    goto :goto_4

    :cond_5
    const/16 v0, 0x43

    goto :goto_4

    :cond_6
    const/4 v3, 0x4

    goto :goto_4

    :cond_7
    const/4 v3, 0x4

    const/16 v0, 0x51

    goto :goto_4

    :cond_8
    const/4 v3, 0x4

    const/16 v0, 0x36

    goto :goto_4

    :cond_9
    const/4 v3, 0x4

    const/16 v0, 0x4e

    :goto_4
    xor-int/2addr v0, v14

    xor-int v0, v16, v0

    int-to-char v0, v0

    aput-char v0, v11, v2

    add-int/lit8 v2, v2, 0x1

    move v0, v3

    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILcom/jscape/util/a/a/r;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/util/a/a/a;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/util/a/a/a;
    .locals 1

    const-class v0, Lcom/jscape/util/a/a/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/util/a/a/a;

    return-object p0
.end method

.method public static a()[Lcom/jscape/util/a/a/a;
    .locals 1

    sget-object v0, Lcom/jscape/util/a/a/a;->e:[Lcom/jscape/util/a/a/a;

    invoke-virtual {v0}, [Lcom/jscape/util/a/a/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/util/a/a/a;

    return-object v0
.end method


# virtual methods
.method public abstract b(Ljava/lang/String;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation
.end method
