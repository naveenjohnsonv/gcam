.class public Lcom/jscape/util/a/a/u;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/a/a/p;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "s,f7hXaEix2uCkOiLgrw$O&cbgEqO-9"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/a/a/u;->a:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x62

    goto :goto_1

    :cond_1
    const/16 v4, 0x4c

    goto :goto_1

    :cond_2
    const/16 v4, 0x67

    goto :goto_1

    :cond_3
    const/16 v4, 0x24

    goto :goto_1

    :cond_4
    const/16 v4, 0x71

    goto :goto_1

    :cond_5
    const/16 v4, 0x2f

    goto :goto_1

    :cond_6
    const/16 v4, 0x47

    :goto_1
    const/16 v5, 0x66

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/util/a/a/t;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/a/a/f;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/util/a/a/t;->d()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/util/a/a/f;

    sget-object v1, Lcom/jscape/util/a/a/u;->a:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/jscape/util/a/a/t;->c()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/jscape/util/a/a/f;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/jscape/util/a/a/f; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/a/u;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1
.end method


# virtual methods
.method public a(Lcom/jscape/util/a/a/m;Lcom/jscape/util/a/a/j;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/a/a/f;
        }
    .end annotation

    check-cast p1, Lcom/jscape/util/a/a/t;

    invoke-static {}, Lcom/jscape/util/a/a/q;->g()[Lcom/jscape/util/aq;

    move-result-object v0

    invoke-virtual {p1}, Lcom/jscape/util/a/a/t;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/jscape/util/a/a/j;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/util/a/a/i;

    if-eqz v0, :cond_4

    :try_start_0
    invoke-virtual {v2}, Lcom/jscape/util/a/a/i;->b()Z

    move-result v3
    :try_end_0
    .catch Lcom/jscape/util/a/a/f; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    if-nez v3, :cond_2

    :try_start_1
    invoke-virtual {v2}, Lcom/jscape/util/a/a/i;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3
    :try_end_1
    .catch Lcom/jscape/util/a/a/f; {:try_start_1 .. :try_end_1} :catch_3

    :cond_1
    if-eqz v3, :cond_2

    :try_start_2
    invoke-virtual {p1}, Lcom/jscape/util/a/a/t;->e()V

    invoke-virtual {v2}, Lcom/jscape/util/a/a/i;->c()V
    :try_end_2
    .catch Lcom/jscape/util/a/a/f; {:try_start_2 .. :try_end_2} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/a/u;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1

    :cond_2
    if-nez v0, :cond_0

    goto :goto_0

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/util/a/a/u;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/util/a/a/f; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/util/a/a/u;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/util/a/a/f; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/a/u;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1

    :cond_3
    :goto_0
    invoke-direct {p0, p1}, Lcom/jscape/util/a/a/u;->a(Lcom/jscape/util/a/a/t;)V

    :cond_4
    return-void
.end method
