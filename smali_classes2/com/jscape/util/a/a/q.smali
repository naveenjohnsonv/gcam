.class public Lcom/jscape/util/a/a/q;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/a/a/m;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/jscape/util/a/a/m;"
    }
.end annotation


# static fields
.field private static e:[Lcom/jscape/util/aq;

.field private static final f:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/jscape/util/a/a/a;

.field private final c:Z

.field private d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x3

    new-array v3, v2, [Lcom/jscape/util/aq;

    invoke-static {v3}, Lcom/jscape/util/a/a/q;->b([Lcom/jscape/util/aq;)V

    const-string v5, "Z|\u0019\"FM_K\u0007Z|\u001b:Z]\u0007"

    const/16 v6, 0x10

    const/16 v7, 0x8

    const/4 v8, -0x1

    const/4 v9, 0x0

    :goto_0
    const/16 v10, 0x1d

    const/4 v11, 0x1

    add-int/2addr v8, v11

    add-int v12, v8, v7

    invoke-virtual {v5, v8, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    const/4 v15, 0x0

    :goto_2
    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    add-int/lit8 v12, v9, 0x1

    if-eqz v13, :cond_1

    aput-object v10, v1, v9

    add-int/2addr v8, v7

    if-ge v8, v6, :cond_0

    invoke-virtual {v5, v8}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v9, v12

    goto :goto_0

    :cond_0
    const/16 v6, 0x1e

    const/16 v5, 0x12

    const-string v7, ":!\u001a-yTR\u0003/\u001d\u007fMJG\u0007%Nx\u000bF`\u0001:GQO\u0018%\u0017b"

    move v9, v12

    const/4 v8, -0x1

    move-object/from16 v17, v7

    move v7, v5

    move-object/from16 v5, v17

    goto :goto_3

    :cond_1
    aput-object v10, v1, v9

    add-int/2addr v8, v7

    if-ge v8, v6, :cond_2

    invoke-virtual {v5, v8}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v9, v12

    :goto_3
    add-int/2addr v8, v11

    add-int v10, v8, v7

    invoke-virtual {v5, v8, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v10, v11

    const/4 v13, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/a/a/q;->f:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v3, v15, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v11, :cond_8

    const/4 v4, 0x2

    if-eq v3, v4, :cond_7

    if-eq v3, v2, :cond_6

    if-eq v3, v0, :cond_5

    const/4 v4, 0x5

    if-eq v3, v4, :cond_4

    const/16 v3, 0x27

    goto :goto_4

    :cond_4
    const/16 v3, 0x25

    goto :goto_4

    :cond_5
    const/16 v3, 0x37

    goto :goto_4

    :cond_6
    const/16 v3, 0x5e

    goto :goto_4

    :cond_7
    const/16 v3, 0x72

    goto :goto_4

    :cond_8
    const/16 v3, 0x41

    goto :goto_4

    :cond_9
    const/16 v3, 0x6b

    :goto_4
    xor-int/2addr v3, v10

    xor-int v3, v16, v3

    int-to-char v3, v3

    aput-char v3, v12, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/util/a/a/q;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/a/a/q<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p1, Lcom/jscape/util/a/a/q;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/jscape/util/a/a/q;->b:Lcom/jscape/util/a/a/a;

    iget-boolean v2, p1, Lcom/jscape/util/a/a/q;->c:Z

    iget-object p1, p1, Lcom/jscape/util/a/a/q;->d:Ljava/lang/Object;

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/jscape/util/a/a/q;-><init>(Ljava/lang/String;Lcom/jscape/util/a/a/a;ZLjava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/jscape/util/a/a/a;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/jscape/util/a/a/q;-><init>(Ljava/lang/String;Lcom/jscape/util/a/a/a;ZLjava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/jscape/util/a/a/a;ZLjava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/jscape/util/a/a/a;",
            "ZTT;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/a/a/q;->a:Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/a/a/q;->b:Lcom/jscape/util/a/a/a;

    iput-boolean p3, p0, Lcom/jscape/util/a/a/q;->c:Z

    iput-object p4, p0, Lcom/jscape/util/a/a/q;->d:Ljava/lang/Object;

    return-void
.end method

.method public static a(Ljava/lang/String;Z)Lcom/jscape/util/a/a/q;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/jscape/util/a/a/q<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/a/a/q;

    sget-object v1, Lcom/jscape/util/a/a/a;->a:Lcom/jscape/util/a/a/a;

    invoke-direct {v0, p0, v1, p1}, Lcom/jscape/util/a/a/q;-><init>(Ljava/lang/String;Lcom/jscape/util/a/a/a;Z)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;Z)Lcom/jscape/util/a/a/q;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/jscape/util/a/a/q<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/a/a/q;

    sget-object v1, Lcom/jscape/util/a/a/a;->b:Lcom/jscape/util/a/a/a;

    invoke-direct {v0, p0, v1, p1}, Lcom/jscape/util/a/a/q;-><init>(Ljava/lang/String;Lcom/jscape/util/a/a/a;Z)V

    return-object v0
.end method

.method public static b([Lcom/jscape/util/aq;)V
    .locals 0

    sput-object p0, Lcom/jscape/util/a/a/q;->e:[Lcom/jscape/util/aq;

    return-void
.end method

.method public static c(Ljava/lang/String;Z)Lcom/jscape/util/a/a/q;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/jscape/util/a/a/q<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/a/a/q;

    sget-object v1, Lcom/jscape/util/a/a/a;->c:Lcom/jscape/util/a/a/a;

    invoke-direct {v0, p0, v1, p1}, Lcom/jscape/util/a/a/q;-><init>(Ljava/lang/String;Lcom/jscape/util/a/a/a;Z)V

    return-object v0
.end method

.method public static d(Ljava/lang/String;Z)Lcom/jscape/util/a/a/q;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/jscape/util/a/a/q<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/a/a/q;

    sget-object v1, Lcom/jscape/util/a/a/a;->d:Lcom/jscape/util/a/a/a;

    invoke-direct {v0, p0, v1, p1}, Lcom/jscape/util/a/a/q;-><init>(Ljava/lang/String;Lcom/jscape/util/a/a/a;Z)V

    return-object v0
.end method

.method public static g()[Lcom/jscape/util/aq;
    .locals 1

    sget-object v0, Lcom/jscape/util/a/a/q;->e:[Lcom/jscape/util/aq;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/a/a/q;->g()[Lcom/jscape/util/aq;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jscape/util/a/a/q;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/jscape/util/a/a/q;->e()Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, p0

    :cond_1
    :goto_0
    return-object p1
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/a/a/q;->b:Lcom/jscape/util/a/a/a;

    invoke-virtual {v0, p1}, Lcom/jscape/util/a/a/a;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/util/a/a/q;->d:Ljava/lang/Object;

    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/a/a/q;->d:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/util/a/a/q;->d:Ljava/lang/Object;

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/a/a/q;->a:Ljava/lang/String;

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/util/a/a/q;->c:Z

    return v0
.end method

.method public e()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/a/a/q;->d:Ljava/lang/Object;

    return-object v0
.end method

.method public f()Lcom/jscape/util/a/a/q;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jscape/util/a/a/q<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/a/a/q;

    invoke-direct {v0, p0}, Lcom/jscape/util/a/a/q;-><init>(Lcom/jscape/util/a/a/q;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/jscape/util/a/a/q;->g()[Lcom/jscape/util/aq;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/util/a/a/q;->f:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/util/a/a/q;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x27

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/util/a/a/q;->b:Lcom/jscape/util/a/a/a;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x3

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/jscape/util/a/a/q;->c:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/a/a/q;->d:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object v1
.end method
