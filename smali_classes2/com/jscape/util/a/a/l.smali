.class public Lcom/jscape/util/a/a/l;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/a/a/p;


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x3d

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x61

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\u000fN\u0019]\u001bq9;P[U\u000ec##F\u0015@\u000f$0!V\u0015P\\b9<\u0003\u0016A\u0008q7\"O\u0002\u0014\u0019|5\"V\u0008]\nav!S\u000f]\u0013jv\u0015\u0006\u0008iR\u001f\u001cF\nA\u0015v3*\u0003\u0014D\u0008m9 \u0003 \u0011\u000fYv L\u000f\u0014\u001ak# GU"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x5d

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/a/a/l;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x37

    goto :goto_2

    :cond_2
    const/16 v12, 0x65

    goto :goto_2

    :cond_3
    const/16 v12, 0x1d

    goto :goto_2

    :cond_4
    const/16 v12, 0x55

    goto :goto_2

    :cond_5
    const/16 v12, 0x1a

    goto :goto_2

    :cond_6
    const/16 v12, 0x42

    goto :goto_2

    :cond_7
    const/16 v12, 0x2f

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/util/a/a/k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/a/a/f;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p1}, Lcom/jscape/util/a/a/k;->d()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/util/a/a/f;

    sget-object v1, Lcom/jscape/util/a/a/l;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/jscape/util/a/a/k;->c()Ljava/util/List;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/jscape/util/a/a/f;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/jscape/util/a/a/f; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/a/l;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1
.end method

.method private a(Lcom/jscape/util/a/a/k;Ljava/util/Iterator;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/a/a/k;",
            "Ljava/util/Iterator<",
            "Lcom/jscape/util/a/a/i;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/a/a/f;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/a/a/q;->g()[Lcom/jscape/util/aq;

    move-result-object v0

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/util/a/a/i;

    :try_start_0
    invoke-virtual {v1}, Lcom/jscape/util/a/a/i;->b()Z

    move-result v2
    :try_end_0
    .catch Lcom/jscape/util/a/a/f; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    if-nez v2, :cond_2

    :try_start_1
    invoke-virtual {v1}, Lcom/jscape/util/a/a/i;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/jscape/util/a/a/k;->c(Ljava/lang/String;)Z

    move-result v2
    :try_end_1
    .catch Lcom/jscape/util/a/a/f; {:try_start_1 .. :try_end_1} :catch_2

    :cond_1
    if-nez v2, :cond_3

    :cond_2
    if-nez v0, :cond_0

    goto :goto_0

    :cond_3
    :try_start_2
    new-instance p2, Lcom/jscape/util/a/a/f;

    sget-object v0, Lcom/jscape/util/a/a/l;->a:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/jscape/util/a/a/k;->c()Ljava/util/List;

    move-result-object p1

    aput-object p1, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/util/a/a/f;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_2
    .catch Lcom/jscape/util/a/a/f; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/a/l;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/util/a/a/l;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Lcom/jscape/util/a/a/f; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/a/l;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1

    :cond_4
    :goto_0
    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/util/a/a/m;Lcom/jscape/util/a/a/j;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/a/a/f;
        }
    .end annotation

    check-cast p1, Lcom/jscape/util/a/a/k;

    invoke-static {}, Lcom/jscape/util/a/a/q;->g()[Lcom/jscape/util/aq;

    move-result-object v0

    invoke-virtual {p2}, Lcom/jscape/util/a/a/j;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/util/a/a/i;

    if-eqz v0, :cond_5

    :try_start_0
    invoke-virtual {v1}, Lcom/jscape/util/a/a/i;->b()Z

    move-result v2
    :try_end_0
    .catch Lcom/jscape/util/a/a/f; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    if-nez v2, :cond_3

    :try_start_1
    invoke-virtual {v1}, Lcom/jscape/util/a/a/i;->a()Ljava/lang/String;

    move-result-object v2
    :try_end_1
    .catch Lcom/jscape/util/a/a/f; {:try_start_1 .. :try_end_1} :catch_3

    if-eqz v0, :cond_2

    :try_start_2
    invoke-virtual {p1, v2}, Lcom/jscape/util/a/a/k;->c(Ljava/lang/String;)Z

    move-result v2
    :try_end_2
    .catch Lcom/jscape/util/a/a/f; {:try_start_2 .. :try_end_2} :catch_4

    :cond_1
    if-eqz v2, :cond_3

    :try_start_3
    invoke-direct {p0, p1, p2}, Lcom/jscape/util/a/a/l;->a(Lcom/jscape/util/a/a/k;Ljava/util/Iterator;)V

    invoke-virtual {v1}, Lcom/jscape/util/a/a/i;->a()Ljava/lang/String;

    move-result-object v2
    :try_end_3
    .catch Lcom/jscape/util/a/a/f; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    invoke-virtual {p1, v2}, Lcom/jscape/util/a/a/k;->b(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/jscape/util/a/a/i;->c()V

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/a/l;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1

    :cond_3
    if-nez v0, :cond_0

    goto :goto_0

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/util/a/a/l;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Lcom/jscape/util/a/a/f; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/util/a/a/l;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Lcom/jscape/util/a/a/f; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/util/a/a/l;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Lcom/jscape/util/a/a/f; {:try_start_6 .. :try_end_6} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/a/l;->a(Lcom/jscape/util/a/a/f;)Lcom/jscape/util/a/a/f;

    move-result-object p1

    throw p1

    :cond_4
    :goto_0
    invoke-direct {p0, p1}, Lcom/jscape/util/a/a/l;->a(Lcom/jscape/util/a/a/k;)V

    :cond_5
    return-void
.end method
