.class public Lcom/jscape/util/a/i;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/jscape/util/a/l;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/jscape/util/a/l;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Lcom/jscape/util/a/l;

    invoke-interface {p1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/jscape/util/a/l;

    invoke-direct {p0, p1}, Lcom/jscape/util/a/i;-><init>([Lcom/jscape/util/a/l;)V

    return-void
.end method

.method public varargs constructor <init>([Lcom/jscape/util/a/l;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/util/a/i;->a:Ljava/util/Map;

    invoke-static {}, Lcom/jscape/util/a/b;->f()I

    move-result v0

    array-length v1, p1

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v1, :cond_1

    aget-object v3, p1, v2

    iget-object v4, p0, Lcom/jscape/util/a/i;->a:Ljava/util/Map;

    const/4 v5, 0x0

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method private static a(Lcom/jscape/util/a/m;)Lcom/jscape/util/a/m;
    .locals 0

    return-object p0
.end method

.method private b([Ljava/lang/String;)V
    .locals 5

    invoke-direct {p0, p1}, Lcom/jscape/util/a/i;->c([Ljava/lang/String;)Lcom/jscape/util/ab;

    move-result-object p1

    invoke-static {}, Lcom/jscape/util/a/b;->g()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/util/a/i;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/util/a/l;

    invoke-interface {v2, p1}, Lcom/jscape/util/a/l;->a(Lcom/jscape/util/ab;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/jscape/util/a/i;->a:Ljava/util/Map;

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private c([Ljava/lang/String;)Lcom/jscape/util/ab;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Lcom/jscape/util/ab<",
            "Lcom/jscape/util/a/h;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/jscape/util/a/b;->f()I

    move-result v1

    array-length v2, p1

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v2, :cond_1

    aget-object v4, p1, v3

    new-instance v5, Lcom/jscape/util/a/h;

    invoke-direct {v5, v4}, Lcom/jscape/util/a/h;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    if-eqz v1, :cond_0

    :cond_1
    new-instance p1, Lcom/jscape/util/ab;

    invoke-direct {p1, v0}, Lcom/jscape/util/ab;-><init>(Ljava/util/Collection;)V

    return-object p1
.end method


# virtual methods
.method public a(Lcom/jscape/util/a/l;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/a/i;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public a([Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/jscape/util/a/i;->b([Ljava/lang/String;)V

    return-void
.end method

.method public a()[Lcom/jscape/util/a/l;
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/a/i;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    new-array v1, v1, [Lcom/jscape/util/a/l;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/util/a/l;

    return-object v0
.end method

.method public b(Lcom/jscape/util/a/l;)Z
    .locals 0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/util/a/i;->a(Lcom/jscape/util/a/l;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lcom/jscape/util/a/m; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/a/i;->a(Lcom/jscape/util/a/m;)Lcom/jscape/util/a/m;

    move-result-object p1

    throw p1
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/jscape/util/a/b;->f()I

    move-result v1

    invoke-virtual {p0}, Lcom/jscape/util/a/i;->a()[Lcom/jscape/util/a/l;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    const/4 v3, 0x0

    :cond_0
    array-length v4, v2

    if-ge v3, v4, :cond_2

    if-lez v3, :cond_1

    :try_start_0
    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Lcom/jscape/util/a/m; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/a/i;->a(Lcom/jscape/util/a/m;)Lcom/jscape/util/a/m;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    aget-object v4, v2, v3

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    if-eqz v1, :cond_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
