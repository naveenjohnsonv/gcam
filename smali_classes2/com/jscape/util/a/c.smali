.class public Lcom/jscape/util/a/c;
.super Lcom/jscape/util/a/b;


# static fields
.field protected static final j:Ljava/lang/String; = "|"


# instance fields
.field private k:Ljava/util/List;


# direct methods
.method public constructor <init>([Ljava/lang/String;ZI)V
    .locals 1

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-direct {p0, v0, p2, p3}, Lcom/jscape/util/a/b;-><init>(Ljava/lang/String;ZI)V

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/util/a/c;->k:Ljava/util/List;

    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/a/c;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method protected e()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {}, Lcom/jscape/util/a/b;->g()I

    move-result v1

    iget-object v2, p0, Lcom/jscape/util/a/c;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v2, :cond_2

    if-lez v3, :cond_1

    const-string v4, "|"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    iget-object v4, p0, Lcom/jscape/util/a/c;->k:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    add-int/lit8 v3, v3, 0x1

    if-nez v1, :cond_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    invoke-static {}, Lcom/jscape/util/a/b;->f()I

    move-result v0

    if-nez v0, :cond_1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    move-object v1, p1

    goto :goto_0

    :cond_1
    move-object v1, p0

    :goto_0
    const/4 v2, 0x0

    if-eqz v1, :cond_5

    if-nez v0, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v1, v3, :cond_2

    goto :goto_2

    :cond_2
    if-nez v0, :cond_4

    invoke-super {p0, p1}, Lcom/jscape/util/a/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    return v2

    :cond_3
    check-cast p1, Lcom/jscape/util/a/c;

    goto :goto_1

    :cond_4
    move-object p1, p0

    :goto_1
    iget-object v0, p0, Lcom/jscape/util/a/c;->k:Ljava/util/List;

    iget-object p1, p1, Lcom/jscape/util/a/c;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_5
    :goto_2
    return v2
.end method

.method public h()[Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/a/c;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    invoke-super {p0}, Lcom/jscape/util/a/b;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/jscape/util/a/c;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
