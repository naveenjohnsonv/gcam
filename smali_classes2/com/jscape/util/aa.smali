.class public Lcom/jscape/util/aa;
.super Ljava/lang/Object;


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field private final a:J

.field private b:J


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "$\u0017`Ir\u0010\u0001\u001e\u001faUc\r\u0010*\u001fi\u0000jY\u0017\u0001\n-Vg\\\u000f\rP"

    const/16 v5, 0x1e

    const/16 v6, 0xd

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x59

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x23

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v14, v11

    const/4 v15, 0x0

    :goto_2
    if-gt v14, v15, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v13, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x1f

    const/16 v4, 0x14

    const-string v6, "Pe\u0013z\u0015$c`a\u001a?\u0012> de\u001b/\u0019d\n>$\u00145\t$twvJ"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v17, v6

    move v6, v4

    move-object/from16 v4, v17

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    add-int/2addr v7, v10

    add-int v9, v7, v6

    invoke-virtual {v4, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v9, v12

    const/4 v13, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/aa;->c:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v15

    rem-int/lit8 v2, v15, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    move v2, v12

    goto :goto_4

    :cond_4
    const/16 v2, 0x69

    goto :goto_4

    :cond_5
    const/16 v2, 0x5f

    goto :goto_4

    :cond_6
    const/16 v2, 0x79

    goto :goto_4

    :cond_7
    const/16 v2, 0x54

    goto :goto_4

    :cond_8
    const/16 v2, 0x27

    goto :goto_4

    :cond_9
    const/16 v2, 0x31

    :goto_4
    xor-int/2addr v2, v9

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method public constructor <init>(J)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/jscape/util/aa;->c:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    const-wide/16 v1, 0x0

    invoke-static {p1, p2, v1, v2, v0}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    iput-wide p1, p0, Lcom/jscape/util/aa;->a:J

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/util/aa;->a:J

    return-wide v0
.end method

.method public a(J)Lcom/jscape/util/aa;
    .locals 3

    sget-object v0, Lcom/jscape/util/aa;->c:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    const-wide/16 v1, 0x0

    invoke-static {p1, p2, v1, v2, v0}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    invoke-virtual {p0}, Lcom/jscape/util/aa;->c()J

    move-result-wide v0

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    iget-wide v0, p0, Lcom/jscape/util/aa;->b:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/jscape/util/aa;->b:J

    return-object p0
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/util/aa;->b:J

    return-wide v0
.end method

.method public c()J
    .locals 4

    iget-wide v0, p0, Lcom/jscape/util/aa;->a:J

    iget-wide v2, p0, Lcom/jscape/util/aa;->b:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public d()Lcom/jscape/util/aa;
    .locals 2

    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/jscape/util/aa;->a(J)Lcom/jscape/util/aa;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/jscape/util/aa;
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/jscape/util/aa;->b:J

    return-object p0
.end method

.method public f()Z
    .locals 5

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    iget-wide v1, p0, Lcom/jscape/util/aa;->a:J

    iget-wide v3, p0, Lcom/jscape/util/aa;->b:J

    cmp-long v1, v1, v3

    if-eqz v0, :cond_1

    if-gtz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/aa;->c:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/util/aa;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/jscape/util/aa;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
