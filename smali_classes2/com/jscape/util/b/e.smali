.class public Lcom/jscape/util/b/e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "TT;>;"
    }
.end annotation


# static fields
.field private static final g:[Ljava/lang/String;


# instance fields
.field private final a:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field private final b:I

.field private final c:I

.field private d:I

.field private final e:Z

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "} \tJGwe9=\u0012} \u000cA@dx0l5@Zye8o\u000b\u0012\u0007} \u0017JHt,\u000b} \u0015@Zye8o\u000b\u0012"

    const/16 v4, 0x30

    const/16 v5, 0x9

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/4 v8, 0x1

    add-int/2addr v6, v8

    add-int v9, v6, v5

    invoke-virtual {v3, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0x4f

    move v12, v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    array-length v13, v9

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v12, Ljava/lang/String;

    invoke-direct {v12, v9}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v12}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v11, :cond_1

    add-int/lit8 v11, v7, 0x1

    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v11

    goto :goto_0

    :cond_0
    const/16 v4, 0x1b

    const/16 v3, 0xf

    const-string v5, "$H<atXB\u001f\u001c\"wtXLP\u000bA\u001c6ep^K\u0001S.."

    move v7, v11

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v7, 0x1

    aput-object v9, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v11

    :goto_3
    const/16 v12, 0x73

    add-int/2addr v6, v8

    add-int v9, v6, v5

    invoke-virtual {v3, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/util/b/e;->g:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v9, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v8, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v1, 0x5e

    goto :goto_4

    :cond_4
    const/16 v1, 0x5f

    goto :goto_4

    :cond_5
    const/16 v1, 0x66

    goto :goto_4

    :cond_6
    const/16 v1, 0x60

    goto :goto_4

    :cond_7
    const/16 v1, 0x2a

    goto :goto_4

    :cond_8
    move v1, v10

    goto :goto_4

    :cond_9
    const/16 v1, 0x1e

    :goto_4
    xor-int/2addr v1, v12

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v9, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>([Ljava/lang/Object;IZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;IZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    iput-object p1, p0, Lcom/jscape/util/b/e;->a:[Ljava/lang/Object;

    if-nez v0, :cond_1

    if-eqz p3, :cond_0

    move v1, p2

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move v1, p3

    :goto_0
    :try_start_0
    iput v1, p0, Lcom/jscape/util/b/e;->b:I
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_2

    if-eqz p3, :cond_3

    :try_start_1
    array-length p1, p1
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_1

    move p2, p1

    goto :goto_1

    :cond_2
    move p2, p3

    :cond_3
    :goto_1
    iput p2, p0, Lcom/jscape/util/b/e;->c:I

    iget p1, p0, Lcom/jscape/util/b/e;->b:I

    iput p1, p0, Lcom/jscape/util/b/e;->d:I

    iput-boolean p3, p0, Lcom/jscape/util/b/e;->e:Z

    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/util/b/e;->a(Ljava/lang/UnsupportedOperationException;)Ljava/lang/UnsupportedOperationException;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/b/e;->a(Ljava/lang/UnsupportedOperationException;)Ljava/lang/UnsupportedOperationException;

    move-result-object p1

    throw p1
.end method

.method private static a(Ljava/lang/UnsupportedOperationException;)Ljava/lang/UnsupportedOperationException;
    .locals 0

    return-object p0
.end method

.method private b()V
    .locals 3

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget v1, p0, Lcom/jscape/util/b/e;->d:I

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/jscape/util/b/e;->a:[Ljava/lang/Object;

    array-length v2, v2
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    if-lt v1, v2, :cond_2

    if-nez v0, :cond_1

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/util/b/e;->e:Z
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/util/b/e;->d:I

    :cond_2
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/util/b/e;->a(Ljava/lang/UnsupportedOperationException;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/util/b/e;->a(Ljava/lang/UnsupportedOperationException;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/b/e;->a(Ljava/lang/UnsupportedOperationException;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget v0, p0, Lcom/jscape/util/b/e;->b:I

    iput v0, p0, Lcom/jscape/util/b/e;->d:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/util/b/e;->f:I

    return-void
.end method

.method public hasNext()Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget v1, p0, Lcom/jscape/util/b/e;->f:I

    if-nez v0, :cond_1

    iget v0, p0, Lcom/jscape/util/b/e;->c:I
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    if-ge v1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/util/b/e;->a(Ljava/lang/UnsupportedOperationException;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/b/e;->a(Ljava/lang/UnsupportedOperationException;)Ljava/lang/UnsupportedOperationException;

    move-result-object v0

    throw v0
.end method

.method public next()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/b/e;->a:[Ljava/lang/Object;

    iget v1, p0, Lcom/jscape/util/b/e;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/jscape/util/b/e;->d:I

    aget-object v0, v0, v1

    iget v1, p0, Lcom/jscape/util/b/e;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/jscape/util/b/e;->f:I

    invoke-direct {p0}, Lcom/jscape/util/b/e;->b()V

    return-object v0
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/jscape/util/b/e;->g:[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/util/b/e;->a:[Ljava/lang/Object;

    invoke-static {v3}, Lcom/jscape/util/v;->d([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/jscape/util/b/e;->b:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/jscape/util/b/e;->c:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v3, 0x3

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/jscape/util/b/e;->d:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/jscape/util/b/e;->e:Z

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/util/b/e;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v2, 0x7d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    new-array v0, v3, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-object v1
.end method
