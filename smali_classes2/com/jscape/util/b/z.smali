.class public Lcom/jscape/util/b/z;
.super Lcom/jscape/util/g/H;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/jscape/util/g/H<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "TU;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/jscape/util/g/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/g/e<",
            "-TT;-TU;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/Collection;Lcom/jscape/util/g/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "TU;>;",
            "Lcom/jscape/util/g/e<",
            "-TT;-TU;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/util/g/H;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/b/z;->a:Ljava/util/Collection;

    iput-object p2, p0, Lcom/jscape/util/b/z;->b:Lcom/jscape/util/g/e;

    return-void
.end method

.method constructor <init>(Ljava/util/Collection;Lcom/jscape/util/g/e;Lcom/jscape/util/b/b;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/util/b/z;-><init>(Ljava/util/Collection;Lcom/jscape/util/g/e;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/b/z;->a:Ljava/util/Collection;

    new-instance v1, Lcom/jscape/util/g/a/j;

    iget-object v2, p0, Lcom/jscape/util/b/z;->b:Lcom/jscape/util/g/e;

    invoke-direct {v1, v2, p1}, Lcom/jscape/util/g/a/j;-><init>(Lcom/jscape/util/g/e;Ljava/lang/Object;)V

    invoke-static {v0, v1}, Lcom/jscape/util/b/a;->c(Ljava/util/Collection;Lcom/jscape/util/g/H;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
