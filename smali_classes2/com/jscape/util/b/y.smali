.class public Lcom/jscape/util/b/y;
.super Lcom/jscape/util/g/z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Lcom/jscape/util/g/O<",
        "-TT;>;>",
        "Lcom/jscape/util/g/z<",
        "Ljava/util/Iterator<",
        "TT;>;TU;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/util/g/O;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TU;"
        }
    .end annotation
.end field

.field private final c:Lcom/jscape/util/g/q;


# direct methods
.method public constructor <init>(Lcom/jscape/util/g/O;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TU;)V"
        }
    .end annotation

    sget-object v0, Lcom/jscape/util/g/q;->a:Lcom/jscape/util/g/q;

    invoke-direct {p0, p1, v0}, Lcom/jscape/util/b/y;-><init>(Lcom/jscape/util/g/O;Lcom/jscape/util/g/q;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/util/g/O;Lcom/jscape/util/g/q;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TU;",
            "Lcom/jscape/util/g/q;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/util/g/z;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/b/y;->a:Lcom/jscape/util/g/O;

    iput-object p2, p0, Lcom/jscape/util/b/y;->c:Lcom/jscape/util/g/q;

    return-void
.end method


# virtual methods
.method public a(Ljava/util/Iterator;)Lcom/jscape/util/g/O;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator<",
            "TT;>;)TU;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/b/y;->a:Lcom/jscape/util/g/O;

    iget-object v1, p0, Lcom/jscape/util/b/y;->c:Lcom/jscape/util/g/q;

    invoke-static {p1, v0, v1}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/O;Lcom/jscape/util/g/q;)Lcom/jscape/util/g/O;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Iterator;

    invoke-virtual {p0, p1}, Lcom/jscape/util/b/y;->a(Ljava/util/Iterator;)Lcom/jscape/util/g/O;

    move-result-object p1

    return-object p1
.end method
