.class public Lcom/jscape/util/b/k;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/b/m;


# instance fields
.field protected final a:[I

.field protected final b:I

.field protected final c:I

.field protected d:I


# direct methods
.method public constructor <init>([I)V
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/jscape/util/b/k;-><init>([III)V

    return-void
.end method

.method public constructor <init>([III)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/b/k;->a:[I

    iput p2, p0, Lcom/jscape/util/b/k;->b:I

    iput p3, p0, Lcom/jscape/util/b/k;->c:I

    iput p2, p0, Lcom/jscape/util/b/k;->d:I

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 3

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/util/b/k;->d:I

    if-nez v0, :cond_1

    iget v0, p0, Lcom/jscape/util/b/k;->b:I

    iget v2, p0, Lcom/jscape/util/b/k;->c:I

    add-int/2addr v0, v2

    if-ge v1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public b()I
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/b/k;->a:[I

    iget v1, p0, Lcom/jscape/util/b/k;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/jscape/util/b/k;->d:I

    aget v0, v0, v1

    return v0
.end method

.method public c()V
    .locals 1

    iget v0, p0, Lcom/jscape/util/b/k;->b:I

    iput v0, p0, Lcom/jscape/util/b/k;->d:I

    return-void
.end method
