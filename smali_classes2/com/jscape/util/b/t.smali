.class public Lcom/jscape/util/b/t;
.super Lcom/jscape/util/g/z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/jscape/util/g/z<",
        "Ljava/util/Collection<",
        "TT;>;",
        "Ljava/util/List<",
        "TT;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/util/g/H;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/g/H<",
            "-TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    new-instance v0, Lcom/jscape/util/g/N;

    invoke-direct {v0}, Lcom/jscape/util/g/N;-><init>()V

    invoke-direct {p0, v0}, Lcom/jscape/util/b/t;-><init>(Lcom/jscape/util/g/H;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/util/g/H;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/util/g/z;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/b/t;->a:Lcom/jscape/util/g/H;

    return-void
.end method


# virtual methods
.method public a(Ljava/util/Collection;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/b/t;->a:Lcom/jscape/util/g/H;

    invoke-static {p1, v0}, Lcom/jscape/util/b/a;->a(Ljava/util/Collection;Lcom/jscape/util/g/H;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/jscape/util/b/t;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
