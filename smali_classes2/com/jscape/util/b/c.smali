.class public Lcom/jscape/util/b/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "TT;>;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private final a:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, ".CiQl\u007fw\u0013VaL{n>\u0013H$Punw\u000fStNuh#\u0019B*"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/b/c;->c:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v5, 0x8

    if-eqz v4, :cond_4

    const/4 v6, 0x1

    if-eq v4, v6, :cond_3

    const/4 v6, 0x2

    if-eq v4, v6, :cond_2

    const/4 v6, 0x3

    if-eq v4, v6, :cond_1

    const/4 v6, 0x4

    if-eq v4, v6, :cond_5

    const/4 v6, 0x5

    if-eq v4, v6, :cond_5

    const/16 v5, 0x45

    goto :goto_1

    :cond_1
    const/16 v5, 0x2c

    goto :goto_1

    :cond_2
    const/16 v5, 0x16

    goto :goto_1

    :cond_3
    const/16 v5, 0x34

    goto :goto_1

    :cond_4
    const/16 v5, 0x6e

    :cond_5
    :goto_1
    const/16 v4, 0x12

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>([Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/b/c;->a:[Ljava/lang/Object;

    const/4 p1, -0x1

    iput p1, p0, Lcom/jscape/util/b/c;->b:I

    return-void
.end method

.method public static a([Ljava/lang/Object;)Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/b/c;

    invoke-direct {v0, p0}, Lcom/jscape/util/b/c;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method

.method private static a(Ljava/util/NoSuchElementException;)Ljava/util/NoSuchElementException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public hasNext()Z
    .locals 3

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget v1, p0, Lcom/jscape/util/b/c;->b:I

    const/4 v2, 0x1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jscape/util/b/c;->a:[Ljava/lang/Object;

    array-length v0, v0
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    sub-int/2addr v0, v2

    if-ge v1, v0, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/util/b/c;->a(Ljava/util/NoSuchElementException;)Ljava/util/NoSuchElementException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/b/c;->a(Ljava/util/NoSuchElementException;)Ljava/util/NoSuchElementException;

    move-result-object v0

    throw v0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/util/b/c;->hasNext()Z

    move-result v0
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/util/b/c;->a:[Ljava/lang/Object;

    iget v1, p0, Lcom/jscape/util/b/c;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/jscape/util/b/c;->b:I

    aget-object v0, v0, v1

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/b/c;->a(Ljava/util/NoSuchElementException;)Ljava/util/NoSuchElementException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/b/c;->a(Ljava/util/NoSuchElementException;)Ljava/util/NoSuchElementException;

    move-result-object v0

    throw v0

    :cond_1
    move-object v0, p0

    :goto_0
    return-object v0
.end method

.method public remove()V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    sget-object v1, Lcom/jscape/util/b/c;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
