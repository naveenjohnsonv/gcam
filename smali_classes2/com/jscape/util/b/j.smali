.class public Lcom/jscape/util/b/j;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "TT;>;"
    }
.end annotation


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator<",
            "+TT;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/jscape/util/g/H;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/g/H<",
            "-TT;>;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private d:Z

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "@q=\u0011Fd6`n \u0008]*#%f#\u001bY!*q-"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/b/j;->f:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v5, 0x5c

    if-eqz v4, :cond_5

    const/4 v6, 0x1

    if-eq v4, v6, :cond_4

    const/4 v6, 0x2

    if-eq v4, v6, :cond_3

    const/4 v6, 0x3

    if-eq v4, v6, :cond_2

    const/4 v6, 0x4

    if-eq v4, v6, :cond_1

    const/4 v6, 0x5

    goto :goto_1

    :cond_1
    const/16 v5, 0x2c

    goto :goto_1

    :cond_2
    const/16 v5, 0x66

    goto :goto_1

    :cond_3
    const/16 v5, 0x57

    goto :goto_1

    :cond_4
    const/16 v5, 0x1b

    goto :goto_1

    :cond_5
    const/16 v5, 0x1d

    :goto_1
    const/16 v4, 0x18

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/util/Iterator;Lcom/jscape/util/g/H;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator<",
            "+TT;>;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/b/j;->a:Ljava/util/Iterator;

    iput-object p2, p0, Lcom/jscape/util/b/j;->b:Lcom/jscape/util/g/H;

    return-void
.end method

.method private a()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/b/j;->c:Ljava/lang/Object;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/jscape/util/b/j;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public static a(Ljava/util/Collection;Lcom/jscape/util/g/H;)Ljava/util/Iterator;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "+TT;>;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/b/j;->a(Ljava/util/Iterator;Lcom/jscape/util/g/H;)Ljava/util/Iterator;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Enumeration;Lcom/jscape/util/g/H;)Ljava/util/Iterator;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Enumeration<",
            "TT;>;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/b/h;->a(Ljava/util/Enumeration;)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/b/j;->a(Ljava/util/Iterator;Lcom/jscape/util/g/H;)Ljava/util/Iterator;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Iterator;Lcom/jscape/util/g/H;)Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator<",
            "+TT;>;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/b/j;

    invoke-direct {v0, p0, p1}, Lcom/jscape/util/b/j;-><init>(Ljava/util/Iterator;Lcom/jscape/util/g/H;)V

    return-object v0
.end method

.method public static a([Ljava/lang/Object;Lcom/jscape/util/g/H;)Ljava/util/Iterator;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/b/c;->a([Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/b/j;->a(Ljava/util/Iterator;Lcom/jscape/util/g/H;)Ljava/util/Iterator;

    move-result-object p0

    return-object p0
.end method

.method private static a(Ljava/util/NoSuchElementException;)Ljava/util/NoSuchElementException;
    .locals 0

    return-object p0
.end method

.method private b()Z
    .locals 3

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/jscape/util/b/j;->a:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/util/b/j;->d:Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_2

    if-nez v0, :cond_2

    if-nez v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/util/b/j;->a:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/util/b/j;->c:Ljava/lang/Object;

    iget-object v2, p0, Lcom/jscape/util/b/j;->b:Lcom/jscape/util/g/H;

    invoke-virtual {v2, v1}, Lcom/jscape/util/g/H;->a(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/jscape/util/b/j;->d:Z
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_2

    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/util/b/j;->a(Ljava/util/NoSuchElementException;)Ljava/util/NoSuchElementException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/util/NoSuchElementException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/util/b/j;->a(Ljava/util/NoSuchElementException;)Ljava/util/NoSuchElementException;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/util/NoSuchElementException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/b/j;->a(Ljava/util/NoSuchElementException;)Ljava/util/NoSuchElementException;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/jscape/util/b/j;->e:Z

    iget-boolean v1, p0, Lcom/jscape/util/b/j;->d:Z

    :cond_2
    if-nez v0, :cond_4

    if-nez v1, :cond_3

    const/4 v0, 0x0

    :try_start_4
    iput-object v0, p0, Lcom/jscape/util/b/j;->c:Ljava/lang/Object;
    :try_end_4
    .catch Ljava/util/NoSuchElementException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_1

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/b/j;->a(Ljava/util/NoSuchElementException;)Ljava/util/NoSuchElementException;

    move-result-object v0

    throw v0

    :cond_3
    :goto_1
    iget-boolean v1, p0, Lcom/jscape/util/b/j;->d:Z

    :cond_4
    return v1
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/util/b/j;->d:Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_2

    if-nez v1, :cond_1

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/util/b/j;->b()Z

    move-result v1

    if-nez v0, :cond_2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move v0, v1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/b/j;->a(Ljava/util/NoSuchElementException;)Ljava/util/NoSuchElementException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/util/b/j;->a(Ljava/util/NoSuchElementException;)Ljava/util/NoSuchElementException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/util/NoSuchElementException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/b/j;->a(Ljava/util/NoSuchElementException;)Ljava/util/NoSuchElementException;

    move-result-object v0

    throw v0
.end method

.method public next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/util/b/j;->hasNext()Z

    move-result v0
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/jscape/util/b/j;->d:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/util/b/j;->e:Z

    invoke-direct {p0}, Lcom/jscape/util/b/j;->a()Ljava/lang/Object;

    move-result-object v0
    :try_end_1
    .catch Ljava/util/NoSuchElementException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/util/b/j;->a(Ljava/util/NoSuchElementException;)Ljava/util/NoSuchElementException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/util/NoSuchElementException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/b/j;->a(Ljava/util/NoSuchElementException;)Ljava/util/NoSuchElementException;

    move-result-object v0

    throw v0

    :cond_1
    move-object v0, p0

    :goto_0
    return-object v0
.end method

.method public remove()V
    .locals 2

    iget-boolean v0, p0, Lcom/jscape/util/b/j;->e:Z

    sget-object v1, Lcom/jscape/util/b/j;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/jscape/util/aq;->c(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/jscape/util/b/j;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/util/b/j;->e:Z

    return-void
.end method
