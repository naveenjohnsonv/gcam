.class public Lcom/jscape/util/b/f;
.super Ljava/lang/Object;


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field private final a:[I

.field private b:I

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x18

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x19

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "w\u001d`lA\"TF=|{v;SR\u0011`/O*T@\u0015/\u000b\u0018T}yQ<SX\u001be2\u000b\u0018Tb`G\'A]\u001b|2"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x30

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/b/f;->d:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/16 v13, 0x2d

    if-eqz v12, :cond_6

    if-eq v12, v7, :cond_5

    const/4 v14, 0x2

    if-eq v12, v14, :cond_4

    if-eq v12, v0, :cond_3

    const/4 v14, 0x4

    if-eq v12, v14, :cond_6

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v13, 0x2c

    goto :goto_2

    :cond_2
    const/16 v13, 0x57

    goto :goto_2

    :cond_3
    const/16 v13, 0x16

    goto :goto_2

    :cond_4
    const/16 v13, 0xb

    goto :goto_2

    :cond_5
    const/16 v13, 0x6d

    :cond_6
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array p1, p1, [I

    iput-object p1, p0, Lcom/jscape/util/b/f;->a:[I

    return-void
.end method

.method private f()V
    .locals 2

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/jscape/util/b/f;->b:I

    iget-object v1, p0, Lcom/jscape/util/b/f;->a:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/util/b/f;->b:I

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/util/b/f;->c:Z

    :cond_1
    return-void
.end method


# virtual methods
.method public a()Lcom/jscape/util/b/m;
    .locals 4

    new-instance v0, Lcom/jscape/util/b/l;

    iget-object v1, p0, Lcom/jscape/util/b/f;->a:[I

    iget v2, p0, Lcom/jscape/util/b/f;->b:I

    iget-boolean v3, p0, Lcom/jscape/util/b/f;->c:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/jscape/util/b/l;-><init>([IIZ)V

    return-object v0
.end method

.method public a(I)V
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/b/f;->a:[I

    iget v1, p0, Lcom/jscape/util/b/f;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/jscape/util/b/f;->b:I

    aput p1, v0, v1

    invoke-direct {p0}, Lcom/jscape/util/b/f;->f()V

    return-void
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/b/f;->a:[I

    array-length v0, v0

    return v0
.end method

.method public c()I
    .locals 1

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/jscape/util/b/f;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/util/b/f;->a:[I

    array-length v0, v0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/jscape/util/b/f;->b:I

    :goto_0
    return v0
.end method

.method public d()Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/util/b/f;->c()I

    move-result v1

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/util/b/f;->b:I

    iput-boolean v0, p0, Lcom/jscape/util/b/f;->c:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/b/f;->d:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/b/f;->a:[I

    invoke-static {v2}, Lcom/jscape/util/v;->b([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/util/b/f;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/util/b/f;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
