.class public Lcom/jscape/util/b/w;
.super Lcom/jscape/util/g/z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/jscape/util/g/z<",
        "Ljava/util/Iterator<",
        "TT;>;TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/util/g/H;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/g/H<",
            "-TT;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/jscape/util/g/H;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/jscape/util/b/w;-><init>(Lcom/jscape/util/g/H;Ljava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/util/g/H;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/g/H<",
            "-TT;>;TT;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/util/g/z;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/b/w;->a:Lcom/jscape/util/g/H;

    iput-object p2, p0, Lcom/jscape/util/b/w;->c:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(Ljava/util/Iterator;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator<",
            "TT;>;)TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/b/w;->a:Lcom/jscape/util/g/H;

    iget-object v1, p0, Lcom/jscape/util/b/w;->c:Ljava/lang/Object;

    invoke-static {p1, v0, v1}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/H;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Iterator;

    invoke-virtual {p0, p1}, Lcom/jscape/util/b/w;->a(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
