.class public Lcom/jscape/util/b/o;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/b/q;


# static fields
.field private static final g:[Ljava/lang/String;


# instance fields
.field protected final a:[J

.field protected final b:I

.field protected final c:I

.field protected d:I


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "~\u0010\u001a^\u001c!\u007f3\\#_\u0006<b;_\u001d\r\u0018\u001e_\u001dW4\'d3I:D\u0010\'w&_\u0001\u0010\u000e1w&QN"

    const/16 v5, 0x2b

    const/16 v6, 0x12

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x14

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x15

    const/16 v4, 0x9

    const-string v6, "`\u000e\u0001K\u0005,|$\u0013\u000b`\u000e\u001dA\u0018\"|%A\u0003\u0013"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v17, v6

    move v6, v4

    move-object/from16 v4, v17

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0xa

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/b/o;->g:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    const/16 v16, 0x24

    const/4 v3, 0x2

    if-eqz v2, :cond_7

    if-eq v2, v10, :cond_8

    if-eq v2, v3, :cond_6

    const/4 v3, 0x3

    if-eq v2, v3, :cond_8

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v16, 0x2

    goto :goto_4

    :cond_4
    const/16 v16, 0x41

    goto :goto_4

    :cond_5
    const/16 v16, 0x61

    goto :goto_4

    :cond_6
    const/16 v16, 0x67

    goto :goto_4

    :cond_7
    const/16 v16, 0x46

    :cond_8
    :goto_4
    xor-int v2, v9, v16

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>([J)V
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/jscape/util/b/o;-><init>([JII)V

    return-void
.end method

.method public constructor <init>([JII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/b/o;->a:[J

    iput p2, p0, Lcom/jscape/util/b/o;->b:I

    iput p3, p0, Lcom/jscape/util/b/o;->c:I

    iput p2, p0, Lcom/jscape/util/b/o;->d:I

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/util/b/o;->d:I

    if-nez v0, :cond_1

    iget v0, p0, Lcom/jscape/util/b/o;->c:I

    if-ge v1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public b()J
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/b/o;->a:[J

    iget v1, p0, Lcom/jscape/util/b/o;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/jscape/util/b/o;->d:I

    aget-wide v0, v0, v1

    return-wide v0
.end method

.method public c()V
    .locals 1

    iget v0, p0, Lcom/jscape/util/b/o;->b:I

    iput v0, p0, Lcom/jscape/util/b/o;->d:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/b/o;->g:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/b/o;->a:[J

    invoke-static {v2}, Lcom/jscape/util/v;->b([J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/util/b/o;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/util/b/o;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/util/b/o;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "JuZq0"

    invoke-static {v1}, Lcom/jscape/util/b/a;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method
