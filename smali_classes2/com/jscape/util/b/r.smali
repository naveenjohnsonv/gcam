.class public Lcom/jscape/util/b/r;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "TR;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator<",
            "+TA;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/jscape/util/g/z;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/g/z<",
            "-TA;+TR;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Iterator;Lcom/jscape/util/g/z;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator<",
            "+TA;>;",
            "Lcom/jscape/util/g/z<",
            "-TA;+TR;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/b/r;->a:Ljava/util/Iterator;

    iput-object p2, p0, Lcom/jscape/util/b/r;->b:Lcom/jscape/util/g/z;

    return-void
.end method

.method public static a(Ljava/util/Collection;Lcom/jscape/util/g/z;)Ljava/util/Iterator;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "+TA;>;",
            "Lcom/jscape/util/g/z<",
            "-TA;+TR;>;)",
            "Ljava/util/Iterator<",
            "TR;>;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/b/r;->a(Ljava/util/Iterator;Lcom/jscape/util/g/z;)Ljava/util/Iterator;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Enumeration;Lcom/jscape/util/g/z;)Ljava/util/Iterator;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Enumeration<",
            "TA;>;",
            "Lcom/jscape/util/g/z<",
            "-TA;+TR;>;)",
            "Ljava/util/Iterator<",
            "TR;>;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/b/h;->a(Ljava/util/Enumeration;)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/b/r;->a(Ljava/util/Iterator;Lcom/jscape/util/g/z;)Ljava/util/Iterator;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Iterator;Lcom/jscape/util/g/z;)Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator<",
            "+TA;>;",
            "Lcom/jscape/util/g/z<",
            "-TA;+TR;>;)",
            "Ljava/util/Iterator<",
            "TR;>;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/b/r;

    invoke-direct {v0, p0, p1}, Lcom/jscape/util/b/r;-><init>(Ljava/util/Iterator;Lcom/jscape/util/g/z;)V

    return-object v0
.end method

.method public static a([Ljava/lang/Object;Lcom/jscape/util/g/z;)Ljava/util/Iterator;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">([TA;",
            "Lcom/jscape/util/g/z<",
            "-TA;+TR;>;)",
            "Ljava/util/Iterator<",
            "TR;>;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/b/c;->a([Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/b/r;->a(Ljava/util/Iterator;Lcom/jscape/util/g/z;)Ljava/util/Iterator;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/b/r;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TR;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/b/r;->b:Lcom/jscape/util/g/z;

    iget-object v1, p0, Lcom/jscape/util/b/r;->a:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jscape/util/g/z;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/b/r;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    return-void
.end method
