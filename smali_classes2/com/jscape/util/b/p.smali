.class public Lcom/jscape/util/b/p;
.super Lcom/jscape/util/b/o;


# static fields
.field private static final h:[Ljava/lang/String;


# instance fields
.field private final e:Z

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x7

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v3

    :goto_0
    const/16 v6, 0x46

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v2, v4

    const-string v8, "\u0011+tg3\u0010S\u0013t\u007fcp3\u0000\u0001O+}m$\u0011\u001c[giuo"

    invoke-virtual {v8, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v3

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x1b

    if-ge v2, v4, :cond_0

    invoke-virtual {v8, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v2

    move v2, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/b/p;->h:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x28

    goto :goto_2

    :cond_2
    const/16 v12, 0x32

    goto :goto_2

    :cond_3
    const/16 v12, 0x14

    goto :goto_2

    :cond_4
    const/16 v12, 0x44

    goto :goto_2

    :cond_5
    const/16 v12, 0x40

    goto :goto_2

    :cond_6
    const/16 v12, 0x4d

    goto :goto_2

    :cond_7
    const/16 v12, 0x7b

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>([JIZ)V
    .locals 1

    if-eqz p3, :cond_0

    move v0, p2

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz p3, :cond_1

    array-length p2, p1

    :cond_1
    invoke-direct {p0, p1, v0, p2}, Lcom/jscape/util/b/o;-><init>([JII)V

    iput-boolean p3, p0, Lcom/jscape/util/b/p;->e:Z

    return-void
.end method

.method private d()V
    .locals 3

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/util/b/p;->d:I

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/jscape/util/b/p;->a:[J

    array-length v2, v2

    if-lt v1, v2, :cond_2

    if-nez v0, :cond_1

    iget-boolean v1, p0, Lcom/jscape/util/b/p;->e:Z

    :cond_0
    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/util/b/p;->d:I

    :cond_2
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/util/b/p;->f:I

    if-nez v0, :cond_1

    iget v0, p0, Lcom/jscape/util/b/p;->c:I

    if-ge v1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public b()J
    .locals 3

    invoke-super {p0}, Lcom/jscape/util/b/o;->b()J

    move-result-wide v0

    iget v2, p0, Lcom/jscape/util/b/p;->f:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/jscape/util/b/p;->f:I

    invoke-direct {p0}, Lcom/jscape/util/b/p;->d()V

    return-wide v0
.end method

.method public c()V
    .locals 1

    invoke-super {p0}, Lcom/jscape/util/b/o;->c()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/util/b/p;->f:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/b/p;->h:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/util/b/p;->e:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/util/b/p;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
