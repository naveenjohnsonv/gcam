.class public Lcom/jscape/util/b/a;
.super Ljava/lang/Object;


# static fields
.field private static b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "jPln1"

    invoke-static {v0}, Lcom/jscape/util/b/a;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Collection;Lcom/jscape/util/g/O;)Lcom/jscape/util/g/O;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Lcom/jscape/util/g/O<",
            "-TT;>;>(",
            "Ljava/util/Collection<",
            "TT;>;TU;)TU;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/O;)Lcom/jscape/util/g/O;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Collection;Lcom/jscape/util/g/O;Lcom/jscape/util/g/H;)Lcom/jscape/util/g/O;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Lcom/jscape/util/g/O<",
            "-TT;>;>(",
            "Ljava/util/Collection<",
            "TT;>;TU;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)TU;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1, p2}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/O;Lcom/jscape/util/g/H;)Lcom/jscape/util/g/O;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Collection;Lcom/jscape/util/g/O;Lcom/jscape/util/g/q;)Lcom/jscape/util/g/O;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Lcom/jscape/util/g/O<",
            "-TT;>;>(",
            "Ljava/util/Collection<",
            "TT;>;TU;",
            "Lcom/jscape/util/g/q;",
            ")TU;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1, p2}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/O;Lcom/jscape/util/g/q;)Lcom/jscape/util/g/O;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Iterator;Lcom/jscape/util/g/O;)Lcom/jscape/util/g/O;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Lcom/jscape/util/g/O<",
            "-TT;>;>(",
            "Ljava/util/Iterator<",
            "TT;>;TU;)TU;"
        }
    .end annotation

    sget-object v0, Lcom/jscape/util/g/q;->a:Lcom/jscape/util/g/q;

    invoke-static {p0, p1, v0}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/O;Lcom/jscape/util/g/q;)Lcom/jscape/util/g/O;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Iterator;Lcom/jscape/util/g/O;Lcom/jscape/util/g/H;)Lcom/jscape/util/g/O;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Lcom/jscape/util/g/O<",
            "-TT;>;>(",
            "Ljava/util/Iterator<",
            "TT;>;TU;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)TU;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/jscape/util/g/H;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1, v1}, Lcom/jscape/util/g/O;->a(Ljava/lang/Object;)V

    :cond_1
    if-eqz v0, :cond_0

    :cond_2
    return-object p1
.end method

.method public static a(Ljava/util/Iterator;Lcom/jscape/util/g/O;Lcom/jscape/util/g/q;)Lcom/jscape/util/g/O;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Lcom/jscape/util/g/O<",
            "-TT;>;>(",
            "Ljava/util/Iterator<",
            "TT;>;TU;",
            "Lcom/jscape/util/g/q;",
            ")TU;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {p2}, Lcom/jscape/util/g/q;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/jscape/util/g/O;->a(Ljava/lang/Object;)V

    if-eqz v0, :cond_0

    :cond_1
    return-object p1
.end method

.method public static a([Ljava/lang/Object;Lcom/jscape/util/g/O;)Lcom/jscape/util/g/O;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Lcom/jscape/util/g/O<",
            "-TT;>;>([TT;TU;)TU;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/b/c;->a([Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/O;)Lcom/jscape/util/g/O;

    move-result-object p0

    return-object p0
.end method

.method public static a([Ljava/lang/Object;Lcom/jscape/util/g/O;Lcom/jscape/util/g/H;)Lcom/jscape/util/g/O;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Lcom/jscape/util/g/O<",
            "-TT;>;>([TT;TU;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)TU;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/b/c;->a([Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1, p2}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/O;Lcom/jscape/util/g/H;)Lcom/jscape/util/g/O;

    move-result-object p0

    return-object p0
.end method

.method public static a([Ljava/lang/Object;Lcom/jscape/util/g/O;Lcom/jscape/util/g/q;)Lcom/jscape/util/g/O;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Lcom/jscape/util/g/O<",
            "-TT;>;>([TT;TU;",
            "Lcom/jscape/util/g/q;",
            ")TU;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/b/c;->a([Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1, p2}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/O;Lcom/jscape/util/g/q;)Lcom/jscape/util/g/O;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Collection;Lcom/jscape/util/g/H;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "TT;>;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;TT;)TT;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1, p2}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/H;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Iterator;Lcom/jscape/util/g/H;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator<",
            "TT;>;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;TT;)TT;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    if-nez v0, :cond_4

    if-nez v0, :cond_2

    invoke-virtual {p1, v1}, Lcom/jscape/util/g/H;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object p1, v1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_0

    goto :goto_1

    :cond_2
    :goto_0
    return-object p1

    :cond_3
    :goto_1
    move-object p1, p2

    :cond_4
    return-object p1
.end method

.method public static a([Ljava/lang/Object;Lcom/jscape/util/g/H;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;TT;)TT;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/b/c;->a([Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1, p2}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/H;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Collection;Ljava/util/Collection;Lcom/jscape/util/g/e;)Ljava/util/Collection;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "TT;>;",
            "Ljava/util/Collection<",
            "TU;>;",
            "Lcom/jscape/util/g/e<",
            "-TT;-TU;>;)",
            "Ljava/util/Collection<",
            "TT;>;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1, p2}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Ljava/util/Collection;Lcom/jscape/util/g/e;)Ljava/util/Collection;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Iterator;Ljava/util/Collection;Lcom/jscape/util/g/e;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator<",
            "TT;>;",
            "Ljava/util/Collection<",
            "TU;>;",
            "Lcom/jscape/util/g/e<",
            "-TT;-TU;>;)",
            "Ljava/util/Collection<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/b/z;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/jscape/util/b/z;-><init>(Ljava/util/Collection;Lcom/jscape/util/g/e;Lcom/jscape/util/b/b;)V

    invoke-static {v0}, Lcom/jscape/util/g/H;->a(Lcom/jscape/util/g/H;)Lcom/jscape/util/g/H;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/H;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Collection;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "TT;>;)",
            "Ljava/util/List<",
            "TU;>;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Collection;Lcom/jscape/util/g/H;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "TT;>;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/H;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Iterator;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator<",
            "TT;>;)",
            "Ljava/util/List<",
            "TU;>;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/g/A;

    invoke-direct {v0}, Lcom/jscape/util/g/A;-><init>()V

    invoke-static {p0, v0}, Lcom/jscape/util/b/r;->a(Ljava/util/Iterator;Lcom/jscape/util/g/z;)Ljava/util/Iterator;

    move-result-object p0

    new-instance v0, Lcom/jscape/util/g/N;

    invoke-direct {v0}, Lcom/jscape/util/g/N;-><init>()V

    invoke-static {p0, v0}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/H;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Iterator;Lcom/jscape/util/g/H;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator<",
            "TT;>;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/jscape/util/g/H;->a(Ljava/lang/Object;)Z

    move-result v3

    if-nez v0, :cond_1

    if-eqz v3, :cond_1

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz v0, :cond_0

    :cond_2
    return-object v1
.end method

.method public static a([Lcom/jscape/util/H;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/jscape/util/H<",
            "TT;>;>([TT;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/b/c;->a([Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/util/b/a;->c(Ljava/util/Iterator;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static a([Ljava/lang/Object;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Ljava/util/List<",
            "TU;>;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/b/c;->a([Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static a([Ljava/lang/Object;Lcom/jscape/util/g/H;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/b/c;->a([Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/H;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Collection;Ljava/lang/Class;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "TT;>;",
            "Ljava/lang/Class<",
            "TR;>;)[TR;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/g/A;

    invoke-direct {v0}, Lcom/jscape/util/g/A;-><init>()V

    invoke-static {p0, p1, v0}, Lcom/jscape/util/b/a;->a(Ljava/util/Collection;Ljava/lang/Class;Lcom/jscape/util/g/z;)[Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Collection;Ljava/lang/Class;Lcom/jscape/util/g/z;)[Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "TT;>;",
            "Ljava/lang/Class<",
            "TR;>;",
            "Lcom/jscape/util/g/z<",
            "-TT;+TR;>;)[TR;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1, p2}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Ljava/lang/Class;Lcom/jscape/util/g/z;)[Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Iterator;Ljava/lang/Class;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator<",
            "TT;>;",
            "Ljava/lang/Class<",
            "TR;>;)[TR;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/g/A;

    invoke-direct {v0}, Lcom/jscape/util/g/A;-><init>()V

    invoke-static {p0, p1, v0}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Ljava/lang/Class;Lcom/jscape/util/g/z;)[Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/Iterator;Ljava/lang/Class;Lcom/jscape/util/g/z;)[Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator<",
            "TT;>;",
            "Ljava/lang/Class<",
            "TR;>;",
            "Lcom/jscape/util/g/z<",
            "-TT;+TR;>;)[TR;"
        }
    .end annotation

    invoke-static {p0, p2}, Lcom/jscape/util/b/r;->a(Ljava/util/Iterator;Lcom/jscape/util/g/z;)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;)Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p2

    invoke-static {p1, p2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Object;

    invoke-interface {p0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static a([Ljava/lang/Object;Ljava/lang/Class;)[Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Ljava/lang/Class<",
            "TR;>;)[TR;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/b/c;->a([Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static b(Ljava/util/Collection;Lcom/jscape/util/g/H;)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "TT;>;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)I"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/b/a;->b(Ljava/util/Iterator;Lcom/jscape/util/g/H;)I

    move-result p0

    return p0
.end method

.method public static b(Ljava/util/Iterator;Lcom/jscape/util/g/H;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator<",
            "TT;>;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)I"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/jscape/util/g/H;->a(Ljava/lang/Object;)Z

    move-result v2

    if-nez v0, :cond_3

    if-eqz v2, :cond_1

    add-int/lit8 v1, v1, 0x1

    :cond_1
    if-eqz v0, :cond_0

    :cond_2
    move v2, v1

    :cond_3
    return v2
.end method

.method public static b([Ljava/lang/Object;Lcom/jscape/util/g/H;)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)I"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/b/c;->a([Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/b/a;->b(Ljava/util/Iterator;Lcom/jscape/util/g/H;)I

    move-result p0

    return p0
.end method

.method public static b(Ljava/util/Collection;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "TT;>;)TT;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/util/b/a;->b(Ljava/util/Iterator;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static b(Ljava/util/Iterator;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator<",
            "TT;>;)TT;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    if-nez v0, :cond_1

    if-eqz v0, :cond_0

    :cond_1
    return-object v1
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/util/b/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public static b(Ljava/util/Collection;Ljava/util/Collection;Lcom/jscape/util/g/e;)Ljava/util/Collection;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "TT;>;",
            "Ljava/util/Collection<",
            "TU;>;",
            "Lcom/jscape/util/g/e<",
            "-TT;-TU;>;)",
            "Ljava/util/Collection<",
            "TT;>;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1, p2}, Lcom/jscape/util/b/a;->b(Ljava/util/Iterator;Ljava/util/Collection;Lcom/jscape/util/g/e;)Ljava/util/Collection;

    move-result-object p0

    return-object p0
.end method

.method public static b(Ljava/util/Iterator;Ljava/util/Collection;Lcom/jscape/util/g/e;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator<",
            "TT;>;",
            "Ljava/util/Collection<",
            "TU;>;",
            "Lcom/jscape/util/g/e<",
            "-TT;-TU;>;)",
            "Ljava/util/Collection<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/b/z;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/jscape/util/b/z;-><init>(Ljava/util/Collection;Lcom/jscape/util/g/e;Lcom/jscape/util/b/b;)V

    invoke-static {p0, v0}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/H;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/util/b/a;->b:Ljava/lang/String;

    return-void
.end method

.method public static c(Ljava/util/Collection;Lcom/jscape/util/g/H;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection<",
            "TT;>;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)TT;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/b/a;->c(Ljava/util/Iterator;Lcom/jscape/util/g/H;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static c(Ljava/util/Iterator;Lcom/jscape/util/g/H;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator<",
            "TT;>;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)TT;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/H;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static c([Ljava/lang/Object;Lcom/jscape/util/g/H;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Lcom/jscape/util/g/H<",
            "-TT;>;)TT;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/b/c;->a([Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/b/a;->c(Ljava/util/Iterator;Lcom/jscape/util/g/H;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static c(Ljava/util/Collection;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/jscape/util/H<",
            "TT;>;>(",
            "Ljava/util/Collection<",
            "TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/util/b/a;->c(Ljava/util/Iterator;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static c(Ljava/util/Iterator;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/jscape/util/H<",
            "TT;>;>(",
            "Ljava/util/Iterator<",
            "TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/util/H;

    invoke-interface {v2}, Lcom/jscape/util/H;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_0

    :cond_1
    return-object v1
.end method
