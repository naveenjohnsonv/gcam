.class public Lcom/jscape/util/b/s;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final e:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private b:Lcom/jscape/util/b/s;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/b/s<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/jscape/util/b/s<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "TT;",
            "Lcom/jscape/util/b/s<",
            "TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x8

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x2e

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "2VR80gw#\u000cJ\u0004[8qzr\u007f\u001aK8l"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x15

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/b/s;->e:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x2a

    goto :goto_2

    :cond_2
    const/16 v12, 0x2f

    goto :goto_2

    :cond_3
    const/16 v12, 0x7f

    goto :goto_2

    :cond_4
    const/16 v12, 0x73

    goto :goto_2

    :cond_5
    const/16 v12, 0x10

    goto :goto_2

    :cond_6
    const/16 v12, 0x58

    goto :goto_2

    :cond_7
    const/16 v12, 0x30

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/Object;Lcom/jscape/util/b/s;Ljava/util/List;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/jscape/util/b/s<",
            "TT;>;",
            "Ljava/util/List<",
            "Lcom/jscape/util/b/s<",
            "TT;>;>;",
            "Ljava/util/Map<",
            "TT;",
            "Lcom/jscape/util/b/s<",
            "TT;>;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/b/s;->a:Ljava/lang/Object;

    iput-object p2, p0, Lcom/jscape/util/b/s;->b:Lcom/jscape/util/b/s;

    iput-object p3, p0, Lcom/jscape/util/b/s;->c:Ljava/util/List;

    iput-object p4, p0, Lcom/jscape/util/b/s;->d:Ljava/util/Map;

    invoke-interface {p4, p1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static a(Ljava/lang/Object;)Lcom/jscape/util/b/s;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/jscape/util/b/s<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/b/s;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const/4 v3, 0x0

    invoke-direct {v0, p0, v3, v1, v2}, Lcom/jscape/util/b/s;-><init>(Ljava/lang/Object;Lcom/jscape/util/b/s;Ljava/util/List;Ljava/util/Map;)V

    return-object v0
.end method


# virtual methods
.method protected a(Ljava/lang/Object;Lcom/jscape/util/b/s;)Lcom/jscape/util/b/s;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/jscape/util/b/s<",
            "TT;>;)",
            "Lcom/jscape/util/b/s<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/b/s;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iget-object v2, p0, Lcom/jscape/util/b/s;->d:Ljava/util/Map;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/jscape/util/b/s;-><init>(Ljava/lang/Object;Lcom/jscape/util/b/s;Ljava/util/List;Ljava/util/Map;)V

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/b/s;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/jscape/util/b/s;->d:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jscape/util/b/s;->d:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/util/b/s;

    invoke-virtual {v1, p2}, Lcom/jscape/util/b/s;->d(Ljava/lang/Object;)Lcom/jscape/util/b/s;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/jscape/util/b/s;->d(Ljava/lang/Object;)Lcom/jscape/util/b/s;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/jscape/util/b/s;->d(Ljava/lang/Object;)Lcom/jscape/util/b/s;

    :cond_1
    return-void
.end method

.method public b()Lcom/jscape/util/b/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jscape/util/b/s<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/b/s;->b:Lcom/jscape/util/b/s;

    return-object v0
.end method

.method public b(Ljava/lang/Object;)Lcom/jscape/util/b/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/jscape/util/b/s<",
            "TT;>;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/jscape/util/b/s;->a(Ljava/lang/Object;Lcom/jscape/util/b/s;)Lcom/jscape/util/b/s;

    move-result-object p1

    iget-object v0, p1, Lcom/jscape/util/b/s;->c:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object p1, p0, Lcom/jscape/util/b/s;->b:Lcom/jscape/util/b/s;

    return-object p1
.end method

.method public c(Ljava/lang/Object;)Lcom/jscape/util/b/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/jscape/util/b/s<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/b/s;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/jscape/util/b/s;

    return-object p1
.end method

.method public c()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/jscape/util/b/s<",
            "TT;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/b/s;->c:Ljava/util/List;

    return-object v0
.end method

.method public d(Ljava/lang/Object;)Lcom/jscape/util/b/s;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/jscape/util/b/s<",
            "TT;>;"
        }
    .end annotation

    invoke-virtual {p0, p1, p0}, Lcom/jscape/util/b/s;->a(Ljava/lang/Object;Lcom/jscape/util/b/s;)Lcom/jscape/util/b/s;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/util/b/s;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p1
.end method

.method public e(Ljava/lang/Object;)Ljava/util/Collection;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/util/Collection<",
            "TT;>;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/b/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/jscape/util/b/s;->c(Ljava/lang/Object;)Lcom/jscape/util/b/s;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/jscape/util/b/s;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iget-object p1, p1, Lcom/jscape/util/b/s;->c:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/util/b/s;

    if-nez v0, :cond_2

    iget-object v2, v2, Lcom/jscape/util/b/s;->a:Ljava/lang/Object;

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_1

    :cond_2
    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/b/s;->e:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/b/s;->a:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/b/s;->c:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
