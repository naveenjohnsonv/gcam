.class public Lcom/jscape/util/l/f;
.super Lcom/jscape/util/g/z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/util/g/z<",
        "Ljava/lang/Class;",
        "Ljava/lang/reflect/Method;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/util/g/H;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/g/H<",
            "-",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v5, "C\u0017\ti\u0005Y\u0001Sl\n"

    move v7, v0

    const/4 v6, -0x1

    const/4 v8, 0x0

    const/16 v9, 0xa

    :goto_0
    const/16 v10, 0x48

    const/4 v11, 0x1

    add-int/2addr v6, v11

    add-int v12, v6, v7

    invoke-virtual {v5, v6, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    const/4 v15, 0x0

    :goto_2
    const/4 v2, 0x5

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_1

    add-int/lit8 v12, v8, 0x1

    aput-object v10, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v9, :cond_0

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v12

    goto :goto_0

    :cond_0
    const/16 v5, 0x1b

    const-string v6, "\u0011]\u000f0V\u0015;]\u000fv\u0012\tA\u001fV\u001fe\r\u001fb\u0012Q\u0018\u007f\t\u0008:"

    move v7, v2

    move v9, v5

    move-object v5, v6

    move v8, v12

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v2, v8, 0x1

    aput-object v10, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v9, :cond_2

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v2

    :goto_3
    const/16 v10, 0x14

    add-int/2addr v6, v11

    add-int v2, v6, v7

    invoke-virtual {v5, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/l/f;->c:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v3, v15, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v11, :cond_8

    const/4 v4, 0x2

    if-eq v3, v4, :cond_7

    const/4 v4, 0x3

    if-eq v3, v4, :cond_6

    if-eq v3, v0, :cond_5

    if-eq v3, v2, :cond_4

    const/16 v2, 0x13

    goto :goto_4

    :cond_4
    const/16 v2, 0x79

    goto :goto_4

    :cond_5
    const/16 v2, 0x69

    goto :goto_4

    :cond_6
    const/16 v2, 0xa

    goto :goto_4

    :cond_7
    const/16 v2, 0x6f

    goto :goto_4

    :cond_8
    const/16 v2, 0x2c

    goto :goto_4

    :cond_9
    const/16 v2, 0x62

    :goto_4
    xor-int/2addr v2, v10

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v12, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/util/g/H;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/g/H<",
            "-",
            "Ljava/lang/reflect/Method;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/util/g/z;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/l/f;->a:Lcom/jscape/util/g/H;

    return-void
.end method

.method public static a(Ljava/lang/Class;Lcom/jscape/util/g/H;)Ljava/lang/reflect/Method;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            "Lcom/jscape/util/g/H<",
            "-",
            "Ljava/lang/reflect/Method;",
            ">;)",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/l/a;->b()[I

    move-result-object v0

    if-nez v0, :cond_1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/jscape/util/b/a;->c([Ljava/lang/Object;Lcom/jscape/util/g/H;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :cond_1
    move-object v1, p0

    :goto_0
    check-cast v1, Ljava/lang/reflect/Method;

    if-nez v0, :cond_3

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/l/f;->a(Ljava/lang/Class;Lcom/jscape/util/g/H;)Ljava/lang/reflect/Method;

    move-result-object v1

    :cond_3
    :goto_1
    return-object v1
.end method

.method private static a(Ljava/lang/Class;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            ")",
            "Ljava/util/Collection<",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {v0}, Lcom/jscape/util/G;->d([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/util/G;->d([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method public static b(Ljava/lang/Class;Lcom/jscape/util/g/H;)[Ljava/lang/reflect/Method;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            "Lcom/jscape/util/g/H<",
            "Ljava/lang/reflect/Method;",
            ">;)[",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/l/f;->a(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object p0

    new-instance v0, Lcom/jscape/util/l/l;

    sget-object v1, Lcom/jscape/util/l/f;->c:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/util/l/l;-><init>(Ljava/lang/String;)V

    new-array v1, v2, [Lcom/jscape/util/g/H;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/jscape/util/l/l;->a([Lcom/jscape/util/g/H;)Lcom/jscape/util/g/H;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/jscape/util/b/a;->a(Ljava/util/Collection;Lcom/jscape/util/g/H;)Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result p1

    new-array p1, p1, [Ljava/lang/reflect/Method;

    invoke-interface {p0, p1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Ljava/lang/reflect/Method;

    return-object p0
.end method

.method public static c(Ljava/lang/Class;Lcom/jscape/util/g/H;)[Ljava/lang/reflect/Method;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class;",
            "Lcom/jscape/util/g/H<",
            "Ljava/lang/reflect/Method;",
            ">;)[",
            "Ljava/lang/reflect/Method;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/l/f;->a(Ljava/lang/Class;)Ljava/util/Collection;

    move-result-object p0

    new-instance v0, Lcom/jscape/util/l/l;

    sget-object v1, Lcom/jscape/util/l/f;->c:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-direct {v0, v2}, Lcom/jscape/util/l/l;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x1

    new-array v3, v2, [Lcom/jscape/util/g/H;

    new-instance v4, Lcom/jscape/util/l/l;

    const/4 v5, 0x0

    aget-object v1, v1, v5

    invoke-direct {v4, v1}, Lcom/jscape/util/l/l;-><init>(Ljava/lang/String;)V

    aput-object v4, v3, v5

    invoke-virtual {v0, v3}, Lcom/jscape/util/l/l;->b([Lcom/jscape/util/g/H;)Lcom/jscape/util/g/H;

    move-result-object v0

    new-array v1, v2, [Lcom/jscape/util/g/H;

    aput-object p1, v1, v5

    invoke-virtual {v0, v1}, Lcom/jscape/util/g/H;->a([Lcom/jscape/util/g/H;)Lcom/jscape/util/g/H;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/jscape/util/b/a;->a(Ljava/util/Collection;Lcom/jscape/util/g/H;)Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result p1

    new-array p1, p1, [Ljava/lang/reflect/Method;

    invoke-interface {p0, p1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Ljava/lang/reflect/Method;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Class;

    invoke-virtual {p0, p1}, Lcom/jscape/util/l/f;->b(Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/l/f;->a:Lcom/jscape/util/g/H;

    invoke-static {p1, v0}, Lcom/jscape/util/l/f;->a(Ljava/lang/Class;Lcom/jscape/util/g/H;)Ljava/lang/reflect/Method;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/l/f;->c:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/l/f;->a:Lcom/jscape/util/g/H;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
