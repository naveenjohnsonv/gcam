.class public Lcom/jscape/util/l/p;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/jscape/util/l/q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0x9

    new-array v1, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "O\u001co Q\\\n\u0006I\u001cu.XX\u0003D\u001dt\u0004[\u001ci(\u0004A\u001cn+\u0004O\nt)\u0004N\u001ba>"

    const/16 v6, 0x26

    move v9, v4

    const/4 v7, -0x1

    const/4 v8, 0x7

    :goto_0
    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v8

    invoke-virtual {v5, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v13, -0x1

    const/16 v14, 0x16

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v15, v11

    move v2, v4

    :goto_2
    const/4 v12, 0x2

    const/4 v3, 0x5

    if-gt v15, v2, :cond_3

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v11, v9, 0x1

    if-eqz v13, :cond_1

    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v11

    goto :goto_0

    :cond_0
    const/16 v6, 0xb

    const-string v5, "k?O\r`\u0005~;O\u001e`"

    move v8, v3

    move v9, v11

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v2

    move v9, v11

    :goto_3
    const/16 v14, 0x36

    add-int/2addr v7, v10

    add-int v2, v7, v8

    invoke-virtual {v5, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v13, v4

    goto :goto_1

    :cond_2
    new-array v0, v0, [Lcom/jscape/util/ap;

    aget-object v2, v1, v4

    new-instance v5, Lcom/jscape/util/l/q;

    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-class v7, Ljava/lang/Boolean;

    invoke-direct {v5, v6, v7}, Lcom/jscape/util/l/q;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v2, v5}, Lcom/jscape/util/ap;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;

    move-result-object v2

    aput-object v2, v0, v4

    aget-object v2, v1, v3

    new-instance v4, Lcom/jscape/util/l/q;

    sget-object v5, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    const-class v6, Ljava/lang/Byte;

    invoke-direct {v4, v5, v6}, Lcom/jscape/util/l/q;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v2, v4}, Lcom/jscape/util/ap;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;

    move-result-object v2

    aput-object v2, v0, v10

    const/4 v2, 0x6

    aget-object v4, v1, v2

    new-instance v5, Lcom/jscape/util/l/q;

    sget-object v6, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    const-class v7, Ljava/lang/Character;

    invoke-direct {v5, v6, v7}, Lcom/jscape/util/l/q;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v4, v5}, Lcom/jscape/util/ap;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;

    move-result-object v4

    aput-object v4, v0, v12

    const/16 v4, 0x8

    aget-object v5, v1, v4

    new-instance v6, Lcom/jscape/util/l/q;

    sget-object v7, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const-class v8, Ljava/lang/Short;

    invoke-direct {v6, v7, v8}, Lcom/jscape/util/l/q;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v5, v6}, Lcom/jscape/util/ap;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;

    move-result-object v5

    const/4 v6, 0x3

    aput-object v5, v0, v6

    aget-object v5, v1, v12

    new-instance v6, Lcom/jscape/util/l/q;

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v8, Ljava/lang/Integer;

    invoke-direct {v6, v7, v8}, Lcom/jscape/util/l/q;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v5, v6}, Lcom/jscape/util/ap;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;

    move-result-object v5

    const/4 v6, 0x4

    aput-object v5, v0, v6

    aget-object v5, v1, v6

    new-instance v6, Lcom/jscape/util/l/q;

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const-class v8, Ljava/lang/Long;

    invoke-direct {v6, v7, v8}, Lcom/jscape/util/l/q;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v5, v6}, Lcom/jscape/util/ap;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;

    move-result-object v5

    aput-object v5, v0, v3

    const/4 v3, 0x7

    aget-object v5, v1, v3

    new-instance v3, Lcom/jscape/util/l/q;

    sget-object v6, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    const-class v7, Ljava/lang/Float;

    invoke-direct {v3, v6, v7}, Lcom/jscape/util/l/q;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v5, v3}, Lcom/jscape/util/ap;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;

    move-result-object v3

    aput-object v3, v0, v2

    aget-object v2, v1, v10

    new-instance v3, Lcom/jscape/util/l/q;

    sget-object v5, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    const-class v6, Ljava/lang/Double;

    invoke-direct {v3, v5, v6}, Lcom/jscape/util/l/q;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v2, v3}, Lcom/jscape/util/ap;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;

    move-result-object v2

    const/16 v16, 0x7

    aput-object v2, v0, v16

    const/4 v2, 0x3

    aget-object v1, v1, v2

    new-instance v2, Lcom/jscape/util/l/q;

    sget-object v3, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    const-class v5, Ljava/lang/Void;

    invoke-direct {v2, v3, v5}, Lcom/jscape/util/l/q;-><init>(Ljava/lang/Class;Ljava/lang/Class;)V

    invoke-static {v1, v2}, Lcom/jscape/util/ap;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/jscape/util/G;->a([Lcom/jscape/util/ap;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/l/p;->a:Ljava/util/Map;

    return-void

    :cond_3
    const/16 v16, 0x7

    aget-char v17, v11, v2

    rem-int/lit8 v0, v2, 0x7

    if-eqz v0, :cond_9

    if-eq v0, v10, :cond_8

    if-eq v0, v12, :cond_7

    const/4 v12, 0x3

    if-eq v0, v12, :cond_6

    const/4 v12, 0x4

    if-eq v0, v12, :cond_5

    if-eq v0, v3, :cond_4

    const/16 v0, 0x72

    goto :goto_4

    :cond_4
    const/16 v0, 0x2b

    goto :goto_4

    :cond_5
    const/16 v0, 0x22

    goto :goto_4

    :cond_6
    const/16 v0, 0x5a

    goto :goto_4

    :cond_7
    const/16 v0, 0x16

    goto :goto_4

    :cond_8
    const/16 v0, 0x65

    goto :goto_4

    :cond_9
    const/16 v0, 0x3b

    :goto_4
    xor-int/2addr v0, v14

    xor-int v0, v17, v0

    int-to-char v0, v0

    aput-char v0, v11, v2

    add-int/lit8 v2, v2, 0x1

    const/16 v0, 0x9

    goto/16 :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Class;)Ljava/lang/Class;
    .locals 1

    invoke-static {}, Lcom/jscape/util/l/a;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/util/l/p;->b(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static a(Ljava/lang/Object;)Z
    .locals 1

    invoke-static {}, Lcom/jscape/util/l/a;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    instance-of v0, p0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/lang/Class;

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    :goto_0
    invoke-virtual {p0}, Ljava/lang/Class;->isArray()Z

    move-result p0

    return p0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Lcom/jscape/util/l/p;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static a([Ljava/lang/Class;[Ljava/lang/Class;Z)Z
    .locals 6

    invoke-static {}, Lcom/jscape/util/l/a;->b()[I

    move-result-object v0

    array-length v1, p0

    const/4 v2, 0x0

    if-nez v0, :cond_1

    array-length v3, p1

    if-eq v1, v3, :cond_0

    return v2

    :cond_0
    move v1, v2

    :cond_1
    array-length v3, p0

    if-ge v1, v3, :cond_8

    if-nez v0, :cond_2

    aget-object v3, p0, v1

    invoke-virtual {v3}, Ljava/lang/Class;->isPrimitive()Z

    move-result v3

    if-nez v0, :cond_9

    if-eqz v3, :cond_2

    aget-object v3, p0, v1

    invoke-static {v3}, Lcom/jscape/util/l/p;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v3

    goto :goto_0

    :cond_2
    aget-object v3, p0, v1

    :goto_0
    aget-object v4, p1, v1

    if-eqz p2, :cond_4

    if-nez v0, :cond_3

    if-eq v3, v4, :cond_4

    return v2

    :cond_3
    move-object v5, v3

    goto :goto_1

    :cond_4
    move-object v5, v4

    :goto_1
    if-nez v0, :cond_5

    if-eqz v5, :cond_6

    goto :goto_2

    :cond_5
    move-object v3, v5

    :goto_2
    invoke-virtual {v3, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v0, :cond_7

    if-nez v3, :cond_6

    goto :goto_3

    :cond_6
    add-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_1

    goto :goto_4

    :cond_7
    move v2, v3

    :goto_3
    return v2

    :cond_8
    :goto_4
    const/4 v3, 0x1

    :cond_9
    return v3
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2

    invoke-static {}, Lcom/jscape/util/l/a;->b()[I

    move-result-object v0

    sget-object v1, Lcom/jscape/util/l/p;->a:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/jscape/util/l/q;

    if-nez v0, :cond_1

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    iget-object p0, p0, Lcom/jscape/util/l/q;->b:Ljava/lang/Class;

    :goto_1
    return-object p0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2

    invoke-static {}, Lcom/jscape/util/l/a;->b()[I

    move-result-object v0

    sget-object v1, Lcom/jscape/util/l/p;->a:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/jscape/util/l/q;

    if-nez v0, :cond_1

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    iget-object p0, p0, Lcom/jscape/util/l/q;->a:Ljava/lang/Class;

    :goto_1
    return-object p0
.end method
