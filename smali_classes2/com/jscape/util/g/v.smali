.class public Lcom/jscape/util/g/v;
.super Lcom/jscape/util/g/q;


# instance fields
.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/jscape/util/g/q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/jscape/util/g/q;Lcom/jscape/util/g/q;)V
    .locals 1

    invoke-direct {p0}, Lcom/jscape/util/g/q;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/jscape/util/g/v;->c:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/jscape/util/g/v;->b(Lcom/jscape/util/g/q;)V

    invoke-direct {p0, p2}, Lcom/jscape/util/g/v;->b(Lcom/jscape/util/g/q;)V

    return-void
.end method

.method private b(Lcom/jscape/util/g/q;)V
    .locals 3

    invoke-static {}, Lcom/jscape/util/g/z;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    instance-of v1, p1, Lcom/jscape/util/g/v;

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jscape/util/g/v;->c:Ljava/util/List;

    move-object v2, p1

    check-cast v2, Lcom/jscape/util/g/v;

    iget-object v2, v2, Lcom/jscape/util/g/v;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/util/g/v;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 3

    invoke-static {}, Lcom/jscape/util/g/z;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/g/v;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/util/g/q;

    invoke-virtual {v2}, Lcom/jscape/util/g/q;->a()Z

    move-result v2

    if-nez v0, :cond_4

    if-nez v0, :cond_2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_0

    goto :goto_1

    :cond_2
    :goto_0
    return v2

    :cond_3
    :goto_1
    const/4 v2, 0x0

    :cond_4
    return v2
.end method
