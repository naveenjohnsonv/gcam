.class public Lcom/jscape/util/g/f;
.super Lcom/jscape/util/g/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/jscape/util/g/e<",
        "TT;TU;>;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/jscape/util/g/e<",
            "-TT;-TU;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/jscape/util/g/e;Lcom/jscape/util/g/e;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/g/e<",
            "-TT;-TU;>;",
            "Lcom/jscape/util/g/e<",
            "-TT;-TU;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/util/g/e;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/jscape/util/g/f;->a:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/jscape/util/g/f;->b(Lcom/jscape/util/g/e;)V

    invoke-direct {p0, p2}, Lcom/jscape/util/g/f;->b(Lcom/jscape/util/g/e;)V

    return-void
.end method

.method private b(Lcom/jscape/util/g/e;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/g/e<",
            "-TT;-TU;>;)V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/g/z;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    instance-of v1, p1, Lcom/jscape/util/g/f;

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jscape/util/g/f;->a:Ljava/util/List;

    move-object v2, p1

    check-cast v2, Lcom/jscape/util/g/f;

    iget-object v2, v2, Lcom/jscape/util/g/f;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/util/g/f;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TU;)Z"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/g/z;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/g/f;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jscape/util/g/e;

    invoke-virtual {v2, p1, p2}, Lcom/jscape/util/g/e;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v0, :cond_4

    if-nez v0, :cond_2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_0

    goto :goto_1

    :cond_2
    :goto_0
    return v2

    :cond_3
    :goto_1
    const/4 v2, 0x1

    :cond_4
    return v2
.end method
