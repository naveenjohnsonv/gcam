.class public abstract Lcom/jscape/util/g/z;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static b:[Lcom/jscape/util/aq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/util/g/z;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/jscape/util/aq;

    invoke-static {v0}, Lcom/jscape/util/g/z;->b([Lcom/jscape/util/aq;)V

    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;)Lcom/jscape/util/g/E;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(TU;)",
            "Lcom/jscape/util/g/E<",
            "TT;TU;>;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/g/E;

    invoke-direct {v0, p0}, Lcom/jscape/util/g/E;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static b([Lcom/jscape/util/aq;)V
    .locals 0

    sput-object p0, Lcom/jscape/util/g/z;->b:[Lcom/jscape/util/aq;

    return-void
.end method

.method public static b()[Lcom/jscape/util/aq;
    .locals 1

    sget-object v0, Lcom/jscape/util/g/z;->b:[Lcom/jscape/util/aq;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/jscape/util/g/z;)Lcom/jscape/util/g/z;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/jscape/util/g/z<",
            "-TR;+TU;>;)",
            "Lcom/jscape/util/g/z<",
            "TV;TU;>;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/g/D;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/util/g/D;-><init>(Lcom/jscape/util/g/z;Lcom/jscape/util/g/z;Lcom/jscape/util/g/G;)V

    return-object v0
.end method

.method public abstract b(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)TR;"
        }
    .end annotation
.end method
