.class public Lcom/jscape/util/g/B;
.super Lcom/jscape/util/g/z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/util/g/z<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/util/g/z;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/jscape/util/g/B;->b(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/util/g/B;->a:Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/g/B;->c:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/util/Map;)Lcom/jscape/util/g/z;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/jscape/util/g/z<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/g/A;->a()Lcom/jscape/util/g/z;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/g/z;->b()[Lcom/jscape/util/aq;

    move-result-object v1

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    new-instance v3, Lcom/jscape/util/g/B;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v3, v4, v2}, Lcom/jscape/util/g/B;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/jscape/util/g/z;->a(Lcom/jscape/util/g/z;)Lcom/jscape/util/g/z;

    move-result-object v0

    if-nez v1, :cond_1

    if-eqz v1, :cond_0

    :cond_1
    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/g/B;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/util/g/B;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/jscape/util/g/B;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
