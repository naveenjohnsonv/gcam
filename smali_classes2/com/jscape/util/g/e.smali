.class public abstract Lcom/jscape/util/g/e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<F:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/jscape/util/g/e;)Lcom/jscape/util/g/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/jscape/util/g/e<",
            "-TF;-TS;>;)",
            "Lcom/jscape/util/g/e<",
            "TF;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/g/h;

    invoke-direct {v0, p0}, Lcom/jscape/util/g/h;-><init>(Lcom/jscape/util/g/e;)V

    return-object v0
.end method


# virtual methods
.method public varargs a([Lcom/jscape/util/g/e;)Lcom/jscape/util/g/e;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/jscape/util/g/e<",
            "-TF;-TS;>;)",
            "Lcom/jscape/util/g/e<",
            "TF;TS;>;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/g/z;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    array-length v1, p1

    const/4 v2, 0x0

    move-object v3, p0

    :cond_0
    if-ge v2, v1, :cond_1

    aget-object v4, p1, v2

    new-instance v5, Lcom/jscape/util/g/f;

    invoke-direct {v5, v3, v4}, Lcom/jscape/util/g/f;-><init>(Lcom/jscape/util/g/e;Lcom/jscape/util/g/e;)V

    if-nez v0, :cond_2

    add-int/lit8 v2, v2, 0x1

    move-object v3, v5

    if-eqz v0, :cond_0

    :cond_1
    move-object v5, v3

    :cond_2
    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_3

    const/4 p1, 0x4

    new-array p1, p1, [Lcom/jscape/util/aq;

    invoke-static {p1}, Lcom/jscape/util/g/z;->b([Lcom/jscape/util/aq;)V

    :cond_3
    return-object v5
.end method

.method public abstract a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;TS;)Z"
        }
    .end annotation
.end method

.method public varargs b([Lcom/jscape/util/g/e;)Lcom/jscape/util/g/e;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/jscape/util/g/e<",
            "-TF;-TS;>;)",
            "Lcom/jscape/util/g/e<",
            "TF;TS;>;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/g/z;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    array-length v1, p1

    const/4 v2, 0x0

    move-object v3, p0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v4, p1, v2

    new-instance v5, Lcom/jscape/util/g/i;

    invoke-direct {v5, v3, v4}, Lcom/jscape/util/g/i;-><init>(Lcom/jscape/util/g/e;Lcom/jscape/util/g/e;)V

    if-nez v0, :cond_2

    add-int/lit8 v2, v2, 0x1

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    move-object v3, v5

    goto :goto_1

    :cond_0
    move-object v3, v5

    goto :goto_0

    :cond_1
    :goto_1
    move-object v5, v3

    :cond_2
    return-object v5
.end method
