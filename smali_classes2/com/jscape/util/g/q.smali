.class public abstract Lcom/jscape/util/g/q;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Lcom/jscape/util/g/q;

.field public static final b:Lcom/jscape/util/g/q;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jscape/util/g/r;

    invoke-direct {v0}, Lcom/jscape/util/g/r;-><init>()V

    sput-object v0, Lcom/jscape/util/g/q;->a:Lcom/jscape/util/g/q;

    new-instance v0, Lcom/jscape/util/g/s;

    invoke-direct {v0}, Lcom/jscape/util/g/s;-><init>()V

    sput-object v0, Lcom/jscape/util/g/q;->b:Lcom/jscape/util/g/q;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/jscape/util/g/q;)Lcom/jscape/util/g/q;
    .locals 1

    new-instance v0, Lcom/jscape/util/g/u;

    invoke-direct {v0, p0}, Lcom/jscape/util/g/u;-><init>(Lcom/jscape/util/g/q;)V

    return-object v0
.end method


# virtual methods
.method public varargs a([Lcom/jscape/util/g/q;)Lcom/jscape/util/g/q;
    .locals 6

    invoke-static {}, Lcom/jscape/util/g/z;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    array-length v1, p1

    const/4 v2, 0x0

    move-object v3, p0

    :cond_0
    if-ge v2, v1, :cond_1

    aget-object v4, p1, v2

    new-instance v5, Lcom/jscape/util/g/t;

    invoke-direct {v5, v3, v4}, Lcom/jscape/util/g/t;-><init>(Lcom/jscape/util/g/q;Lcom/jscape/util/g/q;)V

    if-nez v0, :cond_2

    add-int/lit8 v2, v2, 0x1

    move-object v3, v5

    if-eqz v0, :cond_0

    :cond_1
    move-object v5, v3

    :cond_2
    return-object v5
.end method

.method public abstract a()Z
.end method

.method public varargs b([Lcom/jscape/util/g/q;)Lcom/jscape/util/g/q;
    .locals 6

    invoke-static {}, Lcom/jscape/util/g/z;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    array-length v1, p1

    const/4 v2, 0x0

    move-object v3, p0

    :cond_0
    if-ge v2, v1, :cond_1

    aget-object v4, p1, v2

    new-instance v5, Lcom/jscape/util/g/v;

    invoke-direct {v5, v3, v4}, Lcom/jscape/util/g/v;-><init>(Lcom/jscape/util/g/q;Lcom/jscape/util/g/q;)V

    if-nez v0, :cond_2

    add-int/lit8 v2, v2, 0x1

    move-object v3, v5

    if-eqz v0, :cond_0

    :cond_1
    move-object v5, v3

    :cond_2
    return-object v5
.end method
