.class public abstract Lcom/jscape/util/g/H;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/jscape/util/g/H;)Lcom/jscape/util/g/H;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/jscape/util/g/H<",
            "-TV;>;)",
            "Lcom/jscape/util/g/H<",
            "TV;>;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/g/L;

    invoke-direct {v0, p0}, Lcom/jscape/util/g/L;-><init>(Lcom/jscape/util/g/H;)V

    return-object v0
.end method


# virtual methods
.method public varargs a([Lcom/jscape/util/g/H;)Lcom/jscape/util/g/H;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/jscape/util/g/H<",
            "-TV;>;)",
            "Lcom/jscape/util/g/H<",
            "TV;>;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/g/z;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    array-length v1, p1

    const/4 v2, 0x0

    move-object v3, p0

    :cond_0
    if-ge v2, v1, :cond_1

    aget-object v4, p1, v2

    new-instance v5, Lcom/jscape/util/g/J;

    invoke-direct {v5, v3, v4}, Lcom/jscape/util/g/J;-><init>(Lcom/jscape/util/g/H;Lcom/jscape/util/g/H;)V

    if-nez v0, :cond_2

    add-int/lit8 v2, v2, 0x1

    move-object v3, v5

    if-eqz v0, :cond_0

    :cond_1
    move-object v5, v3

    :cond_2
    return-object v5
.end method

.method public abstract a(Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation
.end method

.method public varargs b([Lcom/jscape/util/g/H;)Lcom/jscape/util/g/H;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/jscape/util/g/H<",
            "-TV;>;)",
            "Lcom/jscape/util/g/H<",
            "TV;>;"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/g/z;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    array-length v1, p1

    const/4 v2, 0x0

    move-object v3, p0

    :cond_0
    if-ge v2, v1, :cond_1

    aget-object v4, p1, v2

    new-instance v5, Lcom/jscape/util/g/M;

    invoke-direct {v5, v3, v4}, Lcom/jscape/util/g/M;-><init>(Lcom/jscape/util/g/H;Lcom/jscape/util/g/H;)V

    if-nez v0, :cond_2

    add-int/lit8 v2, v2, 0x1

    move-object v3, v5

    if-eqz v0, :cond_0

    :cond_1
    move-object v5, v3

    :cond_2
    return-object v5
.end method
