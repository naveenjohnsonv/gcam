.class public Lcom/jscape/util/f/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/f/l;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/jscape/util/f/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "R\'1D`$we)(L[8E\u007f;5]f%Jbh\'Z{+Pt;a"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/f/e;->b:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x4f

    goto :goto_1

    :cond_1
    const/16 v4, 0x21

    goto :goto_1

    :cond_2
    const/16 v4, 0x64

    goto :goto_1

    :cond_3
    const/16 v4, 0x42

    goto :goto_1

    :cond_4
    const/16 v4, 0x37

    goto :goto_1

    :cond_5
    const/16 v4, 0x23

    goto :goto_1

    :cond_6
    const/16 v4, 0x7a

    :goto_1
    const/16 v5, 0x6b

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/jscape/util/f/f;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jscape/util/f/e;->a:Ljava/util/Map;

    invoke-direct {p0, p1}, Lcom/jscape/util/f/e;->a(Ljava/util/Collection;)V

    return-void
.end method

.method public varargs constructor <init>([Lcom/jscape/util/f/f;)V
    .locals 0

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/util/f/e;-><init>(Ljava/util/Collection;)V

    return-void
.end method

.method private static a(Lcom/jscape/util/f/b;)Lcom/jscape/util/f/b;
    .locals 0

    return-object p0
.end method

.method private a(Lcom/jscape/util/f/j;Ljava/lang/Class;)Ljava/lang/Integer;
    .locals 0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result p1

    mul-int/lit8 p1, p1, 0x1f

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result p2

    add-int/2addr p1, p2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method private a(Lcom/jscape/util/f/j;Lcom/jscape/util/f/g;Lcom/jscape/util/f/j;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/b;
        }
    .end annotation

    if-eqz p3, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance p3, Lcom/jscape/util/f/b;

    invoke-direct {p3, p1, p2}, Lcom/jscape/util/f/b;-><init>(Lcom/jscape/util/f/j;Lcom/jscape/util/f/g;)V

    throw p3
    :try_end_0
    .catch Lcom/jscape/util/f/b; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/f/e;->a(Lcom/jscape/util/f/b;)Lcom/jscape/util/f/b;

    move-result-object p1

    throw p1
.end method

.method private a(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/jscape/util/f/f;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/f/g;->b()[I

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/util/f/f;

    iget-object v2, v1, Lcom/jscape/util/f/f;->a:Lcom/jscape/util/f/j;

    iget-object v3, v1, Lcom/jscape/util/f/f;->b:Ljava/lang/Class;

    invoke-direct {p0, v2, v3}, Lcom/jscape/util/f/e;->a(Lcom/jscape/util/f/j;Ljava/lang/Class;)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/jscape/util/f/e;->a:Ljava/util/Map;

    iget-object v1, v1, Lcom/jscape/util/f/f;->c:Lcom/jscape/util/f/j;

    invoke-interface {v3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method private b(Lcom/jscape/util/f/j;Lcom/jscape/util/f/g;)Ljava/lang/Integer;
    .locals 0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/jscape/util/f/e;->a(Lcom/jscape/util/f/j;Ljava/lang/Class;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public a(Lcom/jscape/util/f/j;Lcom/jscape/util/f/g;)Lcom/jscape/util/f/j;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/b;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/jscape/util/f/e;->b(Lcom/jscape/util/f/j;Lcom/jscape/util/f/g;)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/f/e;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jscape/util/f/j;

    invoke-direct {p0, p1, p2, v0}, Lcom/jscape/util/f/e;->a(Lcom/jscape/util/f/j;Lcom/jscape/util/f/g;Lcom/jscape/util/f/j;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/f/e;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/f/e;->a:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
