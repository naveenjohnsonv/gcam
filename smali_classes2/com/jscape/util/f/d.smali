.class public abstract Lcom/jscape/util/f/d;
.super Ljava/lang/Object;


# instance fields
.field protected final a:Lcom/jscape/util/f/l;

.field protected final b:Lcom/jscape/util/f/i;

.field protected final c:Lcom/jscape/util/f/j;

.field protected d:Lcom/jscape/util/f/j;


# direct methods
.method protected constructor <init>(Lcom/jscape/util/f/l;Lcom/jscape/util/f/i;Lcom/jscape/util/f/j;Lcom/jscape/util/f/j;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/f/d;->a:Lcom/jscape/util/f/l;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/f/d;->b:Lcom/jscape/util/f/i;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/util/f/g;->b()[I

    move-result-object p1

    iput-object p4, p0, Lcom/jscape/util/f/d;->c:Lcom/jscape/util/f/j;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/util/f/d;->d:Lcom/jscape/util/f/j;

    if-eqz p1, :cond_0

    const/4 p1, 0x5

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/f/a;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/f/g;->b()[I

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/jscape/util/f/d;->d:Lcom/jscape/util/f/j;

    iget-object v2, p0, Lcom/jscape/util/f/d;->c:Lcom/jscape/util/f/j;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/jscape/util/f/d;->b:Lcom/jscape/util/f/i;

    invoke-interface {v1}, Lcom/jscape/util/f/i;->a()Lcom/jscape/util/f/g;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/util/f/d;->a:Lcom/jscape/util/f/l;

    iget-object v3, p0, Lcom/jscape/util/f/d;->d:Lcom/jscape/util/f/j;

    invoke-interface {v2, v3, v1}, Lcom/jscape/util/f/l;->a(Lcom/jscape/util/f/j;Lcom/jscape/util/f/g;)Lcom/jscape/util/f/j;

    move-result-object v2

    iput-object v2, p0, Lcom/jscape/util/f/d;->d:Lcom/jscape/util/f/j;

    iget-object v3, p0, Lcom/jscape/util/f/d;->b:Lcom/jscape/util/f/i;

    invoke-virtual {v2, v1, v3}, Lcom/jscape/util/f/j;->a(Lcom/jscape/util/f/g;Lcom/jscape/util/f/i;)V

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method
