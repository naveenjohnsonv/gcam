.class public final Lcom/jscape/util/aq;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static b:[I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x1

    new-array v3, v2, [I

    invoke-static {v3}, Lcom/jscape/util/aq;->b([I)V

    const-string v5, "@_\u000c\r\u0001*[cGHB\u0006>ZgJHB\u0012xEgP\u000fY\u0008v\u001eLK\u0004A@*Ld[\u001aH\u000e;L\"W\u001b\r\u000e7]\"_\u0004A\u000f/Lf\u0010\u0011@_\u000c\r\u0001*[cGHB\u0006>ZgJF"

    const/16 v6, 0x4c

    const/16 v7, 0x1b

    const/4 v8, -0x1

    const/4 v9, 0x0

    :goto_0
    const/16 v10, 0x5c

    add-int/2addr v8, v2

    add-int v11, v8, v7

    invoke-virtual {v5, v8, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    add-int/lit8 v11, v9, 0x1

    if-eqz v12, :cond_1

    aput-object v10, v1, v9

    add-int/2addr v8, v7

    if-ge v8, v6, :cond_0

    invoke-virtual {v5, v8}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v9, v11

    goto :goto_0

    :cond_0
    const/16 v6, 0x24

    const/16 v5, 0x11

    const-string v7, ">!rs\u007fT%\u001d96?{H0\u0008(8\u0012>!rsmR%\u0015.qsrC9\u001b4~}"

    move v9, v11

    const/4 v8, -0x1

    move-object/from16 v16, v7

    move v7, v5

    move-object/from16 v5, v16

    goto :goto_3

    :cond_1
    aput-object v10, v1, v9

    add-int/2addr v8, v7

    if-ge v8, v6, :cond_2

    invoke-virtual {v5, v8}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v9, v11

    :goto_3
    const/16 v10, 0x22

    add-int/2addr v8, v2

    add-int v11, v8, v7

    invoke-virtual {v5, v8, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/aq;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v3, v14, 0x7

    const/4 v4, 0x4

    if-eqz v3, :cond_8

    if-eq v3, v2, :cond_7

    const/4 v2, 0x2

    if-eq v3, v2, :cond_6

    const/4 v2, 0x3

    if-eq v3, v2, :cond_5

    if-eq v3, v4, :cond_4

    if-eq v3, v0, :cond_9

    const/16 v4, 0x75

    goto :goto_4

    :cond_4
    const/16 v4, 0x3c

    goto :goto_4

    :cond_5
    const/16 v4, 0x71

    goto :goto_4

    :cond_6
    const/16 v4, 0x34

    goto :goto_4

    :cond_7
    const/16 v4, 0x62

    goto :goto_4

    :cond_8
    const/16 v4, 0x5e

    :cond_9
    :goto_4
    xor-int v2, v10, v4

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v2, 0x1

    goto :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;
    .locals 0

    return-object p0
.end method

.method public static a(JJJLjava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p6}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    invoke-static {p0, p1, p4, p5, p6}, Lcom/jscape/util/aq;->d(JJLjava/lang/String;)V

    return-void
.end method

.method public static a(JJLjava/lang/String;)V
    .locals 0

    cmp-long p0, p0, p2

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0, p4}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/Object;)V
    .locals 2

    sget-object v0, Lcom/jscape/util/aq;->a:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {p0, v0}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p0

    invoke-static {p0, p2}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0

    if-eqz p0, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/aq;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 4

    invoke-static {p0}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p0

    int-to-long v0, p0

    sget-object p0, Lcom/jscape/util/aq;->a:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object p0, p0, v2

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3, p0}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    return-void
.end method

.method public static a(ZLjava/lang/String;)V
    .locals 0

    if-eqz p0, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/aq;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
.end method

.method public static a([BII)V
    .locals 8

    int-to-long v0, p1

    array-length v2, p0

    int-to-long v4, v2

    sget-object v7, Lcom/jscape/util/aq;->a:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v6, v7, v2

    const-wide/16 v2, 0x0

    invoke-static/range {v0 .. v6}, Lcom/jscape/util/aq;->a(JJJLjava/lang/String;)V

    int-to-long v0, p2

    const/4 v2, 0x3

    aget-object v2, v7, v2

    const-wide/16 v3, 0x0

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    add-int/2addr p1, p2

    int-to-long p1, p1

    array-length p0, p0

    int-to-long v0, p0

    const/4 p0, 0x0

    aget-object p0, v7, p0

    invoke-static {p1, p2, v0, v1, p0}, Lcom/jscape/util/aq;->d(JJLjava/lang/String;)V

    return-void
.end method

.method public static b(JJJLjava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p6}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    invoke-static {p0, p1, p4, p5, p6}, Lcom/jscape/util/aq;->c(JJLjava/lang/String;)V

    return-void
.end method

.method public static b(JJLjava/lang/String;)V
    .locals 0

    cmp-long p0, p0, p2

    if-ltz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0, p4}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public static b(ZLjava/lang/String;)V
    .locals 0

    if-nez p0, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/aq;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
.end method

.method public static b([I)V
    .locals 0

    sput-object p0, Lcom/jscape/util/aq;->b:[I

    return-void
.end method

.method public static b()[I
    .locals 1

    sget-object v0, Lcom/jscape/util/aq;->b:[I

    return-object v0
.end method

.method public static c(JJLjava/lang/String;)V
    .locals 0

    cmp-long p0, p0, p2

    if-gez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0, p4}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public static c(ZLjava/lang/String;)V
    .locals 0

    if-eqz p0, :cond_0

    return-void

    :cond_0
    :try_start_0
    new-instance p0, Ljava/lang/IllegalStateException;

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/aq;->a(Ljava/lang/IllegalArgumentException;)Ljava/lang/IllegalArgumentException;

    move-result-object p0

    throw p0
.end method

.method public static d(JJLjava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    cmp-long p0, p0, p2

    if-nez v0, :cond_1

    if-gtz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :cond_1
    :goto_0
    invoke-static {p0, p4}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    return-void
.end method

.method public static e(JJLjava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    cmp-long p0, p0, p2

    if-nez v0, :cond_1

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :cond_1
    :goto_0
    invoke-static {p0, p4}, Lcom/jscape/util/aq;->a(ZLjava/lang/String;)V

    return-void
.end method
