.class public abstract Lcom/jscape/util/n/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/n/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/jscape/util/n/d<",
        "TT;>;"
    }
.end annotation


# static fields
.field private static final C:[Ljava/lang/String;


# instance fields
.field protected a:Lcom/jscape/util/n/a;

.field protected b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "8{G`\u001f\\\u0014KwF6\u0017S\u0003\u000e\u007fQoVL\u0005\nlAs\u0012\u0011\u00178{G`\u001f\\\u0014KwF6\u0018P\u0005KmAw\u0004K\u0014\u000f0"

    const/16 v5, 0x33

    const/16 v6, 0x1b

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x66

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x2c

    const/16 v4, 0x14

    const-string v6, "d\'\u001b<C\u0000H\u0017+\u001ajN\n^G-\u001a/NM\u0017v \u001a>X\u0002NC\u0011\u000c8\\\nNRb\u00129^\u0002YR\u007f"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v16, v6

    move v6, v4

    move-object/from16 v4, v16

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x3a

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/n/c;->C:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v2, 0x17

    goto :goto_4

    :cond_4
    const/16 v2, 0x59

    goto :goto_4

    :cond_5
    const/16 v2, 0x10

    goto :goto_4

    :cond_6
    const/16 v2, 0x70

    goto :goto_4

    :cond_7
    const/16 v2, 0x53

    goto :goto_4

    :cond_8
    const/16 v2, 0x78

    goto :goto_4

    :cond_9
    const/16 v2, 0xd

    :goto_4
    xor-int/2addr v2, v9

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/jscape/util/n/a;->b:Lcom/jscape/util/n/a;

    iput-object v0, p0, Lcom/jscape/util/n/c;->a:Lcom/jscape/util/n/a;

    return-void
.end method

.method private static b(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/n/b;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/util/n/c;->m()V

    invoke-virtual {p0}, Lcom/jscape/util/n/c;->n()V

    invoke-static {}, Lcom/jscape/util/n/b;->b()[I

    move-result-object v0

    iput-object p1, p0, Lcom/jscape/util/n/c;->b:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/util/n/c;->actualStart()V

    sget-object p1, Lcom/jscape/util/n/a;->a:Lcom/jscape/util/n/a;

    iput-object p1, p0, Lcom/jscape/util/n/c;->a:Lcom/jscape/util/n/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-nez v0, :cond_0

    const/4 p1, 0x1

    :try_start_2
    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/util/n/c;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :catchall_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/util/n/b;->a(Ljava/lang/Throwable;)Lcom/jscape/util/n/b;

    move-result-object p1

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected actualStart()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-void
.end method

.method protected actualStop()V
    .locals 0

    return-void
.end method

.method public declared-synchronized h()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/n/c;->a:Lcom/jscape/util/n/a;

    sget-object v1, Lcom/jscape/util/n/a;->a:Lcom/jscape/util/n/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized i()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/util/n/b;->b()[I

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jscape/util/n/c;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/jscape/util/n/a;->b:Lcom/jscape/util/n/a;

    iput-object v0, p0, Lcom/jscape/util/n/c;->a:Lcom/jscape/util/n/a;

    :cond_0
    invoke-virtual {p0}, Lcom/jscape/util/n/c;->actualStop()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized j()V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/util/n/b;->b()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/n/c;->a:Lcom/jscape/util/n/a;

    sget-object v2, Lcom/jscape/util/n/a;->c:Lcom/jscape/util/n/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    if-ne v1, v2, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    if-eqz v0, :cond_3

    :try_start_1
    iget-object v1, p0, Lcom/jscape/util/n/c;->a:Lcom/jscape/util/n/a;

    sget-object v2, Lcom/jscape/util/n/a;->a:Lcom/jscape/util/n/a;

    :cond_1
    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/jscape/util/n/c;->i()V

    :cond_2
    sget-object v0, Lcom/jscape/util/n/a;->c:Lcom/jscape/util/n/a;

    iput-object v0, p0, Lcom/jscape/util/n/c;->a:Lcom/jscape/util/n/a;

    :cond_3
    invoke-virtual {p0}, Lcom/jscape/util/n/c;->k()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected k()V
    .locals 0

    return-void
.end method

.method protected l()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/n/b;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/util/n/c;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/util/n/b;

    sget-object v1, Lcom/jscape/util/n/c;->C:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/util/n/b;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/jscape/util/n/b; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/n/c;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method protected m()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/n/b;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/util/n/c;->h()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/util/n/b;

    sget-object v1, Lcom/jscape/util/n/c;->C:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/util/n/b;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/jscape/util/n/b; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/n/c;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method protected n()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/n/b;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/n/c;->a:Lcom/jscape/util/n/a;

    sget-object v1, Lcom/jscape/util/n/a;->c:Lcom/jscape/util/n/a;

    if-eq v0, v1, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/jscape/util/n/b;

    sget-object v1, Lcom/jscape/util/n/c;->C:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/util/n/b;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/jscape/util/n/b; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/n/c;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {}, Lcom/jscape/util/n/b;->b()[I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/n/c;->C:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/n/c;->a:Lcom/jscape/util/n/a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x5

    new-array v1, v1, [I

    invoke-static {v1}, Lcom/jscape/util/n/b;->b([I)V

    :cond_0
    return-object v0
.end method
