.class public Lcom/jscape/util/am;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/jscape/util/an;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jscape/util/ao;

    invoke-direct {v0}, Lcom/jscape/util/ao;-><init>()V

    sput-object v0, Lcom/jscape/util/am;->a:Lcom/jscape/util/an;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/jscape/util/u;)J
    .locals 2

    sget-object v0, Lcom/jscape/util/am;->a:Lcom/jscape/util/an;

    invoke-interface {v0, p0}, Lcom/jscape/util/an;->a(Lcom/jscape/util/u;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a()Lcom/jscape/util/an;
    .locals 1

    sget-object v0, Lcom/jscape/util/am;->a:Lcom/jscape/util/an;

    return-object v0
.end method

.method public static a(Lcom/jscape/util/an;)V
    .locals 0

    sput-object p0, Lcom/jscape/util/am;->a:Lcom/jscape/util/an;

    return-void
.end method

.method public static a(JLcom/jscape/util/u;)Z
    .locals 3

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    invoke-static {p2}, Lcom/jscape/util/am;->a(Lcom/jscape/util/u;)J

    move-result-wide v1

    cmp-long p0, v1, p0

    if-eqz v0, :cond_1

    if-gtz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :cond_1
    :goto_0
    return p0
.end method

.method public static b(JLcom/jscape/util/u;)Z
    .locals 3

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    invoke-static {p2}, Lcom/jscape/util/am;->a(Lcom/jscape/util/u;)J

    move-result-wide v1

    cmp-long p0, p0, v1

    if-eqz v0, :cond_1

    if-gtz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :cond_1
    :goto_0
    return p0
.end method

.method public static c(JLcom/jscape/util/u;)J
    .locals 2

    invoke-static {p2}, Lcom/jscape/util/am;->a(Lcom/jscape/util/u;)J

    move-result-wide v0

    add-long/2addr v0, p0

    return-wide v0
.end method

.method public static d(JLcom/jscape/util/u;)J
    .locals 2

    invoke-static {p2}, Lcom/jscape/util/am;->a(Lcom/jscape/util/u;)J

    move-result-wide v0

    sub-long/2addr v0, p0

    return-wide v0
.end method

.method public static e(JLcom/jscape/util/u;)J
    .locals 2

    invoke-static {p2}, Lcom/jscape/util/am;->a(Lcom/jscape/util/u;)J

    move-result-wide v0

    sub-long/2addr v0, p0

    return-wide v0
.end method

.method public static f(JLcom/jscape/util/u;)V
    .locals 3

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    cmp-long v0, p0, v1

    if-lez v0, :cond_1

    iget-wide v1, p2, Lcom/jscape/util/u;->e:J

    :cond_0
    mul-long/2addr p0, v1

    invoke-static {p0, p1}, Ljava/util/concurrent/locks/LockSupport;->parkNanos(J)V

    :cond_1
    return-void
.end method
