.class public Lcom/jscape/util/j/SimpleFormatter;
.super Ljava/util/logging/Formatter;


# static fields
.field private static final TEMPLATE:Ljava/lang/String;

.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x36

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x3c

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "1mST9l4%x\u0003Dzg 0(.\u0000qs5`\u0014M\u0005efeYfR\u0011p6B4\u0007R\u0012pubI|R\u0013p14zyC\u0004\'61mST9l4%x\u0003Dzg 0(.\u0000qs5`\u0014M\u0005efeYfR\u0011p6B4\u0007R\u0012pubI|R\u0013p14zyC\u0004\'"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x6d

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/j/SimpleFormatter;->a:[Ljava/lang/String;

    aget-object v0, v1, v7

    sput-object v0, Lcom/jscape/util/j/SimpleFormatter;->TEMPLATE:Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x2d

    goto :goto_2

    :cond_2
    const/16 v12, 0x7e

    goto :goto_2

    :cond_3
    const/16 v12, 0x68

    goto :goto_2

    :cond_4
    const/16 v12, 0x1c

    goto :goto_2

    :cond_5
    const/16 v12, 0x4b

    goto :goto_2

    :cond_6
    const/16 v12, 0x60

    goto :goto_2

    :cond_7
    const/16 v12, 0x28

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/util/logging/Formatter;-><init>()V

    return-void
.end method


# virtual methods
.method public format(Ljava/util/logging/LogRecord;)Ljava/lang/String;
    .locals 6

    invoke-static {}, Lcom/jscape/util/j/b;->e()I

    move-result v0

    sget-object v1, Lcom/jscape/util/j/SimpleFormatter;->a:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {p1}, Ljava/util/logging/LogRecord;->getLevel()Ljava/util/logging/Level;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/logging/Level;->getLocalizedName()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v3, v4

    invoke-virtual {p0, p1}, Lcom/jscape/util/j/SimpleFormatter;->formatMessage(Ljava/util/logging/LogRecord;)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x2

    aput-object v2, v3, v5

    invoke-virtual {p1}, Ljava/util/logging/LogRecord;->getThrown()Ljava/lang/Throwable;

    move-result-object v2

    if-eqz v0, :cond_1

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Ljava/util/logging/LogRecord;->getThrown()Ljava/lang/Throwable;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string p1, ""

    goto :goto_1

    :cond_1
    :goto_0
    invoke-static {v2}, Lcom/jscape/util/L;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    :goto_1
    const/4 v2, 0x3

    aput-object p1, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v1

    if-nez v1, :cond_2

    add-int/2addr v0, v4

    invoke-static {v0}, Lcom/jscape/util/j/b;->b(I)V

    :cond_2
    return-object p1
.end method
