.class public Lcom/jscape/util/j/d;
.super Ljava/util/logging/StreamHandler;


# instance fields
.field private final a:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;Ljava/util/logging/Formatter;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/util/logging/StreamHandler;-><init>(Ljava/io/OutputStream;Ljava/util/logging/Formatter;)V

    iput-object p1, p0, Lcom/jscape/util/j/d;->a:Ljava/io/OutputStream;

    sget-object p1, Ljava/util/logging/Level;->ALL:Ljava/util/logging/Level;

    invoke-virtual {p0, p1}, Lcom/jscape/util/j/d;->setLevel(Ljava/util/logging/Level;)V

    return-void
.end method

.method private static a(Ljava/lang/SecurityException;)Ljava/lang/SecurityException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public a()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/j/d;->a:Ljava/io/OutputStream;

    return-object v0
.end method

.method public declared-synchronized close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/util/j/b;->e()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/util/j/d;->a:Ljava/io/OutputStream;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eq v0, v1, :cond_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/util/j/d;->a(Ljava/lang/SecurityException;)Ljava/lang/SecurityException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/util/j/d;->a(Ljava/lang/SecurityException;)Ljava/lang/SecurityException;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    invoke-super {p0}, Ljava/util/logging/StreamHandler;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public publish(Ljava/util/logging/LogRecord;)V
    .locals 1

    invoke-static {}, Lcom/jscape/util/j/b;->e()I

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/util/j/d;->isLoggable(Ljava/util/logging/LogRecord;)Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/util/j/d;->a(Ljava/lang/SecurityException;)Ljava/lang/SecurityException;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/j/d;->a(Ljava/lang/SecurityException;)Ljava/lang/SecurityException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Ljava/util/logging/StreamHandler;->publish(Ljava/util/logging/LogRecord;)V

    invoke-virtual {p0}, Lcom/jscape/util/j/d;->flush()V

    :cond_1
    return-void
.end method
