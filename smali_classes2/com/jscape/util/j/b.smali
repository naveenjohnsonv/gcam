.class public Lcom/jscape/util/j/b;
.super Ljava/util/logging/Logger;


# static fields
.field private static b:I


# instance fields
.field private a:Lcom/jscape/util/j/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/util/j/b;->e()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x4f

    invoke-static {v0}, Lcom/jscape/util/j/b;->b(I)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/jscape/util/j/d;Ljava/util/logging/Level;)V
    .locals 2

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Ljava/util/logging/Logger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/jscape/util/j/b;->c()V

    invoke-super {p0, p1}, Ljava/util/logging/Logger;->addHandler(Ljava/util/logging/Handler;)V

    iput-object p1, p0, Lcom/jscape/util/j/b;->a:Lcom/jscape/util/j/d;

    invoke-virtual {p0, p2}, Lcom/jscape/util/j/b;->setLevel(Ljava/util/logging/Level;)V

    return-void
.end method

.method public static a(Ljava/io/File;Ljava/util/logging/Level;)Lcom/jscape/util/j/b;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/j/b;

    new-instance v1, Lcom/jscape/util/j/d;

    new-instance v2, Ljava/io/FileOutputStream;

    const/4 v3, 0x1

    invoke-direct {v2, p0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    new-instance p0, Lcom/jscape/util/j/SimpleFormatter;

    invoke-direct {p0}, Lcom/jscape/util/j/SimpleFormatter;-><init>()V

    invoke-direct {v1, v2, p0}, Lcom/jscape/util/j/d;-><init>(Ljava/io/OutputStream;Ljava/util/logging/Formatter;)V

    invoke-direct {v0, v1, p1}, Lcom/jscape/util/j/b;-><init>(Lcom/jscape/util/j/d;Ljava/util/logging/Level;)V

    return-object v0
.end method

.method public static a(Ljava/nio/file/Path;Ljava/util/logging/Level;)Lcom/jscape/util/j/b;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-interface {p0}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/jscape/util/j/b;->a(Ljava/io/File;Ljava/util/logging/Level;)Lcom/jscape/util/j/b;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/util/logging/Level;)Lcom/jscape/util/j/b;
    .locals 4

    new-instance v0, Lcom/jscape/util/j/b;

    new-instance v1, Lcom/jscape/util/j/d;

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Lcom/jscape/util/j/SimpleFormatter;

    invoke-direct {v3}, Lcom/jscape/util/j/SimpleFormatter;-><init>()V

    invoke-direct {v1, v2, v3}, Lcom/jscape/util/j/d;-><init>(Ljava/io/OutputStream;Ljava/util/logging/Formatter;)V

    invoke-direct {v0, v1, p0}, Lcom/jscape/util/j/b;-><init>(Lcom/jscape/util/j/d;Ljava/util/logging/Level;)V

    return-object v0
.end method

.method private static a(Ljava/lang/SecurityException;)Ljava/lang/SecurityException;
    .locals 0

    return-object p0
.end method

.method public static b(I)V
    .locals 0

    sput p0, Lcom/jscape/util/j/b;->b:I

    return-void
.end method

.method private c()V
    .locals 6

    invoke-virtual {p0}, Lcom/jscape/util/j/b;->getHandlers()[Ljava/util/logging/Handler;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/j/b;->e()I

    move-result v1

    array-length v2, v0

    const/4 v3, 0x0

    move v4, v3

    :cond_0
    if-ge v4, v2, :cond_1

    aget-object v5, v0, v4

    :try_start_0
    invoke-super {p0, v5}, Ljava/util/logging/Logger;->removeHandler(Ljava/util/logging/Handler;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v4, v4, 0x1

    if-eqz v1, :cond_2

    if-nez v1, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/j/b;->a(Ljava/lang/SecurityException;)Ljava/lang/SecurityException;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    invoke-virtual {p0, v3}, Lcom/jscape/util/j/b;->setUseParentHandlers(Z)V

    :cond_2
    return-void
.end method

.method public static d()I
    .locals 1

    sget v0, Lcom/jscape/util/j/b;->b:I

    return v0
.end method

.method public static e()I
    .locals 1

    invoke-static {}, Lcom/jscape/util/j/b;->d()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x37

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public a()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/j/b;->a:Lcom/jscape/util/j/d;

    invoke-virtual {v0}, Lcom/jscape/util/j/d;->a()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/OutputStream;)V
    .locals 3

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jscape/util/j/b;->a:Lcom/jscape/util/j/d;

    invoke-virtual {v0}, Lcom/jscape/util/j/d;->close()V

    invoke-static {}, Lcom/jscape/util/j/b;->d()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/util/j/b;->a:Lcom/jscape/util/j/d;

    invoke-super {p0, v1}, Ljava/util/logging/Logger;->removeHandler(Ljava/util/logging/Handler;)V

    new-instance v1, Lcom/jscape/util/j/d;

    new-instance v2, Lcom/jscape/util/j/SimpleFormatter;

    invoke-direct {v2}, Lcom/jscape/util/j/SimpleFormatter;-><init>()V

    invoke-direct {v1, p1, v2}, Lcom/jscape/util/j/d;-><init>(Ljava/io/OutputStream;Ljava/util/logging/Formatter;)V

    iput-object v1, p0, Lcom/jscape/util/j/b;->a:Lcom/jscape/util/j/d;

    invoke-virtual {p0}, Lcom/jscape/util/j/b;->getLevel()Ljava/util/logging/Level;

    move-result-object p1

    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/j/b;->a:Lcom/jscape/util/j/d;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Ljava/util/logging/Level;->OFF:Ljava/util/logging/Level;

    :cond_1
    :goto_0
    :try_start_1
    invoke-virtual {v1, p1}, Lcom/jscape/util/j/d;->setLevel(Ljava/util/logging/Level;)V

    iget-object p1, p0, Lcom/jscape/util/j/b;->a:Lcom/jscape/util/j/d;

    invoke-super {p0, p1}, Ljava/util/logging/Logger;->addHandler(Ljava/util/logging/Handler;)V

    if-eqz v0, :cond_2

    const/4 p1, 0x1

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/j/b;->a(Ljava/lang/SecurityException;)Ljava/lang/SecurityException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/j/b;->a(Ljava/lang/SecurityException;)Ljava/lang/SecurityException;

    move-result-object p1

    throw p1
.end method

.method public addHandler(Ljava/util/logging/Handler;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    return-void
.end method

.method public b()V
    .locals 1

    invoke-virtual {p0}, Lcom/jscape/util/j/b;->a()Ljava/io/OutputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    return-void
.end method

.method public removeHandler(Ljava/util/logging/Handler;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    return-void
.end method

.method public setLevel(Ljava/util/logging/Level;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    invoke-super {p0, p1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    iget-object v0, p0, Lcom/jscape/util/j/b;->a:Lcom/jscape/util/j/d;

    invoke-virtual {v0, p1}, Lcom/jscape/util/j/d;->setLevel(Ljava/util/logging/Level;)V

    return-void
.end method
