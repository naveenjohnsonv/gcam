.class public Lcom/jscape/util/j/c;
.super Ljava/util/logging/StreamHandler;


# instance fields
.field private final a:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1

    new-instance v0, Lcom/jscape/util/j/a;

    invoke-direct {v0}, Lcom/jscape/util/j/a;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/jscape/util/j/c;-><init>(Ljava/io/OutputStream;Ljava/util/logging/Formatter;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Ljava/util/logging/Formatter;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/util/logging/StreamHandler;-><init>(Ljava/io/OutputStream;Ljava/util/logging/Formatter;)V

    iput-object p1, p0, Lcom/jscape/util/j/c;->a:Ljava/io/OutputStream;

    return-void
.end method


# virtual methods
.method public a()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/j/c;->a:Ljava/io/OutputStream;

    return-object v0
.end method

.method public publish(Ljava/util/logging/LogRecord;)V
    .locals 1

    invoke-static {}, Lcom/jscape/util/j/b;->d()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/jscape/util/j/c;->isLoggable(Ljava/util/logging/LogRecord;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-super {p0, p1}, Ljava/util/logging/StreamHandler;->publish(Ljava/util/logging/LogRecord;)V

    invoke-virtual {p0}, Lcom/jscape/util/j/c;->flush()V

    :cond_1
    return-void
.end method
