.class public Lcom/jscape/util/Base64Format;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/z;


# static fields
.field public static final DEFAULT_LINE_LENGTH:I = 0x48

.field private static final a:[Ljava/lang/String;

.field private static alphabet:[C

.field private static codes:[B


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, -0x1

    const-string v4, "y\u001c]7m\u0007Q^]Uro\tKSSAz?zSD(xs4s\\M#qt-hER:jm*aN[\u000f]X\u0019\\qf\u0006VQ\u0016Uzo\u0001OJ\u000fJct\u0018HC\u0004C\'0\\\u000c\u000fH\u000f 9W\u0014\u0014@"

    const/16 v5, 0x52

    const/16 v6, 0x10

    move v7, v3

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x16

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    const/16 v15, 0x2f

    if-gt v13, v14, :cond_6

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x2e

    const/16 v4, 0xc

    const-string v6, "O@\u00030!_\n\u000b\t\u0002%u!\"@\u001e 4V\u0008\u001aE\u000c70^K\u000bH\u0019\"uV\u000e\u0001N\u0019+u\u0012\u001c\u001dF\u0019&u"

    move v7, v3

    move v8, v11

    move-object/from16 v17, v6

    move v6, v4

    move-object/from16 v4, v17

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x42

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/Base64Format;->a:[Ljava/lang/String;

    aget-object v0, v1, v10

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/jscape/util/Base64Format;->alphabet:[C

    const/16 v0, 0x100

    new-array v0, v0, [B

    sput-object v0, Lcom/jscape/util/Base64Format;->codes:[B

    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([BB)V

    const/16 v0, 0x41

    :goto_4
    const/16 v1, 0x5a

    if-gt v0, v1, :cond_3

    :try_start_0
    sget-object v1, Lcom/jscape/util/Base64Format;->codes:[B

    add-int/lit8 v2, v0, -0x41

    int-to-byte v2, v2

    aput-byte v2, v1, v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_3
    const/16 v0, 0x61

    move v1, v0

    :goto_5
    const/16 v2, 0x7a

    if-gt v1, v2, :cond_4

    :try_start_1
    sget-object v2, Lcom/jscape/util/Base64Format;->codes:[B

    add-int/lit8 v3, v1, 0x1a

    sub-int/2addr v3, v0

    int-to-byte v3, v3

    aput-byte v3, v2, v1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_4
    const/16 v0, 0x30

    move v1, v0

    :goto_6
    const/16 v2, 0x39

    if-gt v1, v2, :cond_5

    :try_start_2
    sget-object v2, Lcom/jscape/util/Base64Format;->codes:[B

    add-int/lit8 v3, v1, 0x34

    sub-int/2addr v3, v0

    int-to-byte v3, v3

    aput-byte v3, v2, v1
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :cond_5
    sget-object v0, Lcom/jscape/util/Base64Format;->codes:[B

    const/16 v1, 0x2b

    const/16 v2, 0x3e

    aput-byte v2, v0, v1

    const/16 v1, 0x3f

    aput-byte v1, v0, v15

    return-void

    :cond_6
    aget-char v16, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_b

    if-eq v2, v10, :cond_a

    const/4 v3, 0x2

    if-eq v2, v3, :cond_c

    const/4 v3, 0x3

    if-eq v2, v3, :cond_9

    if-eq v2, v0, :cond_8

    const/4 v3, 0x5

    if-eq v2, v3, :cond_7

    const/16 v15, 0x29

    goto :goto_7

    :cond_7
    const/16 v15, 0x78

    goto :goto_7

    :cond_8
    const/16 v15, 0x17

    goto :goto_7

    :cond_9
    move v15, v10

    goto :goto_7

    :cond_a
    const/16 v15, 0x6b

    goto :goto_7

    :cond_b
    const/16 v15, 0x2d

    :cond_c
    :goto_7
    xor-int v2, v9, v15

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v3, -0x1

    goto/16 :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;
    .locals 0

    return-object p0
.end method

.method private static decode([C)[B
    .locals 12

    array-length v0, p0

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v1

    const/4 v2, 0x0

    move v3, v2

    :cond_0
    array-length v4, p0

    const/16 v5, 0xff

    const/4 v6, 0x3

    if-ge v3, v4, :cond_5

    :try_start_0
    aget-char v4, p0, v3
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_2

    if-eqz v1, :cond_1

    if-gt v4, v5, :cond_3

    :try_start_1
    sget-object v4, Lcom/jscape/util/Base64Format;->codes:[B

    aget-char v7, p0, v3

    aget-byte v4, v4, v7
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :cond_1
    move v7, v5

    goto :goto_2

    :cond_2
    :goto_0
    if-gez v4, :cond_4

    :cond_3
    add-int/lit8 v0, v0, -0x1

    :cond_4
    add-int/lit8 v3, v3, 0x1

    if-nez v1, :cond_0

    goto :goto_1

    :catch_0
    move-exception p0

    :try_start_2
    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p0

    :try_start_3
    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0

    :cond_5
    :goto_1
    div-int/lit8 v3, v0, 0x4

    mul-int/2addr v3, v6

    rem-int/lit8 v4, v0, 0x4

    move v7, v6

    :goto_2
    const/4 v8, 0x2

    if-eqz v1, :cond_7

    if-ne v4, v7, :cond_6

    add-int/lit8 v3, v3, 0x2

    :cond_6
    :try_start_4
    rem-int/lit8 v4, v0, 0x4
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_3

    if-eqz v1, :cond_9

    move v7, v8

    goto :goto_3

    :catch_3
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0

    :cond_7
    :goto_3
    if-ne v4, v7, :cond_8

    add-int/lit8 v3, v3, 0x1

    :cond_8
    move v4, v3

    :cond_9
    new-array v0, v4, [B

    move v3, v2

    move v7, v3

    move v9, v7

    :cond_a
    array-length v10, p0

    if-ge v2, v10, :cond_f

    :try_start_5
    aget-char v10, p0, v2
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_5

    if-eqz v1, :cond_c

    if-eqz v1, :cond_10

    if-le v10, v5, :cond_b

    const/4 v10, -0x1

    goto :goto_4

    :cond_b
    sget-object v10, Lcom/jscape/util/Base64Format;->codes:[B

    aget-char v11, p0, v2

    aget-byte v10, v10, v11

    :cond_c
    :goto_4
    if-eqz v1, :cond_e

    if-ltz v10, :cond_d

    shl-int/lit8 v7, v7, 0x6

    add-int/lit8 v9, v9, 0x6

    or-int/2addr v7, v10

    if-eqz v1, :cond_e

    const/16 v10, 0x8

    if-lt v9, v10, :cond_d

    add-int/lit8 v9, v9, -0x8

    add-int/lit8 v10, v3, 0x1

    shr-int v11, v7, v9

    and-int/2addr v11, v5

    int-to-byte v11, v11

    :try_start_6
    aput-byte v11, v0, v3
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_4

    move v3, v10

    goto :goto_5

    :catch_4
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0

    :cond_d
    :goto_5
    add-int/lit8 v2, v2, 0x1

    :cond_e
    if-nez v1, :cond_a

    goto :goto_6

    :catch_5
    move-exception p0

    :try_start_7
    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_6

    :catch_6
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0

    :cond_f
    :goto_6
    move v10, v3

    move v5, v4

    :cond_10
    if-ne v10, v5, :cond_11

    return-object v0

    :cond_11
    :try_start_8
    new-instance p0, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/Base64Format;->a:[Ljava/lang/String;

    aget-object v1, v1, v6

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    sget-object v1, Lcom/jscape/util/Base64Format;->a:[Ljava/lang/String;

    aget-object v1, v1, v8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_7

    :catch_7
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0
.end method

.method private static encode([BI)[C
    .locals 13

    int-to-long v0, p1

    sget-object v2, Lcom/jscape/util/Base64Format;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const-wide/16 v4, 0x0

    invoke-static {v0, v1, v4, v5, v2}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    array-length v0, p0

    add-int/lit8 v0, v0, 0x2

    div-int/lit8 v0, v0, 0x3

    mul-int/lit8 v0, v0, 0x6

    new-array v0, v0, [C

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v1

    move v2, v3

    move v4, v2

    move v5, v4

    :cond_0
    array-length v6, p0

    if-ge v2, v6, :cond_b

    aget-byte v6, p0, v2

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    add-int/lit8 v7, v2, 0x1

    if-nez v1, :cond_c

    :try_start_0
    array-length v8, p0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_6

    const/4 v9, 0x1

    if-nez v1, :cond_2

    if-ge v7, v8, :cond_1

    aget-byte v7, p0, v7

    and-int/lit16 v7, v7, 0xff

    or-int/2addr v6, v7

    move v7, v9

    goto :goto_0

    :cond_1
    move v7, v3

    :goto_0
    shl-int/lit8 v6, v6, 0x8

    add-int/lit8 v8, v2, 0x2

    array-length v10, p0

    move v12, v8

    move v8, v7

    move v7, v12

    goto :goto_1

    :cond_2
    move v10, v8

    move v8, v3

    :goto_1
    if-nez v1, :cond_4

    if-ge v7, v10, :cond_3

    add-int/lit8 v7, v2, 0x2

    :try_start_1
    aget-byte v7, p0, v7
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    and-int/lit16 v10, v7, 0xff

    goto :goto_2

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0

    :cond_3
    move v9, v3

    goto :goto_3

    :cond_4
    move v6, v7

    :goto_2
    or-int/2addr v6, v10

    :goto_3
    add-int/lit8 v7, v4, 0x3

    :try_start_2
    sget-object v10, Lcom/jscape/util/Base64Format;->alphabet:[C
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_4

    const/16 v11, 0x40

    if-nez v1, :cond_6

    if-eqz v9, :cond_5

    and-int/lit8 v9, v6, 0x3f

    goto :goto_4

    :cond_5
    move v9, v11

    :cond_6
    :goto_4
    aget-char v9, v10, v9

    aput-char v9, v0, v7

    shr-int/lit8 v6, v6, 0x6

    add-int/lit8 v7, v4, 0x2

    :try_start_3
    sget-object v9, Lcom/jscape/util/Base64Format;->alphabet:[C
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    if-nez v1, :cond_7

    if-eqz v8, :cond_8

    and-int/lit8 v8, v6, 0x3f

    :cond_7
    move v11, v8

    :cond_8
    aget-char v8, v9, v11

    aput-char v8, v0, v7

    shr-int/lit8 v6, v6, 0x6

    add-int/lit8 v8, v4, 0x1

    sget-object v9, Lcom/jscape/util/Base64Format;->alphabet:[C

    and-int/lit8 v10, v6, 0x3f

    aget-char v10, v9, v10

    aput-char v10, v0, v8

    shr-int/lit8 v6, v6, 0x6

    and-int/lit8 v6, v6, 0x3f

    :try_start_4
    aget-char v6, v9, v6

    aput-char v6, v0, v4
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1

    add-int/lit8 v5, v5, 0x4

    if-nez v1, :cond_a

    if-ne v5, p1, :cond_9

    add-int/lit8 v5, v4, 0x4

    const/16 v6, 0xd

    aput-char v6, v0, v5

    add-int/lit8 v4, v4, 0x5

    const/16 v5, 0xa

    aput-char v5, v0, v4

    move v5, v3

    move v4, v7

    :cond_9
    add-int/lit8 v2, v2, 0x3

    add-int/lit8 v4, v4, 0x4

    :cond_a
    if-eqz v1, :cond_0

    goto :goto_5

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0

    :catch_2
    move-exception p0

    :try_start_5
    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0

    :catch_4
    move-exception p0

    :try_start_6
    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_5

    :catch_5
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0

    :catch_6
    move-exception p0

    :try_start_7
    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0

    :cond_b
    :goto_5
    move v7, v4

    :cond_c
    new-array p0, v7, [C

    invoke-static {v0, v3, p0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p0
.end method

.method public static formatData([B)Ljava/lang/String;
    .locals 1

    const v0, 0x7fffffff

    invoke-static {p0, v0}, Lcom/jscape/util/Base64Format;->formatData([BI)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formatData([BI)Ljava/lang/String;
    .locals 1

    if-nez p0, :cond_0

    :try_start_0
    const-string p0, ""
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/jscape/util/Base64Format;->encode([BI)[C

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method public static formatData([BII)Ljava/lang/String;
    .locals 1

    const v0, 0x7fffffff

    invoke-static {p0, p1, p2, v0}, Lcom/jscape/util/Base64Format;->formatData([BIII)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static formatData([BIII)Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p0, :cond_0

    :try_start_0
    const-string p0, ""
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/Base64Format;->a(Ljava/lang/RuntimeException;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/jscape/util/aq;->a([BII)V

    new-array p2, p2, [B

    goto :goto_0

    :cond_1
    move-object p2, p0

    :goto_0
    const/4 v0, 0x0

    array-length v1, p2

    invoke-static {p0, p1, p2, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance p0, Ljava/lang/String;

    invoke-static {p2, p3}, Lcom/jscape/util/Base64Format;->encode([BI)[C

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/String;-><init>([C)V

    return-object p0
.end method

.method public static parseData(Ljava/lang/String;)[B
    .locals 0

    invoke-static {p0}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/util/Base64Format;->decode([C)[B

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public format([B)Ljava/lang/String;
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/Base64Format;->formatData([B)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public format([BIII)Ljava/lang/String;
    .locals 0

    invoke-static {p1, p2, p3, p4}, Lcom/jscape/util/Base64Format;->formatData([BIII)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public parse(Ljava/lang/String;)[B
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/Base64Format;->parseData(Ljava/lang/String;)[B

    move-result-object p1

    return-object p1
.end method
