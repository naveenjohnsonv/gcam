.class public Lcom/jscape/util/ae;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/jscape/util/ae;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field public final a:J

.field public final b:Lcom/jscape/util/q;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x12

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x1

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    const-string v7, "\u0002\u0007\")<-O&\u0018*f5\"}#\u0017*{\u0007cB:(\' !\u0016\r\u0003+f#1q \u00106f==f*B9\'\"!ya"

    invoke-virtual {v7, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v8, v4

    move v9, v2

    :goto_1
    const/16 v10, 0x31

    if-gt v8, v9, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    if-ge v3, v10, :cond_0

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/ae;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v9

    rem-int/lit8 v12, v9, 0x7

    const/16 v13, 0x7e

    if-eqz v12, :cond_6

    if-eq v12, v6, :cond_5

    const/4 v14, 0x2

    if-eq v12, v14, :cond_6

    if-eq v12, v0, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v13, 0x2d

    goto :goto_2

    :cond_2
    const/16 v13, 0x65

    goto :goto_2

    :cond_3
    const/16 v13, 0x7f

    goto :goto_2

    :cond_4
    const/16 v13, 0x77

    goto :goto_2

    :cond_5
    const/16 v13, 0x53

    :cond_6
    :goto_2
    xor-int/2addr v10, v13

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v4, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public constructor <init>(JLcom/jscape/util/q;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/jscape/util/ae;->c:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    const-wide/16 v1, 0x0

    invoke-static {p1, p2, v1, v2, v0}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    iput-wide p1, p0, Lcom/jscape/util/ae;->a:J

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/util/ae;->b:Lcom/jscape/util/q;

    return-void
.end method

.method public static a(J)Lcom/jscape/util/ae;
    .locals 2

    new-instance v0, Lcom/jscape/util/ae;

    sget-object v1, Lcom/jscape/util/q;->a:Lcom/jscape/util/q;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/util/ae;-><init>(JLcom/jscape/util/q;)V

    return-object v0
.end method

.method public static b(J)Lcom/jscape/util/ae;
    .locals 2

    new-instance v0, Lcom/jscape/util/ae;

    sget-object v1, Lcom/jscape/util/q;->b:Lcom/jscape/util/q;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/util/ae;-><init>(JLcom/jscape/util/q;)V

    return-object v0
.end method

.method public static c(J)Lcom/jscape/util/ae;
    .locals 2

    new-instance v0, Lcom/jscape/util/ae;

    sget-object v1, Lcom/jscape/util/q;->c:Lcom/jscape/util/q;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/util/ae;-><init>(JLcom/jscape/util/q;)V

    return-object v0
.end method

.method public static d(J)Lcom/jscape/util/ae;
    .locals 2

    new-instance v0, Lcom/jscape/util/ae;

    sget-object v1, Lcom/jscape/util/q;->d:Lcom/jscape/util/q;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/util/ae;-><init>(JLcom/jscape/util/q;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/jscape/util/ae;)I
    .locals 5

    invoke-virtual {p0}, Lcom/jscape/util/ae;->a()J

    move-result-wide v0

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v2

    invoke-virtual {p1}, Lcom/jscape/util/ae;->a()J

    move-result-wide v3

    cmp-long p1, v0, v3

    if-eqz v2, :cond_0

    if-gez p1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    if-eqz v2, :cond_2

    if-nez p1, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    :cond_2
    :goto_0
    return p1
.end method

.method public a()J
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/ae;->b:Lcom/jscape/util/q;

    iget-wide v1, p0, Lcom/jscape/util/ae;->a:J

    invoke-virtual {v0, v1, v2}, Lcom/jscape/util/q;->a(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()J
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/ae;->b:Lcom/jscape/util/q;

    iget-wide v1, p0, Lcom/jscape/util/ae;->a:J

    invoke-virtual {v0, v1, v2}, Lcom/jscape/util/q;->b(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()J
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/ae;->b:Lcom/jscape/util/q;

    iget-wide v1, p0, Lcom/jscape/util/ae;->a:J

    invoke-virtual {v0, v1, v2}, Lcom/jscape/util/q;->c(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/jscape/util/ae;

    invoke-virtual {p0, p1}, Lcom/jscape/util/ae;->a(Lcom/jscape/util/ae;)I

    move-result p1

    return p1
.end method

.method public d()J
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/ae;->b:Lcom/jscape/util/q;

    iget-wide v1, p0, Lcom/jscape/util/ae;->a:J

    invoke-virtual {v0, v1, v2}, Lcom/jscape/util/q;->d(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    if-ne p0, p1, :cond_0

    return v1

    :cond_0
    move-object v2, p1

    goto :goto_0

    :cond_1
    move-object v2, p0

    :goto_0
    const/4 v3, 0x0

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-nez v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v2, v4, :cond_3

    goto :goto_2

    :cond_2
    move-object p1, v2

    :cond_3
    check-cast p1, Lcom/jscape/util/ae;

    invoke-virtual {p0}, Lcom/jscape/util/ae;->a()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/jscape/util/ae;->a()J

    move-result-wide v6

    cmp-long p1, v4, v6

    if-nez v0, :cond_4

    if-nez p1, :cond_5

    goto :goto_1

    :cond_4
    move v1, p1

    :goto_1
    move v3, v1

    :cond_5
    :goto_2
    return v3
.end method

.method public hashCode()I
    .locals 4

    iget-wide v0, p0, Lcom/jscape/util/ae;->a:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/jscape/util/ae;->b:Lcom/jscape/util/q;

    invoke-virtual {v1}, Lcom/jscape/util/q;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/ae;->c:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/util/ae;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/ae;->b:Lcom/jscape/util/q;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
