.class public Lcom/jscape/util/af;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;CCLjava/util/Map;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "CC",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/ai;

    invoke-direct {v0, p1}, Lcom/jscape/util/ai;-><init>(C)V

    new-instance v1, Lcom/jscape/util/ah;

    invoke-direct {v1, p1, p2, p3}, Lcom/jscape/util/ah;-><init>(CCLjava/util/Map;)V

    invoke-static {p0, v0, v1}, Lcom/jscape/util/af;->a(Ljava/lang/String;Lcom/jscape/util/aj;Lcom/jscape/util/aj;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;CC[Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    new-instance v0, Lcom/jscape/util/ai;

    invoke-direct {v0, p1}, Lcom/jscape/util/ai;-><init>(C)V

    new-instance v1, Lcom/jscape/util/ag;

    invoke-direct {v1, p1, p2, p3}, Lcom/jscape/util/ag;-><init>(CC[Ljava/lang/Object;)V

    invoke-static {p0, v0, v1}, Lcom/jscape/util/af;->a(Ljava/lang/String;Lcom/jscape/util/aj;Lcom/jscape/util/aj;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;Lcom/jscape/util/aj;Lcom/jscape/util/aj;)Ljava/lang/String;
    .locals 4

    invoke-interface {p1, p2}, Lcom/jscape/util/aj;->a(Lcom/jscape/util/aj;)V

    invoke-interface {p2, p1}, Lcom/jscape/util/aj;->a(Lcom/jscape/util/aj;)V

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result p2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v1, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-interface {p1, v3, v0}, Lcom/jscape/util/aj;->a(CLjava/lang/StringBuffer;)Lcom/jscape/util/aj;

    move-result-object p1

    add-int/lit8 v2, v2, 0x1

    if-nez p2, :cond_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const/16 v0, 0x7b

    const/16 v1, 0x7d

    invoke-static {p0, v0, v1, p1}, Lcom/jscape/util/af;->a(Ljava/lang/String;CCLjava/util/Map;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    const/16 v0, 0x7b

    const/16 v1, 0x7d

    invoke-static {p0, v0, v1, p1}, Lcom/jscape/util/af;->a(Ljava/lang/String;CC[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
