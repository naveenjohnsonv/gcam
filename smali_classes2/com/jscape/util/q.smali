.class public final enum Lcom/jscape/util/q;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/util/q;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/jscape/util/q;

.field public static final enum b:Lcom/jscape/util/q;

.field public static final enum c:Lcom/jscape/util/q;

.field public static final enum d:Lcom/jscape/util/q;

.field private static final e:[J

.field private static final f:[J

.field private static final synthetic h:[Lcom/jscape/util/q;


# instance fields
.field private final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "X=*\u000e\u0000\u0004\tZ\'\u0005]-9\n\u0011"

    const/16 v6, 0xf

    move v9, v4

    const/4 v7, -0x1

    const/16 v8, 0x9

    :goto_0
    const/16 v10, 0x70

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x2

    const/4 v14, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v15, v12

    move v2, v4

    :goto_2
    const/4 v3, 0x3

    if-gt v15, v2, :cond_3

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    if-eqz v14, :cond_1

    add-int/lit8 v3, v9, 0x1

    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v3

    goto :goto_0

    :cond_0
    const/16 v6, 0x13

    const-string v5, "&OSrrv{(U\t CX|rv{(U"

    move v9, v3

    const/4 v7, -0x1

    const/16 v8, 0x9

    goto :goto_3

    :cond_1
    add-int/lit8 v10, v9, 0x1

    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v2

    move v9, v10

    :goto_3
    add-int/2addr v7, v11

    add-int v2, v7, v8

    invoke-virtual {v5, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v14, v4

    move v10, v13

    goto :goto_1

    :cond_2
    new-instance v2, Lcom/jscape/util/q;

    aget-object v5, v1, v11

    invoke-direct {v2, v5, v4, v4}, Lcom/jscape/util/q;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/util/q;->a:Lcom/jscape/util/q;

    new-instance v2, Lcom/jscape/util/q;

    aget-object v5, v1, v13

    invoke-direct {v2, v5, v11, v11}, Lcom/jscape/util/q;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/util/q;->b:Lcom/jscape/util/q;

    new-instance v2, Lcom/jscape/util/q;

    aget-object v5, v1, v3

    invoke-direct {v2, v5, v13, v13}, Lcom/jscape/util/q;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/util/q;->c:Lcom/jscape/util/q;

    new-instance v2, Lcom/jscape/util/q;

    aget-object v1, v1, v4

    invoke-direct {v2, v1, v3, v3}, Lcom/jscape/util/q;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/jscape/util/q;->d:Lcom/jscape/util/q;

    new-array v1, v0, [Lcom/jscape/util/q;

    sget-object v5, Lcom/jscape/util/q;->a:Lcom/jscape/util/q;

    aput-object v5, v1, v4

    sget-object v4, Lcom/jscape/util/q;->b:Lcom/jscape/util/q;

    aput-object v4, v1, v11

    sget-object v4, Lcom/jscape/util/q;->c:Lcom/jscape/util/q;

    aput-object v4, v1, v13

    aput-object v2, v1, v3

    sput-object v1, Lcom/jscape/util/q;->h:[Lcom/jscape/util/q;

    new-array v1, v0, [J

    fill-array-data v1, :array_0

    sput-object v1, Lcom/jscape/util/q;->e:[J

    new-array v0, v0, [J

    fill-array-data v0, :array_1

    sput-object v0, Lcom/jscape/util/q;->f:[J

    return-void

    :cond_3
    aget-char v16, v12, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v17, 0x2d

    if-eqz v4, :cond_8

    if-eq v4, v11, :cond_7

    if-eq v4, v13, :cond_6

    if-eq v4, v3, :cond_5

    if-eq v4, v0, :cond_4

    const/4 v3, 0x5

    goto :goto_4

    :cond_4
    const/16 v17, 0x32

    goto :goto_4

    :cond_5
    const/16 v17, 0x3f

    goto :goto_4

    :cond_6
    const/16 v17, 0x1d

    goto :goto_4

    :cond_7
    move/from16 v17, v0

    goto :goto_4

    :cond_8
    const/16 v17, 0x6f

    :goto_4
    xor-int v3, v10, v17

    xor-int v3, v16, v3

    int-to-char v3, v3

    aput-char v3, v12, v2

    add-int/lit8 v2, v2, 0x1

    const/4 v4, 0x0

    goto/16 :goto_2

    :array_0
    .array-data 8
        0x1
        0x400
        0x100000
        0x40000000
    .end array-data

    :array_1
    .array-data 8
        0x0
        0x1fffffffffffffL
        0x7ffffffffffL
        0x1ffffffffL
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/util/q;->g:I

    return-void
.end method

.method private static a(IJ)J
    .locals 3

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p0, :cond_0

    return-wide p1

    :cond_0
    if-eqz v0, :cond_2

    if-gez p0, :cond_1

    sget-object v0, Lcom/jscape/util/q;->e:[J

    neg-int p0, p0

    aget-wide v0, v0, p0

    div-long/2addr p1, v0

    return-wide p1

    :cond_1
    sget-object v1, Lcom/jscape/util/q;->f:[J

    aget-wide v1, v1, p0

    cmp-long v1, p1, v1

    goto :goto_0

    :cond_2
    move v1, p0

    :goto_0
    if-eqz v0, :cond_4

    if-lez v1, :cond_3

    const-wide p0, 0x7fffffffffffffffL

    return-wide p0

    :cond_3
    sget-object v1, Lcom/jscape/util/q;->f:[J

    aget-wide v1, v1, p0

    neg-long v1, v1

    if-eqz v0, :cond_6

    cmp-long v1, p1, v1

    :cond_4
    if-gez v1, :cond_5

    const-wide/high16 p0, -0x8000000000000000L

    return-wide p0

    :cond_5
    sget-object v0, Lcom/jscape/util/q;->f:[J

    aget-wide v1, v0, p0

    :cond_6
    mul-long/2addr p1, v1

    return-wide p1
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/util/q;
    .locals 1

    const-class v0, Lcom/jscape/util/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/util/q;

    return-object p0
.end method

.method public static a()[Lcom/jscape/util/q;
    .locals 1

    sget-object v0, Lcom/jscape/util/q;->h:[Lcom/jscape/util/q;

    invoke-virtual {v0}, [Lcom/jscape/util/q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/util/q;

    return-object v0
.end method


# virtual methods
.method public a(J)J
    .locals 1

    iget v0, p0, Lcom/jscape/util/q;->g:I

    invoke-static {v0, p1, p2}, Lcom/jscape/util/q;->a(IJ)J

    move-result-wide p1

    return-wide p1
.end method

.method public a(JLcom/jscape/util/q;)J
    .locals 1

    iget p3, p3, Lcom/jscape/util/q;->g:I

    iget v0, p0, Lcom/jscape/util/q;->g:I

    sub-int/2addr p3, v0

    invoke-static {p3, p1, p2}, Lcom/jscape/util/q;->a(IJ)J

    move-result-wide p1

    return-wide p1
.end method

.method public b(J)J
    .locals 2

    iget v0, p0, Lcom/jscape/util/q;->g:I

    sget-object v1, Lcom/jscape/util/q;->b:Lcom/jscape/util/q;

    iget v1, v1, Lcom/jscape/util/q;->g:I

    sub-int/2addr v0, v1

    invoke-static {v0, p1, p2}, Lcom/jscape/util/q;->a(IJ)J

    move-result-wide p1

    return-wide p1
.end method

.method public c(J)J
    .locals 2

    iget v0, p0, Lcom/jscape/util/q;->g:I

    sget-object v1, Lcom/jscape/util/q;->c:Lcom/jscape/util/q;

    iget v1, v1, Lcom/jscape/util/q;->g:I

    sub-int/2addr v0, v1

    invoke-static {v0, p1, p2}, Lcom/jscape/util/q;->a(IJ)J

    move-result-wide p1

    return-wide p1
.end method

.method public d(J)J
    .locals 2

    iget v0, p0, Lcom/jscape/util/q;->g:I

    sget-object v1, Lcom/jscape/util/q;->d:Lcom/jscape/util/q;

    iget v1, v1, Lcom/jscape/util/q;->g:I

    sub-int/2addr v0, v1

    invoke-static {v0, p1, p2}, Lcom/jscape/util/q;->a(IJ)J

    move-result-wide p1

    return-wide p1
.end method
