.class public Lcom/jscape/util/aw;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:C = '*'

.field public static final b:Ljava/lang/String; = "*"

.field public static final c:C = '?'

.field public static final d:Ljava/lang/String; = "?"

.field private static final f:Ljava/lang/String;


# instance fields
.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "\u0019O"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/aw;->f:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x6d

    goto :goto_1

    :cond_1
    const/16 v4, 0x48

    goto :goto_1

    :cond_2
    const/16 v4, 0x76

    goto :goto_1

    :cond_3
    const/16 v4, 0x13

    goto :goto_1

    :cond_4
    const/16 v4, 0x2f

    goto :goto_1

    :cond_5
    const/16 v4, 0x45

    goto :goto_1

    :cond_6
    const/16 v4, 0x17

    :goto_1
    const/16 v5, 0x20

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/jscape/util/aw;->e:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    const-string v1, "*"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v0, :cond_2

    if-nez v1, :cond_1

    const-string v1, "?"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v0, :cond_2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move p0, v1

    :goto_1
    return p0
.end method

.method private a(Ljava/lang/String;ILjava/lang/String;I)Z
    .locals 6

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x2a

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v0, :cond_4

    if-lt p4, v1, :cond_2

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    if-lt p2, p1, :cond_1

    move p2, v4

    :cond_0
    move v3, p2

    :cond_1
    return v3

    :cond_2
    if-nez v0, :cond_3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    move v5, p2

    goto :goto_0

    :cond_3
    move v1, p2

    goto :goto_2

    :cond_4
    move v5, p4

    :goto_0
    if-lt v5, v1, :cond_7

    invoke-virtual {p3, p4}, Ljava/lang/String;->charAt(I)C

    move-result p1

    if-nez v0, :cond_5

    if-ne p1, v2, :cond_6

    move v3, v4

    goto :goto_1

    :cond_5
    move v3, p1

    :cond_6
    :goto_1
    return v3

    :cond_7
    invoke-virtual {p3, p4}, Ljava/lang/String;->charAt(I)C

    move-result v1

    :goto_2
    if-nez v0, :cond_11

    if-eq v1, v2, :cond_9

    const/16 v2, 0x3f

    if-eq v1, v2, :cond_8

    goto :goto_6

    :cond_8
    add-int/2addr p2, v4

    add-int/2addr p4, v4

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/jscape/util/aw;->a(Ljava/lang/String;ILjava/lang/String;I)Z

    move-result p1

    return p1

    :cond_9
    add-int/2addr p4, v4

    if-nez v0, :cond_b

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    if-lt p4, v1, :cond_a

    return v4

    :cond_a
    invoke-virtual {p3, p4}, Ljava/lang/String;->charAt(I)C

    move-result v1

    goto :goto_3

    :cond_b
    move v1, p4

    :goto_3
    invoke-virtual {p1, v1, p2}, Ljava/lang/String;->indexOf(II)I

    move-result p2

    :cond_c
    const/4 v2, -0x1

    if-eq p2, v2, :cond_10

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/jscape/util/aw;->a(Ljava/lang/String;ILjava/lang/String;I)Z

    move-result v2

    if-nez v0, :cond_f

    if-nez v0, :cond_e

    if-eqz v2, :cond_d

    return v4

    :cond_d
    add-int/lit8 p2, p2, 0x1

    invoke-virtual {p1, v1, p2}, Ljava/lang/String;->indexOf(II)I

    move-result p2

    goto :goto_4

    :cond_e
    move p2, v2

    :goto_4
    if-eqz v0, :cond_c

    goto :goto_5

    :cond_f
    move v3, v2

    :cond_10
    :goto_5
    return v3

    :cond_11
    :goto_6
    if-nez v0, :cond_12

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-ne v1, v2, :cond_14

    add-int/2addr p2, v4

    add-int/2addr p4, v4

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/jscape/util/aw;->a(Ljava/lang/String;ILjava/lang/String;I)Z

    move-result v1

    :cond_12
    if-nez v0, :cond_13

    if-eqz v1, :cond_14

    move v3, v4

    goto :goto_7

    :cond_13
    move v3, v1

    :cond_14
    :goto_7
    return v3
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v1

    const/16 v2, 0x5e

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    :cond_0
    const/16 v4, 0x24

    if-ge v3, v2, :cond_5

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-nez v1, :cond_6

    if-nez v1, :cond_1

    if-eq v5, v4, :cond_3

    const/16 v6, 0x2e

    if-eq v5, v6, :cond_3

    const/16 v6, 0x3f

    if-eq v5, v6, :cond_2

    packed-switch v5, :pswitch_data_0

    packed-switch v5, :pswitch_data_1

    packed-switch v5, :pswitch_data_2

    goto :goto_0

    :pswitch_0
    sget-object v6, Lcom/jscape/util/aw;->f:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    if-eqz v1, :cond_4

    :cond_2
    const-string v6, "."

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v1, :cond_4

    :cond_3
    :pswitch_1
    const-string v6, "\\"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    if-eqz v1, :cond_4

    :goto_0
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_4
    add-int/lit8 v3, v3, 0x1

    if-eqz v1, :cond_0

    :cond_5
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_data_0
    .packed-switch 0x28
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x5b
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x7b
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/aw;->e:Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 2

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/jscape/util/aw;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0, v1}, Lcom/jscape/util/aw;->a(Ljava/lang/String;ILjava/lang/String;I)Z

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    if-nez v0, :cond_1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    move-object v1, p1

    goto :goto_0

    :cond_1
    move-object v1, p0

    :goto_0
    if-eqz v1, :cond_4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-nez v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    if-eq v1, v0, :cond_3

    goto :goto_1

    :cond_2
    move-object p1, v1

    :cond_3
    check-cast p1, Lcom/jscape/util/aw;

    iget-object v0, p0, Lcom/jscape/util/aw;->e:Ljava/lang/String;

    iget-object p1, p1, Lcom/jscape/util/aw;->e:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_4
    :goto_1
    const/4 p1, 0x0

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/aw;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/aw;->e:Ljava/lang/String;

    return-object v0
.end method
