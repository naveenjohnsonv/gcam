.class final enum Lcom/jscape/util/c/b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/util/c/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/jscape/util/c/b;

.field public static final enum b:Lcom/jscape/util/c/b;

.field private static final d:[Lcom/jscape/util/c/b;

.field private static final e:[Ljava/lang/String;


# instance fields
.field public final c:Ljava/security/Provider;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "\rb1\u001c\u0007\u0019n&\u001a}&c"

    move v6, v0

    move v7, v3

    const/4 v5, -0x1

    :goto_0
    const/16 v8, 0x6a

    const/4 v9, 0x1

    add-int/2addr v5, v9

    add-int v10, v5, v6

    invoke-virtual {v4, v5, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    const/16 v12, 0xc

    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v13, v10

    move v14, v3

    :goto_2
    const/4 v15, 0x3

    const/4 v2, 0x2

    if-gt v13, v14, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    if-eqz v11, :cond_1

    add-int/lit8 v2, v7, 0x1

    aput-object v8, v1, v7

    add-int/2addr v5, v6

    if-ge v5, v12, :cond_0

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v7, v2

    goto :goto_0

    :cond_0
    const-string v4, ")F\u00158\u0007=J\u0002>Y\u0002G"

    move v6, v0

    move v7, v2

    const/4 v5, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v10, v7, 0x1

    aput-object v8, v1, v7

    add-int/2addr v5, v6

    if-ge v5, v12, :cond_2

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v6, v2

    move v7, v10

    :goto_3
    const/16 v8, 0x4e

    add-int/2addr v5, v9

    add-int v2, v5, v6

    invoke-virtual {v4, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v3

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/c/b;->e:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/util/c/b;

    sget-object v1, Lcom/jscape/util/c/b;->e:[Ljava/lang/String;

    aget-object v4, v1, v15

    aget-object v5, v1, v9

    invoke-direct {v0, v4, v3, v5}, Lcom/jscape/util/c/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/util/c/b;->a:Lcom/jscape/util/c/b;

    new-instance v0, Lcom/jscape/util/c/b;

    aget-object v4, v1, v2

    aget-object v1, v1, v3

    invoke-direct {v0, v4, v9, v1}, Lcom/jscape/util/c/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/jscape/util/c/b;->b:Lcom/jscape/util/c/b;

    new-array v1, v2, [Lcom/jscape/util/c/b;

    sget-object v2, Lcom/jscape/util/c/b;->a:Lcom/jscape/util/c/b;

    aput-object v2, v1, v3

    aput-object v0, v1, v9

    sput-object v1, Lcom/jscape/util/c/b;->d:[Lcom/jscape/util/c/b;

    return-void

    :cond_3
    aget-char v16, v10, v14

    rem-int/lit8 v3, v14, 0x7

    const/16 v17, 0x5b

    if-eqz v3, :cond_8

    if-eq v3, v9, :cond_7

    if-eq v3, v2, :cond_6

    if-eq v3, v15, :cond_5

    if-eq v3, v0, :cond_9

    const/4 v2, 0x5

    if-eq v3, v2, :cond_4

    goto :goto_4

    :cond_4
    const/16 v17, 0xd

    goto :goto_4

    :cond_5
    const/16 v17, 0x25

    goto :goto_4

    :cond_6
    const/16 v17, 0xb

    goto :goto_4

    :cond_7
    const/16 v17, 0x41

    goto :goto_4

    :cond_8
    const/16 v17, 0x21

    :cond_9
    :goto_4
    xor-int v2, v8, v17

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v10, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    sget-object p1, Lcom/jscape/util/c/b;->e:[Ljava/lang/String;

    const/4 p2, 0x0

    aget-object p1, p1, p2

    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/jscape/util/c/a;->c()Ljava/security/Provider;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/jscape/util/c/a;->b()Ljava/security/Provider;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/jscape/util/c/b;->c:Ljava/security/Provider;

    sget-object p1, Lcom/jscape/util/c/b;->e:[Ljava/lang/String;

    const/4 p2, 0x1

    aget-object p1, p1, p2

    invoke-virtual {p1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/jscape/util/c/b;->c:Ljava/security/Provider;

    if-eqz p1, :cond_1

    invoke-static {p1}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I

    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/util/c/b;
    .locals 1

    const-class v0, Lcom/jscape/util/c/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/util/c/b;

    return-object p0
.end method

.method public static a()[Lcom/jscape/util/c/b;
    .locals 1

    sget-object v0, Lcom/jscape/util/c/b;->d:[Lcom/jscape/util/c/b;

    invoke-virtual {v0}, [Lcom/jscape/util/c/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/util/c/b;

    return-object v0
.end method
