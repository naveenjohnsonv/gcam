.class public Lcom/jscape/util/c/c;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/security/Provider;

.field private static final b:Ljava/security/Provider;

.field private static final c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x5

    const-string v4, "# \u0018.2\u0003+(\r\u00061(\u001d6P:7\u000e\u00119K\u0003g\u0013\u000f\u0000\'\u0006\u0000{\u0012\r\u0006p\u000f\u0012{\u0003O\u0013,\n\u0017a\u0002\u0004\u0011p\'\u000e}\u0008\u0002\u001a\u001d\u0004\u0012|\n\u0004)-\u0016\u0004X\u0014\u000e\u00157\u0001\u0004z\u0003 &\r\u001f\u000b\u00075K\u0015d\u0015O\u0016-\u0000$p\u0012\u0004\r:\u0000\u0005E\u0007\u0012\u0017;\u00172m\u0005\u0013\u0006*"

    const/16 v5, 0x6c

    move v7, v2

    const/4 v6, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x21

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0xd

    const-string v4, "mhX|n\u0007OLrN^.X"

    move v7, v2

    move v8, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x4b

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/util/c/c;->c:[Ljava/lang/String;

    sget-object v0, Lcom/jscape/util/c/b;->a:Lcom/jscape/util/c/b;

    iget-object v0, v0, Lcom/jscape/util/c/b;->c:Ljava/security/Provider;

    sput-object v0, Lcom/jscape/util/c/c;->a:Ljava/security/Provider;

    sget-object v0, Lcom/jscape/util/c/b;->b:Lcom/jscape/util/c/b;

    iget-object v0, v0, Lcom/jscape/util/c/b;->c:Ljava/security/Provider;

    sput-object v0, Lcom/jscape/util/c/c;->b:Ljava/security/Provider;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v1, v14, 0x7

    const/16 v16, 0x40

    if-eqz v1, :cond_8

    if-eq v1, v10, :cond_7

    const/4 v3, 0x2

    if-eq v1, v3, :cond_6

    const/4 v3, 0x3

    if-eq v1, v3, :cond_5

    const/4 v3, 0x4

    if-eq v1, v3, :cond_8

    if-eq v1, v2, :cond_4

    const/16 v16, 0x47

    goto :goto_4

    :cond_4
    const/16 v16, 0x29

    goto :goto_4

    :cond_5
    const/16 v16, 0x44

    goto :goto_4

    :cond_6
    const/16 v16, 0x7f

    goto :goto_4

    :cond_7
    const/16 v16, 0x42

    :cond_8
    :goto_4
    xor-int v1, v9, v16

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "C::",
            "Ljava/util/concurrent/Callable<",
            "TR;>;>(TC;)TR;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/c/d;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R::",
            "Ljava/lang/Runnable;",
            ">(TR;)TR;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/c/d;->a(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object p0

    return-object p0
.end method

.method private static a(Ljava/security/Provider;Z)Ljava/security/KeyStore;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    if-nez p0, :cond_0

    :try_start_0
    sget-object p0, Lcom/jscape/util/c/c;->c:[Ljava/lang/String;

    const/4 p1, 0x1

    aget-object p0, p0, p1

    invoke-static {p0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/c/c;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_0
    if-eqz p1, :cond_1

    :try_start_1
    sget-object p1, Lcom/jscape/util/c/c;->c:[Ljava/lang/String;

    const/4 v0, 0x0

    aget-object p1, p1, v0

    invoke-static {p1, p0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyStore;

    move-result-object p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/c/c;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_1
    sget-object p1, Lcom/jscape/util/c/c;->c:[Ljava/lang/String;

    const/4 v0, 0x2

    aget-object p1, p1, v0

    invoke-static {p1, p0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyStore;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static a(Z)Ljava/security/Provider;
    .locals 0

    if-eqz p0, :cond_0

    sget-object p0, Lcom/jscape/util/c/c;->b:Ljava/security/Provider;

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/jscape/util/c/c;->a:Ljava/security/Provider;

    :goto_0
    return-object p0
.end method

.method public static a()V
    .locals 0

    return-void
.end method

.method public static b(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "C::",
            "Ljava/util/concurrent/Callable<",
            "TR;>;>(TC;)TR;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/c/e;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static b(Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R::",
            "Ljava/lang/Runnable;",
            ">(TR;)TR;"
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/c/e;->a(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object p0

    return-object p0
.end method

.method public static b()Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/inet/ftp/FtpException;->c()I

    move-result v0

    sget-object v1, Lcom/jscape/util/c/c;->a:Ljava/security/Provider;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/security/Provider;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public static b(Z)Ljava/security/Provider;
    .locals 7

    invoke-static {}, Lcom/jscape/inet/ftp/FtpException;->c()I

    move-result v0

    :try_start_0
    sget-object v1, Lcom/jscape/util/c/c;->c:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz p0, :cond_0

    :try_start_1
    sget-object v2, Lcom/jscape/util/c/c;->b:Ljava/security/Provider;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_2
    invoke-static {p0}, Lcom/jscape/util/c/c;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_0
    sget-object v2, Lcom/jscape/util/c/c;->a:Ljava/security/Provider;

    :goto_0
    if-eqz v0, :cond_2

    if-eqz v2, :cond_1

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const-class v4, Ljava/security/Provider;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    invoke-virtual {v1, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    aput-object p0, v0, v5

    aput-object v2, v0, v6

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/security/Provider;

    return-object p0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object p0

    move-object v2, p0

    check-cast v2, Ljava/security/Provider;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    return-object v2

    :catch_1
    sget-object p0, Lcom/jscape/util/c/c;->c:[Ljava/lang/String;

    const/4 v0, 0x5

    aget-object v0, p0, v0

    const/4 v1, 0x6

    aget-object p0, p0, v1

    invoke-static {v0, p0}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    const/4 p0, 0x0

    return-object p0
.end method

.method public static c(Z)Ljava/security/KeyStore;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/inet/ftp/FtpException;->c()I

    move-result v0

    if-eqz p0, :cond_0

    :try_start_0
    sget-object v1, Lcom/jscape/util/c/c;->b:Ljava/security/Provider;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/c/c;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_0
    sget-object v1, Lcom/jscape/util/c/c;->a:Ljava/security/Provider;

    :goto_0
    invoke-static {v1, p0}, Lcom/jscape/util/c/c;->a(Ljava/security/Provider;Z)Ljava/security/KeyStore;

    move-result-object p0

    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {p0, v1, v1}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    if-nez v0, :cond_1

    const/4 v0, 0x3

    new-array v0, v0, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    return-object p0

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/c/c;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
.end method

.method public static c()Ljava/security/Provider;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jscape/util/c/c;->b:Ljava/security/Provider;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/jscape/util/c/c;->a:Ljava/security/Provider;

    :goto_0
    return-object v0
.end method

.method public static d(Z)Ljava/security/SecureRandom;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    if-eqz p0, :cond_0

    :try_start_0
    sget-object v0, Lcom/jscape/util/c/c;->b:Ljava/security/Provider;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/c/c;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_0
    sget-object v0, Lcom/jscape/util/c/c;->a:Ljava/security/Provider;

    :goto_0
    if-eqz p0, :cond_1

    if-eqz v0, :cond_1

    :try_start_1
    sget-object p0, Lcom/jscape/util/c/c;->c:[Ljava/lang/String;

    const/4 v1, 0x7

    aget-object p0, p0, v1

    invoke-static {p0, v0}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/SecureRandom;

    move-result-object p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/c/c;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_1
    new-instance p0, Ljava/security/SecureRandom;

    invoke-direct {p0}, Ljava/security/SecureRandom;-><init>()V

    :goto_1
    return-object p0
.end method

.method public static d()V
    .locals 0

    invoke-static {}, Lcom/jscape/util/c/d;->c()V

    return-void
.end method

.method public static e()Z
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/d;->b()Z

    move-result v0

    return v0
.end method

.method public static f()Ljava/security/Provider;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/jscape/util/c/c;->b(Z)Ljava/security/Provider;

    move-result-object v0

    return-object v0
.end method

.method public static g()Ljava/security/SecureRandom;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/c/c;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/jscape/util/c/c;->d(Z)Ljava/security/SecureRandom;

    move-result-object v0

    return-object v0
.end method

.method public static h()Z
    .locals 4

    invoke-static {}, Lcom/jscape/inet/ftp/FtpException;->b()I

    move-result v0

    const/4 v1, 0x0

    :try_start_0
    sget-object v2, Lcom/jscape/util/c/c;->c:[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    invoke-static {v2}, Ljavax/crypto/Cipher;->getMaxAllowedKeyLength(Ljava/lang/String;)I

    move-result v2
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    const v0, 0x7fffffff

    if-ne v2, v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v2

    :cond_1
    :goto_0
    return v1

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/util/c/c;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/util/c/c;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    return v1
.end method
