.class public Lcom/jscape/util/ar;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method public static a(Ljava/io/InputStream;Ljava/lang/String;)[C
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/ax;

    invoke-direct {v0, p1}, Lcom/jscape/util/ax;-><init>(Ljava/lang/String;)V

    new-instance p1, Ljava/lang/Thread;

    invoke-direct {p1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v1

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    const/16 p1, 0x80

    new-array v2, p1, [C

    const/4 v3, 0x0

    move-object v4, v2

    move v5, v3

    :cond_0
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v6

    const/16 v7, 0x20

    const/16 v8, 0xa

    const/4 v9, -0x1

    if-eq v6, v9, :cond_1

    if-eq v6, v8, :cond_1

    const/16 v10, 0xd

    if-eq v6, v10, :cond_2

    goto :goto_1

    :cond_1
    if-nez v1, :cond_6

    :cond_2
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v10

    if-eqz v1, :cond_7

    if-eq v10, v8, :cond_6

    if-eqz v1, :cond_7

    if-eq v10, v9, :cond_6

    if-eqz v1, :cond_3

    :try_start_0
    instance-of v8, p0, Ljava/io/PushbackInputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v8, :cond_3

    new-instance v8, Ljava/io/PushbackInputStream;

    invoke-direct {v8, p0}, Ljava/io/PushbackInputStream;-><init>(Ljava/io/InputStream;)V

    move-object p0, v8

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/ar;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_3
    :goto_0
    move-object v8, p0

    check-cast v8, Ljava/io/PushbackInputStream;

    invoke-virtual {v8, v10}, Ljava/io/PushbackInputStream;->unread(I)V

    :goto_1
    add-int/2addr p1, v9

    if-eqz v1, :cond_5

    if-gez p1, :cond_4

    add-int/lit16 p1, v5, 0x80

    new-array v2, p1, [C

    sub-int/2addr p1, v5

    add-int/lit8 p1, p1, -0x1

    invoke-static {v4, v3, v2, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v4, v7}, Ljava/util/Arrays;->fill([CC)V

    move-object v4, v2

    :cond_4
    add-int/lit8 v8, v5, 0x1

    int-to-char v6, v6

    aput-char v6, v2, v5

    move v5, v8

    :cond_5
    if-nez v1, :cond_0

    :cond_6
    invoke-virtual {v0}, Lcom/jscape/util/ax;->a()V

    move v10, v5

    :cond_7
    if-eqz v1, :cond_9

    if-nez v10, :cond_8

    const/4 p0, 0x0

    return-object p0

    :cond_8
    move v10, v5

    :cond_9
    new-array p0, v10, [C

    invoke-static {v2, v3, p0, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v2, v7}, Ljava/util/Arrays;->fill([CC)V

    return-object p0
.end method
