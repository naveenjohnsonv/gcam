.class public Lcom/jscape/util/Timeout;
.super Ljava/lang/Object;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private final a:J

.field private final b:J

.field private volatile c:Z

.field private d:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "~7jn\u0006q\\Y9{:RnPP#k`"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/Timeout;->e:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x61

    goto :goto_1

    :cond_1
    const/16 v4, 0x48

    goto :goto_1

    :cond_2
    const/16 v4, 0x22

    goto :goto_1

    :cond_3
    const/16 v4, 0x1e

    goto :goto_1

    :cond_4
    const/16 v4, 0x5e

    goto :goto_1

    :cond_5
    const/4 v4, 0x6

    goto :goto_1

    :cond_6
    const/16 v4, 0x6c

    :goto_1
    const/16 v5, 0x50

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(J)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/jscape/util/Timeout;->e:Ljava/lang/String;

    const-wide/16 v1, 0x0

    invoke-static {p1, p2, v1, v2, v0}, Lcom/jscape/util/w;->a(JJLjava/lang/String;)V

    iput-wide p1, p0, Lcom/jscape/util/Timeout;->a:J

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide p1

    iget-wide v0, p0, Lcom/jscape/util/Timeout;->a:J

    add-long/2addr p1, v0

    iput-wide p1, p0, Lcom/jscape/util/Timeout;->b:J

    return-void
.end method

.method private static a(Ljava/lang/InterruptedException;)Ljava/lang/InterruptedException;
    .locals 0

    return-object p0
.end method

.method public static waitUntilExceeded(J)V
    .locals 1

    new-instance v0, Lcom/jscape/util/Timeout;

    invoke-direct {v0, p0, p1}, Lcom/jscape/util/Timeout;-><init>(J)V

    invoke-virtual {v0}, Lcom/jscape/util/Timeout;->waitUntilExceeded()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/jscape/util/Timeout;->c:Z

    iget-object v1, p0, Lcom/jscape/util/Timeout;->d:Ljava/lang/Thread;

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_1
    return-void
.end method

.method public getValue()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/util/Timeout;->a:J

    return-wide v0
.end method

.method public isExceeded()Z
    .locals 5

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/jscape/util/Timeout;->b:J

    cmp-long v1, v1, v3

    if-nez v0, :cond_1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public remainedTime()J
    .locals 4

    iget-wide v0, p0, Lcom/jscape/util/Timeout;->b:J

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public declared-synchronized waitUntilExceeded()V
    .locals 6

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/util/Timeout;->d:Ljava/lang/Thread;

    :cond_0
    invoke-virtual {p0}, Lcom/jscape/util/Timeout;->isExceeded()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_4

    if-eqz v0, :cond_1

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/util/Timeout;->c:Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_4

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/util/Timeout;->a(Ljava/lang/InterruptedException;)Ljava/lang/InterruptedException;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/jscape/util/Timeout;->remainedTime()J

    move-result-wide v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-wide/16 v3, 0x0

    if-eqz v0, :cond_3

    cmp-long v5, v1, v3

    if-lez v5, :cond_2

    goto :goto_1

    :cond_2
    move-wide v1, v3

    :cond_3
    :goto_1
    :try_start_3
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_1
    if-nez v0, :cond_0

    :cond_4
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
