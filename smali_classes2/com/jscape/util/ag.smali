.class public Lcom/jscape/util/ag;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/aj;


# instance fields
.field private final a:C

.field private final b:C

.field private final c:[Ljava/lang/Object;

.field private final d:Ljava/lang/StringBuffer;

.field private e:Lcom/jscape/util/aj;


# direct methods
.method public constructor <init>(CC[Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-char p1, p0, Lcom/jscape/util/ag;->a:C

    iput-char p2, p0, Lcom/jscape/util/ag;->b:C

    iput-object p3, p0, Lcom/jscape/util/ag;->c:[Ljava/lang/Object;

    new-instance p1, Ljava/lang/StringBuffer;

    invoke-direct {p1}, Ljava/lang/StringBuffer;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/ag;->d:Ljava/lang/StringBuffer;

    return-void
.end method


# virtual methods
.method public a(CLjava/lang/StringBuffer;)Lcom/jscape/util/aj;
    .locals 2

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    iget-char v1, p0, Lcom/jscape/util/ag;->a:C

    if-nez v0, :cond_2

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/jscape/util/ag;->d:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    invoke-virtual {p2, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :goto_0
    iget-object p1, p0, Lcom/jscape/util/ag;->e:Lcom/jscape/util/aj;

    return-object p1

    :cond_0
    move v1, p1

    :cond_1
    iget-char v0, p0, Lcom/jscape/util/ag;->b:C

    goto :goto_1

    :cond_2
    move v0, v1

    move v1, p1

    :goto_1
    if-ne v1, v0, :cond_3

    iget-object p1, p0, Lcom/jscape/util/ag;->d:Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/util/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object p1, p0, Lcom/jscape/util/ag;->d:Ljava/lang/StringBuffer;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/lang/StringBuffer;->setLength(I)V

    goto :goto_0

    :cond_3
    iget-object p2, p0, Lcom/jscape/util/ag;->d:Ljava/lang/StringBuffer;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    return-object p0
.end method

.method protected a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/ag;->c:[Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    aget-object p1, v0, p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a(Lcom/jscape/util/aj;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/util/ag;->e:Lcom/jscape/util/aj;

    return-void
.end method
