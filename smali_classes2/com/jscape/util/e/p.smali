.class public Lcom/jscape/util/e/p;
.super Lcom/jscape/util/g/z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/util/g/z<",
        "Ljava/io/File;",
        "Ljava/util/Collection<",
        "Ljava/io/File;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/io/FileFilter;


# direct methods
.method public constructor <init>(Ljava/io/FileFilter;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/util/g/z;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/e/p;->a:Ljava/io/FileFilter;

    return-void
.end method


# virtual methods
.method public a(Ljava/io/File;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Collection<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/e/p;->a:Ljava/io/FileFilter;

    invoke-static {p1, v0}, Lcom/jscape/util/Q;->a(Ljava/io/File;Ljava/io/FileFilter;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/io/File;

    invoke-virtual {p0, p1}, Lcom/jscape/util/e/p;->a(Ljava/io/File;)Ljava/util/Collection;

    move-result-object p1

    return-object p1
.end method
