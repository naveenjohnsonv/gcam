.class public abstract enum Lcom/jscape/util/e/a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/util/e/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/jscape/util/e/a;

.field public static final enum b:Lcom/jscape/util/e/a;

.field public static final enum c:Lcom/jscape/util/e/a;

.field public static final enum d:Lcom/jscape/util/e/a;

.field public static final enum e:Lcom/jscape/util/e/a;

.field public static final enum f:Lcom/jscape/util/e/a;

.field public static final enum g:Lcom/jscape/util/e/a;

.field public static final enum h:Lcom/jscape/util/e/a;

.field public static final enum i:Lcom/jscape/util/e/a;

.field public static final enum j:Lcom/jscape/util/e/a;

.field public static final enum k:Lcom/jscape/util/e/a;

.field public static final enum l:Lcom/jscape/util/e/a;

.field private static final n:[Lcom/jscape/util/e/a;

.field private static final o:[Ljava/lang/String;


# instance fields
.field public final m:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x7

    const/4 v3, 0x0

    const-string v4, "\u000e;m\u001dS\u0011n\u0007\u000e;m\u0018S\u0011n\u0007\u000e;m\u001dI\u0016t\u0012x\u0007\u0001,#!\u0019>AGjeg_x\u0007\u0001,\u0007\u000e;m\u0017S\u0011n\u0007\u000e;m\u0017A\u0010l\u0007\u000e;m\u001cA\u000bx\u0007\u000e;m\u001dA\u0010l\u0007\u000e;m\u0017I\u0016t\u0007\u000e;m\u001cP\u0016d\u0007\u000e;m\u0018A\u0010l"

    const/16 v5, 0x62

    move v7, v2

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x59

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v1, 0x2

    const/4 v15, 0x4

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v1, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v1

    goto :goto_0

    :cond_0
    const/16 v5, 0xf

    const-string v4, "yL\u001ao>a\u0003\u0007yL\u001ak$|\u000f"

    move v8, v1

    move v7, v2

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v1

    move v8, v11

    :goto_3
    const/16 v9, 0x2e

    add-int/2addr v6, v10

    add-int v1, v6, v7

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/util/e/a;->o:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/util/e/b;

    sget-object v4, Lcom/jscape/util/e/a;->o:[Ljava/lang/String;

    aget-object v5, v4, v3

    const/16 v6, 0x100

    invoke-direct {v0, v5, v3, v6}, Lcom/jscape/util/e/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/jscape/util/e/a;->a:Lcom/jscape/util/e/a;

    new-instance v0, Lcom/jscape/util/e/f;

    aget-object v5, v4, v10

    const/16 v6, 0x80

    invoke-direct {v0, v5, v10, v6}, Lcom/jscape/util/e/f;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/jscape/util/e/a;->b:Lcom/jscape/util/e/a;

    new-instance v0, Lcom/jscape/util/e/g;

    aget-object v5, v4, v15

    const/16 v6, 0x40

    invoke-direct {v0, v5, v1, v6}, Lcom/jscape/util/e/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/jscape/util/e/a;->c:Lcom/jscape/util/e/a;

    new-instance v0, Lcom/jscape/util/e/h;

    aget-object v5, v4, v2

    const/16 v6, 0x20

    const/4 v7, 0x3

    invoke-direct {v0, v5, v7, v6}, Lcom/jscape/util/e/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/jscape/util/e/a;->d:Lcom/jscape/util/e/a;

    new-instance v0, Lcom/jscape/util/e/i;

    const/16 v5, 0xa

    aget-object v6, v4, v5

    const/16 v7, 0x10

    invoke-direct {v0, v6, v15, v7}, Lcom/jscape/util/e/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/jscape/util/e/a;->e:Lcom/jscape/util/e/a;

    new-instance v0, Lcom/jscape/util/e/j;

    const/4 v6, 0x5

    aget-object v7, v4, v6

    const/16 v8, 0x8

    invoke-direct {v0, v7, v6, v8}, Lcom/jscape/util/e/j;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/jscape/util/e/a;->f:Lcom/jscape/util/e/a;

    new-instance v0, Lcom/jscape/util/e/k;

    aget-object v6, v4, v1

    const/4 v7, 0x6

    invoke-direct {v0, v6, v7, v15}, Lcom/jscape/util/e/k;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/jscape/util/e/a;->g:Lcom/jscape/util/e/a;

    new-instance v0, Lcom/jscape/util/e/l;

    const/16 v6, 0xb

    aget-object v9, v4, v6

    invoke-direct {v0, v9, v2, v1}, Lcom/jscape/util/e/l;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/jscape/util/e/a;->h:Lcom/jscape/util/e/a;

    new-instance v0, Lcom/jscape/util/e/m;

    aget-object v9, v4, v8

    invoke-direct {v0, v9, v8, v10}, Lcom/jscape/util/e/m;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/jscape/util/e/a;->i:Lcom/jscape/util/e/a;

    new-instance v0, Lcom/jscape/util/e/c;

    const/16 v9, 0xc

    aget-object v11, v4, v9

    const/16 v12, 0x800

    const/16 v13, 0x9

    invoke-direct {v0, v11, v13, v12}, Lcom/jscape/util/e/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/jscape/util/e/a;->j:Lcom/jscape/util/e/a;

    new-instance v0, Lcom/jscape/util/e/d;

    aget-object v11, v4, v7

    const/16 v12, 0x400

    invoke-direct {v0, v11, v5, v12}, Lcom/jscape/util/e/d;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/jscape/util/e/a;->k:Lcom/jscape/util/e/a;

    new-instance v0, Lcom/jscape/util/e/e;

    aget-object v4, v4, v13

    const/16 v11, 0x200

    invoke-direct {v0, v4, v6, v11}, Lcom/jscape/util/e/e;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/jscape/util/e/a;->l:Lcom/jscape/util/e/a;

    new-array v4, v9, [Lcom/jscape/util/e/a;

    sget-object v9, Lcom/jscape/util/e/a;->a:Lcom/jscape/util/e/a;

    aput-object v9, v4, v3

    sget-object v3, Lcom/jscape/util/e/a;->b:Lcom/jscape/util/e/a;

    aput-object v3, v4, v10

    sget-object v3, Lcom/jscape/util/e/a;->c:Lcom/jscape/util/e/a;

    aput-object v3, v4, v1

    sget-object v1, Lcom/jscape/util/e/a;->d:Lcom/jscape/util/e/a;

    const/4 v3, 0x3

    aput-object v1, v4, v3

    sget-object v1, Lcom/jscape/util/e/a;->e:Lcom/jscape/util/e/a;

    aput-object v1, v4, v15

    sget-object v1, Lcom/jscape/util/e/a;->f:Lcom/jscape/util/e/a;

    const/4 v3, 0x5

    aput-object v1, v4, v3

    sget-object v1, Lcom/jscape/util/e/a;->g:Lcom/jscape/util/e/a;

    aput-object v1, v4, v7

    sget-object v1, Lcom/jscape/util/e/a;->h:Lcom/jscape/util/e/a;

    aput-object v1, v4, v2

    sget-object v1, Lcom/jscape/util/e/a;->i:Lcom/jscape/util/e/a;

    aput-object v1, v4, v8

    sget-object v1, Lcom/jscape/util/e/a;->j:Lcom/jscape/util/e/a;

    aput-object v1, v4, v13

    sget-object v1, Lcom/jscape/util/e/a;->k:Lcom/jscape/util/e/a;

    aput-object v1, v4, v5

    aput-object v0, v4, v6

    sput-object v4, Lcom/jscape/util/e/a;->n:[Lcom/jscape/util/e/a;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    if-eq v2, v1, :cond_7

    const/4 v1, 0x3

    if-eq v2, v1, :cond_6

    if-eq v2, v15, :cond_5

    const/4 v1, 0x5

    if-eq v2, v1, :cond_4

    const/16 v15, 0x65

    goto :goto_4

    :cond_4
    const/16 v15, 0x1b

    goto :goto_4

    :cond_5
    const/16 v15, 0x5f

    goto :goto_4

    :cond_6
    const/16 v15, 0x16

    goto :goto_4

    :cond_7
    const/16 v15, 0x7d

    goto :goto_4

    :cond_8
    const/16 v15, 0x3d

    :cond_9
    :goto_4
    xor-int v1, v9, v15

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v2, 0x7

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/jscape/util/e/a;->m:I

    return-void
.end method

.method constructor <init>(Ljava/lang/String;IILcom/jscape/util/e/b;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/util/e/a;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method private static a(Z)C
    .locals 1

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    if-eqz p0, :cond_0

    const/16 p0, 0x72

    goto :goto_0

    :cond_0
    const/16 p0, 0x2d

    :cond_1
    :goto_0
    return p0
.end method

.method public static a(Lcom/jscape/util/e/PosixFilePermissions;)I
    .locals 6

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/e/a;->a()[Lcom/jscape/util/e/a;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    move v4, v3

    :cond_0
    if-ge v3, v2, :cond_1

    aget-object v5, v1, v3

    invoke-virtual {v5, p0, v4}, Lcom/jscape/util/e/a;->a(Lcom/jscape/util/e/PosixFilePermissions;I)I

    move-result v4

    if-nez v0, :cond_1

    add-int/lit8 v3, v3, 0x1

    if-eqz v0, :cond_0

    :cond_1
    return v4
.end method

.method public static a(I)Lcom/jscape/util/e/PosixFilePermissions;
    .locals 14

    new-instance v13, Lcom/jscape/util/e/PosixFilePermissions;

    sget-object v0, Lcom/jscape/util/e/a;->a:Lcom/jscape/util/e/a;

    invoke-virtual {v0, p0}, Lcom/jscape/util/e/a;->b(I)Z

    move-result v1

    sget-object v0, Lcom/jscape/util/e/a;->b:Lcom/jscape/util/e/a;

    invoke-virtual {v0, p0}, Lcom/jscape/util/e/a;->b(I)Z

    move-result v2

    sget-object v0, Lcom/jscape/util/e/a;->c:Lcom/jscape/util/e/a;

    invoke-virtual {v0, p0}, Lcom/jscape/util/e/a;->b(I)Z

    move-result v3

    sget-object v0, Lcom/jscape/util/e/a;->d:Lcom/jscape/util/e/a;

    invoke-virtual {v0, p0}, Lcom/jscape/util/e/a;->b(I)Z

    move-result v4

    sget-object v0, Lcom/jscape/util/e/a;->e:Lcom/jscape/util/e/a;

    invoke-virtual {v0, p0}, Lcom/jscape/util/e/a;->b(I)Z

    move-result v5

    sget-object v0, Lcom/jscape/util/e/a;->f:Lcom/jscape/util/e/a;

    invoke-virtual {v0, p0}, Lcom/jscape/util/e/a;->b(I)Z

    move-result v6

    sget-object v0, Lcom/jscape/util/e/a;->g:Lcom/jscape/util/e/a;

    invoke-virtual {v0, p0}, Lcom/jscape/util/e/a;->b(I)Z

    move-result v7

    sget-object v0, Lcom/jscape/util/e/a;->h:Lcom/jscape/util/e/a;

    invoke-virtual {v0, p0}, Lcom/jscape/util/e/a;->b(I)Z

    move-result v8

    sget-object v0, Lcom/jscape/util/e/a;->i:Lcom/jscape/util/e/a;

    invoke-virtual {v0, p0}, Lcom/jscape/util/e/a;->b(I)Z

    move-result v9

    sget-object v0, Lcom/jscape/util/e/a;->j:Lcom/jscape/util/e/a;

    invoke-virtual {v0, p0}, Lcom/jscape/util/e/a;->b(I)Z

    move-result v10

    sget-object v0, Lcom/jscape/util/e/a;->k:Lcom/jscape/util/e/a;

    invoke-virtual {v0, p0}, Lcom/jscape/util/e/a;->b(I)Z

    move-result v11

    sget-object v0, Lcom/jscape/util/e/a;->l:Lcom/jscape/util/e/a;

    invoke-virtual {v0, p0}, Lcom/jscape/util/e/a;->b(I)Z

    move-result v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/jscape/util/e/PosixFilePermissions;-><init>(ZZZZZZZZZZZZ)V

    return-object v13
.end method

.method public static a(Ljava/lang/String;I)Lcom/jscape/util/e/PosixFilePermissions;
    .locals 14

    new-instance v13, Lcom/jscape/util/e/PosixFilePermissions;

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/jscape/util/e/a;->a(C)Z

    move-result v1

    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/jscape/util/e/a;->b(C)Z

    move-result v2

    add-int/lit8 v0, p1, 0x2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/jscape/util/e/a;->c(C)Z

    move-result v3

    add-int/lit8 v0, p1, 0x3

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/jscape/util/e/a;->a(C)Z

    move-result v4

    add-int/lit8 v0, p1, 0x4

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/jscape/util/e/a;->b(C)Z

    move-result v5

    add-int/lit8 v0, p1, 0x5

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/jscape/util/e/a;->c(C)Z

    move-result v6

    add-int/lit8 v0, p1, 0x6

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/jscape/util/e/a;->a(C)Z

    move-result v7

    add-int/lit8 v0, p1, 0x7

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/jscape/util/e/a;->b(C)Z

    move-result v8

    add-int/lit8 p1, p1, 0x8

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result p0

    invoke-static {p0}, Lcom/jscape/util/e/a;->c(C)Z

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/jscape/util/e/PosixFilePermissions;-><init>(ZZZZZZZZZZZZ)V

    return-object v13
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/util/e/a;
    .locals 1

    const-class v0, Lcom/jscape/util/e/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/util/e/a;

    return-object p0
.end method

.method private static a(C)Z
    .locals 1

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/16 v0, 0x72

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :cond_1
    :goto_0
    return p0
.end method

.method public static a()[Lcom/jscape/util/e/a;
    .locals 1

    sget-object v0, Lcom/jscape/util/e/a;->n:[Lcom/jscape/util/e/a;

    invoke-virtual {v0}, [Lcom/jscape/util/e/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/util/e/a;

    return-object v0
.end method

.method private static b(Z)C
    .locals 1

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    if-eqz p0, :cond_0

    const/16 p0, 0x77

    goto :goto_0

    :cond_0
    const/16 p0, 0x2d

    :cond_1
    :goto_0
    return p0
.end method

.method public static b(Lcom/jscape/util/e/PosixFilePermissions;)Ljava/lang/String;
    .locals 5

    sget-object v0, Lcom/jscape/util/e/a;->o:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/Object;

    iget-boolean v3, p0, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanRead:Z

    invoke-static {v3}, Lcom/jscape/util/e/a;->a(Z)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-boolean v3, p0, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanWrite:Z

    invoke-static {v3}, Lcom/jscape/util/e/a;->b(Z)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    iget-boolean v3, p0, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanExecute:Z

    invoke-static {v3}, Lcom/jscape/util/e/a;->c(Z)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    iget-boolean v3, p0, Lcom/jscape/util/e/PosixFilePermissions;->groupCanRead:Z

    invoke-static {v3}, Lcom/jscape/util/e/a;->a(Z)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    aput-object v3, v2, v1

    iget-boolean v1, p0, Lcom/jscape/util/e/PosixFilePermissions;->groupCanWrite:Z

    invoke-static {v1}, Lcom/jscape/util/e/a;->b(Z)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    const/4 v3, 0x4

    aput-object v1, v2, v3

    iget-boolean v1, p0, Lcom/jscape/util/e/PosixFilePermissions;->groupCanExecute:Z

    invoke-static {v1}, Lcom/jscape/util/e/a;->c(Z)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    const/4 v3, 0x5

    aput-object v1, v2, v3

    iget-boolean v1, p0, Lcom/jscape/util/e/PosixFilePermissions;->otherCanRead:Z

    invoke-static {v1}, Lcom/jscape/util/e/a;->a(Z)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    const/4 v3, 0x6

    aput-object v1, v2, v3

    iget-boolean v1, p0, Lcom/jscape/util/e/PosixFilePermissions;->otherCanWrite:Z

    invoke-static {v1}, Lcom/jscape/util/e/a;->b(Z)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    const/4 v3, 0x7

    aput-object v1, v2, v3

    iget-boolean p0, p0, Lcom/jscape/util/e/PosixFilePermissions;->otherCanExecute:Z

    invoke-static {p0}, Lcom/jscape/util/e/a;->c(Z)C

    move-result p0

    invoke-static {p0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object p0

    const/16 v1, 0x8

    aput-object p0, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static b(C)Z
    .locals 1

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/16 v0, 0x77

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :cond_1
    :goto_0
    return p0
.end method

.method private static c(Z)C
    .locals 1

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    if-eqz p0, :cond_0

    const/16 p0, 0x78

    goto :goto_0

    :cond_0
    const/16 p0, 0x2d

    :cond_1
    :goto_0
    return p0
.end method

.method private static c(C)Z
    .locals 1

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/16 v0, 0x78

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :cond_1
    :goto_0
    return p0
.end method


# virtual methods
.method public abstract a(Lcom/jscape/util/e/PosixFilePermissions;I)I
.end method

.method public a(ZI)I
    .locals 1

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    iget p1, p0, Lcom/jscape/util/e/a;->m:I

    or-int/2addr p1, p2

    :cond_0
    move p2, p1

    :cond_1
    return p2
.end method

.method public b(I)Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/util/e/a;->m:I

    and-int/2addr p1, v1

    if-nez v0, :cond_1

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    return p1
.end method
