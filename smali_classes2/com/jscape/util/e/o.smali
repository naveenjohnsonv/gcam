.class public Lcom/jscape/util/e/o;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/util/Locale;

.field private static final b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;

.field private static final e:Lcom/jscape/util/Time;

.field private static final f:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x3

    const-string v4, "\n=\u001f\u0003\n=\u001f\u000c\u0000=\u0006?t94\u00058qr}\u000c\u0000=\u0006?t94\u00058qr}\u000b\u0000=\u0006?t944\t2f.h\u0013n|5>1.U(:sxwh\u0013n|5>4hPx{0x9u\u0003k:=egmUk\'t}1>Pnl\u0008h\u0003k:c}1>"

    const/16 v5, 0x65

    move v7, v2

    const/4 v6, -0x1

    const/4 v8, 0x0

    :goto_0
    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v7

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x5

    move v13, v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v14, v10

    const/4 v15, 0x0

    :goto_2
    const/4 v1, 0x4

    if-gt v14, v15, :cond_3

    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v13}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v12, :cond_1

    add-int/lit8 v1, v8, 0x1

    aput-object v10, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v1

    goto :goto_0

    :cond_0
    const/16 v5, 0xf

    const/16 v4, 0xb

    const-string v6, "$\u0019\"\u001bP\u001d\u0010\u0010-\u0016B\u00035\'D"

    move v8, v1

    move v7, v4

    move-object v4, v6

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v8, 0x1

    aput-object v10, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v1

    move v8, v12

    :goto_3
    const/16 v13, 0x21

    add-int/2addr v6, v9

    add-int v1, v6, v7

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/util/e/o;->f:[Ljava/lang/String;

    aget-object v3, v0, v1

    sput-object v3, Lcom/jscape/util/e/o;->c:Ljava/lang/String;

    aget-object v1, v0, v1

    sput-object v1, Lcom/jscape/util/e/o;->d:Ljava/lang/String;

    aget-object v0, v0, v2

    sput-object v0, Lcom/jscape/util/e/o;->b:Ljava/lang/String;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    sput-object v0, Lcom/jscape/util/e/o;->a:Ljava/util/Locale;

    const-wide/16 v0, 0xb7

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->days(J)Lcom/jscape/util/Time;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/e/o;->e:Lcom/jscape/util/Time;

    return-void

    :cond_3
    aget-char v16, v10, v15

    rem-int/lit8 v3, v15, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v9, :cond_8

    const/4 v9, 0x2

    if-eq v3, v9, :cond_7

    if-eq v3, v2, :cond_6

    if-eq v3, v1, :cond_5

    if-eq v3, v11, :cond_4

    const/16 v1, 0x11

    goto :goto_4

    :cond_4
    const/16 v1, 0x58

    goto :goto_4

    :cond_5
    const/16 v1, 0x15

    goto :goto_4

    :cond_6
    const/16 v1, 0x1a

    goto :goto_4

    :cond_7
    const/16 v1, 0x4e

    goto :goto_4

    :cond_8
    const/16 v1, 0x75

    goto :goto_4

    :cond_9
    const/16 v1, 0x48

    :goto_4
    xor-int/2addr v1, v13

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v10, v15

    add-int/lit8 v15, v15, 0x1

    const/4 v9, 0x1

    goto/16 :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Z)C
    .locals 1

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    if-eqz p0, :cond_0

    const/16 p0, 0x72

    goto :goto_0

    :cond_0
    const/16 p0, 0x2d

    :cond_1
    :goto_0
    return p0
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/util/e/n;
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    new-instance v1, Ljava/util/Scanner;

    move-object/from16 v2, p0

    invoke-direct {v1, v2}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/jscape/util/e/o;->f:[Ljava/lang/String;

    const/16 v3, 0x8

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v6

    new-instance v20, Lcom/jscape/util/e/PosixFilePermissions;

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v7}, Lcom/jscape/util/e/o;->a(C)Z

    move-result v8

    const/4 v15, 0x2

    invoke-virtual {v2, v15}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v7}, Lcom/jscape/util/e/o;->b(C)Z

    move-result v9

    const/4 v14, 0x3

    invoke-virtual {v2, v14}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v7}, Lcom/jscape/util/e/o;->c(C)Z

    move-result v10

    const/4 v13, 0x4

    invoke-virtual {v2, v13}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v7}, Lcom/jscape/util/e/o;->a(C)Z

    move-result v11

    const/4 v7, 0x5

    invoke-virtual {v2, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v7}, Lcom/jscape/util/e/o;->b(C)Z

    move-result v12

    const/4 v7, 0x6

    invoke-virtual {v2, v7}, Ljava/lang/String;->charAt(I)C

    move-result v16

    invoke-static/range {v16 .. v16}, Lcom/jscape/util/e/o;->c(C)Z

    move-result v16

    const/4 v7, 0x7

    invoke-virtual {v2, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v7}, Lcom/jscape/util/e/o;->a(C)Z

    move-result v17

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Lcom/jscape/util/e/o;->b(C)Z

    move-result v3

    const/16 v7, 0x9

    invoke-virtual {v2, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/jscape/util/e/o;->c(C)Z

    move-result v2

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x6

    move-object/from16 v7, v20

    move/from16 v13, v16

    move v5, v14

    move/from16 v14, v17

    move/from16 v23, v15

    move v15, v3

    move/from16 v16, v2

    move/from16 v17, v18

    move/from16 v18, v19

    move/from16 v19, v21

    invoke-direct/range {v7 .. v19}, Lcom/jscape/util/e/PosixFilePermissions;-><init>(ZZZZZZZZZZZZ)V

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/util/Scanner;->nextInt(I)I

    move-result v8

    invoke-virtual {v1}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/jscape/util/e/o;->b(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    if-eqz v7, :cond_0

    move-object v10, v2

    goto :goto_0

    :cond_0
    const-string v10, ""

    :goto_0
    if-nez v0, :cond_2

    if-eqz v7, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {v2}, Lcom/jscape/util/e/o;->b(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_2

    :cond_2
    :goto_1
    move-object v2, v7

    :goto_2
    :try_start_1
    sget-object v11, Lcom/jscape/util/e/o;->f:[Ljava/lang/String;

    aget-object v11, v11, v22

    new-array v5, v5, [Ljava/lang/Object;

    if-eqz v7, :cond_3

    invoke-virtual {v1}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    :try_start_2
    aput-object v3, v5, v4

    invoke-virtual {v1}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v5, v4

    invoke-virtual {v1}, Ljava/util/Scanner;->next()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v23

    invoke-static {v11, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/jscape/util/e/o;->c(Ljava/lang/String;)J

    move-result-wide v13

    invoke-virtual {v1}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :try_start_3
    new-instance v1, Lcom/jscape/util/e/n;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_3

    :cond_4
    const-wide/16 v2, 0x0

    :goto_3
    move-wide v11, v2

    move-object v5, v1

    move-object/from16 v7, v20

    :try_start_4
    invoke-direct/range {v5 .. v15}, Lcom/jscape/util/e/n;-><init>(CLcom/jscape/util/e/PosixFilePermissions;ILjava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    if-eqz v0, :cond_5

    const/4 v0, 0x4

    new-array v0, v0, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_5
    return-object v1

    :catch_0
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/util/e/o;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/e/o;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private static a(J)Ljava/lang/String;
    .locals 4

    :try_start_0
    invoke-static {p0, p1}, Lcom/jscape/util/D;->e(J)J

    move-result-wide v0

    sget-object v2, Lcom/jscape/util/e/o;->e:Lcom/jscape/util/Time;

    invoke-virtual {v2}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/jscape/util/e/o;->f:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    sget-object v2, Lcom/jscape/util/e/o;->a:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/jscape/util/e/o;->f:[Ljava/lang/String;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    sget-object v2, Lcom/jscape/util/e/o;->a:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    :goto_0
    sget-object v1, Lcom/jscape/util/e/o;->f:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/e/o;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
.end method

.method public static a(Lcom/jscape/util/e/n;)Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    :try_start_0
    sget-object v0, Lcom/jscape/util/e/o;->f:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    const/16 v2, 0x10

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-char v4, p0, Lcom/jscape/util/e/n;->a:C

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/jscape/util/e/n;->b:Lcom/jscape/util/e/PosixFilePermissions;

    iget-boolean v4, v4, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanRead:Z

    invoke-static {v4}, Lcom/jscape/util/e/o;->a(Z)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/jscape/util/e/n;->b:Lcom/jscape/util/e/PosixFilePermissions;

    iget-boolean v4, v4, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanWrite:Z

    invoke-static {v4}, Lcom/jscape/util/e/o;->b(Z)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/jscape/util/e/n;->b:Lcom/jscape/util/e/PosixFilePermissions;

    iget-boolean v4, v4, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanExecute:Z

    invoke-static {v4}, Lcom/jscape/util/e/o;->c(Z)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lcom/jscape/util/e/n;->b:Lcom/jscape/util/e/PosixFilePermissions;

    iget-boolean v4, v4, Lcom/jscape/util/e/PosixFilePermissions;->groupCanRead:Z

    invoke-static {v4}, Lcom/jscape/util/e/o;->a(Z)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    aput-object v4, v2, v3

    iget-object v3, p0, Lcom/jscape/util/e/n;->b:Lcom/jscape/util/e/PosixFilePermissions;

    iget-boolean v3, v3, Lcom/jscape/util/e/PosixFilePermissions;->groupCanWrite:Z

    invoke-static {v3}, Lcom/jscape/util/e/o;->b(Z)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x6

    iget-object v3, p0, Lcom/jscape/util/e/n;->b:Lcom/jscape/util/e/PosixFilePermissions;

    iget-boolean v3, v3, Lcom/jscape/util/e/PosixFilePermissions;->groupCanExecute:Z

    invoke-static {v3}, Lcom/jscape/util/e/o;->c(Z)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x7

    iget-object v3, p0, Lcom/jscape/util/e/n;->b:Lcom/jscape/util/e/PosixFilePermissions;

    iget-boolean v3, v3, Lcom/jscape/util/e/PosixFilePermissions;->otherCanRead:Z

    invoke-static {v3}, Lcom/jscape/util/e/o;->a(Z)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    aput-object v3, v2, v1

    const/16 v1, 0x8

    iget-object v3, p0, Lcom/jscape/util/e/n;->b:Lcom/jscape/util/e/PosixFilePermissions;

    iget-boolean v3, v3, Lcom/jscape/util/e/PosixFilePermissions;->otherCanWrite:Z

    invoke-static {v3}, Lcom/jscape/util/e/o;->b(Z)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    aput-object v3, v2, v1

    const/16 v1, 0x9

    iget-object v3, p0, Lcom/jscape/util/e/n;->b:Lcom/jscape/util/e/PosixFilePermissions;

    iget-boolean v3, v3, Lcom/jscape/util/e/PosixFilePermissions;->otherCanExecute:Z

    invoke-static {v3}, Lcom/jscape/util/e/o;->c(Z)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    aput-object v3, v2, v1

    const/16 v1, 0xa

    iget v3, p0, Lcom/jscape/util/e/n;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    const/16 v1, 0xb

    iget-object v3, p0, Lcom/jscape/util/e/n;->d:Ljava/lang/String;

    aput-object v3, v2, v1

    const/16 v1, 0xc

    iget-object v3, p0, Lcom/jscape/util/e/n;->e:Ljava/lang/String;

    aput-object v3, v2, v1

    const/16 v1, 0xd

    iget-wide v3, p0, Lcom/jscape/util/e/n;->f:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v1

    const/16 v1, 0xe

    iget-wide v3, p0, Lcom/jscape/util/e/n;->g:J

    invoke-static {v3, v4}, Lcom/jscape/util/e/o;->a(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    const/16 v1, 0xf

    iget-object p0, p0, Lcom/jscape/util/e/n;->h:Ljava/lang/String;

    aput-object p0, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "D0jjL"

    invoke-static {v0}, Lcom/jscape/util/e/PosixFilePermissions;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-object p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/e/o;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
.end method

.method private static a(Ljava/text/DateFormat;Ljava/lang/String;)Ljava/util/Date;
    .locals 0

    :try_start_0
    invoke-virtual {p0, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static a(C)Z
    .locals 1

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/16 v0, 0x72

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :cond_1
    :goto_0
    return p0
.end method

.method private static b(Z)C
    .locals 1

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    if-eqz p0, :cond_0

    const/16 p0, 0x77

    goto :goto_0

    :cond_0
    const/16 p0, 0x2d

    :cond_1
    :goto_0
    return p0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/Long;
    .locals 0

    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static b(C)Z
    .locals 1

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/16 v0, 0x77

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :cond_1
    :goto_0
    return p0
.end method

.method private static c(Z)C
    .locals 1

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    if-eqz p0, :cond_0

    const/16 p0, 0x78

    goto :goto_0

    :cond_0
    const/16 p0, 0x2d

    :cond_1
    :goto_0
    return p0
.end method

.method private static c(Ljava/lang/String;)J
    .locals 6

    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/jscape/util/e/o;->f:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    sget-object v3, Lcom/jscape/util/e/o;->a:Ljava/util/Locale;

    invoke-direct {v0, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v1, v1, v3

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    invoke-static {v0, p0}, Lcom/jscape/util/e/o;->a(Ljava/text/DateFormat;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-virtual {v1, v4, v5}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    if-eqz v2, :cond_1

    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/jscape/util/e/o;->f:[Ljava/lang/String;

    const/4 v4, 0x4

    aget-object v4, v1, v4

    sget-object v5, Lcom/jscape/util/e/o;->a:Ljava/util/Locale;

    invoke-direct {v0, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    aget-object v1, v1, v3

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    invoke-static {v0, p0}, Lcom/jscape/util/e/o;->a(Ljava/text/DateFormat;Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    :cond_1
    if-nez v2, :cond_3

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    :goto_1
    return-wide v0
.end method

.method private static c(C)Z
    .locals 1

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/16 v0, 0x78

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :cond_1
    :goto_0
    return p0
.end method
