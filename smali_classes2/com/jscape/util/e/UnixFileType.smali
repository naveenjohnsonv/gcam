.class public final enum Lcom/jscape/util/e/UnixFileType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/util/e/UnixFileType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/jscape/util/e/UnixFileType;

.field public static final enum BLOCK_DEVICE:Lcom/jscape/util/e/UnixFileType;

.field public static final enum CHARACTER_DEVICE:Lcom/jscape/util/e/UnixFileType;

.field public static final enum DIRECTORY:Lcom/jscape/util/e/UnixFileType;

.field public static final enum DOOR:Lcom/jscape/util/e/UnixFileType;

.field public static final enum NAMED_PIPE:Lcom/jscape/util/e/UnixFileType;

.field public static final enum REGULAR:Lcom/jscape/util/e/UnixFileType;

.field public static final enum SOCKET:Lcom/jscape/util/e/UnixFileType;

.field public static final enum SYMBOLIC_LINK:Lcom/jscape/util/e/UnixFileType;

.field private static final TYPE_MASK:I = 0xf000


# instance fields
.field public final lsCode:C

.field public final mask:I


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const/16 v0, 0x8

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "F8EI7G~V>DB6@\u0010V)IY9HcP3WO=]~V$\tQ(ZN;_xG8\n[ EN<Tg\\1M\u0006F.K@=_\u0007G$O^4Je"

    const/16 v5, 0x42

    const/16 v6, 0xd

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x5a

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x5

    const/4 v0, 0x2

    const/4 v2, 0x4

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v0, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v0

    const/16 v0, 0x8

    goto :goto_0

    :cond_0
    const/16 v5, 0x11

    const-string v4, "J5\\B\u000cL6\\S(OhK,ZS&"

    move v8, v0

    move v6, v2

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v6, v0

    move v8, v11

    :goto_3
    const/16 v9, 0x41

    add-int/2addr v7, v10

    add-int v0, v7, v6

    invoke-virtual {v4, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    const/16 v0, 0x8

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/jscape/util/e/UnixFileType;

    aget-object v5, v1, v15

    const/16 v6, 0x2d

    const v7, 0x8000

    invoke-direct {v4, v5, v3, v6, v7}, Lcom/jscape/util/e/UnixFileType;-><init>(Ljava/lang/String;ICI)V

    sput-object v4, Lcom/jscape/util/e/UnixFileType;->REGULAR:Lcom/jscape/util/e/UnixFileType;

    new-instance v4, Lcom/jscape/util/e/UnixFileType;

    aget-object v5, v1, v0

    const/16 v6, 0x64

    const/16 v7, 0x4000

    invoke-direct {v4, v5, v10, v6, v7}, Lcom/jscape/util/e/UnixFileType;-><init>(Ljava/lang/String;ICI)V

    sput-object v4, Lcom/jscape/util/e/UnixFileType;->DIRECTORY:Lcom/jscape/util/e/UnixFileType;

    new-instance v4, Lcom/jscape/util/e/UnixFileType;

    aget-object v5, v1, v3

    const/16 v6, 0x6c

    const v7, 0xa000

    invoke-direct {v4, v5, v0, v6, v7}, Lcom/jscape/util/e/UnixFileType;-><init>(Ljava/lang/String;ICI)V

    sput-object v4, Lcom/jscape/util/e/UnixFileType;->SYMBOLIC_LINK:Lcom/jscape/util/e/UnixFileType;

    new-instance v4, Lcom/jscape/util/e/UnixFileType;

    const/4 v5, 0x3

    aget-object v6, v1, v5

    const/16 v7, 0x70

    const/16 v8, 0x1000

    invoke-direct {v4, v6, v5, v7, v8}, Lcom/jscape/util/e/UnixFileType;-><init>(Ljava/lang/String;ICI)V

    sput-object v4, Lcom/jscape/util/e/UnixFileType;->NAMED_PIPE:Lcom/jscape/util/e/UnixFileType;

    new-instance v4, Lcom/jscape/util/e/UnixFileType;

    aget-object v5, v1, v2

    const/16 v6, 0x73

    const v7, 0xc000

    invoke-direct {v4, v5, v2, v6, v7}, Lcom/jscape/util/e/UnixFileType;-><init>(Ljava/lang/String;ICI)V

    sput-object v4, Lcom/jscape/util/e/UnixFileType;->SOCKET:Lcom/jscape/util/e/UnixFileType;

    new-instance v4, Lcom/jscape/util/e/UnixFileType;

    aget-object v5, v1, v10

    const/16 v6, 0x63

    const/16 v7, 0x2000

    invoke-direct {v4, v5, v15, v6, v7}, Lcom/jscape/util/e/UnixFileType;-><init>(Ljava/lang/String;ICI)V

    sput-object v4, Lcom/jscape/util/e/UnixFileType;->CHARACTER_DEVICE:Lcom/jscape/util/e/UnixFileType;

    new-instance v4, Lcom/jscape/util/e/UnixFileType;

    const/4 v5, 0x7

    aget-object v6, v1, v5

    const/16 v7, 0x62

    const/16 v8, 0x6000

    const/4 v9, 0x6

    invoke-direct {v4, v6, v9, v7, v8}, Lcom/jscape/util/e/UnixFileType;-><init>(Ljava/lang/String;ICI)V

    sput-object v4, Lcom/jscape/util/e/UnixFileType;->BLOCK_DEVICE:Lcom/jscape/util/e/UnixFileType;

    new-instance v4, Lcom/jscape/util/e/UnixFileType;

    aget-object v1, v1, v9

    const/16 v6, 0x44

    invoke-direct {v4, v1, v5, v6, v3}, Lcom/jscape/util/e/UnixFileType;-><init>(Ljava/lang/String;ICI)V

    sput-object v4, Lcom/jscape/util/e/UnixFileType;->DOOR:Lcom/jscape/util/e/UnixFileType;

    const/16 v1, 0x8

    new-array v1, v1, [Lcom/jscape/util/e/UnixFileType;

    sget-object v6, Lcom/jscape/util/e/UnixFileType;->REGULAR:Lcom/jscape/util/e/UnixFileType;

    aput-object v6, v1, v3

    sget-object v3, Lcom/jscape/util/e/UnixFileType;->DIRECTORY:Lcom/jscape/util/e/UnixFileType;

    aput-object v3, v1, v10

    sget-object v3, Lcom/jscape/util/e/UnixFileType;->SYMBOLIC_LINK:Lcom/jscape/util/e/UnixFileType;

    aput-object v3, v1, v0

    sget-object v0, Lcom/jscape/util/e/UnixFileType;->NAMED_PIPE:Lcom/jscape/util/e/UnixFileType;

    const/4 v3, 0x3

    aput-object v0, v1, v3

    sget-object v0, Lcom/jscape/util/e/UnixFileType;->SOCKET:Lcom/jscape/util/e/UnixFileType;

    aput-object v0, v1, v2

    sget-object v0, Lcom/jscape/util/e/UnixFileType;->CHARACTER_DEVICE:Lcom/jscape/util/e/UnixFileType;

    aput-object v0, v1, v15

    sget-object v0, Lcom/jscape/util/e/UnixFileType;->BLOCK_DEVICE:Lcom/jscape/util/e/UnixFileType;

    aput-object v0, v1, v9

    aput-object v4, v1, v5

    sput-object v1, Lcom/jscape/util/e/UnixFileType;->$VALUES:[Lcom/jscape/util/e/UnixFileType;

    return-void

    :cond_3
    const/16 v16, 0x8

    aget-char v17, v11, v14

    rem-int/lit8 v3, v14, 0x7

    const/16 v18, 0x51

    if-eqz v3, :cond_7

    if-eq v3, v10, :cond_6

    if-eq v3, v0, :cond_5

    const/4 v0, 0x3

    if-eq v3, v0, :cond_8

    if-eq v3, v2, :cond_4

    if-eq v3, v15, :cond_8

    const/16 v18, 0x6d

    goto :goto_4

    :cond_4
    const/16 v18, 0x22

    goto :goto_4

    :cond_5
    const/16 v18, 0x52

    goto :goto_4

    :cond_6
    const/16 v18, 0x3b

    goto :goto_4

    :cond_7
    const/16 v18, 0x4f

    :cond_8
    :goto_4
    xor-int v0, v9, v18

    xor-int v0, v17, v0

    int-to-char v0, v0

    aput-char v0, v11, v14

    add-int/lit8 v14, v14, 0x1

    move/from16 v0, v16

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;ICI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(CI)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-char p3, p0, Lcom/jscape/util/e/UnixFileType;->lsCode:C

    iput p4, p0, Lcom/jscape/util/e/UnixFileType;->mask:I

    return-void
.end method

.method public static isDirectory(C)Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/util/e/UnixFileType;->DIRECTORY:Lcom/jscape/util/e/UnixFileType;

    iget-char v1, v1, Lcom/jscape/util/e/UnixFileType;->lsCode:C

    if-nez v0, :cond_1

    if-ne v1, p0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move p0, v1

    :goto_1
    return p0
.end method

.method public static parse(I)Lcom/jscape/util/e/UnixFileType;
    .locals 7

    invoke-static {}, Lcom/jscape/util/e/UnixFileType;->values()[Lcom/jscape/util/e/UnixFileType;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/e/PosixFilePermissions;->b()Ljava/lang/String;

    move-result-object v1

    array-length v2, v0

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v2, :cond_3

    aget-object v4, v0, v3

    if-nez v1, :cond_4

    if-nez v1, :cond_2

    const v5, 0xf000

    and-int/2addr v5, p0

    iget v6, v4, Lcom/jscape/util/e/UnixFileType;->mask:I

    if-ne v5, v6, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    :cond_2
    if-eqz v1, :cond_0

    :cond_3
    sget-object v4, Lcom/jscape/util/e/UnixFileType;->REGULAR:Lcom/jscape/util/e/UnixFileType;

    :cond_4
    return-object v4
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/jscape/util/e/UnixFileType;
    .locals 1

    const-class v0, Lcom/jscape/util/e/UnixFileType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/util/e/UnixFileType;

    return-object p0
.end method

.method public static values()[Lcom/jscape/util/e/UnixFileType;
    .locals 1

    sget-object v0, Lcom/jscape/util/e/UnixFileType;->$VALUES:[Lcom/jscape/util/e/UnixFileType;

    invoke-virtual {v0}, [Lcom/jscape/util/e/UnixFileType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/util/e/UnixFileType;

    return-object v0
.end method


# virtual methods
.method public format(I)I
    .locals 1

    iget v0, p0, Lcom/jscape/util/e/UnixFileType;->mask:I

    or-int/2addr p1, v0

    return p1
.end method
