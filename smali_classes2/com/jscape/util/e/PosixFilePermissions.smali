.class public Lcom/jscape/util/e/PosixFilePermissions;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static b:Ljava/lang/String;


# instance fields
.field public final directoryDeletionRestricted:Z

.field public final groupCanExecute:Z

.field public final groupCanRead:Z

.field public final groupCanWrite:Z

.field public final otherCanExecute:Z

.field public final otherCanRead:Z

.field public final otherCanWrite:Z

.field public final ownerCanExecute:Z

.field public final ownerCanRead:Z

.field public final ownerCanWrite:Z

.field public final setGroupIdOnExecution:Z

.field public final setUserIdOnExecution:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/jscape/util/e/PosixFilePermissions;->b(Ljava/lang/String;)V

    const/4 v2, 0x0

    const-string v3, "-aDX\u001e}\u0008\u0011kgT\u0014V\u0008\u000e}^^\u0008HA\u0006a@_\u0003I\"\u001c`eT\u0007_\\\u0017Q.DT\u0012n\u0012\u0018|~U)U$\u0005kTD\u0012R\u000e\u00133\u0012Q.PC\tN\u0011>oYt\u001e^\u0002\u0008zR\u000c\u000fQ.XE\u000e^\u0013>oYc\u0003Z\u0005@\u000fQ.PC\tN\u0011>oYc\u0003Z\u0005@\u001eQ.SX\u0014^\u0002\taEH\"^\r\u0018z^^\u0008i\u0004\u000ezEX\u0005O\u0004\u00193\u0010Q.XE\u000e^\u0013>oYf\u0014R\u0015\u00183\u0010Q.PC\tN\u0011>oYf\u0014R\u0015\u00183\u0012Q.XE\u000e^\u0013>oYt\u001e^\u0002\u0008zR\u000c\u0010Q.XF\u0008^\u0013>oYf\u0014R\u0015\u00183"

    const/16 v4, 0xd3

    const/16 v5, 0x23

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x6e

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x2b

    const/16 v3, 0x18

    const-string v5, "\u0012m\u0007\u0017Q?PQ8\u0004;A7L{5\u0011\u0011P\u000cKQ#I\u0012\u0012m\u001b\u0005K\u001dP},\u001a7]\u001dAK9\u0011O"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x2d

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/util/e/PosixFilePermissions;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0xf

    goto :goto_4

    :cond_4
    const/16 v1, 0x55

    goto :goto_4

    :cond_5
    const/16 v1, 0x8

    goto :goto_4

    :cond_6
    const/16 v1, 0x5f

    goto :goto_4

    :cond_7
    const/16 v1, 0x59

    goto :goto_4

    :cond_8
    const/16 v1, 0x60

    goto :goto_4

    :cond_9
    const/16 v1, 0x13

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(ZZZZZZZZZZZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanRead:Z

    iput-boolean p2, p0, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanWrite:Z

    iput-boolean p3, p0, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanExecute:Z

    iput-boolean p4, p0, Lcom/jscape/util/e/PosixFilePermissions;->groupCanRead:Z

    iput-boolean p5, p0, Lcom/jscape/util/e/PosixFilePermissions;->groupCanWrite:Z

    iput-boolean p6, p0, Lcom/jscape/util/e/PosixFilePermissions;->groupCanExecute:Z

    iput-boolean p7, p0, Lcom/jscape/util/e/PosixFilePermissions;->otherCanRead:Z

    iput-boolean p8, p0, Lcom/jscape/util/e/PosixFilePermissions;->otherCanWrite:Z

    iput-boolean p9, p0, Lcom/jscape/util/e/PosixFilePermissions;->otherCanExecute:Z

    iput-boolean p10, p0, Lcom/jscape/util/e/PosixFilePermissions;->setUserIdOnExecution:Z

    iput-boolean p11, p0, Lcom/jscape/util/e/PosixFilePermissions;->setGroupIdOnExecution:Z

    iput-boolean p12, p0, Lcom/jscape/util/e/PosixFilePermissions;->directoryDeletionRestricted:Z

    return-void
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/util/e/PosixFilePermissions;->b:Ljava/lang/String;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/util/e/PosixFilePermissions;->b:Ljava/lang/String;

    return-void
.end method

.method public static ownerDirectoryPermissions()Lcom/jscape/util/e/PosixFilePermissions;
    .locals 14

    new-instance v13, Lcom/jscape/util/e/PosixFilePermissions;

    const/4 v1, 0x1

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/jscape/util/e/PosixFilePermissions;-><init>(ZZZZZZZZZZZZ)V

    return-object v13
.end method

.method public static permissionsOf(Lcom/jscape/util/M;)Lcom/jscape/util/e/PosixFilePermissions;
    .locals 14

    new-instance v13, Lcom/jscape/util/e/PosixFilePermissions;

    invoke-virtual {p0}, Lcom/jscape/util/M;->i()Z

    move-result v1

    invoke-virtual {p0}, Lcom/jscape/util/M;->j()Z

    move-result v2

    invoke-virtual {p0}, Lcom/jscape/util/M;->i()Z

    move-result v4

    invoke-virtual {p0}, Lcom/jscape/util/M;->j()Z

    move-result v5

    invoke-virtual {p0}, Lcom/jscape/util/M;->i()Z

    move-result v7

    invoke-virtual {p0}, Lcom/jscape/util/M;->j()Z

    move-result v8

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/jscape/util/e/PosixFilePermissions;-><init>(ZZZZZZZZZZZZ)V

    return-object v13
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/e/PosixFilePermissions;->a:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanRead:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanWrite:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v2, 0xb

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/util/e/PosixFilePermissions;->ownerCanExecute:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/util/e/PosixFilePermissions;->groupCanRead:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/util/e/PosixFilePermissions;->groupCanWrite:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/util/e/PosixFilePermissions;->groupCanExecute:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/util/e/PosixFilePermissions;->otherCanRead:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/util/e/PosixFilePermissions;->otherCanWrite:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/util/e/PosixFilePermissions;->otherCanExecute:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/util/e/PosixFilePermissions;->setUserIdOnExecution:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v2, 0xa

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/util/e/PosixFilePermissions;->setGroupIdOnExecution:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/util/e/PosixFilePermissions;->directoryDeletionRestricted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
