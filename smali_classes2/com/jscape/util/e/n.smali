.class public Lcom/jscape/util/e/n;
.super Ljava/lang/Object;


# static fields
.field private static final i:[Ljava/lang/String;


# instance fields
.field public final a:C

.field public final b:Lcom/jscape/util/e/PosixFilePermissions;

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:J

.field public final g:J

.field public final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x4

    const-string v4, ",-9-\u0011u~4>\u0011&i007, -P7*a\tu~;-\u000c7Udy\u000eu~,:\u0011/L*-50\r1\u0018\u0007u~/6\u0019\'\u0018\u0015\u0015-\u001a6\u000f\'l783\u007f\u0018$L5;\u0008&\u0013\'\u0018\u0005>,3*\u0013\tu~3(\r\'Wdy"

    const/16 v5, 0x5d

    move v7, v1

    const/4 v6, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x74

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x20

    const/16 v4, 0x13

    const-string v6, "-&hfHn0nbmaR\u007f\u0019Egpb\u0006\u000c-&bnW\u007f\u0013`ka:\u001c"

    move v7, v4

    move-object v4, v6

    move v8, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x2c

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/util/e/n;->i:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    if-eq v2, v1, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v2, 0x51

    goto :goto_4

    :cond_4
    const/16 v2, 0x36

    goto :goto_4

    :cond_5
    const/16 v2, 0x17

    goto :goto_4

    :cond_6
    const/16 v2, 0x2b

    goto :goto_4

    :cond_7
    const/16 v2, 0x28

    goto :goto_4

    :cond_8
    const/16 v2, 0x2a

    goto :goto_4

    :cond_9
    const/16 v2, 0x2d

    :goto_4
    xor-int/2addr v2, v9

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(CLcom/jscape/util/e/PosixFilePermissions;ILjava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-char p1, p0, Lcom/jscape/util/e/n;->a:C

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/e/n;->b:Lcom/jscape/util/e/PosixFilePermissions;

    iput p3, p0, Lcom/jscape/util/e/n;->c:I

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/util/e/n;->d:Ljava/lang/String;

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/util/e/n;->e:Ljava/lang/String;

    iput-wide p6, p0, Lcom/jscape/util/e/n;->f:J

    iput-wide p8, p0, Lcom/jscape/util/e/n;->g:J

    invoke-static {p10}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p10, p0, Lcom/jscape/util/e/n;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/jscape/util/e/UnixFileType;Lcom/jscape/util/e/PosixFilePermissions;ILjava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V
    .locals 11

    move-object v0, p1

    iget-char v1, v0, Lcom/jscape/util/e/UnixFileType;->lsCode:C

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v10}, Lcom/jscape/util/e/n;-><init>(CLcom/jscape/util/e/PosixFilePermissions;ILjava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V

    return-void
.end method

.method public static a(Lcom/jscape/util/M;)Lcom/jscape/util/e/n;
    .locals 12

    new-instance v11, Lcom/jscape/util/e/n;

    invoke-virtual {p0}, Lcom/jscape/util/M;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jscape/util/e/UnixFileType;->DIRECTORY:Lcom/jscape/util/e/UnixFileType;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/jscape/util/e/UnixFileType;->REGULAR:Lcom/jscape/util/e/UnixFileType;

    :goto_0
    move-object v1, v0

    invoke-static {p0}, Lcom/jscape/util/e/PosixFilePermissions;->permissionsOf(Lcom/jscape/util/M;)Lcom/jscape/util/e/PosixFilePermissions;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/jscape/util/M;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/jscape/util/e/n;->i:[Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v0, v0, v4

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/jscape/util/M;->f()Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v4, v0

    invoke-virtual {p0}, Lcom/jscape/util/M;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/jscape/util/e/n;->i:[Ljava/lang/String;

    const/4 v5, 0x6

    aget-object v0, v0, v5

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/jscape/util/M;->g()Ljava/lang/String;

    move-result-object v0

    :goto_2
    move-object v5, v0

    invoke-virtual {p0}, Lcom/jscape/util/M;->e()J

    move-result-wide v6

    invoke-virtual {p0}, Lcom/jscape/util/M;->h()J

    move-result-wide v8

    invoke-virtual {p0}, Lcom/jscape/util/M;->b()Ljava/lang/String;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/jscape/util/e/n;-><init>(Lcom/jscape/util/e/UnixFileType;Lcom/jscape/util/e/PosixFilePermissions;ILjava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V

    return-object v11
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-char v0, p0, Lcom/jscape/util/e/n;->a:C

    invoke-static {v0}, Lcom/jscape/util/e/UnixFileType;->isDirectory(C)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/e/n;->i:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-char v2, p0, Lcom/jscape/util/e/n;->a:C

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/e/n;->b:Lcom/jscape/util/e/PosixFilePermissions;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/util/e/n;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/e/n;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/util/e/n;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lcom/jscape/util/e/n;->f:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v3, 0x8

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lcom/jscape/util/e/n;->g:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v3, 0x9

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/e/n;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
