.class public final enum Lcom/jscape/util/u;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/util/u;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/jscape/util/u;

.field public static final enum b:Lcom/jscape/util/u;

.field public static final enum c:Lcom/jscape/util/u;

.field private static final f:[Lcom/jscape/util/u;


# instance fields
.field public final d:J

.field public final e:J


# direct methods
.method static constructor <clinit>()V
    .locals 27

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xc

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x48

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "z\u0002O&Pb[t\u0004B0L\u000by\nB;Lt]x\u0005H\'\u000cz\u0002@8Vb[t\u0004B0L"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    const/4 v11, 0x2

    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x25

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move/from16 v26, v4

    move v4, v3

    move/from16 v3, v26

    goto :goto_0

    :cond_0
    new-instance v3, Lcom/jscape/util/u;

    aget-object v13, v1, v11

    const/4 v14, 0x0

    const-wide/16 v15, 0x1

    const-wide/32 v17, 0xf4240

    move-object v12, v3

    invoke-direct/range {v12 .. v18}, Lcom/jscape/util/u;-><init>(Ljava/lang/String;IJJ)V

    sput-object v3, Lcom/jscape/util/u;->a:Lcom/jscape/util/u;

    new-instance v3, Lcom/jscape/util/u;

    aget-object v20, v1, v2

    const/16 v21, 0x1

    const-wide/16 v22, 0x3e8

    const-wide/16 v24, 0x3e8

    move-object/from16 v19, v3

    invoke-direct/range {v19 .. v25}, Lcom/jscape/util/u;-><init>(Ljava/lang/String;IJJ)V

    sput-object v3, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    new-instance v3, Lcom/jscape/util/u;

    aget-object v13, v1, v7

    const/4 v14, 0x2

    const-wide/32 v15, 0xf4240

    const-wide/16 v17, 0x1

    move-object v12, v3

    invoke-direct/range {v12 .. v18}, Lcom/jscape/util/u;-><init>(Ljava/lang/String;IJJ)V

    sput-object v3, Lcom/jscape/util/u;->c:Lcom/jscape/util/u;

    new-array v0, v0, [Lcom/jscape/util/u;

    sget-object v1, Lcom/jscape/util/u;->a:Lcom/jscape/util/u;

    aput-object v1, v0, v2

    sget-object v1, Lcom/jscape/util/u;->b:Lcom/jscape/util/u;

    aput-object v1, v0, v7

    aput-object v3, v0, v11

    sput-object v0, Lcom/jscape/util/u;->f:[Lcom/jscape/util/u;

    return-void

    :cond_1
    aget-char v12, v4, v10

    rem-int/lit8 v13, v10, 0x7

    if-eqz v13, :cond_7

    if-eq v13, v7, :cond_6

    if-eq v13, v11, :cond_5

    if-eq v13, v0, :cond_4

    const/4 v11, 0x4

    if-eq v13, v11, :cond_3

    const/4 v11, 0x5

    if-eq v13, v11, :cond_2

    const/16 v11, 0x56

    goto :goto_2

    :cond_2
    const/16 v11, 0x79

    goto :goto_2

    :cond_3
    const/16 v11, 0x57

    goto :goto_2

    :cond_4
    const/16 v11, 0x3c

    goto :goto_2

    :cond_5
    const/16 v11, 0x44

    goto :goto_2

    :cond_6
    move v11, v0

    goto :goto_2

    :cond_7
    const/16 v11, 0x7f

    :goto_2
    xor-int/2addr v11, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;IJJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-wide p3, p0, Lcom/jscape/util/u;->d:J

    iput-wide p5, p0, Lcom/jscape/util/u;->e:J

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/util/u;
    .locals 1

    const-class v0, Lcom/jscape/util/u;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/util/u;

    return-object p0
.end method

.method public static a()[Lcom/jscape/util/u;
    .locals 1

    sget-object v0, Lcom/jscape/util/u;->f:[Lcom/jscape/util/u;

    invoke-virtual {v0}, [Lcom/jscape/util/u;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/util/u;

    return-object v0
.end method
