.class public Lcom/jscape/util/P;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/jscape/util/M;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/util/M;Lcom/jscape/util/M;)I
    .locals 3

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    invoke-virtual {p1}, Lcom/jscape/util/M;->c()C

    move-result v1

    const/16 v2, 0x64

    if-nez v0, :cond_1

    if-ne v1, v2, :cond_0

    invoke-virtual {p2}, Lcom/jscape/util/M;->c()C

    move-result v1

    if-nez v0, :cond_1

    if-eq v1, v2, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    invoke-virtual {p1}, Lcom/jscape/util/M;->c()C

    move-result v1

    if-nez v0, :cond_3

    :cond_1
    if-eq v1, v2, :cond_2

    invoke-virtual {p2}, Lcom/jscape/util/M;->c()C

    move-result v1

    if-nez v0, :cond_3

    if-ne v1, v2, :cond_2

    const/4 p1, 0x1

    return p1

    :cond_2
    invoke-virtual {p1}, Lcom/jscape/util/M;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/jscape/util/M;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    :cond_3
    return v1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/jscape/util/M;

    check-cast p2, Lcom/jscape/util/M;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/util/P;->a(Lcom/jscape/util/M;Lcom/jscape/util/M;)I

    move-result p1

    return p1
.end method
