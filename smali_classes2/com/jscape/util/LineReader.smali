.class public Lcom/jscape/util/LineReader;
.super Ljava/io/FilterInputStream;


# instance fields
.field private delim:[C

.field private endOfStream:Z

.field private hasDelimiter:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0

    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    const/4 p1, 0x2

    new-array p1, p1, [C

    fill-array-data p1, :array_0

    iput-object p1, p0, Lcom/jscape/util/LineReader;->delim:[C

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/jscape/util/LineReader;->hasDelimiter:Z

    iput-boolean p1, p0, Lcom/jscape/util/LineReader;->endOfStream:Z

    return-void

    nop

    :array_0
    .array-data 2
        0xds
        0xas
    .end array-data
.end method

.method public constructor <init>(Ljava/io/InputStream;[C)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/util/LineReader;-><init>(Ljava/io/InputStream;)V

    iput-object p2, p0, Lcom/jscape/util/LineReader;->delim:[C

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public hasDelimiter()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/util/LineReader;->hasDelimiter:Z

    return v0
.end method

.method public readLine()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/jscape/util/LineReader;->readLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public readLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/util/LineReader;->hasDelimiter:Z

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v1

    new-instance v2, Lcom/jscape/util/ByteBuffer;

    invoke-direct {v2}, Lcom/jscape/util/ByteBuffer;-><init>()V

    :try_start_0
    iget-boolean v3, p0, Lcom/jscape/util/LineReader;->endOfStream:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_d

    const/4 v4, 0x0

    if-eqz v1, :cond_1

    if-eqz v3, :cond_0

    return-object v4

    :cond_0
    move-object v5, p0

    move v6, v0

    goto/16 :goto_6

    :cond_1
    move-object v5, p0

    move v6, v0

    :goto_0
    if-eqz v3, :cond_2

    goto/16 :goto_4

    :cond_2
    iget-object v3, v5, Lcom/jscape/util/LineReader;->in:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v3

    const/4 v7, -0x1

    const/4 v8, 0x1

    if-eqz v1, :cond_4

    if-ne v3, v7, :cond_3

    :try_start_1
    iput-boolean v8, v5, Lcom/jscape/util/LineReader;->endOfStream:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v1, :cond_c

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/util/LineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    int-to-byte v7, v3

    invoke-virtual {v2, v7}, Lcom/jscape/util/ByteBuffer;->append(B)V

    iget-object v7, v5, Lcom/jscape/util/LineReader;->delim:[C

    aget-char v7, v7, v6
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz v1, :cond_6

    move v9, v3

    goto :goto_2

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/util/LineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/LineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_4
    move v9, v7

    move v7, v3

    :goto_2
    if-ne v7, v9, :cond_5

    add-int/lit8 v6, v6, 0x1

    if-nez v1, :cond_9

    :cond_5
    move v7, v6

    :cond_6
    if-eqz v1, :cond_8

    if-eqz v7, :cond_7

    move v6, v0

    :cond_7
    iget-object v7, v5, Lcom/jscape/util/LineReader;->delim:[C

    aget-char v7, v7, v6

    :cond_8
    if-eqz v1, :cond_a

    if-ne v7, v3, :cond_9

    add-int/lit8 v6, v6, 0x1

    :cond_9
    iget-object v3, v5, Lcom/jscape/util/LineReader;->delim:[C

    array-length v3, v3

    move v7, v6

    :cond_a
    if-ne v7, v3, :cond_b

    :try_start_4
    iput-boolean v8, v5, Lcom/jscape/util/LineReader;->hasDelimiter:Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    if-nez v1, :cond_c

    goto :goto_3

    :catch_3
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/util/LineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/LineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_b
    :goto_3
    if-nez v1, :cond_13

    :cond_c
    :goto_4
    :try_start_6
    iget-boolean v3, v5, Lcom/jscape/util/LineReader;->hasDelimiter:Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7

    if-eqz v1, :cond_10

    if-eqz v3, :cond_f

    :try_start_7
    iget-boolean v3, v5, Lcom/jscape/util/LineReader;->endOfStream:Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_8

    if-eqz v1, :cond_10

    if-eqz v3, :cond_d

    :try_start_8
    invoke-virtual {v2}, Lcom/jscape/util/ByteBuffer;->length()I

    move-result v3

    iget-object v6, v5, Lcom/jscape/util/LineReader;->delim:[C

    array-length v6, v6
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_a

    if-eqz v1, :cond_e

    if-ne v3, v6, :cond_d

    return-object v4

    :cond_d
    invoke-virtual {v2}, Lcom/jscape/util/ByteBuffer;->length()I

    move-result v3

    iget-object v4, v5, Lcom/jscape/util/LineReader;->delim:[C

    array-length v6, v4

    :cond_e
    sub-int/2addr v3, v6

    goto :goto_5

    :cond_f
    invoke-virtual {v2}, Lcom/jscape/util/ByteBuffer;->length()I

    move-result v3

    :cond_10
    :goto_5
    new-array v3, v3, [B

    :try_start_9
    invoke-virtual {v2}, Lcom/jscape/util/ByteBuffer;->toByteArray()[B

    move-result-object v2

    array-length v4, v3

    invoke-static {v2, v0, v3, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    if-eqz v1, :cond_12

    if-eqz p1, :cond_11

    :try_start_a
    new-instance v0, Ljava/lang/String;

    invoke-static {p1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object p1

    invoke-direct {v0, v3, p1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    return-object v0

    :cond_11
    new-instance p1, Ljava/lang/String;

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-direct {p1, v3, v0}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    :cond_12
    return-object p1

    :catch_5
    move-exception p1

    :try_start_b
    invoke-static {p1}, Lcom/jscape/util/LineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/LineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :catch_7
    move-exception p1

    :try_start_c
    invoke-static {p1}, Lcom/jscape/util/LineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    :catch_8
    move-exception p1

    :try_start_d
    invoke-static {p1}, Lcom/jscape/util/LineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_9

    :catch_9
    move-exception p1

    :try_start_e
    invoke-static {p1}, Lcom/jscape/util/LineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_a

    :catch_a
    move-exception p1

    :try_start_f
    invoke-static {p1}, Lcom/jscape/util/LineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_b

    :catch_b
    move-exception p1

    :try_start_10
    invoke-static {p1}, Lcom/jscape/util/LineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_c

    :catch_c
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/LineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_13
    :goto_6
    iget-boolean v3, v5, Lcom/jscape/util/LineReader;->endOfStream:Z

    goto/16 :goto_0

    :catch_d
    move-exception p1

    :try_start_11
    invoke-static {p1}, Lcom/jscape/util/LineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_e

    :catch_e
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/LineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method
