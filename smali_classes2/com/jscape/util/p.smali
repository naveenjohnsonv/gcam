.class public final enum Lcom/jscape/util/p;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/util/p;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/jscape/util/p;

.field public static final enum b:Lcom/jscape/util/p;

.field public static final enum c:Lcom/jscape/util/p;

.field public static final enum d:Lcom/jscape/util/p;

.field public static final enum e:Lcom/jscape/util/p;

.field private static final h:[Lcom/jscape/util/p;


# instance fields
.field public final f:J

.field public final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 29

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "NKKc\u0014{\u0004@\u0008BK@m\u0014{\u0004@\u0002H@\u0004G[Si\u0007g{sI~Qy\u0008QGUm\u0014{\u0004@\u0002B@\u0008HG@m\u0014{\u0004@"

    const/16 v5, 0x36

    move v8, v3

    const/4 v6, -0x1

    const/16 v7, 0x8

    :goto_0
    const/16 v9, 0x20

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v14, v11

    move v15, v3

    :goto_2
    const/4 v2, 0x4

    const/4 v12, 0x3

    const/4 v1, 0x2

    if-gt v14, v15, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v13, :cond_1

    add-int/lit8 v2, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v2

    goto :goto_0

    :cond_0
    const-string v4, "}l\u0002bl"

    move v7, v1

    move v8, v2

    const/4 v5, 0x5

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    move v7, v1

    move v8, v11

    :goto_3
    add-int/2addr v6, v10

    add-int v1, v6, v7

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v13, v3

    const/16 v9, 0xc

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/jscape/util/p;

    aget-object v18, v0, v12

    const/16 v19, 0x0

    const-wide/16 v20, 0x1

    aget-object v22, v0, v2

    move-object/from16 v17, v4

    invoke-direct/range {v17 .. v22}, Lcom/jscape/util/p;-><init>(Ljava/lang/String;IJLjava/lang/String;)V

    sput-object v4, Lcom/jscape/util/p;->a:Lcom/jscape/util/p;

    new-instance v4, Lcom/jscape/util/p;

    aget-object v24, v0, v3

    const/16 v25, 0x1

    const-wide/16 v26, 0x400

    const/16 v5, 0x9

    aget-object v28, v0, v5

    move-object/from16 v23, v4

    invoke-direct/range {v23 .. v28}, Lcom/jscape/util/p;-><init>(Ljava/lang/String;IJLjava/lang/String;)V

    sput-object v4, Lcom/jscape/util/p;->b:Lcom/jscape/util/p;

    new-instance v4, Lcom/jscape/util/p;

    const/4 v5, 0x7

    aget-object v18, v0, v5

    const/16 v19, 0x2

    const-wide/32 v20, 0x100000

    aget-object v22, v0, v1

    move-object/from16 v17, v4

    invoke-direct/range {v17 .. v22}, Lcom/jscape/util/p;-><init>(Ljava/lang/String;IJLjava/lang/String;)V

    sput-object v4, Lcom/jscape/util/p;->c:Lcom/jscape/util/p;

    new-instance v4, Lcom/jscape/util/p;

    aget-object v24, v0, v10

    const/16 v25, 0x3

    const-wide/32 v26, 0x40000000

    const/4 v5, 0x6

    aget-object v28, v0, v5

    move-object/from16 v23, v4

    invoke-direct/range {v23 .. v28}, Lcom/jscape/util/p;-><init>(Ljava/lang/String;IJLjava/lang/String;)V

    sput-object v4, Lcom/jscape/util/p;->d:Lcom/jscape/util/p;

    new-instance v4, Lcom/jscape/util/p;

    const/4 v5, 0x5

    aget-object v18, v0, v5

    const/16 v19, 0x4

    const-wide v20, 0x10000000000L

    const/16 v16, 0x8

    aget-object v22, v0, v16

    move-object/from16 v17, v4

    invoke-direct/range {v17 .. v22}, Lcom/jscape/util/p;-><init>(Ljava/lang/String;IJLjava/lang/String;)V

    sput-object v4, Lcom/jscape/util/p;->e:Lcom/jscape/util/p;

    new-array v0, v5, [Lcom/jscape/util/p;

    sget-object v5, Lcom/jscape/util/p;->a:Lcom/jscape/util/p;

    aput-object v5, v0, v3

    sget-object v3, Lcom/jscape/util/p;->b:Lcom/jscape/util/p;

    aput-object v3, v0, v10

    sget-object v3, Lcom/jscape/util/p;->c:Lcom/jscape/util/p;

    aput-object v3, v0, v1

    sget-object v1, Lcom/jscape/util/p;->d:Lcom/jscape/util/p;

    aput-object v1, v0, v12

    aput-object v4, v0, v2

    sput-object v0, Lcom/jscape/util/p;->h:[Lcom/jscape/util/p;

    return-void

    :cond_3
    const/16 v16, 0x8

    aget-char v17, v11, v15

    rem-int/lit8 v3, v15, 0x7

    if-eqz v3, :cond_8

    if-eq v3, v10, :cond_7

    if-eq v3, v1, :cond_6

    if-eq v3, v12, :cond_5

    if-eq v3, v2, :cond_4

    const/4 v2, 0x5

    if-eq v3, v2, :cond_9

    const/16 v1, 0x70

    goto :goto_4

    :cond_4
    const/16 v1, 0x76

    goto :goto_4

    :cond_5
    const/16 v1, 0xc

    goto :goto_4

    :cond_6
    const/16 v1, 0x27

    goto :goto_4

    :cond_7
    const/16 v1, 0x22

    goto :goto_4

    :cond_8
    const/16 v1, 0x25

    :cond_9
    :goto_4
    xor-int/2addr v1, v9

    xor-int v1, v17, v1

    int-to-char v1, v1

    aput-char v1, v11, v15

    add-int/lit8 v15, v15, 0x1

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;IJLjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-wide p3, p0, Lcom/jscape/util/p;->f:J

    iput-object p5, p0, Lcom/jscape/util/p;->g:Ljava/lang/String;

    return-void
.end method

.method public static a(D)Lcom/jscape/util/p;
    .locals 3

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    invoke-static {p0, p1}, Ljava/lang/Math;->abs(D)D

    move-result-wide p0

    const-wide/16 v1, 0x0

    cmpg-double v1, v1, p0

    if-nez v0, :cond_1

    if-gtz v1, :cond_0

    sget-object v1, Lcom/jscape/util/p;->b:Lcom/jscape/util/p;

    iget-wide v1, v1, Lcom/jscape/util/p;->f:J

    long-to-double v1, v1

    cmpg-double v1, p0, v1

    if-nez v0, :cond_1

    if-gez v1, :cond_0

    sget-object p0, Lcom/jscape/util/p;->a:Lcom/jscape/util/p;

    return-object p0

    :cond_0
    sget-object v1, Lcom/jscape/util/p;->b:Lcom/jscape/util/p;

    iget-wide v1, v1, Lcom/jscape/util/p;->f:J

    long-to-double v1, v1

    cmpg-double v1, v1, p0

    :cond_1
    if-nez v0, :cond_3

    if-gtz v1, :cond_2

    sget-object v1, Lcom/jscape/util/p;->c:Lcom/jscape/util/p;

    iget-wide v1, v1, Lcom/jscape/util/p;->f:J

    long-to-double v1, v1

    cmpg-double v1, p0, v1

    if-nez v0, :cond_3

    if-gez v1, :cond_2

    sget-object p0, Lcom/jscape/util/p;->b:Lcom/jscape/util/p;

    return-object p0

    :cond_2
    sget-object v1, Lcom/jscape/util/p;->c:Lcom/jscape/util/p;

    iget-wide v1, v1, Lcom/jscape/util/p;->f:J

    long-to-double v1, v1

    cmpg-double v1, v1, p0

    :cond_3
    if-nez v0, :cond_5

    if-gtz v1, :cond_4

    sget-object v1, Lcom/jscape/util/p;->d:Lcom/jscape/util/p;

    iget-wide v1, v1, Lcom/jscape/util/p;->f:J

    long-to-double v1, v1

    cmpg-double v1, p0, v1

    if-nez v0, :cond_5

    if-gez v1, :cond_4

    sget-object p0, Lcom/jscape/util/p;->c:Lcom/jscape/util/p;

    return-object p0

    :cond_4
    sget-object v1, Lcom/jscape/util/p;->d:Lcom/jscape/util/p;

    if-nez v0, :cond_7

    iget-wide v0, v1, Lcom/jscape/util/p;->f:J

    long-to-double v0, v0

    cmpg-double v1, v0, p0

    :cond_5
    if-gtz v1, :cond_6

    sget-object v0, Lcom/jscape/util/p;->e:Lcom/jscape/util/p;

    iget-wide v0, v0, Lcom/jscape/util/p;->f:J

    long-to-double v0, v0

    cmpg-double p0, p0, v0

    if-gez p0, :cond_6

    sget-object p0, Lcom/jscape/util/p;->d:Lcom/jscape/util/p;

    return-object p0

    :cond_6
    sget-object v1, Lcom/jscape/util/p;->e:Lcom/jscape/util/p;

    :cond_7
    return-object v1
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/util/p;
    .locals 1

    const-class v0, Lcom/jscape/util/p;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/util/p;

    return-object p0
.end method

.method public static a([D)Lcom/jscape/util/p;
    .locals 7

    invoke-static {p0}, Lcom/jscape/util/p;->b([D)D

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/jscape/util/p;->a(D)Lcom/jscape/util/p;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v1

    const/4 v2, 0x0

    :cond_0
    array-length v3, p0

    if-ge v2, v3, :cond_1

    aget-wide v3, p0, v2

    iget-wide v5, v0, Lcom/jscape/util/p;->f:J

    long-to-double v5, v5

    div-double/2addr v3, v5

    aput-wide v3, p0, v2

    add-int/lit8 v2, v2, 0x1

    if-eqz v1, :cond_0

    :cond_1
    return-object v0
.end method

.method public static a()[Lcom/jscape/util/p;
    .locals 1

    sget-object v0, Lcom/jscape/util/p;->h:[Lcom/jscape/util/p;

    invoke-virtual {v0}, [Lcom/jscape/util/p;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/util/p;

    return-object v0
.end method

.method private static b([D)D
    .locals 7

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    array-length v1, p0

    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    :cond_0
    if-ge v4, v1, :cond_1

    aget-wide v5, p0, v4

    invoke-static {v5, v6, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    if-eqz v0, :cond_1

    add-int/lit8 v4, v4, 0x1

    if-nez v0, :cond_0

    :cond_1
    return-wide v2
.end method
