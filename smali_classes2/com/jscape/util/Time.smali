.class public Lcom/jscape/util/Time;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/jscape/util/Time;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field public final unit:Ljava/util/concurrent/TimeUnit;

.field public final value:J


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x7

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v3

    :goto_0
    const/16 v6, 0x36

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v2, v4

    const-string v8, "xI\tD])_\u000c\u0000\u0000\u0011O\u0014&\u00145\u0005\tO\t\u000f\u0016\u0008\u0018\n@4\u000f1I\nKX(\u0007z"

    invoke-virtual {v8, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v3

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x24

    if-ge v2, v4, :cond_0

    invoke-virtual {v8, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v2

    move v2, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/Time;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/4 v13, 0x2

    if-eqz v12, :cond_6

    if-eq v12, v7, :cond_5

    if-eq v12, v13, :cond_4

    if-eq v12, v0, :cond_3

    const/4 v14, 0x4

    if-eq v12, v14, :cond_7

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v13, 0x54

    goto :goto_2

    :cond_2
    const/16 v13, 0x6b

    goto :goto_2

    :cond_3
    const/16 v13, 0x1c

    goto :goto_2

    :cond_4
    const/16 v13, 0x4a

    goto :goto_2

    :cond_5
    const/16 v13, 0x5f

    goto :goto_2

    :cond_6
    const/16 v13, 0x62

    :cond_7
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(JLjava/util/concurrent/TimeUnit;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/jscape/util/Time;->a:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    const-wide/16 v1, 0x0

    invoke-static {p1, p2, v1, v2, v0}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    iput-wide p1, p0, Lcom/jscape/util/Time;->value:J

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    return-void
.end method

.method public static days(J)Lcom/jscape/util/Time;
    .locals 3

    new-instance v0, Lcom/jscape/util/Time;

    const-wide/16 v1, 0x3c

    mul-long/2addr p0, v1

    mul-long/2addr p0, v1

    const-wide/16 v1, 0x18

    mul-long/2addr p0, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/util/Time;-><init>(JLjava/util/concurrent/TimeUnit;)V

    return-object v0
.end method

.method public static hours(J)Lcom/jscape/util/Time;
    .locals 3

    new-instance v0, Lcom/jscape/util/Time;

    const-wide/16 v1, 0x3c

    mul-long/2addr p0, v1

    mul-long/2addr p0, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/util/Time;-><init>(JLjava/util/concurrent/TimeUnit;)V

    return-object v0
.end method

.method public static micros(J)Lcom/jscape/util/Time;
    .locals 2

    new-instance v0, Lcom/jscape/util/Time;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/util/Time;-><init>(JLjava/util/concurrent/TimeUnit;)V

    return-object v0
.end method

.method public static millis(J)Lcom/jscape/util/Time;
    .locals 2

    new-instance v0, Lcom/jscape/util/Time;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/util/Time;-><init>(JLjava/util/concurrent/TimeUnit;)V

    return-object v0
.end method

.method public static minutes(J)Lcom/jscape/util/Time;
    .locals 3

    new-instance v0, Lcom/jscape/util/Time;

    const-wide/16 v1, 0x3c

    mul-long/2addr p0, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/util/Time;-><init>(JLjava/util/concurrent/TimeUnit;)V

    return-object v0
.end method

.method public static nanos(J)Lcom/jscape/util/Time;
    .locals 2

    new-instance v0, Lcom/jscape/util/Time;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/util/Time;-><init>(JLjava/util/concurrent/TimeUnit;)V

    return-object v0
.end method

.method public static seconds(J)Lcom/jscape/util/Time;
    .locals 2

    new-instance v0, Lcom/jscape/util/Time;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v0, p0, p1, v1}, Lcom/jscape/util/Time;-><init>(JLjava/util/concurrent/TimeUnit;)V

    return-object v0
.end method


# virtual methods
.method public compareTo(Lcom/jscape/util/Time;)I
    .locals 5

    invoke-virtual {p0}, Lcom/jscape/util/Time;->toNanos()J

    move-result-wide v0

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v2

    invoke-virtual {p1}, Lcom/jscape/util/Time;->toNanos()J

    move-result-wide v3

    cmp-long p1, v0, v3

    if-nez v2, :cond_0

    if-gez p1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    if-nez v2, :cond_2

    if-nez p1, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    :cond_2
    :goto_0
    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lcom/jscape/util/Time;

    invoke-virtual {p0, p1}, Lcom/jscape/util/Time;->compareTo(Lcom/jscape/util/Time;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    if-ne p0, p1, :cond_0

    return v1

    :cond_0
    move-object v2, p1

    goto :goto_0

    :cond_1
    move-object v2, p0

    :goto_0
    const/4 v3, 0x0

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v2, v4, :cond_3

    goto :goto_2

    :cond_2
    move-object p1, v2

    :cond_3
    check-cast p1, Lcom/jscape/util/Time;

    invoke-virtual {p0}, Lcom/jscape/util/Time;->toNanos()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/jscape/util/Time;->toNanos()J

    move-result-wide v6

    cmp-long p1, v4, v6

    if-eqz v0, :cond_4

    if-nez p1, :cond_5

    goto :goto_1

    :cond_4
    move v1, p1

    :goto_1
    move v3, v1

    :cond_5
    :goto_2
    return v3
.end method

.method public hashCode()I
    .locals 4

    iget-wide v0, p0, Lcom/jscape/util/Time;->value:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1}, Ljava/util/concurrent/TimeUnit;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public sleep()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    iget-wide v1, p0, Lcom/jscape/util/Time;->value:J

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->sleep(J)V

    return-void
.end method

.method public sleepSafe()V
    .locals 0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/util/Time;->sleep()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public timedJoin(Ljava/lang/Thread;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    iget-wide v1, p0, Lcom/jscape/util/Time;->value:J

    invoke-virtual {v0, p1, v1, v2}, Ljava/util/concurrent/TimeUnit;->timedJoin(Ljava/lang/Thread;J)V

    return-void
.end method

.method public timedWait(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    iget-wide v1, p0, Lcom/jscape/util/Time;->value:J

    invoke-virtual {v0, p1, v1, v2}, Ljava/util/concurrent/TimeUnit;->timedWait(Ljava/lang/Object;J)V

    return-void
.end method

.method public toDays()J
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    iget-wide v1, p0, Lcom/jscape/util/Time;->value:J

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public toHours()J
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    iget-wide v1, p0, Lcom/jscape/util/Time;->value:J

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public toMicros()J
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    iget-wide v1, p0, Lcom/jscape/util/Time;->value:J

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public toMillis()J
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    iget-wide v1, p0, Lcom/jscape/util/Time;->value:J

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public toMinutes()J
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    iget-wide v1, p0, Lcom/jscape/util/Time;->value:J

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public toNanos()J
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    iget-wide v1, p0, Lcom/jscape/util/Time;->value:J

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public toSeconds()J
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    iget-wide v1, p0, Lcom/jscape/util/Time;->value:J

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/Time;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/util/Time;->value:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
