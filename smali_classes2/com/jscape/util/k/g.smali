.class public Lcom/jscape/util/k/g;
.super Ljava/lang/Object;


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field private a:Lcom/jscape/util/k/TransportAddress;

.field private b:Lcom/jscape/util/k/TransportAddress;

.field private c:J


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "28\u0013\n\u0007]Z\u00156\u0002^SBV\u001c,\u0012\u0004\u0010\\y\u0005O\u001e[C\u0015\u0018\u0013N\u0001QD\u0003d#36\u0019D\u0016WC\u00196\u0019n\u0016GT\u00020\u0007^\u001cF\u0017\u000b5\u0018I\u0012Xv\u0014=\u0005O\u0000G\n"

    move v9, v5

    const/4 v7, -0x1

    const/16 v8, 0x12

    const/16 v10, 0x47

    :goto_0
    const/16 v11, 0x6d

    const/4 v12, 0x1

    add-int/2addr v7, v12

    add-int v13, v7, v8

    invoke-virtual {v6, v7, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    :goto_1
    invoke-virtual {v13}, Ljava/lang/String;->toCharArray()[C

    move-result-object v13

    array-length v15, v13

    move v2, v5

    :goto_2
    const/16 v16, 0x1d

    if-gt v15, v2, :cond_3

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v13}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v11, v9, 0x1

    if-eqz v14, :cond_1

    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v10, :cond_0

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v11

    goto :goto_0

    :cond_0
    const-string v6, "HBip}\' oLx$)8,fVh~\n&\u0003y9d+\"\u007fW0"

    move v9, v11

    move/from16 v10, v16

    const/4 v7, -0x1

    const/16 v8, 0x12

    goto :goto_3

    :cond_1
    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v10, :cond_2

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v2

    move v9, v11

    :goto_3
    const/16 v11, 0x17

    add-int/2addr v7, v12

    add-int v2, v7, v8

    invoke-virtual {v6, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    move v14, v5

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/k/g;->d:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v17, v13, v2

    rem-int/lit8 v3, v2, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v12, :cond_8

    const/4 v4, 0x2

    if-eq v3, v4, :cond_7

    const/4 v4, 0x3

    if-eq v3, v4, :cond_6

    const/4 v4, 0x4

    if-eq v3, v4, :cond_5

    if-eq v3, v0, :cond_4

    const/16 v16, 0x5a

    goto :goto_4

    :cond_4
    const/16 v16, 0x59

    goto :goto_4

    :cond_5
    const/16 v16, 0x1e

    goto :goto_4

    :cond_6
    const/16 v16, 0x47

    goto :goto_4

    :cond_7
    const/16 v16, 0x1a

    goto :goto_4

    :cond_8
    const/16 v16, 0x34

    :cond_9
    :goto_4
    xor-int v3, v11, v16

    xor-int v3, v17, v3

    int-to-char v3, v3

    aput-char v3, v13, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/util/k/TransportAddress;J)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/jscape/util/k/g;-><init>(Lcom/jscape/util/k/TransportAddress;Lcom/jscape/util/k/TransportAddress;J)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/util/k/TransportAddress;Lcom/jscape/util/Time;)V
    .locals 2

    invoke-virtual {p2}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/jscape/util/k/g;-><init>(Lcom/jscape/util/k/TransportAddress;J)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/util/k/TransportAddress;Lcom/jscape/util/k/TransportAddress;J)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/k/g;->a:Lcom/jscape/util/k/TransportAddress;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/k/g;->b:Lcom/jscape/util/k/TransportAddress;

    sget-object p1, Lcom/jscape/util/k/g;->d:[Ljava/lang/String;

    const/4 p2, 0x0

    aget-object p1, p1, p2

    const-wide/16 v0, 0x0

    invoke-static {p3, p4, v0, v1, p1}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    iput-wide p3, p0, Lcom/jscape/util/k/g;->c:J

    return-void
.end method

.method public constructor <init>(Lcom/jscape/util/k/TransportAddress;Lcom/jscape/util/k/TransportAddress;Lcom/jscape/util/Time;)V
    .locals 2

    invoke-virtual {p3}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/jscape/util/k/g;-><init>(Lcom/jscape/util/k/TransportAddress;Lcom/jscape/util/k/TransportAddress;J)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/util/k/g;)V
    .locals 4

    iget-object v0, p1, Lcom/jscape/util/k/g;->a:Lcom/jscape/util/k/TransportAddress;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/jscape/util/k/TransportAddress;

    iget-object v1, p1, Lcom/jscape/util/k/g;->a:Lcom/jscape/util/k/TransportAddress;

    invoke-direct {v0, v1}, Lcom/jscape/util/k/TransportAddress;-><init>(Lcom/jscape/util/k/TransportAddress;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    new-instance v1, Lcom/jscape/util/k/TransportAddress;

    iget-object v2, p1, Lcom/jscape/util/k/g;->b:Lcom/jscape/util/k/TransportAddress;

    invoke-direct {v1, v2}, Lcom/jscape/util/k/TransportAddress;-><init>(Lcom/jscape/util/k/TransportAddress;)V

    iget-wide v2, p1, Lcom/jscape/util/k/g;->c:J

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/jscape/util/k/g;-><init>(Lcom/jscape/util/k/TransportAddress;Lcom/jscape/util/k/TransportAddress;J)V

    return-void
.end method


# virtual methods
.method public a()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/g;->a:Lcom/jscape/util/k/TransportAddress;

    return-object v0
.end method

.method public a(J)V
    .locals 3

    sget-object v0, Lcom/jscape/util/k/g;->d:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    const-wide/16 v1, 0x0

    invoke-static {p1, p2, v1, v2, v0}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    iput-wide p1, p0, Lcom/jscape/util/k/g;->c:J

    return-void
.end method

.method public a(Lcom/jscape/util/k/TransportAddress;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/util/k/g;->a:Lcom/jscape/util/k/TransportAddress;

    return-void
.end method

.method public b()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/g;->b:Lcom/jscape/util/k/TransportAddress;

    return-object v0
.end method

.method public b(Lcom/jscape/util/k/TransportAddress;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/g;->b:Lcom/jscape/util/k/TransportAddress;

    return-void
.end method

.method public c()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/util/k/g;->c:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/k/TransportAddress;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    move-object v1, p1

    goto :goto_0

    :cond_1
    move-object v1, p0

    :goto_0
    if-eqz v1, :cond_4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    if-eq v1, v0, :cond_3

    goto :goto_1

    :cond_2
    move-object p1, v1

    :cond_3
    check-cast p1, Lcom/jscape/util/k/g;

    iget-object v0, p0, Lcom/jscape/util/k/g;->b:Lcom/jscape/util/k/TransportAddress;

    iget-object p1, p1, Lcom/jscape/util/k/g;->b:Lcom/jscape/util/k/TransportAddress;

    invoke-virtual {v0, p1}, Lcom/jscape/util/k/TransportAddress;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_4
    :goto_1
    const/4 p1, 0x0

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/g;->b:Lcom/jscape/util/k/TransportAddress;

    invoke-virtual {v0}, Lcom/jscape/util/k/TransportAddress;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/k/g;->d:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/k/g;->a:Lcom/jscape/util/k/TransportAddress;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/k/g;->b:Lcom/jscape/util/k/TransportAddress;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/jscape/util/k/g;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
