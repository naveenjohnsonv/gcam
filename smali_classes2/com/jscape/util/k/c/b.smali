.class Lcom/jscape/util/k/c/b;
.super Ljava/io/OutputStream;


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/nio/ByteBuffer;

.field private final b:[B

.field final c:Lcom/jscape/util/k/c/j;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x11

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/4 v6, 0x1

    add-int/2addr v4, v6

    add-int/2addr v3, v4

    const-string v7, "\u000c\u0016r,J?,6\u0011 *G=\u007f:\u0006.\u0011\u000c\u0016r,J?,6\u0011 *G=\u007f:\u0006."

    invoke-virtual {v7, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v8, v4

    move v9, v2

    :goto_1
    if-gt v8, v9, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x23

    if-ge v3, v4, :cond_0

    invoke-virtual {v7, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/k/c/b;->d:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v10, v4, v9

    rem-int/lit8 v11, v9, 0x7

    const/16 v12, 0x7b

    if-eqz v11, :cond_7

    if-eq v11, v6, :cond_6

    if-eq v11, v0, :cond_5

    const/4 v13, 0x3

    if-eq v11, v13, :cond_4

    const/4 v13, 0x4

    if-eq v11, v13, :cond_3

    const/4 v13, 0x5

    if-eq v11, v13, :cond_2

    const/16 v11, 0x77

    goto :goto_2

    :cond_2
    const/16 v11, 0x29

    goto :goto_2

    :cond_3
    const/16 v11, 0x50

    goto :goto_2

    :cond_4
    const/16 v11, 0x32

    goto :goto_2

    :cond_5
    move v11, v12

    goto :goto_2

    :cond_6
    const/16 v11, 0x19

    goto :goto_2

    :cond_7
    const/16 v11, 0x24

    :goto_2
    xor-int/2addr v11, v12

    xor-int/2addr v10, v11

    int-to-char v10, v10

    aput-char v10, v4, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Lcom/jscape/util/k/c/j;)V
    .locals 1

    iput-object p1, p0, Lcom/jscape/util/k/c/b;->c:Lcom/jscape/util/k/c/j;

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    sget-object v0, Lcom/jscape/util/k/c/c;->b:Lcom/jscape/util/k/c/c;

    invoke-static {p1}, Lcom/jscape/util/k/c/j;->a(Lcom/jscape/util/k/c/j;)Ljavax/net/ssl/SSLEngine;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jscape/util/k/c/c;->b(Ljavax/net/ssl/SSLEngine;)Ljava/nio/ByteBuffer;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/util/k/c/b;->a:Ljava/nio/ByteBuffer;

    const/4 p1, 0x1

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/jscape/util/k/c/b;->b:[B

    return-void
.end method

.method constructor <init>(Lcom/jscape/util/k/c/j;Lcom/jscape/util/k/c/k;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/util/k/c/b;-><init>(Lcom/jscape/util/k/c/j;)V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/c/b;->c:Lcom/jscape/util/k/c/j;

    iget-object v1, p0, Lcom/jscape/util/k/c/b;->a:Ljava/nio/ByteBuffer;

    invoke-static {v0, v1}, Lcom/jscape/util/k/c/j;->b(Lcom/jscape/util/k/c/j;Ljava/nio/ByteBuffer;)V

    return-void
.end method

.method private a(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/k/c/b;->c:Lcom/jscape/util/k/c/j;

    invoke-static {v1}, Lcom/jscape/util/k/c/j;->e(Lcom/jscape/util/k/c/j;)Lcom/jscape/util/k/c/n;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jscape/util/k/c/n;->a()Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_0
    move p1, v1

    :goto_0
    if-gtz p1, :cond_2

    :cond_1
    return-void

    :cond_2
    :try_start_1
    new-instance p1, Ljava/io/IOException;

    sget-object v0, Lcom/jscape/util/k/c/b;->d:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/c/b;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/c/b;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method private a([BII)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/c/b;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/jscape/util/k/c/b;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result p3

    iget-object v0, p0, Lcom/jscape/util/k/c/b;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, p2, p3}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    iget-object p1, p0, Lcom/jscape/util/k/c/b;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    return-void
.end method

.method private b()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/k/c/b;->c:Lcom/jscape/util/k/c/j;

    invoke-static {v0}, Lcom/jscape/util/k/c/j;->e(Lcom/jscape/util/k/c/j;)Lcom/jscape/util/k/c/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/util/k/c/n;->a()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lcom/jscape/util/k/c/b;->d:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/k/c/b;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/c/b;->c:Lcom/jscape/util/k/c/j;

    invoke-static {v0}, Lcom/jscape/util/k/c/j;->d(Lcom/jscape/util/k/c/j;)V

    return-void
.end method

.method public write(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/c/b;->b:[B

    int-to-byte p1, p1

    const/4 v1, 0x0

    aput-byte p1, v0, v1

    invoke-virtual {p0, v0}, Lcom/jscape/util/k/c/b;->write([B)V

    return-void
.end method

.method public write([BII)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/util/k/c/b;->b()V

    :cond_0
    if-lez p3, :cond_1

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/util/k/c/b;->a([BII)V

    iget-object v1, p0, Lcom/jscape/util/k/c/b;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    sub-int/2addr p3, v1

    iget-object v1, p0, Lcom/jscape/util/k/c/b;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    add-int/2addr p2, v1

    invoke-direct {p0}, Lcom/jscape/util/k/c/b;->a()V

    invoke-direct {p0, p3}, Lcom/jscape/util/k/c/b;->a(I)V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method
