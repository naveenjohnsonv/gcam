.class Lcom/jscape/util/k/c/l;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljavax/net/ssl/SSLEngine;

.field private final b:Lcom/jscape/util/k/c/m;

.field private final c:Lcom/jscape/util/k/c/n;

.field private d:Ljava/nio/ByteBuffer;


# direct methods
.method private constructor <init>(Ljavax/net/ssl/SSLEngine;Lcom/jscape/util/k/c/m;Lcom/jscape/util/k/c/n;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/k/c/l;->a:Ljavax/net/ssl/SSLEngine;

    iput-object p2, p0, Lcom/jscape/util/k/c/l;->b:Lcom/jscape/util/k/c/m;

    iput-object p3, p0, Lcom/jscape/util/k/c/l;->c:Lcom/jscape/util/k/c/n;

    return-void
.end method

.method constructor <init>(Ljavax/net/ssl/SSLEngine;Lcom/jscape/util/k/c/m;Lcom/jscape/util/k/c/n;Lcom/jscape/util/k/c/k;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/util/k/c/l;-><init>(Ljavax/net/ssl/SSLEngine;Lcom/jscape/util/k/c/m;Lcom/jscape/util/k/c/n;)V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private c()Z
    .locals 3

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/k/c/l;->a:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v1

    sget-object v2, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NOT_HANDSHAKING:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-eqz v0, :cond_0

    if-eq v1, v2, :cond_1

    sget-object v2, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->FINISHED:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    :cond_0
    if-eq v1, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private d()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    sget-object v1, Lcom/jscape/util/k/c/c;->b:Lcom/jscape/util/k/c/c;

    iget-object v2, p0, Lcom/jscape/util/k/c/l;->a:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v1, v2}, Lcom/jscape/util/k/c/c;->b(Ljavax/net/ssl/SSLEngine;)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/util/k/c/l;->d:Ljava/nio/ByteBuffer;

    move-object v1, p0

    :cond_0
    invoke-direct {v1}, Lcom/jscape/util/k/c/l;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {v1}, Lcom/jscape/util/k/c/l;->f()V

    invoke-direct {v1}, Lcom/jscape/util/k/c/l;->h()V

    goto :goto_3

    :cond_1
    :goto_0
    :try_start_0
    invoke-direct {v1}, Lcom/jscape/util/k/c/l;->g()Z

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_3

    if-eqz v2, :cond_2

    :try_start_1
    invoke-direct {v1}, Lcom/jscape/util/k/c/l;->h()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    if-nez v0, :cond_6

    :cond_2
    :try_start_2
    invoke-direct {v1}, Lcom/jscape/util/k/c/l;->i()Z

    move-result v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_5

    :cond_3
    :goto_1
    if-eqz v0, :cond_4

    if-eqz v2, :cond_6

    :try_start_3
    invoke-direct {v1}, Lcom/jscape/util/k/c/l;->j()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_4

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/k/c/l;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_4
    :goto_2
    if-nez v2, :cond_0

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/jscape/util/k/c/l;->d:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_5

    return-void

    :cond_5
    :goto_3
    if-nez v0, :cond_6

    goto :goto_0

    :cond_6
    :goto_4
    invoke-direct {v1}, Lcom/jscape/util/k/c/l;->c()Z

    move-result v2

    goto :goto_2

    :catch_2
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/util/k/c/l;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/util/k/c/l;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    :goto_5
    invoke-static {v0}, Lcom/jscape/util/k/c/l;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method private e()Z
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/k/c/l;->a:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v0

    sget-object v1, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_TASK:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private f()V
    .locals 2

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/jscape/util/k/c/l;->a:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngine;->getDelegatedTask()Ljava/lang/Runnable;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method private g()Z
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/k/c/l;->a:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v0

    sget-object v1, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private h()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/c/l;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/jscape/util/k/c/l;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/jscape/util/k/c/l;->c:Lcom/jscape/util/k/c/n;

    iget-object v1, p0, Lcom/jscape/util/k/c/l;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Lcom/jscape/util/k/c/n;->a(Ljava/nio/ByteBuffer;)V

    return-void
.end method

.method private i()Z
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/k/c/l;->a:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v0

    sget-object v1, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_UNWRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private j()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/c/l;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/jscape/util/k/c/l;->b:Lcom/jscape/util/k/c/m;

    iget-object v1, p0, Lcom/jscape/util/k/c/l;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Lcom/jscape/util/k/c/m;->a(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/util/k/c/l;->d:Ljava/nio/ByteBuffer;

    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/util/k/c/l;->c()Z

    move-result v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/util/k/c/l;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/util/k/c/l;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/jscape/util/k/c/l;->d()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    sget-object v1, Lcom/jscape/util/k/c/c;->b:Lcom/jscape/util/k/c/c;

    iget-object v2, p0, Lcom/jscape/util/k/c/l;->a:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v1, v2}, Lcom/jscape/util/k/c/c;->b(Ljavax/net/ssl/SSLEngine;)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/util/k/c/l;->d:Ljava/nio/ByteBuffer;

    :cond_0
    invoke-direct {p0}, Lcom/jscape/util/k/c/l;->h()V

    :cond_1
    iget-object v1, p0, Lcom/jscape/util/k/c/l;->a:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v1}, Ljavax/net/ssl/SSLEngine;->isOutboundDone()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/jscape/util/k/c/l;->d:Ljava/nio/ByteBuffer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
