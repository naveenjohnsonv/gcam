.class public Lcom/jscape/util/k/c/h;
.super Ljava/lang/Object;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "\u001cBk\t a\u0008!TD \'c\u000f;af>/k\u0004;Tu?n}\u0004!Pe +b\"&Ao)<U\u0014&Eb?s"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/k/c/h;->b:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x46

    goto :goto_1

    :cond_1
    const/16 v4, 0x21

    goto :goto_1

    :cond_2
    const/16 v4, 0x69

    goto :goto_1

    :cond_3
    const/16 v4, 0x6b

    goto :goto_1

    :cond_4
    const/16 v4, 0x20

    goto :goto_1

    :cond_5
    const/16 v4, 0x16

    goto :goto_1

    :cond_6
    const/16 v4, 0x68

    :goto_1
    const/16 v5, 0x27

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public varargs constructor <init>([Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/k/c/h;->a:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Ljavax/net/ssl/SSLEngine;)V
    .locals 2

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Ljavax/net/ssl/SSLEngine;->setUseClientMode(Z)V

    iget-object v1, p0, Lcom/jscape/util/k/c/h;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    array-length v0, v1

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/jscape/util/k/c/h;->a:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljavax/net/ssl/SSLEngine;->setEnabledCipherSuites([Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/k/c/h;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/k/c/h;->a:[Ljava/lang/String;

    invoke-static {v1}, Lcom/jscape/util/v;->d([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
