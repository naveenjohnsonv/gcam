.class Lcom/jscape/util/k/c/a;
.super Ljava/io/InputStream;


# instance fields
.field private a:Ljava/nio/ByteBuffer;

.field private final b:[B

.field final c:Lcom/jscape/util/k/c/j;


# direct methods
.method private constructor <init>(Lcom/jscape/util/k/c/j;)V
    .locals 1

    iput-object p1, p0, Lcom/jscape/util/k/c/a;->c:Lcom/jscape/util/k/c/j;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    sget-object v0, Lcom/jscape/util/k/c/c;->b:Lcom/jscape/util/k/c/c;

    invoke-static {p1}, Lcom/jscape/util/k/c/j;->a(Lcom/jscape/util/k/c/j;)Ljavax/net/ssl/SSLEngine;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jscape/util/k/c/c;->b(Ljavax/net/ssl/SSLEngine;)Ljava/nio/ByteBuffer;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/util/k/c/a;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    const/4 p1, 0x1

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/jscape/util/k/c/a;->b:[B

    return-void
.end method

.method constructor <init>(Lcom/jscape/util/k/c/j;Lcom/jscape/util/k/c/k;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jscape/util/k/c/a;-><init>(Lcom/jscape/util/k/c/j;)V

    return-void
.end method

.method private a([BII)I
    .locals 2

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/k/c/a;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    iget-object v0, p0, Lcom/jscape/util/k/c/a;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    :cond_1
    iget-object p3, p0, Lcom/jscape/util/k/c/a;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {p3, p1, p2, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    return v1
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/k/c/a;->a:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/jscape/util/k/c/a;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/util/k/c/a;->c:Lcom/jscape/util/k/c/j;

    iget-object v1, p0, Lcom/jscape/util/k/c/a;->a:Ljava/nio/ByteBuffer;

    invoke-static {v0, v1}, Lcom/jscape/util/k/c/j;->a(Lcom/jscape/util/k/c/j;Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/util/k/c/a;->a:Ljava/nio/ByteBuffer;

    :cond_1
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/util/k/c/a;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/k/c/a;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method private a(I)Z
    .locals 1

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :cond_1
    :goto_0
    return p1
.end method


# virtual methods
.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/c/a;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    return v0
.end method

.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/c/a;->c:Lcom/jscape/util/k/c/j;

    invoke-static {v0}, Lcom/jscape/util/k/c/j;->c(Lcom/jscape/util/k/c/j;)V

    return-void
.end method

.method public read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/jscape/util/k/c/a;->b:[B

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3, v2}, Lcom/jscape/util/k/c/a;->read([BII)I

    move-result v1

    :cond_1
    if-eqz v1, :cond_0

    if-eqz v0, :cond_1

    const/4 v2, -0x1

    if-eqz v0, :cond_2

    if-eq v1, v2, :cond_3

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/k/c/a;->b:[B

    aget-byte v1, v0, v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v2, 0xff

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/k/c/a;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_2
    :goto_0
    and-int/2addr v2, v1

    :cond_3
    return v2
.end method

.method public read([BII)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/k/c/a;->c:Lcom/jscape/util/k/c/j;

    invoke-static {v1}, Lcom/jscape/util/k/c/j;->b(Lcom/jscape/util/k/c/j;)Lcom/jscape/util/k/c/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jscape/util/k/c/m;->a()Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    invoke-direct {p0}, Lcom/jscape/util/k/c/a;->a()V

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/util/k/c/a;->a([BII)I

    move-result v1

    :cond_1
    return v1

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/util/k/c/a;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/c/a;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method
