.class Lcom/jscape/util/k/c/m;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljavax/net/ssl/SSLEngine;

.field private final b:Ljava/nio/channels/ReadableByteChannel;

.field private c:Ljava/nio/ByteBuffer;

.field private d:Ljavax/net/ssl/SSLEngineResult;

.field private e:Z


# direct methods
.method private constructor <init>(Ljavax/net/ssl/SSLEngine;Ljava/io/InputStream;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/k/c/m;->a:Ljavax/net/ssl/SSLEngine;

    invoke-static {p2}, Ljava/nio/channels/Channels;->newChannel(Ljava/io/InputStream;)Ljava/nio/channels/ReadableByteChannel;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/util/k/c/m;->b:Ljava/nio/channels/ReadableByteChannel;

    sget-object p1, Lcom/jscape/util/k/c/c;->a:Lcom/jscape/util/k/c/c;

    iget-object p2, p0, Lcom/jscape/util/k/c/m;->a:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {p1, p2}, Lcom/jscape/util/k/c/c;->b(Ljavax/net/ssl/SSLEngine;)Ljava/nio/ByteBuffer;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/util/k/c/m;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    return-void
.end method

.method constructor <init>(Ljavax/net/ssl/SSLEngine;Ljava/io/InputStream;Lcom/jscape/util/k/c/k;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/util/k/c/m;-><init>(Ljavax/net/ssl/SSLEngine;Ljava/io/InputStream;)V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private b(Ljava/nio/ByteBuffer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/c/m;->a:Ljavax/net/ssl/SSLEngine;

    iget-object v1, p0, Lcom/jscape/util/k/c/m;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, p1}, Ljavax/net/ssl/SSLEngine;->unwrap(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/util/k/c/m;->d:Ljavax/net/ssl/SSLEngineResult;

    return-void
.end method

.method private b()Z
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/k/c/m;->d:Ljavax/net/ssl/SSLEngineResult;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v0

    sget-object v1, Ljavax/net/ssl/SSLEngineResult$Status;->BUFFER_OVERFLOW:Ljavax/net/ssl/SSLEngineResult$Status;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private c(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    sget-object v0, Lcom/jscape/util/k/c/c;->b:Lcom/jscape/util/k/c/c;

    iget-object v1, p0, Lcom/jscape/util/k/c/m;->a:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0, v1, p1}, Lcom/jscape/util/k/c/c;->a(Ljavax/net/ssl/SSLEngine;Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object p1

    return-object p1
.end method

.method private c()Z
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/k/c/m;->d:Ljavax/net/ssl/SSLEngineResult;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v0

    sget-object v1, Ljavax/net/ssl/SSLEngineResult$Status;->BUFFER_UNDERFLOW:Ljavax/net/ssl/SSLEngineResult$Status;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private d()V
    .locals 4

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/k/c/m;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/jscape/util/k/c/m;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    if-ne v1, v2, :cond_0

    sget-object v1, Lcom/jscape/util/k/c/c;->a:Lcom/jscape/util/k/c/c;

    iget-object v2, p0, Lcom/jscape/util/k/c/m;->a:Ljavax/net/ssl/SSLEngine;

    iget-object v3, p0, Lcom/jscape/util/k/c/m;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v2, v3}, Lcom/jscape/util/k/c/c;->a(Ljavax/net/ssl/SSLEngine;Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/util/k/c/m;->c:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_3

    :cond_0
    iget-object v1, p0, Lcom/jscape/util/k/c/m;->c:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    :cond_1
    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jscape/util/k/c/m;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/jscape/util/k/c/m;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    :cond_3
    return-void
.end method

.method private e()Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/util/k/c/m;->b()Z

    move-result v1

    if-eqz v0, :cond_2

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/jscape/util/k/c/m;->c()Z

    move-result v1

    if-eqz v0, :cond_2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move v0, v1

    :goto_1
    return v0
.end method

.method private f()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/c/m;->b:Ljava/nio/channels/ReadableByteChannel;

    iget-object v1, p0, Lcom/jscape/util/k/c/m;->c:Ljava/nio/ByteBuffer;

    invoke-interface {v0, v1}, Ljava/nio/channels/ReadableByteChannel;->read(Ljava/nio/ByteBuffer;)I

    iget-object v0, p0, Lcom/jscape/util/k/c/m;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    return-void
.end method

.method private g()V
    .locals 2

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/util/k/c/m;->d:Ljavax/net/ssl/SSLEngineResult;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v0

    sget-object v1, Ljavax/net/ssl/SSLEngineResult$Status;->CLOSED:Ljavax/net/ssl/SSLEngineResult$Status;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/util/k/c/m;->e:Z

    :cond_1
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    move-object v1, p0

    :cond_0
    invoke-direct {v1, p1}, Lcom/jscape/util/k/c/m;->b(Ljava/nio/ByteBuffer;)V

    invoke-direct {v1}, Lcom/jscape/util/k/c/m;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {v1, p1}, Lcom/jscape/util/k/c/m;->c(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :cond_1
    :goto_0
    :try_start_1
    invoke-direct {v1}, Lcom/jscape/util/k/c/m;->c()Z

    move-result v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_2

    if-eqz v2, :cond_4

    :try_start_2
    invoke-direct {v1}, Lcom/jscape/util/k/c/m;->d()V

    invoke-direct {v1}, Lcom/jscape/util/k/c/m;->f()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :cond_2
    :goto_1
    if-nez v2, :cond_0

    :try_start_3
    invoke-direct {v1}, Lcom/jscape/util/k/c/m;->g()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_3

    monitor-exit p0

    return-object p1

    :cond_3
    :goto_2
    if-nez v0, :cond_4

    goto :goto_0

    :cond_4
    :goto_3
    :try_start_4
    invoke-direct {v1}, Lcom/jscape/util/k/c/m;->e()Z

    move-result v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/util/k/c/m;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_1
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/util/k/c/m;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/util/k/c/m;->e:Z

    return v0
.end method
