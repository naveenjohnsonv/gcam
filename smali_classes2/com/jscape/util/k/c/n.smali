.class Lcom/jscape/util/k/c/n;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljavax/net/ssl/SSLEngine;

.field private final b:Ljava/nio/channels/WritableByteChannel;

.field private c:Ljava/nio/ByteBuffer;

.field private d:Ljavax/net/ssl/SSLEngineResult;

.field private e:Z


# direct methods
.method private constructor <init>(Ljavax/net/ssl/SSLEngine;Ljava/io/OutputStream;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/k/c/n;->a:Ljavax/net/ssl/SSLEngine;

    invoke-static {p2}, Ljava/nio/channels/Channels;->newChannel(Ljava/io/OutputStream;)Ljava/nio/channels/WritableByteChannel;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/util/k/c/n;->b:Ljava/nio/channels/WritableByteChannel;

    sget-object p1, Lcom/jscape/util/k/c/c;->a:Lcom/jscape/util/k/c/c;

    iget-object p2, p0, Lcom/jscape/util/k/c/n;->a:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {p1, p2}, Lcom/jscape/util/k/c/c;->b(Ljavax/net/ssl/SSLEngine;)Ljava/nio/ByteBuffer;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/util/k/c/n;->c:Ljava/nio/ByteBuffer;

    return-void
.end method

.method constructor <init>(Ljavax/net/ssl/SSLEngine;Ljava/io/OutputStream;Lcom/jscape/util/k/c/k;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/util/k/c/n;-><init>(Ljavax/net/ssl/SSLEngine;Ljava/io/OutputStream;)V

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/k/c/n;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/util/k/c/n;->b:Ljava/nio/channels/WritableByteChannel;

    iget-object v1, p0, Lcom/jscape/util/k/c/n;->c:Ljava/nio/ByteBuffer;

    invoke-interface {v0, v1}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I

    :cond_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/k/c/n;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/k/c/n;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method private b(Ljava/nio/ByteBuffer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/k/c/n;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    :cond_0
    invoke-direct {p0, p1}, Lcom/jscape/util/k/c/n;->c(Ljava/nio/ByteBuffer;)V

    invoke-direct {p0}, Lcom/jscape/util/k/c/n;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/jscape/util/k/c/n;->d()V

    :cond_1
    invoke-direct {p0}, Lcom/jscape/util/k/c/n;->c()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/jscape/util/k/c/n;->e()V

    if-eqz v0, :cond_1

    return-void
.end method

.method private c(Ljava/nio/ByteBuffer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/c/n;->a:Ljavax/net/ssl/SSLEngine;

    iget-object v1, p0, Lcom/jscape/util/k/c/n;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, v1}, Ljavax/net/ssl/SSLEngine;->wrap(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/util/k/c/n;->d:Ljavax/net/ssl/SSLEngineResult;

    iget-object p1, p0, Lcom/jscape/util/k/c/n;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    return-void
.end method

.method private c()Z
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/k/c/n;->d:Ljavax/net/ssl/SSLEngineResult;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v0

    sget-object v1, Ljavax/net/ssl/SSLEngineResult$Status;->BUFFER_OVERFLOW:Ljavax/net/ssl/SSLEngineResult$Status;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private d()V
    .locals 3

    sget-object v0, Lcom/jscape/util/k/c/c;->a:Lcom/jscape/util/k/c/c;

    iget-object v1, p0, Lcom/jscape/util/k/c/n;->a:Ljavax/net/ssl/SSLEngine;

    iget-object v2, p0, Lcom/jscape/util/k/c/n;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, Lcom/jscape/util/k/c/c;->a(Ljavax/net/ssl/SSLEngine;Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/jscape/util/k/c/n;->c:Ljava/nio/ByteBuffer;

    return-void
.end method

.method private e()V
    .locals 2

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/util/k/c/n;->d:Ljavax/net/ssl/SSLEngineResult;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngineResult;->getStatus()Ljavax/net/ssl/SSLEngineResult$Status;

    move-result-object v0

    sget-object v1, Ljavax/net/ssl/SSLEngineResult$Status;->CLOSED:Ljavax/net/ssl/SSLEngineResult$Status;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/util/k/c/n;->e:Z

    :cond_1
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/nio/ByteBuffer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/jscape/util/k/c/n;->b(Ljava/nio/ByteBuffer;)V

    invoke-direct {p0}, Lcom/jscape/util/k/c/n;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/util/k/c/n;->e:Z

    return v0
.end method
