.class public Lcom/jscape/util/k/c/j;
.super Ljava/lang/Object;


# static fields
.field private static g:[I


# instance fields
.field private final a:Ljavax/net/ssl/SSLEngine;

.field private final b:Lcom/jscape/util/k/c/m;

.field private final c:Lcom/jscape/util/k/c/n;

.field private final d:Lcom/jscape/util/k/c/l;

.field private final e:Ljava/io/InputStream;

.field private final f:Ljava/io/OutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [I

    invoke-static {v0}, Lcom/jscape/util/k/c/j;->b([I)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljavax/net/ssl/SSLEngine;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/c/j;->a:Ljavax/net/ssl/SSLEngine;

    new-instance v1, Lcom/jscape/util/k/c/m;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, v2}, Lcom/jscape/util/k/c/m;-><init>(Ljavax/net/ssl/SSLEngine;Ljava/io/InputStream;Lcom/jscape/util/k/c/k;)V

    iput-object v1, p0, Lcom/jscape/util/k/c/j;->b:Lcom/jscape/util/k/c/m;

    new-instance p2, Lcom/jscape/util/k/c/n;

    invoke-direct {p2, p1, p3, v2}, Lcom/jscape/util/k/c/n;-><init>(Ljavax/net/ssl/SSLEngine;Ljava/io/OutputStream;Lcom/jscape/util/k/c/k;)V

    iput-object p2, p0, Lcom/jscape/util/k/c/j;->c:Lcom/jscape/util/k/c/n;

    new-instance p2, Lcom/jscape/util/k/c/l;

    iget-object p3, p0, Lcom/jscape/util/k/c/j;->b:Lcom/jscape/util/k/c/m;

    iget-object v1, p0, Lcom/jscape/util/k/c/j;->c:Lcom/jscape/util/k/c/n;

    invoke-direct {p2, p1, p3, v1, v2}, Lcom/jscape/util/k/c/l;-><init>(Ljavax/net/ssl/SSLEngine;Lcom/jscape/util/k/c/m;Lcom/jscape/util/k/c/n;Lcom/jscape/util/k/c/k;)V

    iput-object p2, p0, Lcom/jscape/util/k/c/j;->d:Lcom/jscape/util/k/c/l;

    new-instance p1, Lcom/jscape/util/k/c/a;

    invoke-direct {p1, p0, v2}, Lcom/jscape/util/k/c/a;-><init>(Lcom/jscape/util/k/c/j;Lcom/jscape/util/k/c/k;)V

    iput-object p1, p0, Lcom/jscape/util/k/c/j;->e:Ljava/io/InputStream;

    new-instance p1, Lcom/jscape/util/k/c/b;

    invoke-direct {p1, p0, v2}, Lcom/jscape/util/k/c/b;-><init>(Lcom/jscape/util/k/c/j;Lcom/jscape/util/k/c/k;)V

    iput-object p1, p0, Lcom/jscape/util/k/c/j;->f:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    const/4 p1, 0x3

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method static a(Lcom/jscape/util/k/c/j;Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/util/k/c/j;->b(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object p0

    return-object p0
.end method

.method static a(Lcom/jscape/util/k/c/j;)Ljavax/net/ssl/SSLEngine;
    .locals 0

    iget-object p0, p0, Lcom/jscape/util/k/c/j;->a:Ljavax/net/ssl/SSLEngine;

    return-object p0
.end method

.method private a(Ljava/nio/ByteBuffer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/jscape/util/k/c/j;->c:Lcom/jscape/util/k/c/n;

    invoke-virtual {v1}, Lcom/jscape/util/k/c/n;->a()Z

    move-result v1

    if-nez v1, :cond_1

    :try_start_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jscape/util/k/c/j;->c:Lcom/jscape/util/k/c/n;

    invoke-virtual {v1, p1}, Lcom/jscape/util/k/c/n;->a(Ljava/nio/ByteBuffer;)V

    iget-object v1, p0, Lcom/jscape/util/k/c/j;->c:Lcom/jscape/util/k/c/n;

    invoke-virtual {v1}, Lcom/jscape/util/k/c/n;->a()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/jscape/util/k/c/j;->a(Z)V

    invoke-direct {p0}, Lcom/jscape/util/k/c/j;->c()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/c/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method private a(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_0

    :try_start_0
    iget-object p1, p0, Lcom/jscape/util/k/c/j;->d:Lcom/jscape/util/k/c/l;

    invoke-virtual {p1}, Lcom/jscape/util/k/c/l;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/c/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    return-void
.end method

.method static b(Lcom/jscape/util/k/c/j;)Lcom/jscape/util/k/c/m;
    .locals 0

    iget-object p0, p0, Lcom/jscape/util/k/c/j;->b:Lcom/jscape/util/k/c/m;

    return-object p0
.end method

.method private b(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/jscape/util/k/c/j;->b:Lcom/jscape/util/k/c/m;

    invoke-virtual {v1}, Lcom/jscape/util/k/c/m;->a()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_2

    if-eqz v0, :cond_2

    :try_start_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/jscape/util/k/c/j;->b:Lcom/jscape/util/k/c/m;

    invoke-virtual {v1, p1}, Lcom/jscape/util/k/c/m;->a(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object p1

    iget-object v1, p0, Lcom/jscape/util/k/c/j;->b:Lcom/jscape/util/k/c/m;

    invoke-virtual {v1}, Lcom/jscape/util/k/c/m;->a()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/jscape/util/k/c/j;->a(Z)V

    invoke-direct {p0}, Lcom/jscape/util/k/c/j;->c()V

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/c/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    :cond_2
    return-object p1
.end method

.method static b(Lcom/jscape/util/k/c/j;Ljava/nio/ByteBuffer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/jscape/util/k/c/j;->a(Ljava/nio/ByteBuffer;)V

    return-void
.end method

.method public static b([I)V
    .locals 0

    sput-object p0, Lcom/jscape/util/k/c/j;->g:[I

    return-void
.end method

.method private c()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/c/j;->d:Lcom/jscape/util/k/c/l;

    invoke-virtual {v0}, Lcom/jscape/util/k/c/l;->a()V

    return-void
.end method

.method static c(Lcom/jscape/util/k/c/j;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/jscape/util/k/c/j;->d()V

    return-void
.end method

.method private d()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/c/j;->a:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->closeInbound()V

    return-void
.end method

.method static d(Lcom/jscape/util/k/c/j;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/util/k/c/j;->e()V

    return-void
.end method

.method static e(Lcom/jscape/util/k/c/j;)Lcom/jscape/util/k/c/n;
    .locals 0

    iget-object p0, p0, Lcom/jscape/util/k/c/j;->c:Lcom/jscape/util/k/c/n;

    return-object p0
.end method

.method private e()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/c/j;->a:Ljavax/net/ssl/SSLEngine;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->closeOutbound()V

    return-void
.end method

.method public static f()[I
    .locals 1

    sget-object v0, Lcom/jscape/util/k/c/j;->g:[I

    return-object v0
.end method


# virtual methods
.method public a()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/c/j;->e:Ljava/io/InputStream;

    return-object v0
.end method

.method public b()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/c/j;->f:Ljava/io/OutputStream;

    return-object v0
.end method
