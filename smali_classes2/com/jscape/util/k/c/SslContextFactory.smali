.class public Lcom/jscape/util/k/c/SslContextFactory;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/16 v0, 0x12

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x7

    const/4 v4, 0x0

    const-string v5, "kZwdN#@\u0014LsWa\u0016b\u001cwyWf/b\u0000KUEq\u0017h\u000cLsWa\u0016b\u001cLT][;\u0003u]w\u0003u]w\u0003OcP\u0003OcP\u001ePdC<\u001db\u0007Qu]q\u001e~\u0006Ss\nf\u0013~\\lsWa\u0016b\u001cvR\u0003XsP\u0003XsP\u0005\u001ae\u001e7\u000c\u0007kZwdN#@\u0004g#\u0014+\u0004g#\u0014+\u0005\u001ae\u001e7\u000c\u000eLsWa\u0016b\u001cLT]B\u001ah\u0000"

    const/16 v6, 0x8d

    move v8, v4

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x1e

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v2

    invoke-virtual {v5, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v4

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v2

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v2, 0xe

    const-string v5, "NqUc\u0014`\u001eNV_@\u0018j\u0002\u0003Vq_"

    move v6, v0

    move v8, v11

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v2

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v11

    :goto_3
    const/16 v9, 0x1c

    add-int/2addr v7, v10

    add-int v11, v7, v2

    invoke-virtual {v5, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v4

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/k/c/SslContextFactory;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v0, v14, 0x7

    if-eqz v0, :cond_9

    if-eq v0, v10, :cond_8

    const/4 v3, 0x2

    if-eq v0, v3, :cond_7

    const/4 v3, 0x3

    if-eq v0, v3, :cond_6

    const/4 v3, 0x4

    if-eq v0, v3, :cond_5

    const/4 v3, 0x5

    if-eq v0, v3, :cond_4

    const/16 v0, 0x6c

    goto :goto_4

    :cond_4
    const/16 v0, 0x13

    goto :goto_4

    :cond_5
    const/16 v0, 0x61

    goto :goto_4

    :cond_6
    const/16 v0, 0xc

    goto :goto_4

    :cond_7
    const/16 v0, 0x3a

    goto :goto_4

    :cond_8
    const/16 v0, 0x8

    goto :goto_4

    :cond_9
    const/16 v0, 0x21

    :goto_4
    xor-int/2addr v0, v9

    xor-int/2addr v0, v15

    int-to-char v0, v0

    aput-char v0, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/16 v0, 0x12

    goto :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private static varargs addKey(Ljava/security/KeyStore;Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/util/k/c/SslContextFactory;->a:[Ljava/lang/String;

    const/16 v1, 0x11

    aget-object v0, v0, v1

    const/4 v1, 0x0

    new-array v1, v1, [C

    invoke-virtual {p0, v0, p1, v1, p2}, Ljava/security/KeyStore;->setKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V

    return-void
.end method

.method private static varargs addTrustedCertificates(Ljava/security/KeyStore;[Ljava/security/cert/Certificate;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    array-length v1, p1

    const/4 v2, 0x0

    :cond_0
    if-ge v2, v1, :cond_1

    aget-object v3, p1, v2

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "-"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4, v3}, Ljava/security/KeyStore;->setCertificateEntry(Ljava/lang/String;Ljava/security/cert/Certificate;)V

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public static contextFor(Ljava/security/KeyStore;Ljava/lang/String;)Ljavax/net/ssl/SSLContext;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/c/c;->e()Z

    move-result v0

    invoke-static {p0, p1, v0}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor(Ljava/security/KeyStore;Ljava/lang/String;Z)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static contextFor(Ljava/security/KeyStore;Ljava/lang/String;Ljava/security/KeyStore;)Ljavax/net/ssl/SSLContext;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/c/c;->e()Z

    move-result v0

    invoke-static {p0, p1, p2, v0}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor(Ljava/security/KeyStore;Ljava/lang/String;Ljava/security/KeyStore;Z)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static contextFor(Ljava/security/KeyStore;Ljava/lang/String;Ljava/security/KeyStore;Z)Ljavax/net/ssl/SSLContext;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    invoke-virtual {p0}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {p0, v2}, Ljava/security/KeyStore;->isKeyEntry(Ljava/lang/String;)Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_0

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    invoke-virtual {p0, v2, p1}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object p1

    move-object v3, p1

    check-cast v3, Ljava/security/PrivateKey;

    invoke-virtual {p0, v2}, Ljava/security/KeyStore;->getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;

    move-result-object p0

    goto :goto_1

    :cond_2
    move-object p0, v3

    :goto_1
    invoke-static {v3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {p0}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {p2}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v0, :cond_4

    :try_start_2
    invoke-virtual {p2, v2}, Ljava/security/KeyStore;->isCertificateEntry(Ljava/lang/String;)Z

    move-result v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v4, :cond_5

    goto :goto_2

    :catch_2
    move-exception p0

    :try_start_3
    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_4
    :goto_2
    invoke-virtual {p2, v2}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    if-nez v0, :cond_3

    :cond_6
    const/4 p2, 0x0

    :try_start_4
    new-array p2, p2, [Ljava/security/cert/Certificate;

    invoke-interface {p1, p2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/security/cert/Certificate;

    invoke-static {v3, p0, p1, p3}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;[Ljava/security/cert/Certificate;Z)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_7

    const/4 p1, 0x3

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/k/c/j;->b([I)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    :cond_7
    return-object p0

    :catch_4
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
.end method

.method public static contextFor(Ljava/security/KeyStore;Ljava/lang/String;Z)Ljavax/net/ssl/SSLContext;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    invoke-virtual {p0}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {p0, v2}, Ljava/security/KeyStore;->isKeyEntry(Ljava/lang/String;)Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_0

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    invoke-virtual {p0, v2, p1}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object p1

    move-object v3, p1

    check-cast v3, Ljava/security/PrivateKey;

    invoke-virtual {p0, v2}, Ljava/security/KeyStore;->getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;

    move-result-object p0

    goto :goto_1

    :cond_2
    move-object p0, v3

    :goto_1
    invoke-static {v3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {p0}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {v3, p0, p2}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;Z)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static contextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;)Ljavax/net/ssl/SSLContext;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/k/c/f;

    invoke-direct {v0}, Lcom/jscape/util/k/c/f;-><init>()V

    invoke-static {p0, p1, v0}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;Ljavax/net/ssl/TrustManager;)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static contextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;Ljavax/net/ssl/TrustManager;)Ljavax/net/ssl/SSLContext;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/c/c;->e()Z

    move-result v0

    invoke-static {p0, p1, p2, v0}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;Ljavax/net/ssl/TrustManager;Z)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static contextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;Ljavax/net/ssl/TrustManager;Z)Ljavax/net/ssl/SSLContext;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {p3}, Lcom/jscape/util/c/c;->b(Z)Ljava/security/Provider;

    move-result-object v0

    invoke-static {p3}, Lcom/jscape/util/c/c;->c(Z)Ljava/security/KeyStore;

    move-result-object v1

    invoke-static {v1, p0, p1}, Lcom/jscape/util/k/c/SslContextFactory;->addKey(Ljava/security/KeyStore;Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;)V

    invoke-static {v0, v1, p3}, Lcom/jscape/util/k/c/SslContextFactory;->keyManagersFor(Ljava/security/Provider;Ljava/security/KeyStore;Z)[Ljavax/net/ssl/KeyManager;

    move-result-object p0

    const/4 p1, 0x1

    new-array p1, p1, [Ljavax/net/ssl/TrustManager;

    const/4 v1, 0x0

    aput-object p2, p1, v1

    invoke-static {p3}, Lcom/jscape/util/c/c;->d(Z)Ljava/security/SecureRandom;

    move-result-object p2

    invoke-static {v0, p0, p1, p2, p3}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor(Ljava/security/Provider;[Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;Z)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static contextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;Z)Ljavax/net/ssl/SSLContext;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/k/c/f;

    invoke-direct {v0}, Lcom/jscape/util/k/c/f;-><init>()V

    invoke-static {p0, p1, v0, p2}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;Ljavax/net/ssl/TrustManager;Z)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static contextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;[Ljava/security/cert/Certificate;)Ljavax/net/ssl/SSLContext;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/c/c;->e()Z

    move-result v0

    invoke-static {p0, p1, p2, v0}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;[Ljava/security/cert/Certificate;Z)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static contextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;[Ljava/security/cert/Certificate;Z)Ljavax/net/ssl/SSLContext;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {p3}, Lcom/jscape/util/c/c;->b(Z)Ljava/security/Provider;

    move-result-object v0

    invoke-static {p3}, Lcom/jscape/util/c/c;->c(Z)Ljava/security/KeyStore;

    move-result-object v1

    invoke-static {v1, p0, p1}, Lcom/jscape/util/k/c/SslContextFactory;->addKey(Ljava/security/KeyStore;Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;)V

    invoke-static {v1, p2}, Lcom/jscape/util/k/c/SslContextFactory;->addTrustedCertificates(Ljava/security/KeyStore;[Ljava/security/cert/Certificate;)V

    invoke-static {v0, v1, p3}, Lcom/jscape/util/k/c/SslContextFactory;->keyManagersFor(Ljava/security/Provider;Ljava/security/KeyStore;Z)[Ljavax/net/ssl/KeyManager;

    move-result-object p0

    invoke-static {v0, v1, p3}, Lcom/jscape/util/k/c/SslContextFactory;->trustManagersFor(Ljava/security/Provider;Ljava/security/KeyStore;Z)[Ljavax/net/ssl/TrustManager;

    move-result-object p1

    invoke-static {p3}, Lcom/jscape/util/c/c;->d(Z)Ljava/security/SecureRandom;

    move-result-object p2

    invoke-static {v0, p0, p1, p2, p3}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor(Ljava/security/Provider;[Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;Z)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static contextFor(Ljava/security/Provider;[Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;Z)Ljavax/net/ssl/SSLContext;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    const/16 v1, 0xb

    if-eqz p0, :cond_0

    :try_start_0
    sget-object v2, Lcom/jscape/util/k/c/SslContextFactory;->a:[Ljava/lang/String;

    aget-object v1, v2, v1

    invoke-static {v1, p0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/net/ssl/SSLContext;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_0
    sget-object p0, Lcom/jscape/util/k/c/SslContextFactory;->a:[Ljava/lang/String;

    aget-object p0, p0, v1

    invoke-static {p0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    :goto_0
    if-eqz v0, :cond_1

    if-nez p4, :cond_2

    :try_start_1
    invoke-virtual {p0, p1, p2, p3}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_1
    :goto_1
    if-nez v0, :cond_3

    :cond_2
    :try_start_2
    nop

    nop

    move-result-object p1

    invoke-static {p1}, Lcom/jscape/util/c/c;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_3
    :goto_2
    return-object p0
.end method

.method public static contextFor([Ljava/security/cert/Certificate;)Ljavax/net/ssl/SSLContext;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/c/c;->e()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor([Ljava/security/cert/Certificate;Z)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static contextFor([Ljava/security/cert/Certificate;Z)Ljavax/net/ssl/SSLContext;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/c/c;->b(Z)Ljava/security/Provider;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/util/c/c;->c(Z)Ljava/security/KeyStore;

    move-result-object v1

    invoke-static {v1, p0}, Lcom/jscape/util/k/c/SslContextFactory;->addTrustedCertificates(Ljava/security/KeyStore;[Ljava/security/cert/Certificate;)V

    invoke-static {v0, v1, p1}, Lcom/jscape/util/k/c/SslContextFactory;->trustManagersFor(Ljava/security/Provider;Ljava/security/KeyStore;Z)[Ljavax/net/ssl/TrustManager;

    move-result-object p0

    invoke-static {p1}, Lcom/jscape/util/c/c;->d(Z)Ljava/security/SecureRandom;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v2, p0, v1, p1}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor(Ljava/security/Provider;[Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;Z)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static contextFor([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Z)Ljavax/net/ssl/SSLContext;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {p2}, Lcom/jscape/util/c/c;->b(Z)Ljava/security/Provider;

    move-result-object v0

    invoke-static {p2}, Lcom/jscape/util/c/c;->d(Z)Ljava/security/SecureRandom;

    move-result-object v1

    invoke-static {v0, p0, p1, v1, p2}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor(Ljava/security/Provider;[Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;Z)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static defaultContext()Ljavax/net/ssl/SSLContext;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/c/c;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/jscape/util/k/c/SslContextFactory;->defaultContext(Z)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    return-object v0
.end method

.method public static defaultContext(Z)Ljavax/net/ssl/SSLContext;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/c/c;->b(Z)Ljava/security/Provider;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljavax/net/ssl/TrustManager;

    new-instance v2, Lcom/jscape/util/k/c/f;

    invoke-direct {v2}, Lcom/jscape/util/k/c/f;-><init>()V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p0}, Lcom/jscape/util/c/c;->d(Z)Ljava/security/SecureRandom;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v2, p0}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor(Ljava/security/Provider;[Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;Z)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method private static keyManagersFor(Ljava/security/Provider;Ljava/security/KeyStore;Z)[Ljavax/net/ssl/KeyManager;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    if-eqz p0, :cond_0

    :try_start_0
    sget-object v1, Lcom/jscape/util/k/c/SslContextFactory;->a:[Ljava/lang/String;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-static {v1, p0}, Ljavax/net/ssl/KeyManagerFactory;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/net/ssl/KeyManagerFactory;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_0
    invoke-static {}, Ljavax/net/ssl/KeyManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljavax/net/ssl/KeyManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/KeyManagerFactory;

    move-result-object p0

    :goto_0
    if-eqz v0, :cond_1

    if-nez p2, :cond_2

    const/4 p2, 0x0

    :try_start_1
    new-array p2, p2, [C

    invoke-virtual {p0, p1, p2}, Ljavax/net/ssl/KeyManagerFactory;->init(Ljava/security/KeyStore;[C)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_1
    :goto_1
    if-nez v0, :cond_3

    :cond_2
    :try_start_2
    nop

    nop

    move-result-object p1

    invoke-static {p1}, Lcom/jscape/util/c/c;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_3
    :goto_2
    invoke-virtual {p0}, Ljavax/net/ssl/KeyManagerFactory;->getKeyManagers()[Ljavax/net/ssl/KeyManager;

    move-result-object p0

    return-object p0
.end method

.method private static lambda$contextFor$0(Ljavax/net/ssl/SSLContext;[Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)Ljava/lang/Void;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0, p1, p2, p3}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    const/4 p0, 0x0

    return-object p0
.end method

.method private static lambda$keyManagersFor$1(Ljavax/net/ssl/KeyManagerFactory;Ljava/security/KeyStore;)Ljava/lang/Void;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [C

    invoke-virtual {p0, p1, v0}, Ljavax/net/ssl/KeyManagerFactory;->init(Ljava/security/KeyStore;[C)V

    const/4 p0, 0x0

    return-object p0
.end method

.method private static lambda$trustManagersFor$2(Ljavax/net/ssl/TrustManagerFactory;Ljava/security/KeyStore;)Ljava/lang/Void;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0, p1}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V

    const/4 p0, 0x0

    return-object p0
.end method

.method public static reuseSession(Ljavax/net/ssl/SSLSocket;Ljavax/net/ssl/SSLSocket;)Ljavax/net/ssl/SSLSocket;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object p0

    :try_start_0
    invoke-static {p0, p1}, Lcom/jscape/util/k/c/SslContextFactory;->reuseSessionOracle(Ljavax/net/ssl/SSLSession;Ljavax/net/ssl/SSLSocket;)Ljavax/net/ssl/SSLSocket;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    invoke-static {p0, p1}, Lcom/jscape/util/k/c/SslContextFactory;->reuseSessionBc(Ljavax/net/ssl/SSLSession;Ljavax/net/ssl/SSLSocket;)Ljavax/net/ssl/SSLSocket;

    move-result-object p0

    return-object p0
.end method

.method private static reuseSessionBc(Ljavax/net/ssl/SSLSession;Ljavax/net/ssl/SSLSocket;)Ljavax/net/ssl/SSLSocket;
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-interface {p0}, Ljavax/net/ssl/SSLSession;->getSessionContext()Ljavax/net/ssl/SSLSessionContext;

    move-result-object v0

    sget-object v1, Lcom/jscape/util/k/c/SslContextFactory;->a:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v3, v1, v2

    invoke-static {v0, v3}, Lcom/jscape/util/l/e;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    const/16 v4, 0x9

    aget-object v4, v1, v4

    const/4 v5, 0x7

    aget-object v5, v1, v5

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const/4 v6, 0x1

    new-array v7, v6, [Ljava/lang/Class;

    const-class v8, [B

    const/4 v9, 0x0

    aput-object v8, v7, v9

    new-array v8, v6, [Ljava/lang/Object;

    invoke-interface {p0}, Ljavax/net/ssl/SSLSession;->getId()[B

    move-result-object p0

    aput-object p0, v8, v9

    invoke-static {v5, v7, v8}, Lcom/jscape/util/l/i;->a(Ljava/lang/Class;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Ljava/lang/Object;

    invoke-static {v3, v4, p0}, Lcom/jscape/util/l/g;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    const/16 v3, 0x10

    aget-object v3, v1, v3

    invoke-static {v0, v3}, Lcom/jscape/util/l/e;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const/4 v3, 0x5

    aget-object v3, v1, v3

    new-array v4, v2, [Ljava/lang/Object;

    const/16 v5, 0xe

    aget-object v5, v1, v5

    new-array v7, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v8

    invoke-virtual {v8}, Ljava/net/InetAddress;->getHostName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getPort()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v6

    invoke-static {v5, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    sget-object v7, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v5, v7}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    aput-object p0, v4, v6

    invoke-static {v0, v3, v4}, Lcom/jscape/util/l/g;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v3, 0x6

    aget-object v3, v1, v3

    new-array v4, v2, [Ljava/lang/Object;

    const/16 v5, 0xa

    aget-object v1, v1, v5

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v9

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getPort()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v9

    aput-object p0, v4, v6

    invoke-static {v0, v3, v4}, Lcom/jscape/util/l/g;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    return-object p1
.end method

.method private static reuseSessionOracle(Ljavax/net/ssl/SSLSession;Ljavax/net/ssl/SSLSocket;)Ljavax/net/ssl/SSLSocket;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-interface {p0}, Ljavax/net/ssl/SSLSession;->getSessionContext()Ljavax/net/ssl/SSLSessionContext;

    move-result-object v0

    const/4 v1, 0x6

    const/16 v2, 0xa

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    :try_start_0
    sget-object v6, Lcom/jscape/util/k/c/SslContextFactory;->a:[Ljava/lang/String;

    aget-object v7, v6, v5

    invoke-static {v0, v7}, Lcom/jscape/util/l/e;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aget-object v8, v6, v1

    new-array v9, v3, [Ljava/lang/Object;

    aget-object v10, v6, v2

    new-array v11, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v12

    invoke-virtual {v12}, Ljava/net/InetAddress;->getHostName()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v4

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getPort()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v5

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    sget-object v11, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v10, v11}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v4

    aput-object p0, v9, v5

    invoke-static {v7, v8, v9}, Lcom/jscape/util/l/g;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    aget-object v8, v6, v1

    new-array v9, v3, [Ljava/lang/Object;

    aget-object v6, v6, v2

    new-array v10, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v11

    invoke-virtual {v11}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v4

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getPort()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v5

    invoke-static {v6, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    sget-object v10, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v6, v10}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v9, v4

    aput-object p0, v9, v5

    invoke-static {v7, v8, v9}, Lcom/jscape/util/l/g;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/jscape/util/l/a; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    sget-object v6, Lcom/jscape/util/k/c/SslContextFactory;->a:[Ljava/lang/String;

    const/16 v7, 0xf

    aget-object v7, v6, v7

    invoke-static {v0, v7}, Lcom/jscape/util/l/e;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const/16 v7, 0x8

    aget-object v7, v6, v7

    new-array v8, v5, [Ljava/lang/Object;

    aget-object v9, v6, v2

    new-array v10, v3, [Ljava/lang/Object;

    invoke-interface {p0}, Ljavax/net/ssl/SSLSession;->getPeerHost()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v4

    invoke-interface {p0}, Ljavax/net/ssl/SSLSession;->getPeerPort()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v10, v5

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    sget-object v9, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {p0, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    aput-object p0, v8, v4

    invoke-static {v0, v7, v8}, Lcom/jscape/util/l/g;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    aget-object v7, v6, v1

    new-array v8, v3, [Ljava/lang/Object;

    aget-object v9, v6, v2

    new-array v10, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v11

    invoke-virtual {v11}, Ljava/net/InetAddress;->getHostName()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v4

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getPort()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v5

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    sget-object v10, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v9, v10}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    aput-object p0, v8, v5

    invoke-static {v0, v7, v8}, Lcom/jscape/util/l/g;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    aget-object v1, v6, v1

    new-array v7, v3, [Ljava/lang/Object;

    aget-object v2, v6, v2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v6

    invoke-virtual {v6}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getPort()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v4

    aput-object p0, v7, v5

    invoke-static {v0, v1, v7}, Lcom/jscape/util/l/g;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object p1
.end method

.method public static supportedCipherSuites()[Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/jscape/util/k/c/SslContextFactory;->supportedCipherSuites(Z)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static supportedCipherSuites(Z)[Ljava/lang/String;
    .locals 1

    :try_start_0
    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->defaultContext(Z)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    invoke-virtual {p0}, Ljavax/net/ssl/SSLContext;->getSupportedSSLParameters()Ljavax/net/ssl/SSLParameters;

    move-result-object p0

    invoke-virtual {p0}, Ljavax/net/ssl/SSLParameters;->getCipherSuites()[Ljava/lang/String;

    move-result-object p0

    new-instance v0, Lcom/jscape/util/k/c/g;

    invoke-direct {v0}, Lcom/jscape/util/k/c/g;-><init>()V

    invoke-static {p0, v0}, Lcom/jscape/util/v;->a([Ljava/lang/Object;Ljava/util/Comparator;)[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/String;

    return-object p0
.end method

.method public static supportedProtocols()[Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/jscape/util/c/c;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/jscape/util/k/c/SslContextFactory;->supportedProtocols(Z)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static supportedProtocols(Z)[Ljava/lang/String;
    .locals 0

    :try_start_0
    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->defaultContext(Z)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    invoke-virtual {p0}, Ljavax/net/ssl/SSLContext;->getSupportedSSLParameters()Ljavax/net/ssl/SSLParameters;

    move-result-object p0

    invoke-virtual {p0}, Ljavax/net/ssl/SSLParameters;->getProtocols()[Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/String;

    return-object p0
.end method

.method public static systemContext()Ljavax/net/ssl/SSLContext;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Ljavax/net/ssl/TrustManager;

    new-instance v1, Lcom/jscape/util/k/c/f;

    invoke-direct {v1}, Lcom/jscape/util/k/c/f;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    const/4 v3, 0x0

    invoke-static {v3, v3, v0, v1, v2}, Lcom/jscape/util/k/c/SslContextFactory;->contextFor(Ljava/security/Provider;[Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;Z)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    return-object v0
.end method

.method public static systemContextFor(Ljava/security/KeyStore;Ljava/lang/String;)Ljavax/net/ssl/SSLContext;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    invoke-virtual {p0}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {p0, v2}, Ljava/security/KeyStore;->isKeyEntry(Ljava/lang/String;)Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_0

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    invoke-virtual {p0, v2, p1}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object p1

    move-object v3, p1

    check-cast v3, Ljava/security/PrivateKey;

    invoke-virtual {p0, v2}, Ljava/security/KeyStore;->getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;

    move-result-object p0

    goto :goto_1

    :cond_2
    move-object p0, v3

    :goto_1
    invoke-static {v3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {p0}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {v3, p0}, Lcom/jscape/util/k/c/SslContextFactory;->systemContextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static systemContextFor(Ljava/security/KeyStore;Ljava/lang/String;Ljava/security/KeyStore;)Ljavax/net/ssl/SSLContext;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    invoke-virtual {p0}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v0, :cond_1

    :try_start_0
    invoke-virtual {p0, v2}, Ljava/security/KeyStore;->isKeyEntry(Ljava/lang/String;)Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_0

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    invoke-virtual {p0, v2, p1}, Ljava/security/KeyStore;->getKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object p1

    move-object v3, p1

    check-cast v3, Ljava/security/PrivateKey;

    invoke-virtual {p0, v2}, Ljava/security/KeyStore;->getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;

    move-result-object p0

    goto :goto_1

    :cond_2
    move-object p0, v3

    :goto_1
    invoke-static {v3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {p0}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {p2}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v0, :cond_4

    :try_start_2
    invoke-virtual {p2, v2}, Ljava/security/KeyStore;->isCertificateEntry(Ljava/lang/String;)Z

    move-result v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v4, :cond_5

    goto :goto_2

    :catch_2
    move-exception p0

    :try_start_3
    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_4
    :goto_2
    invoke-virtual {p2, v2}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    if-nez v0, :cond_3

    :cond_6
    const/4 p2, 0x0

    new-array p2, p2, [Ljava/security/cert/Certificate;

    invoke-interface {p1, p2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/security/cert/Certificate;

    invoke-static {v3, p0, p1}, Lcom/jscape/util/k/c/SslContextFactory;->systemContextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;[Ljava/security/cert/Certificate;)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static systemContextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;)Ljavax/net/ssl/SSLContext;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/k/c/f;

    invoke-direct {v0}, Lcom/jscape/util/k/c/f;-><init>()V

    invoke-static {p0, p1, v0}, Lcom/jscape/util/k/c/SslContextFactory;->systemContextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;Ljavax/net/ssl/TrustManager;)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static systemContextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;Ljavax/net/ssl/TrustManager;)Ljavax/net/ssl/SSLContext;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sget-object v0, Lcom/jscape/util/k/c/SslContextFactory;->a:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    invoke-static {v0, p0, p1}, Lcom/jscape/util/k/c/SslContextFactory;->addKey(Ljava/security/KeyStore;Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;)V

    const/4 p0, 0x0

    invoke-static {v1, v0, p0}, Lcom/jscape/util/k/c/SslContextFactory;->keyManagersFor(Ljava/security/Provider;Ljava/security/KeyStore;Z)[Ljavax/net/ssl/KeyManager;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljavax/net/ssl/TrustManager;

    aput-object p2, v0, p0

    invoke-static {p1, v0}, Lcom/jscape/util/k/c/SslContextFactory;->systemContextFor([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static systemContextFor(Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;[Ljava/security/cert/Certificate;)Ljavax/net/ssl/SSLContext;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sget-object v0, Lcom/jscape/util/k/c/SslContextFactory;->a:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    invoke-static {v0, p0, p1}, Lcom/jscape/util/k/c/SslContextFactory;->addKey(Ljava/security/KeyStore;Ljava/security/PrivateKey;[Ljava/security/cert/Certificate;)V

    invoke-static {v0, p2}, Lcom/jscape/util/k/c/SslContextFactory;->addTrustedCertificates(Ljava/security/KeyStore;[Ljava/security/cert/Certificate;)V

    const/4 p0, 0x0

    invoke-static {v1, v0, p0}, Lcom/jscape/util/k/c/SslContextFactory;->keyManagersFor(Ljava/security/Provider;Ljava/security/KeyStore;Z)[Ljavax/net/ssl/KeyManager;

    move-result-object p1

    invoke-static {v1, v0, p0}, Lcom/jscape/util/k/c/SslContextFactory;->trustManagersFor(Ljava/security/Provider;Ljava/security/KeyStore;Z)[Ljavax/net/ssl/TrustManager;

    move-result-object p0

    invoke-static {p1, p0}, Lcom/jscape/util/k/c/SslContextFactory;->systemContextFor([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;)Ljavax/net/ssl/SSLContext;

    move-result-object p0

    return-object p0
.end method

.method public static systemContextFor([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;)Ljavax/net/ssl/SSLContext;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sget-object v0, Lcom/jscape/util/k/c/SslContextFactory;->a:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v0, p0, p1, v1}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    return-object v0
.end method

.method private static trustManagersFor(Ljava/security/Provider;Ljava/security/KeyStore;Z)[Ljavax/net/ssl/TrustManager;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/c/j;->f()[I

    move-result-object v0

    if-eqz p0, :cond_0

    :try_start_0
    sget-object v1, Lcom/jscape/util/k/c/SslContextFactory;->a:[Ljava/lang/String;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-static {v1, p0}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_0
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object p0

    :goto_0
    if-eqz v0, :cond_1

    if-nez p2, :cond_2

    :try_start_1
    invoke-virtual {p0, p1}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_1
    :goto_1
    if-nez v0, :cond_3

    :cond_2
    :try_start_2
    nop

    nop

    move-result-object p1

    invoke-static {p1}, Lcom/jscape/util/c/c;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/c/SslContextFactory;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_3
    :goto_2
    invoke-virtual {p0}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object p0

    return-object p0
.end method
