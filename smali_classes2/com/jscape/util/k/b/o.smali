.class public Lcom/jscape/util/k/b/o;
.super Lcom/jscape/util/n/c;

# interfaces
.implements Lcom/jscape/util/k/a/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/util/n/c<",
        "Lcom/jscape/util/k/a/g<",
        "Lcom/jscape/util/k/a/C;",
        ">;>;",
        "Lcom/jscape/util/k/a/f<",
        "Lcom/jscape/util/k/a/C;",
        ">;"
    }
.end annotation


# static fields
.field private static final e:[Ljava/lang/String;


# instance fields
.field private final c:Lcom/jscape/util/k/b/i;

.field private final d:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x1b

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x37

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "R\u000e,wx\u001c\rr\u0018!\u007f^\u0007\"o\u0004,hr\u001alz\u0003.oxU\u0012-A*dx\u000b9u\u000e=Ox\u001a:h\u0002*!"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x2e

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/k/b/o;->e:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x7b

    goto :goto_2

    :cond_2
    const/16 v12, 0x5f

    goto :goto_2

    :cond_3
    const/16 v12, 0x2a

    goto :goto_2

    :cond_4
    const/16 v12, 0x2b

    goto :goto_2

    :cond_5
    const/16 v12, 0x78

    goto :goto_2

    :cond_6
    const/16 v12, 0x56

    goto :goto_2

    :cond_7
    const/16 v12, 0x36

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lcom/jscape/util/k/b/i;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/util/n/c;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/b/o;->c:Lcom/jscape/util/k/b/i;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/k/b/o;->d:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public static a(Lcom/jscape/util/k/g;Ljava/util/concurrent/ExecutorService;)Lcom/jscape/util/k/b/o;
    .locals 2

    new-instance v0, Lcom/jscape/util/k/b/o;

    new-instance v1, Lcom/jscape/util/k/b/i;

    invoke-direct {v1, p0}, Lcom/jscape/util/k/b/i;-><init>(Lcom/jscape/util/k/g;)V

    invoke-direct {v0, v1, p1}, Lcom/jscape/util/k/b/o;-><init>(Lcom/jscape/util/k/b/i;Ljava/util/concurrent/ExecutorService;)V

    return-object v0
.end method


# virtual methods
.method protected actualStart()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/b/o;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/jscape/util/k/b/g;

    invoke-direct {v1, p0}, Lcom/jscape/util/k/b/g;-><init>(Lcom/jscape/util/k/b/o;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public g()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected o()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/k/b/o;->c:Lcom/jscape/util/k/b/i;

    invoke-virtual {v0}, Lcom/jscape/util/k/b/i;->b()Lcom/jscape/util/k/a/C;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/k/b/o;->b:Ljava/lang/Object;

    check-cast v1, Lcom/jscape/util/k/a/g;

    invoke-interface {v1, p0, v0}, Lcom/jscape/util/k/a/g;->a(Lcom/jscape/util/k/a/f;Lcom/jscape/util/k/a/r;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/util/k/b/o;->b:Ljava/lang/Object;

    check-cast v1, Lcom/jscape/util/k/a/g;

    new-instance v2, Lcom/jscape/util/k/a/c;

    invoke-direct {v2, v0}, Lcom/jscape/util/k/a/c;-><init>(Ljava/lang/Throwable;)V

    invoke-interface {v1, p0, v2}, Lcom/jscape/util/k/a/g;->a(Lcom/jscape/util/k/a/f;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/k/b/o;->e:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/k/b/o;->c:Lcom/jscape/util/k/b/i;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/k/b/o;->d:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
