.class public Lcom/jscape/util/k/b/l;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/k/a/C;
.implements Lcom/jscape/util/k/b/h;


# static fields
.field private static final j:Ljava/lang/String;


# instance fields
.field private final a:Ljava/net/Socket;

.field private final b:J

.field private final c:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Ljavax/net/ssl/SSLSocket;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private e:Ljava/io/OutputStream;

.field private f:Ljava/io/InputStream;

.field private g:Lcom/jscape/util/k/TransportAddress;

.field private h:Lcom/jscape/util/k/TransportAddress;

.field private final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "V\"N{ND\u001aV\""

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/k/b/l;->j:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x1d

    goto :goto_1

    :cond_1
    const/16 v4, 0x5d

    goto :goto_1

    :cond_2
    const/16 v4, 0x44

    goto :goto_1

    :cond_3
    const/16 v4, 0x60

    goto :goto_1

    :cond_4
    const/16 v4, 0x49

    goto :goto_1

    :cond_5
    const/16 v4, 0x76

    goto :goto_1

    :cond_6
    const/16 v4, 0x54

    :goto_1
    const/16 v5, 0x27

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/net/Socket;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/jscape/util/k/b/j;->b()[Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/b/l;->a:Ljava/net/Socket;

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/jscape/util/k/b/l;->b:J

    :try_start_0
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Lcom/jscape/util/k/b/l;->c:Ljava/util/Stack;

    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, Lcom/jscape/util/k/b/l;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v1, Lcom/jscape/util/k/TransportAddress;

    invoke-virtual {p1}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {p1}, Ljava/net/Socket;->getLocalPort()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/net/InetAddress;I)V

    iput-object v1, p0, Lcom/jscape/util/k/b/l;->g:Lcom/jscape/util/k/TransportAddress;

    new-instance v1, Lcom/jscape/util/k/TransportAddress;

    invoke-virtual {p1}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {p1}, Ljava/net/Socket;->getPort()I

    move-result p1

    invoke-direct {v1, v2, p1}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/net/InetAddress;I)V

    iput-object v1, p0, Lcom/jscape/util/k/b/l;->h:Lcom/jscape/util/k/TransportAddress;

    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/k/b/l;->i:Ljava/util/Map;

    iget-object p1, p0, Lcom/jscape/util/k/b/l;->a:Ljava/net/Socket;

    invoke-direct {p0, p1}, Lcom/jscape/util/k/b/l;->a(Ljava/net/Socket;)V

    if-eqz v0, :cond_0

    const/4 p1, 0x5

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/l;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/net/Socket;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {p1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object p1

    iput-object v0, p0, Lcom/jscape/util/k/b/l;->e:Ljava/io/OutputStream;

    iput-object p1, p0, Lcom/jscape/util/k/b/l;->f:Ljava/io/InputStream;

    return-void
.end method


# virtual methods
.method public a(Ljavax/net/ssl/SSLSocketFactory;)Ljavax/net/ssl/SSLSocket;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/b/j;->b()[Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/k/b/l;->c:Ljava/util/Stack;

    if-nez v0, :cond_1

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/util/k/b/l;->a:Ljava/net/Socket;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/util/k/b/l;->c:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    :cond_1
    move-object v0, v1

    check-cast v0, Ljava/net/Socket;

    :goto_0
    iget-object v1, p0, Lcom/jscape/util/k/b/l;->h:Lcom/jscape/util/k/TransportAddress;

    invoke-virtual {v1}, Lcom/jscape/util/k/TransportAddress;->hostName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/util/k/b/l;->h:Lcom/jscape/util/k/TransportAddress;

    invoke-virtual {v2}, Lcom/jscape/util/k/TransportAddress;->getPort()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object p1

    check-cast p1, Ljavax/net/ssl/SSLSocket;

    invoke-direct {p0, p1}, Lcom/jscape/util/k/b/l;->a(Ljava/net/Socket;)V

    iget-object v0, p0, Lcom/jscape/util/k/b/l;->c:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/util/k/b/l;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/l;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method public a(Lcom/jscape/util/k/TransportAddress;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/b/l;->g:Lcom/jscape/util/k/TransportAddress;

    return-void
.end method

.method public a(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/b/j;->b()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/k/b/l;->c:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljavax/net/ssl/SSLSocket;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/l;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    :try_start_1
    iget-object p1, p0, Lcom/jscape/util/k/b/l;->c:Ljava/util/Stack;

    if-nez v0, :cond_2

    invoke-virtual {p1}, Ljava/util/Stack;->empty()Z

    move-result p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz p1, :cond_1

    :try_start_2
    iget-object p1, p0, Lcom/jscape/util/k/b/l;->a:Ljava/net/Socket;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/jscape/util/k/b/l;->c:Ljava/util/Stack;

    invoke-virtual {p1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object p1

    :cond_2
    check-cast p1, Ljava/net/Socket;

    :goto_1
    invoke-direct {p0, p1}, Lcom/jscape/util/k/b/l;->a(Ljava/net/Socket;)V

    return-void

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/util/k/b/l;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/l;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method public attributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/b/l;->i:Ljava/util/Map;

    return-object v0
.end method

.method public availableBytes()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/k/b/l;->f:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/jscape/util/k/a/Connection$ConnectionException;

    invoke-direct {v1, v0}, Lcom/jscape/util/k/a/Connection$ConnectionException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public b()Ljava/net/Socket;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/b/l;->a:Ljava/net/Socket;

    return-object v0
.end method

.method public b(Lcom/jscape/util/k/TransportAddress;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/b/l;->h:Lcom/jscape/util/k/TransportAddress;

    return-void
.end method

.method public close()V
    .locals 2

    invoke-static {}, Lcom/jscape/util/k/b/j;->b()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jscape/util/k/b/l;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/jscape/util/k/b/l;->a:Ljava/net/Socket;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    :cond_1
    iget-object v0, p0, Lcom/jscape/util/k/b/l;->c:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    return-void
.end method

.method public closed()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/b/l;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public creationTime()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/util/k/b/l;->b:J

    return-wide v0
.end method

.method public flush()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/k/b/l;->e:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/jscape/util/k/a/Connection$WriteException;

    invoke-direct {v1, v0}, Lcom/jscape/util/k/a/Connection$WriteException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public localAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/b/l;->g:Lcom/jscape/util/k/TransportAddress;

    return-object v0
.end method

.method public read()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/k/b/l;->f:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0
    :try_end_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/jscape/util/k/a/Connection$ConnectionException;

    invoke-direct {v1, v0}, Lcom/jscape/util/k/a/Connection$ConnectionException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    const/4 v0, -0x1

    return v0

    :catch_2
    move-exception v0

    new-instance v1, Lcom/jscape/util/k/a/Connection$ReadTimeoutException;

    invoke-direct {v1, v0}, Lcom/jscape/util/k/a/Connection$ReadTimeoutException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public read([BII)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/k/b/l;->f:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result p1
    :try_end_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/util/k/a/Connection$ConnectionException;

    invoke-direct {p2, p1}, Lcom/jscape/util/k/a/Connection$ConnectionException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :catch_1
    const/4 p1, -0x1

    return p1

    :catch_2
    move-exception p1

    new-instance p2, Lcom/jscape/util/k/a/Connection$ReadTimeoutException;

    invoke-direct {p2, p1}, Lcom/jscape/util/k/a/Connection$ReadTimeoutException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public remoteAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/b/l;->h:Lcom/jscape/util/k/TransportAddress;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/jscape/util/k/b/l;->j:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/jscape/util/k/b/l;->g:Lcom/jscape/util/k/TransportAddress;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/jscape/util/k/b/l;->h:Lcom/jscape/util/k/TransportAddress;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(B)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/k/b/l;->e:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/jscape/util/k/a/Connection$WriteException;

    invoke-direct {v0, p1}, Lcom/jscape/util/k/a/Connection$WriteException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public write([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/k/b/l;->e:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/jscape/util/k/a/Connection$WriteException;

    invoke-direct {p2, p1}, Lcom/jscape/util/k/a/Connection$WriteException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method
