.class public Lcom/jscape/util/k/b/m;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/k/a/s;


# static fields
.field private static final i:Ljava/lang/String;


# instance fields
.field private final b:Ljavax/net/ssl/SSLSocketFactory;

.field private final c:[Ljava/lang/String;

.field private final d:[Ljava/lang/String;

.field private final e:Lcom/jscape/util/k/b/a;

.field private final f:Z

.field private final g:Z

.field private final h:Ljavax/net/ssl/HandshakeCompletedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const-string v0, "\u0013dr</G\u0014#eD\n/C\u00195Xe\u001b#E"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/k/b/m;->i:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v5, 0x12

    const/16 v6, 0x43

    const/4 v7, 0x5

    if-eqz v4, :cond_3

    const/4 v8, 0x1

    if-eq v4, v8, :cond_4

    const/4 v8, 0x2

    if-eq v4, v8, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_2

    const/4 v5, 0x4

    if-eq v4, v5, :cond_3

    if-eq v4, v7, :cond_1

    const/16 v5, 0x79

    goto :goto_1

    :cond_1
    const/16 v5, 0x32

    goto :goto_1

    :cond_2
    const/16 v5, 0x7a

    goto :goto_1

    :cond_3
    move v5, v6

    :cond_4
    :goto_1
    xor-int v4, v7, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/net/ssl/SSLSocketFactory;[Ljava/lang/String;[Ljava/lang/String;Lcom/jscape/util/k/b/a;ZZLjavax/net/ssl/HandshakeCompletedListener;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/b/m;->b:Ljavax/net/ssl/SSLSocketFactory;

    iput-object p2, p0, Lcom/jscape/util/k/b/m;->c:[Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/util/k/b/m;->d:[Ljava/lang/String;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/util/k/b/m;->e:Lcom/jscape/util/k/b/a;

    iput-boolean p5, p0, Lcom/jscape/util/k/b/m;->f:Z

    iput-boolean p6, p0, Lcom/jscape/util/k/b/m;->g:Z

    iput-object p7, p0, Lcom/jscape/util/k/b/m;->h:Ljavax/net/ssl/HandshakeCompletedListener;

    return-void
.end method

.method private a(Ljavax/net/ssl/SSLSocket;)V
    .locals 3

    invoke-static {}, Lcom/jscape/util/k/b/j;->b()[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/k/b/m;->c:[Ljava/lang/String;

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    array-length v2, v1

    if-lez v2, :cond_0

    invoke-virtual {p1, v1}, Ljavax/net/ssl/SSLSocket;->setEnabledProtocols([Ljava/lang/String;)V

    :cond_0
    if-nez v0, :cond_3

    iget-object v1, p0, Lcom/jscape/util/k/b/m;->d:[Ljava/lang/String;

    :cond_1
    if-eqz v1, :cond_2

    if-nez v0, :cond_3

    iget-object v1, p0, Lcom/jscape/util/k/b/m;->d:[Ljava/lang/String;

    array-length v2, v1

    if-lez v2, :cond_2

    invoke-virtual {p1, v1}, Ljavax/net/ssl/SSLSocket;->setEnabledCipherSuites([Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/jscape/util/k/b/m;->b(Ljavax/net/ssl/SSLSocket;)V

    :cond_2
    iget-object v1, p0, Lcom/jscape/util/k/b/m;->e:Lcom/jscape/util/k/b/a;

    invoke-virtual {v1, p1}, Lcom/jscape/util/k/b/a;->a(Ljavax/net/ssl/SSLSocket;)V

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/jscape/util/k/b/m;->f:Z

    invoke-virtual {p1, v0}, Ljavax/net/ssl/SSLSocket;->setUseClientMode(Z)V

    :cond_3
    iget-object v0, p0, Lcom/jscape/util/k/b/m;->h:Ljavax/net/ssl/HandshakeCompletedListener;

    if-eqz v0, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/jscape/util/k/b/m;->h:Ljavax/net/ssl/HandshakeCompletedListener;

    invoke-virtual {p1, v0}, Ljavax/net/ssl/SSLSocket;->addHandshakeCompletedListener(Ljavax/net/ssl/HandshakeCompletedListener;)V

    :cond_5
    return-void
.end method

.method private b(Ljavax/net/ssl/SSLSocket;)V
    .locals 3

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getSSLParameters()Ljavax/net/ssl/SSLParameters;

    move-result-object v0

    :try_start_0
    sget-object v1, Lcom/jscape/util/k/b/m;->i:Ljava/lang/String;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {v0, v1, v2}, Lcom/jscape/util/l/g;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p1, v0}, Ljavax/net/ssl/SSLSocket;->setSSLParameters(Ljavax/net/ssl/SSLParameters;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method


# virtual methods
.method public a(Lcom/jscape/util/k/a/r;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/b;
        }
    .end annotation

    :try_start_0
    check-cast p1, Lcom/jscape/util/k/b/h;

    iget-object v0, p0, Lcom/jscape/util/k/b/m;->b:Ljavax/net/ssl/SSLSocketFactory;

    invoke-interface {p1, v0}, Lcom/jscape/util/k/b/h;->a(Ljavax/net/ssl/SSLSocketFactory;)Ljavax/net/ssl/SSLSocket;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/jscape/util/k/b/m;->a(Ljavax/net/ssl/SSLSocket;)V

    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->startHandshake()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/jscape/util/k/a/b;

    invoke-direct {v0, p1}, Lcom/jscape/util/k/a/b;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public b(Lcom/jscape/util/k/a/r;)V
    .locals 1

    :try_start_0
    check-cast p1, Lcom/jscape/util/k/b/h;

    iget-boolean v0, p0, Lcom/jscape/util/k/b/m;->g:Z

    invoke-interface {p1, v0}, Lcom/jscape/util/k/b/h;->a(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method
