.class public abstract enum Lcom/jscape/util/k/b/a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/util/k/b/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/jscape/util/k/b/a;

.field public static final enum b:Lcom/jscape/util/k/b/a;

.field public static final enum c:Lcom/jscape/util/k/b/a;

.field private static final d:[Lcom/jscape/util/k/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x7

    const/4 v3, 0x0

    const/4 v4, -0x1

    move v5, v3

    :goto_0
    const/16 v6, 0x6c

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v2, v4

    const-string v8, "\'__Hs\u001b`\u000c-UX^s\u001bu6S^De\u00081_]Th\u000ca\'"

    invoke-virtual {v8, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v3

    :goto_1
    const/4 v11, 0x2

    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x1d

    if-ge v2, v4, :cond_0

    invoke-virtual {v8, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v2

    move v2, v14

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/jscape/util/k/b/b;

    aget-object v4, v1, v7

    invoke-direct {v2, v4, v3}, Lcom/jscape/util/k/b/b;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/jscape/util/k/b/a;->a:Lcom/jscape/util/k/b/a;

    new-instance v2, Lcom/jscape/util/k/b/c;

    aget-object v4, v1, v11

    invoke-direct {v2, v4, v7}, Lcom/jscape/util/k/b/c;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/jscape/util/k/b/a;->b:Lcom/jscape/util/k/b/a;

    new-instance v2, Lcom/jscape/util/k/b/d;

    aget-object v1, v1, v3

    invoke-direct {v2, v1, v11}, Lcom/jscape/util/k/b/d;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/jscape/util/k/b/a;->c:Lcom/jscape/util/k/b/a;

    new-array v0, v0, [Lcom/jscape/util/k/b/a;

    sget-object v1, Lcom/jscape/util/k/b/a;->a:Lcom/jscape/util/k/b/a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/jscape/util/k/b/a;->b:Lcom/jscape/util/k/b/a;

    aput-object v1, v0, v7

    aput-object v2, v0, v11

    sput-object v0, Lcom/jscape/util/k/b/a;->d:[Lcom/jscape/util/k/b/a;

    return-void

    :cond_1
    aget-char v12, v4, v10

    rem-int/lit8 v13, v10, 0x7

    if-eqz v13, :cond_7

    if-eq v13, v7, :cond_6

    if-eq v13, v11, :cond_5

    if-eq v13, v0, :cond_4

    const/4 v11, 0x4

    if-eq v13, v11, :cond_3

    const/4 v11, 0x5

    if-eq v13, v11, :cond_2

    const/16 v11, 0x48

    goto :goto_2

    :cond_2
    const/16 v11, 0x32

    goto :goto_2

    :cond_3
    const/16 v11, 0x4d

    goto :goto_2

    :cond_4
    const/16 v11, 0x6d

    goto :goto_2

    :cond_5
    const/16 v11, 0x60

    goto :goto_2

    :cond_6
    const/16 v11, 0x76

    goto :goto_2

    :cond_7
    const/16 v11, 0xf

    :goto_2
    xor-int/2addr v11, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;ILcom/jscape/util/k/b/b;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/jscape/util/k/b/a;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/util/k/b/a;
    .locals 1

    const-class v0, Lcom/jscape/util/k/b/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/util/k/b/a;

    return-object p0
.end method

.method public static a()[Lcom/jscape/util/k/b/a;
    .locals 1

    sget-object v0, Lcom/jscape/util/k/b/a;->d:[Lcom/jscape/util/k/b/a;

    invoke-virtual {v0}, [Lcom/jscape/util/k/b/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/util/k/b/a;

    return-object v0
.end method


# virtual methods
.method public abstract a(Ljavax/net/ssl/SSLSocket;)V
.end method
