.class public Lcom/jscape/util/k/b/n;
.super Lcom/jscape/util/n/c;

# interfaces
.implements Lcom/jscape/util/k/a/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/util/n/c<",
        "Lcom/jscape/util/k/a/g<",
        "Lcom/jscape/util/k/a/C;",
        ">;>;",
        "Lcom/jscape/util/k/a/f<",
        "Lcom/jscape/util/k/a/C;",
        ">;"
    }
.end annotation


# static fields
.field private static final j:[Ljava/lang/String;


# instance fields
.field private final c:Lcom/jscape/util/k/TransportAddress;

.field private final d:I

.field private final e:Lcom/jscape/util/Time;

.field private final f:Lcom/jscape/util/k/b/j;

.field private final g:Ljava/util/concurrent/ExecutorService;

.field private h:Ljava/net/ServerSocket;

.field private i:Lcom/jscape/util/k/TransportAddress;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "9faf\t\u0016C\tjg}\u0018\rpJrci\u0008\u0010g\u0019z?\u000fF)`b\u0019\u000cf+mf\u007f\t\u0011qW\u0012F)gu\t\u0001w\u001efp^\t\u0010t\u0003jg0\u0013F)qb\u000f\tg\u001eYc\u007f\r\u000fg\u001elp~Q\nF)`l\u000f\tn\u0005n?"

    const/16 v4, 0x5a

    const/16 v5, 0x18

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x3c

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x16

    const/16 v3, 0xa

    const-string v5, "\ne:(MK!S1s\u000b\ne/\"CK>R*<|"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v17, v5

    move v5, v3

    move-object/from16 v3, v17

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x70

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/util/k/b/n;->j:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    const/16 v16, 0x3e

    if-eqz v15, :cond_8

    if-eq v15, v9, :cond_7

    const/4 v1, 0x2

    if-eq v15, v1, :cond_9

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    goto :goto_4

    :cond_4
    const/16 v16, 0x5e

    goto :goto_4

    :cond_5
    const/16 v16, 0x50

    goto :goto_4

    :cond_6
    const/16 v16, 0x31

    goto :goto_4

    :cond_7
    const/16 v16, 0x35

    goto :goto_4

    :cond_8
    const/16 v16, 0x56

    :cond_9
    :goto_4
    xor-int v1, v8, v16

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Lcom/jscape/util/k/TransportAddress;ILcom/jscape/util/Time;Lcom/jscape/util/k/b/j;Ljava/util/concurrent/ExecutorService;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/util/n/c;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/b/n;->c:Lcom/jscape/util/k/TransportAddress;

    iput p2, p0, Lcom/jscape/util/k/b/n;->d:I

    iput-object p3, p0, Lcom/jscape/util/k/b/n;->e:Lcom/jscape/util/Time;

    invoke-static {p4}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p4, p0, Lcom/jscape/util/k/b/n;->f:Lcom/jscape/util/k/b/j;

    invoke-static {p5}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p5, p0, Lcom/jscape/util/k/b/n;->g:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method private static b(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private d(Ljava/net/Socket;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/jscape/util/k/b/n;->b(Ljava/net/Socket;)V

    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/Exception;)V
    .locals 1

    invoke-static {}, Lcom/jscape/util/k/b/j;->b()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/jscape/util/k/b/n;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jscape/util/k/b/n;->b:Ljava/lang/Object;

    check-cast v0, Lcom/jscape/util/k/a/g;

    invoke-interface {v0, p0, p1}, Lcom/jscape/util/k/a/g;->a(Lcom/jscape/util/k/a/f;Ljava/lang/Throwable;)V

    :cond_0
    invoke-virtual {p0}, Lcom/jscape/util/k/b/n;->i()V

    :cond_1
    return-void
.end method

.method protected a(Ljava/net/Socket;)V
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/b/n;->g:Ljava/util/concurrent/ExecutorService;

    nop

    array-length v0, v0

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method protected actualStart()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/util/k/b/n;->o()V

    new-instance v0, Lcom/jscape/util/k/TransportAddress;

    iget-object v1, p0, Lcom/jscape/util/k/b/n;->h:Ljava/net/ServerSocket;

    invoke-virtual {v1}, Ljava/net/ServerSocket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/util/k/b/n;->h:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/net/InetAddress;I)V

    iput-object v0, p0, Lcom/jscape/util/k/b/n;->i:Lcom/jscape/util/k/TransportAddress;

    iget-object v0, p0, Lcom/jscape/util/k/b/n;->g:Ljava/util/concurrent/ExecutorService;

    nop

    move/16 p265, p8303

    add-int/2addr p9, p4

    return-wide v0

    return-void
.end method

.method protected actualStop()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/b/n;->h:Ljava/net/ServerSocket;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/net/ServerSocket;)V

    return-void
.end method

.method protected b(Ljava/net/Socket;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/jscape/util/k/b/n;->c(Ljava/net/Socket;)V

    new-instance v0, Lcom/jscape/util/k/b/l;

    invoke-direct {v0, p1}, Lcom/jscape/util/k/b/l;-><init>(Ljava/net/Socket;)V

    iget-object v1, p0, Lcom/jscape/util/k/b/n;->b:Ljava/lang/Object;

    check-cast v1, Lcom/jscape/util/k/a/g;

    invoke-interface {v1, p0, v0}, Lcom/jscape/util/k/a/g;->a(Lcom/jscape/util/k/a/f;Lcom/jscape/util/k/a/r;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    iget-object p1, p0, Lcom/jscape/util/k/b/n;->b:Ljava/lang/Object;

    check-cast p1, Lcom/jscape/util/k/a/g;

    invoke-interface {p1, p0, v0}, Lcom/jscape/util/k/a/g;->a(Lcom/jscape/util/k/a/f;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method protected c(Ljava/net/Socket;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/b/j;->b()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/k/b/n;->e:Lcom/jscape/util/Time;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/util/k/b/n;->e:Lcom/jscape/util/Time;

    invoke-virtual {v0}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-virtual {p1, v0}, Ljava/net/Socket;->setSoTimeout(I)V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/n;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/n;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/jscape/util/k/b/n;->f:Lcom/jscape/util/k/b/j;

    invoke-virtual {v0, p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/Socket;)Ljava/net/Socket;

    return-void
.end method

.method public g()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/b/n;->i:Lcom/jscape/util/k/TransportAddress;

    return-object v0
.end method

.method protected o()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Ljava/net/ServerSocket;

    iget-object v1, p0, Lcom/jscape/util/k/b/n;->c:Lcom/jscape/util/k/TransportAddress;

    invoke-virtual {v1}, Lcom/jscape/util/k/TransportAddress;->getPort()I

    move-result v1

    iget v2, p0, Lcom/jscape/util/k/b/n;->d:I

    iget-object v3, p0, Lcom/jscape/util/k/b/n;->c:Lcom/jscape/util/k/TransportAddress;

    invoke-virtual {v3}, Lcom/jscape/util/k/TransportAddress;->inetAddress()Ljava/net/InetAddress;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Ljava/net/ServerSocket;-><init>(IILjava/net/InetAddress;)V

    iput-object v0, p0, Lcom/jscape/util/k/b/n;->h:Ljava/net/ServerSocket;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/ServerSocket;->setReuseAddress(Z)V

    return-void
.end method

.method protected p()V
    .locals 2

    invoke-static {}, Lcom/jscape/util/k/b/j;->b()[Ljava/lang/String;

    move-result-object v0

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/util/k/b/n;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jscape/util/k/b/n;->h:Ljava/net/ServerSocket;

    invoke-virtual {v1}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {p0, v1}, Lcom/jscape/util/k/b/n;->a(Ljava/net/Socket;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/util/k/b/n;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    invoke-virtual {p0, v0}, Lcom/jscape/util/k/b/n;->a(Ljava/lang/Exception;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/k/b/n;->j:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/k/b/n;->c:Lcom/jscape/util/k/TransportAddress;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/util/k/b/n;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x5

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/k/b/n;->e:Lcom/jscape/util/Time;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/k/b/n;->f:Lcom/jscape/util/k/b/j;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/k/b/n;->g:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/k/b/n;->h:Ljava/net/ServerSocket;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/k/b/n;->i:Lcom/jscape/util/k/TransportAddress;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
