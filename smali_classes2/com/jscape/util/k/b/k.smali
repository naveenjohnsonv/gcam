.class public Lcom/jscape/util/k/b/k;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/k/a/y;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/k/a/y<",
        "Lcom/jscape/util/k/a/C;",
        ">;"
    }
.end annotation


# static fields
.field private static final e:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/net/ServerSocket;

.field private final b:Lcom/jscape/util/Time;

.field private final c:Lcom/jscape/util/k/b/j;

.field private final d:Lcom/jscape/util/k/TransportAddress;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "/fTO`9yB\"RRp$n>!P)UKp#Mb5EIc2^l(XEv#rqfMAv4xs2YR("

    const/16 v5, 0x31

    const/16 v6, 0xf

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x4e

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x8

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v14, v11

    const/4 v15, 0x0

    :goto_2
    if-gt v14, v15, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v13, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x1e

    const/16 v4, 0x13

    const-string v6, "i \u0003\t0z>1P\u0011\u00142|>1e\u0002\u0015n\ni \u0004\u000f>t40tM"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v17, v6

    move v6, v4

    move-object/from16 v4, v17

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    add-int/2addr v7, v10

    add-int v9, v7, v6

    invoke-virtual {v4, v7, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v9, v12

    const/4 v13, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/k/b/k;->e:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v15

    rem-int/lit8 v2, v15, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v2, 0x53

    goto :goto_4

    :cond_4
    const/16 v2, 0x19

    goto :goto_4

    :cond_5
    const/16 v2, 0x5b

    goto :goto_4

    :cond_6
    const/16 v2, 0x6e

    goto :goto_4

    :cond_7
    const/16 v2, 0x78

    goto :goto_4

    :cond_8
    move v2, v12

    goto :goto_4

    :cond_9
    const/16 v2, 0x4d

    :goto_4
    xor-int/2addr v2, v9

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/net/ServerSocket;Lcom/jscape/util/Time;Lcom/jscape/util/k/b/j;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/b/k;->a:Ljava/net/ServerSocket;

    iput-object p2, p0, Lcom/jscape/util/k/b/k;->b:Lcom/jscape/util/Time;

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/util/k/b/k;->c:Lcom/jscape/util/k/b/j;

    new-instance p1, Lcom/jscape/util/k/TransportAddress;

    iget-object p2, p0, Lcom/jscape/util/k/b/k;->a:Ljava/net/ServerSocket;

    invoke-virtual {p2}, Ljava/net/ServerSocket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object p2

    iget-object p3, p0, Lcom/jscape/util/k/b/k;->a:Ljava/net/ServerSocket;

    invoke-virtual {p3}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result p3

    invoke-direct {p1, p2, p3}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/net/InetAddress;I)V

    iput-object p1, p0, Lcom/jscape/util/k/b/k;->d:Lcom/jscape/util/k/TransportAddress;

    return-void
.end method

.method public static a(Lcom/jscape/util/k/TransportAddress;ILcom/jscape/util/Time;Lcom/jscape/util/k/b/j;)Lcom/jscape/util/k/b/k;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/b/j;->b()[Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/net/ServerSocket;

    invoke-virtual {p0}, Lcom/jscape/util/k/TransportAddress;->getPort()I

    move-result v2

    invoke-virtual {p0}, Lcom/jscape/util/k/TransportAddress;->inetAddress()Ljava/net/InetAddress;

    move-result-object p0

    invoke-direct {v1, v2, p1, p0}, Ljava/net/ServerSocket;-><init>(IILjava/net/InetAddress;)V

    const/4 p0, 0x1

    if-nez v0, :cond_0

    :try_start_0
    invoke-virtual {v1, p0}, Ljava/net/ServerSocket;->setReuseAddress(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p2, :cond_1

    :try_start_1
    invoke-virtual {p2}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide p0

    long-to-int p0, p0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/b/k;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/b/k;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_0
    :goto_0
    invoke-virtual {v1, p0}, Ljava/net/ServerSocket;->setSoTimeout(I)V

    :cond_1
    new-instance p0, Lcom/jscape/util/k/b/k;

    invoke-direct {p0, v1, p2, p3}, Lcom/jscape/util/k/b/k;-><init>(Ljava/net/ServerSocket;Lcom/jscape/util/Time;Lcom/jscape/util/k/b/j;)V

    return-object p0
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/net/Socket;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/b/j;->b()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/k/b/k;->b:Lcom/jscape/util/Time;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/util/k/b/k;->b:Lcom/jscape/util/Time;

    invoke-virtual {v0}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-virtual {p1, v0}, Ljava/net/Socket;->setSoTimeout(I)V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/k;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/k;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/jscape/util/k/b/k;->c:Lcom/jscape/util/k/b/j;

    invoke-virtual {v0, p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/Socket;)Ljava/net/Socket;

    return-void
.end method


# virtual methods
.method public a()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/b/k;->d:Lcom/jscape/util/k/TransportAddress;

    return-object v0
.end method

.method public b()Lcom/jscape/util/k/a/C;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/c;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/k/b/k;->a:Ljava/net/ServerSocket;

    invoke-virtual {v1}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jscape/util/k/b/k;->a(Ljava/net/Socket;)V

    new-instance v1, Lcom/jscape/util/k/b/l;

    invoke-direct {v1, v0}, Lcom/jscape/util/k/b/l;-><init>(Ljava/net/Socket;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v1

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    new-instance v0, Lcom/jscape/util/k/a/c;

    invoke-direct {v0, v1}, Lcom/jscape/util/k/a/c;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_1
    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    new-instance v0, Lcom/jscape/util/k/a/d;

    invoke-direct {v0}, Lcom/jscape/util/k/a/d;-><init>()V

    throw v0
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/b/k;->a:Ljava/net/ServerSocket;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/net/ServerSocket;)V

    return-void
.end method

.method public bridge synthetic connect()Lcom/jscape/util/k/a/r;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/c;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/util/k/b/k;->b()Lcom/jscape/util/k/a/C;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/k/b/k;->e:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/k/b/k;->a:Ljava/net/ServerSocket;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/k/b/k;->b:Lcom/jscape/util/Time;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/k/b/k;->c:Lcom/jscape/util/k/b/j;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/k/b/k;->d:Lcom/jscape/util/k/TransportAddress;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
