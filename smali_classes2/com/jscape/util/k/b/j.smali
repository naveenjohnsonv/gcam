.class public Lcom/jscape/util/k/b/j;
.super Ljava/lang/Object;


# static fields
.field private static i:[Ljava/lang/String;

.field private static final j:[Ljava/lang/String;


# instance fields
.field public final a:Z

.field public final b:Ljava/lang/Integer;

.field public final c:Ljava/lang/Integer;

.field public final d:Ljava/lang/Integer;

.field public final e:Ljava/lang/Boolean;

.field public final f:Ljava/lang/Boolean;

.field public final g:Lcom/jscape/util/Time;

.field public final h:Lcom/jscape/util/Time;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/jscape/util/k/b/j;->b([Ljava/lang/String;)V

    const/4 v2, 0x0

    const-string v3, "k?\u0003\u0001Wa\u001c1z3\u0011Rb\u00105L\u0018\u001eQ9\u0011k?\u0002\u0001Z`72y\u0017\u0001FW\u001c=zL\rk?\u0005\u0007DJ\u001a\u0003z\u001d\u0005M9\u001f\u0014p\u0012\u000fQp%&m\u0010\tQp\u00105lQ\u001fFa\u00004z0\u0000Pv\u00104lL\u000fk?\u0005\u0016Ub\u0013.|2\u0008Uw\u0006z\u000ck?\u0002\u000b`m\u0018\"p\u0004\u0010\t"

    const/16 v4, 0x71

    const/16 v5, 0x14

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x46

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x1a

    const/16 v3, 0xd

    const-string v5, "n:\u0018\u0008_f\u00150N\u001d\u000cT<\u000cn:\u001f\u0004Tq1.s\u0002\u0004\u000c"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x43

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/util/k/b/j;->j:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    if-eqz v15, :cond_9

    if-eq v15, v9, :cond_8

    const/4 v1, 0x2

    if-eq v15, v1, :cond_7

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v1, 0x33

    goto :goto_4

    :cond_4
    const/16 v1, 0x42

    goto :goto_4

    :cond_5
    const/16 v1, 0x72

    goto :goto_4

    :cond_6
    const/16 v1, 0x22

    goto :goto_4

    :cond_7
    const/16 v1, 0x37

    goto :goto_4

    :cond_8
    const/16 v1, 0x59

    goto :goto_4

    :cond_9
    move v1, v9

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(ZLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/jscape/util/Time;Lcom/jscape/util/Time;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/jscape/util/k/b/j;->a:Z

    iput-object p2, p0, Lcom/jscape/util/k/b/j;->b:Ljava/lang/Integer;

    iput-object p3, p0, Lcom/jscape/util/k/b/j;->c:Ljava/lang/Integer;

    iput-object p4, p0, Lcom/jscape/util/k/b/j;->d:Ljava/lang/Integer;

    iput-object p5, p0, Lcom/jscape/util/k/b/j;->e:Ljava/lang/Boolean;

    iput-object p6, p0, Lcom/jscape/util/k/b/j;->f:Ljava/lang/Boolean;

    iput-object p7, p0, Lcom/jscape/util/k/b/j;->g:Lcom/jscape/util/Time;

    iput-object p8, p0, Lcom/jscape/util/k/b/j;->h:Lcom/jscape/util/Time;

    return-void
.end method

.method public static a()Lcom/jscape/util/k/b/j;
    .locals 10

    new-instance v9, Lcom/jscape/util/k/b/j;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/jscape/util/k/b/j;-><init>(ZLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/jscape/util/Time;Lcom/jscape/util/Time;)V

    return-object v9
.end method

.method private static a(Ljava/net/SocketException;)Ljava/net/SocketException;
    .locals 0

    return-object p0
.end method

.method public static b([Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/util/k/b/j;->i:[Ljava/lang/String;

    return-void
.end method

.method public static b()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/util/k/b/j;->i:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/net/DatagramSocket;)Ljava/net/DatagramSocket;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/b/j;->b()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/util/k/b/j;->a:Z
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :try_start_1
    invoke-virtual {p1, v1}, Ljava/net/DatagramSocket;->setReuseAddress(Z)V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/jscape/util/k/b/j;->b:Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_6

    if-nez v0, :cond_2

    if-eqz v1, :cond_1

    :try_start_3
    iget-object v1, p0, Lcom/jscape/util/k/b/j;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/net/DatagramSocket;->setReceiveBufferSize(I)V
    :try_end_3
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_7

    :cond_1
    iget-object v1, p0, Lcom/jscape/util/k/b/j;->c:Ljava/lang/Integer;

    :cond_2
    if-nez v0, :cond_4

    if-eqz v1, :cond_3

    :try_start_4
    iget-object v1, p0, Lcom/jscape/util/k/b/j;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/net/DatagramSocket;->setSendBufferSize(I)V
    :try_end_4
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    if-nez v0, :cond_5

    :try_start_5
    iget-object v1, p0, Lcom/jscape/util/k/b/j;->d:Ljava/lang/Integer;
    :try_end_5
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_2

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1

    :cond_4
    :goto_2
    if-eqz v1, :cond_5

    :try_start_6
    iget-object v0, p0, Lcom/jscape/util/k/b/j;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/net/DatagramSocket;->setTrafficClass(I)V
    :try_end_6
    .catch Ljava/net/SocketException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_3

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1

    :cond_5
    :goto_3
    :try_start_7
    iget-object v0, p0, Lcom/jscape/util/k/b/j;->h:Lcom/jscape/util/Time;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/jscape/util/k/b/j;->h:Lcom/jscape/util/Time;

    invoke-virtual {v0}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-virtual {p1, v0}, Ljava/net/DatagramSocket;->setSoTimeout(I)V
    :try_end_7
    .catch Ljava/net/SocketException; {:try_start_7 .. :try_end_7} :catch_5

    :cond_6
    return-object p1

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1

    :catch_6
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Ljava/net/SocketException; {:try_start_8 .. :try_end_8} :catch_7

    :catch_7
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1
.end method

.method public a(Ljava/net/Socket;)Ljava/net/Socket;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/b/j;->b()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    :try_start_0
    iget-boolean v2, p0, Lcom/jscape/util/k/b/j;->a:Z
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_0

    :try_start_1
    invoke-virtual {p1, v1}, Ljava/net/Socket;->setReuseAddress(Z)V

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    :try_start_2
    iget-object v2, p0, Lcom/jscape/util/k/b/j;->b:Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_c

    if-nez v0, :cond_2

    if-eqz v2, :cond_1

    :try_start_3
    iget-object v2, p0, Lcom/jscape/util/k/b/j;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/net/Socket;->setReceiveBufferSize(I)V
    :try_end_3
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_d

    :cond_1
    iget-object v2, p0, Lcom/jscape/util/k/b/j;->c:Ljava/lang/Integer;

    :cond_2
    if-nez v0, :cond_4

    if-eqz v2, :cond_3

    :try_start_4
    iget-object v2, p0, Lcom/jscape/util/k/b/j;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/net/Socket;->setSendBufferSize(I)V
    :try_end_4
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    if-nez v0, :cond_5

    :try_start_5
    iget-object v2, p0, Lcom/jscape/util/k/b/j;->d:Ljava/lang/Integer;
    :try_end_5
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_2

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1

    :cond_4
    :goto_2
    if-eqz v2, :cond_5

    :try_start_6
    iget-object v2, p0, Lcom/jscape/util/k/b/j;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/net/Socket;->setTrafficClass(I)V
    :try_end_6
    .catch Ljava/net/SocketException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_3

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1

    :cond_5
    :goto_3
    :try_start_7
    iget-object v2, p0, Lcom/jscape/util/k/b/j;->e:Ljava/lang/Boolean;
    :try_end_7
    .catch Ljava/net/SocketException; {:try_start_7 .. :try_end_7} :catch_a

    if-nez v0, :cond_7

    if-eqz v2, :cond_6

    :try_start_8
    iget-object v2, p0, Lcom/jscape/util/k/b/j;->e:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v2}, Ljava/net/Socket;->setTcpNoDelay(Z)V
    :try_end_8
    .catch Ljava/net/SocketException; {:try_start_8 .. :try_end_8} :catch_b

    :cond_6
    if-nez v0, :cond_8

    :try_start_9
    iget-object v2, p0, Lcom/jscape/util/k/b/j;->f:Ljava/lang/Boolean;
    :try_end_9
    .catch Ljava/net/SocketException; {:try_start_9 .. :try_end_9} :catch_5

    goto :goto_4

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1

    :cond_7
    :goto_4
    if-eqz v2, :cond_8

    :try_start_a
    iget-object v2, p0, Lcom/jscape/util/k/b/j;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v2}, Ljava/net/Socket;->setKeepAlive(Z)V
    :try_end_a
    .catch Ljava/net/SocketException; {:try_start_a .. :try_end_a} :catch_6

    goto :goto_5

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1

    :cond_8
    :goto_5
    :try_start_b
    iget-object v2, p0, Lcom/jscape/util/k/b/j;->g:Lcom/jscape/util/Time;
    :try_end_b
    .catch Ljava/net/SocketException; {:try_start_b .. :try_end_b} :catch_8

    if-nez v0, :cond_a

    if-eqz v2, :cond_9

    :try_start_c
    iget-object v0, p0, Lcom/jscape/util/k/b/j;->g:Lcom/jscape/util/Time;

    invoke-virtual {v0}, Lcom/jscape/util/Time;->toSeconds()J

    move-result-wide v2

    long-to-int v0, v2

    invoke-virtual {p1, v1, v0}, Ljava/net/Socket;->setSoLinger(ZI)V
    :try_end_c
    .catch Ljava/net/SocketException; {:try_start_c .. :try_end_c} :catch_9

    :cond_9
    iget-object v2, p0, Lcom/jscape/util/k/b/j;->h:Lcom/jscape/util/Time;

    :cond_a
    if-eqz v2, :cond_b

    :try_start_d
    iget-object v0, p0, Lcom/jscape/util/k/b/j;->h:Lcom/jscape/util/Time;

    invoke-virtual {v0}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-virtual {p1, v0}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_d
    .catch Ljava/net/SocketException; {:try_start_d .. :try_end_d} :catch_7

    goto :goto_6

    :catch_7
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1

    :cond_b
    :goto_6
    return-object p1

    :catch_8
    move-exception p1

    :try_start_e
    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1
    :try_end_e
    .catch Ljava/net/SocketException; {:try_start_e .. :try_end_e} :catch_9

    :catch_9
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1

    :catch_a
    move-exception p1

    :try_start_f
    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1
    :try_end_f
    .catch Ljava/net/SocketException; {:try_start_f .. :try_end_f} :catch_b

    :catch_b
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1

    :catch_c
    move-exception p1

    :try_start_10
    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1
    :try_end_10
    .catch Ljava/net/SocketException; {:try_start_10 .. :try_end_10} :catch_d

    :catch_d
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object p1

    throw p1
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    invoke-static {}, Lcom/jscape/util/k/b/j;->b()[Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/k/b/j;->j:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/util/k/b/j;->a:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/k/b/j;->b:Ljava/lang/Integer;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v3, v1, v2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/util/k/b/j;->c:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x4

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/util/k/b/j;->d:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/util/k/b/j;->e:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x7

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/util/k/b/j;->f:Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x6

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/util/k/b/j;->g:Lcom/jscape/util/Time;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v3, 0x5

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/k/b/j;->h:Lcom/jscape/util/Time;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v1

    if-nez v1, :cond_0

    new-array v1, v2, [Ljava/lang/String;

    invoke-static {v1}, Lcom/jscape/util/k/b/j;->b([Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method
