.class public Lcom/jscape/util/k/b/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/k/a/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/k/a/v<",
        "Lcom/jscape/util/k/a/C;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/util/k/g;

.field private final b:Lcom/jscape/util/k/b/j;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x13

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x63

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\u0007L\u001f\u0002;I\u007f_<\r\u001f9O\u007f_\t\u001e\u001ee\u001cx\u0003\u000f\u0006=VYD\u0002\u0002\u0008;VuYL\u0017\t=QyY\u0005\u001c\u00197P\'"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x30

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/k/b/i;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/16 v13, 0xf

    if-eqz v12, :cond_5

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_6

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v13, 0x79

    goto :goto_2

    :cond_2
    const/16 v13, 0x41

    goto :goto_2

    :cond_3
    const/16 v13, 0x3b

    goto :goto_2

    :cond_4
    const/16 v13, 0xe

    goto :goto_2

    :cond_5
    const/16 v13, 0x48

    :cond_6
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lcom/jscape/util/k/g;)V
    .locals 1

    invoke-static {}, Lcom/jscape/util/k/b/j;->a()Lcom/jscape/util/k/b/j;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/jscape/util/k/b/i;-><init>(Lcom/jscape/util/k/g;Lcom/jscape/util/k/b/j;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/util/k/g;Lcom/jscape/util/k/b/j;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/b/i;->a:Lcom/jscape/util/k/g;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/k/b/i;->b:Lcom/jscape/util/k/b/j;

    return-void
.end method

.method public static a(Lcom/jscape/util/k/TransportAddress;Lcom/jscape/util/Time;)Lcom/jscape/util/k/b/i;
    .locals 4

    new-instance v0, Lcom/jscape/util/k/b/i;

    new-instance v1, Lcom/jscape/util/k/g;

    invoke-virtual {p1}, Lcom/jscape/util/Time;->toMillis()J

    move-result-wide v2

    invoke-direct {v1, p0, v2, v3}, Lcom/jscape/util/k/g;-><init>(Lcom/jscape/util/k/TransportAddress;J)V

    invoke-static {}, Lcom/jscape/util/k/b/j;->a()Lcom/jscape/util/k/b/j;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/jscape/util/k/b/i;-><init>(Lcom/jscape/util/k/g;Lcom/jscape/util/k/b/j;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;IJ)Lcom/jscape/util/k/b/i;
    .locals 1

    new-instance v0, Lcom/jscape/util/k/TransportAddress;

    invoke-direct {v0, p0, p1}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/lang/String;I)V

    invoke-static {p2, p3}, Lcom/jscape/util/Time;->millis(J)Lcom/jscape/util/Time;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/jscape/util/k/b/i;->a(Lcom/jscape/util/k/TransportAddress;Lcom/jscape/util/Time;)Lcom/jscape/util/k/b/i;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;ILcom/jscape/util/Time;)Lcom/jscape/util/k/b/i;
    .locals 1

    new-instance v0, Lcom/jscape/util/k/TransportAddress;

    invoke-direct {v0, p0, p1}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/lang/String;I)V

    invoke-static {v0, p2}, Lcom/jscape/util/k/b/i;->a(Lcom/jscape/util/k/TransportAddress;Lcom/jscape/util/Time;)Lcom/jscape/util/k/b/i;

    move-result-object p0

    return-object p0
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public a()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/b/i;->a:Lcom/jscape/util/k/g;

    invoke-virtual {v0}, Lcom/jscape/util/k/g;->b()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/net/Socket;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/b/j;->b()[Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/k/b/i;->a:Lcom/jscape/util/k/g;

    invoke-virtual {v1}, Lcom/jscape/util/k/g;->c()J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {p1, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object v1, p0, Lcom/jscape/util/k/b/i;->b:Lcom/jscape/util/k/b/j;

    invoke-virtual {v1, p1}, Lcom/jscape/util/k/b/j;->a(Ljava/net/Socket;)Ljava/net/Socket;

    move-result-object v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jscape/util/k/b/i;->a:Lcom/jscape/util/k/g;

    invoke-virtual {v0}, Lcom/jscape/util/k/g;->a()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_0
    move-object p1, v1

    :goto_0
    iget-object v0, p0, Lcom/jscape/util/k/b/i;->a:Lcom/jscape/util/k/g;

    invoke-virtual {v0}, Lcom/jscape/util/k/g;->a()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/util/k/TransportAddress;->asSocketAddress()Ljava/net/InetSocketAddress;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/net/Socket;->bind(Ljava/net/SocketAddress;)V

    :cond_1
    return-void

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/util/k/b/i;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/b/i;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public b()Lcom/jscape/util/k/a/C;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/c;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/net/Socket;

    invoke-direct {v1}, Ljava/net/Socket;-><init>()V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    invoke-virtual {p0, v1}, Lcom/jscape/util/k/b/i;->a(Ljava/net/Socket;)V

    iget-object v0, p0, Lcom/jscape/util/k/b/i;->a:Lcom/jscape/util/k/g;

    invoke-virtual {v0}, Lcom/jscape/util/k/g;->b()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jscape/util/k/TransportAddress;->asSocketAddress()Ljava/net/InetSocketAddress;

    move-result-object v0

    iget-object v2, p0, Lcom/jscape/util/k/b/i;->a:Lcom/jscape/util/k/g;

    invoke-virtual {v2}, Lcom/jscape/util/k/g;->c()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {v1, v0, v2}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    new-instance v0, Lcom/jscape/util/k/b/l;

    invoke-direct {v0, v1}, Lcom/jscape/util/k/b/l;-><init>(Ljava/net/Socket;)V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-object v0, v1

    goto :goto_1

    :catch_2
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_0
    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    new-instance v1, Lcom/jscape/util/k/a/c;

    invoke-direct {v1, v0}, Lcom/jscape/util/k/a/c;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_3
    :goto_1
    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/net/Socket;)V

    new-instance v0, Lcom/jscape/util/k/a/d;

    invoke-direct {v0}, Lcom/jscape/util/k/a/d;-><init>()V

    throw v0
.end method

.method public bridge synthetic connect()Lcom/jscape/util/k/a/r;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/c;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/util/k/b/i;->b()Lcom/jscape/util/k/a/C;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/k/b/i;->c:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/k/b/i;->a:Lcom/jscape/util/k/g;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/k/b/i;->b:Lcom/jscape/util/k/b/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
