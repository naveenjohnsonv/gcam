.class public Lcom/jscape/util/k/b/e;
.super Lcom/jscape/util/k/a/m;

# interfaces
.implements Lcom/jscape/util/k/b/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/jscape/util/k/a/m<",
        "TM;>;",
        "Lcom/jscape/util/k/b/h;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/jscape/util/k/b/l;Lcom/jscape/util/h/I;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/b/l;",
            "Lcom/jscape/util/h/I<",
            "TM;>;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/util/k/a/m;-><init>(Lcom/jscape/util/k/a/C;Lcom/jscape/util/h/I;Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method


# virtual methods
.method public a(Ljavax/net/ssl/SSLSocketFactory;)Ljavax/net/ssl/SSLSocket;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/util/k/b/e;->c()Lcom/jscape/util/k/a/C;

    move-result-object v0

    check-cast v0, Lcom/jscape/util/k/b/l;

    invoke-virtual {v0, p1}, Lcom/jscape/util/k/b/l;->a(Ljavax/net/ssl/SSLSocketFactory;)Ljavax/net/ssl/SSLSocket;

    move-result-object p1

    return-object p1
.end method

.method public a(Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jscape/util/k/b/e;->c()Lcom/jscape/util/k/a/C;

    move-result-object v0

    check-cast v0, Lcom/jscape/util/k/b/l;

    invoke-virtual {v0, p1}, Lcom/jscape/util/k/b/l;->a(Z)V

    return-void
.end method

.method public b()Ljava/net/Socket;
    .locals 1

    invoke-virtual {p0}, Lcom/jscape/util/k/b/e;->c()Lcom/jscape/util/k/a/C;

    move-result-object v0

    check-cast v0, Lcom/jscape/util/k/b/l;

    invoke-virtual {v0}, Lcom/jscape/util/k/b/l;->b()Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method
