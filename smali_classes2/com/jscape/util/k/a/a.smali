.class public Lcom/jscape/util/k/a/a;
.super Ljava/io/InputStream;


# instance fields
.field private final a:Lcom/jscape/util/k/a/C;


# direct methods
.method public constructor <init>(Lcom/jscape/util/k/a/C;)V
    .locals 0

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/a/a;->a:Lcom/jscape/util/k/a/C;

    return-void
.end method


# virtual methods
.method public available()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/a;->a:Lcom/jscape/util/k/a/C;

    invoke-interface {v0}, Lcom/jscape/util/k/a/C;->availableBytes()I

    move-result v0

    return v0
.end method

.method public read()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/a;->a:Lcom/jscape/util/k/a/C;

    invoke-interface {v0}, Lcom/jscape/util/k/a/C;->read()I

    move-result v0

    return v0
.end method

.method public read([BII)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/a;->a:Lcom/jscape/util/k/a/C;

    invoke-interface {v0, p1, p2, p3}, Lcom/jscape/util/k/a/C;->read([BII)I

    move-result p1

    return p1
.end method
