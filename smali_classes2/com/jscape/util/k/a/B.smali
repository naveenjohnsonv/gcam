.class public Lcom/jscape/util/k/a/B;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/k/a/A;
.implements Lcom/jscape/util/k/a/w;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/jscape/util/k/a/A<",
        "TM;>;",
        "Lcom/jscape/util/k/a/w<",
        "TM;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/util/k/a/C;

.field private b:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "TM;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/io/OutputStream;

.field private final d:Ljava/io/InputStream;

.field private final e:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Lcom/jscape/util/k/a/C;Lcom/jscape/util/h/I;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/C;",
            "Lcom/jscape/util/h/I<",
            "TM;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    iput-object p1, p0, Lcom/jscape/util/k/a/B;->a:Lcom/jscape/util/k/a/C;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/k/a/B;->b:Lcom/jscape/util/h/I;

    new-instance p1, Lcom/jscape/util/k/a/a;

    iget-object p2, p0, Lcom/jscape/util/k/a/B;->a:Lcom/jscape/util/k/a/C;

    invoke-direct {p1, p2}, Lcom/jscape/util/k/a/a;-><init>(Lcom/jscape/util/k/a/C;)V

    iput-object p1, p0, Lcom/jscape/util/k/a/B;->d:Ljava/io/InputStream;

    new-instance p1, Lcom/jscape/util/k/a/e;

    iget-object p2, p0, Lcom/jscape/util/k/a/B;->a:Lcom/jscape/util/k/a/C;

    invoke-direct {p1, p2}, Lcom/jscape/util/k/a/e;-><init>(Lcom/jscape/util/k/a/C;)V

    iput-object p1, p0, Lcom/jscape/util/k/a/B;->c:Ljava/io/OutputStream;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/jscape/util/k/a/B;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_0

    const-string p1, "sGizvb"

    invoke-static {p1}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lcom/jscape/util/h/I;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jscape/util/h/I<",
            "TM;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/B;->b:Lcom/jscape/util/h/I;

    return-object v0
.end method

.method public a(Lcom/jscape/util/h/I;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/h/I<",
            "TM;>;)V"
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/a/B;->b:Lcom/jscape/util/h/I;

    return-void
.end method

.method public attributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/B;->a:Lcom/jscape/util/k/a/C;

    invoke-interface {v0}, Lcom/jscape/util/k/a/C;->attributes()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/jscape/util/k/a/C;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/B;->a:Lcom/jscape/util/k/a/C;

    return-object v0
.end method

.method public close()V
    .locals 2

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/util/k/a/B;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/jscape/util/k/a/B;->a:Lcom/jscape/util/k/a/C;

    invoke-interface {v0}, Lcom/jscape/util/k/a/C;->close()V

    return-void
.end method

.method public closed()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/B;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public creationTime()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/k/a/B;->a:Lcom/jscape/util/k/a/C;

    invoke-interface {v0}, Lcom/jscape/util/k/a/C;->creationTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public localAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/B;->a:Lcom/jscape/util/k/a/C;

    invoke-interface {v0}, Lcom/jscape/util/k/a/C;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method public read()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TM;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/k/a/B;->b:Lcom/jscape/util/h/I;

    iget-object v1, p0, Lcom/jscape/util/k/a/B;->d:Ljava/io/InputStream;

    invoke-interface {v0, v1}, Lcom/jscape/util/h/I;->read(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/k/a/Connection$ConnectionException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/util/k/a/Connection$ConnectionException;

    move-result-object v0

    throw v0

    :catch_1
    new-instance v0, Lcom/jscape/util/k/a/Connection$EOFException;

    invoke-direct {v0}, Lcom/jscape/util/k/a/Connection$EOFException;-><init>()V

    throw v0

    :catch_2
    move-exception v0

    new-instance v1, Lcom/jscape/util/k/a/Connection$ReadTimeoutException;

    invoke-direct {v1, v0}, Lcom/jscape/util/k/a/Connection$ReadTimeoutException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public remoteAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/B;->a:Lcom/jscape/util/k/a/C;

    invoke-interface {v0}, Lcom/jscape/util/k/a/C;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/B;->a:Lcom/jscape/util/k/a/C;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/k/a/B;->b:Lcom/jscape/util/h/I;

    iget-object v1, p0, Lcom/jscape/util/k/a/B;->c:Ljava/io/OutputStream;

    invoke-interface {v0, p1, v1}, Lcom/jscape/util/h/I;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V

    iget-object p1, p0, Lcom/jscape/util/k/a/B;->c:Ljava/io/OutputStream;

    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/jscape/util/k/a/Connection$WriteException;

    invoke-direct {v0, p1}, Lcom/jscape/util/k/a/Connection$WriteException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method
