.class public Lcom/jscape/util/k/a/q;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/k/a/n;
.implements Ljava/lang/Runnable;


# static fields
.field private static final a:I = -0x1


# instance fields
.field private final b:Lcom/jscape/util/k/a/C;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:[B

.field private final e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private f:Lcom/jscape/util/k/a/o;


# direct methods
.method public constructor <init>(Lcom/jscape/util/k/a/C;Ljava/util/concurrent/ExecutorService;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/a/q;->b:Lcom/jscape/util/k/a/C;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/k/a/q;->c:Ljava/util/concurrent/ExecutorService;

    new-array p1, p3, [B

    iput-object p1, p0, Lcom/jscape/util/k/a/q;->d:[B

    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/jscape/util/k/a/q;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method private static b(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public a()Lcom/jscape/util/k/a/C;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/q;->b:Lcom/jscape/util/k/a/C;

    return-object v0
.end method

.method public a(Lcom/jscape/util/k/a/o;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/a/q;->f:Lcom/jscape/util/k/a/o;

    iget-object p1, p0, Lcom/jscape/util/k/a/q;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, p0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method protected a(Ljava/lang/Throwable;)V
    .locals 1

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/util/k/a/q;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/util/k/a/q;->f:Lcom/jscape/util/k/a/o;

    invoke-interface {v0, p0, p1}, Lcom/jscape/util/k/a/o;->a(Lcom/jscape/util/k/a/n;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method public attributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/q;->b:Lcom/jscape/util/k/a/C;

    invoke-interface {v0}, Lcom/jscape/util/k/a/C;->attributes()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected b()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/k/a/q;->b:Lcom/jscape/util/k/a/C;

    iget-object v2, p0, Lcom/jscape/util/k/a/q;->d:[B

    iget-object v3, p0, Lcom/jscape/util/k/a/q;->d:[B

    array-length v3, v3

    const/4 v4, 0x0

    invoke-interface {v1, v2, v4, v3}, Lcom/jscape/util/k/a/C;->read([BII)I

    move-result v1
    :try_end_0
    .catch Lcom/jscape/util/k/a/Connection$ReadTimeoutException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/jscape/util/k/a/Connection$EOFException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_0

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    :try_start_1
    iget-object v2, p0, Lcom/jscape/util/k/a/q;->f:Lcom/jscape/util/k/a/o;

    invoke-interface {v2, p0}, Lcom/jscape/util/k/a/o;->a(Lcom/jscape/util/k/a/n;)V
    :try_end_1
    .catch Lcom/jscape/util/k/a/Connection$ReadTimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/jscape/util/k/a/Connection$EOFException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    invoke-static {v1}, Lcom/jscape/util/k/a/q;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    throw v1
    :try_end_2
    .catch Lcom/jscape/util/k/a/Connection$ReadTimeoutException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Lcom/jscape/util/k/a/Connection$EOFException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_0
    :goto_0
    if-nez v0, :cond_4

    :cond_1
    :try_start_3
    iget-object v2, p0, Lcom/jscape/util/k/a/q;->f:Lcom/jscape/util/k/a/o;

    iget-object v3, p0, Lcom/jscape/util/k/a/q;->d:[B

    invoke-interface {v2, p0, v3, v4, v1}, Lcom/jscape/util/k/a/o;->a(Lcom/jscape/util/k/a/n;[BII)V
    :try_end_3
    .catch Lcom/jscape/util/k/a/Connection$ReadTimeoutException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/jscape/util/k/a/Connection$EOFException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    :catch_1
    move-exception v1

    :try_start_4
    invoke-static {v1}, Lcom/jscape/util/k/a/q;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v1

    throw v1
    :try_end_4
    .catch Lcom/jscape/util/k/a/Connection$ReadTimeoutException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Lcom/jscape/util/k/a/Connection$EOFException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    if-eqz v0, :cond_2

    :try_start_5
    iget-object v0, p0, Lcom/jscape/util/k/a/q;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0
    :try_end_5
    .catch Lcom/jscape/util/k/a/Connection$ReadTimeoutException; {:try_start_5 .. :try_end_5} :catch_3

    if-nez v0, :cond_4

    goto :goto_1

    :catch_3
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/util/k/a/q;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Lcom/jscape/util/k/a/Connection$ReadTimeoutException; {:try_start_6 .. :try_end_6} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/k/a/q;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/jscape/util/k/a/q;->f:Lcom/jscape/util/k/a/o;

    invoke-interface {v0, p0}, Lcom/jscape/util/k/a/o;->a(Lcom/jscape/util/k/a/n;)V

    goto :goto_3

    :catch_5
    if-eqz v0, :cond_3

    :try_start_7
    iget-object v0, p0, Lcom/jscape/util/k/a/q;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0
    :try_end_7
    .catch Lcom/jscape/util/k/a/Connection$ReadTimeoutException; {:try_start_7 .. :try_end_7} :catch_6

    if-nez v0, :cond_4

    goto :goto_2

    :catch_6
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/util/k/a/q;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Lcom/jscape/util/k/a/Connection$ReadTimeoutException; {:try_start_8 .. :try_end_8} :catch_7

    :catch_7
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/k/a/q;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/jscape/util/k/a/q;->f:Lcom/jscape/util/k/a/o;

    invoke-interface {v0, p0}, Lcom/jscape/util/k/a/o;->c(Lcom/jscape/util/k/a/n;)V

    :cond_4
    :goto_3
    return-void
.end method

.method public close()V
    .locals 3

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/util/k/a/q;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/jscape/util/k/a/q;->b:Lcom/jscape/util/k/a/C;

    invoke-interface {v1}, Lcom/jscape/util/k/a/C;->close()V

    :cond_1
    iget-object v1, p0, Lcom/jscape/util/k/a/q;->f:Lcom/jscape/util/k/a/o;

    if-eqz v0, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    invoke-interface {v1, p0}, Lcom/jscape/util/k/a/o;->b(Lcom/jscape/util/k/a/n;)V

    :cond_3
    return-void
.end method

.method public closed()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/q;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public creationTime()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/k/a/q;->b:Lcom/jscape/util/k/a/C;

    invoke-interface {v0}, Lcom/jscape/util/k/a/C;->creationTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/q;->b:Lcom/jscape/util/k/a/C;

    invoke-interface {v0}, Lcom/jscape/util/k/a/C;->flush()V

    return-void
.end method

.method public localAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/q;->b:Lcom/jscape/util/k/a/C;

    invoke-interface {v0}, Lcom/jscape/util/k/a/C;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method public remoteAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/q;->b:Lcom/jscape/util/k/a/C;

    invoke-interface {v0}, Lcom/jscape/util/k/a/C;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method public run()V
    .locals 2

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/k/a/q;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v1, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/util/k/a/q;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    if-nez v0, :cond_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/util/k/a/q;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    invoke-virtual {p0, v0}, Lcom/jscape/util/k/a/q;->a(Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/q;->b:Lcom/jscape/util/k/a/C;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(B)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/q;->b:Lcom/jscape/util/k/a/C;

    invoke-interface {v0, p1}, Lcom/jscape/util/k/a/C;->write(B)V

    return-void
.end method

.method public write([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/q;->b:Lcom/jscape/util/k/a/C;

    invoke-interface {v0, p1, p2, p3}, Lcom/jscape/util/k/a/C;->write([BII)V

    return-void
.end method
