.class public Lcom/jscape/util/k/a/m;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/k/a/i;
.implements Lcom/jscape/util/k/a/w;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/jscape/util/k/a/i<",
        "TM;>;",
        "Lcom/jscape/util/k/a/w<",
        "TM;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/util/k/a/B;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/k/a/B<",
            "TM;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private d:Lcom/jscape/util/k/a/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/k/a/j<",
            "TM;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/jscape/util/k/a/B;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/B<",
            "TM;>;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/a/m;->a:Lcom/jscape/util/k/a/B;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/k/a/m;->b:Ljava/util/concurrent/ExecutorService;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/jscape/util/k/a/m;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>(Lcom/jscape/util/k/a/C;Lcom/jscape/util/h/I;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/C;",
            "Lcom/jscape/util/h/I<",
            "TM;>;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/k/a/B;

    invoke-direct {v0, p1, p2}, Lcom/jscape/util/k/a/B;-><init>(Lcom/jscape/util/k/a/C;Lcom/jscape/util/h/I;)V

    invoke-direct {p0, v0, p3}, Lcom/jscape/util/k/a/m;-><init>(Lcom/jscape/util/k/a/B;Ljava/util/concurrent/ExecutorService;)V

    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 1

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jscape/util/k/a/m;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/jscape/util/k/a/m;->d:Lcom/jscape/util/k/a/j;

    invoke-interface {v0, p0, p1}, Lcom/jscape/util/k/a/j;->a(Lcom/jscape/util/k/a/i;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method private static b(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    return-object p0
.end method

.method private d()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/k/a/m;->a:Lcom/jscape/util/k/a/B;

    invoke-virtual {v1}, Lcom/jscape/util/k/a/B;->read()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/util/k/a/m;->d:Lcom/jscape/util/k/a/j;

    invoke-interface {v2, p0, v1}, Lcom/jscape/util/k/a/j;->a(Lcom/jscape/util/k/a/i;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/jscape/util/k/a/Connection$ReadTimeoutException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/jscape/util/k/a/Connection$EOFException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/util/k/a/m;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0
    :try_end_1
    .catch Lcom/jscape/util/k/a/Connection$ReadTimeoutException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_2

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/util/k/a/m;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Lcom/jscape/util/k/a/Connection$ReadTimeoutException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/k/a/m;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/jscape/util/k/a/m;->d:Lcom/jscape/util/k/a/j;

    invoke-interface {v0, p0}, Lcom/jscape/util/k/a/j;->a(Lcom/jscape/util/k/a/i;)V

    goto :goto_2

    :catch_3
    if-eqz v0, :cond_1

    :try_start_3
    iget-object v0, p0, Lcom/jscape/util/k/a/m;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0
    :try_end_3
    .catch Lcom/jscape/util/k/a/Connection$ReadTimeoutException; {:try_start_3 .. :try_end_3} :catch_4

    if-nez v0, :cond_2

    goto :goto_1

    :catch_4
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/util/k/a/m;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Lcom/jscape/util/k/a/Connection$ReadTimeoutException; {:try_start_4 .. :try_end_4} :catch_5

    :catch_5
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/k/a/m;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/jscape/util/k/a/m;->d:Lcom/jscape/util/k/a/j;

    invoke-interface {v0, p0}, Lcom/jscape/util/k/a/j;->c(Lcom/jscape/util/k/a/i;)V

    :cond_2
    :goto_2
    return-void
.end method


# virtual methods
.method public a()Lcom/jscape/util/h/I;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jscape/util/h/I<",
            "TM;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/m;->a:Lcom/jscape/util/k/a/B;

    invoke-virtual {v0}, Lcom/jscape/util/k/a/B;->a()Lcom/jscape/util/h/I;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/jscape/util/h/I;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/h/I<",
            "TM;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/m;->a:Lcom/jscape/util/k/a/B;

    invoke-virtual {v0, p1}, Lcom/jscape/util/k/a/B;->a(Lcom/jscape/util/h/I;)V

    return-void
.end method

.method public a(Lcom/jscape/util/k/a/j;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/j<",
            "TM;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/a/m;->d:Lcom/jscape/util/k/a/j;

    iget-object p1, p0, Lcom/jscape/util/k/a/m;->b:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, p0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public attributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/m;->a:Lcom/jscape/util/k/a/B;

    invoke-virtual {v0}, Lcom/jscape/util/k/a/B;->attributes()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/jscape/util/k/a/C;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/m;->a:Lcom/jscape/util/k/a/B;

    invoke-virtual {v0}, Lcom/jscape/util/k/a/B;->c()Lcom/jscape/util/k/a/C;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 3

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/util/k/a/m;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/jscape/util/k/a/m;->a:Lcom/jscape/util/k/a/B;

    invoke-virtual {v1}, Lcom/jscape/util/k/a/B;->close()V

    :cond_1
    iget-object v1, p0, Lcom/jscape/util/k/a/m;->d:Lcom/jscape/util/k/a/j;

    if-eqz v0, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    invoke-interface {v1, p0}, Lcom/jscape/util/k/a/j;->b(Lcom/jscape/util/k/a/i;)V

    :cond_3
    return-void
.end method

.method public closed()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/m;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public creationTime()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/k/a/m;->a:Lcom/jscape/util/k/a/B;

    invoke-virtual {v0}, Lcom/jscape/util/k/a/B;->creationTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public localAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/m;->a:Lcom/jscape/util/k/a/B;

    invoke-virtual {v0}, Lcom/jscape/util/k/a/B;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method public remoteAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/m;->a:Lcom/jscape/util/k/a/B;

    invoke-virtual {v0}, Lcom/jscape/util/k/a/B;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method public run()V
    .locals 2

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/k/a/m;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v1, :cond_1

    :try_start_1
    invoke-direct {p0}, Lcom/jscape/util/k/a/m;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    if-nez v0, :cond_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/util/k/a/m;->b(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    invoke-direct {p0, v0}, Lcom/jscape/util/k/a/m;->a(Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/m;->a:Lcom/jscape/util/k/a/B;

    invoke-virtual {v0}, Lcom/jscape/util/k/a/B;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/m;->a:Lcom/jscape/util/k/a/B;

    invoke-virtual {v0, p1}, Lcom/jscape/util/k/a/B;->write(Ljava/lang/Object;)V

    return-void
.end method
