.class public interface abstract Lcom/jscape/util/k/a/C;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/k/a/z;


# virtual methods
.method public abstract availableBytes()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation
.end method

.method public abstract read()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation
.end method

.method public abstract read([BII)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation
.end method
