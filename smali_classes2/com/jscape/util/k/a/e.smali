.class public Lcom/jscape/util/k/a/e;
.super Ljava/io/OutputStream;


# instance fields
.field private final a:Lcom/jscape/util/k/a/C;


# direct methods
.method public constructor <init>(Lcom/jscape/util/k/a/C;)V
    .locals 0

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/a/e;->a:Lcom/jscape/util/k/a/C;

    return-void
.end method


# virtual methods
.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/e;->a:Lcom/jscape/util/k/a/C;

    invoke-interface {v0}, Lcom/jscape/util/k/a/C;->flush()V

    return-void
.end method

.method public write(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/e;->a:Lcom/jscape/util/k/a/C;

    int-to-byte p1, p1

    invoke-interface {v0, p1}, Lcom/jscape/util/k/a/C;->write(B)V

    return-void
.end method

.method public write([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/e;->a:Lcom/jscape/util/k/a/C;

    invoke-interface {v0, p1, p2, p3}, Lcom/jscape/util/k/a/C;->write([BII)V

    return-void
.end method
