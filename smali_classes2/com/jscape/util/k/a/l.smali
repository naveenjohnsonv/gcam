.class public Lcom/jscape/util/k/a/l;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/k/a/i;
.implements Lcom/jscape/util/k/a/w;
.implements Lcom/jscape/util/k/a/o;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/jscape/util/k/a/i<",
        "TM;>;",
        "Lcom/jscape/util/k/a/w<",
        "TM;>;",
        "Lcom/jscape/util/k/a/o;"
    }
.end annotation


# instance fields
.field private final b:Lcom/jscape/util/k/a/n;

.field private c:Lcom/jscape/util/h/I;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/I<",
            "TM;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/jscape/util/h/o;

.field private final e:Lcom/jscape/util/h/o;

.field private final f:Lcom/jscape/util/h/e;

.field private final g:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private h:Lcom/jscape/util/k/a/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/k/a/j<",
            "TM;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/jscape/util/k/a/n;Lcom/jscape/util/h/I;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/n;",
            "Lcom/jscape/util/h/I<",
            "TM;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/a/l;->b:Lcom/jscape/util/k/a/n;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/k/a/l;->c:Lcom/jscape/util/h/I;

    new-instance p1, Lcom/jscape/util/h/o;

    invoke-direct {p1}, Lcom/jscape/util/h/o;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/k/a/l;->d:Lcom/jscape/util/h/o;

    new-instance p1, Lcom/jscape/util/h/o;

    invoke-direct {p1}, Lcom/jscape/util/h/o;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/k/a/l;->e:Lcom/jscape/util/h/o;

    new-instance p1, Lcom/jscape/util/h/e;

    iget-object p2, p0, Lcom/jscape/util/k/a/l;->e:Lcom/jscape/util/h/o;

    invoke-virtual {p2}, Lcom/jscape/util/h/o;->a()[B

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/jscape/util/h/e;-><init>([B)V

    iput-object p1, p0, Lcom/jscape/util/k/a/l;->f:Lcom/jscape/util/h/e;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/jscape/util/k/a/l;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-nez v0, :cond_0

    const/4 p1, 0x5

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private c()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TM;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/jscape/util/k/a/l;->f:Lcom/jscape/util/h/e;

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/jscape/util/h/e;->available()I

    move-result v0
    :try_end_0
    .catch Lcom/jscape/util/k/a/Connection$EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/jscape/util/k/a/l;->c:Lcom/jscape/util/h/I;

    iget-object v2, p0, Lcom/jscape/util/k/a/l;->f:Lcom/jscape/util/h/e;

    invoke-interface {v0, v2}, Lcom/jscape/util/h/I;->read(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v2

    :cond_1
    iget-object v0, p0, Lcom/jscape/util/k/a/l;->e:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->b()I

    move-result v0

    iget-object v3, p0, Lcom/jscape/util/k/a/l;->f:Lcom/jscape/util/h/e;

    invoke-virtual {v3}, Lcom/jscape/util/h/e;->available()I

    move-result v3

    sub-int/2addr v0, v3

    iget-object v3, p0, Lcom/jscape/util/k/a/l;->e:Lcom/jscape/util/h/o;

    invoke-virtual {v3, v0}, Lcom/jscape/util/h/o;->b(I)Lcom/jscape/util/h/o;

    iget-object v0, p0, Lcom/jscape/util/k/a/l;->f:Lcom/jscape/util/h/e;

    iget-object v3, p0, Lcom/jscape/util/k/a/l;->e:Lcom/jscape/util/h/o;

    invoke-virtual {v3}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jscape/util/k/a/l;->e:Lcom/jscape/util/h/o;

    invoke-virtual {v5}, Lcom/jscape/util/h/o;->b()I

    move-result v5

    invoke-virtual {v0, v3, v4, v5}, Lcom/jscape/util/h/e;->a([BII)V

    return-object v2

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/k/a/l;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Lcom/jscape/util/k/a/Connection$EOFException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-object v1
.end method


# virtual methods
.method public a()Lcom/jscape/util/h/I;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jscape/util/h/I<",
            "TM;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/l;->c:Lcom/jscape/util/h/I;

    return-object v0
.end method

.method public a(Lcom/jscape/util/h/I;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/h/I<",
            "TM;>;)V"
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/a/l;->c:Lcom/jscape/util/h/I;

    return-void
.end method

.method public a(Lcom/jscape/util/k/a/j;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/k/a/j<",
            "TM;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/a/l;->h:Lcom/jscape/util/k/a/j;

    iget-object p1, p0, Lcom/jscape/util/k/a/l;->b:Lcom/jscape/util/k/a/n;

    invoke-interface {p1, p0}, Lcom/jscape/util/k/a/n;->a(Lcom/jscape/util/k/a/o;)V

    return-void
.end method

.method public a(Lcom/jscape/util/k/a/n;)V
    .locals 0

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/jscape/util/k/a/l;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-nez p1, :cond_1

    :cond_0
    iget-object p1, p0, Lcom/jscape/util/k/a/l;->h:Lcom/jscape/util/k/a/j;

    invoke-interface {p1, p0}, Lcom/jscape/util/k/a/j;->a(Lcom/jscape/util/k/a/i;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/jscape/util/k/a/n;Ljava/lang/Throwable;)V
    .locals 0

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/jscape/util/k/a/l;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-nez p1, :cond_1

    :cond_0
    iget-object p1, p0, Lcom/jscape/util/k/a/l;->h:Lcom/jscape/util/k/a/j;

    invoke-interface {p1, p0, p2}, Lcom/jscape/util/k/a/j;->a(Lcom/jscape/util/k/a/i;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method public a(Lcom/jscape/util/k/a/n;[BII)V
    .locals 1

    iget-object p1, p0, Lcom/jscape/util/k/a/l;->e:Lcom/jscape/util/h/o;

    invoke-virtual {p1, p2, p3, p4}, Lcom/jscape/util/h/o;->write([BII)V

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lcom/jscape/util/k/a/l;->f:Lcom/jscape/util/h/e;

    iget-object p3, p0, Lcom/jscape/util/k/a/l;->e:Lcom/jscape/util/h/o;

    invoke-virtual {p3}, Lcom/jscape/util/h/o;->a()[B

    move-result-object p3

    iget-object p4, p0, Lcom/jscape/util/k/a/l;->e:Lcom/jscape/util/h/o;

    invoke-virtual {p4}, Lcom/jscape/util/h/o;->b()I

    move-result p4

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0, p4}, Lcom/jscape/util/h/e;->a([BII)V

    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/jscape/util/k/a/l;->c()Ljava/lang/Object;

    move-result-object p2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz p2, :cond_2

    :try_start_1
    iget-object p3, p0, Lcom/jscape/util/k/a/l;->h:Lcom/jscape/util/k/a/j;

    invoke-interface {p3, p0, p2}, Lcom/jscape/util/k/a/j;->a(Lcom/jscape/util/k/a/i;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz p1, :cond_2

    if-nez p1, :cond_0

    goto :goto_1

    :catch_0
    move-exception p2

    :try_start_2
    invoke-static {p2}, Lcom/jscape/util/k/a/l;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p2

    throw p2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p2

    if-eqz p1, :cond_1

    :try_start_3
    iget-object p1, p0, Lcom/jscape/util/k/a/l;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    if-nez p1, :cond_2

    goto :goto_0

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/util/k/a/l;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/k/a/l;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/jscape/util/k/a/l;->h:Lcom/jscape/util/k/a/j;

    invoke-interface {p1, p0, p2}, Lcom/jscape/util/k/a/j;->a(Lcom/jscape/util/k/a/i;Ljava/lang/Throwable;)V

    :cond_2
    :goto_1
    return-void
.end method

.method public attributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/k/a/l;->b:Lcom/jscape/util/k/a/n;

    invoke-interface {v0}, Lcom/jscape/util/k/a/n;->attributes()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/jscape/util/k/a/n;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/l;->b:Lcom/jscape/util/k/a/n;

    return-object v0
.end method

.method public b(Lcom/jscape/util/k/a/n;)V
    .locals 1

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/jscape/util/k/a/l;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-nez p1, :cond_1

    :cond_0
    iget-object p1, p0, Lcom/jscape/util/k/a/l;->h:Lcom/jscape/util/k/a/j;

    new-instance v0, Lcom/jscape/util/k/a/Connection$ConnectionClosedException;

    invoke-direct {v0}, Lcom/jscape/util/k/a/Connection$ConnectionClosedException;-><init>()V

    invoke-interface {p1, p0, v0}, Lcom/jscape/util/k/a/j;->a(Lcom/jscape/util/k/a/i;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method public c(Lcom/jscape/util/k/a/n;)V
    .locals 0

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/jscape/util/k/a/l;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-nez p1, :cond_1

    :cond_0
    iget-object p1, p0, Lcom/jscape/util/k/a/l;->h:Lcom/jscape/util/k/a/j;

    invoke-interface {p1, p0}, Lcom/jscape/util/k/a/j;->c(Lcom/jscape/util/k/a/i;)V

    :cond_1
    return-void
.end method

.method public close()V
    .locals 3

    invoke-static {}, Lcom/jscape/util/k/a/Connection$ConnectionException;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jscape/util/k/a/l;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/jscape/util/k/a/l;->b:Lcom/jscape/util/k/a/n;

    invoke-interface {v1}, Lcom/jscape/util/k/a/n;->close()V

    :cond_1
    iget-object v1, p0, Lcom/jscape/util/k/a/l;->h:Lcom/jscape/util/k/a/j;

    if-eqz v0, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    invoke-interface {v1, p0}, Lcom/jscape/util/k/a/j;->b(Lcom/jscape/util/k/a/i;)V

    :cond_3
    return-void
.end method

.method public closed()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/l;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public creationTime()J
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/k/a/l;->b:Lcom/jscape/util/k/a/n;

    invoke-interface {v0}, Lcom/jscape/util/k/a/n;->creationTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public localAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/l;->b:Lcom/jscape/util/k/a/n;

    invoke-interface {v0}, Lcom/jscape/util/k/a/n;->localAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method public remoteAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/l;->b:Lcom/jscape/util/k/a/n;

    invoke-interface {v0}, Lcom/jscape/util/k/a/n;->remoteAddress()Lcom/jscape/util/k/TransportAddress;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/a/l;->b:Lcom/jscape/util/k/a/n;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/a/Connection$ConnectionException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/k/a/l;->c:Lcom/jscape/util/h/I;

    iget-object v1, p0, Lcom/jscape/util/k/a/l;->d:Lcom/jscape/util/h/o;

    invoke-interface {v0, p1, v1}, Lcom/jscape/util/h/I;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V

    iget-object p1, p0, Lcom/jscape/util/k/a/l;->b:Lcom/jscape/util/k/a/n;

    iget-object v0, p0, Lcom/jscape/util/k/a/l;->d:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/jscape/util/k/a/l;->d:Lcom/jscape/util/h/o;

    invoke-virtual {v2}, Lcom/jscape/util/h/o;->b()I

    move-result v2

    invoke-interface {p1, v0, v1, v2}, Lcom/jscape/util/k/a/n;->write([BII)V

    iget-object p1, p0, Lcom/jscape/util/k/a/l;->b:Lcom/jscape/util/k/a/n;

    invoke-interface {p1}, Lcom/jscape/util/k/a/n;->flush()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lcom/jscape/util/k/a/l;->d:Lcom/jscape/util/h/o;

    invoke-virtual {p1}, Lcom/jscape/util/h/o;->c()Lcom/jscape/util/h/o;

    return-void

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/util/k/a/Connection$ConnectionException;->wrap(Ljava/lang/Throwable;)Lcom/jscape/util/k/a/Connection$ConnectionException;

    move-result-object p1

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    iget-object v0, p0, Lcom/jscape/util/k/a/l;->d:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->c()Lcom/jscape/util/h/o;

    throw p1
.end method
