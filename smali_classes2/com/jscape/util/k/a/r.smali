.class public interface abstract Lcom/jscape/util/k/a/r;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/AutoCloseable;


# virtual methods
.method public abstract attributes()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract close()V
.end method

.method public abstract closed()Z
.end method

.method public abstract creationTime()J
.end method

.method public abstract localAddress()Lcom/jscape/util/k/TransportAddress;
.end method

.method public abstract remoteAddress()Lcom/jscape/util/k/TransportAddress;
.end method
