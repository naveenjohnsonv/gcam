.class public Lcom/jscape/util/k/h;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x36

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x29

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "t\u0002\u0005\u0005T`M\u0018,,(\u0017nG\u0018\u000f\u0003\u001fR!hhM\u0003\u0002S!LY\u001e\tLDiNM\u0001\u0006LGsN\\\u0018\u0001\t\u0017c@K\u0008B%g>fE>\u0008L0\r\u000b\u0010K0\u0019)}\\\u0016S@\u0004|\u0008dCJ0Sz\u0010\u0014^\u001fEk/\td\t\u0019]\u001b2\\\u0011BJ_l1\u000c\n0\u001e7\u0006,\u0013eR>\u0008\u001e%"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x75

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/k/h;->b:[Ljava/lang/String;

    aget-object v0, v1, v7

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/k/h;->a:Ljava/util/regex/Pattern;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x8

    goto :goto_2

    :cond_2
    const/16 v12, 0x28

    goto :goto_2

    :cond_3
    const/16 v12, 0x1e

    goto :goto_2

    :cond_4
    const/16 v12, 0x45

    goto :goto_2

    :cond_5
    const/16 v12, 0x4b

    goto :goto_2

    :cond_6
    const/16 v12, 0x44

    goto :goto_2

    :cond_7
    const/16 v12, 0x11

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    return-object p0
.end method

.method public static a(I)Ljava/net/InetAddress;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    const/4 v0, 0x4

    new-array v1, v0, [B

    ushr-int/lit8 v2, p0, 0x18

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    const/4 v3, 0x0

    aput-byte v2, v1, v3

    ushr-int/lit8 v2, p0, 0x10

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    const/4 v3, 0x1

    aput-byte v2, v1, v3

    ushr-int/lit8 v2, p0, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    const/4 v3, 0x2

    aput-byte v2, v1, v3

    invoke-static {}, Lcom/jscape/util/k/TransportAddress;->b()Ljava/lang/String;

    move-result-object v2

    and-int/lit16 p0, p0, 0xff

    int-to-byte p0, p0

    const/4 v3, 0x3

    aput-byte p0, v1, v3

    :try_start_0
    invoke-static {v1}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object p0

    if-nez v2, :cond_0

    new-array v0, v0, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-object p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/h;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
.end method

.method public static a(Ljava/net/InetAddress;)Ljava/net/InetAddress;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    invoke-virtual {p0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object p0

    invoke-static {}, Lcom/jscape/util/k/TransportAddress;->b()Ljava/lang/String;

    move-result-object v0

    array-length v1, p0

    add-int/lit8 v1, v1, -0x1

    :cond_0
    if-ltz v1, :cond_1

    :try_start_0
    aget-byte v2, p0, v1
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_2

    if-eqz v0, :cond_2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    const/4 v2, 0x0

    :try_start_1
    aput-byte v2, p0, v1
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_2

    add-int/lit8 v1, v1, -0x1

    if-nez v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_2
    invoke-static {p0}, Lcom/jscape/util/k/h;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
    :try_end_2
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p0

    :try_start_3
    invoke-static {p0}, Lcom/jscape/util/k/h;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
    :try_end_3
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/h;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    move v2, v1

    :cond_2
    if-ltz v2, :cond_3

    :try_start_4
    aget-byte v0, p0, v1

    add-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    aput-byte v0, p0, v1

    invoke-static {p0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object p0
    :try_end_4
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_3

    return-object p0

    :catch_3
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/h;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_3
    new-instance p0, Ljava/net/UnknownHostException;

    invoke-direct {p0}, Ljava/net/UnknownHostException;-><init>()V

    throw p0
.end method

.method public static a(Lcom/jscape/util/k/TransportAddress;Lcom/jscape/util/k/TransportAddress;)Z
    .locals 3

    invoke-static {}, Lcom/jscape/util/k/TransportAddress;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/util/k/TransportAddress;->getPort()I

    move-result v1

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/jscape/util/k/TransportAddress;->getPort()I

    move-result v2
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v1, v2, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/util/k/TransportAddress;->getHost()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1}, Lcom/jscape/util/k/TransportAddress;->getHost()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/jscape/util/k/h;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    :cond_0
    if-eqz v0, :cond_2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move p0, v1

    :goto_1
    return p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/h;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/h;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
.end method

.method public static a(Ljava/lang/String;ILjava/lang/String;I)Z
    .locals 1

    invoke-static {}, Lcom/jscape/util/k/TransportAddress;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-ne p1, p3, :cond_1

    :try_start_0
    invoke-static {p0, p2}, Lcom/jscape/util/k/h;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/h;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    move p0, p1

    :goto_2
    return p0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    invoke-static {}, Lcom/jscape/util/k/TransportAddress;->b()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v1

    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return p0

    :catchall_0
    if-eqz v0, :cond_0

    if-eqz p0, :cond_1

    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_2

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :cond_2
    :goto_0
    return p0

    :catchall_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/h;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
.end method

.method public static a(Ljava/lang/String;)[Ljava/net/InetAddress;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/TransportAddress;->b()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/jscape/util/k/h;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v0, :cond_5

    if-eqz v2, :cond_4

    const/4 p0, 0x4

    invoke-virtual {v1, p0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    and-int/lit16 p0, p0, 0xff

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr p0, v2

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr p0, v2

    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x18

    or-int/2addr p0, v2

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    rsub-int/lit8 v1, v1, 0x20

    shl-int/2addr v2, v1

    and-int/2addr v2, p0

    if-eqz v0, :cond_1

    if-ne v2, p0, :cond_0

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/jscape/util/k/h;->b:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/h;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_1
    move v1, p0

    move v4, v2

    :goto_0
    shl-int v1, v4, v1

    new-array v2, v1, [Ljava/net/InetAddress;

    :cond_2
    if-ge v3, v1, :cond_3

    if-eqz v0, :cond_3

    :try_start_2
    invoke-static {p0}, Lcom/jscape/util/k/h;->a(I)Ljava/net/InetAddress;

    move-result-object v4

    aput-object v4, v2, v3
    :try_end_2
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_1

    add-int/lit8 p0, p0, 0x1

    add-int/lit8 v3, v3, 0x1

    if-nez v0, :cond_2

    goto :goto_1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/h;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_3
    :goto_1
    return-object v2

    :cond_4
    move v2, v4

    :cond_5
    new-array v0, v2, [Ljava/net/InetAddress;

    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object p0

    aput-object p0, v0, v3

    return-object v0

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/k/h;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    :try_start_0
    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-object p0
.end method
