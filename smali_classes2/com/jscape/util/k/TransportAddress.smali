.class public Lcom/jscape/util/k/TransportAddress;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static b:Ljava/lang/String;

.field private static final c:Ljava/lang/String;


# instance fields
.field private host:Ljava/lang/String;

.field private inetAddress:Ljava/net/InetAddress;

.field private port:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const-string v0, "CYppU"

    invoke-static {v0}, Lcom/jscape/util/k/TransportAddress;->b(Ljava/lang/String;)V

    const-string v0, "pl=\u0010\u0000"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/k/TransportAddress;->c:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v5, 0x40

    const/4 v6, 0x1

    if-eqz v4, :cond_4

    if-eq v4, v6, :cond_3

    const/4 v7, 0x2

    if-eq v4, v7, :cond_2

    const/4 v7, 0x3

    if-eq v4, v7, :cond_1

    const/4 v7, 0x4

    if-eq v4, v7, :cond_5

    const/4 v7, 0x5

    if-eq v4, v7, :cond_5

    move v5, v6

    goto :goto_1

    :cond_1
    const/4 v5, 0x6

    goto :goto_1

    :cond_2
    const/16 v5, 0x34

    goto :goto_1

    :cond_3
    const/16 v5, 0x2c

    goto :goto_1

    :cond_4
    const/16 v5, 0x66

    :cond_5
    :goto_1
    const/16 v4, 0x33

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/util/k/TransportAddress;)V
    .locals 2

    iget-object v0, p1, Lcom/jscape/util/k/TransportAddress;->inetAddress:Ljava/net/InetAddress;

    iget-object v1, p1, Lcom/jscape/util/k/TransportAddress;->host:Ljava/lang/String;

    iget p1, p1, Lcom/jscape/util/k/TransportAddress;->port:I

    invoke-direct {p0, v0, v1, p1}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/net/InetAddress;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/net/InetAddress;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Ljava/net/InetAddress;I)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1, v0, p2}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/net/InetAddress;Ljava/lang/String;I)V

    return-void
.end method

.method private constructor <init>(Ljava/net/InetAddress;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/k/TransportAddress;->inetAddress:Ljava/net/InetAddress;

    iput-object p2, p0, Lcom/jscape/util/k/TransportAddress;->host:Ljava/lang/String;

    iput p3, p0, Lcom/jscape/util/k/TransportAddress;->port:I

    return-void
.end method

.method public constructor <init>(Ljava/net/InetSocketAddress;)V
    .locals 2

    invoke-virtual {p1}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {p1}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/net/InetSocketAddress;->getPort()I

    move-result p1

    invoke-direct {p0, v0, v1, p1}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/net/InetAddress;Ljava/lang/String;I)V

    return-void
.end method

.method private static a(Ljava/net/UnknownHostException;)Ljava/net/UnknownHostException;
    .locals 0

    return-object p0
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/util/k/TransportAddress;->b:Ljava/lang/String;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/util/k/TransportAddress;->b:Ljava/lang/String;

    return-void
.end method

.method public static forUrl(Ljava/net/URL;)Lcom/jscape/util/k/TransportAddress;
    .locals 2

    new-instance v0, Lcom/jscape/util/k/TransportAddress;

    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/net/URL;->getPort()I

    move-result p0

    invoke-direct {v0, v1, p0}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method


# virtual methods
.method public asSocketAddress()Ljava/net/InetSocketAddress;
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/k/TransportAddress;->inetAddress:Ljava/net/InetAddress;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v1, p0, Lcom/jscape/util/k/TransportAddress;->inetAddress:Ljava/net/InetAddress;

    iget v2, p0, Lcom/jscape/util/k/TransportAddress;->port:I

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v1, p0, Lcom/jscape/util/k/TransportAddress;->host:Ljava/lang/String;

    iget v2, p0, Lcom/jscape/util/k/TransportAddress;->port:I

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    :goto_0
    return-object v0
.end method

.method public domainAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/k/TransportAddress;

    invoke-virtual {p0}, Lcom/jscape/util/k/TransportAddress;->inetAddress()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/jscape/util/k/TransportAddress;->port:I

    invoke-direct {v0, v1, v2}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    invoke-static {}, Lcom/jscape/util/k/TransportAddress;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    move-object v1, p1

    goto :goto_0

    :cond_1
    move-object v1, p0

    :goto_0
    const/4 v2, 0x0

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v1, v3, :cond_3

    goto :goto_2

    :cond_2
    move-object p1, v1

    :cond_3
    check-cast p1, Lcom/jscape/util/k/TransportAddress;

    if-eqz v0, :cond_4

    iget v1, p0, Lcom/jscape/util/k/TransportAddress;->port:I

    iget v3, p1, Lcom/jscape/util/k/TransportAddress;->port:I

    if-eq v1, v3, :cond_4

    return v2

    :cond_4
    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/jscape/util/k/TransportAddress;->inetAddress:Ljava/net/InetAddress;

    if-eqz v1, :cond_6

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/jscape/util/k/TransportAddress;->inetAddress:Ljava/net/InetAddress;

    if-eqz v0, :cond_6

    invoke-virtual {v1, v0}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_5
    move-object v0, p1

    goto :goto_1

    :cond_6
    move-object v0, p0

    :goto_1
    iget-object v0, v0, Lcom/jscape/util/k/TransportAddress;->host:Ljava/lang/String;

    iget-object p1, p1, Lcom/jscape/util/k/TransportAddress;->host:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/jscape/util/k/h;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1

    :cond_7
    :goto_2
    return v2
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/TransportAddress;->host:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    iget v0, p0, Lcom/jscape/util/k/TransportAddress;->port:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/k/TransportAddress;->host:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/jscape/util/k/TransportAddress;->port:I

    add-int/2addr v0, v1

    return v0
.end method

.method public hostName()Ljava/lang/String;
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/util/k/TransportAddress;->inetAddress()Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostName()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    iget-object v0, p0, Lcom/jscape/util/k/TransportAddress;->host:Ljava/lang/String;

    return-object v0
.end method

.method public inetAddress()Ljava/net/InetAddress;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/k/TransportAddress;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/k/TransportAddress;->inetAddress:Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/util/k/TransportAddress;->inetAddress:Ljava/net/InetAddress;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/k/TransportAddress;->a(Ljava/net/UnknownHostException;)Ljava/net/UnknownHostException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/k/TransportAddress;->a(Ljava/net/UnknownHostException;)Ljava/net/UnknownHostException;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/jscape/util/k/TransportAddress;->host:Ljava/lang/String;

    invoke-static {v0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public ipAddress()Lcom/jscape/util/k/TransportAddress;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/k/TransportAddress;

    invoke-virtual {p0}, Lcom/jscape/util/k/TransportAddress;->inetAddress()Ljava/net/InetAddress;

    move-result-object v1

    iget v2, p0, Lcom/jscape/util/k/TransportAddress;->port:I

    invoke-direct {v0, v1, v2}, Lcom/jscape/util/k/TransportAddress;-><init>(Ljava/net/InetAddress;I)V

    return-object v0
.end method

.method public setHost(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jscape/util/k/TransportAddress;->host:Ljava/lang/String;

    return-void
.end method

.method public setPort(I)V
    .locals 0

    iput p1, p0, Lcom/jscape/util/k/TransportAddress;->port:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/jscape/util/k/TransportAddress;->c:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/jscape/util/k/TransportAddress;->host:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget v2, p0, Lcom/jscape/util/k/TransportAddress;->port:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
