.class Lcom/jscape/util/k/f;
.super Lcom/jscape/util/g/z;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/jscape/util/g/z<",
        "Ljava/net/NetworkInterface;",
        "Lcom/jscape/util/k/NetworkInterfaceInfo;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/util/g/z;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/net/NetworkInterface;)Lcom/jscape/util/k/NetworkInterfaceInfo;
    .locals 4

    invoke-virtual {p1}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v0

    new-instance v1, Lcom/jscape/util/k/e;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/jscape/util/k/e;-><init>(Lcom/jscape/util/k/i;)V

    invoke-static {v0, v1}, Lcom/jscape/util/b/r;->a(Ljava/util/Enumeration;Lcom/jscape/util/g/z;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/jscape/util/k/NetworkInterfaceInfo;

    invoke-virtual {p1}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/net/NetworkInterface;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Ljava/net/NetworkInterface;->getDisplayName()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-direct {v1, v2, p1, v0}, Lcom/jscape/util/k/NetworkInterfaceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    return-object v1
.end method

.method public bridge synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/net/NetworkInterface;

    invoke-virtual {p0, p1}, Lcom/jscape/util/k/f;->a(Ljava/net/NetworkInterface;)Lcom/jscape/util/k/NetworkInterfaceInfo;

    move-result-object p1

    return-object p1
.end method
