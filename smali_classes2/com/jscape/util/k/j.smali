.class public Lcom/jscape/util/k/j;
.super Ljava/lang/Object;


# static fields
.field public static final a:I = 0xffff

.field public static final b:I = 0x1

.field private static final f:[Ljava/lang/String;


# instance fields
.field private c:I

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/16 v0, 0x8

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "\u001c.&vzW\'0+l\n\u001c.&vzW\'0+l\u0017\u0010 b0j]7~?-$l\u00183(.+:yZ>;a\u000b\u001c.&vzW\'0+1x\u0015\u000e 0\"JY<9*9:wO7,\r-#v\\o\u000b\u001c.&vzW\'0+1x"

    const/16 v5, 0x5b

    const/16 v6, 0xa

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x22

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x16

    const-string v4, "ex%.}[6t\rex 1\u007fJ7\u000b7 /k\u0012"

    move v6, v0

    move v8, v11

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x35

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/k/j;->f:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v0, v14, 0x7

    if-eqz v0, :cond_9

    if-eq v0, v10, :cond_8

    const/4 v2, 0x2

    if-eq v0, v2, :cond_7

    const/4 v2, 0x3

    if-eq v0, v2, :cond_6

    const/4 v2, 0x4

    if-eq v0, v2, :cond_5

    const/4 v2, 0x5

    if-eq v0, v2, :cond_4

    const/16 v0, 0x70

    goto :goto_4

    :cond_4
    const/16 v0, 0x1a

    goto :goto_4

    :cond_5
    const/16 v0, 0x3a

    goto :goto_4

    :cond_6
    const/16 v0, 0x74

    goto :goto_4

    :cond_7
    const/16 v0, 0x60

    goto :goto_4

    :cond_8
    const/16 v0, 0x6d

    goto :goto_4

    :cond_9
    const/16 v0, 0x7c

    :goto_4
    xor-int/2addr v0, v9

    xor-int/2addr v0, v15

    int-to-char v0, v0

    aput-char v0, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/16 v0, 0x8

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x1

    const v1, 0xffff

    invoke-direct {p0, v0, v1}, Lcom/jscape/util/k/j;-><init>(II)V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 20

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/jscape/util/k/TransportAddress;->b()Ljava/lang/String;

    int-to-long v10, v1

    sget-object v12, Lcom/jscape/util/k/j;->f:[Ljava/lang/String;

    const/4 v3, 0x5

    aget-object v9, v12, v3

    const-wide/16 v5, 0x1

    const-wide/32 v7, 0xffff

    move-wide v3, v10

    invoke-static/range {v3 .. v9}, Lcom/jscape/util/w;->a(JJJLjava/lang/String;)V

    int-to-long v3, v2

    const/4 v5, 0x3

    aget-object v19, v12, v5

    const-wide/16 v15, 0x1

    const-wide/32 v17, 0xffff

    move-wide v13, v3

    invoke-static/range {v13 .. v19}, Lcom/jscape/util/w;->a(JJJLjava/lang/String;)V

    aget-object v5, v12, v5

    invoke-static {v3, v4, v10, v11, v5}, Lcom/jscape/util/w;->b(JJLjava/lang/String;)V

    iput v1, v0, Lcom/jscape/util/k/j;->c:I

    iput v2, v0, Lcom/jscape/util/k/j;->d:I

    new-instance v3, Ljava/util/BitSet;

    sub-int v1, v2, v1

    invoke-direct {v3, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v3, v0, Lcom/jscape/util/k/j;->e:Ljava/util/BitSet;

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "igMzDb"

    invoke-static {v1}, Lcom/jscape/util/k/TransportAddress;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private static a(Lcom/jscape/util/k/d;)Lcom/jscape/util/k/d;
    .locals 0

    return-object p0
.end method

.method private b(II)V
    .locals 6

    new-instance v0, Ljava/util/BitSet;

    sub-int v1, p2, p1

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    invoke-static {}, Lcom/jscape/util/k/TransportAddress;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/util/k/j;->e:Ljava/util/BitSet;

    invoke-virtual {v2}, Ljava/util/BitSet;->size()I

    move-result v2

    const/4 v3, 0x0

    :cond_0
    if-ge v3, v2, :cond_5

    if-eqz v1, :cond_5

    iget-object v4, p0, Lcom/jscape/util/k/j;->e:Ljava/util/BitSet;

    invoke-virtual {v4, v3}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-eqz v1, :cond_1

    if-eqz v4, :cond_3

    iget v4, p0, Lcom/jscape/util/k/j;->c:I

    add-int/2addr v4, v3

    :cond_1
    if-eqz v1, :cond_4

    if-gt p1, v4, :cond_3

    if-eqz v1, :cond_2

    if-gt v4, p2, :cond_3

    move v5, p1

    goto :goto_0

    :cond_2
    move v5, p2

    :goto_0
    sub-int/2addr v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Ljava/util/BitSet;->set(IZ)V

    :cond_3
    add-int/lit8 v3, v3, 0x1

    :cond_4
    if-nez v1, :cond_0

    :cond_5
    iput-object v0, p0, Lcom/jscape/util/k/j;->e:Ljava/util/BitSet;

    return-void
.end method


# virtual methods
.method public declared-synchronized a()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/jscape/util/k/j;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .locals 11

    monitor-enter p0

    int-to-long v7, p1

    const-wide/16 v2, 0x1

    const-wide/32 v4, 0xffff

    :try_start_0
    sget-object v9, Lcom/jscape/util/k/j;->f:[Ljava/lang/String;

    const/4 v10, 0x1

    aget-object v6, v9, v10

    move-wide v0, v7

    invoke-static/range {v0 .. v6}, Lcom/jscape/util/w;->a(JJJLjava/lang/String;)V

    iget v0, p0, Lcom/jscape/util/k/j;->d:I

    int-to-long v0, v0

    aget-object v2, v9, v10

    invoke-static {v0, v1, v7, v8, v2}, Lcom/jscape/util/w;->b(JJLjava/lang/String;)V

    iget v0, p0, Lcom/jscape/util/k/j;->d:I

    invoke-direct {p0, p1, v0}, Lcom/jscape/util/k/j;->b(II)V

    iput p1, p0, Lcom/jscape/util/k/j;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized a(II)V
    .locals 21

    move-object/from16 v1, p0

    move/from16 v0, p1

    move/from16 v2, p2

    monitor-enter p0

    int-to-long v10, v0

    const-wide/16 v5, 0x1

    const-wide/32 v7, 0xffff

    :try_start_0
    sget-object v12, Lcom/jscape/util/k/j;->f:[Ljava/lang/String;

    const/4 v13, 0x3

    aget-object v9, v12, v13

    move-wide v3, v10

    invoke-static/range {v3 .. v9}, Lcom/jscape/util/w;->a(JJJLjava/lang/String;)V

    int-to-long v3, v2

    const-wide/16 v16, 0x1

    const-wide/32 v18, 0xffff

    aget-object v20, v12, v13

    move-wide v14, v3

    invoke-static/range {v14 .. v20}, Lcom/jscape/util/w;->a(JJJLjava/lang/String;)V

    aget-object v5, v12, v13

    invoke-static {v3, v4, v10, v11, v5}, Lcom/jscape/util/w;->b(JJLjava/lang/String;)V

    invoke-direct/range {p0 .. p2}, Lcom/jscape/util/k/j;->b(II)V

    iput v0, v1, Lcom/jscape/util/k/j;->c:I

    iput v2, v1, Lcom/jscape/util/k/j;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/jscape/util/k/j;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(I)V
    .locals 10

    monitor-enter p0

    int-to-long v7, p1

    const-wide/16 v2, 0x1

    const-wide/32 v4, 0xffff

    :try_start_0
    sget-object v9, Lcom/jscape/util/k/j;->f:[Ljava/lang/String;

    const/4 v0, 0x0

    aget-object v6, v9, v0

    move-wide v0, v7

    invoke-static/range {v0 .. v6}, Lcom/jscape/util/w;->a(JJJLjava/lang/String;)V

    iget v0, p0, Lcom/jscape/util/k/j;->c:I

    int-to-long v0, v0

    const/4 v2, 0x1

    aget-object v2, v9, v2

    invoke-static {v7, v8, v0, v1, v2}, Lcom/jscape/util/w;->b(JJLjava/lang/String;)V

    iget v0, p0, Lcom/jscape/util/k/j;->c:I

    invoke-direct {p0, v0, p1}, Lcom/jscape/util/k/j;->b(II)V

    iput p1, p0, Lcom/jscape/util/k/j;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized c()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/k/d;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/util/k/TransportAddress;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/k/j;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget v2, p0, Lcom/jscape/util/k/j;->d:I

    iget v3, p0, Lcom/jscape/util/k/j;->c:I
    :try_end_1
    .catch Lcom/jscape/util/k/d; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sub-int/2addr v2, v3

    const/4 v3, 0x1

    add-int/2addr v2, v3

    if-eqz v0, :cond_1

    if-eq v1, v2, :cond_0

    :try_start_2
    iget-object v0, p0, Lcom/jscape/util/k/j;->e:Ljava/util/BitSet;

    invoke-virtual {v0, v1, v3}, Ljava/util/BitSet;->set(IZ)V

    iget v0, p0, Lcom/jscape/util/k/j;->c:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    :try_start_3
    new-instance v0, Lcom/jscape/util/k/d;

    sget-object v1, Lcom/jscape/util/k/j;->f:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Lcom/jscape/util/k/d;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Lcom/jscape/util/k/d; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    :goto_0
    add-int/2addr v1, v2

    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/util/k/j;->a(Lcom/jscape/util/k/d;)Lcom/jscape/util/k/d;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Lcom/jscape/util/k/d; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_1
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/util/k/j;->a(Lcom/jscape/util/k/d;)Lcom/jscape/util/k/d;

    move-result-object v0

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/jscape/util/k/j;->c:I

    sub-int/2addr p1, v0

    iget-object v0, p0, Lcom/jscape/util/k/j;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/BitSet;->set(IZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/k/j;->f:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/util/k/j;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x7

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/util/k/j;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/k/j;->e:Ljava/util/BitSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
