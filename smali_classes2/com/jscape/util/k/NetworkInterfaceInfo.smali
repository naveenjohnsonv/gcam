.class public Lcom/jscape/util/k/NetworkInterfaceInfo;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private addresses:[Ljava/lang/String;

.field private displayName:Ljava/lang/String;

.field private name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xf

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x41

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "EKz+Gso\u0008\u0012P#Yf>N\u001c\'\u000ej5[qh \u0005j\'Feb\n\u000eW,Rl#\u0012\u0005\u007f/Q>$\u000cEK\u007f&Pqf\u001a\u0018{1\t"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x39

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/k/NetworkInterfaceInfo;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/16 v13, 0x42

    if-eqz v12, :cond_6

    if-eq v12, v7, :cond_5

    const/4 v14, 0x2

    if-eq v12, v14, :cond_4

    if-eq v12, v0, :cond_3

    const/4 v14, 0x4

    if-eq v12, v14, :cond_2

    const/4 v14, 0x5

    goto :goto_2

    :cond_2
    const/16 v13, 0x75

    goto :goto_2

    :cond_3
    move v13, v0

    goto :goto_2

    :cond_4
    const/16 v13, 0x5f

    goto :goto_2

    :cond_5
    const/16 v13, 0x2a

    goto :goto_2

    :cond_6
    const/16 v13, 0x28

    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p3, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p3

    check-cast p3, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/util/k/NetworkInterfaceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/jscape/util/k/TransportAddress;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/jscape/util/k/NetworkInterfaceInfo;->setName(Ljava/lang/String;)V

    if-eqz v0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    move-object p1, p2

    :cond_1
    invoke-virtual {p0, p1}, Lcom/jscape/util/k/NetworkInterfaceInfo;->setDisplayName(Ljava/lang/String;)V

    invoke-virtual {p0, p3}, Lcom/jscape/util/k/NetworkInterfaceInfo;->setAddresses([Ljava/lang/String;)V

    return-void
.end method

.method private static a(Ljava/net/SocketException;)Ljava/net/SocketException;
    .locals 0

    return-object p0
.end method

.method public static getSystemInet4Addresses()[Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/g/a/m;

    const-class v1, Ljava/net/Inet4Address;

    invoke-direct {v0, v1}, Lcom/jscape/util/g/a/m;-><init>(Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/jscape/util/k/NetworkInterfaceInfo;->getSystemInetAddresses(Lcom/jscape/util/g/H;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSystemInet6Addresses()[Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/g/a/m;

    const-class v1, Ljava/net/Inet6Address;

    invoke-direct {v0, v1}, Lcom/jscape/util/g/a/m;-><init>(Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/jscape/util/k/NetworkInterfaceInfo;->getSystemInetAddresses(Lcom/jscape/util/g/H;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSystemInetAddresses()[Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/g/N;

    invoke-direct {v0}, Lcom/jscape/util/g/N;-><init>()V

    invoke-static {v0}, Lcom/jscape/util/k/NetworkInterfaceInfo;->getSystemInetAddresses(Lcom/jscape/util/g/H;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSystemInetAddresses(Lcom/jscape/util/g/H;)[Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/g/H<",
            "Ljava/net/InetAddress;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/b/A;->a()Lcom/jscape/util/b/A;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/k/TransportAddress;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v2

    invoke-static {v2}, Lcom/jscape/util/b/h;->a(Ljava/util/Enumeration;)Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/NetworkInterface;

    invoke-virtual {v3}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v3

    invoke-static {v3, p0}, Lcom/jscape/util/b/j;->a(Ljava/util/Enumeration;Lcom/jscape/util/g/H;)Ljava/util/Iterator;

    move-result-object v3

    new-instance v4, Lcom/jscape/util/k/e;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/jscape/util/k/e;-><init>(Lcom/jscape/util/k/i;)V

    invoke-static {v3, v4}, Lcom/jscape/util/b/r;->a(Ljava/util/Iterator;Lcom/jscape/util/g/z;)Ljava/util/Iterator;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;Lcom/jscape/util/g/O;)Lcom/jscape/util/g/O;

    if-nez v1, :cond_0

    :cond_1
    invoke-virtual {v0}, Lcom/jscape/util/b/A;->c()Ljava/util/Collection;

    move-result-object p0

    const-class v0, Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/jscape/util/b/a;->a(Ljava/util/Collection;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Ljava/lang/String;

    return-object p0
.end method

.method public static getSystemInterfaces()[Lcom/jscape/util/k/NetworkInterfaceInfo;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-static {}, Lcom/jscape/util/k/TransportAddress;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/NetworkInterface;

    :try_start_0
    invoke-virtual {v3}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v4

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v4, :cond_3

    :try_start_1
    invoke-virtual {v3}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v4
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_1
    new-instance v5, Lcom/jscape/util/k/e;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/jscape/util/k/e;-><init>(Lcom/jscape/util/k/i;)V

    invoke-static {v4, v5}, Lcom/jscape/util/b/r;->a(Ljava/util/Enumeration;Lcom/jscape/util/g/z;)Ljava/util/Iterator;

    move-result-object v4

    invoke-static {v4}, Lcom/jscape/util/b/a;->a(Ljava/util/Iterator;)Ljava/util/List;

    move-result-object v4

    :try_start_2
    new-instance v5, Lcom/jscape/util/k/NetworkInterfaceInfo;

    invoke-virtual {v3}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Ljava/net/NetworkInterface;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v3}, Ljava/net/NetworkInterface;->getDisplayName()Ljava/lang/String;

    move-result-object v3
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :cond_2
    invoke-virtual {v3}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-direct {v5, v6, v3, v4}, Lcom/jscape/util/k/NetworkInterfaceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    if-nez v1, :cond_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/k/NetworkInterfaceInfo;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object v0

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/util/k/NetworkInterfaceInfo;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/k/NetworkInterfaceInfo;->a(Ljava/net/SocketException;)Ljava/net/SocketException;

    move-result-object v0

    throw v0

    :cond_4
    :goto_1
    const/4 v1, 0x0

    new-array v1, v1, [Lcom/jscape/util/k/NetworkInterfaceInfo;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/util/k/NetworkInterfaceInfo;

    return-object v0
.end method


# virtual methods
.method public getAddresses()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/NetworkInterfaceInfo;->addresses:[Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/NetworkInterfaceInfo;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/k/NetworkInterfaceInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public setAddresses([Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/NetworkInterfaceInfo;->addresses:[Ljava/lang/String;

    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/NetworkInterfaceInfo;->displayName:Ljava/lang/String;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/k/NetworkInterfaceInfo;->name:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/k/NetworkInterfaceInfo;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/k/NetworkInterfaceInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/jscape/util/k/NetworkInterfaceInfo;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/k/NetworkInterfaceInfo;->addresses:[Ljava/lang/String;

    invoke-static {v1}, Lcom/jscape/util/v;->d([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
