.class public Lcom/jscape/util/x;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# static fields
.field private static final i:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/jscape/util/Time;

.field private final c:I

.field private d:Z

.field private e:I

.field private f:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private volatile g:Z

.field private h:Ljava/lang/Exception;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/16 v0, 0xa

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "\u0014;?pf\u0005gKhq\u000c\u0014;/dk\u0005gTw)a8\u0012\u0014;-qq\u0003oHo\u0005kq\u0003pNz 8\t\u0014;>`v\u0013nL&\u0008\u0014;)ww\tp\u0005\u0017zz(%h\u0007z\u0018z8q`\u000brLhlsd\nw]5\u0012yo8`h\u0016vYy `%\u001d`Yh)8\u001bzz(%d\u0012v]v<q%\u000flL~>sd\n\"Nz p`H"

    const/16 v5, 0x84

    move v7, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x5a

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v0

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v3

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v1, v7

    add-int/2addr v6, v0

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v5, 0x19

    const/16 v0, 0xe

    const-string v4, "[tn+2h9\u00031n:>Zp\n[tb>>L \u0007 >"

    move v7, v10

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v8, v1, v7

    add-int/2addr v6, v0

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    move v7, v10

    :goto_3
    const/16 v8, 0x15

    add-int/2addr v6, v9

    add-int v10, v6, v0

    invoke-virtual {v4, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v3

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/x;->i:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    const/16 v16, 0x5f

    if-eqz v15, :cond_7

    if-eq v15, v9, :cond_6

    const/4 v2, 0x2

    if-eq v15, v2, :cond_5

    const/4 v2, 0x3

    if-eq v15, v2, :cond_8

    const/4 v2, 0x4

    if-eq v15, v2, :cond_8

    const/4 v2, 0x5

    if-eq v15, v2, :cond_4

    const/16 v16, 0x58

    goto :goto_4

    :cond_4
    const/16 v16, 0x3c

    goto :goto_4

    :cond_5
    const/16 v16, 0x16

    goto :goto_4

    :cond_6
    const/16 v16, 0x41

    goto :goto_4

    :cond_7
    const/16 v16, 0x62

    :cond_8
    :goto_4
    xor-int v2, v8, v16

    xor-int/2addr v2, v14

    int-to-char v2, v2

    aput-char v2, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/util/concurrent/Callable;JI)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable<",
            "TT;>;JI)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/x;->a:Ljava/util/concurrent/Callable;

    sget-object p1, Lcom/jscape/util/x;->i:[Ljava/lang/String;

    const/4 v0, 0x7

    aget-object v0, p1, v0

    const-wide/16 v1, 0x0

    invoke-static {p2, p3, v1, v2, v0}, Lcom/jscape/util/w;->b(JJLjava/lang/String;)V

    invoke-static {p2, p3}, Lcom/jscape/util/Time;->millis(J)Lcom/jscape/util/Time;

    move-result-object p2

    iput-object p2, p0, Lcom/jscape/util/x;->b:Lcom/jscape/util/Time;

    int-to-long p2, p4

    const/4 v0, 0x5

    aget-object p1, p1, v0

    invoke-static {p2, p3, v1, v2, p1}, Lcom/jscape/util/w;->a(JJLjava/lang/String;)V

    iput p4, p0, Lcom/jscape/util/x;->c:I

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static a(Ljava/util/concurrent/Callable;JI)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable<",
            "TT;>;JI)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/x;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/jscape/util/x;-><init>(Ljava/util/concurrent/Callable;JI)V

    invoke-virtual {v0}, Lcom/jscape/util/x;->call()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private b()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jscape/util/x;->d:Z

    iput v0, p0, Lcom/jscape/util/x;->e:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jscape/util/x;->f:Ljava/lang/Object;

    iput-object v0, p0, Lcom/jscape/util/x;->h:Ljava/lang/Exception;

    return-void
.end method

.method private c()Z
    .locals 3

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/util/x;->d:Z
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v0, :cond_0

    if-nez v1, :cond_2

    :try_start_1
    iget v1, p0, Lcom/jscape/util/x;->e:I
    :try_end_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_0
    if-nez v0, :cond_1

    :try_start_2
    iget v2, p0, Lcom/jscape/util/x;->c:I
    :try_end_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_0

    if-ge v1, v2, :cond_2

    :try_start_3
    iget-boolean v1, p0, Lcom/jscape/util/x;->g:Z

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/x;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/util/concurrent/CancellationException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/x;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    if-nez v0, :cond_3

    if-nez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    :goto_1
    move v0, v1

    :goto_2
    return v0

    :catch_2
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/util/x;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/util/concurrent/CancellationException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/x;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private d()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/x;->a:Ljava/util/concurrent/Callable;

    invoke-interface {v1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lcom/jscape/util/x;->f:Ljava/lang/Object;

    iput-boolean v0, p0, Lcom/jscape/util/x;->d:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    iput-object v1, p0, Lcom/jscape/util/x;->h:Ljava/lang/Exception;

    iget v1, p0, Lcom/jscape/util/x;->e:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/jscape/util/x;->e:I

    :goto_0
    return-void
.end method

.method private e()V
    .locals 2

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/util/x;->d:Z
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/util/x;->g:Z
    :try_end_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_0
    if-nez v1, :cond_2

    :cond_1
    return-void

    :cond_2
    :try_start_2
    new-instance v0, Ljava/util/concurrent/CancellationException;

    invoke-direct {v0}, Ljava/util/concurrent/CancellationException;-><init>()V

    throw v0
    :try_end_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/x;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/util/x;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/util/concurrent/CancellationException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/x;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private f()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/util/x;->d:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/util/x;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/x;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/jscape/util/x;->h:Ljava/lang/Exception;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz v0, :cond_3

    if-nez v1, :cond_2

    :cond_1
    return-void

    :cond_2
    :try_start_3
    iget-object v1, p0, Lcom/jscape/util/x;->h:Ljava/lang/Exception;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :cond_3
    throw v1

    :catch_2
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/util/x;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/x;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/util/x;->g:Z

    return-void
.end method

.method public call()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    invoke-direct {p0}, Lcom/jscape/util/x;->b()V

    :cond_0
    invoke-direct {p0}, Lcom/jscape/util/x;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    if-nez v0, :cond_2

    if-nez v0, :cond_1

    :try_start_0
    iget v1, p0, Lcom/jscape/util/x;->e:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-lez v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/jscape/util/x;->b:Lcom/jscape/util/Time;

    invoke-virtual {v1}, Lcom/jscape/util/Time;->sleepSafe()V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/x;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/x;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/jscape/util/x;->d()V

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_2
    move-object v0, p0

    goto :goto_2

    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/jscape/util/x;->e()V

    invoke-direct {p0}, Lcom/jscape/util/x;->f()V

    iget-object v0, p0, Lcom/jscape/util/x;->f:Ljava/lang/Object;

    :goto_2
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/x;->i:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/x;->a:Ljava/util/concurrent/Callable;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/x;->b:Lcom/jscape/util/Time;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x8

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/util/x;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/util/x;->d:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v2, 0x9

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/util/x;->e:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/x;->f:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/util/x;->g:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/x;->h:Ljava/lang/Exception;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
