.class public Lcom/jscape/util/v;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x2

    const/4 v3, 0x0

    const-string v4, "(\u0007\u0002(\u0007\u0002_z\u0002_z"

    const/16 v5, 0xb

    move v7, v1

    move v8, v3

    const/4 v6, -0x1

    :goto_0
    const/16 v9, 0x2f

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x4

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x9

    const-string v4, "(\u001a7.\u0004(\u001a7."

    move v8, v11

    move v7, v15

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v0, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x1a

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/util/v;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v2, v14, 0x7

    const/16 v17, 0x41

    if-eqz v2, :cond_7

    if-eq v2, v10, :cond_6

    if-eq v2, v1, :cond_8

    const/4 v1, 0x3

    if-eq v2, v1, :cond_5

    if-eq v2, v15, :cond_8

    const/4 v1, 0x5

    if-eq v2, v1, :cond_4

    move/from16 v17, v10

    goto :goto_4

    :cond_4
    const/16 v17, 0x13

    goto :goto_4

    :cond_5
    const/16 v17, 0x58

    goto :goto_4

    :cond_6
    const/16 v17, 0x75

    goto :goto_4

    :cond_7
    const/16 v17, 0x5c

    :cond_8
    :goto_4
    xor-int v1, v9, v17

    xor-int v1, v16, v1

    int-to-char v1, v1

    aput-char v1, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v1, 0x2

    goto :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([BBI)I
    .locals 1

    array-length v0, p0

    invoke-static {p0, p1, p2, v0}, Lcom/jscape/util/v;->a([BBII)I

    move-result p0

    return p0
.end method

.method public static a([BBII)I
    .locals 2

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    :cond_0
    if-ge p2, p3, :cond_3

    if-eqz v0, :cond_4

    if-eqz v0, :cond_2

    aget-byte v1, p0, p2

    if-ne p1, v1, :cond_1

    move p1, p2

    goto :goto_0

    :cond_1
    add-int/lit8 p2, p2, 0x1

    if-nez v0, :cond_0

    goto :goto_1

    :cond_2
    :goto_0
    return p1

    :cond_3
    :goto_1
    const/4 p1, -0x1

    :cond_4
    return p1
.end method

.method public static a([B[BII)I
    .locals 5

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    :cond_0
    if-ge p2, p3, :cond_9

    aget-byte v1, p0, p2

    if-nez v0, :cond_a

    const/4 v2, 0x0

    if-nez v0, :cond_2

    aget-byte v3, p1, v2

    if-eq v1, v3, :cond_1

    goto :goto_2

    :cond_1
    move v1, v2

    :cond_2
    array-length v2, p1

    if-ge v1, v2, :cond_5

    if-nez v0, :cond_4

    if-nez v0, :cond_4

    if-ge p2, p3, :cond_5

    aget-byte v2, p0, p2

    aget-byte v3, p1, v1

    if-eq v2, v3, :cond_3

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 p2, p2, 0x1

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_4
    move v3, p3

    move v2, v1

    move v1, p2

    goto :goto_1

    :cond_5
    :goto_0
    move v4, v1

    move v1, p2

    move p2, v4

    array-length v2, p1

    move v3, v2

    move v2, p2

    :goto_1
    if-nez v0, :cond_6

    if-ne p2, v3, :cond_7

    move v3, p3

    move p2, v1

    :cond_6
    if-nez v0, :cond_8

    if-gt p2, v3, :cond_7

    goto :goto_3

    :cond_7
    move p2, v1

    :goto_2
    add-int/lit8 p2, p2, 0x1

    if-eqz v0, :cond_0

    goto :goto_4

    :cond_8
    move v1, p2

    move v2, v3

    :goto_3
    sub-int/2addr v1, v2

    return v1

    :cond_9
    :goto_4
    const/4 v1, -0x1

    :cond_a
    return v1
.end method

.method public static a([B)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([B)Z"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p0, :cond_1

    :cond_0
    array-length p0, p0

    if-nez v0, :cond_2

    if-lez p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :cond_2
    :goto_0
    return p0
.end method

.method public static a([B[BI)Z
    .locals 6

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_6

    if-ltz p2, :cond_5

    if-nez v0, :cond_1

    array-length v2, p0

    array-length v3, p1

    sub-int/2addr v2, v3

    if-le p2, v2, :cond_0

    goto :goto_3

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v2, p2

    :goto_0
    array-length v3, p1

    :goto_1
    add-int/lit8 v3, v3, -0x1

    if-ltz v3, :cond_4

    add-int/lit8 v4, p2, 0x1

    aget-byte p2, p0, p2

    :goto_2
    add-int/lit8 v5, v2, 0x1

    aget-byte v2, p1, v2

    if-eq p2, v2, :cond_3

    if-nez v0, :cond_2

    return v1

    :cond_2
    move p2, v1

    move v2, v5

    goto :goto_2

    :cond_3
    move p2, v4

    move v2, v5

    goto :goto_1

    :cond_4
    const/4 p0, 0x1

    return p0

    :cond_5
    :goto_3
    move p2, v1

    :cond_6
    return p2
.end method

.method public static a([C)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([C)Z"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p0, :cond_1

    :cond_0
    array-length p0, p0

    if-nez v0, :cond_2

    if-lez p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :cond_2
    :goto_0
    return p0
.end method

.method public static a([I)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([I)Z"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p0, :cond_1

    :cond_0
    array-length p0, p0

    if-nez v0, :cond_2

    if-lez p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :cond_2
    :goto_0
    return p0
.end method

.method public static a([II)Z
    .locals 4

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :cond_0
    array-length v3, p0

    if-ge v2, v3, :cond_4

    aget v3, p0, v2

    if-nez v0, :cond_3

    if-nez v0, :cond_2

    if-ne v3, p1, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_2
    :goto_0
    return v3

    :cond_3
    move v1, v3

    :cond_4
    :goto_1
    return v1
.end method

.method public static a([J)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([J)Z"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p0, :cond_1

    :cond_0
    array-length p0, p0

    if-eqz v0, :cond_2

    if-lez p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :cond_2
    :goto_0
    return p0
.end method

.method public static a([JJ)Z
    .locals 5

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :cond_0
    array-length v3, p0

    if-ge v2, v3, :cond_4

    aget-wide v3, p0, v2

    cmp-long v3, v3, p1

    if-nez v0, :cond_3

    if-nez v0, :cond_2

    if-nez v3, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_2
    :goto_0
    return v3

    :cond_3
    move v1, v3

    :cond_4
    :goto_1
    return v1
.end method

.method public static a([Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)Z"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p0, :cond_1

    :cond_0
    array-length p0, p0

    if-eqz v0, :cond_2

    if-lez p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :cond_2
    :goto_0
    return p0
.end method

.method public static a([Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;TT;)Z"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :cond_0
    array-length v3, p0

    if-ge v2, v3, :cond_4

    aget-object v3, p0, v2

    if-eqz v0, :cond_3

    if-eqz v3, :cond_2

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v0, :cond_1

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :cond_1
    return v3

    :cond_2
    add-int/lit8 v2, v2, 0x1

    :cond_3
    if-nez v0, :cond_0

    :cond_4
    return v1
.end method

.method public static a([S)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([S)Z"
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p0, :cond_1

    :cond_0
    array-length p0, p0

    if-nez v0, :cond_2

    if-lez p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :cond_2
    :goto_0
    return p0
.end method

.method public static a([SS)Z
    .locals 4

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :cond_0
    array-length v3, p0

    if-ge v2, v3, :cond_4

    aget-short v3, p0, v2

    if-nez v0, :cond_3

    if-nez v0, :cond_2

    if-ne v3, p1, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_2
    :goto_0
    return v3

    :cond_3
    move v1, v3

    :cond_4
    :goto_1
    return v1
.end method

.method public static a([BB)[B
    .locals 2

    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/jscape/util/v;->a([BIIB)[B

    move-result-object p0

    return-object p0
.end method

.method public static a([BI)[B
    .locals 1

    array-length v0, p0

    invoke-static {p0, p1, v0}, Lcom/jscape/util/v;->b([BII)[B

    move-result-object p0

    return-object p0
.end method

.method public static a([BII)[B
    .locals 2

    sub-int/2addr p2, p1

    new-array v0, p2, [B

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1, p2}, Lcom/jscape/util/v;->a([BI[BII)[B

    move-result-object p0

    return-object p0
.end method

.method public static a([BIIB)[B
    .locals 1

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    add-int/2addr p2, p1

    :cond_0
    if-ge p1, p2, :cond_1

    if-eqz v0, :cond_1

    aput-byte p3, p0, p1

    add-int/lit8 p1, p1, 0x1

    if-nez v0, :cond_0

    :cond_1
    return-object p0
.end method

.method public static a([BI[B)[B
    .locals 2

    array-length v0, p2

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v1, v0}, Lcom/jscape/util/v;->a([BI[BII)[B

    move-result-object p0

    return-object p0
.end method

.method public static a([BI[BII)[B
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p2
.end method

.method public static a([B[B)[B
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/jscape/util/v;->a([BI[B)[B

    move-result-object p0

    return-object p0
.end method

.method public static varargs a([[B)[B
    .locals 8

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    array-length v1, p0

    const/4 v2, 0x0

    move v3, v2

    move v4, v3

    :cond_0
    if-ge v3, v1, :cond_1

    aget-object v5, p0, v3

    array-length v5, v5

    add-int/2addr v4, v5

    if-eqz v0, :cond_1

    add-int/lit8 v3, v3, 0x1

    if-nez v0, :cond_0

    :cond_1
    new-array v1, v4, [B

    array-length v3, p0

    move v4, v2

    move v5, v4

    :cond_2
    if-ge v4, v3, :cond_4

    aget-object v6, p0, v4

    if-eqz v0, :cond_3

    array-length v7, v6

    invoke-static {v6, v2, v1, v5, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v6, v6

    add-int/2addr v5, v6

    add-int/lit8 v4, v4, 0x1

    if-nez v0, :cond_2

    goto :goto_0

    :cond_3
    move-object v1, v6

    :cond_4
    :goto_0
    return-object v1
.end method

.method public static a([Ljava/lang/Object;I[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">([TT;I[TV;)[TV;"
        }
    .end annotation

    array-length v0, p2

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v1, v0}, Lcom/jscape/util/v;->a([Ljava/lang/Object;I[Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static a([Ljava/lang/Object;I[Ljava/lang/Object;II)[Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">([TT;I[TV;II)[TV;"
        }
    .end annotation

    invoke-static {p0, p1, p2, p3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p2
.end method

.method public static a([Ljava/lang/Object;Ljava/util/Comparator;)[Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;",
            "Ljava/util/Comparator<",
            "TT;>;)[TT;"
        }
    .end annotation

    invoke-static {p0, p1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    return-object p0
.end method

.method public static a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">([TT;[TV;)[TV;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/jscape/util/v;->a([Ljava/lang/Object;I[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static b([BBI)I
    .locals 3

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    array-length v1, p0

    add-int/lit8 v1, v1, -0x1

    :cond_0
    if-lt v1, p2, :cond_3

    if-eqz v0, :cond_4

    if-eqz v0, :cond_2

    aget-byte v2, p0, v1

    if-ne p1, v2, :cond_1

    move p1, v1

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v1, -0x1

    if-nez v0, :cond_0

    goto :goto_1

    :cond_2
    :goto_0
    return p1

    :cond_3
    :goto_1
    const/4 p1, -0x1

    :cond_4
    return p1
.end method

.method public static b([B)Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    array-length v0, p0

    :goto_1
    invoke-static {p0, v0}, Lcom/jscape/util/v;->b([BI)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static b([BI)Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    if-nez p0, :cond_0

    sget-object p0, Lcom/jscape/util/v;->a:[Ljava/lang/String;

    const/4 p1, 0x5

    aget-object p0, p0, p1

    return-object p0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    sget-object p0, Lcom/jscape/util/v;->a:[Ljava/lang/String;

    aget-object p0, p0, v1

    return-object p0

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x5b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    array-length v3, p0

    invoke-static {v3, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    :cond_2
    if-ge v1, p1, :cond_6

    if-eqz v0, :cond_4

    if-lez v1, :cond_3

    sget-object v3, Lcom/jscape/util/v;->a:[Ljava/lang/String;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    aget-byte v3, p0, v1

    goto :goto_0

    :cond_4
    move v3, v1

    :goto_0
    const/16 v4, 0x10

    if-ge v3, v4, :cond_5

    const/16 v3, 0x30

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_5
    aget-byte v3, p0, v1

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_2

    :cond_6
    const/16 p0, 0x5d

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static b([I)Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p0, :cond_0

    sget-object p0, Lcom/jscape/util/v;->a:[Ljava/lang/String;

    const/4 v0, 0x5

    aget-object p0, p0, v0

    return-object p0

    :cond_0
    array-length v1, p0

    const/4 v2, 0x0

    if-nez v1, :cond_1

    sget-object p0, Lcom/jscape/util/v;->a:[Ljava/lang/String;

    aget-object p0, p0, v2

    return-object p0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x5b

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    array-length v3, p0

    if-ge v2, v3, :cond_6

    if-eqz v0, :cond_4

    if-lez v2, :cond_3

    sget-object v3, Lcom/jscape/util/v;->a:[Ljava/lang/String;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    aget v3, p0, v2

    goto :goto_0

    :cond_4
    move v3, v2

    :goto_0
    const/16 v4, 0x10

    if-ge v3, v4, :cond_5

    const/16 v3, 0x30

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_5
    aget v3, p0, v2

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_2

    :cond_6
    const/16 p0, 0x5d

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static b([J)Ljava/lang/String;
    .locals 7

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    sget-object p0, Lcom/jscape/util/v;->a:[Ljava/lang/String;

    const/4 v0, 0x5

    aget-object p0, p0, v0

    return-object p0

    :cond_0
    array-length v1, p0

    const/4 v2, 0x0

    if-nez v1, :cond_1

    sget-object p0, Lcom/jscape/util/v;->a:[Ljava/lang/String;

    aget-object p0, p0, v2

    return-object p0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x5b

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    array-length v3, p0

    if-ge v2, v3, :cond_6

    if-nez v0, :cond_4

    if-lez v2, :cond_3

    sget-object v3, Lcom/jscape/util/v;->a:[Ljava/lang/String;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    aget-wide v3, p0, v2

    const-wide/16 v5, 0x10

    cmp-long v3, v3, v5

    goto :goto_0

    :cond_4
    move v3, v2

    :goto_0
    if-gez v3, :cond_5

    const/16 v3, 0x30

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_5
    aget-wide v3, p0, v2

    invoke-static {v3, v4}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    if-eqz v0, :cond_2

    :cond_6
    const/16 p0, 0x5d

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static b([BB)Z
    .locals 4

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :cond_0
    array-length v3, p0

    if-ge v2, v3, :cond_4

    aget-byte v3, p0, v2

    if-nez v0, :cond_3

    if-nez v0, :cond_2

    if-ne v3, p1, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_2
    :goto_0
    return v3

    :cond_3
    move v1, v3

    :cond_4
    :goto_1
    return v1
.end method

.method public static b([BI[BII)Z
    .locals 5

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_9

    array-length v2, p0

    if-ge p1, v2, :cond_8

    array-length v2, p0

    sub-int/2addr v2, p1

    if-nez v0, :cond_7

    if-lt v2, p4, :cond_8

    if-nez v0, :cond_6

    array-length v2, p2

    if-ge p3, v2, :cond_8

    array-length v2, p2

    sub-int/2addr v2, p3

    if-nez v0, :cond_1

    if-ge v2, p4, :cond_0

    goto :goto_2

    :cond_0
    move v2, v1

    :cond_1
    if-ge v2, p4, :cond_4

    add-int v3, v2, p1

    aget-byte v3, p0, v3

    if-nez v0, :cond_5

    if-nez v0, :cond_3

    add-int v4, v2, p3

    aget-byte v4, p2, v4

    if-eq v3, v4, :cond_2

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_3
    move v1, v3

    :goto_0
    return v1

    :cond_4
    :goto_1
    const/4 v3, 0x1

    :cond_5
    return v3

    :cond_6
    move p1, p3

    goto :goto_3

    :cond_7
    move p1, v2

    goto :goto_3

    :cond_8
    :goto_2
    move p1, v1

    :cond_9
    :goto_3
    return p1
.end method

.method public static b([B[B)Z
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/jscape/util/v;->a([B[BI)Z

    move-result p0

    return p0
.end method

.method public static b([B[BI)Z
    .locals 1

    array-length v0, p1

    sub-int/2addr p2, v0

    invoke-static {p0, p1, p2}, Lcom/jscape/util/v;->a([B[BI)Z

    move-result p0

    return p0
.end method

.method public static b([BII)[B
    .locals 2

    sub-int/2addr p2, p1

    new-array v0, p2, [B

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public static varargs b([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    invoke-virtual {p0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v1

    const/4 v2, 0x0

    :cond_0
    array-length v3, v0

    if-ge v2, v3, :cond_1

    if-eqz v1, :cond_1

    array-length v3, p0

    sub-int/2addr v3, v2

    add-int/lit8 v3, v3, -0x1

    aget-object v3, p0, v3

    aput-object v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    if-nez v1, :cond_0

    :cond_1
    return-object v0
.end method

.method public static c([B[BI)I
    .locals 1

    array-length v0, p0

    invoke-static {p0, p1, p2, v0}, Lcom/jscape/util/v;->a([B[BII)I

    move-result p0

    return p0
.end method

.method public static c([B[B)Z
    .locals 1

    array-length v0, p0

    invoke-static {p0, p1, v0}, Lcom/jscape/util/v;->b([B[BI)Z

    move-result p0

    return p0
.end method

.method public static c([BB)[B
    .locals 3

    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [B

    array-length v1, p0

    const/4 v2, 0x0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length p0, p0

    aput-byte p1, v0, p0

    return-object v0
.end method

.method public static c([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    invoke-static {p0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    return-object p0
.end method

.method public static d([BB)I
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/jscape/util/v;->a([BBI)I

    move-result p0

    return p0
.end method

.method public static d([B[B)I
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/jscape/util/v;->c([B[BI)I

    move-result p0

    return p0
.end method

.method public static d([Ljava/lang/Object;)Ljava/lang/String;
    .locals 5

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p0, :cond_0

    sget-object p0, Lcom/jscape/util/v;->a:[Ljava/lang/String;

    const/4 v0, 0x4

    aget-object p0, p0, v0

    return-object p0

    :cond_0
    array-length v1, p0

    if-nez v1, :cond_1

    sget-object p0, Lcom/jscape/util/v;->a:[Ljava/lang/String;

    const/4 v0, 0x1

    aget-object p0, p0, v0

    return-object p0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x5b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    :cond_2
    array-length v3, p0

    if-ge v2, v3, :cond_4

    if-lez v2, :cond_3

    sget-object v3, Lcom/jscape/util/v;->a:[Ljava/lang/String;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    aget-object v3, p0, v2

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_2

    :cond_4
    const/16 p0, 0x5d

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static d([B[BI)Z
    .locals 5

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    array-length v1, p0

    const/4 v2, 0x0

    if-eqz v0, :cond_7

    if-lt v1, p2, :cond_6

    array-length v1, p1

    if-eqz v0, :cond_1

    if-ge v1, p2, :cond_0

    goto :goto_2

    :cond_0
    move v1, v2

    :cond_1
    if-ge v1, p2, :cond_4

    aget-byte v3, p0, v1

    if-eqz v0, :cond_5

    if-eqz v0, :cond_3

    aget-byte v4, p1, v1

    if-eq v3, v4, :cond_2

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_1

    goto :goto_1

    :cond_3
    move v2, v3

    :goto_0
    return v2

    :cond_4
    :goto_1
    const/4 v3, 0x1

    :cond_5
    return v3

    :cond_6
    :goto_2
    move v1, v2

    :cond_7
    return v1
.end method

.method public static e([BB)I
    .locals 1

    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p0, p1, v0}, Lcom/jscape/util/v;->b([BBI)I

    move-result p0

    return p0
.end method

.method public static e([B[B)[[B
    .locals 5

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v1

    const/4 v2, 0x0

    :cond_0
    array-length v3, p0

    if-ge v2, v3, :cond_3

    invoke-static {p0, p1, v2}, Lcom/jscape/util/v;->c([B[BI)I

    move-result v3

    const/4 v4, -0x1

    if-eqz v1, :cond_4

    if-eqz v1, :cond_2

    if-ne v3, v4, :cond_1

    goto :goto_0

    :cond_1
    invoke-static {p0, v2, v3}, Lcom/jscape/util/v;->b([BII)[B

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    array-length v4, p1

    :cond_2
    add-int v2, v3, v4

    if-nez v1, :cond_0

    :cond_3
    :goto_0
    if-eqz v1, :cond_5

    array-length v4, p0

    move v3, v2

    :cond_4
    if-ge v3, v4, :cond_5

    invoke-static {p0, v2}, Lcom/jscape/util/v;->a([BI)[B

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p0

    new-array p0, p0, [[B

    invoke-interface {v0, p0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [[B

    return-object p0
.end method
