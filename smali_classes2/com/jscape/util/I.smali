.class public Lcom/jscape/util/I;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final e:[Ljava/lang/String;


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "\u000e\u007f\u0012$g$\u0006`0\u0012:s|\u0014n0\u00101ea\u0016M*\t073\u0011C<\u000f1so\u0014a0\u0012:c$\u0006\u0002$\u000b;`$\u0006`0\u0012:s|\u0008\u000e\u007f\u00115{4\u0011\u001f\n\u000e\u007f\u0010&v1\u0004G;Z"

    const/16 v4, 0x4b

    const/16 v5, 0xd

    const/4 v6, -0x1

    const/4 v7, 0x0

    :goto_0
    const/16 v8, 0x19

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0x46

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v13, v10

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v12, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x1f

    const/16 v3, 0xa

    const-string v5, "\u001fa\\+*q^\u0013dK\u0014(pHn:>I\u0012uVohlN\u001ccPn,0"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v16, v5

    move v5, v3

    move-object/from16 v3, v16

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    add-int/2addr v6, v9

    add-int v8, v6, v5

    invoke-virtual {v3, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v8, v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/util/I;->e:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v10, v14

    rem-int/lit8 v1, v14, 0x7

    if-eqz v1, :cond_9

    if-eq v1, v9, :cond_8

    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    if-eq v1, v2, :cond_5

    const/4 v2, 0x5

    if-eq v1, v2, :cond_4

    const/16 v1, 0x6d

    goto :goto_4

    :cond_4
    const/16 v1, 0x58

    goto :goto_4

    :cond_5
    const/16 v1, 0xe

    goto :goto_4

    :cond_6
    const/16 v1, 0x4d

    goto :goto_4

    :cond_7
    const/16 v1, 0x7e

    goto :goto_4

    :cond_8
    move v1, v11

    goto :goto_4

    :cond_9
    const/16 v1, 0x3b

    :goto_4
    xor-int/2addr v1, v8

    xor-int/2addr v1, v15

    int-to-char v1, v1

    aput-char v1, v10, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(JJZ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/jscape/util/I;->e:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    invoke-static {p3, p4, p1, p2, v0}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    iput-wide p1, p0, Lcom/jscape/util/I;->a:J

    iput-wide p3, p0, Lcom/jscape/util/I;->b:J

    invoke-virtual {p0}, Lcom/jscape/util/I;->i()V

    iput-boolean p5, p0, Lcom/jscape/util/I;->d:Z

    return-void
.end method

.method public constructor <init>(Lcom/jscape/util/I;)V
    .locals 6

    iget-wide v1, p1, Lcom/jscape/util/I;->a:J

    iget-wide v3, p1, Lcom/jscape/util/I;->b:J

    iget-boolean v5, p1, Lcom/jscape/util/I;->d:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/jscape/util/I;-><init>(JJZ)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/util/I;->a:J

    return-wide v0
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/util/I;->b:J

    return-wide v0
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jscape/util/I;->d:Z

    return v0
.end method

.method public declared-synchronized d()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/jscape/util/I;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()Z
    .locals 5

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    iget-boolean v1, p0, Lcom/jscape/util/I;->d:Z

    if-eqz v0, :cond_2

    if-nez v1, :cond_1

    iget-wide v1, p0, Lcom/jscape/util/I;->c:J

    iget-wide v3, p0, Lcom/jscape/util/I;->b:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v1, v1, v3

    if-eqz v0, :cond_2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move v0, v1

    :goto_1
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    if-ne p0, p1, :cond_0

    return v1

    :cond_0
    move-object v2, p1

    goto :goto_0

    :cond_1
    move-object v2, p0

    :goto_0
    const/4 v3, 0x0

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v2, v4, :cond_3

    goto :goto_2

    :cond_2
    move-object p1, v2

    :cond_3
    check-cast p1, Lcom/jscape/util/I;

    iget-wide v4, p0, Lcom/jscape/util/I;->a:J

    iget-wide v6, p1, Lcom/jscape/util/I;->a:J

    cmp-long v2, v4, v6

    if-eqz v0, :cond_5

    if-eqz v2, :cond_4

    return v3

    :cond_4
    iget-wide v4, p0, Lcom/jscape/util/I;->b:J

    iget-wide v6, p1, Lcom/jscape/util/I;->b:J

    cmp-long v2, v4, v6

    :cond_5
    if-eqz v0, :cond_6

    if-nez v2, :cond_7

    goto :goto_1

    :cond_6
    move v1, v2

    :goto_1
    move v3, v1

    :cond_7
    :goto_2
    return v3
.end method

.method public declared-synchronized f()Z
    .locals 5

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    iget-boolean v1, p0, Lcom/jscape/util/I;->d:Z

    if-nez v0, :cond_2

    if-nez v1, :cond_1

    iget-wide v1, p0, Lcom/jscape/util/I;->c:J

    iget-wide v3, p0, Lcom/jscape/util/I;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v1, v1, v3

    if-nez v0, :cond_2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move v0, v1

    :goto_1
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g()V
    .locals 7

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/util/I;->e()Z

    move-result v0

    sget-object v1, Lcom/jscape/util/I;->e:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/jscape/util/aq;->c(ZLjava/lang/String;)V

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    iget-wide v1, p0, Lcom/jscape/util/I;->c:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/jscape/util/I;->c:J

    iget-boolean v5, p0, Lcom/jscape/util/I;->d:Z

    if-eqz v0, :cond_0

    if-eqz v5, :cond_2

    if-eqz v0, :cond_1

    iget-wide v5, p0, Lcom/jscape/util/I;->b:J

    add-long/2addr v5, v3

    cmp-long v5, v1, v5

    :cond_0
    if-nez v5, :cond_2

    :cond_1
    iget-wide v0, p0, Lcom/jscape/util/I;->a:J

    iput-wide v0, p0, Lcom/jscape/util/I;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized h()V
    .locals 7

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/jscape/util/I;->f()Z

    move-result v0

    sget-object v1, Lcom/jscape/util/I;->e:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/jscape/util/aq;->c(ZLjava/lang/String;)V

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    iget-wide v1, p0, Lcom/jscape/util/I;->c:J

    const-wide/16 v3, 0x1

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/jscape/util/I;->c:J

    iget-boolean v5, p0, Lcom/jscape/util/I;->d:Z

    if-nez v0, :cond_0

    if-eqz v5, :cond_2

    if-nez v0, :cond_1

    iget-wide v5, p0, Lcom/jscape/util/I;->a:J

    sub-long/2addr v5, v3

    cmp-long v5, v1, v5

    :cond_0
    if-nez v5, :cond_2

    :cond_1
    iget-wide v0, p0, Lcom/jscape/util/I;->b:J

    iput-wide v0, p0, Lcom/jscape/util/I;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public hashCode()I
    .locals 5

    iget-wide v0, p0, Lcom/jscape/util/I;->a:J

    const/16 v2, 0x20

    ushr-long v3, v0, v2

    xor-long/2addr v0, v3

    long-to-int v0, v0

    mul-int/lit8 v0, v0, 0x1d

    iget-wide v3, p0, Lcom/jscape/util/I;->b:J

    ushr-long v1, v3, v2

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public declared-synchronized i()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/jscape/util/I;->a:J

    iput-wide v0, p0, Lcom/jscape/util/I;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/I;->e:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/util/I;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/util/I;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/jscape/util/I;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/jscape/util/I;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
