.class public Lcom/jscape/util/i/f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Ljava/security/KeyPair;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final a:Ljava/security/Provider;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "w\u001f\u0012h-CA\u001d{qePSM\u000csk\u0000 JA\u0003\u001f\u0012h-,\tP\u0017LH\n,)w\u001f\u0012\u0000NE$\n`v\u0013AUAzyz\u001c-,)w\u001f2O"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/i/f;->b:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    const/16 v5, 0x43

    if-eqz v4, :cond_6

    const/4 v6, 0x1

    if-eq v4, v6, :cond_5

    const/4 v6, 0x2

    if-eq v4, v6, :cond_4

    const/4 v6, 0x3

    if-eq v4, v6, :cond_3

    const/4 v6, 0x4

    if-eq v4, v6, :cond_2

    const/4 v6, 0x5

    if-eq v4, v6, :cond_1

    const/16 v4, 0x47

    goto :goto_1

    :cond_1
    const/16 v4, 0x42

    goto :goto_1

    :cond_2
    move v4, v5

    goto :goto_1

    :cond_3
    const/4 v4, 0x6

    goto :goto_1

    :cond_4
    const/16 v4, 0x7c

    goto :goto_1

    :cond_5
    const/16 v4, 0x71

    goto :goto_1

    :cond_6
    const/16 v4, 0x19

    :goto_1
    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/security/Provider;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/i/f;->a:Ljava/security/Provider;

    return-void
.end method

.method public static a()Lcom/jscape/util/i/f;
    .locals 2

    new-instance v0, Lcom/jscape/util/i/f;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/jscape/util/i/f;-><init>(Ljava/security/Provider;)V

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;Ljava/security/Provider;)Ljava/security/KeyPair;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    new-instance v0, Lorg/bouncycastle/util/io/pem/PemReader;

    new-instance v1, Ljava/io/InputStreamReader;

    sget-object v2, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-direct {v1, p0, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v1}, Lorg/bouncycastle/util/io/pem/PemReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v0}, Lorg/bouncycastle/util/io/pem/PemReader;->readPemObject()Lorg/bouncycastle/util/io/pem/PemObject;

    move-result-object p0

    invoke-virtual {p0}, Lorg/bouncycastle/util/io/pem/PemObject;->getContent()[B

    move-result-object p0

    new-instance v0, Lcom/jscape/util/h/e;

    invoke-direct {v0, p0}, Lcom/jscape/util/h/e;-><init>([B)V

    invoke-static {v0, p1}, Lcom/jscape/util/i/i;->a(Ljava/io/InputStream;Ljava/security/Provider;)Ljava/security/KeyPair;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/L;->b(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method

.method public static a(Ljava/security/KeyPair;Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/o;

    invoke-direct {v0}, Lcom/jscape/util/h/o;-><init>()V

    invoke-static {p0, v0}, Lcom/jscape/util/i/i;->a(Ljava/security/KeyPair;Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->d()[B

    move-result-object p0

    const/16 v0, 0x48

    invoke-static {p0, v0}, Lcom/jscape/util/Base64Format;->formatData([BI)Ljava/lang/String;

    move-result-object p0

    sget-object v0, Lcom/jscape/util/i/f;->b:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    sget-object v0, Ljava/nio/charset/StandardCharsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public static b()Lcom/jscape/util/i/f;
    .locals 2

    new-instance v0, Lcom/jscape/util/i/f;

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/util/i/f;-><init>(Ljava/security/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Ljava/security/KeyPair;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/i/f;->a:Ljava/security/Provider;

    invoke-static {p1, v0}, Lcom/jscape/util/i/f;->a(Ljava/io/InputStream;Ljava/security/Provider;)Ljava/security/KeyPair;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/security/KeyPair;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/jscape/util/i/f;->a(Ljava/security/KeyPair;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/util/i/f;->a(Ljava/io/InputStream;)Ljava/security/KeyPair;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Ljava/security/KeyPair;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/util/i/f;->b(Ljava/security/KeyPair;Ljava/io/OutputStream;)V

    return-void
.end method
