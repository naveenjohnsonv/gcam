.class public Lcom/jscape/util/i/m;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Ljava/security/PublicKey;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/security/Provider;


# direct methods
.method public constructor <init>(Ljava/security/Provider;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/i/m;->a:Ljava/security/Provider;

    return-void
.end method

.method public static a()Lcom/jscape/util/i/m;
    .locals 2

    new-instance v0, Lcom/jscape/util/i/m;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/jscape/util/i/m;-><init>(Ljava/security/Provider;)V

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;Ljava/security/Provider;)Ljava/security/PublicKey;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/X;->c(Ljava/io/InputStream;)[B

    move-result-object p0

    new-instance v0, Lcom/jscape/util/h/P;

    invoke-direct {v0, p0}, Lcom/jscape/util/h/P;-><init>([B)V

    const/4 p0, 0x3

    new-array p0, p0, [Lcom/jscape/util/h/K;

    new-instance v1, Lcom/jscape/util/i/n;

    invoke-direct {v1, p1}, Lcom/jscape/util/i/n;-><init>(Ljava/security/Provider;)V

    const/4 v2, 0x0

    aput-object v1, p0, v2

    new-instance v1, Lcom/jscape/util/i/k;

    invoke-direct {v1, p1}, Lcom/jscape/util/i/k;-><init>(Ljava/security/Provider;)V

    const/4 v2, 0x1

    aput-object v1, p0, v2

    new-instance v1, Lcom/jscape/util/i/l;

    invoke-direct {v1, p1}, Lcom/jscape/util/i/l;-><init>(Ljava/security/Provider;)V

    const/4 p1, 0x2

    aput-object v1, p0, p1

    invoke-static {v0, p0}, Lcom/jscape/util/h/M;->a(Lcom/jscape/util/h/L;[Lcom/jscape/util/h/K;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/security/PublicKey;

    return-object p0
.end method

.method public static a(Ljava/security/PublicKey;Ljava/io/OutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/P;

    invoke-direct {v0}, Lcom/jscape/util/h/P;-><init>()V

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/jscape/util/h/N;

    invoke-static {}, Lcom/jscape/util/i/n;->b()Lcom/jscape/util/i/n;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {}, Lcom/jscape/util/i/k;->b()Lcom/jscape/util/i/k;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {}, Lcom/jscape/util/i/l;->b()Lcom/jscape/util/i/l;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-static {p0, v0, v1}, Lcom/jscape/util/h/M;->a(Ljava/lang/Object;Lcom/jscape/util/h/L;[Lcom/jscape/util/h/N;)V

    invoke-virtual {v0}, Lcom/jscape/util/h/P;->a()[B

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public static b()Lcom/jscape/util/i/m;
    .locals 2

    new-instance v0, Lcom/jscape/util/i/m;

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/util/i/m;-><init>(Ljava/security/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Ljava/security/PublicKey;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/i/m;->a:Ljava/security/Provider;

    invoke-static {p1, v0}, Lcom/jscape/util/i/m;->a(Ljava/io/InputStream;Ljava/security/Provider;)Ljava/security/PublicKey;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/security/PublicKey;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/jscape/util/i/m;->a(Ljava/security/PublicKey;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/util/i/m;->a(Ljava/io/InputStream;)Ljava/security/PublicKey;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Ljava/security/PublicKey;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/util/i/m;->b(Ljava/security/PublicKey;Ljava/io/OutputStream;)V

    return-void
.end method
