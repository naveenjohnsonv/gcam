.class public Lcom/jscape/util/i/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Ljava/security/KeyPair;",
        ">;"
    }
.end annotation


# static fields
.field private static b:Ljava/lang/String;


# instance fields
.field private final a:Ljava/security/Provider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/jscape/util/i/i;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "DL7MJc"

    invoke-static {v0}, Lcom/jscape/util/i/i;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/security/Provider;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/i/i;->a:Ljava/security/Provider;

    return-void
.end method

.method public static a()Lcom/jscape/util/i/i;
    .locals 2

    new-instance v0, Lcom/jscape/util/i/i;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/jscape/util/i/i;-><init>(Ljava/security/Provider;)V

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;Ljava/security/Provider;)Ljava/security/KeyPair;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/X;->c(Ljava/io/InputStream;)[B

    move-result-object p0

    new-instance v0, Lcom/jscape/util/h/P;

    invoke-direct {v0, p0}, Lcom/jscape/util/h/P;-><init>([B)V

    const/4 p0, 0x3

    new-array p0, p0, [Lcom/jscape/util/h/K;

    new-instance v1, Lcom/jscape/util/i/j;

    invoke-direct {v1, p1}, Lcom/jscape/util/i/j;-><init>(Ljava/security/Provider;)V

    const/4 v2, 0x0

    aput-object v1, p0, v2

    new-instance v1, Lcom/jscape/util/i/g;

    invoke-direct {v1, p1}, Lcom/jscape/util/i/g;-><init>(Ljava/security/Provider;)V

    const/4 p1, 0x1

    aput-object v1, p0, p1

    new-instance p1, Lcom/jscape/util/i/h;

    invoke-direct {p1}, Lcom/jscape/util/i/h;-><init>()V

    const/4 v1, 0x2

    aput-object p1, p0, v1

    invoke-static {v0, p0}, Lcom/jscape/util/h/M;->a(Lcom/jscape/util/h/L;[Lcom/jscape/util/h/K;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/security/KeyPair;

    return-object p0
.end method

.method public static a(Ljava/security/KeyPair;Ljava/io/OutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/P;

    invoke-direct {v0}, Lcom/jscape/util/h/P;-><init>()V

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/jscape/util/h/N;

    invoke-static {}, Lcom/jscape/util/i/j;->b()Lcom/jscape/util/i/j;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {}, Lcom/jscape/util/i/g;->b()Lcom/jscape/util/i/g;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {}, Lcom/jscape/util/i/h;->a()Lcom/jscape/util/i/h;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-static {p0, v0, v1}, Lcom/jscape/util/h/M;->a(Ljava/lang/Object;Lcom/jscape/util/h/L;[Lcom/jscape/util/h/N;)V

    invoke-virtual {v0}, Lcom/jscape/util/h/P;->a()[B

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public static b()Lcom/jscape/util/i/i;
    .locals 2

    new-instance v0, Lcom/jscape/util/i/i;

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/jscape/util/i/i;-><init>(Ljava/security/Provider;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/util/i/i;->b:Ljava/lang/String;

    return-void
.end method

.method public static c()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/util/i/i;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Ljava/security/KeyPair;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/i/i;->a:Ljava/security/Provider;

    invoke-static {p1, v0}, Lcom/jscape/util/i/i;->a(Ljava/io/InputStream;Ljava/security/Provider;)Ljava/security/KeyPair;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/security/KeyPair;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p1, p2}, Lcom/jscape/util/i/i;->a(Ljava/security/KeyPair;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/util/i/i;->a(Ljava/io/InputStream;)Ljava/security/KeyPair;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Ljava/security/KeyPair;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/util/i/i;->b(Ljava/security/KeyPair;Ljava/io/OutputStream;)V

    return-void
.end method
