.class public Lcom/jscape/util/i/b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Ljava/security/KeyStore;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/security/Provider;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "@$tsdp\u0006@$tsdp"

    const/16 v5, 0xd

    const/4 v6, 0x6

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/4 v9, 0x1

    add-int/2addr v7, v9

    add-int v10, v7, v6

    invoke-virtual {v4, v7, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v12, 0x5

    move v14, v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v15, v10

    const/4 v2, 0x0

    :goto_2
    const/4 v3, 0x3

    if-gt v15, v2, :cond_3

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    if-eqz v13, :cond_1

    add-int/lit8 v10, v8, 0x1

    aput-object v2, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v10

    goto :goto_0

    :cond_0
    const/4 v5, 0x7

    const-string v4, "5K\u000b\u00035K\u000b"

    move v6, v3

    move v8, v10

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v3, v8, 0x1

    aput-object v2, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v6, v2

    move v8, v3

    :goto_3
    add-int/2addr v7, v9

    add-int v2, v7, v6

    invoke-virtual {v4, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v13, 0x0

    const/16 v14, 0x6a

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/i/b;->d:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v10, v2

    rem-int/lit8 v11, v2, 0x7

    const/16 v17, 0x47

    if-eqz v11, :cond_8

    if-eq v11, v9, :cond_7

    const/4 v9, 0x2

    if-eq v11, v9, :cond_6

    if-eq v11, v3, :cond_5

    if-eq v11, v0, :cond_4

    goto :goto_4

    :cond_4
    const/16 v17, 0x50

    goto :goto_4

    :cond_5
    const/16 v17, 0x25

    goto :goto_4

    :cond_6
    const/16 v17, 0x32

    goto :goto_4

    :cond_7
    const/16 v17, 0x6a

    goto :goto_4

    :cond_8
    const/16 v17, 0x15

    :goto_4
    xor-int v3, v14, v17

    xor-int v3, v16, v3

    int-to-char v3, v3

    aput-char v3, v10, v2

    add-int/lit8 v2, v2, 0x1

    const/4 v9, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/security/Provider;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/i/b;->a:Ljava/lang/String;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/i/b;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/jscape/util/i/b;->c:Ljava/security/Provider;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/util/i/b;
    .locals 3

    new-instance v0, Lcom/jscape/util/i/b;

    sget-object v1, Lcom/jscape/util/i/b;->d:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/jscape/util/i/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/security/Provider;)V

    return-object v0
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static a(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyStore;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p3, :cond_0

    :try_start_0
    invoke-static {p1, p3}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyStore;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/util/i/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_0
    invoke-static {p1}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object p1

    :goto_0
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object p2

    invoke-virtual {p1, p0, p2}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/L;->b(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method

.method public static a(Ljava/security/KeyStore;Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object p1

    invoke-virtual {p0, p2, p1}, Ljava/security/KeyStore;->store(Ljava/io/OutputStream;[C)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/L;->b(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method

.method public static b(Ljava/lang/String;)Lcom/jscape/util/i/b;
    .locals 3

    new-instance v0, Lcom/jscape/util/i/b;

    sget-object v1, Lcom/jscape/util/i/b;->d:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/jscape/util/i/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/security/Provider;)V

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Lcom/jscape/util/i/b;
    .locals 3

    new-instance v0, Lcom/jscape/util/i/b;

    sget-object v1, Lcom/jscape/util/i/b;->d:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/jscape/util/i/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/security/Provider;)V

    return-object v0
.end method

.method public static d(Ljava/lang/String;)Lcom/jscape/util/i/b;
    .locals 3

    new-instance v0, Lcom/jscape/util/i/b;

    sget-object v1, Lcom/jscape/util/i/b;->d:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {}, Lcom/jscape/util/c/c;->c()Ljava/security/Provider;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/jscape/util/i/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/security/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Ljava/security/KeyStore;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/i/b;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/util/i/b;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/jscape/util/i/b;->c:Ljava/security/Provider;

    invoke-static {p1, v0, v1, v2}, Lcom/jscape/util/i/b;->a(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyStore;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/security/KeyStore;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/i/b;->b:Ljava/lang/String;

    invoke-static {p1, v0, p2}, Lcom/jscape/util/i/b;->a(Ljava/security/KeyStore;Ljava/lang/String;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/util/i/b;->a(Ljava/io/InputStream;)Ljava/security/KeyStore;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Ljava/security/KeyStore;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/util/i/b;->a(Ljava/security/KeyStore;Ljava/io/OutputStream;)V

    return-void
.end method
