.class public final enum Lcom/jscape/util/o;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/jscape/util/o;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/jscape/util/o;

.field public static final enum b:Lcom/jscape/util/o;

.field public static final enum c:Lcom/jscape/util/o;

.field public static final enum d:Lcom/jscape/util/o;

.field private static final f:[Lcom/jscape/util/o;


# instance fields
.field public final e:J


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "Ri\u000eVk#\u0007Os\u001fJt(\u0011\u000f[b\u0013]d6\u0010B\u007f\tLx)\u001bT"

    const/16 v5, 0x1e

    const/16 v6, 0xe

    move v8, v3

    const/4 v7, -0x1

    :goto_0
    const/16 v9, 0x56

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    move v14, v3

    :goto_2
    const/4 v15, 0x2

    const/4 v2, 0x3

    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    if-eqz v12, :cond_1

    add-int/lit8 v2, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v2

    goto :goto_0

    :cond_0
    const/16 v5, 0x1f

    const/16 v4, 0xf

    const-string v6, "\u0000?N\u00009kM\u001f\"T\u0011%tF\t\u000f\n?N\u00009kM\u001f\"T\u0011%tF\t"

    move v8, v2

    const/4 v7, -0x1

    move-object/from16 v17, v6

    move v6, v4

    move-object/from16 v4, v17

    goto :goto_3

    :cond_1
    add-int/lit8 v11, v8, 0x1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v6, v2

    move v8, v11

    :goto_3
    const/16 v9, 0xb

    add-int/2addr v7, v10

    add-int v2, v7, v6

    invoke-virtual {v4, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move v12, v3

    goto :goto_1

    :cond_2
    new-instance v4, Lcom/jscape/util/o;

    aget-object v5, v1, v3

    const-wide/16 v6, 0x1

    invoke-direct {v4, v5, v3, v6, v7}, Lcom/jscape/util/o;-><init>(Ljava/lang/String;IJ)V

    sput-object v4, Lcom/jscape/util/o;->a:Lcom/jscape/util/o;

    new-instance v4, Lcom/jscape/util/o;

    aget-object v5, v1, v10

    const-wide/16 v6, 0x3e8

    invoke-direct {v4, v5, v10, v6, v7}, Lcom/jscape/util/o;-><init>(Ljava/lang/String;IJ)V

    sput-object v4, Lcom/jscape/util/o;->b:Lcom/jscape/util/o;

    new-instance v4, Lcom/jscape/util/o;

    aget-object v5, v1, v15

    const-wide/32 v6, 0xf4240

    invoke-direct {v4, v5, v15, v6, v7}, Lcom/jscape/util/o;-><init>(Ljava/lang/String;IJ)V

    sput-object v4, Lcom/jscape/util/o;->c:Lcom/jscape/util/o;

    new-instance v4, Lcom/jscape/util/o;

    aget-object v1, v1, v2

    const-wide/32 v5, 0x3b9aca00

    invoke-direct {v4, v1, v2, v5, v6}, Lcom/jscape/util/o;-><init>(Ljava/lang/String;IJ)V

    sput-object v4, Lcom/jscape/util/o;->d:Lcom/jscape/util/o;

    new-array v0, v0, [Lcom/jscape/util/o;

    sget-object v1, Lcom/jscape/util/o;->a:Lcom/jscape/util/o;

    aput-object v1, v0, v3

    sget-object v1, Lcom/jscape/util/o;->b:Lcom/jscape/util/o;

    aput-object v1, v0, v10

    sget-object v1, Lcom/jscape/util/o;->c:Lcom/jscape/util/o;

    aput-object v1, v0, v15

    aput-object v4, v0, v2

    sput-object v0, Lcom/jscape/util/o;->f:[Lcom/jscape/util/o;

    return-void

    :cond_3
    aget-char v16, v11, v14

    rem-int/lit8 v3, v14, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v10, :cond_8

    if-eq v3, v15, :cond_7

    if-eq v3, v2, :cond_6

    if-eq v3, v0, :cond_5

    const/4 v15, 0x5

    if-eq v3, v15, :cond_4

    goto :goto_4

    :cond_4
    const/16 v2, 0x30

    goto :goto_4

    :cond_5
    const/16 v2, 0x6d

    goto :goto_4

    :cond_6
    const/16 v2, 0x5f

    goto :goto_4

    :cond_7
    const/16 v2, 0xc

    goto :goto_4

    :cond_8
    const/16 v2, 0x76

    goto :goto_4

    :cond_9
    const/16 v2, 0x46

    :goto_4
    xor-int/2addr v2, v9

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    const/4 v3, 0x0

    goto/16 :goto_2
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-wide p3, p0, Lcom/jscape/util/o;->e:J

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/jscape/util/o;
    .locals 1

    const-class v0, Lcom/jscape/util/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/jscape/util/o;

    return-object p0
.end method

.method public static a()[Lcom/jscape/util/o;
    .locals 1

    sget-object v0, Lcom/jscape/util/o;->f:[Lcom/jscape/util/o;

    invoke-virtual {v0}, [Lcom/jscape/util/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jscape/util/o;

    return-object v0
.end method
