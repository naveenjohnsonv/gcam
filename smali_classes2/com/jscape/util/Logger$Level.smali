.class public final Lcom/jscape/util/Logger$Level;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field public static final ERROR:Lcom/jscape/util/Logger$Level;

.field public static final FINE:Lcom/jscape/util/Logger$Level;

.field public static final INFO:Lcom/jscape/util/Logger$Level;

.field public static final OFF:Lcom/jscape/util/Logger$Level;


# instance fields
.field private name:Ljava/lang/String;

.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x3

    const/4 v4, 0x0

    const-string v5, "\u0007{\u0004\u0004\u0001s\u0004b"

    const/16 v6, 0x8

    move v8, v3

    move v9, v4

    const/4 v7, -0x1

    :goto_0
    const/16 v10, 0x69

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    move v15, v4

    :goto_2
    const/4 v2, 0x2

    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    if-eqz v13, :cond_1

    add-int/lit8 v2, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v2

    goto :goto_0

    :cond_0
    const/16 v6, 0xa

    const-string v5, "\u0007}\u0005a\u0005\u0004f\u0019k\u0019"

    move v8, v0

    move v9, v2

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v12, v9, 0x1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v2

    move v9, v12

    :goto_3
    const/16 v10, 0x60

    add-int/2addr v7, v11

    add-int v2, v7, v8

    invoke-virtual {v5, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    move v13, v4

    goto :goto_1

    :cond_2
    new-instance v5, Lcom/jscape/util/Logger$Level;

    aget-object v6, v1, v2

    invoke-direct {v5, v6, v11}, Lcom/jscape/util/Logger$Level;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/jscape/util/Logger$Level;->FINE:Lcom/jscape/util/Logger$Level;

    new-instance v5, Lcom/jscape/util/Logger$Level;

    aget-object v6, v1, v11

    invoke-direct {v5, v6, v2}, Lcom/jscape/util/Logger$Level;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/jscape/util/Logger$Level;->INFO:Lcom/jscape/util/Logger$Level;

    new-instance v2, Lcom/jscape/util/Logger$Level;

    aget-object v5, v1, v3

    invoke-direct {v2, v5, v3}, Lcom/jscape/util/Logger$Level;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/jscape/util/Logger$Level;->ERROR:Lcom/jscape/util/Logger$Level;

    new-instance v2, Lcom/jscape/util/Logger$Level;

    aget-object v1, v1, v4

    invoke-direct {v2, v1, v0}, Lcom/jscape/util/Logger$Level;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/jscape/util/Logger$Level;->OFF:Lcom/jscape/util/Logger$Level;

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v4, v15, 0x7

    const/16 v17, 0x2b

    if-eqz v4, :cond_7

    if-eq v4, v11, :cond_6

    if-eq v4, v2, :cond_8

    if-eq v4, v3, :cond_5

    if-eq v4, v0, :cond_8

    const/4 v2, 0x5

    if-eq v4, v2, :cond_4

    const/16 v17, 0x57

    goto :goto_4

    :cond_4
    const/16 v17, 0x30

    goto :goto_4

    :cond_5
    const/16 v17, 0x44

    goto :goto_4

    :cond_6
    const/16 v17, 0x54

    goto :goto_4

    :cond_7
    const/16 v17, 0x21

    :cond_8
    :goto_4
    xor-int v2, v10, v17

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v12, v15

    add-int/lit8 v15, v15, 0x1

    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method protected constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/Logger$Level;->name:Ljava/lang/String;

    iput p2, p0, Lcom/jscape/util/Logger$Level;->value:I

    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 2

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    check-cast p1, Lcom/jscape/util/Logger$Level;

    iget v1, p0, Lcom/jscape/util/Logger$Level;->value:I

    iget p1, p1, Lcom/jscape/util/Logger$Level;->value:I

    if-eqz v0, :cond_1

    if-le v1, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz v0, :cond_3

    :cond_1
    if-ge v1, p1, :cond_2

    const/4 p1, -0x1

    return p1

    :cond_2
    const/4 v1, 0x0

    :cond_3
    return v1
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/Logger$Level;->name:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/jscape/util/Logger$Level;->value:I

    return v0
.end method

.method public intValue()I
    .locals 1

    iget v0, p0, Lcom/jscape/util/Logger$Level;->value:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/Logger$Level;->name:Ljava/lang/String;

    return-object v0
.end method
