.class public Lcom/jscape/util/ak;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/ThreadFactory;


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final b:Lcom/jscape/util/r;

.field protected final c:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/jscape/util/r;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/ak;->a:Ljava/lang/String;

    new-instance p1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object p1, p0, Lcom/jscape/util/ak;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/ak;->b:Lcom/jscape/util/r;

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;
    .locals 2

    new-instance v0, Lcom/jscape/util/ak;

    sget-object v1, Lcom/jscape/util/r;->b:Lcom/jscape/util/r;

    invoke-direct {v0, p0, v1}, Lcom/jscape/util/ak;-><init>(Ljava/lang/String;Lcom/jscape/util/r;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/util/concurrent/ThreadFactory;
    .locals 2

    new-instance v0, Lcom/jscape/util/ak;

    sget-object v1, Lcom/jscape/util/r;->a:Lcom/jscape/util/r;

    invoke-direct {v0, p0, v1}, Lcom/jscape/util/ak;-><init>(Ljava/lang/String;Lcom/jscape/util/r;)V

    return-object v0
.end method


# virtual methods
.method protected a(Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 1

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p2, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    return-object v0
.end method

.method public newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 4

    iget-object v0, p0, Lcom/jscape/util/ak;->a:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/jscape/util/ak;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/jscape/util/ak;->a(Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Thread;

    move-result-object p1

    iget-object v0, p0, Lcom/jscape/util/ak;->b:Lcom/jscape/util/r;

    invoke-virtual {v0, p1}, Lcom/jscape/util/r;->a(Ljava/lang/Thread;)V

    return-object p1
.end method
