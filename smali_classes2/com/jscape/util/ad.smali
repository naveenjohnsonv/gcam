.class Lcom/jscape/util/ad;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcom/jscape/util/ad;

.field private static final b:Ljava/util/Map;


# instance fields
.field private final c:Ljava/io/PrintStream;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/jscape/util/ad;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-direct {v0, v1}, Lcom/jscape/util/ad;-><init>(Ljava/io/PrintStream;)V

    sput-object v0, Lcom/jscape/util/ad;->a:Lcom/jscape/util/ad;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/jscape/util/ad;->b:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/io/PrintStream;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/ad;->c:Ljava/io/PrintStream;

    const/4 p1, 0x1

    iput p1, p0, Lcom/jscape/util/ad;->d:I

    return-void
.end method

.method public static declared-synchronized a(Ljava/io/PrintStream;)Lcom/jscape/util/ad;
    .locals 3

    const-class v0, Lcom/jscape/util/ad;

    monitor-enter v0

    :try_start_0
    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v1

    invoke-static {p0}, Lcom/jscape/util/w;->a(Ljava/lang/Object;)V

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    if-ne p0, v2, :cond_0

    sget-object p0, Lcom/jscape/util/ad;->a:Lcom/jscape/util/ad;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :cond_0
    :try_start_1
    sget-object v2, Lcom/jscape/util/ad;->b:Ljava/util/Map;

    if-nez v1, :cond_2

    invoke-interface {v2, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v2, Lcom/jscape/util/ad;->b:Ljava/util/Map;

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/jscape/util/ad;

    invoke-direct {v1, p0}, Lcom/jscape/util/ad;-><init>(Ljava/io/PrintStream;)V

    sget-object v2, Lcom/jscape/util/ad;->b:Ljava/util/Map;

    invoke-interface {v2, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    return-object v1

    :cond_2
    :goto_0
    :try_start_2
    invoke-interface {v2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/jscape/util/ad;

    invoke-direct {p0}, Lcom/jscape/util/ad;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/jscape/util/ad;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/jscape/util/ad;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/jscape/util/ad;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/jscape/util/ad;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jscape/util/ad;->d()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/jscape/util/ad;->c:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized c()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v1, Lcom/jscape/util/ad;->a:Lcom/jscape/util/ad;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p0, v1, :cond_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/jscape/util/ad;->b()V

    :cond_1
    if-nez v0, :cond_2

    iget v0, p0, Lcom/jscape/util/ad;->d:I

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/jscape/util/ad;->c:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    iget v1, p0, Lcom/jscape/util/ad;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()Ljava/io/PrintStream;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/ad;->c:Ljava/io/PrintStream;

    return-object v0
.end method
