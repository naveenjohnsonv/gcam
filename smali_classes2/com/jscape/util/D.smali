.class public Lcom/jscape/util/D;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/jscape/util/E;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jscape/util/F;

    invoke-direct {v0}, Lcom/jscape/util/F;-><init>()V

    sput-object v0, Lcom/jscape/util/D;->a:Lcom/jscape/util/E;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/jscape/util/E;
    .locals 1

    sget-object v0, Lcom/jscape/util/D;->a:Lcom/jscape/util/E;

    return-object v0
.end method

.method public static a(Lcom/jscape/util/E;)V
    .locals 0

    sput-object p0, Lcom/jscape/util/D;->a:Lcom/jscape/util/E;

    return-void
.end method

.method public static a(J)Z
    .locals 3

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v1

    cmp-long p0, v1, p0

    if-nez v0, :cond_1

    if-gtz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :cond_1
    :goto_0
    return p0
.end method

.method public static b()J
    .locals 2

    sget-object v0, Lcom/jscape/util/D;->a:Lcom/jscape/util/E;

    invoke-interface {v0}, Lcom/jscape/util/E;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public static b(J)Z
    .locals 3

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v1

    cmp-long p0, p0, v1

    if-nez v0, :cond_1

    if-gtz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :cond_1
    :goto_0
    return p0
.end method

.method public static c()J
    .locals 3

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/16 v1, 0xb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public static c(J)J
    .locals 2

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v0

    add-long/2addr v0, p0

    return-wide v0
.end method

.method public static d(J)J
    .locals 2

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v0

    sub-long/2addr v0, p0

    return-wide v0
.end method

.method public static e(J)J
    .locals 2

    invoke-static {}, Lcom/jscape/util/D;->b()J

    move-result-wide v0

    sub-long/2addr v0, p0

    return-wide v0
.end method

.method public static f(J)V
    .locals 3

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    cmp-long v0, p0, v1

    if-lez v0, :cond_1

    const-wide/32 v1, 0xf4240

    :cond_0
    mul-long/2addr p0, v1

    invoke-static {p0, p1}, Ljava/util/concurrent/locks/LockSupport;->parkNanos(J)V

    :cond_1
    return-void
.end method
