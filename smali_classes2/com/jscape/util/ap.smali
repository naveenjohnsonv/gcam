.class public final Lcom/jscape/util/ap;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<F:",
        "Ljava/lang/Object;",
        "S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field public a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TF;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xc

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x66

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "|)7<k[\rE:-:v\t\u0000h-+(O\u0005Hu"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x16

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/ap;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0xd

    goto :goto_2

    :cond_2
    const/16 v12, 0x46

    goto :goto_2

    :cond_3
    const/16 v12, 0x2d

    goto :goto_2

    :cond_4
    const/16 v12, 0x28

    goto :goto_2

    :cond_5
    const/16 v12, 0x38

    goto :goto_2

    :cond_6
    const/16 v12, 0x2e

    goto :goto_2

    :cond_7
    const/16 v12, 0x4a

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lcom/jscape/util/ap;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;TS;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/ap;->a:Ljava/lang/Object;

    iput-object p2, p0, Lcom/jscape/util/ap;->b:Ljava/lang/Object;

    return-void
.end method

.method public static a(Ljava/lang/Object;)Lcom/jscape/util/ap;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F:",
            "Ljava/lang/Object;",
            ">(TF;)",
            "Lcom/jscape/util/ap<",
            "TF;TF;>;"
        }
    .end annotation

    invoke-static {p0, p0}, Lcom/jscape/util/ap;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/jscape/util/ap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            ">(TF;TS;)",
            "Lcom/jscape/util/ap<",
            "TF;TS;>;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/ap;

    invoke-direct {v0, p0, p1}, Lcom/jscape/util/ap;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TF;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/ap;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TS;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/ap;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public b(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/jscape/util/ap;->a:Ljava/lang/Object;

    return-void
.end method

.method public c(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/jscape/util/ap;->b:Ljava/lang/Object;

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    if-ne p0, p1, :cond_0

    return v1

    :cond_0
    move-object v2, p1

    goto :goto_0

    :cond_1
    move-object v2, p0

    :goto_0
    const/4 v3, 0x0

    if-eqz v2, :cond_c

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v2, v4, :cond_3

    goto :goto_4

    :cond_2
    move-object p1, v2

    :cond_3
    check-cast p1, Lcom/jscape/util/ap;

    iget-object v2, p0, Lcom/jscape/util/ap;->a:Ljava/lang/Object;

    if-eqz v0, :cond_5

    if-eqz v2, :cond_4

    if-eqz v0, :cond_7

    iget-object v4, p1, Lcom/jscape/util/ap;->a:Ljava/lang/Object;

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    goto :goto_1

    :cond_4
    iget-object v2, p1, Lcom/jscape/util/ap;->a:Ljava/lang/Object;

    :cond_5
    if-eqz v0, :cond_7

    if-eqz v2, :cond_6

    :goto_1
    return v3

    :cond_6
    iget-object v2, p0, Lcom/jscape/util/ap;->b:Ljava/lang/Object;

    :cond_7
    if-eqz v0, :cond_a

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/jscape/util/ap;->b:Ljava/lang/Object;

    iget-object p1, p1, Lcom/jscape/util/ap;->b:Ljava/lang/Object;

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz v0, :cond_8

    if-nez p1, :cond_b

    goto :goto_2

    :cond_8
    move v1, p1

    goto :goto_3

    :cond_9
    iget-object v2, p1, Lcom/jscape/util/ap;->b:Ljava/lang/Object;

    :cond_a
    if-eqz v2, :cond_b

    :goto_2
    return v3

    :cond_b
    :goto_3
    return v1

    :cond_c
    :goto_4
    return v3
.end method

.method public hashCode()I
    .locals 4

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/util/ap;->a:Ljava/lang/Object;

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :goto_1
    mul-int/lit8 v1, v1, 0x1f

    iget-object v3, p0, Lcom/jscape/util/ap;->b:Ljava/lang/Object;

    if-eqz v0, :cond_2

    if-eqz v3, :cond_3

    :cond_2
    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v1, v2

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/ap;->c:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/ap;->a:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/ap;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
