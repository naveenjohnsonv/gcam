.class public Lcom/jscape/util/ConfigurationProperties;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final base:Ljava/util/Properties;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "?F\u001c\u007fTlE\t\u0003\u0001eSy\u0000\u001bB\u0001\u007fX>F\u0002QMaXg\u0000J\u0006\u001e-\u001dwSMM\u0002~\u001dxO\u0018M\t$1?F\u001c\u007fTlE\t\u0003\u000feRrE\u000cMM|\\rU\u0008\u0003\u000beO>K\u0008ZM-\u0018m\u0007MJ\u001e*SqTME\u0002\u007fSz\u000e;?F\u001c\u007fTlE\t\u0003\u0004dI{G\u0008QM|\\rU\u0008\u0003\u000beO>K\u0008ZM-\u0018m\u0007MK\u000cy\u001dkN\u001eV\u001dzRlT\u0008GMlRlM\u000cWC)?F\u001c\u007fTlE\t\u0003\u001bkQkEME\u0002x\u001duE\u0014\u0003J/N9\u0000\u0004PMdRj\u0000\u000bL\u0018dY06?F\u001c\u007fTlE\t\u0003\u001bkQkEME\u0002x\u001duE\u0014\u0003J/N9\u0000\u0005B\u001e*HpS\u0018S\u001deOjE\t\u0003\u000beOsA\u0019\rM/N1?F\u001c\u007fTlE\t\u0003\u0004dI{G\u0008QM|\\rU\u0008\u0003\u000beO>K\u0008ZM-\u0018m\u0007MJ\u001e*SqTME\u0002\u007fSz\u000e0?F\u001c\u007fTlE\t\u0003\u001e~OwN\n\u0003\u001bkQkEME\u0002x\u001duE\u0014\u0003J/N9\u0000\u0004PMdRj\u0000\u000bL\u0018dY0"

    const/16 v4, 0x160

    const/16 v5, 0x2e

    move v7, v2

    const/4 v6, -0x1

    :goto_0
    const/16 v8, 0x2d

    const/4 v9, 0x1

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const/4 v11, -0x1

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->toCharArray()[C

    move-result-object v10

    array-length v12, v10

    move v13, v2

    :goto_2
    if-gt v12, v13, :cond_3

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v8}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v10, v7, 0x1

    if-eqz v11, :cond_1

    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    goto :goto_0

    :cond_0
    const/16 v4, 0x57

    const/16 v3, 0x38

    const-string v5, "Q(r\u0011:\u0002+gmo\u000b=\u0017nu,o\u00116P(l?#\u000f6\tn$hpCs\u0018/pmv\n \u0005>s\"q\u00106\u0014ne\"q\t2\u0004`\u001e@\"m\u0002:\u0017;q,w\r<\u001e\u001eq\"s\u0001!\u0004\'f>#\u001f1\u0011=fp"

    move v7, v10

    const/4 v6, -0x1

    move-object/from16 v17, v5

    move v5, v3

    move-object/from16 v3, v17

    goto :goto_3

    :cond_1
    aput-object v8, v0, v7

    add-int/2addr v6, v5

    if-ge v6, v4, :cond_2

    invoke-virtual {v3, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    move v7, v10

    :goto_3
    const/16 v8, 0x43

    add-int/2addr v6, v9

    add-int v10, v6, v5

    invoke-virtual {v3, v6, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move v11, v2

    goto :goto_1

    :cond_2
    sput-object v0, Lcom/jscape/util/ConfigurationProperties;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v14, v10, v13

    rem-int/lit8 v15, v13, 0x7

    const/16 v16, 0x40

    if-eqz v15, :cond_8

    if-eq v15, v9, :cond_7

    const/4 v1, 0x2

    if-eq v15, v1, :cond_8

    const/4 v1, 0x3

    if-eq v15, v1, :cond_6

    const/4 v1, 0x4

    if-eq v15, v1, :cond_5

    const/4 v1, 0x5

    if-eq v15, v1, :cond_4

    const/16 v16, 0xd

    goto :goto_4

    :cond_4
    const/16 v16, 0x33

    goto :goto_4

    :cond_5
    const/16 v16, 0x10

    goto :goto_4

    :cond_6
    const/16 v16, 0x27

    goto :goto_4

    :cond_7
    const/16 v16, 0xe

    :cond_8
    :goto_4
    xor-int v1, v8, v16

    xor-int/2addr v1, v14

    int-to-char v1, v1

    aput-char v1, v10, v13

    add-int/lit8 v13, v13, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 1

    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    invoke-direct {p0, v0}, Lcom/jscape/util/ConfigurationProperties;-><init>(Ljava/util/Properties;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Properties;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method public static forFile(Ljava/io/File;)Lcom/jscape/util/ConfigurationProperties;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p0

    invoke-static {p0}, Lcom/jscape/util/ConfigurationProperties;->forFile(Ljava/nio/file/Path;)Lcom/jscape/util/ConfigurationProperties;

    move-result-object p0

    return-object p0
.end method

.method public static forFile(Ljava/nio/file/Path;)Lcom/jscape/util/ConfigurationProperties;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p0}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object p0

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/jscape/util/X;->a(Ljava/io/File;Ljava/util/Properties;)Ljava/util/Properties;

    move-result-object p0

    new-instance v0, Lcom/jscape/util/ConfigurationProperties;

    invoke-direct {v0, p0}, Lcom/jscape/util/ConfigurationProperties;-><init>(Ljava/util/Properties;)V

    return-object v0
.end method

.method public static readValue(Ljava/io/InputStream;)Lcom/jscape/util/ConfigurationProperties;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;Ljava/util/Properties;)Ljava/util/Properties;

    move-result-object p0

    new-instance v0, Lcom/jscape/util/ConfigurationProperties;

    invoke-direct {v0, p0}, Lcom/jscape/util/ConfigurationProperties;-><init>(Ljava/util/Properties;)V

    return-object v0
.end method

.method public static toFile(Lcom/jscape/util/ConfigurationProperties;Ljava/io/File;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/jscape/util/ConfigurationProperties;->toFile(Lcom/jscape/util/ConfigurationProperties;Ljava/nio/file/Path;)V

    return-void
.end method

.method public static toFile(Lcom/jscape/util/ConfigurationProperties;Ljava/nio/file/Path;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object p0, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    invoke-interface {p1}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object p1

    const-string v0, ""

    invoke-static {p0, v0, p1}, Lcom/jscape/util/X;->a(Ljava/util/Properties;Ljava/lang/String;Ljava/io/File;)V

    return-void
.end method

.method public static writeValue(Lcom/jscape/util/ConfigurationProperties;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object p0, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    const-string v0, ""

    invoke-static {p0, v0, p1}, Lcom/jscape/util/X;->a(Ljava/util/Properties;Ljava/lang/String;Ljava/io/OutputStream;)V

    return-void
.end method


# virtual methods
.method public asProperties()Ljava/util/Properties;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    return-object v0
.end method

.method public booleanValueOf(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    invoke-virtual {v1, p1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;

    sget-object v1, Lcom/jscape/util/ConfigurationProperties;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/ConfigurationProperties;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public booleanValueOf(Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 2

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    invoke-virtual {v1, p1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p2

    :cond_1
    return-object p2
.end method

.method public intValueOf(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    invoke-virtual {v1, p1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;

    sget-object v1, Lcom/jscape/util/ConfigurationProperties;->a:[Ljava/lang/String;

    const/4 v4, 0x5

    aget-object v1, v1, v4

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/ConfigurationProperties;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    :try_start_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :catch_1
    new-instance v0, Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;

    sget-object v1, Lcom/jscape/util/ConfigurationProperties;->a:[Ljava/lang/String;

    const/4 v4, 0x2

    aget-object v1, v1, v4

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public intValueOf(Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    invoke-virtual {v0, p1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    return-object p2
.end method

.method public longValueOf(Ljava/lang/String;)Ljava/lang/Long;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    invoke-virtual {v1, p1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;

    sget-object v1, Lcom/jscape/util/ConfigurationProperties;->a:[Ljava/lang/String;

    aget-object v1, v1, v3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/ConfigurationProperties;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    :try_start_1
    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :catch_1
    new-instance v0, Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;

    sget-object v1, Lcom/jscape/util/ConfigurationProperties;->a:[Ljava/lang/String;

    const/4 v4, 0x7

    aget-object v1, v1, v4

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public longValueOf(Ljava/lang/String;Ljava/lang/Long;)Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    invoke-virtual {v0, p1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    return-object p2
.end method

.method public put(Ljava/lang/String;Ljava/lang/Object;)Lcom/jscape/util/ConfigurationProperties;
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    return-object p0
.end method

.method public put(Ljava/lang/String;Ljava/lang/Object;Lcom/jscape/util/ConfigurationProperties$Format;)Lcom/jscape/util/ConfigurationProperties;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TT;",
            "Lcom/jscape/util/ConfigurationProperties$Format<",
            "TT;>;)",
            "Lcom/jscape/util/ConfigurationProperties;"
        }
    .end annotation

    invoke-interface {p3, p2}, Lcom/jscape/util/ConfigurationProperties$Format;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    invoke-virtual {p3, p1, p2}, Ljava/util/Properties;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    return-object p0
.end method

.method public stringValueOf(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    invoke-virtual {v1, p1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance v0, Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;

    sget-object v1, Lcom/jscape/util/ConfigurationProperties;->a:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/ConfigurationProperties;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    return-object v1
.end method

.method public stringValueOf(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    invoke-virtual {v1, p1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    move-object p2, p1

    :cond_1
    return-object p2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/ConfigurationProperties;->a:[Ljava/lang/String;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public valueOf(Ljava/lang/String;Lcom/jscape/util/ConfigurationProperties$Format;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lcom/jscape/util/ConfigurationProperties$Format<",
            "TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    iget-object v1, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    invoke-virtual {v1, p1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_2

    const/4 v0, 0x0

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-interface {p2, v1}, Lcom/jscape/util/ConfigurationProperties$Format;->parse(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p2

    :try_start_1
    new-instance v1, Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;

    sget-object v3, Lcom/jscape/util/ConfigurationProperties;->a:[Ljava/lang/String;

    const/4 v4, 0x4

    aget-object v3, v3, v4

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v0

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    aput-object p1, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/ConfigurationProperties;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_1
    :try_start_2
    new-instance p2, Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;

    sget-object v1, Lcom/jscape/util/ConfigurationProperties;->a:[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v1, v1, v3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/jscape/util/ConfigurationProperties$UnsupportedValueException;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/ConfigurationProperties;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_2
    :goto_1
    return-object v1
.end method

.method public valueOf(Ljava/lang/String;Lcom/jscape/util/ConfigurationProperties$Format;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lcom/jscape/util/ConfigurationProperties$Format<",
            "TT;>;TT;)TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/ConfigurationProperties;->base:Ljava/util/Properties;

    invoke-virtual {v0, p1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :try_start_0
    invoke-interface {p2, p1}, Lcom/jscape/util/ConfigurationProperties$Format;->parse(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    return-object p3
.end method
