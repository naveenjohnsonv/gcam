.class public Lcom/jscape/util/ByteBuffer;
.super Ljava/lang/Object;


# static fields
.field private static final DEFAULT_BUFFER_SIZE:I = 0x1fa0


# instance fields
.field private buffer:[B

.field private bytes:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x1fa0

    invoke-direct {p0, v0}, Lcom/jscape/util/ByteBuffer;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/jscape/util/ByteBuffer;->buffer:[B

    const/4 p1, 0x0

    iput p1, p0, Lcom/jscape/util/ByteBuffer;->bytes:I

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/ByteBuffer;->buffer:[B

    iget v0, p0, Lcom/jscape/util/ByteBuffer;->bytes:I

    array-length p1, p1

    add-int/2addr v0, p1

    iput v0, p0, Lcom/jscape/util/ByteBuffer;->bytes:I

    return-void
.end method

.method private expand(I)V
    .locals 4

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jscape/util/ByteBuffer;->buffer:[B

    array-length v0, v0

    if-le p1, v0, :cond_1

    int-to-double v0, p1

    const-wide v2, 0x3ff199999999999aL    # 1.1

    mul-double/2addr v0, v2

    double-to-int p1, v0

    :cond_0
    new-array p1, p1, [B

    iget-object v0, p0, Lcom/jscape/util/ByteBuffer;->buffer:[B

    iget v1, p0, Lcom/jscape/util/ByteBuffer;->bytes:I

    const/4 v2, 0x0

    invoke-static {v0, v2, p1, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object p1, p0, Lcom/jscape/util/ByteBuffer;->buffer:[B

    :cond_1
    return-void
.end method


# virtual methods
.method public append(B)V
    .locals 3

    const/4 v0, 0x1

    new-array v1, v0, [B

    const/4 v2, 0x0

    aput-byte p1, v1, v2

    invoke-virtual {p0, v1, v2, v0}, Lcom/jscape/util/ByteBuffer;->append([BII)V

    return-void
.end method

.method public append(C)V
    .locals 0

    int-to-byte p1, p1

    invoke-virtual {p0, p1}, Lcom/jscape/util/ByteBuffer;->append(B)V

    return-void
.end method

.method public append(Lcom/jscape/util/ByteBuffer;)V
    .locals 0

    invoke-virtual {p1}, Lcom/jscape/util/ByteBuffer;->toByteArray()[B

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/util/ByteBuffer;->append([B)V

    return-void
.end method

.method public append(Ljava/lang/String;Ljava/nio/charset/Charset;)V
    .locals 0

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object p2

    :goto_0
    invoke-virtual {p1, p2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/jscape/util/ByteBuffer;->append([B)V

    :cond_1
    return-void
.end method

.method public append([B)V
    .locals 2

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/jscape/util/ByteBuffer;->append([BII)V

    :cond_0
    return-void
.end method

.method public append([BII)V
    .locals 5

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    if-eqz p1, :cond_1

    iget v1, p0, Lcom/jscape/util/ByteBuffer;->bytes:I

    add-int/2addr v1, p3

    invoke-direct {p0, v1}, Lcom/jscape/util/ByteBuffer;->expand(I)V

    const/4 v1, 0x0

    :cond_0
    if-ge v1, p3, :cond_1

    iget-object v2, p0, Lcom/jscape/util/ByteBuffer;->buffer:[B

    iget v3, p0, Lcom/jscape/util/ByteBuffer;->bytes:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/jscape/util/ByteBuffer;->bytes:I

    add-int v4, p2, v1

    aget-byte v4, p1, v4

    aput-byte v4, v2, v3

    add-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method

.method public append([C)V
    .locals 3

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    const/4 v1, 0x0

    :cond_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    aget-char v2, p1, v1

    invoke-virtual {p0, v2}, Lcom/jscape/util/ByteBuffer;->append(C)V

    add-int/lit8 v1, v1, 0x1

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method public contains([B)Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/jscape/util/ByteBuffer;->contains([BZ)Z

    move-result p1

    return p1
.end method

.method public contains([BZ)Z
    .locals 6

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    const/4 v1, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v0, :cond_3

    if-eqz p2, :cond_2

    invoke-virtual {p0, p1}, Lcom/jscape/util/ByteBuffer;->indexOf([B)I

    move-result p2

    if-nez v0, :cond_1

    if-eq p2, v1, :cond_0

    move p2, v2

    goto :goto_0

    :cond_0
    move p2, v3

    :cond_1
    :goto_0
    if-eqz v0, :cond_8

    goto :goto_1

    :cond_2
    move p2, v3

    :goto_1
    move v5, v3

    move v3, p2

    move p2, v5

    :cond_3
    array-length v4, p1

    if-ge p2, v4, :cond_7

    aget-byte v4, p1, p2

    invoke-virtual {p0, v4}, Lcom/jscape/util/ByteBuffer;->indexOf(B)I

    move-result v4

    if-nez v0, :cond_9

    if-nez v0, :cond_4

    if-eq v4, v1, :cond_5

    move v4, v2

    :cond_4
    if-eqz v0, :cond_6

    move v3, v4

    :cond_5
    add-int/lit8 p2, p2, 0x1

    if-eqz v0, :cond_3

    goto :goto_2

    :cond_6
    move p2, v4

    goto :goto_3

    :cond_7
    :goto_2
    move p2, v3

    :cond_8
    :goto_3
    move v4, p2

    :cond_9
    return v4
.end method

.method public endsWith(Ljava/lang/String;Ljava/nio/charset/Charset;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    invoke-virtual {p0, p2}, Lcom/jscape/util/ByteBuffer;->toString(Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    if-ne p0, p1, :cond_0

    return v1

    :cond_0
    move-object v2, p1

    goto :goto_0

    :cond_1
    move-object v2, p0

    :goto_0
    const/4 v3, 0x0

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-nez v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v2, v4, :cond_3

    goto :goto_1

    :cond_2
    move-object p1, v2

    :cond_3
    check-cast p1, Lcom/jscape/util/ByteBuffer;

    iget v2, p0, Lcom/jscape/util/ByteBuffer;->bytes:I

    if-nez v0, :cond_5

    iget v4, p1, Lcom/jscape/util/ByteBuffer;->bytes:I

    if-eq v2, v4, :cond_4

    return v3

    :cond_4
    iget-object v2, p0, Lcom/jscape/util/ByteBuffer;->buffer:[B

    iget-object p1, p1, Lcom/jscape/util/ByteBuffer;->buffer:[B

    invoke-static {v2, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    :cond_5
    if-nez v0, :cond_6

    if-nez v2, :cond_7

    return v3

    :cond_6
    move v1, v2

    :cond_7
    return v1

    :cond_8
    :goto_1
    return v3
.end method

.method public equals(Ljava/lang/String;Ljava/nio/charset/Charset;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    invoke-virtual {p0, p2}, Lcom/jscape/util/ByteBuffer;->toString(Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getBuffer(I)Lcom/jscape/util/ByteBuffer;
    .locals 1

    iget v0, p0, Lcom/jscape/util/ByteBuffer;->bytes:I

    invoke-virtual {p0, p1, v0}, Lcom/jscape/util/ByteBuffer;->getBuffer(II)Lcom/jscape/util/ByteBuffer;

    move-result-object p1

    return-object p1
.end method

.method public getBuffer(II)Lcom/jscape/util/ByteBuffer;
    .locals 3

    sub-int/2addr p2, p1

    :try_start_0
    new-array v0, p2, [B

    iget-object v1, p0, Lcom/jscape/util/ByteBuffer;->buffer:[B

    const/4 v2, 0x0

    invoke-static {v1, p1, v0, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance p1, Lcom/jscape/util/ByteBuffer;

    invoke-direct {p1, v0}, Lcom/jscape/util/ByteBuffer;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    new-instance p1, Lcom/jscape/util/ByteBuffer;

    invoke-direct {p1}, Lcom/jscape/util/ByteBuffer;-><init>()V

    return-object p1
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/ByteBuffer;->buffer:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/jscape/util/ByteBuffer;->bytes:I

    add-int/2addr v0, v1

    return v0
.end method

.method public indexOf(B)I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/jscape/util/ByteBuffer;->indexOf(BI)I

    move-result p1

    return p1
.end method

.method public indexOf(BI)I
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte p1, v0, v1

    invoke-virtual {p0, v0, p2}, Lcom/jscape/util/ByteBuffer;->indexOf([BI)I

    move-result p1

    return p1
.end method

.method public indexOf(Ljava/lang/String;Ljava/nio/charset/Charset;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/jscape/util/ByteBuffer;->indexOf(Ljava/lang/String;Ljava/nio/charset/Charset;I)I

    move-result p1

    return p1
.end method

.method public indexOf(Ljava/lang/String;Ljava/nio/charset/Charset;I)I
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    invoke-virtual {p1, p2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-virtual {p0, p1, p3}, Lcom/jscape/util/ByteBuffer;->indexOf([BI)I

    move-result p1

    return p1
.end method

.method public indexOf([B)I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/jscape/util/ByteBuffer;->indexOf([BI)I

    move-result p1

    return p1
.end method

.method public declared-synchronized indexOf([BI)I
    .locals 6

    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v1

    const/4 v2, 0x0

    move v3, v2

    :cond_0
    iget v4, p0, Lcom/jscape/util/ByteBuffer;->bytes:I

    if-ge p2, v4, :cond_9

    iget-object v4, p0, Lcom/jscape/util/ByteBuffer;->buffer:[B

    aget-byte v4, v4, p2

    aget-byte v5, p1, v3

    if-eqz v1, :cond_a

    if-eqz v1, :cond_2

    if-ne v5, v4, :cond_1

    add-int/lit8 v3, v3, 0x1

    if-nez v1, :cond_5

    :cond_1
    move v5, v3

    :cond_2
    if-eqz v1, :cond_4

    if-eqz v5, :cond_3

    move v3, v2

    :cond_3
    aget-byte v5, p1, v3

    :cond_4
    if-eqz v1, :cond_6

    if-ne v5, v4, :cond_5

    add-int/lit8 v3, v3, 0x1

    :cond_5
    array-length v4, p1

    move v5, v3

    :cond_6
    if-eqz v1, :cond_7

    if-ne v5, v4, :cond_8

    array-length v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-int v5, p2, v0

    const/4 v4, 0x1

    :cond_7
    add-int v0, v5, v4

    if-nez v1, :cond_9

    :cond_8
    add-int/lit8 p2, p2, 0x1

    if-nez v1, :cond_0

    :cond_9
    move v5, v0

    :cond_a
    monitor-exit p0

    return v5

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public length()I
    .locals 1

    iget v0, p0, Lcom/jscape/util/ByteBuffer;->bytes:I

    return v0
.end method

.method public startsWith(Ljava/lang/String;Ljava/nio/charset/Charset;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    invoke-virtual {p0, p2}, Lcom/jscape/util/ByteBuffer;->toString(Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public toByteArray()[B
    .locals 4

    iget v0, p0, Lcom/jscape/util/ByteBuffer;->bytes:I

    new-array v1, v0, [B

    iget-object v2, p0, Lcom/jscape/util/ByteBuffer;->buffer:[B

    const/4 v3, 0x0

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/util/ByteBuffer;->buffer:[B

    iget v2, p0, Lcom/jscape/util/ByteBuffer;->bytes:I

    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v4, v2, v3}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    return-object v0
.end method

.method public toString(Ljava/nio/charset/Charset;)Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/util/ByteBuffer;->buffer:[B

    iget v2, p0, Lcom/jscape/util/ByteBuffer;->bytes:I

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2, p1}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    return-object v0
.end method
