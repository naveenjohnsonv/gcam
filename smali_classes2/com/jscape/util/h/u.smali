.class public Lcom/jscape/util/h/u;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lcom/jscape/util/Time;

.field private static final e:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<",
            "Lcom/jscape/util/h/w;",
            ">;"
        }
    .end annotation
.end field

.field private volatile c:Z

.field private d:Lcom/jscape/util/h/w;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "\u007f\u007flmx$BEx>ku&\u0011Io0"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/h/u;->e:Ljava/lang/String;

    const-wide/16 v0, 0xa

    invoke-static {v0, v1}, Lcom/jscape/util/Time;->millis(J)Lcom/jscape/util/Time;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/h/u;->a:Lcom/jscape/util/Time;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x2c

    goto :goto_1

    :cond_1
    const/4 v4, 0x7

    goto :goto_1

    :cond_2
    const/16 v4, 0x57

    goto :goto_1

    :cond_3
    const/16 v4, 0x46

    goto :goto_1

    :cond_4
    const/16 v4, 0x50

    goto :goto_1

    :cond_5
    const/16 v4, 0x45

    goto :goto_1

    :cond_6
    const/16 v4, 0x62

    :goto_1
    const/16 v5, 0x4e

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    const v0, 0x7fffffff

    invoke-direct {p0, v0}, Lcom/jscape/util/h/u;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/jscape/util/h/u;->b:Ljava/util/concurrent/BlockingQueue;

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method static a(Lcom/jscape/util/h/u;)V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/util/h/u;->d()V

    return-void
.end method

.method private b()V
    .locals 6

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/util/h/u;->c:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/jscape/util/h/u;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_4

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    move-object v2, p0

    goto :goto_2

    :cond_1
    move-object v2, p0

    :goto_0
    if-nez v1, :cond_4

    if-nez v0, :cond_2

    :try_start_2
    iget-boolean v1, v2, Lcom/jscape/util/h/u;->c:Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    if-nez v1, :cond_4

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/u;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_2
    :goto_1
    :try_start_3
    iget-object v1, v2, Lcom/jscape/util/h/u;->b:Ljava/util/concurrent/BlockingQueue;

    sget-object v3, Lcom/jscape/util/h/u;->a:Lcom/jscape/util/Time;

    iget-wide v3, v3, Lcom/jscape/util/Time;->value:J

    sget-object v5, Lcom/jscape/util/h/u;->a:Lcom/jscape/util/Time;

    iget-object v5, v5, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v3, v4, v5}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jscape/util/h/w;

    iput-object v1, v2, Lcom/jscape/util/h/u;->d:Lcom/jscape/util/h/w;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    :catch_1
    if-eqz v0, :cond_3

    goto :goto_3

    :cond_3
    :goto_2
    invoke-direct {v2}, Lcom/jscape/util/h/u;->c()Z

    move-result v1

    goto :goto_0

    :cond_4
    :goto_3
    return-void

    :catch_2
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/util/h/u;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/util/h/u;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/u;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method private c()Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/h/u;->d:Lcom/jscape/util/h/w;

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/jscape/util/h/w;->a()Z

    move-result v1

    if-nez v0, :cond_2

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method private d()V
    .locals 2

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-boolean v1, p0, Lcom/jscape/util/h/u;->c:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/jscape/util/h/u;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/jscape/util/h/u;->a:Lcom/jscape/util/Time;

    invoke-virtual {v1}, Lcom/jscape/util/Time;->sleepSafe()V

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a([BII)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/util/h/u;->b()V

    invoke-direct {p0}, Lcom/jscape/util/h/u;->c()Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    :try_start_1
    iget-boolean v1, p0, Lcom/jscape/util/h/u;->c:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    iget-object v0, p0, Lcom/jscape/util/h/u;->d:Lcom/jscape/util/h/w;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/util/h/w;->a([BII)I

    move-result v1

    :cond_1
    return v1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/util/h/u;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/util/h/u;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/util/h/u;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/u;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method

.method public a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jscape/util/h/u;->c:Z

    return-void
.end method

.method public b([BII)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    :try_start_0
    iget-boolean v2, p0, Lcom/jscape/util/h/u;->c:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v2, :cond_2

    if-nez v0, :cond_1

    if-nez v0, :cond_1

    if-nez v1, :cond_2

    :try_start_1
    iget-object v2, p0, Lcom/jscape/util/h/u;->b:Ljava/util/concurrent/BlockingQueue;

    new-instance v3, Lcom/jscape/util/h/w;

    const/4 v4, 0x0

    invoke-direct {v3, p1, p2, p3, v4}, Lcom/jscape/util/h/w;-><init>([BIILcom/jscape/util/h/v;)V

    sget-object v4, Lcom/jscape/util/h/u;->a:Lcom/jscape/util/Time;

    iget-wide v4, v4, Lcom/jscape/util/Time;->value:J

    sget-object v6, Lcom/jscape/util/h/u;->a:Lcom/jscape/util/Time;

    iget-object v6, v6, Lcom/jscape/util/Time;->unit:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v4, v5, v6}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    if-eqz v0, :cond_0

    goto :goto_1

    :cond_1
    move p1, v1

    goto :goto_2

    :cond_2
    :goto_1
    iget-boolean p1, p0, Lcom/jscape/util/h/u;->c:Z

    :goto_2
    if-nez v0, :cond_3

    if-eqz p1, :cond_4

    goto :goto_3

    :cond_3
    move v1, p1

    :goto_3
    if-eqz v1, :cond_5

    :cond_4
    return-void

    :cond_5
    :try_start_2
    new-instance p1, Ljava/io/IOException;

    sget-object p2, Lcom/jscape/util/h/u;->e:Ljava/lang/String;

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/u;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :catch_2
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/util/h/u;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/util/h/u;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/u;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
.end method
