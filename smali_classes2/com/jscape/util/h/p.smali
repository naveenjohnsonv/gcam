.class public Lcom/jscape/util/h/p;
.super Ljava/io/OutputStream;


# static fields
.field private static final e:[Ljava/lang/String;


# instance fields
.field private final a:Ljava/io/OutputStream;

.field private final b:Z

.field private final c:Lcom/jscape/util/h/G;

.field private d:J


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x1b

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x8

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "j\u000fr9\u0019\u000c_N/r#\u001d\u0010Ez\u0014u2\u000c\u0008\u0011R\u0002f$\u0008X\u0008\u0005@h \u0003\u0000C\u0014\u000f\u0005@p%\u0004\u0011EL\u000eE.\u0019\u0000B\u0014"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x34

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/h/p;->e:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v13, 0x2

    if-eq v12, v13, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x39

    goto :goto_2

    :cond_2
    const/16 v12, 0x6d

    goto :goto_2

    :cond_3
    const/16 v12, 0x65

    goto :goto_2

    :cond_4
    const/16 v12, 0x5f

    goto :goto_2

    :cond_5
    const/16 v12, 0xf

    goto :goto_2

    :cond_6
    const/16 v12, 0x68

    goto :goto_2

    :cond_7
    const/16 v12, 0x21

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/jscape/util/h/p;-><init>(Ljava/io/OutputStream;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Z)V
    .locals 1

    sget-object v0, Lcom/jscape/util/h/G;->a:Lcom/jscape/util/h/G;

    invoke-direct {p0, p1, p2, v0}, Lcom/jscape/util/h/p;-><init>(Ljava/io/OutputStream;ZLcom/jscape/util/h/G;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;ZLcom/jscape/util/h/G;)V
    .locals 0

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/h/p;->a:Ljava/io/OutputStream;

    iput-boolean p2, p0, Lcom/jscape/util/h/p;->b:Z

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/util/h/p;->c:Lcom/jscape/util/h/G;

    const-wide/16 p1, 0x0

    iput-wide p1, p0, Lcom/jscape/util/h/p;->d:J

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/jscape/util/h/p;->d:J

    return-wide v0
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/util/h/p;->b:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/jscape/util/h/p;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/p;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/p;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/jscape/util/h/p;->c:Lcom/jscape/util/h/G;

    iget-wide v1, p0, Lcom/jscape/util/h/p;->d:J

    invoke-interface {v0, p0, v1, v2}, Lcom/jscape/util/h/G;->b(Lcom/jscape/util/h/p;J)V

    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/p;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/h/p;->e:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/h/p;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/jscape/util/h/p;->b:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/jscape/util/h/p;->d:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/p;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    iget-wide v0, p0, Lcom/jscape/util/h/p;->d:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/jscape/util/h/p;->d:J

    iget-object p1, p0, Lcom/jscape/util/h/p;->c:Lcom/jscape/util/h/G;

    invoke-interface {p1, p0, v0, v1}, Lcom/jscape/util/h/G;->a(Lcom/jscape/util/h/p;J)V

    return-void
.end method

.method public write([BII)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/p;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    iget-wide p1, p0, Lcom/jscape/util/h/p;->d:J

    int-to-long v0, p3

    add-long/2addr p1, v0

    iput-wide p1, p0, Lcom/jscape/util/h/p;->d:J

    iget-object p3, p0, Lcom/jscape/util/h/p;->c:Lcom/jscape/util/h/G;

    invoke-interface {p3, p0, p1, p2}, Lcom/jscape/util/h/G;->a(Lcom/jscape/util/h/p;J)V

    return-void
.end method
