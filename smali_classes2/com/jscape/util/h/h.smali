.class public Lcom/jscape/util/h/h;
.super Ljava/io/InputStream;


# static fields
.field private static final a:[B

.field private static final b:[B

.field private static final c:[B

.field private static final i:Ljava/lang/String;


# instance fields
.field private final d:Ljava/io/InputStream;

.field private final e:[B

.field private final f:[B

.field private final g:Lcom/jscape/util/h/e;

.field private final h:Lcom/jscape/util/h/o;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const-string v0, "pl\u0015x_* W-4\u0017qk?Wc\u0016,Ue"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    const/4 v4, 0x2

    const/4 v5, 0x1

    if-gt v1, v3, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/h/h;->i:Ljava/lang/String;

    new-array v0, v4, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jscape/util/h/h;->a:[B

    new-array v0, v5, [B

    const/16 v1, 0xa

    aput-byte v1, v0, v2

    sput-object v0, Lcom/jscape/util/h/h;->b:[B

    new-array v0, v5, [B

    const/16 v1, 0xd

    aput-byte v1, v0, v2

    sput-object v0, Lcom/jscape/util/h/h;->c:[B

    return-void

    :cond_0
    aget-char v6, v0, v3

    rem-int/lit8 v7, v3, 0x7

    if-eqz v7, :cond_6

    if-eq v7, v5, :cond_5

    if-eq v7, v4, :cond_4

    const/4 v4, 0x3

    if-eq v7, v4, :cond_3

    const/4 v4, 0x4

    if-eq v7, v4, :cond_2

    const/4 v4, 0x5

    if-eq v7, v4, :cond_1

    const/16 v4, 0x41

    goto :goto_1

    :cond_1
    const/16 v4, 0x59

    goto :goto_1

    :cond_2
    const/16 v4, 0x2f

    goto :goto_1

    :cond_3
    const/16 v4, 0x4a

    goto :goto_1

    :cond_4
    const/16 v4, 0x63

    goto :goto_1

    :cond_5
    const/16 v4, 0x1f

    goto :goto_1

    :cond_6
    const/16 v4, 0x20

    :goto_1
    const/16 v5, 0x12

    xor-int/2addr v4, v5

    xor-int/2addr v4, v6

    int-to-char v4, v4

    aput-char v4, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    nop

    :array_0
    .array-data 1
        0xdt
        0xat
    .end array-data
.end method

.method public constructor <init>(Ljava/io/InputStream;[B[B)V
    .locals 5

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/h/h;->d:Ljava/io/InputStream;

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object p1

    array-length v0, p2

    int-to-long v0, v0

    sget-object v2, Lcom/jscape/util/h/h;->i:Ljava/lang/String;

    const-wide/16 v3, 0x0

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput-object p2, p0, Lcom/jscape/util/h/h;->e:[B

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/util/h/h;->f:[B

    new-instance p2, Lcom/jscape/util/h/e;

    const/4 p3, 0x0

    new-array p3, p3, [B

    invoke-direct {p2, p3}, Lcom/jscape/util/h/e;-><init>([B)V

    iput-object p2, p0, Lcom/jscape/util/h/h;->g:Lcom/jscape/util/h/e;

    new-instance p2, Lcom/jscape/util/h/o;

    iget-object p3, p0, Lcom/jscape/util/h/h;->e:[B

    array-length p3, p3

    invoke-direct {p2, p3}, Lcom/jscape/util/h/o;-><init>(I)V

    iput-object p2, p0, Lcom/jscape/util/h/h;->h:Lcom/jscape/util/h/o;

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-void
.end method

.method public static a(Ljava/io/InputStream;[B)Lcom/jscape/util/h/h;
    .locals 5

    new-instance v0, Lcom/jscape/util/h/h;

    new-instance v1, Lcom/jscape/util/h/h;

    new-instance v2, Lcom/jscape/util/h/h;

    sget-object v3, Lcom/jscape/util/h/h;->a:[B

    sget-object v4, Lcom/jscape/util/h/h;->b:[B

    invoke-direct {v2, p0, v3, v4}, Lcom/jscape/util/h/h;-><init>(Ljava/io/InputStream;[B[B)V

    sget-object p0, Lcom/jscape/util/h/h;->c:[B

    sget-object v3, Lcom/jscape/util/h/h;->b:[B

    invoke-direct {v1, v2, p0, v3}, Lcom/jscape/util/h/h;-><init>(Ljava/io/InputStream;[B[B)V

    sget-object p0, Lcom/jscape/util/h/h;->b:[B

    invoke-direct {v0, v1, p0, p1}, Lcom/jscape/util/h/h;-><init>(Ljava/io/InputStream;[B[B)V

    return-object v0
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a(I)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/h;->h:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->c()Lcom/jscape/util/h/o;

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/h/h;->h:Lcom/jscape/util/h/o;

    invoke-virtual {v1, p1}, Lcom/jscape/util/h/o;->write(I)V

    const/4 v1, 0x1

    :cond_0
    iget-object v2, p0, Lcom/jscape/util/h/h;->e:[B

    array-length v2, v2

    const/4 v3, -0x1

    const/4 v4, 0x0

    if-ge v1, v2, :cond_3

    iget-object p1, p0, Lcom/jscape/util/h/h;->d:Ljava/io/InputStream;

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result p1

    if-nez v0, :cond_3

    if-nez v0, :cond_3

    if-eq p1, v3, :cond_3

    :try_start_0
    iget-object v2, p0, Lcom/jscape/util/h/h;->h:Lcom/jscape/util/h/o;

    invoke-virtual {v2, p1}, Lcom/jscape/util/h/o;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_2

    :try_start_1
    iget-object v2, p0, Lcom/jscape/util/h/h;->e:[B

    aget-byte v2, v2, v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eq p1, v2, :cond_1

    :try_start_2
    iget-object p1, p0, Lcom/jscape/util/h/h;->g:Lcom/jscape/util/h/e;

    iget-object v0, p0, Lcom/jscape/util/h/h;->h:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/h/h;->h:Lcom/jscape/util/h/o;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->b()I

    move-result v1

    invoke-virtual {p1, v0, v4, v1}, Lcom/jscape/util/h/e;->a([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    :cond_2
    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/util/h/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/util/h/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_3
    :goto_0
    if-eq p1, v3, :cond_4

    :try_start_5
    iget-object p1, p0, Lcom/jscape/util/h/h;->g:Lcom/jscape/util/h/e;

    iget-object v1, p0, Lcom/jscape/util/h/h;->f:[B

    iget-object v2, p0, Lcom/jscape/util/h/h;->f:[B

    array-length v2, v2

    invoke-virtual {p1, v1, v4, v2}, Lcom/jscape/util/h/e;->a([BII)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    if-eqz v0, :cond_5

    goto :goto_1

    :catch_3
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/util/h/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_4
    :goto_1
    iget-object p1, p0, Lcom/jscape/util/h/h;->g:Lcom/jscape/util/h/e;

    iget-object v0, p0, Lcom/jscape/util/h/h;->h:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/h/h;->h:Lcom/jscape/util/h/o;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->b()I

    move-result v1

    invoke-virtual {p1, v0, v4, v1}, Lcom/jscape/util/h/e;->a([BII)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    :cond_5
    return-void

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/h;->d:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void
.end method

.method public read()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/h/h;->g:Lcom/jscape/util/h/e;

    invoke-virtual {v1}, Lcom/jscape/util/h/e;->read()I

    move-result v1

    const/4 v2, -0x1

    if-nez v0, :cond_1

    if-eq v1, v2, :cond_0

    return v1

    :cond_0
    iget-object v1, p0, Lcom/jscape/util/h/h;->d:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    if-nez v0, :cond_3

    :cond_1
    if-eq v1, v2, :cond_3

    if-nez v0, :cond_3

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/h/h;->e:[B

    const/4 v2, 0x0

    aget-byte v0, v0, v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v1, v0, :cond_2

    goto :goto_0

    :cond_2
    invoke-direct {p0, v1}, Lcom/jscape/util/h/h;->a(I)V

    iget-object v0, p0, Lcom/jscape/util/h/h;->g:Lcom/jscape/util/h/e;

    invoke-virtual {v0}, Lcom/jscape/util/h/e;->read()I

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/jscape/util/h/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_3
    :goto_0
    return v1
.end method
