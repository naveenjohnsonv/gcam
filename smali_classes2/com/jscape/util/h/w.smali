.class public Lcom/jscape/util/h/w;
.super Ljava/lang/Object;


# static fields
.field private static final c:[Ljava/lang/String;


# instance fields
.field public final a:[B

.field public b:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x13

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x34

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "mS\u001f]n-6L\\\u001fN9a(FZ\u001fG*\t\u0004\u001d\u0004Iq~(\\\u0000"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x1d

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/h/w;->c:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x79

    goto :goto_2

    :cond_2
    const/16 v12, 0x39

    goto :goto_2

    :cond_3
    const/16 v12, 0x23

    goto :goto_2

    :cond_4
    const/16 v12, 0x1b

    goto :goto_2

    :cond_5
    const/16 v12, 0x5f

    goto :goto_2

    :cond_6
    const/16 v12, 0x9

    goto :goto_2

    :cond_7
    const/16 v12, 0x1c

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method private constructor <init>([BII)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array p3, p3, [B

    invoke-static {p1, p2, p3}, Lcom/jscape/util/v;->a([BI[B)[B

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/util/h/w;->a:[B

    const/4 p1, 0x0

    iput p1, p0, Lcom/jscape/util/h/w;->b:I

    return-void
.end method

.method constructor <init>([BIILcom/jscape/util/h/v;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/jscape/util/h/w;-><init>([BII)V

    return-void
.end method


# virtual methods
.method public a([BII)I
    .locals 2

    iget-object v0, p0, Lcom/jscape/util/h/w;->a:[B

    array-length v0, v0

    iget v1, p0, Lcom/jscape/util/h/w;->b:I

    sub-int/2addr v0, v1

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result p3

    iget-object v0, p0, Lcom/jscape/util/h/w;->a:[B

    iget v1, p0, Lcom/jscape/util/h/w;->b:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p1, p0, Lcom/jscape/util/h/w;->b:I

    add-int/2addr p1, p3

    iput p1, p0, Lcom/jscape/util/h/w;->b:I

    return p3
.end method

.method public a()Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/util/h/w;->b:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jscape/util/h/w;->a:[B

    array-length v0, v0

    if-lt v1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/h/w;->c:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/h/w;->a:[B

    array-length v2, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/util/h/w;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
