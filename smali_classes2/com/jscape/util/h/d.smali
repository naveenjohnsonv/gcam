.class public Lcom/jscape/util/h/d;
.super Ljava/io/InputStream;


# instance fields
.field private final a:Lcom/jscape/util/h/u;

.field private b:[B


# direct methods
.method public constructor <init>(Lcom/jscape/util/h/u;)V
    .locals 0

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/h/d;->a:Lcom/jscape/util/h/u;

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/h/d;->a:Lcom/jscape/util/h/u;

    invoke-virtual {v0}, Lcom/jscape/util/h/u;->a()V

    return-void
.end method

.method public read()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/jscape/util/h/d;->b:[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v2, :cond_0

    :try_start_1
    new-array v2, v1, [B

    iput-object v2, p0, Lcom/jscape/util/h/d;->b:[B

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/d;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/d;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    :goto_0
    move-object v2, p0

    :cond_1
    iget-object v3, v2, Lcom/jscape/util/h/d;->b:[B

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v1}, Lcom/jscape/util/h/d;->read([BII)I

    move-result v3

    :cond_2
    if-eqz v3, :cond_1

    if-nez v0, :cond_2

    const/4 v1, -0x1

    if-nez v0, :cond_3

    if-eq v3, v1, :cond_4

    :try_start_2
    iget-object v0, v2, Lcom/jscape/util/h/d;->b:[B

    aget-byte v3, v0, v4
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    const/16 v1, 0xff

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/d;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_3
    :goto_1
    and-int/2addr v1, v3

    :cond_4
    return v1
.end method

.method public read([BII)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/d;->a:Lcom/jscape/util/h/u;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/util/h/u;->a([BII)I

    move-result p1

    return p1
.end method
