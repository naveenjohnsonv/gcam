.class public Lcom/jscape/util/h/B;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/V;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/V<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/util/h/L;

.field private final b:Lcom/jscape/util/h/N;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/N<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/jscape/util/h/L;Lcom/jscape/util/h/N;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/h/L;",
            "Lcom/jscape/util/h/N<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/h/B;->a:Lcom/jscape/util/h/L;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/h/B;->b:Lcom/jscape/util/h/N;

    return-void
.end method

.method private static a(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/B;->a:Lcom/jscape/util/h/L;

    invoke-interface {v0}, Lcom/jscape/util/h/L;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    iget-object v2, p0, Lcom/jscape/util/h/B;->b:Lcom/jscape/util/h/N;

    invoke-interface {v2, p1, v0}, Lcom/jscape/util/h/N;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/B;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    return-void

    :catchall_1
    move-exception p1

    :try_start_2
    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :catchall_2
    move-exception v2

    if-eqz v0, :cond_1

    :try_start_3
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v3

    :try_start_4
    invoke-virtual {p1, v3}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    goto :goto_1

    :catchall_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/B;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    throw p1

    :cond_1
    :goto_1
    throw v2
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/h/B;->a:Lcom/jscape/util/h/L;

    invoke-interface {v0}, Lcom/jscape/util/h/L;->writeAllowed()Z

    move-result v0

    return v0
.end method
