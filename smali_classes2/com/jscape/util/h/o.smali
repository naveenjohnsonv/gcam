.class public Lcom/jscape/util/h/o;
.super Ljava/io/OutputStream;


# static fields
.field public static final a:I = 0x80

.field private static d:Ljava/lang/String;

.field private static final e:[Ljava/lang/String;


# instance fields
.field private b:[B

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/jscape/util/h/o;->b(Ljava/lang/String;)V

    const/4 v5, 0x0

    const-string v6, "\u0001GV\u000c\u000f*K*R[C\u0011k%\u0001_FI>7J\"_}Y\u000b5M7uF^\u001a$Uc]PY\u0019#]1\u0008^I\u0011\"L+\u001b"

    move v9, v5

    const/4 v7, -0x1

    const/16 v8, 0xd

    const/16 v10, 0x33

    :goto_0
    const/16 v11, 0x27

    const/4 v12, 0x1

    add-int/2addr v7, v12

    add-int v13, v7, v8

    invoke-virtual {v6, v7, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    const/4 v14, -0x1

    :goto_1
    invoke-virtual {v13}, Ljava/lang/String;->toCharArray()[C

    move-result-object v13

    array-length v15, v13

    move v2, v5

    :goto_2
    if-gt v15, v2, :cond_3

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v13}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v11, v9, 0x1

    if-eqz v14, :cond_1

    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v10, :cond_0

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v11

    goto :goto_0

    :cond_0
    const/16 v2, 0x19

    const-string v6, "\u0015SB\u0018\u001b>_>FOW\u0005\u007f\u000b{\u0012VW\u00188X>]H\u0005"

    move v10, v2

    move v9, v11

    const/4 v7, -0x1

    const/16 v8, 0xd

    goto :goto_3

    :cond_1
    aput-object v2, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v10, :cond_2

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    move v8, v2

    move v9, v11

    :goto_3
    add-int/2addr v7, v12

    add-int v2, v7, v8

    invoke-virtual {v6, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    move v14, v5

    const/16 v11, 0x33

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/h/o;->e:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v13, v2

    rem-int/lit8 v3, v2, 0x7

    if-eqz v3, :cond_9

    if-eq v3, v12, :cond_8

    const/4 v4, 0x2

    if-eq v3, v4, :cond_7

    const/4 v4, 0x3

    if-eq v3, v4, :cond_6

    if-eq v3, v0, :cond_5

    const/4 v4, 0x5

    if-eq v3, v4, :cond_4

    const/16 v3, 0x1f

    goto :goto_4

    :cond_4
    const/16 v3, 0x62

    goto :goto_4

    :cond_5
    const/16 v3, 0x58

    goto :goto_4

    :cond_6
    const/16 v3, 0xb

    goto :goto_4

    :cond_7
    const/16 v3, 0x15

    goto :goto_4

    :cond_8
    move v3, v12

    goto :goto_4

    :cond_9
    const/16 v3, 0x64

    :goto_4
    xor-int/2addr v3, v11

    xor-int v3, v16, v3

    int-to-char v3, v3

    aput-char v3, v13, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x80

    invoke-direct {p0, v0}, Lcom/jscape/util/h/o;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    new-array p1, p1, [B

    invoke-direct {p0, p1}, Lcom/jscape/util/h/o;-><init>([B)V

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/jscape/util/h/o;-><init>([BI)V

    return-void
.end method

.method public constructor <init>([BI)V
    .locals 7

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/h/o;->b:[B

    int-to-long v0, p2

    array-length p1, p1

    int-to-long v4, p1

    sget-object p1, Lcom/jscape/util/h/o;->e:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v6, p1, v2

    const-wide/16 v2, 0x0

    invoke-static/range {v0 .. v6}, Lcom/jscape/util/aq;->a(JJJLjava/lang/String;)V

    iput p2, p0, Lcom/jscape/util/h/o;->c:I

    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/jscape/util/h/o;->d:Ljava/lang/String;

    return-void
.end method

.method private c(I)V
    .locals 3

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/h/o;->b:[B

    array-length v2, v1

    if-nez v0, :cond_0

    if-le p1, v2, :cond_1

    array-length v0, v1

    mul-int/lit8 v0, v0, 0x2

    move v2, p1

    move p1, v0

    :cond_0
    invoke-static {p1, v2}, Ljava/lang/Math;->max(II)I

    move-result p1

    iget-object v0, p0, Lcom/jscape/util/h/o;->b:[B

    new-array p1, p1, [B

    iget v1, p0, Lcom/jscape/util/h/o;->c:I

    const/4 v2, 0x0

    invoke-static {v0, v2, p1, v2, v1}, Lcom/jscape/util/v;->a([BI[BII)[B

    move-result-object p1

    iput-object p1, p0, Lcom/jscape/util/h/o;->b:[B

    :cond_1
    return-void
.end method

.method public static f()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jscape/util/h/o;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/jscape/util/h/o;
    .locals 5

    int-to-long v0, p1

    sget-object v2, Lcom/jscape/util/h/o;->e:[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const-wide/16 v3, 0x0

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/aq;->b(JJLjava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/jscape/util/h/o;->c(I)V

    iput p1, p0, Lcom/jscape/util/h/o;->c:I

    return-object p0
.end method

.method public a([BI)Lcom/jscape/util/h/o;
    .locals 0

    iput-object p1, p0, Lcom/jscape/util/h/o;->b:[B

    iput p2, p0, Lcom/jscape/util/h/o;->c:I

    return-object p0
.end method

.method public a(Ljava/nio/charset/Charset;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/util/h/o;->b:[B

    iget v2, p0, Lcom/jscape/util/h/o;->c:I

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2, p1}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    return-object v0
.end method

.method public a(Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/o;->b:[B

    iget v1, p0, Lcom/jscape/util/h/o;->c:I

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    return-void
.end method

.method public a()[B
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/h/o;->b:[B

    return-object v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/jscape/util/h/o;->c:I

    return v0
.end method

.method public b(I)Lcom/jscape/util/h/o;
    .locals 3

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    return-object p0

    :cond_0
    iget v1, p0, Lcom/jscape/util/h/o;->c:I

    sub-int/2addr v1, p1

    goto :goto_0

    :cond_1
    move v1, p1

    :goto_0
    if-nez v0, :cond_3

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/jscape/util/h/o;->c()Lcom/jscape/util/h/o;

    if-eqz v0, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/jscape/util/h/o;->b:[B

    const/4 v2, 0x0

    invoke-static {v0, p1, v0, v2, v1}, Lcom/jscape/util/v;->a([BI[BII)[B

    :cond_3
    iput v1, p0, Lcom/jscape/util/h/o;->c:I

    :cond_4
    return-object p0
.end method

.method public c()Lcom/jscape/util/h/o;
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/util/h/o;->c:I

    return-object p0
.end method

.method public close()V
    .locals 0

    return-void
.end method

.method public d()[B
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/h/o;->b:[B

    iget v1, p0, Lcom/jscape/util/h/o;->c:I

    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Lcom/jscape/util/v;->a([BII)[B

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/util/h/o;->c:I

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/h/o;->e:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/h/o;->b:[B

    array-length v2, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/util/h/o;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(I)V
    .locals 3

    iget v0, p0, Lcom/jscape/util/h/o;->c:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/jscape/util/h/o;->c(I)V

    iget-object v1, p0, Lcom/jscape/util/h/o;->b:[B

    iget v2, p0, Lcom/jscape/util/h/o;->c:I

    int-to-byte p1, p1

    aput-byte p1, v1, v2

    iput v0, p0, Lcom/jscape/util/h/o;->c:I

    return-void
.end method

.method public write([BII)V
    .locals 3

    iget v0, p0, Lcom/jscape/util/h/o;->c:I

    add-int/2addr v0, p3

    invoke-direct {p0, v0}, Lcom/jscape/util/h/o;->c(I)V

    iget-object v1, p0, Lcom/jscape/util/h/o;->b:[B

    iget v2, p0, Lcom/jscape/util/h/o;->c:I

    invoke-static {p1, p2, v1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput v0, p0, Lcom/jscape/util/h/o;->c:I

    return-void
.end method
