.class public Lcom/jscape/util/h/g;
.super Ljava/io/InputStream;


# static fields
.field private static final a:I = 0x200

.field private static final b:I = 0x1

.field private static final c:I = -0x1


# instance fields
.field private final d:Ljava/io/InputStream;

.field private final e:Ljava/util/zip/Deflater;

.field private final f:[B

.field private g:[B

.field private h:Z


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    new-instance v0, Ljava/util/zip/Deflater;

    invoke-direct {v0}, Ljava/util/zip/Deflater;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/jscape/util/h/g;-><init>(Ljava/io/InputStream;Ljava/util/zip/Deflater;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/util/zip/Deflater;)V
    .locals 1

    const/16 v0, 0x200

    invoke-direct {p0, p1, p2, v0}, Lcom/jscape/util/h/g;-><init>(Ljava/io/InputStream;Ljava/util/zip/Deflater;I)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/util/zip/Deflater;I)V
    .locals 0

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/h/g;->d:Ljava/io/InputStream;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/h/g;->e:Ljava/util/zip/Deflater;

    new-array p1, p3, [B

    iput-object p1, p0, Lcom/jscape/util/h/g;->f:[B

    const/4 p1, 0x1

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/jscape/util/h/g;->g:[B

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/h/g;->e:Ljava/util/zip/Deflater;

    invoke-virtual {v1}, Ljava/util/zip/Deflater;->needsInput()Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    const/4 v2, 0x0

    if-nez v0, :cond_0

    if-eqz v1, :cond_2

    :try_start_1
    iget-object v1, p0, Lcom/jscape/util/h/g;->d:Ljava/io/InputStream;

    iget-object v3, p0, Lcom/jscape/util/h/g;->f:[B

    iget-object v4, p0, Lcom/jscape/util/h/g;->f:[B

    array-length v4, v4

    invoke-virtual {v1, v3, v2, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    :cond_0
    if-nez v0, :cond_1

    if-lez v1, :cond_1

    :try_start_2
    iget-object v3, p0, Lcom/jscape/util/h/g;->e:Ljava/util/zip/Deflater;

    iget-object v4, p0, Lcom/jscape/util/h/g;->f:[B

    invoke-virtual {v3, v4, v2, v1}, Ljava/util/zip/Deflater;->setInput([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    if-eqz v0, :cond_2

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/util/h/g;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/g;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    if-gez v1, :cond_2

    :try_start_4
    iget-object v0, p0, Lcom/jscape/util/h/g;->e:Ljava/util/zip/Deflater;

    invoke-virtual {v0}, Ljava/util/zip/Deflater;->finish()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/g;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_2
    :goto_1
    return-void

    :catch_3
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/util/h/g;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/g;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public available()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-boolean v1, p0, Lcom/jscape/util/h/g;->h:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/g;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jscape/util/h/g;->e:Ljava/util/zip/Deflater;

    invoke-virtual {v0}, Ljava/util/zip/Deflater;->end()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/jscape/util/h/g;->d:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/util/h/g;->d:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    throw v0
.end method

.method public read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/h/g;->g:[B

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lcom/jscape/util/h/g;->read([BII)I

    move-result v1

    const/4 v3, -0x1

    if-nez v0, :cond_1

    if-ne v1, v3, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jscape/util/h/g;->g:[B

    aget-byte v1, v0, v2

    const/16 v3, 0xff

    :cond_1
    and-int/2addr v3, v1

    :goto_0
    return v3
.end method

.method public read([BII)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    if-lez p3, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/jscape/util/h/g;->e:Ljava/util/zip/Deflater;

    invoke-virtual {v2}, Ljava/util/zip/Deflater;->finished()Z

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_2

    if-nez v0, :cond_2

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/jscape/util/h/g;->a()V

    iget-object v2, p0, Lcom/jscape/util/h/g;->e:Ljava/util/zip/Deflater;

    invoke-virtual {v2, p1, p2, p3}, Ljava/util/zip/Deflater;->deflate([BII)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr p2, v2

    sub-int/2addr p3, v2

    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-static {p1}, Lcom/jscape/util/h/g;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/g;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    move v2, v1

    :cond_2
    if-nez v0, :cond_4

    if-nez v2, :cond_3

    :try_start_2
    iget-object p1, p0, Lcom/jscape/util/h/g;->e:Ljava/util/zip/Deflater;

    invoke-virtual {p1}, Ljava/util/zip/Deflater;->finished()Z

    move-result v2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    if-nez v0, :cond_4

    if-eqz v2, :cond_3

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/jscape/util/h/g;->h:Z

    const/4 v1, -0x1

    goto :goto_1

    :catch_2
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/util/h/g;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/g;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    move v2, v1

    :cond_4
    return v2
.end method

.method public skip(J)J
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/h/g;->g:[B

    array-length v1, v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    const/16 v2, 0x200

    if-ge v1, v2, :cond_0

    :try_start_1
    new-array v1, v2, [B

    iput-object v1, p0, Lcom/jscape/util/h/g;->g:[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    const-wide/32 v1, 0x7fffffff

    invoke-static {p1, p2, v1, v2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    long-to-int v1, p1

    :cond_1
    const-wide/16 p1, 0x0

    :cond_2
    if-lez v1, :cond_5

    iget-object v2, p0, Lcom/jscape/util/h/g;->g:[B

    const/4 v3, 0x0

    array-length v4, v2

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-virtual {p0, v2, v3, v4}, Lcom/jscape/util/h/g;->read([BII)I

    move-result v2

    if-nez v0, :cond_4

    if-gez v2, :cond_3

    goto :goto_1

    :cond_3
    int-to-long v3, v2

    add-long/2addr p1, v3

    sub-int/2addr v1, v2

    goto :goto_0

    :cond_4
    move v1, v2

    :goto_0
    if-eqz v0, :cond_2

    :cond_5
    :goto_1
    return-wide p1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/util/h/g;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/g;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method
