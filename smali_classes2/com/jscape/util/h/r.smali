.class public Lcom/jscape/util/h/r;
.super Ljava/io/OutputStream;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field protected final a:Ljava/io/RandomAccessFile;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "\"8"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/h/r;->b:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x55

    goto :goto_1

    :cond_1
    const/16 v4, 0x34

    goto :goto_1

    :cond_2
    const/16 v4, 0x15

    goto :goto_1

    :cond_3
    const/16 v4, 0x35

    goto :goto_1

    :cond_4
    const/16 v4, 0xd

    goto :goto_1

    :cond_5
    const/16 v4, 0x72

    goto :goto_1

    :cond_6
    const/16 v4, 0x6d

    :goto_1
    const/16 v5, 0x3d

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Ljava/io/RandomAccessFile;)V
    .locals 0

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/h/r;->a:Ljava/io/RandomAccessFile;

    return-void
.end method

.method public static a(Ljava/io/File;JZ)Lcom/jscape/util/h/r;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/RandomAccessFile;

    sget-object v2, Lcom/jscape/util/h/r;->b:Ljava/lang/String;

    invoke-direct {v1, p0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    if-nez v0, :cond_0

    :try_start_0
    invoke-virtual {v1, p1, p2}, Ljava/io/RandomAccessFile;->seek(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p3, :cond_1

    goto :goto_0

    :catch_0
    move-exception p0

    :try_start_1
    invoke-static {p0}, Lcom/jscape/util/h/r;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p0

    throw p0

    :cond_0
    :goto_0
    invoke-virtual {v1, p1, p2}, Ljava/io/RandomAccessFile;->setLength(J)V

    :cond_1
    new-instance p0, Lcom/jscape/util/h/r;

    invoke-direct {p0, v1}, Lcom/jscape/util/h/r;-><init>(Ljava/io/RandomAccessFile;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    return-object p0

    :catch_1
    move-exception p0

    invoke-static {v1}, Lcom/jscape/util/X;->a(Ljava/io/RandomAccessFile;)V

    invoke-static {p0}, Lcom/jscape/util/L;->b(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method

.method public static a(Ljava/lang/String;JZ)Lcom/jscape/util/h/r;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p0, v0}, Ljava/nio/file/Paths;->get(Ljava/lang/String;[Ljava/lang/String;)Ljava/nio/file/Path;

    move-result-object p0

    invoke-static {p0, p1, p2, p3}, Lcom/jscape/util/h/r;->a(Ljava/nio/file/Path;JZ)Lcom/jscape/util/h/r;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/nio/file/Path;JZ)Lcom/jscape/util/h/r;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p0}, Ljava/nio/file/Path;->toFile()Ljava/io/File;

    move-result-object p0

    invoke-static {p0, p1, p2, p3}, Lcom/jscape/util/h/r;->a(Ljava/io/File;JZ)Lcom/jscape/util/h/r;

    move-result-object p0

    return-object p0
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/h/r;->a:Ljava/io/RandomAccessFile;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/RandomAccessFile;)V

    return-void
.end method

.method public write(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/r;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p1}, Ljava/io/RandomAccessFile;->write(I)V

    return-void
.end method

.method public write([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/r;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/RandomAccessFile;->write([BII)V

    return-void
.end method
