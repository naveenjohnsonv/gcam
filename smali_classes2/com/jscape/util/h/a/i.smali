.class public Lcom/jscape/util/h/a/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/util/K;


# direct methods
.method public constructor <init>(Lcom/jscape/util/K;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/h/a/i;->a:Lcom/jscape/util/K;

    return-void
.end method

.method public static a(Lcom/jscape/util/K;Ljava/io/InputStream;)J
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x8

    new-array v0, v0, [B

    invoke-static {v0, p1}, Lcom/jscape/util/X;->a([BLjava/io/InputStream;)[B

    move-result-object p1

    const/4 v0, 0x0

    invoke-interface {p0, p1, v0}, Lcom/jscape/util/K;->e([BI)J

    move-result-wide p0

    return-wide p0
.end method

.method public static a(Ljava/io/InputStream;)J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/util/y;->a:Lcom/jscape/util/K;

    invoke-static {v0, p0}, Lcom/jscape/util/h/a/i;->a(Lcom/jscape/util/K;Ljava/io/InputStream;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(JLcom/jscape/util/K;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x8

    new-array v0, v0, [B

    const/4 v1, 0x0

    invoke-interface {p2, p0, p1, v0, v1}, Lcom/jscape/util/K;->a(J[BI)[B

    move-result-object p0

    invoke-virtual {p3, p0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public static a(JLjava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/util/y;->a:Lcom/jscape/util/K;

    invoke-static {p0, p1, v0, p2}, Lcom/jscape/util/h/a/i;->a(JLcom/jscape/util/K;Ljava/io/OutputStream;)V

    return-void
.end method

.method public static b(Ljava/io/InputStream;)J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/util/ac;->a:Lcom/jscape/util/K;

    invoke-static {v0, p0}, Lcom/jscape/util/h/a/i;->a(Lcom/jscape/util/K;Ljava/io/InputStream;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static b(JLjava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/jscape/util/ac;->a:Lcom/jscape/util/K;

    invoke-static {p0, p1, v0, p2}, Lcom/jscape/util/h/a/i;->a(JLcom/jscape/util/K;Ljava/io/OutputStream;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Long;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object p1, p0, Lcom/jscape/util/h/a/i;->a:Lcom/jscape/util/K;

    invoke-static {v0, v1, p1, p2}, Lcom/jscape/util/h/a/i;->a(JLcom/jscape/util/K;Ljava/io/OutputStream;)V

    return-void
.end method

.method public c(Ljava/io/InputStream;)Ljava/lang/Long;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/a/i;->a:Lcom/jscape/util/K;

    invoke-static {v0, p1}, Lcom/jscape/util/h/a/i;->a(Lcom/jscape/util/K;Ljava/io/InputStream;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/util/h/a/i;->c(Ljava/io/InputStream;)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/util/h/a/i;->a(Ljava/lang/Long;Ljava/io/OutputStream;)V

    return-void
.end method
