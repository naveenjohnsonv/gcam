.class public Lcom/jscape/util/h/a/j;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Ljava/util/Map<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/nio/charset/Charset;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/jscape/util/h/a/n;

.field private final d:Lcom/jscape/util/h/a/k;

.field private final e:Lcom/jscape/util/h/a/l;


# direct methods
.method public constructor <init>(Ljava/nio/charset/Charset;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/util/h/a/f;->b()I

    move-result v0

    iput-object p1, p0, Lcom/jscape/util/h/a/j;->a:Ljava/nio/charset/Charset;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/h/a/j;->b:Ljava/lang/String;

    new-instance p1, Lcom/jscape/util/h/a/n;

    invoke-direct {p1, p0}, Lcom/jscape/util/h/a/n;-><init>(Lcom/jscape/util/h/a/j;)V

    iput-object p1, p0, Lcom/jscape/util/h/a/j;->c:Lcom/jscape/util/h/a/n;

    new-instance p1, Lcom/jscape/util/h/a/k;

    invoke-direct {p1, p0}, Lcom/jscape/util/h/a/k;-><init>(Lcom/jscape/util/h/a/j;)V

    iput-object p1, p0, Lcom/jscape/util/h/a/j;->d:Lcom/jscape/util/h/a/k;

    new-instance p1, Lcom/jscape/util/h/a/l;

    invoke-direct {p1}, Lcom/jscape/util/h/a/l;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/h/a/j;->e:Lcom/jscape/util/h/a/l;

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/jscape/util/h/a/f;->b(I)V

    :cond_0
    return-void
.end method

.method static a(Lcom/jscape/util/h/a/j;)Lcom/jscape/util/h/a/k;
    .locals 0

    iget-object p0, p0, Lcom/jscape/util/h/a/j;->d:Lcom/jscape/util/h/a/k;

    return-object p0
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private a(Ljava/io/Reader;Ljava/lang/StringBuffer;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/a/f;->b()I

    move-result v0

    new-instance v1, Lcom/jscape/util/h/a/n;

    invoke-direct {v1, p0}, Lcom/jscape/util/h/a/n;-><init>(Lcom/jscape/util/h/a/j;)V

    :cond_0
    invoke-virtual {p1}, Ljava/io/Reader;->read()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    int-to-char v4, v2

    invoke-interface {v1, v4, p2}, Lcom/jscape/util/h/a/m;->a(CLjava/lang/StringBuffer;)Lcom/jscape/util/h/a/m;

    move-result-object v1

    invoke-interface {v1}, Lcom/jscape/util/h/a/m;->a()Z

    move-result v4

    if-nez v4, :cond_0

    :cond_1
    if-eqz v0, :cond_2

    if-ne v2, v3, :cond_3

    if-eqz v0, :cond_3

    :try_start_0
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->length()I

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/a/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    if-nez v2, :cond_3

    const/4 p1, 0x0

    return-object p1

    :cond_3
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->setLength(I)V

    return-object p1
.end method

.method static b(Lcom/jscape/util/h/a/j;)Lcom/jscape/util/h/a/l;
    .locals 0

    iget-object p0, p0, Lcom/jscape/util/h/a/j;->e:Lcom/jscape/util/h/a/l;

    return-object p0
.end method

.method static c(Lcom/jscape/util/h/a/j;)Lcom/jscape/util/h/a/n;
    .locals 0

    iget-object p0, p0, Lcom/jscape/util/h/a/j;->c:Lcom/jscape/util/h/a/n;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/InputStreamReader;

    iget-object v1, p0, Lcom/jscape/util/h/a/j;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, p1, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-static {}, Lcom/jscape/util/h/a/f;->b()I

    move-result p1

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    :cond_0
    :goto_0
    invoke-direct {p0, v0, v2}, Lcom/jscape/util/h/a/j;->a(Ljava/io/Reader;Ljava/lang/StringBuffer;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    :try_start_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    if-eqz p1, :cond_2

    if-nez v4, :cond_1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/jscape/util/h/a/j;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    :cond_2
    const-string v5, ""

    if-eqz p1, :cond_4

    const/4 v6, -0x1

    if-eq v4, v6, :cond_3

    if-eqz p1, :cond_5

    :try_start_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v6, v6, -0x1

    if-ne v4, v6, :cond_5

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/util/h/a/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_3
    :goto_1
    invoke-interface {v1, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/a/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_4
    :goto_2
    if-nez p1, :cond_7

    :cond_5
    if-nez v4, :cond_6

    :try_start_3
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    if-nez p1, :cond_7

    goto :goto_3

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/util/h/a/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_6
    :goto_3
    const/4 v5, 0x0

    invoke-virtual {v3, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_4

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/a/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_7
    :goto_4
    if-nez p1, :cond_0

    goto :goto_5

    :catch_4
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/util/h/a/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/a/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_8
    :goto_5
    return-object v1
.end method

.method public a(Ljava/util/Map;Ljava/io/OutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/OutputStreamWriter;

    iget-object v1, p0, Lcom/jscape/util/h/a/j;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, p2, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-static {}, Lcom/jscape/util/h/a/f;->c()I

    move-result p2

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    :try_start_0
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jscape/util/h/a/j;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    if-nez p2, :cond_3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p2, :cond_2

    if-eqz v2, :cond_1

    :try_start_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :cond_1
    const-string v1, ""

    goto :goto_1

    :cond_2
    :goto_0
    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    :goto_1
    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    if-eqz p2, :cond_0

    goto :goto_2

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/util/h/a/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/util/h/a/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/a/j;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_3
    :goto_2
    invoke-virtual {v0}, Ljava/io/Writer;->flush()V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/util/h/a/j;->a(Ljava/io/InputStream;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/util/h/a/j;->a(Ljava/util/Map;Ljava/io/OutputStream;)V

    return-void
.end method
