.class public Lcom/jscape/util/h/a/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "[B>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/util/K;


# direct methods
.method public constructor <init>(Lcom/jscape/util/K;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/h/a/e;->a:Lcom/jscape/util/K;

    return-void
.end method

.method public static a([BIILcom/jscape/util/K;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p2, p3, p4}, Lcom/jscape/util/h/a/f;->a(ILcom/jscape/util/K;Ljava/io/OutputStream;)V

    invoke-virtual {p4, p0, p1, p2}, Ljava/io/OutputStream;->write([BII)V

    return-void
.end method

.method public static a([BIILjava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p2, p3}, Lcom/jscape/util/h/a/f;->a(ILjava/io/OutputStream;)V

    invoke-virtual {p3, p0, p1, p2}, Ljava/io/OutputStream;->write([BII)V

    return-void
.end method

.method public static a([BLcom/jscape/util/K;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1, p2}, Lcom/jscape/util/h/a/e;->a([BIILcom/jscape/util/K;Ljava/io/OutputStream;)V

    return-void
.end method

.method public static a([BLjava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/jscape/util/h/a/e;->a([BIILjava/io/OutputStream;)V

    return-void
.end method

.method public static a(Lcom/jscape/util/K;Ljava/io/InputStream;)[B
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/jscape/util/h/a/f;->a(Lcom/jscape/util/K;Ljava/io/InputStream;)I

    move-result p0

    new-array p0, p0, [B

    invoke-static {p0, p1}, Lcom/jscape/util/X;->a([BLjava/io/InputStream;)[B

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/io/InputStream;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/h/a/f;->a(Ljava/io/InputStream;)I

    move-result v0

    new-array v0, v0, [B

    invoke-static {v0, p0}, Lcom/jscape/util/X;->a([BLjava/io/InputStream;)[B

    move-result-object p0

    return-object p0
.end method

.method public static b([BIILjava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p2, p3}, Lcom/jscape/util/h/a/f;->b(ILjava/io/OutputStream;)V

    invoke-virtual {p3, p0, p1, p2}, Ljava/io/OutputStream;->write([BII)V

    return-void
.end method

.method public static b([BLjava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/jscape/util/h/a/e;->b([BIILjava/io/OutputStream;)V

    return-void
.end method

.method public static b(Ljava/io/InputStream;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/h/a/f;->b(Ljava/io/InputStream;)I

    move-result v0

    new-array v0, v0, [B

    invoke-static {v0, p0}, Lcom/jscape/util/X;->a([BLjava/io/InputStream;)[B

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public c([BLjava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/a/e;->a:Lcom/jscape/util/K;

    invoke-static {p1, v0, p2}, Lcom/jscape/util/h/a/e;->a([BLcom/jscape/util/K;Ljava/io/OutputStream;)V

    return-void
.end method

.method public c(Ljava/io/InputStream;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/a/e;->a:Lcom/jscape/util/K;

    invoke-static {v0, p1}, Lcom/jscape/util/h/a/e;->a(Lcom/jscape/util/K;Ljava/io/InputStream;)[B

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/util/h/a/e;->c(Ljava/io/InputStream;)[B

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, [B

    invoke-virtual {p0, p1, p2}, Lcom/jscape/util/h/a/e;->c([BLjava/io/OutputStream;)V

    return-void
.end method
