.class public Lcom/jscape/util/h/a/p;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "[B>;"
    }
.end annotation


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private final a:Lcom/jscape/util/K;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x10

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x47

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\r:Z5)\u001eT.{Rp#\u0018T\'u\u0010\r:Z5)\u001eT.{Rp#\u0018T\'u"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x21

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/h/a/p;->b:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x67

    goto :goto_2

    :cond_2
    const/16 v12, 0x38

    goto :goto_2

    :cond_3
    const/16 v12, 0xa

    goto :goto_2

    :cond_4
    const/16 v12, 0x52

    goto :goto_2

    :cond_5
    const/16 v12, 0x79

    goto :goto_2

    :cond_6
    const/16 v12, 0x1c

    goto :goto_2

    :cond_7
    const/16 v12, 0x8

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Lcom/jscape/util/K;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/h/a/p;->a:Lcom/jscape/util/K;

    return-void
.end method

.method public static a([BIILcom/jscape/util/K;Ljava/io/OutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    int-to-long v0, p2

    sget-object v2, Lcom/jscape/util/h/a/p;->b:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const-wide/32 v3, 0x10000

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/aq;->d(JJLjava/lang/String;)V

    int-to-short v0, p2

    invoke-static {v0, p3, p4}, Lcom/jscape/util/h/a/q;->a(SLcom/jscape/util/K;Ljava/io/OutputStream;)V

    invoke-virtual {p4, p0, p1, p2}, Ljava/io/OutputStream;->write([BII)V

    return-void
.end method

.method public static a([BIILjava/io/OutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    int-to-long v0, p2

    sget-object v2, Lcom/jscape/util/h/a/p;->b:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const-wide/32 v3, 0x10000

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/aq;->d(JJLjava/lang/String;)V

    int-to-short v0, p2

    invoke-static {v0, p3}, Lcom/jscape/util/h/a/q;->a(SLjava/io/OutputStream;)V

    invoke-virtual {p3, p0, p1, p2}, Ljava/io/OutputStream;->write([BII)V

    return-void
.end method

.method public static a([BLcom/jscape/util/K;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1, p2}, Lcom/jscape/util/h/a/p;->a([BIILcom/jscape/util/K;Ljava/io/OutputStream;)V

    return-void
.end method

.method public static a([BLjava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/jscape/util/h/a/p;->a([BIILjava/io/OutputStream;)V

    return-void
.end method

.method public static a(Lcom/jscape/util/K;Ljava/io/InputStream;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/jscape/util/h/a/q;->a(Lcom/jscape/util/K;Ljava/io/InputStream;)S

    move-result p0

    const v0, 0xffff

    and-int/2addr p0, v0

    new-array p0, p0, [B

    invoke-static {p0, p1}, Lcom/jscape/util/X;->a([BLjava/io/InputStream;)[B

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/io/InputStream;)[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/h/a/q;->a(Ljava/io/InputStream;)S

    move-result v0

    const v1, 0xffff

    and-int/2addr v0, v1

    new-array v0, v0, [B

    invoke-static {v0, p0}, Lcom/jscape/util/X;->a([BLjava/io/InputStream;)[B

    move-result-object p0

    return-object p0
.end method

.method public static b([BIILjava/io/OutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    int-to-long v0, p2

    sget-object v2, Lcom/jscape/util/h/a/p;->b:[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const-wide/32 v3, 0x10000

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/aq;->d(JJLjava/lang/String;)V

    int-to-short v0, p2

    invoke-static {v0, p3}, Lcom/jscape/util/h/a/q;->b(SLjava/io/OutputStream;)V

    invoke-virtual {p3, p0, p1, p2}, Ljava/io/OutputStream;->write([BII)V

    return-void
.end method

.method public static b([BLjava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lcom/jscape/util/h/a/p;->b([BIILjava/io/OutputStream;)V

    return-void
.end method

.method public static b(Ljava/io/InputStream;)[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0}, Lcom/jscape/util/h/a/q;->b(Ljava/io/InputStream;)S

    move-result v0

    const v1, 0xffff

    and-int/2addr v0, v1

    new-array v0, v0, [B

    invoke-static {v0, p0}, Lcom/jscape/util/X;->a([BLjava/io/InputStream;)[B

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public c([BLjava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/a/p;->a:Lcom/jscape/util/K;

    invoke-static {p1, v0, p2}, Lcom/jscape/util/h/a/p;->a([BLcom/jscape/util/K;Ljava/io/OutputStream;)V

    return-void
.end method

.method public c(Ljava/io/InputStream;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/a/p;->a:Lcom/jscape/util/K;

    invoke-static {v0, p1}, Lcom/jscape/util/h/a/p;->a(Lcom/jscape/util/K;Ljava/io/InputStream;)[B

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/util/h/a/p;->c(Ljava/io/InputStream;)[B

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, [B

    invoke-virtual {p0, p1, p2}, Lcom/jscape/util/h/a/p;->c([BLjava/io/OutputStream;)V

    return-void
.end method
