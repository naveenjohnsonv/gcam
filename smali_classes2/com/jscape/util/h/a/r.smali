.class public Lcom/jscape/util/h/a/r;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/util/K;

.field private final b:Ljava/nio/charset/Charset;


# direct methods
.method public constructor <init>(Lcom/jscape/util/K;Ljava/nio/charset/Charset;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/h/a/r;->a:Lcom/jscape/util/K;

    iput-object p2, p0, Lcom/jscape/util/h/a/r;->b:Ljava/nio/charset/Charset;

    return-void
.end method

.method public static a(Lcom/jscape/util/K;Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/String;

    invoke-static {p0, p2}, Lcom/jscape/util/h/a/p;->a(Lcom/jscape/util/K;Ljava/io/InputStream;)[B

    move-result-object p0

    invoke-direct {v0, p0, p1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v0
.end method

.method public static a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/String;

    invoke-static {p1}, Lcom/jscape/util/h/a/p;->a(Ljava/io/InputStream;)[B

    move-result-object p1

    invoke-direct {v0, p1, p0}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/jscape/util/K;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p0

    invoke-static {p0, p1, p3}, Lcom/jscape/util/h/a/p;->a([BLcom/jscape/util/K;Ljava/io/OutputStream;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p0

    invoke-static {p0, p2}, Lcom/jscape/util/h/a/p;->a([BLjava/io/OutputStream;)V

    return-void
.end method

.method public static b(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/lang/String;

    invoke-static {p1}, Lcom/jscape/util/h/a/p;->b(Ljava/io/InputStream;)[B

    move-result-object p1

    invoke-direct {v0, p1, p0}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p0

    invoke-static {p0, p2}, Lcom/jscape/util/h/a/p;->b([BLjava/io/OutputStream;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/a/r;->a:Lcom/jscape/util/K;

    iget-object v1, p0, Lcom/jscape/util/h/a/r;->b:Ljava/nio/charset/Charset;

    invoke-static {v0, v1, p1}, Lcom/jscape/util/h/a/r;->a(Lcom/jscape/util/K;Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/a/r;->a:Lcom/jscape/util/K;

    iget-object v1, p0, Lcom/jscape/util/h/a/r;->b:Ljava/nio/charset/Charset;

    invoke-static {p1, v0, v1, p2}, Lcom/jscape/util/h/a/r;->a(Ljava/lang/String;Lcom/jscape/util/K;Ljava/nio/charset/Charset;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/util/h/a/r;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/util/h/a/r;->a(Ljava/lang/String;Ljava/io/OutputStream;)V

    return-void
.end method
