.class public Lcom/jscape/util/h/a/CommonLineReader;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/AutoCloseable;


# static fields
.field private static final CR:I = 0xd

.field private static final LF:I = 0xa

.field private static final a:[Ljava/lang/String;


# instance fields
.field private final buffer:Lcom/jscape/util/h/o;

.field private final in:Ljava/io/InputStream;

.field private final maxLineLength:I

.field private previousChar:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "H\u0004\u0007P\u0016ct\nA&T\u0000Hi\u000c\u0019\u0014&E\u000e\u0011\u0003NeDH\u0003_\u000b\u000fq\u0001J\rE\u0006\u0001\tH\u0004\u0008D\u0008Ix\u0016\u0019"

    const/16 v5, 0x2f

    const/16 v6, 0x10

    const/4 v7, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x17

    const/4 v10, 0x1

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_0

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x24

    const/16 v4, 0xe

    const-string v6, "\u001fSM\u0014\\\u000e#\\\u0006N#V\u0014w\u0015p\u001cP\u000bV\u0016\u0006Z\u001dX4\\\u0019.V\u0001\u001d\u001dP\u0016w"

    move v8, v11

    const/4 v7, -0x1

    move-object/from16 v16, v6

    move v6, v4

    move-object/from16 v4, v16

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v7, v6

    if-ge v7, v5, :cond_2

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v8, v11

    :goto_3
    const/16 v9, 0x40

    add-int/2addr v7, v10

    add-int v11, v7, v6

    invoke-virtual {v4, v7, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/util/h/a/CommonLineReader;->a:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    const/4 v3, 0x4

    if-eq v2, v3, :cond_5

    if-eq v2, v0, :cond_4

    const/16 v2, 0xa

    goto :goto_4

    :cond_4
    const/16 v2, 0x38

    goto :goto_4

    :cond_5
    const/16 v2, 0x79

    goto :goto_4

    :cond_6
    const/16 v2, 0x26

    goto :goto_4

    :cond_7
    const/16 v2, 0x7d

    goto :goto_4

    :cond_8
    const/16 v2, 0x33

    goto :goto_4

    :cond_9
    const/16 v2, 0x73

    :goto_4
    xor-int/2addr v2, v9

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    const v0, 0x7fffffff

    invoke-direct {p0, p1, v0}, Lcom/jscape/util/h/a/CommonLineReader;-><init>(Ljava/io/InputStream;I)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/h/a/CommonLineReader;->in:Ljava/io/InputStream;

    int-to-long v0, p2

    sget-object p1, Lcom/jscape/util/h/a/CommonLineReader;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object p1, p1, v2

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3, p1}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput p2, p0, Lcom/jscape/util/h/a/CommonLineReader;->maxLineLength:I

    new-instance p1, Lcom/jscape/util/h/o;

    invoke-direct {p1}, Lcom/jscape/util/h/o;-><init>()V

    iput-object p1, p0, Lcom/jscape/util/h/a/CommonLineReader;->buffer:Lcom/jscape/util/h/o;

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method private makeLine(Ljava/nio/charset/Charset;)Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/jscape/util/h/a/CommonLineReader;->buffer:Lcom/jscape/util/h/o;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v1

    iget-object v2, p0, Lcom/jscape/util/h/a/CommonLineReader;->buffer:Lcom/jscape/util/h/o;

    invoke-virtual {v2}, Lcom/jscape/util/h/o;->b()I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2, p1}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    return-object v0
.end method

.method private makeLine()[B
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/h/a/CommonLineReader;->buffer:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/h/a/CommonLineReader;->buffer:Lcom/jscape/util/h/o;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->b()I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Lcom/jscape/util/v;->a([BII)[B

    move-result-object v0

    return-object v0
.end method

.method public static readLine(Lcom/jscape/util/h/a/CommonLineReader;Ljava/nio/charset/Charset;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/a/f;->b()I

    move-result v0

    invoke-virtual {p0, p1}, Lcom/jscape/util/h/a/CommonLineReader;->readLine(Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object p0

    if-eqz v0, :cond_1

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance p0, Ljava/io/EOFException;

    invoke-direct {p0}, Ljava/io/EOFException;-><init>()V

    throw p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    return-object p0
.end method

.method public static readLine(Lcom/jscape/util/h/a/CommonLineReader;)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/a/f;->b()I

    move-result v0

    invoke-virtual {p0}, Lcom/jscape/util/h/a/CommonLineReader;->readLine()[B

    move-result-object p0

    if-eqz v0, :cond_1

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    new-instance p0, Ljava/io/EOFException;

    invoke-direct {p0}, Ljava/io/EOFException;-><init>()V

    throw p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    return-object p0
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/h/a/CommonLineReader;->in:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/InputStream;)V

    iget-object v0, p0, Lcom/jscape/util/h/a/CommonLineReader;->buffer:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->c()Lcom/jscape/util/h/o;

    return-void
.end method

.method public consumeLF()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/a/f;->b()I

    move-result v0

    :try_start_0
    iget v1, p0, Lcom/jscape/util/h/a/CommonLineReader;->previousChar:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_1

    const/16 v0, 0xd

    if-eq v1, v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/jscape/util/h/a/CommonLineReader;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v1

    :cond_1
    const/16 v0, 0xa

    if-ne v1, v0, :cond_2

    :try_start_1
    iput v0, p0, Lcom/jscape/util/h/a/CommonLineReader;->previousChar:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_2
    :goto_0
    return-void

    :catch_1
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method public readLine([B)I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/a/f;->b()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/jscape/util/h/a/CommonLineReader;->in:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_a

    move v3, v2

    :goto_1
    const/16 v4, 0xa

    const/16 v5, 0xd

    if-eqz v0, :cond_1

    if-ne v3, v5, :cond_0

    :try_start_0
    iput v2, p0, Lcom/jscape/util/h/a/CommonLineReader;->previousChar:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_0
    move v3, v2

    move v6, v4

    goto :goto_2

    :cond_1
    move v6, v5

    :goto_2
    if-eqz v0, :cond_5

    if-ne v3, v6, :cond_4

    :try_start_1
    iget v3, p0, Lcom/jscape/util/h/a/CommonLineReader;->previousChar:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v0, :cond_3

    if-ne v3, v5, :cond_4

    :try_start_2
    iput v2, p0, Lcom/jscape/util/h/a/CommonLineReader;->previousChar:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    if-nez v0, :cond_2

    goto :goto_3

    :cond_2
    move v3, v1

    goto :goto_5

    :cond_3
    move v4, v5

    goto :goto_4

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p1

    :try_start_6
    invoke-static {p1}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_4
    :goto_3
    if-eqz v0, :cond_6

    move v3, v2

    goto :goto_4

    :cond_5
    move v4, v6

    :goto_4
    if-ne v3, v4, :cond_7

    :try_start_7
    iput v2, p0, Lcom/jscape/util/h/a/CommonLineReader;->previousChar:I
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    move v2, v1

    :cond_6
    return v2

    :catch_6
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_7
    add-int/lit8 v3, v1, 0x1

    int-to-byte v4, v2

    aput-byte v4, p1, v1

    :goto_5
    array-length v1, p1

    if-lt v3, v1, :cond_9

    if-eqz v0, :cond_8

    return v3

    :cond_8
    move v1, v3

    goto :goto_1

    :cond_9
    move v1, v3

    goto :goto_0

    :cond_a
    if-eqz v0, :cond_c

    if-eqz v1, :cond_b

    goto :goto_6

    :cond_b
    move v1, v3

    :cond_c
    :goto_6
    return v1
.end method

.method public readLine(Ljava/nio/charset/Charset;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/a/f;->c()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/util/h/a/CommonLineReader;->buffer:Lcom/jscape/util/h/o;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->c()Lcom/jscape/util/h/o;

    :cond_0
    iget-object v1, p0, Lcom/jscape/util/h/a/CommonLineReader;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_9

    const/16 v2, 0xa

    const/16 v3, 0xd

    if-nez v0, :cond_2

    if-ne v1, v3, :cond_1

    :try_start_0
    iput v1, p0, Lcom/jscape/util/h/a/CommonLineReader;->previousChar:I

    invoke-direct {p0, p1}, Lcom/jscape/util/h/a/CommonLineReader;->makeLine(Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_1
    move v4, v2

    goto :goto_0

    :cond_2
    move v4, v3

    :goto_0
    if-nez v0, :cond_4

    if-ne v1, v4, :cond_5

    :try_start_1
    iget v4, p0, Lcom/jscape/util/h/a/CommonLineReader;->previousChar:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v0, :cond_3

    if-ne v4, v3, :cond_5

    :try_start_2
    iput v1, p0, Lcom/jscape/util/h/a/CommonLineReader;->previousChar:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    if-eqz v0, :cond_7

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2

    :catch_1
    move-exception p1

    :try_start_3
    invoke-static {p1}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_4
    move v2, v4

    :cond_5
    :goto_1
    move v4, v1

    :goto_2
    if-ne v4, v2, :cond_6

    :try_start_6
    iput v1, p0, Lcom/jscape/util/h/a/CommonLineReader;->previousChar:I

    invoke-direct {p0, p1}, Lcom/jscape/util/h/a/CommonLineReader;->makeLine(Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object p1
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_3

    :catch_5
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_6
    iget-object v2, p0, Lcom/jscape/util/h/a/CommonLineReader;->buffer:Lcom/jscape/util/h/o;

    invoke-virtual {v2, v1}, Lcom/jscape/util/h/o;->write(I)V

    :cond_7
    iget-object v1, p0, Lcom/jscape/util/h/a/CommonLineReader;->buffer:Lcom/jscape/util/h/o;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->b()I

    move-result v1

    iget v2, p0, Lcom/jscape/util/h/a/CommonLineReader;->maxLineLength:I

    if-lt v1, v2, :cond_0

    invoke-direct {p0, p1}, Lcom/jscape/util/h/a/CommonLineReader;->makeLine(Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object p1

    if-nez v0, :cond_8

    :cond_8
    :goto_3
    return-object p1

    :cond_9
    if-nez v0, :cond_a

    :try_start_7
    iget-object v0, p0, Lcom/jscape/util/h/a/CommonLineReader;->buffer:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->e()Z

    move-result v0
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    if-eqz v0, :cond_a

    const/4 p1, 0x0

    goto :goto_4

    :catch_6
    move-exception p1

    :try_start_8
    invoke-static {p1}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    :catch_7
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_a
    invoke-direct {p0, p1}, Lcom/jscape/util/h/a/CommonLineReader;->makeLine(Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object p1

    :goto_4
    return-object p1
.end method

.method public readLine()[B
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/a/f;->b()I

    move-result v0

    iget-object v1, p0, Lcom/jscape/util/h/a/CommonLineReader;->buffer:Lcom/jscape/util/h/o;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->c()Lcom/jscape/util/h/o;

    :cond_0
    iget-object v1, p0, Lcom/jscape/util/h/a/CommonLineReader;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_9

    const/16 v2, 0xa

    const/16 v3, 0xd

    if-eqz v0, :cond_2

    if-ne v1, v3, :cond_1

    :try_start_0
    iput v1, p0, Lcom/jscape/util/h/a/CommonLineReader;->previousChar:I

    invoke-direct {p0}, Lcom/jscape/util/h/a/CommonLineReader;->makeLine()[B

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    move v4, v2

    goto :goto_0

    :cond_2
    move v4, v3

    :goto_0
    if-eqz v0, :cond_4

    if-ne v1, v4, :cond_5

    :try_start_1
    iget v4, p0, Lcom/jscape/util/h/a/CommonLineReader;->previousChar:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v0, :cond_3

    if-ne v4, v3, :cond_5

    :try_start_2
    iput v1, p0, Lcom/jscape/util/h/a/CommonLineReader;->previousChar:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    if-nez v0, :cond_7

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2

    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {v0}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception v0

    :try_start_4
    invoke-static {v0}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :catch_3
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :catch_4
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_4
    move v2, v4

    :cond_5
    :goto_1
    move v4, v1

    :goto_2
    if-ne v4, v2, :cond_6

    :try_start_6
    iput v1, p0, Lcom/jscape/util/h/a/CommonLineReader;->previousChar:I

    invoke-direct {p0}, Lcom/jscape/util/h/a/CommonLineReader;->makeLine()[B

    move-result-object v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_3

    :catch_5
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_6
    iget-object v2, p0, Lcom/jscape/util/h/a/CommonLineReader;->buffer:Lcom/jscape/util/h/o;

    invoke-virtual {v2, v1}, Lcom/jscape/util/h/o;->write(I)V

    :cond_7
    iget-object v1, p0, Lcom/jscape/util/h/a/CommonLineReader;->buffer:Lcom/jscape/util/h/o;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->b()I

    move-result v1

    iget v2, p0, Lcom/jscape/util/h/a/CommonLineReader;->maxLineLength:I

    if-lt v1, v2, :cond_0

    invoke-direct {p0}, Lcom/jscape/util/h/a/CommonLineReader;->makeLine()[B

    move-result-object v1

    if-eqz v0, :cond_8

    return-object v1

    :cond_8
    move-object v0, v1

    :goto_3
    return-object v0

    :cond_9
    if-eqz v0, :cond_a

    :try_start_7
    iget-object v0, p0, Lcom/jscape/util/h/a/CommonLineReader;->buffer:Lcom/jscape/util/h/o;

    invoke-virtual {v0}, Lcom/jscape/util/h/o;->e()Z

    move-result v0
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    if-eqz v0, :cond_a

    const/4 v0, 0x0

    goto :goto_4

    :catch_6
    move-exception v0

    :try_start_8
    invoke-static {v0}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    :catch_7
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/a/CommonLineReader;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_a
    invoke-direct {p0}, Lcom/jscape/util/h/a/CommonLineReader;->makeLine()[B

    move-result-object v0

    :goto_4
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/h/a/CommonLineReader;->a:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/h/a/CommonLineReader;->in:Ljava/io/InputStream;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/util/h/a/CommonLineReader;->maxLineLength:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/h/a/CommonLineReader;->buffer:Lcom/jscape/util/h/o;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/util/h/a/CommonLineReader;->previousChar:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
