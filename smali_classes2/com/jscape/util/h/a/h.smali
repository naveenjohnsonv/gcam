.class public Lcom/jscape/util/h/a/h;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/I;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/I<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private final a:[B

.field private final b:Ljava/nio/charset/Charset;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "\u0007Tnf\u0005H7 NxwUQ6\"Oi3\u0006L*7[p3\u0001A(7\u0000=~\u0014J3\u0001Omc\u001aJ,7^5:UJ=#Ota\u0010\\"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/h/a/h;->c:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x74

    goto :goto_1

    :cond_1
    const/16 v4, 0x14

    goto :goto_1

    :cond_2
    const/16 v4, 0x59

    goto :goto_1

    :cond_3
    const/16 v4, 0x3f

    goto :goto_1

    :cond_4
    const/16 v4, 0x31

    goto :goto_1

    :cond_5
    const/16 v4, 0x16

    goto :goto_1

    :cond_6
    const/16 v4, 0x7e

    :goto_1
    const/16 v5, 0x2c

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>([BLjava/nio/charset/Charset;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/h/a/h;->a:[B

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/h/a/h;->b:Ljava/nio/charset/Charset;

    return-void
.end method

.method public static a([BII[BLjava/io/InputStream;)I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/a/f;->b()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    move v5, p1

    move v3, v1

    move v4, v3

    :goto_0
    invoke-virtual {p4}, Ljava/io/InputStream;->read()I

    move-result v6

    invoke-static {v6}, Lcom/jscape/util/h/a/h;->a(I)Z

    move-result v7

    if-nez v7, :cond_4

    add-int/lit8 v4, v5, 0x1

    int-to-byte v6, v6

    aput-byte v6, p0, v5

    add-int/lit8 v1, v1, 0x1

    add-int v5, p1, v1

    :try_start_0
    invoke-static {p0, v1, v5, p3}, Lcom/jscape/util/h/a/h;->a([BII[B)Z

    move-result v5
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_2

    if-eqz v5, :cond_1

    if-nez v0, :cond_0

    move v3, v2

    goto :goto_1

    :cond_0
    move v4, v2

    goto :goto_3

    :cond_1
    :goto_1
    move v5, v1

    :cond_2
    if-lt v5, p2, :cond_3

    move v4, v2

    goto :goto_2

    :cond_3
    move v5, v4

    move v4, v2

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/h/a/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_4
    :goto_2
    move v2, v3

    :goto_3
    if-eqz v0, :cond_6

    if-eqz v2, :cond_5

    array-length p0, p3

    sub-int/2addr v1, p0

    :cond_5
    move v2, v4

    :cond_6
    if-eqz v0, :cond_8

    if-eqz v2, :cond_7

    goto :goto_4

    :cond_7
    const/4 p0, -0x1

    goto :goto_5

    :cond_8
    move v1, v2

    :goto_4
    move p0, v1

    :goto_5
    return p0
.end method

.method public static a([B[BLjava/io/InputStream;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1, p2}, Lcom/jscape/util/h/a/h;->a([BII[BLjava/io/InputStream;)I

    move-result p0

    return p0
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method

.method public static a(Ljava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/a/f;->b()I

    move-result v0

    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->markSupported()Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    if-eqz v1, :cond_8

    new-instance v1, Lcom/jscape/util/h/o;

    invoke-direct {v1}, Lcom/jscape/util/h/o;-><init>()V

    :cond_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v2

    invoke-static {v2}, Lcom/jscape/util/h/a/h;->a(I)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v1, v2}, Lcom/jscape/util/h/o;->write(I)V

    :cond_1
    invoke-static {v1}, Lcom/jscape/util/h/a/h;->a(Lcom/jscape/util/h/o;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v1, p1}, Lcom/jscape/util/h/a/h;->a(Lcom/jscape/util/h/o;Ljava/io/InputStream;)V

    if-eqz v0, :cond_1

    :cond_2
    :try_start_1
    invoke-static {v2}, Lcom/jscape/util/h/a/h;->a(I)Z

    move-result p1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    const/4 v3, 0x1

    if-eqz v0, :cond_4

    if-nez p1, :cond_3

    :try_start_2
    invoke-virtual {v1}, Lcom/jscape/util/h/o;->b()I

    move-result p1

    sub-int/2addr p1, v3

    invoke-virtual {v1, p1}, Lcom/jscape/util/h/o;->a(I)Lcom/jscape/util/h/o;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    :cond_3
    invoke-static {v2}, Lcom/jscape/util/h/a/h;->a(I)Z

    move-result p1

    :cond_4
    if-eqz v0, :cond_5

    if-eqz p1, :cond_6

    :try_start_3
    invoke-virtual {v1}, Lcom/jscape/util/h/o;->e()Z

    move-result p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/h/a/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_5
    :goto_0
    if-eqz p1, :cond_6

    const/4 p0, 0x0

    goto :goto_1

    :cond_6
    new-instance p1, Ljava/lang/String;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->b()I

    move-result v1

    invoke-direct {p1, v2, v4, v1, p0}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    move-object p0, p1

    :goto_1
    if-nez v0, :cond_7

    :try_start_4
    new-array p1, v3, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/h/a/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_7
    :goto_2
    return-object p0

    :catch_2
    move-exception p0

    :try_start_5
    invoke-static {p0}, Lcom/jscape/util/h/a/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/h/a/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_8
    :try_start_6
    new-instance p0, Ljava/io/IOException;

    sget-object p1, Lcom/jscape/util/h/a/h;->c:Ljava/lang/String;

    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    :catch_4
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/h/a/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method

.method public static a([BLjava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/a/f;->c()I

    move-result v0

    new-instance v1, Lcom/jscape/util/h/o;

    invoke-direct {v1}, Lcom/jscape/util/h/o;-><init>()V

    :cond_0
    invoke-virtual {p2}, Ljava/io/InputStream;->read()I

    move-result v2

    invoke-static {v2}, Lcom/jscape/util/h/a/h;->a(I)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1, v2}, Lcom/jscape/util/h/o;->write(I)V

    invoke-static {v1, p0}, Lcom/jscape/util/h/a/h;->a(Lcom/jscape/util/h/o;[B)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_1
    :try_start_0
    invoke-static {v2}, Lcom/jscape/util/h/a/h;->a(I)Z

    move-result p2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_3

    if-nez p2, :cond_2

    :try_start_1
    invoke-virtual {v1}, Lcom/jscape/util/h/o;->b()I

    move-result p2

    array-length p0, p0

    sub-int/2addr p2, p0

    invoke-virtual {v1, p2}, Lcom/jscape/util/h/o;->a(I)Lcom/jscape/util/h/o;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_2
    invoke-static {v2}, Lcom/jscape/util/h/a/h;->a(I)Z

    move-result p2

    :cond_3
    if-nez v0, :cond_4

    if-eqz p2, :cond_5

    :try_start_2
    invoke-virtual {v1}, Lcom/jscape/util/h/o;->e()Z

    move-result p2
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/h/a/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_4
    :goto_0
    if-eqz p2, :cond_5

    const/4 p0, 0x0

    goto :goto_1

    :cond_5
    new-instance p0, Ljava/lang/String;

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->a()[B

    move-result-object p2

    const/4 v0, 0x0

    invoke-virtual {v1}, Lcom/jscape/util/h/o;->b()I

    move-result v1

    invoke-direct {p0, p2, v0, v1, p1}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    :goto_1
    return-object p0

    :catch_1
    move-exception p0

    :try_start_3
    invoke-static {p0}, Lcom/jscape/util/h/a/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/h/a/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method

.method private static a(Lcom/jscape/util/h/o;Ljava/io/InputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/a/f;->c()I

    move-result v0

    invoke-virtual {p0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v1

    invoke-virtual {p0}, Lcom/jscape/util/h/o;->b()I

    move-result p0

    const/4 v2, 0x1

    sub-int/2addr p0, v2

    aget-byte p0, v1, p0

    if-nez v0, :cond_1

    const/16 v1, 0xd

    if-eq p0, v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1, v2}, Ljava/io/InputStream;->mark(I)V

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result p0

    :cond_1
    :try_start_0
    invoke-static {p0}, Lcom/jscape/util/h/a/h;->a(I)Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_2

    if-nez v1, :cond_3

    goto :goto_0

    :cond_2
    move p0, v1

    :goto_0
    const/16 v0, 0xa

    if-eq p0, v0, :cond_3

    :try_start_1
    invoke-virtual {p1}, Ljava/io/InputStream;->reset()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/h/a/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0

    :cond_3
    :goto_1
    return-void

    :catch_1
    move-exception p0

    invoke-static {p0}, Lcom/jscape/util/h/a/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p0

    throw p0
.end method

.method public static a(Ljava/lang/String;[BLjava/nio/charset/Charset;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p0

    invoke-static {p0, p1, p3}, Lcom/jscape/util/h/a/h;->a([B[BLjava/io/OutputStream;)V

    return-void
.end method

.method public static a([BII[BLjava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p4, p0, p1, p2}, Ljava/io/OutputStream;->write([BII)V

    invoke-virtual {p4, p3}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public static a([B[BLjava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1, p2}, Lcom/jscape/util/h/a/h;->a([BII[BLjava/io/OutputStream;)V

    return-void
.end method

.method private static a(I)Z
    .locals 1

    invoke-static {}, Lcom/jscape/util/h/a/f;->b()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :cond_1
    :goto_0
    return p0
.end method

.method private static a(Lcom/jscape/util/h/o;)Z
    .locals 3

    invoke-static {}, Lcom/jscape/util/h/a/f;->b()I

    move-result v0

    invoke-virtual {p0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v1

    invoke-virtual {p0}, Lcom/jscape/util/h/o;->b()I

    move-result p0

    const/4 v2, 0x1

    sub-int/2addr p0, v2

    aget-byte p0, v1, p0

    if-eqz v0, :cond_1

    const/16 v1, 0xd

    if-eq p0, v1, :cond_2

    if-eqz v0, :cond_1

    const/16 v0, 0xa

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    move v2, p0

    :cond_2
    :goto_0
    move p0, v2

    :goto_1
    return p0
.end method

.method private static a(Lcom/jscape/util/h/o;[B)Z
    .locals 3

    invoke-static {}, Lcom/jscape/util/h/a/f;->b()I

    move-result v0

    invoke-virtual {p0}, Lcom/jscape/util/h/o;->b()I

    move-result v1

    if-eqz v0, :cond_0

    array-length v2, p1

    if-lt v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/jscape/util/h/o;->a()[B

    move-result-object v1

    invoke-virtual {p0}, Lcom/jscape/util/h/o;->b()I

    move-result p0

    invoke-static {v1, p1, p0}, Lcom/jscape/util/v;->b([B[BI)Z

    move-result v1

    :cond_0
    if-eqz v0, :cond_2

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move p0, v1

    :goto_1
    return p0
.end method

.method private static a([BII[B)Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/h/a/f;->b()I

    move-result v0

    if-eqz v0, :cond_0

    array-length v1, p3

    if-lt p1, v1, :cond_1

    invoke-static {p0, p3, p2}, Lcom/jscape/util/v;->b([B[BI)Z

    move-result p1

    :cond_0
    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    move p0, p1

    :goto_1
    return p0
.end method


# virtual methods
.method public a(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/a/h;->a:[B

    iget-object v1, p0, Lcom/jscape/util/h/a/h;->b:Ljava/nio/charset/Charset;

    invoke-static {v0, v1, p1}, Lcom/jscape/util/h/a/h;->a([BLjava/nio/charset/Charset;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/a/h;->a:[B

    iget-object v1, p0, Lcom/jscape/util/h/a/h;->b:Ljava/nio/charset/Charset;

    invoke-static {p1, v0, v1, p2}, Lcom/jscape/util/h/a/h;->a(Ljava/lang/String;[BLjava/nio/charset/Charset;Ljava/io/OutputStream;)V

    return-void
.end method

.method public bridge synthetic read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jscape/util/h/a/h;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/jscape/util/h/a/h;->a(Ljava/lang/String;Ljava/io/OutputStream;)V

    return-void
.end method
