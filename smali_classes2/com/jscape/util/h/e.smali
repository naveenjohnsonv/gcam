.class public Lcom/jscape/util/h/e;
.super Ljava/io/InputStream;


# static fields
.field private static final e:[Ljava/lang/String;


# instance fields
.field private a:[B

.field private b:[B

.field private c:I

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x9

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x14

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, ".Z\u0004|c{KjG\t.Z\u0007\u007fkoZvG$@\u0003\u001c|LnMc\u0003!w}iKQ\u000e\u001a|lq\u001fy\u0018\u001d\u007fkyM,\u0016\rwjhW?"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x38

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/h/e;->e:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    const/4 v13, 0x2

    if-eq v12, v13, :cond_5

    if-eq v12, v0, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x2b

    goto :goto_2

    :cond_2
    const/16 v12, 0x8

    goto :goto_2

    :cond_3
    const/16 v12, 0x19

    goto :goto_2

    :cond_4
    const/16 v12, 0xd

    goto :goto_2

    :cond_5
    const/16 v12, 0x7c

    goto :goto_2

    :cond_6
    const/16 v12, 0x6e

    goto :goto_2

    :cond_7
    const/16 v12, 0x16

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>([B)V
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/jscape/util/h/e;-><init>([BII)V

    return-void
.end method

.method public constructor <init>([BII)V
    .locals 0

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/util/h/e;->a([BII)V

    return-void
.end method

.method public static a()Lcom/jscape/util/h/e;
    .locals 2

    new-instance v0, Lcom/jscape/util/h/e;

    const/4 v1, 0x0

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lcom/jscape/util/h/e;-><init>([B)V

    return-object v0
.end method


# virtual methods
.method public a([BII)V
    .locals 0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/h/e;->b:[B

    iput p2, p0, Lcom/jscape/util/h/e;->c:I

    iput p3, p0, Lcom/jscape/util/h/e;->d:I

    return-void
.end method

.method public available()I
    .locals 1

    iget v0, p0, Lcom/jscape/util/h/e;->d:I

    return v0
.end method

.method public close()V
    .locals 0

    return-void
.end method

.method public read()I
    .locals 4

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/jscape/util/h/e;->a:[B

    if-nez v1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/jscape/util/h/e;->a:[B

    :cond_0
    move-object v1, p0

    :cond_1
    iget-object v2, v1, Lcom/jscape/util/h/e;->a:[B

    invoke-virtual {v1, v2}, Lcom/jscape/util/h/e;->read([B)I

    move-result v2

    :cond_2
    if-eqz v2, :cond_1

    if-nez v0, :cond_2

    const/4 v3, -0x1

    if-nez v0, :cond_3

    if-eq v2, v3, :cond_4

    iget-object v0, v1, Lcom/jscape/util/h/e;->a:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    const/16 v3, 0xff

    :cond_3
    and-int/2addr v3, v2

    :cond_4
    return v3
.end method

.method public read([B)I
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/jscape/util/h/e;->read([BII)I

    move-result p1

    return p1
.end method

.method public read([BII)I
    .locals 2

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/util/h/e;->d:I

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object p3, p0, Lcom/jscape/util/h/e;->b:[B

    iget v0, p0, Lcom/jscape/util/h/e;->c:I

    invoke-static {p3, v0, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p1, p0, Lcom/jscape/util/h/e;->c:I

    add-int/2addr p1, v1

    iput p1, p0, Lcom/jscape/util/h/e;->c:I

    iget p1, p0, Lcom/jscape/util/h/e;->d:I

    sub-int/2addr p1, v1

    iput p1, p0, Lcom/jscape/util/h/e;->d:I

    :cond_1
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/h/e;->e:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/util/h/e;->b:[B

    array-length v2, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/util/h/e;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/util/h/e;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
