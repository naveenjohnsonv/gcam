.class public Lcom/jscape/util/h/y;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/J;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/J<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/jscape/util/h/z;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/z<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/jscape/util/h/B;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jscape/util/h/B<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/jscape/util/h/L;Lcom/jscape/util/h/I;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/h/L;",
            "Lcom/jscape/util/h/I<",
            "TT;>;)V"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/z;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/jscape/util/h/K;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-direct {v0, p1, v1}, Lcom/jscape/util/h/z;-><init>(Lcom/jscape/util/h/L;[Lcom/jscape/util/h/K;)V

    new-instance v1, Lcom/jscape/util/h/B;

    invoke-direct {v1, p1, p2}, Lcom/jscape/util/h/B;-><init>(Lcom/jscape/util/h/L;Lcom/jscape/util/h/N;)V

    invoke-direct {p0, v0, v1}, Lcom/jscape/util/h/y;-><init>(Lcom/jscape/util/h/z;Lcom/jscape/util/h/B;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/util/h/z;Lcom/jscape/util/h/B;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jscape/util/h/z<",
            "TT;>;",
            "Lcom/jscape/util/h/B<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/h/y;->a:Lcom/jscape/util/h/z;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/h/y;->b:Lcom/jscape/util/h/B;

    return-void
.end method

.method public static a(Lcom/jscape/util/h/L;Lcom/jscape/util/h/I;)Lcom/jscape/util/h/y;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/jscape/util/h/L;",
            "Lcom/jscape/util/h/I<",
            "TT;>;)",
            "Lcom/jscape/util/h/y<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/y;

    invoke-direct {v0, p0, p1}, Lcom/jscape/util/h/y;-><init>(Lcom/jscape/util/h/L;Lcom/jscape/util/h/I;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/y;->b:Lcom/jscape/util/h/B;

    invoke-virtual {v0, p1}, Lcom/jscape/util/h/B;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/h/y;->a:Lcom/jscape/util/h/z;

    invoke-virtual {v0}, Lcom/jscape/util/h/z;->a()Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/y;->a:Lcom/jscape/util/h/z;

    invoke-virtual {v0}, Lcom/jscape/util/h/z;->b()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/h/y;->b:Lcom/jscape/util/h/B;

    invoke-virtual {v0}, Lcom/jscape/util/h/B;->c()Z

    move-result v0

    return v0
.end method
