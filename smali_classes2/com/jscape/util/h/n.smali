.class public Lcom/jscape/util/h/n;
.super Ljava/io/OutputStream;


# instance fields
.field private final a:Lcom/jscape/util/h/u;

.field private b:[B


# direct methods
.method public constructor <init>(Lcom/jscape/util/h/u;)V
    .locals 0

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/h/n;->a:Lcom/jscape/util/h/u;

    return-void
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/h/n;->a:Lcom/jscape/util/h/u;

    invoke-virtual {v0}, Lcom/jscape/util/h/u;->a()V

    return-void
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/n;->a:Lcom/jscape/util/h/u;

    invoke-static {v0}, Lcom/jscape/util/h/u;->a(Lcom/jscape/util/h/u;)V

    return-void
.end method

.method public write(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/h/n;->b:[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :try_start_1
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/jscape/util/h/n;->b:[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    iget-object v1, p0, Lcom/jscape/util/h/n;->b:[B

    :cond_1
    const/4 v0, 0x0

    int-to-byte p1, p1

    aput-byte p1, v1, v0

    iget-object p1, p0, Lcom/jscape/util/h/n;->b:[B

    invoke-virtual {p0, p1}, Lcom/jscape/util/h/n;->write([B)V

    return-void

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/util/h/n;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/n;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method public write([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/n;->a:Lcom/jscape/util/h/u;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/util/h/u;->b([BII)V

    return-void
.end method
