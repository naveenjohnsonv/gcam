.class public Lcom/jscape/util/h/b/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/N;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/N<",
        "TT;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class;",
            "Ljava/beans/PersistenceDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private static b:[Lcom/jscape/util/aq;

.field private static final c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/jscape/util/h/b/c;->b([Lcom/jscape/util/aq;)V

    const/4 v2, 0x0

    const/16 v3, 0x19

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x51

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "v\u0008tK\u0008\u0002\u0017_\u0015uM\u0014ETk7Ja\u0014A\u001bW\u001ft\n!P\u0015k\n\u0010Q\u0017R\nc\n\u000fV\u001d_TuA\u0008K\u0015_\u0013|E\u000eK\u001b]T~I\u0016"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x3b

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/h/b/c;->c:[Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/jscape/util/h/b/c;->a:Ljava/util/Map;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/16 v13, 0x2b

    if-eqz v12, :cond_5

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_4

    const/4 v14, 0x3

    if-eq v12, v14, :cond_3

    const/4 v14, 0x4

    if-eq v12, v14, :cond_6

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v13, 0x25

    goto :goto_2

    :cond_2
    const/16 v13, 0x73

    goto :goto_2

    :cond_3
    const/16 v13, 0x75

    goto :goto_2

    :cond_4
    const/16 v13, 0x57

    goto :goto_2

    :cond_5
    const/16 v13, 0x62

    :cond_6
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/beans/XMLEncoder;)V
    .locals 4

    invoke-static {}, Lcom/jscape/util/h/b/c;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    sget-object v1, Lcom/jscape/util/h/b/c;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Class;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/beans/PersistenceDelegate;

    invoke-virtual {p1, v3, v2}, Ljava/beans/XMLEncoder;->setPersistenceDelegate(Ljava/lang/Class;Ljava/beans/PersistenceDelegate;)V

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method

.method public static declared-synchronized a(Ljava/lang/Class;)V
    .locals 2

    const-class v0, Lcom/jscape/util/h/b/c;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/jscape/util/h/b/c;->a:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized a(Ljava/lang/Class;Ljava/beans/PersistenceDelegate;)V
    .locals 2

    const-class v0, Lcom/jscape/util/h/b/c;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/jscape/util/h/b/c;->a:Ljava/util/Map;

    invoke-interface {v1, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 4

    sget-object v0, Lcom/jscape/util/h/b/c;->c:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v1

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private static b(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private b(Ljava/beans/XMLEncoder;)V
    .locals 1

    invoke-static {}, Lcom/jscape/util/h/b/c;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/beans/XMLEncoder;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-direct {p0, p1}, Lcom/jscape/util/h/b/c;->a(Ljava/lang/Exception;)V

    :goto_0
    return-void
.end method

.method public static b([Lcom/jscape/util/aq;)V
    .locals 0

    sput-object p0, Lcom/jscape/util/h/b/c;->b:[Lcom/jscape/util/aq;

    return-void
.end method

.method public static b()[Lcom/jscape/util/aq;
    .locals 1

    sget-object v0, Lcom/jscape/util/h/b/c;->b:[Lcom/jscape/util/aq;

    return-object v0
.end method


# virtual methods
.method public write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/b/c;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    new-instance v1, Ljava/beans/XMLEncoder;

    invoke-direct {v1, p2}, Ljava/beans/XMLEncoder;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    invoke-direct {p0, v1}, Lcom/jscape/util/h/b/c;->a(Ljava/beans/XMLEncoder;)V

    invoke-virtual {v1, p1}, Ljava/beans/XMLEncoder;->writeObject(Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/beans/XMLEncoder;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0, v1}, Lcom/jscape/util/h/b/c;->b(Ljava/beans/XMLEncoder;)V

    if-eqz v0, :cond_0

    const/4 p1, 0x2

    :try_start_1
    new-array p1, p1, [I

    invoke-static {p1}, Lcom/jscape/util/aq;->b([I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/b/c;->b(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_0
    :goto_0
    return-void

    :catchall_0
    move-exception p1

    invoke-direct {p0, v1}, Lcom/jscape/util/h/b/c;->b(Ljava/beans/XMLEncoder;)V

    throw p1
.end method
