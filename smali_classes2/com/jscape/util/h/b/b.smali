.class public Lcom/jscape/util/h/b/b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/K;
.implements Ljava/beans/ExceptionListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/jscape/util/h/K<",
        "TT;>;",
        "Ljava/beans/ExceptionListener;"
    }
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x20

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x59

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "aC{/V=KAB}/VtWC\u0011f\"NxZP\u0011o2Kp\u0019||En\u0013G^dnNnZEAlnQiPH\u001fanF!aC{/V=KAPm)Jz\u0019@P}!\u0004{KK\\)\u0018iQ\u0019BXe%\n"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x56

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v15, v4

    move v4, v3

    move v3, v15

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/h/b/b;->a:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/16 v13, 0x7d

    if-eqz v12, :cond_6

    if-eq v12, v7, :cond_5

    const/4 v14, 0x2

    if-eq v12, v14, :cond_4

    if-eq v12, v0, :cond_3

    const/4 v14, 0x4

    if-eq v12, v14, :cond_6

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v13, 0x60

    goto :goto_2

    :cond_2
    const/16 v13, 0x44

    goto :goto_2

    :cond_3
    const/16 v13, 0x19

    goto :goto_2

    :cond_4
    const/16 v13, 0x50

    goto :goto_2

    :cond_5
    const/16 v13, 0x68

    :cond_6
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public exceptionThrown(Ljava/lang/Exception;)V
    .locals 4

    invoke-static {}, Lcom/jscape/util/h/b/c;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    sget-object v1, Lcom/jscape/util/h/b/b;->a:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v1

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    if-nez v0, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    :cond_0
    sget-object v0, Lcom/jscape/util/h/b/b;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    invoke-virtual {v1, v2, v0, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-void
.end method

.method public read(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/b/c;->b()[Lcom/jscape/util/aq;

    move-result-object v0

    :try_start_0
    new-instance v1, Ljava/beans/XMLDecoder;

    invoke-direct {v1, p1}, Ljava/beans/XMLDecoder;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v1, p0}, Ljava/beans/XMLDecoder;->setExceptionListener(Ljava/beans/ExceptionListener;)V

    invoke-virtual {v1}, Ljava/beans/XMLDecoder;->readObject()Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance p1, Ljava/io/IOException;

    sget-object v0, Lcom/jscape/util/h/b/b;->a:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/util/h/b/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :cond_1
    :goto_0
    :try_start_3
    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/jscape/util/aq;

    invoke-static {v0}, Lcom/jscape/util/h/b/c;->b([Lcom/jscape/util/aq;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    :cond_2
    return-object p1

    :catch_1
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/b/b;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method
