.class public Lcom/jscape/util/h/s;
.super Ljava/io/OutputStream;


# static fields
.field private static final a:I = 0x200

.field private static final b:I = 0x1

.field private static final h:[Ljava/lang/String;


# instance fields
.field private final c:Ljava/io/OutputStream;

.field private final d:Ljava/util/zip/Inflater;

.field private final e:[B

.field private final f:[B

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0xe

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x4b

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "\u0019\u001d\nU\u000eGB)\u0005\u0017C\nNL\u0018\u0007\u0000\u000bC\u0006D\u0005j34y-\n\u0006#\n\u000cY\u0000D\u00038\u0010V"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x27

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/util/h/s;->h:[Ljava/lang/String;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x29

    goto :goto_2

    :cond_2
    const/16 v12, 0x61

    goto :goto_2

    :cond_3
    const/16 v12, 0x24

    goto :goto_2

    :cond_4
    const/16 v12, 0x7b

    goto :goto_2

    :cond_5
    const/16 v12, 0x33

    goto :goto_2

    :cond_6
    const/16 v12, 0x22

    goto :goto_2

    :cond_7
    move v12, v7

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1

    new-instance v0, Ljava/util/zip/Inflater;

    invoke-direct {v0}, Ljava/util/zip/Inflater;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/jscape/util/h/s;-><init>(Ljava/io/OutputStream;Ljava/util/zip/Inflater;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Ljava/util/zip/Inflater;)V
    .locals 1

    const/16 v0, 0x200

    invoke-direct {p0, p1, p2, v0}, Lcom/jscape/util/h/s;-><init>(Ljava/io/OutputStream;Ljava/util/zip/Inflater;I)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Ljava/util/zip/Inflater;I)V
    .locals 0

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/h/s;->c:Ljava/io/OutputStream;

    invoke-static {p2}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p2, p0, Lcom/jscape/util/h/s;->d:Ljava/util/zip/Inflater;

    new-array p1, p3, [B

    iput-object p1, p0, Lcom/jscape/util/h/s;->e:[B

    const/4 p1, 0x1

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/jscape/util/h/s;->f:[B

    return-void
.end method

.method private static a(Ljava/lang/Exception;)Ljava/lang/Exception;
    .locals 0

    return-object p0
.end method

.method private a()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/util/h/s;->g:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    sget-object v1, Lcom/jscape/util/h/s;->h:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/s;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    :try_start_0
    iget-boolean v0, p0, Lcom/jscape/util/h/s;->g:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/jscape/util/h/s;->flush()V

    iget-object v0, p0, Lcom/jscape/util/h/s;->d:Ljava/util/zip/Inflater;

    invoke-virtual {v0}, Ljava/util/zip/Inflater;->end()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/jscape/util/h/s;->c:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v2, p0, Lcom/jscape/util/h/s;->c:Ljava/io/OutputStream;

    invoke-static {v2}, Lcom/jscape/util/X;->a(Ljava/io/OutputStream;)V

    iput-boolean v1, p0, Lcom/jscape/util/h/s;->g:Z

    throw v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/s;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    iput-boolean v1, p0, Lcom/jscape/util/h/s;->g:Z

    return-void
.end method

.method public flush()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-direct {p0}, Lcom/jscape/util/h/s;->a()V

    if-nez v0, :cond_5

    iget-object v1, p0, Lcom/jscape/util/h/s;->d:Ljava/util/zip/Inflater;

    invoke-virtual {v1}, Ljava/util/zip/Inflater;->finished()Z

    move-result v1
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_3

    if-nez v1, :cond_4

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/jscape/util/h/s;->d:Ljava/util/zip/Inflater;

    invoke-virtual {v1}, Ljava/util/zip/Inflater;->finished()Z

    move-result v1
    :try_end_1
    .catch Ljava/util/zip/DataFormatException; {:try_start_1 .. :try_end_1} :catch_2

    if-nez v1, :cond_4

    if-nez v0, :cond_5

    :try_start_2
    iget-object v1, p0, Lcom/jscape/util/h/s;->d:Ljava/util/zip/Inflater;

    invoke-virtual {v1}, Ljava/util/zip/Inflater;->needsInput()Z

    move-result v1
    :try_end_2
    .catch Ljava/util/zip/DataFormatException; {:try_start_2 .. :try_end_2} :catch_0

    const/4 v2, 0x0

    if-nez v0, :cond_1

    if-nez v1, :cond_4

    :try_start_3
    iget-object v1, p0, Lcom/jscape/util/h/s;->d:Ljava/util/zip/Inflater;

    iget-object v3, p0, Lcom/jscape/util/h/s;->e:[B

    iget-object v4, p0, Lcom/jscape/util/h/s;->e:[B

    array-length v4, v4

    invoke-virtual {v1, v3, v2, v4}, Ljava/util/zip/Inflater;->inflate([BII)I

    move-result v1
    :try_end_3
    .catch Ljava/util/zip/DataFormatException; {:try_start_3 .. :try_end_3} :catch_1

    :cond_1
    if-nez v0, :cond_3

    const/4 v3, 0x1

    if-ge v1, v3, :cond_2

    goto :goto_0

    :cond_2
    :try_start_4
    iget-object v3, p0, Lcom/jscape/util/h/s;->c:Ljava/io/OutputStream;

    iget-object v4, p0, Lcom/jscape/util/h/s;->e:[B

    invoke-virtual {v3, v4, v2, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/util/zip/DataFormatException; {:try_start_4 .. :try_end_4} :catch_2

    :cond_3
    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_5
    invoke-static {v0}, Lcom/jscape/util/h/s;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_5
    .catch Ljava/util/zip/DataFormatException; {:try_start_5 .. :try_end_5} :catch_1

    :catch_1
    move-exception v0

    :try_start_6
    invoke-static {v0}, Lcom/jscape/util/h/s;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
    :try_end_6
    .catch Ljava/util/zip/DataFormatException; {:try_start_6 .. :try_end_6} :catch_2

    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/jscape/util/h/s;->c:Ljava/io/OutputStream;

    goto :goto_1

    :cond_5
    move-object v0, p0

    :goto_1
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    return-void

    :catch_3
    move-exception v0

    invoke-static {v0}, Lcom/jscape/util/h/s;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object v0

    throw v0
.end method

.method public write(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/s;->f:[B

    int-to-byte p1, p1

    const/4 v1, 0x0

    aput-byte p1, v0, v1

    const/4 p1, 0x1

    invoke-virtual {p0, v0, v1, p1}, Lcom/jscape/util/h/s;->write([BII)V

    return-void
.end method

.method public write([BII)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/jscape/util/h/s;->a()V

    move-object v1, p0

    :goto_0
    :try_start_0
    iget-object v2, v1, Lcom/jscape/util/h/s;->d:Ljava/util/zip/Inflater;

    invoke-virtual {v2}, Ljava/util/zip/Inflater;->needsInput()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v2, :cond_3

    if-nez v0, :cond_2

    if-nez v0, :cond_1

    if-ge p3, v4, :cond_0

    goto :goto_5

    :cond_0
    const/16 v2, 0x200

    goto :goto_1

    :cond_1
    move v2, v4

    :goto_1
    invoke-static {p3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v5, v1, Lcom/jscape/util/h/s;->d:Ljava/util/zip/Inflater;

    invoke-virtual {v5, p1, p2, v2}, Ljava/util/zip/Inflater;->setInput([BII)V

    add-int/2addr p2, v2

    sub-int/2addr p3, v2

    goto :goto_2

    :cond_2
    move-object v2, v1

    move-object v1, v0

    move v0, p3

    goto :goto_3

    :cond_3
    :goto_2
    iget-object v2, v1, Lcom/jscape/util/h/s;->d:Ljava/util/zip/Inflater;

    iget-object v5, v1, Lcom/jscape/util/h/s;->e:[B

    iget-object v6, v1, Lcom/jscape/util/h/s;->e:[B

    array-length v6, v6

    invoke-virtual {v2, v5, v3, v6}, Ljava/util/zip/Inflater;->inflate([BII)I

    move-result v2
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_2

    move-object v7, v0

    move v0, p3

    move p3, v2

    move-object v2, v1

    move-object v1, v7

    :goto_3
    if-lez p3, :cond_4

    :try_start_1
    iget-object v5, v2, Lcom/jscape/util/h/s;->c:Ljava/io/OutputStream;

    iget-object v6, v2, Lcom/jscape/util/h/s;->e:[B

    invoke-virtual {v5, v6, v3, p3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/util/zip/DataFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {p1}, Lcom/jscape/util/h/s;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1

    :cond_4
    :goto_4
    if-gtz p3, :cond_9

    iget-object p3, v2, Lcom/jscape/util/h/s;->d:Ljava/util/zip/Inflater;

    invoke-virtual {p3}, Ljava/util/zip/Inflater;->finished()Z

    move-result p3

    if-nez v1, :cond_4

    if-nez v1, :cond_6

    if-eqz p3, :cond_5

    goto :goto_5

    :cond_5
    iget-object p3, v2, Lcom/jscape/util/h/s;->d:Ljava/util/zip/Inflater;

    invoke-virtual {p3}, Ljava/util/zip/Inflater;->needsDictionary()Z

    move-result p3
    :try_end_2
    .catch Ljava/util/zip/DataFormatException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_6
    if-nez p3, :cond_8

    if-eqz v1, :cond_7

    :goto_5
    return-void

    :cond_7
    move p3, v0

    move-object v0, v1

    move-object v1, v2

    goto :goto_0

    :cond_8
    :try_start_3
    new-instance p1, Ljava/io/IOException;

    sget-object p2, Lcom/jscape/util/h/s;->h:[Ljava/lang/String;

    aget-object p2, p2, v4

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_3
    .catch Ljava/util/zip/DataFormatException; {:try_start_3 .. :try_end_3} :catch_1

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/util/h/s;->a(Ljava/lang/Exception;)Ljava/lang/Exception;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/util/zip/DataFormatException; {:try_start_4 .. :try_end_4} :catch_2

    :cond_9
    move p3, v0

    move-object v0, v1

    move-object v1, v2

    goto :goto_2

    :catch_2
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/X;->a(Ljava/lang/Throwable;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method
