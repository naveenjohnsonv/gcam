.class public Lcom/jscape/util/h/P;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/h/L;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private final a:Z

.field private b:[B


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "\u0015\u0005d[(\u0002d7\u0015{W?[L:\u0015oR?\t\n"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/h/P;->c:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x1b

    goto :goto_1

    :cond_1
    const/16 v4, 0x57

    goto :goto_1

    :cond_2
    const/16 v4, 0x76

    goto :goto_1

    :cond_3
    const/16 v4, 0x18

    goto :goto_1

    :cond_4
    const/16 v4, 0x25

    goto :goto_1

    :cond_5
    const/16 v4, 0x4c

    goto :goto_1

    :cond_6
    const/16 v4, 0x74

    :goto_1
    const/16 v5, 0x2c

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/jscape/util/h/P;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [B

    invoke-direct {p0, v0, p1}, Lcom/jscape/util/h/P;-><init>([BZ)V

    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/jscape/util/h/P;-><init>([BZ)V

    return-void
.end method

.method public constructor <init>([BZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/h/P;->b:[B

    iput-boolean p2, p0, Lcom/jscape/util/h/P;->a:Z

    return-void
.end method

.method static a(Lcom/jscape/util/h/P;[B)[B
    .locals 0

    iput-object p1, p0, Lcom/jscape/util/h/P;->b:[B

    return-object p1
.end method


# virtual methods
.method public a()[B
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/h/P;->b:[B

    return-object v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/e;

    iget-object v1, p0, Lcom/jscape/util/h/P;->b:[B

    invoke-direct {v0, v1}, Lcom/jscape/util/h/e;-><init>([B)V

    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/jscape/util/h/c;

    invoke-direct {v0, p0}, Lcom/jscape/util/h/c;-><init>(Lcom/jscape/util/h/P;)V

    return-object v0
.end method

.method public readAllowed()Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jscape/util/h/P;->b:[B

    array-length v1, v1

    if-nez v0, :cond_2

    if-gtz v1, :cond_1

    iget-boolean v1, p0, Lcom/jscape/util/h/P;->a:Z

    if-nez v0, :cond_2

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :cond_2
    move v0, v1

    :goto_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/util/h/P;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/util/h/P;->b:[B

    invoke-static {v1}, Lcom/jscape/util/v;->b([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeAllowed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
