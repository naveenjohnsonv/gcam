.class public Lcom/jscape/util/h/q;
.super Ljava/io/OutputStream;


# static fields
.field private static final a:[B

.field private static final b:[B

.field private static final c:[B

.field private static final h:Ljava/lang/String;


# instance fields
.field private final d:Ljava/io/OutputStream;

.field private final e:[B

.field private final f:[B

.field private final g:Lcom/jscape/util/h/S;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const-string v0, "d<\u000e\u0019\nh\u000cC}/v$)\u0013C3\rM\u0000\'"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    const/16 v4, 0xa

    const/4 v5, 0x2

    const/4 v6, 0x1

    if-gt v1, v3, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/h/q;->h:Ljava/lang/String;

    new-array v0, v5, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jscape/util/h/q;->a:[B

    new-array v0, v6, [B

    aput-byte v4, v0, v2

    sput-object v0, Lcom/jscape/util/h/q;->b:[B

    new-array v0, v6, [B

    const/16 v1, 0xd

    aput-byte v1, v0, v2

    sput-object v0, Lcom/jscape/util/h/q;->c:[B

    return-void

    :cond_0
    aget-char v7, v0, v3

    rem-int/lit8 v8, v3, 0x7

    if-eqz v8, :cond_5

    if-eq v8, v6, :cond_6

    if-eq v8, v5, :cond_4

    const/4 v4, 0x3

    if-eq v8, v4, :cond_3

    const/4 v4, 0x4

    if-eq v8, v4, :cond_2

    const/4 v4, 0x5

    if-eq v8, v4, :cond_1

    const/16 v4, 0x28

    goto :goto_1

    :cond_1
    const/16 v4, 0x5e

    goto :goto_1

    :cond_2
    const/16 v4, 0x3f

    goto :goto_1

    :cond_3
    const/16 v4, 0x6e

    goto :goto_1

    :cond_4
    const/16 v4, 0x3d

    goto :goto_1

    :cond_5
    const/16 v4, 0x71

    :cond_6
    :goto_1
    const/16 v5, 0x57

    xor-int/2addr v4, v5

    xor-int/2addr v4, v7

    int-to-char v4, v4

    aput-char v4, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :array_0
    .array-data 1
        0xdt
        0xat
    .end array-data
.end method

.method public constructor <init>(Ljava/io/OutputStream;[B[B)V
    .locals 4

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    iput-object p1, p0, Lcom/jscape/util/h/q;->d:Ljava/io/OutputStream;

    array-length p1, p2

    int-to-long v0, p1

    sget-object p1, Lcom/jscape/util/h/q;->h:Ljava/lang/String;

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3, p1}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    iput-object p2, p0, Lcom/jscape/util/h/q;->e:[B

    invoke-static {p3}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/jscape/util/h/q;->f:[B

    new-instance p1, Lcom/jscape/util/h/S;

    iget-object p2, p0, Lcom/jscape/util/h/q;->e:[B

    array-length p2, p2

    invoke-direct {p1, p2}, Lcom/jscape/util/h/S;-><init>(I)V

    iput-object p1, p0, Lcom/jscape/util/h/q;->g:Lcom/jscape/util/h/S;

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_0

    const-string p1, "fMh2Pb"

    invoke-static {p1}, Lcom/jscape/util/h/o;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static a(Ljava/io/OutputStream;[B)Lcom/jscape/util/h/q;
    .locals 4

    new-instance v0, Lcom/jscape/util/h/q;

    new-instance v1, Lcom/jscape/util/h/q;

    new-instance v2, Lcom/jscape/util/h/q;

    sget-object v3, Lcom/jscape/util/h/q;->b:[B

    invoke-direct {v2, p0, v3, p1}, Lcom/jscape/util/h/q;-><init>(Ljava/io/OutputStream;[B[B)V

    sget-object p0, Lcom/jscape/util/h/q;->c:[B

    sget-object v3, Lcom/jscape/util/h/q;->b:[B

    invoke-direct {v1, v2, p0, v3}, Lcom/jscape/util/h/q;-><init>(Ljava/io/OutputStream;[B[B)V

    sget-object p0, Lcom/jscape/util/h/q;->a:[B

    invoke-direct {v0, v1, p0, p1}, Lcom/jscape/util/h/q;-><init>(Ljava/io/OutputStream;[B[B)V

    return-object v0
.end method

.method private static a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 0

    return-object p0
.end method


# virtual methods
.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/h/q;->g:Lcom/jscape/util/h/S;

    invoke-virtual {v1}, Lcom/jscape/util/h/S;->c()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/jscape/util/h/q;->g:Lcom/jscape/util/h/S;

    invoke-virtual {v1}, Lcom/jscape/util/h/S;->d()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    and-int/lit16 v1, v1, 0xff

    :try_start_1
    iget-object v2, p0, Lcom/jscape/util/h/q;->d:Ljava/io/OutputStream;

    invoke-virtual {v2, v1}, Ljava/io/OutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_2

    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/jscape/util/h/q;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/jscape/util/h/q;->d:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jscape/util/h/q;->d:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    throw v0
.end method

.method public flush()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jscape/util/h/q;->d:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

.method public write(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/jscape/util/h/q;->g:Lcom/jscape/util/h/S;

    invoke-virtual {v1, p1}, Lcom/jscape/util/h/S;->a(I)I

    iget-object p1, p0, Lcom/jscape/util/h/q;->g:Lcom/jscape/util/h/S;

    iget-object v1, p0, Lcom/jscape/util/h/q;->e:[B

    invoke-virtual {p1, v1}, Lcom/jscape/util/h/S;->a([B)Z

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    :try_start_1
    iget-object p1, p0, Lcom/jscape/util/h/q;->g:Lcom/jscape/util/h/S;

    invoke-virtual {p1}, Lcom/jscape/util/h/S;->e()V

    iget-object p1, p0, Lcom/jscape/util/h/q;->d:Ljava/io/OutputStream;

    iget-object v1, p0, Lcom/jscape/util/h/q;->f:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    if-eqz v0, :cond_3

    :cond_0
    :try_start_2
    iget-object p1, p0, Lcom/jscape/util/h/q;->g:Lcom/jscape/util/h/S;

    invoke-virtual {p1}, Lcom/jscape/util/h/S;->b()Z

    move-result p1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    :cond_1
    if-nez v0, :cond_2

    if-eqz p1, :cond_3

    :try_start_3
    iget-object p1, p0, Lcom/jscape/util/h/q;->g:Lcom/jscape/util/h/S;

    invoke-virtual {p1}, Lcom/jscape/util/h/S;->d()I

    move-result p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/q;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/jscape/util/h/q;->d:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    :cond_3
    return-void

    :catch_1
    move-exception p1

    :try_start_4
    invoke-static {p1}, Lcom/jscape/util/h/q;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    move-exception p1

    :try_start_5
    invoke-static {p1}, Lcom/jscape/util/h/q;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :catch_3
    move-exception p1

    invoke-static {p1}, Lcom/jscape/util/h/q;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method
