.class public Lcom/jscape/util/h/S;
.super Ljava/lang/Object;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field private final a:[B

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-string v0, "L,Z\n-h\th(L\n<t\u0015kmHK#h\n "

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-gt v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jscape/util/h/S;->e:Ljava/lang/String;

    return-void

    :cond_0
    aget-char v3, v0, v2

    rem-int/lit8 v4, v2, 0x7

    if-eqz v4, :cond_6

    const/4 v5, 0x1

    if-eq v4, v5, :cond_5

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_3

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const/16 v4, 0x9

    goto :goto_1

    :cond_1
    const/16 v4, 0x7b

    goto :goto_1

    :cond_2
    const/16 v4, 0x29

    goto :goto_1

    :cond_3
    const/16 v4, 0x4c

    goto :goto_1

    :cond_4
    const/16 v4, 0x58

    goto :goto_1

    :cond_5
    const/16 v4, 0x2b

    goto :goto_1

    :cond_6
    const/16 v4, 0x68

    :goto_1
    const/16 v5, 0x66

    xor-int/2addr v4, v5

    xor-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    int-to-long v0, p1

    sget-object v2, Lcom/jscape/util/h/S;->e:Ljava/lang/String;

    const-wide/16 v3, 0x0

    invoke-static {v0, v1, v3, v4, v2}, Lcom/jscape/util/aq;->a(JJLjava/lang/String;)V

    new-array p1, p1, [B

    iput-object p1, p0, Lcom/jscape/util/h/S;->a:[B

    return-void
.end method

.method private f()V
    .locals 2

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/jscape/util/h/S;->b:I

    iget-object v1, p0, Lcom/jscape/util/h/S;->a:[B

    array-length v1, v1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/util/h/S;->b:I

    :cond_1
    return-void
.end method

.method private g()V
    .locals 2

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/jscape/util/h/S;->c:I

    iget-object v1, p0, Lcom/jscape/util/h/S;->a:[B

    array-length v1, v1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/util/h/S;->c:I

    :cond_1
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lcom/jscape/util/h/S;->a:[B

    array-length v0, v0

    return v0
.end method

.method public a(I)I
    .locals 3

    iget-object v0, p0, Lcom/jscape/util/h/S;->a:[B

    iget v1, p0, Lcom/jscape/util/h/S;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/jscape/util/h/S;->b:I

    int-to-byte p1, p1

    aput-byte p1, v0, v1

    invoke-direct {p0}, Lcom/jscape/util/h/S;->f()V

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object p1

    iget v0, p0, Lcom/jscape/util/h/S;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/jscape/util/h/S;->d:I

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/jscape/util/h/S;->a:[B

    array-length p1, p1

    if-gt v0, p1, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/jscape/util/h/S;->d()I

    move-result p1

    :goto_0
    return p1
.end method

.method public a([B)Z
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/jscape/util/h/S;->a([BII)Z

    move-result p1

    return p1
.end method

.method public a([BII)Z
    .locals 8

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iget v2, p0, Lcom/jscape/util/h/S;->d:I

    if-eq p3, v2, :cond_0

    return v1

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v2, p3

    :goto_0
    iget v3, p0, Lcom/jscape/util/h/S;->c:I

    :cond_2
    const/4 v4, 0x1

    if-ge v2, p3, :cond_7

    if-nez v0, :cond_8

    iget-object v5, p0, Lcom/jscape/util/h/S;->a:[B

    array-length v5, v5

    if-nez v0, :cond_5

    if-ne v3, v5, :cond_3

    move v3, v1

    :cond_3
    add-int v5, p2, v2

    aget-byte v5, p1, v5

    if-nez v0, :cond_4

    iget-object v6, p0, Lcom/jscape/util/h/S;->a:[B

    aget-byte v6, v6, v3

    move v7, v5

    move v5, v3

    move v3, v7

    goto :goto_1

    :cond_4
    move v1, v5

    goto :goto_2

    :cond_5
    move v6, v5

    move v5, v3

    :goto_1
    if-eq v3, v6, :cond_6

    :goto_2
    return v1

    :cond_6
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, v5, 0x1

    if-eqz v0, :cond_2

    :cond_7
    move v3, v4

    :cond_8
    return v3
.end method

.method public b()Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/util/h/S;->d:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jscape/util/h/S;->a:[B

    array-length v0, v0

    if-ne v1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public c()Z
    .locals 2

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jscape/util/h/S;->d:I

    if-nez v0, :cond_1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v1

    :goto_1
    return v0
.end method

.method public d()I
    .locals 3

    invoke-static {}, Lcom/jscape/util/h/o;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jscape/util/h/S;->c()Z

    move-result v1

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/jscape/util/h/S;->a:[B

    iget v1, p0, Lcom/jscape/util/h/S;->c:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/jscape/util/h/S;->c:I

    aget-byte v0, v0, v1

    and-int/lit16 v1, v0, 0xff

    :cond_1
    invoke-direct {p0}, Lcom/jscape/util/h/S;->g()V

    iget v0, p0, Lcom/jscape/util/h/S;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/jscape/util/h/S;->d:I

    return v1
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/jscape/util/h/S;->b:I

    iput v0, p0, Lcom/jscape/util/h/S;->c:I

    iput v0, p0, Lcom/jscape/util/h/S;->d:I

    return-void
.end method
