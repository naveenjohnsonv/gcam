.class public final Lcom/jscape/util/W;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/util/z;


# static fields
.field public static final a:Ljava/lang/String; = ":"


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, ":"

    invoke-direct {p0, v0}, Lcom/jscape/util/W;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/util/W;->b:Ljava/lang/String;

    return-void
.end method

.method public static a([B)Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    const-string p0, ""

    return-object p0

    :cond_0
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Lcom/jscape/util/W;->a([BII)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a([BII)Ljava/lang/String;
    .locals 1

    const-string v0, ":"

    invoke-static {p0, p1, p2, v0}, Lcom/jscape/util/W;->a([BIILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a([BIILjava/lang/String;)Ljava/lang/String;
    .locals 1

    new-instance v0, Lcom/jscape/util/W;

    invoke-direct {v0, p3}, Lcom/jscape/util/W;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, p1, p2}, Lcom/jscape/util/W;->b([BII)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a([BLjava/lang/String;)Ljava/lang/String;
    .locals 2

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p0, :cond_0

    const-string p0, ""

    return-object p0

    :cond_0
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1, p1}, Lcom/jscape/util/W;->a([BIILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;)[B
    .locals 1

    const-string v0, ":"

    invoke-static {p0, v0}, Lcom/jscape/util/W;->a(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 1

    new-instance v0, Lcom/jscape/util/W;

    invoke-direct {v0, p1}, Lcom/jscape/util/W;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Lcom/jscape/util/W;->parse(Ljava/lang/String;)[B

    move-result-object p0

    return-object p0
.end method

.method private b(Ljava/lang/String;)[B
    .locals 8

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    div-int/2addr v0, v1

    new-array v0, v0, [B

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v2

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v3, v5, :cond_3

    add-int/lit8 v5, v3, 0x2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v2, :cond_1

    if-gt v5, v6, :cond_0

    move v6, v1

    move v7, v3

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    goto :goto_2

    :cond_1
    move v7, v5

    :goto_1
    add-int/2addr v6, v7

    :goto_2
    invoke-virtual {p1, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const/16 v6, 0x10

    invoke-static {v3, v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v0, v4

    add-int/lit8 v4, v4, 0x1

    if-eqz v2, :cond_2

    goto :goto_3

    :cond_2
    move v3, v5

    goto :goto_0

    :cond_3
    :goto_3
    return-object v0
.end method


# virtual methods
.method public b([BII)Ljava/lang/String;
    .locals 7

    invoke-static {}, Lcom/jscape/util/X;->c()Z

    move-result v0

    const-string v1, ""

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    return-object v1

    :cond_0
    invoke-static {p1, p2, p3}, Lcom/jscape/util/aq;->a([BII)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    :cond_1
    if-ge v3, p3, :cond_4

    if-nez v0, :cond_4

    if-lez v3, :cond_2

    iget-object v4, p0, Lcom/jscape/util/W;->b:Ljava/lang/String;

    goto :goto_0

    :cond_2
    move-object v4, v1

    :goto_0
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int v4, p2, v3

    aget-byte v5, p1, v4

    and-int/lit16 v5, v5, 0xff

    const/16 v6, 0xf

    if-gt v5, v6, :cond_3

    const-string v5, "0"

    goto :goto_1

    :cond_3
    move-object v5, v1

    :goto_1
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    if-eqz v0, :cond_1

    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public format([B)Ljava/lang/String;
    .locals 2

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    :cond_0
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/jscape/util/W;->b([BII)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public parse(Ljava/lang/String;)[B
    .locals 5

    invoke-static {}, Lcom/jscape/util/X;->b()Z

    move-result v0

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/jscape/util/W;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0, p1}, Lcom/jscape/util/W;->b(Ljava/lang/String;)[B

    move-result-object p1

    return-object p1

    :cond_0
    move-object p1, v1

    :cond_1
    iget-object v1, p0, Lcom/jscape/util/W;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/jscape/util/at;->a(Ljava/lang/String;Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object p1

    array-length v1, p1

    new-array v1, v1, [B

    :cond_2
    array-length v3, p1

    if-ge v2, v3, :cond_3

    if-eqz v0, :cond_3

    aget-object v3, p1, v2

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_2

    :cond_3
    return-object v1
.end method
