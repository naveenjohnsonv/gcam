.class public Lcom/jscape/a/b;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/a/d;


# static fields
.field private static final a:I = 0x4


# instance fields
.field private final b:Ljava/util/zip/CRC32;

.field private final c:Lcom/jscape/util/K;


# direct methods
.method public constructor <init>(Lcom/jscape/util/K;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    iput-object v0, p0, Lcom/jscape/a/b;->b:Ljava/util/zip/CRC32;

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/a/b;->c:Lcom/jscape/util/K;

    return-void
.end method

.method public static d()Lcom/jscape/a/b;
    .locals 2

    new-instance v0, Lcom/jscape/a/b;

    new-instance v1, Lcom/jscape/util/y;

    invoke-direct {v1}, Lcom/jscape/util/y;-><init>()V

    invoke-direct {v0, v1}, Lcom/jscape/a/b;-><init>(Lcom/jscape/util/K;)V

    return-object v0
.end method

.method public static e()Lcom/jscape/a/b;
    .locals 2

    new-instance v0, Lcom/jscape/a/b;

    new-instance v1, Lcom/jscape/util/ac;

    invoke-direct {v1}, Lcom/jscape/util/ac;-><init>()V

    invoke-direct {v0, v1}, Lcom/jscape/a/b;-><init>(Lcom/jscape/util/K;)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public a(B)Lcom/jscape/a/d;
    .locals 1

    iget-object v0, p0, Lcom/jscape/a/b;->b:Ljava/util/zip/CRC32;

    invoke-virtual {v0, p1}, Ljava/util/zip/CRC32;->update(I)V

    return-object p0
.end method

.method public a([B)Lcom/jscape/a/d;
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/jscape/a/b;->a([BII)Lcom/jscape/a/d;

    move-result-object p1

    return-object p1
.end method

.method public a([BII)Lcom/jscape/a/d;
    .locals 1

    iget-object v0, p0, Lcom/jscape/a/b;->b:Ljava/util/zip/CRC32;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/zip/CRC32;->update([BII)V

    return-object p0
.end method

.method public b()Lcom/jscape/a/d;
    .locals 1

    iget-object v0, p0, Lcom/jscape/a/b;->b:Ljava/util/zip/CRC32;

    invoke-virtual {v0}, Ljava/util/zip/CRC32;->reset()V

    return-object p0
.end method

.method public b([B)[B
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/jscape/a/b;->b([BII)[B

    move-result-object p1

    return-object p1
.end method

.method public b([BII)[B
    .locals 0

    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/a/b;->a([BII)Lcom/jscape/a/d;

    invoke-virtual {p0}, Lcom/jscape/a/b;->c()[B

    move-result-object p1

    return-object p1
.end method

.method public c()[B
    .locals 4

    iget-object v0, p0, Lcom/jscape/a/b;->b:Ljava/util/zip/CRC32;

    invoke-virtual {v0}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-virtual {p0}, Lcom/jscape/a/b;->b()Lcom/jscape/a/d;

    iget-object v1, p0, Lcom/jscape/a/b;->c:Lcom/jscape/util/K;

    const/4 v2, 0x4

    new-array v2, v2, [B

    const/4 v3, 0x0

    invoke-interface {v1, v0, v2, v3}, Lcom/jscape/util/K;->a(I[BI)[B

    move-result-object v0

    return-object v0
.end method
