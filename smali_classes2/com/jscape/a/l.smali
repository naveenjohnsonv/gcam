.class public Lcom/jscape/a/l;
.super Ljava/lang/Object;


# static fields
.field public static final a:I = 0x40

.field public static final b:I = 0x10

.field public static final c:I = 0x50

.field public static final d:I = 0x8

.field private static final e:I = 0x67452301

.field private static final f:I = -0x10325477

.field private static final g:I = -0x67452302

.field private static final h:I = 0x10325476

.field private static final i:I = -0x3c2d1e10

.field private static final j:I = 0x5a827999

.field private static final k:I = 0x6ed9eba1

.field private static final l:I = -0x70e44324

.field private static final m:I = -0x359d3e2a

.field private static final s:[Ljava/lang/String;


# instance fields
.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/String;

    const/4 v3, 0x4

    const-string v5, "H\u0004\u0018\u0011\u00157l:\u001d\u001b]p\u000bV\u0012X2\\^\tT\u0017\u000c!P*\u0004H\u0004\u001f\u0011"

    const/16 v6, 0x1f

    move v8, v3

    const/4 v7, -0x1

    const/4 v9, 0x0

    :goto_0
    const/16 v10, 0x2a

    const/4 v11, 0x1

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, -0x1

    :goto_1
    invoke-virtual {v12}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    array-length v14, v12

    const/4 v15, 0x0

    :goto_2
    if-gt v14, v15, :cond_3

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v12}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v10

    add-int/lit8 v12, v9, 0x1

    if-eqz v13, :cond_1

    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_0

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v12

    goto :goto_0

    :cond_0
    const/16 v6, 0x9

    const-string v5, "S\u001f\u0005\n\u0004S\u001f\u0002\n"

    move v8, v3

    move v9, v12

    const/4 v7, -0x1

    goto :goto_3

    :cond_1
    aput-object v10, v1, v9

    add-int/2addr v7, v8

    if-ge v7, v6, :cond_2

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    move v9, v12

    :goto_3
    const/16 v10, 0x31

    add-int/2addr v7, v11

    add-int v12, v7, v8

    invoke-virtual {v5, v7, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/a/l;->s:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v16, v12, v15

    rem-int/lit8 v2, v15, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v11, :cond_8

    const/4 v4, 0x2

    if-eq v2, v4, :cond_7

    const/4 v4, 0x3

    if-eq v2, v4, :cond_6

    if-eq v2, v3, :cond_5

    if-eq v2, v0, :cond_4

    const/16 v2, 0x3d

    goto :goto_4

    :cond_4
    const/16 v2, 0x1b

    goto :goto_4

    :cond_5
    const/16 v2, 0x70

    goto :goto_4

    :cond_6
    const/4 v2, 0x6

    goto :goto_4

    :cond_7
    const/16 v2, 0x51

    goto :goto_4

    :cond_8
    const/16 v2, 0xe

    goto :goto_4

    :cond_9
    const/16 v2, 0x4e

    :goto_4
    xor-int/2addr v2, v10

    xor-int v2, v16, v2

    int-to-char v2, v2

    aput-char v2, v12, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0}, Lcom/jscape/a/l;->g()V

    return-void
.end method

.method private a(III)I
    .locals 0

    xor-int/2addr p2, p3

    and-int/2addr p1, p2

    xor-int/2addr p1, p3

    return p1
.end method

.method private b(III)I
    .locals 0

    xor-int/2addr p1, p2

    xor-int/2addr p1, p3

    return p1
.end method

.method private c(III)I
    .locals 1

    and-int v0, p1, p2

    and-int/2addr p1, p3

    or-int/2addr p1, v0

    and-int/2addr p2, p3

    or-int/2addr p1, p2

    return p1
.end method

.method private d(III)I
    .locals 0

    xor-int/2addr p1, p2

    xor-int/2addr p1, p3

    return p1
.end method

.method private g()V
    .locals 1

    const v0, 0x67452301

    iput v0, p0, Lcom/jscape/a/l;->n:I

    const v0, -0x10325477

    iput v0, p0, Lcom/jscape/a/l;->o:I

    const v0, -0x67452302

    iput v0, p0, Lcom/jscape/a/l;->p:I

    const v0, 0x10325476

    iput v0, p0, Lcom/jscape/a/l;->q:I

    const v0, -0x3c2d1e10

    iput v0, p0, Lcom/jscape/a/l;->r:I

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/a/l;->g()V

    return-void
.end method

.method public a([I)V
    .locals 10

    iget v0, p0, Lcom/jscape/a/l;->n:I

    iget v1, p0, Lcom/jscape/a/l;->o:I

    invoke-static {}, Lcom/jscape/a/f;->d()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jscape/a/l;->p:I

    iget v4, p0, Lcom/jscape/a/l;->q:I

    iget v5, p0, Lcom/jscape/a/l;->r:I

    const/4 v6, 0x0

    :cond_0
    array-length v7, p1

    if-ge v6, v7, :cond_a

    const/16 v7, 0x10

    if-nez v2, :cond_2

    if-lt v6, v7, :cond_1

    add-int/lit8 v7, v6, -0x3

    aget v7, p1, v7

    add-int/lit8 v8, v6, -0x8

    aget v8, p1, v8

    xor-int/2addr v7, v8

    add-int/lit8 v8, v6, -0xe

    aget v8, p1, v8

    xor-int/2addr v7, v8

    add-int/lit8 v8, v6, -0x10

    aget v8, p1, v8

    xor-int/2addr v7, v8

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/jscape/a/c;->a(II)I

    move-result v7

    aput v7, p1, v6

    :cond_1
    const/16 v7, 0x14

    :cond_2
    const/4 v8, 0x5

    if-nez v2, :cond_4

    if-ge v6, v7, :cond_3

    invoke-static {v0, v8}, Lcom/jscape/a/c;->a(II)I

    move-result v7

    invoke-direct {p0, v1, v3, v4}, Lcom/jscape/a/l;->a(III)I

    move-result v9

    add-int/2addr v7, v9

    add-int/2addr v7, v5

    aget v9, p1, v6

    add-int/2addr v7, v9

    const v9, 0x5a827999

    add-int/2addr v7, v9

    if-eqz v2, :cond_9

    :cond_3
    const/16 v7, 0x28

    :cond_4
    if-nez v2, :cond_6

    if-ge v6, v7, :cond_5

    invoke-static {v0, v8}, Lcom/jscape/a/c;->a(II)I

    move-result v7

    invoke-direct {p0, v1, v3, v4}, Lcom/jscape/a/l;->b(III)I

    move-result v9

    add-int/2addr v7, v9

    add-int/2addr v7, v5

    aget v9, p1, v6

    add-int/2addr v7, v9

    const v9, 0x6ed9eba1

    add-int/2addr v7, v9

    if-eqz v2, :cond_9

    :cond_5
    const/16 v7, 0x3c

    :cond_6
    if-nez v2, :cond_8

    if-ge v6, v7, :cond_7

    invoke-static {v0, v8}, Lcom/jscape/a/c;->a(II)I

    move-result v7

    invoke-direct {p0, v1, v3, v4}, Lcom/jscape/a/l;->c(III)I

    move-result v9

    add-int/2addr v7, v9

    add-int/2addr v7, v5

    aget v9, p1, v6

    add-int/2addr v7, v9

    const v9, -0x70e44324

    add-int/2addr v7, v9

    if-eqz v2, :cond_9

    :cond_7
    invoke-static {v0, v8}, Lcom/jscape/a/c;->a(II)I

    move-result v7

    invoke-direct {p0, v1, v3, v4}, Lcom/jscape/a/l;->d(III)I

    move-result v8

    add-int/2addr v7, v8

    add-int/2addr v7, v5

    aget v5, p1, v6

    add-int/2addr v7, v5

    const v5, -0x359d3e2a    # -3715189.5f

    goto :goto_0

    :cond_8
    move v5, v7

    move v7, v6

    :goto_0
    add-int/2addr v7, v5

    :cond_9
    const/16 v5, 0x1e

    invoke-static {v1, v5}, Lcom/jscape/a/c;->a(II)I

    move-result v1

    add-int/lit8 v6, v6, 0x1

    move v5, v4

    move v4, v3

    move v3, v1

    move v1, v0

    move v0, v7

    if-eqz v2, :cond_0

    :cond_a
    iget p1, p0, Lcom/jscape/a/l;->n:I

    add-int/2addr p1, v0

    iput p1, p0, Lcom/jscape/a/l;->n:I

    iget p1, p0, Lcom/jscape/a/l;->o:I

    add-int/2addr p1, v1

    iput p1, p0, Lcom/jscape/a/l;->o:I

    iget p1, p0, Lcom/jscape/a/l;->p:I

    add-int/2addr p1, v3

    iput p1, p0, Lcom/jscape/a/l;->p:I

    iget p1, p0, Lcom/jscape/a/l;->q:I

    add-int/2addr p1, v4

    iput p1, p0, Lcom/jscape/a/l;->q:I

    iget p1, p0, Lcom/jscape/a/l;->r:I

    add-int/2addr p1, v5

    iput p1, p0, Lcom/jscape/a/l;->r:I

    invoke-static {}, Lcom/jscape/util/aq;->b()[I

    move-result-object p1

    if-nez p1, :cond_b

    const-string p1, "vhYrwb"

    invoke-static {p1}, Lcom/jscape/a/f;->b(Ljava/lang/String;)V

    :cond_b
    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/jscape/a/l;->n:I

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/jscape/a/l;->o:I

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/jscape/a/l;->p:I

    return v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/jscape/a/l;->q:I

    return v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Lcom/jscape/a/l;->r:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/a/l;->s:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/a/l;->n:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x4

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/a/l;->o:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/a/l;->p:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/a/l;->q:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/a/l;->r:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
