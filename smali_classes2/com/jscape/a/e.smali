.class public Lcom/jscape/a/e;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 0

    return-object p0
.end method

.method public static a(Ljava/io/File;Lcom/jscape/a/d;)[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Ljava/io/File;->toPath()Ljava/nio/file/Path;

    move-result-object p0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/nio/file/OpenOption;

    invoke-static {p0, v0}, Ljava/nio/file/Files;->newInputStream(Ljava/nio/file/Path;[Ljava/nio/file/OpenOption;)Ljava/io/InputStream;

    move-result-object p0

    invoke-static {}, Lcom/jscape/a/f;->d()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {p0, p1}, Lcom/jscape/a/e;->a(Ljava/io/InputStream;Lcom/jscape/a/d;)[B

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz p0, :cond_0

    :try_start_1
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/a/e;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_0
    :goto_0
    return-object p1

    :catchall_1
    move-exception p1

    :try_start_2
    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :catchall_2
    move-exception v1

    if-eqz p0, :cond_1

    :try_start_3
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v2

    :try_start_4
    invoke-virtual {p1, v2}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    goto :goto_1

    :catchall_4
    move-exception p0

    invoke-static {p0}, Lcom/jscape/a/e;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_1
    :goto_1
    throw v1
.end method

.method public static a(Ljava/io/InputStream;Lcom/jscape/a/d;)[B
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p1}, Lcom/jscape/a/d;->b()Lcom/jscape/a/d;

    invoke-static {}, Lcom/jscape/a/f;->d()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2000

    new-array v1, v1, [B

    const/4 v2, 0x0

    move v3, v2

    :cond_0
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    invoke-interface {p1, v1, v2, v4}, Lcom/jscape/a/d;->a([BII)Lcom/jscape/a/d;

    const/4 v3, 0x1

    if-nez v0, :cond_3

    if-eqz v0, :cond_0

    :cond_1
    if-nez v0, :cond_2

    if-eqz v3, :cond_3

    :try_start_0
    invoke-interface {p1}, Lcom/jscape/a/d;->c()[B

    move-result-object p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/a/e;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_2
    move v2, v3

    :cond_3
    new-array p0, v2, [B

    :goto_0
    return-object p0
.end method

.method public static a(Ljava/io/InputStream;Lcom/jscape/a/d;J)[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/jscape/a/f;->d()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/jscape/util/h/o;

    invoke-direct {v1}, Lcom/jscape/util/h/o;-><init>()V

    :cond_0
    new-instance v2, Lcom/jscape/util/h/j;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p2, p3, v3}, Lcom/jscape/util/h/j;-><init>(Ljava/io/InputStream;JZ)V

    invoke-static {v2, p1}, Lcom/jscape/a/e;->a(Ljava/io/InputStream;Lcom/jscape/a/d;)[B

    move-result-object v2

    array-length v3, v2

    if-lez v3, :cond_1

    if-nez v0, :cond_1

    :try_start_0
    invoke-virtual {v1, v2}, Lcom/jscape/util/h/o;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {p0}, Lcom/jscape/a/e;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0

    :cond_1
    :goto_0
    invoke-virtual {v1}, Lcom/jscape/util/h/o;->d()[B

    move-result-object p0

    return-object p0
.end method

.method public static b(Ljava/io/File;Lcom/jscape/a/d;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/jscape/a/e;->a(Ljava/io/File;Lcom/jscape/a/d;)[B

    move-result-object p0

    const-string p1, ""

    invoke-static {p0, p1}, Lcom/jscape/util/W;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
