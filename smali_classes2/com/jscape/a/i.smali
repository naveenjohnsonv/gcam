.class public Lcom/jscape/a/i;
.super Ljava/lang/Object;


# static fields
.field public static final a:I = 0x40

.field public static final b:I = 0x10

.field public static final c:I = 0x8

.field private static final d:I = 0x67452301

.field private static final e:I = -0x10325477

.field private static final f:I = -0x67452302

.field private static final g:I = 0x10325476

.field private static final l:[Ljava/lang/String;


# instance fields
.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/String;

    const-string v4, "\u0007s\u000b!\u0004\u0007s\r!"

    const/16 v5, 0x9

    move v7, v0

    const/4 v6, -0x1

    const/4 v8, 0x0

    :goto_0
    const/16 v9, 0x1d

    const/4 v10, 0x1

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, -0x1

    :goto_1
    invoke-virtual {v11}, Ljava/lang/String;->toCharArray()[C

    move-result-object v11

    array-length v13, v11

    const/4 v14, 0x0

    :goto_2
    if-gt v13, v14, :cond_3

    new-instance v9, Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v9}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v11, v8, 0x1

    if-eqz v12, :cond_1

    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    goto :goto_0

    :cond_0
    const/16 v5, 0x15

    const-string v4, "3G>\u0015\u0010R#hiyvqm\u000e)@x1e~Z"

    move v7, v0

    move v8, v11

    const/4 v6, -0x1

    goto :goto_3

    :cond_1
    aput-object v9, v1, v8

    add-int/2addr v6, v7

    if-ge v6, v5, :cond_2

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v7

    move v8, v11

    :goto_3
    const/16 v9, 0x29

    add-int/2addr v6, v10

    add-int v11, v6, v7

    invoke-virtual {v4, v6, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    sput-object v1, Lcom/jscape/a/i;->l:[Ljava/lang/String;

    return-void

    :cond_3
    aget-char v15, v11, v14

    rem-int/lit8 v2, v14, 0x7

    if-eqz v2, :cond_9

    if-eq v2, v10, :cond_8

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    const/4 v3, 0x3

    if-eq v2, v3, :cond_6

    if-eq v2, v0, :cond_5

    const/4 v3, 0x5

    if-eq v2, v3, :cond_4

    const/16 v2, 0x37

    goto :goto_4

    :cond_4
    const/16 v2, 0x38

    goto :goto_4

    :cond_5
    const/16 v2, 0x3c

    goto :goto_4

    :cond_6
    move v2, v10

    goto :goto_4

    :cond_7
    const/16 v2, 0x74

    goto :goto_4

    :cond_8
    const/16 v2, 0x4e

    goto :goto_4

    :cond_9
    const/16 v2, 0x36

    :goto_4
    xor-int/2addr v2, v9

    xor-int/2addr v2, v15

    int-to-char v2, v2

    aput-char v2, v11, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0}, Lcom/jscape/a/i;->f()V

    return-void
.end method

.method private a(III)I
    .locals 0

    xor-int/2addr p2, p3

    and-int/2addr p1, p2

    xor-int/2addr p1, p3

    return p1
.end method

.method private a(IIIIIII)I
    .locals 0

    invoke-direct {p0, p2, p3, p4}, Lcom/jscape/a/i;->a(III)I

    move-result p3

    add-int/2addr p1, p3

    add-int/2addr p1, p5

    add-int/2addr p1, p7

    invoke-static {p1, p6}, Lcom/jscape/a/c;->a(II)I

    move-result p1

    add-int/2addr p2, p1

    return p2
.end method

.method private b(III)I
    .locals 0

    xor-int/2addr p1, p2

    and-int/2addr p1, p3

    xor-int/2addr p1, p2

    return p1
.end method

.method private b(IIIIIII)I
    .locals 0

    invoke-direct {p0, p2, p3, p4}, Lcom/jscape/a/i;->b(III)I

    move-result p3

    add-int/2addr p1, p3

    add-int/2addr p1, p5

    add-int/2addr p1, p7

    invoke-static {p1, p6}, Lcom/jscape/a/c;->a(II)I

    move-result p1

    add-int/2addr p2, p1

    return p2
.end method

.method private c(III)I
    .locals 0

    xor-int/2addr p1, p2

    xor-int/2addr p1, p3

    return p1
.end method

.method private c(IIIIIII)I
    .locals 0

    invoke-direct {p0, p2, p3, p4}, Lcom/jscape/a/i;->c(III)I

    move-result p3

    add-int/2addr p1, p3

    add-int/2addr p1, p5

    add-int/2addr p1, p7

    invoke-static {p1, p6}, Lcom/jscape/a/c;->a(II)I

    move-result p1

    add-int/2addr p2, p1

    return p2
.end method

.method private d(III)I
    .locals 0

    not-int p3, p3

    or-int/2addr p1, p3

    xor-int/2addr p1, p2

    return p1
.end method

.method private d(IIIIIII)I
    .locals 0

    invoke-direct {p0, p2, p3, p4}, Lcom/jscape/a/i;->d(III)I

    move-result p3

    add-int/2addr p1, p3

    add-int/2addr p1, p5

    add-int/2addr p1, p7

    invoke-static {p1, p6}, Lcom/jscape/a/c;->a(II)I

    move-result p1

    add-int/2addr p2, p1

    return p2
.end method

.method private f()V
    .locals 1

    const v0, 0x67452301

    iput v0, p0, Lcom/jscape/a/i;->h:I

    const v0, -0x10325477

    iput v0, p0, Lcom/jscape/a/i;->i:I

    const v0, -0x67452302

    iput v0, p0, Lcom/jscape/a/i;->j:I

    const v0, 0x10325476

    iput v0, p0, Lcom/jscape/a/i;->k:I

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-direct {p0}, Lcom/jscape/a/i;->f()V

    return-void
.end method

.method public a([I)V
    .locals 30

    move-object/from16 v8, p0

    iget v1, v8, Lcom/jscape/a/i;->h:I

    iget v9, v8, Lcom/jscape/a/i;->i:I

    iget v10, v8, Lcom/jscape/a/i;->j:I

    iget v11, v8, Lcom/jscape/a/i;->k:I

    const/4 v12, 0x0

    aget v5, p1, v12

    const/4 v6, 0x7

    const v7, -0x28955b88

    move-object/from16 v0, p0

    move v2, v9

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->a(IIIIIII)I

    move-result v13

    const/4 v14, 0x1

    aget v5, p1, v14

    const/16 v6, 0xc

    const v7, -0x173848aa

    move v1, v11

    move v2, v13

    move v3, v9

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->a(IIIIIII)I

    move-result v11

    const/4 v15, 0x2

    aget v5, p1, v15

    const/16 v6, 0x11

    const v7, 0x242070db

    move v1, v10

    move v2, v11

    move v3, v13

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->a(IIIIIII)I

    move-result v10

    const/16 v16, 0x3

    aget v5, p1, v16

    const/16 v6, 0x16

    const v7, -0x3e423112

    move v1, v9

    move v2, v10

    move v3, v11

    move v4, v13

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->a(IIIIIII)I

    move-result v9

    const/16 v17, 0x4

    aget v5, p1, v17

    const/4 v6, 0x7

    const v7, -0xa83f051

    move v1, v13

    move v2, v9

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->a(IIIIIII)I

    move-result v13

    const/16 v18, 0x5

    aget v5, p1, v18

    const/16 v6, 0xc

    const v7, 0x4787c62a

    move v1, v11

    move v2, v13

    move v3, v9

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->a(IIIIIII)I

    move-result v11

    const/16 v19, 0x6

    aget v5, p1, v19

    const/16 v6, 0x11

    const v7, -0x57cfb9ed

    move v1, v10

    move v2, v11

    move v3, v13

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->a(IIIIIII)I

    move-result v10

    const/16 v20, 0x7

    aget v5, p1, v20

    const/16 v6, 0x16

    const v7, -0x2b96aff

    move v1, v9

    move v2, v10

    move v3, v11

    move v4, v13

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->a(IIIIIII)I

    move-result v9

    const/16 v21, 0x8

    aget v5, p1, v21

    const/4 v6, 0x7

    const v7, 0x698098d8

    move v1, v13

    move v2, v9

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->a(IIIIIII)I

    move-result v13

    const/16 v22, 0x9

    aget v5, p1, v22

    const/16 v6, 0xc

    const v7, -0x74bb0851

    move v1, v11

    move v2, v13

    move v3, v9

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->a(IIIIIII)I

    move-result v11

    invoke-static {}, Lcom/jscape/a/f;->d()Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0xa

    aget v5, p1, v24

    const/16 v6, 0x11

    const v7, -0xa44f

    move v1, v10

    move v2, v11

    move v3, v13

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->a(IIIIIII)I

    move-result v10

    const/16 v25, 0xb

    aget v5, p1, v25

    const/16 v6, 0x16

    const v7, -0x76a32842

    move v1, v9

    move v2, v10

    move v3, v11

    move v4, v13

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->a(IIIIIII)I

    move-result v9

    const/16 v26, 0xc

    aget v5, p1, v26

    const/4 v6, 0x7

    const v7, 0x6b901122

    move v1, v13

    move v2, v9

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->a(IIIIIII)I

    move-result v13

    const/16 v27, 0xd

    aget v5, p1, v27

    const/16 v6, 0xc

    const v7, -0x2678e6d

    move v1, v11

    move v2, v13

    move v3, v9

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->a(IIIIIII)I

    move-result v11

    const/16 v28, 0xe

    aget v5, p1, v28

    const/16 v6, 0x11

    const v7, -0x5986bc72

    move v1, v10

    move v2, v11

    move v3, v13

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->a(IIIIIII)I

    move-result v10

    const/16 v29, 0xf

    aget v5, p1, v29

    const/16 v6, 0x16

    const v7, 0x49b40821

    move v1, v9

    move v2, v10

    move v3, v11

    move v4, v13

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->a(IIIIIII)I

    move-result v9

    aget v5, p1, v14

    const/4 v6, 0x5

    const v7, -0x9e1da9e

    move v1, v13

    move v2, v9

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->b(IIIIIII)I

    move-result v13

    aget v5, p1, v19

    const/16 v6, 0x9

    const v7, -0x3fbf4cc0

    move v1, v11

    move v2, v13

    move v3, v9

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->b(IIIIIII)I

    move-result v11

    aget v5, p1, v25

    const/16 v6, 0xe

    const v7, 0x265e5a51

    move v1, v10

    move v2, v11

    move v3, v13

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->b(IIIIIII)I

    move-result v10

    aget v5, p1, v12

    const/16 v6, 0x14

    const v7, -0x16493856

    move v1, v9

    move v2, v10

    move v3, v11

    move v4, v13

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->b(IIIIIII)I

    move-result v9

    aget v5, p1, v18

    const/4 v6, 0x5

    const v7, -0x29d0efa3

    move v1, v13

    move v2, v9

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->b(IIIIIII)I

    move-result v13

    aget v5, p1, v24

    const/16 v6, 0x9

    const v7, 0x2441453

    move v1, v11

    move v2, v13

    move v3, v9

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->b(IIIIIII)I

    move-result v11

    aget v5, p1, v29

    const/16 v6, 0xe

    const v7, -0x275e197f

    move v1, v10

    move v2, v11

    move v3, v13

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->b(IIIIIII)I

    move-result v10

    aget v5, p1, v17

    const/16 v6, 0x14

    const v7, -0x182c0438

    move v1, v9

    move v2, v10

    move v3, v11

    move v4, v13

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->b(IIIIIII)I

    move-result v9

    aget v5, p1, v22

    const/4 v6, 0x5

    const v7, 0x21e1cde6

    move v1, v13

    move v2, v9

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->b(IIIIIII)I

    move-result v13

    aget v5, p1, v28

    const/16 v6, 0x9

    const v7, -0x3cc8f82a

    move v1, v11

    move v2, v13

    move v3, v9

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->b(IIIIIII)I

    move-result v11

    aget v5, p1, v16

    const/16 v6, 0xe

    const v7, -0xb2af279

    move v1, v10

    move v2, v11

    move v3, v13

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->b(IIIIIII)I

    move-result v10

    aget v5, p1, v21

    const/16 v6, 0x14

    const v7, 0x455a14ed

    move v1, v9

    move v2, v10

    move v3, v11

    move v4, v13

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->b(IIIIIII)I

    move-result v9

    aget v5, p1, v27

    const/4 v6, 0x5

    const v7, -0x561c16fb

    move v1, v13

    move v2, v9

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->b(IIIIIII)I

    move-result v13

    aget v5, p1, v15

    const/16 v6, 0x9

    const v7, -0x3105c08

    move v1, v11

    move v2, v13

    move v3, v9

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->b(IIIIIII)I

    move-result v11

    aget v5, p1, v20

    const/16 v6, 0xe

    const v7, 0x676f02d9

    move v1, v10

    move v2, v11

    move v3, v13

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->b(IIIIIII)I

    move-result v10

    aget v5, p1, v26

    const/16 v6, 0x14

    const v7, -0x72d5b376

    move v1, v9

    move v2, v10

    move v3, v11

    move v4, v13

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->b(IIIIIII)I

    move-result v9

    aget v5, p1, v18

    const/4 v6, 0x4

    const v7, -0x5c6be

    move v1, v13

    move v2, v9

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->c(IIIIIII)I

    move-result v13

    aget v5, p1, v21

    const/16 v6, 0xb

    const v7, -0x788e097f

    move v1, v11

    move v2, v13

    move v3, v9

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->c(IIIIIII)I

    move-result v11

    aget v5, p1, v25

    const/16 v6, 0x10

    const v7, 0x6d9d6122

    move v1, v10

    move v2, v11

    move v3, v13

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->c(IIIIIII)I

    move-result v10

    aget v5, p1, v28

    const/16 v6, 0x17

    const v7, -0x21ac7f4

    move v1, v9

    move v2, v10

    move v3, v11

    move v4, v13

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->c(IIIIIII)I

    move-result v9

    aget v5, p1, v14

    const/4 v6, 0x4

    const v7, -0x5b4115bc

    move v1, v13

    move v2, v9

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->c(IIIIIII)I

    move-result v13

    aget v5, p1, v17

    const/16 v6, 0xb

    const v7, 0x4bdecfa9    # 2.9204306E7f

    move v1, v11

    move v2, v13

    move v3, v9

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->c(IIIIIII)I

    move-result v11

    aget v5, p1, v20

    const/16 v6, 0x10

    const v7, -0x944b4a0

    move v1, v10

    move v2, v11

    move v3, v13

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->c(IIIIIII)I

    move-result v10

    aget v5, p1, v24

    const/16 v6, 0x17

    const v7, -0x41404390

    move v1, v9

    move v2, v10

    move v3, v11

    move v4, v13

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->c(IIIIIII)I

    move-result v9

    aget v5, p1, v27

    const/4 v6, 0x4

    const v7, 0x289b7ec6

    move v1, v13

    move v2, v9

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->c(IIIIIII)I

    move-result v13

    aget v5, p1, v12

    const/16 v6, 0xb

    const v7, -0x155ed806

    move v1, v11

    move v2, v13

    move v3, v9

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->c(IIIIIII)I

    move-result v11

    aget v5, p1, v16

    const/16 v6, 0x10

    const v7, -0x2b10cf7b

    move v1, v10

    move v2, v11

    move v3, v13

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->c(IIIIIII)I

    move-result v10

    aget v5, p1, v19

    const/16 v6, 0x17

    const v7, 0x4881d05    # 3.2000097E-36f

    move v1, v9

    move v2, v10

    move v3, v11

    move v4, v13

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->c(IIIIIII)I

    move-result v9

    aget v5, p1, v22

    const/4 v6, 0x4

    const v7, -0x262b2fc7

    move v1, v13

    move v2, v9

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->c(IIIIIII)I

    move-result v13

    aget v5, p1, v26

    const/16 v6, 0xb

    const v7, -0x1924661b

    move v1, v11

    move v2, v13

    move v3, v9

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->c(IIIIIII)I

    move-result v11

    aget v5, p1, v29

    const/16 v6, 0x10

    const v7, 0x1fa27cf8

    move v1, v10

    move v2, v11

    move v3, v13

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->c(IIIIIII)I

    move-result v10

    aget v5, p1, v15

    const/16 v6, 0x17

    const v7, -0x3b53a99b

    move v1, v9

    move v2, v10

    move v3, v11

    move v4, v13

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->c(IIIIIII)I

    move-result v9

    aget v5, p1, v12

    const/4 v6, 0x6

    const v7, -0xbd6ddbc

    move v1, v13

    move v2, v9

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->d(IIIIIII)I

    move-result v12

    aget v5, p1, v20

    const/16 v6, 0xa

    const v7, 0x432aff97

    move v1, v11

    move v2, v12

    move v3, v9

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->d(IIIIIII)I

    move-result v11

    aget v5, p1, v28

    const/16 v6, 0xf

    const v7, -0x546bdc59

    move v1, v10

    move v2, v11

    move v3, v12

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->d(IIIIIII)I

    move-result v10

    aget v5, p1, v18

    const/16 v6, 0x15

    const v7, -0x36c5fc7

    move v1, v9

    move v2, v10

    move v3, v11

    move v4, v12

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->d(IIIIIII)I

    move-result v9

    aget v5, p1, v26

    const/4 v6, 0x6

    const v7, 0x655b59c3

    move v1, v12

    move v2, v9

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->d(IIIIIII)I

    move-result v12

    aget v5, p1, v16

    const/16 v6, 0xa

    const v7, -0x70f3336e

    move v1, v11

    move v2, v12

    move v3, v9

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->d(IIIIIII)I

    move-result v11

    aget v5, p1, v24

    const/16 v6, 0xf

    const v7, -0x100b83

    move v1, v10

    move v2, v11

    move v3, v12

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->d(IIIIIII)I

    move-result v10

    aget v5, p1, v14

    const/16 v6, 0x15

    const v7, -0x7a7ba22f

    move v1, v9

    move v2, v10

    move v3, v11

    move v4, v12

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->d(IIIIIII)I

    move-result v9

    aget v5, p1, v21

    const/4 v6, 0x6

    const v7, 0x6fa87e4f

    move v1, v12

    move v2, v9

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->d(IIIIIII)I

    move-result v12

    aget v5, p1, v29

    const/16 v6, 0xa

    const v7, -0x1d31920

    move v1, v11

    move v2, v12

    move v3, v9

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->d(IIIIIII)I

    move-result v11

    aget v5, p1, v19

    const/16 v6, 0xf

    const v7, -0x5cfebcec

    move v1, v10

    move v2, v11

    move v3, v12

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->d(IIIIIII)I

    move-result v10

    aget v5, p1, v27

    const/16 v6, 0x15

    const v7, 0x4e0811a1    # 5.7071418E8f

    move v1, v9

    move v2, v10

    move v3, v11

    move v4, v12

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->d(IIIIIII)I

    move-result v9

    aget v5, p1, v17

    const/4 v6, 0x6

    const v7, -0x8ac817e

    move v1, v12

    move v2, v9

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->d(IIIIIII)I

    move-result v12

    aget v5, p1, v25

    const/16 v6, 0xa

    const v7, -0x42c50dcb

    move v1, v11

    move v2, v12

    move v3, v9

    move v4, v10

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->d(IIIIIII)I

    move-result v11

    aget v5, p1, v15

    const/16 v6, 0xf

    const v7, 0x2ad7d2bb

    move v1, v10

    move v2, v11

    move v3, v12

    move v4, v9

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->d(IIIIIII)I

    move-result v10

    aget v5, p1, v22

    const/16 v6, 0x15

    const v7, -0x14792c6f

    move v1, v9

    move v2, v10

    move v3, v11

    move v4, v12

    invoke-direct/range {v0 .. v7}, Lcom/jscape/a/i;->d(IIIIIII)I

    move-result v0

    iget v1, v8, Lcom/jscape/a/i;->h:I

    add-int/2addr v1, v12

    iput v1, v8, Lcom/jscape/a/i;->h:I

    iget v1, v8, Lcom/jscape/a/i;->i:I

    add-int/2addr v1, v0

    iput v1, v8, Lcom/jscape/a/i;->i:I

    iget v0, v8, Lcom/jscape/a/i;->j:I

    add-int/2addr v0, v10

    iput v0, v8, Lcom/jscape/a/i;->j:I

    iget v0, v8, Lcom/jscape/a/i;->k:I

    add-int/2addr v0, v11

    iput v0, v8, Lcom/jscape/a/i;->k:I

    if-eqz v23, :cond_0

    new-array v0, v14, [I

    invoke-static {v0}, Lcom/jscape/util/aq;->b([I)V

    :cond_0
    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/jscape/a/i;->h:I

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/jscape/a/i;->i:I

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/jscape/a/i;->j:I

    return v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/jscape/a/i;->k:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/a/i;->l:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/a/i;->h:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/a/i;->i:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x2

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/jscape/a/i;->j:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/jscape/a/i;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
