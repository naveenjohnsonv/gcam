.class public Lcom/jscape/a/k;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/a/d;


# static fields
.field private static final a:I = 0x14

.field private static final b:Lcom/jscape/util/K;

.field private static final e:[Ljava/lang/String;


# instance fields
.field private final c:Lcom/jscape/a/l;

.field private final d:Lcom/jscape/a/m;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x9

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x25

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "?\u0012er&uHa\u000f\u0010@zF6`hL\u007fUhu)gE~\u000f"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x1a

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move/from16 v16, v4

    move v4, v3

    move/from16 v3, v16

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/a/k;->e:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/util/y;

    invoke-direct {v0}, Lcom/jscape/util/y;-><init>()V

    sput-object v0, Lcom/jscape/a/k;->b:Lcom/jscape/util/K;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    const/16 v13, 0x22

    const/16 v14, 0x36

    if-eqz v12, :cond_4

    if-eq v12, v7, :cond_3

    if-eq v12, v0, :cond_5

    const/4 v15, 0x3

    if-eq v12, v15, :cond_5

    const/4 v13, 0x4

    if-eq v12, v13, :cond_2

    const/4 v13, 0x5

    if-eq v12, v13, :cond_4

    const/16 v13, 0x8

    goto :goto_2

    :cond_2
    const/16 v13, 0x65

    goto :goto_2

    :cond_3
    const/16 v13, 0x17

    goto :goto_2

    :cond_4
    move v13, v14

    :cond_5
    :goto_2
    xor-int v12, v6, v13

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    new-instance v0, Lcom/jscape/a/l;

    invoke-direct {v0}, Lcom/jscape/a/l;-><init>()V

    invoke-direct {p0, v0}, Lcom/jscape/a/k;-><init>(Lcom/jscape/a/l;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/a/l;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/a/k;->c:Lcom/jscape/a/l;

    new-instance p1, Lcom/jscape/a/m;

    iget-object v0, p0, Lcom/jscape/a/k;->c:Lcom/jscape/a/l;

    invoke-direct {p1, v0}, Lcom/jscape/a/m;-><init>(Lcom/jscape/a/l;)V

    iput-object p1, p0, Lcom/jscape/a/k;->d:Lcom/jscape/a/m;

    return-void
.end method

.method public static d()Lcom/jscape/a/k;
    .locals 2

    new-instance v0, Lcom/jscape/a/k;

    new-instance v1, Lcom/jscape/a/l;

    invoke-direct {v1}, Lcom/jscape/a/l;-><init>()V

    invoke-direct {v0, v1}, Lcom/jscape/a/k;-><init>(Lcom/jscape/a/l;)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/16 v0, 0x14

    return v0
.end method

.method public a(B)Lcom/jscape/a/d;
    .locals 1

    iget-object v0, p0, Lcom/jscape/a/k;->d:Lcom/jscape/a/m;

    invoke-virtual {v0, p1}, Lcom/jscape/a/m;->a(B)V

    return-object p0
.end method

.method public a([B)Lcom/jscape/a/d;
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/jscape/a/k;->a([BII)Lcom/jscape/a/d;

    move-result-object p1

    return-object p1
.end method

.method public a([BII)Lcom/jscape/a/d;
    .locals 1

    iget-object v0, p0, Lcom/jscape/a/k;->d:Lcom/jscape/a/m;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/a/m;->a([BII)V

    return-object p0
.end method

.method public b()Lcom/jscape/a/d;
    .locals 1

    iget-object v0, p0, Lcom/jscape/a/k;->d:Lcom/jscape/a/m;

    invoke-virtual {v0}, Lcom/jscape/a/m;->a()V

    iget-object v0, p0, Lcom/jscape/a/k;->c:Lcom/jscape/a/l;

    invoke-virtual {v0}, Lcom/jscape/a/l;->a()V

    return-object p0
.end method

.method public b([B)[B
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/jscape/a/k;->b([BII)[B

    move-result-object p1

    return-object p1
.end method

.method public b([BII)[B
    .locals 0

    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/a/k;->a([BII)Lcom/jscape/a/d;

    invoke-virtual {p0}, Lcom/jscape/a/k;->c()[B

    move-result-object p1

    return-object p1
.end method

.method public c()[B
    .locals 4

    iget-object v0, p0, Lcom/jscape/a/k;->d:Lcom/jscape/a/m;

    invoke-virtual {v0}, Lcom/jscape/a/m;->b()V

    const/16 v0, 0x14

    new-array v0, v0, [B

    sget-object v1, Lcom/jscape/a/k;->b:Lcom/jscape/util/K;

    iget-object v2, p0, Lcom/jscape/a/k;->c:Lcom/jscape/a/l;

    invoke-virtual {v2}, Lcom/jscape/a/l;->b()I

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3}, Lcom/jscape/util/K;->a(I[BI)[B

    sget-object v1, Lcom/jscape/a/k;->b:Lcom/jscape/util/K;

    iget-object v2, p0, Lcom/jscape/a/k;->c:Lcom/jscape/a/l;

    invoke-virtual {v2}, Lcom/jscape/a/l;->c()I

    move-result v2

    const/4 v3, 0x4

    invoke-interface {v1, v2, v0, v3}, Lcom/jscape/util/K;->a(I[BI)[B

    sget-object v1, Lcom/jscape/a/k;->b:Lcom/jscape/util/K;

    iget-object v2, p0, Lcom/jscape/a/k;->c:Lcom/jscape/a/l;

    invoke-virtual {v2}, Lcom/jscape/a/l;->d()I

    move-result v2

    const/16 v3, 0x8

    invoke-interface {v1, v2, v0, v3}, Lcom/jscape/util/K;->a(I[BI)[B

    sget-object v1, Lcom/jscape/a/k;->b:Lcom/jscape/util/K;

    iget-object v2, p0, Lcom/jscape/a/k;->c:Lcom/jscape/a/l;

    invoke-virtual {v2}, Lcom/jscape/a/l;->e()I

    move-result v2

    const/16 v3, 0xc

    invoke-interface {v1, v2, v0, v3}, Lcom/jscape/util/K;->a(I[BI)[B

    sget-object v1, Lcom/jscape/a/k;->b:Lcom/jscape/util/K;

    iget-object v2, p0, Lcom/jscape/a/k;->c:Lcom/jscape/a/l;

    invoke-virtual {v2}, Lcom/jscape/a/l;->f()I

    move-result v2

    const/16 v3, 0x10

    invoke-interface {v1, v2, v0, v3}, Lcom/jscape/util/K;->a(I[BI)[B

    invoke-virtual {p0}, Lcom/jscape/a/k;->b()Lcom/jscape/a/d;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/a/k;->e:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/a/k;->c:Lcom/jscape/a/l;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/a/k;->d:Lcom/jscape/a/m;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
