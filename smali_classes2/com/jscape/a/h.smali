.class public Lcom/jscape/a/h;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/jscape/a/d;


# static fields
.field private static final a:I = 0x10

.field private static final b:Lcom/jscape/util/K;

.field private static final e:[Ljava/lang/String;


# instance fields
.field private final c:Lcom/jscape/a/i;

.field private final d:Lcom/jscape/a/j;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x9

    const/4 v4, -0x1

    move v5, v2

    :goto_0
    const/16 v6, 0x8

    const/4 v7, 0x1

    add-int/2addr v4, v7

    add-int/2addr v3, v4

    const-string v8, "NEl\u001a3W7\u0010X\u000f/!;O.P>\u0005\n|\u0006!Y?_"

    invoke-virtual {v8, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    array-length v9, v4

    move v10, v2

    :goto_1
    if-gt v9, v10, :cond_1

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v6}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v6, v5, 0x1

    aput-object v4, v1, v5

    const/16 v4, 0x19

    if-ge v3, v4, :cond_0

    invoke-virtual {v8, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    move v5, v6

    move v14, v4

    move v4, v3

    move v3, v14

    goto :goto_0

    :cond_0
    sput-object v1, Lcom/jscape/a/h;->e:[Ljava/lang/String;

    new-instance v0, Lcom/jscape/util/ac;

    invoke-direct {v0}, Lcom/jscape/util/ac;-><init>()V

    sput-object v0, Lcom/jscape/a/h;->b:Lcom/jscape/util/K;

    return-void

    :cond_1
    aget-char v11, v4, v10

    rem-int/lit8 v12, v10, 0x7

    if-eqz v12, :cond_7

    if-eq v12, v7, :cond_6

    if-eq v12, v0, :cond_5

    const/4 v13, 0x3

    if-eq v12, v13, :cond_4

    const/4 v13, 0x4

    if-eq v12, v13, :cond_3

    const/4 v13, 0x5

    if-eq v12, v13, :cond_2

    const/16 v12, 0x5a

    goto :goto_2

    :cond_2
    const/16 v12, 0x39

    goto :goto_2

    :cond_3
    const/16 v12, 0x5d

    goto :goto_2

    :cond_4
    const/16 v12, 0x67

    goto :goto_2

    :cond_5
    const/4 v12, 0x6

    goto :goto_2

    :cond_6
    const/16 v12, 0x6d

    goto :goto_2

    :cond_7
    const/16 v12, 0x6a

    :goto_2
    xor-int/2addr v12, v6

    xor-int/2addr v11, v12

    int-to-char v11, v11

    aput-char v11, v4, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    new-instance v0, Lcom/jscape/a/i;

    invoke-direct {v0}, Lcom/jscape/a/i;-><init>()V

    invoke-direct {p0, v0}, Lcom/jscape/a/h;-><init>(Lcom/jscape/a/i;)V

    return-void
.end method

.method public constructor <init>(Lcom/jscape/a/i;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/jscape/util/aq;->a(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/jscape/a/h;->c:Lcom/jscape/a/i;

    new-instance p1, Lcom/jscape/a/j;

    iget-object v0, p0, Lcom/jscape/a/h;->c:Lcom/jscape/a/i;

    invoke-direct {p1, v0}, Lcom/jscape/a/j;-><init>(Lcom/jscape/a/i;)V

    iput-object p1, p0, Lcom/jscape/a/h;->d:Lcom/jscape/a/j;

    return-void
.end method

.method public static d()Lcom/jscape/a/h;
    .locals 2

    new-instance v0, Lcom/jscape/a/h;

    new-instance v1, Lcom/jscape/a/i;

    invoke-direct {v1}, Lcom/jscape/a/i;-><init>()V

    invoke-direct {v0, v1}, Lcom/jscape/a/h;-><init>(Lcom/jscape/a/i;)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/16 v0, 0x10

    return v0
.end method

.method public a(B)Lcom/jscape/a/d;
    .locals 1

    iget-object v0, p0, Lcom/jscape/a/h;->d:Lcom/jscape/a/j;

    invoke-virtual {v0, p1}, Lcom/jscape/a/j;->a(B)V

    return-object p0
.end method

.method public a([B)Lcom/jscape/a/d;
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/jscape/a/h;->a([BII)Lcom/jscape/a/d;

    move-result-object p1

    return-object p1
.end method

.method public a([BII)Lcom/jscape/a/d;
    .locals 1

    iget-object v0, p0, Lcom/jscape/a/h;->d:Lcom/jscape/a/j;

    invoke-virtual {v0, p1, p2, p3}, Lcom/jscape/a/j;->a([BII)V

    return-object p0
.end method

.method public b()Lcom/jscape/a/d;
    .locals 1

    iget-object v0, p0, Lcom/jscape/a/h;->d:Lcom/jscape/a/j;

    invoke-virtual {v0}, Lcom/jscape/a/j;->a()V

    iget-object v0, p0, Lcom/jscape/a/h;->c:Lcom/jscape/a/i;

    invoke-virtual {v0}, Lcom/jscape/a/i;->a()V

    return-object p0
.end method

.method public b([B)[B
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/jscape/a/h;->b([BII)[B

    move-result-object p1

    return-object p1
.end method

.method public b([BII)[B
    .locals 0

    invoke-virtual {p0, p1, p2, p3}, Lcom/jscape/a/h;->a([BII)Lcom/jscape/a/d;

    invoke-virtual {p0}, Lcom/jscape/a/h;->c()[B

    move-result-object p1

    return-object p1
.end method

.method public c()[B
    .locals 4

    iget-object v0, p0, Lcom/jscape/a/h;->d:Lcom/jscape/a/j;

    invoke-virtual {v0}, Lcom/jscape/a/j;->b()V

    const/16 v0, 0x10

    new-array v0, v0, [B

    sget-object v1, Lcom/jscape/a/h;->b:Lcom/jscape/util/K;

    iget-object v2, p0, Lcom/jscape/a/h;->c:Lcom/jscape/a/i;

    invoke-virtual {v2}, Lcom/jscape/a/i;->b()I

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3}, Lcom/jscape/util/K;->a(I[BI)[B

    sget-object v1, Lcom/jscape/a/h;->b:Lcom/jscape/util/K;

    iget-object v2, p0, Lcom/jscape/a/h;->c:Lcom/jscape/a/i;

    invoke-virtual {v2}, Lcom/jscape/a/i;->c()I

    move-result v2

    const/4 v3, 0x4

    invoke-interface {v1, v2, v0, v3}, Lcom/jscape/util/K;->a(I[BI)[B

    sget-object v1, Lcom/jscape/a/h;->b:Lcom/jscape/util/K;

    iget-object v2, p0, Lcom/jscape/a/h;->c:Lcom/jscape/a/i;

    invoke-virtual {v2}, Lcom/jscape/a/i;->d()I

    move-result v2

    const/16 v3, 0x8

    invoke-interface {v1, v2, v0, v3}, Lcom/jscape/util/K;->a(I[BI)[B

    sget-object v1, Lcom/jscape/a/h;->b:Lcom/jscape/util/K;

    iget-object v2, p0, Lcom/jscape/a/h;->c:Lcom/jscape/a/i;

    invoke-virtual {v2}, Lcom/jscape/a/i;->e()I

    move-result v2

    const/16 v3, 0xc

    invoke-interface {v1, v2, v0, v3}, Lcom/jscape/util/K;->a(I[BI)[B

    invoke-virtual {p0}, Lcom/jscape/a/h;->b()Lcom/jscape/a/d;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/jscape/a/h;->e:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/jscape/a/h;->c:Lcom/jscape/a/i;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jscape/a/h;->d:Lcom/jscape/a/j;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
